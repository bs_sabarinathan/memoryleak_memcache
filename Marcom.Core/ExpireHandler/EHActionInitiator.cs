﻿using BrandSystems.Marcom.Core.ExpireHandler.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ExpireHandler
{
    internal class EHActionInitiator : IEHActionInitiator
    {
         #region Member Variables
        protected int _id;
        protected int _attributeID;
        protected int _entityID;
        protected int _actionSrcID;
        protected int _actionType;
        protected int _timeDuration;
        protected int _actionPlace;
        protected DateTime _executionDate;
        protected string _actionSrcValue;
        protected int  _status;
        protected DateTime _UpdatedOn;
        protected int _actionownerId;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EHActionInitiator() { }

        public EHActionInitiator(int pattributeID, int PentityID, int pactionSrcID, DateTime pexecutionDate, int ptimeDuration, int pactionPlace, int pactionType, string pactionSrcValue, int pstatus, int pactionownerId, DateTime pUpdatedOn)
        {
            this._attributeID = pattributeID;
            this._entityID = PentityID;
            this._actionSrcID = pactionSrcID;
            this._executionDate = pexecutionDate;
            this._timeDuration = ptimeDuration;
            this._actionPlace = pactionPlace;
            this._actionType = pactionType;
            this._actionSrcValue = pactionSrcValue;
            this._status = pstatus;
            this._actionownerId = pactionownerId;
            this._UpdatedOn = pUpdatedOn;
        }

        public EHActionInitiator(int pId)
        {
            this._id = pId;
        }

        #endregion
        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeID; }
            set { _attributeID = value; }

        }
        public virtual int ActionSrcID
        {
            get { return _actionSrcID; }
            set { _actionSrcID = value; }

        }
        public virtual int EntityID
        {
            get { return _entityID; }
            set { _entityID = value; }

        }

        public virtual DateTime ExecutionDate
        {
            get { return _executionDate; }
            set { _executionDate = value; }
        }
        public virtual int TimeDuration
        {
            get { return _timeDuration; }
            set { _timeDuration = value; }

        }
        public virtual int ActionPlace
        {
            get { return _actionPlace; }
            set { _actionPlace = value; }

        }

        public virtual int ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }

        }
        public virtual string ActionSrcValue
        {
            get { return _actionSrcValue; }
            set { _actionSrcValue = value; }
        }
        public virtual int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual int ActionownerId
        {
            get { return _actionownerId; }
            set { _actionownerId = value; }

        }

        public DateTime UpdatedOn
        {
            get { return _UpdatedOn; }
            set { _UpdatedOn = value; }

        }
       

        #endregion 				

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EHActionInitiator castObj = null;
            try
            {
                castObj = (EHActionInitiator)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
