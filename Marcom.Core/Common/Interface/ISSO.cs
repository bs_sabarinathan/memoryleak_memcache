﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface ISSO
    {
         string key { get; set; }
         string IV { get; set; }
         IList<string> Algorithmoption { get; set; }
         string AlgorithmValue { get; set; }
         IList<string> PaddingModeoption { get; set; }
         string PaddingModeValue { get; set; }
         IList<BrandSystems.Marcom.Core.Access.GlobalRole> UserGroupsoption { get; set; }
          string UserGroupsvalue { get; set; }
          string ClientIntranetUrl { get; set; }

         IList<string> CipherModeoption { get; set; }
         string CipherModeValue { get; set; }

         string SSOTimeDifference { get; set; }
         string SSODefaultGroups { get; set; }
         IList<string> SSOTokenModeoption { get; set; }
         string SSOTokenModeValue { get; set; }  
    }
}
