using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// ObjectiveConditionValue object for table 'PM_ObjectiveConditionValue'.
	/// </summary>
	
	internal class ObjectiveConditionValue : IObjectiveConditionValue 
	{
		#region Member Variables

        protected int _id;
		protected int _conditionid;
		protected int _value;
		
		
		#endregion
		
		#region Constructors
		public ObjectiveConditionValue() {}
			
		public ObjectiveConditionValue(int pConditionid, int pValue)
		{
			this._conditionid = pConditionid; 
			this._value = pValue; 
		}
				
		public ObjectiveConditionValue(int pConditionid)
		{
			this._conditionid = pConditionid; 
		}
		
		#endregion
		
		#region Public Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
		public int Conditionid
		{
			get { return _conditionid; }
			set { _conditionid = value; }
			
		}
		
		public int Value
		{
			get { return _value; }
			set { _value = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			ObjectiveConditionValue castObj = null;
			try
			{
				castObj = (ObjectiveConditionValue)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._conditionid == castObj.Conditionid );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _conditionid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
