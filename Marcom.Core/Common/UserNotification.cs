using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserNotification object for table 'CM_User_Notification'.
	/// </summary>
	
	internal class UserNotification : IUserNotification 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected int _entityid;
		protected int _actorid;
		protected DateTimeOffset _createdon;
		protected int _typeid;
		protected bool _isviewed;
		protected bool _issentinmail;        
        protected string _typename;
        protected string _attributename;
        protected string _fromvalue;
        protected string _tovalue;
        protected int _associtedEntityID;
        protected int _phaseID;
        protected string _stepID;
		
		
		#endregion
		
		#region Constructors
		public UserNotification() {}

        public UserNotification(int pUserid, int pEntityid, int pActorid, DateTimeOffset pCreatedOn, int pTypeid, bool pIsViewed, bool pIsSentInMail, string pTypeName, string pAttributeName, string pFromValue, string pToValue, int passocitedEntityID, int pphaseID, string pstepID)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._actorid = pActorid; 
			this._createdon = pCreatedOn; 
			this._typeid = pTypeid; 
			this._isviewed = pIsViewed; 
			this._issentinmail = pIsSentInMail;
            this._typename = pTypeName;
            this._attributename = pAttributeName;
            this._fromvalue = pFromValue;
            this._tovalue = pToValue;
            this._associtedEntityID = passocitedEntityID;
            this._phaseID = pphaseID;
            this._stepID = pstepID; 
		}
				
		public UserNotification(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Userid
		{
			get { return _userid; }
			set { _userid = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public int Actorid
		{
			get { return _actorid; }
			set { _actorid = value; }
			
		}
		
		public DateTimeOffset CreatedOn
		{
			get { return _createdon; }
			set { _createdon = value; }
			
		}
		
		public int Typeid
		{
			get { return _typeid; }
			set { _typeid = value; }
			
		}
		
		public bool IsViewed
		{
			get { return _isviewed; }
			set { _isviewed = value; }
			
		}
		
		public bool IsSentInMail
		{
			get { return _issentinmail; }
			set { _issentinmail = value; }
			
		}

        public virtual string TypeName
        {

            get { return _typename; }
            set {  _typename = value; }

        }

        public virtual string AttributeName
        {

            get { return _attributename; }
            set {  _attributename = value; }

        }

        public virtual string FromValue
        {

            get { return _fromvalue; }
            set {_fromvalue = value; }

        }

        public virtual string ToValue
        {

            get { return _tovalue; }
            set {_tovalue = value; }

        }
        public virtual int AssocitedEntityID
        {
            get { return _associtedEntityID; }
            set { _associtedEntityID = value; }

        }

        public virtual int PhaseID
        {
            get { return _phaseID; }
            set { _phaseID = value; }

        }
        public virtual string StepID
        {
            get { return _stepID; }
            set { _stepID = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserNotification castObj = null;
			try
			{
				castObj = (UserNotification)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
