using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFeedComment interface for table 'CM_Feed_Comment'.
    /// </summary>
    public interface IFeedComment
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Feedid
        {
            get;
            set;

        }

        int Actor
        {
            get;
            set;

        }

        string Comment
        {
            get;
            set;

        }

        string CommentedOn
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        string Usermail
        {
            get;
            set;

        }

        string Userimage
        {
            get;
            set;

        }
        

        #endregion
    }
}
