(function ($) {


    $.fn.AdjustHeightWidth = function () {
        ResizeMe();
        $(window).off("resize", ResizeMe);
        $(window).resize(ResizeMe);
        $(window).off("scroll", TreeScroll);
        var ResizeLeft = "if ($('#fixedHeight').length > 0) {" + "$('#left').css('min-height', '');" + "$('#separator').css('min-height', '');" + "$('#left').css('min-height', $('#fixedHeight').height() + 'px');" + "$('#separator').css('min-height', $('#fixedHeight').height() + 'px');" + "} else {" + "$('#left').css('min-height', '');" + "$('#separator').css('min-height', '');" + "$('#left').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight()) + 'px');" + "$('#separator').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight()) + 'px');" + "$('#mediaBankPage #left').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight() - $('#singlePageTitle').outerHeight() - 126) + 'px');" + "$('#mediaBankPage #separator').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight() - $('#singlePageTitle').outerHeight() - 126) + 'px');" + "}" + "$('#OverviewDiv #EntityFeedsdiv').height($('#OverviewDiv .span5').height() - 156);" + "$('#CostcentreOverviewDiv #EntityFeedsdiv').height($('#CostcentreOverviewDiv .span5').height() - 156);" + "$('#ObjectiveOverviewDiv #EntityFeedsdiv').height($('#ObjectiveOverviewDiv .span5').height() - 156);" + "$('#CalenderOverviewDiv #CalenderFeedsdiv').height($('#CalenderOverviewDiv .span5').height() - 156);" + "$('#cmssectionview #OverviewDiv #CmsPageFeedsdiv').height($('#cmssectionview #OverviewDiv .span5').height() - 156);" + "$('#AttachmentsDiv #left_folderTree').css('min-height', ($('#AttachmentsDiv #right_DamView').height()));" + "$('#damentityselect #left_entity_folderTree').css('min-height', ($('#damentityselect #entitycontentScroll').height()));" + "$('#attachfromdam #assetscontainerattach #left_entity_folderTree').css('min-height', ($('#attachfromdam #assetscontainerattach #entitycontentScroll').height()));" + "$('#damtask #left_attach_folderTree').css('min-height', ($('#damtask #contentScroll').height()));" + "$('#WorkTask .rightSection #Taskfeeddivforworktask').height($('#WorkTask .leftSection').height() - 80);" + "$('#ApprovalTask .rightSection #Taskfeeddivforapprovaltask').height($('#ApprovalTask .leftSection').height() - 80);" + "$('#ReviewTask .rightSection #feeddivforreviewTask').height($('#ReviewTask .leftSection').height() - 80);" + "$('#UnassignedTask .rightSection #feeddivforunassignedassigned').height($('#UnassignedTask .leftSection').height() - 80);" + "$('#NotifyWorkMyTaskPopup .rightSection #Notifyfeeddivworktaskforedit').height($('#NotifyWorkMyTaskPopup .leftSection').height() - 80);" + "$('#NotifyApprovalMyTaskPopup .rightSection #Notifyfeeddivapprovaltaskforedit').height($('#NotifyApprovalMyTaskPopup .leftSection').height() - 80);" + "$('#NotifyReviewMyTaskPopup .rightSection #Notifyfeeddivforreviewtaskforedit').height($('#NotifyReviewMyTaskPopup .leftSection').height() - 80);" + "$('#NotifyUnassignedTask .rightSection #notifyfeeddivforunassignedassigned').height($('#NotifyUnassignedTask .leftSection').height() - 80);" + "$('#TaskEditWorkTaskPopup .rightSection #feeddivforworktaskedit').height($('#TaskEditWorkTaskPopup .leftSection').height() - 80);" + "$('#TaskEditApprovalTaskPopup .rightSection #feeddivapprovaltaskedit').height($('#TaskEditApprovalTaskPopup .leftSection').height() - 80);" + "$('#TaskEditReviewTaskPopup .rightSection #feeddivforreviewtaskforedit').height($('#TaskEditReviewTaskPopup .leftSection').height() - 80);" + "$('#addTask.addTaskPopup .modal-body #lftSideForm').css('min-height', ($('#addTask.addTaskPopup .modal-body #rytSideAttachments').height()) - 10);";
        var i = setInterval(ResizeLeft, 500);

        var HandlePopup = "$('.modal:visible').each(function(index){var ind=$(this).attr('index');if(ind==undefined){ind=index;$(this).attr('index',ind);$(this).find('.modal-backdrop').attr('index',ind);}else{ind=parseInt(ind);};$(this).css({'margin-top':(-1*$(this).height()/2),'margin-left':(-1*$(this).width()/2),'z-index':(1050+ind*10)});if($(this).attr('data-width')!=undefined){$(this).width($(this).attr('data-width'))};if(!$(this).parent().is('body')){$(document.body).append($(this));};if(!$(this).find('.modal-backdrop').parent().is('body')){$(document.body).append($(this).find('.modal-backdrop').removeAttr('style'));};$('.modal:visible').find('.dropdown-toggle').click(function(e){var menu=$(this).next('.dropdown-menu'),mousex=e.pageX+20,mousey=e.pageY+20,menuWidth=menu.width(),menuHeight=menu.height(),menuVisX=$(window).width()-(mousex+menuWidth),menuVisY=$(window).height()-(mousey+menuHeight);if(menuVisX<20){menu.css({'left':'-89px'});}if(menuVisY<20){menu.css({'top':'auto','bottom':'100%',});$(this).children('.caret').removeClass('.caret').removeClass('.caret-reversed').addClass('caret caret-reversed');}});});if($('.modal:visible').length>$('.modal-backdrop:visible').length){$(document.body).append('<div class=\"modal-backdrop fade in\"></div>')};$('.modal-backdrop:visible').each(function(index){var ind=$(this).attr('index');if(ind==undefined){ind=index;$(this).attr('index',ind);}else{ind=parseInt(ind);};$(this).css('z-index',(1040+(ind&&1||0)+ind*10));});if($('.modal-backdrop:visible').length>$('.modal:visible').length){$('.modal-backdrop[index=\"'+parseInt(($('.modal-backdrop:visible').length-1))+'\"]').remove();};if($('.modal-scrollable').length>0){if($('.modal-scrollable').html().length == 0) {$('.modal-scrollable').remove();}};if ($('.modal:visible').length == 0)$('body').removeClass('modal-open');if ($('.modal:visible').length > 0)$('.modal:visible').removeClass('modal-overflow');";
        var j = setInterval(HandlePopup, 200);

    };

    

    function ResizeMe() {
        var contentHeight = $('html').outerHeight() - ($('#footer').outerHeight() + $('#header').outerHeight() + $('#FilterSettingsModal').outerHeight()) - 24;
        var dataHeight = $('html').outerHeight() - ($('#footer').outerHeight() + $('#header').outerHeight() + $('#content .contentHeader').outerHeight() + $('#FilterSettingsModal').outerHeight() + $('.title').outerHeight() + 1) - 24;
        $('#container').css('min-height', contentHeight + 'px');
        $('#mypagenotification').height(dataHeight - 120);
        var cmsTreeHeight = $('#cmsTreeHolder').outerHeight();
        if (cmsTreeHeight != null && cmsTreeHeight > dataHeight) {
            $('#cmssectionview').css("min-height", (cmsTreeHeight - 70));
            $('#DivCMSContentBind').css("min-height", (cmsTreeHeight - 123));
        } else {
            $('#cmssectionview').css("min-height", (dataHeight - 28));
            $('#DivCMSContentBind').css("min-height", (dataHeight - 81));
        }
        if ($('#treeHolder').length > 0) {
            $('#treeHolder').css('overflow', 'hidden').height(dataHeight);
            $('#ListContainer').css('overflow-y', 'scroll').height(dataHeight);
        } else if ($('#EntitiesTree').length > 0) {
            if ($('#ListContainer').length > 0) {
                $('#EntitiesTree').css('overflow', 'hidden').height(dataHeight);
                $('#ListContainer').css('overflow-y', 'scroll').height(dataHeight);
            } else if ($('#ganttChart').length > 0) {
                $('#EntitiesTree').css('overflow', 'hidden').height(dataHeight);
                $('#ganttChart .ganttview-data').height(dataHeight);
                $('#ganttChart .ganttview-list').height(dataHeight + 35);
                $('#ganttChart .ganttview-list-container').height(dataHeight + 35);
                $('#ganttChart .ganttview-list-container-data').height(dataHeight);
                $('#ganttChart .ganttview-grid').height(dataHeight);
                $('#ganttChart .ganttview-grid-row').height($('#ganttChart #GanttBlockContainer').height());
                $('#left').css('min-height', '');
                $('#separator').css('min-height', '');
                if ($('#SectionTabs').length > 0) {
                    var sectiondataHeight = dataHeight - 147;
                    $('#AttributeGroupInCustomTab #EntitiesTree').css('overflow', 'hidden').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-data').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list').height(sectiondataHeight + 35);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list-container').height(sectiondataHeight + 35);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list-container-data').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-grid').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-grid-row').height($('#ganttChart #GanttBlockContainer').height());
                }
            } else {
                $('#EntitiesTree').removeAttr("style");
                $(window).on("scroll", TreeScroll);
            }
        }
        $('#resultsearchvalues').height(dataHeight + 35);
        if ($('#left').length == 0) {
            $('#DefaultTasks').css('min-height', contentHeight + 'px');
        }
        if (($('.modal-footer #btnDropdownDynamic > ul.dropdown-menu').offset + $('.modal-footer #btnDropdownDynamic > ul.dropdown-menu').height) > $(window).height) {
            $('.modal-footer #btnDropdownDynamic').addClass('dropup');
        } else {
            $('.modal-footer #btnDropdownDynamic').removeClass('dropup');
        }
    };

    function TreeScroll() {
        $('#EntitiesTree').trigger("onTreeScroll");
    };
})(jQuery);
$(document).ready(function () {
    $(window).AdjustHeightWidth();
});