using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

	/// <summary>
	/// EntityAcl object for table 'AM_Entity_Acl'.
	/// </summary>
	internal class EntityAcl : IEntityAcl 
	{
		#region Member Variables

        protected int _id;
		protected int _roleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected int _featureid;
		protected int _accesspermission;
		
		
		#endregion
		
		#region Constructors
		public EntityAcl() {}
			
        public EntityAcl(int pId, int pRoleid, int pModuleid, int pEntityTypeid, int pFeatureid, int pAccessPermission)
		{
			 this._id = pId;
			this._roleid = pRoleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._featureid = pFeatureid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        
        public int Roleid
		{
			get { return _roleid; }
			set { _roleid = value; }
			
		}
		
		public int Moduleid
		{
			get { return _moduleid; }
			set { _moduleid = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
		
		public int Featureid
		{
			get { return _featureid; }
			set { _featureid = value; }
			
		}
		
		public int AccessPermission
		{
			get { return _accesspermission; }
			set { _accesspermission = value; }
			
		}
		
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			EntityAcl castObj = null;
			try
			{
				castObj = (EntityAcl)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
		
	}
		
}
