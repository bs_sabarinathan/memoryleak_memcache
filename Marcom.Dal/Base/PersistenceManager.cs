﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Planning;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using BrandSystems.Marcom.Dal.Common;
using BrandSystems.Marcom.Dal.User;
using BrandSystems.Marcom.Dal.Access;
using BrandSystems.Marcom.Dal.Metadata;
using System.IO;
using System.Web;
using System.Xml;
using NHibernate.Tool.hbm2ddl;
using BrandSystems.Marcom.Dal.Task;
using BrandSystems.Marcom.Dal.Report;
using BrandSystems.Marcom.Dal.DAM;
using BrandSystems.Marcom.Dal.CMS;
using BrandSystems.Marcom.Dal.ExpireHandler;
using System.Collections;


namespace BrandSystems.Marcom.Dal.Base
{
    public class PersistenceManager : IDisposable
    {
        private static PersistenceManager _instance = new PersistenceManager();
        //private ISessionFactory _sessionFactory = null;
        private ISessionFactory[] _sessionFactory = new ISessionFactory[10];

        /*
        //private ITransaction transaction;
        private ReportRepository _reportRepository = null;

        private PlanningRepository _planningRepository = null;

        private CommonRepository _commonRepository = null;

        private UserRepository _userRepository = null;

        private AccessRepository _accessRepository = null;

        private MetadataRepository _metadataRepository = null;

        private TaskRepository _taskRepository = null;

        private DamRepository _damRepository = null;

        private CmsRepository _cmsRepository = null;
        */
        
        private ReportRepository[] _reportRepository = new ReportRepository[10];

        private PlanningRepository[] _planningRepository = new PlanningRepository[10];

        private CommonRepository[] _commonRepository = new CommonRepository[10];

        private UserRepository[] _userRepository = new UserRepository[10];

        private AccessRepository[] _accessRepository = new AccessRepository[10];

        private MetadataRepository[] _metadataRepository = new MetadataRepository[10];

        private TaskRepository[] _taskRepository = new TaskRepository[10];

        private DamRepository[] _damRepository = new DamRepository[10];

        private CmsRepository[] _cmsRepository = new CmsRepository[10];

        private ExpireHandlerRepository[] _expirehandlerRepository = new ExpireHandlerRepository[10];

        public static PersistenceManager Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Configures NHibernate and creates a member-level session factory.
        /// </summary>
        public void Initialize()
        {
             
            /*
            // Initialize
            Configuration cfg = new Configuration();

             cfg.Configure("E:/1.cfg.xml");

            // Add class mappings to configuration object
            Readingxmlfiles(cfg);
            cfg.AddAssembly(this.GetType().Assembly);

            // Create session factory from configuration object
            _sessionFactory = cfg.BuildSessionFactory();

            _planningRepository = new PlanningRepository(_sessionFactory);

            _commonRepository = new CommonRepository(_sessionFactory);

            _userRepository = new UserRepository(_sessionFactory);

            _accessRepository = new AccessRepository(_sessionFactory);
            
            _metadataRepository = new MetadataRepository(_sessionFactory);

            _taskRepository = new TaskRepository(_sessionFactory);

            _taskRepository = new TaskRepository(_sessionFactory);

            _reportRepository = new ReportRepository(_sessionFactory);

            _damRepository = new DamRepository(_sessionFactory);

            _cmsRepository = new CmsRepository(_sessionFactory);
           
             */


            
             // Initialize
             Configuration[] configAry = new Configuration[10];

             
             string TenantInfoPath = System.Configuration.ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
             TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
             XmlDocument xdcDocument = new XmlDocument();
             xdcDocument.Load(TenantInfoPath);
             XmlElement xelRoot = xdcDocument.DocumentElement;
             XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
             int tenantid = 0;
             string TenantsFilePath = "";
             foreach (XmlNode xndNode in xnlNodes)
             { 
                string nhbconfigpath = System.Configuration.ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "Tenants\\" + xndNode["NhbConfig"].InnerText;
                tenantid = Int16.Parse(xndNode["TenantID"].InnerText);
                TenantsFilePath = xndNode["FilePath"].InnerText;
                configAry[tenantid] = new Configuration();
                configAry[tenantid].Configure(nhbconfigpath);
                Readingxmlfiles(configAry[tenantid], TenantsFilePath);
                configAry[tenantid].AddAssembly(this.GetType().Assembly);

                _sessionFactory[tenantid] = configAry[tenantid].BuildSessionFactory();
                _planningRepository[tenantid] = new PlanningRepository(_sessionFactory[tenantid]);
                _commonRepository[tenantid] = new CommonRepository(_sessionFactory[tenantid]);
                _userRepository[tenantid] = new UserRepository(_sessionFactory[tenantid]);
                _accessRepository[tenantid] = new AccessRepository(_sessionFactory[tenantid]);
                _metadataRepository[tenantid] = new MetadataRepository(_sessionFactory[tenantid]);
                _taskRepository[tenantid] = new TaskRepository(_sessionFactory[tenantid]);
                _taskRepository[tenantid] = new TaskRepository(_sessionFactory[tenantid]);
                _reportRepository[tenantid] = new ReportRepository(_sessionFactory[tenantid]);
                _damRepository[tenantid] = new DamRepository(_sessionFactory[tenantid]);
                _cmsRepository[tenantid] = new CmsRepository(_sessionFactory[tenantid]);
                _expirehandlerRepository[tenantid] = new ExpireHandlerRepository(_sessionFactory[tenantid]);
        }              

        }

        public void BeginTransaction(int tenantid)
        {
            ISession session = _sessionFactory[tenantid].OpenSession();
            session.CacheMode = CacheMode.Ignore;
            session.FlushMode = FlushMode.Commit;
            CurrentSessionContext.Bind(session);
            session.BeginTransaction();
        }

        public void CommitTransaction(int TenantID)
        {
            try
            {
                if (CurrentSessionContext.HasBind(_sessionFactory[TenantID]))
                {
                    using (ISession session = _sessionFactory[TenantID].GetCurrentSession())
                    {
                        session.Transaction.Commit();
                        CurrentSessionContext.Unbind(_sessionFactory[TenantID]);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void RollbackTransaction(int TenantID)
        {
            if (_sessionFactory != null)
            {
                if (CurrentSessionContext.HasBind(_sessionFactory[TenantID]))
                {
                    using (ISession session = _sessionFactory[TenantID].GetCurrentSession())
                    {
                        session.Transaction.Rollback();
                        CurrentSessionContext.Unbind(_sessionFactory[TenantID]);
                    }
                }
            }
        }
        public void Readingxmlfiles(Configuration cfg, string TenantFilePaths)
        {
            
            string mappingfilesPath = System.Configuration.ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + TenantFilePaths; 
            mappingfilesPath = mappingfilesPath + "DynamicMappingfiles\\";
            foreach (string file in Directory.EnumerateFiles(mappingfilesPath, "*.xml"))
            {
              cfg.AddFile(new FileInfo(file));
            }
         
        }

        public void Close()
        {

        }

        //public void Dispose(bool isDisposing)
        //{
        //    if (isDisposing)
        //    {
        //        this.Close();

        //        if (this.transaction != null)
        //        {
        //            this.transaction.Dispose();
        //            this.transaction = null;
        //        }

        //        if (this.session != null)
        //        {
        //            this.session.Dispose();
        //            this.session = null;
        //        }

        //        this.interceptor = null;

        //        if (Thread.GetData(mysessions) == this)
        //            Thread.SetData(mysessions, null);
        //    }
        //}

        public void Dispose()
        {
            //this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        //Planning Manager
        public PlanningRepository[] PlanningRepository
        {
            get { return _planningRepository; }
        }

        //Common Manager
        public CommonRepository[] CommonRepository
        {
            get { return _commonRepository; }
        }

        //User Manager
        public UserRepository[] UserRepository
        {
            get { return _userRepository; }
        }

        //Report Manager
        public ReportRepository[] ReportRepository
        {
            get { return _reportRepository; }
        }

        //Access Manager
        public AccessRepository[] AccessRepository
        {
            get { return _accessRepository; }
        }

        public MetadataRepository[] MetadataRepository
        {
            get { return _metadataRepository; }
        }

        public TaskRepository[] TaskRepository
        {
            get { return _taskRepository; }
        }

        public DamRepository[] DamRepository
        {
            get { return _damRepository; }
        }
        public CmsRepository[] CmsRepository
        {
            get { return _cmsRepository; }
        }

        public ExpireHandlerRepository[] ExpireHandlerRepository
        {
            get { return _expirehandlerRepository; }
        }
    }
}
