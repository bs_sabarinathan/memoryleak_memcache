﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.User.Model
{
    public partial class InsertUserDetailsDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
        protected int _IUserID;
        protected string _IEmail;
        protected byte[] _IPassword;
        protected string _IPasswordSalt;
        protected DateTime _ICreationTime;
        protected string _IPAddress;
        protected string _IPNumber;
		protected string _IOS;
	
        protected bool _bIsDeleted;
		protected bool _bIsChanged;
       


		#endregion
		
		#region Constructors
		public InsertUserDetailsDao() {}

        public InsertUserDetailsDao(int UserID, string Email, byte[] Password, string PasswordSalt, DateTime CreationTime, string IPAddress, string IPNumber, string OS)
		{
            this._IUserID = UserID;
            this._IEmail=Email;
            this._IPassword=Password;
            this._IPasswordSalt = PasswordSalt;
            this._ICreationTime = CreationTime;
            this._IPAddress = IPAddress;
            this._IPNumber = IPNumber;
            this._IOS = OS;
           
			
            
		}

        public InsertUserDetailsDao(int Id)
		{
			this._id = Id; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int UserID
        {
            get { return _IUserID; }
            set { _IUserID = value; }

        }

        public virtual string Email
        {
            get { return _IEmail; }
            set
            {
                _bIsChanged |= (_IEmail != value);
                _IEmail = value;
            }

        }

        public virtual byte[] Password
        {
            get { return _IPassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_IPassword != value);
                _IPassword = value;
            }

        }


        public virtual string PasswordSalt
        {
            get { return _IPasswordSalt; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_IPasswordSalt != value);
                _IPasswordSalt = value;
            }

        }
        public virtual DateTime CreationTime
        {
            get { return _ICreationTime; }
            set
            {
                _ICreationTime = value;
            }

        }
       
        public virtual string IPAddress
		{
            get { return _IPAddress; }
			set 
			{
                _bIsChanged |= (_IPAddress != value);
                _IPAddress = value; 
			}
			
		}



        public virtual string IPNumber
        {
            get { return _IPNumber; }
            set
            {
                _bIsChanged |= (_IPNumber != value);
                _IPNumber = value;
            }

        }

        public virtual string OS
        {
            get { return _IOS ; }
            set
            {
                _bIsChanged |= (_IOS != value);
                _IOS = value;
            }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}


		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string InsertUserDetailsDao = "InsertUserDetailsDao";			
			public const string Id = "Id";
            public const string UserID = "UserID";
            public const string Email = "Email";
            public const string Password = "Password";
            public const string PasswordSalt = "PasswordSalt";
            public const string CreationTime = "CreationTime";
            public const string IPAddress = "IPAddress";
            public const string IPNumber = "IPNumber";
            public const string OS = "OS";
            
           
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string InsertUserDetailsDao = "InsertUserDetailsDao";
            public const string Id = "Id";
            public const string UserID = "UserID";
            public const string Email = "Email";
            public const string Password = "Password";
            public const string PasswordSalt = "PasswordSalt";
            public const string CreationTime = "CreationTime";
            public const string IPAddress = "IPAddress";
            public const string IPNumber = "IPNumber";
            public const string OS = "OS";

		}

		#endregion
    }
}
