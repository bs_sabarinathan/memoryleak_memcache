﻿app
.directive('multiselectDropdown', [function () { return function (scope, element, attributes) { element = $(element[0]); element.multiselect({ enableFiltering: true, enableCaseInsensitiveFiltering: true, includeSelectAllOption: false, dropRight: true }); scope.$watch(function () { return element[0].length; }, function () { element.multiselect('rebuild'); }); scope.$watchCollection(attributes.ngModel, function () { element.multiselect('refresh'); }); scope.$on("$destroy", function () { $(window).off("resize.Viewport"); }); } }])
.directive('multiselectpartial', [function () {
    return function (scope, element, attributes)
    {
        element = $(element[0]); element.multiselectpartial({});
        scope.$watch(function () { return element[0].length; }, function () {
            element.multiselectpartial('rebuild');
        });
        scope.$watch(attributes.ngModel, function () {
            element.multiselectpartial('refresh');
        });
        scope.$on("$destroy", function () {
            $(window).off("resize.Viewport");
        });
    }
}]);