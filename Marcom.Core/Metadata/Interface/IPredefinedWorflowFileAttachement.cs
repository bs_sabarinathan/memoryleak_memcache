﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IPredefinedWorflowFileAttachement
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int PredefinedID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        DateTime Createdon
        {
            get;
            set;

        }

        Guid Fileguid
        {
            get;
            set;

        }

        string Extension
        {
            get;
            set;

        }

        long Size
        {
            get;
            set;

        }

        string MimeType
        {
            get;
            set;

        }

        int OwnerID
        {
            get;
            set;
        }

        int VersionNo
        {
            get;
            set;
        }

        #endregion
    }
}
