﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// AttributeSequenceDao object for table 'MM_AttributeSequence'.
    /// </summary>
    /// 
    public partial class AttributeSequenceDao : BaseDao, ICloneable
    {
        #region public Properties
        
        public virtual int Id { get; set; }
        public virtual int AttributeID { get; set; }
        public virtual int StartWithValue { get; set; }
        public virtual string Pattern { get; set; }
        
        #endregion

        
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
