﻿(function (ng, app) {
    "use strict";

    function muiDAMasseteditCtrl($window, $location, $timeout, $scope, $cookies, $resource, AssetEditService, $filter, $stateParams, $rootScope, $compile, $sce, $translate, $modalInstance, params, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imageBaseUrlcloud = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imageBaseUrlcloud = cloudpath;
        }
        AssetEditService.GetAllExtensionTypesforDAM().then(function (data) {
            if (data.Response != null) {
                $scope.DAMtypeswithExtAssetedit = data.Response.m_Item1;
            }
        });
        $scope.AssetAccess = { "ddlGlobalRole": [], "accesrolechange": 0, "AssetName": "", "name": '' };

        $scope.SendMailWithMdata = {};
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        var pre_call = "";
        $scope.Calanderopen = function ($event, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope[pre_call] = false;
            $scope[Call_from] = true;
            pre_call = Call_from;
        };
        var timerObj = {};
        $scope.PerioddirectiveCalanderopen = function ($event, attrid, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") $scope.fields["PeriodStartDateopen_" + attrid] = true;
            else $scope.fields["PeriodEndDateopen_" + attrid] = true;
        };
        $scope.EntityUserListInAssetEdit = [];
        $scope.PersonalUserIdsInAssetEdit = [];
        var IsPageLoad = true;
        var PagenoforScroll = 0;
        $scope.EditversionID = 0;
        $scope.newsFeedinfo = [];
        $scope.Dropdown = [];
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.Editverionfileguid = '';
        $scope.GlobalRoles = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.treeTexts = {};
        $scope.fieldoptions = [];
        $scope.OwnerList = [];
        $scope.AssetPreviewObj = {
            AssetId: 0,
            AssetCeatedOn: "",
            AssetUpdatedOn: "",
            PublisherID: 0,
            PublishedOn: "",
            VersionCeatedOn: ""
        };
        $scope.AssetCreated = 0;
        $scope.items = [];
        $scope.curEntityID = 0;
        $scope.ProductionEntityID = 0;
        $scope.LinkedAsset = false;
        $scope.oldAssetTypeID = 0;
        $scope.owner = {};
        $scope.NormalDropdownCaption = {};
        $scope.NormalDropdownCaptionObj = [];
        $scope.normaltreeSources = {};
        $scope.notReadOnly = true;
        $scope.AssetlastUpdatedOn = '';
        $scope.AssetTypechange = false;
        $scope.ProcessType = 0;
        $scope.videofileguid = '';
        $scope.treeSources = {};
        $scope.DropDownTreePricing = {};
        $scope.PercentageVisibleSettings = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.atributesRelationList = {};
        $scope.treePreviewObj = {};
        $scope.AssetActiveFileID = 0;
        $scope.Ispdf = 0;
        $scope.onerrorfileguid = '';
        $scope.assetEditLinkDetails = {
            folderName: "",
            publishedon: "",
            publishedby: "",
            linkdetails: ""
        };
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.selAssetType = [];
        $scope.tempHolder = '';
        $scope.AssetAccess.AssetName = '-';
        $scope.previousAssetdyn_Count = '';
        $scope.PreviousAssetTypeCategPath = '-';
        $scope.previousAssetdyn_Count = '-';
        $scope.PreviousDAMtypesExtAssetedit = '';
        $scope.previousassetType = '';
        var perioddates = [];
        var tempNewsFeedHolder = [];
        var periodforTypeEdit = [];
        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaption = {};
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        var FeedInitiated = false;
        var Cusratiocheckbox = null;
        $scope.UserId = $cookies['UserId'];
        var presentassetid = '';
        var jcrop_api;
        var wRatio;
        var hRatio;
        //$scope.DAMtypeswithExtAssetedit = '';

        //function GetAllExtensionTypesforDAM() {
        //AssetEditService.GetAllExtensionTypesforDAM().then(function(data) {
        //	if (data.Response != null) {
        //		$scope.DAMtypeswithExtAssetedit = data.Response.m_Item1;
        //	}
        //});
        //}
        $scope.EditNext = function () {
            $scope.processname = "Edit Asset";
            $scope.processrequestname = "Loading Next Asset";
            IsPageLoad = false;
            $scope.AssetTypechange = false;
            PagenoforScroll = 0;
            presentassetid = $scope.AssetEditID;
            $scope.Category = -1;
            $scope.ActiveVersionNo = ''
            $scope.videofileguid = '';
            var selAsset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                return e.AssetUniqueID == $scope.AssetEditID;
            });
            GlobalAssetEditID = selAsset[0].ActiveFileID;
            var indexofasset = $scope.AssetFilesObj_Temp.AssetFiles.indexOf(selAsset[0]);
            var temp = $scope.AssetFilesObj_Temp.AssetFiles[indexofasset + 1];
            if (temp != undefined) {
                $scope.$broadcast('openassetpopup', temp.AssetUniqueID, false, false, $scope.processname);
                $scope.LoadAssetNewsFeedBlock(temp.AssetUniqueID);
            }
        }
        $scope.closeView = function () {
            $('#diveditPrev').removeClass('firstRecord');
            $('#diveditNext').removeClass('lastRecord');
            $modalInstance.dismiss('cancel');
        }
        $scope.FileIdForAssetCategory = "";
        $scope.AssetCategory = {};
        $scope.cntChecks = 1;
        $scope.EditPrevious = function () {
            $scope.processname = "Edit Asset";
            $scope.processrequestname = "Loading Previous Asset";
            IsPageLoad = false;
            $scope.AssetTypechange = false;
            PagenoforScroll = 0;
            presentassetid = $scope.AssetEditID;
            $scope.Category = -1;
            $scope.ActiveVersionNo = ''
            $scope.videofileguid = '';
            var selAsset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                return e.AssetUniqueID == $scope.AssetEditID;
            });
            GlobalAssetEditID = selAsset[0].ActiveFileID;
            var indexofasset = $scope.AssetFilesObj_Temp.AssetFiles.indexOf(selAsset[0]);
            var temp = $scope.AssetFilesObj_Temp.AssetFiles[indexofasset - 1];
            if (temp != undefined) {
                $scope.$broadcast('openassetpopup', temp.AssetUniqueID, false, false, $scope.processname);
                $scope.LoadAssetNewsFeedBlock(temp.AssetUniqueID);
            }
        }
        $scope.Versioning = function () { }
        $scope.Assetdyn_Cont = '';
        $scope.assetfiles = [];
        $scope.DAMattributesRelationList = [];
        $scope.ActiveVersionNo = '';
        $scope.AssetEditID = 0;
        $scope.mediaGeneratorAssetDetails = {};
        $scope.assettypeidfrbtn = 0;
        $scope.assetFolderid = 0;
        $scope.assetEntityId = 0;
        $scope.imageunitFormat = '';
        $scope.convertWidth = 0;
        $scope.convertHeight = 0;
        $scope.ActiveVersionSize = 0;
        $scope.ActiveVersionDpi = 0;
        $scope.imageunitshortFormat = 'px';
        $scope.Category = -1;
        var frmNotification = false;
        $scope.AssetTypeID_temp = false;
        $scope.AssetPublish = false;
        $scope.videofileguid = '';
        var damviewtype = '';
        $scope.selectAssetType = function () {
            $scope.AssetTypechange = true;
            $scope.changeAssetType($scope.assetType)
        };
        timerObj.openasseteditTimer = $timeout(function () {
            openasseteditpopup(params.AssetID, params.IsLock, params.isNotify, params.viewtype);
        }, 100);
        $scope.$on('openassetpopup', function (event, assetid, IsLock, Isnotify, viewtype) {
            openasseteditpopup(assetid, IsLock, Isnotify, viewtype);
        });
        $scope.GetEntityMembers = function () {
            AssetEditService.GetMember(params.ID).then(function (member) {
                $scope.PersonalUserIdsInAssetEdit = [];
                $scope.EntityUserListInAssetEdit = [];
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserListInAssetEdit.length > 0) {
                                $.each($scope.EntityUserListInAssetEdit, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserListInAssetEdit.push(value);
                            } else $scope.EntityUserListInAssetEdit.push(value);
                        }
                    });
                }
            });
        }

        function openasseteditpopup(assetid, IsLock, Isnotify, viewtype) {

            damviewtype = viewtype;
            var frmNotification = false;
            $scope.notReadOnly = (IsLock == true) ? false : true;
            $("#AssetEditpopup").modal("show");
            $scope.AssetEditID = assetid;
            $scope.Islock_temp = IsLock;
            GetAssetAttribuuteDetails(assetid);
            frmNotification = Isnotify;
            $scope.EntityIsLock = IsLock;
            $scope.Rolename = [];
            $scope.GlobalRoles = [];
            $scope.AssetAccess.accesrolechange = 0;
            AssetEditService.GetAssetAccess().then(function (getassetaccess) {
                $scope.GlobalRoles = getassetaccess.Response;
            });
            if (frmNotification == false) {
                GlobalAssetEditID = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                    return e.AssetUniqueID == $scope.AssetEditID;
                })[0].ActiveFileID;
            } else {
                GlobalAssetEditID = assetid;
            }
        }
        $scope.OlAssetID = 0;
        $scope.ActiveVersion = [];
        $scope.AssetRolesCaption = "-";
        $scope.showspan = false;
        $scope.DisableEnableforLink = true;
        $scope.DisableEnableAssetRole = true;
        $scope.lockmenu = true;
        $scope.EntityIsLock = false;
        $scope.ShowpublishIcon = true;
        $scope.IsShow = false;
        $scope.UnPublish = false;
        $scope.HideDuplicate = true;
        $scope.Isassetlibrary = false;
        $scope.IsShowInAssetlibrary = false;
        var mediaAssetApproved = false;

        function GetAssetActiveFileinfo_edit(assetid, assetfiles, activefileid) {
            var activefiledets = $.grep(assetfiles, function (e) {
                return e.ID == activefileid
            })[0];
            if (activefiledets != "" && activefiledets != undefined) {
                $scope.IsImgInfo = true;
                var generatedids = activefiledets;
                var Activefileexn = activefiledets.Extension;
                $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                if ($scope.Activefileformat == "pdf") $scope.Ispdf = 1;
                var addinfo = (activefiledets.Additionalinfo != "") ? JSON.parse(activefiledets.Additionalinfo) : "";
                if (activefiledets.Size != undefined) {
                    $scope.orgSize = parseSize(activefiledets.Size);
                    $scope.ActiveVersionSize = parseSize(activefiledets.Size);
                    $scope.IsSize = true;
                }
                if (addinfo != undefined && addinfo != "") {
                    $scope.orgWidth = addinfo.Width;
                    $scope.orgHeight = addinfo.Height;
                    $scope.orgDpi = addinfo.Dpi;
                    $scope.ActiveVersionDpi = addinfo.Dpi;
                    $scope.IsResolution = true;
                    $scope.IsDPI = true;
                    $scope.displayimgUnitmenu = false;
                    if (addinfo.FrameRate != "" && addinfo.FrameRate != null) $scope.FrameRate = addinfo.FrameRate.replace('fps', '');
                    else $scope.FrameRate = 0;
                    getassetdimensionalunitsettings();
                    timerObj.imageunitchangetimer = $timeout(function () {
                        $scope.imageunitchange($scope.userDimensionUnit);
                    }, 100);
                    if ($scope.ActiveVersionDpi > 0 || $scope.FrameRate > 0) $scope.IsDimensioninfo = true;
                } else {
                    $scope.orgWidth = 0;
                    $scope.orgHeight = 0;
                    $scope.orgDpi = 0;
                    $scope.ActiveVersionDpi = 0;
                    $scope.IsResolution = false;
                    $scope.IsDPI = false;
                    $scope.IsDimensioninfo = false;
                }
            } else {
                $scope.IsImgInfo = false;
            }
        }

        function GetAssetAttribuuteDetails(assetid) {
            clearScopes();
            $rootScope.currentPathName = $location.path().replace('/', '');
            $scope.cntChecks = 1;
            var SearchPage = $rootScope.currentPathName;
            SearchPage = SearchPage.split("/");
            $scope.IsSearchpage = SearchPage[SearchPage.length - 1];
            if ($rootScope.currentPathName === "mui/marcommediabank" || $rootScope.currentPathName === "mediabank") $scope.ShowpublishIcon = false;
            else $scope.ShowpublishIcon = true;
            if ($rootScope.currentPathName === "mui/marcommediabank" || $scope.IsSearchpage === "customsearchresult" || $scope.IsSearchpage === "customsearchresults" || $rootScope.currentPathName === "mui/marcommediabank") {
                $scope.Isassetlibrary = true;
                $scope.IsShowInAssetlibrary = true;
                $scope.AssetTypechange = false;
            } else {
                $scope.Isassetlibrary = false;
                $scope.IsShowInAssetlibrary = false;
            }
            $scope.assettypeidfrbtn = 0;
            perioddates = [];
            //GetAllExtensionTypesforDAM();
            $scope.AssetTypechange = false;
            PagenoforScroll = 0;
            $("#linewsFeeds").removeClass("active");
            $("#limetaData").removeClass("active");
            $("#limetaData").addClass("active");
            $("#metaData").removeClass("active");
            $("#newsFeeds").removeClass("active");
            $("#metaData").addClass("active");
            $("#lifilemetaData").removeClass("active");
            $("#filemetaData").removeClass("active")
            Cropdefault();
            $scope.RoleIdsaved = [];
            $scope.editable = '';
            if (assetid !== undefined) {
                AssetEditService.GetAttributeDetails(assetid).then(function (attrList) {
                    if (attrList.Response != null) {
                        $scope.DocID = 0;
                        params.ID = attrList.Response.EntityID;
                        $timeout(function () { $scope.GetEntityMembers(); }, 100);
                        $scope.documentId = "";
                        $scope.IsTemplate = attrList.Response.MediaGeneratorData == null ? false : true;
                        if ($scope.IsTemplate) {
                            $scope.TemplateAsset.templateType = attrList.Response.MediaGeneratorData.TemplateType;
                            $scope.TemplateAsset.docversionID = attrList.Response.MediaGeneratorData.DocVersionID;
                            $scope.DocID = attrList.Response.MediaGeneratorData.DocID;
                            $scope.documentId = attrList.Response.MediaGeneratorData.Jpeg;
                            $scope.TemplateAsset.IsApprove = attrList.Response.MediaGeneratorData.Approved;
                            AssetEditService.getCurrentTemplateAssetInfo($scope.DocID).then(function (templAstRes) {
                                var TemplateAssetInfo = templAstRes.Response;
                                $scope.TemplateAsset.serverUrl = TemplateAssetInfo.ServerURL;
                                $scope.TemplateAsset.IsEnableHighResPDF = TemplateAssetInfo.EnableHighResPDF;
                            });
                        }
                        mediaAssetApproved = attrList.Response.MediaGeneratorData == null ? false : attrList.Response.MediaGeneratorData.Approved;
                        GetAssetActiveFileinfo_edit(assetid, attrList.Response.Files, attrList.Response.ActiveFileID);
                        getuserassetdimensionalunitsettings();
                        $scope.OlAssetID = assetid;
                        $scope.oldAssetTypeID = attrList.Response.AssetTypeid;
                        $scope.assettypeidfrbtn = attrList.Response.AssetTypeid;
                        $scope.assetType = attrList.Response.AssetTypeid;
                        $scope.previousassetType = $scope.assetType;
                        $scope.AssetPublish = attrList.Response.IsPublish;
                        var ID = assetid;
                        $scope.StopeUpdateStatusonPageLoad = false;
                        $scope.detailsLoader = false;
                        $scope.detailsData = true;
                        $scope.Assetdyn_Cont = '';
                        $scope.attributedata = attrList.Response.AttributeData;
                        $scope.AssetPreviewObj.AssetId = assetid;
                        $scope.AssetPreviewObj.AssetCeatedOn = attrList.Response.StrCreatedDate != null ? dateFormat(attrList.Response.StrCreatedDate, $scope.DefaultSettings.DateFormat) : "";
                        $scope.AssetPreviewObj.AssetUpdatedOn = attrList.Response.StrUpdatedOn != "0001-01-01" ? dateFormat(attrList.Response.StrUpdatedOn, $scope.DefaultSettings.DateFormat) : "-";
                        $scope.AssetlastUpdatedOn = $scope.AssetPreviewObj.AssetUpdatedOn;
                        $scope.AssetPreviewObj.PublisherID = attrList.Response.PublisherID;
                        $scope.AssetPreviewObj.PublishedOn = attrList.Response.StrPublishedOn != "" ? dateFormat(attrList.Response.StrPublishedOn, $scope.DefaultSettings.DateFormat) : "-";
                        $scope.ddlGlobalRole = [];
                        $scope.ddltempGlobalRole = [];
                        $scope.assetFolderid = attrList.Response.FolderID;
                        $scope.MediaBankSettings.folderName = attrList.Response.folderName;
                        $scope.assetEntityId = attrList.Response.EntityID;
                        $scope.ProductionEntityID = attrList.Response.ProductionEntityID;
                        $scope.AssetActiveFileID = attrList.Response.ActiveFileID;
                        try {
                            var ResultTask = $rootScope.currentPathName;
                            ResultTask = ResultTask.split("/");
                            if ($rootScope.currentPathName == "mui/admin/tasklistlibrary") {
                                $scope.assetEditLinkDetails.publishedon = attrList.Response.PublishedOn != "" ? dateFormat(attrList.Response.PublishedOn, $scope.DefaultSettings.DateFormat) : "-";
                                $scope.assetEditLinkDetails.publishedby = attrList.Response.PublishLinkDetails[0];
                            } else {
                                $scope.assetEditLinkDetails.folderName = attrList.Response.folderName;
                                $scope.assetEditLinkDetails.publishedon = attrList.Response.PublishLinkDetails[0].publishedON != "" ? dateFormat(attrList.Response.PublishLinkDetails[0].publishedON, $scope.DefaultSettings.DateFormat) : "-";
                                $scope.assetEditLinkDetails.publishedby = attrList.Response.PublishLinkDetails[0].publishedby;
                                $scope.assetEditLinkDetails.linkdetails = attrList.Response.PublishLinkDetails[0].LinkUserDetail;
                            }
                        } catch (e) { }
                        timerObj.GetEntityLocationPathTimer = $timeout(function () {
                            if ($scope.assetFolderid > 0) GetEntityLocationPath($scope.assetEntityId);
                            else GetEntityLocationPath($scope.ProductionEntityID);
                        }, 100);
                        if (attrList.Response.AssetRoles != null) {
                            $scope.AssetAccess.ddlGlobalRole = attrList.Response.AssetRoles;
                            $scope.ddltempGlobalRole = attrList.Response.AssetRoles;
                            $scope.AssetRolesCaption = jQuery.map($scope.GlobalRoles, function (relation) {
                                if ($.inArray(relation.ID.toString(), $scope.AssetAccess.ddlGlobalRole) != -1) return relation.Role;
                            }).join(', ');
                            if ($scope.AssetRolesCaption == "") $scope.AssetRolesCaption = "-";
                        } else {
                            $scope.AssetAccess.ddlGlobalRole = [];
                            $scope.ddltempGlobalRole = [];
                        }
                        if (attrList.Response.TargetEntityID.length > 0) {
                            $scope.curEntityID = parseInt(params.ID, 10);
                            if (attrList.Response.TargetEntityID.contains($scope.curEntityID)) $scope.LinkedAsset = true;
                            else {
                                $scope.LinkedAsset = false;
                                $scope.showspan = false;
                                $scope.DisableEnableforLink = true;
                                $scope.DisableEnableAssetRole = true;
                                $scope.lockmenu = true;
                            }
                        } else {
                            $scope.LinkedAsset = false;
                            $scope.showspan = false;
                            $scope.DisableEnableforLink = true;
                            $scope.DisableEnableAssetRole = true;
                            $scope.lockmenu = true;
                        }
                        if ($scope.LinkedAsset == true) {
                            $scope.showspan = true;
                            $scope.DisableEnableforLink = false;
                            $scope.DisableEnableAssetRole = false;
                            $scope.lockmenu = false;
                        }
                        if ($scope.AssetPublish != null) {
                            if ($scope.AssetPublish == true) {
                                $scope.UnPublish = true;
                                $scope.IsPublish = false;
                            } else {
                                $scope.UnPublish = false;
                                $scope.IsPublish = true;
                            }
                        }
                        if ($scope.SearchAsset.Access == 1) {
                            $scope.Isassetlibrary = true;
                            $scope.IsShowInAssetlibrary = true;
                            $scope.AssetTypechange = false;
                        }
                        $scope.assetfiles = attrList.Response.Files;
                        if (attrList.Response.MediaGeneratorData != undefined) $scope.mediaGeneratorAssetDetails = attrList.Response.MediaGeneratorData;
                        else $scope.mediaGeneratorAssetDetails = {};
                        $('#diveditPrev').removeClass('firstRecord');
                        $('#diveditNext').removeClass('lastRecord');
                        if (frmNotification == false) {
                            if ($scope.AssetFilesObj_Temp.AssetFiles.length == 1) {
                                $('#diveditPrev').addClass('firstRecord');
                                $('#diveditNext').addClass('lastRecord');
                            } else {
                                var selAsset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                                    return e.AssetUniqueID == ID;
                                });
                                var indexofasset = $scope.AssetFilesObj_Temp.AssetFiles.indexOf(selAsset[0]);
                                if (indexofasset == 0) {
                                    $('#diveditPrev').removeClass('firstRecord');
                                    $('#diveditPrev').addClass('firstRecord');
                                } else if (indexofasset == $scope.AssetFilesObj_Temp.AssetFiles.length - 1) {
                                    $('#diveditNext').removeClass('lastRecord');
                                    $('#diveditNext').addClass('lastRecord');
                                }
                            }
                        } else {
                            $('#diveditPrev').addClass('firstRecord');
                            $('#diveditNext').addClass('lastRecord');
                        }
                        $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                            return e.ID == attrList.Response.ActiveFileID;
                        })[0];
                        if ($scope.ActiveVersion != '' && $scope.ActiveVersion != undefined) {
                            $scope.AssetPreviewObj.VersionCeatedOn = $scope.ActiveVersion.StrCreatedDate != "" ? dateFormat($scope.ActiveVersion.StrCreatedDate, $scope.DefaultSettings.DateFormat) : "-";
                            $scope.EditversionID = $scope.ActiveVersion.ID;
                            $scope.Editverionfileguid = $scope.ActiveVersion.Fileguid;
                            $scope.MediaBankSettings.EditableAssetGuid = $scope.ActiveVersion.Fileguid;
                            $scope.MediaBankSettings.EditableAssetExt = $scope.ActiveVersion.Extension.toLowerCase();
                            $scope.MediaBankSettings.AssetID = assetid;
                            $scope.MediaBankSettings.EntityId = $scope.assetEntityId;
                            $scope.MediaBankSettings.FolderId = $scope.assetFolderid;
                            $scope.ProcessType = $scope.ActiveVersion.ProcessType;
                            $scope.videofileguid = imageBaseUrlcloud + "DAMFiles/Preview/" + $scope.ActiveVersion.Fileguid + '.mp4';
                            if ($scope.ProcessType == 3) {
                                if ($scope.ActiveVersion.Status == 2) {
                                    $('#videoHolder').html('');
                                    $scope.posterpath = imageBaseUrlcloud + "DAMFiles/Preview/Big_" + $scope.ActiveVersion.Fileguid + '.jpg?' + generateUniqueTracker() + '';
                                    timerObj.previewTimer = $timeout(function () {
                                        var HTML = '<video id="video1_' + $scope.ActiveVersion.Fileguid + '" class="video-js" controls preload="auto"  poster=\"' + imageBaseUrlcloud + 'DAMFiles/Preview/Big_' + $scope.ActiveVersion.Fileguid + '.jpg\"><source src=\"' + $scope.videofileguid.toString() + '\" type="video/mp4"></video>'
                                        $('#videoHolder').html(HTML);
                                        vjs('video1_' + $scope.ActiveVersion.Fileguid + '');
                                    }, 100);
                                } else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) {
                                    $('#videoHolder').html('');
                                    $scope.posterpath = "assets/img/loading.gif";
                                    timerObj.previewTimer = $timeout(function () {
                                        CheckPreviewGeneratorForVersion()
                                    }, 5000);
                                    var HTML = '<div class="loadicon-large"><span><img src="' + $scope.posterpath + '" class="noBorder" />Loading...</span></div>'
                                    $('#videoHolder').html(HTML);
                                } else if ($scope.ActiveVersion.Status == 3) {
                                    $scope.posterpath = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + ".jpg?" + generateUniqueTracker();
                                    var HTML = '<img src="' + $scope.posterpath + '" />'
                                    $('#videoHolder').html(HTML);
                                }
                            }
                            $scope.Category = attrList.Response.Category;
                            if (attrList.Response.Category == 0) {
                                $scope.IsAttachment = true;
                                $scope.IsImgInfo = true;
                                $scope.IsSize = true;
                                $scope.IsResolution = true;
                                $scope.IsDPI = true;
                            } else $scope.IsAttachment = false;
                            $scope.IsImgInfo = false;
                            $scope.ActiveVersionNo = $scope.ActiveVersion.VersionNo;
                            $scope.filepathfrview = imageBaseUrlcloud + "DAMFiles/Original/" + $scope.ActiveVersion.Fileguid + $scope.ActiveVersion.Extension + '?' + generateUniqueTracker();
                            $scope.filedownloadpath = "DAMDownload.aspx?FileID=" + $scope.ActiveVersion.Fileguid + "&amp;FileFriendlyName=" + $scope.ActiveVersion.Name + "&amp;Ext=" + $scope.ActiveVersion.Extension;
                            $scope.OriginalDownload.FileName = $scope.ActiveVersion.Name;
                            $scope.OriginalDownload.FileID = $scope.ActiveVersion.Fileguid;
                            $scope.OriginalDownload.FileExt = $scope.ActiveVersion.Extension;
                            if ($scope.ProcessType == 1 || $scope.ProcessType == 2 || $scope.ProcessType == 4) {
                                if ($scope.ActiveVersion.Status == 2) {
                                    $scope.fileguid = imageBaseUrlcloud + "DAMFiles/Preview/Big_" + $scope.ActiveVersion.Fileguid + '.jpg?' + generateUniqueTracker() + '';
                                    $scope.ErroDiv = false;
                                    $scope.IsCrop = true;
                                    $scope.IsImgInfo = true;
                                    $scope.onerrorfileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + ".jpg?" + generateUniqueTracker();
                                } else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) {
                                    timerObj.previewTimer = $timeout(function () {
                                        CheckPreviewGeneratorForVersion()
                                    }, 5000);
                                    $scope.fileguid = "assets/img/loading.gif";
                                    $scope.ErroDiv = false;
                                    $scope.IsCrop = false;
                                    $scope.IsImgInfo = false;
                                }
                                if ($scope.ActiveVersion.Status == 3) {
                                    $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + ".jpg?" + generateUniqueTracker();
                                    $scope.ErroDiv = true;
                                    $scope.IsCrop = true;
                                    $scope.IsImgInfo = true;
                                    $scope.IsSize = true;
                                    $scope.IsResolution = false;
                                    $scope.IsDPI = false;
                                }
                            } else {
                                $scope.ErroDiv = false;
                                $scope.IsAttachment = true;
                                $scope.IsCrop = false;
                                $scope.IsImgInfo = true;
                                $scope.IsResolution = false;
                                $scope.IsDPI = false;
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + ".jpg?" + generateUniqueTracker();
                            }
                        } else {
                            if (attrList.Response.Category == 1) {
                                $scope.IsAttachment = false;
                                $scope.IsShow = true;
                                $scope.ErroDiv = false;
                                $scope.IsCrop = false;
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/BLANK.jpg?" + generateUniqueTracker();
                                $scope.Category = attrList.Response.Category;
                            } else if (attrList.Response.Category == 2) {
                                $scope.IsAttachment = false;
                                $scope.IsCrop = false;
                                $scope.ErroDiv = false;
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_big/LINK.jpg?" + generateUniqueTracker();
                                $scope.Category = attrList.Response.Category;
                            }
                            $scope.filedownloadpath = '';
                        }
                        $scope.AssetCategory["AssetCategoryTree_"] = [];
                        $scope.AssetCategory["filterval_"] = '';
                        AssetEditService.GetAllExtensionTypesforDAM().then(function (data) {
                            if (data.Response != null) {
                                $scope.DAMtypeswithExtAssetedit = data.Response.m_Item1;

                                AssetEditService.GetAssetCategoryPathInAssetEdit($scope.assetType).then(function (data) {
                                    if (data.Response != null) {
                                        if ($scope.DAMtypeswithExtAssetedit.length > 0) {
                                            if (data.Response[0] != "" && data.Response[0] != null) {
                                                var temp = $.grep($scope.DAMtypeswithExtAssetedit, function (e) {
                                                    return e.id == $scope.assettypeidfrbtn;
                                                })[0].damCaption;
                                                $scope.AssetTypeCaption = temp;
                                                $scope.PreviousAssetTypeCaption = $scope.AssetTypeCaption;
                                            } else $scope.AssetTypeCaption = "-";
                                            if (data.Response[1].AssetTypeIDs.length > 0) {
                                                $scope.AssetTypeCategoryPath = data.Response[0].CategoryPath
                                                $scope.PreviousAssetTypeCategPath = $scope.AssetTypeCategoryPath;
                                                $scope.DAMtypeswithExtAssetedit = $.grep($scope.DAMtypeswithExtAssetedit, function (e) {
                                                    return $.inArray(e.Id, data.Response[1].AssetTypeIDs) != -1;
                                                })
                                                $scope.PreviousDAMtypesExtAssetedit = $scope.DAMtypeswithExtAssetedit;
                                            } else $scope.AssetTypeCategoryPath = "-";
                                        } else {
                                            $scope.AssetTypeCategoryPath = "-";
                                            $scope.AssetTypeCaption = "-";
                                        }
                                    }
                                });
                            }
                        });
                        AssetEditService.GetAssetCategoryTree($scope.assetType).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_"] = JSON.parse(data.Response);
                            }
                        });
                        timerObj.assettypeTimer = $timeout(function () {
                            $("#dynamicAssetTypeTreeInAsstEdit").html('');
                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_\" tree-data=\"AssetCategory.AssetCategoryTree_\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                            $("#dynamicAssetTypeTreeInAsstEdit").html($compile(htmltree)($scope));
                        }, 300);
                        $("#AssetEditpopup").modal("show");
                        if ($scope.SnippetObj.snippetuiclass == true) {
                            if ($("#AssetEditpopup").hasClass("searchSnippetStyle")) {
                                $("#AssetEditpopup").removeClass('searchSnippetStyle').addClass('searchSnippetStyle');
                            } else {
                                $("#AssetEditpopup").addClass('searchSnippetStyle');
                            }
                        } else $("#AssetEditpopup").removeClass('searchSnippetStyle')
                        $("#Assetdynamicdetail").empty();
                        $scope.AssetAccess.AssetName = $('<div />').html(attrList.Response.Name).text();
                        for (var i = 0; i < $scope.attributedata.length; i++) {
                            if ($scope.attributedata[i].TypeID == 6) {
                                $scope.Assetdyn_Cont2 = '';
                                var CaptionObj = $scope.attributedata[i].Caption.split(",");
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    if (j == 0) {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                }
                                $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                                $scope.items = [];
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                    var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                    if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                                    } else {
                                        if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  title=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + $scope.attributedata[i].ID + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                        } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 7) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                                $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                                GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                                $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group relative">';
                                $scope.Assetdyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                                $scope.Assetdyn_Cont += '<div class="controls">';
                                if ($scope.Islock_temp == false && $scope.Isassetlibrary == false && $scope.LinkedAsset == false && $scope.attributedata[i].IsReadOnly == false) {
                                    $scope.Assetdyn_Cont += '<div xeditabletree isdam=\"true\" editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                                    if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                        treeTextVisbileflag = false;
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                        } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                                    } else {
                                        $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                                    }
                                    $scope.Assetdyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.Assetdyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                                    $scope.Assetdyn_Cont += ' </div>';
                                } else {
                                    if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                        treeTextVisbileflag = false;
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                        } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                                    } else {
                                        $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                                    }
                                    $scope.Assetdyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.Assetdyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                                }
                                $scope.Assetdyn_Cont += '</div></div>';
                            } else if ($scope.attributedata[i].TypeID == 12) {
                                $scope.dyn_Cont2 = '';
                                var CaptionObj = $scope.attributedata[i].Caption;
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    if ($scope.attributedata[i].Lable.length == 1) {
                                        var k = j;
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        } else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    fields.push(CaptionObj[k]);
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    } else {
                                        if (j == 0) {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            } else {
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        } else {
                                            var k = j;
                                            if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                                var treeTexts = [];
                                                var fields = [];
                                                $scope.items.push({
                                                    caption: $scope.attributedata[i].Lable[j].Label,
                                                    level: j + 1
                                                });
                                                if (k == CaptionObj.length) {
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                } else {
                                                    if (CaptionObj[k] != undefined) {
                                                        for (k; k < CaptionObj.length; k++) {
                                                            treeTexts.push(CaptionObj[k]);
                                                            fields.push(CaptionObj[k]);
                                                        }
                                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                                    } else {
                                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                    }
                                                }
                                            } else {
                                                if (CaptionObj[j] != undefined) {
                                                    $scope.items.push({
                                                        caption: $scope.attributedata[i].Lable[j].Label,
                                                        level: j + 1
                                                    });
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                                } else {
                                                    $scope.items.push({
                                                        caption: $scope.attributedata[i].Lable[j].Label,
                                                        level: j + 1
                                                    });
                                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                }
                                            }
                                        }
                                    }
                                }
                                $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                                $scope.items = [];
                                for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                                    var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                    } else {
                                        if ($scope.Islock_temp == false) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  title=\"' + inlineEditabletitile + '\"  isdam=\"true\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                        } else if ($scope.Islock_temp == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption) {
                                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                                }
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" isdam=\"true\"  data-mode=\"inline\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption != undefined) {
                                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                                }
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls\"><span class="editable">' + $scope.attributedata[i].Value + '</span></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  data-mode=\"inline\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls\"><span class="editable"> ' + $scope.attributedata[i].Value + '</span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 2) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption != undefined) {
                                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                                }
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext  href=\"javascript:;\"   data-mode=\"inline\"   attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 11) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.setFieldKeys();
                                $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setUploaderCaption();
                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                                $scope.Assetdyn_Cont += '<div class=\"controls\">';
                                if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                                    $scope.attributedata[i].Value = "NoThumpnail.jpg";
                                }
                                $scope.Assetdyn_Cont += '<div class="entityDetailImgPreviewHolder"><img src="' + imageBaseUrlcloud + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Lable + '"';
                                $scope.Assetdyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreviewae_' + $scope.attributedata[i].ID + '"></div>';
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '</div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefile(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                        $scope.Assetdyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                        $scope.Assetdyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.Assetdyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">' + $translate.instant('LanguageContents.Res_4729.Caption');
                                        $scope.Assetdyn_Cont += '</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '</div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 3) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                                    if ($scope.attributedata[i].Caption[0] != undefined) {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                            }
                                        }
                                    } else {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                            $scope.Assetdyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                            $scope.Assetdyn_Cont += '</div></div>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                                $scope.Assetdyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                                $scope.Assetdyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                                $scope.Assetdyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                                $scope.Assetdyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '"';
                                                $scope.Assetdyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                                $scope.Assetdyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                                $scope.Assetdyn_Cont += '</div></div>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                                $scope.Assetdyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                                $scope.Assetdyn_Cont += '</div></div>';
                                            }
                                        }
                                    }
                                } else {
                                    if ($scope.attributedata[i].Caption[0] != undefined) {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a isdam=\"true\" data-mode=\"inline\"  xeditabledropdown href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                            }
                                        }
                                    } else {
                                        $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                        $scope.setNormalDropdownCaption();
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a isdam=\"true\"  xeditabledropdown href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                            }
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 4) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    } else {
                                        if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a isdam=\"true\" data-mode=\"inline\" xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        }
                                    }
                                } else {
                                    $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    } else {
                                        if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a isdam=\"true\" data-mode=\"inline\"  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        }
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var inlineEditabletitile = $scope.attributedata[i].Caption;
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                                    datstartval = new Date.create($scope.attributedata[i].Value);
                                    $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                    $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                } else {
                                    $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                                    $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                                }
                                if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                                    if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    } else {
                                        $scope.MinValue = $scope.attributedata[i].MinValue;
                                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                        if ($scope.Islock_temp == false) {
                                            $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  title=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        } else if ($scope.Islock_temp == true) {
                                            $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        }
                                    }
                                } else {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            } else if ($scope.attributedata[i].TypeID == 10) {
                                var inlineEditabletitile = $scope.attributedata[i].Caption;
                                periodforTypeEdit = $scope.attributedata[i].Value;
                                $scope.Assetdyn_Cont += '<div data-periodcontainerID="periodcontainerID" class="control-group">';
                                if ($scope.attributedata[i].Value == "-") {
                                    $scope.IsStartDateEmpty = true;
                                    $scope.Assetdyn_Cont += '<div data-addperiodID="addperiodID">';
                                    $scope.Assetdyn_Cont += '</div>';
                                } else {
                                    for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                        var datStartUTCval = "";
                                        var datstartval = "";
                                        var datEndUTCval = "";
                                        var datendval = "";
                                        datstartval = new Date.create($scope.attributedata[i].Value[j].Startdate);
                                        datendval = new Date.create($scope.attributedata[i].Value[j].EndDate);
                                        perioddates.push({
                                            ID: $scope.attributedata[i].Value[j].Id,
                                            value: datendval
                                        });
                                        $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                        $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                        $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                        $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                        if ($scope.attributedata[i].Value[j].Description == undefined) {
                                            $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                        } else {
                                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                            $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                        }
                                        $('#fsedateid').css("visibility", "hidden");
                                        $scope.Assetdyn_Cont += '<div class="period" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                        $scope.Assetdyn_Cont += '<div class="inputHolder span11">';
                                        $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                        $scope.Assetdyn_Cont += '<div class="controls">';
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                            $scope.Assetdyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.MinValue = $scope.attributedata[i].MinValue;
                                                $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                                if ($scope.MinValue < 0) {
                                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                                } else {
                                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                                }
                                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                                } else {
                                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                                }
                                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                                $scope.Assetdyn_Cont += '<a isdam="true" xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  title=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}} to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                                $scope.Assetdyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                            }
                                        }
                                        $scope.Assetdyn_Cont += '</div></div>';
                                        $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                        $scope.Assetdyn_Cont += '<div class="controls">';
                                        if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                            $scope.Assetdyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                        } else {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                                $scope.Assetdyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  title=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                            } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                            }
                                        }
                                        $scope.Assetdyn_Cont += '</div></div></div>';
                                        if (j != 0) {
                                            if ($scope.Islock_temp == false && $scope.LinkedAsset == false || $scope.Isassetlibrary == true) {
                                                $scope.Assetdyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                            }
                                        }
                                        $scope.Assetdyn_Cont += '</div>';
                                        if (j == ($scope.attributedata[i].Value.length - 1)) {
                                            $scope.Assetdyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                            $scope.Assetdyn_Cont += '</div>';
                                        }
                                    }
                                }
                                $scope.Assetdyn_Cont += ' </div>';
                                $scope.Assetdyn_Cont += '<div class="control-group nomargin">';
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<label data-tempid="startendID" class="control-label" for="label">Start / End date</label>';
                                    $scope.Assetdyn_Cont += '<div class="controls">';
                                    $scope.Assetdyn_Cont += '<span>-</span>';
                                    $scope.Assetdyn_Cont += '</div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false || $scope.Isassetlibrary == true) {
                                        if ($scope.attributedata[i].Value == "-") {
                                            $scope.Assetdyn_Cont += '<label id="fsedateid" class="control-label" for="label">Start / End date</label>';
                                        }
                                        $scope.Assetdyn_Cont += '<div class="controls">';
                                        $scope.Assetdyn_Cont += '<a class="ng-pristine ng-valid editable editable-click"  isdam="true" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  title=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
                                        $scope.Assetdyn_Cont += '</div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<span class="controls">[Add Start / End Date ]</span>';
                                    }
                                }
                                $scope.Assetdyn_Cont += '</div>';
                            } else if ($scope.attributedata[i].TypeID == 14) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineLinkName_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["SingleLineLinkType_" + $scope.attributedata[i].ID] = 1;
                                $scope.fields["SingleLineLinkUrl_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != "") {
                                    $scope.fields["SingleLineLinkName_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                    $scope.fields["SingleLineLinkType_" + $scope.attributedata[i].ID] = parseInt($scope.attributedata[i].specialValue);
                                    $scope.fields["SingleLineLinkUrl_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                }
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineLinkUrl_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditablelink  href=\"javascript:;\"   data-mode=\"inline\"   attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + assetid + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineLinkUrl_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineLinkUrl_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 17) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                if ($scope.attributedata[i].Caption[0] != undefined) {
                                    if ($scope.attributedata[i].Caption.length > 1) {
                                        $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                        $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                    }
                                } else {
                                    $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                                    $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                                }
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-original-title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\"  >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 16) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption != undefined) {
                                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                                }
                                var dateExpire;
                                if ($scope.attributedata[i].Value != undefined && $scope.attributedata[i].Value != "-" && $scope.attributedata[i].Value != null) {
                                    var dateExpireUTCval = "";
                                    var dateExpireval = "";
                                    dateExpireval = new Date.create($scope.attributedata[i].Value);
                                    dateExpire = ConvertDateFromStringToString(ConvertDateToString(dateExpireval));
                                } else {
                                    dateExpire = "-"
                                }
                                $scope.fields["ExpireDateTime_" + $scope.attributedata[i].ID] = dateExpire;
                                if ($scope.attributedata[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false && $scope.ProductionEntityID > 0) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</a></span></div></div>';
                                    } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 13) {
                                var ID = 0;
                                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                                else ID = $scope.AssetEditID;
                                $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                                $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                                for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {
                                    if (price.selection.length > 0) {
                                        var selectiontext = "";
                                        var valueMatches = [];
                                        if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                            return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                        });
                                        if (valueMatches.length > 0) {
                                            selectiontext = "";
                                            for (var x = 0, val; val = valueMatches[x++];) {
                                                selectiontext += val.caption;
                                                if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                else selectiontext += "</br>";
                                            }
                                        } else selectiontext = "-";
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                                    } else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                                    }
                                    if ($scope.attributedata[i].IsReadOnly == false && $scope.Islock_temp == false) {
                                        $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                                        $scope.Assetdyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\"  isdam=\"true\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  title=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                                    } else {
                                        $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                                        $scope.Assetdyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 8) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                if ($scope.attributedata[i].Caption != undefined) {
                                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                                }
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.Islock_temp == false) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-mode=\"inline\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.Islock_temp == true) {
                                        $scope.Assetdyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else if ($scope.attributedata[i].TypeID == 18) {
                                $scope.Assetdyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        }
                        $rootScope.currentPathName = $location.path().replace('/', '');
                        if ($rootScope.currentPathName === "mediabank") {
                            if ($scope.AssetPublish = "true") {
                                $scope.ShowpublishIcon = false;
                                $scope.Createtask = false;
                                $scope.Add = false;
                                $scope.UnPublish = true;
                                $scope.addlight = false;
                                $scope.task = false;
                            } else {
                                $scope.Createtask = true;
                                $scope.Add = true;
                                $scope.UnPublish = false;
                                $scope.addlight = true;
                                if ($scope.Category == 0) $scope.task = true;
                                else $scope.task = false;
                            }
                        } else {
                            $scope.Createtask = true;
                            $scope.Add = true;

                            AssetEditService.CheckAccessToPublish($cookies['UserId'], assetid).then(function (result) {
                                if (result.Response == "true") {
                                    if ($scope.AssetPublish != null) {
                                        if ($scope.AssetPublish == true) {
                                            $scope.UnPublish = true;
                                            $scope.IsPublish = false;
                                        } else {
                                            $scope.UnPublish = false;
                                            $scope.IsPublish = true;
                                        }
                                    }
                                }
                                else {
                                    $scope.UnPublish = false;
                                    $scope.IsPublish = false;
                                }
                            });

                            $scope.addlight = true;
                            if ($scope.Category == 0) $scope.task = true;
                            else $scope.task = false;
                        }
                        if ($rootScope.currentPathName === "mui/marcommediabank") {
                            $scope.HideDuplicate = true;
                            $scope.UnPublish = false;
                            $scope.task = false;
                        }
                        var test = $rootScope.currentPathName;
                        test = test.split("/");
                        if (test[test.length - 1] == "task") $scope.task = false;
                        if ($scope.Islock_temp == true) {
                            $scope.showspan = true;
                            $scope.DisableEnableforLink = false;
                            $scope.DisableEnableAssetRole = false;
                            $scope.lockmenu = false;
                            $scope.EntityIsLock = true;
                            $scope.HideDuplicate = true;
                        }
                        if ($scope.LinkedAsset || $scope.EntityIsLock) {
                            $('#ISshowImageType').removeClass('ng-hide');
                            $('#IShideImageType').addClass('ng-hide');
                        } else if ($scope.LinkedAsset == false || $scope.EntityIsLock == false) {
                            $('#IShideImageType').removeClass('ng-hide');
                            $('#ISshowImageType').addClass('ng-hide');
                        }
                        if ($rootScope.currentPathName === "mui/marcommediabank" || $scope.IsSearchpage === "customsearchresults" || $scope.IsSearchpage === "customsearchresult" || $rootScope.currentPathName === "mediabank") {
                            if ($scope.Isassetlibrary) {
                                $('#ISshowImageType').removeClass('ng-hide');
                                $('#IShideImageType').addClass('ng-hide');
                            }
                            if ($scope.Isassetlibrary == false) {
                                $('#IShideImageType').removeClass('ng-hide');
                                $('#ISshowImageType').addClass('ng-hide');
                            }
                        }
                        if ($scope.IsSearchpage === "customsearchresult" || $scope.IsSearchpage === "customsearchresults") {
                            $scope.addlight = false;
                        }
                        $("#Assetdynamicdetail").empty();
                        $("#Assetdynamicdetail").append($scope.Assetdyn_Cont);
                        $compile($("#Assetdynamicdetail").contents())($scope);
                        $scope.previousAssetdyn_Count = $scope.Assetdyn_Cont;
                        $('#clicknextprevious').modal('hide');
                    }
                });
            }
        }
        $scope.openExpireAction = function (event) {
            var attrevent = event.target;
            var sourcefrom = 1;
            $scope.SourceAttributeID = attrevent.dataset.attributeid;
            $scope.SourceAttributename = attrevent.dataset.attrlable;
            $scope.dateExpireval = $scope.fields["ExpireDateTime_" + $scope.SourceAttributeID];
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/expirehandleractions.html',
                controller: "mui.muiexpireaddActionsCtrl",
                resolve: {
                    params: function () {
                        return {
                            dateExpireval: $scope.dateExpireval,
                            AssetEditID: $scope.AssetEditID,
                            assetEntityId: $scope.assetEntityId,
                            sourcefrom: sourcefrom,
                            assettypeidfrbtn: $scope.assettypeidfrbtn,
                            ProductionEntityID: $scope.ProductionEntityID,
                            SourceAttributeID: $scope.SourceAttributeID,
                            SourceAttributename: $scope.SourceAttributename
                        };
                    }
                },
                scope: $scope,
                windowClass: 'expirehandlerAddActionsPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.$on('updateExpireactionAssetEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            $scope.fields["ExpireDateTime_" + AttributeID] = Attributevalue;
        });

        function CheckPreviewGeneratorForVersion() {
            var assetids = [];
            if ($('.iv-Popup').css('display') == 'block') {
                if ($scope.fileguid == "assets/img/loading.gif") {
                    if ($scope.EditversionID != 0) assetids.push($scope.EditversionID);
                }
                if (assetids.length > 0) {
                    AssetEditService.CheckPreviewGeneratorID(assetids).then(function (result) {
                        if (result.Response != "") {
                            var generatedids = result.Response.split(',');
                            for (var j = 0; j < generatedids.length; j++) {
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/Preview/Big_" + $scope.Editverionfileguid + '.jpg?' + generateUniqueTracker() + '';
                                // console.log("CheckPreviewGeneratorForVersion Response ." + imageBaseUrlcloud + "DAMFiles/Preview/Big_" + $scope.Editverionfileguid + '.jpg?' + generateUniqueTracker() + '');
                                $scope.EditversionID = 0;
                                $scope.Editverionfileguid = '';
                                $scope.IsCrop = true;
                                GetAssetActiveFileinfo($scope.AssetPreviewObj.AssetId);
                            }
                        }
                    });
                }
            }
            if ($scope.fileguid == "assets/img/loading.gif") {
                timerObj.EditPreviewGeneratorTimer = $timeout(function () {
                    CheckPreviewGeneratorForVersion()
                }, 3000);
            }
        }
        $scope.DeleteAttachmentVersionByAssetID = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    timerObj.deleteAttachmentTimer = $timeout(function () {
                        AssetEditService.DeleteAttachmentVersionByAssetID($scope.fileid).then(function (result) {
                            if (result.Response) {
                                var deletedAsset = $filter('filter')($scope.assetfiles, {
                                    ID: parseInt($scope.fileid)
                                }, true);
                                var fileindex = $scope.assetfiles.indexOf(deletedAsset[0]);
                                $scope.assetfiles.splice(fileindex, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4794.Caption'));
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_4298.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.SaveFiletoAsset = [];

        function SaveFileDetails(file, response, fileDescription) {
            $scope.SaveFiletoAsset = [];
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            $scope.SaveFiletoAsset.push({
                "AssetID": ID,
                "Status": 0,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "CreatedOn": Date.now(),
                "Extension": extension,
                "Name": file.name,
                "VersionNo": 0,
                "Description": file.name,
                "OwnerID": $cookies['UserId']
            });
            AssetEditService.SaveFiletoAsset($scope.SaveFiletoAsset).then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    var datetimetdy = ConvertDateToString(new Date.create());
                    var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                    $scope.AssetlastUpdatedOn = datetimetdy1;
                    NotifySuccess($translate.instant('LanguageContents.Res_4787.Caption'));
                    GetAssetAttribuuteDetails(ID);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4265.Caption'));
                }
            });
        }
        $scope.OriginalDownload = {
            FileName: "",
            FileID: "",
            FileExt: ""
        };
        $scope.populateImage = function (FileGuid, Extension, FileName) {
            $scope.filepathfrview = imageBaseUrlcloud + "DAMFiles/Original/" + FileGuid + Extension + '?' + generateUniqueTracker();
            $scope.OriginalDownload.FileName = FileName;
            $scope.OriginalDownload.FileID = FileGuid;
            $scope.OriginalDownload.FileExt = Extension;
        }
        $scope.downloadOriginal = function () {
            $scope.ProgressMsgHeader = "Downloading Assets";
            var extn = $scope.OriginalDownload.FileExt == '.zip' ? 'zip' : $scope.OriginalDownload.FileExt;
            var filename = $scope.OriginalDownload.FileName;
            var actualfilename = "";
            for (var i = 0; i < filename.length; i++) {
                if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                    actualfilename = actualfilename + (filename[i]);
                }
            }
            blockUIForDownload();
            location.href = 'DAMDownload.aspx?FileID=' + $scope.OriginalDownload.FileID + '&FileFriendlyName=' + actualfilename + '&Ext=' + extn + '';
            timerObj.newsfeedtimer = $timeout(function () {
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                $scope.newsFeedinfo.push({
                    "AssetID": ID,
                    "action": "Download Asset",
                    "TypeName": "Asset Download"
                });
                AssetEditService.CreateNewsFeedinfo($scope.newsFeedinfo).then(function (result) {
                    if (result.Response == null) { } else {
                        $scope.newsFeedinfo = [];
                        ID = 0;
                    }
                });
            }, 100);
        }
        $scope.UpdateAssetVersion = function (VersionNo) {
            var fileid = $.grep($scope.assetfiles, function (e) {
                return e.VersionNo == VersionNo
            })[0].ID;
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            AssetEditService.UpdateAssetVersion(ID, fileid).then(function (result) {
                if (result.Response != null && result.Response != '') {
                    GetAssetAttribuuteDetails(ID);
                    var datetimetdy = ConvertDateToString(new Date.create());
                    var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                    $scope.AssetlastUpdatedOn = datetimetdy1;
                    NotifySuccess($translate.instant('LanguageContents.Res_4807.Caption'));
                    timerObj.thumpnailcallbackTimer = $timeout(function () {
                        //$scope.$emit('CallBackThumbnailAssetViewfrmMUI', ID);
                        var assetlist = [];
                        assetlist.push(ID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        //$scope.$emit('CallBackThumbnailAssetView', ID);
                    }, 100);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4381.Caption'));
                }
            });
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.chngDropDown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.chngDropDown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["chngDropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["chngMultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["chngMultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["chngDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["chngDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.chngDropDown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["chngMultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["chngMultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.chngDropDown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["chngMultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.UpdateAssetDetials = [];
        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                $scope.UpdateAssetDetials = [];
                $scope.UpdateAssetDetials.push({
                    "NewValue": optionarray,
                    "AttributetypeID": attributetypeid
                });
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (result) {
                    if (result.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 1 || attributetypeid == 2) {
                if (attrID == 68 && optionarray.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4582.Caption'));
                    $scope.fields.SingleLineTextValue_68 = $scope.AssetAccess.AssetName;
                    return false;
                }
                $scope.UpdateAssetDetials = [];
                $scope.UpdateAssetDetials.push({
                    "NewValue": [optionarray],
                    "AttributetypeID": attributetypeid
                });
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                var numbers = /^[0-9]+$/;
                var cou = optionarray.match(numbers);
                if (attributetypeid == 1 && attrID == 5 && cou == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5735.Caption'));
                } else if (attributetypeid == 1 && optionarray.length > 8 && attrID == 5 && cou != null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5750.Caption'));
                } else {
                    ID = $scope.AssetEditID;
                    AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (result) {
                        if (result.StatusCode != 0) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            var datetimetdy = ConvertDateToString(new Date.create());
                            var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                            $scope.AssetlastUpdatedOn = datetimetdy1;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                            if (attrID == 68) {
                                $scope.AssetAccess.AssetName = optionarray;
                            }
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            if (attrID == 68) {
                                $('#breadcrumlink').text(optionarray);
                            }
                            var assetlist = [];
                            assetlist.push($scope.AssetEditID);
                            $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        }
                    });
                }
                $scope.treeSelection = [];
            } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                $scope.UpdateAssetDetials = [];
                $scope.UpdateAssetDetials.push({
                    "NewValue": optionarray,
                    "AttributetypeID": attributetypeid
                });
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (result) {
                    if (result.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        var datetimetdy = ConvertDateToString(new Date.create());
                        var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                        $scope.AssetlastUpdatedOn = datetimetdy1;
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                    }
                });
                if (attributetypeid == 3) {
                    $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                }
                $scope.treeSelection = [];
            } else if (attributetypeid == 12) {
                $scope.UpdateAssetDetials = [];
                $scope.UpdateAssetDetials.push({
                    "NewValue": optionarray,
                    "AttributetypeID": attributetypeid
                });
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 5) {
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                var sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                $scope.UpdateAssetDetials = [];
                $scope.UpdateAssetDetials.push({
                    "NewValue": [sdate],
                    "AttributetypeID": attributetypeid
                });
                AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (result) {
                    if (result.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.DefaultSettings.DateFormat);
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    var ID = 0;
                    if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                    else ID = $scope.AssetEditID;
                    $scope.UpdateAssetDetials = [];
                    $scope.UpdateAssetDetials.push({
                        "NewValue": optionarray == "" ? ["0"] : [optionarray],
                        "AttributetypeID": attributetypeid
                    });
                    AssetEditService.SaveDetailBlockForAssets(parseInt(ID, 10), attrID, 0, $scope.UpdateAssetDetials).then(function (result) {
                        if (result.Response == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        } else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                            $scope.fields['SingleLineText_' + attrID] = optionarray == "" ? 0 : optionarray;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                            var assetlist = [];
                            assetlist.push($scope.AssetEditID);
                            $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        }
                    });
                    $scope.treeSelection = [];
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            }
            $("#Linkicon_" + ID).addClass('displayNone');
            $scope.treeSelection = [];
            $scope.$emit('CallBackThumbnailAssetViewfrmMUI', ID);
        };

        function UpdateTreeScope(ColumnName, Value) {
            if ($scope.ListViewDetails != undefined) {
                for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
                    if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                if (EntityId == parseInt(params.ID, 10)) {
                                    $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.InitializeFileUpload = function () {
            $("#totalAssetActivityAttachProgress_Version").empty();
            $("#totalAssetActivityAttachProgress_Version").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="uploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
            StrartUpload_Version();
            $('#totalAssetActivityAttachProgress_Version').hide();

            $("#AssetVersion").modal('show');
        };

        function StrartUpload_Version() {
            var uploader_V = "";
            if (uploader_V != '' && uploader_V != null && uploader_V != undefined) {
                uploader_V.destroy();
            }
            $('.moxie-shim').remove();
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader_V = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_V_task',
                    container: 'filescontainer_V_task',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_V.bind('Init', function (up, params) { });
                uploader_V.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_V.addFile(input.files);
                    };
                    input.init();
                });
                uploader_V.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_V.start();
                    $('#totalAssetActivityAttachProgress_Version').show();
                    $('#totalAssetActivityAttachProgress_Version .bar').css('width', '0');
                });
                uploader_V.bind('UploadProgress', function (up, file) {
                    $('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    $("#totalAssetActivityAttachProgress_Version").empty();
                    $("#totalAssetActivityAttachProgress_Version").hide();
                    up.refresh();
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {

                    var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;

                    TotalUploadProgress_V(file);
                    timerObj.fileuploadtimer = $timeout(function () {
                        SaveFileDetails_V(file, response.response);
                    }, 50);
                });
                uploader_V.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader_V.settings.multipart_params.key = uniqueKey;
                    uploader_V.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {

                var uploader_V = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_V_task',
                    container: 'filescontainer_V_task',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
                    chunk_size: '64Kb',
                    multi_selection: false,
                    multipart_params: {}
                });
                uploader_V.bind('Init', function (up, params) { });
                uploader_V.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_V.addFile(input.files);
                    };
                    input.init();
                });
                uploader_V.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_V.start();
                    $('#totalAssetActivityAttachProgress_Version').show();
                    $('#totalAssetActivityAttachProgress_Version .bar').css('width', '0');
                });
                uploader_V.bind('UploadProgress', function (up, file) {
                    $('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    $("#totalAssetActivityAttachProgress_Version").empty();
                    $("#totalAssetActivityAttachProgress_Version").hide();
                    up.refresh();
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {
                    TotalUploadProgress_V(file);
                    timerObj.fileuploadtimer = $timeout(function () {
                        SaveFileDetails_V(file, response.response);
                    }, 50);
                });
                uploader_V.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

            function TotalUploadProgress_V(file) {
                var UploadedCount = 0;
                $('#filelistActivity .attachmentBox').each(function () {
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#totalAssetActivityAttachProgress_Version .count').html(UploadedCount + ' of ' + 1 + ' Uploaded');
                $('#totalAssetActivityAttachProgress_Version .size').html(plupload.formatSize(file.loaded) + ' / ' + plupload.formatSize(file.size));
                $('#totalAssetActivityAttachProgress_Version .bar').css("width", Math.round(((file.loaded / file.size) * 100)) + "%");
            };
        }

        function SaveFileDetails_V(file, response) {
            var version = 0;
            if ($scope.Category == 0) {
                if ($scope.ActiveVersionNo == null || $scope.ActiveVersionNo == '') {
                    version = 0;
                } else {
                    version = $scope.ActiveVersionNo;
                }
            }
            $scope.SaveFiletoAsset = [];
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            $scope.SaveFiletoAsset.push({
                "AssetID": ID,
                "Status": 0,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "CreatedOn": Date.now(),
                "Extension": extension,
                "Name": file.name,
                "VersionNo": version,
                "Description": file.name,
                "OwnerID": $cookies['UserId']
            });
            AssetEditService.SaveFiletoAsset($scope.SaveFiletoAsset).then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    $('#totalAssetActivityAttachProgress_Version').hide();
                    NotifySuccess($translate.instant('LanguageContents.Res_4787.Caption'));
                    GetAssetAttribuuteDetails(ID);
                    var assetlist = [];
                    assetlist.push($scope.AssetEditID);
                    $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                    $scope.$emit('CallBackThumbnailAssetViewfrmMUI', 0);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4265.Caption'));
                    $('#totalAssetActivityAttachProgress_Version').hide();
                }
            });
        }
        $scope.fileid = '';
        $scope.AssetAccess.accesrolechange = "";
        $scope.ChangeAssesAccesRoles = function () {
            var rst = {};
            rst.ID = $scope.AssetEditID;
            rst.RoleID = $scope.AssetAccess.ddlGlobalRole;
            if ($scope.AssetPublish == true) {
                if (rst.RoleID.length > 0) {
                    AssetEditService.AssetRoleAccess(rst).then(function (addoptionsResponse) {
                        if (addoptionsResponse.StatusCode = 400) {
                            $scope.AssetRolesCaption = jQuery.map($scope.GlobalRoles, function (relation) {
                                if ($.inArray(relation.ID.toString(), $scope.AssetAccess.ddlGlobalRole) != -1) return relation.Role;
                            }).join(', ');
                            if ($scope.AssetRolesCaption == "") $scope.AssetRolesCaption = "-";
                            $scope.ddltempGlobalRole = $scope.AssetAccess.ddlGlobalRole;
                            var assetlist = [];
                            assetlist.push($scope.AssetEditID);
                            if ($scope.IsShowallAssets.Fromshowallassets == 1) {
                                $scope.$emit('loadShowallAssetDetailSection', assetlist, $scope.IsShowallAssets.DamViewName);
                            }
                            if (damviewtype == 'Thumbnail') {
                                $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                            } else if (damviewtype == 'Summary') {
                                $scope.$emit('loadAssetDetailSectionfrSummary', assetlist);
                            } else if (damviewtype == 'List') {
                                $scope.$emit('loadAssetDetailSectionfrList', assetlist);
                            }
                            NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4383.Caption'));
                        }

                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_5751.Caption'));
                    if ($scope.AssetRolesCaption != "-" && $scope.AssetRolesCaption.length > 1) {
                        if ($scope.ddltempGlobalRole != undefined && $scope.ddltempGlobalRole.length > 0) {
                            $scope.AssetAccess.ddlGlobalRole = $scope.ddltempGlobalRole;
                        }
                    }
                    return false;
                }
            } else {
                AssetEditService.AssetRoleAccess(rst).then(function (addoptionsResponse) {
                    if (addoptionsResponse.StatusCode = 400) {
                        $scope.AssetRolesCaption = jQuery.map($scope.GlobalRoles, function (relation) {
                            if ($.inArray(relation.ID.toString(), $scope.AssetAccess.ddlGlobalRole) != -1) return relation.Role;
                        }).join(', ');
                        if ($scope.AssetRolesCaption == "") $scope.AssetRolesCaption = "-";
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        if ($scope.IsShowallAssets.Fromshowallassets == 1) {
                            $scope.$emit('loadShowallAssetDetailSection', assetlist, $scope.IsShowallAssets.DamViewName);
                        }
                        if (damviewtype == 'Thumbnail') {
                            $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        } else if (damviewtype == 'Summary') {
                            $scope.$emit('loadAssetDetailSectionfrSummary', assetlist);
                        } else if (damviewtype == 'List') {
                            $scope.$emit('loadAssetDetailSectionfrList', assetlist);
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4383.Caption'));
                    }
                });
            }
        }
        $scope.ShowAssesAccesRoles = function (val) {
            $scope.AssetAccess.accesrolechange = val;
        }
        var fileextfrdownload = '';
        var fileguidfrdownload = '';
        var filenamefrdownload = '';
        $scope.loadcontextmenu = function (e) {
            fileextfrdownload = $(e.target).attr('data-extension');
            fileguidfrdownload = $(e.target).attr('data-Fileguid');
            filenamefrdownload = $(e.target).attr('data-Filename');
            var AssetId = $(e.target).attr('data-assetid');
            $scope.fileid = $(e.target).attr('data-fileid');
            var ActivefileId = $(e.target).attr('data-activeversion');
            if ($scope.fileid == ActivefileId) {
                $scope.shownewversion = true;
                $scope.activeversion = false;
            } else {
                $scope.activeversion = true;
                $scope.shownewversion = false;
            }
        }
        $scope.IsMailSender = false;
        $scope.MailprocessMailFlag = false;
        $scope.ShowSendContainer = function () {
            $scope.IsMailSender = true;
        }
        $scope.sendAssetMail = function () {
            if ($scope.AssetEditID != 0) {
                if ($('#sendMail').hasClass('disabled')) {
                    return;
                }
                $('#sendMail').addClass('disabled');
                $scope.MailprocessMailFlag = true;
                var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var assetSelection = [],
					toArray = new Array(),
					mailSubject = $scope.sendMailSubject != "" ? $scope.sendMailSubject : "Marcom Media Bank";
                for (var i = 0, mail; mail = $scope.SendMail.mailtags[i++];) {
                    if (mail.text.match(emailvalid)) {
                        toArray.push(mail.text);
                    } else {
                        $('#sendMail').removeClass('disabled');
                        $scope.MailprocessMailFlag = false;
                        return false;
                    }
                }
                assetSelection.push({
                    "AssetId": $scope.AssetEditID
                });
                if (assetSelection.length > 0 && toArray.length > 0) {
                    if ($('#checkmetadata').next('i').attr('class') == "checkbox") {
                        $scope.includemdata = false;
                    } else if ($('#checkmetadata').next('i').attr('class') == "checkbox checked") {
                        $scope.includemdata = true;
                    } else {
                        $scope.includemdata = false;
                    }
                    var srcObj = {};
                    srcObj.Toaddress = toArray.join(",");
                    srcObj.subject = mailSubject;
                    srcObj.AssetArr = assetSelection;
                    timerObj.sendmailTimer = $timeout(function () {
                        AssetEditService.SendMail(srcObj).then(function (result) {
                            if (result.Response == null) {
                                $('#sendMail').removeClass('disabled');
                                $scope.IsMailSender = false;
                                $scope.MailprocessMailFlag = false;
                                NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                            } else {
                                $('#sendMail').removeClass('disabled');
                                $scope.sendMailSubject = "";
                                $scope.MailprocessMailFlag = false;
                                $scope.SendMail.mailtags = [];
                                $scope.IsMailSender = false;
                                NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                            }
                        });
                    }, 100);
                } else {
                    $scope.MailprocessMailFlag = false;
                    $('#sendMail').removeClass('disabled');
                    $scope.IsMailSender = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.DecodedAssetName = function (name) {
            if (name != null & name != "") {
                return $('<div />').html(name).text();
            } else {
                return "";
            }
        };
        $scope.AssetTitleChange = function (AssetName) {
            $scope.AssetAccess.name = AssetName;
            timerObj.assettitleTimer = $timeout(function () {
                $("#AssetNameChange").focus().select()
            }, 10);
        }
        $scope.assetupdatedets = [];

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        $scope.UpdateAssetDetails = function (type) {
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            if (type == "name") {
                if ($scope.AssetAccess.name != "") {
                    $scope.fields.SingleLineTextValue_68 = $scope.AssetAccess.name;
                    $scope.AssetAccess.AssetName = $scope.AssetAccess.name;
                    $scope.assetupdatedets.push({
                        "AssetID": ID,
                        "AssetName": $scope.AssetAccess.name
                    });
                    AssetEditService.UpdateAssetDetails($scope.assetupdatedets).then(function (result) {
                        if (result.Response) {
                            var datetimetdy = ConvertDateToString(new Date.create());
                            var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                            $scope.AssetlastUpdatedOn = datetimetdy1;
                            NotifySuccess($translate.instant('LanguageContents.Res_4715.Caption'));
                            $("#Linkicon_" + ID).addClass('displayNone');
                            $scope.$emit('CallBackThumbnailAssetViewfrmMUI', $scope.assetFolderid == null ? 0 : $scope.assetFolderid);
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4336.Caption'));
                        }
                        $scope.assetupdatedets = [];
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_4095.Caption'));
                    return false;
                }
            }
        }
        $scope.select2Options = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.showOriginal = function (filepath) {
            var urlstring = "";
            AssetEditService.getkeyvaluefromwebconfig("DocTemplateURLForView").then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    urlstring = result.Response;
                    urlstring = urlstring.replace("[docpath]", filepath + ".docx");
                    urlstring = urlstring.replace("[assetname]", $scope.AssetAccess.AssetName);
                    $('#optimakerIFrameSaveWordTemplate').css('display', 'none');
                    $('#wordeditiframe').attr('src', urlstring.toString());
                    timerObj.iFramepopupTimer = $timeout(function () {
                        $('#wordeditiframe').load(function () {
                            $('#wordeditiframe').css('visibility', 'visible');
                        });
                        $("#word-iFramePopup").modal('show');
                    }, 500);
                    $("#wordeditiframe").addClass('heightcent');
                }
            });
        };
        $scope.editOriginal = function (filepath) {
            var urlstring = "";
            AssetEditService.getkeyvaluefromwebconfig("DocTemplateURLForEdit").then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    urlstring = result.Response;
                    urlstring = urlstring.replace("[docpath]", filepath + ".docx");
                    urlstring = urlstring.replace("[assetname]", $scope.AssetAccess.AssetName);
                    $('#wordeditiframe').attr('src', urlstring.toString());
                    $('#optimakerIFrameSaveWordTemplate').css('display', 'block');
                    timerObj.iFramepopupTimer = $timeout(function () {
                        $('#wordeditiframe').load(function () {
                            $('#wordeditiframe').css('visibility', 'visible');
                        });
                        $("#word-iFramePopup").modal('show');
                    }, 500);
                }
            });
        };
        $scope.LoadassetsfrmOptimaker = function () {
            $scope.$broadcast('CallbackfromOptiIfrm', 0);
        };
        $scope.SaveEditDoc = function () {
            $('#wordeditiframe').contents().find('#SaveDocFile').click();
        };
        $scope.downloadEditWordPDF = function () {
            $('#asseteditloadingDownloadPageModel').modal("show");
            var a = document.createElement('a'),
				fileid = $scope.OriginalDownload.FileID,
				extn = ".pdf";
            var filename = $scope.OriginalDownload.FileName + extn;
            a.href = 'DAMDownload.aspx?FileID=' + $scope.OriginalDownload.FileID + '&FileFriendlyName=' + $scope.OriginalDownload.FileName + '&Ext=.pdf';
            a.download = $scope.OriginalDownload.FileName + extn;
            document.body.appendChild(a);
            $('#asseteditloadingDownloadPageModel').modal("hide");
            a.click();
            timerObj.newsfeedinfoTimer = $timeout(function () {
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                $scope.newsFeedinfo.push({
                    "AssetID": ID,
                    "action": "Download Asset",
                    "TypeName": "Asset Download"
                });
                AssetEditService.CreateNewsFeedinfo($scope.newsFeedinfo).then(function (result) {
                    if (result.Response == null) { } else {
                        $scope.newsFeedinfo = [];
                        ID = 0;
                    }
                });
            }, 100);
        }
        $scope.GetNewsFeedforPaging = function () {
            try {
                $('#AssetFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;
                        var ID = 0;
                        if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                        else ID = $scope.AssetEditID;
                        $scope.userimgsrc = '';
                        $scope.commentuserimgsrc = '';
                        AssetEditService.GetAssetFeeds(ID, PagenoforScroll).then(function (result) {
                            var getEntityNewsFeedResultForScroll = result;
                            var feeddivHtml = '';
                            if (getEntityNewsFeedResultForScroll.Response.length > 0) tempNewsFeedHolder = $.merge(getEntityNewsFeedResultForScroll.Response, tempNewsFeedHolder);
                            for (var i = 0; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                                feeddivHtml = '';
                                var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                                if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >Comment</a></span>';
                                if (feedcomCount > 1) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment">';
                                if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#AssetFeedsdiv').append(feeddivHtml);
                                feeddivHtml = '';
                            }
                        });
                    }
                });
            } catch (e) { }
        }
        $scope.AddNewsFeed = function () {
            if ($("#Assetfeedtextholder").text() != "") {
                return false;
            }
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            var addnewsfeed = {};
            addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
            addnewsfeed.TemplateID = 104;
            addnewsfeed.EntityID = (params.ID != undefined) ? parseInt(params.ID) : $scope.assetEntityId;
            addnewsfeed.TypeName = "";
            addnewsfeed.AttributeName = "";
            addnewsfeed.FromValue = "";
            addnewsfeed.ToValue = $('#Assetfeedcomment').text();
            addnewsfeed.associatedentityid = ID;
            addnewsfeed.version = $scope.ActiveVersionNo;
            addnewsfeed.PersonalUserIds = $scope.PersonalUserIdsInAssetEdit;
            AssetEditService.FeedAsset(addnewsfeed).then(function (savenewsfeed) { });
            $scope.PersonalUserIdsInAssetEdit = [];
            $('#Assetfeedcomment').empty();
            if (document.getElementById('Assetfeedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                document.getElementById('Assetfeedcomment').innerHTML = '<span id=\'Assetfeedtextholder\' class=\'placeholder\'>Write a comment...</span>';
            }
            timerObj.latestfeedTimer = $timeout(function () {
                $scope.TimerForLatestFeed();
            }, 100);
        };
        $scope.LoadAssetNewsFeedBlock = function (navAssetID) {
            PagenoforScroll = 0;
            $('#AssetFeedsdiv').unbind('scroll');
            $("#AssetFeedsdiv").unbind("click");
            $scope.GetNewsFeedforPaging();
            var ID = 0;
            if (navAssetID > 0) ID = navAssetID;
            else if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            try {
                $('#AssetFeedsdiv').html('');
                AssetEditService.GetAssetFeeds(ID, 0).then(function (result) {
                    var getEntityNewsFeedResult = result;
                    tempNewsFeedHolder = [];
                    tempNewsFeedHolder = $.merge(getEntityNewsFeedResult.Response, tempNewsFeedHolder);
                    var feeddivHtml = '';
                    $('#AssetFeedsdiv').html('');
                    for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                        feeddivHtml = '';
                        var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                        if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                            $scope.userimgsrc = $scope.NewUserImgSrc;
                        } else {
                            $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                        }
                        feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                        feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                        if (feedcomCount > 1) {
                            feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                        } else {
                            feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                        }
                        feeddivHtml = feeddivHtml + '<ul class="subComment">';
                        if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                            var j = 0;
                            if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }
                            for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                if (k == 0) {
                                    feeddivHtml = feeddivHtml + '<li';
                                } else {
                                    feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                }
                                feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                feeddivHtml = feeddivHtml + '</div></div></li>';
                            }
                        }
                        feeddivHtml = feeddivHtml + '</ul></li>';
                        feedcomCount = 0;
                        $('#AssetFeedsdiv').append(feeddivHtml);
                        feeddivHtml = '';
                    }
                    $('#AssetFeedsdiv').scrollTop(1);
                    timerObj.NewsFeedUniqueTimerAsset = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);
                });
            } catch (e) { }
            $scope.TimerForLastestFeedCallBack = function () {
                if (FeedInitiated != true) {
                    $scope.TimerForLatestFeed();
                }
                if (timerObj.NewsFeedUniqueTimerAsset != undefined) {
                    timerObj.NewsFeedUniqueTimerAsset = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);
                }
            }
            try {
                $scope.TimerForLatestFeed = function () {
                    if (navAssetID > 0) ID = navAssetID;
                    else if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                    else ID = $scope.AssetEditID;
                    FeedInitiated = true;
                    AssetEditService.GetLastAssetFeeds(ID).then(function (getEntityNewsFeedResult) {
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult.Response.length > 0) tempNewsFeedHolder = getEntityNewsFeedResult.Response;
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            feeddivHtml = '';
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment"';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            $('#AssetFeedsdiv [data-id]').each(function () {
                                if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult.Response[i].FeedId) {
                                    $(this).remove();
                                }
                            });
                            $('#AssetFeedsdiv').prepend(feeddivHtml);
                            feedcomCount = 0;
                        }
                        FeedInitiated = false;
                    });
                }
            } catch (e) { }
            $('#AssetFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = $.grep(tempNewsFeedHolder, function (e) {
                    return e.FeedId == parseInt(feedid);
                });
                $("#Overviewfeed_" + feedid).next('div').hide();
                if (feedfilter != '') {
                    for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                        if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                            $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                            if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                            else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        } else {
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                            $(this)[0].innerHTML = "Hide comment(s)";
                        }
                    }
                }
            });
            $('#AssetFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                timerObj.commentTimer = $timeout(function () {
                    $('#feedcomment_' + commentuniqueid).html('').focus();
                }, 10);
            });
            $('#AssetFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {
                var entityid = $(this).attr('data-entityid');
                var taskid = $(this).attr('data-taskid');
                var typeid = $(this).attr('data-typeid');
                AssetEditService.IsActiveEntity(taskid).then(function (result) {
                    if (result.Response == true) {
                        $("#loadNotificationtask").modal("show");
                        $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, entityid, params.ID]);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $('#AssetFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);
                    AssetEditService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#AssetFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
        };
        $scope.downloadcustom = function () {
            $scope.CustomDownloadbtn = true;
            $scope.CustomImgDiv = true;
            $scope.CustomCancelshow = true;
            $scope.CustomDownloadProfileOptionDiv = true;
            $scope.CustomdownOptionDiv = false;
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            if (ID !== undefined) {
                timerObj.assetactivefileTimer = $timeout(function () {
                    AssetEditService.GetAssetActiveFileinfo(ID).then(function (result) {
                        if (result.Response != "") {
                            var generatedids = result.Response;
                            var Activefileexn = result.Response[0].Extension;
                            $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                            var addinfo = (result.Response[0].Additionalinfo != "") ? JSON.parse(result.Response[0].Additionalinfo) : "";
                            if (result.Response[0].Size != undefined) {
                                $scope.orgSize = parseSize(result.Response[0].Size);
                                $scope.ActiveVersionSize = parseSize(result.Response[0].Size);
                            }
                            if (addinfo != undefined && addinfo != "") {
                                $scope.orgWidth = addinfo.Width;
                                $scope.orgHeight = addinfo.Height;
                                $scope.orgDpi = addinfo.Dpi;
                                $scope.ActiveVersionDpi = addinfo.Dpi;
                                $scope.IsResolution = true;
                                $scope.IsDPI = true;
                                if (addinfo.FrameRate != "") $scope.FrameRate = addinfo.FrameRate.replace('fps', '');
                                else $scope.FrameRate = 0;
                            } else {
                                $scope.orgWidth = 0;
                                $scope.orgHeight = 0;
                                $scope.orgDpi = 0;
                                $scope.ActiveVersionDpi = 0;
                                $scope.IsResolution = false;
                                $scope.IsDPI = false;
                                $scope.FrameRate = 0;
                            }
                        }
                        getprofile();
                        GetMimeTypevalue();
                        timerObj.profilesetingTimer = $timeout(function () {
                            profileseting();
                        }, 100);
                    });
                }, 100);
            }
        }
        $scope.crop = function () {
            $scope.cropImgDiv = true;
            $scope.CropApplybtn = true;
            $scope.StaticOptionDiv = true;
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            if (ID !== undefined) {
                timerObj.assetactivefileTimer = $timeout(function () {
                    AssetEditService.GetAssetActiveFileinfo(ID).then(function (result) {
                        if (result.Response != "") {
                            var generatedids = result.Response;
                            var Activefileexn = result.Response[0].Extension;
                            $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                            var addinfo = (result.Response[0].Additionalinfo != "") ? JSON.parse(result.Response[0].Additionalinfo) : "";
                            if (addinfo != undefined && addinfo != "") {
                                $scope.orgWidth = addinfo.Width;
                                $scope.orgHeight = addinfo.Height;
                                $scope.orgDpi = addinfo.Dpi;
                            } else {
                                $scope.orgWidth = 0;
                                $scope.orgHeight = 0;
                                $scope.orgDpi = 0;
                            }
                        }
                        initJcrop(0, 0);
                        GetMimeTypevalue();
                        getprofile();
                    });
                }, 100);
            }
        }
        $scope.cropopen = function () {
            Cropdefault();
            $scope.CropUnit = 1;
            $scope.crop();
        }
        $scope.Customdownload = function () {
            var Dpi = 0;
            var fileformate = "";
            var ScaleHeight = 0;
            var ScaleWidth = 0;
            $scope.validcropinput = true;
            if ($scope.CustomdownFormat == 0) {
                if ($scope.profileid > 0) {
                    Dpi = $scope.ProfileDpi;
                    fileformate = $scope.Profileformate;
                    ScaleWidth = $scope.ProfileWidth;
                    ScaleHeight = $scope.ProfileHeight;
                } else {
                    $scope.validcropinput = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_4638.Caption'));
                    return false;
                }
            } else if ($scope.CustomdownFormat == 1) {
                Dpi = $scope.CusDpi;
                fileformate = $scope.CusFileFormat;
                ScaleWidth = $scope.CusWidth;
                ScaleHeight = $scope.CusHeight;
            }
            $("input[id*='Cusdown']").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $scope.validcropinput = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_4593.Caption'));
                    return false;
                }
            });
            if ($scope.validcropinput) {
                $scope.CropprocessFlag = true;
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID
                else ID = $scope.AssetEditID;
                $scope.fileCropRescale.push({
                    "AssetID": ID,
                    "TopLeft": 0,
                    "TopRight": 0,
                    "BottomLeft": $scope.orgWidth,
                    "BottomRight": $scope.orgHeight,
                    "CropFormat": 1,
                    "ScaleWidth": ScaleWidth,
                    "ScaleHeight": ScaleHeight,
                    "Dpi": Dpi,
                    "profileid": $scope.profileid,
                    "fileformate": fileformate,
                    "cropfrom": 1
                });
                AssetEditService.CreateCropRescale($scope.fileCropRescale).then(function (result) {
                    if (result.Response != null && result.Response != undefined) {
                        $scope.CustompDownload.FilePath = result.Response[0].FilePath
                        $scope.CustompDownload.FileName = result.Response[0].FileName;
                        $scope.CustompDownload.FileExt = result.Response[0].FileFomat;
                        $('#asseteditloadingDownloadPageModel').modal("show");
                        var a = document.createElement('a'),
							FilePath = $scope.CustompDownload.FilePath,
							extn = $scope.CustompDownload.FileExt;
                        var filename = $scope.CustompDownload.FileName + extn;
                        a.href = 'DAMDownload.aspx?FilePath=' + $scope.CustompDownload.FilePath + '&FileFriendlyName=' + $scope.CustompDownload.FileName + '&CropDownload=1&Ext=' + $scope.CustompDownload.FileExt + '';
                        a.download = $scope.CustompDownload.FileName;
                        document.body.appendChild(a);
                        $('#asseteditloadingDownloadPageModel').modal("hide");
                        a.click();
                        $scope.CropprocessFlag = false;
                        $scope.CustomCancelshow = true;
                        NotifySuccess($translate.instant('LanguageContents.Res_4784.Caption'));
                    } else {
                        $scope.CropprocessFlag = false;
                        $scope.CustomDownloadbtn = true;
                        $scope.CustomCancelshow = true;
                        NotifyError($translate.instant('LanguageContents.Res_4260.Caption'));
                    }
                    $scope.fileCropRescale = [];
                });
            }
        }

        function GetMimeTypevalue() {
            AssetEditService.GetMimeType().then(function (data) {
                if (data.Response != null) {
                    $scope.mimes = data.Response;
                }
            });
        }
        $scope.changeCropFormat = function () {
            if ($scope.CropFormat == 0) {
                $scope.StaticOptionDiv = true;
                $scope.ProfileOptionDiv = false;
                $scope.CustomOptionDiv = false;
                $scope.profileid = 0;
                $scope.Cusratio = false;
                $scope.Customratiospan = false;
                $('#CusratioIcon').removeClass('checked');
            } else if ($scope.CropFormat == 1) {
                $scope.StaticOptionDiv = false;
                $scope.ProfileOptionDiv = true;
                $scope.CustomOptionDiv = false;
                $scope.Cusratio = false;
                $scope.Customratiospan = false;
                timerObj.profilesetingTimer = $timeout(function () {
                    profileseting();
                }, 200);
                $('#CusratioIcon').removeClass('checked');
            } else if ($scope.CropFormat == 2) {
                $scope.profileid = 0;
                $scope.StaticOptionDiv = false;
                $scope.ProfileOptionDiv = false;
                $scope.CustomOptionDiv = true;
                $scope.CusWidth = $scope.orgWidth;
                $scope.CusHeight = $scope.orgHeight;
                $scope.CusDpi = $scope.orgDpi;
                $scope.CusFileFormat = $scope.Activefileformat;
            }
        }

        function getprofile() {
            var UserId = $cookies['UserId'];
            AssetEditService.GetProfileFilesByUser(UserId).then(function (result) {
                if (result.Response != "") {
                    $scope.DamProfileType = result.Response;
                }
            });
        }

        function profileseting() {
            if ($scope.DamProfileType != null && $scope.DamProfileType != undefined) {
                $scope.profileid = $scope.DamProfileType[0].ID;
                $scope.ProfileType = $scope.DamProfileType[0].ID;
                $scope.ProfileWidth = $scope.DamProfileType[0].Width;
                $scope.ProfileHeight = $scope.DamProfileType[0].Height;
                $scope.Profileformate = $scope.DamProfileType[0].Extension;
                $scope.ProfileDpi = $scope.DamProfileType[0].DPI;
            } else {
                $scope.profileid = 0;
                $scope.ProfileType = 0;
                $scope.ProfileWidth = 0;
                $scope.ProfileHeight = 0;
                $scope.Profileformate = "-";
                $scope.ProfileDpi = 0;
            }
        }
        $scope.changeProfileType = function () {
            $scope.profileid = $scope.ProfileType;
            var Profile = $.grep($scope.DamProfileType, function (e) {
                return e.ID == $scope.ProfileType;
            });
            if (Profile.length > 0 && Profile != undefined) {
                $scope.ProfileWidth = Profile[0].Width;
                $scope.ProfileHeight = Profile[0].Height;
                $scope.Profileformate = Profile[0].Extension;
                $scope.ProfileDpi = Profile[0].DPI;
            } else {
                $scope.ProfileWidth = 0;
                $scope.ProfileHeight = 0;
                $scope.Profileformate = "-";
                $scope.ProfileDpi = 0;
            }
        }
        $scope.CropprocessFlag = false;

        function initJcrop(WidthRatio, HeightRatio) {
            if ($('#cropImgContainer .jcrop-holder').length > 0) {
                jcrop_api.destroy();
                $('#cropasseteditimage').removeAttr('style');
            }
            var originalImgHeight = jQuery('#cropImgContainer #cropasseteditimage').height();
            var originalImgWidth = jQuery('#cropImgContainer #cropasseteditimage').width();
            var orgWidth = $scope.orgWidth;
            var orgHeight = $scope.orgHeight;
            $('#cropasseteditimage').Jcrop({
                boxWidth: orgWidth,
                boxHeight: orgHeight,
                onChange: showPreview,
                onSelect: showPreview,
                trueSize: [orgWidth, orgHeight],
                setSelect: [0, 0, originalImgWidth, originalImgHeight],
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: WidthRatio / HeightRatio
                });
                jcrop_api.focus();
                var w = $scope.orgWidth;
                var h = $scope.orgHeight;
                jcrop_api.animateTo([0, 0, w, h]);
            });

            function showPreview(c) {
                $scope.PreviewHeight = $('#cropImgContainer #cropasseteditimage').height();
                $scope.PreviewWidth = $('#cropImgContainer #cropasseteditimage').width();
                var Width = parseInt(c.w);
                var Height = parseInt(c.h);
                var topx = parseInt(c.x);
                var topy = parseInt(c.y);
                var btnx = parseInt(c.x2);
                var btny = parseInt(c.y2);
                $scope.corpWidth = 0;
                $scope.corpHeight = 0;
                $scope.TopXValue = parseInt(c.x);
                $scope.TopYValue = parseInt(c.y);
                $scope.ButtonXValue = parseInt(c.x2);
                $scope.ButtonYValue = parseInt(c.y2);
                if (Width < 0) {
                    Width = Width * (-1);
                }
                if (Height < 0) {
                    Height = Height * (-1);
                }
                $scope.corpWidth = Math.round(c.w);
                $scope.corpHeight = Math.round(c.h);
                $(document).trigger("OncorpImageChange", c);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $(document).on("OncorpImageChange", function (event, c) {
            $scope.corpWidth = Math.round(c.w);
            $scope.corpHeight = Math.round(c.h);
            if ($scope.Cusratio == true) {
                $scope.CusWidth = Math.round(c.w);
                $scope.CusHeight = Math.round(c.h);
            } else {
                $scope.CusWidth = $scope.orgWidth;
                $scope.CusHeight = $scope.orgHeight;
            }
        });
        $scope.fileCropRescale = [];
        $scope.CropRescalevalue = function (callingoption) {
            if ($('#Cropcalling').hasClass('disabled')) {
                return;
            }
            $('#Cropcalling').addClass('disabled');
            var Dpi = 0;
            var fileformate = "";
            var ScaleHeight = 0;
            var ScaleWidth = 0;
            $scope.validcropinput = true;
            if ($scope.CropFormat == 0) {
                Dpi = $scope.orgDpi;
            } else if ($scope.CropFormat == 1) {
                if ($scope.profileid > 0) {
                    Dpi = $scope.ProfileDpi;
                    fileformate = $scope.Profileformate;
                    ScaleWidth = $scope.ProfileWidth;
                    ScaleHeight = $scope.ProfileHeight;
                } else {
                    $scope.validcropinput = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_4638.Caption'));
                    $('#Cropcalling').removeClass('disabled');
                    return false;
                }
            } else if ($scope.CropFormat == 2) {
                Dpi = $scope.CusDpi;
                fileformate = $scope.CusFileFormat;
                ScaleWidth = $scope.CusWidth;
                ScaleHeight = $scope.CusHeight;
            }
            $("input[id*='Cus']").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $('#Cropcalling').removeClass('disabled');
                    return false;
                }
            });
            if ($scope.validcropinput) {
                $scope.CropprocessFlag = true;
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                $scope.fileCropRescale.push({
                    "AssetID": ID,
                    "TopLeft": $scope.TopXValue,
                    "TopRight": $scope.TopYValue,
                    "BottomLeft": $scope.ButtonXValue,
                    "BottomRight": $scope.ButtonYValue,
                    "CropFormat": $scope.CropFormat,
                    "ScaleWidth": ScaleWidth,
                    "ScaleHeight": ScaleHeight,
                    "Dpi": Dpi,
                    "profileid": $scope.profileid,
                    "fileformate": fileformate,
                    "cropfrom": 2
                });
                AssetEditService.CreateCropRescale($scope.fileCropRescale).then(function (result) {
                    if (result.Response != null && result.Response != undefined) {
                        $scope.CropDownload.FilePath = result.Response[0].FilePath
                        $scope.CropDownload.FileName = result.Response[0].FileName;
                        $scope.CropDownload.FileExt = result.Response[0].FileFomat;
                        if (callingoption == 1) {
                            $('#asseteditloadingDownloadPageModel').modal("show");
                            var a = document.createElement('a'),
								FilePath = $scope.CropDownload.FilePath,
								extn = $scope.CropDownload.FileExt;
                            var filename = $scope.CropDownload.FileName + extn;
                            a.href = 'DAMDownload.aspx?FilePath=' + $scope.CropDownload.FilePath + '&FileFriendlyName=' + $scope.CropDownload.FileName + '&CropDownload=2&Ext=' + $scope.CropDownload.FileExt + '';
                            a.download = $scope.CropDownload.FileName;
                            document.body.appendChild(a);
                            $('#asseteditloadingDownloadPageModel').modal("hide");
                            Cropdefault();
                            a.click();
                        }
                        Cropdefault();
                        $scope.CropprocessFlag = false;
                        $('#Cropcalling').removeClass('disabled');
                        NotifySuccess($translate.instant('LanguageContents.Res_4783.Caption'));
                    } else {
                        $scope.CropprocessFlag = false;
                        $scope.CropApplybtn = true;
                        $('#Cropcalling').removeClass('disabled');
                        NotifyError($translate.instant('LanguageContents.Res_4259.Caption'));
                    }
                    $scope.fileCropRescale = [];
                    $scope.corpWidth = 0;
                    $scope.corpHeight = 0;
                    $('#cropAssetPopup').modal("hide");
                });
            }
        }
        $scope.CropCancel = function () {
            Cropdefault();
        }
        $scope.CustomCancel = function () {
            Cropdefault();
        }
        $scope.changeCustomdownFormat = function () {
            if ($scope.CustomdownFormat == 0) {
                $scope.StaticOptionDiv = false;
                $scope.CustomDownloadProfileOptionDiv = true;
                $scope.CustomdownOptionDiv = false;
                timerObj.profilesetingTimer = $timeout(function () {
                    profileseting();
                }, 200);
            } else if ($scope.CustomdownFormat == 1) {
                $scope.profileid = 0;
                $scope.StaticOptionDiv = false;
                $scope.CustomDownloadProfileOptionDiv = false;
                $scope.CustomdownOptionDiv = true;
                $scope.CusWidth = $scope.orgWidth;
                $scope.CusHeight = $scope.orgHeight;
                $scope.CusDpi = $scope.orgDpi;
                $scope.CusFileFormat = $scope.Activefileformat;
            }
        }

        function Cropdefault() {
            wRatio = 0;
            hRatio = 0;
            $scope.CustomdownOptionDiv = false;
            $scope.CustomDownloadProfileOptionDiv = false
            $scope.CustomdownFormat = 0;
            $scope.CustomDownloadbtn = true;
            $scope.CustomImgDiv = true;
            $scope.CustomCancelshow = true;
            $scope.Cusratio = false;
            Cusratiocheckbox = null
            $('#CusratioIcon').removeClass('checked');
            $scope.DamProfileType = {};
            $scope.CropDownload = {
                FileName: "",
                FilePath: "",
                FileExt: ""
            };
            $scope.CustompDownload = {
                FileName: "",
                FilePath: "",
                FileExt: ""
            };
            $scope.CusRWidth = 0;
            $scope.CusRHeight = 0;
            $scope.Customratiospan = false;
            $scope.orgWidth = 0;
            $scope.orgHeight = 0;
            $scope.orgDpi = 0;
            $scope.corpWidth = 0;
            $scope.corpHeight = 0;
            $scope.ProfileWidth = 0;
            $scope.ProfileHeight = 0;
            $scope.Profileformate = "";
            $scope.ProfileDpi = 0;
            $scope.CropFormat = 0;
            $scope.ProfileType = 0;
            $scope.profileid = 0;
            $scope.CusDpi = 0;
            $scope.CusFileFormat = "-";
            $scope.CusWidth = 0;
            $scope.CusHeight = 0;
            $scope.CropApplybtn = false;
            $scope.CustomOptionDiv = false;
            $scope.ProfileOptionDiv = false;
            $scope.StaticOptionDiv = false;
            $scope.cropImgDiv = false;
            $scope.CustomImgDiv = false;
            $scope.validcropinput = true;
            $scope.mimes = {};
            $scope.orgSize = 0;
            if ($('#cropImgContainer .jcrop-holder').length > 0) {
                jcrop_api.destroy();
                $('#cropasseteditimage').removeAttr('style');
            }
        }
        $scope.selectCusratio = function (event) {
            Cusratiocheckbox = event.target;
            if (Cusratiocheckbox.checked) {
                $scope.Cusratio = true
                $scope.Customratiospan = true;
                var abGcd = gcd($scope.orgWidth, $scope.orgHeight);
                wRatio = $scope.orgWidth / abGcd;
                hRatio = $scope.orgHeight / abGcd;
                $scope.CusRWidth = wRatio;
                $scope.CusRHeight = hRatio;
            } else {
                initJcrop(0, 0);
                $scope.Cusratio = false
                $scope.Customratiospan = false;
                $scope.CusRWidth = 0;
                $scope.CusRHeight = 0;
            }
        }

        function gcd(a, b) {
            return (b == 0) ? a : gcd(b, a % b);
        }
        $("#CusRWidth").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusRWidth)) {
                initJcrop($scope.CusRWidth, $scope.CusRHeight);
                $scope.Cusratio = true
                $scope.Customratiospan = true;
            } else {
                $scope.CusRWidth = wRatio;
                bootbox.alert($translate.instant('LanguageContents.Res_4644.Caption'));
                return false;
            }
        });
        $("#CusRHeight").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusRHeight)) {
                initJcrop($scope.CusRWidth, $scope.CusRHeight);
                $scope.Cusratio = true
                $scope.Customratiospan = true;
            } else {
                $scope.CusRHeight = hRatio;
                bootbox.alert($translate.instant('LanguageContents.Res_4644.Caption'));
                return false;
            }
        });
        $("#CusWidth").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusWidth)) { } else {
                $scope.CusWidth = $scope.orgWidth;
                bootbox.alert($translate.instant('LanguageContents.Res_4645.Caption'));
                return false;
            }
        });
        $("#CusHeight").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusHeight)) { } else {
                $scope.CusHeight = $scope.orgHeight;
                bootbox.alert($translate.instant('LanguageContents.Res_4645.Caption'));
                return false;
            }
        });
        $("#CusDpi").focusout(function () {
            var re = /^[0-9.]+$/;
            if (re.test($scope.CusDpi)) { } else {
                $scope.CusDpi = $scope.orgDpi;
                bootbox.alert($translate.instant('LanguageContents.Res_4642.Caption'));
                return false;
            }
        });
        $("#CusdownWidth").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusWidth)) { } else {
                $scope.CusWidth = $scope.orgWidth;
                bootbox.alert($translate.instant('LanguageContents.Res_4645.Caption'));
                return false;
            }
        });
        $("#CusdownHeight").focusout(function () {
            var re = /^[0-9]+$/;
            if (re.test($scope.CusHeight)) { } else {
                $scope.CusHeight = $scope.orgHeight;
                bootbox.alert($translate.instant('LanguageContents.Res_4645.Caption'));
                return false;
            }
        });
        $("#CusDpi").focusout(function () {
            var re = /^[0-9.]+$/;
            if (re.test($scope.CusDpi)) { } else {
                $scope.CusDpi = $scope.orgDpi;
                bootbox.alert($translate.instant('LanguageContents.Res_4642.Caption'));
                return false;
            }
        });

        function TwoImaginePdfDownload(filepath, filename, flag, DocVersionID) {
            $.ajax({
                url: filepath,
                type: 'HEAD',
                error: function () { },
                success: function () {
                    blockUIForDownload();
                    $scope.ProgressMsgHeader = "Downloading Assets";
                    location.href = 'Dam2ImagineDownload.aspx?FileName=' + filename + '&resol=' + flag + '&token=' + $('#assetdownload_token_value_id').val() + '&docversionid=' + DocVersionID;
                }
            });
        }

        function OptimakerPdfDownload(filepath, filename, flag, DocVersionID) {
            $.ajax({
                url: filepath,
                type: 'HEAD',
                error: function () { },
                success: function () {
                    blockUIForDownload();
                    $scope.ProgressMsgHeader = "Downloading Assets";
                    location.href = 'DamOptimakerDocDownload.aspx?FileName=' + filename + '&resol=' + flag + '&token=' + $('#assetdownload_token_value_id').val() + '&docversionid=' + DocVersionID;
                }
            });
        }
        $scope.downloadMediafiles = function (flag) {
            var filepath = '';
            if (flag == "H" || flag == "L") {
                var filename = "";
                if (flag == "H") {
                    if (mediaAssetApproved) {
                        filename = $scope.mediaGeneratorAssetDetails.HighResPdf;
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_5541.Caption'));
                        return;
                    }
                } else filename = $scope.mediaGeneratorAssetDetails.LowResPdf;
                if (filename != null || filename != undefined) if ($scope.IsTemplate && flag == "H" && $scope.mediaGeneratorAssetDetails.TemplateType == 2) {
                    filepath = imageBaseUrlcloud + 'DAMFiles\\Templates\\MediaGenerator\\HighResPdf\\' + filename;
                    var fileLocationkey = filepath;
                    if (cloudsetup.storageType == clientFileStoragetype.Amazon) {
                        fileLocationkey = filepath.replace(imageBaseUrlcloud, TenantFilePath);
                    }
                    AssetEditService.IsFileExist(fileLocationkey).then(function (result) {
                        if (result.Response) {
                            TwoImaginePdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID);
                        } else {
                            $('#TemplatePdfGeneratingPageModel').modal("show");
                            AssetEditService.HighResPdfDownload(filename).then(function (DownloadResult) {
                                if (DownloadResult.Response) {
                                    $('#TemplatePdfGeneratingPageModel').modal("hide");
                                    TwoImaginePdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID);
                                }
                            })
                        }
                    });
                } else if ($scope.IsTemplate && flag == "L" && $scope.mediaGeneratorAssetDetails.TemplateType == 2) {
                    filepath = imageBaseUrlcloud + 'DAMFiles\\Templates\\MediaGenerator\\LowResPdf\\' + filename;
                    var fileLocationkey = filepath;
                    if (cloudsetup.storageType == clientFileStoragetype.Amazon) {
                        fileLocationkey = filepath.replace(imageBaseUrlcloud, TenantFilePath);
                    }
                    AssetEditService.IsFileExist(fileLocationkey).then(function (result) {
                        if (result.Response) {
                            TwoImaginePdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID)
                        } else {
                            var dwnldImagAsset = {};
                            dwnldImagAsset.entityID = params.ID;
                            dwnldImagAsset.folderID = $scope.assetFolderid;
                            dwnldImagAsset.userID = $cookies['UserId'];
                            dwnldImagAsset.docVersionID = $scope.TemplateAsset.docversionID;
                            dwnldImagAsset.serverUrl = $scope.TemplateAsset.serverUrl;
                            $('#TemplatePdfGeneratingPageModel').modal("show");
                            AssetEditService.Download2ImagineAsset(dwnldImagAsset).then(function (UrlRes) {
                                if (UrlRes.Response) {
                                    var arr = [];
                                    arr.push(UrlRes.Response);
                                    $scope.$emit('loadAssetDetailSectionfrThumbnail', arr);
                                    $('#TemplatePdfGeneratingPageModel').modal("hide");
                                    TwoImaginePdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID)
                                }
                            })
                        }
                    });
                } else if ($scope.IsTemplate && flag == "H" && $scope.mediaGeneratorAssetDetails.TemplateType == 1) {
                    timerObj.pdfdownloadTimer = $timeout(function () {
                        filepath = imageBaseUrlcloud + 'DAMFiles\\Templates\\MediaGenerator\\HighResPdf\\' + filename;
                        var fileLocationkey = filepath;
                        if (cloudsetup.storageType == clientFileStoragetype.Amazon) {
                            fileLocationkey = filepath.replace(imageBaseUrlcloud, TenantFilePath);
                        }
                        AssetEditService.IsFileExist(fileLocationkey).then(function (result) {
                            if (result.Response) {
                                OptimakerPdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID)
                            }
                        });
                    }, 10);
                } else {
                    timerObj.pdfdownloadTimer = $timeout(function () {
                        filepath = imageBaseUrlcloud + 'DAMFiles\\Templates\\MediaGenerator\\LowResPdf\\' + filename;
                        OptimakerPdfDownload(filepath, filename, flag, $scope.mediaGeneratorAssetDetails.DocVersionID)
                    }, 10);
                }
            } else if (flag == "J") {
                if ($scope.TemplateAsset.IsApprove) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5589.Caption'));
                } else {
                    if ($scope.TemplateAsset.templateType == 1) {
                        AssetEditService.GetMediaGeneratorSettingsBaseURL().then(function (data) {
                            var urlstring = data.Response.replace("[docversionid]", $scope.mediaGeneratorAssetDetails.DocVersionID).replace("[docid]", $scope.mediaGeneratorAssetDetails.DocID).replace("[mid]|[docid]", $scope.assetEntityId + "|" + $scope.mediaGeneratorAssetDetails.DocID + "|" + TenantID).replace("[fid]", $scope.assetFolderid).replace("[cid]", $cookies['UserId']);
                            $('#loadIframeImg').css('display', 'block');
                            $('#optimakeriframe').attr('src', urlstring.toString());
                            timerObj.iframeTimer = $timeout(function () {
                                $('#optimakeriframe').load(function () {
                                    $('#loadIframeImg').css('display', 'none');
                                    $('#optimakeriframe').css('visibility', 'visible');
                                });
                                $("#optiMaker-iFramePopup").modal('show');
                            }, 100);
                        });
                    } else if ($scope.TemplateAsset.templateType == 2) {
                        var getResUrl = {};
                        getResUrl.documentId = $scope.documentId;
                        getResUrl.serverUrl = $scope.TemplateAsset.serverUrl;
                        AssetEditService.Get2ImagineResourceUrl(getResUrl).then(function (urlRes) {
                            var urlstring = urlRes.Response;
                            $('#loadIframeImg').css('display', 'block');
                            $('#optimakeriframe').attr('src', urlstring.toString());
                            timerObj.iframeTimer = $timeout(function () {
                                $('#optimakeriframe').load(function () {
                                    $('#loadIframeImg').css('display', 'none');
                                    $('#optimakeriframe').css('visibility', 'visible');
                                });
                                $("#optiMaker-iFramePopup").modal('show');
                            }, 100);
                        });
                    }
                }
            }
        };
        var fileDownloadCheckTimer;

        function blockUIForDownload() {
            var token = new Date.create().getTime();
            $('#assetdownload_token_value_id').val(token);
            $scope.ProgressContent = "Downloading";
            $('#asseteditloadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                var cookieValue = $.cookie('fileDownloadToken');
                if (cookieValue == token) {
                    finishDownload();
                } else {
                    finishDownload();
                }
            }, 1000);
        }

        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken');
            if ($scope.IsTemplate) {
                timerObj.downloadmodalTimer = $timeout(function () {
                    $('#asseteditloadingDownloadPageModel').modal("hide");
                }, 1000);
            } else {
                timerObj.downloadmodalTimer = $timeout(function () {
                    $('#asseteditloadingDownloadPageModel').modal("hide");
                }, 500);
            }
        }
        $scope.$on("CallbackfromOptiIfrm", function () {
            $scope.$emit('CallBackThumbnailAssetView', 0);
            $("#AssetEditpopup").modal('hide');
        });
        $scope.callcontent = function (file) {
            var msg = "";
            msg += "<b>File name:</b> " + (file.Name != null ? file.Name.toString() : "-") + "<br/><br/>";
            msg += "<b>Uploaded by:</b> " + (file.OwnerName != null ? file.OwnerName.toString() : "-") + "<br/><br/>";
            msg += "<b>File size: </b> " + (file.Size != null ? parseSize(file.Size) : "-") + "<br/><br/>";
            return msg;
        };

        function parseSize(bytes) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
        $scope.DownloadAttachment = function () {
            $('#asseteditloadingDownloadPageModel').modal("show");
            var a = document.createElement('a'),
				fileid = fileguidfrdownload,
				extn = fileextfrdownload;
            var filename = filenamefrdownload + extn;
            a.href = 'DAMDownload.aspx?FileID=' + fileguidfrdownload + '&FileFriendlyName=' + filenamefrdownload + '&Ext=' + fileextfrdownload + '';
            a.download = filenamefrdownload + extn;
            document.body.appendChild(a);
            $('#asseteditloadingDownloadPageModel').modal("hide");
            a.click();
            timerObj.createfeedTimer = $timeout(function () {
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                $scope.newsFeedinfo.push({
                    "AssetID": ID,
                    "action": "Download Asset",
                    "TypeName": "Asset Download"
                });
                AssetEditService.CreateNewsFeedinfo($scope.newsFeedinfo).then(function (result) {
                    if (result.Response == null) { } else {
                        $scope.newsFeedinfo = [];
                        ID = 0;
                    }
                });
            }, 100);
        };
        $scope.SaveDetailBlockForLink = function (assetid, attrID, attributetypeid, url, name, linktype) {
            var savelink = {};
            savelink.EntityID = assetid;
            savelink.AttributetypeID = attributetypeid;
            savelink.AttributeID = attrID;
            savelink.NewUrl = url;
            savelink.linkType = linktype;
            savelink.linkname = name;
            savelink.moduleType = 5;
            AssetEditService.SaveDetailBlockForLink(savelink).then(function (result) {
                if (result.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                } else {
                    $scope.fields["SingleLineLinkUrl_" + attrID] = url;
                    $scope.fields["SingleLineLinkName_" + attrID] = name;
                    $scope.fields["SingleLineLinkType_" + attrID] = linktype;
                    bootbox.alert($translate.instant('LanguageContents.Res_4935.Caption'));
                }
            });
        }

        function GetAssetActiveFileinfo(assetid) {
            AssetEditService.GetAssetActiveFileinfo(assetid).then(function (result) {
                if (result.Response != "") {
                    $scope.IsImgInfo = true;
                    var generatedids = result.Response;
                    var Activefileexn = result.Response[0].Extension;
                    $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                    if ($scope.Activefileformat == "pdf") $scope.Ispdf = 1;
                    var addinfo = (result.Response[0].Additionalinfo != "") ? JSON.parse(result.Response[0].Additionalinfo) : "";
                    if (result.Response[0].Size != undefined) {
                        $scope.orgSize = parseSize(result.Response[0].Size);
                        $scope.ActiveVersionSize = parseSize(result.Response[0].Size);
                        $scope.IsSize = true;
                    }
                    if (addinfo != undefined && addinfo != "") {
                        $scope.orgWidth = addinfo.Width;
                        $scope.orgHeight = addinfo.Height;
                        $scope.orgDpi = addinfo.Dpi;
                        $scope.ActiveVersionDpi = addinfo.Dpi;
                        $scope.IsResolution = true;
                        $scope.IsDPI = true;
                        $scope.displayimgUnitmenu = false;
                        if (addinfo.FrameRate != "" && addinfo.FrameRate != null) $scope.FrameRate = addinfo.FrameRate.replace('fps', '');
                        else $scope.FrameRate = 0;
                        getassetdimensionalunitsettings();
                        timerObj.imageunitchangetimer = $timeout(function () {
                            $scope.imageunitchange($scope.userDimensionUnit);
                        }, 100);
                        if ($scope.ActiveVersionDpi > 0 || $scope.FrameRate > 0) $scope.IsDimensioninfo = true;
                    } else {
                        $scope.orgWidth = 0;
                        $scope.orgHeight = 0;
                        $scope.orgDpi = 0;
                        $scope.ActiveVersionDpi = 0;
                        $scope.IsResolution = false;
                        $scope.IsDPI = false;
                        $scope.IsDimensioninfo = false;
                    }
                } else {
                    $scope.IsImgInfo = false;
                }
            });
        }
        $scope.SetimgUnitmenu = function () {
            $scope.displayimgUnitmenu = true;
        }
        $scope.hidePopover = function () {
            $scope.displayimgUnitmenu = false;
        }
        $scope.imageunitchange = function (dimensionunitIds) {
            if (dimensionunitIds == 3) {
                $scope.convertWidth = pixels_dpi_to_mm($scope.orgWidth, $scope.orgDpi).toFixed(1);
                $scope.convertHeight = pixels_dpi_to_mm($scope.orgHeight, $scope.orgDpi).toFixed(1);
                $scope.imageunitshortFormat = 'mm'
            } else if (dimensionunitIds == 2) {
                $scope.convertWidth = pixels_dpi_to_inch($scope.orgWidth, $scope.orgDpi).toFixed(1);
                $scope.convertHeight = pixels_dpi_to_inch($scope.orgHeight, $scope.orgDpi).toFixed(1);
                $scope.imageunitshortFormat = 'inch'
            } else if (dimensionunitIds == 1 || dimensionunitIds == 0) {
                $scope.convertWidth = $scope.orgWidth;
                $scope.convertHeight = $scope.orgHeight;
                $scope.imageunitshortFormat = 'px'
            } else {
                AssetEditService.GetDimensionunitsettings().then(function (GetDimensionunitresult) {
                    if (GetDimensionunitresult.StatusCode == 200) {
                        $scope.userDimensionUnit = GetDimensionunitresult.Response.DimensionUnit;
                        if ($scope.userDimensionUnit != undefined || $scope.userDimensionUnit > 0) {
                            $scope.dimensionunitlist = $scope.userDimensionUnit;
                            timerObj.imageunitchangetimer = $timeout(function () {
                                $scope.imageunitchange($scope.userDimensionUnit);
                            }, 50);
                        } else {
                            $scope.dimensionunitlist = 1;
                            timerObj.imageunitchangetimer = $timeout(function () {
                                $scope.imageunitchange($scope.userDimensionUnit);
                            }, 50);
                        }
                    }
                });
            }
        }
        $scope.formatSizeUnits = function (bytes) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

        function pixels_dpi_to_cm(pixels, dpi) {
            var output;
            output = pixels / dpi;
            output = output * 2.54;
            return output;
        }

        function pixels_dpi_to_mm(pixels, dpi) {
            var output = 0;
            if (pixels > 0 && dpi > 0) {
                output = Math.round(pixels / dpi);
                output = Math.round(output * 25.4);
            }
            return output;
        }

        function pixels_dpi_to_inch(pixels, dpi) {
            var output = 0;
            if (pixels > 0 && dpi > 0) output = Math.round(pixels / dpi);
            return output;
        }
        $scope.SaveFileCheckList = function (CheckListID, VersionNo, ChklistName, ChkListStatus) {
            $scope.UpdateAssetVersion(VersionNo);
        }
        $scope.CheckListSelection = function (status) {
            var baseClass = "radio";
            if (status) baseClass += " checked";
            return baseClass;
        }
        $scope.OptionObj = {};
        $scope.fieldoptions = [];

        function createDynHTmlforAssetType(attrData) {
            $scope.dyn_Cont = ''
            $scope.DAMattributesRelationList = attrData;
            for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.DAMattributesRelationList[i].AttributeID != 70 && $scope.DAMattributesRelationList[i].AttributeID != 1 && $scope.DAMattributesRelationList[i].ID != 5) {
                        if ($scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.Name) {
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngTextSingleLine_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"fields.chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                        }
                        if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                            $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID];
                        } else {
                            $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                        }
                    }
                    if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1 && $scope.DAMattributesRelationList[i].AttributeID == 5) {
                        if ($scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.Name) {
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngTextSingleLine_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"fields.chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                        }
                        if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                            $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID];
                        } else {
                            $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                        }
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.DAMattributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngListSingleSelection_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"fields.chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                    } else if ($scope.DAMattributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                        if ($scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                            var value = $.grep($scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID], function (e) {
                                return e.Caption == $scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID];
                            })[0].Id
                            $scope.fields["chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = value;
                        } else {
                            $scope.fields["chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                        }
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngListSingleSelection_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.DAMattributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                    } else {
                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngListSingleSelection_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.DAMattributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                        if ($scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                            var value = $.grep($scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID], function (e) {
                                return e.Caption == $scope.fields["NormalDropDown_" + $scope.DAMattributesRelationList[i].AttributeID];
                            })[0].Id
                            $scope.fields["chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = value;
                        } else {
                            $scope.fields["chngListSingleSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                        }
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 2) {
                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngTextMultiLine_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" name=\"fields.chngTextMultiLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"fields.chngTextMultiLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                    if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                        $scope.fields["chngTextMultiLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID];
                    } else {
                        $scope.fields["chngTextMultiLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 4) {
                    $scope.fields["chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = [];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.OptionObj["chngoption_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngListMultiSelection_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  id=\"chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.chngoption_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,4)\"   ></select></div></div>";
                    if ($scope.fields["NormalMultiDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["NormalMultiDropDown_" + $scope.DAMattributesRelationList[i].AttributeID].length != 0 && $scope.fields["NormalMultiDropDown_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                        var selectedmultiselectvalue = $scope.fields["NormalMultiDropDown_" + $scope.DAMattributesRelationList[i].AttributeID].split(',');
                        for (var v = 0, val; val = selectedmultiselectvalue[v++];) {
                            var idval = $.grep($scope.DAMattributesRelationList[i].Options, function (e) {
                                return e.Caption == val.trim();
                            })[0].Id;
                            $scope.fields["chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID].push(parseInt(idval));
                        }
                    } else {
                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                $scope.fields["chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID].push(val.trim());
                            }
                        } else {
                            $scope.fields["chngListMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID] = "";
                        }
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 5 && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    if ($scope.fields["DateTime_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["DateTime_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                        $scope.fields["chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["DateTime_" + $scope.DAMattributesRelationList[i].AttributeID];
                    } else {
                        $scope.fields["chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                    }
                    $scope.MinValue = $scope.DAMattributesRelationList[i].MinValue;
                    $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                    $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                    $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                    if ($scope.MinValue < 0) {
                        $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                    } else {
                        $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                    }
                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                        $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                    } else {
                        $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                    }
                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                    $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                    $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'date_" + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"date_" + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(fields.chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID + "," + $scope.DAMattributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID + "\"></div></div>";
                    $scope.fields["fields.chngDateTime_" + $scope.DAMattributesRelationList[i].AttributeID] = false;
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 16) {
                    var temp = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.fields["chngSingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] = "-";
                    if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != null && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined & $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != '-') {
                        $scope.fields["chngSingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] = $('<div />').html($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID]).text();
                    }
                    var dateExpire;
                    if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != "-" && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != null) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        dateExpireUTCval = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID].substr(6, ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID].indexOf('+') - 6));
                        dateExpireval = new Date.create(parseInt(dateExpireUTCval));
                        dateExpire = ConvertDateFromStringToString(ConvertDateToString(dateExpireval));
                    } else {
                        dateExpire = "-"
                    }
                    $scope.fields["ExpireDateTime_" + $scope.DAMattributesRelationList[i].AttributeID] = dateExpire;
                    if ($scope.DAMattributesRelationList[i].IsReadOnly == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.DAMattributesRelationList[i].AttributeID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.DAMattributesRelationList[i].Caption + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.DAMattributesRelationList[i].AttributeID + '}}</span></div></div>';
                    } else {
                        if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false && $scope.ProductionEntityID > 0) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.DAMattributesRelationList[i].AttributeID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.DAMattributesRelationList[i].Caption + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.DAMattributesRelationList[i].AttributeID + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.DAMattributesRelationList[i].Caption + '" ng-click="openExpireAction($event)">{{fields.ExpireDateTime_' + $scope.DAMattributesRelationList[i].AttributeID + '}}</a></span></div></div>';
                        } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.DAMattributesRelationList[i].AttributeID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.DAMattributesRelationList[i].Caption + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.DAMattributesRelationList[i].AttributeID + '}}</span></div></div>';
                        }
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 10) {
                    var temp = periodforTypeEdit;
                    if (periodforTypeEdit == '-') {
                        periodforTypeEdit = [];
                    }
                    $scope.itemsfredit = [];
                    if (periodforTypeEdit.length > 0) {
                        for (var k = 0; k < periodforTypeEdit.length; k++) {
                            var datStartUTCval = "";
                            var datstartval = "";
                            var datEndUTCval = "";
                            var datendval = "";
                            if (periodforTypeEdit[k].Startdate != undefined && periodforTypeEdit[k].Startdate != '' && periodforTypeEdit[k].Startdate != null) {
                                datStartUTCval = periodforTypeEdit[k].Startdate.substr(6, (periodforTypeEdit[k].Startdate.indexOf('+') - 6));
                                datstartval = new Date.create(parseInt(datStartUTCval));
                            }
                            if (periodforTypeEdit[k].EndDate != undefined && periodforTypeEdit[k].EndDate != '' && periodforTypeEdit[k].EndDate != null) {
                                datEndUTCval = periodforTypeEdit[k].EndDate.substr(6, (periodforTypeEdit[k].EndDate.indexOf('+') - 6));
                                datendval = new Date.create(parseInt(datEndUTCval));
                            }
                            $scope.itemsfredit.push({
                                startDate: datstartval.addDays(1),
                                endDate: datendval.addDays(1),
                                comment: periodforTypeEdit[k].Description,
                                sortorder: parseInt(periodforTypeEdit[k].SortOrder)
                            });
                        }
                    } else {
                        $scope.itemsfredit.push({
                            startDate: [],
                            endDate: [],
                            comment: '',
                            sortorder: 0
                        });
                    }
                    $scope.items = [];
                    $scope.items = $scope.itemsfredit;
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.OptionObj["option_" + k + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                    $scope.MinValue = $scope.DAMattributesRelationList[i].MinValue;
                    $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                    $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                    $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                    if ($scope.MinValue < 0) {
                        $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                    } else {
                        $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                    }
                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                        $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                    } else {
                        $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                    }
                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                    $scope.fields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                    $scope.fields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                    $scope.dyn_Cont += "<div class=\"control-group\">";
                    $scope.dyn_Cont += "<label for=\"fields.TextSingleLine_ " + k + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\">";
                    $scope.dyn_Cont += "<div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                    $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                    $scope.dyn_Cont += "<input class=\"sdate Period_" + k + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + k + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'item.startDate')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.startDate\" min-date=\"fields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\"/>";
                    $scope.dyn_Cont += "<input class=\"edate Period_" + k + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + k + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"Calanderopen($event,'item.endDate')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.endDate\" min-date=\"fields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/>";
                    $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + k + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.DAMattributesRelationList[i].Caption + " Comment --\" />";
                    $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                    $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\">";
                    $scope.dyn_Cont += "<i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.DAMattributesRelationList[i].Caption + "]</a></div></div>";
                    $scope.fields["Period_" + k + $scope.DAMattributesRelationList[i].AttributeID] = "";
                    $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                    $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID] != "-") {
                        $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["SingleLineTextValue_" + $scope.DAMattributesRelationList[i].AttributeID];
                    } else {
                        $scope.fields["chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID] = $('<div />').html($scope.DAMattributesRelationList[i].DefaultValue).text();
                    }
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngTextSingleLine_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"fields.chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"chngTextSingleLine_" + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 12) {
                    var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                    for (var j = 0; j < totLevelCnt1; j++) {
                        if (totLevelCnt1 == 1) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngMultiSelectDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                            $scope.dyn_Cont += "<input ui-select2 =\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.chngMultiSelectDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            $scope.setFieldKeys();
                        } else {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngMultiSelectDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2 =\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.chngMultiSelectDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            } else {
                                $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                if (j == ($scope.DAMattributesRelationList[i].Levels.length - 1)) {
                                    $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngMultiSelectDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.chngMultiSelectDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngMultiSelectDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.chngMultiSelectDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                        }
                        $scope.setFieldKeys();
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 17) {
                    $scope.fields["ListTagwords_" + $scope.DAMattributesRelationList[i].AttributeID] = [];
                    if ($scope.fields["TagWordsSeleted_" + $scope.DAMattributesRelationList[i].AttributeID] != '-' && $scope.fields["TagWordsSeleted_" + $scope.DAMattributesRelationList[i].AttributeID] != null && $scope.fields["TagWordsSeleted_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined) {
                        $scope.fields["ListTagwords_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["TagWordsSeleted_" + $scope.DAMattributesRelationList[i].AttributeID];
                    } else {
                        $scope.fields["ListTagwords_" + $scope.DAMattributesRelationList[i].AttributeID] = [];
                    }
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.OptionObj["tagoption_" + $scope.DAMattributesRelationList[i].AttributeID] = [];
                    $scope.tempscope = [];
                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                    $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label>";
                    $scope.dyn_Cont += "<div class=\"controls\">";
                    $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.DAMattributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.DAMattributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                    $scope.dyn_Cont += "</div></div>";
                    if ($scope.DAMattributesRelationList[i].InheritFromParent) { } else { }
                    $scope.setFieldKeys();
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 6) {
                    var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                    for (var j = 0; j < $scope.DAMattributesRelationList[i].Levels.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                        $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                        $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                        $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                            return item.Caption
                        };
                        $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                            return item.Caption
                        };
                        if (j == 0) {
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                            $scope.dyn_Cont += "<input ui-select2=\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",6 )\"  ng-model=\"fields.chngDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                        } else {
                            $scope.chngDropDown["OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.chngDropDown_ " + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                            $scope.dyn_Cont += "<input ui-select2=\"chngDropDown.OptionValues" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",6 )\"  ng-model=\"fields.chngDropDown_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                        }
                    }
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 13) {
                    var fileID = 0;
                    $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = true;
                    $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = $scope.DAMattributesRelationList[i].DropDownPricing;
                    $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity'  data-attributeid=" + $scope.DAMattributesRelationList[i].AttributeID.toString() + "></div>";
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 7) {
                    $scope.fields["Tree_" + $scope.DAMattributesRelationList[i].AttributeID] = [];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    $scope.treesrcdirec["Attr_" + $scope.DAMattributesRelationList[i].AttributeID] = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                    if ($scope.treesrcdirec["Attr_" + $scope.DAMattributesRelationList[i].AttributeID].length > 0) {
                        treeTextVisbileflag = false;
                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.DAMattributesRelationList[i].AttributeID])) {
                            $scope.treePreviewObj["Attr_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                        } else $scope.treePreviewObj["Attr_" + $scope.DAMattributesRelationList[i].AttributeID] = false;
                    } else {
                        $scope.treePreviewObj["Attr_" + $scope.DAMattributesRelationList[i].AttributeID] = false;
                    }
                    $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.DAMattributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                    $scope.dyn_Cont += '    <label class="control-label">' + $scope.DAMattributesRelationList[i].AttributeCaption + '</label>';
                    $scope.dyn_Cont += '    <div class="controls treeNode-controls">';
                    $scope.dyn_Cont += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.DAMattributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.DAMattributesRelationList[i].AttributeID + '"></div>';
                    $scope.dyn_Cont += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.DAMattributesRelationList[i].AttributeID + '">';
                    $scope.dyn_Cont += '            <span ng-if="doing_async">...loading...</span>';
                    $scope.dyn_Cont += '            <abn-tree tree-filter="filterValue_' + $scope.DAMattributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.DAMattributesRelationList[i].AttributeID + '\" accessable="' + $scope.DAMattributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                    $scope.dyn_Cont += '        </div>';
                    $scope.dyn_Cont += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.DAMattributesRelationList[i].AttributeID + '\">';
                    $scope.dyn_Cont += '            <div class="controls">';
                    $scope.dyn_Cont += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.DAMattributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                    $scope.dyn_Cont += '        </div></div>';
                    $scope.dyn_Cont += '</div></div>';
                } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 11) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.DAMattributesRelationList[i].AttributeID] = true;
                    if ($scope.fields["uploader_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.fields["uploader_" + $scope.DAMattributesRelationList[i].AttributeID] != null) {
                        $scope.fields["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.fields["uploader_" + $scope.DAMattributesRelationList[i].AttributeID];
                    } else {
                        $scope.fields["uploader_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Caption;
                    }
                    $scope.setFieldKeys();
                    if ($scope.UploaderCaption["uploader_" + $scope.DAMattributesRelationList[i].AttributeID] != undefined && $scope.UploaderCaption["uploader_" + $scope.DAMattributesRelationList[i].AttributeID] != null) {
                        $scope.UploaderCaption["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.UploaderCaption["uploader_" + $scope.DAMattributesRelationList[i].AttributeID];
                    }
                    $scope.setUploaderCaption();
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.DAMattributesRelationList[i].AttributeID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.DAMattributesRelationList[i].Caption.toString() + '</label>';
                    $scope.dyn_Cont += '<div class=\"controls\">';
                    //$scope.dyn_Cont += '<img src="UploadedImages/' + $scope.fields["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] + '" alt="' + $scope.fields["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] + '"';
                    //$scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreviewae_' + $scope.DAMattributesRelationList[i].AttributeID + '">';
                    if ($scope.DAMattributesRelationList[i].Value == "" || $scope.DAMattributesRelationList[i].Value == null && $scope.DAMattributesRelationList[i].Value == undefined) {
                        $scope.DAMattributesRelationList[i].Value = "NoThumpnail.jpg";
                    }
                    $scope.dyn_Cont += '<img src="' + imageBaseUrlcloud + 'UploadedImages/' + $scope.fields["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] + '" alt="' + $scope.fields["chnguploader_" + $scope.DAMattributesRelationList[i].AttributeID] + '"';
                    $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreviewae_' + $scope.DAMattributesRelationList[i].AttributeID + '">';
                    if ($scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                        $scope.dyn_Cont += '</div></div>';
                    } else {
                        if ($scope.Islock_temp == false && $scope.LinkedAsset == false && $scope.Isassetlibrary == false) {
                            $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefile(" + $scope.DAMattributesRelationList[i].AttributeID + ")' attributeTypeID='" + $scope.DAMattributesRelationList[i].AttributeTypeID + "'";
                            $scope.dyn_Cont += 'entityid="' + $scope.AssetEditID + '" attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" id="Uploader_' + $scope.DAMattributesRelationList[i].AttributeID + '"';
                            $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.DAMattributesRelationList[i].Caption + '"';
                            $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.DAMattributesRelationList[i].AttributeID] + '\">' + $translate.instant('LanguageContents.Res_4729.Caption');
                            $scope.dyn_Cont += '</a></div></div>';
                        } else if ($scope.Islock_temp == true || $scope.LinkedAsset == true || $scope.Isassetlibrary == true) {
                            $scope.dyn_Cont += '</div></div>';
                        }
                    }
                }
            }
            $("#Assetdynamicdetail").empty();
            $("#Assetdynamicdetail").append($scope.dyn_Cont);
            $compile($("#Assetdynamicdetail").contents())($scope);
            $("#AssetCategoryInAsstEdit").modal('hide');
        }
        $scope.changeAssetType = function (assettypeID) {
            if (assettypeID != null && assettypeID != undefined && $scope.AssetTypechange == true) {
                $scope.AssetTypechange = true;
                var selAsset = $.grep($scope.DAMtypeswithExtAssetedit, function (e) {
                    return e.Id == assettypeID;
                })[0];
                $scope.assettypeidfrbtn = selAsset.id;
                if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                $.each($scope.DAMtypeswithExtAssetedit, function (i, el) {
                    $scope.tagAllOptions.data.push({
                        "id": el.Id,
                        "text": el.damCaption,
                        "ShortDescription": el.ShortDescription,
                        "ColorCode": el.ColorCode
                    });
                });
                $scope.selAssetType.splice(0, $scope.selAssetType.length);
                var tempTypeHolder = $.grep($scope.DAMtypeswithExtAssetedit, function (e) {
                    return e.Id == assettypeID
                });
                $scope.selAssetType.push({
                    "id": tempTypeHolder[0].Id,
                    "text": tempTypeHolder[0].damCaption,
                    "ShortDescription": tempTypeHolder[0].ShortDescription,
                    "ColorCode": tempTypeHolder[0].ColorCode
                });
                $scope.tempHolder = $scope.selAssetType[0];
                AssetEditService.GetAssetAttributueswithTypeID(assettypeID).then(function (result) {
                    if (result.Response != null) {
                        $scope.chngDropDown = {};
                        createDynHTmlforAssetType(result.Response);
                    }
                });
            }
        };
        $scope.newAttributeData = [];
        $scope.savechangedAssetData = function () {
            $scope.entityName = $scope.AssetAccess.AssetName;
            $scope.newAttributeData = [];
            var AssetAttribute = $scope.DAMattributesRelationList;
            for (var i = 0; i < AssetAttribute.length; i++) {
                if (AssetAttribute[i].AttributeTypeID == 3) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Owner) {
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                            "Level": 0
                        });
                    } else if ($scope.fields['chngListSingleSelection_' + AssetAttribute[i].AttributeID] != undefined && $scope.fields['chngListSingleSelection_' + AssetAttribute[i].AttributeID] != "") {
                        var value = $scope.fields['chngListSingleSelection_' + AssetAttribute[i].AttributeID];
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 1) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['chngTextSingleLine_' + AssetAttribute[i].AttributeID];
                    else {
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": ($scope.fields['chngTextSingleLine_' + AssetAttribute[i].AttributeID] != null) ? $scope.fields['chngTextSingleLine_' + AssetAttribute[i].AttributeID].toString() : "",
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 2) {
                    $scope.newAttributeData.push({
                        "AttributeID": AssetAttribute[i].AttributeID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": ($scope.fields['chngTextMultiLine_' + AssetAttribute[i].AttributeID] != null) ? $scope.fields['chngTextMultiLine_' + AssetAttribute[i].AttributeID].toString() : "",
                        "Level": 0
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 4) {
                    if ($scope.fields['chngListMultiSelection_' + AssetAttribute[i].AttributeID] != "" && $scope.fields['chngListMultiSelection_' + AssetAttribute[i].AttributeID] != undefined) {
                        if ($scope.fields['chngListMultiSelection_' + AssetAttribute[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['chngListMultiSelection_' + AssetAttribute[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.newAttributeData.push({
                                    "AttributeID": AssetAttribute[i].AttributeID,
                                    "AttributeCaption": AssetAttribute[i].Caption,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 8) {
                    $scope.newAttributeData.push({
                        "AttributeID": AssetAttribute[i].AttributeID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": ($scope.fields['chngTextSingleLine_' + AssetAttribute[i].AttributeID] != null) ? $scope.fields['chngTextSingleLine_' + AssetAttribute[i].AttributeID].toString() : "",
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                if (AssetAttribute[i].AttributeTypeID == 6) {
                    for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                        if ($scope.fields['chngDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['chngDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)] != undefined) {
                            $scope.newAttributeData.push({
                                "AttributeID": AssetAttribute[i].AttributeID,
                                "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": [$scope.fields['chngDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].id],
                                "Level": $scope.fields['chngDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].Level,
                                "Value": "-1"
                            });
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 5 && AssetAttribute[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date();
                    var MyDateString;
                    if (AssetAttribute[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        MyDateString = $scope.fields["chngDateTime_" + AssetAttribute[i].ID];
                    } else {
                        MyDateString = "";
                    }
                    if ($scope.fields["chngDateTime_" + AssetAttribute[i].ID] != undefined) {
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": MyDateString,
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 12) {
                    for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                        var levelCount = AssetAttribute[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.newAttributeData.push({
                                    "AttributeID": AssetAttribute[i].AttributeID,
                                    "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
                                    "Level": $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
                                    "Value": "-1"
                                });
                            }
                        } else {
                            if ($scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == (AssetAttribute[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.newAttributeData.push({
                                            "AttributeID": AssetAttribute[i].AttributeID,
                                            "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
                                            "Value": "-1"
                                        });
                                    }
                                } else {
                                    $scope.newAttributeData.push({
                                        "AttributeID": AssetAttribute[i].AttributeID,
                                        "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['chngMultiSelectDropDown_' + AssetAttribute[i].AttributeID + '_' + (j + 1)].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == AssetAttribute[i].AttributeID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 13) {
                    if (AssetAttribute[i].Levels != null) {
                        for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + AssetAttribute[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                        $scope.newAttributeData.push({
                                            "AttributeID": AssetAttribute[i].AttributeID,
                                            "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + AssetAttribute[i].AttributeID] != "" && $scope.fields['ListTagwords_' + AssetAttribute[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + AssetAttribute[i].AttributeID].length; j++) {
                            $scope.newAttributeData.push({
                                "AttributeID": AssetAttribute[i].AttributeID,
                                "AttributeCaption": AssetAttribute[i].AttributeCaption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + AssetAttribute[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 16) {
                    if ($scope.fields["chngSingleLineTextValue_" + AssetAttribute[i].AttributeID] != undefined) {
                        var dateValue = "";
                        if ($scope.fields["chngSingleLineTextValue_" + AssetAttribute[i].AttributeID] == '-') {
                            dateValue = "";
                        } else {
                            dateValue = $scope.fields["chngSingleLineTextValue_" + AssetAttribute[i].AttributeID];
                        }
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].AttributeCaption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": dateValue,
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 11) {
                    if ($scope.fields["chnguploader_" + AssetAttribute[i].AttributeID] != undefined) {
                        var imgValue = "";
                        if ($scope.fields["chnguploader_" + AssetAttribute[i].AttributeID] == '-') {
                            imgValue = "";
                        } else {
                            imgValue = $scope.fields["chnguploader_" + AssetAttribute[i].AttributeID];
                        }
                        $scope.newAttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].AttributeCaption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": imgValue,
                            "Level": 0
                        });
                    }
                }
            }
            $scope.StartEndDate = [];
            for (var m = 0; m < $scope.items.length; m++) {
                $scope.StartEndDate.push({
                    startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                    endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                    comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                    sortorder: 0
                });
            }
            $scope.SaveAsset = [];
            $scope.SaveAsset.push({
                "Typeid": $scope.tempHolder.id,
                "AttributeData": $scope.newAttributeData,
                "Name": $scope.entityName,
                "AssetID": $scope.OlAssetID,
                "oldAssetTypeId": $scope.oldAssetTypeID,
                "PeriodData": $scope.StartEndDate
            });
            AssetEditService.SaveAssetTypeofAsset($scope.SaveAsset).then(function (result) {
                if (result.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                } else {
                    $scope.$emit('loadAssetDetailSection', result.Response);
                    GetAssetAttribuuteDetails(result.Response);
                    var assetlist = [];
                    assetlist.push($scope.AssetEditID);
                    if ($scope.IsShowallAssets.Fromshowallassets == 1) {
                        $scope.$emit('loadShowallAssetDetailSection', assetlist, $scope.IsShowallAssets.DamViewName);
                    } else {
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $scope.AssetTypechange = false;
                }
            });
        }
        $scope.KeepexistingAssetData = function (assetid) {
            $scope.AssetTypechange = false;
            var d = assetid;
            $scope.Assetdyn_Cont = $scope.previousAssetdyn_Count;
            $scope.AssetTypeCaption = $scope.PreviousAssetTypeCaption;
            $scope.AssetTypeCategoryPath = $scope.PreviousAssetTypeCategPath;
            $scope.DAMtypeswithExtAssetedit = $scope.PreviousDAMtypesExtAssetedit;
            $scope.assetType = $scope.previousassetType;
            if ($scope.Assetdyn_Cont != '') {
                $("#Assetdynamicdetail").empty();
                $("#Assetdynamicdetail").append($scope.Assetdyn_Cont);
                $compile($("#Assetdynamicdetail").contents())($scope);
                AssetEditService.GetAssetCategoryTree($scope.assetType).then(function (data) {
                    if (data.Response != null) {
                        $scope.AssetCategory["AssetCategoryTree_"] = JSON.parse(data.Response);
                        $scope.PreviousAssetCategoryTree = $scope.AssetCategory["AssetCategoryTree_"];
                    }
                });
                timerObj.assettypeTimer = $timeout(function () {
                    $("#dynamicAssetTypeTreeInAsstEdit").html('');
                    var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_\" tree-data=\"AssetCategory.AssetCategoryTree_\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                    $("#dynamicAssetTypeTreeInAsstEdit").html($compile(htmltree)($scope));
                    $scope.PreviousAssetTypeTree = htmltree;
                }, 300);
            } else {
                GetAssetAttribuuteDetails(assetid);
            }
        };
        $scope.openSendAssetMail = function () {
            $scope.sendMetadatMailSubject = "";
            $scope.SendMailWithMdata.mailtags = [];
            $('#sendAssetMailPopup').modal("show");
            $scope.includemdata = false;
        }
        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked) $scope.includemdata = true;
            else $scope.includemdata = false;
        }
        $scope.SendAssetWitMwtadata = function () {
            if ($scope.AssetEditID != 0) {
                if ($('#sendMail').hasClass('disabled')) {
                    return;
                }
                $('#sendMail').addClass('disabled');
                $scope.MailprocessMailFlag = true;
                var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var assetSelection = [],
					toArray = new Array(),
					mailSubject = $scope.sendMetadatMailSubject != "" ? $scope.sendMetadatMailSubject : "Marcom Media Bank";
                for (var i = 0, mail; mail = $scope.SendMailWithMdata.mailtags[i++];) {
                    if (mail.text.match(emailvalid)) {
                        toArray.push(mail.text);
                    } else {
                        $('#sendMail').removeClass('disabled');
                        $scope.MailprocessMailFlag = false;
                        return false;
                    }
                }
                assetSelection.push({
                    "AssetId": $scope.AssetEditID
                });
                if (assetSelection.length > 0 && toArray.length > 0) {
                    var srcObj = {};
                    srcObj.Toaddress = toArray.join(",");
                    srcObj.subject = mailSubject;
                    srcObj.AssetArr = assetSelection;
                    srcObj.metadata = $scope.includemdata;
                    timerObj.sendmailmetadataTimer = $timeout(function () {
                        AssetEditService.SendMailWithMetaData(srcObj).then(function (result) {
                            if (result.Response == null) {
                                $('#sendMail').removeClass('disabled');
                                $scope.IsMailSender = false;
                                $scope.MailprocessMailFlag = false;
                                NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                            } else {
                                $('#sendMail').removeClass('disabled');
                                $scope.sendMetadatMailSubject = "";
                                $scope.MailprocessMailFlag = false;
                                $scope.SendMailWithMdata.mailtags = [];
                                $scope.IsMailSender = false;
                                NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                $('#sendAssetMailPopup').modal("hide");
                            }
                        });
                    }, 100);
                } else {
                    $scope.MailprocessMailFlag = false;
                    $('#sendMail').removeClass('disabled');
                    $scope.IsMailSender = false;
                    if (assetSelection.length == 0) bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
                    else if (toArray.length == 0) bootbox.alert($translate.instant('LanguageContents.Res_1138.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }

        function getuserassetdimensionalunitsettings() {
            var getdimensionID = '';
            AssetEditService.getdimensionunit().then(function (Getdimensionunitresult) {
                if (Getdimensionunitresult.StatusCode == 200) {
                    var option = '';
                    for (var i = 0; i < Getdimensionunitresult.Response.m_Item1.length; i++) {
                        option += '<option value="' + Getdimensionunitresult.Response.m_Item1[i].dimensionunitIds + '">' + Getdimensionunitresult.Response.m_Item1[i].dimensionunitName + ' </option>';
                    }
                    $("#dimensionunit1").html(option);
                    timerObj.getassetdimensionTimer = $timeout(function () {
                        getassetdimensionalunitsettings();
                    }, 50);
                }
            });
        }

        function getassetdimensionalunitsettings() {
            var DimensionunitSelectedID = '';
            AssetEditService.GetDimensionunitsettings().then(function (GetDimensionunitresult) {
                if (GetDimensionunitresult.StatusCode == 200) {
                    $scope.userDimensionUnit = GetDimensionunitresult.Response.DimensionUnit;
                    if ($scope.userDimensionUnit != undefined || $scope.userDimensionUnit > 0) $scope.dimensionunitlist = $scope.userDimensionUnit;
                    else $scope.dimensionunitlist = 1;
                }
            });
        }
        $scope.AssetPatharray = [];

        function GetEntityLocationPath(EntityIDVal) {
            AssetEditService.GetOwnerName(EntityIDVal).then(function (GetOwner) {
                var ownername = GetOwner.Response[0].ownername[0];
                AssetEditService.GetPath(EntityIDVal).then(function (getbc) {
                    $scope.AssetPatharray = [];
                    if (getbc != null && getbc != undefined) {
                        for (var i = 0; i < getbc.Response.length; i++) {
                            $scope.AssetPatharray.push({
                                "ID": getbc.Response[i].ID,
                                "Name": getbc.Response[i].Name,
                                "UniqueKey": getbc.Response[i].UniqueKey,
                                "ColorCode": getbc.Response[i].ColorCode,
                                "ShortDescription": getbc.Response[i].ShortDescription,
                                "TypeID": getbc.Response[i].TypeID,
                                "mystyle": "0088CC",
                                "EntityTypeID": getbc.Response[i].TypeID,
                                "ownerfirstname": ownername.FirstName,
                                "ownerlastname": ownername.LastName
                            });
                        }
                    }
                });
            });
        }
        $scope.DecodedTaskOwnerName = function (TaskName, value1, value2) {
            if (TaskName != null & TaskName != "") {
                var tooltiphtml = "";
                var lable2 = "Entity Owner";
                var lable1 = "Entity Name";
                tooltiphtml += '<div class="additionalInfo-Qtip">';
                tooltiphtml += '    <div class="th-AssetInfoTooltip">';
                tooltiphtml += '        <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + lable1 + '</span><span class="th-AssetInfoValue">' + $('<div />').html(TaskName).text() + '</span></div>';
                tooltiphtml += '        <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + lable2 + '</span><span class="th-AssetInfoValue">' + $('<div />').html(value1 + " " + value2).text() + '</span></div>';
                tooltiphtml += '    </div>';
                tooltiphtml += '</div>';
                return tooltiphtml;
            } else {
                return "";
            }
        };
        $scope.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.BreadCrumOverview = function (ID, TypeId) {
            if (TypeId == 5) {
                $scope.SearchAsset.Access = 0;
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/planningtool/costcentre/detail/section/' + ID + '/overview');
            } else if (TypeId == 10) {
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/objective/detail/section/' + ID + '/overview');
            } else {
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/planningtool/default/detail/section/' + ID + '/overview');
            }
            $modalInstance.dismiss('cancel');
        }
        $scope.MovePerformAssetOperation = function (actionId) {
            if (actionId == 1 || actionId == 2) {
                var remainRecord = [];
                remainRecord = $.grep($scope.damclipboard.assetselection, function (e) {
                    return e.AssetId == $scope.AssetPreviewObj.AssetId;
                });
                if (remainRecord.length == 0) {
                    $scope.damclipboard.assetselection.push({
                        "AssetId": $scope.AssetPreviewObj.AssetId
                    });
                    $scope.damclipboard.actioncode = actionId;
                }
                NotifySuccess($translate.instant('LanguageContents.Res_4099.Caption'));
            }
        }

        $scope.ChangePreviewProcess = function () {
            StrartUploadReplaceThumpnail();
            $timeout(function () {
                $("#bsmuipickfiles").click();
            }, 200)
        }


        $scope.ReformatProcess = function () {
            var folderid;
            var EntityID;
            if ($scope.Isassetlibrary == true) {
                folderid = $scope.assetFolderid;
                EntityID = $scope.ProductionEntityID;
            } else {
                if ($scope.Folderselect.ID != undefined) {
                    folderid = $scope.Folderselect.ID;
                    EntityID = ($scope.Folderselect.EntityID != undefined) ? $scope.Folderselect.EntityID : $scope.ProductionEntityID;
                } else {
                    folderid = $scope.assetFolderid;
                    EntityID = $scope.ProductionEntityID;
                }
            }
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/assetreformat.html',
                controller: "mui.muiassetreformatCtrl",
                resolve: {
                    params: function () {
                        return {
                            thubmnailGuid: $scope.ActiveVersion.Fileguid,
                            assetid: $scope.OlAssetID,
                            folderid: folderid,
                            EntityID: EntityID,
                            fromisassetlibrary: ($scope.IsShowInAssetlibrary == true) ? 1 : 0
                        };
                    }
                },
                scope: $scope,
                windowClass: 'reformatAssetPopup popup-widthXL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $timeout(function () { $scope.closeView(); }, 100);
            });
        }
        $scope.$on('ReloadAssetPreview', function (event, assetid) {
            $scope.fileguid = "assets/img/loading.gif";
        });
        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        $scope.isModelUpdated = function (attrID, levelcnt, attributeLevel, attrType, flag) {
            var currntlevel = attributeLevel + 1;
            var dropdown_text = 'dropdown_text_' + attrID + '_' + currntlevel, modelVal = {};
            for (var j = currntlevel; j <= levelcnt; j++) {
                $scope['dropdown_' + attrID + '_' + currntlevel] = 0;
                modelVal = ($.grep($scope["ddtoption_" + attrID + "_" + currntlevel].data, function (e) {
                    return e.Caption.toString() == $scope.treeTexts[dropdown_text].toString().trim()
                }))[0];
                if (modelVal != undefined) {
                    $scope['dropdown_' + attrID + '_' + currntlevel] = modelVal.id;
                }

            }
        }
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                var updateentityattrib = {};
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                updateentityattrib.EntityID = parseInt($scope.AssetEditID, 10);
                updateentityattrib.AttributeID = attrID;
                AssetEditService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var GetTreeRes;
                        if (choosefromParent) AssetEditService.GetDropDownTreePricingObjectFromParentDetail(attrID, choosefromParent, choosefromParent, parseInt($scope.AssetEditID, 10), 0).then(function (GetTree) {
                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                        return e.level == p;
                                    }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                            });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            } else selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }
                            }
                        });
                        else AssetEditService.GetDropDownTreePricingObject(attrID, choosefromParent, choosefromParent, parseInt($scope.AssetEditID, 10), 0).then(function (GetTree) {
                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                        return e.level == p;
                                    }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                            });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            } else selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }
        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var ID = 0;
            if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
            else ID = $scope.AssetEditID;
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }
                var tempperioddates = [];
                for (var i = 0; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }
                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }
                newperiod.EntityID = entityid;
                newperiod.StartDate = ConvertDateToString($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = ConvertDateToString($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;
                newperiod.attributeid = attrID;
                AssetEditService.InsertAssetPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else var newid = resultperiod.Response;
                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }
                    perioddates.push({
                        ID: newid,
                        value: edate
                    });
                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate))
                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString()), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString()), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    } else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("visibility", "hidden");
                    $scope.dyn_Cont += '<div class="period" data-dynPeriodID="' + newid + '">';
                    $scope.dyn_Cont += '<div class="inputHolder span11">';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5056.Caption') + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a class="no-minwidth"  isdam="true" xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  title=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}} to {{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5057.Caption') + '</label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + ID + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  title=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';
                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }
                    var divcompile = $compile($scope.dyn_Cont)($scope);
                    $("#Assetdynamicdetail div[data-addperiodid]").append(divcompile);
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    var assetlist = [];
                    assetlist.push($scope.AssetEditID);
                    $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                });
            } else {
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {
                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[" + $translate.instant('LanguageContents.Res_5065.Caption') + "]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });
                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = new Date.create(ConvertDateFromStringToStringByFormat($scope.fields['PeriodEndDate_Dir_' + periodid]));
                }
                var diffval = ((new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime())));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }
                var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
                var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
                if (parseInt(diffvalend) < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }
                var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
                var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));
                    return false;
                }
                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = sdate;
                updateperiod.EndDate = edate;
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                updateperiod.attributeid = attrID;
                AssetEditService.AssetPeriod(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        $scope.fields["PeriodStartDate_" + periodid] = ConvertDateFromStringToString(sdate);
                        $scope.fields["PeriodEndDate_" + periodid] = ConvertDateFromStringToString(edate);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
            }
        }
        $scope.AddDefaultEndDatefrEdit = function (stratdateval, enddateval) {
            var objsetenddate = new Date.create($scope.fields[stratdateval]);
            $scope.fields[enddateval] = objsetenddate.addDays(7);
        };
        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex) {
            if (currentindex != undefined) {
                var enddate1 = null;
                if (currentindex != 0) {
                    enddate1 = $scope.items[currentindex - 1].endDate;
                }
                if (enddate1 != null && enddate1 >= startdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                    $scope.items[currentindex].startDate = null;
                    $scope.items[currentindex].endDate = null;
                } else {
                    $("#Assetdynamicdetail").addClass('notvalidate');
                    if (startdate == null) {
                        $scope.items[currentindex].endDate = null;
                    } else {
                        $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                    }
                }
            } else {
                if (enddate.toUpperCase().contains("STARTDATE")) {
                    var temp = startdate;
                    startdate = enddate;
                    enddate = temp;
                }
                var objsetenddate = new Date.create($scope.fields[startdate]);
                $scope.fields[enddate] = objsetenddate.addDays(7);
            }
        };
        $scope.deletePeriodDate = function (periodid) {
            AssetEditService.DeleteAssetPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    $("#Assetdynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');
                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: [],
                    endDate: [],
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.lastSubmit = [];
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.treeNodeSelectedHolder = [];

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                updateentityattrib.AssetID = parseInt($scope.AssetEditID, 10);
                updateentityattrib.AttributeID = attrID;
                AssetEditService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var assetlist = [];
                        assetlist.push($scope.AssetEditID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        AssetEditService.GetAttributeTreeNode(attrID, $scope.AssetEditID, false).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            timerObj.latestfeedTimer = $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };
        $scope.DownloadWithMetadata = function (result) {
            var arr = [];
            arr[0] = $scope.AssetEditID;
            var include = result;
            if (include == 0) $scope.ProgressMsgHeader = "Downloading Metadata";
            if (include == 1) $scope.ProgressMsgHeader = "Downloading Package";
            blockUIForDownload();
            AssetEditService.DownloadAssetWithMetadata(arr, result).then(function (result) {
                if (result.Response == null) {
                    NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                } else {
                    if (include == false) {
                        var fileid = result.Response[1],
							extn = '.xml';
                        if (result.Response[0] == null || result.Response[0] == "") var filename = "MediabankAssets" + extn;
                        else var filename = result.Response[0];
                    } else {
                        var fileid = result.Response[1],
							extn = '.zip';
                        var filename = fileid + extn;
                    }
                    if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                        location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                    } else {
                        location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                    }
                    if (include == true) NotifySuccess($translate.instant('LanguageContents.Res_5805.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_5806.Caption'));
                }
            });
        }
        $scope.CreateAssetTask = function () {
            timerObj.assettask = $timeout(function () {
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == $scope.ActiveVersion.Fileguid
                })[0];
                if (flagattTask == null) {
                    $scope.DamAssetTaskselectionFiles.AssetFiles.push({
                        "AssetUniqueID": $scope.OlAssetID,
                        "ProcessType": $scope.ActiveVersion.ProcessType,
                        "FileGuid": $scope.ActiveVersion.Fileguid,
                        "Extension": $scope.ActiveVersion.Extension.toLowerCase(),
                        "Status": $scope.ActiveVersion.Status,
                        "ActiveFileID": $scope.AssetActiveFileID,
                        "Size": $scope.orgSize,
                        "AssetName": $scope.AssetAccess.AssetName,
                        "AssetTypeid": $scope.oldAssetTypeID,
                        "EntityID": $scope.assetEntityId,
                        "Category": $scope.Category
                    });
                    if (data[0] != undefined) {
                        $scope.DamAssetTaskselectionFiles.AssetDynamicData.push({
                            "ID": $scope.OlAssetID
                        });
                    }
                }
                OpenWorkTask(2);
            }, 100);
        }

        function OpenWorkTask(tasktype) {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 0) {
                if (tasktype == 32 || tasktype == 36) {
                    if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                        return false;
                    }
                }
                var assetid = 0;
                if (tasktype == 32 || tasktype == 36) assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                //$scope.$emit('redirectMUICreateTask', "Assetedit", tasktype, 0, $scope.OlAssetID);

                /****** Open taskcreation popup ******/

                var opentaskpopupdata = {};
                opentaskpopupdata.AssetID = $scope.OlAssetID;
                opentaskpopupdata.IsTask = true;
                opentaskpopupdata.IsLock = false;
                opentaskpopupdata.TaskType = tasktype;
                opentaskpopupdata.Fromplace = "Task";
                opentaskpopupdata.IsAssetedit = true;
                opentaskpopupdata.Type = "addtaskfromasset";
                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskCreation.html',
                    controller: "mui.task.muitaskTaskCreationCtrl",
                    resolve: {
                        params: function () {
                            return {
                                data: opentaskpopupdata
                            };
                        }
                    },
                    scope: $scope,
                    windowClass: 'addTaskPopup popup-widthL',
                    backdrop: "static"
                });
                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () { });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4608.Caption'));
        }
        $scope.Addtolightbox = function () {
            $scope.damlightbox.lightboxselection.push({
                "AssetId": $scope.ActiveVersion.AssetID,
                "Guid": $scope.ActiveVersion.Fileguid,
                "Ext": $scope.ActiveVersion.Extension,
                "Description": $scope.ActiveVersion.Description
            });
            timerObj.assetselectionTimer = $timeout(function () {
                $scope.AssetSelectionFiles = [];
            }, 200);
            NotifySuccess($translate.instant('LanguageContents.Res_4066.Caption'));
        }
        $scope.PublishAssets = function (result) {
            if (result == true) {
                if ($scope.AssetRolesCaption.length > 1) {
                    var arr = [];
                    arr[0] = $scope.ActiveVersion.AssetID;
                    AssetEditService.PublishAssets(arr).then(function (result) {
                        if (result.Response == true) {
                            NotifySuccess($translate.instant('LanguageContents.Res_5807.Caption'));
                            $scope.$broadcast('UpdatePublishIcon', $scope.ActiveVersion.AssetID);
                            $scope.ShowpublishIcon = true;
                            $scope.AssetPublish = true;
                            $scope.UnPublish = true;
                            $scope.IsPublish = false;
                            if ($scope.IsShowallAssets.Fromshowallassets == 1) {
                                var assetlist = [];
                                assetlist.push($scope.AssetEditID);
                                $scope.$emit('loadShowallAssetDetailSection', assetlist, $scope.IsShowallAssets.DamViewName);
                            }
                            if (damviewtype == 'Thumbnail') {
                                $scope.$emit('loadAssetDetailSectionfrThumbnail', arr);
                            } else if (damviewtype == 'Summary') {
                                $scope.$emit('loadAssetDetailSectionfrSummary', arr);
                            } else if (damviewtype == 'List') {
                                $scope.$emit('loadAssetDetailSectionfrList', arr);
                            }
                            $("#Publishicon_" + $scope.ActiveVersion.AssetID + "").removeClass("displayNone");
                            GetAssetAttribuuteDetails($scope.ActiveVersion.AssetID);
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                        }
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_5752.Caption'));
                    return false;
                }
            } else {
                var arr = [];
                arr[0] = $scope.ActiveVersion.AssetID;
                AssetEditService.UnPublishAssets(arr).then(function (result) {
                    if (result.Response == true) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5753.Caption'));
                        $scope.ShowpublishIcon = false;
                        $scope.UnPublish = false;
                        $scope.IsPublish = true;
                        $("#Publishicon_" + $scope.ActiveVersion.AssetID + "").addClass("displayNone");
                        $scope.AssetPublish = false;
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                    }
                });
            }
        }
        $scope.DuplicateAssets = function () {
            $scope.AssetSelectedId = [];
            $scope.AssetSelectedId.push($scope.AssetEditID);
            if ($('#Linkicon_' + $scope.AssetEditID)[0].className == "icon-link") {
                bootbox.alert($translate.instant('LanguageContents.Res_5754.Caption'));
            } else {
                var arr = $scope.AssetSelectedId;
                AssetEditService.DuplicateAssetFiles(arr).then(function (result) {
                    var count = $scope.AssetSelectedId.length;
                    if (result.Response == 0) {
                        $('#loadingDuplicateAssetsPageModel').modal("hide");
                        NotifyError($translate.instant('LanguageContents.Res_4324.Caption'));
                    } else {
                        $('#loadingDuplicateAssetsPageModel').modal("hide");
                        NotifySuccess(count + " " + "Duplicate Asset(s) created successfully");
                        $scope.$emit('LoadMuiAttachment', $scope.assetEntityId);
                    }
                });
            }
        }
        $scope.AssetTypeTreePopUp = function (fileid) {
            $("#AssetCategoryInAsstEdit").modal('show');
        };
        $scope.AssetTypeCategorySelection = function (branch, parentArr) {
            var i = 0;
            if (branch.ischecked == true) {
                $scope.cntChecks += 1;
                AssetEditService.GetAssetCategoryPathInAssetEdit(branch.id).then(function (data) {
                    if (data.Response != null) {
                        AssetEditService.GetAllExtensionTypesforDAM().then(function (assetdata) {
                            if (assetdata.Response != null) {
                                var temp = $.grep(assetdata.Response.m_Item1, function (e) {
                                    return e.Id == branch.id;
                                })[0].damCaption;
                                $scope.AssetTypeCategoryPath = data.Response[0].CategoryPath;
                                $scope.AssetTypeCaption = temp;
                                $scope.DAMtypeswithExtAssetedit = $.grep(assetdata.Response.m_Item1, function (e) {
                                    return $.inArray(e.Id, data.Response[1].AssetTypeIDs) != -1;
                                })
                                if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                                $.each($scope.DAMtypeswithExtAssetedit, function (i, el) {
                                    $scope.tagAllOptions.data.push({
                                        "id": el.Id,
                                        "text": el.damCaption,
                                        "ShortDescription": el.ShortDescription,
                                        "ColorCode": el.ColorCode
                                    });
                                });
                                $scope.selAssetType.splice(0, $scope.selAssetType.length);
                                var tempTypeHolder = $.grep($scope.DAMtypeswithExtAssetedit, function (e) {
                                    return e.Id == branch.id
                                });
                                $scope.selAssetType.push({
                                    "id": tempTypeHolder[0].Id,
                                    "text": tempTypeHolder[0].damCaption,
                                    "ShortDescription": tempTypeHolder[0].ShortDescription,
                                    "ColorCode": tempTypeHolder[0].ColorCode
                                });
                                $scope.tempHolder = $scope.selAssetType[0];
                            }
                            $scope.assetType = branch.id;
                            $scope.changeAssetType($scope.assetType);
                        });
                    }
                });
            } else {
                $scope.cntChecks -= 1;
            }
        }
        $scope.CloseAssetCategoryTree = function () {
            if ($scope.cntChecks == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_5755.Caption'));
            } else {
                $("#AssetCategoryInAsstEdit").modal('hide');
            }
        };
        $scope.UploadAttributeId = 0;
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAssetEdit").click();
        }
        $timeout(function () {
            StrartUpload_UploaderAttrAsset();
        }, 100)

        function StrartUpload_UploaderAttrAsset() {

            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $('.moxie-shim').remove();
                var uploader_AttrAsset = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAssetEdit',
                    container: 'filescontainerassetEdit',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_AttrAsset.bind('Init', function (up, params) {
                    uploader_AttrAsset.splice();
                });
                uploader_AttrAsset.init();
                uploader_AttrAsset.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_AttrAsset.start();
                });
                uploader_AttrAsset.bind('UploadProgress', function (up, file) { });
                uploader_AttrAsset.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_AttrAsset.bind('Destroy', function (up) {
                    console.log("Uploader uploader_AttrAsset destroyed");
                });
                uploader_AttrAsset.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveUploaderFileDetails(file, response.response);
                });
                uploader_AttrAsset.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_AttrAsset.settings.multipart_params.key = uniqueKey;
                    uploader_AttrAsset.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                var uploader_AttrAsset = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAssetEdit',
                    container: 'filescontainerassetEdit',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_AttrAsset.bind('Init', function (up, params) { });

                uploader_AttrAsset.init();
                uploader_AttrAsset.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_AttrAsset.start();
                });
                uploader_AttrAsset.bind('UploadProgress', function (up, file) { });
                uploader_AttrAsset.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_AttrAsset.bind('Destroy', function (up) {
                    console.log("Uploader uploader_AttrAsset destroyed");
                });
                uploader_AttrAsset.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveUploaderFileDetails(file, response.response);
                });
                uploader_AttrAsset.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

        }

        function SaveUploaderFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.now();
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.EntityID = parseInt($scope.AssetEditID);
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreviewae_" + $scope.UploadAttributeId;
            AssetEditService.copyuploadedImage(uplaodImageObject.FileName).then(function (ImgRes) {
                if (ImgRes.Response != null) {
                    uplaodImageObject.FileName = ImgRes.Response;
                    AssetEditService.UpdateAssetImageName(uplaodImageObject).then(function (uplaodImageObjectResult) {
                        if (uplaodImageObjectResult.Response != 0) {
                            $scope.fields["chnguploader_" + $scope.UploadAttributeId] = uplaodImageObject.FileName;
                            NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                            $('#' + PreviewID).attr('src', imageBaseUrlcloud + 'UploadedImages/' + uplaodImageObject.FileName);
                        }
                    });
                }
            })
        }
        $scope.ReassignMembersData = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: false,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function clearScopes() {
            $("#Assetdynamicdetail").empty();
            $scope.treelevels = {};
            $scope.treeTexts = {};
            $scope.fieldoptions = [];
            $scope.fields = {
                usersID: ''
            };
            $scope.imageunitFormat = '';
            $scope.convertWidth = 0;
            $scope.convertHeight = 0;
            $scope.ActiveVersionSize = 0;
            $scope.ActiveVersionDpi = 0;
            $scope.imageunitshortFormat = 'px';
            $scope.assetType = '';
            $scope.Assetdyn_Cont = '';
            $scope.AssetAccess.AssetName = '-';
            $scope.DAMtypeswithExtAssetedit = '';
            $scope.AssetTypeCategoryPath = "-";
            $scope.AssetTypeCaption = "-";
            $scope.ActiveVersion = [];
            $scope.AssetlastUpdatedOn = '';
            $scope.AssetPreviewObj = {
                AssetId: 0,
                AssetCeatedOn: "",
                AssetUpdatedOn: "",
                PublisherID: 0,
                PublishedOn: "",
                VersionCeatedOn: ""
            };
        };
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };

        var uploader_ReplaceThump = "";

        function StrartUploadReplaceThumpnail() {
            if (uploader_ReplaceThump != '') {
                uploader_ReplaceThump.destroy();
            }
            $('.moxie-shim').remove();
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploader_ReplaceThump = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'bsmuipickfiles',
                    container: 'bsmuipickfiles_replace',
                    max_file_size: '10mb',
                    flash_swf_url: 'js/plupload.flash.swf',
                    silverlight_xap_url: 'js/plupload.silverlight.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }],
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_ReplaceThump.bind('Init', function (up, params) { });
                uploader_ReplaceThump.init();
                uploader_ReplaceThump.bind('FilesAdded', function (up, files) {
                    //$.each(files, function (i, file) { });
                    up.refresh();
                    uploader_ReplaceThump.start();
                    //$('#bsmuipickfiles').hide();
                    $('#fileuploadprogress').show();
                    $('#fileuploadprogress .bar').css('width', '0');
                    $('#fileuploadprogress #fileuploadbarText').html('0 %');
                });
                uploader_ReplaceThump.bind('fileuploadprogress', function (up, file) {
                    $('#fileuploadprogress .bar').css("width", file.percent + "%");
                    $('#fileuploadprogress #fileuploadbarText').html(file.percent + "%");
                });
                uploader_ReplaceThump.bind('Error', function (up, err) {

                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    up.refresh();
                });
                uploader_ReplaceThump.bind('FileUploaded', function (up, file, response) {
                    var img = new Image();
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    var physicalpath = TenantFilePath + 'DAMFiles/Cropped/Temp/' + file.id + fileext;
                    img.src = amazonURL + cloudsetup.BucketName + "/" + physicalpath;
                    //$('#bsmuipickfiles').show();
                    $('#fileuploadprogress').hide();
                    $('#muifileuploadImage').modal('show');
                    $('#muifileuploadImage #fileuploadCropReportImg').attr('style', '');
                    $('#muifileuploadImage #fileuploadCropReportImg').removeAttr('src').css('visibility', 'hidden');
                    $('#muifileuploadImage #fileuploadCropReportImg').attr('src', img.src);
                    $("<img/>").attr("src", imageBaseUrlcloud + 'DAMFiles/Cropped/Temp/' + file.id + fileext).load(function () {
                        var w = this.width;
                        var h = this.height;
                        var path = file.id + fileext;
                        $scope.tempResponse = response.response;
                        $('#muifileuploadImage #fileuploadCropReportImg').attr('data-width', w).attr('data-height', h);
                        $('#muifileuploadImage #fileuploadCropReportImg').css('visibility', 'visible');
                        $timeout(function () {
                            AssigneNewImage(w, h, path);
                        }, 200);
                    });
                });
                uploader_ReplaceThump.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Cropped/Temp/" + file.id + fileext);
                    uploader_ReplaceThump.settings.multipart_params.key = uniqueKey;
                    uploader_ReplaceThump.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                uploader_ReplaceThump = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'bsmuipickfiles',
                    container: 'bsmuipickfiles_replace',
                    max_file_size: '10mb',
                    flash_swf_url: 'js/plupload.flash.swf',
                    silverlight_xap_url: 'js/plupload.silverlight.xap',
                    url: 'Handlers/AssetCropHandler.ashx?Type=Attachment',
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]
                });
                uploader_ReplaceThump.bind('Init', function (up, params) { });
                uploader_ReplaceThump.init();
                uploader_ReplaceThump.bind('FilesAdded', function (up, files) {
                    $.each(files, function (i, file) { });
                    up.refresh();
                    uploader_ReplaceThump.start();
                    //$('#bsmuipickfiles').hide();
                    $('#fileuploadprogress').show();
                    $('#fileuploadprogress .bar').css('width', '0');
                    $('#fileuploadprogress #fileuploadbarText').html('0 %');
                });
                uploader_ReplaceThump.bind('fileuploadprogress', function (up, file) {
                    $('#fileuploadprogress .bar').css("width", file.percent + "%");
                    $('#fileuploadprogress #fileuploadbarText').html(file.percent + "%");
                });
                uploader_ReplaceThump.bind('Error', function (up, err) {
                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    up.refresh();
                });
                uploader_ReplaceThump.bind('FileUploaded', function (up, file, response) {
                    $scope.tempFile = file;
                    $scope.tempResponse = response.response;
                    var img = new Image();
                    img.src = TenantFilePath + 'DAMFiles/Cropped/Temp/' + response.response;
                    //$('#bsmuipickfiles').show();
                    $('#fileuploadprogress').hide();
                    $('#muifileuploadImage').modal('show');
                    $('#muifileuploadImage #fileuploadCropReportImg').removeAttr('src').css('visibility', 'hidden');
                    $('#muifileuploadImage #fileuploadCropReportImg').attr('style', '');
                    var w = parseInt(response.response.split(',')[1]);
                    var h = parseInt(response.response.split(',')[2]);
                    var path = response.response.split(',')[0];
                    timerObj.assignNewImage = $timeout(function () {
                        AssigneNewImage(w, h, path);
                    }, 200);
                });
            }

        }

        function AssigneNewImage(w, h, path) {
            $('#muifileuploadImage #fileuploadCropReportImg').attr('src', imageBaseUrlcloud + 'DAMFiles/Cropped/Temp/' + path);
            $('#muifileuploadImage #fileuploadCropReportImg').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#muifileuploadImage #fileuploadCropReportImg').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#muifileuploadImage #fileuploadCropReportImg').parent().css('margin-top', margine + 'px');
            } else {
                $('#muifileuploadImage #fileuploadCropReportImg').css('height', '528px');
                $('#muifileuploadImage #fileuploadCropReportImg').parent().css('margin-top', '0px');
            }
            $('#muifileuploadImage #fileuploadCropReportImg').css('visibility', 'visible');
            initJcrop();
        }

        function initJcrop() {
            if ($('#muifileuploadImage .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#fileuploadCropReportImg').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: 4 / 5
                });
                jcrop_api.focus();
                var w = $('#muifileuploadImage #fileuploadCropReportImg').width();
                var h = $('#muifileuploadImage #fileuploadCropReportImg').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                var nw = 0;
                var nh = 0;
                if (w <= h) {
                    nw = w;
                    nh = Math.round((w / 4) * 5);
                } else {
                    nh = h;
                    nw = Math.round((h / 4) * 5);
                }
                jcrop_api.animateTo([0, 0, nw, nh]);
            });

            function showPreview(c) {
                var orgw = $('#muifileuploadImage #fileuploadCropReportImg').attr('data-width');
                var orgh = $('#muifileuploadImage #fileuploadCropReportImg').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };

        $scope.UploadReportImage = function () {
            var saveobj = {};
            saveobj.imgsourcepath = $("#fileuploadCropReportImg").attr('src');
            saveobj.imgwidth = $scope.userimgWidth;
            saveobj.imgheight = $scope.userimgHeight;
            saveobj.imgX = $scope.userimgX;
            saveobj.imgY = $scope.userimgY;
            saveobj.Preview = $scope.tempResponse.split(',')[0];
            saveobj.Fileguid = $scope.MediaBankSettings.EditableAssetGuid;
            saveobj.AssetExt = $scope.MediaBankSettings.EditableAssetExt;
            saveobj.AssetId = $scope.MediaBankSettings.AssetID;
            AssetEditService.UpdateAssetThumpnail(saveobj).then(function (result) {
                if (result.Response == null) {
                    NotifyError($translate.instant('LanguageContents.Res_1145.Caption'));
                } else {
                    $scope.OrgimgWidth = 0;
                    $scope.OrgimgHeight = 0;
                    //$scope.$emit('LoadMuiAttachment', $scope.MediaBankSettings.EntityId);
                    $scope.$emit('ChangeAssetPreview', saveobj.AssetId);
                    if ($scope.DownloadFileObj != undefined && $scope.DownloadFileObj != null)
                        if ($scope.DownloadFileObj.DownloadFilesArr.length > 0)
                            $scope.$emit('refreshDamDownloadScope', $scope.DownloadFileObj.DownloadFilesArr);
                    // GetAssetAttribuuteDetails(saveobj.AssetId);
                    timerObj.previewTimer = $timeout(function () {
                        CheckPreviewGeneratorForVersion();
                        var assetlist = [];
                        assetlist.push(saveobj.AssetId);
                        $scope.$emit('loadAssetDetailSection', assetlist);
                    }, 500);

                }
                if (params.isManagefromDam) {
                    $timeout(function () {
                        $modalInstance.dismiss('cancel');
                    }, 100);
                }
            });
        }

        $scope.Viewlightbox = function () {
            //if ($('#icon_selectassets').attr('class') != 'icon-check') {
            //    if ($scope.selectall == false) {
            //        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
            //        selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
            //    } else {
            //        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
            //    }
            //}
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damlightbox.html',
                controller: "mui.DAM.lightboxCtrl",
                resolve: {
                    params: function () {
                        return {
                            isfromDAM: true
                        };
                    }
                },
                scope: $scope,
                windowClass: 'in LightBoxPreviewPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            //refreshDownLoadScope(selectedAssetIDs);
            //refreshDeleteableScope(selectedAssetIDs);
        }

        $scope.deleteAsset = function (assetid) {

            $scope.AssetFilesObj.AssetSelection = [];
            $scope.AssetFilesObj.selectedAssetId = [];
            $scope.AssetFilesObj.AssetSelection.push(assetid);
            $scope.AssetFilesObj.selectedAssetId.push(assetid);
            var action = "Delete";
            $scope.$emit('DamAssetAction', action);
        }

        $scope.$on('assetManageAcions', function (event, action) {
            switch (action) {
                case "ManageVersion":
                    $scope.InitializeFileUpload();
                    break;
                case "ReplaceThumpnail":
                    $scope.ChangePreviewProcess();
                    break;
            }
        });
        $scope.$on('closeasseteditpopup', function () {
            $modalInstance.dismiss('cancel');
        });

        $scope.closeManageVersion = function () {
            if (params.isManagefromDam) {
                $timeout(function () {
                    $modalInstance.dismiss('cancel');
                }, 100)
            }
            $("#AssetVersion").modal('hide');

        }

        $scope.$on("$destroy", function () {
            $(window).off("resize.Viewport");
            Cropdefault();
            $timeout.cancel(timerObj);
        });
    }
    app.controller("mui.DAM.asseteditCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', 'AssetEditService', '$filter', '$stateParams', '$rootScope', '$compile', '$sce', '$translate', '$modalInstance', 'params', '$modal', muiDAMasseteditCtrl]);
    app.directive('xeditablelink', ['$timeout', '$compile', '$resource', '$window', function ($timeout, $compile, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var dttype = attrs.editabletypeid;
                var isDataTypeExists = false;
                var access = true;
                access = attrs.isreadonly != undefined ? (attrs.isreadonly == "true" ? true : false) : true;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                inlinePopup += '<div class="ngcompile">';
                inlinePopup += '<form class="form-horizontal">';
                inlinePopup += '<div class="control-group linkURL">';
                inlinePopup += '<div class="controls">';
                inlinePopup += '<input type="text" id="SingleTextXeditalblelink_' + attributeID + '"  ng-model="SingleTextXeditalblelink_' + attributeID + '" placeholder="Link Name" class="linkName" />';
                inlinePopup += '<select class="linkType" ui-select2 ng-model="linktype_' + attrs.attributeid + '"  id="linktype_' + attrs.attributeid + '" name="LinkType" data-placeholder="Select link Type">';
                inlinePopup += '<option ng-repeat="item in DefaultSettings.LinkTypes" value="{{item.Id}}">{{item.Type}}</option>';
                inlinePopup += '</select>';
                inlinePopup += '<br/>';
                inlinePopup += '<input class="URLString"  id="SingleTextXeditalblelinkUrl_' + attributeID + '"  ng-model="SingleTextXeditalblelinkUrl_' + attributeID + '" type="text"   placeholder="URL" />';
                inlinePopup += '</div>';
                inlinePopup += '</div>';
                inlinePopup += '</form>';
                inlinePopup += '</div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope['SingleTextXeditalblelink_' + attributeID] = scope.fields["SingleLineLinkName_" + attributeID];
                            scope['SingleTextXeditalblelinkUrl_' + attributeID] = scope.fields["SingleLineLinkUrl_" + attributeID];
                            scope['linktype_' + attributeID] = scope.fields["SingleLineLinkType_" + attributeID];
                            $timeout(function () {
                                $('#SingleTextXeditalblelink_' + attributeID).focus().select()
                            }, 10);
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.SaveDetailBlockForLink(attrs.entityid, attrs.attributeid, attrs.attributetypeid, scope['SingleTextXeditalblelinkUrl_' + attributeID], scope['SingleTextXeditalblelink_' + attributeID], scope['linktype_' + attributeID]);
                });
            }
        };
    }]);
    app.directive('fallbackSrc', function () {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this).attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    });
})(angular, app);