﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class DAMTreeValueDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _attributeid;
        protected int _Nodeid;
        protected int _level;
        protected string _value;
        protected int _parennode;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public DAMTreeValueDao() { }

        public DAMTreeValueDao(int pId, int pEntityid, int pAttributeid, int pNodeid, int pLevel, string pValue, int pParentnode)
        {
            this._id = pId;
            this._entityid = pEntityid;
            this._attributeid = pAttributeid;
            this._Nodeid = pNodeid;
            this._level = pLevel;
            this._value = pValue;
            this._parennode = pParentnode;
        }

        public DAMTreeValueDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Nodeid
        {
            get { return _Nodeid; }
            set { _Nodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }
        public virtual string Value
        {
            get { return _value; }
            set { _bIsChanged |= (_value != value); _value = value; }

        }

        public virtual int ParentNode
        {
            get { return _parennode; }
            set { _bIsChanged |= (_parennode != value); _parennode = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string DAMTreeValueDao = "DAMTreeValueDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
            public const string Value = "Value";
            public const string ParentNode = "ParentNode";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string DAMTreeValueDao = "DAMTreeValueDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
            public const string Value = "Value";
            public const string ParentNode = "ParentNode";
        }

        #endregion


    }
}
