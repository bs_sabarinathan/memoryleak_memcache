﻿using BrandSystems.Marcom.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access
{
    class UserAccessModule
    {
        public enum Operations
        {
            // Read = 1,
            View = 1,
            Edit = 2,
            Delete = 4,
            Create = 8,
            Self = 16,
            Simulate = 32,
            Allow = 64
        }
        public enum Feature
        {

            GanttView = 1,
            ListView = 2,
            Report = 3,
            CalenderView = 4,
            Financials = 5,
            Workflow = 6,
            Attachments = 7,
            Presentation = 8,
            Member = 9,
            Plan = 10,
            CostCenter = 11,
            Objective = 12,
            Dashboard = 13,
            Mypage = 14,
            GeneralSettings = 15,
            MetadataSettings = 16,
            ExternalLink = 17,
            Duplicate = 18,
            ApproveBudget = 19,
            Overview = 20,
            Task = 21,
            ViewEditAll = 22,
            Support = 23,
            Requestplannedamount = 24,
            Notification = 25,
            FundRequest = 26,
            SimulateUser = 27,
            DynamicEntity = 28,
            SSOExternalUser = 29,
            ViewAll = 31,
            CalenderEdit = 37

        }
        public enum Module
        {
            Common = 1,
            Admin = 2,
            Planning = 3,
            UserAccess = 4
        }

        
    }
}
