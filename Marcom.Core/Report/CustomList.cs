﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Report.Interface;

namespace BrandSystems.Marcom.Core.Report
{
    internal class CustomList : ICustomList, ICloneable
    {

        #region "Member variables"

        protected int _id;
        protected string _name;
        protected string _description;
        protected string _xmldata;

        #endregion

        #region "Constructors"

        public CustomList()
        { 
        }

        public CustomList(int pid, string pname, string pdescription, string pxmldata)
        {
            this._id = pid;
            this._name = pname;
            this._description = pdescription;
            this._xmldata = pxmldata;
        }

        #endregion

        #region "Public Properties"

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string XmlData
        {
            get
            {
                return _xmldata;
            }
            set
            {
                _xmldata = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        #endregion

        #region "IClonable Methods"

        public virtual object Clone()
        {
           return this.MemberwiseClone();
        }

        #endregion


    }
}
