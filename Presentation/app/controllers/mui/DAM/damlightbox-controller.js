﻿(function (ng, app) {
    "use strict";

    function muidamlightCtrl($window, $location, $timeout, $scope, $cookies, $resource, $translate, DamLightBoxService, $compile, $modal, $modalInstance, params) {
        var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
        $scope.callbackid = 0;
        var bootboxconfirmTimer, previewtimer, destroytimer, loadlightboxtimer;
        $scope.AssetLightboxSelectionFiles = [];
        $scope.FilterByOptionChange = function (id) {
            if (id != $scope.lightFilterStatus.filterstatus) {
                $scope.lightPageNoobj.pageno = 1;
                $scope.lightFilterStatus.filterstatus = id;
                var res = $.grep($scope.lightTaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.lightDamViewName.viewName = res[0].Name;
                GetAssetsData($scope.lightFilterStatus.filterstatus, $scope.lightOrderBy.order, $scope.lightPageNoobj.pageno);
            }
        }
        $scope.LightBox = {};
        $scope.select2Options = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Created date (Ascending)",
            ID: 3
        }, {
            "Name": "Created date (Descending)",
            ID: 4
        }];
        $scope.OrderbyOption = function (orderid) {
            if (orderid != $scope.lightOrderBy.order) {
                $scope.lightPageNoobj.pageno = 1;
                $scope.lightOrderBy.order = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $scope.lightOrderbyName.orderbyname = orderobj[0].Name;
                var res = $.grep($scope.lightTaskStatusObj, function (e) {
                    return e.ID == $scope.lightFilterStatus.filterstatus;
                });
                GetAssetsData($scope.lightFilterStatus.filterstatus, $scope.lightOrderBy.order, $scope.lightPageNoobj.pageno);
            }
        }

        function refreshAssetobjects() {
            $scope.lightPageNoobj = {
                pageno: 1
            };
            $scope.lightTaskStatusObj = [{
                "Name": "Thumbnail",
                ID: 1
            }, {
                "Name": "Summary",
                ID: 2
            }, {
                "Name": "List",
                ID: 3
            }];
            $scope.lightSettingsDamAttributes = {};
            $scope.lightOrderbyObj = [{
                "Name": "Name (Ascending)",
                ID: 1
            }, {
                "Name": "Name (Descending)",
                ID: 2
            }, {
                "Name": "Creation date (Ascending)",
                ID: 3
            }, {
                "Name": "Creation date (Descending)",
                ID: 4
            }];
            $scope.lightFilterStatus = {
                filterstatus: 1
            };
            $scope.lightDamViewName = {
                viewName: "Thumbnail"
            };
            $scope.lightOrderbyName = {
                orderbyname: "Creation date (Descending)"
            };
            $scope.lightOrderBy = {
                order: 4
            };
            $scope.AssetLightboxSelectionFiles = [];
        }
        refreshAssetobjects();
        loadlightboxtimer = $timeout(function () { LoadSystemLightbox(); }, 100);

        function LoadSystemLightbox() {
            $scope.lightDamViewName.viewName = "Thumbnail";
            $scope.lightFilterStatus.filterstatus = 1;
            $scope.lightOrderbyName.orderbyname = "Creation date (Descending)";
            $scope.AssetLightboxSelectionFiles.splice(0, $scope.AssetLightboxSelectionFiles.length);
            refreshViewObj();
            PageLoad();
        }

        function PageLoad() {
            $scope.subview = "Thumbnail";
            $scope.lightPageNoobj.pageno = 1;
            GetDAMViewSettings();

            drpclick();
        }

        function GetDAMViewSettings() {
            DamLightBoxService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.lightSettingsDamAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.lightSettingsDamAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.lightSettingsDamAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                    $scope.try_async_load();
                }
            });
        }
        $scope.try_async_load = function () {
            LoadAssetsData($scope.lightFilterStatus.filterstatus, $scope.lightOrderBy.order, $scope.lightPageNoobj.pageno);
        }

        function GetDamAssetIds() {
            var result = $.map($scope.damlightbox.lightboxselection, function (n, i) {
                return n.AssetId;
            }).join(',');
            if (result.length > 0) return result.split(",");
            else return null;
        }

        function GetAssetsData(filter, orderby, pageno) {
            var assetdIDs = GetDamAssetIds();
            if (assetdIDs != null) {
                var getlightboxassets = {};
                getlightboxassets.assetArr = assetdIDs;
                getlightboxassets.pageNo = pageno;
                getlightboxassets.orderby = orderby;
                getlightboxassets.View = filter;
                DamLightBoxService.GetLightBoxAsset(getlightboxassets).then(function (data) {
                    var res = [];
                    res = data.Response;
                    if ($scope.lightFilterStatus.filterstatus == 1) LoadThumbnailView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 2) LoadSummaryView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 3) LoadListView(res);
                });
            }
        }

        function LoadAssetsData(filter, orderby, pageno) {
            var assetdIDs = GetDamAssetIds();
            if (assetdIDs != null) {
                var getlightboxassets = {};
                getlightboxassets.assetArr = assetdIDs;
                getlightboxassets.pageNo = pageno;
                getlightboxassets.orderby = orderby;
                getlightboxassets.View = filter;
                DamLightBoxService.GetLightBoxAsset(getlightboxassets).then(function (data) {
                    var res = [];
                    res = data.Response;
                    if ($scope.lightFilterStatus.filterstatus == 1) LoadThumbnailView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 2) LoadSummaryView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 3) LoadListView(res);
                    loadScrollSettings();
                });
            }
        }

        function getMoreListItems() {
            $scope.lightPageNoobj.pageno = $scope.lightPageNoobj.pageno + 1;
            var assetdIDs = GetDamAssetIds();
            if (assetdIDs != null) {
                DamLightBoxService.GetLightBoxAsset(assetdIDs, $scope.lightFilterStatus.filterstatus, $scope.lightOrderBy.order, $scope.lightPageNoobj.pageno, true).then(function (data) {
                    var res = [];
                    res = data.Response;
                    if ($scope.lightFilterStatus.filterstatus == 1) LoadThumbnailView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 2) LoadSummaryView(res);
                    else if ($scope.lightFilterStatus.filterstatus == 3) LoadListView(res);
                });
            }
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }

        function loadScrollSettings() {
            $(".imglistSection").bind("scroll", function (e) {
                if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
                    checkListItemContents();
                }
            });
        }
        $scope.AssetChkClass = {};
        $scope.AssetSelectionClass = {};
        $scope.AssetFiles = [];
        var DynamicData = [];

        function refreshViewObj() {
            $(".imglistSection").scrollTop(0);
            var html = '';
            $("#lightboxentityassetdata").html(html);
            $scope.AssetSelection = {};
            $scope.AssetChkClass = {};
            $scope.AssetSelectionClass = {};
            $scope.AssetFiles = [];
            $scope.AssetDynamicData = [];
        }

        function IsAssetSelected(assetid) {
            var ligthboxavailable = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                return e == assetid
            })
            if (ligthboxavailable.length > 0) {
                return true;
            }
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                return e == assetid;
            });
            if (remainRecord.length > 0) {
                return true;
            }
            return false;
        }

        function LoadThumbnailView(res) {
            var html = '';
            if ($scope.lightPageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    html += '<div class="th-Box">';
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div id="select_' + asset.AssetUniqueID + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                    html += '<i id="lightchkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="LightBoxthumbnailActionMenu"></i>';
                    html += '</div>';
                    html += '<div class="th-Block">';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.DefaultSettings.DateFormat) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $("#lightboxentityassetdata").append($compile(html)($scope));
            } else {
                if ($scope.lightPageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">There is no item.</div>';
                    $("#lightboxentityassetdata").html(emptyHtml);
                }
            }
            $("#LightBoxPreview").modal("show");
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            if ($scope.lightFilterStatus.filterstatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            else if ($scope.lightFilterStatus.filterstatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            else if ($scope.lightFilterStatus.filterstatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                        return rel.AssetId == assetid;
                    });
                    if (damSelect.length > 0) {
                        damSelect[0].Selected = true;
                    }
                    if ($scope.AssetLightboxSelectionFiles.length > 0) {
                        $scope.AssetLightboxSelectionFiles = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                            return e != assetid;
                        });
                        $scope.AssetLightboxSelectionFiles.push(assetid);
                    }
                    else {
                        $scope.AssetLightboxSelectionFiles.push(assetid);
                    }
                }
                if ($scope.damlightbox.lightboxselection.length == $scope.AssetLightboxSelectionFiles.length) {
                    if ($("#icon_lightboxselectassets").hasClass("icon-unchecked")) {
                        $("#icon_lightboxselectassets").removeClass("icon-unchecked");
                        $("#icon_lightboxselectassets").addClass("icon-check");
                        $("#listselectall").addClass("checked");
                    }
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
                if ($scope.lightboxSelectAllAssetsObj.checkselectall) {
                    $scope.lightboxSelectAllAssetsObj.checkselectall = false;
                    $("#icon_lightboxselectassets").removeClass("icon-check");
                    $("#icon_lightboxselectassets").addClass("icon-unchecked");
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                        return rel.AssetId == assetid;
                    });
                    if (damSelect.length > 0) {
                        damSelect[0].Selected = false;
                    }
                    $scope.AssetLightboxSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetLightboxSelectionFiles), 1);
                }
                if ($("#icon_lightboxselectassets").hasClass("icon-check")) {
                    $("#icon_lightboxselectassets").removeClass("icon-check");
                    $("#icon_lightboxselectassets").addClass("icon-unchecked");
                    $("#listselectall").removeClass("checked");
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.lightSettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (attr.ID == 62 || attr.ID == 63) {
                                if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01' && data[0]["" + attr.ID + ""] != '-') html += '<span class="' + cls.toString() + '">' + dateFormat(data[0]["" + attr.ID + ""], $scope.format) + '</span>';
                            }
                            else if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        } else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function LoadSummaryView(res) {
            var html = '';
            if ($scope.lightPageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-sum-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div class="th-sum-Box"> ';
                    html += '        <div id="select_' + asset.AssetUniqueID + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + i + '" type="checkbox" /> ';
                    html += '                    <i id="lightchkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '<div class="th-sum-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="LightBoxthumbnailActionMenu"></i>';
                    html += '</div>';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer"> ';
                    if (asset.Category == 0) {
                        if (asset.ProcessType > 0) {
                            if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.AssetUniqueID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 3) {
                                html += '<img  src="' + imageBaseUrlcloud + '' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.DefaultSettings.DateFormat) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    html += GenereteSummaryDetailBlock(asset);
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                }
                $("#lightboxentityassetdata").append($compile(html)($scope));
            } else {
                if ($scope.lightPageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">There is no item.</div>';
                    $("#lightboxentityassetdata").html(emptyHtml);
                }
            }
        }

        function GenereteSummaryDetailBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.lightSettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (attr.ID == 62 || attr.ID == 63) {
                                if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01' && data[0]["" + attr.ID + ""] != '-') html += '<span class="' + cls.toString() + '">' + dateFormat(data[0]["" + attr.ID + ""], $scope.format) + '</span>';
                            }
                            else if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="' + cls.toString() + '">-</span>';
                        } else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.lightSettingsDamAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += '                                    <label class="control-label">' + attr.Caption + '</label> ';
                    html += '                                    <div class="controls"> ';
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            if (attr.ID == 62 || attr.ID == 63) {
                                if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01' && data[0]["" + attr.ID + ""] != '-') html += '<span class="control-label">' + dateFormat(data[0]["" + attr.ID + ""], $scope.format) + '</span>';
                            }
                            else if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="control-label">-</span>';
                        } else {
                            html += '<label class="control-label">-</label> ';
                        }
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += '                                    </div> ';
                    html += '                                </div> ';
                }
            }
            return html;
        }

        function LoadListView(res) {
            var html = '';
            if ($scope.lightPageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if ($scope.lightPageNoobj.pageno == 1) {
                    html += '<div>';
                    html += '    <table id="AssetListViewTableTask" class="table table-normal-left ListviewTable">';
                    html += '        <thead>';
                    html += GenerateHeader();
                    html += '        </thead>';
                    html += '        <tbody>';
                    html += GenerateList(assets);
                    html += '        </tbody>';
                    html += '    </table>';
                    html += '</div>';
                    $("#lightboxentityassetdata").append($compile(html)($scope));
                } else {
                    html += GenerateList(assets);
                    $("#AssetListViewTableTask tbody").append($compile(html)($scope));
                }
            } else {
                if ($scope.lightPageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">There is no item.</div>';
                    $("#lightboxentityassetdata").html(emptyHtml);
                }
            }
        }

        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input type="checkbox" />';
            //html += '           <i class="checkbox" id="listselectall" ng-click="selectallassets(0)"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.lightSettingsDamAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.lightSettingsDamAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                var assetselection = "";
                if (IsAssetSelected(asset.AssetUniqueID)) {
                    assetselection = "li-Selection selected";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                } else {
                    assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                }
                $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                html += '<tr id="select_' + asset.AssetUniqueID + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                html += '           <i id="lightchkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview"  ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <span class="thmbListviewImgSpan">';
                html += '           <div class="thmbListview-eIconContainer"> ';
                html += '               <span class="thmbListview-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.lightSettingsDamAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.lightSettingsDamAttributes["ListViewSettings"][i++];) {
                        html += '<td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                        if ($scope.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetDynamicData, function (e) {
                                return e.ID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                if (attr.ID == 62 || attr.ID == 63) {
                                    if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01' && data[0]["" + attr.ID + ""] != '-') html += '<span class="thmbListviewDescSpan">' + dateFormat(data[0]["" + attr.ID + ""], $scope.format) + '</span>';
                                }
                                else if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                else html += '<span class="thmbListviewDescSpan">-</span>';
                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '   <td><span class="thmbListview-options"><i id="lightchkBoxIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="LightBoxthumbnailActionMenu"></i></span></td>';
                html += '</tr>';
            }
            return html;
        }
        $('#LightBoxPreview').on('hidden.bs.modal', function () {
            previewtimer = $timeout.cancel(previewtimer);
        });
        $('#LightBoxPreview').on('shown.bs.modal', function () {
            previewtimer = $timeout(function () {
                CheckPreviewGenerator();
            }, 5000);
        });

        function CheckPreviewGenerator() {
            getPreviewIDs();
        }
        var PreviewGeneratorTimer;

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                var data = {};
                data.AssetIDs = assetids;
                DamLightBoxService.CheckPreviewGeneratorID(data).then(function (result) {
                    var generatedids = result.Response.split(',');
                    for (var j = 0; j < generatedids.length; j++) {
                        if (generatedids[j] != "") {
                            var src = $('#' + generatedids[j]).attr('data-src');
                            try {
                                var upd = $.grep($scope.AssetFiles, function (e) {
                                    return e.ActiveFileID == generatedids[j]
                                });
                                if (upd.length > 0) {
                                    $.grep($scope.AssetFiles, function (e) {
                                        return e.ActiveFileID == generatedids[j]
                                    })[0].Status = 2;
                                }
                                var lightboxid = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                    return e.AssetId == upd[0].AssetID
                                });
                                if (lightboxid.length > 0) {
                                    $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    })[0].Status = 2;
                                    $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    })[0].AssetStatus = 2;
                                }
                            } catch (e) { }
                            var extn = '.jpg';
                            if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                if (GlobalAssetEditID == generatedids[j]) {
                                    $('#asseteditimage').attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Big_' + src + '' + extn + '?' + generateUniqueTracker());
                                }
                            }
                            $('#' + generatedids[j]).removeClass('loadingImg');
                            $('#' + generatedids[j]).attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator();
            }, 3000);
        }

        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.lightSettingsDamAttributes["ThumbnailSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.lightSettingsDamAttributes["ThumbnailSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attributedatares = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        });
                        if (attributedatares.length > 0) $scope.attributedata.push(attributedatares[0]);
                    }
                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span></div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }
                    }
                }
            }
            return html;
        }
        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamLightBoxService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '<div class="th-AssetInfoTooltip">';
                    html += DrawThumbnailBlock(attrList);
                    html += '</div> ';
                    $('#' + qtip_id).html(html);
                });
            }
        };
        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamLightBoxService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '       <div class="lv-ImgDetailedPreview">';
                    html += '           <div class="idp-Box"> ';
                    html += '               <div class="idp-Block"> ';
                    html += '                   <div class="idp-leftSection"> ';
                    html += '                       <div class="idp-ImgBlock"> ';
                    html += '                           <div class="idp-ImgDiv"> ';
                    html += '                               <div class="idp-ImgContainer"> ';
                    if (attrList.Response.Category == 0) {
                        if ($scope.ActiveVersion.ProcessType > 0) {
                            if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 3) {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (attrList.Response.Category == 1) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (attrList.Response.Category == 2) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                               </div> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                       <div class="idp-baiscDetailBlock"> ';
                    html += '                           <div class="idp-ActionButtonContainer"> ';
                    html += '                               <span> ';
                    html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (attrList.Response.Category == 0) {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                               </span> ';
                    html += '                           </div> ';
                    html += '                           <div class="idp-detail-eIconContainer"> ';
                    html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '                   <div class="idp-rightSection"> ';
                    html += '                       <div class="idp-expandedDetailBlock"> ';
                    html += '                           <div class="idp-expandedDetails"> ';
                    html += '                               <form class="form-horizontal"> ';
                    try {
                        html += DrawListBlock(attrList);
                    } catch (e) { }
                    html += '                               </form> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '               </div> ';
                    html += '           </div> ';
                    html += '       </div>';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.lightSettingsDamAttributes["ListViewSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.lightSettingsDamAttributes["ListViewSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attributedatares = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        });
                        if (attributedatares.length > 0) $scope.attributedata.push(attributedatares[0]);
                    }
                }
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Value + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].Caption == null || $scope.attributedata[i].Caption == undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html("-").text();
                            $scope.attributedata[i].Caption = "-";
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }
        $scope.downloadLightboxFiles = function () {
            if ($scope.damlightbox.lightboxselection.length > 0) {
                $scope.ProgressMsgHeader = "Downloading Assets";
                var assetSelection = [];
                assetSelection = $.grep($scope.damlightbox.lightboxselection, function (e) {
                    return e.Selected == true;
                });
                if (assetSelection.length == 1) {
                    destroytimer = $timeout(function () {
                        var IsLightboxcropped = false;
                        if (assetSelection[0].Iscropped == true) IsLightboxcropped = true;
                        else IsLightboxcropped = false;
                        var fileid = assetSelection[0].Guid,
							extn = assetSelection[0].Ext == '.zip' ? 'zip' : assetSelection[0].Ext;
                        var filename = assetSelection[0].Description;
                        var actualfilename = "";
                        for (var i = 0; i < filename.length; i++) {
                            if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                                actualfilename = actualfilename + (filename[i]);
                            }
                        }
                        blockUIForDownload();
                        location.href = 'DAMDownload.aspx?FileID=' + assetSelection[0].Guid + '&FileFriendlyName=' + actualfilename + '&Ext=' + extn + '&token=' + $('#lightboxdownload_token_value_id').val() + '&IsLightboxcropped=' + IsLightboxcropped;
                        refreshTaskAssetSelection();
                        assetSelection = [];
                    }, 100);
                } else if (assetSelection.length > 1) {
                    destroytimer = $timeout(function () {
                        DamLightBoxService.DownloadFiles(assetSelection).then(function (result) {
                            if (result.Response == null) {
                                NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                                $('#lightboxloadingDownloadPageModel').modal("hide");
                            } else {
                                var fileid = result.Response,
									extn = '.zip';
                                var filename = result.Response + extn;
                                blockUIForDownload();
                                location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#lightboxdownload_token_value_id').val();
                                refreshTaskAssetSelection();
                                assetSelection = [];
                            }
                        });
                    }, 100);
                } else if (assetSelection.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
                    refreshTaskAssetSelection();
                    assetSelection = [];
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4516.Caption'));
            }
        }
        $scope.removefromLightboxFiles = function () {
            var assetidstoremovelightbox = [];
            if ($scope.damlightbox.lightboxselection.length > 0) {
                var assetSelection = [];
                assetSelection = $.grep($scope.damlightbox.lightboxselection, function (e) {
                    return e.Selected == true;
                });
                if (assetSelection.length > 0) bootbox.confirm($translate.instant('LanguageContents.Res_1155.Caption'), function (result) {
                    if (result) {
                        bootboxconfirmTimer = $timeout(function () {
                            for (var i = 0, file; file = assetSelection[i++];) {
                                assetidstoremovelightbox.push(file.AssetId);
                                $scope.damlightbox.lightboxselection.splice($.inArray(file, $scope.damlightbox.lightboxselection), 1);
                            }
                            if ($scope.damlightbox.lightboxselection.length > 0) {
                                var assetCollection = 0;
                                assetCollection = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                    return e.Selected == true;
                                }).length;
                                if (assetCollection != 0) {
                                    if (assetCollection != $scope.damlightbox.lightboxselection.length) $scope.selectedAllassets = false;
                                    else $scope.selectedAllassets = true;
                                } else $scope.selectedAllassets = false;
                                refreshTaskAssetSelection();
                                $scope.try_async_load();
                            } else {
                                $scope.selectedAllassets = false;
                                refreshViewObj();
                                refreshTaskAssetSelection();
                            }
                        }, 100);
                    }
                });
                else bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4516.Caption'));
            }
        }
        var fileDownloadCheckTimer;

        function blockUIForDownload() {
            var token = new Date().getTime();
            $('#lightboxdownload_token_value_id').val(token);
            $scope.ProgressContent = "Downloading";
            $('#lightboxloadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                finishDownload();
            }, 1000);
        }

        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken');
            $('#lightboxloadingDownloadPageModel').modal("hide");
        }
        $scope.IsLightBoxMailSender = false;
        $scope.processMailFlag = false;
        $scope.openSendPopup = function () {
            if ($scope.AssetLightboxSelectionFiles.length > 0) {
                $timeout(function () {
                    $scope.mailSubject = "";
                    $scope.LightBox = {};
                    $scope.LightBox.mailtags = [];
                    $scope.IsLightBoxMailSender = false;
                    $scope.includemdata = false;
                    $('#sendlightboxmailPopup').modal('show');
                }, 100);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked) $scope.includemdata = true;
            else $scope.includemdata = false;
        }
        $scope.includemdata = false;
        $scope.sendMail = function () {
            if ($scope.AssetLightboxSelectionFiles.length > 0) {
                if ($('#sendLightboxMail').hasClass('disabled')) {
                    return;
                }
                $('#sendLightboxMail').addClass('disabled');
                $scope.ProgressMsgHeader = "Mail Send";
                $scope.ProgressContent = "sending mail..";
                $scope.processMailFlag = true;
                var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var assetSelection = [],
					toArray = new Array(),
					mailSubject = $scope.mailSubject != "" ? $scope.mailSubject : "Marcom Media Bank";
                for (var i = 0, mail; mail = $scope.LightBox.mailtags[i++];) {
                    if (mail.text.match(emailvalid)) {
                        toArray.push(mail.text);
                    } else {
                        $('#sendLightboxMail').removeClass('disabled');
                        bootbox.alert($translate.instant('LanguageContents.Res_1137.Caption'));
                        $scope.processMailFlag = false;
                        return false;
                    }
                }
                for (var i = 0; i < $scope.AssetLightboxSelectionFiles.length; i++) {
                    assetSelection.push({
                        "AssetId": $scope.AssetLightboxSelectionFiles[i]
                    });
                }
                if (assetSelection.length > 0 && toArray.length > 0) {
                    var srcObj = {};
                    srcObj.Toaddress = toArray.join(",");
                    srcObj.subject = mailSubject;
                    srcObj.AssetArr = assetSelection;
                    srcObj.metadata = $scope.includemdata;
                    destroytimer = $timeout(function () {
                        DamLightBoxService.SendMailWithMetaData(srcObj).then(function (result) {
                            if (result.Response == null) {
                                $('#sendLightboxMail').removeClass('disabled');
                                $scope.IsLightBoxMailSender = false;
                                $scope.processMailFlag = false;
                                $scope.mailSubject = "";
                                $scope.LightBox.mailtags = [];
                                NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                                $("#loadingDownloadPageModel").modal("hide");
                                $('#sendLightboxMail').modal("hide");
                                $('#sendlightboxmailPopup').modal('hide');
                                refreshTaskAssetSelection();
                            } else {
                                $('#sendLightboxMail').removeClass('disabled');
                                $scope.mailSubject = "";
                                $scope.processMailFlag = false;
                                $scope.LightBox.mailtags = [];
                                $scope.IsLightBoxMailSender = false;
                                NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                $("#loadingDownloadPageModel").modal("hide");
                                $('#sendLightboxMail').modal("hide");
                                $('#sendlightboxmailPopup').modal('hide');
                                refreshTaskAssetSelection();
                            }
                        });
                    }, 100);
                } else {
                    $scope.processMailFlag = false;
                    $('#sendLightboxMail').removeClass('disabled');
                    $scope.IsLightBoxMailSender = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_1138.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.lightboxSelectAllAssetsObj = {
            checkselectall: false
        };
        $scope.Dselectallassets = function () {
            if ($scope.lightboxSelectAllAssetsObj.checkselectall == false) {
                $scope.lightboxSelectAllAssetsObj.checkselectall = true;
                $scope.selectallassets(0);
            } else {
                $scope.lightboxSelectAllAssetsObj.checkselectall = true;
                $scope.selectallassets(0);
            }
        }
        $scope.selectallassets = function (frommenu) {
            if (frommenu == 0) {
                $scope.lightboxSelectAllAssetsObj.checkselectall = ($scope.lightboxSelectAllAssetsObj.checkselectall) ? false : true;
            } else {
                $scope.lightboxSelectAllAssetsObj.checkselectall = true;
            }
            if ($scope.lightboxSelectAllAssetsObj.checkselectall) {
                $("#icon_lightboxselectassets").removeClass("icon-unchecked");
                $("#icon_lightboxselectassets").addClass("icon-check");
                $("#listselectall").addClass("checked");
            } else {
                $("#icon_lightboxselectassets").removeClass("icon-check");
                $("#icon_lightboxselectassets").addClass("icon-unchecked");
                $("#listselectall").removeClass("checked");
            }
            if ($scope.lightDamViewName.viewName == "Thumbnail") {
                for (var i = 0, asset; asset = $scope.AssetFiles[i++];) {
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.lightboxSelectAllAssetsObj.checkselectall;
                    if ($scope.lightboxSelectAllAssetsObj.checkselectall == true) {
                        $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                    } else {
                        $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "th-Selection selected" : "th-Selection";
                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.lightboxSelectAllAssetsObj.checkselectall);
                }
            } else if ($scope.lightDamViewName.viewName == "Summary") {
                for (var i = 0, asset; asset = $scope.AssetFiles[i++];) {
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.lightboxSelectAllAssetsObj.checkselectall;
                    if ($scope.lightboxSelectAllAssetsObj.checkselectall == true) {
                        $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                    } else {
                        $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').removeAttr('checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "th-sum-Selection selected" : "th-sum-Selection";
                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.lightboxSelectAllAssetsObj.checkselectall);
                }
            } else if ($scope.lightDamViewName.viewName == "List") {
                for (var i = 0, asset; asset = $scope.AssetFiles[i++];) {
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.lightboxSelectAllAssetsObj.checkselectall;
                    if ($scope.lightboxSelectAllAssetsObj.checkselectall == true) {
                        $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                    } else {
                        $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                        $('#lightchkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.lightboxSelectAllAssetsObj.checkselectall == true) ? "li-Selection selected" : "li-Selection";
                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.lightboxSelectAllAssetsObj.checkselectall);
                }
            }
        }

        function updatescopesforactions(assetid, activefileid, ischecked) {
            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                    return rel.AssetId == assetid;
                });
                if (ischecked) {
                    if (damSelect.length > 0) {
                        damSelect[0].Selected = true;
                    }
                    $scope.AssetLightboxSelectionFiles.push(assetid);
                    if (res[0].Category == 0) {
                        AddAttachmentsToTask(assetid, activefileid, true);
                    }
                } else {
                    if (damSelect.length > 0) {
                        damSelect[0].Selected = false;
                    }
                    $scope.AssetLightboxSelectionFiles.splice($.inArray(res[0], $scope.AssetLightboxSelectionFiles), 1);
                    if (res[0].Category == 0) {
                        AddAttachmentsToTask(assetid, activefileid, false);
                    }
                }
            }
        }
        $scope.DownloadWithMetadata = function (result) {
            if ($scope.AssetLightboxSelectionFiles.length > 0) {
                var arr = [];
                for (var i = 0; i < $scope.AssetLightboxSelectionFiles.length; i++) {
                    arr[i] = $scope.AssetLightboxSelectionFiles[i];
                }
                var include = result;
                if (include == 0) $scope.ProgressMsgHeader = "Downloading Metadata";
                if (include == 1) $scope.ProgressMsgHeader = "Downloading Package";
                blockUIForDownload();
                DamLightBoxService.DownloadAssetWithMetadata(arr, result).then(function (result) {
                    if (result.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                    } else {
                        if (arr.length == 1) {
                            if (include == false) {
                                var fileid = result.Response[1],
									extn = '.xml';
                                if (result.Response[0] == null || result.Response[0] == "") var filename = "MediabankAssets" + extn;
                                else var filename = result.Response[0];
                            } else {
                                var fileid = result.Response[1],
									extn = '.zip';
                                var filename = fileid + extn;
                            }
                        } else {
                            var fileid = result.Response[1],
								extn = '.zip';
                            var filename = fileid + extn;
                        }

                        if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                            location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                        } else {
                            location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                        }
                        if (include == true) NotifySuccess($translate.instant('LanguageContents.Res_5805.Caption'));
                        else NotifySuccess($translate.instant('LanguageContents.Res_5806.Caption'));
                        refreshTaskAssetSelection();
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.PerformAssetOperation = function () {
            if ($scope.AssetLightboxSelectionFiles.length > 0) {
                for (var i = 0; i < $scope.AssetLightboxSelectionFiles.length; i++) {
                    $scope.damclipboard.assetselection.push({
                        "AssetId": $scope.AssetLightboxSelectionFiles[i]
                    });
                }
                NotifySuccess($translate.instant('LanguageContents.Res_1139.Caption'));
                refreshTaskAssetSelection();
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.SetAssetActionId = function (assetid) {
            $('#lightchkBoxIcon_' + assetid + '').addClass('checked');
            if ($scope.AssetLightboxSelectionFiles.length > 0) {
                var asset = [],
					data = [];
                asset = $.grep($scope.AssetLightboxSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (asset.length == 0) {
                    $scope.AssetLightboxSelectionFiles.push(assetid);
                    var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                        return rel.AssetId == assetid;
                    });
                    if (damSelect.length > 0) {
                        damSelect[0].Selected = true;
                    }
                    if ($scope.lightFilterStatus.filterstatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                    else if ($scope.lightFilterStatus.filterstatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                    else if ($scope.lightFilterStatus.filterstatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";
                    $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
                    $scope.AssetSelection["asset_'" + assetid + ""] = true;
                }
            } else {
                $scope.AssetLightboxSelectionFiles.push(assetid);
                var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                    return rel.AssetId == assetid;
                });
                if (damSelect.length > 0) {
                    damSelect[0].Selected = true;
                }
                if ($scope.lightFilterStatus.filterstatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                else if ($scope.lightFilterStatus.filterstatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                else if ($scope.lightFilterStatus.filterstatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";
                $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
                $scope.AssetSelection["asset_'" + assetid + ""] = true;
            }
            var assets = [];
            assets = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            AddAttachmentsToTask(assetid, assets[0].ActiveFileID, true);
        }

        function refreshTaskAssetSelection() {
            if ($("#listselectall").length != null) {
                if ($("#listselectall").hasClass("checked")) {
                    $("#listselectall").removeClass("checked");
                }
            }
            $("#icon_lightboxselectassets").removeClass("icon-check");
            $("#icon_lightboxselectassets").addClass("icon-unchecked");
            for (var i = 0; i < $scope.AssetLightboxSelectionFiles.length; i++) {
                $('#lightchkBoxIcon_' + $scope.AssetLightboxSelectionFiles[i]).attr("class", "checkbox");
                $scope.AssetChkClass["asset_" + $scope.AssetLightboxSelectionFiles[i] + ""] = "checkbox";
                $scope.AssetSelection["asset_'" + $scope.AssetLightboxSelectionFiles[i] + ""] = false;
                var damSelect = $.grep($scope.damlightbox.lightboxselection, function (rel) {
                    return rel.AssetId == $scope.AssetLightboxSelectionFiles[i];
                });
                if (damSelect.length > 0) {
                    damSelect[0].Selected = false;
                }
                if ($scope.lightDamViewName.viewName == "Thumbnail") {
                    $('#select_' + $scope.AssetLightboxSelectionFiles[i]).attr("class", "th-Selection");
                    $scope.AssetSelectionClass["asset_" + $scope.AssetLightboxSelectionFiles[i]] = "th-Selection";
                } else if ($scope.lightDamViewName.viewName == "Summary") {
                    $('#select_' + $scope.AssetLightboxSelectionFiles[i]).attr("class", "th-sum-Selection");
                    $scope.AssetSelectionClass["asset_" + $scope.AssetLightboxSelectionFiles[i]] = "th-sum-Selection";
                } else if ($scope.lightDamViewName.viewName == "List") {
                    $('#select_' + $scope.AssetLightboxSelectionFiles[i]).attr("class", "li-Selection");
                    $scope.AssetSelectionClass["asset_" + $scope.AssetLightboxSelectionFiles[i]] = "li-Selection";
                }
            }
            $scope.AssetLightboxSelectionFiles = [];
            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if (ischecked == true) {
                var response = $.grep($scope.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep($scope.AssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep($scope.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                if ($scope.DamAssetTaskselectionFiles.AssetDynamicData != undefined) {
                    var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                        return e.ID == assetid
                    })[0];
                    $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
                }
            }
        }
        $scope.createAssettask = function (tasktype) {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 0) {
                if (tasktype == 32 || tasktype == 36) {
                    if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                        refreshTaskAssetSelection();
                        return false;
                    }
                }
                var assetid = 0;
                if (tasktype == 32 || tasktype == 36) assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                var tempDamAssetSelected = $scope.DamAssetTaskselectionFiles.AssetFiles;
                var tempSelectedAssetDynData = $scope.DamAssetTaskselectionFiles.AssetDynamicData;
                refreshTaskAssetSelection();
                $scope.DamAssetTaskselectionFiles.AssetFiles = tempDamAssetSelected;
                $scope.DamAssetTaskselectionFiles.AssetDynamicData = tempSelectedAssetDynData;
                var opentaskpopupdata = {};
                opentaskpopupdata.AssetID = assetid;
                opentaskpopupdata.IsTask = true;
                opentaskpopupdata.IsLock = false;
                opentaskpopupdata.TaskType = tasktype;
                opentaskpopupdata.Fromplace = "Task";
                opentaskpopupdata.Type = "addtaskfromasset";
                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskCreation.html',
                    controller: "mui.task.muitaskTaskCreationCtrl",
                    resolve: {
                        params: function () {
                            return {
                                data: opentaskpopupdata
                            };
                        }
                    },
                    scope: $scope,
                    windowClass: 'addTaskPopup popup-widthL',
                    backdrop: "static"
                });
                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () { });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4608.Caption'));
        }
        $scope.reformatprocess = function () {
            if ($scope.AssetLightboxSelectionFiles.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
            var reformate = [],
				temparr = [];
            for (var i = 0; i < $scope.AssetLightboxSelectionFiles.length; i++) {
                temparr = $.grep($scope.AssetFiles, function (e) {
                    return e.Category == 0 && e.ProcessType == 1 && e.Status == 2 && e.Extension != ".pdf" && e.AssetUniqueID == $scope.AssetLightboxSelectionFiles[i];
                });
                if (temparr != null) {
                    if (temparr.length > 0) {
                        reformate.push(temparr[0]);
                    }
                }
            }
            if ($scope.AssetLightboxSelectionFiles.length > 0 && reformate.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1165.Caption'));
                refreshTaskAssetSelection();
                return false;
            } else if ($scope.AssetLightboxSelectionFiles.length > 0 && reformate.length > 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_1166.Caption'));
                refreshTaskAssetSelection();
                return false;
            } else if ($scope.AssetLightboxSelectionFiles.length > 0 && reformate.length == 1) {
                destroytimer = $timeout(function () {
                    var folderid;
                    var EntityID;
                    var Assetid;
                    folderid = reformate[0].FolderID;
                    EntityID = reformate[0].EntityID;
                    Assetid = reformate[0].AssetID;
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/assetreformat.html',
                        controller: "mui.muiassetreformatCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    thubmnailGuid: $scope.ActiveVersion.Fileguid,
                                    assetid: Assetid,
                                    folderid: folderid,
                                    EntityID: EntityID,
                                    fromisassetlibrary: 0
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'reformatAssetPopup popup-widthXL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                }, 100);
                refreshTaskAssetSelection();
            }
        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
            $timeout.cancel(previewtimer);
            $timeout.cancel(destroytimer);
            $timeout.cancel(bootboxconfirmTimer);
            $timeout.cancel(loadlightboxtimer);
        });
        $scope.closethispopup = function () {
            $modalInstance.dismiss('cancel');
        }


        function drpclick() {

            //$('.modal:visible').find(".dropdown-toggle").each(function (index, e) {
            //    var posX = $(e).offset().left, posY = $(e).offset().top;
            //    console.log((e.pageX - posX) + ' , ' + (e.pageY - posY));

            //});

            //$('.modal:visible').find(".dropdown-toggle").unbind('click').click(function (e) {
            //    var menu = $(this).next('.dropdown-menu'),
            //        mousex = e.pageX + 20, //Get X coodrinates
            //        mousey = e.pageY + 20, //Get Y coordinates
            //        menuWidth = menu.width(), //Find width of tooltip
            //        menuHeight = menu.height(), //Find height of tooltip
            //        menuVisX = $(window).width() - (mousex + menuWidth), //Distance of element from the right edge of viewport
            //        menuVisY = $(window).height() - (mousey + menuHeight); //Distance of element from the bottom of viewport

            //    console.log(menuVisX + ' , ' + menuVisY);

            //    if (menuVisX < 20) { //If tooltip exceeds the X coordinate of viewport
            //        menu.css({ 'left': '-89px' });
            //    } if (menuVisY < 20) { //If tooltip exceeds the Y coordinate of viewport
            //        menu.css({
            //            'top': 'auto',
            //            'bottom': '100%',
            //        });

            //        $(this).children('.caret').removeClass('.caret').removeClass('.caret-reversed').addClass("caret caret-reversed");
            //    }
            //});


        }

    }
    app.controller("mui.DAM.lightboxCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$translate', 'DamLightBoxService', '$compile', '$modal', '$modalInstance', 'params', muidamlightCtrl]);
})(angular, app);