﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Presentation.ReportViewer" %>

<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head  runat="server">
    <title></title>
    
    <%--<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script type="text/javascript">
      
    </script>--%>

</head>
<body style="margin:0px;padding:0px;">
    <form id="form1" runat="server">
        
    <div style="margin:0px;padding:0px;" id="ReportContainer">
        
        <asp:HiddenField ID="SelectedIDs" runat="server" />
        <asp:HiddenField ID="ReportId" runat="server" />
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ToolbarMode="StandardToolbar" ClientInstanceName="ReportViewer" EnableTheming="True" Theme="Office2010Blue">
             <SettingsRemoteSource EndpointConfigurationName="ReportServer" 
                OnRequestParameters="documentViewer_RequestParameters" 
                AuthenticationType="Forms" 
                OnRequestCredentials="documentViewer_RequestCredentials" />
        </dx:ASPxDocumentViewer>
        </div>
    </form>
</body>
</html>

