using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// SubscriptionTypeDao object for table 'CM_SubscriptionType'.
	/// </summary>
	
	public partial class SubscriptionTypeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
        protected bool _isappdefault;
        protected bool _isappmandatory;
        protected bool _ismaildefault;
        protected bool _ismailmandatory;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public SubscriptionTypeDao() {}

        public SubscriptionTypeDao(string pCaption, bool pisAppDefault, bool pisAppMandatory, bool pisMailDefault, bool pisMailMandatory)
		{
			this._caption = pCaption;
            this._isappdefault = pisAppDefault;
            this._isappmandatory = pisAppMandatory;
            this._ismaildefault = pisMailDefault;
            this._ismailmandatory = pisMailMandatory; 
		}
				
		public SubscriptionTypeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}

        public virtual bool isAppDefault
		{
			get { return _isappdefault; }
            set { _bIsChanged |= (_isappdefault != value); _isappdefault = value; }
			
		}
        public virtual bool isAppMandatory
        {
            get { return _isappmandatory; }
            set { _bIsChanged |= (_isappmandatory != value); _isappmandatory = value; }

        }
        public virtual bool isMailDefault
        {
            get { return _ismaildefault; }
            set { _bIsChanged |= (_ismaildefault != value); _ismaildefault = value; }

        }
        public virtual bool isMailMandatory
        {
            get { return _ismailmandatory; }
            set { _bIsChanged |= (_ismailmandatory != value); _ismailmandatory = value; }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string SubscriptionTypeDao = "SubscriptionTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";
            public const string isAppDefault = "isAppDefault";
            public const string isAppMandatory = "isAppMandatory";
            public const string isMailDefault = "isMailDefault";
            public const string isMailMandatory = "isMailMandatory";		
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string SubscriptionTypeDao = "SubscriptionTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";
            public const string isAppDefault = "isAppDefault";
            public const string isAppMandatory = "isAppMandatory";
            public const string isMailDefault = "isMailDefault";
            public const string isMailMandatory = "isMailMandatory";			
		}

		#endregion
		
		
	}
	
}
