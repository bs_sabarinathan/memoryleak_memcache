﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Base
{
    [Serializable]
    public class QueryHintInterceptor : EmptyInterceptor
    {
        internal const string QueryHintHashJoinCommentString = "queryhint-option-hash";
        internal const string QueryHintRecompileCommentString = "queryhint-recompile";

        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            if (sql.ToString().Contains(QueryHintHashJoinCommentString))
            {
                return sql.Insert(sql.Length, " option(HASH JOIN)");
            }
            if (sql.ToString().Contains(QueryHintRecompileCommentString))
            {
                return sql.Insert(sql.Length, " option(RECOMPILE)");
            }
            return base.OnPrepareStatement(sql);
        }
    }
}
