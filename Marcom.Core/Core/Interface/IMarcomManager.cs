﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Collections;
using BrandSystems.Marcom.Core.Metadata;


namespace BrandSystems.Marcom.Core.Interface
{
    public interface IMarcomManager
    {
        IUser User { get; set; }
        Guid SessionID { get; set; }
        List<SortOrderIdsCollection> EntitySortorderIdColle { get; set; }
        List<SortOrderIdsCollection> EntityFilterSortorderIDColln { get; set; }
        int EntitySortorderIdColleHash { get; set; }
        string EntityMainQuery { get; set; }
        Tuple<ArrayList, ArrayList> GeneralColumnDefs { get; set; }

        IReportManager ReportManager { get; }
        IUserManager UserManager { get; }
        IPlanningManager PlanningManager { get; }
        IAccessManager AccessManager { get; }
        IEventManager EventManager { get; }
        IPluginManager PluginManager { get; }
        IMetadataManager MetadataManager { get; }
        ICommonManager CommonManager { get; }
        ITaskManager TaskManager { get; }
        IDigitalAssetManager DigitalAssetManager { get; }
        //ITransaction GetTransaction();
        //ITransaction GetTransaction(bool create);

        ITransaction GetTransaction(int tenantid);
        ITransaction GetTransaction(bool create, int tenantid);
        IList<IAdditionalSettings> GlobalAdditionalSettings { get; set; }
        ICmsManager CmsManager { get; }
        IExpireHandlerManager ExpireHandlerManager { get; }
        IExternalTaskManager ExternalTaskManager { get; }
        bool PowerMode { get; set; }
    }
}
