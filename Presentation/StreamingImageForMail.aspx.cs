﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace Presentation
{
    public partial class StreamingImageForMail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPath = null;

            string[] filetypes = { ".jpg", ".jpeg", ".png", ".psd", ".bmp", ".tif", ".tiff", ".gif", ".emf", ".eps", ".pdf" };

            string tenanturl = HttpContext.Current.Request.Url.Host;
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePathByHostName(tenanturl);

            try
            {

                if (Request.QueryString["Ext"] == "-")
                {
                    if (File.Exists(Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/" + Request.QueryString["FileID"])))
                    {
                        strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/" + Request.QueryString["FileID"]);
                    }
                    else
                    {
                        strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/Preview/NoPreview.jpg");
                    }
                }
                else
                {
                    int pos = Array.IndexOf(filetypes, Request.QueryString["Ext"].ToLower());
                    if (pos > -1)
                    {
                        string nam = Request.QueryString["FileID"];
                        if (nam.Contains("."))
                        {
                            nam = nam.Substring(0, nam.LastIndexOf('.'));
                            if (File.Exists(Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/" + Request.QueryString["FileID"])))
                            {
                                strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/" + Request.QueryString["FileID"]);
                            }
                            else
                            {
                                strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Preview/NoPreview.jpg");
                            }
                        }
                        else
                        {
                            if (File.Exists(Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Preview/Small_" + Request.QueryString["FileID"] + ".jpg")))
                            {
                                strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Preview/Small_" + Request.QueryString["FileID"] + ".jpg");
                            }
                            else
                            {
                                strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Preview/NoPreview.jpg");
                            }
                        }
                    }
                    else
                    {
                        string ext = Request.QueryString["Ext"].ToUpper();
                        if (File.Exists(Server.MapPath( "~/" +TenantFilePath +"DAMFiles/StaticPreview/" + ext.Substring(1, ext.Length - 1) + ".png")))
                            strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/StaticPreview/" + ext.Substring(1, ext.Length - 1) + ".png");
                        else
                            strPath = Server.MapPath( "~/" +TenantFilePath +"DAMFiles/Original/Preview/NoPreview.jpg");
                    }

                }


                string fileName = Request.QueryString["FileID"];

                if ((fileName != null))
                {
                    if (fileName.Length > 0)
                    {
                        if (File.Exists(strPath))
                        {
                            DownloadFile(strPath, true);

                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void DownloadFile(string fname, bool forceDownload)
        {

            string fullpath = System.IO.Path.GetFullPath(fname);
            string strFriendlyName = Request.QueryString["FileFriendlyName"];

            string name = System.IO.Path.GetFileName(fullpath);
            name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
            string ext = System.IO.Path.GetExtension(fullpath);
            string type = "";

            switch (ext)
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;
                case "-":
                    type = "text/HTML";
                    break;
                case ".txt":
                    type = "text/plain";
                    break;
                case ".doc":
                case ".rtf":
                case ".docx":
                    type = "Application/msword";
                    break;
                case ".csv":
                case ".xls":
                case ".xlsx":
                    type = "Application/x-msexcel";
                    break;
                case ".pdf":
                    type = "Application/pdf";
                    break;
                case ".tiff":
                    type = "image/tiff";
                    break;
                case ".bmp":
                    type = "image/bmp";
                    break;
                case ".jpeg":
                    type = "image/jpeg";
                    break;
                case ".jpg":
                    type = "image/jpeg";
                    break;
                case ".png":
                    type = "image/png";
                    break;
                case ".gif":
                    type = "image/gif";
                    break;
                case ".zip":
                    type = "Application/zip";
                    break;
                default:
                    type = "application/octet-stream";
                    break;
            }

            if ((forceDownload))
            {

                Response.AppendHeader("content-disposition", "attachment; filename=\"" + name + "\"");
            }
            if (!string.IsNullOrEmpty(type))
            {
                Response.ContentType = type;
                Response.WriteFile(fullpath);
                Response.End();

            }


        }


        public StreamingImageForMail()
        {
            Load += Page_Load;
        }
    }


}