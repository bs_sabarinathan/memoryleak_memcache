using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// GanttHeaderBar object for table 'PM_GanttHeaderBar'.
	/// </summary>
	
	internal class GanttHeaderBar : IGanttHeaderBar 
	{
		#region Member Variables

		protected int _id;
		protected DateTime _startdate;
		protected DateTime _enddate;
		protected string _colorcode;
		protected string _description;
		
		
		#endregion
		
		#region Constructors
		public GanttHeaderBar() {}
			
		public GanttHeaderBar(DateTime pStartDate, DateTime pEndDate, string pColorCode, string pDescription)
		{
			this._startdate = pStartDate; 
			this._enddate = pEndDate; 
			this._colorcode = pColorCode; 
			this._description = pDescription; 
		}
				
		public GanttHeaderBar(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public DateTime StartDate
		{
			get { return _startdate; }
			set { _startdate = value; }
			
		}
		
		public DateTime EndDate
		{
			get { return _enddate; }
			set { _enddate = value; }
			
		}
		
		public string ColorCode
		{
			get { return _colorcode; }
			set 
			{
			  if (value != null && value.Length > 10)
			    throw new ArgumentOutOfRangeException("ColorCode", "ColorCode value, cannot contain more than 10 characters");
			  _colorcode = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			GanttHeaderBar castObj = null;
			try
			{
				castObj = (GanttHeaderBar)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
