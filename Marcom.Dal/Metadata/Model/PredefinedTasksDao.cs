﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// AttributeDao object for table 'MM_Attribute'.
    /// </summary>

    public partial class PredefinedTasksDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected int _workflowtypeid;
        protected bool _active;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public PredefinedTasksDao() { }

        public PredefinedTasksDao(string pName, string pDescription)
        {
            this._name = pName;
            this._description = pDescription;
        }

        public PredefinedTasksDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

      
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Name", "Caption value, cannot contain more than 500 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual int WorkFlowType
        {
            get { return _workflowtypeid; }
            set { _bIsChanged |= (_workflowtypeid != value); _workflowtypeid = value; }

        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string PredefinedTasksDao = "PredefinedTasksDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string WorkFlowType = "WorkFlowType";
            public const string Active = "Active";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PredefinedTasksDao = "PredefinedTasksDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string WorkFlowType = "WorkFlowType";
            public const string Active = "Active";
        }

        #endregion


    }

}
