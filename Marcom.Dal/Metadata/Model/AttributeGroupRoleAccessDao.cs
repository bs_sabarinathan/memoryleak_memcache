﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class AttributeGroupRoleAccessDao : BaseDao, ICloneable 
    {
         #region Member Variables

		protected int _id;
        protected int _entitytypeid;
        protected int _globalroleid;
        protected int _attrgroupid;

		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public AttributeGroupRoleAccessDao() {}

        public AttributeGroupRoleAccessDao(int pId, int pEntitytypeId, int pGlobalroleId, int pAttrGroupId)
		{
			this._id = pId;
            this._entitytypeid = pEntitytypeId;
            this._globalroleid = pGlobalroleId;
            this._attrgroupid = pAttrGroupId;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int ID
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
        }

        public virtual int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }
        }

        public virtual int AttributeGroupID
        {
            get { return _attrgroupid; }
            set { _bIsChanged |= (_attrgroupid != value); _attrgroupid = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string AttributeGroupRoleAccessDao = "AttributeGroupRoleAccessDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string GlobalRoleID = "GlobalRoleID";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string AttributeGroupRoleAccessDao = "AttributeGroupRoleAccessDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string GlobalRoleID = "GlobalRoleID";
		}

		#endregion
    }
}
