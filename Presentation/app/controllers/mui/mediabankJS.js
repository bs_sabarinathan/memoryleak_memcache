///#source 1 1 /app/controllers/mui/mediabank-controller.js
(function (ng, app) {
    "use strict";

    function muimediabankCtrl($window, $location, $timeout, $scope, $cookies, $resource, $rootScope, $translate, MediabankService, $compile, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/")
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            TenantFilePath1 = cloudpath;
            imagesrcpath = cloudpath;
        }
        $scope.PageNo = 1;
        $scope.MediaAttributeOptions = {
            "Attributes": []
        };
        $scope.MediaAssetTypes = {
            "AssetTypes": []
        };
        $scope.isLibraryExpanded = false;
        $scope.SelectedTagWords = [];
        $scope.totalRecords = 0;
        $scope.damattributeshow = false;
        $scope.clearTagwords = false;
        $scope.RowsPerPage = 18;
        $scope.TaskStatusObj = [{
            "Name": "Thumbnail",
            ID: 1
        }, {
            "Name": "Summary",
            ID: 2
        }, {
            "Name": "List",
            ID: 3
        }];
        $scope.SettingsDamAttributes = {};
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Published date (Ascending)",
            ID: 3
        }, {
            "Name": "Published date (Descending)",
            ID: 4
        }, {
            "Name": "Created On(Ascending)",
            ID: 5
        }, {
            "Name": "Created On(Descending)",
            ID: 6
        }, {
            "Name": "Last updated date On(Ascending)",
            ID: 7
        }, {
            "Name": "Last updated date On(Descending)",
            ID: 8
        }];
        $scope.OrderByObjChange = [{
            "Name": $translate.instant('LanguageContents.Res_1.Caption'),
            ID: 1
        }, {
            "Name": $translate.instant('LanguageContents.Res_5556.Caption'),
            ID: 3
        }, {
            "Name": $translate.instant('LanguageContents.Res_31.Caption'),
            ID: 5
        }, {
            "Name": $translate.instant('LanguageContents.Res_5557.Caption'),
            ID: 7
        }];
        $scope.OrderByObjSelection = [{
            "Name": $translate.instant('LanguageContents.Res_5558.Caption'),
            ID: 1
        }, {
            "Name": $translate.instant('LanguageContents.Res_5559.Caption'),
            ID: 2
        }];
        $scope.FilterStatus = 1;
        $scope.DamViewName = "Thumbnail";
        $scope.OrderbyName = $translate.instant('LanguageContents.Res_5559.Caption');
        $scope.OrderbyNameChange = $translate.instant('LanguageContents.Res_5556.Caption');
        $scope.callbackid = 0;
        $scope.OrderBy = 4;
        $scope.ProgressMsgHeader = "";
        $scope.ProgressContent = "";
        $scope.FilterByOptionChange = function (id) {
            if (id != $scope.FilterStatus) {
                $scope.PageNo = 1;
                $scope.FilterStatus = id;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.DamViewName = res[0].Name;
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }
        }
        //$(document).on('click', '.checkbox-custom > input[id=SelectAllAssets]', function (e) {
        //    var status = this.checked;
        //    $('#MediabankAssetListViewTable > tbody input:checkbox').each(function () {
        //        this.checked = status;
        //        if (status) {
        //            $(this).next('i').addClass('checked');
        //        } else {
        //            $(this).next('i').removeClass('checked');
        //        }
        //    });
        //});

        function RemoveCheckBoxSelection() {
            $('#MediabankAssetListViewTable > tbody input:checkbox').each(function () {
                $(this).next('i').removeClass('checked');
                $(this).prop('checked', false);
            });
            $("#SelectAllAssets").next().removeClass('checked');
        }
        $scope.OrderbyOptionChange = function (orderid) {
            if (orderid != $scope.OrderBy) {
                $scope.PageNo = 1;
                $scope.OrderBy = orderid;
                var orderobj = $.grep($scope.OrderByObjChange, function (e) {
                    return e.ID == orderid;
                });
                $scope.OrderbyNameChange = orderobj[0].Name;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus;
                });
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }
        }
        $scope.OrderByObjSelect = function (orderid) {
            var orderbyId = orderid;
            if ($scope.OrderBy == 1 || $scope.OrderBy == 3 || $scope.OrderBy == 5 || $scope.OrderBy == 7) {
                if (orderbyId == 1) {
                    var orderobj = $.grep($scope.OrderByObjSelection, function (e) {
                        return e.ID == orderid;
                    });
                    $scope.OrderbyName = orderobj[0].Name;
                    return false;
                } else {
                    $scope.OrderBy = $scope.OrderBy + 1;
                    var orderobj = $.grep($scope.OrderByObjSelection, function (e) {
                        return e.ID == orderid;
                    });
                    $scope.OrderbyName = orderobj[0].Name;
                    $timeout(function () { GetFilteredAssetsData(); }, 200);
                }
            } else {
                if ($scope.OrderBy == 1 || $scope.OrderBy == 3 || $scope.OrderBy == 5 || $scope.OrderBy == 7) {
                    $scope.OrderBy = $scope.OrderBy + 1;
                    var orderobj = $.grep($scope.OrderByObjSelection, function (e) {
                        return e.ID == orderid;
                    });
                    $scope.OrderbyName = orderobj[0].Name;
                    $timeout(function () { GetFilteredAssetsData(); }, 200);
                } else {
                    $scope.OrderBy = $scope.OrderBy - 1;
                    var orderobj = $.grep($scope.OrderByObjSelection, function (e) {
                        return e.ID == orderid;
                    });
                    $scope.OrderbyName = orderobj[0].Name;
                    $timeout(function () { GetFilteredAssetsData(); }, 200);
                }
            }
        }
        $scope.MediaBankAssetTypes = [];
        $scope.selectedAssettypes = [];
        $scope.selectedAttributes = [];
        $scope.FilterSchema = "";
        $scope.FilterSchemaArr = [];
        $timeout(function () { PageLoad(); }, 10);

        function PageLoad() {
            $scope.RowsPerPage = (Math.floor($(".mediabankmainUIcontent").width() / 176) * 3);
            $scope.subview = "Thumbnail";
            $scope.PageNo = 1;
            GetAllExtensionTypesforDAM();
            GetMediaBankFilterAttributes();
        }

        function GetAllExtensionTypesforDAM() {
            MediabankService.GetAllExtensionTypesforDAM().then(function (data) {
                $scope.MediaBankAssetTypes = data.Response;
                GetDAMViewSettings();
                GetDAMToolTipSettings();
            });
        }

        function GetMediaBankFilterAttributes() {
            MediabankService.GetCustomFilterAttributes(4).then(function (data) {
                var res = data.Response;
                if (res.m_Item2 == 1) {
                    $scope.isLibraryExpanded = true;
                }
                if (res.m_Item1 != undefined) $scope.damattributeshow = true;
                LoadAssetAttributes(res.m_Item1);
            });
        }
        $scope.FilterbyAttributeSelection = function (event, attrid, attrtype, optionid, attrlevel) {
            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) {
                    return e.AttributeID == attrid;
                });
                if (remainRecord.length == 0) {
                    if (attrtype != 7) {
                        $scope.selectedAttributes.push({
                            "Condition": 0,
                            "AttributeID": attrid,
                            "AttributeTypeID": attrtype,
                            "AttributeLevel": attrlevel,
                            "AttributeCaption": $(checkbox).attr('attributecaption'),
                            "Operator": "IN",
                            "Values": ['' + $(checkbox).attr('optioncaption').toString() + ''],
                            "ValuesIDArr": [optionid]
                        });
                    }
                    else {
                        $scope.selectedAttributes.push({
                            "Condition": 0,
                            "AttributeID": attrid,
                            "AttributeTypeID": attrtype,
                            "AttributeLevel": attrlevel,
                            "AttributeCaption": event.target.AttrType,
                            "Operator": "IN",
                            "Values": ['' + event.target.Caption.toString() + ''],
                            "ValuesIDArr": [optionid]
                        });
                    }

                } else {
                    var valueRecords = [],
						valueIds = [];
                    if (attrtype != 7) {
                        valueRecords = $.grep(remainRecord[0].Values, function (e) {
                            return e == $(checkbox).attr('optioncaption');
                        });
                    }
                    else {
                        valueRecords = $.grep(remainRecord[0].Values, function (e) {
                            return e == event.target.Caption;
                        });
                    }

                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) {
                        return e == optionid;
                    });
                    if (attrtype != 7) {
                        if (valueRecords.length == 0) {
                            remainRecord[0].Values.push($(checkbox).attr('optioncaption'));
                        }
                    }
                    else {
                        if (valueRecords.length == 0) {
                            remainRecord[0].Values.push(event.target.Caption);
                        }
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) {
                    return e.AttributeID == attrid;
                });
                if (remainRecord.length > 0) {
                    var valueRecords = [],
						valueIds = [];
                    if (attrtype != 7) {
                        valueRecords = $.grep(remainRecord[0].Values, function (e) {
                            return e == $(checkbox).attr('optioncaption');
                        });
                    }
                    else {
                        valueRecords = $.grep(remainRecord[0].Values, function (e) {
                            return e == event.target.Caption;
                        });
                    }


                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) {
                        return e == optionid;
                    });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            $scope.PageNo = 1;
            $timeout(function () { GetFilteredAssetsData(); }, 200);
        }
        $scope.RemoveCustomAttributefromFilter = function (attrid, valueCaption) {
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) {
                return e.AttributeID == attrid;
            });
            if (remainRecord.length > 0) {
                var valueRecords = [],
					valueIds = [];
                valueRecords = $.grep(remainRecord[0].Values, function (e) {
                    return e == valueCaption;
                });
                remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                if (remainRecord[0].Values.length == 0) {
                    $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                }
                var attrValue = {};
                attrValue = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) {
                    return e.Caption == valueCaption;
                })[0];
                if (attrValue != undefined) {
                    attrValue["Isselectd"] = false;
                    var valueIds = [];
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) {
                        return e == attrValue.Id;
                    });
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                }
                $scope.PageNo = 1;
                $timeout(function ()
                { GetFilteredAssetsData(); }, 100);
            }
        }

        function LoadAssetAttributes(res) {
            $scope.treeNodeSelectedHolder = [];
            var apple_selected, tree, treedata_avm, treedata_geography;
            $scope.treesrcdirec = {};
            $scope.my_tree = tree = {};
            $scope.treesrcdirec = {};
            $scope.my_tree = tree = {};
            $scope.treePreviewObj = {};

            $scope.MediaAttributeOptions.Attributes = res;
            $("#media-customsearch-AssetAttributes").html("");
            var html = "";
            for (var i = 0, type; type = res[i++];) {
                if (type.AttributeTypeID != 17 && type.AttributeTypeID != 7) {

                    $scope.MediaAttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += ' <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : MediaAttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    if (type.Options != null) {
                        if (type.Options.length > 0) {
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;
                            }
                            $scope.MediaAttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.MediaAttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAttributeOptions.FilterExpand_' + type.AttributeID + '"> ';
                            html += '  <input class="searchBy" ng-model="MediaAttributeOptions.FilterText_' + type.AttributeID + '" type="text" placeholder="Search by ' + type.AttributeCaption + '" /> ';
                            html += '  <ul>';
                            html += ' <li ng-repeat="item in MediaAttributeOptions.FilterOption_' + type.AttributeID + ' | filter:MediaAttributeOptions.FilterText_' + type.AttributeID + ' " "> ';
                            html += '  <label class="checkbox checkbox-custom "><input data-ng-model="item.Isselectd" attributecaption="{{MediaAttributeOptions.FilterProp_' + type.AttributeID + '.AttributeCaption}}" optioncaption="{{item.Caption}}" ng-click=\"FilterbyAttributeSelection($event,' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeID + ',' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeTypeID + ',item.Id,' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].Level + ')\" type="checkbox"><i ng-class="MediaoptionClass(' + type.AttributeID + ',item.Id)"></i>{{item.Caption}}</label>';
                            html += ' </li> ';
                            html += ' </ul>';
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                } else if (type.AttributeTypeID == 17) {
                    $scope.MediaAttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : MediaAttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    if (type.Options != null) {
                        if (type.Options.length > 0) {
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;
                            }
                            $scope.MediaAttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.MediaAttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAttributeOptions.FilterExpand_' + type.AttributeID + '"> ';
                            $scope.tempscope = [];
                            html += "<div class=\"tagwordsDiv\">";
                            html += "<directive-tagwords item-attrid = \"" + type.AttributeID + "\" item-cleartag = \"clearTagwords\" searchfrtag=\"alert(attrid,optionid,isselected)\"  item-show-hide-progress =\"null\" item-tagword-id=\"SelectedTagWords\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                            html += "</div>";
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                } else if (type.AttributeTypeID == 7) {

                    $scope.MediaAttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += ' <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : MediaAttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    var typeoptions = JSON.parse(type.Options).Children;
                    if (typeoptions != null) {

                        if (typeoptions.length > 0) {
                            $scope.MediaAttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.MediaAttributeOptions["FilterOption_" + type.AttributeID + ""] = typeoptions;
                            $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""] = type;

                            $scope.treesrcdirec["FilterText_" + type.AttributeID] = JSON.parse(type.Options).Children;
                            $scope.treesrcdirec.attributeCaption = typeoptions[0].Caption;
                            if ($scope.treesrcdirec["FilterText_" + type.AttributeID].length > 0) {
                                if (IsNotEmptyTree($scope.treesrcdirec["FilterText_" + type.AttributeID])) {
                                    $scope.treePreviewObj["FilterText_" + type.AttributeID] = true;
                                } else $scope.treePreviewObj["FilterText_" + type.AttributeID] = false;
                            } else {
                                $scope.treePreviewObj["FilterText_" + type.AttributeID] = false;
                            }
                            html += '<div class="controls treeNode-controls" ng-show="MediaAttributeOptions.FilterExpand_' + type.AttributeID + '">';
                            //html += '<div class="input-group inlineBlock treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + type.AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + type.AttributeID + '"></div>';
                            html += '<div class="input-group inlineBlock treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + type.AttributeID + '" placeholder="Search"></div>';
                            //html += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + type.AttributeID + '">';
                            html += '<div id="treeNodeSearchDropdown_Attr_' + type.AttributeID + '">';
                            html += '<span ng-if="doing_async">...loading...</span>';
                            html += '<abn-tree-mediabank tree-filter="filterValue_' + type.AttributeID + '" tree-data=\"treesrcdirec.FilterText_' + type.AttributeID + '\" accessable="true" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree-mediabank><i ng-class="MediaoptionClass(' + type.AttributeID + ',item.Id)">';
                            html += '</div><span class=\"signInfo margin-left20x\"></span></div></div>';
                            html += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + type.AttributeID + '\">';
                            html += '<div class="controls">';
                            html += '<eu-tree tree-data=\"treesrcdirec.FilterText_' + type.AttributeID + '\" node-attributeid="' + type.AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            html += '</div></div>';



                        }
                    }
                    html += " </div>";
                }

                html += ' <hr class="sr-dv-filtergroupSeparator" />';
            }
            $("#media-customsearch-AssetAttributes").html($compile(html)($scope));
        }


        function IsNotEmptyTree(treeObj) {
            var treeTextVisbileflag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowOrHideAttributeToAttributeRelation(branch.AttributeId + "_0", 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }


        $scope.alert = function (attrid, optionid, isselected) {
            if (isselected) {
                $scope.pageno = 1;
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) {
                    return e.AttributeID == attrid;
                });
                var optionCaption = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) {
                    return e.Id == optionid
                })[0].Caption;
                var attrCaption = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeCaption;
                var attrType = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeTypeID;
                if (remainRecord.length == 0) {
                    $scope.selectedAttributes.push({
                        "Condition": 0,
                        "AttributeID": attrid,
                        "AttributeTypeID": attrType,
                        "AttributeLevel": 0,
                        "AttributeCaption": attrCaption,
                        "Operator": "IN",
                        "Values": ['' + optionCaption.toString() + ''],
                        "ValuesIDArr": [optionid]
                    });
                } else {
                    var valueRecords = [],
						valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) {
                        return e == optionCaption;
                    });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) {
                        return e == optionid;
                    });
                    if (valueRecords.length == 0) {
                        remainRecord[0].Values.push(optionCaption);
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            } else {
                var remainRecord = [];
                $scope.pageno = 1;
                remainRecord = $.grep($scope.selectedAttributes, function (e) {
                    return e.AttributeID == attrid;
                });
                if (remainRecord.length > 0) {
                    var valueRecords = [],
						valueIds = [];
                    var optionCaption = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) {
                        return e.Id == optionid
                    })[0].Caption;
                    valueRecords = $.grep(remainRecord[0].Values, function (e) {
                        return e == optionCaption;
                    });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) {
                        return e == optionid;
                    });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            $scope.PageNo = 1;
            $timeout(function () { GetFilteredAssetsData(); }, 200);
        }
        $scope.RemoveSearchType = function (id) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAssettypes, function (e) {
                return e.Id == id;
            })[0];
            if (selctionRecord != undefined) {
                $scope.selectedAssettypes.splice($.inArray(selctionRecord, $scope.selectedAssettypes), 1);
                $scope.PageNo = 1;
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }
        }
        $scope.makeexpandcollapse = function (id) {
            $scope.MediaAttributeOptions["FilterExpand_" + id + ""] = !$scope.MediaAttributeOptions["FilterExpand_" + id + ""];
        }
        $scope.PerformClear = function (id) {
            $scope.MediaAttributeOptions["FilterText_" + id + ""] = "";
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) {
                return e.AttributeID == id;
            });
            if (remainRecord.length > 0) {
                $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
            }
            var selctionRecord = [];
            selctionRecord = $.grep($scope.MediaAttributeOptions.Attributes, function (e) {
                return e.AttributeID == id;
            })[0];
            if (selctionRecord != undefined) {
                for (var j = 0, attr; attr = selctionRecord.Options[j++];) {
                    attr["Isselectd"] = false;
                }
            }
            if (remainRecord[0].AttributeTypeID == 17) {
                $timeout(function () {
                    $scope.clearTagwords = true;
                }, 100);
            }
            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0) $timeout(function () { GetFilteredAssetsData(); }, 200);
            else LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);
        }
        $scope.MediaoptionClass = function (attrid, optionid) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAttributes, function (e) {
                return e.AttributeID == attrid;
            });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0) {
                    var valueIds = [];
                    valueIds = $.grep(selctionRecord[0].ValuesIDArr, function (e) {
                        return e == optionid;
                    });
                    if (valueIds != undefined) if (valueIds.length > 0) return "checkbox checked";
                    else return "checkbox";
                    else return "checkbox";
                }
            }
            return "checkbox";
        }

        function GetDAMViewSettings() {
            MediabankService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsDamAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsDamAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SettingsDamAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                    LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);
                }
            });
        }

        function GetDAMToolTipSettings() {
            MediabankService.GetDAMToolTipSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsDamAttributes["ThumbnailToolTipSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsDamAttributes["ListViewToolTipSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }

        function GetAssetsData(filter, orderby, pageno) {
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0) assetypes = $scope.selectedAssettypes;
            else assetypes = $scope.MediaBankAssetTypes.m_Item1;
            var mediaAssettypes = new Array();
            mediaAssettypes = $.grep($scope.MediaBankAssetTypes.m_Item1, function (e) {
                return e.Id;
            });
            var criteria = GetXML();
            var AssetData = {};
            AssetData.View = filter;
            AssetData.orderby = orderby;
            AssetData.pageNo = pageno;
            AssetData.assetTypesArr = mediaAssettypes;
            AssetData.filterschema = criteria;
            AssetData.totalRecords = $scope.RowsPerPage;
            MediabankService.GetMediaAssets(AssetData).then(function (data) {
                var res = [];
                res = data.Response;
                var assets = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                if (res != null) {
                    assets = res[0].AssetFiles;
                    for (var j = 0, asset; asset = assets[j++];) {
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                if ($scope.FilterStatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus == 3) LoadListView(res);
                $timeout(function () {
                    if ($scope.PageNo < 5) {
                        _frequentLoadInitialSearchBlock();
                    }
                }, 50);
            });
        }

        function _getNumberofItemsPerRow() {
            var _itemPerRow = (Math.floor($(".mediabankmainUIcontent").width() / 176));
            var _itemsLoaded = $(".mediabankcontainerbox").length;
            if ((_itemsLoaded % _itemPerRow) > 0) {
                $scope.RowsPerPage = _itemPerRow - (_itemsLoaded % _itemPerRow) + (_itemPerRow * 3);
            } else $scope.RowsPerPage = (_itemPerRow * 3);
        }

        function GetFilteredAssetsData() {
            var criteria = GetXML();
            _getNumberofItemsPerRow();
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0) assetypes = $scope.selectedAssettypes;
            else assetypes = $scope.MediaBankAssetTypes.m_Item1;
            var AssetData = {};
            AssetData.View = $scope.FilterStatus;
            AssetData.orderby = $scope.OrderBy;
            AssetData.pageNo = $scope.PageNo;
            AssetData.assetTypesArr = assetypes;
            AssetData.filterschema = criteria;
            AssetData.totalRecords = $scope.RowsPerPage;
            MediabankService.GetMediaAssets(AssetData).then(function (data) {
                var res = [];
                res = data.Response;
                var assets = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                if (res != null) {
                    assets = res[0].AssetFiles;
                    if (assets.length == 0) {
                        $scope.isResultEmpty = true;
                        $(".noResultFound").text("Your search did not match any item.");
                    } else {
                        $scope.isResultEmpty = false;
                    }
                    for (var j = 0, asset; asset = assets[j++];) {
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                $scope.PageNo = 1;
                if ($scope.FilterStatus == 1) {
                    $scope.PageNo = 1;
                    LoadThumbnailView(res);
                } else if ($scope.FilterStatus == 2) {
                    $scope.PageNo = 1;
                    LoadSummaryView(res);
                } else if ($scope.FilterStatus == 3) {
                    $scope.PageNo = 1;
                    LoadListView(res);
                }
                $timeout(function () { }, 100);
            });
        }
        $(window).resize(function () {
            _getNumberofItemsPerRow();
        });

        function _frequentLoadInitialSearchBlock() {
            $timeout(function () {
                if ($scope.PageNo < 5) {
                    checkListItemContents();
                }
            }, 50);
        }

        function LoadAssetsData(filter, orderby, pageno) {
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0) assetypes = $scope.selectedAssettypes;
            else assetypes = $scope.MediaBankAssetTypes.m_Item1;
            var mediaAssettypes = new Array();
            mediaAssettypes = $.grep($scope.MediaBankAssetTypes.m_Item1, function (e) {
                return e.Id;
            });
            var criteria = GetXML();
            _getNumberofItemsPerRow();
            LoadAssettypes();
            var AssetData = {};
            AssetData.View = filter;
            AssetData.orderby = orderby;
            AssetData.pageNo = pageno;
            AssetData.assetTypesArr = mediaAssettypes;
            AssetData.filterschema = criteria;
            AssetData.totalRecords = $scope.RowsPerPage;
            MediabankService.GetMediaAssets(AssetData).then(function (data) {
                var res = [];
                res = data.Response;
                var assets = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                if (res != null) {
                    assets = res[0].AssetFiles;
                    for (var j = 0, asset; asset = assets[j++];) {
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                if ($scope.FilterStatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus == 3) LoadListView(res);
                loadScrollSettings();
            });
        }
        $scope.makeAssettypeexpandcollapse = function () {
            $scope.MediaAssetTypes["FilterExpand"] = !$scope.MediaAssetTypes["FilterExpand"];
        }
        $scope.MediaTypeoptionClass = function (id) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAssettypes, function (e) {
                return e.Id == id;
            });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0) return "checkbox checked";
                else return "checkbox";
            }
            return "checkbox";
        }
        $scope.MediaTypePerformClear = function () {
            $scope.MediaAttributeOptions["FilterText"] = "";
            var remainRecord = [];
            $scope.selectedAssettypes = [];
            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0) $timeout(function () { GetFilteredAssetsData(); }, 200);
            else LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);
        }

        function LoadAssettypes() {
            $scope.MediaAssetTypes.AssetTypes = $scope.MediaBankAssetTypes.m_Item1;
            $("#damui-Assettypes").html("");
            var html = "";
            $scope.MediaAssetTypes["FilterExpand"] = true;
            html += ' <div class="sr-dv-filterGroup">';
            html += ' <div class="sr-dv-filterGroupHeader"> ';
            html += '                       <h4 ng-click=\'makeAssettypeexpandcollapse()\'>' + $translate.instant('LanguageContents.Res_2507.Caption') + '</h4> ';
            html += '                       <span class="clearfilter"> ';
            html += '                            <span>|</span> ';
            html += '                            <a href="javascript:void(0);" ng-click="MediaTypePerformClear()">' + $translate.instant('LanguageContents.Res_414.Caption') + '</a> ';
            html += '                        </span> ';
            html += "                        <i ng-click=\'makeAssettypeexpandcollapse()\' ng-class=\'{\"icon-collapse-alt\" :MediaAttributeOptions.FilterExpand,\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand}\'></i> ";
            html += '</div> ';
            if ($scope.MediaBankAssetTypes.m_Item1 != null) {
                if ($scope.MediaBankAssetTypes.m_Item1.length > 0) {
                    for (var l = 0, type; type = $scope.MediaBankAssetTypes.m_Item1[l++];) {
                        type["Isselectd"] = false;
                    }
                    $scope.MediaAssetTypes["FilterText"] = "";
                    $scope.MediaAssetTypes["AssetTypeFilterOption"] = $scope.MediaBankAssetTypes.m_Item1;
                    html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAssetTypes.FilterExpand"> ';
                    html += '  <input class="searchBy" ng-model="MediaAssetTypes.FilterText" type="text" placeholder="Search by Asset type" /> ';
                    html += '  <ul>';
                    html += ' <li ng-repeat="item in MediaAssetTypes.AssetTypeFilterOption | filter:MediaAssetTypes.FilterText"> ';
                    html += '  <label class="checkbox checkbox-custom "><input data-ng-model="item.Isselectd" ng-click=\"FilterbyAssettype($event,item.Id,item.damCaption)\" type="checkbox"><i ng-class="MediaTypeoptionClass(item.Id)" class="cutomfiltercheck"></i>{{item.damCaption}}</label>';
                    html += ' </li> ';
                    html += ' </ul>';
                    html += '</div>';
                }
            }
            html += " </div>";
            html += ' <hr class="sr-dv-filtergroupSeparator" />';
            $("#damui-Assettypes").html($compile(html)($scope));
        }
        var isfilterRequest = true;
        $scope.assettypefiltercheckbox = false;
        $scope.FilterbyAssettype = function (event, typeid, name) {
            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) {
                    return e.Id == typeid;
                });
                if (remainRecord.length == 0) {
                    $scope.selectedAssettypes.push({
                        "Id": typeid,
                        "Name": name
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) {
                    return e.Id == typeid;
                });
                if (remainRecord.length > 0) {
                    $scope.selectedAssettypes.splice($.inArray(remainRecord[0], $scope.selectedAssettypes), 1);
                }
            }
            $scope.PageNo = 1;
            $timeout(function () { GetFilteredAssetsData(); }, 200);
        }

        function GetXML() {
            var xmlData = '<?xml version="1.0" encoding="utf-8"?>';
            xmlData += '<CustomList>';
            xmlData += "<Criterias>";
            for (var c = 0, where; where = $scope.selectedAttributes[c++];) {
                var values = "";
                if (where.Values.length > 0) {
                    for (var v = 0, data; data = where.Values[v++];) {
                        if (values.length > 0) {
                            values = values + ",'" + data.toString() + "'";
                        } else {
                            values = values + "'" + data.toString() + "'";
                        }
                    }
                }
                xmlData += "<Criteria Condition=\"" + where.Condition + "\" AttributeID=\"" + where.AttributeID + "\" AttributeTypeID=\"" + where.AttributeTypeID + "\" AttributeLevel=\"" + where.AttributeLevel + "\" AttributeCaption=\"" + where.AttributeCaption + "\" Operator=\"" + where.Operator + "\" Value=\"" + values + "\"/>";
            }
            xmlData += "</Criterias>";
            xmlData += '</CustomList>';
            return xmlData;
        }

        function getMoreListItems() {
            $scope.PageNo = $scope.PageNo + 1;
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0) assetypes = $scope.selectedAssettypes;
            else assetypes = $scope.MediaBankAssetTypes.m_Item1;
            var mediaAssettypes = new Array();
            mediaAssettypes = $.grep($scope.MediaBankAssetTypes.m_Item1, function (e) {
                return e.Id;
            });
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0) assetypes = $scope.selectedAssettypes;
            else assetypes = $scope.MediaBankAssetTypes.m_Item1;
            var criteria = GetXML();
            var AssetData = {};
            AssetData.View = $scope.FilterStatus;
            AssetData.orderby = $scope.OrderBy;
            AssetData.pageNo = $scope.PageNo;
            AssetData.assetTypesArr = assetypes;
            AssetData.filterschema = criteria;
            AssetData.totalRecords = $scope.RowsPerPage;
            MediabankService.GetMediaAssets(AssetData).then(function (data) {
                var res = [];
                loading = false;
                res = data.Response;
                if (data.Response != null) {
                    if (res[0].AssetFiles.length > 0) {
                        if ($scope.FilterStatus == 1) LoadThumbnailView(res);
                        else if ($scope.FilterStatus == 2) LoadSummaryView(res);
                        else if ($scope.FilterStatus == 3) LoadListView(res);
                    }
                }
            });
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }
        var loading = false;

        function loadScrollSettings() {
            $(window).scroll(function () {
                if (loading) {
                    return;
                }
                if ($(window).scrollTop() > $(document).height() - $(window).height() - 500) {
                    loading = true;
                    checkListItemContents();
                }
            });
        }
        $scope.AssetSelectionFiles = [];
        $scope.AssetChkClass = {};
        $scope.AssetSelectionClass = {};
        $scope.AssetFiles = [];
        var DynamicData = [];

        function refreshViewObj() {
            $(window).scrollTop(0);
            var html = '';
            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-summary").html(html);
            $("#MediaBank-dv-listview").html(html);
            $scope.AssetSelection = {};
            $scope.AssetChkClass = {};
            $scope.AssetSelectionClass = {};
            $scope.AssetFiles = [];
            $scope.AssetDynamicData = [];
        }

        function IsAssetSelected(assetid) {
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                return e.AssetId == assetid;
            });
            if (remainRecord.length > 0) {
                return true;
            }
            return false;
        }

        function LoadThumbnailView(res) {
            var html = '';
            $("#MediaBank-dv-summary").html(html);
            $("#MediaBank-dv-listview").html(html);
            if ($scope.PageNo == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if (assets.length > 0) {
                    for (var i = 0, asset; asset = assets[i++];) {
                        if ($scope.PageNo == 1 && i == 1) {
                            var ht_Count = "hit";
                            $scope.totalRecords = asset.Total_COUNT;
                            if ($scope.totalRecords > 1) ht_Count = "hits";
                            else ht_Count = "hit";
                            $(".mediabanksearchresult #searchPartCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                        }
                        $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        var assetselection = "";
                        if (IsAssetSelected(asset.AssetUniqueID)) {
                            assetselection = "th-Selection selected";
                            $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                        } else {
                            assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                            $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                        }
                        $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                        html += '<div id="AssetBlockId_' + asset.AssetUniqueID + '" class=\"th-Box mediabankcontainerbox\" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                        html += '<div ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                        html += '<div class="th-chkBx">';
                        html += '<i class="icon-stop"></i>';
                        html += '<label class="checkbox checkbox-custom">';
                        html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                        html += '<i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                        html += '</label>';
                        html += '</div>';
                        html += '<div class="th-options">';
                        html += '<i class="icon-stop"></i>';
                        html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i>';
                        html += '</div>';
                        html += '<div class="th-Block">';
                        html += '<div class="th-ImgBlock">';
                        html += '<div  class="th-ImgContainer">';
                        if (asset.Category == 0) {
                            if (asset.Extension != null) {
                                if (asset.ProcessType > 0) {
                                    if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                                    else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                    else if (asset.Status == 3) {
                                        html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                    }
                                } else {
                                    html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                }
                            } else html += '<img src="' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        } else if (asset.Category == 1) {
                            html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        } else if (asset.Category == 2) {
                            html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="th-DetailBlock">';
                        html += '<div class="th-ActionButtonContainer">';
                        html += '<span>';
                        html += '<i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                        html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                        html += '</span>';
                        html += '</div>';
                        html += '<div class="th-detail-eIconContainer">';
                        html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                        html += '</div>';
                        html += '<div class="th-Details">';
                        html += GenereteDetailBlock(asset);
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    $("#MediaBank-dv-thumbnail").append($compile(html)($scope));
                } else {
                    if ($scope.PageNo == 1) {
                        $scope.totalRecords = 0;
                        $(".mediabanksearchresult #searchPartCaption").text("Total 0 hit");
                        var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                        $("#MediaBank-dv-thumbnail").html(emptyHtml);
                    }
                }
            } else {
                if ($scope.PageNo == 1) {
                    $scope.totalRecords = 0;
                    $(".mediabanksearchresult #searchPartCaption").text("Total 0 hit");
                    var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                    $("#MediaBank-dv-thumbnail").html(emptyHtml);
                }
            }
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            var tempobj1 = $.grep($scope.AssetFiles, function (e) {
                return e.AssetID == assetid;
            });
            if (tempobj1 != null)
                tempobj1[0].Checked = checkbox.checked;
            if (checkbox.checked) {
                var tempobj = $.grep($scope.AssetFiles, function (e) {
                    return e.Checked == true;
                });
                if (tempobj != null) {
                    if (tempobj.length == $scope.AssetFiles.length) {
                        $("#SelectAllAssets").parent().find("i").addClass("checked");
                    }
                }
                else { $("#SelectAllAssets").parent().find("i").removeClass("checked"); }
            }
            else { $("#SelectAllAssets").parent().find("i").removeClass("checked"); }
            if ($scope.FilterStatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            else if ($scope.FilterStatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            else if ($scope.FilterStatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid);
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    if (res[0] != undefined) $scope.AssetSelectionFiles.push({
                        "AssetId": assetid
                    });
                    else $scope.AssetSelectionFiles.push({
                        "AssetId": assetid
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetSelectionFiles), 1);
                }
            }
        }
        $scope.DownloadFileObj = [];

        function DownloadAssets(checkbox, assetid, name, ext, guid) {
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.push({
                        AssetId: assetid,
                        FileName: name,
                        Fileguid: guid,
                        Extension: ext
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.splice($.inArray(remainRecord[0], $scope.DownloadFileObj), 1);
                }
            }
        }
        $scope.download = function () {
            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                var asset = [],
					data = [],
					dwnData = [];
                asset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid.AssetId;
                });
                dwnData = $.grep($scope.DownloadFileObj, function (e) {
                    return e.AssetId == assetid.AssetId;
                });
                if (dwnData.length == 0) $scope.DownloadFileObj.push({
                    AssetId: assetid.AssetId,
                    FileName: asset[0].FileName,
                    Fileguid: asset[0].FileGuid,
                    Extension: asset[0].Extension
                });
            }
            $scope.ProgressMsgHeader = "Downloading Assets";
            if ($scope.DownloadFileObj.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4610.Caption'));
            }
            if ($scope.DownloadFileObj.length == 1) {
                var arr = $scope.DownloadFileObj;
                var ID = 0;
                if ($scope.DownloadFileObj[0].AssetId != undefined) {
                    ID = $scope.DownloadFileObj[0].AssetId;
                }
                $timeout(function () {
                    var fileid = $scope.DownloadFileObj[0].Fileguid,
						extn = $scope.DownloadFileObj[0].Extension == '.zip' ? 'zip' : $scope.DownloadFileObj[0].Extension;
                    var filename = $scope.DownloadFileObj[0].FileName;
                    var actualfilename = "";
                    for (var i = 0; i < filename.length; i++) {
                        if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                            actualfilename = actualfilename + (filename[i]);
                        }
                    }
                    blockUIForDownload();
                    location.href = 'DAMDownload.aspx?FileID=' + $scope.DownloadFileObj[0].Fileguid + '&FileFriendlyName=' + actualfilename + '&Ext=' + extn + '&token=' + $('#mediasdownload_token_value_id').val();
                    refreshDownLoadScope(arr);
                    $scope.DownloadFileObj = [];
                    clearassetSelection();
                }, 100);
            } else if ($scope.DownloadFileObj.length > 1) {
                var arr = $scope.DownloadFileObj;
                $timeout(function () {
                    MediabankService.DownloadFiles(arr).then(function (result) {
                        if (result.Response == null) {
                            NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                        } else {
                            $scope.DownloadFileObj = [];
                            var fileid = result.Response,
								extn = '.zip';
                            var filename = result.Response + extn;
                            blockUIForDownload();
                            location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#mediasdownload_token_value_id').val();
                            refreshDownLoadScope(arr);
                            clearassetSelection();
                        }
                    });
                }, 100);
            }
        }
        var fileDownloadCheckTimer;

        function blockUIForDownload() {
            var token = new Date().getTime();
            $('#mediasdownload_token_value_id').val(token);
            $scope.ProgressContent = "Downloading";
            $('#mediasloadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                var cookieValue = $.cookie('fileDownloadToken');
                if (cookieValue == token) finishDownload();
            }, 1000);
        }

        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken');
            $('#mediasloadingDownloadPageModel').modal("hide");
        }

        function refreshDownLoadScope(arr) {
            for (var i = 0, item; item = arr[i++];) {
                if ($scope.DamViewName == "Thumbnail") $scope.AssetSelectionClass["asset_" + item.AssetId] = "th-Selection";
                else if ($scope.DamViewName == "Summary") $scope.AssetSelectionClass["asset_" + item.AssetId] = "th-sum-Selection";
                else if ($scope.DamViewName == "List") $scope.AssetSelectionClass["asset_" + item.AssetId] = "li-Selection";
                $scope.AssetSelection["asset_" + item.AssetId] = false;
                $scope.AssetChkClass["asset_" + item.AssetId + ""] = "checkbox";
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }

        function LoadSummaryView(res) {
            var html = '';
            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-listview").html(html);
            if ($scope.PageNo == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if (assets.length > 0) {
                    for (var i = 0, asset; asset = assets[i++];) {
                        if ($scope.PageNo == 1 && i == 1) {
                            var ht_Count = "hit";
                            $scope.totalRecords = asset.Total_COUNT;
                            if ($scope.totalRecords > 1) ht_Count = "hits";
                            else ht_Count = "hit";
                            $(".mediabanksearchresult #searchPartCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                        }
                        $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        var assetselection = "";
                        if (IsAssetSelected(asset.AssetUniqueID)) {
                            assetselection = "th-sum-Selection selected";
                            $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                        } else {
                            assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                            $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                        }
                        $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                        html += '<div id="AssetBlockId_' + asset.AssetUniqueID + '" class="th-sum-Box mediabankcontainerbox"> ';
                        html += '        <div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                        html += '            <div class="th-sum-chkBx"> ';
                        html += '   <i class="icon-stop"></i> ';
                        html += '   <label class="checkbox checkbox-custom"> ';
                        html += '   <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + i + '" type="checkbox" /> ';
                        html += '   <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                        html += '   </label> ';
                        html += '            </div> ';
                        html += '            <div class="th-sum-Block"> ';
                        html += '            <div class="th-sum-options">';
                        html += '                <i class="icon-stop"></i>';
                        html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i>';
                        html += '            </div>';
                        html += '   <div class="th-sum-leftSection"> ';
                        html += '   <div class="th-sum-ImgBlock"> ';
                        html += '       <div class="th-sum-ImgContainer"> ';
                        if (asset.Category == 0) {
                            if (asset.Extension != null) {
                                if (asset.ProcessType > 0) {
                                    if (asset.Status == 2) html += '<img  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg' + '" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                                    else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                    else if (asset.Status == 3) {
                                        html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                    }
                                } else {
                                    html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                }
                            } else {
                                html += '<img src="' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        } else if (asset.Category == 1) {
                            html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        } else if (asset.Category == 2) {
                            html += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }
                        html += '       </div> ';
                        html += '   </div> ';
                        html += '   <div class="th-sum-baiscDetailBlock"> ';
                        html += '       <div class="th-sum-ActionButtonContainer"> ';
                        html += '           <span> ';
                        html += '  <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                        html += '  <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                        html += '           </span> ';
                        html += '       </div> ';
                        html += '       <div class="th-sum-detail-eIconContainer"> ';
                        html += ' <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                        html += '       </div> ';
                        html += '       <div class="th-sum-basicDetails"> ';
                        html += GenereteSummaryDetailBlock(asset);
                        html += '       </div> ';
                        html += '   </div> ';
                        html += '   </div> ';
                        html += '   <div class="th-sum-rightSection"> ';
                        html += '   <div class="th-sum-expandedDetailBlock"> ';
                        html += '       <div class="th-sum-expandedHeader"> ';
                        html += '           <h4>' + asset.AssetName + '</h4> ';
                        html += '       </div> ';
                        html += '       <div class="th-sum-expandedDetails"> ';
                        html += '           <form class="form-horizontal"> ';
                        html += DrawSummaryBlock(asset);
                        html += '           </form> ';
                        html += '       </div> ';
                        html += '   </div> ';
                        html += '   </div> ';
                        html += '            </div> ';
                        html += '        </div> ';
                        html += '    </div> ';
                    }
                    $("#MediaBank-dv-summary").append($compile(html)($scope));
                } else {
                    if ($scope.PageNo == 1) {
                        $scope.totalRecords = 0;
                        $(".mediabanksearchresult #searchPartCaption").text("Total 0 hit");
                        var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                        $("#MediaBank-dv-summary").append($compile(html)($scope));
                    }
                }
            } else {
                if ($scope.PageNo == 1) {
                    $scope.totalRecords = 0;
                    $(".mediabanksearchresult #searchPartCaption").text("Total 0 hit");
                    var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                    $("#MediaBank-dv-summary").append($compile(html)($scope));
                }
            }
        }

        function GenereteSummaryDetailBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) if (data[0]["" + attr.ID + ""] != '1900-01-01') {
                                html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                            }
                        }
                    }
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += ' <label class="control-label">' + attr.Caption + '</label> ';
                    html += ' <div class="controls"> ';
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="control-label">-</span>';
                        } else {
                            html += '<label class="control-label">-</label> ';
                        }
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += ' </div> ';
                    html += '  </div> ';
                }
            }
            return html;
        }
        $scope.SelectAllAssetFiles = function (event) {
            var status = event.target.checked;
            $('#MediabankAssetListViewTable > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = true;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                } else {
                    $(this).next('i').removeClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = false;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                }
            });
        }

        function LoadListView(res) {
            var html = '';
            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-summary").html(html);
            if ($scope.PageNo == 1) {
                refreshViewObj();
            }
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    if ($("#SelectAllAssets").parent().find("i").hasClass("checked")) { asset["Checked"] = true; }
                    else { asset["Checked"] = false; }
                    $scope.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if ($scope.PageNo == 1) {
                    html += '<div>';
                    html += '    <table id="MediabankAssetListViewTable" class="table table-normal-left ListviewTable">';
                    html += '        <thead>';
                    html += GenerateHeader();
                    html += '        </thead>';
                    html += '        <tbody>';
                    html += GenerateList(assets);
                    html += '        </tbody>';
                    html += '    </table>';
                    html += '</div>';
                    $("#MediaBank-dv-listview").append($compile(html)($scope));
                } else {
                    html += GenerateList(assets);
                    $("#MediabankAssetListViewTable tbody").append($compile(html)($scope));
                    var a;
                    setTimeout(function () {
                        for (var j = 0, a; asset = $scope.AssetFiles[j++];) {
                            if ($("#SelectAllAssets").parent().find("i").hasClass("checked")) {
                                $("#MediabankAssetListViewTable").find("#chkBoxIcon_" + asset.AssetUniqueID).addClass("checked");
                                $scope.AssetSelectionClass["asset_" + asset.AssetID] = "li-Selection selected";
                                var temp = $.grep($scope.AssetSelectionFiles, function (e) {
                                    return e.AssetId == asset.AssetID;
                                });
                                if (temp.length == 0) $scope.AssetSelectionFiles.push({ "AssetId": asset.AssetID });
                            } else {
                                $("#MediabankAssetListViewTable").find("#chkBoxIcon_" + asset.AssetUniqueID).removeClass("checked");
                                $scope.AssetSelectionClass["asset_" + asset.AssetID] = "li-Selection";
                            }
                        }
                    }, 100);
                }
            } else {
                if ($scope.PageNo == 1) {
                    $scope.totalRecords = 0;
                    $(".mediabanksearchresult #searchPartCaption").text("Total 0 hit");
                    var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                    $("#MediaBank-dv-listview").html(emptyHtml);
                }
            }
        }

        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input id="SelectAllAssets" ng-click="SelectAllAssetFiles($event)" type="checkbox" />';
            html += '           <i class="checkbox"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                if ($scope.PageNo == 1 && j == 1) {
                    var ht_Count = "hit";
                    $scope.totalRecords = asset.Total_COUNT;
                    if ($scope.totalRecords > 1) ht_Count = "hits";
                    else ht_Count = "hit";
                    $(".mediabanksearchresult #searchPartCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                }
                $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                var assetselection = "";
                if (IsAssetSelected(asset.AssetUniqueID)) {
                    assetselection = "th-li-Selection selected";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                } else {
                    assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                }
                $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                html += '<tr id="AssetBlockId_' + asset.AssetUniqueID + '" class="mediabankcontainerbox" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + 'data-backcolor=' + asset.ColorCode + 'data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                html += '           <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <span class="thmbListviewImgSpan">';
                html += '           <div class="thmbListview-eIconContainer" > ';
                html += '               <span class="thmbListview-eIcon" ng-click="Editasset($event,' + asset.AssetUniqueID + ',' + asset.EntityID + ')" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                        html += '<td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" ng-click=\"Editasset($event,' + asset.AssetUniqueID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                        if ($scope.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetDynamicData, function (e) {
                                return e.ID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                if (data.length > 0) {
                                    if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null) if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                    else html += '<span class="thmbListviewDescSpan">-</span>';
                                    else html += '<span class="thmbListviewDescSpan">-</span>';
                                } else html += '<span class="thmbListviewDescSpan">-</span>';
                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '   <td><span class="thmbListview-options"><i id="assetlistoptionsIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i></span></td>';
                html += '</tr>';
            }
            return html;
        }
        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                MediabankService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '       <div class="lv-ImgDetailedPreview">';
                    html += '           <div class="idp-Box"> ';
                    html += '               <div class="idp-Block"> ';
                    html += '                   <div class="idp-leftSection"> ';
                    html += '                       <div class="idp-ImgBlock"> ';
                    html += '                           <div class="idp-ImgDiv"> ';
                    html += '                               <div class="idp-ImgContainer"> ';
                    if (attrList.Response.Category == 0) {
                        if ($scope.ActiveVersion.ProcessType > 0) {
                            if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg\'" />';
                            else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset($event,' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            else if ($scope.ActiveVersion.Status == 3) {
                                html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        } else {
                            html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }
                    } else if (attrList.Response.Category == 1) {
                        html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.png" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    } else if (attrList.Response.Category == 2) {
                        html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.png" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    html += '                               </div> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                       <div class="idp-baiscDetailBlock"> ';
                    html += '                           <div class="idp-ActionButtonContainer"> ';
                    html += '                               <span> ';
                    html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    html += '                               </span> ';
                    html += '                           </div> ';
                    html += '                           <div class="idp-detail-eIconContainer"> ';
                    html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '                   <div class="idp-rightSection"> ';
                    html += '                       <div class="idp-expandedDetailBlock"> ';
                    html += '                           <div class="idp-expandedDetails"> ';
                    html += '                               <form class="form-horizontal"> ';
                    try {
                        html += DrawListBlock(attrList);
                    } catch (e) { }
                    html += '                               </form> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '               </div> ';
                    html += '           </div> ';
                    html += '       </div>';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsDamAttributes["ListViewToolTipSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.SettingsDamAttributes["ListViewToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attrvar = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (attrvar != null) $scope.attributedata.push(attrvar);
                    }
                }
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate;
                                datstartval = new Date(datStartUTCval);
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate;
                                datendval = new Date(datEndUTCval);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datstartval = new Date($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide\"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div></div>';
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    }
                }
            }
            return html;
        }
        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                MediabankService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '<div class="th-AssetInfoTooltip">';
                    html += DrawThumbnailBlock(attrList);
                    html += '</div> ';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsDamAttributes["ThumbnailToolTipSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.SettingsDamAttributes["ThumbnailToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attrvar = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (attrvar != null) $scope.attributedata.push(attrvar);
                    }
                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span></div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate;
                                datstartval = new Date(datStartUTCval);
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate;
                                datendval = new Date(datEndUTCval);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datstartval = new Date($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div>';
                            } else {
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }
        $scope.Editasset = function (event, AssetID, entityId) {
            if ($(event.target).hasClass('disabled')) {
                return;
            }
            $(event.target).addClass("disabled");
            $scope.IsLockMediaEntity = true;
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: AssetID,
                            IsLock: $scope.IsLockMediaEntity,
                            isNotify: true,
                            viewtype: 'ViewType'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                if ($(event.target).hasClass("disabled")) {
                    $(event.target).removeClass("disabled");
                }
            });
        }
        $scope.AddFilesFromDAM = function () {
            var count = $scope.AssetSelectionFiles.length;
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr.push($scope.AssetSelectionFiles[i].AssetId);
            }
            var g = arr.length;
            if (g == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            } else {
                $scope.entitylockstatus.isUpdatedfrmsection = false;
                var data = {};
                data.AssetArr = arr;
                data.EntityID = $scope.MediaBankSettings.EntityId;
                data.FolderID = $scope.MediaBankSettings.FolderId;
                MediabankService.AddPublishedFilesToDAM(data).then(function (result) {
                    if (result.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_4262.Caption'));
                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                            $location.path('/mui/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 35) {
                            $location.path('/mui/calender/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else {
                            if ($scope.IsfromassetlibrarytoCMS.Isfromassetlibrary == true) {
                                $scope.IsfromassetlibrarytoCMS.IstoCMSattachments = true;
                                $location.path('/mui/cms/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                            } else $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        }
                    } else {
                        NotifySuccess(count + " " + "File(s) Created Successfully ");
                        $scope.MediaBankSettings.Redirectfromdam = false;
                        $scope.MediaBankSettings.FolderId = '';
                        $scope.MediaBankSettings.directfromMediaBank = true;
                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                            $location.path('/mui/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 35) {
                            $location.path('/mui/calender/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else {
                            if ($scope.IsfromassetlibrarytoCMS.Isfromassetlibrary == true) {
                                $scope.IsfromassetlibrarytoCMS.IstoCMSattachments = true;
                                $location.path('/mui/cms/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                            } else $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        }
                    }
                });
            }
        }
        $scope.AddLinkFilesFromDAM = function () {
            var count = $scope.AssetSelectionFiles.length;
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr.push($scope.AssetSelectionFiles[i].AssetId);
            }
            var l = arr.length;
            if (l == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            } else {
                $scope.entitylockstatus.isUpdatedfrmsection = false;
                var data = {};
                data.AssetArr = arr;
                data.EntityID = $scope.MediaBankSettings.EntityId;
                data.FolderID = $scope.MediaBankSettings.FolderId;
                MediabankService.AddPublishedLinkFiles(data).then(function (result) {
                    if (result.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_4262.Caption'));
                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                            $location.path('/mui/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 35) {
                            $location.path('/mui/calender/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else {
                            if ($scope.IsfromassetlibrarytoCMS.Isfromassetlibrary == true) {
                                $scope.IsfromassetlibrarytoCMS.IstoCMSattachments = true;
                                $location.path('/mui/cms/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                            } else $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        }
                    } else {
                        NotifySuccess(count + " " + "Files(s) created successfully");
                        $scope.MediaBankSettings.Redirectfromdam = false;
                        $scope.MediaBankSettings.FolderId = '';
                        $scope.MediaBankSettings.directfromMediaBank = true;
                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                            $location.path('/mui/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else if ($scope.MediaBankSettings.ProductionTypeID == 35) {
                            $location.path('/mui/calender/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        } else {
                            if ($scope.IsfromassetlibrarytoCMS.Isfromassetlibrary == true) {
                                $scope.IsfromassetlibrarytoCMS.IstoCMSattachments = true;
                                $location.path('/mui/cms/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                            } else $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                        }
                    }
                });
            }
        }
        $scope.Addtolightbox = function () {
            var imageassetcount = 0;
            var imageasset = [];
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.damlightbox.lightboxselection, function (e) {
                        return e.AssetId == assetid.AssetId;
                    });
                    if (remainRecord.length == 0) {
                        var asset = [],
                            data = [];
                        asset = $.grep($scope.AssetFiles, function (e) {
                            return e.AssetUniqueID == assetid.AssetId;
                        });
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == assetid.AssetId;
                        });
                        if (asset[0].FileGuid != null) if (asset.length > 0) $scope.damlightbox.lightboxselection.push({
                            "AssetRawData": asset[0],
                            "EntityID": asset[0].EntityID,
                            "ShortDesc": asset[0].ShortDescription,
                            "ColorCode": asset[0].ColorCode,
                            "AssetId": assetid.AssetId,
                            "AssetName": asset[0].Name,
                            "Description": asset[0].Description != null ? asset[0].Description : "-",
                            "Guid": asset[0].FileGuid,
                            "Ext": asset[0].Extension,
                            "AssetStatus": asset[0].Status,
                            "FileSize": parseSize(asset[0].Size),
                            "DynamicData": data,
                            "MimeType": asset[0].MimeType,
                            "Extension": asset[0].Extension.substring(1, asset[0].Extension.length).toLowerCase(),
                            "Category": asset[0].Category,
                            "Iscropped": false
                        });
                        imageassetcount = imageassetcount + 1;
                        imageasset.push(assetid.AssetId);
                    }
                }
                if ($scope.damlightbox.lightboxselection.length > 0 && imageassetcount > 0) {
                    $timeout(function () {
                        $scope.AssetSelectionFiles = [];
                    }, 200);
                    var ht_Count = "";
                    if (imageassetcount > 1) ht_Count = "Assets";
                    if (imageassetcount == 1) ht_Count = "Asset";
                    NotifySuccess(imageassetcount + " " + ht_Count + " " + " Added to light box successfully.");
                    $scope.DownloadFileObj = [];
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    clearassetSelection();
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.DownloadWithMetadata = function (result) {
            if ($scope.AssetSelectionFiles.length > 0) {
                var arr = [];
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    arr[i] = $scope.AssetSelectionFiles[i].AssetId;
                }
                var include = result;
                if (include == 0) $scope.ProgressMsgHeader = "Downloading Metadata";
                if (include == 1) $scope.ProgressMsgHeader = "Downloading Package";
                blockUIForDownload();
                MediabankService.DownloadAssetWithMetadata(arr, result).then(function (result) {
                    if (result.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                    } else {
                        if (arr.length == 1) {
                            if (include == false) {
                                var fileid = result.Response[1],
									extn = '.xml';
                                if (result.Response[0] == null || result.Response[0] == "") var filename = "MediabankAssets" + extn;
                                else var filename = result.Response[0];
                            } else {
                                var fileid = result.Response[1],
									extn = '.zip';
                                var filename = fileid + extn;
                            }
                        } else {
                            var fileid = result.Response[1],
								extn = '.zip';
                            var filename = fileid + extn;
                        }

                        if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                            location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                        } else {
                            location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#mediasdownload_token_value_id').val();
                        }

                        if (include == true) NotifySuccess("Assets Package Downloaded Sucessfully");
                        else NotifySuccess("Assets Metadata Downloaded Sucessfully");
                        $timeout(function () {
                            clearassetSelection();
                            refreshDownLoadScope(arr);
                            finishDownload();
                        }, 100);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }

        function clearassetSelection() {
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                $('#chkBoxIcon_' + $scope.AssetSelectionFiles[i].AssetId).attr("class", "checkbox");
                if ($scope.DamViewName == "Thumbnail") $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "th-Selection";
                else if ($scope.DamViewName == "Summary") $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "th-sum-Selection";
                else if ($scope.DamViewName == "List") $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "li-Selection";
                if ($("#SelectAllAssets").parent().find("i").hasClass("checked")) { $("#SelectAllAssets").parent().find("i").removeClass("checked") }
                $scope.AssetFiles[i].Checked = false;
            }
            $scope.AssetSelectionFiles.splice(0, $scope.AssetSelectionFiles.length);
        }
        $scope.PerformAssetOperation = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];
                    $scope.damclipboard.assetselection.push({
                        "AssetId": assetid.AssetId
                    });
                }
                $timeout(function () {
                    $scope.AssetSelectionFiles = [];
                }, 200);
                NotifySuccess("File Copied.");
                $scope.DownloadFileObj.DownloadFilesArr = [];
                clearassetSelection();
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.sendMetadatMailSubject = "";
        $scope.openSendAssetMail = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                $scope.sendMetadatMailSubject = "";
                $scope.mediabank.mailtags = [];
                $('#sendMediabankMailPopup').modal("show");
                $scope.includemdata = false;
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked) $scope.includemdata = true;
            else $scope.includemdata = false;
        }
        $scope.sendmailOptions = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.SendAssetWitMwtadata = function () {
            $scope.MailprocessMailFlag = true;
            var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var assetSelection = [],
				toArray = new Array(),
				mailSubject = $scope.sendMetadatMailSubject != "" ? $scope.sendMetadatMailSubject : "Marcom Media Bank";
            for (var i = 0, mail; mail = $scope.mediabank.mailtags[i++];) {
                if (mail.text.match(emailvalid)) {
                    toArray.push(mail.text);
                } else {
                    $scope.MailprocessMailFlag = false;
                    bootbox.alert("Please Provide Valid Mail ID");
                    return false;
                }
            }
            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                var remainRecord = [];
                assetSelection.push({
                    "AssetId": assetid.AssetId
                });
            }
            if ($scope.AssetSelectionFiles.length > 0 && toArray.length > 0) {
                var srcObj = {};
                srcObj.Toaddress = toArray.join(",");
                srcObj.subject = mailSubject;
                srcObj.AssetArr = assetSelection;
                srcObj.metadata = $scope.includemdata;
                $timeout(function () {
                    MediabankService.SendMailWithMetaData(srcObj).then(function (result) {
                        if (result.Response == null) {
                            $scope.sendMetadatMailSubject = "";
                            $scope.MailprocessMailFlag = false;
                            $scope.mediabank.mailtags = [];
                            NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                        } else {
                            $scope.sendMetadatMailSubject = "";
                            $scope.MailprocessMailFlag = false;
                            $scope.mediabank.mailtags = [];
                            NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                            $('#sendMediabankMailPopup').modal("hide");
                            clearassetSelection();
                        }
                    });
                }, 100);
            } else {
                $scope.MailprocessMailFlag = false;
                bootbox.alert("Please Enter the Mail ID");
            }
        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
            clearInterval(IntervaltocheckRoute);
            $scope.IsfromassetlibrarytoCMS.Isfromassetlibrary = false;
            RecursiveUnbindAndRemove($("[ng-controller='mui.muimediabankCtrl']"));
        });
        var IntervaltocheckRoute = setInterval(function () {
            $rootScope.currentPathName = $location.path().replace('/', '');
            if ($rootScope.currentPathName === "mui/mediabank") {
                $scope.Copy = false;
                $scope.div = true;
                $scope.div2 = false;
                $scope.div3 = false;
                $scope.showCopyandLink = true;
                $scope.IsMarcomMediabank = false;
            } else {
                $scope.Copy = true;
                $scope.div = true;
                $scope.div2 = true;
                $scope.div3 = true;
                $scope.showCopyandLink = false;
                $scope.IsMarcomMediabank = true;
            }
        }, 100);
        $scope.SetAssetActionId = function (assetid) {
            $('#chkBoxIcon_' + assetid + '').addClass('checked');
            if ($scope.AssetSelectionFiles.length > 0) {
                var asset = [],
					data = [];
                asset = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e.AssetId == assetid;
                });
                if (asset.length == 0) {
                    $scope.AssetSelectionFiles.push({
                        "AssetId": assetid
                    });
                    if ($scope.FilterStatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                    else if ($scope.FilterStatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                    else if ($scope.FilterStatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";
                    $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
                }
            } else {
                $scope.AssetSelectionFiles.push({
                    "AssetId": assetid
                });
                $scope.AssetSelectionFiles.selectedAssetId = assetid;
                $scope.CustomSelect = assetid;
                if ($scope.FilterStatus == 1) $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                else if ($scope.FilterStatus == 2) $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                else if ($scope.FilterStatus == 3) $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";
                $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
            }
        }
        $scope.UnPublishAssets = function () {
            var arr = [];
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    arr[i] = $scope.AssetSelectionFiles[i].AssetId;
                }
                MediabankService.UnPublishAssets(arr).then(function (result) {
                    if (result.Response == true) {
                        if (arr.length > 1) {
                            NotifySuccess("" + arr.length + "" + " Assets UnPublished successfully");
                        } else {
                            NotifySuccess("1 Asset UnPublished successfully");
                        }
                        for (var j = 0; j < arr.length; j++) {
                            $('#AssetBlockId_' + arr[j]).addClass('displayNone');
                            for (var i = 0; i < $scope.AssetFilesObj_Temp.AssetFiles.length; i++) {
                                var temparr = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                                    return e.IsPublish == true && e.AssetUniqueID == arr[j];
                                });
                                if (temparr.length > 0) {
                                    $scope.AssetFilesObj_Temp.AssetFiles[i].IsPublish = false;
                                }
                            }
                        }
                        clearassetSelection();
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.viewlightbox = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damlightbox.html',
                controller: "mui.DAM.lightboxCtrl",
                resolve: {
                    params: function () {
                        return {
                            isfromDAM: false
                        };
                    }
                },
                scope: $scope,
                windowClass: 'in LightBoxPreviewPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            clearassetSelection();
        }
        $scope.Reformatprocess = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                var reformate = [],
					temparr = [];
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    temparr = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                        return e.Category == 0 && e.ProcessType == 1 && e.Status == 2 && e.Extension != ".pdf" && e.AssetUniqueID == $scope.AssetSelectionFiles[i].AssetId
                    });
                    if (temparr != null) {
                        if (temparr.length > 0) {
                            reformate.push(temparr[0]);
                        }
                    }
                }
                if ($scope.AssetSelectionFiles.length > 0 && reformate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1165.Caption'));
                    refreshTaskAssetSelection();
                    return false;
                } else if ($scope.AssetSelectionFiles.length > 0 && reformate.length > 1) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1166.Caption'));
                    refreshTaskAssetSelection();
                    return false;
                } else if ($scope.AssetSelectionFiles.length > 0 && reformate.length == 1) {
                    $timeout(function () {
                        var folderid;
                        var EntityID;
                        folderid = reformate[0].FolderID;
                        EntityID = reformate[0].EntityID;
                        var modalInstance = $modal.open({
                            templateUrl: 'views/mui/assetreformat.html',
                            controller: "mui.muiassetreformatCtrl",
                            resolve: {
                                params: function () {
                                    return {
                                        thubmnailGuid: reformate[0].FileGuid,
                                        assetid: reformate[0].AssetUniqueID,
                                        folderid: folderid,
                                        EntityID: EntityID,
                                        fromisassetlibrary: 1
                                    };
                                }
                            },
                            scope: $scope,
                            windowClass: 'reformatAssetPopup popup-widthXL',
                            backdrop: "static"
                        });
                        modalInstance.result.then(function (selectedItem) {
                            $scope.selected = selectedItem;
                        }, function () { });
                    }, 100);
                    clearassetSelection();
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.AddFilestoCMS = function (attachType) {
            if ($scope.DownloadFileObj.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            } else if ($scope.DownloadFileObj.length > 1) {
                bootbox.alert('Can not attach more than one asset.');
                return false;
            } else if ($scope.DownloadFileObj.length == 1) {
                var ste = $scope.DownloadFileObj[0].Extension.split('.').join("");
                var fileType = o.Mime.mimes[ste.toLowerCase()].split('/')[0];
                if (attachType == 'video' && o.Mime.mimes[ste.toLowerCase()].split('/')[0] != 'video') {
                    bootbox.alert('Can not attach this asset as video.');
                    return false;
                }
                var arr = [];
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    arr.push($scope.AssetSelectionFiles[i].AssetId);
                }
                var data = {};
                data.AssetArr = arr;
                data.EntityID = $scope.MediaBankSettings.EntityId;
                data.FolderID = $scope.MediaBankSettings.FolderId;
                data.AttachType = attachType;
                MediabankService.LinkpublishedfilestoCMS(data).then(function (result) {
                    if (result.Response != null) {
                        if (attachType == 'link') {
                            NotifySuccess("Asset linked successfully");
                        }
                        $scope.contentDatafrCMS.attachType = attachType;
                        $scope.contentDatafrCMS.fileData = result.Response;
                        $scope.MediaBankSettings.Redirectfromdam = false;
                        $scope.MediaBankSettings.FolderId = '';
                        $scope.MediaBankSettings.directfromMediaBank = true;
                        $scope.MediaBankSettings.attachType = attachType;
                        $scope.MediaBankSettings.resultResponse = result.Response;
                        if ($scope.MediaBankSettings.ProductionTypeID == 17) {
                            $timeout(function () {
                                $scope.MediaBankSettings.ProductionTypeID = 0;
                                $scope.MediaBankSettings.FolderName = '';
                                $scope.MediaBankSettings.frmCMS = false;
                            }, 500);
                            $location.path('/mui/cms/detail/section/' + $scope.MediaBankSettings.EntityId + '/content');
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4262.Caption'))
                    }
                });
            }
        };
    }
    app.controller("mui.muimediabankCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$rootScope', '$translate', 'MediabankService', '$compile', '$modal', muimediabankCtrl]);

    app.directive('abnTreeMediabank', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            //controller: 'mui.muimediabankCtrl',
            template: "<ul  class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">\n  <li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"assetlibTreeSearch treesearchcls\">\n   " + "<a  ng-hide=\"treeAccessable\" class=\"treesearchcls\"  ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"  class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "<a  ng-show=\"treeAccessable\"  class=\"treesearchcls padding-bottom0px\" ng-click=\"user_clicks_branch_Expand_Collapse(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"   class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\" ng-click='checktoFilter($event,row.branch)' ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "</div></li>\n</ul>",
            scope: {
                treeData: '=',
                treeFilter: '=',
                onSelect: '&',
                initialSelection: '@',
                accessable: '@',
                treeControl: '='
            },
            link: function (scope, element, attrs) {
                var error, expand_all_parents, expand_level, for_all_ancestors, for_each_branch, get_parent, n, on_treeData_change, select_branch, selected_branch, tree;
                error = function (s) {
                    console.log('ERROR:' + s);
                    return void 0;
                };
                if (attrs.iconExpand == null) {
                    attrs.iconExpand = 'icon-caret-right';
                }
                if (attrs.iconCollapse == null) {
                    attrs.iconCollapse = 'icon-caret-down';
                }
                if (attrs.iconLeaf == null) {
                    attrs.iconLeaf = 'icon-fixed-width icon-blank';
                }
                if (attrs.expandLevel == null) {
                    attrs.expandLevel = '100';
                }
                expand_level = parseInt(attrs.expandLevel, 10);
                if (!scope.treeData) {
                    console.log('no treeData defined for the tree!');
                    return;
                }
                if (scope.treeData.length == null) {
                    if (treeData.Caption != null) {
                        scope.treeData = [treeData];
                    } else {
                        console.log('treeData should be an array of root branches');
                        return;
                    }
                }
                for_each_branch = function (f) {
                    var do_f, root_branch, _i, _len, _ref, _results;
                    do_f = function (branch, level) {
                        var child, _i, _len, _ref, _results;
                        f(branch, level);
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(do_f(child, level + 1));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(do_f(root_branch, 1));
                    }
                    return _results;
                };
                selected_branch = null;
                select_branch = function (branch, parentArr) {
                    if (!branch) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        selected_branch = null;
                        return;
                    }
                    if (branch !== undefined) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        branch.selected = true;
                        selected_branch = branch;
                        expand_all_parents(branch);
                        if (branch.onSelect != null) {
                            return $timeout(function () {
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                return branch.onSelect(branch, test);
                            });
                        } else {
                            if (scope.onSelect != null) {
                                return $timeout(function () {
                                    var test = [];
                                    var parent;
                                    parent = get_parent(branch);
                                    if (parent != null) {
                                        test.push(parent);
                                        scope.parenttester = [];
                                        var ty = recursiveparent(parent);
                                        if (ty != undefined) {
                                            if (ty.length > 0) {
                                                for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                    test.push(obj);
                                                }
                                            }
                                        }
                                    }
                                    return scope.onSelect({
                                        branch: branch,
                                        parent: test
                                    });
                                });
                            }
                        }
                    }
                };
                scope.parenttester = [];
                scope.user_clicks_branch = function (branch) {
                    if (branch !== selected_branch) {
                        var test = [];
                        var parent;
                        parent = get_parent(branch);
                        if (parent != null) {
                            test.push(parent);
                            scope.parenttester = [];
                            var ty = recursiveparent(parent);
                            if (ty != undefined) {
                                if (ty.length > 0) {
                                    for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                        test.push(obj);
                                    }
                                }
                            }
                        }
                        return select_branch(branch, test);
                    }
                };
                scope.filterItem = function (item) {
                    if (!scope.treeFilter) {
                        item.isShow = true;
                        if (item.expanded != undefined) {
                            item.expanded = true;
                        }
                        return true;
                    }
                    var found = item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase()) != -1;
                    if (!found) {
                        var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                        angular.forEach(itemColl, function (item) {
                            var match = scope.filterItem(item);
                            if (match) {
                                found = true;
                                item.isShow = true;
                            }
                        });
                    }
                    return found;
                };

                function collapseOnemptySearch(item) {
                    var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                    angular.forEach(itemColl, function (item) {
                        item.isShow = true;
                    });
                }
                scope.user_clicks_branch_Expand_Collapse = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    }
                };
                scope.user_clicks_branch_1 = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        branch.ischecked = !branch.ischecked
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    } else {
                        branch.ischecked = branch.ischecked;
                        return select_branch(branch, test);
                    }
                };

                scope.checktoFilter = function (event, branch) {
                    branch.ischecked = !(branch.ischecked);
                    event.target = {};
                    event.target.Caption = branch.Caption;
                    event.target.AttrType = branch.attributeCaption;
                    event.target.checked = branch.ischecked;
                    scope.$parent.FilterbyAttributeSelection(event, branch.AttributeId, 7, branch.id, branch.Level);

                }

                function Expandcollapse(Children, expanded) {
                    for (var z = 0, childObj; childObj = Children[z++];) {
                        childObj.isShow = expanded;
                        if (childObj.Children.length > 0) {
                            Expandcollapse(childObj.Children, expanded);
                        }
                    }
                }

                function recursiveparent(parent) {
                    var parent1;
                    parent1 = get_parent(parent);
                    if (parent1 != null && parent1 != undefined) {
                        scope.parenttester.push(parent1);
                        recursiveparent(parent1);
                    }
                    return scope.parenttester;
                }
                get_parent = function (child) {
                    var parent;
                    parent = void 0;
                    if (child.parent_uid) {
                        for_each_branch(function (b) {
                            if (b.uid === child.parent_uid) {
                                return parent = b;
                            }
                        });
                    }
                    return parent;
                };
                for_all_ancestors = function (child, fn) {
                    var parent;
                    parent = get_parent(child);
                    if (parent != null) {
                        fn(parent);
                        return for_all_ancestors(parent, fn);
                    }
                };
                expand_all_parents = function (child) {
                    return for_all_ancestors(child, function (b) {
                        return b.expanded = true;
                    });
                };
                scope.tree_rows = [];
                scope.treeAccessable = attrs.accessable != null ? (attrs.accessable == "false" ? false : true) : false;
                on_treeData_change = function () {
                    var add_branch_to_list, root_branch, _i, _len, _ref, _results;
                    for_each_branch(function (b, level) {
                        if (!b.uid) {
                            return b.uid = "" + Math.random();
                        }
                    });
                    for_each_branch(function (b) {
                        var child, _i, _len, _ref, _results;
                        if (angular.isArray(b.Children)) {
                            _ref = b.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(child.parent_uid = b.uid);
                            }
                            return _results;
                        }
                    });
                    scope.tree_rows = [];
                    for_each_branch(function (branch) {
                        var child, f;
                        if (branch.Children) {
                            if (branch.Children.length > 0) {
                                f = function (e) {
                                    if (typeof e === 'string') {
                                        return {
                                            Caption: e,
                                            Children: []
                                        };
                                    } else {
                                        return e;
                                    }
                                };
                                return branch.Children = (function () {
                                    var _i, _len, _ref, _results;
                                    _ref = branch.Children;
                                    _results = [];
                                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                        child = _ref[_i];
                                        _results.push(f(child));
                                    }
                                    return _results;
                                })();
                            }
                        } else {
                            return branch.Children = [];
                        }
                    });
                    add_branch_to_list = function (level, branch, visible) {
                        var child, child_visible, tree_icon, _i, _len, _ref, _results;
                        if (branch.expanded == null) {
                            branch.expanded = false;
                        }
                        if (!branch.Children || branch.Children.length === 0) {
                            tree_icon = attrs.iconLeaf;
                        } else {
                            if (branch.expanded) {
                                tree_icon = attrs.iconCollapse;
                            } else {
                                tree_icon = attrs.iconExpand;
                            }
                        }
                        branch.attributeCaption = scope.$parent.treesrcdirec.attributeCaption;
                        scope.tree_rows.push({
                            level: level,
                            branch: branch,
                            Caption: branch.Caption,
                            tree_icon: tree_icon,
                            visible: visible,
                            ischecked: branch.ischecked != undefined ? branch.ischecked : false,
                            isShow: branch.isShow != undefined ? branch.isShow : true,

                        });
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                child_visible = visible && branch.expanded;
                                _results.push(add_branch_to_list(level + 1, child, child_visible));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(add_branch_to_list(1, root_branch, true));
                    }
                    return _results;
                };
                scope.$watch('treeData', on_treeData_change, true);
                if (attrs.initialSelection != null) {
                    for_each_branch(function (b) {
                        if (b.Caption === attrs.initialSelection) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return $timeout(function () {
                                return select_branch(b, test);
                            });
                        }
                    });
                }
                n = scope.treeData.length;
                for_each_branch(function (b, level) {
                    b.level = level;
                    return b.expanded = b.level < expand_level;
                });
                if (scope.treeControl != null) {
                    if (angular.isObject(scope.treeControl)) {
                        tree = scope.treeControl;
                        tree.expand_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = true;
                            });
                        };
                        tree.collapse_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = false;
                            });
                        };
                        tree.get_first_branch = function () {
                            n = scope.treeData.length;
                            if (n > 0) {
                                return scope.treeData[0];
                            }
                        };
                        tree.select_first_branch = function () {
                            var b;
                            b = tree.get_first_branch();
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return tree.select_branch(b, test);
                        };
                        tree.get_selected_branch = function () {
                            return selected_branch;
                        };
                        tree.get_parent_branch = function (b) {
                            return get_parent(b);
                        };
                        tree.select_branch = function (b, parentArr) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            select_branch(b, test);
                            return b;
                        };
                        tree.get_children = function (b) {
                            return b.Children;
                        };
                        tree.select_parent_branch = function (b) {
                            var p;
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                p = tree.get_parent_branch(b);
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                if (p != null) {
                                    tree.select_branch(p, test);
                                    return p;
                                }
                            }
                        };
                        tree.add_branch = function (parent, new_branch) {
                            if (parent != null) {
                                parent.Children.push(new_branch);
                                parent.expanded = true;
                            } else {
                                scope.treeData.push(new_branch);
                            }
                            return new_branch;
                        };
                        tree.add_root_branch = function (new_branch) {
                            tree.add_branch(null, new_branch);
                            return new_branch;
                        };
                        tree.expand_branch = function (b) {
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                b.expanded = true;
                                return b;
                            }
                        };
                        tree.collapse_branch = function (b) {
                            if (b == null) {
                                b = selected_branch;
                            }
                            if (b != null) {
                                b.expanded = false;
                                return b;
                            }
                        };
                    }
                }
            }
        };
    }]);
    app.directive('treecontext', [function () {
        return {
            restrict: 'A',
            compile: function compile(tElement, tAttrs, transclude, scope) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.treecontext),
							last = null;
                        var id = $('#' + iAttrs.treecontext);
                        ul.css({
                            'display': 'none'
                        });
                        $(iElement).click(function (event) {
                            event.stopPropagation();
                            var top = event.clientY + 10;
                            if ((ul.height() + top + 14) > $(window).height()) {
                                top = top - (ul.height() + 50);
                            } else { }
                            ul.css({
                                display: "block",
                                zIndex: 999999
                            });
                            var target = $(event.target);
                            last = event.timeStamp;
                            if (event.stopPropagation) event.stopPropagation();
                            if (event.preventDefault) event.preventDefault();
                            event.cancelBubble = true;
                            event.returnValue = false;
                            jQuery('#' + iAttrs.treecontext).show().appendTo(jQuery(this).parent())
                        });
                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (target.hasClass("treesearchcls")) {
                                return;
                            }
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                                jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
                            }
                        });
                        $(window).scroll(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                                jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
                            }
                        });
                    }
                };
            }
        };
    }]);
    app.directive('euTree', ['$compile', function ($compile) {
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {
                scope.selectedNode = null;
                var treeformflag = false;
                var treenodetree = new Array();

                function CallBackTreeStructure(treeobj) {
                    treenodetree = new Array();
                    processrecords(treeobj);
                    var dtstring = treenodetree.join("");
                    return dtstring;
                }

                function processrecords(treeobj) {
                    if (treeobj != undefined) {
                        for (var i = 0, node; node = treeobj[i++];) {
                            if (node.ischecked == true) {
                                if (treenodetree.length > 0) if (treenodetree[treenodetree.length - 1] != "[") treenodetree.push(",");
                                treenodetree.push(node.Caption);
                                treeformflag = false;
                                if (ischildSelected(node.Children)) {
                                    treenodetree.push("[");
                                    processrecords(node.Children);
                                    treenodetree.push("]");
                                } else {
                                    processrecords(node.Children);
                                }
                            } else processrecords(node.Children);
                        }
                    }
                }

                function ischildSelected(children) {
                    for (var j = 0, child; child = children[j++];) {
                        if (child.ischecked == true) {
                            treeformflag = true;
                            return treeformflag
                        }
                    }
                    return treeformflag;
                }
                scope.$watch(attrs.treeData, function (val) {
                    var treestring = '';
                    treenodetree = new Array();
                    treestring = (attrs.fileid == undefined || attrs.fileid == null) ? CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.nodeAttributeid]) : CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.fileid + attrs.nodeAttributeid]);
                    var template = '';
                    var placeoftree = (attrs.treeplace != null || attrs.treeplace != undefined) ? attrs.treeplace : "inline";
                    if (placeoftree === "detail") template = angular.element('<a href="javscript:void(0)" class="treeAttributeSelectionText">' + treestring + '</a>');
                    else template = angular.element('<span class="treeAttributeSelectionText">' + treestring + '</span>');
                    var linkFunction = $compile(template);
                    linkFunction(scope);
                    element.html(null).append(template);
                }, true);
            }
        };
    }]);

})(angular, app);
///#source 1 1 /app/services/mediabank-service.js
(function (ng, app) {
    "use strict";

    function MediabankService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetAllExtensionTypesforDAM: GetAllExtensionTypesforDAM,
            GetCustomFilterAttributes: GetCustomFilterAttributes,
            GetDAMViewSettings: GetDAMViewSettings,
            GetDAMToolTipSettings: GetDAMToolTipSettings,
            GetMediaAssets: GetMediaAssets,
            DownloadFiles: DownloadFiles,
            GetAttributeDetails: GetAttributeDetails,
            AddPublishedFilesToDAM: AddPublishedFilesToDAM,
            AddPublishedLinkFiles: AddPublishedLinkFiles,
            DownloadAssetWithMetadata: DownloadAssetWithMetadata,
            SendMailWithMetaData: SendMailWithMetaData,
            UnPublishAssets: UnPublishAssets,
            GetLockStatus: GetLockStatus,
            LinkpublishedfilestoCMS: LinkpublishedfilestoCMS
        });

        function GetAllExtensionTypesforDAM() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMExtensions/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomFilterAttributes(typeid) {
            var request = $http({
                method: "get",
                url: "api/dam/GetCustomFilterAttributes/" + typeid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMViewSettings() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMViewSettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMToolTipSettings() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMToolTipSettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMediaAssets(jobj) {
            var request = $http({
                method: "post",
                ignoreLoadingBar : true,
                url: "api/dam/GetMediaAssets/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DownloadFiles(arrObj) {
            var request = $http({
                method: "post",
                url: "api/dam/DownloadDamZip/",
                params: {
                    action: "add"
                },
                data: {
                    AssetArr: arrObj
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeDetails(damid) {
            var request = $http({
                method: "get",
                ignoreLoadingBar : true,
                url: "api/dam/GetAssetAttributesDetails/" + damid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AddPublishedFilesToDAM(jobj) {
            var request = $http({
                method: "post",
                url: "api/dam/AddPublishedFilesToDAM/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }
        function AddPublishedLinkFiles(jobj) {
            var request = $http({
                method: "post",
                url: "api/dam/AddPublishedLinkFiles/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DownloadAssetWithMetadata(assetid, result) {
            var request = $http({
                method: "post",
                url: "api/dam/DownloadAssetWithMetadata/",
                params: {
                    action: "add"
                },
                data: {
                    assetid: assetid,
                    result: result
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SendMailWithMetaData(srcObj) {
            var request = $http({
                method: "post",
                url: "api/dam/SendMailWithMetaData/",
                params: {
                    action: "add"
                },
                data: srcObj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UnPublishAssets(arrObj) {
            var request = $http({
                method: "post",
                url: "api/dam/UnPublishAssets/",
                params: {
                    action: "add"
                },
                data: {
                    AssetArr: arrObj
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLockStatus(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetLockStatus/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function LinkpublishedfilestoCMS(jobj) {
            var request = $http({
                method: "post",
                url: "api/cms/LinkpublishedfilestoCMS/", params: { action: "add" }, data: jobj
            }); return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("MediabankService", ['$http', '$q', MediabankService]);
})(angular, app);
