﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class AttributeGroup : IAttributeGroup
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        protected string _description;
        protected bool _bIsChanged;
        protected bool _isPredefined;

        #endregion

        #region Constructors
        public AttributeGroup() { }

        public AttributeGroup(int pId, string pCaption, string pDescription, bool pIsPredefined)
        {
            this._id = pId;
            this._caption = pCaption;
            this._description = pDescription;
            this._description = pDescription;
            this._isPredefined = pIsPredefined;
        }

        public AttributeGroup(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 250 characters");
                _caption = value;
            }

        }
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }
        }

        public bool IsPredefined
        {
            get { return _isPredefined; }
            set { _isPredefined = value; }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityType castObj = null;
            try
            {
                castObj = (EntityType)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
