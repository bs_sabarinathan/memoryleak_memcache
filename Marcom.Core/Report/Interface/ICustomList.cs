﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Report.Interface
{
   public interface ICustomList
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        string XmlData
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        #endregion

    }
}
