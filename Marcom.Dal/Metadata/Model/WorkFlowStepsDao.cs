﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// AttributeDao object for table 'MM_Attribute'.
    /// </summary>

    public partial class WorkFlowStepsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _workflowid;
        protected string _name;
        protected string _helptext;
        protected int _sortorder;
        protected IList<WorkFlowStepPredefinedTasksDao> _workflowtasks;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public WorkFlowStepsDao() { }

        public WorkFlowStepsDao(int pId, int pWorkFlowID, string pName, string pHelpText, int pSortOrder)
        {
            this._id = pId;
            this._workflowid = pWorkFlowID;
            this._name = pName;
            this._helptext = pHelpText;
            this._sortorder = pSortOrder;
        }

        public WorkFlowStepsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int WorkFlowID
        {
            get { return _workflowid; }
            set { _bIsChanged |= (_workflowid != value); _workflowid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual string HelpText
        {
            get { return _helptext; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_helptext != value);
                _helptext = value;
            }

        }


        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }

        public virtual IList<WorkFlowStepPredefinedTasksDao> WorkFlowTasks
        {
            get { return _workflowtasks; }
            set { _bIsChanged |= (_workflowtasks != value); _workflowtasks = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string WorkFlowStepsDao = "WorkFlowStepsDao";
            public const string Id = "Id";
            public const string WorkFlowID = "WorkFlowID";
            public const string Name = "Name";
            public const string HelpText = "HelpText";
            public const string SortOrder = "SortOrder";
            public const string WorkFlowTasks = "WorkFlowTasks";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string WorkFlowStepsDao = "WorkFlowStepsDao";
            public const string Id = "Id";
            public const string WorkFlowID = "WorkFlowID";
            public const string Name = "Name";
            public const string HelpText = "HelpText";
            public const string SortOrder = "SortOrder";
            public const string WorkFlowTasks = "WorkFlowTasks";
        }

        #endregion


    }

}
