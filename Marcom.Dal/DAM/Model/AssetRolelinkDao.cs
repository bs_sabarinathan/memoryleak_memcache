﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class AssetRolelinkDao : BaseDao, ICloneable
    {
        #region "Member variables"

        protected int _id;
        protected int _roleid;
        protected int _assetid;        

        #endregion

        #region "Constructor"

        public AssetRolelinkDao() { }


        public AssetRolelinkDao(int pid, int pRoleid,int pAssetid) 
        {
            this._id = pid;
            this._roleid = pRoleid;
            this._assetid = pAssetid;
        }

        #endregion

        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }       


        public virtual int Roleid
        {
            get { return _roleid; }
            set { _roleid = value; }

        }

        public virtual int Assetid
        {
            get { return _assetid; }
            set { _assetid = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetRolelinkDao = "AssetRolelinkDao";
            public const string ID = "ID";
            public const string RoleID = "Roleid";
            public const string AssetID = "Assetid";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetRolelinkDao = "AssetRolelinkDao";
            public const string ID = "ID";
            public const string RoleID = "Roleid";
           public const string AssetID = "Assetid";
            
        }

        #endregion

    }
}
