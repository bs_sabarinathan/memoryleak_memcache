using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// Milestone object for table 'PM_Milestone'.
	/// </summary>
	
	internal class Milestone : IMilestone 
	{
		#region Member Variables

		protected int _id;
		protected int _entityid;
		protected string _name;
		protected string _description;
		protected int _status;
		protected DateTimeOffset _duedate;
		
		
		#endregion
		
		#region Constructors
		public Milestone() {}
			
		public Milestone(int pEntityid, string pName, string pDescription, int pStatus, DateTimeOffset pDueDate)
		{
			this._entityid = pEntityid; 
			this._name = pName; 
			this._description = pDescription; 
			this._status = pStatus; 
			this._duedate = pDueDate; 
		}
				
		public Milestone(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
			  _name = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}
		
		public int Status
		{
			get { return _status; }
			set { _status = value; }
			
		}
		
		public DateTimeOffset DueDate
		{
			get { return _duedate; }
			set { _duedate = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Milestone castObj = null;
			try
			{
				castObj = (Milestone)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
