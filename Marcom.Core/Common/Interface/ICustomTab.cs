using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
    {
    /// <summary>
    /// INavigation interface for table 'CM_Navigation'.
    /// </summary>
    public interface ICustomTab
        {
        #region Public Properties

        int Id
            {
            get;
            set;

            }

        int Typeid
            {
            get;
            set;

            }

        string Name
            {
            get;
            set;

            }

        string ExternalUrl
            {
            get;
            set;

            }


        bool IsSytemDefined
            {
            get;
            set;

            }

        bool AddUserName
            {
            get;
            set;

            }

        bool AddUserEmail
            {
            get;
            set;

            }

        bool AddUserID
            {
            get;
            set;

            }

        bool AddLanguageCode
            {
            get;
            set;

            }

        bool AddEntityID
            {
            get;
            set;

            }

        int SortOrder
            {
            get;
            set;
            }

        string ControleID
            {
            get;
            set;

            }

        int EntityTypeID
            {
            get;
            set;
            }
        int AttributeGroupID
            {
            get;
            set;
            }
        #endregion
        }
    }
