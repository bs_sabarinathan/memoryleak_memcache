﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    /// <summary>
    /// FileProfileDao object for table 'DAM_ReformatProfile'.
    /// </summary>
    public partial class DamReformatProfileDao : BaseDao, ICloneable
    {

        #region Member Variables

        protected int _id;
        protected string _name;
        protected int _units;
        protected string _profileSrcValue;
      
        #endregion

        #region Constructors
        public DamReformatProfileDao() { }

        public DamReformatProfileDao(int id, string name, int pUnits,string  PprofileSrcValue)
        {
            this._name = name;
            this._id = id;
            this._units = pUnits;
            this._profileSrcValue = PprofileSrcValue;
            

        }

        public DamReformatProfileDao(int pId)
        {
            this._id = pId;
        }

        #endregion


        #region Public Properties


        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual string Name
        {
            get
            {
                return _name
                    ;
            }
            set { _name = value; }
        }

        public virtual string ProfileSrcValue
        {
            get { return _profileSrcValue; }
            set { _profileSrcValue = value; }
        }
        public virtual int Units
        {
            get { return _units; }
            set { _units = value; }
        }
        


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string DamReformatProfileDao = "DamReformatProfileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Units = "Units";
            public const string ProfileSrcValue = "ProfileSrcValue";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string DamReformatProfileDao = "DamReformatProfileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Units = "Units";
            public const string ProfileSrcValue = "ProfileSrcValue";
        }

        #endregion

    }
}





