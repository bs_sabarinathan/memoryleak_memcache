﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class EntityTypeAttributeRelation : IEntityTypeAttributeRelation
    {

        /// <summary>
        /// EntityPeriod object for table 'MM_EntityTypeAttributeRelation'.
        /// </summary>
        #region Member Variables
        protected int _id;
        protected int _entitytypeid;
        protected string _entitytypecaption;
        protected int _attributeid;
        protected string _attributecaption;
        protected int _attributetypeid;
        protected string _validationid;
        protected int _sortorder;
        protected string _defaultvalue;
        protected bool _inheritfromparent;
        protected bool _isspecial;
        protected bool _isreadonly;
        protected bool _choosefromparentonly;
        protected bool _isvalidationneeded;
        protected string _caption;
        protected bool _issystemdefined;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected string _placeholdervalue;
        protected int _minvalue;
        protected int _maxvalue;
        protected bool _ishelptextenabled;
        protected string _helptextdescription;
        #endregion

        #region Constructors
        public EntityTypeAttributeRelation() { }

        public EntityTypeAttributeRelation(int pID, int pEnitytypeId, int pAttributeId, string pValidationId, int pSortOrder, string pDefaultValue, bool pInheritFromParent, bool pIsReadOnly, bool pChooseFromParentOnly, bool pIsValidationNeeded, string pCaption, bool pIsSystemDefined, string pPlaceHolderValue, int MinValue, int MaxValue, bool pIsHelptextEnabled, string pHelptextDecsription)
        {
            this._id = pID;
            this._entitytypeid = pEnitytypeId;
            this._attributeid = pAttributeId;
            this._validationid = pValidationId;
            this._sortorder = pSortOrder;
            this._defaultvalue = pDefaultValue;
            this._inheritfromparent = pInheritFromParent;
            this._isreadonly = pIsReadOnly;
            this._choosefromparentonly = pChooseFromParentOnly;
            this._isvalidationneeded = pIsValidationNeeded;
            this._caption = pCaption;
            this._issystemdefined = pIsSystemDefined;
            this._placeholdervalue = pPlaceHolderValue;
            this._minvalue = MinValue;
            this._maxvalue = MaxValue;
         this._ishelptextenabled = pIsHelptextEnabled;
            this._helptextdescription = pHelptextDecsription;
        }

        public EntityTypeAttributeRelation(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }
        public string EntityTypeCaption
        {
            get { return _entitytypecaption; }
            set { _entitytypecaption = value; }
        }
        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public string AttributeCaption
        {
            get { return _attributecaption; }
            set { _attributecaption = value; }

        }
        public int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }

        }
        public string ValidationID
        {
            get { return _validationid; }
            set { _validationid = value; }

        }

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
        public virtual string DefaultValue
        {
            get { return _defaultvalue; }
            set { _defaultvalue = value; }

        }
        public virtual string PlaceHolderValue
        {
            get { return _placeholdervalue; }
            set { _placeholdervalue = value; }

        }
        public virtual int MinValue
        {
            get { return _minvalue; }
            set { _bIsChanged |= (_minvalue != value); _minvalue = value; }

        }
        public virtual int MaxValue
        {
            get { return _maxvalue; }
            set { _bIsChanged |= (_maxvalue != value); _maxvalue = value; }

        }
        public virtual bool IsHelptextEnabled
        {
            get { return _ishelptextenabled; }
            set { _bIsChanged |= (_ishelptextenabled != value); _ishelptextenabled = value; }
        }

        public virtual string HelptextDecsription
        {
            get { return _helptextdescription; }
            set { _bIsChanged |= (_helptextdescription != value); _helptextdescription = value; }
        }

        public virtual bool InheritFromParent
        {
            get { return _inheritfromparent; }
            set { _inheritfromparent = value; }

        }

        public virtual bool IsReadOnly
        {
            get { return _isreadonly; }
            set { _isreadonly = value; }

        }

        public virtual bool IsSpecial
        {
            get { return _isspecial; }
            set { _isspecial = value; }

        }

        public virtual bool ChooseFromParentOnly
        {
            get { return _choosefromparentonly; }
            set { _choosefromparentonly = value; }

        }

        public virtual bool IsValidationNeeded
        {
            get { return _isvalidationneeded; }
            set { _isvalidationneeded = value; }

        }
        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual bool IsSystemDefined
        {
            get { return _issystemdefined; }
            set { _issystemdefined = value; }

        }
        public dynamic ParentValue { get; set; }
        public dynamic ParentTreeLevelValueCaption { get; set; }
        public dynamic Lable { get; set; }
        public string strAttributeID { get; set; }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeAttributeRelation castObj = null;
            try
            {
                castObj = (EntityTypeAttributeRelation)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }
    internal class EntityTypeAttributeRelationwithLevels : EntityTypeAttributeRelation, IEntityTypeAttributeRelationwithLevels
    {
        public IList<ITreeLevel> Levels { get; set; }
        public IList<IOption> Options { get; set; }
        public IList<ITagOption> TagOptions { get; set; }
        public string tree { get; set; }
        public IList<DropDownTreePricing> DropDownPricing { get; set; }
    }
    public class TreeDropDownLabel : ITreeDropDownLabel
    {
        public int Level { get; set; }
        public string Label { get; set; }
    }

    public class DropDownTreePricing
    {
        public int id { get; set; }
        public string LevelName { get; set; }
        public int level { get; set; }
        public string[] selection { get; set; }
        public int TotalLevel { get; set; }
        public bool isperc { get; set; }
        public IList<IUIDropDownPricing> LevelOptions { get; set; }
    }

    public class UIDropDownPricing : IUIDropDownPricing
    {
        public int NodeId { get; set; }
        public string caption { get; set; }
        public int level { get; set; }
        public int LevelParent { get; set; }
        public string LevelKey { get; set; }
        public string value { get; set; }
    }

    public class TreeDropDownCaption : ITreeDropDownCaption
    {
        public int Level { get; set; }
        public string Caption { get; set; }
    }
}
