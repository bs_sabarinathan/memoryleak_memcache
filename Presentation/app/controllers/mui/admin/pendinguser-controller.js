﻿(function (ng, app) {
    "use strict"; function muiadminpendinguserCtrl($scope, $location, $resource, $timeout, AdminService) {
        $scope.pendinguserlistdata = {}; $scope.ApprovedRejectedlistdata = {}; AdminService.GetPendingUsers().then(function (res) { $scope.pendinguserlistdata = res.Response.m_Item1; $scope.ApprovedRejectedlistdata = res.Response.m_Item2; }); $(document).on('click', '.checkbox-custom > input[ng-model=chkall]', function (e) { var status = this.checked; $('#Pendinguerstable > tbody input:checkbox').each(function () { this.checked = status; if (status) { $(this).next('i').addClass('checked'); } else { $(this).next('i').removeClass('checked'); } }); });
        $scope.checkAllUsers = function () {
            if ($scope.chkall) {
                $scope.chkall = true;
        } else {
                $scope.chkall = false;
        }
        angular.forEach($scope.pendinguserlistdata, function (item) {
            item.Selected = $scope.chkall;
        });
    };
        $scope.RemoveCheckallSelection = function (list, $event) {
            var checkbox = $event.target;
            list.Selected = checkbox.checked;
            var Usercollection = $.grep($scope.pendinguserlistdata, function (e) {
                return e.Selected == true;
            }).length;
            if (Usercollection != $scope.pendinguserlistdata.length)
                $scope.chkall = false;
            else
                $scope.chkall = true;
        }
        $scope.ApproveRejectUser = function (status) { var statusupdate = {}; var selectedusers = ''; $('#Pendinguerstable tr').not(':eq(0)').filter(':has(:checkbox:checked)').find('td:eq(1)').each(function () { selectedusers = this.innerHTML + ',' + selectedusers; }); selectedusers = selectedusers.substring(0, selectedusers.length - 1); statusupdate.status = status; statusupdate.checkedusers = selectedusers; AdminService.ApproveRejectRegisteredUsers(statusupdate).then(function (getstatus) { var a = getstatus.Response; AdminService.GetPendingUsers().then(function (res) { $scope.pendinguserlistdata = res.Response.m_Item1; $scope.ApprovedRejectedlistdata = res.Response.m_Item2; }); }); }
    }
    app.controller("mui.admin.pendinguserCtrl", ['$scope', '$location', '$resource', '$timeout', 'AdminService', muiadminpendinguserCtrl]);
})(angular, app);