﻿using BrandSystems.Marcom.Core.Task.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Task
{
   internal class EntityTaskCheckList:IEntityTaskCheckList
    {
        #region Member Variables


        protected int _id;
        protected int _taskId;
        protected string _name;
        protected int _sortOrder;
        protected int _userId;
        protected int _ownerId;
        protected DateTime? _completedOn;
        protected bool _status;

        #endregion


        #region Constructors
        public EntityTaskCheckList() { }

        public EntityTaskCheckList(int tId, int tTaskId, string tName, int tSortOrder, int tUserId, int tOwnerId, DateTime? tCompletedOn, bool pstatus)
        {
            this._id = tId;
            this._taskId = tTaskId;
            this._name = tName;
            this._sortOrder = tSortOrder;
            this._userId = tUserId;
            this._ownerId = tOwnerId;
            this._completedOn = tCompletedOn;
            this._status = pstatus;
        }

        public EntityTaskCheckList(int tId)
        {
            this.Id = tId;
        }

        #endregion


        #region public properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int TaskId
        {
            get { return _taskId; }
            set { _taskId = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }

        }
        public int OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }
        public DateTime? CompletedOn
        {
            get { return _completedOn; }
            set { _completedOn = value; }

        }

        public bool Status
        {
            get { return _status; }
            set { _status = value; }

        }

        public string UserName { get; set; }
        public string OwnerName { get; set; }
        public string CompletedOnValue { get; set; }
        public bool IsExisting { get; set; }
        #endregion



        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTaskCheckList entityTaskCheckListObj = null;
            try
            {
                entityTaskCheckListObj = (EntityTaskCheckList)obj;
            }
            catch (Exception) { return false; }
            return (entityTaskCheckListObj != null) &&
                (this._id == entityTaskCheckListObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }


}

