﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $location, $templateCache, $cookies, $translate, CcdetailService, $modal) {
        setTimeout(function () { $scope.page_resize(); }, 100);
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName).replace(/\\/g, "\/");
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {

            $scope.reportPreviewImg = cloudpath + "/Files/ReportFiles/Images/Preview/";
        }
        else {
            $scope.reportPreviewImg = "/Files/ReportFiles/Images/Preview/";
        }

        var FilterID = 0;
        var timer;
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.Ganttenddate1 = false;
            model = model1;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.Ganttenddate1 = true;
        };
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        var CollectionIds = "";
        if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") {
            $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID));
        }
        CollectionIds = $.cookie('ListofEntityID').split(",");
        $scope.CurrentTrackingUrl = $stateParams.TrackID;
        $window.ListofEntityID = CollectionIds;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.CurrentCostcentreID = 0;
        $scope.Level = "0";
        $scope.ExpandingEntityID = '0';
        $scope.costcentreidforreport = '0';
        $scope.FilterType = 2;
        $scope.CostCentreEntityType = "";
        $scope.CurrentTreeLevel = 0;
        $scope.SelectedCostCentreCurrency = {
            TypeId: 0,
            Name: '',
            ShortName: '',
            Rate: 1
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detailCtrl']"));
        });
        $("#costcentredetailfilter").on('loadfiltersettings', function (event, TypeID) {
            $("#costcentredetailfiltersettings").trigger('loaddetailfiltersettings', 5);
        });
        $("#costcentredetailfilter").on('applyplandetailfilter', function (event, filterid, filtername, IsDetailFilter) {
            $("#costcentredetailfiltersettings").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        });
        $("#costcentredetailfilter").on('EditFilterSettingsByFilterID', function (event, filterid, typeid, filtertypeval) {
            $("#costcentredetailfiltersettings").trigger('EditFilterSettingsByFilterID', [filterid, typeid, filtertypeval]);
        });
        $("#costcentredetailfilter").on('ClearScope', function (event, filterid) {
            $("#costcentredetailfiltersettings").trigger('ClearScope', [filterid]);
        });
        $("#costcentredetailfilter").on('ClearAndReApply', function (event, filterid) {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.SelectedFilterType = 2;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        $scope.ListViewDetails = [{
            data: "",
            PageIndex: 0,
            UniqueID: "",
            StartRowNo: 0,
            MaxNoofRow: 30,
            FilterID: 0,
            SortOrderColumn: "null",
            IsDesc: false,
            IncludeChildren: false,
            EntityID: '0',
            Level: '0',
            IDArr: [],
            FilterAttributes: []
        }];
        $scope.ListTemplate = "";
        $scope.GanttViewDataPageTemplate = '';
        $scope.GanttViewBlockPageTemplate = '';
        $('#TreeCtrlCss').remove();
        var GlobalChildNode = new Array();
        GlobalChildNode = $window.ListofEntityID;
        $scope.EntityHighlight = false;
        if ($stateParams.ID != undefined && $stateParams.ID != 0) {
            GetCostCentreCurrencyRateById(parseInt($stateParams.ID), 0);
            $scope.ExpandingEntityID = $stateParams.ID;
            $scope.Level = "100";
            $scope.EntityHighlight = true;
        } else {
            GlobalChildNode = $window.ListofEntityID;
            $scope.Level = $window.TreeLevel == undefined ? "0" : $window.TreeLevel;
            if ($window.ListofEntityID.length == 1 && $window.TotalChildrenForSelecedCC == 0) {
                GetCostCentreCurrencyRateById($window.ListofEntityID[0], 0);
            } else {
                $scope.SelectedCostCentreCurrency.Rate = 1;
                $scope.SelectedCostCentreCurrency.ShortName = $scope.DefaultSettings.CurrencyFormat.Name;
                $scope.SelectedCostCentreCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
                $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
                CcdetailService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                    $scope.CurrencyFormatsList = CurrencyListResult.Response;
                });
            }
            GetTreeCount();
        }
        var FilterObj = [];
        $("#EntitiesTree").on("loadactivityfromfilterforCostCenter", function (event, filterid, ApplyFilterobj, filtertype) {
            if (filtertype == undefined) $scope.FilterType = 2;
            $scope.FilterType = filtertype;
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: "",
                StartRowNo: 0,
                MaxNoofRow: 30,
                FilterID: 0,
                SortOrderColumn: "null",
                IsDesc: false,
                IncludeChildren: false,
                EntityID: '0',
                Level: '0',
                IDArr: [],
                FilterAttributes: []
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            if (filterid != undefined) {
                FilterID = filterid;
            } else {
                FilterID = 0;
            }
            FilterObj = ApplyFilterobj;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) $scope.ExpandingEntityID = $stateParams.ID;
            else GlobalChildNode = $window.ListofEntityID;
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        });
        $scope.loadcostcentrefromsearchByID = function (id, level) {
            $scope.Activity.IsActivitySectionLoad = false;
            $scope.Level = level;
            FilterID = 0;
            FilterObj = [];
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: "",
                StartRowNo: 0,
                MaxNoofRow: 30,
                FilterID: 0,
                SortOrderColumn: "null",
                IsDesc: false,
                IncludeChildren: false,
                EntityID: '0',
                Level: '0',
                IDArr: [],
                FilterAttributes: []
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            GlobalChildNode = new Array();
            GlobalChildNode.push(id)
            $scope.ExpandingEntityID = '0';
            $scope.EntityHighlight = true
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        }

        function GetTreeCount() {
            GetCostCenterEntityType();
            if ($scope.FilterType == undefined) $scope.FilterType = 2;
            $('#EntitiesTree').scrollTop(0);
            $scope.TreeClassList = [];
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = FilterID;
            Node.SortOrderColumn = "null";
            Node.IsDesc = false;
            Node.IncludeChildren = true;
            Node.EntityID = '0';
            Node.Level = $scope.Level;
            Node.ExpandingEntityID = $scope.ExpandingEntityID;
            Node.FilterType = $scope.FilterType;
            Node.IDArr = GlobalChildNode;
            Node.FilterAttributes = FilterObj;
            $scope.NodeForward = Node;
            CcdetailService.CostCentreDetail(Node).then(function (NodeCnt) {
                if (NodeCnt.Response != null && NodeCnt.Response.Data != null) {
                    fnPageTemplate(parseInt(NodeCnt.Response.DataCount));
                    if (NodeCnt.Response.Data != null) {
                        $scope.Tempattrgroupfilter = [];
                        var TreeItem = "";
                        var CostCentreID = 0;
                        var Constructunique = '';
                        if ($scope.FilterType == 6) {
                            var optID = "";
                            var totcnt = 0;
                            for (var i = 0; i < NodeCnt.Response.Data.length; i++) {
                                if (optID != NodeCnt.Response.Data[i].OptionCaption) {
                                    totcnt = $.grep(NodeCnt.Response.Data, function (e) {
                                        return e.OptionID == NodeCnt.Response.Data[i].OptionID
                                    });
                                    $scope.Tempattrgroupfilter.push({
                                        1: "",
                                        ApprovedAllocatedAmount: 0,
                                        ApprovedBudget: 0,
                                        ApprovedSubAllocatedAmount: 0,
                                        Available: 0,
                                        BudgetDeviation: 0,
                                        ColorCode: "000000",
                                        Commited: 0,
                                        CostCenterID: 0,
                                        EntityID: 0,
                                        EntityStateID: 0,
                                        Id: 0,
                                        InRequest: 0,
                                        IsLock: true,
                                        Level: -1,
                                        MileStone: "<root/>",
                                        Name: NodeCnt.Response.Data[i]["OptionCaption"],
                                        ParentID: 0,
                                        Period: "<root/>",
                                        Permission: 0,
                                        PlannedAmount: 0,
                                        ShortDescription: "G",
                                        Spent: 0,
                                        Status: "",
                                        TempPeriod: "-",
                                        TotalChildrenCount: totcnt.length + 1,
                                        TypeID: 0,
                                        TypeName: "@Option",
                                        UniqueKey: "0" + i,
                                        class: "1" + "0" + i
                                    });
                                }
                                $scope.Tempattrgroupfilter.push({
                                    1: NodeCnt.Response.Data[i]["1"],
                                    ApprovedAllocatedAmount: parseInt(NodeCnt.Response.Data[i]["ApprovedAllocatedAmount"], 10),
                                    ApprovedBudget: parseInt(NodeCnt.Response.Data[i]["ApprovedBudget"], 10),
                                    ApprovedSubAllocatedAmount: parseInt(NodeCnt.Response.Data[i]["ApprovedSubAllocatedAmount"], 10),
                                    Available: parseInt(NodeCnt.Response.Data[i]["Available"], 10),
                                    BudgetDeviation: parseInt(NodeCnt.Response.Data[i]["BudgetDeviation"], 10),
                                    ColorCode: NodeCnt.Response.Data[i]["ColorCode"],
                                    Commited: parseInt(NodeCnt.Response.Data[i]["Commited"], 10),
                                    CostCenterID: parseInt(NodeCnt.Response.Data[i]["CostCenterID"], 10),
                                    EntityID: parseInt(NodeCnt.Response.Data[i]["EntityID"], 10),
                                    EntityStateID: parseInt(NodeCnt.Response.Data[i]["EntityStateID"], 10),
                                    Id: parseInt(NodeCnt.Response.Data[i]["InRequest"], 10),
                                    InRequest: parseInt(NodeCnt.Response.Data[i]["1"], 10),
                                    IsLock: NodeCnt.Response.Data[i]["IsLock"],
                                    Level: parseInt(NodeCnt.Response.Data[i]["Level"], 10),
                                    MileStone: NodeCnt.Response.Data[i]["MileStone"],
                                    Name: NodeCnt.Response.Data[i]["Name"],
                                    ParentID: parseInt(NodeCnt.Response.Data[i]["ParentID"], 10),
                                    Period: NodeCnt.Response.Data[i]["Period"],
                                    Permission: NodeCnt.Response.Data[i]["Permission"],
                                    PlannedAmount: parseInt(NodeCnt.Response.Data[i]["PlannedAmount"], 10),
                                    ShortDescription: NodeCnt.Response.Data[i]["ShortDescription"],
                                    Spent: parseInt(NodeCnt.Response.Data[i]["Spent"], 10),
                                    Status: NodeCnt.Response.Data[i]["Status"],
                                    TempPeriod: NodeCnt.Response.Data[i]["TempPeriod"],
                                    TotalChildrenCount: NodeCnt.Response.Data[i]["TotalChildrenCount"],
                                    TypeID: NodeCnt.Response.Data[i]["TypeID"],
                                    TypeName: NodeCnt.Response.Data[i]["TypeName"],
                                    UniqueKey: NodeCnt.Response.Data[i]["UniqueKey"],
                                    class: NodeCnt.Response.Data[i]["class"]
                                });
                                optID = NodeCnt.Response.Data[i].OptionCaption;
                            }
                            NodeCnt.Response.Data = $scope.Tempattrgroupfilter;
                            if ($scope.ListViewDetails[0].data == "") {
                                $scope.ListViewDetails[0].data = NodeCnt;
                                $scope.ListViewDetails[0].PageIndex = 0;
                                $scope.ListViewDetails[0].UniqueID = "";
                                $scope.ListViewDetails[0].StartRowNo = 0;
                                $scope.ListViewDetails[0].MaxNoofRow = 30;
                                $scope.ListViewDetails[0].FilterID = FilterID;
                                $scope.ListViewDetails[0].SortOrderColumn = "null";
                                $scope.ListViewDetails[0].IsDesc = false;
                                $scope.ListViewDetails[0].IncludeChildren = true;
                                $scope.ListViewDetails[0].EntityID = '0';
                                $scope.ListViewDetails[0].Level = $scope.Level;
                                $scope.ListViewDetails[0].IDArr = GlobalChildNode;
                                $scope.ListViewDetails[0].FilterAttributes = FilterObj;
                            } else {
                                $scope.ListViewDetails.push({
                                    data: NodeCnt,
                                    PageIndex: 0,
                                    UniqueID: "",
                                    StartRowNo: 0,
                                    MaxNoofRow: 30,
                                    FilterID: FilterID,
                                    SortOrderColumn: "null",
                                    IsDesc: false,
                                    IncludeChildren: true,
                                    EntityID: '0',
                                    Level: $scope.Level,
                                    IDArr: GlobalChildNode,
                                    FilterAttributes: FilterObj
                                });
                            }
                            $scope.ParenttreeList = $scope.Tempattrgroupfilter;
                            var rootID = 0;
                            for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                var UniqueId = '';
                                var IscostCenter = false;
                                var ExtraSpace = "<i class='icon-'></i>";
                                CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                                UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"]);
                                var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                                var ClassName = GenateClass(GeneralUniquekey);
                                ClassName += " mo" + UniqueId;
                                if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                    IscostCenter = true;
                                    ExtraSpace = "";
                                }
                                TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                if ($scope.ParenttreeList[i]["TypeName"] != "@Option") TreeItem += "<i class='icon-'></i>";
                                TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["UniqueKey"], 0);
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    var j = i + 1;
                                    if (j < $scope.ParenttreeList.length && $scope.ParenttreeList[j]["ParentID"] != 0) $scope.ParenttreeList[i]["TotalChildrenCount"] = 1;
                                }
                                if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                    if (UniqueId.split("-").length <= $scope.Level || $scope.ParenttreeList[i]["ParentID"] == 0 || parseInt($scope.Level) == 100 || (i == 0)) {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    } else {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    }
                                } else {
                                    TreeItem += "<i class='icon-'></i>";
                                }
                                TreeItem += " <span style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span  data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-name='text' data-CostCentreIDForReport=" + CostCentreID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                if (parseInt(TreeVal[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + TreeVal[i].Permission + " data-typeID=" + TreeVal[i]["TypeID"] + " data-entityID=" + TreeVal[i]["Id"] + "></i>";
                                TreeItem += "</a> </li>";
                            }
                        } else {
                            if ($scope.ListViewDetails[0].data == "") {
                                $scope.ListViewDetails[0].data = NodeCnt;
                                $scope.ListViewDetails[0].PageIndex = 0;
                                $scope.ListViewDetails[0].UniqueID = "";
                                $scope.ListViewDetails[0].StartRowNo = 0;
                                $scope.ListViewDetails[0].MaxNoofRow = 30;
                                $scope.ListViewDetails[0].FilterID = FilterID;
                                $scope.ListViewDetails[0].SortOrderColumn = "null";
                                $scope.ListViewDetails[0].IsDesc = false;
                                $scope.ListViewDetails[0].IncludeChildren = true;
                                $scope.ListViewDetails[0].EntityID = '0';
                                $scope.ListViewDetails[0].Level = $scope.Level;
                                $scope.ListViewDetails[0].IDArr = GlobalChildNode;
                                $scope.ListViewDetails[0].FilterAttributes = FilterObj;
                            } else {
                                $scope.ListViewDetails.push({
                                    data: NodeCnt,
                                    PageIndex: 0,
                                    UniqueID: "",
                                    StartRowNo: 0,
                                    MaxNoofRow: 30,
                                    FilterID: FilterID,
                                    SortOrderColumn: "null",
                                    IsDesc: false,
                                    IncludeChildren: true,
                                    EntityID: '0',
                                    Level: $scope.Level,
                                    IDArr: GlobalChildNode,
                                    FilterAttributes: FilterObj
                                });
                            }
                            $scope.LoadCostCentreGantHeader($scope.ListViewDetails[0].IDArr);
                            $scope.ParenttreeList = NodeCnt.Response.Data;
                            var rootID = 0;
                            for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                var UniqueId = '';
                                var IscostCenter = false;
                                var ExtraSpace = "<i class='icon-'></i>";
                                CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                                if (rootID == 0) {
                                    rootID = parseInt($scope.ParenttreeList[i]["class"]);
                                    $scope.CurrentTreeLevel = parseInt($scope.ParenttreeList[i]["Level"]);
                                } else if (parseInt($scope.ParenttreeList[i]["class"].split(".")[0]) != rootID) {
                                    rootID = parseInt($scope.ParenttreeList[i]["class"]);
                                    $scope.CurrentTreeLevel = parseInt($scope.ParenttreeList[i]["Level"]);
                                }
                                if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                    ExtraSpace = "";
                                    IscostCenter = true;
                                }
                                UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"]);
                                var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                                var ClassName = GenateClass(GeneralUniquekey);
                                ClassName += " mo" + UniqueId;
                                TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    var j = i + 1;
                                    if (j < $scope.ParenttreeList.length && $scope.ParenttreeList[j]["ParentID"] != 0) $scope.ParenttreeList[i]["TotalChildrenCount"] = 1;
                                }
                                if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                    if ($scope.ParenttreeList[i]["TypeID"] == 5 && $scope.ParenttreeList[i]["ParentID"] == 0 || parseInt($scope.Level) == 100 || i == 0) {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    } else {
                                        TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                    }
                                } else {
                                    TreeItem += "<i class='icon-'></i>";
                                }
                                TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-CostCentreIDForReport=" + CostCentreID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                TreeItem += "</a> </li>";
                            }
                        }
                        var LoadedPage = 0;
                        $scope.$broadcast('onTreePageCreation', [NodeCnt, 0, ""]);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append($compile(TreeItem)($scope));
                        $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done').removeAttr('style');
                        if (FilterID > 0 || FilterObj.length > 0) {
                            if ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length == 0) {
                                $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').parent().css('display', 'none');
                                $('#EntitiesTree li[data-page=' + LoadedPage + ']').css('display', 'none');
                                clearTimeout(timer);
                                timer = setTimeout(function () {
                                    StartPageLoad(false)
                                }, 1);
                            }
                            if (($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length > 0) && ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length < 10)) {
                                clearTimeout(timer);
                                timer = setTimeout(function () {
                                    StartPageLoad(false)
                                }, 1);
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li[data-CostCentreID=" + $scope.CurrentCostcentreID + "] a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        if (SearchEntityID != 0) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + SearchEntityID + '/overview');
                            SearchEntityID = 0;
                        }
                    }
                } else {
                    $("#GanttDataContainer").html("");
                    $("#GanttBlockContainer").html("");
                    $("#EntitiesTree").html("");
                    $("#ListHolder").html("");
                    if (FilterID > 0 || FilterObj.length > 0) {
                        $(window).AdjustHeightWidth();
                    }
                }
                $scope.disableShowAll = false;
            });
            $(window).AdjustHeightWidth();
        }
        $scope.LoadCostCentreGantHeader = function (id) {
            var EntityIDs = [];
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            CcdetailService.GetEntitysLinkedToCostCenter(id).then(function (GetEntityIds) {
                if (GetEntityIds.Response.length != null) {
                    for (var i = 0; i < GetEntityIds.Response.length; i++) {
                        EntityIDs.push(GetEntityIds.Response[i].EntityID);
                    }
                    CcdetailService.GetPeriodByIdForGantt(EntityIDs.join(",") == "" ? "0" : EntityIDs.join(",")).then(function (GantRstl) {
                        if (GantRstl.Response.length != null) {
                            for (var i = 0; i < GantRstl.Response.length; i++) {
                                datStartUTCval = GantRstl.Response[i].Startdate.substr(6, (GantRstl.Response[i].Startdate.indexOf('+') - 6));
                                datstartval.push(new Date.create(parseInt(datStartUTCval)));
                                datEndUTCval = GantRstl.Response[i].EndDate.substr(6, (GantRstl.Response[i].EndDate.indexOf('+') - 6));
                                datendval.push(new Date.create(parseInt(datEndUTCval)));
                            }
                            maxstartDates = ConvertDateToString(new Date.create(Math.min.apply(null, datstartval)));
                            maxendDates = ConvertDateToString(new Date.create(Math.max.apply(null, datendval)));
                            if (maxstartDates != "") start = maxstartDates;
                            if (maxendDates != "") end = maxendDates;
                            if (end == "NaN-NaN-NaN") {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');
                            }
                            var currentYear = new Date.create().getFullYear();
                            var strtDate = maxstartDates.substring(0, 4);
                            var endingDate = maxendDates.substring(0, 4);
                            if (currentYear < endingDate) {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');;
                            } else {
                                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                                    day: 1,
                                    month: 0
                                }).toString('yyyy-MM-dd');
                                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                                    day: 31,
                                    month: 11
                                }).toString('yyyy-MM-dd');
                            }
                            if (start == "NaN-NaN-NaN" || start == "") start = Date.parse('-1year').set({
                                day: 1,
                                month: 0
                            }).toString('yyyy-MM-dd');
                            if (end == "NaN-NaN-NaN" || end == "") end = Date.parse('+1year').set({
                                day: 31,
                                month: 11
                            }).toString('yyyy-MM-dd');
                            start = Date.parse(start);
                            end = Date.parse(end);
                            var dates = [];
                            dates[start.getFullYear()] = [];
                            dates[start.getFullYear()][start.getMonth()] = [start];
                            var last = start;
                            while (last.compareTo(end) == -1) {
                                var next = last.clone().addDays(1);
                                if (!dates[next.getFullYear()]) {
                                    dates[next.getFullYear()] = [];
                                }
                                if (!dates[next.getFullYear()][next.getMonth()]) {
                                    dates[next.getFullYear()][next.getMonth()] = [];
                                }
                                dates[next.getFullYear()][next.getMonth()].push(next);
                                last = next;
                            }
                            $scope.dates = dates;
                            return dates;
                        }
                    });
                }
            });
        }
        var TreePageLoadTimer = undefined;

        function fnPageTemplate(ItemCnt) {
            $scope.GanttViewDataPageTemplate = "";
            $scope.GanttViewBlockPageTemplate = "";
            var PageSize = 1;
            var itemsPerPage = 30;
            if (ItemCnt > itemsPerPage) {
                PageSize = ItemCnt / itemsPerPage;
            }
            var TreeTemplate = '';
            var ListTemplate = '';
            var height = 34 * 30;
            for (var i = 0; i < PageSize; i++) {
                TreeTemplate += "<li class='pending listviewBGDiv' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list CCnav-list nav-list-menu'></ul></li>"
                ListTemplate += "<tbody class='pending listviewBGDiv' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>";
                $scope.GanttViewDataPageTemplate += '<tbody class="widthcent"  data-page="' + i + '"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>';
                $scope.GanttViewBlockPageTemplate += '<div data-page="' + i + '" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            }
            if (TreeTemplate.length > 0) {
                $scope.GanttViewDataPageTemplate += '<tbody class="blankTreeNode"></tbody>';
                $scope.ListTemplate = ListTemplate;
                $("#EntitiesTree").trigger('onListViewTemplateCreation', [ListTemplate]);
                $("#EntitiesTree").html($compile(TreeTemplate + '<li class="blankTreeNode"></li>')($scope));
            }
        }

        function GetCostCenterEntityType() {
            CcdetailService.GetEntityTypeByID(5).then(function (GetCostCentreEnityType) {
                if (GetCostCentreEnityType.Response != null) {
                    $scope.CostCentreEntityType = GetCostCentreEnityType.Response;
                }
            });
        }
        
        var pageQueue = [];
        $('#EntitiesTree').scroll(function () {
            $("#ListHolder").scrollTop($(this).scrollTop());
            $("#GanttDataContainer").scrollTop($(this).scrollTop());
            $timeout.cancel(timer);
            timer = $timeout(function () {
                StartPageLoad(false)
            }, 150);
        });
        $(window).scroll(function () {
            $timeout.cancel(timer);
            timer = $timeout(function () {
                StartPageLoad(true)
            }, 30);
        });

        function StartPageLoad(windowScroll) {
            var obj = $('#EntitiesTree');
            var areaHeight = 0;
            var areaTop = 0;
            if (windowScroll) {
                areaHeight = $(window).height();
                areaTop = $(window).scrollTop();
            } else {
                areaHeight = $(obj).height();
                areaTop = $(obj).position().top;
            }
            $('li.pending', obj).each(function () {
                var top = $(this).position().top - areaTop;
                var height = $(this).height();
                if (top + height + height < 0) { } else if (top > areaHeight) { } else {
                    $(this).removeClass('pending').addClass('inprogress');
                    pageQueue.push($(this).attr('data-page'));
                }
            });
        }
        $timeout(function () { LoadPageContent() }, 30);

        function LoadPageContent() {
            var PageNo = pageQueue.pop();
            if (PageNo == undefined) {
                setTimeout(LoadPageContent, 30);
            } else {
                var StartRowNo = PageNo * 30;
                var MaxNoofRow = 30;
                $timeout.cancel(recall);
                var recall = $timeout(function () {
                    TreeLoadPage(PageNo, StartRowNo, MaxNoofRow);
                }, 100);
            }
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = [];

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function Space(UniKey, ParentLevel) {
            var substr = UniKey.split('.');
            var itag = "";
            var row = (substr.length - ParentLevel);
            if (row > 0) {
                for (var i = 1; i < row; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }

        function TreeSpace(SpaceLength) {
            var itag = "";
            if (SpaceLength > 0) {
                for (var i = 1; i < SpaceLength; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }

        function LoadChildTreeNodes(data, ChildCount, UniqueId, CostCenterId) {
            if (ChildCount > 0) {
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right");
                    Collapse(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false");
                    $('#EntitiesTree').scroll();
                } else {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down");
                    Expand(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true");
                }
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") {
                    $scope.ExpandingEntityID = data;
                    TreeLoad(true, UniqueId, CostCenterId, false);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false");
                }
            }
        }
        var PathforBreadCrum = "";
        $scope.array = [];

        function TreeLoad(ParentOrChild, IdUnique, CostCenterId, CostCentrecreation) {
            var StartRowNo = 0;
            var MaxNoofRow = 30;
            var TreeItem = "";
            var ChildInclude = true;
            TreeItem = "";
            var Constructunique = IdUnique.substr(0, IdUnique.indexOf("-")) + ".";
            if (CostCentrecreation == false) {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = false;
                parentNode.EntityID = CostCenterId;
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
                parentNode.FilterType = 2;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = FilterObj;
                var LoadedPage = 0;
                CcdetailService.CostCentreDetail(parentNode).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: LoadedPage,
                            UniqueID: IdUnique,
                            StartRowNo: StartRowNo,
                            MaxNoofRow: MaxNoofRow,
                            FilterID: 0,
                            SortOrderColumn: "null",
                            IsDesc: false,
                            IncludeChildren: false,
                            EntityID: CostCenterId,
                            Level: $scope.Level,
                            IDArr: GlobalChildNode,
                            FilterAttributes: []
                        });
                        var CostCentreID = '0';
                        var IsCostCentre = false;
                        var rootID = 0;
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var ExtraSpace = "<i class='icon-'></i>";
                            var IscostCenter = false;
                            var UniqueId = '';
                            CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                            if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                ExtraSpace = "";
                                IscostCenter = true;
                            }
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var ClassName = GenateClass(GeneralUniquekey);
                            ClassName += " mo" + UniqueId;
                            TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre=" + IscostCenter + " data-EntityTypeID=" + $scope.ParenttreeList[i]["TypeID"] + "  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                            if (ParentNode.Response.Data[i]["TotalChildrenCount"] > 0) {
                                TreeItem += "<i data-role='Arrow' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + ParentNode.Response.Data[i]["Id"] + " data-TotalChildrenCount=" + ParentNode.Response.Data[i]["TotalChildrenCount"] + " data-Permission=" + ParentNode.Response.Data[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            TreeItem += "</a> </li>";
                        }
                        $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        if (CostCentrecreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                    }
                });
            } else {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = true;
                parentNode.EntityID = CostCenterId;
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
                parentNode.FilterType = 2;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = FilterObj;
                var LoadedPage = 0;
                CcdetailService.CostCentreTreeDetail(parentNode).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: LoadedPage,
                            UniqueID: IdUnique,
                            StartRowNo: StartRowNo,
                            MaxNoofRow: MaxNoofRow,
                            FilterID: 0,
                            SortOrderColumn: "null",
                            IsDesc: false,
                            IncludeChildren: false,
                            EntityID: CostCenterId,
                            Level: $scope.Level,
                            IDArr: GlobalChildNode,
                            FilterAttributes: []
                        });
                        var CostCentreID = '0';
                        var IsCostCentre = false;
                        var rootID = 0;
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var ExtraSpace = "<i class='icon-'></i>";
                            var IscostCenter = false;
                            var UniqueId = '';
                            CostCentreID = $scope.ParenttreeList[i]["CostCenterID"];
                            if ($scope.ParenttreeList[i]["TypeID"] == 5) {
                                ExtraSpace = "";
                                IscostCenter = true;
                            }
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var ClassName = GenateClass(GeneralUniquekey);
                            ClassName += " mo" + UniqueId;
                            TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId.toString() + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscostcentre=" + IscostCenter + " data-EntityTypeID=" + $scope.ParenttreeList[i]["TypeID"] + "  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["class"], $scope.CurrentTreeLevel);
                            if (ParentNode.Response.Data[i]["TotalChildrenCount"] > 0) {
                                TreeItem += "<i data-role='Arrow' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + ParentNode.Response.Data[i]["Id"] + " data-TotalChildrenCount=" + ParentNode.Response.Data[i]["TotalChildrenCount"] + " data-Permission=" + ParentNode.Response.Data[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if (parseInt($scope.ParenttreeList[i]["TypeID"]) == 5) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            TreeItem += "</a> </li>";
                        }
                        $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        if (CostCentrecreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                    }
                });
            }
            $(window).AdjustHeightWidth();
        }
        $(document).click(function () {
            $('#moduleContextmenu').hide();
        })
        $("#treeSettings").click(function (e) {
            if ($scope.disableShowAll == false) {
                $(window).AdjustHeightWidth();
                var TargetControl = $(e.target);
                $scope.muiiscalender.Iscalender = false;
                $scope.muiiscalender.CalenderID = 0;
                $scope.EntityLevelfrFin.levelfrFinancial = TargetControl.attr('data-EntityLevel');
                var col = "";
                if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') {
                    $scope.Level = TargetControl.parent().attr('data-entitylevel');
                    LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'), TargetControl.attr('data-costcentreid'))
                } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') {
                    $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                    $window.GlobalUniquekey = TargetControl.parents('li').attr('data-uniquekey');
                    $(window).trigger("GloblaCCUniqueKeyAccess", [$window.GlobalUniquekey]);
                    $scope.CurrentCostcentreID = TargetControl.attr('data-entityID');
                    $timeout(function () {
                        if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                            $scope.entitytpesdata = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == 5 && e.EntityTypeAccessIsEnabled == true
                            });
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        } else if ($scope.ShowDeleteOption == true || $scope.ShowDeleteOption == true) {
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        }
                    }, 50);
                } else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                    $scope.costcentreidforreport = $(TargetControl).children("span").eq(1).attr('data-costcentreidforreport');
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCostcentreID = $(TargetControl).parents('li').attr('data-CostCentreID');
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    localScope.$apply(function () {
                        var id = TargetControl.attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey');
                        $scope.EntityTypeID = TargetControl.attr('data-entitytypeid');
                        if (TargetControl.attr('data-iscostcentre') == "true")
                            $location.path('/mui/planningtool/costcentre/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                        else {
                            //$location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID'));
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        }
                    });
                    $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') {
                    $scope.costcentreidforreport = $(TargetControl).attr('data-costcentreidforreport');
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCostcentreID = $(TargetControl).parents('li').attr('data-CostCentreID');
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    $scope.EntityTypeID = TargetControl.parent().attr('data-entitytypeid');
                    localScope.$apply(function () {
                        var id = TargetControl.parent().attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.next().attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().parent().attr('data-uniquekey');
                        if (TargetControl.parent().attr('data-iscostcentre') == "true")
                            $location.path('/mui/planningtool/costcentre/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview');
                        else {
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.parent().attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TTargetControl.parent().attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        }
                        // $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + TargetControl.parent().attr('data-entityID'));
                    });
                }
                $(window).AdjustHeightWidth();
            }
        });

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="TreeCtrlCss">';
            $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }

        function TreeLoadPage(PageNo, StartRowNo, MaxNoofRow) {
            var TreeItem = "";
            var ChildInclude = true;
            var Constructunique = '';
            var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = 0;
            parentNode.SortOrderColumn = "null";
            parentNode.IsDesc = false;
            parentNode.IncludeChildren = true;
            parentNode.EntityID = '0';
            parentNode.Level = $scope.Level;
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
            parentNode.FilterType = $scope.FilterType;
            parentNode.IDArr = GlobalChildNode;
            parentNode.FilterAttributes = FilterObj;
            CcdetailService.CostCentreDetail(parentNode).then(function (ParentNode) {
                var optID = "";
                var totcnt = 0;
                var IsAppendVal = false;
                var preval = "";
                if ($scope.FilterType == 6) {
                    $scope.Tempattrgroupfilter = [];
                    for (var i = 0; i < ParentNode.Response.Data.length; i++) {
                        IsAppendVal = false;
                        preval = "";
                        if (optID != ParentNode.Response.Data[i].OptionCaption) {
                            totcnt = $.grep(ParentNode.Response.Data, function (e) {
                                return e.OptionID == ParentNode.Response.Data[i].OptionID
                            });
                            for (var x = 0; x < $scope.ListViewDetails.length; x++) {
                                preval = $.grep($scope.ListViewDetails[x].data.Response.Data, function (e) {
                                    return e.Name == ParentNode.Response.Data[i]["OptionCaption"]
                                });
                                if (preval.length > 0) {
                                    IsAppendVal = true;
                                    break;
                                }
                            }
                            if (IsAppendVal == false) {
                                $scope.Tempattrgroupfilter.push({
                                    1: "",
                                    ApprovedAllocatedAmount: 0,
                                    ApprovedBudget: 0,
                                    ApprovedSubAllocatedAmount: 0,
                                    Available: 0,
                                    BudgetDeviation: 0,
                                    ColorCode: "000000",
                                    Commited: 0,
                                    CostCenterID: 0,
                                    EntityID: 0,
                                    EntityStateID: 0,
                                    Id: 0,
                                    InRequest: 0,
                                    IsLock: true,
                                    Level: -1,
                                    MileStone: "<root/>",
                                    Name: ParentNode.Response.Data[i]["OptionCaption"],
                                    ParentID: 0,
                                    Period: "<root/>",
                                    Permission: 0,
                                    PlannedAmount: 0,
                                    ShortDescription: "G",
                                    Spent: 0,
                                    Status: "",
                                    TempPeriod: "-",
                                    TotalChildrenCount: totcnt.length + 1,
                                    TypeID: 0,
                                    TypeName: "@Option",
                                    UniqueKey: "-0" + i,
                                    class: "1.3" + i
                                });
                            }
                        }
                        $scope.Tempattrgroupfilter.push({
                            1: ParentNode.Response.Data[i]["1"],
                            ApprovedAllocatedAmount: parseInt(ParentNode.Response.Data[i]["ApprovedAllocatedAmount"], 10),
                            ApprovedBudget: parseInt(ParentNode.Response.Data[i]["ApprovedBudget"], 10),
                            ApprovedSubAllocatedAmount: parseInt(ParentNode.Response.Data[i]["ApprovedSubAllocatedAmount"], 10),
                            Available: parseInt(ParentNode.Response.Data[i]["Available"], 10),
                            BudgetDeviation: parseInt(ParentNode.Response.Data[i]["BudgetDeviation"], 10),
                            ColorCode: ParentNode.Response.Data[i]["ColorCode"],
                            Commited: parseInt(ParentNode.Response.Data[i]["Commited"], 10),
                            CostCenterID: parseInt(ParentNode.Response.Data[i]["CostCenterID"], 10),
                            EntityID: parseInt(ParentNode.Response.Data[i]["EntityID"], 10),
                            EntityStateID: parseInt(ParentNode.Response.Data[i]["EntityStateID"], 10),
                            Id: parseInt(ParentNode.Response.Data[i]["InRequest"], 10),
                            InRequest: parseInt(ParentNode.Response.Data[i]["1"], 10),
                            IsLock: ParentNode.Response.Data[i]["IsLock"],
                            Level: parseInt(ParentNode.Response.Data[i]["Level"], 10),
                            MileStone: ParentNode.Response.Data[i]["MileStone"],
                            Name: ParentNode.Response.Data[i]["Name"],
                            ParentID: parseInt(ParentNode.Response.Data[i]["ParentID"], 10),
                            Period: ParentNode.Response.Data[i]["Period"],
                            Permission: ParentNode.Response.Data[i]["Permission"],
                            PlannedAmount: parseInt(ParentNode.Response.Data[i]["PlannedAmount"], 10),
                            ShortDescription: ParentNode.Response.Data[i]["ShortDescription"],
                            Spent: parseInt(ParentNode.Response.Data[i]["Spent"], 10),
                            Status: ParentNode.Response.Data[i]["Status"],
                            TempPeriod: ParentNode.Response.Data[i]["TempPeriod"],
                            TotalChildrenCount: ParentNode.Response.Data[i]["TotalChildrenCount"],
                            TypeID: ParentNode.Response.Data[i]["TypeID"],
                            TypeName: ParentNode.Response.Data[i]["TypeName"],
                            UniqueKey: ParentNode.Response.Data[i]["UniqueKey"],
                            class: ParentNode.Response.Data[i]["class"]
                        });
                        optID = ParentNode.Response.Data[i].OptionCaption;
                    }
                    $scope.ParenttreeNewList = $scope.Tempattrgroupfilter;
                    ParentNode.Response.Data = $scope.Tempattrgroupfilter;
                } else $scope.ParenttreeNewList = ParentNode.Response.Data;
                $scope.ParenttreeNewList = ParentNode.Response.Data;
                if ($scope.ParenttreeNewList != null) {
                    $scope.ListViewDetails.push({
                        data: ParentNode,
                        PageIndex: PageNo,
                        UniqueID: "",
                        StartRowNo: StartRowNo,
                        MaxNoofRow: MaxNoofRow,
                        FilterID: 0,
                        SortOrderColumn: "null",
                        IsDesc: false,
                        IncludeChildren: true,
                        EntityID: '0',
                        Level: $scope.Level,
                        IDArr: GlobalChildNode,
                        FilterAttributes: []
                    });
                    var CostCentreID = '0';
                    for (var i = 0; i < $scope.ParenttreeNewList.length; i++) {
                        var UniqueId = '';
                        var IscostCenter = false;
                        var ExtraSpace = "<i class='icon-'></i>";
                        CostCentreID = $scope.ParenttreeNewList[i]["CostCenterID"];
                        var rootID = 0;
                        if ($scope.ParenttreeNewList[i]["TypeID"] == 5) {
                            ExtraSpace = "";
                            IscostCenter = true;
                        }
                        UniqueId = UniqueKEY($scope.ParenttreeNewList[i]["class"]);
                        var GeneralUniquekey = UniqueKEY($scope.ParenttreeNewList[i]["class"].toString());
                        var ClassName = GenateClass(GeneralUniquekey);
                        ClassName += " mo" + UniqueId;
                        TreeItem += "<li data-over='true' data-CostCentreID=" + CostCentreID + "  class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-role='Activity' data-iscostcentre='" + IscostCenter + "' data-EntityTypeID='" + $scope.ParenttreeNewList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + " data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\"> ";
                        TreeItem += ExtraSpace + Space($scope.ParenttreeNewList[i]["UniqueKey"], 0);
                        if ($scope.ParenttreeNewList[i]["TotalChildrenCount"] > 0) {
                            var level = ($scope.Level == 0 ? 2 : $scope.Level);
                            if (GeneralUniquekey.split("-").length <= level) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-CostCentreID=" + CostCentreID + " data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-CostCentreID=" + CostCentreID + " data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span style='background-color: #" + $scope.ParenttreeNewList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeNewList[i]["ShortDescription"] + "</span>";
                        TreeItem += " <span class='treeItemName' data-name='text' data-CostCentreIDForReport=" + CostCentreID + ">" + $scope.ParenttreeNewList[i]["Name"] + "</span>";
                        TreeItem += "</a> </li>";
                    }
                    if (TreeItem.length > 0) {
                        $('#EntitiesTree li[data-page=' + PageNo + '] > ul').html($compile(TreeItem)($scope));
                        $('#EntitiesTree li[data-page=' + PageNo + ']').removeClass('inprogress').addClass('done').removeAttr('style');
                        if ($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items") == undefined) {
                            $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", i);
                        } else {
                            var items = parseInt($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items")) * i;
                            $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", items);
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, PageNo, ""]);
                        setTimeout(LoadPageContent, 0);
                    }
                }
                $(window).AdjustHeightWidth();
            });
        };
        $scope.listofreportEntityID = '';
        $scope.CostCentreListofReportRecords = function () {

            $("#ReportPageModel").modal('show');
            $scope.chkFinancialDetl = true;
            $scope.chkMetadataDetl = true;
            $scope.chkTaskDetl = true;
            $scope.chkMemberDetl = true;
            $scope.StructuralReportDiv = false;
            $scope.ReportGenerate = false;
            $scope.ReportGanttGenerate = false;
            $scope.ReportEdit = false;
            $scope.ReportTypes = [];
            $scope.SelectedReport = "";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            var IDLIST = new Array();
            var ExpandingEntityID = 0;
            var IncludeChildren = true;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                ExpandingEntityID = $stateParams.ID;
            }
            if (FilterID > 0) {
                IncludeChildren = false;
            }
            CcdetailService.ShowReports(0, true).then(function (saveviewscomment) {
                if (saveviewscomment.Response != null) {
                    var resultcustom = saveviewscomment.Response;
                    for (var i = 0, reportDatacustom; reportDatacustom = resultcustom[i++];) {
                        $scope.ReportTypes.push({
                            "OID": reportDatacustom.OID.toString(),
                            "Caption": reportDatacustom.Caption,
                            "ImageURl": "",
                            "Description": ""
                        });
                    }
                } else { }
            });
            CcdetailService.GetFinancialReportSettings().then(function (ReportResultData) {
                var result = ReportResultData.Response;
                for (var i = 0, reportData; reportData = result[i++];) {
                    $scope.ReportTypes.push({
                        "OID": "F_" + reportData.ID.toString(),
                        "Caption": reportData.ReportSettingsName,
                        "ImageURl": reportData.ReportImage,
                        "Description": reportData.Description
                    });
                }
            });
        }
        $scope.changeReportType = function () {
            var eidlist = $scope.listofreportEntityID;
            $scope.Reportreset = [];
            $scope.selectvalue = 0;
            $scope.selectvalue = $scope.SelectedReport;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.StructuralReportDiv = false;
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            if ($scope.selectvalue.contains('F_')) {
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                var ReportTypeData = $.grep($scope.ReportTypes, function (e) {
                    return e.OID == $scope.selectvalue;
                });
                $scope.ReportImageURL = ReportTypeData[0].ImageURl;
                $scope.reportDescription = ReportTypeData[0].Description;
            } else if ($scope.selectvalue == -200) {
                $scope.reportDescription = "Structure report"
                $scope.ReportImageURL = "Structure.jpg";
                $scope.StructuralReportDiv = true;
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    $scope.SelectedEntityCheckDivStr = true;
                    $scope.SubLevelsCheckDivStr = true;
                } else {
                    $scope.SelectedEntityCheckDivStr = false;
                    $scope.SubLevelsCheckDivStr = false;
                }
            } else {
                if ($scope.selectvalue > 0) {
                    $scope.ReportEdit = true;
                    $scope.ReportGenerate = true;
                    $scope.ReportGanttGenerate = false;
                    $scope.GanttReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.CustomReportDiv = true;
                    } else {
                        $scope.CustomReportDiv = false;
                    }
                    CcdetailService.ShowReports($scope.selectvalue, true).then(function (BindReports) {
                        if (BindReports.Response != null) {
                            $scope.Reportreset = BindReports.Response;
                            var count = BindReports.Response.length;
                            $scope.showCustomEntityCheck = $scope.Reportreset[count - 1].EntityLevel;
                            $scope.showCustomSubLevels = $scope.Reportreset[count - 1].SubLevel;
                            $scope.reportDescription = $scope.Reportreset[count - 1].Description;
                            $scope.ReportValue = $scope.Reportreset[count - 1].OID;
                            $scope.ReportImageURL = $scope.Reportreset[count - 1].Preview;
                            $scope.ReportEdit = "devexpress.reportserver:open?reportId=" + $scope.ReportValue + "&revisionId=0&preview=0";
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4523.Caption'));
                        }
                    });
                } else if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.ReportGanttGenerate = true;
                    $scope.reportDescription = "Generate an excel report of the gantt view. Use filters to filter out desired data before generating a report.";
                    $scope.ReportImageURL = "GanttReport.png";
                    $scope.GanttReportDiv = true;
                    $scope.Ganttoption = '';
                    $scope.GanttEntityType = '';
                    $scope.GanttstartDate = "";
                    $scope.Ganttenddate = "";
                    $scope.GanttSelectedEntityCheck = "";
                    $scope.GanttAllSubLevelsCheck = "";
                    $scope.CustomReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.SelectedEntityCheckDiv = true;
                        $scope.SubLevelsCheckDiv = true;
                    } else {
                        $scope.SelectedEntityCheckDiv = false;
                        $scope.SubLevelsCheckDiv = false;
                    }
                    CcdetailService.GetEntityTypefromDB().then(function (getEntityTypes) {
                        $scope.GanttEntityTypes = getEntityTypes.Response;
                    });
                    CcdetailService.GetAdminSettingselemntnode('ReportSettings', 'GanttViewReport', 0).then(function (getattribtuesResult) {
                        var getattribtues = getattribtuesResult.Response;
                        if ((getattribtues != undefined || getattribtues != null) && (getattribtues.GanttViewReport != undefined || getattribtues.GanttViewReport != null)) {
                            if (getattribtues.GanttViewReport.Attributes.Attribute.length > 0) {
                                $scope.Ganttoptions = getattribtues.GanttViewReport.Attributes.Attribute;
                            } else if (getattribtues.GanttViewReport.Attributes.Attribute.Id != "") {
                                $scope.Ganttoptionsdata = [];
                                $scope.Ganttoptionsdata.push({
                                    "Id": getattribtues.GanttViewReport.Attributes.Attribute.Id,
                                    "Field": getattribtues.GanttViewReport.Attributes.Attribute.Field,
                                    "DisplayName": getattribtues.GanttViewReport.Attributes.Attribute.DisplayName
                                });
                                $scope.Ganttoptions = $scope.Ganttoptionsdata;
                            }
                        }
                    });
                } else {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.GanttReportDiv = false;
                }
            }
        }
        $scope.changeEntityType = function () {
            if ($scope.GanttEntityType.length > 0) {
                $scope.SelectedEntityCheckDiv = false;
                $scope.SubLevelsCheckDiv = false;
                $scope.GanttSelectedEntityCheck = "";
                $scope.GanttAllSubLevelsCheck = "";
            } else if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                $scope.SelectedEntityCheckDiv = true;
                $scope.SubLevelsCheckDiv = true;
            }
        };
        $scope.ChangeEntitylevelCheck = function () {
            if ($scope.GanttSelectedEntityCheck == true) {
                $scope.GanttAllSubLevelsCheck = false;
            }
        };
        $scope.ChangeSubLevelsCheck = function () {
            if ($scope.GanttAllSubLevelsCheck == true) {
                $scope.GanttSelectedEntityCheck = false;
            }
        };
        //$scope.AddDefaultEndDate = function (startdate) {
        //    $("#EntityMetadata").addClass('notvalidate');
        //    var startDate = new Date.create(startdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        //    if (startDate == null) {
        //        $scope.Ganttenddate = null
        //    } else {
        //        $scope.Ganttenddate = dateFormat(startDate.addDays(7), $scope.DefaultSettings.DateFormat);
        //    }
        //};
        $scope.CustomChangeEntitylevelCheck = function () {
            if ($scope.CustomEntityCheck == true) {
                $scope.CustomSubLevelsCheck = false;
            }
        };
        $scope.CustomChangeSubLevelsCheck = function () {
            if ($scope.CustomSubLevelsCheck == true) {
                $scope.CustomEntityCheck = false;
            }
        };
        $scope.ChangeEntitylevelCheckStr = function () {
            if ($scope.GanttSelectedEntityCheckStr == true) {
                $scope.GanttAllSubLevelsCheckStr = false;
            }
        };
        $scope.ChangeSubLevelsCheckStr = function () {
            if ($scope.GanttAllSubLevelsCheckStr == true) {
                $scope.GanttSelectedEntityCheckStr = false;
            }
        };
        $scope.hideReportPopup = function () {
            if ($scope.selectvalue.contains('F_')) {
                var reportid = $scope.selectvalue.split('F_')[1];
                var mypage = "reports/FinancialReport.html?id=" + parseInt(reportid) + "&session=" + $cookies["Session"];
                var myname = "reports/FinancialReport.html?id=" + parseInt(reportid) + "&session=" + $cookies["Session"];
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            } else if ($scope.selectvalue == -200) {
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                $scope.CustomEntityCheck = "";
                $scope.CustomSubLevelsCheck = "";
                $scope.ProcessReport = "Structure Report";
                $("#ReportPageModel").modal("hide");
                $('#loadingReportPageModel').modal("show");
                var ExpandingEntityIDStr = 0;
                var IncludeChildrenStr = true;
                if ($stateParams.ID != undefined && $stateParams.ID != 0 && ($scope.GanttSelectedEntityCheckStr == true)) {
                    ExpandingEntityIDStr = $stateParams.ID;
                }
                if ((FilterID == 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = true;
                }
                if ((FilterID > 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = false;
                } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheckStr == true)) {
                    IncludeChildrenStr = false;
                }
                CcdetailService.GetStrucuralRptDetail(($scope.chkFinancialDetl != undefined ? $scope.chkFinancialDetl : false), ($scope.chkMetadataDetl != undefined ? $scope.chkMetadataDetl : false), ($scope.chkTaskDetl != undefined ? $scope.chkTaskDetl : false), ($scope.chkMemberDetl != undefined ? $scope.chkMemberDetl : false), ExpandingEntityIDStr, ($scope.GanttAllSubLevelsCheckStr != undefined ? $scope.GanttAllSubLevelsCheckStr : false)).then(function (RptResult) {
                    if (RptResult.StatusCode == 200 && RptResult.Response != null) {
                        var a = document.createElement('a'),
							fileid = RptResult.Response,
							extn = '.xlsx';
                        var filename = 'StructuralReport.xlsx';
                        a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                        a.download = fileid + extn;
                        document.body.appendChild(a);
                        $('#loadingReportPageModel').modal("hide");
                        a.click();
                    }
                });
            } else {
                if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    var GetActivityListrepose = [];
                    if ($scope.GanttstartDate.toString().length > 0 && $scope.Ganttenddate.toString().length > 0) {
                        var sdate = new Date.create($scope.GanttstartDate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var edate = new Date.create($scope.Ganttenddate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                        if (diffval < 0) {
                            $scope.GanttstartDate = "";
                            $scope.Ganttenddate = "";
                            bootbox.alert($translate.instant('LanguageContents.Res_4767.Caption'));
                            return false;
                        }
                    } else if ($scope.GanttstartDate.toString().length > 0 || $scope.Ganttenddate.toString().length > 0) {
                        $scope.GanttstartDate = "";
                        $scope.Ganttenddate = "";
                        bootbox.alert($translate.instant('LanguageContents.Res_4578.Caption'));
                        return false;
                    }
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $("#ReportPageModel").modal("hide");
                    $('#loadingReportPageModel').modal("show");
                    $scope.ProcessReport = "Gantt View Report";
                    var GetCoststListrepose = [];
                    var IsMonthlyGantt = $scope.selectvalue == -100 ? false : true;
                    var Node3 = {};
                    Node3.FilterID = FilterID;
                    Node3.SortOrderColumn = "null";
                    Node3.IsDesc = false;
                    Node3.IncludeChildren = IncludeChildren;
                    Node3.EntityID = $scope.costcentreidforreport;
                    Node3.Level = 100;
                    Node3.ExpandingEntityID = ExpandingEntityID;
                    Node3.IsMonthly = IsMonthlyGantt;
                    Node3.IDArr = GlobalChildNode;
                    Node3.FilterAttributes = FilterObj;
                    Node3.EntityTypeArr = $scope.GanttEntityType;
                    Node3.OptionAttributeArr = $scope.Ganttoption;
                    Node3.GanttstartDate = $scope.GanttstartDate;
                    Node3.Ganttenddate = $scope.Ganttenddate;
                    CcdetailService.CostCentreDetailReportSystem(Node3).then(function (GetCoststList3) {
                        if (GetCoststList3.Response != null) {
                            GetCoststListrepose = GetCoststList3.Response;
                            var a = document.createElement('a'),
								fileid = GetCoststListrepose,
								extn = '.xlsx';
                            var datetimetdy = ConvertDateToString(new Date.create(), $scope.DefaultSettings.DateFormat);
                            var filename = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy + extn;
                            a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                            a.download = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy;
                            document.body.appendChild(a);
                            $('#loadingReportPageModel').modal("hide");
                            a.click();
                        }
                    });
                } else if ($scope.selectvalue > 0) {
                    var IDLIST = new Array();
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.CustomEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $scope.listofreportEntityID = '';
                    var Node1 = {};
                    Node1.FilterID = FilterID;
                    Node1.SortOrderColumn = "null";
                    Node1.IsDesc = false;
                    Node1.IncludeChildren = IncludeChildren;
                    Node1.EntityID = $scope.costcentreidforreport;
                    Node1.Level = 100;
                    Node1.ExpandingEntityID = ExpandingEntityID;
                    Node1.IDArr = GlobalChildNode;
                    Node1.FilterAttributes = FilterObj;
                    CcdetailService.CostCentreDetailReport(Node1).then(function (GetCostCenterActivityReportList) {
                        if (GetCostCenterActivityReportList.Response != null) {
                            if (GetCostCenterActivityReportList.Response.length > 0) {
                                for (var i = 0; i < GetCostCenterActivityReportList.Response.length; i++) {
                                    IDLIST.push(GetCostCenterActivityReportList.Response[i]);
                                }
                            }
                            $scope.listofreportEntityID = IDLIST.join(",");
                            var param = {
                                'ID': $scope.ReportValue,
                                'SID': $scope.listofreportEntityID
                            };
                            var w = 1200;
                            var h = 800
                            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                            OpenWindowWithPost("reportviewer.aspx", winprops, "NewFile", param);
                        }
                    });
                }
            }
        }
        $scope.SelectedFilterID = 0;
        $scope.SelectedFilterType = 1;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        CcdetailService.GetFilterSettingsForDetail(5).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });

        function OpenWindowWithPost(url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("report.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
        $scope.CostExpndClass = 'icon-caret-right';
        $scope.disableShowAll = false;
        $scope.ExpandAll = function () {
            if ($scope.disableShowAll == false) {
                $scope.disableShowAll = true;
                $scope.ListViewDetails = [{
                    data: "",
                    PageIndex: 0,
                    UniqueID: "",
                    StartRowNo: 0,
                    MaxNoofRow: 30,
                    FilterID: 0,
                    SortOrderColumn: "null",
                    IsDesc: false,
                    IncludeChildren: false,
                    EntityID: '0',
                    Level: '0',
                    IDArr: [],
                    FilterAttributes: []
                }];
                $scope.ListTemplate = "";
                $scope.GanttViewDataPageTemplate = '';
                $scope.GanttViewBlockPageTemplate = '';
                $('#TreeCtrlCss').remove();
                if ($scope.CostExpandtext == $translate.instant('LanguageContents.Res_4939.Caption')) {
                    $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4940.Caption');
                    $scope.CostExpndClass = 'icon-caret-down';
                    $scope.Level = "100";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                } else {
                    $scope.CostExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
                    $scope.CostExpndClass = 'icon-caret-right';
                    $scope.Level = "0";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                }
                $('.ganttview-data').scrollTop(0);
                $('.list_rightblock').scrollTop(0);
            }
        }
        $scope.SelectedFilterType = 2;
        $scope.SelectedFilterID = 0;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        CcdetailService.GetFilterSettingsForDetail(5).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $("#costcentredetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            } else {
                $("#costcentredetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 5, $scope.SelectedFilterType]);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow")
            }
        };
        $scope.ApplyFilter = function (filterid, filtername, IsDetailFilter) {
            $scope.SelectedFilterID = filterid;
            $scope.SelectedFilterType = IsDetailFilter;
            if (filterid != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_5053.Caption');
                $scope.appliedfilter = filtername;
            } else {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $('.FilterHolder').slideUp("slow");
                $("#costcentredetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            }
            $("#costcentredetailfilter").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        }
        $("#costcentredetailfilter").on('reloadccdetailfilter', function (event, typeId, filtername, filterid, filtertype) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#costcentredetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 5, filtertype]);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_5053.Caption');
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $scope.SelectedFilterID = 0;
                $('.FilterHolder').slideUp("slow")
                $scope.SelectedFilterType = 2;
            }
            CcdetailService.GetFilterSettingsForDetail(typeId).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.atributesRelationList = [];
            });
        });
        $("#moduleContextmenu").click(function () {
            CcdetailService.GetCostCentreAssignedAmount($scope.CurrentCostcentreID).then(function (ResAssgnAmnt) {
                createsubcostcentre($scope.CurrentCostcentreID, ResAssgnAmnt.Response);
            });
        });
        $("#EntitiesTree").on("LoadCostCentreTree", function (event, EntityID, UniqueKey) {
            $scope.SelectedCostCentreCurrency.Rate = 1;
            $scope.SelectedCostCentreCurrency.ShortName = $scope.DefaultSettings.CurrencyFormat.Name;
            $scope.SelectedCostCentreCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
            $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
            $scope.ExpandingEntityID = EntityID;
            TreeLoad(false, UniqueKey, EntityID, true);
        });

        function GetCostCentreCurrencyRateById(CostCentreID, CurrencyId) {
            CcdetailService.GetCostCentreCurrencyRateById(CostCentreID, 0, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.GetCurrencyList(parseInt(resCurrencyRate.Response[0]));
                } else $scope.GetCurrencyList(0);
            });
        }
        $scope.GetCurrencyList = function (CostCentreCurrencyId) {
            CcdetailService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                if (CurrencyListResult.Response != null) {
                    $scope.CurrencyFormatsList = CurrencyListResult.Response;
                    if (CostCentreCurrencyId != 0) $scope.SelectedCostCentreCurrency.TypeId = CostCentreCurrencyId;
                    else $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
                    $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                        return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
                    })[0].Name;
                    $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                        return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
                    })[0].ShortName;
                }
            });
        }
        $scope.ChangeCostCentreCurrencyType = function (currencyId) {
            $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == currencyId;
            })[0].Name;
            $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == currencyId;
            })[0].ShortName;
            $("#CostCentreCurrencyTypediv").removeClass("btn-group open");
            $("#CostCentreCurrencyTypediv").addClass("btn-group");
            CcdetailService.GetCostCentreCurrencyRateById(0, currencyId, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.SelectedCostCentreCurrency.TypeId = parseInt(resCurrencyRate.Response[0]);
                    $scope.$broadcast('onTreePageCreation', ["", 0, ""]);
                }
            });
        }

        function createsubcostcentre(ccID, amount) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/costcentre/subcostcentrecreation.html',
                controller: "mui.planningtool.costcentre.subcostcentrecreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            ccID: ccID,
                            amount: amount,
                            UniqueID: $window.GlobalUniquekey
                        };
                    }
                },
                scope: $scope,
                windowClass: 'subcostcentremodal popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
    }
    app.controller("mui.planningtool.costcentre.detailCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$location', '$templateCache', '$cookies', '$translate', 'CcdetailService', '$modal', muiplanningtoolcostcentredetailCtrl]);
})(angular, app);