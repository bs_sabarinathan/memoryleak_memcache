﻿using BrandSystems.Marcom.Core.Dam.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam
{
    internal class DamReformatProfile:IDamReformatProfile
    {
         #region Member Variables

        protected int _id;
        protected string _name;
        protected int _units;
        protected string _profileSrcValue;

        #endregion


         #region Constructors
        public DamReformatProfile() { }

        public DamReformatProfile(int id, string name, int pUnits, string PprofileSrcValue)
        {
            this._name = name;
            this._id = id;
            this._units = pUnits;
            this._profileSrcValue = PprofileSrcValue;
            
        }

        public DamReformatProfile(int dId)
        {
            this._id = dId;
        }

        #endregion


        #region Public Properties


        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }


        public virtual string Name
        {
            get
            {
                return _name;
            }
            set { _name = value; }
        }

        public virtual string ProfileSrcValue
        {
            get { return _profileSrcValue; }
            set { _profileSrcValue = value; }
        }
        public virtual int Units
        {
            get { return _units; }
            set { _units = value; }
        }
        


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
            {
            if(this == obj) return true;
            DamReformatProfile castObj = null;
            try
                {
                castObj = (DamReformatProfile)obj;
                }
            catch(Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
            }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
            }
        #endregion


    }
}
