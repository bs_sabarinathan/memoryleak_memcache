﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    /// <summary>
    /// DAMTaskOptimakerAssetRelationDao object for table 'DAM_TaskOptimakerAssetRelation'.
    /// </summary>
    public partial class DAMTaskOptimakerAssetRelationDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _taskid;
        protected int _assetid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected bool _approved;
        #endregion

        #region Constructors
        public DAMTaskOptimakerAssetRelationDao() { }

        public DAMTaskOptimakerAssetRelationDao(int pId, int ptaskid, int pAssetid, bool papproved)
        {
            this._id = pId;
            this._taskid = ptaskid;
            this._assetid = pAssetid;
            this._approved = papproved;
        }

        public DAMTaskOptimakerAssetRelationDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int TaskID
        {
            get { return _taskid; }
            set { _bIsChanged |= (_taskid != value); _taskid = value; }

        }
        public virtual int AssetID
        {
            get { return _assetid; }
            set { _bIsChanged |= (_assetid != value); _assetid = value; }

        }
        public virtual bool Approved
        {
            get { return _approved; }
            set { _approved = value; }

        }
        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string DAMTaskOptimakerAssetRelationDao = "DAMTaskOptimakerAssetRelationDao";
            public const string Id = "ID";
            public const string TaskID = "TaskID";
            public const string AssetID = "AssetID";
            public const string Approved = "Approved";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string DAMTaskOptimakerAssetRelationDao = "DAMTaskOptimakerAssetRelationDao";
            public const string Id = "ID";
            public const string TaskID = "TaskID";
            public const string AssetID = "AssetID";
            public const string Approved = "Approved";
        }

        #endregion
    }
}
