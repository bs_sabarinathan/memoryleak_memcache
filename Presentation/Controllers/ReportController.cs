﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Core.Planning.Interface;
using Newtonsoft.Json.Linq;
using Presentation.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class ReportController : ApiController
    {
        ServiceResponse result;
        StreamReader reader;
        [Route("api/Report/InsertUpdateReportCredential")]
        [HttpPost]
        public ServiceResponse InsertUpdateReportCredential([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ReportCredential_InsertUpdate((string)jobj["ReportUrl"], (string)jobj["AdminUsername"], (string)jobj["AdminPassword"], (string)jobj["ViewerUsername"], (string)jobj["ViewerPassword"], (int)jobj["Category"], (int)jobj["DataView"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/validateReportCredential")]
        [HttpPost]
        public ServiceResponse validateReportCredential([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ReportCredential_ValidateSave((string)jobj["ReportUrl"], (string)jobj["AdminUsername"], (string)jobj["AdminPassword"], (string)jobj["ViewerUsername"], (string)jobj["ViewerPassword"], (int)jobj["Category"], (int)jobj["DataView"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/InsertUpdateReport")]
        [HttpPost]
        public ServiceResponse InsertUpdateReport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.Report_InsertUpdate((int)jobj["OID"], (string)jobj["Name"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Preview"], (bool)jobj["Show"], (int)jobj["CategoryId"], (bool)jobj["EntityLevel"], (bool)jobj["SubLevel"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/UpdateReportImage")]
        [HttpPost]
        public ServiceResponse UpdateReportImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.UpdateReportImage((string)jobj["imgsourcepath"], (int)jobj["imgwidth"], (int)jobj["imgheight"], (int)jobj["imgX"], (int)jobj["imgY"], (int)jobj["OID"], (string)jobj["Preview"], (string)jobj["ReportName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/GetReportCredentialByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetReportCredentialByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ReportCredential_Select(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetReportByOID/{OID}")]
        [HttpGet]
        public ServiceResponse GetReportByOID(int OID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.MergeReports(OID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetCustomViewsByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetCustomViewsByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_Select(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetViews")]
        [HttpGet]
        public ServiceResponse GetViews()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_Select(0);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Report/GetViewsValidate")]
        [HttpPost]
        public ServiceResponse GetViewsValidate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_Validate((string)jobj["Name"], (string)jobj["Query"], (int)jobj["ViewID"]);
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = ex.InnerException.Message.ToString();
            }
            return result;
        }
        [Route("api/Report/CustomViews_Update")]
        [HttpPost]
        public ServiceResponse CustomViews_Update([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_Update((int)jobj["ViewID"], (string)jobj["Name"], (string)jobj["Description"], (string)jobj["Query"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/DeleteView/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteView(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_DeleteByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Report/InsertCustomViews")]
        [HttpPost]
        public ServiceResponse InsertCustomViews([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomViews_Insert((string)jobj["Name"], (string)jobj["Description"], (string)jobj["Query"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/GetDataviewbyusername")]
        [HttpPost]
        public ServiceResponse GetDataviewbyusername([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.Dataview_select((int)jobj["DataViewID"],tenantres.tenanturl, (string)jobj["AdminUsername"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/ReportLogin")]
        [HttpPost]
        public ServiceResponse ReportLogin([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ReportLogin((string)jobj["ReportUrl"], (string)jobj["ViewerUsername"], (string)jobj["ViewerPassword"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
         [Route("api/Report/GetListOfReports")]
        [HttpPost]
        public ServiceResponse GetListOfReports([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetListOfReports((string)jobj["ViewerUsername"], (string)jobj["ViewerPassword"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/ShowReports/{OID}/{Show}")]
        [HttpGet]
        public ServiceResponse ShowReports(int OID, bool Show)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ShowReports(OID, Show);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/UpdateReportSchemaResponse")]
        [HttpPost]
        public ServiceResponse UpdateReportSchemaResponse([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.UpdateReportSchemaResponse((int)jobj["status"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/pushviewSchema")]
        [HttpPost]
        public ServiceResponse pushviewSchema([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.pushviewSchema();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetReportViewSchemaResponse")]
        [HttpGet]
        public ServiceResponse GetReportViewSchemaResponse()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetReportViewSchemaResponse();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/ActivityDetailReportSystem")]
        [HttpPost]
        public ServiceResponse ActivityDetailReportSystem([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                int[] entityTypeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["EntityTypeArr"].ToString());
                string[] optionAttributeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jobj["OptionAttributeArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                List<int> Typeids = new List<int>();
                if (entityTypeArr != null)
                {
                    Typeids.AddRange(entityTypeArr);
                }
                List<string> Attributids = new List<string>();
                if (optionAttributeArr != null)
                {
                    Attributids.AddRange(optionAttributeArr);
                }
                ListSettings lstSettings = XmlSetting.ReportListSettings("GanttViewReport", "ReportSettings", "GanttViewReport", 6, Typeids, Attributids, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.GanttReportAttributes("GanttViewReport", Attributids, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if(jr[i]["EntityMemberIDs"]!=null)
                        filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.ReportManager.ListofRecordsSystemReport((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], (string)jobj["GanttstartDate"], (string)jobj["Ganttenddate"], (bool)jobj["IsMonthly"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/CostCentreDetailReportSystem")]
        [HttpPost]
        public ServiceResponse CostCentreDetailReportSystem([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                int[] entityTypeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["EntityTypeArr"].ToString());
                string[] optionAttributeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jobj["OptionAttributeArr"].ToString());

                result.StatusCode = (int)HttpStatusCode.OK;
                List<int> Typeids = new List<int>();
                if (entityTypeArr != null)
                {
                    Typeids.AddRange(entityTypeArr);
                }
                List<string> Attributids = new List<string>();
                if (optionAttributeArr != null)
                {
                    Attributids.AddRange(optionAttributeArr);
                }
                ListSettings lstSettings = XmlSetting.ReportListSettings("GanttViewReport", "ReportSettings", "GanttViewReport", 5, Typeids, Attributids, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.GanttReportAttributes("GanttViewReport", Attributids, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.ReportManager.ListofRecordsSystemReport((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 2, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], (string)jobj["GanttstartDate"], (string)jobj["Ganttenddate"], (bool)jobj["IsMonthly"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/ObjectiveDetailReportSystem")]
        [HttpPost]
        public ServiceResponse ObjectiveDetailReportSystem([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                int[] entityTypeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["EntityTypeArr"].ToString());
                string[] optionAttributeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jobj["OptionAttributeArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                List<int> Typeids = new List<int>();
                if (entityTypeArr != null)
                {
                    Typeids.AddRange(entityTypeArr);
                }
                List<string> Attributids = new List<string>();
                if (optionAttributeArr != null)
                {
                    Attributids.AddRange(optionAttributeArr);
                }
                ListSettings lstSettings = XmlSetting.ReportListSettings("GanttViewReport", "ReportSettings", "GanttViewReport", 6, Typeids, Attributids, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.GanttReportAttributes("GanttViewReport", Attributids, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.ReportManager.ListofRecordsSystemReport((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], (string)jobj["GanttstartDate"], (string)jobj["Ganttenddate"], (bool)jobj["IsMonthly"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Report/GetFinancialSummaryDetlRpt/")]
        [HttpPost]
        public ServiceResponse GetFinancialSummaryDetlRpt([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, (Guid)(jobj["reportsession"]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetFinancialSummaryDetlRpt((string)jobj["entityType"]);

            }
            catch(Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                LogHandler.LogInfo(" Exception   apicontroller GetFinancialSummaryDetlRpt  " + DateTime.Now + " ************************" + ex.ToString(), LogHandler.LogType.General);
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetFinancialSummaryDetlRptByAttribute/")]
        [HttpPost]
        public ServiceResponse GetFinancialSummaryDetlRptByAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, (Guid)(jobj["reportsession"]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetFinancialSummaryDetlRptByAttribute((string)jobj["entityType"], (int)jobj["attributeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetEntityFinancialSummaryDetl/{EntityTypeID}/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetEntityFinancialSummaryDetl(String EntityTypeID, int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                string Attributes = AttributeID.ToString();
                List<string> AttributeIDs = new List<string>();
                List<int> FinancialAttributes = new List<int>();

                foreach (string attribute in Attributes.Split(','))
                {

                    int Num;

                    bool Status = Int32.TryParse(attribute, out Num);



                    if (Status)
                    {
                        if (Int32.Parse(attribute) > 0)
                        {
                            AttributeIDs.Add(attribute);
                        }
                        else
                        {
                            FinancialAttributes.Add(Int32.Parse(attribute));
                        }
                    }
                    else
                    {
                        AttributeIDs.Add(attribute);
                    }



                }
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.ReportManager.GetEntityFinancialSummaryDetl((int)common.EntityTypeId, AttributeIDs, FinancialAttributes);
                result.Response = marcomManager.ReportManager.GetEntityFinancialSummaryDetl(EntityTypeID, AttributeIDs, FinancialAttributes);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetStrucuralRptDetail/{IsshowFinancialDetl}/{IsDetailIncluded}/{IsshowTaskDetl}/{IsshowMemberDetl}/{ExpandingEntityIDStr}/{IncludeChildrenStr}")]
        [HttpGet]
        public ServiceResponse GetStrucuralRptDetail(bool IsshowFinancialDetl, bool IsDetailIncluded, bool IsshowTaskDetl, bool IsshowMemberDetl, int ExpandingEntityIDStr, bool IncludeChildrenStr)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 6, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(6, tenantres.tenantID);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetStrucuralRptDetail(lstSettings, IsshowFinancialDetl, IsDetailIncluded, IsshowTaskDetl, IsshowMemberDetl, ExpandingEntityIDStr, IncludeChildrenStr);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetReportJSONData/{ReportID}")]
        [HttpGet]
        public ServiceResponse GetReportJSONData(int ReportID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetReportJSONData(ReportID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/GetEntityTypeAttributeRelationWithLevelsByID/{IDs}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelationWithLevelsByID(string IDs)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetEntityTypeAttributeRelationWithLevelsByID(IDs);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/InsertUpdateReportSettingXML")]
        [HttpPost]
        public ServiceResponse InsertUpdateReportSettingXML([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.InsertUpdateReportSettingXML((JObject)jobj["reportBlockData"], (int)jobj["ReportID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/insertupdatefinancialreportsettings")]
        [HttpPost]
        public ServiceResponse insertupdatefinancialreportsettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.insertupdatefinancialreportsettings((string)jobj["Name"], (int)jobj["ReportID"], (string)jobj["ReportImage"], (string)jobj["ReportDescription"], (JObject)jobj["reportBlockData"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/GetFinancialReportSettings")]
        [HttpGet]
        public ServiceResponse GetFinancialReportSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetFinancialReportSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/GetAllEntityTypeAttributeRelationWithLevels")]
        [HttpGet]
        public ServiceResponse GetAllEntityTypeAttributeRelationWithLevels()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetAllEntityTypeAttributeRelationWithLevels();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GenerateFinancialExcel/{ReportID}/{SessionID}")]
        [HttpGet]
        public ServiceResponse GenerateFinancialExcel(int ReportID, string SessionID)
        {
            result = new ServiceResponse();
            try
            {
                LogHandler.LogInfo("service call reached core GenerateFinancialExcel " + DateTime.Now + " ************************" , LogHandler.LogType.General);

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(SessionID));
                LogHandler.LogInfo("service call reached core GenerateFinancialExcel " + DateTime.Now + " ************************" + Guid.Parse(SessionID), LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GenerateFinancialExcel(ReportID);
            }
            catch(Exception ex)
            {
                LogHandler.LogInfo("Exception core GenerateFinancialExcel " + DateTime.Now + " ************************"+ex.ToString(), LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/UpdateFinancialSettingsReportImage")]
        [HttpPost]
        public ServiceResponse UpdateFinancialSettingsReportImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.UpdateFinancialSettingsReportImage((string)jobj["imgsourcepath"], (int)jobj["imgwidth"], (int)jobj["imgheight"], (int)jobj["imgX"], (int)jobj["imgY"], (string)jobj["Preview"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/DeletefinancialreportByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeletefinancialreportByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.DeletefinancialreportByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/GetTaskListReport/{EntityID}/{TaskListId}/{IsEntireList}/{IsIncludeSublevel}")]
        [HttpGet]
        public ServiceResponse GetTaskListReport(int EntityID, int TaskListId, bool IsEntireList, bool IsIncludeSublevel)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.ExportTaskListtoExcel(EntityID, TaskListId, IsEntireList, IsIncludeSublevel, tenantres.tenantID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = null;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetFulfillmentAttribute")]
        [HttpPost]
        public ServiceResponse GetFulfillmentAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int[] EntityTypeID = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["EntityTypeIDs"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetFulfillmentAttribute(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/InsertUpdateCustomlist")]
        [HttpPost]
        public ServiceResponse InsertUpdateCustomlist([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.InsertUpdateCustomlist((int)jobj["ID"], (string)jobj["Name"], (string)jobj["Description"], (string)jobj["XmlData"], (string)jobj["ValidatedQuery"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/GetAllCustomList")]
        [HttpGet]
        public ServiceResponse GetAllCustomList()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetAllCustomList();
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = null;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/DeleteCustomList/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCustomList(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.DeleteCustomList((int)ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = null;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Report/ValidateCustomList")]
        [HttpPost]
        public ServiceResponse ValidateCustomList([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.CustomList_Validate((string)jobj["Name"], (string)jobj["XmlData"]);
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = ex.InnerException.Message.ToString();
            }
            return result;
        }
        [Route("api/Report/GetFulfillmentAttributeOptions/{AttributeID}/{AttributeLevel}")]
        [HttpGet]
        public ServiceResponse GetFulfillmentAttributeOptions(int AttributeID, int AttributeLevel)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentAttributeOptions(AttributeID, AttributeLevel);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Report/insertupdatetabsettings")]
        [HttpPost]
        public ServiceResponse insertupdatetabsettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.insertupdatetabsettings((int)jobj["TabType"], (int)jobj["TabLocation"], (JObject)jobj["reportBlockData"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/GetLayoutData/{TabType}/{TabLocation}")]
        [HttpGet]
        public ServiceResponse GetLayoutData(int TabType, int TabLocation)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetLayoutData(TabType, TabLocation);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/GetEntityFinancialSummaryDetl")]
        [HttpPost]
        public ServiceResponse GetEntityFinancialSummaryDetl([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, (Guid)jobj["reportsession"]);
                result.StatusCode = (int)HttpStatusCode.OK;
                string Attributes = (string)jobj["attributeID"];

                string strBody = (string)jobj["orderbyclause"];
                List<string> AttributeIDs = new List<string>();
                List<int> FinancialAttributes = new List<int>();
                if (Attributes.Length>0)
                {
                    foreach (string attribute in Attributes.Split(','))
                    {
                        int Num;
                        bool Status = Int32.TryParse(attribute, out Num);
                        if (Status)
                        {
                            if (Int32.Parse(attribute) > 0)
                            {
                                AttributeIDs.Add(attribute);
                            }
                            else
                            {
                                FinancialAttributes.Add(Int32.Parse(attribute));
                            }
                        }
                        else
                        {
                            AttributeIDs.Add(attribute);
                        }
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GetEntityFinancialSummaryDetl((string)jobj["entityType"], AttributeIDs, FinancialAttributes, strBody);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Report/CalendarDetailReportSystem")]
        [HttpPost]
        public ServiceResponse CalendarDetailReportSystem([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                int[] entityTypeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["EntityTypeArr"].ToString());
                string[] optionAttributeArr = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jobj["OptionAttributeArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                List<int> Typeids = new List<int>();
                if (entityTypeArr != null)
                {
                    Typeids.AddRange(entityTypeArr);
                }
                List<string> Attributids = new List<string>();
                if (optionAttributeArr != null)
                {
                    Attributids.AddRange(optionAttributeArr);
                }
                ListSettings lstSettings = XmlSetting.ReportListSettings("GanttViewReport", "ReportSettings", "GanttViewReport", 6, Typeids, Attributids, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.GanttReportAttributes("GanttViewReport", Attributids, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.ReportManager.ListofRecordsSystemReport((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 7, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], (string)jobj["GanttstartDate"], (string)jobj["Ganttenddate"], (bool)jobj["IsMonthly"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Report/GettableauReportSettings/{ID}")]
        [HttpGet]
        public ServiceResponse GettableauReportSettings(int ID)
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.GettableauReportSettings(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/DeletetableaureportByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeletetableaureportByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.DeletetableaureportByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/UpdatetableauSettingsReportImage")]
        [HttpPost]
        public ServiceResponse UpdatetableauSettingsReportImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.UpdatetableauSettingsReportImage((string)jobj["imgsourcepath"], (int)jobj["imgwidth"], (int)jobj["imgheight"], (int)jobj["imgX"], (int)jobj["imgY"], (string)jobj["Preview"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Report/insertupdatetableaureportsettings")]
        [HttpPost]
        public ServiceResponse insertupdatetableaureportsettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ReportManager.insertupdatetableaureportsettings((string)jobj["Name"], (int)jobj["ReportID"], (string)jobj["ReportImage"], (string)jobj["ReportDescription"], (string)jobj["ReportUrl"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

    }
}