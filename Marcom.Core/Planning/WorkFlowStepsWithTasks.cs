﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;


namespace BrandSystems.Marcom.Core.Planning
{
    internal class WorkFlowStepsWithTasks : IWorkFlowStepsWithTasks
    {
        #region Public Properties

        public int StepID { get; set; }
        public string StepName { get; set; }
        public bool IsActive { get; set; }
        public string WorkFloName { get; set; }
        public int TotalActiveTasks { get; set; }
        public dynamic TaskList{ get; set; }

        #endregion
    }
}
