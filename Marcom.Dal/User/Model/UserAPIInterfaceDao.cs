using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{

	/// <summary>
	/// UserDao object for table 'UM_User'.
	/// </summary>
	public partial class UserAPIInterfaceDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _userid;
        protected string _apiguid;

		protected bool _bIsDeleted;
		protected bool _bIsChanged;
       
		#endregion
		
		#region Constructors
		public UserAPIInterfaceDao() {}

        public UserAPIInterfaceDao(string _pApiguid)
		{
            this._apiguid = _pApiguid;
		}


        public UserAPIInterfaceDao(int pUserid)
		{
            this._userid = pUserid; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int UserID
		{
			get { return _userid; }
            set {  _userid = value; }
			
		}

        public virtual string APIGuid
		{
			get { return _apiguid; }
			set 
			{
                _apiguid = value; 
			}
			
		}
		
		

		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string UserAPIInterfaceDao = "UserAPIInterfaceDao";
            public const string UserID = "UserID";
            public const string APIGuid = "APIGuid";			
			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string UserAPIInterfaceDao = "UserAPIInterfaceDao";
            public const string UserID = "UserID";
            public const string APIGuid = "APIGuid";				
			

		}

		#endregion
		
		
	}
	
}
