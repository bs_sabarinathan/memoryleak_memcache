﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
  public interface IEntityPredefineObjectiveAttributes
    {
      int AttributeId { get; set; }
      string AttributeName { get; set; }
      int AttributeTypeId { get; set; }
      string AttributeValues { get; set; }
      string StartDate { get; set; }
      string EndDate { get; set; }
      int ObjectiveId { get; set; }
      int ObjectiveTypeId { get; set; }
      string ObjectiveName { get; set; }
      string ObjectiveOwner { get; set; }
      string ObjectiveDescription { get; set; }
      bool ObjectiveMandatoryStatus { get; set; }
      int ObjectiveEntitytypeID { get; set; }
    }
}
