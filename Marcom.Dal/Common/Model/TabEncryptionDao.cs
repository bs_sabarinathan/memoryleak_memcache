﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    public partial class TabEncryptionDao : BaseDao, ICloneable 
    {
        #region Member Variables

        protected int _id;
        protected int _cutomtabid;
        protected string _encryKey;
        protected string _encryIV;
        protected string _algorithm;
        protected string _cipherMode;
        protected string _paddingMode;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors

        public TabEncryptionDao() { }

        public TabEncryptionDao(int pId, int pcutomtabid, string pencryKey, string pencryIV, string palgorithm, string ppaddingMode, string pcipherMode)
		{
			this._id = pId;
            this._cutomtabid = pcutomtabid;
            this._encryKey = pencryKey;
            this._encryIV = pencryIV;
            this._algorithm = palgorithm;
            this._paddingMode = ppaddingMode;
            this._cipherMode = pcipherMode; 
		}

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int CustomTabID
        {
            get { return _cutomtabid; }
            set { _bIsChanged |= (_cutomtabid != value); _cutomtabid = value; }

        }

        public virtual string EncryKey
        {
            get { return _encryKey; }
            set
            {
                if (value != null && value.Length > 1500)
                    throw new ArgumentOutOfRangeException("EncryKey", "EncryKey value, cannot contain more than 1500 characters");
                _bIsChanged |= (_encryKey != value);
                _encryKey = value;
            }

        }

        public virtual string EncryIV
        {
            get { return _encryIV; }
            set
            {
                if (value != null && value.Length > 1500)
                    throw new ArgumentOutOfRangeException("EncryIV", "EncryIV value, cannot contain more than 1500 characters");
                _bIsChanged |= (_encryIV != value);
                _encryIV = value;
            }

        }

        public virtual string Algorithm
        {
            get { return _algorithm; }
            set { _bIsChanged |= (_algorithm != value); _algorithm = value; }

        }

        public virtual string PaddingMode
        {
            get { return _paddingMode; }
            set { _bIsChanged |= (_paddingMode != value); _paddingMode = value; }

        }

        public virtual string CipherMode
        {
            get { return _cipherMode; }
            set { _bIsChanged |= (_cipherMode != value); _cipherMode = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion 			

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TabEncryptionDao = "TabEncryptionDao";
            public const string ID = "ID";
            public const string CustomTabID = "CustomTabID";
            public const string EncryKey = "EncryKey";
            public const string EncryIV = "EncryIV";
            public const string Algorithm = "Algorithm";
            public const string PaddingMode = "PaddingMode";
            public const string CipherMode = "CipherMode";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TabEncryptionDao = "TabEncryptionDao";
            public const string ID = "ID";
            public const string CustomTabID = "CustomTabID";
            public const string EncryKey = "EncryKey";
            public const string EncryIV = "EncryIV";
            public const string Algorithm = "Algorithm";
            public const string PaddingMode = "PaddingMode";
            public const string CipherMode = "CipherMode";
        }

        #endregion
    }
}
