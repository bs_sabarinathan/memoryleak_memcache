using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveDao object for table 'PM_Objective'.
	/// </summary>
	
	public partial class ObjectiveDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _typeid;
		protected string _name;
        protected string _description;
		protected string _instruction;
		protected bool _isenablefeedback;
		protected DateTime _startdate;
		protected DateTime _enddate;
		protected int? _daterule;
		protected bool? _ismandatory;
        protected bool? _objectivestatus;
        protected string _objectivecriteria;
		protected ObjectiveNumericDao _numeric;
		protected IList<ObjectiveRatingDao> _ratings;
		protected IList<ObjectiveConditionDao> _conditions;
		protected IList<ObjectiveEntityValueDao> _value;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveDao() {}
			
		public ObjectiveDao(int pId, int pTypeid, string pName,string pDescription, string pInstruction, bool pIsEnableFeedback, DateTime pStartDate, DateTime pEndDate, int? pDateRule, bool? pIsMandatory, bool? pObjectiveStatus, string pObjectiveCriteria)
		{
			this._id = pId; 
			this._typeid = pTypeid; 
			this._name = pName;
            this._description = pDescription;
			this._instruction = pInstruction; 
			this._isenablefeedback = pIsEnableFeedback; 
			this._startdate = pStartDate; 
			this._enddate = pEndDate; 
			this._daterule = pDateRule; 
			this._ismandatory = pIsMandatory;
            this._objectivestatus = pObjectiveStatus;
            this._objectivecriteria = pObjectiveCriteria;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Typeid
		{
			get { return _typeid; }
			set { _bIsChanged |= (_typeid != value); _typeid = value; }
			
		}
		
		public virtual string Name
		{
			get { return _name; }
			set 
			{
              //if (value != null && value.Length > 50)
              //  throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
			  _bIsChanged |= (_name != value); 
			  _name = value; 
			}
			
		}
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }
		public virtual string Instruction
		{
			get { return _instruction; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Instruction", "Instruction value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_instruction != value); 
			  _instruction = value; 
			}
			
		}
		
		public virtual bool IsEnableFeedback
		{
			get { return _isenablefeedback; }
			set { _bIsChanged |= (_isenablefeedback != value); _isenablefeedback = value; }
			
		}
		
		public virtual DateTime StartDate
		{
			get { return _startdate; }
			set { _bIsChanged |= (_startdate != value); _startdate = value; }
			
		}
		
		public virtual DateTime EndDate
		{
			get { return _enddate; }
			set { _bIsChanged |= (_enddate != value); _enddate = value; }
			
		}
		
		public virtual int? DateRule
		{
			get { return _daterule; }
			set { _bIsChanged |= (_daterule != value); _daterule = value; }
			
		}
		
		public virtual bool? IsMandatory
		{
			get { return _ismandatory; }
			set { _bIsChanged |= (_ismandatory != value); _ismandatory = value; }
			
		}

        public virtual string ObjectiveCriteria
        {
            get { return _objectivecriteria; }
            set
            {
                _bIsChanged |= (_objectivecriteria != value);
                _objectivecriteria = value;
            }

        }
		
		public virtual ObjectiveNumericDao Numeric
		{
			get { return _numeric; }
			set { _bIsChanged |= (_numeric != value); _numeric = value; }
			
		}
		
		public virtual IList<ObjectiveRatingDao> Ratings
		{
			get { return _ratings; }
			set { _bIsChanged |= (_ratings != value); _ratings = value; }
			
		}
		
		public virtual IList<ObjectiveConditionDao> Conditions
		{
			get { return _conditions; }
			set { _bIsChanged |= (_conditions != value); _conditions = value; }
			
		}
		
		public virtual IList<ObjectiveEntityValueDao> Value
		{
			get { return _value; }
			set { _bIsChanged |= (_value != value); _value = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

        public virtual bool? ObjectiveStatus
        {
            get { return _objectivestatus; }
            set { _objectivestatus = value; }

        }
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveDao = "ObjectiveDao";			
			public const string Id = "Id";			
			public const string Typeid = "Typeid";			
			public const string Name = "Name";
            public const string Description = "Description";	
			public const string Instruction = "Instruction";			
			public const string IsEnableFeedback = "IsEnableFeedback";			
			public const string StartDate = "StartDate";			
			public const string EndDate = "EndDate";			
			public const string DateRule = "DateRule";			
			public const string IsMandatory = "IsMandatory";			
			public const string Numeric = "Numeric";
			public const string Ratings_ObjectiveRatingCollection = "Ratings_ObjectiveRatingCollection";			
			public const string Ratings = "Ratings";
			public const string Conditions_ObjectiveConditionCollection = "Conditions_ObjectiveConditionCollection";			
			public const string Conditions = "Conditions";			
			public const string Value = "Value";
            public const string ObjectiveStatus = "ObjectiveStatus";
            public const string ObjectiveCriteria = "ObjectiveCriteria";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveDao = "ObjectiveDao";			
			public const string Id = "Id";			
			public const string Typeid = "Typeid";			
			public const string Name = "Name";
            public const string Description = "Description";
			public const string Instruction = "Instruction";			
			public const string IsEnableFeedback = "IsEnableFeedback";			
			public const string StartDate = "StartDate";			
			public const string EndDate = "EndDate";			
			public const string DateRule = "DateRule";			
			public const string IsMandatory = "IsMandatory";			
			public const string Numeric = "Numeric";
			public const string Ratings_ObjectiveRatingCollection = "Ratings_ObjectiveRatingCollection";			
			public const string Ratings = "Ratings";
			public const string Conditions_ObjectiveConditionCollection = "Conditions_ObjectiveConditionCollection";			
			public const string Conditions = "Conditions";			
			public const string Value = "Value";
            public const string ObjectiveStatus = "ObjectiveStatus";
            public const string ObjectiveCriteria = "ObjectiveCriteria";
		}

		#endregion
		
		
	}
	
}
