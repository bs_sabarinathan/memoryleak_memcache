﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class EntityHistory : IEntityHistory 
    {

        #region Member Variables

        protected int _id;
        protected int _EntityID;
        protected DateTimeOffset _VisitedOn;
        protected int _UserID;
        protected string _EntityName;
        protected string _RoleName;
        protected string _ShortDescription;
        protected string _ColorCode;

        #endregion

        #region Constructors
        public EntityHistory() { }

        public EntityHistory(int pEntityID, DateTimeOffset pVisitedOn, int pUserID)
        {
            this._EntityID = pEntityID;
            this._VisitedOn = pVisitedOn;
            this._UserID = pUserID;
        }

        public EntityHistory(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public DateTimeOffset VisitedOn
        {
            get { return _VisitedOn; }
            set { _VisitedOn = value; }
        }

        public int EntityID
        {
            get { return _EntityID; }
            set { _EntityID = value; }
        }

        public string EntityName
        {

            get { return _EntityName; }
            set
            {
                _EntityName = value;
            }

        }
        public string RoleName
        {
            get { return _RoleName; }
            set
            {
                _RoleName = value;
            }
        }

        public string ShortDescription
        {

            get { return _ShortDescription; }
            set
            {
                _ShortDescription = value;
            }

        }
        public string ColorCode
        {
            get { return _ColorCode; }
            set
            {
                _ColorCode = value;
            }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityHistory castObj = null;
            try
            {
                castObj = (EntityHistory)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
