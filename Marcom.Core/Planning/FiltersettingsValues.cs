﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Metadata;
namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// FilterSettings object for table 'PM_FilterValues'.
    /// </summary>
   public class FiltersettingsValues : IFiltersettingsValues
    {
        #region Member Variables

        protected int _id;
        protected int _filterid;
        protected int _attributeid;
        protected int _attributetypeid;
        protected int _level;
        protected int _value;
        protected string _entitytypeids;
        protected string _keyword;
        protected string _startdate;
        protected string _enddate;
        protected string _treevalues;
        protected string _entitymemberids;
        #endregion

        #region Constructors
        public FiltersettingsValues() { }

        public FiltersettingsValues(int pId, int pFilterId, int pAttributeId, int pLevel, int pValue, int pAttributeTypeId, string pEntityTypeIDs, string pKeyword, string pStartDate, string pEndDate, string pTreeValues, string pEntityMemberIDs)
        {
            this._id = pId;
            this._filterid = pFilterId;
            this._attributeid = pAttributeId;
            this._level = pLevel;
            this._value = pValue;
            this._attributeid = pAttributeTypeId;
            this._entitytypeids = pEntityTypeIDs;
            this._keyword = pKeyword;
            this._startdate = pStartDate;
            this._enddate = pEndDate;
            this._treevalues = pTreeValues;
            this._entitymemberids = pEntityMemberIDs;
        }

        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int FilterId
        {
            get { return _filterid; }
            set { _filterid = value; }

        }

        public int AttributeId
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public int AttributeTypeId
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }

        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }
        public int Value
        {
            get { return _value; }
            set { _value = value; }

        }
        public string EntityTypeIDs
        {
            get { return _entitytypeids; }
            set { _entitytypeids = value; }

        }

        public string Keyword
        {
            get { return _keyword; }
            set { _keyword = value; }
        }

        public string StartDate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        public string EndDate
        {
            get { return _enddate; }
            set { _enddate = value; }
        }
        public string TreeValues
        {
            get { return _treevalues; }
            set { _treevalues = value; }
        }

        public IList<DropDownTreePricing> DropdowntreePricingData { get; set; }
        public string EntityMemberIDs
        {
            get { return _entitymemberids; }
            set { _entitymemberids = value; }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FiltersettingsValues castObj = null;
            try
            {
                castObj = (FiltersettingsValues)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._filterid.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _filterid.GetHashCode();
            return hash;
        }
        #endregion
    }
}
