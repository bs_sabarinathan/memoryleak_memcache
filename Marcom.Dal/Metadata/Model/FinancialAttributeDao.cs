﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class FinancialAttributeDao : BaseDao, ICloneable
    {
        #region member Variables

        protected int _id { get; set; }
        protected int _fintypeid { get; set; }
        protected string _caption { get; set; }
        protected int _attributetypeid { get; set; }
        protected bool _issystemdefined { get; set; }
        protected string _description { get; set; }
        protected bool _iscolumn { get; set; }
        protected bool _istooptip { get; set; }
        protected bool _iscommittooltip { get; set; }
        protected int _SortOrder { get; set; }
        protected bool _IsHelptextEnabled { get; set; }
        protected string _HelptextDecsription { get; set; }

        #endregion


        #region Constructors
        public FinancialAttributeDao() { }
        public FinancialAttributeDao(int pid, int pfintypeid, string pcaption, int pattributetypeid, bool pissystemdefined, string pdescription, bool piscolumn, bool pistooptip, bool piscommittooltip, int pSortOrder)
        {
            this._id = pid;
            this._fintypeid = pfintypeid;
            this._caption = pcaption;
            this._attributetypeid = pattributetypeid;
            this._issystemdefined = pissystemdefined;
            this._description = pdescription;
            this._iscolumn = piscolumn;
            this._istooptip = pistooptip;
            this._iscommittooltip = piscommittooltip;
            this._SortOrder = pSortOrder;
        }
        #endregion


        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int FinTypeID
        {
            get { return _fintypeid; }
            set { _fintypeid = value; }
        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }
        }



        public virtual int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }
        }

        public virtual bool IsSystemDefined
        {
            get { return _issystemdefined; }
            set { _issystemdefined = value; }
        }


        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public virtual bool IsColumn
        {
            get { return _iscolumn; }
            set { _iscolumn = value; }
        }

        public virtual bool IsTooltip
        {
            get { return _istooptip; }
            set { _istooptip = value; }
        }

        public virtual bool IsCommitTooltip
        {
            get { return _iscommittooltip; }
            set { _iscommittooltip = value; }
        }

        public virtual int SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; }
        }
        public virtual bool IsHelptextEnabled
        {
            get { return _IsHelptextEnabled; }
            set { _IsHelptextEnabled = value; }
        }

        public virtual string HelptextDecsription
        {
            get { return _HelptextDecsription; }
            set { _HelptextDecsription = value; }
        }

        #endregion



        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion



        #region Public Property and Mapping Constants
        public static class PropertyNames
        {
            //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)

            public const string FinancialAttributeDao = "FinancialAttributeDao";
            public const string ID = "ID";
            public const string FinTypeID = "FinTypeID";
            public const string Caption = "Caption";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string IsSystemDefined = "IsSystemDefined";
            public const string Description = "Description";
            public const string IsColumn = "IsColumn";
            public const string IsTooltip = "IsTooltip";
            public const string IsCommitTooltip = "IsCommitTooltip";
            public const string SortOrder = "SortOrder";
            public const string IsHelptextEnabled = "IsHelptextEnabled";
            public const string HelptextDecsription = "HelptextDecsription";
        }


        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string FinancialAttributeDao = "FinancialAttributeDao";
            public const string ID = "ID";
            public const string FinTypeID = "FinTypeID";
            public const string Caption = "Caption";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string IsSystemDefined = "IsSystemDefined";
            public const string Description = "Description";
            public const string IsColumn = "IsColumn";
            public const string IsTooltip = "IsTooltip";
            public const string IsCommitTooltip = "IsCommitTooltip";
            public const string SortOrder = "SortOrder";
            public const string IsHelptextEnabled = "IsHelptextEnabled";
            public const string HelptextDecsription = "HelptextDecsription";
        }


        #endregion

    }
}
