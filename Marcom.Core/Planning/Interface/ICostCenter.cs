using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICostCenter interface for table 'PM_CostCenter'.
    /// </summary>
    public interface ICostCenter : IBaseEntity
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        decimal? AssignedAmount
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
