using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// AttributeTypeDao object for table 'MM_AttributeType'.
	/// </summary>
	
	public partial class AttributeTypeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _classname;
		protected bool _isselectable;
        protected string _datatype;
        protected string _sqltype;
        protected int _length;
        protected bool _isnullable;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public AttributeTypeDao() {}
			
		public AttributeTypeDao(string pCaption, string pClassName, bool pIsSelectable,string pDataType,string pSqlType,int pLength,bool pIsNullable)
		{
			this._caption = pCaption; 
			this._classname = pClassName; 
			this._isselectable = pIsSelectable;
            this._datatype = pDataType;
            this._sqltype = pSqlType;
            this._length = pLength;
            this._isnullable = pIsNullable; 
		}
				
		public AttributeTypeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual string ClassName
		{
			get { return _classname; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("ClassName", "ClassName value, cannot contain more than 100 characters");
			  _bIsChanged |= (_classname != value); 
			  _classname = value; 
			}
			
		}
		
		public virtual bool IsSelectable
		{
			get { return _isselectable; }
			set { _bIsChanged |= (_isselectable != value); _isselectable = value; }
			
		}

        public virtual string DataType
        {
            get { return _datatype; }
            set { _bIsChanged |= (_datatype != value); _datatype = value; }

        }

        public virtual string SqlType
        {
            get { return _sqltype; }
            set { _bIsChanged |= (_sqltype != value); _sqltype = value; }

        }

        public virtual int Length
        {
            get { return _length; }
            set { _bIsChanged |= (_length != value); _length = value; }

        }

        public virtual bool IsNullable
        {
            get { return _isnullable; }
            set { _bIsChanged |= (_isnullable != value); _isnullable = value; }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string AttributeTypeDao = "AttributeTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string ClassName = "ClassName";			
			public const string IsSelectable = "IsSelectable";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string AttributeTypeDao = "AttributeTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string ClassName = "ClassName";			
			public const string IsSelectable = "IsSelectable";			
		}

		#endregion
		
		
	}
	
}
