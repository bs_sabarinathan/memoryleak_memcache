///#source 1 1 /app/directives/admin-directives.js
(function (ng, app) {
    app.directive("bnsortable", ['$parse', function ($parse) {
        function link($scope, element, attributes) {
            function getNgRepeatExpression() {
                var ngRepeatComment = element.contents().filter(function () {
                    return ((this.nodeType === 8) && (this.nodeValue.indexOf("ngRepeat:") !== -1));
                });
                return (getNgRepeatExpressionFromComment(ngRepeatComment[0]));
            }

            function getNgRepeatExpressionFromComment(comment) {
                var parts = comment.nodeValue.split(":");
                return (parts[1].replace(/^\s+|\s+$/g, ""));
            }

            function getNgRepeatChildren() {
                var attributeVariants = ["[ng-repeat]", "[data-ng-repeat]", "[x-ng-repeat]", "[ng_repeat]", "[ngRepeat]", "[ng\\:repeat]"];
                return (element.children(attributeVariants.join(",")));
            }

            function handleUpdate(event, ui) {
                var children = getNgRepeatChildren();
                var items = $.map(children, function (domItem, index) {
                    var scope = $(domItem).scope();
                    scope[itemName].SortOrder = (index + 1);
                    return (scope[itemName]);
                });
                $scope.$apply(function () {
                    collectionSetter($scope, items);
                });
            }
            var expressionPattern = /^([^\s]+) in (.+)$/i;
            var expression = getNgRepeatExpression();
            if (!expressionPattern.test(expression)) {
                throw (new Error("Expected ITEM in COLLECTION expression."));
            }
            var expressionParts = expression.match(expressionPattern);
            var itemName = expressionParts[1];
            var collectionName = expressionParts[2];
            var collectionGetter = $parse(collectionName);
            var collectionSetter = collectionGetter.assign;
            element.sortable({
                cursor: "move",
                axis: 'y',
                opacity: 0.7,
                stop: handleUpdate
            });
            $scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
        return ({
            link: link,
            restrict: "A"
        });
    }]);
    app.directive('onRepeatLast', [function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var isLast = scope.$last || scope.$parent.$last;
                if (isLast) {
                    scope.$evalAsync(attrs.onRepeatLast);
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        }
    }]);
    app.controller('gridster1Ctrl', ['$window', '$scope', '$element', '$attrs', '$timeout', function ($window, $scope, $element, $attrs, $timeout) {
        var self = this;
        self.$el = $($element);
        self.gridster1 = null;
        self.updateModel = function (event, ui) {
            var serializedGrid = self.gridster1.serialize();
            angular.forEach($scope.widgets, function (widget, idx) {
                widget.grid = serializedGrid[idx];
            });
            $scope.$apply();
        };
        self.defaultOptions = {
            shift_larger_widgets_down: false,
            max_size_x: 6,
            cols: 6,
            margin_ratio: 0.1,
            resize_time: 500,
            serialize_params: function ($w, wgd) {
                return {
                    col: wgd.col,
                    row: wgd.row,
                    sizex: wgd.size_x,
                    sizey: wgd.size_y
                }
            },
            draggable: {
                stop: self.updateModel
            }
        };
        self.options = angular.extend(self.defaultOptions, $scope.$eval($attrs.options));
        self.attachElementTogridster1 = function (li) {
            var $w = li.addClass('gs_w').appendTo(self.gridster1.$el);
            self.gridster1.$widgets = self.gridster1.$widgets.add($w);
            self.gridster1.register_widget($w).add_faux_rows(1).set_dom_grid_height();
            $w.css('opacity', 1);
        };

        function calculateNewDimensions() {
            var containerWidth = self.$el.innerWidth();
            var newMargin = Math.round(containerWidth * self.options.margin_ratio / (self.options.cols * 2));
            var newSize = Math.round(containerWidth * (1 - self.options.margin_ratio) / self.options.cols);
            return [[newSize, newSize], [newMargin, newMargin]];
        }
        self.resizeWidgetDimensions = function () {
            var newDimensions = calculateNewDimensions();
            self.gridster1.resize_widget_dimensions({
                widget_base_dimensions: newDimensions[0],
                widget_margins: newDimensions[1]
            });
            self.updateModel();
        };
        self.hookWidgetResizer = function () {
            self.resizeWidgetDimensions();
            $($window).on('resize', function (evt) {
                evt.preventDefault();
                $window.clearTimeout(self.resizeTimer);
                self.resizeTimer = $window.setTimeout(function () {
                    self.resizeWidgetDimensions();
                }, self.options.resize_time);
            });
        };
        $scope.$watch('widgets.length', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            if (newValue === oldValue + 1) {
                $timeout(function () {
                    var li = self.$el.find('ul').find('li').last();
                    self.attachElementTogridster1(li);
                });
            } else { }
        });
        $scope.removeWidget = function (widget) {
            var id = widget.id;
            var $li = self.$el.find('[data-widget-id="' + id + '"]');
            $li.fadeOut('slow', function () {
                self.gridster1.remove_widget($li, function () {
                    $scope.onremove({
                        widget: widget
                    });
                    self.resizeWidgetDimensions();
                });
            });
        };
    }]);
    app.directive('gridster1', ['$timeout', function ($timeout) {
        return {
            restrict: 'AC',
            scope: {
                widgets: '=',
                onremove: '&'
            },
            templateUrl: 'views/mui/admin/gridster.html',
            controller: 'gridster1Ctrl',
            link: function (scope, elm, attrs, ctrl) {
                elm.css('opacity', 0);
                scope.initGrid = function () {
                    $timeout(function () {
                        var $ul = ctrl.$el.find('ul');
                        ctrl.gridster1 = $ul.gridster1(ctrl.options).data('gridster1');
                        ctrl.hookWidgetResizer();
                        scope.$broadcast('gridReady');
                        ctrl.resizeWidgetDimensions();
                        elm.css('opacity', 1);
                    });
                };
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('widget1', ['$timeout', 'uuid', function ($timeout, uuid) {
        return {
            restrict: 'A',
            require: '^gridster1',
            scope: {
                remove: '&',
                widget: '='
            },
            link: function (scope, element, attrs, ctrl) {
                var $el = $(element);
                if (scope.widget.id === undefined) {
                    scope.widget.id = uuid.generate();
                }
                scope.$on('gridReady', function (event) {
                    var base_size = ctrl.gridster1.options.widget_base_dimensions;
                    var margins = ctrl.gridster1.options.widget_margins;
                    var headerHeight = $el.find('header').outerHeight();
                    var $content = $el.find('.content');
                    $el.resizable({
                        grid: [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)],
                        animate: false,
                        containment: ctrl.$el,
                        autoHide: false,
                        start: function (event, ui) {
                            var base_size = ctrl.gridster1.options.widget_base_dimensions;
                            var margins = ctrl.gridster1.options.widget_margins;
                            element.resizable('option', 'grid', [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)]);
                        },
                        resize: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                        },
                        stop: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                            setTimeout(function () {
                                sizeToGrid($el);
                                ctrl.updateModel();
                            }, 300);
                        }
                    });
                    $('.ui-resizable-handle, .no-drag, .disabled, [disabled]', $el).hover(function () {
                        ctrl.gridster1.disable();
                    }, function () {
                        ctrl.gridster1.enable();
                    });
                });

                function sizeToGrid($el) {
                    var base_size = ctrl.gridster1.options.widget_base_dimensions;
                    var margins = ctrl.gridster1.options.widget_margins;
                    var w = ($el.width() - base_size[0]);
                    var h = ($el.height() - base_size[1]);
                    for (var grid_w = 1; w > 0; w -= ((base_size[0] + (margins[0] * 2)))) {
                        grid_w++;
                    }
                    for (var grid_h = 1; h > 0; h -= ((base_size[1] + (margins[1] * 2)))) {
                        grid_h++;
                    }
                    $el.css({
                        width: '',
                        height: '',
                        top: '',
                        left: '',
                        position: ''
                    });
                    ctrl.gridster1.resize_widget($el, grid_w, grid_h);
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('dropdragdir', [function () {
        return function (scope, element, attrs) {
            var toUpdate;
            var startIndex = -1;
            scope.$watch(attrs.dropdragdir, function (value) {
                toUpdate = value;
            }, true);
            $(element[0]).sortable({
                items: 'li',
                start: function (event, ui) {
                    startIndex = ($(ui.item).index());
                },
                stop: function (event, ui) {
                    var newIndex = ($(ui.item).index());
                    var toMove = toUpdate[startIndex];
                    toUpdate.splice(startIndex, 1);
                    toUpdate.splice(newIndex, 0, toMove);
                    scope.$apply(scope.entitytypeattributrearr);
                },
                axis: 'y'
            })
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive("reportblocksort", ['$parse', '$timeout', function ($parse, $timeout) {
        function link($scope, element, attributes) {
            function getNgRepeatExpression() {
                var ngRepeatComment = element.contents().filter(function () {
                    return ((this.nodeType === 8) && (this.nodeValue.indexOf("ngRepeat:") !== -1));
                });
                return (getNgRepeatExpressionFromComment(ngRepeatComment[0]));
            }

            function getNgRepeatExpressionFromComment(comment) {
                var parts = comment.nodeValue.split(":");
                return (parts[1].replace(/^\s+|\s+$/g, ""));
            }

            function getNgRepeatChildren() {
                var attributeVariants = ["[ng-repeat]", "[data-ng-repeat]", "[x-ng-repeat]", "[ng_repeat]", "[ngRepeat]", "[ng\\:repeat]"];
                return (element.children(attributeVariants.join(",")));
            }

            function handleUpdate(event, ui) {
                var children = getNgRepeatChildren();
                var items = $.map(children, function (domItem, index) {
                    var scope = $(domItem).scope();
                    return (scope[itemName]);
                });
                $scope.$apply(function () {
                    if ($scope.reportJSONObject.root != undefined) {
                        for (var i = 0; i < $scope.reportJSONObject.root.block.length; i++) {
                            if ($scope.reportJSONObject.root.block[i].id == $scope.blocks.id) {
                                $scope.reportJSONObject.root.block[i].columns[0].column = items;
                            }
                        }
                    }
                    collectionSetter($scope, items);
                });
            }
            var expressionPattern = /^([^\s]+) in (.+)$/i;
            var expression = getNgRepeatExpression();
            if (!expressionPattern.test(expression)) {
                throw (new Error("Expected ITEM in COLLECTION expression."));
            }
            var expressionParts = expression.match(expressionPattern);
            var itemName = expressionParts[1];
            var collectionName = expressionParts[2];
            var collectionGetter = $parse(collectionName);
            var collectionSetter = collectionGetter.assign;
            element.sortable({
                cursor: "move",
                update: handleUpdate,
                opacity: 0.7,
                vertical: true,
                axis: 'y',
            });
            $scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
        return ({
            link: link,
            restrict: "A"
        });
    }]);
    app.directive('detailblockcolumns', [function () {
        return {
            restrict: 'A',
            template: '<li class="active" ng-repeat="column in columnvalue.column">' + '<a href="JavaScript: void(0);">' + '<label class="checkbox checkbox-custom pull-left">' + '<input id="Checkbox2" ng-click="columnmanipulation(column,$event)" ng-checked="column.isselected==\'1\'" type="checkbox"><i class="checkbox" ng-class="{\'checkbox checked\': column.isselected==\'1\', \'checkbox\': column.isselected==\'0\'}"></i>' + ' </label>' + '<input type="text" ng-model="column.caption" placeholder="{{column.caption}}" class="input-medium">' + '</a>' + '</li>',
            transclude: true,
            scope: {
                columnvalue: '=',
                columncontent: '=',
                blocktype: '=',
                blockstaticcolumn: '=',
                blockentitytyperelation: '=',
                blockentitytype: '='
            },
            link: function (scope, elem, attrs) {
                var updateStars = function () {
                    if (scope.blockentitytype != "" && scope.blockentitytype != undefined) {
                        if (scope.columnvalue.column != undefined) {
                            var alreadyAvailed = [];
                            for (var i = 0, columndata; columndata = scope.blockstaticcolumn[i++];) {
                                alreadyAvailed = $.grep(scope.columnvalue.column, function (e) {
                                    return e.id == columndata.id;
                                });
                                if (alreadyAvailed.length == 0) {
                                    scope.columnvalue.column.push({
                                        id: columndata.id,
                                        caption: columndata.caption,
                                        isselected: columndata.isselected
                                    });
                                }
                            }
                            var dynamicattributes = [];
                            var dynamicattributesres = [];
                            dynamicattributesres = jQuery.grep(scope.blockentitytyperelation, function (relation) {
                                return scope.blockentitytype.indexOf(relation.EntityTypeID.toString()) != -1;
                            });
                            if (dynamicattributesres.length > 0) {
                                for (var j = 0, columntyperesdata; columntyperesdata = dynamicattributesres[j++];) {
                                    alreadyAvailed = [];
                                    alreadyAvailed = $.grep(scope.columnvalue.column, function (e) {
                                        return e.id == columntyperesdata.strAttributeID;
                                    });
                                    if (alreadyAvailed.length == 0) {
                                        scope.columnvalue.column.push({
                                            id: columntyperesdata.strAttributeID,
                                            caption: columntyperesdata.AttributeCaption,
                                            isselected: '0'
                                        });
                                    }
                                }
                            }
                        } else {
                            var defaultcolumns = [];
                            scope.columnvalue.column = scope.blockstaticcolumn;
                        }
                    } else {
                        if (scope.columnvalue != undefined) {
                            if (scope.columnvalue.column != undefined) {
                                scope.columnvalue.column = [];
                            }
                        } else {
                            var columns = [{
                                "column": []
                            }];
                            scope.columncontent["columns"] = columns;
                        }
                    }
                };
                scope.columnmanipulation = function (column, $event) {
                    var checkbox = $event.target;
                    column.isselected = checkbox.checked == true ? '1' : '0';
                };
                scope.$watch('blocktype', function (oldVal, newVal) {
                    if (newVal) {
                        if (newVal == 3) {
                            updateStars();
                        }
                    }
                });
                scope.$watch('blockentitytype', function (oldVal, newVal) {
                    if (newVal) {
                        if (scope.blocktype == 3) {
                            updateStars();
                        }
                    } else if (newVal == null) {
                        updateStars();
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        }
    }]);
    app.directive('ngblur', [function () {
        return {
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function () {
                    var id = attrs.langtypeid;
                    var typeid = attrs.contentid;
                    var iterater = attrs.iterater;
                    var oldvalue = scope.actualfields["Caption_" + iterater + ""];
                    var newvalue = element[0].value;
                    if (oldvalue != newvalue) {
                        $(element).addClass('success');
                        scope.newlanguagecontent(attrs.langtypeid, attrs.contentid, newvalue, element);
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablelanguagename', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var LanguageID = parseInt(attrs.languageid, 10);
                var IsName = attrs.isname;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            if (value == "") value = "-";
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.UpdateLanguageName(LanguageID, IsName, params.newValue);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablereports', ['$timeout', '$translate', function ($timeout, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var ISIDCaption = parseInt(attrs.isidcaption, 10);
                var ID = parseInt(attrs.isid, 10);
                var OID = parseInt(attrs.isoid, 10);
                var Name = attrs.isname;
                var Caption = attrs.iscaption;
                var Description = attrs.isdescription;
                var preview = attrs.ispreview;
                var Show = attrs.isshow;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        },
                        validate: function (value) {
                            if ($.trim(value) == '') {
                                if (parseInt(attrs.isidcaption, 10) == 1) {
                                    return 'Please enter the caption';
                                }
                                if (parseInt(attrs.isidcaption, 10) == 0) {
                                    return 'Please enter the description';
                                }
                            }
                            if ($.trim(value) == '-') {
                                if (parseInt(attrs.isidcaption, 10) == 1) {
                                    return 'Please enter the valid text';
                                }
                            }
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    if (editable != undefined) {
                        $timeout(function () {
                            $(".input-medium").focus().select()
                        }, 500);
                        $('[class=input-medium]').keyup(function (e) {
                            if (e.keyCode == 32) {
                                if (($('[class=input-medium]').val().length == 1)) {
                                    var value = $.trim($('[class=input-medium]').val());
                                    $('[class=input-medium]').val(value);
                                    bootbox.alert($translate.instant('LanguageContents.Res_1841.Caption'));
                                }
                            }
                        });
                    }
                });
                angular.element(element).on('save', function (e, params) {
                    scope.UpdateReportValues(parseInt(attrs.isidcaption, 10), parseInt(attrs.isid, 10), parseInt(attrs.isoid, 10), attrs.isname, attrs.iscaption, attrs.isdescription, attrs.ispreview, attrs.isshow, attrs.iscategoryid, attrs.isentitylevel, attrs.issublevel, params.newValue);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive("entitytypetreetooltip", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            var attrslst = attrs;
            $(element).qtip({
                content: {
                    text: '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',
                },
                events: {
                    show: function (event, api) {
                        if (attrslst.tooltiptype == "ENTITYTYPETREE") {
                            var qtipContent_id = api.elements.content.attr('id');
                            scope.LoadtQtipForEntityTypeTreeStructure(qtipContent_id, attrslst.entitytypeid);
                        }
                    }
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom center',
                        tooltip: 'bottom center',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('dropdragdir', [function () {
        return function (scope, element, attrs) {
            var toUpdate;
            var startIndex = -1;
            scope.$watch(attrs.dropdragdir, function (value) {
                toUpdate = value;
            }, true);
            $(element[0]).sortable({
                items: 'li',
                start: function (event, ui) {
                    startIndex = ($(ui.item).index());
                },
                stop: function (event, ui) {
                    var newIndex = ($(ui.item).index());
                    var toMove = toUpdate[startIndex];
                    toUpdate.splice(startIndex, 1);
                    toUpdate.splice(newIndex, 0, toMove);
                    scope.$apply(scope.entitytypeattributrearr);
                },
                axis: 'y'
            })
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('contenteditable', ['$sce', function ($sce) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) return;
                ngModel.$render = function () {
                    element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                };
                element.on('blur keyup change', function () {
                    scope.$evalAsync(read);
                });
                read();

                function read() {
                    var html = element.html();
                    if (attrs.stripBr && html == '<br>') {
                        html = '';
                    }
                    ngModel.$setViewValue(html);
                }
            }
        };
    }]);
    app.directive('uicolorpicker', function () {
        return {
            restrict: 'E',
            require: 'ngModel',
            replace: true,
            template: '<span><input  class="input-small" /></span>',
            link: function (scope, $element, attrs, $ngModel) {
                var $input = $element.find('input');
                var fallbackValue = scope.$eval(attrs.fallbackValue);
                var format = scope.$eval(attrs.format) || undefined;

                function setViewValue(color) {
                    var value = fallbackValue;
                    if (color) {
                        value = color.toString(format);
                    } else if (angular.isUndefined(fallbackValue)) {
                        value = color;
                    }
                    $ngModel.$setViewValue(value.replace("#", ""));
                    scope.colorchange(value);
                }
                var onChange = function (color) {
                    scope.$apply(function () {
                        setViewValue(color);
                    });
                };
                var onToggle = function () {
                    $input.spectrum('toggle');
                    return false;
                };
                var options = angular.extend({
                    color: $ngModel.$viewValue,
                    change: onChange,
                }, scope.$eval(attrs.options));
                $ngModel.$render = function () {
                    $input.spectrum('set', $ngModel.$viewValue || '');
                };
                if (options.color) {
                    $input.spectrum('set', options.color || '');
                    setViewValue(options.color);
                }
                $input.spectrum(options);
                scope.$on('$destroy', function () {
                    $input.spectrum('destroy');
                });
            }
        };
    });
    app.directive('intentbuttonsRadio', function () {
        return {
            restrict: 'E',
            scope: {
                model: '=',
                options: '=',
                changeselection: "&"
            },
            controller: function ($scope) {
                $scope.activate = function (option) {
                    $scope.model = option.Lable;
                };
                $scope.Returnclass = function (model) {
                    if ($scope.model == model.Lable) {
                        return "btn";
                    } else return "btn";
                }
            },
            template: "<button type='button' class='{{Returnclass(option)}}' " + "ng-class='{active: option.Lable == model}'" + "ng-repeat='option in options' " + "ng-click='activate(option);changeselection(option);'>" + "<i ng-class='option.class'></i>" + "</button>"
        };
    });
})(angular, app);
///#source 1 1 /app/directives/dam-directives.js
(function(ng,app){app.directive("damlistviewpreviewtooltip",['$compile','$parse','$rootScope',function($compile,$parse,$rootScope){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"lv-DAM_AssetsQtip");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";var attrslst=attrs;$(element).qtip({content:{text:'<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>',},events:{show:function(event,api){if(attrslst.tooltiptype=="assetlistview"){var qtipContent_id=api.elements.content.attr('id');scope.LoadAssetListViewMetadata(qtipContent_id,attrslst.entitytypeid,attrslst.backcolor,attrslst.shortdesc);}}},style:{classes:scope.qtipSkin+" qtip-dark qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'left top',at:'right top',viewport:$(window),corner:{target:'top right',tooltip:'top right',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");element.remove();});}}]);app.directive("damthumbnailviewtooltip",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"th-DAM_AssetsQtip");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";var attrslst=attrs;$(element).qtip({content:{text:'<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>',},events:{show:function(event,api){if(attrslst.tooltiptype=="assetthumbnailview"){var qtipContent_id=api.elements.content.attr('id');scope.LoadAssetthumbnailViewMetadata(qtipContent_id,attrslst.entitytypeid,attrslst.backcolor,attrslst.shortdesc);}}},style:{classes:scope.qtipSkin+" qtip-dark qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'left top',at:'right top',viewport:$(window),corner:{target:'top right',tooltip:'top right',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");element.remove();});}}]);app.directive("dampublisheddetailtooltip",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"th-DAM_AssetsQtip");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";var attrslst=attrs;$(element).qtip({content:{text:'<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>',},events:{show:function(event,api){var qtipContent_id=api.elements.content.attr('id'),linkdetailarr=[];if(attrslst.location=="link")
linkdetailarr=attrslst.linkdetails.split("###");var header=attrslst.location=="link"?"Linked asset":"Published asset",lable1=attrslst.location=="link"?"Source":"Published by",lable2=attrslst.location=="link"?"Owner":"Published on",value1=attrslst.location=="link"?(linkdetailarr.length>0?linkdetailarr[0]:""):attrslst.publishedby,value2=attrslst.location=="link"?(linkdetailarr.length>1?linkdetailarr[1]:""):attrslst.publishedon;value3=attrslst.location=="link"?(linkdetailarr.length>2?linkdetailarr[2]:""):"";if((attrslst.publishedby!=null&&!attrslst.publishedon.contains("1900"))||attrslst.location=="link"){var html="";html+=' <div class="th-AssetInfoTooltip">';html+=' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">'+lable1+'</span><span class="th-AssetInfoValue">'+value1+'</span></div>';html+=' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">'+lable2+'</span><span class="th-AssetInfoValue">'+value2+'</span></div>';if(attrslst.location=="link")
html+=' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Linked by</span><span class="th-AssetInfoValue">'+value3+'</span></div>';html+=' </div>';$('#'+qtipContent_id).html(html);$('#'+qtipContent_id).qtip('show').show();}
else{$('#'+qtipContent_id).html('');$('#'+qtipContent_id).qtip('hide').hide();}}},style:{classes:scope.qtipSkin+" qtip-dark qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'left top',at:'right top',viewport:$(window),corner:{target:'top right',tooltip:'top right',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive('damTaskTree',['$timeout',function($timeout){return{restrict:'E',template:"<ul class=\"nav nav-list nav-pills nav-stacked abn-tree\">\n  "+"<li ng-repeat=\"row in tree_rows | filter:{visible:true} track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row\">\n  "+"<a ng-click=\"user_clicks_branch(row.branch)\">\n  "+"<i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon\"> </i>\n  "+"<i ng-class=\"row.file_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon\" ng-style=\"set_color(row.ColorCode)\" style=\"color: #{{row.ColorCode}}\"> </i>\n  "+"<span my-qtip2 qtip-content=\"{{row.Description}}\" class=\"indented tree-label\">{{ row.Caption }} </span>\n "+"</a>\n  "+"</li>\n  </ul>",replace:true,scope:{treeData:'=',onSelect:'&',initialSelection:'=',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,tree,ininitialfolderfound=false;error=function(s){return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='3';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==selected_branch){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){return branch.onSelect(branch);});}else{if(scope.onSelect!=null){return $timeout(function(){return scope.onSelect({branch:branch});});}}}};scope.user_clicks_branch=function(branch){if(branch!==selected_branch){return select_branch(branch);}};scope.set_color=function(clr){if(clr!=null)
return{'color':"#"+clr.toString().trim()}
else
return''}
get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,file_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
if(!branch.Children||branch.Children.length===0){file_icon='icon-folder-close';}else{if(branch.expanded){file_icon='icon-folder-open';}else{file_icon='icon-folder-close';}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,Description:branch.Description,ColorCode:branch.ColorCode,FolderId:branch.id,tree_icon:tree_icon,file_icon:file_icon,visible:visible});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);scope.$watch('initialSelection',on_treeData_change,true);if(scope.initialSelection!=null){for_each_branch(function(b){if((b.Caption===scope.initialSelection)&&ininitialfolderfound==false){ininitialfolderfound=true;return $timeout(function(){return select_branch(b);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();return tree.select_branch(b);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b){select_branch(b);return b;};tree.get_Children=function(b){return b.Children;};tree.return_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){return p;}}};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){tree.select_branch(p);return p;}}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.remove_branch=function(parent,delete_branch){if(parent!=null){parent.Children.splice($.inArray(delete_branch,parent.Children),1);parent.expanded=true;}
return delete_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};tree.get_siblings=function(b){var p,siblings;if(b==null){b=selected_branch;}
if(b!=null){p=tree.get_parent_branch(b);if(p){siblings=p.Children;}else{siblings=scope.treeData;}
return siblings;}};tree.get_next_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
if(b!=null){siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i<n){return siblings[i+1];}}};tree.get_prev_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i>0){return siblings[i-1];}};tree.select_next_sibling=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_sibling(b);if(next!=null){return tree.select_branch(next);}}};tree.select_prev_sibling=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_sibling(b);if(prev!=null){return tree.select_branch(prev);}}};tree.get_first_child=function(b){var _ref;if(b==null){b=selected_branch;}
if(b!=null){if(((_ref=b.Children)!=null?_ref.length:void 0)>0){return b.Children[0];}}};tree.get_closest_ancestor_next_sibling=function(b){var next,parent;next=tree.get_next_sibling(b);if(next!=null){return next;}else{parent=tree.get_parent_branch(b);return tree.get_closest_ancestor_next_sibling(parent);}};tree.get_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_first_child(b);if(next!=null){return next;}else{next=tree.get_closest_ancestor_next_sibling(b);return next;}}};tree.select_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_branch(b);if(next!=null){tree.select_branch(next);return next;}}};tree.last_descendant=function(b){var last_child;if(b==null){}
n=b.Children.length;if(n===0){return b;}else{last_child=b.Children[n-1];return tree.last_descendant(last_child);}};tree.get_prev_branch=function(b){var parent,prev_sibling;if(b==null){b=selected_branch;}
if(b!=null){prev_sibling=tree.get_prev_sibling(b);if(prev_sibling!=null){return tree.last_descendant(prev_sibling);}else{parent=tree.get_parent_branch(b);return parent;}}};return tree.select_prev_branch=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_branch(b);if(prev!=null){tree.select_branch(prev);return prev;}}};}}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('damOptimakerTree',['$timeout',function($timeout){return{restrict:'E',template:"<ul class=\"nav nav-list nav-pills nav-stacked abn-tree\">\n  "+"<li ng-repeat=\"row in tree_rows | filter:{visible:true} track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row\">\n  "+"<a ng-click=\"user_clicks_branch(row.branch)\">\n  "+"<i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon\" ng-style=\"set_color(row.ColorCode)\" style=\"color: #{{row.ColorCode}}\"> </i>\n  "+"<span class=\"indented tree-label\">{{ row.Caption }} </span>\n "+"</a>\n  "+"</li>\n  </ul>",replace:true,scope:{treeData:'=',onSelect:'&',initialSelection:'=',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,tree,ininitialfolderfound=false;error=function(s){return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='1';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==selected_branch){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){return branch.onSelect(branch);});}else{if(scope.onSelect!=null){return $timeout(function(){return scope.onSelect({branch:branch});});}}}};scope.user_clicks_branch=function(branch){if(branch!==selected_branch){return select_branch(branch);}};scope.set_color=function(clr){if(clr!=null)
return{'color':"#"+clr.toString().trim()}
else
return''}
get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,file_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
if(!branch.Children||branch.Children.length===0){file_icon='icon-folder-close';}else{if(branch.expanded){file_icon='icon-folder-open';}else{file_icon='icon-folder-close';}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,tree_icon:tree_icon,file_icon:file_icon,visible:visible});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);scope.$watch('initialSelection',on_treeData_change,true);if(scope.initialSelection!=null){for_each_branch(function(b){if((b.Caption===scope.initialSelection)&&ininitialfolderfound==false){ininitialfolderfound=true;return $timeout(function(){return select_branch(b);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();return tree.select_branch(b);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b){select_branch(b);return b;};tree.get_Children=function(b){return b.Children;};tree.return_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){return p;}}};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){tree.select_branch(p);return p;}}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.remove_branch=function(parent,delete_branch){if(parent!=null){parent.Children.splice($.inArray(delete_branch,parent.Children),1);parent.expanded=true;}
return delete_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};tree.get_siblings=function(b){var p,siblings;if(b==null){b=selected_branch;}
if(b!=null){p=tree.get_parent_branch(b);if(p){siblings=p.Children;}else{siblings=scope.treeData;}
return siblings;}};tree.get_next_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
if(b!=null){siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i<n){return siblings[i+1];}}};tree.get_prev_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i>0){return siblings[i-1];}};tree.select_next_sibling=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_sibling(b);if(next!=null){return tree.select_branch(next);}}};tree.select_prev_sibling=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_sibling(b);if(prev!=null){return tree.select_branch(prev);}}};tree.get_first_child=function(b){var _ref;if(b==null){b=selected_branch;}
if(b!=null){if(((_ref=b.Children)!=null?_ref.length:void 0)>0){return b.Children[0];}}};tree.get_closest_ancestor_next_sibling=function(b){var next,parent;next=tree.get_next_sibling(b);if(next!=null){return next;}else{parent=tree.get_parent_branch(b);return tree.get_closest_ancestor_next_sibling(parent);}};tree.get_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_first_child(b);if(next!=null){return next;}else{next=tree.get_closest_ancestor_next_sibling(b);return next;}}};tree.select_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_branch(b);if(next!=null){tree.select_branch(next);return next;}}};tree.last_descendant=function(b){var last_child;if(b==null){}
n=b.Children.length;if(n===0){return b;}else{last_child=b.Children[n-1];return tree.last_descendant(last_child);}};tree.get_prev_branch=function(b){var parent,prev_sibling;if(b==null){b=selected_branch;}
if(b!=null){prev_sibling=tree.get_prev_sibling(b);if(prev_sibling!=null){return tree.last_descendant(prev_sibling);}else{parent=tree.get_parent_branch(b);return parent;}}};return tree.select_prev_branch=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_branch(b);if(prev!=null){tree.select_branch(prev);return prev;}}};}}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('dropdragdir',[function(){return function(scope,element,attrs){var toUpdate;var startIndex=-1;scope.$watch(attrs.dropdragdir,function(value){toUpdate=value;},true);$(element[0]).sortable({items:'li',start:function(event,ui){startIndex=($(ui.item).index());},stop:function(event,ui){var newIndex=($(ui.item).index());var toMove=toUpdate[startIndex];toUpdate.splice(startIndex,1);toUpdate.splice(newIndex,0,toMove);scope.$apply(scope.entitytypeattributrearr);},axis:'y'})}}]);app.directive('damTree',['$timeout',function($timeout){return{restrict:'E',template:"<ul  class=\"nav nav-list nav-pills nav-stacked abn-tree\">\n  "+"<li ng-repeat=\"row in tree_rows | filter:{visible:true} track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row\">\n  "+"<a ng-click=\"user_clicks_branch(row.branch)\">\n  "+"<i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon\"> </i>\n  "+"<i ng-class=\"row.file_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon\" ng-style=\"set_color(row.ColorCode)\" style=\"color: #{{row.ColorCode}}\"> </i>\n  "+"<span my-qtip2 qtip-content=\"{{row.Description}}\" class=\"indented tree-label\">{{ row.Caption }} </span>\n "+"</a>\n  "+"<i id=\"ActionMenuIcon\" ng-click=\"context_clicks_branch(row.branch)\"  class=\"icon-reorder menuIcon\" data-toggle='dropdown' data-role=\"\" context=\"folderActionMenu\" > </i>\n  "+"</li>\n  </ul>",replace:true,scope:{treeData:'=',onSelect:'&',onContextclick:'&',initialSelection:'=',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,trackcontext_branch,tree,ininitialfolderfound=false;error=function(s){return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='3';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==selected_branch){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){return branch.onSelect(branch);});}else{if(scope.onSelect!=null){return $timeout(function(){return scope.onSelect({branch:branch});});}}}};trackcontext_branch=function(branch){if(branch.onContextclick!=null){var parent;parent=get_parent(branch);return $timeout(function(){return branch.onContextclick(branch,parent);});}
else{if(scope.onContextclick!=null){var parent;parent=get_parent(branch);return $timeout(function(){return scope.onContextclick({branch:branch,parent:parent});});}}};scope.user_clicks_branch=function(branch){if(branch!==selected_branch){return select_branch(branch);}};scope.set_color=function(clr){if(clr!=null)
return{'color':"#"+clr.toString().trim()}
else
return''}
scope.context_clicks_branch=function(branch){return trackcontext_branch(branch);};get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,file_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
if(!branch.Children||branch.Children.length===0){file_icon='icon-folder-close';}else{if(branch.expanded){file_icon='icon-folder-open';}else{file_icon='icon-folder-close';}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,Description:branch.Description,ColorCode:branch.ColorCode,FolderId:branch.id,tree_icon:tree_icon,file_icon:file_icon,visible:visible});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);scope.$watch('initialSelection',on_treeData_change,true);if(scope.initialSelection!=null){for_each_branch(function(b){if((b.Caption===scope.initialSelection)&&ininitialfolderfound==false){ininitialfolderfound=true;return $timeout(function(){return select_branch(b);});}});}
if(scope.initialSelection!=null){for_each_branch(function(b){if((b.Caption===scope.initialSelection)&&ininitialfolderfound==false){return $timeout(function(){return trackcontext_branch(b);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();return tree.select_branch(b);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b){select_branch(b);return b;};tree.get_Children=function(b){return b.Children;};tree.return_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){return p;}}};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);if(p!=null){tree.select_branch(p);return p;}}};tree.forcefullyselect_parent_branch=function(parent){var p=parent;if(p!=null){tree.select_branch(p);return p;}};tree.breadcrum_selection_branch=function(branchid){var selected_branch=$.grep(scope.tree_rows,function(e){return e.FolderId==branchid;});if(selected_branch!=undefined){tree.select_branch(selected_branch[0].branch);}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.remove_branch=function(parent,delete_branch){if(parent!=null){parent.Children.splice($.inArray(delete_branch,parent.Children),1);parent.expanded=true;}
else{scope.treeData.splice($.inArray(delete_branch,scope.treeData),1);}
return delete_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};tree.get_siblings=function(b){var p,siblings;if(b==null){b=selected_branch;}
if(b!=null){p=tree.get_parent_branch(b);if(p){siblings=p.Children;}else{siblings=scope.treeData;}
return siblings;}};tree.get_next_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
if(b!=null){siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i<n){return siblings[i+1];}}};tree.get_prev_sibling=function(b){var i,siblings;if(b==null){b=selected_branch;}
siblings=tree.get_siblings(b);n=siblings.length;i=siblings.indexOf(b);if(i>0){return siblings[i-1];}};tree.select_next_sibling=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_sibling(b);if(next!=null){return tree.select_branch(next);}}};tree.select_prev_sibling=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_sibling(b);if(prev!=null){return tree.select_branch(prev);}}};tree.get_first_child=function(b){var _ref;if(b==null){b=selected_branch;}
if(b!=null){if(((_ref=b.Children)!=null?_ref.length:void 0)>0){return b.Children[0];}}};tree.get_closest_ancestor_next_sibling=function(b){var next,parent;next=tree.get_next_sibling(b);if(next!=null){return next;}else{parent=tree.get_parent_branch(b);return tree.get_closest_ancestor_next_sibling(parent);}};tree.get_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_first_child(b);if(next!=null){return next;}else{next=tree.get_closest_ancestor_next_sibling(b);return next;}}};tree.select_next_branch=function(b){var next;if(b==null){b=selected_branch;}
if(b!=null){next=tree.get_next_branch(b);if(next!=null){tree.select_branch(next);return next;}}};tree.last_descendant=function(b){var last_child;if(b==null){}
n=b.Children.length;if(n===0){return b;}else{last_child=b.Children[n-1];return tree.last_descendant(last_child);}};tree.get_prev_branch=function(b){var parent,prev_sibling;if(b==null){b=selected_branch;}
if(b!=null){prev_sibling=tree.get_prev_sibling(b);if(prev_sibling!=null){return tree.last_descendant(prev_sibling);}else{parent=tree.get_parent_branch(b);return parent;}}};return tree.select_prev_branch=function(b){var prev;if(b==null){b=selected_branch;}
if(b!=null){prev=tree.get_prev_branch(b);if(prev!=null){tree.select_branch(prev);return prev;}}};}}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('categoryTree',['$timeout',function($timeout){return{restrict:'E',template:"<ul class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">"
+"<li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n"
+" <div class=\"categoryLink\" ng-switch=\"row.branch.IsDeleted\">"
+"     <a ng-switch-when=\"true\" class=\"treesearchcls nopadding read-only\" ng-click=\"user_clicks_branch_1(row.branch,$event)\">"
+"         <i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon point-all treesearchcls\"></i>\n"
+"         <span class=\"indented tree-label treesearchcls bold\" >{{ row.Caption }} </span>\n"
+"     </a>\n"
+"     <a ng-switch-when=\"false\" class=\"treesearchcls nopadding\" ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n"
+"         <i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon treesearchcls\"></i>\n"
+"         <label class=\"inlineBlock radio radio-custom\">"
+"             <input type=\"radio\" name=\"imgType\">"
+"             <i ng-class=\"{'radio checked': row.branch.ischecked , 'radio': !row.branch.ischecked}\"></i>"
+"         </label>"
+"         <span style=\"background-color: #{{row.branch.ColorCode}}\" class=\"eicon-s margin-right5x indented\">{{row.branch.Description}}</span>\n"
+"         <span class=\"indented tree-label treesearchcls\">{{ row.Caption }} </span>\n"
+"     </a>\n"
+"</div>"
+"</li>\n</ul>",scope:{treeData:'=',treeFilter:'=',onSelect:'&',initialSelection:'@',accessable:'@',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,tree;error=function(s){console.log('ERROR:'+s);return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='100';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){console.log('no treeData defined for the tree!');return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{console.log('treeData should be an array of root branches');return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined&&_ref.length>0)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch,parentArr){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==undefined){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return branch.onSelect(branch,test);});}else{if(scope.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return scope.onSelect({branch:branch,parent:test});});}}}};scope.parenttester=[];scope.user_clicks_branch=function(branch){if(branch!==selected_branch){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return select_branch(branch,test);}};scope.filterItem=function(item){if(!scope.treeFilter){item.isShow=true;if(item.expanded!=undefined){item.expanded=true;}
return true;}
var found=item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase())!=-1;if(!found){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){var match=scope.filterItem(item);if(match){found=true;item.isShow=true;}});}
return found;};function collapseOnemptySearch(item){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){item.isShow=true;});}
scope.user_clicks_branch_Expand_Collapse=function(branch,event){var test=[];var parent;parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}};function forcefullyremoveselection(branch){for(var z=0,childObj;childObj=scope.tree_rows[z++];){if(childObj.branch.id!=branch.id)
childObj.branch.ischecked=false;}}
scope.user_clicks_branch_1=function(branch,event){var test=[];var parent;forcefullyremoveselection(branch);parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){branch.ischecked=!branch.ischecked
for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}
else{branch.ischecked=branch.ischecked;return select_branch(branch,test);}};function Expandcollapse(Children,expanded){for(var z=0,childObj;childObj=Children[z++];){childObj.isShow=expanded;if(childObj.Children.length>0){Expandcollapse(childObj.Children,expanded);}}}
function recursiveparent(parent){var parent1;parent1=get_parent(parent);if(parent1!=null&&parent1!=undefined){scope.parenttester.push(parent1);recursiveparent(parent1);}
return scope.parenttester;}
get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];scope.treeAccessable=attrs.accessable!=null?(attrs.accessable=="false"?false:true):false;on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,id:branch.id,tree_icon:tree_icon,visible:visible,ischecked:branch.ischecked!=undefined?branch.ischecked:false,isShow:branch.isShow!=undefined?branch.isShow:true,});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined&&_ref.length>0)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);if(attrs.initialSelection!=null){for_each_branch(function(b){if(b.Caption===attrs.initialSelection){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return $timeout(function(){return select_branch(b,test);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return tree.select_branch(b,test);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b,parentArr){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
select_branch(b,test);return b;};tree.get_children=function(b){return b.Children;};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
if(p!=null){tree.select_branch(p,test);return p;}}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};}}}};}]);app.directive('categoryTreeadmin',['$timeout',function($timeout){return{restrict:'E',template:"<ul class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">"
+"<li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n"
+" <div class=\"categoryLink\" ng-switch=\"row.branch.IsDeleted\">"
+"     <a ng-switch-when=\"true\" class=\"treesearchcls nopadding read-only\" ng-click=\"user_clicks_branch_1(row.branch,$event)\">"
+"         <i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon point-all treesearchcls\"></i>\n"
+"         <span class=\"indented tree-label treesearchcls\" >{{ row.Caption }} </span>\n"
+"     </a>\n"
+"     <a ng-switch-when=\"false\" class=\"treesearchcls nopadding\" ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n"
+"         <i ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\" class=\"indented tree-icon treesearchcls\"></i>\n"
+"         <label class=\"inlineBlock radio radio-custom\">"
+"             <input type=\"radio\" name=\"imgType\">"
+"             <i ng-class=\"{'radio checked': row.branch.ischecked , 'radio': !row.branch.ischecked}\"></i>"
+"         </label>"
+"         <span class=\"indented tree-label treesearchcls\">{{ row.Caption }} </span>\n"
+"     </a>\n"
+"</div>"
+"</li>\n</ul>",scope:{treeData:'=',treeFilter:'=',onSelect:'&',initialSelection:'@',accessable:'@',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,tree;error=function(s){console.log('ERROR:'+s);return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='100';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){console.log('no treeData defined for the tree!');return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{console.log('treeData should be an array of root branches');return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined&&_ref.length>0)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch,parentArr){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==undefined){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return branch.onSelect(branch,test);});}else{if(scope.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return scope.onSelect({branch:branch,parent:test});});}}}};scope.parenttester=[];scope.user_clicks_branch=function(branch){if(branch!==selected_branch){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return select_branch(branch,test);}};scope.filterItem=function(item){if(!scope.treeFilter){item.isShow=true;if(item.expanded!=undefined){item.expanded=true;}
return true;}
var found=item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase())!=-1;if(!found){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){var match=scope.filterItem(item);if(match){found=true;item.isShow=true;}});}
return found;};function collapseOnemptySearch(item){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){item.isShow=true;});}
scope.user_clicks_branch_Expand_Collapse=function(branch,event){var test=[];var parent;parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}};function forcefullyremoveselection(branch){for(var z=0,childObj;childObj=scope.tree_rows[z++];){if(childObj.branch.id!=branch.id)
childObj.branch.ischecked=false;}}
scope.user_clicks_branch_1=function(branch,event){var test=[];var parent;forcefullyremoveselection(branch);parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){branch.ischecked=!branch.ischecked
for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}
else{branch.ischecked=branch.ischecked;return select_branch(branch,test);}};function Expandcollapse(Children,expanded){for(var z=0,childObj;childObj=Children[z++];){childObj.isShow=expanded;if(childObj.Children.length>0){Expandcollapse(childObj.Children,expanded);}}}
function recursiveparent(parent){var parent1;parent1=get_parent(parent);if(parent1!=null&&parent1!=undefined){scope.parenttester.push(parent1);recursiveparent(parent1);}
return scope.parenttester;}
get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];scope.treeAccessable=attrs.accessable!=null?(attrs.accessable=="false"?false:true):false;on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,id:branch.id,tree_icon:tree_icon,visible:visible,ischecked:branch.ischecked!=undefined?branch.ischecked:false,isShow:branch.isShow!=undefined?branch.isShow:true,});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined&&_ref.length>0)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);if(attrs.initialSelection!=null){for_each_branch(function(b){if(b.Caption===attrs.initialSelection){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return $timeout(function(){return select_branch(b,test);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return tree.select_branch(b,test);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b,parentArr){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
select_branch(b,test);return b;};tree.get_children=function(b){return b.Children;};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
if(p!=null){tree.select_branch(p,test);return p;}}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};}}}};}]);})(angular,app);
///#source 1 1 /app/directives/dashboard-directives.js
(function (ng, app) {




})(angular, app);
///#source 1 1 /app/directives/mui-directives.js
(function(ng,app){app.directive('compile',['$compile',function($compile){return function(scope,element,attrs){scope.$watch(function(scope){return scope.$eval(attrs.compile);},function(value){element.html(value);$compile(element.contents())(scope);});};}]);app.directive('attributeGroup',[function(){return{restrict:'EA',templateUrl:'views/mui/planningtool/component/attributegroup.html',controller:'mui.planningtool.component.attributegroupCtrl',replace:true,scope:{itemIndex:'@',itemGroupId:'@',itemTitle:'@',itemProcessedNumber:'=itemProcessed',itemIsLock:'@',itemLanguageContent:'=',itemImagecrop:'=',itemImagefilename:'@'}}}]);app.directive('attributegroupIndetailblock',[function(){return{restrict:'EA',templateUrl:'views/mui/planningtool/component/attributegroupIndetailblock.html',controller:'mui.planningtool.component.attributegroupCtrl',replace:true,scope:{itemIndex:'@',itemGroupId:'@',itemTitle:'@',itemProcessedNumber:'=itemProcessed',itemIsLock:'@',itemLanguageContent:'=',itemImagecrop:'=',itemImagefilename:'@'}}}]);app.directive('attributeGrouplistview',[function(){return{restrict:'EA',templateUrl:'views/mui/planningtool/component/attributegroupListView.html',controller:'mui.planningtool.component.attributegroupCtrl',replace:true,scope:{itemIndex:'@',itemGroupId:'@',itemTitle:'@',itemProcessedNumber:'=itemProcessed',itemIsLock:'@',itemLanguageContent:'=',itemImagecrop:'=',itemImagefilename:'@'}}}]);app.directive('attributeGrouplistviewindetail',[function(){return{restrict:'EA',templateUrl:'views/mui/planningtool/component/attributegroupListViewInDetailBlock.html',controller:'mui.planningtool.component.attributegroupCtrl',replace:true,scope:{itemIndex:'@',itemGroupId:'@',itemTitle:'@',itemProcessedNumber:'=itemProcessed',itemIsLock:'@',itemLanguageContent:'=',itemImagecrop:'=',itemImagefilename:'@'}}}]);app.directive('attributeGrouplistganttviewincustomtab',[function(){return{restrict:'EA',replace:false,transclude:true,templateUrl:'views/mui/planningtool/component/attributegroupListGanttViewInCustomTab.html',controller:'mui.planningtool.component.attributegrouplistganttviewincustomtabCtrl',scope:{itemIndex:'@',itemGroupId:'@',itemTitle:'@',itemProcessedNumber:'=itemProcessed',itemIsLock:'@',itemLanguageContent:'=',itemImagecrop:'=',itemImagefilename:'@'}}}]);app.directive('directiveTagwords',[function(){return{restrict:'EA',templateUrl:'views/mui/planningtool/component/tagwords.html',controller:'mui.planningtool.component.tagwordsCtrl',replace:false,transclude:true,scope:{itemAttrid:'@',itemTagwordId:'=',itemTagwordList:'=',itemShowHideProgress:'=',searchfrtag:'&',itemCleartag:'='}}}]);app.directive('ngblur',[function(){return function(scope,element,attrs){if(element.length>0){element[0].onblur=function(){eval(attrs.ngblur);};}};}]);app.directive('ngfocus',[function(){return function(scope,element,attrs){if(element.length>0){element[0].onfocus=function(){eval(attrs.ngfocus);};};};}]);app.directive('ngload',[function(){return function(scope,element,attrs){if(element.length>0){element[0].justload=function(){eval(attrs.ngload);};element[0].justload();};};}]);app.directive('xuserinfoeditable',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var loadXuserinfoeditable=function(){angular.element(element).editable({display:function(value,srcData){if((value==="")||(value===undefined))value="-";ngModel.$setViewValue(value);element.html(value);scope.$apply();}});}
$timeout(function(){loadXuserinfoeditable();},10);angular.element(element).on('save',function(e,params){scope.saveuserdetailsinfo(attrs.attributeid,params.newValue);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablusergender',['$timeout','$compile',function($timeout,$compile){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){var status='';ngModel.$setViewValue(value);if(value=="1")
status='Reached';else
status='Not Reached';if(!(value instanceof Date))element.html(status);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditable44',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var loadXeditable2=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);scope.$apply();}});}
$timeout(function(){loadXeditable2();},10);angular.element(element).on('save',function(e,params){scope.saveuserdetailsinfo(attrs.attributeid,params.newValue.toString('dd/MM/yyyy'));});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabledatepicker',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var loadXeditable2=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);scope.$apply();}});}
$timeout(function(){loadXeditable2();},10);angular.element(element).on('save',function(e,params){scope.saveuserdetailsinfo(attrs.attributeid,params.newValue.toString('dd/MM/yyyy'));});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabledropdownlanguage',['$timeout','$compile','$resource','$window','CommonService',function($timeout,$compile,$resource,$window,CommonService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel,cookies){var Version=1;var langid=attrs.langid;var UserID=attrs.userid;var isDataTypeExists=false;CommonService.GetLanguageTypes().then(function(languagetypes){$window.XeditableDirectivesSource["languagetext"]=languagetypes.Response;scope.normaltreeSources["languagetext"]=languagetypes.Response;});for(var variable in $.fn.editabletypes){if(langid==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(langid,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="languagetext" name="language settings" class="ngcompileDropdown">';inlinePopup+='<option value=\"{{ndata.ID}}\" ng-repeat=\"ndata in normaltreeSources.languagetext\">{{ndata.Name}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes['marcomlangage']=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['languagetext']=(($.grep(scope.normaltreeSources["languagetext"],function(e){return e.Name==scope.fields['languagetext']}))[0]).ID;}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];var dropdowntext='languagetext';var langnewid=scope.languagetext;scope.saveLanguageUserSettings(attrs.userid,langnewid,scope['languagetext']);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabletreedropdownmypage',['$timeout','$compile','$resource','$window',function($timeout,$compile,$resource,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var levelCount=scope.treelevels['dropdown_levels_'+attrs.attributeid].length
var isDataTypeExists=false;if(attrs.choosefromparent=="false"){MetadataService.GetTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
else{MetadataService.GetTreeNodeByEntityID(attrs.attributeid,attrs.entityid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
function DesignXeditable(){MetadataService.GetTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
for(var i=1;i<=levelCount;i++){if(i==1){scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['dropdown_'+attrs.attributeid].Children;scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}
else{scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=[];scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='<table>';for(var i=1;i<=levelCount;i++){if(i==1){var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}
else{var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td>  <label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}}
inlinePopupDesign1+='</table>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);for(var i=1;i<=levelCount;i++){var dropdown_text='dropdown_text_'+attrs.attributeid+'_'+i;if(i==1){scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope.treeSources["dropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}
else{if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)]!=undefined){if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined){scope.BindChildDropdownSource((attrs.attributeid),levelCount,(i-1),6);scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}}}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];for(var i=1;i<=levelCount;i++){var dropdown_text='DropDown_'+attrs.attributeid+'_'+i;if((scope['dropdown_'+attrs.attributeid+'_'+i]!=undefined)&&(scope['dropdown_'+attrs.attributeid+'_'+i]!="")){scope.fields[dropdown_text]=scope['dropdown_'+attrs.attributeid+'_'+i];if(scope['dropdown_'+attrs.attributeid+'_'+i].id==undefined||scope['dropdown_'+attrs.attributeid+'_'+i].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['dropdown_'+attrs.attributeid+'_'+i].id);}
scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]=scope['dropdown_'+attrs.attributeid+'_'+i].Caption;}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]='-';}
scope.$apply();}
scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});});}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabletextmypage',['$timeout','$compile','$resource','$window',function($timeout,$compile,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeID=attrs.attributeid;var Address=function(options){this.init(attributeID,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';if(parseInt(attrs.attributetypeid)==1)
inlinePopup+='<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'"></label></div>';else if(parseInt(attrs.attributetypeid)==2)
inlinePopup+='<div><label><textarea class="ngcompile" id="SingleTextXeditalble'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ></textarea></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeID]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope['SingleTextValue_'+attrs.attributeid]=scope.fields['SingleLineTextValue_'+attrs.attributeid];var isValidationReq="";if(scope.listAttributeValidationResult!=undefined){isValidationReq=$.grep(scope.listAttributeValidationResult,function(e){return(e.AttributeID==parseInt(attrs.attributeid));});}
$("#overviewDetialblock").removeClass('warning');if(isValidationReq!=""&&isValidationReq!=undefined){if(isValidationReq.length>0&&isValidationReq[0].RelationShipID!=0){var metrics=[['#SingleTextXeditalble'+attrs.attributeid+'',isValidationReq[0].ValueType,isValidationReq[0].ErrorMessage]];var options={'groupClass':'warning',};$("#overviewDetialblock").nod(metrics,options);}}
$timeout(function(){$('#SingleTextXeditalble'+attrs.attributeid).focus().select()},10);}},10);});angular.element(element).on('save',function(e,params){scope.fields['SingleLineTextValue_'+attrs.attributeid]=scope['SingleTextValue_'+attrs.attributeid].toString().trim()==""?"-":scope['SingleTextValue_'+attrs.attributeid];scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope['SingleTextValue_'+attrs.attributeid]);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabledropdownmypage',['$timeout','$compile','$resource','$window','MetadataService',function($timeout,$compile,$resource,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;MetadataService.GetOptionDetailListByIDForMyPage(attrs.attributeid,scope.UserId).then(function(optionlist){$window.XeditableDirectivesSource["NormalDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalDropDown_"+attrs.attributeid]=optionlist.Response;});for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="NormalDropDown_'+attrs.attributeid+'" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_'+attrs.attributeid+'\" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return e.Caption==scope.fields['NormalDropDown_'+attrs.attributeid]}))[0];if(tempvar!=undefined)
scope['NormalDropDown_'+attrs.attributeid]=tempvar.Id;else
scope['NormalDropDown_'+attrs.attributeid]="";}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];var dropdowntext='NormalDropDown_'+attrs.attributeid;if(scope['NormalDropDown_'+attrs.attributeid]!=undefined){var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return parseInt(e.Id)==parseInt(scope['NormalDropDown_'+attrs.attributeid])}))[0];scope.fields[dropdowntext]=tempvar.Caption;scope.treeSelection.push(tempvar.Id);scope.$apply();}
else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablemultiselectdropdownmypage',['$timeout','$compile','$resource','$window','MetadataService',function($timeout,$compile,$resource,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;MetadataService.GetOptionDetailListByIDForMyPage(attrs.attributeid,scope.UserId).then(function(optionlist){$window.XeditableDirectivesSource["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;});for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 id="NormalMultiDropDown1_'+attrs.attributeid+'" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_'+attrs.attributeid+'" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalMultiDropDown_'+attrs.attributeid+'\" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);var captions=scope.fields['NormalMultiDropDown_'+attrs.attributeid].split(',')
scope['NormalMultiDropDown_'+attrs.attributeid]=[];var tempvar=[];for(var x=0;x<=captions.length;x++){if(captions[x]!=undefined){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid],function(e){return e.Caption.trim()==captions[x].trim()}))[0]);scope['NormalMultiDropDown_'+attrs.attributeid].push(tempvar[x].Id);}}
if(scope.listAttributeValidationResult!=undefined){var isValidationReq=$.grep(scope.listAttributeValidationResult,function(e){return(e.AttributeID==parseInt(attrs.attributeid));});$("#overviewDetialblock").removeClass('warning');if(isValidationReq.length>0&&isValidationReq[0].RelationShipID!=0){var metrics=[['#NormalMultiDropDown1_'+attrs.attributeid+'',isValidationReq[0].ValueType,isValidationReq[0].ErrorMessage]];var options={'groupClass':'warning',};$("#overviewDetialblock").nod(metrics,options);}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];var dropdowntext='NormalMultiDropDown_'+attrs.attributeid;var tempcap=[];if(scope['NormalMultiDropDown_'+attrs.attributeid]!=undefined&&scope['NormalMultiDropDown_'+attrs.attributeid].length>0){var tempvar=[];var tempval=scope['NormalMultiDropDown_'+attrs.attributeid];for(var x=0;x<tempval.length;x++){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid],function(e){return parseInt(e.Id)==parseInt(tempval[x]);}))[0]);}
for(var j=0;j<tempvar.length;j++){tempcap.push(tempvar[j].Caption);scope.treeSelection.push(tempvar[j].Id);scope.$apply();}
scope.fields['NormalMultiDropDown_'+attrs.attributeid]=tempcap.join(',');}
else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablemultiselecttreedropdownmypage',['$timeout','$compile','$resource','$window','MetadataService',function($timeout,$compile,$resource,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var levelCount=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid].length
var isDataTypeExists=false;if(attrs.choosefromparent=="false"){MetadataService.GetTreeNode().then(function(GetTree){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
else{MetadataService.GetTreeNodeByEntityID(attrs.attributeid,attrs.entityid).then(function(){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
function DesignXeditable(){for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
for(var i=1;i<=levelCount;i++){scope['ddtoption_'+attrs.attributeid+'_'+i]={};if(i==1){scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['multiselectdropdown_'+attrs.attributeid].Children;}
else{if(i==levelCount)
scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=true;else
scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=[];}
scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<table>';for(var i=1;i<=levelCount;i++){var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;if(i==1){var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"multiselectdropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 12)\" />';inlinePopupDesign1+='</label></td></tr>';}
else{var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td>  <label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"multiselectdropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 12)\" />';inlinePopupDesign1+='</label></td></tr>';}}
inlinePopupDesign1+='</table>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);for(var i=1;i<=levelCount;i++){var dropdown_text='multiselectdropdown_text_'+attrs.attributeid+'_'+i;if(i==1){scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=($.grep(scope.treeSources["multiselectdropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}
else{if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)]!=undefined){if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined){scope.BindChildDropdownSource((attrs.attributeid),levelCount,(i-1),12);if(i==levelCount){var tempcaption=scope.treeTexts[dropdown_text].toString().trim().split(',');scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=[];var tempval=[];for(var k=0;k<tempcaption.length;k++){if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined)
scope['multiselectdropdown_'+attrs.attributeid+'_'+i].push(($.grep(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==tempcaption[k].trim();}))[0]);}}
else{scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=($.grep(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}}}}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];for(var i=1;i<=levelCount;i++){var dropdown_text='MultiSelectDropDown_'+attrs.attributeid+'_'+i;if(i==levelCount){if((scope['multiselectdropdown_'+attrs.attributeid+'_'+i]!=undefined)&&(scope['multiselectdropdown_'+attrs.attributeid+'_'+i]!="")){var tempval=[];for(var k=0;k<(scope['multiselectdropdown_'+attrs.attributeid+'_'+i].length);k++){scope.fields[dropdown_text]=scope['multiselectdropdown_'+attrs.attributeid+'_'+i][k];if(scope['multiselectdropdown_'+attrs.attributeid+'_'+i][k].id==undefined||scope['multiselectdropdown_'+attrs.attributeid+'_'+i][k].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+i][k].id);}
tempval.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+i][k].Caption);}
if(tempval.length>0)
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+i]=tempval.join(", ");else
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+i]='-';}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+i]='-';}}
else{if((scope['multiselectdropdown_'+attrs.attributeid+'_'+i]!=undefined)&&(scope['multiselectdropdown_'+attrs.attributeid+'_'+i]!="")){scope.fields[dropdown_text]=scope['multiselectdropdown_'+attrs.attributeid+'_'+i];if(scope['multiselectdropdown_'+attrs.attributeid+'_'+i].id==undefined||scope['multiselectdropdown_'+attrs.attributeid+'_'+i].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+i].id);}
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+i]=scope['multiselectdropdown_'+attrs.attributeid+'_'+i].Caption;}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+i]='-';}}
scope.$apply();}
scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabletreedropdown1mypage',['$timeout','$compile','$resource','$window',function($timeout,$compile,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var periodstartdateid=attrs.periodstartdateId;var attrID=attrs.attributeid;var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<div class="row-fluid"><div class="inputHolder">';inlinePopupDesign1+='<div class="ngcompile"><input placeholder="-- Start date --" id="'+periodstartdateid+'" data-ng-model="fields.PeriodStartDate_Dir_'+attrID+'" type="text"  data-date-format="'+Defaultdate+'" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open"'+attrID+']  ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"'+attrID+'"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='</div></div>';$scope.fields["DatePart_Calander_Open"+attrID]=false;Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){});angular.element(element).on('save',function(e,params){scope.savePeriodVal(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,attrs.primaryid);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditabletreedropdown1234',['$timeout','$compile','$resource','$window',function($timeout,$compile,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename+attrs.primaryid;var CurrentEntityID=parseInt(attrs.entityid,10)
var periodstartdateid=attrs.periodstartdateId;var periodenddateid=attrs.periodenddateId;var attrID=attrs.primaryid;var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<div class="row-fluid"><div class="inputHolder">';inlinePopupDesign1+='<div class="ngcompile"><input placeholder="" id="'+periodstartdateid+'" data-ng-model="fields.DateTime_Dir_'+attrID+'" type="text"  data-date-format="'+Defaultdate+'" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open"'+attrID+']  ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"'+attrID+'"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  required="" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='</div></div>';$scope.fields["DatePart_Calander_Open"+attrID]=false;Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);if(scope.fields["DateTime_"+attrID]!=""&&scope.fields["DateTime_"+attrID]!="-")
scope.fields["DateTime_Dir_"+attrID]=scope.fields["DateTime_"+attrID];else
scope.fields["DateTime_Dir_"+attrID]=null;}},10);});angular.element(element).on('save',function(e,params){if(scope.fields["DateTime_Dir_"+attrID]==null&&scope.fields["DateTime_Dir_"+attrID]==undefined)
scope.fields["DateTime_Dir_"+attrID]="";scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,attrs.attributeid);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.service('uuid',[function(){this.generate=function(){var uuid="",i,random;for(i=0;i<32;i++){random=Math.random()*16|0;if(i==8||i==12||i==16||i==20){uuid+="-"}
uuid+=(i==12?4:(i==16?(random&3|8):random)).toString(16);}
return uuid;};}]);app.directive('onRepeatLast',[function(){return{restrict:'A',link:function(scope,element,attrs){var isLast=scope.$last||scope.$parent.$last;if(isLast){scope.$evalAsync(attrs.onRepeatLast);}}}}]);app.directive('gridster',['$timeout',function($timeout){return{restrict:'AC',scope:{widgets:'=',onremove:'&',onedit:'&'},templateUrl:'views/mui/dashboard/gridster.html',controller:'gridsterCtrl',link:function(scope,elm,attrs,ctrl){elm.css('opacity',0);scope.initGrid=function(){$timeout(function(){var $ul=ctrl.$el.find('#dashboardBlock');var newDimensions=ctrl.calculateNewDimensions();var MaxWidth=(newDimensions[0][0]+(newDimensions[1][0]*2))*ctrl.options.cols;$ul.width(MaxWidth);ctrl.options.widget_margins=newDimensions[1];ctrl.options.widget_base_dimensions=newDimensions[0];ctrl.gridster=$ul.gridster(ctrl.options).data('gridster');ctrl.hookWidgetResizer();scope.$broadcast('gridReady');ctrl.resizeWidgetDimensions(true);elm.css('opacity',1);});};scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('widget',['$timeout','$window','$resource','uuid','CommonService',function($timeout,$window,$resource,uuid,CommonService){return{restrict:'A',require:'^gridster',scope:{remove:'&',editwid:'&',widget:'='},link:function(scope,element,attrs,ctrl){var $el=$(element);if(scope.widget.id===undefined){scope.widget.id=uuid.generate();}
scope.$on('gridReady',function(event){var base_size=ctrl.gridster.options.widget_base_dimensions;var margins=ctrl.gridster.options.widget_margins;var headerHeight=$el.find('header').outerHeight();var $content=$el.find('.content');$el.resizable({grid:[base_size[0]+(margins[0]*2),base_size[1]+(margins[1]*2)],animate:false,containment:ctrl.$el,autoHide:false,start:function(event,ui){var base_size=ctrl.gridster.options.widget_base_dimensions;var margins=ctrl.gridster.options.widget_margins;element.resizable('option','grid',[base_size[0]+(margins[0]*2),base_size[1]+(margins[1]*2)]);},resize:function(event,ui){$content.outerHeight($el.innerHeight()-headerHeight);},stop:function(event,ui){$content.outerHeight($el.innerHeight()-headerHeight);setTimeout(function(){sizeToGrid($el);ctrl.updateModel();},300);}});$('.ui-resizable-handle, .no-drag, .disabled, [disabled]',$el).hover(function(){ctrl.gridster.disable();},function(){ctrl.gridster.enable();});});function sizeToGrid($el){var base_size=ctrl.gridster.options.widget_base_dimensions;var margins=ctrl.gridster.options.widget_margins;var w=(parseInt($el.width())-base_size[0]);var h=(parseInt($el.height())-base_size[1]);for(var grid_w=1;w>0;w-=((base_size[0]+(margins[0]*2)))){grid_w++;}
for(var grid_h=1;h>0;h-=((base_size[1]+(margins[1]*2)))){grid_h++;}
$el.css({width:'',height:'',top:'',left:'',position:''});ctrl.gridster.resize_widget($el,grid_w,grid_h);var newDimensions=ctrl.calculateNewDimensions();$('#dynamicWidgets li[data-widget-id="'+$el.attr("data-widget-id").replace(/(\r\n|\n|\r)/gm,"")+'"]').each(function(){$('.box-content',this).height((newDimensions[0][0]*grid_h)-32);$('.graph',this).height($('.box-content',this).height()-20);var chartid=$(this).find('.graph').attr("id");if(chartid!=undefined){$(document).trigger("OnChartLoad",chartid);}});var TempId=$window.TemplateID;var Wizaid=$el.attr("data-widget-Realid").replace(/(\r\n|\n|\r)/gm,"");var colval=$el.attr("data-col").replace(/(\r\n|\n|\r)/gm,"");var rowval=$el.attr("data-row").replace(/(\r\n|\n|\r)/gm,"");var sizex=grid_w;var sizey=grid_h;var widgetData1=[];var gridData1={col:parseInt(colval),row:parseInt(rowval),sizex:grid_w,sizey:grid_h};widgetData1.push({grid:gridData1,widgtid:Wizaid,templateid:$window.TemplateID});var editwidgetresize={};editwidgetresize.widgetdatalist=widgetData1;if($window.ISTemplate==true){editwidgetresize.IsAdminPage=true;}
else{editwidgetresize.IsAdminPage=false;}
CommonService.WidgetDragEditing(editwidgetresize).then(function(saveusercomment1){});}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('activityItem',['$compile','$http','$templateCache',function($compile,$http,$templateCache){var templateText,templateLoader,baseURL='views/mui/',typeTemplateMapping={NewsFeed:'dashboard/NewsFeedWidget.html',Milestone:'dashboard/MilestoneWidhet.html',Workspace:'dashboard/WorkspaceWidget.html',topxactivity:'dashboard/TopxActivityWidget.html',CostCenterFinancial:'dashboard/CostCenterWidget.html',ActivityFinancial:'dashboard/FinancialSummaryWidget.html',Notification:'dashboard/NotificationWidget.html',Browser:'dashboard/BrowserWidget.html',BrowserVersion:'dashboard/BrowserVersionWidget.html',OS:'dashboard/OSWidget.html',CountryName:'dashboard/CountryNameWidget.html',MyTask:'mytask.html',FundingRequest:'myFundingRequest.html',PublishedAsset:'dashboard/PublishedAssets.html',DownloadedAsset:'dashboard/DownloadedAssets.html',UploadAsset:'dashboard/UploadAssets.html'};return{restrict:'E',replace:true,transclude:true,compile:function(tElement,tAttrs,transclusionFunc){var tplURL=baseURL+typeTemplateMapping[tAttrs.type];templateLoader=$http.get(tplURL,{cache:$templateCache}).success(function(html){tElement.html(html);});return function(scope,element,attrs){templateLoader.then(function(templateText){element.html($compile(tElement.html())(scope));});scope.type=attrs.type;scope.metrics=attrs.matrixid;scope.DimensionID=attrs.dimensionid;scope.visualType=attrs.visualtype;scope.WizardID=attrs.wizardid;scope.WizardTypeID=attrs.wizardtypeid;scope.WizardRealID=attrs.wizardrealid;scope.WizardNoOfItem=attrs.noofitem;scope.WizardNoOfYear=attrs.noofyear;scope.WizardNoOfMonth=attrs.noofmonth;};},};}])
app.directive('refreshui',function($location,$route,$window,$templateCache){return function(scope,element,attrs){element.bind('click',function(){if(element[0]&&element[0].href&&element[0].href!==$location.absUrl()){$window.location.reload();}
if(element[0]&&element[0].href&&element[0].href===$location.absUrl()){$route.reload();}});}});})(angular,app);
///#source 1 1 /app/directives/planningtool-directives.js
(function(ng,app){app.directive('dirownernameautopopulate',['$resource','UserService',function($resource,UserService){return function(scope,element,attrs){element.autocomplete({source:function(request,response){UserService.AutoCompleteMemberList(request.term,6).then(function(optionlist){if(optionlist.Response.length==0){bootbox.alert('Invalid Owner');element.val(scope.OwnerList[0].UserName);}
else
response(JSON.parse(optionlist.Response));});},minLength:3,focus:function(event,ui){element.val(ui.item.FirstName+' '+ui.item.LastName);return false;},select:function(event,ui){scope.OwnerList=[];scope.OwnerList.push({"Roleid":1,"RoleName":"Owner","Userid":parseInt(ui.item.Id,10),"UserName":ui.item.FirstName+' '+ui.item.LastName,"UserEmail":ui.item.Email,"IsInherited":'0',"InheritedFromEntityid":'0'});scope.$apply;return false;},open:function(event,ui){if($('.ui-autocomplete').find('li').length>3){$('.ui-autocomplete').css('padding-right','20px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
else{$('.ui-autocomplete').css('padding-right','2px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
$(".ui-helper-hidden-accessible").hide();$(this).autocomplete('widget').css('z-index',10000);return false;},change:function(event,ui){if(element.val().length<3){bootbox.alert('Invalid Owner');element.val(scope.OwnerList[0].UserName);}
if(ui.item===null){scope.myModelId=null;}}}).data("ui-autocomplete")._renderItem=function(ul,item){var html='';html=html+'<a class="membersClues" href="javascript:;" tabindex="-1">';html=html+'<div class="chooseMember">';html=html+'<div class="row-fluid">';html=html+'<div class="span2 memberImage">';html=html+'<img src="Handlers/UserImage.ashx?id='+item.Id+'">';html=html+'</div>';html=html+'<div class="span10">';html=html+'<div class="row-fluid memberDetail">';html=html+'<div class="span2">';html=html+'<span>Name</span><br>';html=html+'<span>Email</span>';html=html+'</div>';html=html+'<div class="span10 memberValues">';html=html+'<span>'+item.FirstName+' '+item.LastName+'</span><br>';html=html+'<span>'+item.Email+'</span>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</a>';return $("<li></li>").data("item.autocomplete",item).append(html).appendTo(ul);};}}]);app.directive('autocomp',['$resource','$translate','UserService',function($resource,$translate,UserService){return function(scope,element,attrs){element.autocomplete({source:function(request,response){UserService.AutoCompleteMemberList(request.term,6).then(function(optionlist){response(JSON.parse(optionlist.Response));});},minLength:3,focus:function(event,ui){element.val(ui.item.FirstName+' '+ui.item.LastName);return false;},select:function(event,ui){scope.AutoCompleteSelectedObj=[];scope.AutoCompleteSelectedObj.push(ui.item);scope.$apply;return false;},open:function(event,ui){if($('.ui-autocomplete').find('li').length>3){$('.ui-autocomplete').css('padding-right','20px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
else{$('.ui-autocomplete').css('padding-right','2px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
$(".ui-helper-hidden-accessible").hide();$(this).autocomplete('widget').css('z-index',100000);return false;},change:function(event,ui){if(ui.item===null){scope.myModelId=null;}}}).data("ui-autocomplete")._renderItem=function(ul,item){var html='';html=html+'<a class="membersClues" href="javascript:;" tabindex="-1">';html=html+'<div class="chooseMember">';html=html+'<div class="row-fluid">';html=html+'<div class="span2 memberImage">';html=html+'<img src="Handlers/UserImage.ashx?id='+item.Id+'">';html=html+'</div>';html=html+'<div class="span10">';html=html+'<div class="row-fluid memberDetail">';html=html+'<div class="span2">';html=html+'<span>Name</span><br>';html=html+'<span>Email</span>';html=html+'</div>';html=html+'<div class="span10 memberValues">';html=html+'<span>'+item.FirstName+' '+item.LastName+'</span><br>';html=html+'<span>'+item.Email+'</span>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</a>';return $("<li></li>").data("item.autocomplete",item).append(html).appendTo(ul);};}}]);app.directive('userautocomplete',['$resource','$translate','UserService',function($resource,$translate,UserService){return function(scope,element,attrs){element.autocomplete({source:function(request,response){UserService.AutoCompleteMemberList(request.term,6).then(function(optionlist){response(JSON.parse(optionlist.Response));});},minLength:3,focus:function(event,ui){element.val(ui.item.FirstName+' '+ui.item.LastName);return false;},select:function(event,ui){scope.AutoCompleteSelectedObj=[];scope.AutoCompleteSelectedObj.push(ui.item);scope.AutoCompleteSelectedUserObj.UserSelection=ui.item;return false;},open:function(event,ui){if($('.ui-autocomplete').find('li').length>3){$('.ui-autocomplete').css('padding-right','20px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
else{$('.ui-autocomplete').css('padding-right','2px').css('width','340px').css('max-height','225px').css('overflow-y','auto').addClass('dropdown-menu');}
$(".ui-helper-hidden-accessible").hide();$(this).autocomplete('widget').css('z-index',100000);return false;},change:function(event,ui){if(ui.item===null){scope.myModelId=null;}}}).data("ui-autocomplete")._renderItem=function(ul,item){var html='';html=html+'<a class="membersClues" href="javascript:;" tabindex="-1">';html=html+'<div class="chooseMember">';html=html+'<div class="row-fluid">';html=html+'<div class="span2 memberImage">';html=html+'<img src="Handlers/UserImage.ashx?id='+item.Id+'">';html=html+'</div>';html=html+'<div class="span10">';html=html+'<div class="row-fluid memberDetail">';html=html+'<div class="span2">';html=html+'<span>Name</span><br>';html=html+'<span>Email</span>';html=html+'</div>';html=html+'<div class="span10 memberValues">';html=html+'<span>'+item.FirstName+' '+item.LastName+'</span><br>';html=html+'<span>'+item.Email+'</span>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</div>';html=html+'</a>';return $("<li></li>").data("item.autocomplete",item).append(html).appendTo(ul);};}}]);app.directive('xeditablefinancialamount',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeID=attrs.attributeid;var Address=function(options){this.init(attributeID,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble_'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'"></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeID]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$('[id=SingleTextXeditalble_'+attrs.attributeid+']').autoNumeric('init',scope.DecimalSettings['FinancialAutoNumeric']);$('[id=SingleTextXeditalble_'+attrs.attributeid+']').css('text-align','right');$timeout(function(){$('[id=SingleTextXeditalble_'+attrs.attributeid+']:enabled:visible:first').focus().select()},100);$compile($('.ngcompile'))(scope);scope['SingleTextValue_'+attrs.attributeid]=scope.fields['TotalAssignedAmount'];}},10);});angular.element(element).on('save',function(e,params){scope.fields['TotalAssignedAmount']=scope['SingleTextValue_'+attrs.attributeid].toString().trim()==""?"0":scope['SingleTextValue_'+attrs.attributeid];scope.savefinanicalamount(scope.fields['TotalAssignedAmount']);});}};}]);app.service('sharedProperties',[function(){var stringValue='test string value';var objectValue={data:'test object value'};var itemID=0;return{getString:function(){return stringValue;},getID:function(){return itemID;},setString:function(value,id){stringValue=value;itemID=id;},getObject:function(){return objectValue;}}}]);app.directive('xeditablefinancialtext',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$translate,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.finid,10);var attributeID=attrs.attributeid;var finid=attrs.finid;var editablefinid=attrs.editablefinid;var Address=function(options){this.init(editablefinid,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';if(parseInt(attrs.attributetypeid)==1)
inlinePopup+='<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble'+attrs.finid+'_'+attributeID+'" data-ng-model="SingleTextValue_'+attrs.finid+'_'+attributeID+'"></label></div>';else if(parseInt(attrs.attributetypeid)==2)
inlinePopup+='<div><label><textarea class="ngcompile" id="SingleTextXeditalble'+attrs.finid+'_'+attributeID+'" data-ng-model="SingleTextValue_'+attrs.finid+'_'+attributeID+'" ></textarea></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[editablefinid]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope['SingleTextValue_'+attrs.finid+'_'+attributeID]=scope.fields['SingleLineTextValue_'+attrs.finid+'_'+attributeID]=="-"?"":scope.fields['SingleLineTextValue_'+attrs.finid+'_'+attributeID];$timeout(function(){$('#SingleTextXeditalble'+attrs.finid+'_'+attributeID).focus().select()},10);}},10);});angular.element(element).on('save',function(e,params){scope.saveFinancialMetadataValues(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope['SingleTextValue_'+attrs.finid+'_'+attributeID]);});}};}]);app.directive('xeditablefinancialdropdown',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$translate,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var attributeID=attrs.attributeid;var finid=attrs.finid;var dttype=attrs.editablefinid;var Currentfinid=parseInt(attrs.finid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(dttype==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(dttype,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="NormalDropDown_'+attrs.finid+'_'+attributeID+'" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_'+attrs.finid+'_'+attributeID+'\" value=\"{{ndata.ID}}\">{{ndata.Caption}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[dttype]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.finid+'_'+attributeID],function(e){return e.Caption==scope.fields['NormalDropDown_'+attrs.finid+'_'+attributeID]}))[0];if(tempvar!=undefined)
scope['NormalDropDown_'+attrs.finid+'_'+attributeID]=tempvar.ID;else
scope['NormalDropDown_'+attrs.finid+'_'+attributeID]='';}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];var dropdowntext='NormalDropDown_'+attrs.finid+'_'+attributeID;if(scope['NormalDropDown_'+attrs.finid+'_'+attributeID]!=undefined){var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.finid+'_'+attributeID],function(e){return parseInt(e.ID)==parseInt(scope['NormalDropDown_'+attrs.finid+'_'+attributeID])}))[0];scope.fields[dropdowntext]=tempvar.Caption;scope.treeSelection.push(tempvar.ID);scope.$apply();}
else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveFinancialMetadataValues(attrs.attributeid,attrs.attributetypeid,Currentfinid,scope.treeSelection);});}};}]);app.directive('xeditablefinancialmultiselectdropdown',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var finid=attrs.finid;var attributeID=attrs.attributeid;var dttype=attrs.editablefinid;var Currentfinid=parseInt(attrs.finid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(dttype==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(dttype,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label> \
                                                <select multiselect-dropdown ng-options="ndata.ID as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_'+attrs.finid+'_'+attributeID+'\" id="NormalMultiDropDown1_'+attrs.finid+'_'+attributeID+'" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_'+attrs.finid+'_'+attributeID+'" name="city" class="ngcompileDropdown multiselect"> \
                                                </select> \
                                            </label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[dttype]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);var captions=scope.fields['NormalMultiDropDown_'+attrs.finid+'_'+attributeID].split(',')
scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID]=[];var tempvar=[];for(var x=0;x<=captions.length-1;x++){if(captions[x]!=undefined&&captions[x]!='-'){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.finid+'_'+attributeID],function(e){return e.Caption.trim()==captions[x].trim()}))[0]);scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID].push(tempvar[x].ID);}
else{scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID]=[];}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];var dropdowntext='NormalMultiDropDown_'+attrs.finid+'_'+attributeID;var tempcap=[];if(scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID]!=undefined&&scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID].length>0){var tempvar=[];var tempval=scope['NormalMultiDropDown_'+attrs.finid+'_'+attributeID];for(var x=0;x<tempval.length;x++){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.finid+'_'+attributeID],function(e){return parseInt(e.ID)==parseInt(tempval[x]);}))[0]);}
for(var j=0;j<tempvar.length;j++){tempcap.push(tempvar[j].Caption);scope.treeSelection.push(tempvar[j].ID);scope.$apply();}
scope.fields['NormalMultiDropDown_'+attrs.finid+'_'+attributeID]=tempcap.join(',');}
else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveFinancialMetadataValues(attrs.attributeid,attrs.attributetypeid,Currentfinid,scope.treeSelection);});}};}]);app.directive('xeditablepredefineobjecitve',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.html(value);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){scope.savepredefineobjectiveData(attrs.plannedtarget,attrs.targetoutcome,attrs.objectiveid,attrs.targettype,params.newValue);});}};}]);app.directive('xeditableadditionalobjecitve',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var objid=attrs.objid;var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.html(value);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){scope.saveadditionalobjecitveData(attrs.plannedtarget,attrs.targetoutcome,attrs.objid,attrs.targettype,params.newValue);$('#AddtionalObjectivename').val='';});}};}]);app.directive('xeditabletext',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeID=attrs.attributeid;var Address=function(options){this.init(attributeID,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';if(parseInt(attrs.attributetypeid)==1||parseInt(attrs.attributetypeid)==8)
inlinePopup+='<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble_'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'"></label></div>';else if(parseInt(attrs.attributetypeid)==2)
inlinePopup+='<div><label><textarea class="small-textarea ngcompile" id="SingleTextXeditalble_'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ></textarea></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeID]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);if(parseInt(attrs.attributetypeid)==8){$('#SingleTextXeditalble_'+attrs.attributeid).autoNumeric('init',{aSep:' ',vMin:"0"});}
scope['SingleTextValue_'+attrs.attributeid]=scope.fields['SingleLineTextValue_'+attrs.attributeid]=="-"?"":scope.fields['SingleLineTextValue_'+attrs.attributeid];$timeout(function(){$('#SingleTextXeditalble_'+attrs.attributeid).focus().select()},10);}},10);});angular.element(element).on('save',function(e,params){if(scope['SingleTextValue_'+attrs.attributeid]=="")
scope['SingleTextValue_'+attrs.attributeid]='-'
if(scope['SingleTextValue_'+attrs.attributeid]!=undefined&&scope['SingleTextValue_'+attrs.attributeid].length<=0){var isValidationReq=$.grep(scope.listAttributeValidationResult,function(e){return(e.AttributeID==parseInt(attrs.attributeid));});$("#overviewDetialblock").removeClass('warning');if(isValidationReq.length>0&&isValidationReq[0].RelationShipID!=0){var metrics=[['#SingleTextXeditalble_'+attrs.attributeid+'',isValidationReq[0].ValueType,isValidationReq[0].ErrorMessage]];var options={'groupClass':'warning',};$("#overviewDetialblock").nod(metrics,options);bootbox.alert(isValidationReq[0].ErrorMessage);}}
else{if(parseInt(attrs.attributetypeid)==8){scope['SingleTextValue_'+attrs.attributeid]=scope['SingleTextValue_'+attrs.attributeid].replace(/ /g,'');}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope['SingleTextValue_'+attrs.attributeid]);}});}};}]);app.directive('xeditabletextforcurrencyamount',['$timeout','$compile','$resource','$translate','$window','PlanningService',function($timeout,$compile,$resource,$translate,$window,PlanningService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeID=attrs.attributeid;var Address=function(options){this.init(attributeID,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);PlanningService.GetCurrencyListFFsettings().then(function(CurrencyListResult){if(CurrencyListResult.Response!=null){$window.XeditableDirectivesSource["NormalDropDown_"+attrs.attributeid]=CurrencyListResult.Response;scope.normaltreeSources["NormalDropDown_"+attrs.attributeid]=CurrencyListResult.Response;}});var inlinePopup='';if(scope.isfromtask!=1)
inlinePopup+='<div><input class="margin-right5x ngcompile" type="text" id="SingleTextXeditalble_'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ng-change= Getamountentered('+attrs.attributeid+')><select ui-select2 ng-model="NormalDropDown_'+attrs.attributeid+'" class="currencySelector ngcompileDropdown"  id="NormalDropDown_'+attrs.attributeid+'" ng-change= GetCostCentreCurrencyRateById('+attrs.attributeid+')><option ng-repeat="ndata in normaltreeSources.NormalDropDown_'+attrs.attributeid+'" value="{{ndata.Id}}">{{ndata.ShortName}}</option></select></div>';else if(scope.isfromtask==1)
inlinePopup+='<div><input class="margin-right5x ngcompile" type="text" id="SingleTextXeditalble_'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ng-change= Getamountenteredforedit('+attrs.attributeid+')><select ui-select2 ng-model="NormalDropDown_'+attrs.attributeid+'" class="currencySelector ngcompileDropdown"  id="NormalDropDown_'+attrs.attributeid+'" ng-change= GetCostCentreCurrencyRateByIdforedit('+attrs.attributeid+')><option ng-repeat="ndata in normaltreeSources.NormalDropDown_'+attrs.attributeid+'" value="{{ndata.Id}}">{{ndata.ShortName}}</option></select></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeID]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){mar=(scope.DecimalSettings.FinancialAutoNumeric.vMax).substr((scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".")+1);scope.mindec="";if(mar.length==1){$('#SingleTextXeditalble_'+attrs.attributeid).autoNumeric('init',{aSep:' ',vMin:"0",mDec:"1"});}
if(mar.length!=1){if(mar.length<5){scope.mindec="0.";for(i=0;i<mar.length;i++){scope.mindec=scope.mindec+"0";}
$('#SingleTextXeditalble_'+attrs.attributeid).autoNumeric('init',{aSep:' ',vMin:scope.mindec});}
else{$('#SingleTextXeditalble_'+attrs.attributeid).autoNumeric('init',{aSep:' ',vMin:"0",mDec:"0"});}}
$compile($('.ngcompile'))(scope);$compile($('.ngcompileDropdown'))(scope);scope['SingleTextValue_'+attrs.attributeid]=scope.fields['SingleLineTextValue_'+attrs.attributeid]=="-"?"":scope.fields['SingleLineTextValue_'+attrs.attributeid];$timeout(function(){$('#SingleTextXeditalble_'+attrs.attributeid).focus().select()},10);scope['ischanged_'+attrs.attributeid]=false;var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return e.Id==scope.fields['NormalDropDown_'+attrs.attributeid]}))[0];scope['NormalDropDown_'+attrs.attributeid]=tempvar.Id;}},10);});angular.element(element).on('save',function(e,params){var curr=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return e.Id==scope['NormalDropDown_'+attrs.attributeid];}));scope.amountcurrencytypear=[];if(scope['SingleTextValue_'+attrs.attributeid]==""){scope['SingleTextValue_'+attrs.attributeid]=0;}
scope.amountcurrencytypear.push((scope['SingleTextValue_'+attrs.attributeid].toString()).replace(/ /g,''));scope.amountcurrencytypear.push(scope['NormalDropDown_'+attrs.attributeid]);scope.amountcurrencytypear.push(curr[0]["ShortName"]);scope.amountcurrencytypear.push(attrs.attributeid);if(scope['SingleTextValue_'+attrs.attributeid]!=undefined&&scope['SingleTextValue_'+attrs.attributeid].length<=0){var isValidationReq=$.grep(scope.listAttributeValidationResult,function(e){return(e.AttributeID==parseInt(attrs.attributeid));});$("#overviewDetialblock").removeClass('warning');if(isValidationReq.length>0&&isValidationReq[0].RelationShipID!=0){var metrics=[['#SingleTextXeditalble_'+attrs.attributeid+'',isValidationReq[0].ValueType,isValidationReq[0].ErrorMessage]];var options={'groupClass':'warning',};$("#overviewDetialblock").nod(metrics,options);bootbox.alert(isValidationReq[0].ErrorMessage);}}
else{if(scope.isfrommypage!=1){scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.amountcurrencytypear);}
else
scope.saveDropdownTreeFromMyPage(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.amountcurrencytypear);}});}};}]);app.directive('xeditabletextforname',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeID=attrs.attributeid;var Address=function(options){this.init(attributeID,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';if(parseInt(attrs.attributetypeid)==1)
inlinePopup+='<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ></label></div>';else if(parseInt(attrs.attributetypeid)==2)
inlinePopup+='<div><label><textarea class="ngcompile" id="SingleTextXeditalble'+attrs.attributeid+'" data-ng-model="SingleTextValue_'+attrs.attributeid+'" ></textarea></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeID]=Address;var loadXeditable=function(){angular.element(element).editable({title:attrs.originalTitle,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope['SingleTextValue_'+attrs.attributeid]=scope.RootLevelEntityNameTemp;}},10);});angular.element(element).on('save',function(e,params){scope.ActivityName=params.newValue.toString().trim()==""?"-":params.newValue.text();scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,params.newValue.toString());});}};}]);app.directive('xeditable',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeName=attrs.attributename;var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.text(value);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,params.newValue.toString());});}};}]);app.directive('xeditabletaskcomment',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeName=attrs.attributename;var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.html(value);scope.DynamicTaskListDescriptionObj=value;scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){scope.DynamicTaskListDescriptionObj=params.newValue;scope.UpdateEntityOverAllStatus();});}};}]);app.directive('xeditablemilestone',['$timeout','$compile','$resource','$translate',function($timeout,$compile,$resource,$translate){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){if(scope.IsLock==true){return false;}
var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="xeditmilestonestatus" name="city" class="ngcompileDropdown">';inlinePopup+='<option ng-repeat="opt in MilstoneStatus" value="{{opt.StatusDescription}}">{{opt.StatusDescription}}</option>';inlinePopup+='</label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['xeditmilestonestatus']=scope.milestone.Status;}},10);});angular.element(element).on('save',function(e,params){var milestoneSatusId=0;if(scope['xeditmilestonestatus']!=undefined){scope.milestone.Status=scope['xeditmilestonestatus'];if(scope.milestone.Status=="Not Reached")
milestoneSatusId=0;else
milestoneSatusId=1;}
scope.saveMilestStatus(attrs.milestoneid,attrs.milestonename,attrs.milestoneduedate,attrs.milestoneshortdescription,milestoneSatusId);});}};}]);app.directive('xeditabletreedropdown',['$timeout','$compile','$resource','$translate','$window','MetadataService',function($timeout,$compile,$resource,$translate,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var levelCount=scope.treelevels['dropdown_levels_'+attrs.attributeid].length
var isDataTypeExists=false;if(attrs.choosefromparent=="false"){MetadataService.GetTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
else{MetadataService.GetTreeNodeByEntityID(attrs.attributeid,attrs.entityid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
function DesignXeditable(){MetadataService.GetTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
for(var i=1;i<=levelCount;i++){if(i==1){scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['dropdown_'+attrs.attributeid].Children;scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}
else{scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=[];scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='<table>';for(var i=1;i<=levelCount;i++){if(i==1){var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}
else{var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td>  <label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}}
inlinePopupDesign1+='</table>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('hide',function(e,editable){for(var i=1;i<=levelCount;i++){scope['IsChanged_'+attrs.attributeid+'_'+i]=true;scope['dropdown_'+attrs.attributeid+'_'+i]="";}
scope.$apply();});angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);for(var i=1;i<=levelCount;i++){scope['IsChanged_'+attrs.attributeid+'_'+i]=false;var dropdown_text='dropdown_text_'+attrs.attributeid+'_'+i;if(i==1){scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope.treeSources["dropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}
else{if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)]!=undefined){if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined){scope.BindChildDropdownSource((attrs.attributeid),levelCount,(i-1),6);scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}}}}}},30);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];for(var i=1;i<=levelCount;i++){scope['IsChanged_'+attrs.attributeid+'_'+i]=true;var dropdown_text='DropDown_'+attrs.attributeid+'_'+i;if((scope['dropdown_'+attrs.attributeid+'_'+i]!=undefined)&&(scope['dropdown_'+attrs.attributeid+'_'+i]!="")){scope.fields[dropdown_text]=scope['dropdown_'+attrs.attributeid+'_'+i];if(scope['dropdown_'+attrs.attributeid+'_'+i].id==undefined||scope['dropdown_'+attrs.attributeid+'_'+i].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['dropdown_'+attrs.attributeid+'_'+i].id);}
scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]=scope['dropdown_'+attrs.attributeid+'_'+i].Caption;}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]='-';}
scope.$apply();}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});});}}};}]);app.directive('xeditableoptimizedtreedropdown',['$timeout','$compile','$resource','$translate','$window','MetadataService',function($timeout,$compile,$resource,$translate,$window,MetadataService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var levelCount=scope.treelevels['dropdown_levels_'+attrs.attributeid].length
var isDataTypeExists=false;DesignXeditable();function DesignXeditable(){MetadataService.GetTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["dropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
for(var i=1;i<=levelCount;i++){if(i==1){scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['dropdown_'+attrs.attributeid].Children;scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}
else{scope['ddtoption_'+attrs.attributeid+'_'+i]={};scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=[];scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='<table>';for(var i=1;i<=levelCount;i++){if(i==1){var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}
else{var levelcaption=scope.treelevels['dropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td>  <label><span>'+levelcaption+': </span></label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\"  data-ng-model=\"dropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 6)\" />';inlinePopupDesign1+='</label></td></tr>';}}
inlinePopupDesign1+='</table>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);for(var i=1;i<=levelCount;i++){var dropdown_text='dropdown_text_'+attrs.attributeid+'_'+i;if(i==1){scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope.treeSources["dropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}
else{if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)]!=undefined){if(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined){scope.BindChildDropdownSource((attrs.attributeid),levelCount,(i-1),6);scope['dropdown_'+attrs.attributeid+'_'+i]=($.grep(scope['dropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}}}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];for(var i=1;i<=levelCount;i++){var dropdown_text='DropDown_'+attrs.attributeid+'_'+i;if((scope['dropdown_'+attrs.attributeid+'_'+i]!=undefined)&&(scope['dropdown_'+attrs.attributeid+'_'+i]!="")){scope.fields[dropdown_text]=scope['dropdown_'+attrs.attributeid+'_'+i];if(scope['dropdown_'+attrs.attributeid+'_'+i].id==undefined||scope['dropdown_'+attrs.attributeid+'_'+i].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['dropdown_'+attrs.attributeid+'_'+i].id);}
scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]=scope['dropdown_'+attrs.attributeid+'_'+i].Caption;}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['dropdown_text_'+attrs.attributeid+'_'+i]='-';}
scope.$apply();}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});});}}};}]);app.directive('xeditabledropdown',['$timeout','$compile','$resource','$translate','$window','MetadataService','DamService',function($timeout,$compile,$resource,$translate,$window,MetadataService,DamService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;var GetOptionListByID='';if(attrs.isdam==null){MetadataService.GetOptionDetailListByID(attrs.attributeid,CurrentEntityID).then(function(optionlist){$window.XeditableDirectivesSource["NormalDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalDropDown_"+attrs.attributeid]=optionlist.Response;});}
else{if(attrs.isdam=true){DamService.GetOptionDetailListByAssetID(attrs.attributeid,CurrentEntityID).then(function(optionlist){$window.XeditableDirectivesSource["NormalDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalDropDown_"+attrs.attributeid]=optionlist.Response;});}}
for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="NormalDropDown_'+attrs.attributeid+'" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_'+attrs.attributeid+'\" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('hide',function(e,editable){scope['NormalDropDown_'+attrs.attributeid]="";scope.$apply();});angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['ischanged_'+attrs.attributeid]=false;var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return e.Caption==scope.fields['NormalDropDown_'+attrs.attributeid]}))[0];if(tempvar!=null)
scope['NormalDropDown_'+attrs.attributeid]=tempvar.Id;else
scope['NormalDropDown_'+attrs.attributeid]='';}},10);});angular.element(element).on('save',function(e,params){scope['ischanged_'+attrs.attributeid]=true;scope.treeSelection=[];var dropdowntext='NormalDropDown_'+attrs.attributeid;if(scope['NormalDropDown_'+attrs.attributeid]!=undefined){var tempvar=($.grep(scope.normaltreeSources["NormalDropDown_"+attrs.attributeid],function(e){return parseInt(e.Id)==parseInt(scope['NormalDropDown_'+attrs.attributeid])}))[0];scope.fields[dropdowntext]=tempvar.Caption;scope.treeSelection.push(tempvar.Id);scope.$apply();}
else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});}};}]);app.directive('xeditablemultiselectdropdown',['$timeout','$compile','$resource','$translate','$window','MetadataService','DamService',function($timeout,$compile,$resource,$translate,$window,MetadataService,DamService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid]=[];var GetOptionListByID=''
if(attrs.isdam==null){MetadataService.GetOptionDetailListByID(attrs.attributeid,CurrentEntityID).then(function(optionlist){$window.XeditableDirectivesSource["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;});}
else{if(attrs.isdam=true){DamService.GetOptionDetailListByAssetID(attrs.attributeid,CurrentEntityID).then(function(optionlist){$window.XeditableDirectivesSource["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid]=optionlist.Response;});}}
for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label> \
                                                <select multiselect-dropdown ng-options="ndata.Id as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_'+attrs.attributeid+'\" id="NormalMultiDropDown1_'+attrs.attributeid+'" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_'+attrs.attributeid+'" name="city" class="ngcompileDropdown multiselect"> \
                                                </select> \
                                            </label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('hide',function(e,editable){scope['IsChanged_'+attrs.attributeid]=true;scope['NormalMultiDropDown_'+attrs.attributeid]=[];scope.$apply();});angular.element(element).on('shown',function(e,editable){scope['IsChanged_'+attrs.attributeid]=false;$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);var captions=scope.fields['NormalMultiDropDown_'+attrs.attributeid].split(',')
scope['NormalMultiDropDown_'+attrs.attributeid]=[];var tempvar=[];for(var x=0;x<=captions.length;x++){if(captions[x]!=undefined){if($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid],function(e){return e.Caption.trim()==captions[x].trim()}).length>0){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid],function(e){return e.Caption.trim()==captions[x].trim()}))[0]);scope['NormalMultiDropDown_'+attrs.attributeid].push(tempvar[x].Id);}}}}},10);});angular.element(element).on('save',function(e,params){scope['IsChanged_'+attrs.attributeid]=true;scope.treeSelection=[];var dropdowntext='NormalMultiDropDown_'+attrs.attributeid;var tempcap=[];if(scope['NormalMultiDropDown_'+attrs.attributeid]!=undefined&&scope['NormalMultiDropDown_'+attrs.attributeid].length>0){var tempvar=[];var tempval=scope['NormalMultiDropDown_'+attrs.attributeid];for(var x=0;x<tempval.length;x++){tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_"+attrs.attributeid],function(e){return parseInt(e.Id)==parseInt(tempval[x]);}))[0]);}
for(var j=0;j<tempvar.length;j++){tempcap.push(tempvar[j].Caption);scope.treeSelection.push(tempvar[j].Id);scope.$apply();}
scope.fields['NormalMultiDropDown_'+attrs.attributeid]=tempcap.join(',');}
if(scope.listAttributeValidationResult!=undefined){var isValidationReq=$.grep(scope.listAttributeValidationResult,function(e){return(e.AttributeID==parseInt(attrs.attributeid));});$("#overviewDetialblock").removeClass('warning');if(isValidationReq.length>0&&isValidationReq[0].RelationShipID!=0&&tempvar==undefined){var metrics=[['#NormalMultiDropDown1_'+attrs.attributeid+'',isValidationReq[0].ValueType,isValidationReq[0].ErrorMessage]];var options={'groupClass':'warning',};$("#overviewDetialblock").nod(metrics,options);bootbox.alert(isValidationReq[0].ErrorMessage);}
else{scope.fields[dropdowntext]=scope.fields['NormalMultiDropDown_'+attrs.attributeid];scope.treeSelection.push(0);scope.$apply();scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);}}
else{if(scope['NormalMultiDropDown_'+attrs.attributeid]!=undefined&&scope['NormalMultiDropDown_'+attrs.attributeid].length>0){}else{scope.fields[dropdowntext]='-';scope.treeSelection.push(0);scope.$apply();}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);}});}};}]);app.directive('xeditabletreedropdown1',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename+attrs.primaryid;var CurrentEntityID=parseInt(attrs.entityid,10)
var periodstartdateid=attrs.periodstartdateId;var periodenddateid=attrs.periodenddateId;var attrID=attrs.primaryid;var isDataTypeExists=false;var isDam=attrs.isdam;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';if(isDam=="true"){inlinePopupDesign1+='<div class="row-fluid"><div class="inputHolder">';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- Start date --" id="'+periodstartdateid+'" data-ng-model="fields.PeriodStartDate_Dir_'+attrID+'" type="text" ng-click=\"PerioddirectiveCalanderopen($event,'+attrs.primaryid+',\'start\')\" datepicker-popup=\"{{format}}\" is-open=\"fields.PeriodStartDateopen_'+attrID+'\" min="fields.DatePartMinDate_'+attrID+'" max="fields.DatePartMaxDate_'+attrID+'" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" data-date-format="'+Defaultdate+'" name="startDate" required="" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- End date --" id="'+periodenddateid+'" data-ng-model="fields.PeriodEndDate_Dir_'+attrID+'" type="text"  ng-click=\"PerioddirectiveCalanderopen($event,'+attrs.primaryid+',\'end\')\"  datepicker-popup=\"{{format}}\" is-open=\"fields.PeriodEndDateopen_'+attrID+'\" min="fields.DatePartMinDate_'+attrID+'" max="fields.DatePartMaxDate_'+attrID+'" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="EndDate"  data-date-format="'+Defaultdate+'" class="ng-pristine ng-invalid ng-invalid-required margin-bottom10x"></div>';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- Comment Start/End Date --" type="text" data-ng-model="fields.PeriodDateDesc_Dir_'+attrID+' " name="comment" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='</div></div>';scope.fields["fields.PeriodStartDateopen_"+attrID]=false;scope.fields["fields.PeriodEndDateopen_"+attrID]=false;}
else{inlinePopupDesign1+='<div class="row-fluid"><div class="inputHolder">';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- Start date --" id="'+periodstartdateid+'" data-ng-model="fields.PeriodStartDate_Dir_'+attrID+'" type="text"  data-date-format="'+Defaultdate+'" ng-click=\"Calanderopen($event,'+attrs.primaryid+',\'start\')\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.PeriodStartDateopen_'+attrID+'\" min="fields.DatePartMinDate_'+attrID+'" max="fields.DatePartMaxDate_'+attrID+'" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- End date --" id="'+periodenddateid+'" data-ng-model="fields.PeriodEndDate_Dir_'+attrID+'" type="text" data-date-format="'+Defaultdate+'" ng-click=\"Calanderopen($event,'+attrs.primaryid+',\'end\')\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.PeriodEndDateopen_'+attrID+'\" min="fields.DatePartMinDate_'+attrID+'" max="fields.DatePartMaxDate_'+attrID+'" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="EndDate" class="ng-pristine ng-invalid ng-invalid-required margin-bottom10x"></div>';inlinePopupDesign1+='<div class="ngcompile"><input class="width2x" placeholder="-- Comment Start/End Date --" type="text" data-ng-model="fields.PeriodDateDesc_Dir_'+attrID+' " name="comment" class="ng-pristine ng-invalid ng-invalid-required"></div>';inlinePopupDesign1+='</div></div>';scope.fields["fields.PeriodStartDateopen_"+attrID]=false;scope.fields["fields.PeriodEndDateopen_"+attrID]=false;}
Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope.fields["PeriodStartDate_Dir_0"]="";scope.fields["PeriodEndDate_Dir_0"]="";scope.fields["PeriodDateDesc_Dir_0"]="";}},10);});angular.element(element).on('save',function(e,params){var a=$.grep(scope.tempholidays,function(e){return e==dateFormat(scope.fields['PeriodStartDate_Dir_'+attrs.primaryid],scope.format);});var b=$.grep(scope.tempholidays,function(e){return e==dateFormat(scope.fields['PeriodEndDate_Dir_'+attrs.primaryid],scope.format);});if(a!=null||b!=null){if(a.length>0){bootbox.alert("Selected startdate should not same as non businessday");scope.fields['PeriodStartDate_Dir_'+attrs.primaryid]="";}
else if(b.length>0){bootbox.alert("Selected enddate should not same as non businessday");scope.fields['PeriodEndDate_Dir_'+attrs.primaryid]="";}
else{scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.fields["DateTime_Dir_"+attrID]);}}});}};}]);app.directive('xeditabledropdown12',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 ng-options=\"ndata as ndata.StepName for ndata in workflowstepOptions" data-ng-model="xeditWorkflowStep" name="city" class="ngcompileDropdown" /></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['xeditWorkflowStep']=($.grep(scope.workflowstepOptions,function(e){return e.StepName==scope.CurrentWorkflowStepName}))[0];}},10);});angular.element(element).on('save',function(e,params){if(scope['xeditWorkflowStep']!=undefined){scope.CurrentWorkflowStepName=scope['xeditWorkflowStep'].StepName;scope.SelectedWorkflowStep=scope['xeditWorkflowStep'].StepID;scope.WorkFlowStepID=scope['xeditWorkflowStep'].StepID;scope.$apply();}
else{scope.CurrentWorkflowStepName='-';$scope.SelectedWorkflowStep=0;scope.WorkFlowStepID=0;scope.$apply();}
scope.UpdateEntityWorkFlowStep(scope.SelectedWorkflowStep);});}};}]);app.directive('xeditablemultiselecttreedropdown',['$timeout','$compile','$resource','$translate','$window','DamService',function($timeout,$compile,$resource,$translate,$window,DamService){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var levelCount=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid].length
var isDataTypeExists=false;if(attrs.isdam==null||attrs.isdam==undefined){if(attrs.choosefromparent=="false"){DamService.GetDamTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
else{DamService.GetDamTreeNodeByEntityID(attrs.attributeid,attrs.entityid).then(function(GetTree){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}}
else if(attrs.isdam=="true"){if(attrs.choosefromparent=="false"){DamService.GetDamTreeNode(attrs.attributeid).then(function(GetTree){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}
else{DamService.GetDamTreeNodeByEntityID(attrs.attributeid,attrs.entityid).then(function(GetTree){scope.treeSources["multiselectdropdown_"+attrs.attributeid]=JSON.parse(GetTree.Response);DesignXeditable();});}}
function DesignXeditable(){for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
for(var i=1;i<=levelCount;i++){scope['ddtoption_'+attrs.attributeid+'_'+i]={};if(levelCount==1){scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=true;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['multiselectdropdown_'+attrs.attributeid].Children;}
else{if(i==1){scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=scope.treeSources['multiselectdropdown_'+attrs.attributeid].Children;}
else{if(i==levelCount)
scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=true;else
scope['ddtoption_'+attrs.attributeid+'_'+i].multiple=false;scope['ddtoption_'+attrs.attributeid+'_'+i].data=[];}}
scope['ddtoption_'+attrs.attributeid+'_'+i].formatResult=function(item){return item.Caption};scope['ddtoption_'+attrs.attributeid+'_'+i].formatSelection=function(item){return item.Caption};}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<table>';for(var i=1;i<=levelCount;i++){var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;if(levelCount==1){var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label class="control-label">'+levelcaption+': </label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"multiselectdropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" />';inlinePopupDesign1+='</label></td></tr>';}
else{if(i==1){var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label class="control-label">'+levelcaption+': </label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"multiselectdropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 12)\" />';inlinePopupDesign1+='</label></td></tr>';}
else{var levelcaption=scope.treelevels['multiselectdropdown_levels_'+attrs.attributeid][i-1].caption;inlinePopupDesign1+='<tr><td><label class="control-label">'+levelcaption+': </label></td><td><label>';inlinePopupDesign1+='<input ui-select2=\"ddtoption_'+attrs.attributeid+'_'+i+'\" type="hidden"  data-ng-model=\"multiselectdropdown_'+attrs.attributeid+'_'+i+'\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource('+attrs.attributeid+','+levelCount+','+i+', 12)\" />';inlinePopupDesign1+='</label></td></tr>';}}}
inlinePopupDesign1+='</table>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('hide',function(e,editable){for(var i=1;i<=levelCount;i++){scope['IsChanged_'+attrs.attributeid+'_'+i]=true;scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=[];}
scope.$apply();});angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);if(levelCount==1){for(var tl=1;tl<=levelCount;tl++){scope['IsChanged_'+attrs.attributeid+'_'+i]=false;var dropdown_text='multiselectdropdown_text_'+attrs.attributeid+'_'+tl;if(tl==levelCount){var tempcaption=scope.treeTexts[dropdown_text].toString().trim().split(',');scope['multiselectdropdown_'+attrs.attributeid+'_'+tl]=[];var tempval=[];for(var k=0;k<tempcaption.length;k++){scope['multiselectdropdown_'+attrs.attributeid+'_'+tl].push(($.grep(scope.treeSources["multiselectdropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==tempcaption[k].trim();}))[0]);}}}}
else{for(var i=1;i<=levelCount;i++){scope['IsChanged_'+attrs.attributeid+'_'+i]=false;var dropdown_text='multiselectdropdown_text_'+attrs.attributeid+'_'+i;if(i==1){scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=($.grep(scope.treeSources["multiselectdropdown_"+attrs.attributeid].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}
else{if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)]!=undefined){if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined){scope.BindChildDropdownSource((attrs.attributeid),levelCount,(i-1),12);if(i==levelCount){var tempcaption=scope.treeTexts[dropdown_text].toString().trim().split(',');scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=[];var tempval=[];for(var k=0;k<tempcaption.length;k++){if(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children!=undefined)
scope['multiselectdropdown_'+attrs.attributeid+'_'+i].push(($.grep(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==tempcaption[k].trim();}))[0]);}}
else{scope['multiselectdropdown_'+attrs.attributeid+'_'+i]=($.grep(scope['multiselectdropdown_'+attrs.attributeid+'_'+(i-1)].Children,function(e){return e.Caption.toString()==scope.treeTexts[dropdown_text].toString().trim()}))[0];}}}}}}}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];for(var li=1;li<=levelCount;li++){scope['IsChanged_'+attrs.attributeid+'_'+i]=true;var dropdown_text='MultiSelectDropDown_'+attrs.attributeid+'_'+li;if(li==levelCount){if((scope['multiselectdropdown_'+attrs.attributeid+'_'+li]!=undefined)&&(scope['multiselectdropdown_'+attrs.attributeid+'_'+li]!="")){var tempval=[];for(var k=0;k<(scope['multiselectdropdown_'+attrs.attributeid+'_'+li].length);k++){scope.fields[dropdown_text]=scope['multiselectdropdown_'+attrs.attributeid+'_'+li][k];if(scope['multiselectdropdown_'+attrs.attributeid+'_'+li][k].id==undefined||scope['multiselectdropdown_'+attrs.attributeid+'_'+li][k].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+li][k].id);}
tempval.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+li][k].Caption);}
if(tempval.length>0)
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+li]=tempval.join(", ");else
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+li]='-';}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+li]='-';}}
else{if((scope['multiselectdropdown_'+attrs.attributeid+'_'+li]!=undefined)&&(scope['multiselectdropdown_'+attrs.attributeid+'_'+li]!="")){scope.fields[dropdown_text]=scope['multiselectdropdown_'+attrs.attributeid+'_'+li];if(scope['multiselectdropdown_'+attrs.attributeid+'_'+li].id==undefined||scope['multiselectdropdown_'+attrs.attributeid+'_'+li].id==""){scope.treeSelection.push(0);}
else{scope.treeSelection.push(scope['multiselectdropdown_'+attrs.attributeid+'_'+li].id);}
scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+li]=scope['multiselectdropdown_'+attrs.attributeid+'_'+li].Caption;}
else{scope.fields[dropdown_text]='-';scope.treeSelection.push(0);scope.treeTexts['multiselectdropdown_text_'+attrs.attributeid+'_'+li]='-';}}
scope.$apply();}
scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});}}};}]);app.directive('xeditabletreedatetime',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename+attrs.primaryid;var CurrentEntityID=parseInt(attrs.entityid,10)
var datetimeid=attrs.datetimeId;var attrID=attrs.primaryid;var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<div class="ngcompile">';inlinePopupDesign1+='<input type="text" class="width2x" ng-click="Calanderopen($event, '+attrID+')"  datepicker-popup="{{format}}" ng-model="fields.DateTime_Dir_'+attrID+'" is-open="calanderopened" min="fields.DatePartMinDate_'+attrID+'" max="fields.DatePartMaxDate_'+attrID+'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />'
inlinePopupDesign1+='</div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){scope.opened=true;$compile($('.ngcompile'))(scope);if(scope.fields["DateTime_"+attrID]!=""&&scope.fields["DateTime_"+attrID]!="-")
if(scope.fields["DateTime_"+attrID]instanceof Date)
scope.fields["DateTime_Dir_"+attrID]=scope.fields["DateTime_"+attrID];else{scope.fields["DateTime_Dir_"+attrID]=new Date.create(dateFormat(scope.fields["DateTime_"+attrID],scope.DefaultSettings.DateFormat));}
else
scope.fields["DateTime_Dir_"+attrID]=null;}},10);});angular.element(element).on('save',function(e,params){if(scope.fields["DateTime_Dir_"+attrID]==null&&scope.fields["DateTime_Dir_"+attrID]==undefined)
scope.fields["DateTime_Dir_"+attrID]="";var a=$.grep(scope.tempholidays,function(e){return e==dateFormat(scope.fields["DateTime_Dir_"+attrID],scope.format);});if(a!=null){if(a.length>0){bootbox.alert("Selected date should not same as non businessday");scope.fields["DateTime_Dir_"+attrID]="";}
else{scope.saveDropdownTree(attrs.attributeid,attrs.attributetypeid,CurrentEntityID,scope.fields["DateTime_Dir_"+attrID]);}}});}};}]);app.directive("redactor",[function(){return{require:'?ngModel',link:function($scope,elem,attrs,controller){controller.$render=function(){elem.redactor({keyupCallback:function(){$scope.$apply(controller.$setViewValue(elem.getCode()));},execCommandCallback:function(){$scope.$apply(controller.$setViewValue(elem.getCode()));}});if(controller.$viewValue!=undefined){elem.setCode(controller.$viewValue);}};}};}]);app.directive('choiceTree',[function(){return{template:'<ul><choice ng-repeat="choice in tree"></choice></ul>',replace:true,transclude:true,restrict:'E',scope:{tree:'=ngModel',}};}]);app.directive('choice',['$compile',function($compile){return{restrict:'E',template:'<li>'+'<label class="checkbox" ng-click="choiceClicked(choice)">'+'<input type="checkbox" ng-checked="choice.checked1"> <span>{{choice.name}}</span>'+'</label>'+'</li>',require:"^ngController",link:function(scope,elm,attrs,myGreatParentControler){scope.choiceClicked=function(choice){choice.checked1=!choice.checked1;myGreatParentControler.setSelected(choice);function checkChildren(c){angular.forEach(c.children,function(c){c.checked1=choice.checked1;if(c.id!=choice.id){myGreatParentControler.setSelected(c);}
checkChildren(c);});}
checkChildren(choice);};if(scope.choice.children.length>0){var childChoice=$compile('<choice-tree ng-model="choice.children"></choice-tree>')(scope)
elm.append(childChoice);}}};}]);app.directive('bDatepicker',[function(){return{restrict:'A',link:function(scope,el,attr){el.datepicker();}};}]);app.directive('cellHighlight',[function(){return{restrict:'C',link:function postLink(scope,iElement,iAttrs){iElement.find('i').mouseover(function(){$(this).parent('li').css('opacity','0.7');}).mouseout(function(){$(this).parent('li').css('opacity','1.0');$(this).parent('li').removeClass('open');});}};}]);app.directive('context',[function(){return{restrict:'A',compile:function compile(tElement,tAttrs,transclude,scope){return{post:function postLink(scope,iElement,iAttrs,controller){var ul=$('#'+iAttrs.context),last=null;ul.css({'display':'none'});$(iElement).click(function(event){event.stopPropagation();var top=event.clientY+10;if((ul.height()+top+14)>$(window).height()){top=top-(ul.height()+50);}else{}
ul.css({position:"fixed",display:"block",left:event.clientX-220+'px',top:top+'px',zIndex:999999});var target=$(event.target);var menuOption=target.attr('data-role');if(menuOption=='TaskMenuOption'){var taskStatus=target.attr('data-taskstatus');scope.OrganizeTaskMenuOption(parseInt(taskStatus));scope.ShowHideSettoComplete(parseInt(target.attr('data-tasktypeid')),parseInt(target.attr('data-taskstatus')));scope.contextTaskID=target.attr('data-taskid');scope.SetContextTaskID(scope.contextTaskID);scope.ContextTaskListID=target.attr('data-tasklist');scope.SetContextTaskListID(scope.ContextTaskListID);scope.ContextTaskEntityID=parseInt(target.attr('data-entityid'));scope.SetContextTaskListEntityID(scope.ContextTaskEntityID);}
else if(menuOption=='TaskListMenuOption'){scope.ContextTaskListID=target.attr('data-menuid');scope.ContextTaskEntityID=parseInt(target.attr('data-entityid'));scope.SetContextTaskListID(scope.ContextTaskListID);scope.SetContextTaskListEntityID(scope.ContextTaskEntityID);}
else if(menuOption=='TaskFlagOption'){scope.ContextTaskListID=target.attr('data-tasklist');scope.SetContextTaskListID(scope.ContextTaskListID);scope.contextTaskID=target.attr('data-taskid');scope.SetContextTaskID(scope.contextTaskID);}
else if(menuOption=='TaskDetailsMember'){scope.ContextMemberID=target.attr('data-id');scope.SetContextMemberID(scope.ContextMemberID);}
else if(menuOption=='TaskFileDetail'){scope.contextFileID=target.attr('data-FileID');scope.SetContextFileID(scope.contextFileID);}
else if(menuOption=='TaskChecklistOptions'){scope.contextCheckListID=target.attr('data-id');scope.contextCheckListIndex=target.attr('data-indexval');scope.SetcontextCheckListID(target.attr('data-id'),target.attr('data-indexval'));}
else if(menuOption=='tasklistExportMenuholder'){}
else if(menuOption=='TaskFileAttachmentMenuOption'){var top=event.clientY+10;if((ul.height()+top+14)>$(window).height()){top=top-(ul.height()+40);}else{}
ul.css({position:"fixed",display:"block",left:event.clientX+10+'px',top:top+'px',zIndex:999999});}
last=event.timeStamp;if(event.stopPropagation)event.stopPropagation();if(event.preventDefault)event.preventDefault();event.cancelBubble=true;event.returnValue=false;});$(document).click(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});$(window).scroll(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});}};}};}]);app.directive('contextmenuoptions',[function(){return{restrict:'A',compile:function compile(tElement,tAttrs,transclude,scope){return{post:function postLink(scope,iElement,iAttrs,controller){var ul=$('#'+iAttrs.contextmenuoptions),last=null;ul.css({'display':'none'});$(iElement).click(function(event){event.stopPropagation();var top=event.clientY+10;if((ul.height()+top+14)>$(window).height()){top=top-(ul.height()+50);}else{}
if(top<0){ul.css({position:"fixed",display:"block",left:event.clientX-220+'px',top:10+'px',zIndex:999999});}
else{ul.css({position:"fixed",display:"block",left:event.clientX-220+'px',top:(top)+'px',zIndex:999999});}
var target=$(event.target);last=event.timeStamp;if(event.stopPropagation)event.stopPropagation();if(event.preventDefault)event.preventDefault();event.cancelBubble=true;event.returnValue=false;});$(document).click(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});$(window).scroll(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});}};}};}]);app.directive('contextmenuoptions',[function(){return{restrict:'A',compile:function compile(tElement,tAttrs,transclude,scope){return{post:function postLink(scope,iElement,iAttrs,controller){var ul=$('#'+iAttrs.contextmenuoptions),last=null;ul.css({'display':'none'});$(iElement).click(function(event){event.stopPropagation();var top=event.clientY+10;if((ul.height()+top+14)>$(window).height()){top=top-(ul.height()+50);}else{}
if(top<0){ul.css({position:"fixed",display:"block",left:event.clientX-220+'px',top:10+'px',zIndex:999999});}
else{ul.css({position:"fixed",display:"block",left:event.clientX-220+'px',top:(top)+'px',zIndex:999999});}
var target=$(event.target);last=event.timeStamp;if(event.stopPropagation)event.stopPropagation();if(event.preventDefault)event.preventDefault();event.cancelBubble=true;event.returnValue=false;});$(document).click(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});$(window).scroll(function(event){var target=$(event.target);if(!target.is(".popover")&&!target.parents().is(".popover")){if(last===event.timeStamp)
return;ul.css({'display':'none'});}});}};}};}]);app.directive("myQtip2",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"qtip-dark qtip-desc");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";$(element).qtip({content:{text:function(api){return($(this).attr('qtip-content')!="")?$(this).attr('qtip-content'):"-";},button:scope.closeButton},style:{classes:scope.qtipSkin+" qtip-shadow qtip-rounded qtip-desc",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},events:{hide:function(event,api){event:'unfocus click mouseleave'}},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'bottom center',at:'top center',viewport:$(window),adjust:{method:'shift',mouse:true},corner:{target:'bottom center',tooltip:'bottom center',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive("myQtipt2b",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"qtip-dark qtip-desc margin-top10x");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="bottom center";scope.qtipContentPos="bottom center";$(element).qtip({content:{text:function(api){return($(this).attr('qtip-content')!="")?$(this).attr('qtip-content'):"-";},button:scope.closeButton},style:{classes:scope.qtipSkin+" qtip-shadow qtip-rounded qtip-desc",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},events:{hide:function(event,api){event:'unfocus click mouseleave'}},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'top center',at:'bottom center',viewport:$(window),adjust:{method:'shift',mouse:true},corner:{target:'top center',tooltip:'top center',mimic:'bottom'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive("bsQtipStatic",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"qtip-dark");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";$(element).qtip({content:{text:function(api){return $(this).attr('qtip-content')==null?'<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>':$(this).attr('qtip-content');},button:scope.closeButton},style:{classes:scope.qtipSkin+" qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},events:{hide:function(event,api){event:'unfocus click mouseleave'}},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'bottom center',at:'top center',viewport:$(window),adjust:{method:'shift',mouse:true},corner:{target:'bottom center',tooltip:'bottom center',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive("bsQtip2Dynamic",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"qtip-dark");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";var attrslst=attrs;$(element).qtip({content:{text:'<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',},events:{show:function(event,api){var qtip_id=api.elements.content.attr('id'),entityid=attrslst.entityid,metadatatype=attrslst.metadatatype,costcenterid=attrslst.costcenterid,placeholder=attrslst.resultplace;if(parseInt(metadatatype)==1)
scope.loadtQtipContent(qtip_id,entityid,costcenterid,placeholder);else
scope.loadtQtipSpentContent(qtip_id,entityid,costcenterid,placeholder);}},style:{classes:scope.qtipSkin+" qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'top right',at:'top left',viewport:$(window),corner:{target:'top right',tooltip:'top right',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive('multiselectDropdown',[function(){return function(scope,element,attributes){element=$(element[0]);element.multiselect({enableFiltering:true,enableCaseInsensitiveFiltering:true,includeSelectAllOption:false,dropRight:true});scope.$watch(function(){return element[0].length;},function(){element.multiselect('rebuild');});scope.$watchCollection(attributes.ngModel,function(){element.multiselect('refresh');});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}}]);app.directive('multiselectpartial',[function(){return function(scope,element,attributes){element=$(element[0]);element.multiselectpartial({});scope.$watch(function(){return element[0].length;},function(){element.multiselectpartial('rebuild');});scope.$watch(attributes.ngModel,function(){element.multiselectpartial('refresh');});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}}]);app.filter('unique',[function(){return function(input,key){var unique={};var uniqueList=[];for(var i=0;i<input.length;i++){if(typeof unique[input[i][key]]=="undefined"){unique[input[i][key]]="";uniqueList.push(input[i]);}}
return uniqueList;};}]);app.filter('AvoidAssetApprovalTask',function(){return function(items){var filtered=[];for(var i=0;i<items.length;i++){var item=items[i];if(item.TaskTypeId!=32&&item.TaskTypeId!=36){filtered.push(item);}}
return filtered;};});app.service('sharedProperties',[function(){var stringValue='test string value';var objectValue={data:'test object value'};var itemID=0;return{getString:function(){return stringValue;},getID:function(){return itemID;},setString:function(value,id){stringValue=value;itemID=id;},getObject:function(){return objectValue;}}}]);app.directive('multiselect',['$parse','$document','$compile','optionParser',function($parse,$document,$compile,optionParser){return{restrict:'E',require:'ngModel',link:function(originalScope,element,attrs,modelCtrl){var exp=attrs.options,parsedResult=optionParser.parse(exp),isMultiple=attrs.multiple?true:false,required=false,scope=originalScope.$new(),changeHandler=attrs.change||anguler.noop;scope.items=[];scope.header='';scope.multiple=isMultiple;scope.disabled=false;originalScope.$on('$destroy',function(){scope.$destroy();});var popUpEl=angular.element('<multiselect-popup></multiselect-popup>');if(attrs.required||attrs.ngRequired){required=true;}
attrs.$observe('required',function(newVal){required=newVal;});scope.$watch(function(){return $parse(attrs.disabled)(originalScope);},function(newVal){scope.disabled=newVal;});scope.$watch(function(){return $parse(attrs.multiple)(originalScope);},function(newVal){isMultiple=newVal||false;});scope.$watch(function(){return parsedResult.source(originalScope);},function(newVal){if(angular.isDefined(newVal))
parseModel();},true);scope.$watch(function(){return modelCtrl.$modelValue;},function(newVal,oldVal){if(angular.isDefined(newVal)){markChecked(newVal);scope.$eval(changeHandler);}
getHeaderText();modelCtrl.$setValidity('required',scope.valid());},true);function parseModel(){scope.items.length=0;var model=parsedResult.source(originalScope);if(!angular.isDefined(model))return;for(var i=0;i<model.length;i++){var local={};local[parsedResult.itemName]=model[i];scope.items.push({label:parsedResult.viewMapper(local),model:model[i],checked:false});}}
parseModel();element.append($compile(popUpEl)(scope));function getHeaderText(){if(is_empty(modelCtrl.$modelValue))return scope.header='';if(isMultiple){scope.header=modelCtrl.$modelValue.length+' '+'selected';}else{var local={};local[parsedResult.itemName]=modelCtrl.$modelValue;scope.header=parsedResult.viewMapper(local);}}
function is_empty(obj){if(!obj)return true;if(obj.length&&obj.length>0)return false;for(var prop in obj)if(obj[prop])return false;return true;};scope.valid=function validModel(){if(!required)return true;var value=modelCtrl.$modelValue;return(angular.isArray(value)&&value.length>0)||(!angular.isArray(value)&&value!=null);};function selectSingle(item){if(item.checked){scope.uncheckAll();}else{scope.uncheckAll();item.checked=!item.checked;}
setModelValue(false);}
function selectMultiple(item){item.checked=!item.checked;setModelValue(true);}
function setModelValue(isMultiple){var value;if(isMultiple){value=[];angular.forEach(scope.items,function(item){if(item.checked)value.push(item.model);})}else{angular.forEach(scope.items,function(item){if(item.checked){value=item.model;return false;}})}
modelCtrl.$setViewValue(value);}
function markChecked(newVal){if(!angular.isArray(newVal)){angular.forEach(scope.items,function(item){if(angular.equals(item.model,newVal)){item.checked=true;return false;}});}else{angular.forEach(newVal,function(i){angular.forEach(scope.items,function(item){if(angular.equals(item.model,i)){item.checked=true;}});});}}
scope.checkAll=function(){if(!isMultiple)return;angular.forEach(scope.items,function(item){item.checked=true;});setModelValue(true);};scope.uncheckAll=function(){angular.forEach(scope.items,function(item){item.checked=false;});setModelValue(true);};scope.$watch('clearscope',function(){angular.forEach(scope.items,function(item){item.checked=false;});setModelValue(true);});scope.select=function(item){if(isMultiple===false){selectSingle(item);scope.toggleSelect();}else{selectMultiple(item);}}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}])
app.directive('multiselectPopup',['$document',function($document){return{restrict:'E',scope:false,replace:true,templateUrl:'views/mui/multiselect.tmpl.html',link:function(scope,element,attrs){scope.isVisible=false;scope.toggleSelect=function(){if(element.hasClass('open')){element.removeClass('open');$document.unbind('click',clickHandler);}else{element.addClass('open');$document.bind('click',clickHandler);scope.focus();}};function clickHandler(event){if(elementMatchesAnyInArray(event.target,element.find(event.target.tagName)))
return;element.removeClass('open');$document.unbind('click',clickHandler);scope.$apply();}
scope.focus=function focus(){var searchBox=element.find('input')[0];searchBox.focus();}
scope.$on("$destroy",function(){$(window).off("resize.Viewport");});var elementMatchesAnyInArray=function(element,elementArray){for(var i=0;i<elementArray.length;i++)
if(element==elementArray[i])
return true;return false;}}}}]);app.factory('optionParser',['$parse',function($parse){var TYPEAHEAD_REGEXP=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(input){var match=input.match(TYPEAHEAD_REGEXP),modelMapper,viewMapper,source;if(!match){throw new Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'"+" but got '"+input+"'.");}
return{itemName:match[3],source:$parse(match[4]),viewMapper:$parse(match[2]||match[1]),modelMapper:$parse(match[1])};}};}])
app.directive('xeditableobjective',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeName=attrs.attributename;var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.text(value);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){if(attrs.objectiveedittype==111){if(scope.ObjectiveDescription==undefined){scope.SaveObjectiveDeatils(attrs.objectiveid,$('<div />').html(attrs.objectivename).text(),attrs.objectiveedittype,$('<div />').html(attrs.objectivedescripton).text(),params.newValue);}else{scope.SaveObjectiveDeatils(attrs.objectiveid,$('<div />').html(attrs.objectivename).text(),attrs.objectiveedittype,scope.ObjectiveDescription,params.newValue);}}
else{if(scope.ObjectiveName==undefined){scope.SaveObjectiveDeatils(attrs.objectiveid,$('<div />').html(attrs.objectivename).text(),attrs.objectiveedittype,$('<div />').html(attrs.objectivedescripton).text(),params.newValue);}else{scope.SaveObjectiveDeatils(attrs.objectiveid,scope.ObjectiveName,attrs.objectiveedittype,$('<div />').html(attrs.objectivedescripton).text(),params.newValue);}}});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablecalender',['$timeout',function($timeout){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var CurrentEntityID=parseInt(attrs.entityid,10);var attributeName=attrs.attributename;var loadXeditable=function(){angular.element(element).editable({display:function(value,srcData){ngModel.$setViewValue(value);element.text(value);scope.$apply();}});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('save',function(e,params){if(attrs.calenderedittype==111){if(scope.CalenderDescription==undefined){scope.SaveCalenderDeatils(attrs.calenderid,$('<div />').html(attrs.calendername).text(),attrs.calenderedittype,$('<div />').html(attrs.calenderdescripton).text(),params.newValue);}else{scope.SaveCalenderDeatils(attrs.calenderid,$('<div />').html(attrs.calendername).text(),attrs.calenderedittype,scope.CalenderDescription,params.newValue);}}
else{if(scope.CalenderName==undefined){scope.SaveCalenderDeatils(attrs.calenderid,$('<div />').html(attrs.calendername).text(),attrs.calenderedittype,$('<div />').html(attrs.calenderdescripton).text(),params.newValue);}else{scope.SaveCalenderDeatils(attrs.calenderid,scope.CalenderName,attrs.calenderedittype,$('<div />').html(attrs.calenderdescripton).text(),params.newValue);}}});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditableobjectiveownerlist',['$timeout','$compile','$resource','$translate',function($timeout,$compile,$resource,$translate){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="xeditobjectiveowner" name="city" class="ngcompileDropdown">';inlinePopup+='<option ng-repeat="ndata in ObjectiveOwnerList" value=\"{{ndata.UserId}}\">{{ndata.UserName}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['xeditobjectiveowner']=($.grep(scope.ObjectiveOwnerList,function(e){return e.UserName==scope.ObjectiveFulfillDeatils.OwnerName}))[0].UserId;}},10);});angular.element(element).on('save',function(e,params){if(scope['xeditobjectiveowner']!=undefined){var tempvar=($.grep(scope.ObjectiveOwnerList,function(e){return parseInt(e.UserId,10)==parseInt(scope['xeditobjectiveowner'],10)}))[0];scope.ObjectiveOwnerName=tempvar.UserName;scope.objectiveRoleId=tempvar.RoleId;scope.ObjectiveOwnerId=tempvar.UserId;}
else{scope.CurrentWorkflowStepName='-';$scope.SelectedWorkflowStep=0;scope.WorkFlowStepID=0;scope.$apply();}
scope.UpdateObjectiveOwner(scope.ObjectiveOwnerId,scope.objectiveRoleId,scope.ObjectiveOwnerName);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablecalenderownerlist',['$timeout','$compile','$resource','$translate',function($timeout,$compile,$resource,$translate){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="xeditcalenderowner" name="city" class="ngcompileDropdown">';inlinePopup+='<option ng-repeat="ndata in CalenderOwnerList" value=\"{{ndata.UserId}}\">{{ndata.UserName}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['xeditcalenderowner']=($.grep(scope.CalenderOwnerList,function(e){return e.UserName==scope.CalenderFulfillDeatils.OwnerName}))[0].UserId;}},10);});angular.element(element).on('save',function(e,params){if(scope['xeditcalenderowner']!=undefined){var tempvar=($.grep(scope.CalenderOwnerList,function(e){return parseInt(e.UserId,10)==parseInt(scope['xeditcalenderowner'],10)}))[0];scope.CalenderOwnerName=tempvar.UserName;scope.calenderRoleId=tempvar.RoleId;scope.CalenderOwnerId=tempvar.UserId;}
else{scope.CurrentWorkflowStepName='-';$scope.SelectedWorkflowStep=0;scope.WorkFlowStepID=0;scope.$apply();}
scope.UpdateCalenderOwner(scope.CalenderOwnerId,scope.calenderRoleId,scope.CalenderOwnerName);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditableobjectivestatus',['$timeout','$compile','$resource','$translate',function($timeout,$compile,$resource,$translate){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><label><select ui-select2 data-ng-model="xeditobjectivestatus" name="city" class="ngcompileDropdown">';inlinePopup+='<option ng-repeat="ndata in ObjectivestatusList" value=\"{{ndata.StatusId}}\">{{ndata.StatusDescription}}</option></select></label></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompileDropdown'))(scope);scope['xeditobjectivestatus']=($.grep(scope.ObjectivestatusList,function(e){return e.StatusDescription==scope.ObjectiveStatus}))[0].StatusId;}},10);});angular.element(element).on('save',function(e,params){var objSatusId=0;if(scope['xeditobjectivestatus']!=undefined){scope.ObjectiveStatus=($.grep(scope.ObjectivestatusList,function(e){return parseInt(e.StatusId,10)==parseInt(scope['xeditobjectivestatus'],10)}))[0].StatusDescription;objSatusId=scope['xeditobjectivestatus'];}
scope.UpdateObjectivestatus(objSatusId);});scope.$on("$destroy",function(){$(window).off("resize.Viewport");});}};}]);app.directive('xeditablecmspublishdate',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename+attrs.primaryid;var CurrentEntityID=parseInt(attrs.entityid,10)
var datetimeid=attrs.datetimeId;var attrID=attrs.primaryid;var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<div class="row-fluid"><div class="inputHolder">';inlinePopupDesign1+='     <div class="ngcompile control-group">';inlinePopupDesign1+='         <label class="control-label">Published Date</label>';inlinePopupDesign1+='         <div class="controls">';inlinePopupDesign1+='             <input placeholder="-- Date Time --" id="'+datetimeid+'" data-ng-model="Publishfields.PublishDate_Dir" type="text"  data-date-format="'+Defaultdate+'"  required="" name="startDate" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + Publishfields.PublishDate_Dir] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + Publishfields.PublishDate_Dir + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" class="ng-pristine ng-invalid ng-invalid-required">';inlinePopupDesign1+='     </div></div>';inlinePopupDesign1+='     <div class="control-group">';inlinePopupDesign1+='         <label class="control-label">Published Time</label>';inlinePopupDesign1+='         <div class="controls">';inlinePopupDesign1+='             <select ui-select2 data-ng-model="Publishfields.PublishTime_Dir" name="city" class="ngcompile"><option ng-repeat=\"pdata in PublishedTime\" value=\"{{pdata.name}}\">{{pdata.name}}</option></select></label>';inlinePopupDesign1+=' </div></div></div></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope.Publishfields["PublishDate_Dir"]=scope.Publishfields["CmsPagePublishDate"];scope.Publishfields["PublishTime_Dir"]=scope.Publishfields["CmsPagePublishTime"];}},10);});angular.element(element).on('save',function(e,params){if(typeof scope.Publishfields["PublishDate_Dir"]!="string"){var datstartval="";datstartval=new Date(scope.Publishfields["PublishDate_Dir"].toString());if(datstartval=='NaN-NaN-NaN'){scope.Publishfields["PublishDate_Dir"]=ConvertDateToString(ConvertStringToDateByFormat(scope.Publishfields["PublishDate_Dir"].toString(),GlobalUserDateFormat))}
else
scope.Publishfields["PublishDate_Dir"]=ConvertDateToString(datstartval);}
scope.saveCmsEntityDetailsValue("PublishDateTime",CurrentEntityID,scope.Publishfields["PublishDate_Dir"]+" @ "+scope.Publishfields["PublishTime_Dir"]);});}};}]);app.directive('xeditablecmsrevisions',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var attributeName=attrs.attributename+attrs.primaryid;var CurrentEntityID=parseInt(attrs.entityid,10)
var datetimeid=attrs.datetimeId;var attrID=attrs.primaryid;var isDataTypeExists=false;for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopupDesign1='';inlinePopupDesign1+='<div><label>Revisions <select ui-select2 data-ng-model="Publishfields.Revisions_Dir" name="city" class="ngcompile"><option ng-repeat=\"pdata in Publishfields.Revisions\" value=\"{{pdata.ContentVersionID}}\">Created On: {{pdata.CreatedOn}}</option></select></label></div></div></div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopupDesign1,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){$compile($('.ngcompile'))(scope);scope.Publishfields["Revisions_Dir"]=scope.RevisionID;}},10);});angular.element(element).on('save',function(e,params){scope.saveCmsEntityDetailsValue("Revisions",CurrentEntityID,scope.Publishfields["Revisions_Dir"].toString());});}};}]);app.directive('xeditabletagwords',['$timeout','$compile','$resource','$translate','$window',function($timeout,$compile,$resource,$translate,$window){return{restrict:'A',require:"ngModel",link:function(scope,element,attrs,ngModel){var Version=1;var attributeName=attrs.attributename;var CurrentEntityID=parseInt(attrs.entityid,10)
var isDataTypeExists=false;scope.normaltreeSources["TagWords_"+attrs.attributeid]=[];scope.normaltreeSources["TagWordsShowHide_"+attrs.attributeid]=false;var GetOptionListByID=''
var attrId=attrs.attributeid.split('_');attrId=attrId[0];for(var variable in $.fn.editabletypes){if(attributeName==variable)
isDataTypeExists=true;}
if(isDataTypeExists===false){var Address=function(options){this.init(attributeName,options,Address.defaults);};$.fn.editableutils.inherit(Address,$.fn.editabletypes.abstractinput);var inlinePopup='';inlinePopup+='<div><div class="ngcompiletagwords loadIcon-small" ng-hide = "TagWordsShowHide_'+attrId+'" class="loadIcon-small width1halfx"></div> \
                                                <div name="tagwords" class="ngcompiletagwords"> \
                                                <directive-tagwords item-attrid = "'+attrs.attributeid+'" item-show-hide-progress = "TagWordsShowHide_'+attrId+'" item-tagword-id="TagWords_'+attrId+'" item-tagword-list="normaltreeSources.TagWords_'+attrId+'"></directive-tagwords class="ngcompiletagwords"></div> \
                                            </div>';Address.defaults=$.extend({},$.fn.editabletypes.abstractinput.defaults,{tpl:inlinePopup,inputclass:''});$.fn.editabletypes[attributeName]=Address;}
var loadXeditable=function(){angular.element(element).editable({title:attrs.title,display:function(value,srcData){},savenochange:true});}
$timeout(function(){loadXeditable();},10);angular.element(element).on('shown',function(e,editable){$timeout(function(){if(editable!=undefined){scope.normaltreeSources["TagWordsShowHide_"+attrId]
$timeout(function(){$compile($('.ngcompiletagwords'))(scope);$('.ngcompiletagwords').parents(".popover-content").css("min-width","286px").css("min-height","120px");},10);var tagwrds=new Array();for(var i=0;i<scope.fields['TagWordsSeleted_'+attrId].length;i++){tagwrds[i]=scope.fields['TagWordsSeleted_'+attrId][i];}
scope['TagWords_'+attrId]=tagwrds;}},10);});angular.element(element).on('save',function(e,params){scope.treeSelection=[];scope.fields['TagWordsSeleted_'+attrId]=[];var dropdowntext='TagWords_'+attrId;var tempcap=[];if(scope['TagWords_'+attrId]!=undefined&&scope['TagWords_'+attrId].length>0){var tempvar=[];scope.treeSelection=scope['TagWords_'+attrId];scope.fields['TagWordsSeleted_'+attrId]=scope['TagWords_'+attrId];for(var j=0;j<scope['TagWords_'+attrId].length;j++){tempcap.push($.grep(scope.normaltreeSources["TagWords_"+attrId],function(e){return e.Id==scope['TagWords_'+attrId][j];})[0].Caption);}
scope.fields['TagWordsCaption_'+attrId]=tempcap.join(',');}
else{scope.treeSelection.push(0);scope.fields['TagWordsCaption_'+attrId]="-";scope.fields['TagWordsSeleted_'+attrId]=[];}
scope.saveDropdownTree(attrId,attrs.attributetypeid,CurrentEntityID,scope.treeSelection);});}};}]);app.directive("dirattributegrouplistview",['$compile','$parse',function($compile,$parse){return function(scope,element,attrs){scope.qtipSkin=(attrs.skin?"qtip-"+attrs.skin:"qtip-dark");scope.closeButton=(attrs.closeButton==null?false:true)
scope.qtipPointerPos="top center";scope.qtipContentPos="top center";var attrslst=attrs;$(element).qtip({content:{text:'<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',},events:{show:function(event,api){var qtip_id=api.elements.content.attr('id');scope.LoadAttributeGroupToolTip(qtip_id);}},style:{classes:scope.qtipSkin+" qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(0);},event:'unfocus click mouseleave'},position:{my:'top right',at:'top left',viewport:$(window),corner:{target:'top right',tooltip:'top right',mimic:'top'},target:'mouse'}});scope.$on("$destroy",function(){$(element).qtip('destroy',true);$(window).off("resize.Viewport");});}}]);app.directive('costcentreTree',['$timeout',function($timeout){return{restrict:'E',template:"<ul  class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">\n  <li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n   "
+"<a  ng-hide=\"treeAccessable\" class=\"treesearchcls\"  ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n    <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n"
+"<a  ng-show=\"treeAccessable\"  class=\"treesearchcls\" ng-click=\"user_clicks_branch_Expand_Collapse(row.branch,$event)\">\n   <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n"
+"</div></li>\n</ul>",scope:{treeData:'=',treeFilter:'=',onSelect:'&',initialSelection:'@',accessable:'@',treeControl:'='},link:function(scope,element,attrs){var error,expand_all_parents,expand_level,for_all_ancestors,for_each_branch,get_parent,n,on_treeData_change,select_branch,selected_branch,tree;error=function(s){console.log('ERROR:'+s);return void 0;};if(attrs.iconExpand==null){attrs.iconExpand='icon-caret-right';}
if(attrs.iconCollapse==null){attrs.iconCollapse='icon-caret-down';}
if(attrs.iconLeaf==null){attrs.iconLeaf='icon-fixed-width icon-blank';}
if(attrs.expandLevel==null){attrs.expandLevel='100';}
expand_level=parseInt(attrs.expandLevel,10);if(!scope.treeData){console.log('no treeData defined for the tree!');return;}
if(scope.treeData.length==null){if(treeData.Caption!=null){scope.treeData=[treeData];}else{console.log('treeData should be an array of root branches');return;}}
for_each_branch=function(f){var do_f,root_branch,_i,_len,_ref,_results;do_f=function(branch,level){var child,_i,_len,_ref,_results;f(branch,level);if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(do_f(child,level+1));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(do_f(root_branch,1));}
return _results;};selected_branch=null;select_branch=function(branch,parentArr){if(!branch){if(selected_branch!=null){selected_branch.selected=false;}
selected_branch=null;return;}
if(branch!==undefined){if(selected_branch!=null){selected_branch.selected=false;}
branch.selected=true;selected_branch=branch;expand_all_parents(branch);if(branch.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return branch.onSelect(branch,test);});}else{if(scope.onSelect!=null){return $timeout(function(){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return scope.onSelect({branch:branch,parent:test});});}}}};scope.parenttester=[];scope.user_clicks_branch=function(branch){if(branch!==selected_branch){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return select_branch(branch,test);}};scope.filterItem=function(item){if(!scope.treeFilter){item.isShow=true;if(item.expanded!=undefined){item.expanded=true;}
return true;}
var found=item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase())!=-1;if(!found){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){var match=scope.filterItem(item);if(match){found=true;item.isShow=true;}});}
return found;};function collapseOnemptySearch(item){var itemColl=item.branch!=undefined?item.branch.Children:item.Children;angular.forEach(itemColl,function(item){item.isShow=true;});}
scope.user_clicks_branch_Expand_Collapse=function(branch,event){var test=[];var parent;parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}};scope.user_clicks_branch_1=function(branch,event){var test=[];var parent;parent=get_parent(branch);var target=$(event.target);if(target.hasClass("tree-icon")){branch.ischecked=!branch.ischecked
for(var z=0,childObj;childObj=branch.Children[z++];){childObj.isShow=branch.expanded;Expandcollapse(childObj.Children,branch.expanded);}
return false;}
else{branch.ischecked=branch.ischecked;return select_branch(branch,test);}};function Expandcollapse(Children,expanded){for(var z=0,childObj;childObj=Children[z++];){childObj.isShow=expanded;if(childObj.Children.length>0){Expandcollapse(childObj.Children,expanded);}}}
function recursiveparent(parent){var parent1;parent1=get_parent(parent);if(parent1!=null&&parent1!=undefined){scope.parenttester.push(parent1);recursiveparent(parent1);}
return scope.parenttester;}
get_parent=function(child){var parent;parent=void 0;if(child.parent_uid){for_each_branch(function(b){if(b.uid===child.parent_uid){return parent=b;}});}
return parent;};for_all_ancestors=function(child,fn){var parent;parent=get_parent(child);if(parent!=null){fn(parent);return for_all_ancestors(parent,fn);}};expand_all_parents=function(child){return for_all_ancestors(child,function(b){return b.expanded=true;});};scope.tree_rows=[];scope.treeAccessable=attrs.accessable!=null?(attrs.accessable=="false"?false:true):false;on_treeData_change=function(){var add_branch_to_list,root_branch,_i,_len,_ref,_results;for_each_branch(function(b,level){if(!b.uid){return b.uid=""+Math.random();}});for_each_branch(function(b){var child,_i,_len,_ref,_results;if(angular.isArray(b.Children)){_ref=b.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(child.parent_uid=b.uid);}
return _results;}});scope.tree_rows=[];for_each_branch(function(branch){var child,f;if(branch.Children){if(branch.Children.length>0){f=function(e){if(typeof e==='string'){return{Caption:e,Children:[]};}else{return e;}};return branch.Children=(function(){var _i,_len,_ref,_results;_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];_results.push(f(child));}
return _results;})();}}else{return branch.Children=[];}});add_branch_to_list=function(level,branch,visible){var child,child_visible,tree_icon,_i,_len,_ref,_results;if(branch.expanded==null){branch.expanded=false;}
if(!branch.Children||branch.Children.length===0){tree_icon=attrs.iconLeaf;}else{if(branch.expanded){tree_icon=attrs.iconCollapse;}else{tree_icon=attrs.iconExpand;}}
scope.tree_rows.push({level:level,branch:branch,Caption:branch.Caption,tree_icon:tree_icon,visible:visible,ischecked:branch.ischecked!=undefined?branch.ischecked:false,isShow:branch.isShow!=undefined?branch.isShow:true,});if(branch.Children!=null){_ref=branch.Children;_results=[];for(_i=0,_len=_ref.length;_i<_len;_i++){child=_ref[_i];child_visible=visible&&branch.expanded;_results.push(add_branch_to_list(level+1,child,child_visible));}
return _results;}};_ref=scope.treeData;_results=[];if(_ref!=undefined)
for(_i=0,_len=_ref.length;_i<_len;_i++){root_branch=_ref[_i];_results.push(add_branch_to_list(1,root_branch,true));}
return _results;};scope.$watch('treeData',on_treeData_change,true);if(attrs.initialSelection!=null){for_each_branch(function(b){if(b.Caption===attrs.initialSelection){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return $timeout(function(){return select_branch(b,test);});}});}
n=scope.treeData.length;for_each_branch(function(b,level){b.level=level;return b.expanded=b.level<expand_level;});if(scope.treeControl!=null){if(angular.isObject(scope.treeControl)){tree=scope.treeControl;tree.expand_all=function(){return for_each_branch(function(b,level){return b.expanded=true;});};tree.collapse_all=function(){return for_each_branch(function(b,level){return b.expanded=false;});};tree.get_first_branch=function(){n=scope.treeData.length;if(n>0){return scope.treeData[0];}};tree.select_first_branch=function(){var b;b=tree.get_first_branch();var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
return tree.select_branch(b,test);};tree.get_selected_branch=function(){return selected_branch;};tree.get_parent_branch=function(b){return get_parent(b);};tree.select_branch=function(b,parentArr){var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
select_branch(b,test);return b;};tree.get_children=function(b){return b.Children;};tree.select_parent_branch=function(b){var p;if(b==null){b=tree.get_selected_branch();}
if(b!=null){p=tree.get_parent_branch(b);var test=[];var parent;parent=get_parent(branch);if(parent!=null){test.push(parent);scope.parenttester=[];var ty=recursiveparent(parent);if(ty!=undefined){if(ty.length>0){for(var k=0,obj;obj=scope.parenttester[k++];){test.push(obj);}}}}
if(p!=null){tree.select_branch(p,test);return p;}}};tree.add_branch=function(parent,new_branch){if(parent!=null){parent.Children.push(new_branch);parent.expanded=true;}else{scope.treeData.push(new_branch);}
return new_branch;};tree.add_root_branch=function(new_branch){tree.add_branch(null,new_branch);return new_branch;};tree.expand_branch=function(b){if(b==null){b=tree.get_selected_branch();}
if(b!=null){b.expanded=true;return b;}};tree.collapse_branch=function(b){if(b==null){b=selected_branch;}
if(b!=null){b.expanded=false;return b;}};}}}};}]);})(angular,app);
