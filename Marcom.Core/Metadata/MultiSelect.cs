/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// MultiSelect object for table 'MM_MultiSelect'.
	/// </summary>
	
	internal class MultiSelect : IMultiSelect
	{
		#region Member Variables

        protected int _id;
		protected int _entityid;
        protected int _attributeid;
        protected int _optionid;
		
		
		#endregion
		
		#region Constructors
		public MultiSelect() {}

        public MultiSelect(int pId, int pEntityid, int pAttributeid, int pOptionid)
		{
            this._id = pId;
			 this._entityid = pEntityid;
            this._optionid = pOptionid;
            this._attributeid = pAttributeid;
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
		
        public virtual int Entityid
        {
            get { return _entityid; }
            set {  _entityid = value; }

        }
        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Optionid
        {
            get { return _optionid; }
            set { _optionid = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			MultiSelect castObj = null;
			try
			{
                castObj = (MultiSelect)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
		
	}
	
}
