﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
   public class EntitytaskphasesDao : BaseDao, ICloneable
    {
        public virtual int ID { get; set; }
        public virtual string TempID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string ColorCode { get; set; }
        public virtual int EntityID { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual int StatusCode { get; set; }


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
