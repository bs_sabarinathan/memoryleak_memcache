﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
   internal class GanttviewHeaderBar : IGanttviewHeaderBar
    {
        protected int _id;
        protected string _name;
        protected string _startdate;
        protected string _enddate;
        protected string _description;
        protected string _colorcode;

        public GanttviewHeaderBar()
        { 
        
        }

        public GanttviewHeaderBar(int pid, string pname, string pstartdate, string penddate, string pdescription, string pcolorcode)
        {
            this._id = pid;
            this._name = pname;
            this._startdate = pstartdate;
            this._enddate = penddate;
            this._description = pdescription;
            this._colorcode = pcolorcode;
        }


        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id=value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Startdate
        {
            get
            {
                return _startdate;
            }
            set
            {
                _startdate = value;
            }
        }

        public string EndDate
        {
            get
            {
                return _enddate;
            }
            set
            {
                _enddate = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public string ColorCode
        {
            get
            {
                return _colorcode;
            }
            set
            {
                _colorcode = value;
            }
        }
    }
}
