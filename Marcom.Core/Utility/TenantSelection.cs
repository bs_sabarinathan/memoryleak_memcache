﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BrandSystems.Marcom.Core.User;
using Amazon.S3;

namespace BrandSystems.Marcom.Core.Utility
{
    public class TenantSelection
    {

        public TenantSelection()
        {
            //do nothing
        }

        public string GetTenantFilePath(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            return xn["FilePath"].InnerText;
        }

        public StorageBlock GetTenantAmazonStoragebytenantId(int TenantID)
        {
            StorageBlock _awsStorage = new StorageBlock();
            try
            {

                string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
                TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
                System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
                xdcDocument.Load(TenantInfoPath);
                System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
                if (Convert.ToInt32(xn["FileSystem"].InnerText) == (int)StorageArea.Amazon)
                {

                    System.Xml.XmlNode _aws = xn.SelectSingleNode("AWSSetup");

                    _awsStorage.AWSAccessKeyID = _aws.SelectSingleNode("AWSAccessKeyID").InnerText;
                    _awsStorage.AWSSecretAccessKey = _aws.SelectSingleNode("AWSSecretAccessKey").InnerText;
                    _awsStorage.BucketName = _aws.SelectSingleNode("BucketName").InnerText;
                    _awsStorage.ServiceURL = _aws.SelectSingleNode("ServiceURL").InnerText;
                    _awsStorage.RegionEndpoint = _aws.SelectSingleNode("RegionEndpoint").InnerText;
                    _awsStorage.Uploaderurl = _aws.SelectSingleNode("UploaderUrl").InnerText;

                    AmazonS3Config S3Config = new AmazonS3Config()
                    {
                        ServiceURL = _awsStorage.ServiceURL,
                        RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_awsStorage.RegionEndpoint)
                    };
                    AmazonS3Client client = new AmazonS3Client(_awsStorage.AWSAccessKeyID, _awsStorage.AWSSecretAccessKey, S3Config);

                    _awsStorage.S3 = client;

                    _awsStorage.storageType = Convert.ToInt32(xn["FileSystem"].InnerText);
                }
                return _awsStorage;
            }
            catch (Exception ex)
            {
                return _awsStorage;
            }
        }

        public string GetTenantHostURLbyTenantID(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            return xn.Attributes["TenantHost"].Value;
        }

        public string GetTenantFilePathByHostName(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return xn["FilePath"].InnerText;
        }

        public StorageBlock GetTenantAmazonStorageByHostName(string TenantHost)
        {
            StorageBlock _awsStorage = new StorageBlock();
            try
            {

                string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
                TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
                System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
                xdcDocument.Load(TenantInfoPath);
                System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
                if (Convert.ToInt32(xn["FileSystem"].InnerText) == (int)StorageArea.Amazon)
                {
                    System.Xml.XmlNode _aws = xn.SelectSingleNode("AWSSetup");

                    _awsStorage.AWSAccessKeyID = _aws.SelectSingleNode("AWSAccessKeyID").InnerText;
                    _awsStorage.AWSSecretAccessKey = _aws.SelectSingleNode("AWSSecretAccessKey").InnerText;
                    _awsStorage.BucketName = _aws.SelectSingleNode("BucketName").InnerText;
                    _awsStorage.ServiceURL = _aws.SelectSingleNode("ServiceURL").InnerText;
                    _awsStorage.RegionEndpoint = _aws.SelectSingleNode("RegionEndpoint").InnerText;
                    _awsStorage.Uploaderurl = _aws.SelectSingleNode("UploaderUrl").InnerText;

                    AmazonS3Config S3Config = new AmazonS3Config()
                    {
                        ServiceURL = _awsStorage.ServiceURL,
                        RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_awsStorage.RegionEndpoint)
                    };

                    AmazonS3Client client = new AmazonS3Client(_awsStorage.AWSAccessKeyID, _awsStorage.AWSSecretAccessKey, S3Config);

                    _awsStorage.S3 = client;

                    _awsStorage.storageType = Convert.ToInt32(xn["FileSystem"].InnerText);
                }
                return _awsStorage;
            }
            catch (Exception ex)
            {
                return _awsStorage;
            }
        }

        public int GetTenantIDByHostName(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return int.Parse(xn["TenantID"].InnerText);
        }

        public string GetTenantDBConnStrbyTenantID(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            return xn["conn"].InnerText;
        }

        public string GetTenantDBConnStrbyTenantHost(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return xn["conn"].InnerText;
        }

        public string GetTenantReportDBConnStrbyTenantHost(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return xn["ReportServerconn"].InnerText;
        }

        public string GetTenantNamebyTenantID(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            return xn["TenantName"].InnerText;
        }

        public string GetTenantNamebyTenantHost(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return xn["TenantName"].InnerText;
        }
    }
}
