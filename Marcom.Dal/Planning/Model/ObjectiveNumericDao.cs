using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveNumericDao object for table 'PM_Objective_Numeric'.
	/// </summary>
	
	public partial class ObjectiveNumericDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _objectiveid;
        protected decimal _globalbaseline;
        protected decimal _globaltarget;
		protected int _unitid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveNumericDao() {}

        public ObjectiveNumericDao(int pObjetiveId, decimal pGlobalBaseLine, decimal pGlobalTarget, int pUnitid)
		{
            this._objectiveid = pObjetiveId;
			this._globalbaseline = pGlobalBaseLine; 
			this._globaltarget = pGlobalTarget; 
			this._unitid = pUnitid; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int ObjectiveId
        {
            get { return _objectiveid; }
            set { _bIsChanged |= (_objectiveid != value); _objectiveid = value; }

        }

        public virtual decimal GlobalBaseLine
		{
			get { return _globalbaseline; }
			set { _bIsChanged |= (_globalbaseline != value); _globalbaseline = value; }
			
		}

        public virtual decimal GlobalTarget
		{
			get { return _globaltarget; }
			set { _bIsChanged |= (_globaltarget != value); _globaltarget = value; }
			
		}
		
		public virtual int Unitid
		{
			get { return _unitid; }
			set { _bIsChanged |= (_unitid != value); _unitid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveNumericDao = "ObjectiveNumericDao";
            public const string ObjectiveId = "ObjectiveId";
			public const string GlobalBaseLine = "GlobalBaseLine";			
			public const string GlobalTarget = "GlobalTarget";			
			public const string Unitid = "Unitid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveNumericDao = "ObjectiveNumericDao";
            public const string ObjectiveId = "ObjectiveId";		
			public const string GlobalTarget = "GlobalTarget";			
			public const string Unitid = "Unitid";			
		}

		#endregion
		
		
	}
	
}
