﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IObjectiveFulfillCondtions
    {
        int Id{get; set;}
        int? EntityTypeid{get;set;}
        int? Attributeid{ get; set;}
        int? Optionid { get; set; }
        int ConditionType{ get; set;}
        IList<int> ObjectiveConditionValue { get; set; }
        string EntityTypeCaption { get; set; }
        string AttributeCaption { get; set; }
        string OptionCaption { get; set; }
        string StartDate { get; set; }
        string EndDate { get; set; }
        string DateRule { get; set; }
        string Mandatory { get; set; }
        string FulfillCondition { get; set; }
        string ObjectiveName { get; set; }
        string ObjectiveDescription { get; set; }
        int OwnerId { get; set; }
        string OwnerName { get; set; }
        bool IsMandatory { get; set; }
        int IsDateRule { get; set; }
        int ConditionId { get; set; }
        int AttributeLevel { get; set; }
        string ObjectiveOptionValue { get; set; }
    }
}
