﻿using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam
{
    internal class DamTypeAttributeRelation : IDamTypeAttributeRelation
    {
        #region Member Variables
        protected int _id;
        protected int _sortorder;
        protected int _typeid;
        protected string _caption;
        protected string _defaultvalue;
        public dynamic _Options { get; set; }
        #endregion



         #region Constructors
        public DamTypeAttributeRelation() { }

        public DamTypeAttributeRelation(int pID,  int pSortOrder, int pTypeID, string pCaption,dynamic pOptions, string pDefaultValue)
        {
            this._id = pID;
            this._sortorder = pSortOrder;
            this._typeid = pTypeID;
            this._caption = pCaption;
            this._Options = pOptions;
            this._defaultvalue = pDefaultValue;
        }

        public DamTypeAttributeRelation(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }

        }
        public string Caption
        {
            get { return _caption; }
            set { _caption = value; }
        }


       public dynamic Options
        {
            get { return _Options; }
            set { _Options = value; }
        }

       public string DefaultValue
       {
           get { return _defaultvalue; }
           set { _defaultvalue = value; }
       }
        #endregion
    }
}
