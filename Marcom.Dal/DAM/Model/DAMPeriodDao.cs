﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class DAMPeriodDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected DateTime _startdate;
        protected DateTime _enddate;
        protected string _description;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    EntityPeriodDao toCompare = obj as EntityPeriodDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Entityid, toCompare.Entityid))
        //        return false;
        //    if (!Object.Equals(this.SortOrder, toCompare.SortOrder))
        //        return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Entityid.GetHashCode();
        //    hashCode = (hashCode * 7) + SortOrder.GetHashCode();
        //    return hashCode;
        //}

        //#endregion

        #region Constructors
        public DAMPeriodDao() { }

        public DAMPeriodDao(int pId, int pEntityid, int pSortOrder, DateTime pStartdate, DateTime pEndDate, string pDescription)
        {
            this._id = pId;
            this._entityid = pEntityid;
            this._sortorder = pSortOrder;
            this._startdate = pStartdate;
            this._enddate = pEndDate;
            this._description = pDescription;
        }

        #endregion

        #region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual DateTime Startdate
        {
            get { return _startdate; }
            set { _bIsChanged |= (_startdate != value); _startdate = value; }

        }

        public virtual DateTime EndDate
        {
            get { return _enddate; }
            set { _bIsChanged |= (_enddate != value); _enddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string DAMPeriodDao = "DAMPeriodDao";
            public const string Entityid = "Entityid";
            public const string Startdate = "Startdate";
            public const string EndDate = "EndDate";
            public const string Description = "Description";
            public const string SortOrder = "SortOrder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string DAMPeriodDao = "DAMPeriodDao";
            public const string Entityid = "Entityid";
            public const string Startdate = "Startdate";
            public const string EndDate = "EndDate";
            public const string Description = "Description";
            public const string SortOrder = "SortOrder";
        }

        #endregion


    }
}
