app.directive('angucompleteAlt', ['$q', '$parse', '$http', '$sce', '$timeout', '$templateCache', '$location', function ($q, $parse, $http, $sce, $timeout, $templateCache, $location) {
    var KEY_DW = 40;
    var KEY_RT = 39;
    var KEY_UP = 38;
    var KEY_LF = 37;
    var KEY_ES = 27;
    var KEY_EN = 13;
    var KEY_BS = 8;
    var KEY_DEL = 46;
    var KEY_TAB = 9;
    var MIN_LENGTH = 0;
    var PAUSE = 500;
    var BLUR_TIMEOUT = 300;
    var REQUIRED_CLASS = 'autocomplete-required';
    var TEXT_SEARCHING = 'Searching...';
    var TEXT_NORESULTS = 'No results found';
    var TEMPLATE_URL = '';
    $templateCache.put(TEMPLATE_URL, '<div class="angucomplete-holder" ng-class="{\'angucomplete-dropdown-visible\': showDropdown}">' + ' <input id="{{id}}_value" ng-model="searchStr" ng-disabled="disableInput" type="text" placeholder="{{placeholder}}" ng-focus="onFocusHandler()" class="{{inputClass}}" ng-focus="resetHideResults()" ng-blur="hideResults($event)" autocapitalize="off" autocorrect="off" ng-change="inputChangeHandler(searchStr)"/>' + ' <div id="{{id}}_dropdown" class="angucomplete-dropdown" ng-show="showDropdown">' + ' <div class="angucomplete-row" ng-repeat="result in results" ng-click="selectResult(result)" ng-mouseenter="hoverRow($index)" ng-class="{\'angucomplete-selected-row\': $index == currentIndex}">' + ' <div ng-if="imageField" class="angucomplete-image-holder">' + ' <img ng-if="result.image && result.image != \'\'" ng-src="{{result.image}}" class="angucomplete-image"/>' + ' <div ng-if="!result.image && result.image != \'\'" class="angucomplete-image-default"></div>' + ' </div>' + ' <div class="angucomplete-title" ng-if="matchClass" ng-bind-html="result.title"></div>' + ' <div class="angucomplete-title" ng-if="!matchClass">{{ result.title }}</div>' + ' <div ng-show="false" ng-if="matchClass && result.description && result.description != \'\'" class="angucomplete-description" ng-bind-html="result.description"></div>' + ' <div ng-show="false" ng-if="!matchClass && result.description && result.description != \'\'" class="angucomplete-description">{{result.description}}</div>' + ' </div>' + ' </div>' + '</div>');
    return {
        restrict: 'EA',
        require: '^?form',
        scope: {
            selectedObject: '=',
            disableInput: '=',
            initialValue: '@',
            localData: '=',
            remoteUrlRequestFormatter: '=',
            remoteUrlResponseFormatter: '=',
            remoteUrlErrorCallback: '=',
            id: '@',
            placeholder: '@',
            onSelect: '&',
            remoteUrl: '@',
            remoteUrlDataField: '@',
            titleField: '@',
            descriptionField: '@',
            imageField: '@',
            inputClass: '@',
            pause: '@',
            searchFields: '@',
            minlength: '@',
            matchClass: '@',
            clearSelected: '@',
            overrideSuggestions: '@',
            fieldRequired: '@',
            fieldRequiredClass: '@',
            inputChanged: '=',
            autoMatch: '@',
            focusOut: '&',
            focusIn: '&',
            searchtype: '=',
            control: '=',
            clickhandler: '&'
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || TEMPLATE_URL;
        },
        link: function (scope, elem, attrs, ctrl) {
            var inputField = elem.find('input');
            var minlength = MIN_LENGTH;
            var searchTimer = null;
            var hideTimer;
            var requiredClassName = REQUIRED_CLASS;
            var responseFormatter;
            var validState = null;
            var httpCanceller = null;
            var dd = elem[0].querySelector('.angucomplete-dropdown');
            var isScrollOn = false;
            var mousedownOn = null;
            var unbindInitialValue;
            elem.on('mousedown', function (event) {
                mousedownOn = event.target.id;
            });
            scope.currentIndex = null;
            scope.searching = false;
            scope.searchStr = scope.initialValue;
            unbindInitialValue = scope.$watch('initialValue', function (newval, oldval) {
                if (newval && newval.length > 0) {
                    scope.searchStr = scope.initialValue;
                    handleRequired(true);
                    unbindInitialValue();
                }
            });
            scope.$on('angucomplete-alt:clearInput', function (event, elementId) {
                if (!elementId) {
                    scope.searchStr = null;
                    clearResults();
                } else {
                    if (scope.id === elementId) {
                        scope.searchStr = null;
                        clearResults();
                    }
                }
            });
            scope.reloadcustompage = function (result) {
                scope.searchStr = extractTitle(result.originalObject);
                if (scope.searchStr != "") {
                    var lastRoute = $location.path();
                    if (lastRoute.contains("/mui/customsearchresult")) if (lastRoute == "/mui/customsearchresults") $location.path("/mui/customsearchresult");
                    else $location.path("/mui/customsearchresults");
                    else $location.path("/mui/customsearchresult");
                }
            }
            scope.internalControl = scope.control || {};
            scope.internalControl.ClearTextSearch = function () {
                scope.searchStr = "";
            }

            function ie8EventNormalizer(event) {
                return event.which ? event.which : event.keyCode;
            }

            function callOrAssign(value) {
                if (typeof scope.selectedObject === 'function') {
                    scope.selectedObject(value);
                } else {
                    scope.selectedObject = value;
                }
                if (value) {
                    handleRequired(true);
                } else {
                    handleRequired(false);
                }
            }

            function callFunctionOrIdentity(fn) {
                return function (data) {
                    return scope[fn] ? scope[fn](data) : data;
                };
            }

            function setInputString(str) {
                callOrAssign({
                    originalObject: str
                });
                if (scope.clearSelected) {
                    scope.searchStr = null;
                }
                clearResults();
            }

            function extractTitle(data) {
                return scope.titleField.split(',').map(function (field) {
                    return extractValue(data, field);
                }).join(' ');
            }

            function extractValue(obj, key) {
                var keys, result;
                if (key) {
                    keys = key.split('.');
                    result = obj;
                    keys.forEach(function (k) {
                        result = result[k];
                    });
                } else {
                    result = obj;
                }
                return result;
            }

            function findMatchString(target, str) {
                var words = str;
                var settings = {
                    caseSensitive: false,
                    wordsOnly: false
                };
                if (words.constructor === String) {
                    words = [words];
                }
                words = jQuery.grep(words, function (word, i) {
                    return word != '';
                });
                words = jQuery.map(words, function (word, i) {
                    return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                });
                if (words.length == 0) {
                    return this;
                };
                var flag = settings.caseSensitive ? "" : "i";
                var patternstr = "(" + words.join("|") + ")";
                if (settings.wordsOnly) {
                    pattern = "\\b" + patternstr + "\\b";
                }
                var result, matches, re = new RegExp(patternstr, flag);
                if (!target) {
                    return;
                }
                matches = target.match(re);
                if (matches) {
                    result = target.replace(re, '<span class="' + scope.matchClass + '">' + matches[0] + '</span>');
                } else {
                    result = target;
                }
                return $sce.trustAsHtml(result);
            }

            function handleRequired(valid) {
                validState = scope.searchStr;
                if (scope.fieldRequired && ctrl) {
                    ctrl.$setValidity(requiredClassName, valid);
                }
            }

            function keyupHandler(event) {
                var which = ie8EventNormalizer(event);
                if (which === KEY_LF || which === KEY_RT) {
                    return;
                }
                if (which === KEY_UP || which === KEY_EN) {
                    event.preventDefault();
                } else if (which === KEY_DW) {
                    event.preventDefault();
                    if (!scope.showDropdown && scope.searchStr && scope.searchStr.length >= minlength) {
                        initResults();
                        scope.searching = true;
                        searchTimerComplete(scope.searchStr);
                    }
                } else if (which === KEY_ES) {
                    clearResults();
                    scope.$apply(function () {
                        inputField.val(scope.searchStr);
                    });
                } else {
                    if (!scope.searchStr || scope.searchStr === '') {
                        scope.showDropdown = false;
                    } else if (scope.searchStr.length >= minlength) {
                        initResults();
                        if (searchTimer) {
                            $timeout.cancel(searchTimer);
                        }
                        scope.searching = true;
                        searchTimer = $timeout(function () {
                            searchTimerComplete(scope.searchStr);
                        }, scope.pause);
                    }
                    if (validState && validState !== scope.searchStr && !scope.clearSelected) {
                        callOrAssign(undefined);
                    }
                }
            }

            function handleOverrideSuggestions(event) {
                if (scope.overrideSuggestions && !(scope.selectedObject && scope.selectedObject.originalObject === scope.searchStr)) {
                    if (event) {
                        event.preventDefault();
                    }
                    setInputString(scope.searchStr);
                }
            }

            function dropdownRowOffsetHeight(row) {
                var css = getComputedStyle(row);
                return row.offsetHeight + parseInt(css.marginTop, 10) + parseInt(css.marginBottom, 10);
            }

            function dropdownHeight() {
                return dd.getBoundingClientRect().top + parseInt(getComputedStyle(dd).maxHeight, 10);
            }

            function dropdownRow() {
                return elem[0].querySelectorAll('.angucomplete-row')[scope.currentIndex];
            }

            function dropdownRowTop() {
                return dropdownRow().getBoundingClientRect().top - (dd.getBoundingClientRect().top + parseInt(getComputedStyle(dd).paddingTop, 10));
            }

            function dropdownScrollTopTo(offset) {
                dd.scrollTop = dd.scrollTop + offset;
            }

            function updateInputField() {
                var current = scope.results[scope.currentIndex];
                if (scope.matchClass) {
                    inputField.val(extractTitle(current.originalObject));
                } else {
                    inputField.val(current.title);
                }
            }

            function keydownHandler(event) {
                var which = ie8EventNormalizer(event);
                var row = null;
                var rowTop = null;
                if (which === KEY_EN && scope.results) {
                    if (scope.currentIndex >= 0 && scope.currentIndex < scope.results.length) {
                        event.preventDefault();
                        scope.selectResult(scope.results[scope.currentIndex]);
                    } else {
                        handleOverrideSuggestions(event);
                        clearResults();
                    }
                    scope.$apply();
                } else if (which === KEY_DW && scope.results) {
                    event.preventDefault();
                    if ((scope.currentIndex + 1) < scope.results.length && scope.showDropdown) {
                        scope.$apply(function () {
                            scope.currentIndex++;
                            updateInputField();
                        });
                        if (isScrollOn) {
                            row = dropdownRow();
                            if (dropdownHeight() < row.getBoundingClientRect().bottom) {
                                dropdownScrollTopTo(dropdownRowOffsetHeight(row));
                            }
                        }
                    }
                } else if (which === KEY_UP && scope.results) {
                    event.preventDefault();
                    if (scope.currentIndex >= 1) {
                        scope.$apply(function () {
                            scope.currentIndex--;
                            updateInputField();
                        });
                        if (isScrollOn) {
                            rowTop = dropdownRowTop();
                            if (rowTop < 0) {
                                dropdownScrollTopTo(rowTop - 1);
                            }
                        }
                    } else if (scope.currentIndex === 0) {
                        scope.$apply(function () {
                            scope.currentIndex = -1;
                            inputField.val(scope.searchStr);
                        });
                    }
                } else if (which === KEY_TAB) {
                    if (scope.results && scope.results.length > 0 && scope.showDropdown) {
                        if (scope.currentIndex === -1 && scope.overrideSuggestions) {
                            handleOverrideSuggestions();
                        } else {
                            if (scope.currentIndex === -1) {
                                scope.currentIndex = 0;
                            }
                            scope.selectResult(scope.results[scope.currentIndex]);
                            scope.$digest();
                        }
                    } else {
                        if (scope.searchStr && scope.searchStr.length > 0) {
                            handleOverrideSuggestions();
                        }
                    }
                }
            }

            function cancelHttpRequest() {
                if (httpCanceller) {
                    httpCanceller.resolve();
                }
            }

            function getRemoteResults(str) {
                $http.post('api/Planning/QuickLuceneSearch', {
                    Text: str,
                    Searchtype: scope.searchtype
                }).success(function (response) {
                    scope.searching = false;
                    if (response.Response != null) {
                        if (response.Response.items.length > 0) {
                            scope.showDropdown = true;
                            processResults(extractValue(responseFormatter(response.Response), scope.remoteUrlDataField), str);
                        } else scope.showDropdown = false;
                    } else scope.showDropdown = false;
                }).error(function (response) { })
            }

            function clearResults() {
                scope.showDropdown = false;
                scope.results = [];
                if (dd) {
                    dd.scrollTop = 0;
                }
            }

            function initResults() {
                scope.currentIndex = -1;
                scope.results = [];
            }

            function getLocalResults(str) {
                var i, match, s, value, searchFields = scope.searchFields.split(','),
					matches = [];
                scope.showDropdown = true;
                for (i = 0; i < scope.localData.length; i++) {
                    match = false;
                    for (s = 0; s < searchFields.length; s++) {
                        value = extractValue(scope.localData[i], searchFields[s]) || '';
                        match = match || (value.toLowerCase().indexOf(str.toLowerCase()) >= 0);
                    }
                    if (match) {
                        matches[matches.length] = scope.localData[i];
                    }
                }
                scope.searching = false;
                processResults(matches, str);
            }

            function checkExactMatch(result, obj, str) {
                for (var key in obj) {
                    if (obj[key].toLowerCase() === str.toLowerCase()) {
                        scope.selectResult(result);
                        return;
                    }
                }
            }

            function searchTimerComplete(str) {
                if (str.length < minlength) {
                    return;
                }
                if (scope.localData) {
                    scope.$apply(function () {
                        getLocalResults(str);
                    });
                } else {
                    getRemoteResults(str);
                }
            }

            function processResults(responseData, str) {
                var i, description, image, text, formattedText, formattedDesc;
                if (responseData && responseData.length > 0) {
                    scope.results = [];
                    for (i = 0; i < responseData.length; i++) {
                        if (scope.titleField && scope.titleField !== '') {
                            text = formattedText = extractTitle(responseData[i]);
                        }
                        description = '';
                        if (scope.descriptionField) {
                            description = formattedDesc = extractValue(responseData[i], scope.descriptionField);
                        }
                        image = '';
                        if (scope.imageField) {
                            image = extractValue(responseData[i], scope.imageField);
                        }
                        if (scope.matchClass) {
                            formattedText = findMatchString(text, str);
                            formattedDesc = findMatchString(description, str);
                        }
                        scope.results[scope.results.length] = {
                            title: formattedText,
                            description: formattedDesc,
                            image: image,
                            originalObject: responseData[i]
                        };
                        if (scope.autoMatch) {
                            checkExactMatch(scope.results[scope.results.length - 1], {
                                title: text,
                                desc: description || ''
                            }, scope.searchStr);
                        }
                    }
                } else {
                    scope.results = [];
                }
            }
            scope.onFocusHandler = function () {
                if (scope.focusIn) {
                    scope.focusIn();
                }
            };
            scope.hideResults = function (event) {
                if (mousedownOn === scope.id + '_dropdown') {
                    mousedownOn = null;
                } else {
                    hideTimer = $timeout(function () {
                        clearResults();
                        scope.$apply(function () {
                            if (scope.searchStr && scope.searchStr.length > 0) {
                                inputField.val(scope.searchStr);
                            }
                        });
                    }, BLUR_TIMEOUT);
                    cancelHttpRequest();
                    if (scope.focusOut) {
                        scope.focusOut();
                    }
                }
            };
            scope.resetHideResults = function () {
                if (hideTimer) {
                    $timeout.cancel(hideTimer);
                }
            };
            scope.hoverRow = function (index) {
                scope.currentIndex = index;
            };
            scope.selectResult = function (result) {
                if (scope.matchClass) {
                    result.title = extractTitle(result.originalObject);
                    result.description = extractValue(result.originalObject, scope.descriptionField);
                }
                if (scope.clearSelected) {
                    scope.searchStr = null;
                } else {
                    scope.searchStr = result.title;
                }
                callOrAssign(result);
                clearResults();
                scope.clickhandler({
                    message: scope.searchStr
                });
                $timeout(function () {
                    if (scope.searchStr != "") {
                        var lastRoute = $location.path();
                        if (lastRoute.contains("/mui/customsearchresult")) if (lastRoute == "/mui/customsearchresults") $location.path("/mui/customsearchresult");
                        else $location.path("/mui/customsearchresults");
                        else $location.path("/mui/customsearchresult");
                    }
                }, 100);
            };
            scope.inputChangeHandler = function (str) {
                if (str.length < minlength) {
                    clearResults();
                }
                if (scope.inputChanged) {
                    str = scope.inputChanged(str);
                }
                return scope.onSelect({
                    text: str
                });
            };
            if (scope.fieldRequiredClass && scope.fieldRequiredClass !== '') {
                requiredClassName = scope.fieldRequiredClass;
            }
            if (scope.minlength && scope.minlength !== '') {
                minlength = scope.minlength;
            }
            if (!scope.pause) {
                scope.pause = PAUSE;
            }
            if (!scope.clearSelected) {
                scope.clearSelected = false;
            }
            if (!scope.overrideSuggestions) {
                scope.overrideSuggestions = false;
            }
            if (scope.fieldRequired && ctrl) {
                if (scope.initialValue) {
                    handleRequired(true);
                } else {
                    handleRequired(false);
                }
            }
            scope.textSearching = attrs.textSearching ? attrs.textSearching : TEXT_SEARCHING;
            scope.textNoResults = attrs.textNoResults ? attrs.textNoResults : TEXT_NORESULTS;
            inputField.on('keydown', keydownHandler);
            inputField.on('keyup', keyupHandler);
            responseFormatter = callFunctionOrIdentity('remoteUrlResponseFormatter');
            scope.$on('$destroy', function () {
                handleRequired(true);
            });
            $timeout(function () {
                var css = getComputedStyle(dd);
                isScrollOn = css.maxHeight && css.overflowY === 'auto';
            });
        }
    };
}]);