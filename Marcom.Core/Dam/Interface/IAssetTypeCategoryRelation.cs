﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetTypeCategoryRelation
    {
        int Id { get; set; }
        int AssetTypeId { get; set; }
        int CategoryId { get; set; }
        bool IsDeleted { get; set; }
    }
}
