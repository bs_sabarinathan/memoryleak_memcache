﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    /// <summary>
    /// TaskFileDao object for table 'TM_File'.
    /// </summary>

    public partial class TaskFileDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected int _versionno;
        protected string _mimetype;
        protected string _extension;
        protected long _size;
        protected int _ownerid;
        protected DateTimeOffset _createdon;
        protected string _checksum;
        protected int _moduleid;
        protected int _entityid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected Guid _fileguid;
        protected string _description;
        #endregion

        #region Constructors
        public TaskFileDao() { }

        public TaskFileDao(string pName, int pVersionNo, string pMimeType, string pExtension, long pSize, int pOwnerid, DateTimeOffset pCreatedOn, string pChecksum, int pModuleid, int pEntityid, Guid pFileguid, string pDescription)
        {
            this._name = pName;
            this._versionno = pVersionNo;
            this._mimetype = pMimeType;
            this._extension = pExtension;
            this._size = pSize;
            this._ownerid = pOwnerid;
            this._createdon = pCreatedOn;
            this._checksum = pChecksum;
            this._moduleid = pModuleid;
            this._entityid = pEntityid;
            this._fileguid = pFileguid;
            this._description = pDescription;
        }

        public TaskFileDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual int VersionNo
        {
            get { return _versionno; }
            set { _bIsChanged |= (_versionno != value); _versionno = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set { _bIsChanged |= (_description != value); _description = value; }

        }



        public virtual string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 250 characters");
                _bIsChanged |= (_mimetype != value);
                _mimetype = value;
            }

        }

        public virtual string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 50 characters");
                _bIsChanged |= (_extension != value);
                _extension = value;
            }

        }


        public virtual long Size
        {
            get { return _size; }
            set { _bIsChanged |= (_size != value); _size = value; }

        }

        public virtual int Ownerid
        {
            get { return _ownerid; }
            set { _bIsChanged |= (_ownerid != value); _ownerid = value; }

        }

        public virtual DateTimeOffset CreatedOn
        {
            get { return _createdon; }
            set { _bIsChanged |= (_createdon != value); _createdon = value; }

        }

        public virtual string Checksum
        {
            get { return _checksum; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Checksum", "Checksum value, cannot contain more than 50 characters");
                _bIsChanged |= (_checksum != value);
                _checksum = value;
            }

        }

        public virtual int Moduleid
        {
            get { return _moduleid; }
            set { _bIsChanged |= (_moduleid != value); _moduleid = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual Guid Fileguid
        {
            get { return _fileguid; }
            set { _bIsChanged |= (_fileguid != value); _fileguid = value; }

        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TaskFileDao = "TaskFileDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string VersionNo = "VersionNo";
            public const string MimeType = "MimeType";
            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string Ownerid = "Ownerid";
            public const string CreatedOn = "CreatedOn";
            public const string Checksum = "Checksum";
            public const string Moduleid = "Moduleid";
            public const string Entityid = "Entityid";
            public const string Fileguid = "Fileguid";
            public const string Description = "Description";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TaskFileDao = "TaskFileDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string VersionNo = "VersionNo";
            public const string MimeType = "MimeType";
            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string Ownerid = "Ownerid";
            public const string CreatedOn = "CreatedOn";
            public const string Checksum = "Checksum";
            public const string Moduleid = "Moduleid";
            public const string Entityid = "Entityid";
            public const string Fileguid = "Fileguid";
            public const string Description = "Description";

        }

        #endregion


    }
}
