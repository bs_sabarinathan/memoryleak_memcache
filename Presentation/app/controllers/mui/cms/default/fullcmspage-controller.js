﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope, $location, FullpagecmsService, $timeout, $compile, $sce, $stateParams, $modal, $translate) {
        $scope.ISFullPages = true;
        var cleanUpFunc = $scope.$on('navigationClick', function (event, FeatureID) {
            cleanUpFunc();
            if (FeatureID == 33) {
                GetAllCmsEntitiesByNavID();
            }
        });
        $scope.ColorCode = "";
        $scope.ShortDescription = "";
        $scope.IsEditFeatureEnabled = false;
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        $scope.ShowFullPageAdd = false;
        $scope.ParentID = 0;
        $scope.UniqueKey = "";
        $scope.Level = 0;
        $scope.ID = 0;
        $scope.Index = -1;
        FullpagecmsService.GetIsEditFeatureEnabled().then(function (data) {
            if (data.Response != null && data.StatusCode == 200) {
                $scope.IsEditFeatureEnabled = data.Response;
            }
        });
        $scope.OpenCmsEntityCreationPopUp = function () {
            $scope.ISFullPages = true;
            $scope.ParentID = 0;
            $scope.UniqueKey = "";
            $scope.Level = 0;
            $scope.ID = 0;
            $scope.Index = -1;
            $("#cmsEntityModel").trigger("onCMSEntityCreation")
        }
        $scope.DeleteCmsEntity = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                if (result) {
                    if ($scope.ID != 0) {
                        FullpagecmsService.DeleteCmsEntity($scope.ID).then(function (data) {
                            if (data.Response != false) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.CmsItemCollections.splice($scope.Index, 1);
                                $scope.Index = -1;
                            }
                        });
                    }
                }
            });
        }
        GetAllCmsEntitiesByNavID();

        function GetAllCmsEntitiesByNavID() {
            $scope.CmsItemCollections = {};
            FullpagecmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, 0, 1000).then(function (data) {
                if (data.Response != null && data.Response.length > 0) {
                    $scope.CmsItemCollections = data.Response;
                    if ($stateParams.ID == undefined) {
                        $stateParams.ID = $scope.CmsItemCollections[0].ID;
                    }
                    $location.path('/mui/fullpagecms/list/' + $stateParams.ID + '/entitycontent');
                    GetCmsRevisedContent($stateParams.ID);
                    $("#cmsentity_" + $stateParams.ID).addClass("active");
                } else {
                    $scope.ShowFullPageAdd = true;
                }
            });
        }

        function GetCmsRevisedContent(ID) {
            FullpagecmsService.GetRevisedContentByFeature(ID).then(function (data) {
                if (data.Response != null) {
                    $timeout(function () {
                        $('#htmlEntityContent').html(data.Response.Content);
                        $('.btnbrowse').css("display", 'none');
                    }, 200);
                }
            });
        }
        $("#fullpagecontent").on("ReloadFullPage", function (event) {
            $scope.ShowFullPageAdd = false;
            GetAllCmsEntitiesByNavID();
        });
       
        $scope.rootValues = [];
        FullpagecmsService.GetEntityType(6).then(function (entitytypes) {
            $scope.rootValues = entitytypes.Response;
        });
        $scope.OpenPopupForPageCreation = function (ID, Caption) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/cms/createcmsentity.html',
                controller: "mui.cms.createcmsentityCtrl",
                resolve: {
                    params: function () {
                        return {
                            ID: ID,
                            Caption: Caption
                        };
                    }
                },
                scope: $scope,
                windowClass: 'addCMSEntityModel popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.fullcmspageCtrl']"));
        });
    }
    app.controller('mui.cms.default.fullcmspageCtrl', ['$scope', '$location', 'FullpagecmsService', '$timeout', '$compile', '$sce', '$stateParams', '$modal', '$translate', muiplanningtooldefaultCtrl]);
    function FullpagecmsService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetIsEditFeatureEnabled: GetIsEditFeatureEnabled,
            DeleteCmsEntity: DeleteCmsEntity,
            GetAllCmsEntitiesByNavID: GetAllCmsEntitiesByNavID,
            GetRevisedContentByFeature: GetRevisedContentByFeature,
            GetEntityType: GetEntityType
        });
        function GetIsEditFeatureEnabled() { var request = $http({ method: "get", url: "api/cms/GetIsEditFeatureEnabled/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteCmsEntity(ID) { var request = $http({ method: "delete", url: "api/cms/DeleteCmsEntity/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllCmsEntitiesByNavID(NavID, StartpageNo, MaxPageNo) { var request = $http({ method: "get", url: "api/cms/GetAllCmsEntitiesByNavID/" + NavID + "/" + StartpageNo + "/" + MaxPageNo, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetRevisedContentByFeature(EntityID) { var request = $http({ method: "get", url: "api/cms/GetRevisedContentByFeature/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityType(ModuleID) { var request = $http({ method: "get", url: "api/Metadata/GetEntityType/" + ModuleID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("FullpagecmsService", ['$http', '$q', FullpagecmsService]);
})(angular, app);