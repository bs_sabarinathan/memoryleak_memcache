using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// CalenderEntityValue object for table 'PM_CalenderEntityValue'.
	/// </summary>
	
	internal class CalenderEntityValue : ICalenderEntityValue
	{
		#region Member Variables

        protected int _id;
		protected int _calenderid;
		protected int _entityid;
		#endregion
		
		#region Constructors
		public CalenderEntityValue() {}

        public CalenderEntityValue(int pId, int pCalenderid, int pEntityid)
		{
            this._id = pId;
            this._calenderid = pCalenderid; 
			this._entityid = pEntityid; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int Calenderid
		{
			get { return _calenderid; }
            set { _calenderid = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			CalenderEntityValue castObj = null;
			try
			{
                castObj = (CalenderEntityValue)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _calenderid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
