﻿(function (ng, app) {
    "use strict";

    function muiadminganttviewheaderbarCtrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
            $scope.GanttEndDate1 = false;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.GanttEndDate1 = true;
        };
        $scope.CurrentGanttHeaderID = 0;
        $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
        $scope.ScreenText = $translate.instant('LanguageContents.Res_5049.Caption');
        $scope.ColorCodeGlobalObj = {};
        $scope.ColorCodeGlobalObj.colorcode = 'ffffff';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { }
        GetAllGanttHeaderBar();
        $scope.GanttHeaderList = null;

        function GetAllGanttHeaderBar() {
            AdminService.GetAllGanttHeaderBar().then(function (res) {
                if (res.Response != null) {
                    $scope.GanttHeaderList = res.Response;
                }
            });
        }
        $scope.addGanttHeader = function (ID) {
            if (GanttHeaderBarValidation(ID)) {
                var res = {};
                res.Id = ID;
                res.Name = $scope.GanttHeadername;
                res.Description = $scope.GanttHeadergdesc;
                if (ID == 0) {
                    res.Startdate = ConvertDateToString($scope.GanttStartDate);
                    res.EndDate = ConvertDateToString($scope.GanttEndDate);
                } else {
                    if (typeof ($scope.GanttStartDate) != 'string') {
                        res.Startdate = ConvertDateToString($scope.GanttStartDate);
                    }
                    else { res.Startdate = ConvertDateFromStringToStringByFormat($scope.GanttStartDate); }
                    if (typeof ($scope.GanttEndDate) != 'string') {
                        res.EndDate = ConvertDateToString($scope.GanttEndDate);
                    } else { res.EndDate = ConvertDateFromStringToStringByFormat($scope.GanttEndDate); }
                }
                res.ColorCode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
                AdminService.InsertUpdateGanttHeaderBar(res).then(function (result) {
                    if (result.Response != 0) {
                        if (ID == 0) {
                            $scope.GanttHeaderList.push({
                                Id: result.Response,
                                Name: $scope.GanttHeadername,
                                Description: $scope.GanttHeadergdesc,
                                Startdate: ConvertDateToString($scope.GanttStartDate),
                                EndDate: ConvertDateToString($scope.GanttEndDate),
                                ColorCode: $scope.ColorCodeGlobalObj.colorcode.replace("#", "")
                            });
                        } else {
                            var currentobj = $.grep($scope.GanttHeaderList, function (item, i) {
                                return item.Id == ID
                            });
                            currentobj[0].Name = $scope.GanttHeadername;
                            currentobj[0].Description = $scope.GanttHeadergdesc;
                            currentobj[0].Startdate = dateFormat($scope.GanttStartDate, GlobalUserDateFormat);
                            currentobj[0].EndDate = dateFormat($scope.GanttEndDate, GlobalUserDateFormat);
                            currentobj[0].ColorCode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
                            GetAllGanttHeaderBar();
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4199.Caption'));
                        $("#GanttHeadermodal").modal('hide');
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#GanttHeadermodal").modal('hide');
                    }
                });
            }
        }
        $scope.GetGanttHeaderByIndex = function (Index) {
            $scope.buttonText = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.ScreenText = $translate.instant('LanguageContents.Res_5050.Caption');
            $scope.CurrentGanttHeaderID = $scope.GanttHeaderList[Index].Id;
            $scope.GanttHeadername = $scope.GanttHeaderList[Index].Name;
            $scope.GanttHeadergdesc = $scope.GanttHeaderList[Index].Description;
            $scope.GanttStartDate = dateFormat($scope.GanttHeaderList[Index].Startdate);
            $scope.GanttEndDate = dateFormat($scope.GanttHeaderList[Index].EndDate);
            $scope.ColorCodeGlobalObj.colorcode = $scope.GanttHeaderList[Index].ColorCode;
            $("#GanttHeadermodal").modal('show');
        }
        $scope.DeleteGanttHeaderByID = function (Index, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2006.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteGanttHeaderBar(ID).then(function (res) {
                            if (res.Response != false) {
                                $scope.GanttHeaderList.splice(Index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.CustomDateFormat = function (CurrentDate) {
            return dateFormat(CurrentDate, $scope.format);
        }
        $scope.OpenGanttHeaderBar = function () {
            $('#GanttHeadermodal').modal('show');
            $scope.ClearAll();
            $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
            $scope.ScreenText = $translate.instant('LanguageContents.Res_5049.Caption');
           
        }
        $scope.ClearAll = function () {
            $scope.CurrentGanttHeaderID = 0;
            $scope.GanttHeadername = '';
            $scope.GanttHeadergdesc = '';
            $scope.GanttStartDate = '';
            $scope.GanttEndDate = '';
            $scope.ColorCodeGlobalObj.colorcode = 'ffffff';
        }

        function GanttHeaderBarValidation(ID) {
            if ($scope.GanttHeadername == undefined || $scope.GanttHeadername == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4576.Caption'));
                return false;
            }
            if ($scope.GanttStartDate == undefined || $scope.GanttStartDate == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4627.Caption'));
                return false;
            }
            if ($scope.GanttEndDate == undefined || $scope.GanttEndDate == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4620.Caption'));
                return false;
            }
            if ($scope.GanttEndDate < $scope.GanttStartDate) {
                bootbox.alert($translate.instant('LanguageContents.Res_4238.Caption'));
                return false;
            }
            if (IsDateExist(ID) == false) {
                bootbox.alert($translate.instant('LanguageContents.Res_4852.Caption'));
                return false;
            }
            return true;
        }

        function IsDateExist(ID) {
            for (var i = 0; i < $scope.GanttHeaderList.length; i++) {
                if (ID == 0 || $scope.GanttHeaderList[i].Id != ID) {
                    var strtdate = formatteddateFormat($scope.GanttHeaderList[i].Startdate, $scope.DefaultSettings.DateFormat);
                    var enddate = formatteddateFormat($scope.GanttHeaderList[i].EndDate, $scope.DefaultSettings.DateFormat);
                    if (strtdate >= $scope.GanttStartDate) {
                        for (var j = 0; j < $scope.GanttHeaderList.length; j++) {
                            if (ID == 0 || $scope.GanttHeaderList[j].Id != ID) {
                                var Internalstrtdate = formatteddateFormat($scope.GanttHeaderList[j].Startdate, $scope.DefaultSettings.DateFormat);
                                var Internalenddate = formatteddateFormat($scope.GanttHeaderList[j].EndDate, $scope.DefaultSettings.DateFormat);
                                if ($scope.GanttEndDate >= Internalstrtdate && $scope.GanttStartDate <= Internalenddate) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
    app.controller("mui.admin.ganttviewheaderbarCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminganttviewheaderbarCtrl]);
})(angular, app);