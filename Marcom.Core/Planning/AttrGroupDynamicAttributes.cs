﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class AttrGroupDynamicAttributes : IAttrGroupDynamicAttributes
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        // protected List<KeyValuePair<string, string>> _attributes;
        protected IDictionary _attributes;

        #endregion

        #region Constructors
        public AttrGroupDynamicAttributes() { }

        public AttrGroupDynamicAttributes(int pId, int pEntityId ,IDictionary pAttributes)
        {
            this._id = pId;
            this._entityid = pEntityId;
            this._attributes = pAttributes;
        }

        public AttrGroupDynamicAttributes(int pId)
        {
            this._id = pId;
        }



        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }
        }
        public IDictionary Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttrGroupDynamicAttributes castObj = null;
            try
            {
                castObj = (AttrGroupDynamicAttributes)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


        public int _Id { get; set; }
    }
}
