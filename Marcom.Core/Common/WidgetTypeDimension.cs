﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;


namespace BrandSystems.Marcom.Core.Common
{
    /// <summary>
    /// WidgetTypeDimension object for table 'CM_WidgetTypeDimension'.
    /// </summary>
   internal  class WidgetTypeDimension :IWidgetTypeDimension
    {
        #region Member Variables

		protected int _id;
		protected int _WidgetTypeID;
		protected string _DimensionName;
	
		#endregion
		
		#region Constructors
		public WidgetTypeDimension() {}
			
		public WidgetTypeDimension(int pWidgetTypeID, string pDimensionName)
		{
			this._WidgetTypeID = pWidgetTypeID; 
			this._DimensionName =pDimensionName; 
			
		}

        public WidgetTypeDimension(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public int WidgetTypeID
		{
            get { return _WidgetTypeID; }
            set { _WidgetTypeID = value; }
			
		}


        public string DimensionName
		{
            get { return _DimensionName; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("DimensionName", "DimensionName value, cannot contain more than 100 characters");
              _DimensionName = value; 
			}
			
		}
		
		
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            WidgetTypeDimension castObj = null;
			try
			{
                castObj = (WidgetTypeDimension)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
    }
}
