using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.User.Interface;
using System.Security.Cryptography;

namespace BrandSystems.Marcom.Core.User
{
                          
	/// <summary>
	/// User object for table 'UM_User'.
	/// </summary>
	public class PendingUser : IPendingUser,ICloneable
	{
		#region Member Variables

		protected int _id;
		protected string _firstname;
		protected string _lastname;
		protected string _username;
		protected byte[] _password;
        protected string _saltpassword;
		protected string _email;
		
		protected string _language;
		protected string _timezone;
        protected string _companyname;
        protected string _partners;
        protected string _title;
        protected string _department;

        protected bool _gender;
        protected DateTime _dob;
        protected string _phone;
        protected string _address;
        protected string _city;
        protected int _zipcode;
        protected string _website;       
        protected string _activationstatus;

		#endregion
		
		#region Constructors
		public PendingUser() {}

        public PendingUser(string pFirstName, string pLastName, string pUserName, byte[] pPassword, string pEmail, string pLanguage, string pTimeZone, string saltPassword, bool pGender, DateTime pDOB, string pPhone, string pAddress, string pCity, int pZipcode, string pWebsite, string pCompanyName, string pPartners,string pTitle, string pDepartment, string pActivationStatus)
		{
			this._firstname = pFirstName; 
			this._lastname = pLastName; 
			this._username = pUserName; 
			this._password = pPassword; 
			this._email = pEmail; 
			
			this._language = pLanguage; 
			this._timezone = pTimeZone;

            this._companyname = pCompanyName;
            this._partners = pPartners;
            this._title = pTitle;
            this._department = pDepartment;
            this._saltpassword = saltPassword;
            
            this._gender = pGender;
            this._dob = pDOB;
            this._phone = pPhone;
            this._address = pAddress;
            this._city = pCity;
            this._zipcode = pZipcode;
            this._website = pWebsite;           
            this._activationstatus = pActivationStatus;
		}

        public PendingUser(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string FirstName
		{
			get { return _firstname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("FirstName", "FirstName value, cannot contain more than 50 characters");
			  _firstname = value; 
			}
			
		}
		
		public string LastName
		{
			get { return _lastname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("LastName", "LastName value, cannot contain more than 50 characters");
			  _lastname = value; 
			}
			
		}
		
		public string UserName
		{
			get { return _username; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("UserName", "UserName value, cannot contain more than 250 characters");
			  _username = value; 
			}
			
		}

        public byte[] Password
		{
			get { return _password; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
              _password = value; 
			}
			
		}

        public string SaltPassword
        {
            get { return _saltpassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");

                _saltpassword = value;
            }

        }
		
		public string Email
		{
			get { return _email; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 250 characters");
			  _email = value; 
			}
			
		}
		
		
		public string Language
		{
			get { return _language; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Language", "Language value, cannot contain more than 50 characters");
			  _language = value; 
			}
			
		}
		
		public string TimeZone
		{
			get { return _timezone; }
			set 
			{
			  if (value != null && value.Length > 1000)
			    throw new ArgumentOutOfRangeException("TimeZone", "TimeZone value, cannot contain more than 50 characters");
			  _timezone = value; 
			}
			
		}
		
		
      

        public bool Gender
        {
            get { return _gender; }
            set { _gender = value; }

        }

        public DateTime DOB
        {
            get { return _dob; }
            set
            {
                _dob = value;
            }

        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }

        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Address", "Address value, cannot contain more than 250 characters");
                _address = value;
            }

        }

        public string City
        {
            get { return _city; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("City", "City value, cannot contain more than 50 characters");
                _city = value;
            }

        }

        public int ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        public string Website
        {
            get { return _website; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Website", "Website value, cannot contain more than 50 characters");
                _website = value;
            }

        }

     
        public virtual string ActivationStatus
        {
            get { return _activationstatus; }
            set { _activationstatus = value; }


        }
        public virtual string CompanyName
        {
            get { return _companyname; }
            set {_companyname = value; }

        }
        public virtual string Partners
        {
            get { return _partners; }
            set { _partners = value;}

        }
        public virtual string Title
        {
            get { return _title; }
            set{ _title = value;}

        }
        public virtual string Department
        {
            get { return _department; }
            set { _department = value; }

        } 

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			User castObj = null;
			try
			{
				castObj = (User)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}

      
		#endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
		
	}
	
}
