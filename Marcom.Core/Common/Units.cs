﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;


namespace BrandSystems.Marcom.Core.Common
{

    /// <summary>
    ///  object for table 'PM_Objective_Unit'.
    /// </summary>
   internal class Units : IUnits
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        
       
        #endregion
        
        #region Constructors
        public Units() { }

        public Units(int pid, string pcaption)
		{
            this._id = pid;
            this._caption = pcaption;

        }
       #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Caption
        {
            get { return _caption; }
            set { _caption = value; }
        }

        
        #endregion 

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
        if (this == obj) return true;
        Units UnitsObj = null;
        try
        {
        UnitsObj = (Units)obj;
        }
        catch (Exception) { return false; }
        return (UnitsObj != null) &&
            (this._id == UnitsObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


        int hash = 57;
        hash = 27 * hash * _id.GetHashCode();
        return hash;
        }
        #endregion


    }
}
