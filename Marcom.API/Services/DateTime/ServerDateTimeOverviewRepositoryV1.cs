﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Marcom.API.Authorization;
using Marcom.API.Models;

namespace Marcom.API.Services
{
    public class ServerDateTimeOverviewRepositoryV1
    {

        public ServerDateTimeModelV1 GetServerDateTime()
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            ServerDateTimeModelV1 servertime = new ServerDateTimeModelV1();

            servertime.Formatted = new Formatted {
                 
                Weekday = DateTime.Now.DayOfWeek.ToString(),
                Month = DateTime.Now.Month.ToString(),
                Time = DateTime.Now.TimeOfDay.ToString(),
                LongHand=DateTime.Now.ToLongDateString(),
                LongHandTime=DateTime.Now.ToLongTimeString(),
                Shorttime = DateTime.Now.ToShortTimeString(),
                Slim = DateTime.Now.ToShortDateString(),
                FullSlim = DateTime.Now.ToShortTimeString()
            
            };
            servertime.Time = new TimeFormat
            {
                DaysInMonth=DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month),
                Date = DateTime.Now.Day,
                Hour = DateTime.Now.Hour,
                MilliSecond = DateTime.Now.Millisecond,
                Minute = DateTime.Now.Minute,
                Month = DateTime.Now.Month,
                Second = DateTime.Now.Second,
                Year = DateTime.Now.Year,
                Day = (int)(DateTime.Now.DayOfWeek),
                Week = Convert.ToInt32(Math.Floor((double)DateTime.Now.Day / 7)) + 1,
                Zone = TimeZoneInfo.Local.DisplayName

            
            };

            return servertime;
        }

    }
}