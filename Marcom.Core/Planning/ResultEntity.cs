﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.IO;
using System.Collections;

namespace BrandSystems.Marcom.Core.Planning
{

    public class ResultEntity
    {

        private string intID;
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string ID
        {
            get { return intID; }
            set { intID = value; }
        }

        private string strFileGUID;
        [DataMember(IsRequired = false, Order = 2), XmlAttribute()]
        public string FileGUID
        {
            get { return strFileGUID; }
            set { strFileGUID = value; }
        }

        private int intTypeID;
        [DataMember(IsRequired = false, Order = 3), XmlAttribute()]
        public int TypeID
        {
            get { return intTypeID; }
            set { intTypeID = value; }
        }

        private int intTypeCategoryID;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public int TypeCategoryID
        {
            get { return intTypeCategoryID; }
            set { intTypeCategoryID = value; }
        }

        private string strName;
        [DataMember(IsRequired = false, Order = 5), XmlAttribute()]
        public string Name
        {
            get { return strName; }
            set { strName = value; }
        }

        private string strPath;
        [DataMember(IsRequired = false, Order = 6), XmlAttribute()]
        public string Path
        {
            get { return strPath; }
            set { strPath = value; }
        }

        private string strDescription;
        [DataMember(IsRequired = false, Order = 7), XmlAttribute()]
        public string Description
        {
            get { return strDescription; }
            set { strDescription = value; }
        }

        private string strThumbnailUrl;
        [DataMember(IsRequired = false, Order = 8), XmlAttribute()]
        public string ThumbnailUrl
        {
            get { return strThumbnailUrl; }
            set { strThumbnailUrl = value; }
        }

        private ICollection<KeyValuePair<string, string>> listMetadata = new Dictionary<string, string>();
        [DataMember(IsRequired = false, Order = 9)]
        public ICollection<KeyValuePair<string, string>> Metadata
        {
            get { return listMetadata; }
            set { listMetadata = value; }
        }

        private string shortdescription;
        [DataMember(IsRequired = false, Order = 10)]
        public string ShortDescription
        {
            get { return shortdescription; }
            set { shortdescription = value; }
        }

        private string colorcode;
        [DataMember(IsRequired = false, Order = 10)]
        public string ColorCode
        {
            get { return colorcode; }
            set { colorcode = value; }
        }

        private string caption;
        [DataMember(IsRequired = false, Order = 10)]
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }

        private int parentid;
        [DataMember(IsRequired = false, Order = 10)]
        public int ParentID
        {
            get { return parentid; }
            set { parentid = value; }
        }

        private string status;
        [DataMember(IsRequired = false, Order = 11)]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private int intLevel;
        [DataMember(IsRequired = false, Order = 12), XmlAttribute()]
        public int Level
        {
            get { return intLevel; }
            set { intLevel = value; }
        }

        private string intMemberAccess;
        [DataMember(IsRequired = false, Order = 13), XmlAttribute()]
        public string MemberAccess
        {
            get { return intMemberAccess; }
            set { intMemberAccess = value; }
        }

        private string stringExtension;
        [DataMember(IsRequired = false, Order = 14), XmlAttribute()]
        public string Extension
        {
            get { return stringExtension; }
            set { stringExtension = value; }
        }

        public string Searchentitytype { get; set; }

        public IList pathInfo { get; set; }

        public string AssetDetails { get; set; }

        public string Entitytypedetails { get; set; }

        public string AttributeDetails { get; set; }

        public string UniqueKey { get; set; }

        public string SearchType { get; set; }

        public string PathDetails { get; set; }

        public string Users { get; set; }

        public string Tagwords { get; set; }

        public string AssetValueDetails { get; set; }

        public int AssetId { get; set; }

        public List<SearchAttributeDetails> AttributeValues { get; set; }


    }

    public class SearchAttributeDetails
    {
        public string AttributeId { get; set; }

        public string AttributeCaption { get; set; }

        public string MatchedValues { get; set; }
    }

    public class AutoTaskCls
    {
        public int BriefID { get; set; }
        public int BriefOwnerUserID { get; set; }
        public string TaskName { get; set; }
        public int CheckUserAttribute { get; set; }
        public int AssigneeGlobalRoleID { get; set; }
    }
}
