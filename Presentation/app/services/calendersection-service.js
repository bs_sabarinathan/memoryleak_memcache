﻿app.service('CalendersectionService', function ($http, $q) {
    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ GetPath:GetPath,GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation, GetCustomTabUrlTabsByTypeID: GetCustomTabUrlTabsByTypeID, GetCustomEntityTabsByTypeID: GetCustomEntityTabsByTypeID, GetLockStatus: GetLockStatus, GetDataForBreadcrumLoadWithLocalWorkSpacePath: GetDataForBreadcrumLoadWithLocalWorkSpacePath, GetDataForBreadcrumLoadWithPath: GetDataForBreadcrumLoadWithPath }); function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetCustomTabUrlTabsByTypeID(tabID, entityID) { var request = $http({ method: "get", url: "api/common/GetCustomTabUrlTabsByTypeID/" + tabID + "/" + entityID, params: { action: "get", }, }); return (request.then(handleSuccess, handleError)); }
    function GetCustomEntityTabsByTypeID(TypeID, EntityTypeID, EntityID, CalID) {
        if (arguments.length == 1) { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID, params: { action: "get", } }); } else { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID + "/" + CalID + "/" + EntityTypeID + "/" + EntityID, params: { action: "get", }, }); }
        return (request.then(handleSuccess, handleError));
    }
    function GetPath(EntityID) { var request = $http({ method: "get", url: "api/Metadata/GetPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetLockStatus(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetLockStatus/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDataForBreadcrumLoadWithLocalWorkSpacePath(EntityID, IsWorkspace) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithLocalWorkSpacePath/" + EntityID + "/" + IsWorkspace, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDataForBreadcrumLoadWithPath(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
        return ($q.reject(response.data.message));
    }
    function handleSuccess(response) { return (response.data); }
});