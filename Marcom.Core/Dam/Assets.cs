﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.Core.DAM
{

    /// <summary>
    /// Attachments object for table 'DM_Assets'.
    /// </summary>

    public class Assets : IAssets
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _folderid;
        protected string _name;
        protected int _activefileid;
        protected int _assettypeid;
        protected DateTime _createdon;
        protected int _createdby;
        protected int _status;
        protected bool _active;
        protected string _assetaccess;
        protected int _category;
        protected bool _ispublish;
        protected string _url;
        protected int _linkedassetid;
        protected DateTime _UpdatedOn;
        protected string _assetTypeName;
        protected int publisherID;
        protected DateTime? _publishedOn;
        protected DateTime? _ExpiryDate;

        #endregion

        #region Constructors
        public Assets() { }

        public Assets(int pEntityid, string pName, int pActiveFileid, int pAssetTypeid, DateTime pCreatedon, int pFolderid, int pCreatedBy, int pStatus, bool pActive, string pAssetAccess, int pCategory, bool pIsPublish, string Purl, int pLinkedAssetID, DateTime pUpdatedOn, string pAssetTypeName, int pPublisherID, DateTime? pPublishedOn,DateTime? pExpiryDate)
        {
            this._entityid = pEntityid;
            this._name = pName;
            this._activefileid = pActiveFileid;
            this._assettypeid = pAssetTypeid;
            this._createdon = pCreatedon;
            this._folderid = pFolderid;
            this._createdby = pCreatedBy;
            this._status = pStatus;
            this._active = pActive;
            this._assetaccess = pAssetAccess;
            this._category = pCategory;
            this._ispublish = pIsPublish;
            this._url = Purl;
            this._linkedassetid = pLinkedAssetID;
            this._UpdatedOn = pUpdatedOn;
            this._assetTypeName = pAssetTypeName;
            this._publishedOn = pPublishedOn;
            this.publisherID = pPublisherID;
            this._ExpiryDate = pExpiryDate;
        }

        public Assets(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");

                _name = value;
            }

        }
        public int ActiveFileID
        {
            get { return _activefileid; }
            set { _activefileid = value; }
        }

        public int FolderID
        {
            get { return _folderid; }
            set { _folderid = value; }

        }


        public DateTime Createdon
        {
            get { return _createdon; }
            set { _createdon = value; }

        }
        public int CreatedBy
        {
            get { return _createdby; }
            set { _createdby = value; }

        }
        public int AssetTypeid
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }

        }


        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public IList<IAttributeData> AttributeData { get; set; }
        public IList<IDAMFile> Files { get; set; }

        public dynamic MediaGeneratorData { get; set; }

        public virtual string AssetAccess
        {
            get { return _assetaccess; }
            set { _assetaccess = value; }
        }

        public virtual int Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public virtual bool IsPublish
        {
            get { return _ispublish; }
            set { _ispublish = value; }
        }

        public virtual string Url
        {
            get { return _url; }
            set { _url = value; }

        }
        public virtual int LinkedAssetID
        {
            get { return _linkedassetid; }
            set { _linkedassetid = value; }
        }

        public string StrCreatedDate { get; set; }

        public DateTime UpdatedOn
        {
            get { return _UpdatedOn; }
            set { _UpdatedOn = value; }

        }

        public string StrUpdatedOn { get; set; }

        public string assetTypeName
        {
            get { return _assetTypeName; }
            set { _assetTypeName = value; }
        }

        public int PublisherID
        {
            get { return publisherID; }
            set { publisherID = value; }

        }

        public DateTime? PublishedOn
        {
            get { return _publishedOn; }
            set { _publishedOn = value; }

        }
        public DateTime? ExpiryDate
        {
            get { return _ExpiryDate; }
            set { _ExpiryDate = value; }

        }


        public string[] AssetRoles { get; set; }
        public string[] TargetEntityID { get; set; }
        public int ProductionEntityID { get; set; }

        public string folderName { get; set; }

        public IList PublishLinkDetails { get; set; }
        public string StrPublishedOn { get; set; }
        public string StrExpiryDate { get; set; }


        public string searchtype { get; set; }
        #endregion
    }

    public class DamAttributeList
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public bool IsSpecial { get; set; }
        public int Field { get; set; }
        public int Level { get; set; }
        public int SortOrder { get; set; }
        public string Caption { get; set; }
    }
}
