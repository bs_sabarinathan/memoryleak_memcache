﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class InvoiceDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected DateTime? _createddate;
        protected string _ponumber;
        protected string _invoiceumber;
        protected int _supplierid;
        protected string _description;
        protected DateTime? _paymentdate;
        protected int? _approverid;
        protected string _poid;
        protected byte[] _invoiceCopy;
      
        protected int _userid;
        protected bool _IsExternal;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public InvoiceDao() { }

        public InvoiceDao(int pEntityid, DateTime? pCreateddate, string pPonumber, string pInvoiceNumber, int pSupplierid, string pDescription, DateTime? pPaymentdate, int? pApproverId, int pUserID, byte[] pInvoiceCopy, string pPOID, bool pIsExternal)
        {
            this._entityid = pEntityid;
            this._createddate = pCreateddate;
            this._ponumber = pPonumber;
            this._supplierid = pSupplierid;
            this._description = pDescription;
            this._paymentdate = pPaymentdate;
            this._invoiceCopy = pInvoiceCopy;
            this._approverid = pApproverId;
            this._userid = pUserID;
            this._poid = pPOID;
            this._invoiceumber = pInvoiceNumber;
            this._IsExternal = pIsExternal;
        }

        public InvoiceDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual DateTime? CreateDate
        {
            get { return _createddate; }
            set { _bIsChanged |= (_createddate != value); _createddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string PoNumber
        {
            get { return _ponumber; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("PONumber", "PONumber value, cannot contain more than 50 characters");
                _bIsChanged |= (_ponumber != value);
                _ponumber = value;
            }

        }

        public virtual string InvoiceNumber
        {
            get { return _invoiceumber; }
            set
            {
                _bIsChanged |= (_invoiceumber != value);
                _invoiceumber = value;
            }

        }

        public virtual string POID
        {
            get { return _poid; }
            set
            {
                _bIsChanged |= (_poid != value);
                _poid = value;
            }

        }


        public virtual int SupplierID
        {
            get { return _supplierid; }
            set { _bIsChanged |= (_supplierid != value); _supplierid = value; }

        }

       

        public virtual DateTime? PaymentDate
        {
            get { return _paymentdate; }
            set { _bIsChanged |= (_paymentdate != value); _paymentdate = value; }

        }

        public virtual int? ApproverID
        {
            get { return _approverid; }
            set { _bIsChanged |= (_approverid != value); _approverid = value; }

        }

        public virtual byte[] InvoiceCopy
        {
            get { return _invoiceCopy; }
            set { _bIsChanged |= (_invoiceCopy != value); _invoiceCopy = value; }

        }


      

        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }


        public virtual bool IsExternal
        {
            get { return _IsExternal; }
            set { _IsExternal = value; }
        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string PurchaseOrderDao = "PurchaseOrderDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string PoNumber = "PoNumber";
            public const string SupplierID = "SupplierID";
            public const string Description = "Description";
            public const string ApproverID = "ApproverID";
            public const string InvoiceNumber = "InvoiceNumber";
            public const string UserID = "UserID";
            public const string CreateDate = "CreateDate";
            public const string POID = "POID";
          
            public const string PaymentDate = "PaymentDate";
            public const string InvoiceCopy = "InvoiceCopy";
            public const string IsExternal = "IsExternal";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PurchaseOrderDao = "PurchaseOrderDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string PoNumber = "PoNumber";
            public const string SupplierID = "SupplierID";
            public const string Description = "Description";
            public const string ApproverID = "ApproverID";
            public const string InvoiceNumber = "InvoiceNumber";
            public const string UserID = "UserID";
            public const string CreateDate = "CreateDate";
            public const string POID = "POID";
            public const string PaymentDate = "PaymentDate";
            public const string InvoiceCopy = "InvoiceCopy";
            public const string IsExternal = "IsExternal";
           
        }

        #endregion


    }

}
