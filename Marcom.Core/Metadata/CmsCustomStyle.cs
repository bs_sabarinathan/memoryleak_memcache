﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class CmsCustomStyle : ICmsCustomStyle
    {
        #region Member Variables

        protected int _id;
        protected string _classname;
        protected string _csscode;

        #endregion

        #region Constructors
        public CmsCustomStyle() { }

        public CmsCustomStyle(int pId, string pClassName, string pCssCode)
        {
            this._id = pId;
            this._classname = pClassName;
            this._csscode = pCssCode;
        }

        public CmsCustomStyle(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string ClassName
        {
            get { return _classname; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Class Name", "Class Name value, cannot contain more than 100 characters");
                _classname = value;
            }

        }

        public string CssCode
        {
            get { return _csscode; }
            set { _csscode = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            CmsCustomStyle castObj = null;
            try
            {
                castObj = (CmsCustomStyle)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
