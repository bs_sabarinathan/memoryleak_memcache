﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.admin.listsettingsCtrl",
    function muiadminobjectivetabCtrl($scope, $resource, $location, $translate, CommonService, AdminService) {
        $scope.filterText = '';
        $scope.filterText = '';
        var availableattrs = [];
        var allEntityTypes = [];
        var allAttributes = [];
        $scope.EntityTypeData = [];
        $scope.entitydatavalues = [];
        $scope.attributedatavalues = [];
        $scope.entitytype = 10;
        $scope.additionalObjAtributes = [];

        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1000, "Caption": "Name", "Description": "Name", "Id": -1000, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1001, "Caption": "Type", "Description": "Type", "Id": -1001, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1002, "Caption": "Planned target", "Description": "Planned target", "Id": -1002, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1003, "Caption": "Target outcome", "Description": "Target outcome", "Id": -1003, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1004, "Caption": "Rating", "Description": "Rating", "Id": -1004, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1005, "Caption": "Fulfillment state", "Description": "Fulfillment state", "Id": -1005, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1006, "Caption": "Unit", "Description": "Unit", "Id": -1006, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });
        $scope.additionalObjAtributes.push({ "AttributeTypeID": -1007, "Caption": "Status", "Description": "Status", "Id": -1007, "IsSpecial": false, "IsSystemDefined": true, "Level": 0, "Type": 0 });


        AdminService.GetObjectiveTypeAttributeByEntityType($scope.entitytype, true).then(function (attributesresult) {
            allAttributes = attributesresult.Response;
            // allAttributes = $.grep(getattributes.Response, function (e) { return e.AttributeTypeID != 7 });
            CommonService.GetObjectiveTabAdminSettings(AdminLogoSettings, $scope.entitytype).then(function (getattribtuesResp) {
                var getattribtues = JSON.parse(getattribtuesResp.Response);
                if (getattribtues.ObjectiveTabView.Attributes != null) {
                    $scope.attributedatavalues = getattribtues.ObjectiveTabView.Attributes.Attribute;
                    var attribtueslength = getattribtues.ObjectiveTabView.Attributes.Attribute.length;
                    var Attrhtml2 = '';
                    if (attribtueslength != undefined) {
                        for (var b = 0; b < attribtueslength; b++) {
                            Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '</a></li>';
                        }
                        $('#sortableAttribute2').html(Attrhtml2);
                    }
                    else {
                        Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute.WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '</a></li>';
                        $('#sortableAttribute2').html(Attrhtml2);
                    }
                }
                else {
                    $('#sortableAttribute2').html('');
                }
                var Attrhtml = '';
                for (var i = 0; i < allAttributes.length; i++) {
                    if (allAttributes[i].Id != 77) {
                        if (allAttributes[i].AttributeTypeID == 6 || allAttributes[i].AttributeTypeID == 12) {
                            var AttrValues = [];
                            if ($scope.attributedatavalues[0] != undefined)
                                AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                            else {
                                if ($scope.attributedatavalues.DisplayName == allAttributes[i].Caption)
                                    AttrValues = $.grep(allAttributes, function (e) { return e.Caption == $scope.attributedatavalues.DisplayName; });
                            }
                            //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                            if (AttrValues.length == 0) {
                                Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                availableattrs.push(allAttributes[i]);
                            }
                        }
                        else {
                            var AttrValues = [];
                            if ($scope.attributedatavalues[0] != undefined)
                                AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                            else {
                                if ($scope.attributedatavalues.Id == allAttributes[i].Id)
                                    AttrValues = $.grep(allAttributes, function (e) { return e.Id == $scope.attributedatavalues.Id; });
                            }
                            //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                            if (AttrValues.length == 0) {
                                Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                availableattrs.push(allAttributes[i]);
                            }
                        }
                    }
                }
                $('#sortableAttribute1').html(Attrhtml);
            });
        });
        $("#sortableAttribute2").droppable({
            drop: function (event, ui) {
                var dropeedid = ui.helper[0].attributes["data-id"].value;
                var attrDelObj = $.grep(availableattrs, function (e) { return e.Id == dropeedid; });
                availableattrs.splice($.inArray(attrDelObj[0], availableattrs), 1);
            }
        });
        $("#sortableAttribute1").droppable({
            drop: function (event, ui) {
                var dropeedid = ui.helper[0].attributes["data-id"].value;
                var attrAddObj = $.grep(allAttributes, function (e) { return e.Id == dropeedid; })[0];
                availableattrs.push(attrAddObj);
            }
        });
        $scope.FilterAvailableAttrs = function () {
            var search = '';
            var id = $("#sortableAttribute1 >li").attr("data-id");
            var ids = [];
            if (availableattrs.length == 0) {
                $.each($("#sortableAttribute1 >li"), function (i, el) {
                    ids.push($(el).attr("data-id"));
                    availableattrs.push($.grep(allAttributes, function (e) {
                        return e.Id == parseInt($(el).attr("data-id"));
                    })[0]);
                });
            }
            var searchresult = $.grep(availableattrs, function (e) {
                return e.Caption.toLowerCase().contains($scope.filterText.toLowerCase());
            });
            $('#sortableAttribute1').html('');
            var Attrhtml = '';
            for (var i = 0; i < searchresult.length; i++) {
                if (searchresult[i].AttributeTypeID == 6 || searchresult[i].AttributeTypeID == 12) {
                    var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == searchresult[i].Caption; });
                    Attrhtml += '<li data-Id="' + searchresult[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + searchresult[i].Level + '"  data-Type="' + searchresult[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + searchresult[i].Id + '" data-DisplayName="' + searchresult[i].Caption + '"  data-IsSpecial="' + searchresult[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + searchresult[i].Caption + '</a></li>';
                }
                else {
                    var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == searchresult[i].Id; });
                    Attrhtml += '<li data-Id="' + searchresult[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + searchresult[i].Level + '" data-Type="' + searchresult[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + searchresult[i].Id + '" data-DisplayName="' + searchresult[i].Caption + '"  data-IsSpecial="' + searchresult[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + searchresult[i].Caption + '</a></li>';
                }
            }
            $('#sortableAttribute1').html(Attrhtml);
        }
        var AdminLogoSettings = 'ObjectiveTabView';
        $scope.loadattributes = function () {
            availableattrs = [];
            $scope.filterText = '';
            if ($scope.entitytype == 0) {
                $('#sortableAttribute2').html('');
                $('#sortableAttribute1').html('');
                return true;
            }
            if ($scope.entitytype == 10) {
                AdminService.GetObjectiveTypeAttributeByEntityType($scope.entitytype, true).then(function (attributesresult) {
                    allAttributes = attributesresult.Response;

                    CommonService.GetObjectiveTabAdminSettings(AdminLogoSettings, $scope.entitytype).then(function (getattribtuesResp) {
                        var getattribtues = JSON.parse(getattribtuesResp.Response);
                        if (getattribtues.ObjectiveTabView.Attributes != null) {
                            $scope.attributedatavalues = getattribtues.ObjectiveTabView.Attributes.Attribute;
                            var attribtueslength = getattribtues.ObjectiveTabView.Attributes.Attribute.length;
                            var Attrhtml2 = '';
                            if (attribtueslength != undefined) {
                                for (var b = 0; b < attribtueslength; b++) {
                                    Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '</a></li>';
                                }
                                $('#sortableAttribute2').html(Attrhtml2);
                            }
                            else {
                                Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute.WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '</a></li>';
                                $('#sortableAttribute2').html(Attrhtml2);
                            }
                            var Attrhtml = '';
                            for (var i = 0; i < allAttributes.length; i++) {
                                if (allAttributes[i].Id != 77) {
                                    if (allAttributes[i].AttributeTypeID == 6 || allAttributes[i].AttributeTypeID == 12) {
                                        var AttrValues = [];
                                        if ($scope.attributedatavalues[0] != undefined)
                                            AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                                        else {
                                            if ($scope.attributedatavalues.DisplayName == allAttributes[i].Caption)
                                                AttrValues = $.grep(allAttributes, function (e) { return e.Caption == $scope.attributedatavalues.DisplayName; });
                                        }
                                        //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                                        if (AttrValues.length == 0) {
                                            Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                            availableattrs.push(allAttributes[i]);
                                        }
                                    }
                                    else {
                                        var AttrValues = [];
                                        if ($scope.attributedatavalues[0] != undefined)
                                            AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                                        else {
                                            if ($scope.attributedatavalues.Id == allAttributes[i].Id)
                                                AttrValues = $.grep(allAttributes, function (e) { return e.Id == $scope.attributedatavalues.Id; });
                                        }
                                        //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                                        if (AttrValues.length == 0) {
                                            Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                            availableattrs.push(allAttributes[i]);
                                        }
                                    }
                                }
                            }
                            $('#sortableAttribute1').html(Attrhtml);
                        }
                        else {
                            $('#sortableAttribute2').html('');
                            var Attrhtml = '';
                            for (var i = 0; i < allAttributes.length; i++) {
                                if (allAttributes[i].Id != 77) {
                                    Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                }
                            }
                            $('#sortableAttribute1').html(Attrhtml);
                        }
                    });
                });
                MetadataService.GetAttributeTypeByEntityTypeID($scope.entitytype, true).then(function (getattributesfromdb) {
                    allAttributes = getattributesfromdb.Response;
                    // allAttributes = $.grep(getattributes.Response, function (e) { return e.AttributeTypeID != 7 });
                    CommonService.GetObjectiveTabAdminSettings(AdminLogoSettings, $scope.entitytype).then(function (getattribtuesResp) {
                        var getattribtues = JSON.parse(getattribtuesResp.Response);
                        if (getattribtues.ObjectiveTabView.Attributes != null) {
                            $scope.attributedatavalues = getattribtues.ObjectiveTabView.Attributes.Attribute;
                            var attribtueslength = getattribtues.ObjectiveTabView.Attributes.Attribute.length;
                            var Attrhtml2 = '';
                            if (attribtueslength != undefined) {
                                for (var b = 0; b < attribtueslength; b++) {
                                    Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute[b].DisplayName + '</a></li>';
                                }
                                $('#sortableAttribute2').html(Attrhtml2);
                            }
                            else {
                                Attrhtml2 += '<li data-Id="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Id + '" data-IsOrderBy="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsOrderBy + '" data-IsSelect="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSelect + '" data-IsFilter="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsFilter + '" data-Level="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Level + '" data-Type="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Type + '" data-WhereCondition="' + getattribtues.ObjectiveTabView.Attributes.Attribute.WhereCondition + '" data-Field="' + getattribtues.ObjectiveTabView.Attributes.Attribute.Field + '" data-DisplayName="' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '" data-IsSpecial="' + getattribtues.ObjectiveTabView.Attributes.Attribute.IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ObjectiveTabView.Attributes.Attribute.DisplayName + '</a></li>';
                                $('#sortableAttribute2').html(Attrhtml2);
                            }
                            var Attrhtml = '';
                            for (var i = 0; i < allAttributes.length; i++) {
                                if (allAttributes[i].Id != 77) {
                                    if (allAttributes[i].AttributeTypeID == 6 || allAttributes[i].AttributeTypeID == 12) {
                                        var AttrValues = [];
                                        if ($scope.attributedatavalues[0] != undefined)
                                            AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                                        else {
                                            if ($scope.attributedatavalues.DisplayName == allAttributes[i].Caption)
                                                AttrValues = $.grep(allAttributes, function (e) { return e.Caption == $scope.attributedatavalues.DisplayName; });
                                        }
                                        //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == allAttributes[i].Caption; });
                                        if (AttrValues.length == 0) {
                                            Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                            availableattrs.push(allAttributes[i]);
                                        }
                                    }
                                    else {
                                        var AttrValues = [];
                                        if ($scope.attributedatavalues[0] != undefined)
                                            AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                                        else {
                                            if ($scope.attributedatavalues.Id == allAttributes[i].Id)
                                                AttrValues = $.grep(allAttributes, function (e) { return e.Id == $scope.attributedatavalues.Id; });
                                        }
                                        //var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                                        if (AttrValues.length == 0) {
                                            Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                            availableattrs.push(allAttributes[i]);
                                        }
                                    }
                                }
                            }
                            $('#sortableAttribute1').html(Attrhtml);
                        }
                        else {
                            $('#sortableAttribute2').html('');
                            var Attrhtml = '';
                            for (var i = 0; i < allAttributes.length; i++) {
                                if (allAttributes[i].Id != 77) {
                                    Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                }
                            }
                            $('#sortableAttribute1').html(Attrhtml);
                        }
                    });
                });
            }
            else if ($scope.entitytype == -10) {

                allAttributes = $scope.additionalObjAtributes;

                CommonService.GetObjectiveTabAdminSettings("AditionalObjectiveTabView", $scope.entitytype == -10 ? 10 : $scope.entitytype).then(function (getattribtuesResp) {
                    var getattribtues = JSON.parse(getattribtuesResp.Response);
                    if (getattribtues.AditionalObjectiveTabView.Attributes != null) {
                        $scope.attributedatavalues = getattribtues.AditionalObjectiveTabView.Attributes.Attribute;
                        var attribtueslength = getattribtues.AditionalObjectiveTabView.Attributes.Attribute.length;
                        var Attrhtml2 = '';
                        if (attribtueslength != undefined) {
                            for (var b = 0; b < attribtueslength; b++) {
                                Attrhtml2 += '<li data-Id="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].Id + '" data-IsOrderBy="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].IsOrderBy + '" data-IsSelect="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].IsSelect + '" data-IsFilter="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].IsFilter + '" data-Level="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].Level + '" data-Type="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].Type + '" data-WhereCondition="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].WhereCondition + '" data-Field="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].Field + '" data-DisplayName="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].DisplayName + '" data-IsSpecial="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute[b].DisplayName + '</a></li>';
                            }
                            $('#sortableAttribute2').html(Attrhtml2);
                        }
                        else {
                            Attrhtml2 += '<li data-Id="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.Id + '" data-IsOrderBy="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.IsOrderBy + '" data-IsSelect="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.IsSelect + '" data-IsFilter="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.IsFilter + '" data-Level="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.Level + '" data-Type="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.Type + '" data-WhereCondition="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.WhereCondition + '" data-Field="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.Field + '" data-DisplayName="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.DisplayName + '" data-IsSpecial="' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.AditionalObjectiveTabView.Attributes.Attribute.DisplayName + '</a></li>';
                            $('#sortableAttribute2').html(Attrhtml2);
                        }
                        var Attrhtml = '';
                        for (var i = 0; i < allAttributes.length; i++) {
                            var AttrValues = [];
                            if ($scope.attributedatavalues[0] != undefined)
                                AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; });
                            else {
                                if ($scope.attributedatavalues.Id == allAttributes[i].Id)
                                    AttrValues = $.grep(allAttributes, function (e) { return e.Id == $scope.attributedatavalues.Id; });
                            }
                            if (AttrValues.length == 0) {
                                Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                availableattrs.push(allAttributes[i]);
                            }
                        }
                        $('#sortableAttribute1').html(Attrhtml);
                    }
                    else {
                        $('#sortableAttribute2').html('');
                        var Attrhtml = '';
                        for (var i = 0; i < allAttributes.length; i++) {
                            if (allAttributes[i].Id != 77) {
                                Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + allAttributes[i].Level + '" data-Type="' + allAttributes[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '"  data-IsSpecial="' + allAttributes[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                            }
                        }
                        $('#sortableAttribute1').html(Attrhtml);
                    }
                });
            }
        }

        $(function () {
            $("#sortableAttribute1, #sortableAttribute2").sortable({
                connectWith: ".connectedAttributeSortable",
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                helper: 'clone'
            }).disableSelection();
        });

        $scope.save = function () {
            var entitytype = 10;
            if ($scope.entitytype != undefined) {
                entitytype = $scope.entitytype;
            }
            var ActivityRootLevel = 'ObjectiveTabView';
            var listSettings = {};
            if ($scope.entitytype == -10) {
                entitytype = 10;
                ActivityRootLevel = 'AditionalObjectiveTabView';
            }
            listSettings.Key = ActivityRootLevel;
            listSettings.TypeId = entitytype;
            if ($scope.entitytype == -10)
                listSettings.AditionalObjectiveTabView = {
                    "Attributes": [],
                };
            else {
                listSettings.ObjectiveTabView = {
                    "Attributes": [],
                };
            }
            var AttributeArray = { "Attribute": [] };
            for (var j = 0; j < $("#sortableAttribute2").find('li').length; j++) {
                var level = 0;
                var Field = $("#sortableAttribute2").find('li').eq(j).attr("data-Id");
                if ($("#sortableAttribute2").find('li').eq(j).attr("data-Type") == 6 || $("#sortableAttribute2").find('li').eq(j).attr("data-Type") == 12) {
                    level = $("#sortableAttribute2").find('li').eq(j).attr("data-level");
                    Field = Field + "_" + level;
                }
                var Attribute =
                {
                    "Id": $("#sortableAttribute2").find('li').eq(j).attr("data-Id"),
                    "IsOrderBy": $("#sortableAttribute2").find('li').eq(j).attr("data-IsOrderBy"),
                    "IsSelect": $("#sortableAttribute2").find('li').eq(j).attr("data-IsSelect"),
                    "IsFilter": $("#sortableAttribute2").find('li').eq(j).attr("data-IsFilter"),
                    "Level": level,
                    "Type": $("#sortableAttribute2").find('li').eq(j).attr("data-Type"),
                    "WhereCondition": $("#sortableAttribute2").find('li').eq(j).attr("data-WhereCondition"),
                    "Field": Field,
                    "DisplayName": $("#sortableAttribute2").find('li').eq(j).attr("data-DisplayName"),
                    "IsSpecial": $("#sortableAttribute2").find('li').eq(j).attr("data-IsSpecial")
                };
                AttributeArray.Attribute.push(Attribute)
            }
            if ($scope.entitytype == -10) {
                listSettings.AditionalObjectiveTabView.Attributes.push(AttributeArray);
                // listSettings.$save();
                listSettings.View = { AditionalObjectiveTabView: listSettings.AditionalObjectiveTabView };
            }
            else {
                listSettings.ObjectiveTabView.Attributes.push(AttributeArray);
                // listSettings.$save();
                listSettings.View = { ObjectiveTabView: listSettings.ObjectiveTabView };
            }
            CommonService.AdminSettingsObjectivetabRootLevelInsertUpdate(listSettings).then(function () {
                NotifySuccess($translate.instant('LanguageContents.Res_4802.Caption'));
            });
        };
    }
    //);
    app.controller("mui.admin.objectivetabCtrl", ['$scope', '$resource', '$location', '$translate', 'CommonService', 'AdminService', muiadminobjectivetabCtrl]);
})(angular, app);