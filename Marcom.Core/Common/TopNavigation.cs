﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    public class TopNavigation : ITopNavigation
    {
        public IList<INavigation> TopMenu{ get; set; }
        public IList<INavigation> ChildMenu { get; set; }
    }
}
