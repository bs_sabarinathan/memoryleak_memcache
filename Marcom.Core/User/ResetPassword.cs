using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.User.Interface;
using System.Security.Cryptography;

namespace BrandSystems.Marcom.Core.User
{

	/// <summary>
    /// User object for table 'UM_ResetPasswordTrack'.
	/// </summary>
    public class ResetPassword : IResetPassword, ICloneable
	{
		#region Member Variables
        protected int _id;
        protected Guid _TrackingID;
        protected int _UserID;
        protected DateTime _ResetPasswordDate;
        protected bool _bIsChanged;
        
		#endregion
		
		#region Constructors
	    public ResetPassword() {}

        public ResetPassword(int pid, Guid pTrackingID, int pUserID, DateTime pResetPasswordDate)
		{
			this._id = pid ;
            this._TrackingID = pTrackingID;
			this._UserID = pUserID; 
			this._ResetPasswordDate = pResetPasswordDate; 

		}

        public ResetPassword(int pId)
		{
			this._id = pId; 
		}
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual Guid TrackingID
        {
            get { return _TrackingID; }
            set { _bIsChanged |= (_TrackingID != value); _TrackingID = value; }

        }

        public virtual int UserID
        {
            get { return _UserID; }
            set { _bIsChanged |= (_UserID != value); _UserID = value; }

        }

        public virtual DateTime ResetPasswordDate
        {

            get { return _ResetPasswordDate; }
            set { _bIsChanged |= (_ResetPasswordDate != value); _ResetPasswordDate = value; }

        }
		

		#endregion 				
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			User castObj = null;
			try
			{
				castObj = (User)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}

      
		#endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
		
	}
	
}
