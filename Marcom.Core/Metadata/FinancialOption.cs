﻿using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;
using System;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class FinancialOption : IFinancialOption
    {

        #region member variables
        public int _id { get; set; }
        public string _caption { get; set; }
        public int _finAttributeID { get; set; }
        public int _sortOrder { get; set; }

        #endregion

        #region Constructors
        public FinancialOption() { }

        public FinancialOption(int pid, string pcaption, int pfinattributeid, int psortorder)
        {
            this._id = pid;
            this._caption = pcaption;
            this._finAttributeID = pfinattributeid;
            this._sortOrder = psortorder;
        }
        public FinancialOption(int pid)
        {
            this._id = pid;
        }


        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }
        public virtual int FinAttributeID
        {
            get { return _finAttributeID; }
            set { _finAttributeID = value; }

        }
        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FinancialOption castObj = null;
            try
            {
                castObj = (FinancialOption)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
