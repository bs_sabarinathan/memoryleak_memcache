﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, PlanfiltersettingsService) {
        $scope.treeNodeSelectedHolderValues = [];
        $scope.appliedfilter = "No filter applied";
        var IsUpdate = 0;
        var IsSave = 0;
        $scope.deletefiltershow = false;
        $scope.FilterFields = {};
        var TypeID = $scope.EntityTypeID;
        $scope.visible = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.rememberAttrValue = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {}; 
        $scope.TreePricing = [];
        $scope.treeTexts = {};
        $scope.FilterDataXML = [];
        angular.element(document).ready(function () {
            $scope.$on('LoadRootLevelFilter', function (event, typeid, data) {
                if (data != undefined)
                    $scope.rememberAttrValue = data;
                $scope.FilterSettingsLoad();
            });

        });
        var rememberfilter = [];
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilterbutton = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsUpdate = 0;
            IsSave = 0;
        };
        $("#rootlevelfilter").on('ClearScope', function (event, TypeID) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
            }
        });
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearScopeModle = function () {
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationListWithTree, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            IsSave = 0;
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else if (TypeID == 5) {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            else if (TypeID == 10) {
                RememberObjFilterAttributes = [];
                RememberObjFilterID = 0;
                $cookieStore.remove('ObjAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('ObjFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('ObjFilterName' + parseInt($cookies['UserId'], 10));
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            $scope.deletefiltershow = false;
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
                var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 13
                });
                if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                    for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                        if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                    }
                }
            }
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattributes = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            if (TypeID == 6)
                $("#Div_list").trigger('ClearAndReApply');
            else if (TypeID == 5)
                $("#Div_listCC").trigger('ClearAndReApply');
            else if (TypeID == 10)
                $("#Div_Objlist").trigger('ClearAndReApply');

            if (TypeID == 6) {
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            } else if (TypeID == 5) {
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
            else if (TypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
        }
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            if ($scope.dyn_Cont.length == 0) {
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                $scope.EntityHierarchyTypesResult = [];
                $scope.dyn_Cont = '';
                $scope.Updatefilter = false;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $("#dynamic_Controls").html('');
                $scope.atributesRelationList = [];
                $scope.atributesRelationListWithTree = [];
                $scope.ngsaveasfilter = '';
                $scope.ngKeywordtext = '';
                var OptionFrom = 0;
                var IsKeyword = "false";
                var IsEntityType = "false";
                var CurrentActiveVersion = 0;
                var elementNode = 'Filter';
                var KeywordOptionsresponse;
                var CheckingPricicingAttr = [];
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    PlanfiltersettingsService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                        KeywordOptionsresponse = KeywordOptionsResult.Response;
                        OptionFrom = KeywordOptionsresponse.split(',')[0];
                        IsKeyword = KeywordOptionsresponse.split(',')[1];
                        IsEntityType = KeywordOptionsresponse.split(',')[3];
                        CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                        var IDList = new Array();
                        var filterattids = {};
                        filterattids.IDList = IDList;
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'Filter';
                        filterattids.OptionFrom = OptionFrom;
                        PlanfiltersettingsService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                    if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                        if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                            var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                            $scope.tagAllOptionsEntityStatus.data = [];
                                            if (EntityStatusOptions != null) {
                                                $.each(EntityStatusOptions, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.dyn_Cont += "<div class='control-group'>";
                                            $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                            $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Name  for ndata in  OntimeStatusLists \"></select></div>";
                                        } else {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption  for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                        if ($scope.atributesRelationList[i].AttributeId != 75 && $scope.atributesRelationList[i].AttributeId != 74) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"  id=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].LevelTreeNodes \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                        $scope.dyn_Cont += "<select multiple='multiple' ui-select2 data-placeholder='Select " + $scope.atributesRelationList[i].DisplayName + " options' ng-model='FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "'\ >";
                                        $scope.dyn_Cont += "<option ng-repeat='ndata in atributesRelationList[" + i + "].LevelTreeNodes' value='{{ndata.Id}}'>{{ndata.Caption}}</option>";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                        var k = $scope.TreePricing.length;
                                        var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                        });
                                        if (treecount12.length == 0) {
                                            var mm = $scope.atributesRelationList[i].AttributeId;
                                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                            $scope.FilterDataXML.push({
                                                "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                            });
                                        }
                                    }
                                }
                                if ($scope.EntityTypeID == 6 && IsEntityType == "True") {
                                    $scope.dyn_Cont += "<div class='control-group'>";
                                    $scope.dyn_Cont += "<span>EntityType : </span>";
                                    $scope.dyn_Cont += "<input id='ddlChildren' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' style='width: 200px;' />";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                    return e.AttributeTypeId == 7
                                })
                                for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                    if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                        $scope.treePreviewObj = {};
                                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        }
                                        $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                        $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_Cont += '</div></div>';
                                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                        $scope.dyn_Cont += '<div class="controls">';
                                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_Cont += '</div></div></div>';
                                    }
                                }
                                if (IsKeyword == "True") {
                                    $scope.dyn_Cont += "<div class=\"control-group\">";
                                    $scope.dyn_Cont += "<span>Keyword : </span>";
                                    $scope.dyn_Cont += "<input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'>";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                                $scope.Applyfilterbutton = false;
                                $scope.Updatefilter = false;
                                $scope.Deletefilter = false;
                                $scope.Savefilter = true;
                            }
                            RememberFilterOnReLoad($scope.rememberAttrValue);
                        });
                    });
                });
            }
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k].id,
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            if (orgLevel != undefined) {
                                for (var k = 0; k < orgLevel.length; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = [];
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    whereConditionData = [];
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    if (TypeID == 6)
                        $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    else if (TypeID == 5)
                        $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    else if (TypeID == 10)
                        $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);

                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = FilterData.FilterName;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = FilterData.FilterName;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                    else if (TypeID == 10) {
                        RememberObjFilterID = $scope.FilterID.selectedFilterID;
                        RememberObjFilterAttributes = $scope.filterattributes;
                        RemeberObjFilterName = FilterData.FilterName;
                        $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterId, typeId) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId) {
            $scope.Updatefilter = false;
            $scope.Applyfilterbutton = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.ClearScopeModle();
            $scope.FilterID.selectedFilterID = filterId;
            PlanfiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if ($scope.filterSettingValues.FilterValues.length > 0) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                    return e.AttributeId == parseInt(71);
                                })[0].EntityStatusOptionValues;
                                var seletedoptionvalue = [];
                                for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
									return e.AttributeId == parseInt(71);
                                }).length; sts++) {
                                    seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                        return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                    })[0]);
                                    if (seletedoptionvalue != null) {
                                        $.each(seletedoptionvalue, function (i, el) {
                                            $scope.tagAllOptionsEntityStatus.data.push({
                                                "id": el.ID,
                                                "text": el.StatusOptions,
                                                "ShortDescription": el.ShortDesc,
                                                "ColorCode": el.ColorCode
                                            });
                                        });
                                    }
                                    if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                }
                            } else {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        }
                    }
                }
                if ($scope.EntityTypeID == 6) {
                    PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                        if (filterSettingsValues.Response.EntityTypeID != "") {
                            for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                                if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                                    if ($.grep($scope.tagAllOptions.data, function (e) {
										return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                        return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0]);
                                }
                            }
                            $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                        }
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                    for (var k = 0; k < dataVal.length; k++) {
                        if ($scope.atributesRelationList[i].AttributeId == 71) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k].id,
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        } else {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            var FilterData = {};
            FilterData.FilterId = $scope.FilterID.selectedFilterID;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    if (TypeID == 6)
                        $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    else if (TypeID == 5)
                        $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    else if (TypeID == 10)
                        $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);

                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = $scope.appliedfilter;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = $scope.appliedfilter;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                    else if (TypeID == 10) {
                        RememberObjFilterID = $scope.FilterID.selectedFilterID;
                        RememberObjFilterAttributes = $scope.filterattributes;
                        RemeberObjFilterName = FilterData.FilterName;
                        $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $scope.ApplyFilter = function (FilterID, FilterName) {
            if (FilterID != undefined && FilterID != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                if ($scope.atributesRelationList.length > 0) {
                    if ($scope.atributesRelationList.length == 0) {
                        $scope.atributesRelationListtemp = $scope.atributesRelationList;
                    } else $scope.atributesRelationListtemp = $scope.atributesRelationList;
                }
            }
            if ($scope.atributesRelationList != undefined) if ($scope.atributesRelationList.length == 0) $scope.atributesRelationList = $scope.atributesRelationListtemp;
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var StartDate = '01/01/1990';
            var EndDate = '01/01/1990';
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId, e.ischecked == true;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            if ($scope.ngKeywordtext != '') whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': $scope.ngKeywordtext
                            });
                            else whereConditionData.push({
                                'AttributeID': nodeval.AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': ''
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    }
                }
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            } else {
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            $scope.FilterID.filterattributes = $scope.AppllyFilterObj.selectedattributes;
            if (TypeID == 6) {
                RememberPlanFilterID = FilterID;
                RememberPlanFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberPlanFilterName = $scope.appliedfilter;
                $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            } else if (TypeID == 5) {
                RememberCCFilterID = FilterID;
                RememberCCFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberCCFilterName = $scope.appliedfilter;
                $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
            else if (TypeID == 10) {
                RememberObjFilterID = FilterID;
                RememberObjFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberObjFilterName = $scope.appliedfilter;
                $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
            $scope.FilterID.filterattributes = [];
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            $scope.filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        PlanfiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4398.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                if (TypeID == 6)
                                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                else if (TypeID == 5)
                                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                else if (TypeID == 10)
                                    $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);

                                $scope.FilterID.selectedFilterID = 0;
                                $scope.FilterID = {
                                    selectedFilterID: 0
                                };
                                $scope.AppllyFilterObj = {};
                                $scope.ApplyFilter(0, 'No filter applied');
                                if (TypeID == 6) {
                                    $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 5) {
                                    $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 10) {
                                    $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 6 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            } else if ($scope.EntityTypeID == 5 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            }
            if (RememberPlanFilterAttributes.length != 0) {
                $scope.ddlParententitytypeId = [];
                $scope.filterSettingValues.FilterAttributes = RememberPlanFilterAttributes;
                for (var i = 0; i < $scope.filterSettingValues.FilterAttributes.length; i++) {
                    if ($scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 4) {
                        if ($scope.filterSettingValues.FilterAttributes[i].AttributeID == 71) {
                            var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeId == parseInt(71);
                            })[0].EntityStatusOptionValues;
                            var seletedoptionvalue = [];
                            for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterAttributes, function (e) {
								return e.AttributeID == parseInt(71);
                            }).length; sts++) {
                                seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                    return e.id == parseInt($scope.filterSettingValues.FilterAttributes[sts].SelectedValue);
                                })[0]);
                                if (seletedoptionvalue != null) {
                                    $.each(seletedoptionvalue, function (i, el) {
                                        $scope.tagAllOptionsEntityStatus.data.push({
                                            "id": el.ID,
                                            "text": el.StatusOptions,
                                            "ShortDescription": el.ShortDesc,
                                            "ColorCode": el.ColorCode
                                        });
                                    });
                                }
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] = seletedoptionvalue;
                            }
                        } else {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                        }
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                    }
                }
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.length == undefined) {
                    if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs != '0') {
                        if ($.grep($scope.tagAllOptions.data, function (e) {
							return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0]);
                    }
                } else {
                    for (var l = 0; l < $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',').length; l++) {
                        if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l] != '0') {
                            if ($.grep($scope.tagAllOptions.data, function (e) {
								return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0]);
                        }
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                $scope.ngKeywordtext = $scope.filterSettingValues.FilterAttributes[0].Keyword;
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.filtersettingsCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            if ($scope.treesrcdirec["Attr_" + attributeid].length != undefined) {
                for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                    branch.ischecked = false;
                    if (branch.Children.length > 0) {
                        ClearRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        };
    }
    app.controller("mui.planningtool.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'PlanfiltersettingsService', muiplanningtoolfiltersettingsCtrl]);
})(angular, app);