﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IDamTreeValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;
        }

        int Attributeid
        {
            get;
            set;

        }
        int Nodeid
        {
            get;
            set;

        }
        int Level
        {
            get;
            set;
        }

        string Value
        {
            get;
            set;
        }

        int ParentNode
        {
            get;
            set;
        }

        #endregion
    }
}
