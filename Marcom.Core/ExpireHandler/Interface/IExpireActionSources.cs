﻿using BrandSystems.Marcom.Core.Planning.Interface;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ExpireHandler.Interface
{
    public interface IExpireActionSources
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int ActionID
        {
            get;
            set;
        }

        int SourceID
        {
            get;
            set;
        }

        int SourceEnityID
        {
            get;
            set;
        }

        int SourceFrom
        {
            get;
            set;
        }

        string Actionexutedays
        {
            get;
            set;
        }

        DateTime Actionexutedate
        {
            get;
            set;
        }

        bool Actionexute
        {
            get;
            set;
        }

        int ActionownerId
        {
            get;
            set;
        }

        IList<IAttributeData> AttributeData { get; set; }
        IList<IExpireActionSourcedatas> ActionSourcedatas { get; set; }
        #endregion
    }
}
