﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class NotificationSelection : INotificationSelection
    {
        public int Typeid { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserImage { get; set; }
        public string NotificationHappendTime { get; set; }
        public string NotificationText { get; set; }
        public bool Isviewed { get; set; }
        public int Notificationid { get; set; }
        public int TaskListID { get; set; }
        public int TaskEntityID { get; set; }
    }

}
