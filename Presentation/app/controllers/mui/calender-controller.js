﻿(function (ng, app) {
    "use strict"; function muiplanningtoolcalenderCtrl($scope) { $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calenderCtrl']")); }); }
    app.controller("mui.planningtool.calenderCtrl", ['$scope', muiplanningtoolcalenderCtrl]);
})(angular, app);