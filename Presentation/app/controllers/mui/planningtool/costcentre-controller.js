﻿(function (ng, app) {
    "use strict";
    //app.controller(
	//	"mui.planningtool.costcentreCtrl",
    function muiplanningtoolcostcentreCtrl($scope) {
        // --- Define Controller Methods. ------------------- //
        // ...


		    $scope.$on("$destroy", function () {
		        RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentreCtrl']"));
		    });
		    // --- Initialize. ---------------------------------- //
		    // ...
		}
    //);
    app.controller("mui.planningtool.costcentreCtrl", ['$scope', muiplanningtoolcostcentreCtrl]);
})(angular, app);