﻿(function (ng, app) {
    "use strict";

    function muiadminbroadcastmsgCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, $translate, AdminService) {
        $scope.userid = parseInt($cookies['UserId']);
        $timeout(function () {
            Glistmsg();
        }, 10);
        AdminService.GetAdditionalSettings().then(function (GetAdditionalresult) {
            if (GetAdditionalresult.StatusCode == 200) {
                $scope.DefaultSettings.DateFormat = GetAdditionalresult.Response[0].SettingValue;
            } else {
                $scope.DateFormat = "yyyy-mm-dd";
            }
        });

        function Glistmsg() {
            AdminService.GetBroadcastMessages().then(function (res) {
                var OSResult = res.Response;
                for (var i = 0; i < res.Response.length; i++) {
                    var datstartval = new Date(OSResult[i]["Createdate"]);
                    var condatstartval = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                    var resultdate = dateFormat(condatstartval.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    OSResult[i]["Createdate"] = resultdate;
                }
                $scope.listdata = OSResult;
            });
        };
        $scope.gridOptions = {
            data: 'listdata',
            enablePinning: false,
            enableColumnReordering: false,
            columnDefs: [{
                field: "Username",
                displayName: $translate.instant('LanguageContents.Res_4987.Caption'),
                width: 120
            }, {
                field: "Createdate",
                displayName: $translate.instant('LanguageContents.Res_814.Caption'),
                width: 120
            }, {
                field: "Messages",
                displayName: $translate.instant('LanguageContents.Res_4482.Caption'),
                width: 700
            }]
        };
        $scope.addBroadcastMessage = function () {
            var insertBroadcastMessage = {};
            insertBroadcastMessage.userId = parseInt($cookies['UserId']);;
            insertBroadcastMessage.username = $cookies['Username'];
            if ($scope.broadcastmsg != undefined && $scope.broadcastmsg.trim() != "")
                insertBroadcastMessage.broadcastmsg = $scope.broadcastmsg;
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_5718.Caption'));
                return false;
            }
            AdminService.InsertBroadcastMessages(insertBroadcastMessage).then(function (res) {
                if (res.Response != 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4739.Caption'));
                    $scope.broadcastmsg = '';
                    $timeout(function () {
                        Glistmsg();
                    }, 10);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4526.Caption'));
                    return false;
                }
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.broadcastmsgCtrl']"));
        });
    }
    app.controller("mui.admin.broadcastmsgCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', '$translate', 'AdminService', muiadminbroadcastmsgCtrl]);
})(angular, app);