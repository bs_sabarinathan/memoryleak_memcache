﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;


namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IWidgetTypes interface for table 'CM_WidgetTypes'.
    /// </summary>
    public interface IWidgetTypes
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string TypeName
        {
            get;
            set;

        }


        int ISDynamic
        {
            get;
            set;

        }

      #endregion
    }
}
