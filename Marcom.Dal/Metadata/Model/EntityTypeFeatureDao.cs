using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// EntityTypeFeatureDao object for table 'MM_EntityType_Feature'.
	/// </summary>
	
	public partial class EntityTypeFeatureDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _typeid;
		protected int _featureid;
        //protected string _validationid;
        //protected int _sortorder;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    EntityTypeFeatureDao toCompare = obj as EntityTypeFeatureDao;
        //    if (toCompare == null)
        //    {
            //    return false;
        //    }

        //    if (!Object.Equals(this.Typeid, toCompare.Typeid))
            //    return false;
        //    if (!Object.Equals(this.Featureid, toCompare.Featureid))
        //        return false;
        //    //if (!Object.Equals(this.Typeid, toCompare.ValidationId))
        //    //    return false;
        //    //if (!Object.Equals(this.Featureid, toCompare.SortOrder))
        //    //    return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Typeid.GetHashCode();
        //    hashCode = (hashCode * 7) + Featureid.GetHashCode();
        //    return hashCode;
        //}

        #endregion
		
		#region Constructors
		public EntityTypeFeatureDao() {}
			
        public EntityTypeFeatureDao(int pId, int pTypeid, int pFeatureid, string pValidationid, int sortorder)
		{
            this._id = pId;
			this._typeid = pTypeid; 
			this._featureid = pFeatureid; 
            //this._validationid = pValidationid;
            //this._sortorder = sortorder; 
		}
		
		#endregion
		
		#region Public Properties
		

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

		public virtual int TypeID
		{
			get { return _typeid; }
			set { _bIsChanged |= (_typeid != value); _typeid = value; }
			
		}
		
		public virtual int FeatureID
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}

        //public virtual string ValidationId
        //{
        //    get { return _validationid; }
        //    set { _bIsChanged |= (_validationid != value); _validationid = value; }

        //}

        //public virtual int SortOrder
        //{
        //    get { return _sortorder; }
        //    set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        //}

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
			public const string EntityTypeFeatureDao = "EntityTypeFeatureDao";			
			public const string Typeid = "Typeid";			
			public const string Featureid = "Featureid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";
			public const string EntityTypeFeatureDao = "EntityTypeFeatureDao";			
			public const string Typeid = "Typeid";			
			public const string Featureid = "Featureid";			
		}

		#endregion
		
		
	}
	
}
