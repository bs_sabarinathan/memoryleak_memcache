using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveEntityValue interface for table 'PM_ObjectiveEntityValue'.
    /// </summary>
    public interface IObjectiveEntityValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }
        int Objectiveid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        decimal PlannedTarget
        {
            get;
            set;

        }

        decimal TargetOutcome
        {
            get;
            set;

        }

        int RatingObjective
        {
            get;
            set;

        }

        string Comments
        {
            get;
            set;

        }

        int Status
        {
            get;
            set;

        }

        int Fulfilment
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
