﻿(function (ng, app) {

    "use strict";

    app.factory("GetModule", function ($resource) {
            return $resource('metadata/GetModule', { get: { method: 'GET' } });
        });

    app.factory("Module", function ($resource) {
            return $resource('metadata/Module/:ID', { ID: '@ID' }, { update: { method: 'PUT' } });
    });

    app.factory("Module", function ($resource) {
        return $resource('metadata/Module/:ID', { ID: '@ID'}, { update: { method: 'PUT' } });
    });

}(angular, app));