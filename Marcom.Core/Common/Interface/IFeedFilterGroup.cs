using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFeedTemplate interface for table 'CM_Feed_Template'.
    /// </summary>
    public interface IFeedFilterGroup
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

      

        string Template
        {
            get;
            set;

        }
        string FeedGroup
        {
            get;
            set;

        }
        
        

        #endregion
    }
}
