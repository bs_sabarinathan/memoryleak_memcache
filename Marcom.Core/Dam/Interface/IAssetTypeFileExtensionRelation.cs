﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetTypeFileExtensionRelation
    {
        int Id { get; set; }
        int AssetTypeID { get; set; }
        string Extension { get; set; }
        int SortOrder { get; set; }
    }
}
