﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report.Interface
{
    /// <summary>
    /// IUser interface for table 'RM_ReportCredential'.
    /// </summary>
    public interface IFinancialReportSettings
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string ReportName
        {
            get;
            set;

        }

        string ReportImage
        {
            get;
            set;

        }

        string ReportDescription
        {
            get;
            set;

        }

       
        #endregion
    }
}
