using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// Financial object for table 'PM_Financial'.
	/// </summary>
	
	internal class Financial : IFinancial 
	{
		#region Member Variables

        protected int _id;
		protected int _entityid;
		protected int _costcenterid;
		protected decimal _plannedamount;
		protected decimal _requestedamount;
		protected decimal _approvedallocatedamount;
        protected DateTimeOffset _lastupdateon;
        protected int _Status;
		protected decimal _approvedbudget;
		protected decimal _commited;
		protected decimal _spent;
		protected DateTimeOffset _approvedbudgetdate;
        protected decimal _q1;
        protected decimal _q2;
        protected decimal _q3;
        protected decimal _q4;
        protected decimal _m1;
        protected decimal _m2;
        protected decimal _m3;
        protected decimal _m4;
        protected decimal _m5;
        protected decimal _m6;
        protected decimal _m7;
        protected decimal _m8;
        protected decimal _m9;
        protected decimal _m10;
        protected decimal _m11;
        protected decimal _m12;
        protected decimal _h1;
        protected decimal _h2;
        protected decimal _y;
        protected decimal _actualq1;
        protected decimal _actualq2;
        protected decimal _actualq3;
        protected decimal _actualq4;
        protected decimal _actualm1;
        protected decimal _actualm2;
        protected decimal _actualm3;
        protected decimal _actualm4;
        protected decimal _actualm5;
        protected decimal _actualm6;
        protected decimal _actualm7;
        protected decimal _actualm8;
        protected decimal _actualm9;
        protected decimal _actualm10;
        protected decimal _actualm11;
        protected decimal _actualm12;
        protected decimal _actualh1;
        protected decimal _actualh2;
        protected decimal _actualy;
        protected decimal _subassignedamount;
        protected decimal _availableassignedamount;
        protected DateTimeOffset _lastupdatedamounton;
		
		
		#endregion
		
		#region Constructors
		public Financial() {}

        public Financial(int pId, int pEntityid, int pCostCenterid, decimal pPlannedAmount, decimal pRequestedAmount, decimal pApprovedAllocatedAmount, DateTimeOffset pLastUpdateOn, int pStatus, decimal pApprovedBudget, decimal pCommited, decimal pSpent, DateTimeOffset pApprovedBudgetDate, decimal pQ1, decimal pQ2, decimal pQ3, decimal pQ4, decimal pm1, decimal pm2, decimal pm3, decimal pm4, decimal pm5, decimal pm6, decimal pm7, decimal pm8, decimal pm9, decimal pm10, decimal pm11, decimal pm12, decimal ph1, decimal ph2, decimal py, decimal pactualQ1, decimal pactualQ2, decimal pactualQ3, decimal pactualQ4, decimal pactualm1, decimal pactualm2, decimal pactualm3, decimal pactualm4, decimal pactualm5, decimal pactualm6, decimal pactualm7, decimal pactualm8, decimal pactualm9, decimal pactualm10, decimal pactualm11, decimal pactualm12, decimal pactualh1, decimal pactualh2, decimal pactualy, DateTimeOffset plastupdatedamounton, decimal pSubAssignedAmount, decimal pAvailableAssignedAmount)
		{
            this._id = pId;
			this._entityid = pEntityid; 
			this._costcenterid = pCostCenterid; 
			this._plannedamount = pPlannedAmount; 
			this._requestedamount = pRequestedAmount; 
			this._approvedallocatedamount = pApprovedAllocatedAmount;
            this._lastupdateon = pLastUpdateOn;
            this._Status = pStatus; 
			this._approvedbudget = pApprovedBudget; 
			this._commited = pCommited; 
			this._spent = pSpent; 
			this._approvedbudgetdate = pApprovedBudgetDate;
            this._q1 = pQ1;
            this._q2 = pQ2;
            this._q3 = pQ3;
            this._q4 = pQ4;
            this._m1 = pm1;
            this._m2 = pm2;
            this._m3 = pm3;
            this._m4 = pm4;
            this._m5 = pm5;
            this._m6 = pm6;
            this._m7 = pm7;
            this._m8 = pm8;
            this._m9 = pm9;
            this._m11 = pm11;
            this._m12 = pm12;
            this._h1 = ph1;
            this._h2 = ph2;
            this._y = py;
            this._actualq1 = pactualQ1;
            this._actualq2 = pactualQ2;
            this._actualq3 = pactualQ3;
            this._actualq4 = pactualQ4;
            this._actualm1 = pactualm1;
            this._actualm2 = pactualm2;
            this._actualm3 = pactualm3;
            this._actualm4 = pactualm4;
            this._actualm5 = pactualm5;
            this._actualm6 = pactualm6;
            this._actualm7 = pactualm7;
            this._actualm8 = pactualm8;
            this._actualm9 = pactualm9;
            this._actualm11 = pactualm11;
            this._actualm12 = pactualm12;
            this._actualh1 = pactualh1;
            this._actualh2 = pactualh2;
            this._actualy = pactualy;
            this._lastupdatedamounton = plastupdatedamounton;
            this._subassignedamount = pSubAssignedAmount;
            this._availableassignedamount = pAvailableAssignedAmount;
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public int CostCenterid
		{
			get { return _costcenterid; }
			set { _costcenterid = value; }
			
		}
		
		public decimal PlannedAmount
		{
			get { return _plannedamount; }
			set { _plannedamount = value; }
			
		}
		
		public decimal RequestedAmount
		{
			get { return _requestedamount; }
			set { _requestedamount = value; }
			
		}
		
		public decimal ApprovedAllocatedAmount
		{
			get { return _approvedallocatedamount; }
			set { _approvedallocatedamount = value; }
			
		}

        public DateTimeOffset LastUpdateOn
        {
            get { return _lastupdateon; }
            set { _lastupdateon = value; }

        }

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }

        }

		
		public decimal ApprovedBudget
		{
			get { return _approvedbudget; }
			set { _approvedbudget = value; }
			
		}
		
		public decimal Commited
		{
			get { return _commited; }
			set { _commited = value; }
			
		}
		
		public decimal Spent
		{
			get { return _spent; }
			set { _spent = value; }
			
		}
		
		public DateTimeOffset ApprovedBudgetDate
		{
			get { return _approvedbudgetdate; }
			set { _approvedbudgetdate = value; }
			
		}

        public virtual decimal Q1
        {
            get { return _q1; }
            set {_q1 = value; }

        }

        public virtual decimal Q2
        {
            get { return _q2; }
            set { _q2 = value; }

        }

        public virtual decimal Q3
        {
            get { return _q3; }
            set {  _q3 = value; }

        }

        public virtual decimal Q4
        {
            get { return _q4; }
            set { _q4 = value; }

        }

        public virtual decimal M1
        {
            get { return _m1; }
            set { _m1 = value; }

        }

        public virtual decimal M2
        {
            get { return _m2; }
            set {_m2 = value; }

        }

        public virtual decimal M3
        {
            get { return _m3; }
            set { _m3 = value; }

        }

        public virtual decimal M4
        {
            get { return _m4; }
            set {_m4 = value; }

        }

        public virtual decimal M5
        {
            get { return _m5; }
            set {_m5 = value; }

        }

        public virtual decimal M6
        {
            get { return _m6; }
            set { _m6 = value; }

        }

        public virtual decimal M7
        {
            get { return _m7; }
            set {_m7 = value; }

        }

        public virtual decimal M8
        {
            get { return _m8; }
            set {  _m8 = value; }

        }

        public virtual decimal M9
        {
            get { return _m9; }
            set { _m9 = value; }

        }

        public virtual decimal M10
        {
            get { return _m10; }
            set {_m10 = value; }

        }

        public virtual decimal M11
        {
            get { return _m11; }
            set { _m11 = value; }

        }

        public virtual decimal M12
        {
            get { return _m12; }
            set { _m12 = value; }

        }

        public virtual decimal H1
        {
            get { return _h1; }
            set { _h1 = value; }

        }

        public virtual decimal H2
        {
            get { return _h2; }
            set {  _h2 = value; }

        }

        public virtual decimal Y
        {
            get { return _y; }
            set {  _y = value; }

        }
        public virtual decimal ActualQ1
        {
            get { return _actualq1; }
            set {  _actualq1 = value; }

        }

        public virtual decimal ActualQ2
        {
            get { return _actualq2; }
            set {  _actualq2 = value; }

        }

        public virtual decimal ActualQ3
        {
            get { return _actualq3; }
            set {  _actualq3 = value; }

        }

        public virtual decimal ActualQ4
        {
            get { return _actualq4; }
            set {  _actualq4 = value; }

        }


        public virtual decimal ActualM1
        {
            get { return _actualm1; }
            set { _actualm1 = value; }

        }

        public virtual decimal ActualM2
        {
            get { return _actualm2; }
            set { _actualm2 = value; }

        }

        public virtual decimal ActualM3
        {
            get { return _actualm3; }
            set {  _actualm3 = value; }

        }

        public virtual decimal ActualM4
        {
            get { return _actualm4; }
            set {  _actualm4 = value; }

        }

        public virtual decimal ActualM5
        {
            get { return _actualm5; }
            set {  _actualm5 = value; }

        }

        public virtual decimal ActualM6
        {
            get { return _actualm6; }
            set { _actualm6 = value; }

        }

        public virtual decimal ActualM7
        {
            get { return _actualm7; }
            set {  _actualm7 = value; }

        }

        public virtual decimal ActualM8
        {
            get { return _actualm8; }
            set {  _actualm8 = value; }

        }

        public virtual decimal ActualM9
        {
            get { return _actualm9; }
            set {  _actualm9 = value; }

        }

        public virtual decimal ActualM10
        {
            get { return _actualm10; }
            set { _actualm10 = value; }

        }

        public virtual decimal ActualM11
        {
            get { return _actualm11; }
            set { _actualm11 = value; }

        }

        public virtual decimal ActualM12
        {
            get { return _actualm12; }
            set {  _actualm12 = value; }

        }

        public virtual decimal ActualH1
        {
            get { return _actualh1; }
            set { _actualh1 = value; }

        }

        public virtual decimal ActualH2
        {
            get { return _actualh2; }
            set {  _actualh2 = value; }

        }

        public virtual decimal ActualY
        {
            get { return _actualy; }
            set {  _actualy = value; }

        }

        public decimal SubAssignedAmount
        {
            get { return _subassignedamount; }
            set { _subassignedamount = value; }

        }

        public decimal AvailableAssignedAmount
        {
            get { return _availableassignedamount; }
            set { _availableassignedamount = value; }

        }

        public DateTimeOffset LastUpdatedAmountOn
        {
            get { return _lastupdatedamounton; }
            set { _lastupdatedamounton = value; }

        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Financial castObj = null;
			try
			{
				castObj = (Financial)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
		
	}
	
}
