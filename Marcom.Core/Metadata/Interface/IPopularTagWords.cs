﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IPopularTagWords
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int AttributeID
        {
            get;
            set;

        }

        int TotalHits
        {
            get;
            set;

        }

        string UsedOn
        {
            get;
            set;

        }



        #endregion
    }
}
