﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IAttributeGroupRoleAccess
    {
        int ID { get; set; }
        int EntityTypeID { get; set; }
        int GlobalRoleID { get; set; }
        int AttributeGroupID { get; set; }
    }
}
