﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User.Interface
{
    public interface ILoginCheck
    {
        #region Public Properties
      
        int Id
        {
            get;
            set;
        }
        string Email
        {
            get;
            set;

        }
        DateTime LoginTime
        {
            get;
            set;
        }
        string IpAddress
        {
            get;
            set;
        }
        bool NotifyAdmin
        {
            get;
            set;
        }
   
    #endregion
    }
}
