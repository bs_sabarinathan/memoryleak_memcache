﻿(function (ng, app) {
    "use strict";

    function muiadminuserdetailsCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, MetadataService) {
        $scope.EnableEntitytypeAttributeAdd = true;
        $scope.EnableEntitytypeAttributeUpdate = false;
        $scope.tempAttributeGrpID = 0;
        $scope.attributegroupGridData = [];
        $scope.Category = 0;
        $scope.AdminTaskCheckList = [{
            ID: 0,
            StatusOptions: "",
            IsDeleted: false,
            SortOrder: 1,
            IsExisting: true
        }];
        $scope.IsAssociate = 0;
        $scope.AttributeIDList = [];
        $scope.Fetureattribues = [];
        $scope.EntityHietemp = [];
        $scope.ShowForUserDetails = false;
        $scope.ShowForNormalEntities = true;
        $scope.ColorCodeGlobal = 'ffffff';
        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: false,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showHexInput: true,
            showBasicColors: true
        });
        $timeout(function () {
            $("#appendedPrependedDropdownButton").val($scope.ColorCodeGlobal);
            $(".current-color").css('background-color', '#' + $scope.ColorCodeGlobal);
        }, 500);
        $scope.DragDropvalues = '';
        $scope.DragDropAttribteGroupvalues = '';
        $scope.attributeObjCaption = '';
        $scope.attributeObjAttributeTypeId = 0;
        $scope.EntityHierarchyTypesResult = [];
        $scope.ParententityDropdowndata = [];
        $scope.AttributeOptionvalues = {};
        $scope.entitytypeattributrearr = [];
        $scope.FulfillmentAttributes = [];
        $scope.entitytypefeaturearr = [];
        $scope.modules = {};
        $scope.entityattributes = {};
        $scope.attributes = {};
        $scope.Features = [];
        $scope.entitytpesdata = [];
        $scope.AttributeOptionvalues = [];
        $scope.ressdfwe = {};
        $scope.entitytypefeatures = {};
        $scope.entitytypeattributrearr = [];
        $scope.EntityID = 0;
        $scope.EntType = {
            EntityTypeCaption: "",
            EntityTypeDescription: "",
            ddlModuleID: 0,
            EntitytperelCaption: "",
            ddlAttributeID: 0,
            ddlSortorder: "",
            Defaultvalue: "",
            Inheritfromparent: false,
            Isreadonly: false,
            Choosefromparentonly: false,
            Isvalidationneeded: false,
            ddlEntityFeatureID: '',
            AttributeMultiselectDefaultValue: '',
            Shortdescription: '',
            Colourcode: '',
            ShowHideRootLevel: true,
            ShowHideInheritFromParent: true,
            ddlAttributeGroupID: 0,
            AttributeGroupCaption: "",
            IsSeparateBlock: false,
            EnableAttributeGroupUpdate: true,
            EnableAttributeGroupAdd: true
        };

        function ValidEntitytypeattribute() {
            var entitytypeValues = [
				['#EntityTypeCaption', 'presence', 'Please Enter the Entity type caption'],
				['#Shortdescription', 'presence', 'Please Enter the Description'],
				['#ddlModuleID', 'presence', 'Please select module']
            ];
            $("#entitytypeformPage1").nod(entitytypeValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
            sentValidation = true;
        }

        function ValidEntitytypeattributePage2() {
            var entitytypeValues1 = [
				['#EntitytpeattrrelCaption', 'presence', 'Please Enter the Attribute Caption'],
				['#ddlAttributeID', 'presence', 'Please Enter the Attribute Type']
            ];
            $("#entitytypeformPage2").nod(entitytypeValues1, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp1',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
        }

        function ValidEntitytypeattributePage4() {
            var entitytypeValues1 = [
				['#AttributeGroupCaption', 'presence', 'Please enter the Attribute Group Caption'],
				['#dropdownAttributeGrpID', 'presence', 'Please select Attribute group yype']
            ];
            $("#entitytypeformPage4").nod(entitytypeValues1, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp4',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
            $("#entitytypeformPage4").addClass('notvalidate');
        }
        $scope.EnityType = ['EnityType', 'EntityTypeAttribute', 'EntityTypeAttributeGroup', 'AttributeToAttibuteRelations'];
        $scope.step = 0;
        $scope.isCurrentStep = function (step) {
            return $scope.step === step;
        };
        $scope.setCurrentStep = function (step) {
            $scope.step = step;
        };
        $scope.getCurrentStep = function () {
            $scope.steps = $scope.EnityType;
            return $scope.EnityType[$scope.step];
        };
        $scope.isFirstStep = function () {
            return $scope.step === 0;
        };
        $scope.isLastStep = function () {
            return $scope.step === ($scope.steps.length - 1);
        };
        $scope.visibleLastStep = true;
        $scope.getNextLabel = function () {
            $scope.visibleLastStep = true;
            if ($scope.IsOlderMetadataVersion.IsOlderVersion == false && $scope.isLastStep()) $scope.visibleLastStep = false;
            return ($scope.isLastStep()) ? 'Submit' : 'Next';
        };

        function ValidationGet() {
            MetadataService.GetAttributeValidationByEntityTypeId(12, 0).then(function (AttrValditeResult) {
                $scope.AtributeValueList = [];
                if (AttrValditeResult.Response != null) {
                    jQuery.each(AttrValditeResult.Response, function (index, Val) {
                        $scope.AtributeValueList.push({
                            Id: Val.Id,
                            Name: Val.Name,
                            EntityTypeID: Val.EntityTypeID,
                            RelationShipID: Val.RelationShipID,
                            ValueType: Val.ValueType,
                            Value: Val.Value,
                            ErrorMessage: Val.ErrorMessage,
                            AttributeID: Val.AttributeID
                        });
                    });
                }
            });
        }
        ValidationGet();
        MetadataService.GetEntityTypeIsAssociate().then(function () { });
        MetadataService.GettingChildEntityTypes(-1).then(function () { });
        $scope.workflowList = [];
        MetadataService.GetWorkFlowDetails().then(function () { });
        MetadataService.GetEntityType(-1).then(function (entitytypes) {
            $scope.entitytpesdata = entitytypes.Response;
        });
        var sentValidation = false;
        $scope.addEntityTypeAttribute = function () {
            $timeout(function () {
                $(".pick-a-color").pickAColor({
                    showSpectrum: false,
                    showSavedColors: false,
                    saveColorsPerElement: false,
                    fadeMenuToggle: true,
                    showAdvanced: true,
                    showHexInput: true,
                    showBasicColors: true
                });
                $scope.ColorCodeGlobal = 'ffffff';
                $timeout(function () {
                    $("#appendedPrependedDropdownButton").val($scope.ColorCodeGlobal);
                    $(".current-color").css('background-color', '#' + $scope.ColorCodeGlobal);
                }, 10);
            });
            $scope.attributegroupGridData = [];
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            $timeout(function () {
                $scope.EntType.ddlModuleID = 3
            }, 200);
            $scope.Category = 2;
            categoryid = 2;
            $scope.showStatusOptions = true;
            $scope.AdminTaskCheckList = [{
                ID: 0,
                StatusOptions: "",
                IsDeleted: false,
                SortOrder: 1,
                IsExisting: false
            }];
            $scope.attributerelationarr = [];
            $scope.step = 0;
            $scope.ColorCodeGlobal = '';
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.ParententityDropdowndata = [];
            $scope.entitytypeattributrearr = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableEntityfeatureAdd = true;
            $scope.EnableMultiselectFromParent = false;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.entitytypefeaturearr = [];
            $scope.EntityID = 0;
            $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.EntType = {
                EntityTypeCaption: "",
                EntityTypeDescription: "",
                ddlModuleID: 0,
                EntitytperelCaption: "",
                ddlAttributeID: 0,
                ddlSortorder: "",
                Defaultvalue: '',
                Inheritfromparent: false,
                Isreadonly: false,
                Choosefromparentonly: false,
                Isvalidationneeded: false,
                ddlEntityFeatureID: '',
                Shortdescription: '',
                Colourcode: '',
                ShowHideRootLevel: true,
                ShowHideInheritFromParent: true
            };
            $scope.entitytypeattributrearr = [];
            if (sentValidation == false) ValidEntitytypeattribute();
            $("#entitytypeformPage1").addClass('notvalidate');
            $timeout(function () {
                $('#EntityTypeCaption').focus();
            }, 1000);
            $scope.EntType.ShowHideRootLevel = true;
            $scope.EntType.ParentEntityType = true;
        };
        $scope.ColorCodeGlobal = '008000';
        $scope.LoadControl = function () {
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.entityattrelID = 0;
            $("#entitytypeformPage2").addClass('notvalidate');
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.AttributeOptionvalues = [];
            var ID = parseInt($scope.EntType.ddlAttributeID);
            MetadataService.GetAdminOptionListID(ID).then(function (GetAttributeOptions) {
                $scope.AttributeOptionvalues = GetAttributeOptions.Response;
                var attributeObj = $.grep($scope.attributes, function (e) {
                    return e.Id == parseInt($scope.EntType.ddlAttributeID);
                });
                $scope.attributeObjCaption = attributeObj[0].Caption;
                $scope.attributeObjAttributeTypeId = attributeObj[0].AttributeTypeID;
                $scope.EntType.EntitytperelCaption = attributeObj[0].Caption;
                if (attributeObj[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                    return false;
                }
                if (attributeObj[0].AttributeTypeID == 4) {
                    $scope.EnableMultiselectddl = true;
                    $scope.EnableMultiselectFromParent = true;
                }
                if (attributeObj[0].AttributeTypeID == 3) {
                    $scope.EnableMultiselectFromParent = true;
                    $scope.EnableSingleSelection = true;
                }
                if (attributeObj[0].AttributeTypeID == 6 || attributeObj[0].AttributeTypeID == 12) {
                    $scope.EnableMultiselectFromParent = true;
                }
                if (attributeObj[0].AttributeTypeID == 1 || attributeObj[0].AttributeTypeID == 2) {
                    $scope.Enableinputtxt = true;
                    $scope.EnablePlaceHolder = true;
                }
            });
        }
        $scope.editentitytypetableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetEntityTypeByID(row)" ng-show="row.entity.Category==2 || row.entity.Category==3 ||row.entity.Id == 5" data-toggle="modal" data-target="#entitytypeattributerelationModal"><i class="icon-edit"></i></a> ';
        $scope.entitytypedelete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteEntityTypeByID(row)" ng-show="(row.entity.Category==2 && row.entity.Id!=5 && IsOlderMetadataVersion.IsOlderVersion==true)" data-toggle="modal"><i class="icon-remove"></i></a> ';
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridentityattributerelation = {
            data: 'entitytpesdata',
            enablePinning: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "Id",
                displayName: 'Entity type id',
                width: 100
            }, {
                field: "Caption",
                displayName: 'Entity type name',
                width: 180
            }, {
                field: "Description",
                displayName: 'Description',
                width: 180
            }, {
                field: "ModuleCaption",
                displayName: 'Module name',
                width: 180
            }, {
                field: "IsRootLevel",
                displayName: 'Root level',
                width: 100
            }, {
                field: "IsAssociate",
                displayName: 'Associate',
                width: 100
            }, {
                field: "WorkFlowName",
                displayName: 'WorkFlow',
                width: 100,
                visible: false
            }, {
                field: "WorkFlowID",
                displayName: 'WorkFlowID',
                width: 100,
                visible: false
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editentitytypetableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.entitytypedelete,
                width: 40
            }]
        };
        var categoryid = '';
        $scope.GetEntityTypeByID = function GetEntityTypeByID() {
            var row;
            row = $.grep($scope.entitytpesdata, function (e) {
                return e.Id == parseInt(12);
            });
            if (row.Id == 12) {
                $scope.ShowForUserDetails = true;
                $scope.ShowForNormalEntities = false;
            } else {
                $scope.ShowForUserDetails = false;
                $scope.ShowForNormalEntities = true;
            }
            $scope.IsAssociate = row.IsAssociate;
            $scope.Category = 0;
            $scope.EntityID = row.Id;
            if (row.Category == 3) {
                $scope.showStatusOptions = false;
                $scope.Categorformatey = 0;
                categoryid = row.Category;
            } else {
                $scope.showStatusOptions = true;
                $scope.Category = 2;
            }
            if (row.Id == 5) {
                $scope.showStatusOptions = true;
                $scope.Category = 2;
            }
            $scope.step = 0;
            $scope.EntType.ddlParententitytypeId = [];
            $scope.EntType.ddlEntityFeatureID = '';
            $scope.ParententityDropdowndata = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.AttributeOptionvalues = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.entitytypeattributrearr = [];
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = '';
            $scope.EntType.ddlWorkFlow = '';
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.MultiselectDefaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.Choosefromparentonly = false;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EntType.IsRootLevel = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $timeout(function () {
                $(".pick-a-color").pickAColor({
                    showSpectrum: true,
                    showSavedColors: false,
                    saveColorsPerElement: true,
                    fadeMenuToggle: true,
                    showAdvanced: true,
                    showHexInput: true,
                    showBasicColors: true
                });
            }, 100);
            $scope.Features = [];
            $scope.myData = [];
            $scope.listdata = [];
            MetadataService.GetEntityTypeAttributeRelationByID(12).then(function (entityTypeAttributeRelation) {
                if (entityTypeAttributeRelation.Response != null) {
                    $scope.entityattributes = entityTypeAttributeRelation.Response;
                    $scope.count = 1;
                    $scope.validationcolorid = "";
                    MetadataService.GetUserVisiblity().then(function (uservisibleinfo) {
                        $scope.userVisbileData = uservisibleinfo.Response;
                        for (var i = 0; i < $scope.entityattributes.length; i++) {
                            var checked = false;
                            if ($scope.userVisbileData != null) {
                                var val = $.grep($scope.userVisbileData, function (e) {
                                    return e.AttributeId == $scope.entityattributes[i].AttributeID;
                                }).length;
                                if (val > 0) {
                                    checked = true;
                                }
                            }
                            if ($scope.AtributeValueList.length != 0) {
                                $scope.validationcolorid = $.grep($scope.AtributeValueList, function (e) {
                                    return e.AttributeID == parseInt($scope.entityattributes[i].AttributeID);
                                });
                            }
                            if ($scope.validationcolorid.length != 0) {
                                $scope.entitytypeattributrearr.push({
                                    "IsEnable": checked,
                                    "Id": $scope.entityattributes[i].ID,
                                    "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                    "ValidationID": 1,
                                    "SortOrder": $scope.entityattributes[i].SortOrder,
                                    "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                    "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                    "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                    "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                    "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                    "Caption": $scope.entityattributes[i].Caption,
                                    "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                    "ID": $scope.entityattributes[i].ID,
                                    "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                    "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                    "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                    "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                    "MinValue": $scope.entityattributes[i].MinValue,
                                    "MaxValue": $scope.entityattributes[i].MaxValue,
                                    "isvalidationset": 1,
                                    "IsHelptextEnabled": false,
                                    "HelptextDecsription":""
                                });
                            } else $scope.entitytypeattributrearr.push({
                                "IsEnable": checked,
                                "Id": $scope.entityattributes[i].ID,
                                "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                "ValidationID": 1,
                                "SortOrder": $scope.entityattributes[i].SortOrder,
                                "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                "Caption": $scope.entityattributes[i].Caption,
                                "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                "ID": $scope.entityattributes[i].ID,
                                "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                "MinValue": $scope.entityattributes[i].MinValue,
                                "MaxValue": $scope.entityattributes[i].MaxValue,
                                "isvalidationset": 0,
                                "IsHelptextEnabled": false,
                                "HelptextDecsription":""
                            });
                            $scope.globalentitytyperelation.entitytyperel.push({
                                "IsEnable": checked,
                                "Id": $scope.entityattributes[i].ID,
                                "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                "ValidationID": 1,
                                "SortOrder": $scope.entityattributes[i].SortOrder,
                                "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                "Caption": $scope.entityattributes[i].Caption,
                                "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                "ID": $scope.entityattributes[i].ID,
                                "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                "MinValue": $scope.entityattributes[i].MinValue,
                                "MaxValue": $scope.entityattributes[i].MaxValue
                            });
                            $scope.count = $scope.entityattributes[i].ID + 1;
                        }
                    });
                    $timeout(function () { DragDropAttributeGroup(); }, 200);
                }
            });
            $scope.EntType.EntityID = row.Id;
            $scope.EntType.EntityID = row.Id;
            $scope.EntType.EntityTypeCaption = row.Caption;
            $scope.EntType.EntityTypeDescription = row.Description;
            $scope.EntType.Shortdescription = row.ShortDescription;
            $scope.EntType.IsRootLevel = row.IsRootLevel;
            if (row.ColorCode != "Null") {
                $scope.ColorCodeGlobal = row.ColorCode;
            }
            $timeout(function () {
                $("#appendedPrependedDropdownButton").val($scope.ColorCodeGlobal);
                $(".current-color").css('background-color', '#' + $scope.ColorCodeGlobal);
            }, 500);
            var entityModule = $.grep($scope.modules, function (e) {
                return e.Id == row.ModuleID;
            });
            if (row.Id == 12) {
                $scope.EntType.ShowHideRootLevel = false;
                $scope.EntType.ParentEntityType = false;
            } else {
                $scope.EntType.ShowHideRootLevel = true;
                $scope.EntType.ParentEntityType = true;
            }
        };
        $scope.GetEntityTypeByID();
        $scope.globalentitytyperelation = {
            entitytyperel: []
        };
        $scope.DeleteEntityTypeByID = function DeleteEntityTypeByID(row) {
            if (row.entity.IsAssociate == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_1818.Caption'));
                return true;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_2082.Caption'), function (result) {
                if (result) {
                    var ID = row.entity.Id;
                    MetadataService.DeleteEntityType(ID).then(function (deleteentitytypebyId) {
                        if (deleteentitytypebyId.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4295.Caption'));
                        } else if (deleteentitytypebyId.StatusCode == 401) { } else {
                            var index = $scope.entitytpesdata.indexOf(row.entity)
                            $scope.entitytpesdata.splice(index, 1);
                            NotifySuccess($translate.instant('LanguageContents.Res_4251.Caption'));
                        }
                    });
                }
            });
        };
        $scope.GetAttributeByEntityTypeID = function GetEntityTypeByID(row) {
            if ($scope.IsAssociate == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_1819.Caption'));
                return true;
            }
            $scope.RowIndex = row.Id;
            $scope.sortorder = row.SortOrder;
            $scope.AttributeOptionvalues = [];
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableEntitytypeAttributeUpdate = true;
            $scope.EnableEntitytypeAttributeAdd = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = 0;
            $scope.AttributeOptionvalues = [];
            $scope.EntType.Inheritfromparent = row.InheritFromParent;
            $scope.EntType.Isreadonly = row.IsReadOnly;
            $scope.EntType.Choosefromparentonly = row.ChooseFromParentOnly;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EntityAttribureRowID = 0;
            $scope.attributeObjCaption = '';
            $scope.EntityAttribureRowID = row.ID;
            var ID = row.AttributeID;
            var entityAttribute = $.grep($scope.attributes, function (e) {
                return e.Id == row.AttributeID;
            });
            $scope.EntType.ddlAttributeID = entityAttribute[0].Id;
            $scope.attributeObjAttributeTypeId = entityAttribute[0].AttributeTypeID;
            MetadataService.GetAdminOptionListID(ID).then(function (GetAttributeOptions) {
                $scope.AttributeOptionvalues = GetAttributeOptions.Response;
                if (entityAttribute[0].AttributeTypeID == 4) {
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.EnableMultiselectddl = true;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    }
                    var multiselectarr = [];
                    $scope.EntType.MultiselectDefaultvalue = [];
                    var defaultmultiselectvalue = row.DefaultValue.split(',');
                    if (row.DefaultValue != "") {
                        for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                            $scope.EntType.MultiselectDefaultvalue.push(defaultmultiselectvalue[j]);
                        }
                    }
                } else if (entityAttribute[0].AttributeTypeID == 3) {
                    var AttributeSingleselectionVal = $.grep($scope.AttributeOptionvalues, function (e) {
                        return e.Id == parseInt(row.DefaultValue);
                    });
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.EnableSingleSelection = true;
                    }
                    $scope.EntType.Defaultvalue = '';
                } else if (entityAttribute[0].AttributeTypeID == 1 || row.AttributeTypeID == 2) {
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.Enableinputtxt = true;
                        $scope.EnablePlaceHolder = true;
                    }
                    $scope.EntType.TextDefaultvalue = '';
                    $scope.txtDefaultvalue = row.DefaultValue;
                    $scope.EntType.TextDefaultvalue = row.DefaultValue;
                    $scope.txtPlaceHolder = row.PlaceHolderValue;
                    $scope.EntType.PlaceHolderTextValue = row.PlaceHolderValue;
                }
            });
            if (entityAttribute[0].AttributeTypeID == 1 || entityAttribute[0].AttributeTypeID == 2) {
                if (entityAttribute[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                } else {
                    $scope.Enableinputtxt = true;
                    $scope.EnablePlaceHolder = true;
                }
            }
            if (entityAttribute[0].AttributeTypeID == 3 || entityAttribute[0].AttributeTypeID == 4 || entityAttribute[0].AttributeTypeID == 6 || entityAttribute[0].AttributeTypeID == 12) {
                if (entityAttribute[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                } else {
                    $scope.EnableMultiselectFromParent = true;
                }
            }
            if (row.AttributeTypeID == 1 || row.AttributeTypeID == 2) {
                $scope.EntType.TextDefaultvalue = row.DefaultValue;
                $scope.EntType.PlaceHolderTextValue = row.PlaceHolderValue;
            } else if (row.AttributeTypeID == 3) {
                $scope.EntType.Defaultvalue = row.DefaultValue;
            } else if (row.AttributeTypeID == 4) {
                $scope.EntType.MultiselectDefaultvalue = [];
                var arryDefaultVal = row.DefaultValue.split(",");
                for (var j = 0; j < arryDefaultVal.length; j++) {
                    $scope.EntType.MultiselectDefaultvalue.push(arryDefaultVal[j].toString());
                }
            } else {
                $scope.EntType.TextDefaultvalue = row.DefaultValue;
            }
            $scope.EntType.entityattrelID = row.AttributeID;
            $scope.EntType.EntitytperelCaption = row.Caption;
            $scope.EntType.Colourcode = row.ColourCode;
            $scope.EntType.Inheritfromparent = row.InheritFromParent;
            $scope.EntType.Isreadonly = row.IsReadOnly;
            $scope.EntType.Choosefromparentonly = row.ChooseFromParentOnly;
        };
        $scope.Attrs = [];
        $timeout(function () {
            MetadataService.GetAttribute().then(function (attribute) {
                var EntityStatusattribute = attribute.Response;
                $scope.attributes = attribute.Response;
                var tempattr = $.grep(attribute.Response, function (e) {
                    return (e.Id == 87);
                });
                $scope.attributeslist = $.grep(attribute.Response, function (e) {
                    return (e.Id != 71 && e.Id != 74 && e.Id != 75 && e.Id != 77);
                });
                $scope.attributeslistforuserdetails = $.grep(attribute.Response, function (e) {
                    return (e.Id != 71 && e.Id != 74 && e.Id != 75 && e.Id != 77 && e.AttributeTypeID != 7 && e.AttributeTypeID != 8 && e.AttributeTypeID != 10 && e.AttributeTypeID != 11 && e.AttributeTypeID != 6 && e.AttributeTypeID != 12 && e.IsSpecial != 1);
                });
                $.each($scope.attributeslistforuserdetails, function (i, el) {
                    $scope.Attrs.push({
                        "id": el.Id,
                        "text": el.Caption,
                    });
                });
                if (tempattr != undefined && tempattr.length > 0) {
                    $.each(tempattr, function (i, el) {
                        $scope.Attrs.push({
                            "id": el.Id,
                            "text": el.Caption,
                        });
                    });
                }
                $scope.optionsSrc = $scope.Attrs;
                $scope.formatAttribute = function (item) {
                    return "<span style=\"background-color: #ccc; margin-top: -1px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
                };
                $scope.formatAttributeSelection = function (item) {
                    if ($scope.EntType.ddlAttributeID != "") return "<span style=\"background-color: #ccc; margin-top: -2px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
                };
                $scope.attributeOptionsConfig = {
                    formatResult: $scope.formatAttribute,
                    formatSelection: $scope.formatAttributeSelection
                };
            });
        }, 500);
        $scope.count = 1;
        $scope.formatAttribute = function (item) {
            return "<span style=\"background-color: #ccc; margin-top: -1px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
        };
        $scope.formatAttributeSelection = function (item) {
            if ($scope.EntType.ddlAttributeID != "") return "<span style=\"background-color: #ccc; margin-top: -2px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
        };
        $scope.attributeOptionsConfig = {
            formatResult: $scope.formatAttribute,
            formatSelection: $scope.formatAttributeSelection
        };
        $scope.entitytypeattributerel = function () {
            $("#btnTemp1").click();
            $("#entitytypeformPage2").removeClass('notvalidate');
            if ($("#entitytypeformPage2 .error").length > 0) {
                return false;
            }
            $timeout(function () {
                applyAlternateColor();
            }, 100);
            var Defaultvalue = '';
            var Placeholder = '';
            if ($scope.attributeObjAttributeTypeId == 1 || $scope.attributeObjAttributeTypeId == 2) {
                Defaultvalue = $scope.EntType.TextDefaultvalue == null ? "" : $scope.EntType.TextDefaultvalue;
                Placeholder = $scope.EntType.PlaceHolderTextValue == null ? "" : $scope.EntType.PlaceHolderTextValue;
            } else if ($scope.attributeObjAttributeTypeId == 3) {
                Defaultvalue = $scope.EntType.Defaultvalue == null ? "" : $scope.EntType.Defaultvalue.toString();
            } else if ($scope.attributeObjAttributeTypeId == 4) {
                Defaultvalue = $scope.EntType.MultiselectDefaultvalue == null ? "" : $scope.EntType.MultiselectDefaultvalue.toString();
            } else {
                Defaultvalue = $scope.EntType.TextDefaultvalue == null ? "" : $scope.EntType.TextDefaultvalue;
            }
            if ($scope.EntType.EntitytperelCaption == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1820.Caption'));
                return false;
            }
            if ($scope.EntType.ddlAttributeID == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_1821.Caption'));
                return false;
            }
            if ($scope.EntType.entityattrelID != 0 && $scope.EntType.entityattrelID != undefined) {
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeID = $scope.EntType.ddlAttributeID;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ValidationID = 1;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].SortOrder = $scope.sortorder;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].DefaultValue = Defaultvalue;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].PlaceHolderValue = Placeholder;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].InheritFromParent = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].InheritFromParent;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsReadOnly = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsReadOnly;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ChooseFromParentOnly = $scope.EntType.Choosefromparentonly;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsValidationNeeded = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsValidationNeeded, $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].Caption = $scope.EntType.EntitytperelCaption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsSystemDefined = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsSystemDefined, $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ID = $scope.EntityAttribureRowID;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].EntityTypeName = $scope.EntType.EntityTypeCaption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeName = $.grep($scope.attributes, function (e) {
                    return e.Id == $scope.EntType.entityattrelID;
                })[0].Caption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeTypeID = $scope.attributeObjAttributeTypeId;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeCaption = $.grep($scope.attributes, function (e) {
                    return e.Id == $scope.EntType.entityattrelID;
                })[0].Caption;
                $scope.EnableEntitytypeAttributeUpdate = false;
                $scope.EnableEntitytypeAttributeAdd = true;
                $scope.EntType.entityattrelID = 0;
            } else {
                if ($.grep($scope.entitytypeattributrearr, function (e) {
					return e.AttributeID == $scope.EntType.ddlAttributeID;
                }).length > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_2080.Caption'));
                    $scope.AttributeMultiselectDefaultValue = '';
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                    $scope.EnableSingleSelection = false;
                    $scope.EnableMultiselectddl = false;
                    $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
                    $scope.attributeObjCaption = '';
                    $scope.EnableMultiselectFromParent = false;
                    return false;
                }
                var sortorder = 0;
                for (var i = 0; i < $scope.entitytypeattributrearr.length; i++) {
                    if ($scope.entitytypeattributrearr[i].SortOrder > sortorder) {
                        sortorder = $scope.entitytypeattributrearr[i].SortOrder;
                    }
                }
                $scope.entitytypeattributrearr.push({
                    "Id": $scope.count,
                    "AttributeID": parseInt($scope.EntType.ddlAttributeID),
                    "ValidationID": 1,
                    "SortOrder": sortorder + 1,
                    "DefaultValue": Defaultvalue,
                    "InheritFromParent": $scope.EntType.Inheritfromparent,
                    "IsReadOnly": $scope.EntType.Isreadonly,
                    "ChooseFromParentOnly": $scope.EntType.Choosefromparentonly,
                    "IsValidationNeeded": true,
                    "Caption": $scope.EntType.EntitytperelCaption,
                    "IsSystemDefined": false,
                    "ID": 0,
                    "EntityTypeName": $scope.EntType.EntityTypeCaption,
                    "AttributeTypeID": $scope.attributeObjAttributeTypeId,
                    "AttributeCaption": $scope.attributeObjCaption,
                    "PlaceHolderValue": Placeholder,
                    "MinValue": 0,
                    "MaxValue": 0,
                    "IsHelptextEnabled": false,
                    "HelptextDecsription": ''
                });
                $scope.AttributeIDList.push($scope.EntType.ddlAttributeID);
            }
            var saveAttribute = {};
            saveAttribute.EntityTypeID = 12;
            saveAttribute.AttributeData = $scope.entitytypeattributrearr;
            MetadataService.EntityTypeAttributeRelation(saveAttribute).then(function (EntityAttributeResponse) {
                $scope.entitytypeattributrearr = [];
                MetadataService.GetEntityTypeAttributeRelationByID(12).then(function (entityTypeAttributeRelation) {
                    if (entityTypeAttributeRelation.Response != null) {
                        $scope.entityattributes = entityTypeAttributeRelation.Response;
                        $scope.count = 1;
                        $scope.validationcolorid = "";
                        MetadataService.GetUserVisiblity().then(function (uservisibleinfo) {
                            $scope.userVisbileData = uservisibleinfo.Response;
                            for (var i = 0; i < $scope.entityattributes.length; i++) {
                                var checked = false;
                                if ($scope.userVisbileData != null) {
                                    var val = $.grep($scope.userVisbileData, function (e) {
                                        return e.AttributeId == $scope.entityattributes[i].AttributeID;
                                    }).length;
                                    if (val > 0) {
                                        checked = true;
                                    }
                                }
                                if ($scope.AtributeValueList.length != 0) {
                                    $scope.validationcolorid = $.grep($scope.AtributeValueList, function (e) {
                                        return e.AttributeID == parseInt($scope.entityattributes[i].AttributeID);
                                    });
                                }
                                if ($scope.validationcolorid.length != 0) {
                                    $scope.entitytypeattributrearr.push({
                                        "IsEnable": checked,
                                        "Id": $scope.entityattributes[i].ID,
                                        "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                        "ValidationID": 1,
                                        "SortOrder": $scope.entityattributes[i].SortOrder,
                                        "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                        "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                        "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                        "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                        "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                        "Caption": $scope.entityattributes[i].Caption,
                                        "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                        "ID": $scope.entityattributes[i].ID,
                                        "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                        "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                        "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                        "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                        "MinValue": $scope.entityattributes[i].MinValue,
                                        "MaxValue": $scope.entityattributes[i].MaxValue,
                                        "isvalidationset": 1,
                                        "IsHelptextEnabled": false,
                                        "HelptextDecsription": ''
                                    });
                                } else $scope.entitytypeattributrearr.push({
                                    "IsEnable": checked,
                                    "Id": $scope.entityattributes[i].ID,
                                    "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                    "ValidationID": 1,
                                    "SortOrder": $scope.entityattributes[i].SortOrder,
                                    "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                    "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                    "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                    "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                    "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                    "Caption": $scope.entityattributes[i].Caption,
                                    "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                    "ID": $scope.entityattributes[i].ID,
                                    "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                    "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                    "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                    "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                    "MinValue": $scope.entityattributes[i].MinValue,
                                    "MaxValue": $scope.entityattributes[i].MaxValue,
                                    "isvalidationset": 0,
                                    "IsHelptextEnabled": false,
                                    "HelptextDecsription": ''
                                });
                                $scope.globalentitytyperelation.entitytyperel.push({
                                    "IsEnable": checked,
                                    "Id": $scope.entityattributes[i].ID,
                                    "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                    "ValidationID": 1,
                                    "SortOrder": $scope.entityattributes[i].SortOrder,
                                    "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                    "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                    "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                    "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                    "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                    "Caption": $scope.entityattributes[i].Caption,
                                    "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                    "ID": $scope.entityattributes[i].ID,
                                    "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                    "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                    "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                    "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                    "MinValue": $scope.entityattributes[i].MinValue,
                                    "MaxValue": $scope.entityattributes[i].MaxValue
                                });
                                $scope.count = $scope.entityattributes[i].ID + 1;
                            }
                        });
                        $timeout(function () { DragDropAttributeGroup(); }, 200);
                    }
                });
            });
            $scope.AttributeMultiselectDefaultValue = '';
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
            $scope.attributeObjCaption = '';
            $scope.EnableMultiselectFromParent = false;
            $scope.count = $scope.count + 1;
        };
        $scope.clearattribtue = function clearattribtue() {
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = 0;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.EnableEntitytypeAttributeUpdate = false;
        }
        $scope.DeleteAttributeByEntityTypeID = function DeleteAttributeByEntityTypeID(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_1824.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = row.attribs.Id;
                        var attrDelObj = $.grep($scope.entitytypeattributrearr, function (e) {
                            return e.Id == ID;
                        });
                        $scope.entitytypeattributrearr.splice($.inArray(attrDelObj[0], $scope.entitytypeattributrearr), 1);
                        $timeout(function () {
                            applyAlternateColor();
                        }, 100);
                        MetadataService.DeleteEntityAttributeRelation(ID).then(function (deleteentityAttributeByTypeId) {
                            if (deleteentityAttributeByTypeId.SourceCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_5502.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_5501.Caption'));
                                var IsEnable = 0;
                                var updatefeature = {};
                                updatefeature.IsEnable = IsEnable;
                                updatefeature.AttributeId = row.attribs.AttributeID;
                                MetadataService.InsertUpdateUserVisibleInfo(updatefeature).then(function (updatemodulesettings) { });
                            }
                        });
                        $scope.AttributeMultiselectDefaultValue = '';
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                        $scope.EnableSingleSelection = false;
                        $scope.EnableMultiselectddl = false;
                        $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
                        $scope.attributeObjCaption = '';
                        $scope.EnableEntitytypeAttributeUpdate = false;
                        $scope.EnableEntitytypeAttributeAdd = true;
                        $scope.EntType.entityattrelID = 0;
                        $scope.EnableMultiselectFromParent = false;
                    }, 100);
                }
            });
        };
        $scope.UpdateInhertReadOnly = function (row, Typeofcolumn) {
            if (Typeofcolumn == "Inherit") $.grep($scope.entitytypeattributrearr, function (e) {
                return e.Id == parseInt(row.Id);
            })[0].InheritFromParent = row.InheritFromParent;
            else if (Typeofcolumn == "ReadOnly") $.grep($scope.entitytypeattributrearr, function (e) {
                return e.Id == parseInt(row.Id);
            })[0].IsReadOnly = row.IsReadOnly;
            var saveAttribute = {};
            saveAttribute.EntityTypeID = 12;
            saveAttribute.AttributeData = $scope.entitytypeattributrearr;
            MetadataService.EntityTypeAttributeRelation(saveAttribute).then(function (EntityAttributeResponse) {
                if (EntityAttributeResponse.Response != null && EntityAttributeResponse.StatusCode == 200) NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                else NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                $timeout(function () { DragDropAttributeGroup(); }, 200);
            });
        }
        $scope.$watch("entitytypeattributrearr", function (value) {
            $timeout(function () {
                applyAlternateColor();
            }, 100);
        }, true);

        function applyAlternateColor() {
            var flag = true;
            var backgroundcolor = "#F3F3F3";
            $(".entypeAttrbtTableContent > li").each(function (i, val) {
                if (parseInt($(this).data('attributeid')) !== 71 && parseInt($(this).data('attributeid')) !== 74 && parseInt($(this).data('attributeid')) !== 75) {
                    if (flag) {
                        backgroundcolor = "#F3F3F3";
                        flag = false;
                    } else {
                        backgroundcolor = "#FDFDFD";
                        flag = true;
                    }
                    $(this).css("background", backgroundcolor);
                }
            });
        }
        $scope.childValidation = [];
        var RelationID = '';
        $scope.Validate = function AttributeValidate(attrbs) {
            $("#ValidateAttribute").modal('show');
            ClearAttributeValidation();
            ValidationGet();
            $timeout(function () {
                $scope.AttributeID = attrbs.AttributeID;
                $scope.AttributeTypeID = attrbs.AttributeTypeID;
                $scope.AtributeValidationList = [];
                $scope.childValidation = [];
                $scope.childValidation = $.grep($scope.AtributeValueList, function (n, i) {
                    return ($scope.AtributeValueList[i].RelationShipID == attrbs.ID);
                });
                RelationID = '';
                RelationID = attrbs.ID;
                var tempList = $scope.childValidation;
            }, 100);
        };
        $scope.SaveValidation = function SaveValidation() {
            var attrVal = {};
            attrVal.EntityTypeID = 12;
            attrVal.AttributeID = $scope.AttributeID;
            attrVal.AttributeTypeID = $scope.AttributeTypeID;
            attrVal.AttributeValidationList = $scope.AtributeValueList;
            MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                if (AttrValditeResult.Response != null) {
                    ClearAttributeValidation();
                    $scope.AtributeValueList = [];
                }
            });
            var validationset = $.grep($scope.AtributeValueList, function (e) {
                return e.AttributeID == parseInt(attrVal.AttributeID);
            });
            if (validationset.length != 0) {
                if ($('#att' + attrVal.AttributeID).find('.icon-ok').hasClass('ng-hide') == true) {
                    $('#att' + attrVal.AttributeID).find('.icon-ok').removeClass('ng-hide');
                    $('#att' + attrVal.AttributeID).find('.color-primary').addClass('ng-hide');
                } else $('#validation' + attrVal.AttributeID).removeClass('color-primary');
            } else {
                if ($('#att' + attrVal.AttributeID).find('.color-primary').hasClass('ng-hide') == true) {
                    $('#att' + attrVal.AttributeID).find('.icon-ok').addClass('ng-hide');
                    $('#att' + attrVal.AttributeID).find('.color-primary').removeClass('ng-hide');
                } else $('#validation' + attrVal.AttributeID).addClass('color-primary');
            }
        }

        function ControlReset() {
            $scope.ParententityDropdowndata = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.entitytypeattributrearr = [];
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = 0;
            $scope.EntType.ddlWorkFlow = 0;
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.MultiselectDefaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.Choosefromparentonly = false;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntityHierarchyTypesResult = [];
            $scope.entitytpesdata = [];
            $scope.Fetureattribues = [];
            $(".pick-a-color")[0].attributes[0].value = '';
            $(".pick-a-color").pickAColor({
                showSpectrum: true,
                showSavedColors: false,
                saveColorsPerElement: true,
                fadeMenuToggle: true,
                showAdvanced: true,
                showHexInput: true,
                showBasicColors: true
            });
            $scope.Features = [];
        }
        $scope.UpdateUserVisibility = function (row) {
            var countchecked = 0;
            for (var k = 0; k < $(".checkForCount").length; k++) {
                if ($(".checkForCount")[k].checked == true) {
                    countchecked = countchecked + 1;
                }
            }
            if (countchecked > 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_2081.Caption'));
                $(".checkForCount")[row.SortOrder - 1].checked = false;
                return false;
            }
            var IsEnable = $(".checkForCount")[this.$index].checked;
            if (IsEnable == false) {
                IsEnable = 0;
            } else {
                IsEnable = 1;
            }
            var updatefeature = {};
            updatefeature.IsEnable = IsEnable;
            updatefeature.AttributeId = row.AttributeID;
            MetadataService.InsertUpdateUserVisibleInfo(updatefeature).then(function (updatemodulesettings) {
                if (updatemodulesettings.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                }
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.userdetailsCtrl']"));
        });
        $scope.ValiadtionOptions = [{
            key: "presence",
            Value: "Presence"
        }, {
            key: "max-length",
            Value: "Max-Length"
        }, {
            key: "min-length",
            Value: "min-length:Number"
        }, {
            key: "between",
            Value: "Between:Number"
        }, {
            key: "integer",
            Value: "Integer"
        }];
        $scope.AtributeValueList = [];
        $scope.ValidationAdd = function AttributeValidationAdd() {
            if ($scope.ValidationType.length > 0 && $scope.validationErorr.length > 0) {
                if ($scope.Currentindex == -1) {
                    $scope.AtributeValueList.push({
                        Id: 0,
                        Name: "",
                        EntityTypeID: 0,
                        RelationShipID: RelationID,
                        ValueType: $scope.ValidationType,
                        Value: $scope.ValidationValue,
                        ErrorMessage: $scope.validationErorr,
                        AttributeID: $scope.AttributeID
                    });
                    $scope.childValidation.push({
                        Id: 0,
                        Name: "",
                        EntityTypeID: 0,
                        RelationShipID: RelationID,
                        ValueType: $scope.ValidationType,
                        Value: $scope.ValidationValue,
                        ErrorMessage: $scope.validationErorr,
                        AttributeID: $scope.AttributeID
                    });
                } else {
                    $scope.AtributeValueList[$scope.Currentindex].ValueType = $scope.ValidationType
                    $scope.AtributeValueList[$scope.Currentindex].Value = $scope.ValidationValue;
                    $scope.AtributeValueList[$scope.Currentindex].ErrorMessage = $scope.validationErorr;
                    $scope.childValidation[$scope.Currentindex].ValueType = $scope.ValidationType
                    $scope.childValidation[$scope.Currentindex].Value = $scope.ValidationValue;
                    $scope.childValidation[$scope.Currentindex].ErrorMessage = $scope.validationErorr;
                    $scope.Currentindex = -1;
                }
                ClearAttributeValidation();
            }
        };
        $scope.EnableValidationSave = true;
        $scope.EnableValidationUpdate = false;
        $scope.DeleteValidationAttributeByID = function (Index) {
            $scope.tempIndex = Index;
            if ($scope.AtributeValueList[Index].Id > 0) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                    if (result) {
                        MetadataService.DeleteAttributeValidation($scope.AtributeValueList[Index].Id).then(function (deleteByID) {
                            if (deleteByID.Response != null && deleteByID.Response == true) {
                                $scope.AtributeValueList.splice($scope.tempIndex, 1);
                                $scope.childValidation.splice($scope.tempIndex, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                            }
                        });
                    }
                });
            }
        };

        function ClearAttributeValidation() {
            $scope.ValidationType = "";
            $scope.ValidationValue = "";
            $scope.validationErorr = "";
        }
        $scope.Currentindex = -1;
        $scope.GetEntityAttributeByIndex = function (index) {
            $scope.ValidationType = $scope.AtributeValueList[index].ValueType;
            $scope.ValidationValue = $scope.AtributeValueList[index].Value;
            $scope.validationErorr = $scope.AtributeValueList[index].ErrorMessage;
            $scope.Currentindex = index;
        }
        var DragDropAttributeGroup = function () {
            $('#entitytypeattrrelationlist').tableDnD({
                onDrop: function (table, row) {
                    $scope.DragDropAttribteGroupvalues = table.tBodies[0].rows;;
                    var value = 1;
                    for (var i = 0; i < table.tBodies[0].rows.length; i++) {
                        if (table.tBodies[0].rows[i].id != 0 && table.tBodies[0].rows[i].id != 74 && table.tBodies[0].rows[i].id != 75) {
                            $.grep($scope.entitytypeattributrearr, function (e) {
                                return e.AttributeID == parseInt(table.tBodies[0].rows[i].id);
                            })[0].SortOrder = value;
                            value++;
                        }
                    }
                    $scope.entitytypeattributrearr.sort(function (a, b) {
                        return a.SortOrder - b.SortOrder
                    })
                    var saveAttribute = {};
                    saveAttribute.EntityTypeID = 12;
                    saveAttribute.AttributeData = $scope.entitytypeattributrearr;
                    MetadataService.EntityTypeAttributeRelation(saveAttribute).then(function (res) { });
                }
            });
        };
        $scope.ValidationValueCheck = function () {
            var selectvalue = $scope.ValidationType;
            if ($scope.ValidationType == "presence" || $scope.ValidationType == "integer" || $scope.ValidationType == "float" || $scope.ValidationType == "same-as:Selector" || $scope.ValidationType == "email") {
                $scope.ValidationValue = '';
                $scope.ShowValue = false;
            } else {
                $scope.ShowValue = true;
            }
        }
        $scope.closevalidationpopup = function (attr) {
            var validationset = $.grep($scope.AtributeValueList, function (e) {
                return e.AttributeID == parseInt(attr.AttributeID);
            });
            if (validationset.length != 0) {
                if ($('#att' + attr.AttributeID).find('.icon-ok').hasClass('ng-hide') == true) {
                    $('#att' + attr.AttributeID).find('.icon-ok').removeClass('ng-hide');
                    $('#att' + attr.AttributeID).find('.color-primary').addClass('ng-hide');
                } else $('#validation' + attr.AttributeID).removeClass('color-primary');
            } else {
                if ($('#att' + attr.AttributeID).find('.color-primary').hasClass('ng-hide') == true) {
                    $('#att' + attr.AttributeID).find('.icon-ok').addClass('ng-hide');
                    $('#att' + attr.AttributeID).find('.color-primary').removeClass('ng-hide');
                } else $('#validation' + attr.AttributeID).addClass('color-primary');
            }
        }
    }
    app.controller("mui.admin.userdetailsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'MetadataService', muiadminuserdetailsCtrl]);
})(angular, app);