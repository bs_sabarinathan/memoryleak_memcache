﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;


namespace BrandSystems.Marcom.Core.Dam
{
    internal class OptimakerSettings : IOptimakerSettings
    {
         #region "Member variables"

        protected int _id;
        protected string _name;
        protected int _categoryid;
        protected string _description;
        protected int _docid;
        protected int _deptid;
        protected int _doctype;
        protected int _docversionid;
        protected string _previewimage;
        protected bool _autoapproval;
        protected int _templateEngineType;
        protected int _assetType;
        protected string _serverURL;
        protected bool _enableHighResPdf;
        #endregion

        #region "Constructor"

        public OptimakerSettings() { }


        public OptimakerSettings(int pid, string pname, int pcategoryid, string pdescription, int pdocid, int pdepid, int pdoctype, int pdocversionid, string ppreviewimage, bool pautoapproval, int pTemplateEngineType, int pAssetType, string pServerURL, bool pEnableHighResPDF)
        {
            this._id = pid;
            this._name = pname;
            this._categoryid = pcategoryid;
            this._description = pdescription;
            this._docid = pdocid;
            this._deptid = pdepid;
            this._doctype = pdoctype;
            this._docversionid = pdocversionid;
            this._previewimage = ppreviewimage;
            this._autoapproval = pautoapproval;
            this._templateEngineType = pTemplateEngineType;
            this._assetType = pAssetType;
            this._serverURL = pServerURL;
            this._enableHighResPdf = pEnableHighResPDF;
        }

        #endregion


        #region "Public propertiers"

        public  int ID
        {
            get { return _id; }
            set { _id = value; }

        }

       

        public  string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }



        public  int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }

        }

         public  int DocID
        {
            get { return _docid; }
            set { _docid = value; }

        }


        public  int DepartmentID
        {
            get { return _deptid; }
            set { _deptid = value; }

        }

        public  int DocType
        {
            get { return _doctype; }
            set { _doctype = value; }

        }


        public  int DocVersionID
        {
            get { return _docversionid; }
            set { _docversionid = value; }

        }


        public  string PreviewImage
        {
            get { return _previewimage; }
            set { _previewimage = value; }

        }
        public bool AutoApproval
        {
            get { return _autoapproval; }
            set { _autoapproval = value; }

        }
        public int TemplateEngineType
        {
            get { return _templateEngineType; }
            set { _templateEngineType = value; }
        }
        public int AssetType
        {
            get { return _assetType; }
            set { _assetType = value; }
        }
        public string ServerURL
        {
            get { return _serverURL; }
            set { _serverURL = value; }
        }

        public bool EnableHighResPDF
        {
            get { return _enableHighResPdf; }
            set { _enableHighResPdf = value; }
        }

        #endregion


        
    }
}
