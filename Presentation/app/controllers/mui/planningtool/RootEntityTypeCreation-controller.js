﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolRootEntityTypeCreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $translate, $compile, RootentitytypecreationService, $modalInstance, params) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }
        var timeoutvariableforentitycreation = {};
        $scope.listofValidations = [];
        $scope.GetNonBusinessDaysforDatePicker();
        var oldpopup = null;
        $scope.Calanderopen = function ($event, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[oldpopup] = false;
            $scope[Call_from] = true;
            oldpopup = Call_from;
        }
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        $scope.treeCategory = [];
        var remValtimer = "";
        $scope.DynamicAddValidation = function (attrID) {
            var IsValExist = $.grep($scope.listValidationResult, function (e) {
                return parseInt(e[0].split('_')[1]) == attrID;
            });
            if (IsValExist == null || IsValExist.length == 0) {
                var getVal = $.grep($scope.listofValidations, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });
                if (getVal != null && getVal.length > 0) {
                    $scope.listValidationResult.push(getVal[0]);
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer) $timeout.cancel(remValtimer);
                    remValtimer = $timeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTemp',
                            'silentSubmit': 'true'
                        });
                    }, 10);
                }
            }
        }
        $scope.DynamicRemoveValidation = function (attrID) {
            if ($scope.listValidationResult != undefined) {
                var IsValExist = $.grep($scope.listValidationResult, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });
                if (IsValExist != null && IsValExist.length > 0) {
                    $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                        return parseInt(e[0].split('_')[1]) != attrID;
                    });
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer) $timeout.cancel(remValtimer);
                    remValtimer = $timeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTemp',
                            'silentSubmit': 'true'
                        });
                    }, 10);
                }
            }
        }
        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            var Attributetypename = '';
            var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
            });
            var ID = attrID.split("_");
            if (relationobj != undefined) {
                if (relationobj.length > 0) {
                    for (var i = 0; i <= relationobj.length - 1; i++) {
                        Attributetypename = '';
                        if (relationobj[i].AttributeTypeID == 3) {
                            Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                        } else if (relationobj[i].AttributeTypeID == 4) {
                            Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                        } else if (relationobj[i].AttributeTypeID == 6) {
                            Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        } else if (relationobj[i].AttributeTypeID == 7) {
                            Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                        } else if (relationobj[i].AttributeTypeID == 12) {
                            Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        }
                        if ($scope.fields[Attributetypename] == undefined) {
                            if (attrTypeID == 1) {
                                $scope.fields["TextSingleLine_" + ID[0]] = "";
                            } else if (attrTypeID == 2) {
                                $scope.fields["TextMultiLine_" + ID[0]] = "";
                            } else if (attrTypeID == 3) {
                                $scope.fields["ListSingleSelection_" + ID[0]] = "";
                            } else if (attrTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + ID[0]] = "";
                            } else if (attrTypeID == 5) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            } else if (attrTypeID == 6) {
                                $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                            } else if (attrTypeID == 12) {
                                $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                            } else if (attrTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                            }
                            continue;
                        }
                        if (relationobj[i].AttributeTypeID == 4) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                } catch (e) { };
                                return true;
                            }
                        } else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                            if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                } catch (e) { };
                                return true;
                            }
                        } else if (relationobj[i].AttributeTypeID == 7) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                } catch (e) { };
                                return true;
                            }
                        } else {
                            if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                } catch (e) { };
                                return true;
                            }
                        }
                    }
                    if (attrTypeID == 1) {
                        $scope.fields["TextSingleLine_" + ID[0]] = "";
                    } else if (attrTypeID == 2) {
                        $scope.fields["TextMultiLine_" + ID[0]] = "";
                    } else if (attrTypeID == 3) {
                        $scope.fields["ListSingleSelection_" + ID[0]] = "";
                    } else if (attrTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + ID[0]] = "";
                    } else if (attrTypeID == 5) {
                        $scope.fields["DatePart_" + ID[0]] = null;
                    } else if (attrTypeID == 6) {
                        $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                    } else if (attrTypeID == 12) {
                        $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                    } else if (attrTypeID == 17) {
                        $scope.fields["ListTagwords_" + ID[0]] = [];
                    }
                    try {
                        $scope.DynamicRemoveValidation(parseInt(ID[0]));
                    } catch (e) { };
                    return false;
                } else return true;
            } else return true;
        };
        $scope.EntityStatusResult = [];
        $scope.tagwordids = [];
        $scope.Dropdown = {};
        $scope.treeSourcesObj = [];
        $scope.treeSources = {};
        $scope.IsToShowRootEntity = false;
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.fields = {
            usersID: ''
        };
        $scope.varfields = {
            CostcenterInfo: []
        }
        $scope.dynamicEntityValuesHolder = {};
        $scope.fieldKeys = [];
        $scope.costcemtreObject = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.OptionObj = {};
        $scope.rootEntityID = '';
        $scope.rootEntityCaption = '';
        $scope.ImageFileName = '';
        $scope.metadatano = 1;
        $scope.financialno = 2;
        $scope.memberno = 3;
        GetEntityTypeTabCollections();
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttr").click();
        }
        $scope.attributeGroupDetailAsTab = [];
        $scope.attributeGroupAddedValues = {};

        function StrartUpload_UploaderAttr() {
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $('.moxie-shim').remove();
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_Attr.bind('Init', function (up, params) { });
                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }
        var uploaderAttrInfo = [];
        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + extension;
            uploaderAttrInfo.push({ attrID: $scope.UploadAttributeId, attrFileName: ImageFileName });
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#overviewUplaodImagediv').modal('hide');
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + ImageFileName);
            if ($("#Uploader_" + $scope.UploadAttributeId).parent().parent().hasClass('error') == true) {
                if (uploaderAttrInfo.length > 0) {                   
                        $("#Uploader_" + $scope.UploadAttributeId).parent().parent().removeClass("error");
                        $("#Uploader_" + $scope.UploadAttributeId).next().hide();                    
                }
            }
        }

        function GetEntityTypeTabCollections() {
            RootentitytypecreationService.GetPlantabsettings().then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.ShowFinancial = gettabresult.Response.Financials;
                    if ($scope.ShowFinancial == "false") {
                        $scope.memberno = 2;
                        $scope.ShowFinancial = false;
                    }
                }
            });
        }
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.dyn_Cont = '';
        $scope.items = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: null,
                    endDate: null,
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };

        $scope.changeval = function (attrid) {
            if ($scope.fields["ListMultiSelection_" + attrid].length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().addClass('icon-asterisk validationmark margin-right5x');
                    $("#EntityMetadata").html($compile());
                }
            }
            else {
                var ID = "ListMultiSelection_" + attrid;
                $("#" + ID).next().find('.multiselect-container').next().removeAttr("style");
                $("#" + ID).next().find('.multiselect-container').next().css({ 'color': '#B94A48' });
            }
        }
        $scope.changevalues = function (attrTypeid, attrid) {
            if (attrid == 12) {
                var MultiselectId = "TreeMultiSelection_" + attrTypeid + "_" + 1;
                $("#EntityMetadata .error").find('#' + MultiselectId).next().removeAttr("style");
                $("#EntityMetadata .error").find('#' + MultiselectId).next().addClass('icon-asterisk validationmark margin-right5x');
                if ($("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().hasClass('error') == true)
                    $("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().removeClass('error');
            }
        }
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $("#rootLevelEntity").on("onRootEntityCreation", function (event, id) {
            $("#btnWizardFinish").removeAttr('disabled');
            $('#MyWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $scope.ClearModelObject($scope.fields);
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $scope.OwnerList.push({
                "Roleid": 1,
                "RoleName": "Owner",
                "UserEmail": $scope.ownerEmail,
                "DepartmentName": "-",
                "Title": "-",
                "Userid": parseInt($scope.OwnerID, 10),
                "UserName": $scope.OwnerName,
                "IsInherited": '0',
                "InheritedFromEntityid": '0'
            });
        });
        $scope.treePreviewObj = {};
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            $scope.$apply();
        }
        $scope.costCenterList = [];
        RootentitytypecreationService.GetCostcentreforEntityCreation(($scope.rootEntityID) == "" ? '0' : ($scope.rootEntityID), 0, 0).then(function (costCenterList) {
            $scope.varfields.CostcenterInfo = [];
            $scope.costCenterList = costCenterList.Response;
        });
        $scope.MemberLists = [];
        $scope.tempCount = 1;
        $scope.addcostCentre = function () {
            if ($scope.varfields.CostcenterInfo.length > 0 && $scope.varfields.CostcenterInfo != null) {
                $.each($scope.varfields.CostcenterInfo, function (val, item) {
                    var result = $.grep($scope.costcemtreObject, function (e) {
                        return e.CostcenterId == item;
                    });
                    if (result.length == 0) {
                        var costCentrevalues = $.grep($scope.costCenterList, function (e) {
                            return e.Id == parseInt(item)
                        })[0];
                        $scope.costcemtreObject.push({
                            "CostcenterId": item,
                            "costcentername": costCentrevalues.costcentername,
                            "Sortorder": 1,
                            "Isassociate": 1,
                            "Isactive": 1,
                            "OwnerName": costCentrevalues.username,
                            "OwnerID": costCentrevalues.UserID
                        });
                        var memberObj = $.grep($scope.MemberLists, function (e) {
                            return e.Roleid == costCentrevalues.RoleID && e.Userid == costCentrevalues.UserID;
                        });
                        if (memberObj.length == 0) {
                            $scope.MemberLists.push({
                                "TID": $scope.count,
                                "UserEmail": costCentrevalues.usermail,
                                "DepartmentName": costCentrevalues.Designation,
                                "Title": costCentrevalues.Title,
                                "Roleid": costCentrevalues.RoleID,
                                "RoleName": 'BudgetApprover',
                                "Userid": costCentrevalues.UserID,
                                "UserName": costCentrevalues.username,
                                "IsInherited": '0',
                                "InheritedFromEntityid": '0',
                                "FromGlobal": 1,
                                "CostCentreID": item
                            });
                            $scope.count = $scope.count + 1;
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1919.Caption'));
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
            }
            $scope.varfields.CostcenterInfo.splice(0, $scope.varfields.CostcenterInfo.length);
            if ($scope.treeCategory.length > 0) $scope.RecursiveCostCentreTreeClearChecked($scope.treeCategory);
        }
        $scope.RecursiveCostCentreTreeClearChecked = function (Treeval) {
            $.each(Treeval, function (val, item) {
                item.ischecked = false;
                if (item.Children.length > 0) {
                    $scope.RecursiveCostCentreTreeClearChecked(item.Children);
                }
            });
        }
        $scope.deleteCostCentre = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function (result) {
                if (result) {
                    timeoutvariableforentitycreation.membersplice = $timeout(function () {
                        $scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
                        var memberToRempve = $.grep($scope.MemberLists, function (e) {
                            return e.CostCentreID == item.CostcenterId
                        });
                        if (memberToRempve.length > 0) $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };
        //var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
        //$('#btnWizardNext').show();
        //$('#btnWizardPrev').hide();
        //window["tid_wizard_steps_all_complete_count"] = 0;
        //window["tid_wizard_steps_all_complete"] = setInterval(function () {
        //    KeepAllStepsMarkedComplete();
        //}, 25);
        $scope.changeTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            RefreshCostcentreSource();
            if (currentWizardStep === 1) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            } else {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
        }
        $scope.changeTab2 = function (e) {
            if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
                loadfinnacialtree();
            }
            timeoutvariableforentitycreation.buttonclicktimeout = $timeout(function () {
                $("#btnTemp").click();
            }, 100);
            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#MyWizard ul.steps").find("li").addClass("complete");
            $("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 3) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.changePrevTab = function () {
            if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
                loadfinnacialtree();
            }
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            RefreshCostcentreSource();
            if ($scope.DynamicAttributeGroupTabs.length > 0) {
                if ($scope.ShowFinancial == "false" && ($scope.DynamicAttributeGroupTabs.length + 1) < currentWizardStep) {
                    currentWizardStep = $scope.DynamicAttributeGroupTabs.length + 1;
                    $('#MyWizard').wizard('manualpreviousstep', currentWizardStep);
                }
                else {
                    $('#MyWizard').wizard('previous', '');
                    currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
                }
            }
            else {
                if (($scope.ShowFinancial == "false" || $scope.ShowFinancial == false) && currentWizardStep == 3) {
                    currentWizardStep = 1;
                    $('#MyWizard').wizard('manualpreviousstep', currentWizardStep);
                } else {
                    $('#MyWizard').wizard('previous', '');
                    currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
                }
            }
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }

            if ($scope.DynamicAttributeGroupTabs.length > 0 && currentWizardStep > 1 && currentWizardStep <= ($scope.DynamicAttributeGroupTabs.length + 1)) {
                $scope.attributeGroupDetailAsTab.length = 0;
                $scope.attributeGroupDetailAsTab.push($scope.DynamicAttributeGroupTabs[currentWizardStep - 2]);
            }

            if (currentWizardStep < totalWizardSteps) {
                $('#btnWizardNext').show();
            } else {
                $('#btnWizardNext').hide();
            }
        }
        $scope.status = true;
        $scope.changenexttab = function (event) {
            if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
                loadfinnacialtree();
            }
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }

            $("#btnTemp").click();

            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().css({ 'color': '#B94A48' });
                }
                if ($("#EntityMetadata .error").find('.va-middle').length > 0) {
                    if (uploaderAttrInfo.length > 0) {
                        if (parseInt(($("#EntityMetadata .error").find('.va-middle').attr('id')).split('_')[1]) == uploaderAttrInfo[0]["attrID"]) {
                            $("#EntityMetadata .error").find('.va-middle').next().hide();
                            $("#EntityMetadata .error").find('.va-middle').parent().parent().removeClass("error");                            
                        }
                    }

                }
                if ($("#EntityMetadata .error").find('directive-tagwords').length > 0) {
                    if ($scope.fields["ListTagwords_" + $("#EntityMetadata .error").find('directive-tagwords').attr('item-attrid')].length > 0) {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').parent().parent().removeClass("error");
                    }
                    else {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').removeAttr("style");
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                    }
                }
                return false;
            }
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);

            if ($scope.DynamicAttributeGroupTabs.length > 0) {
                if ($scope.ShowFinancial == "false" && currentWizardStep > $scope.DynamicAttributeGroupTabs.length) {
                    currentWizardStep = $scope.DynamicAttributeGroupTabs.length + 3;
                    $('#MyWizard').wizard('manualnextstep', currentWizardStep);
                }
                    //else if ($scope.ShowFinancial == "true" && currentWizardStep > $scope.DynamicAttributeGroupTabs.length) {
                    //    currentWizardStep = $scope.DynamicAttributeGroupTabs.length + 2;
                    //    $('#MyWizard').wizard('manualnextstep', currentWizardStep);
                    //}
                else {
                    $('#MyWizard').wizard('next', '');
                    currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
                    totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
                }
            }
            else {

                if (($scope.ShowFinancial == "false" || $scope.ShowFinancial == false) && currentWizardStep == 1) {
                    currentWizardStep = 3;
                    $('#MyWizard').wizard('manualnextstep', currentWizardStep);
                } else {
                    $('#MyWizard').wizard('next', '');
                    currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
                    totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
                }
            }
            RefreshCostcentreSource();
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }

            if ($scope.DynamicAttributeGroupTabs.length > 0 && currentWizardStep > 1 && currentWizardStep <= ($scope.DynamicAttributeGroupTabs.length + 1)) {
                $scope.attributeGroupDetailAsTab.length = 0;
                $scope.attributeGroupDetailAsTab.push($scope.DynamicAttributeGroupTabs[currentWizardStep - 2]);

                if ($scope.attributeGroupDetailAsTab.length > 0)
                    $timeout(function () { $scope.$broadcast("attrprocess" + ($scope.attributeGroupDetailAsTab[0].ID)) }, 200);
            }

            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
            } else {
                $('#btnWizardNext').show();
            }
        }
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.AutoCompleteSelectedObj = [];
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = [];
        $scope.OwnerList = [];
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.ownerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.OwnerID, 10),
            "UserName": $scope.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.contrls = '';
        var Version = 1;
        $scope.wizard = {
            newval: ''
        };
        $scope.entityObjectString = '{';
        $scope.FinancialRequestObj = [];
        $scope.optionsLists = [];
        $scope.entityNameTyping = "";
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.count = 1;
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10);
            });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.fields['userRoles'])
                })[0];
                if (membervalues != undefined) {
                    $scope.MemberLists.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                        "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                        "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                        "Roleid": parseInt($scope.fields['userRoles'], 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                        "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "CostCentreID": 0,
                        "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2
                    });
                    $scope.fields.usersID = '';
                    $scope.count = $scope.count + 1;
                    $scope.AutoCompleteSelectedUserObj = [];
                    $scope.AutoCompleteSelectedObj = [];
                }
            } else bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
        };
        $scope.deleteOptions = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    timeoutvariableforentitycreation.memberspliceto = $timeout(function () {
                        $scope.MemberLists.splice($.inArray(item, $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };

        $scope.saveRootEnt = function () {
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#btnTemp").click();
            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().css({ 'color': '#B94A48' });
                }
                if ($("#EntityMetadata .error").find('.va-middle').length > 0 ){
                    if (uploaderAttrInfo.length > 0) {
                        if (parseInt(($("#EntityMetadata .error").find('.va-middle').attr('id')).split('_')[1]) == uploaderAttrInfo[0]["attrID"]) {
                            $("#EntityMetadata .error").find('.va-middle').next().hide();
                            $("#EntityMetadata .error").find('.va-middle').parent().parent().removeClass("error");                           
                        }
                    }

                }
                if ($("#EntityMetadata .error").find('directive-tagwords').length > 0) {
                    if ($scope.fields["ListTagwords_" + $("#EntityMetadata .error").find('directive-tagwords').attr('item-attrid')].length > 0) {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').parent().parent().removeClass("error");
                    }
                    else {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').removeAttr("style");
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                    }
                }
                return false;
            }
            else {
                $("#btnWizardFinish").attr('disabled', 'disabled');
                $("#btnWizardFinishEnt").attr('disabled', 'disabled');

                if (uploaderAttrInfo.length > 0) {

                    RootentitytypecreationService.copyuploadedImage(uploaderAttrInfo[0].attrFileName).then(function (newuplAttrName) {
                        var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfo[0].attrID });
                        var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                        $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                        saveRootEnt();
                    })
                }
                else { saveRootEnt(); }
            }


        }

        function saveRootEnt() {

            var SaveEntity = {};
            if (parseInt($scope.OwnerList[0].Userid) == parseInt($cookies.UserId)) {
                $scope.IsLock = false;
            } else {
                $scope.IsLock = true;
            }

            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                "Value": "-1"
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        } else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                } else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]],
                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) {
                            return e.level == (j + 1);
                        }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                        return relation.NodeId.toString() === opt;
                                    });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                            var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields['MTextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString()).replace(/ /g, ''),
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({
                        amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                        currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                        Attributeid: $scope.atributesRelationList[i].AttributeID
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    if ($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        var dte = new Date.create($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID]);
                        var tdte = ConvertDateToString(dte);
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": tdte,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null) {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString("yyyy-MM-dd");
                        } else {
                            MyDateString = "";
                        }
                    } else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].attrFileID == undefined || $scope.atributesRelationList[i].attrFileID == null) {
                        $scope.atributesRelationList[i].attrFileID = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.atributesRelationList[i].attrFileID,
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
            }
            SaveEntity.ParentId = 0;
            SaveEntity.Typeid = $scope.rootEntityID;
            SaveEntity.Active = true;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.entityName;
            SaveEntity.EntityMembers = [];
            SaveEntity.EntityCostRelations = $scope.costcemtreObject;
            SaveEntity.Periods = [];
            $scope.StartEndDate = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.toString().length != 0 && $scope.items[m].endDate.toString().length != null) {
                        $scope.StartEndDate.push({
                            startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                            endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                            comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
            }
            SaveEntity.Periods.push($scope.StartEndDate);
            SaveEntity.AttributeData = $scope.AttributeData;
            SaveEntity.EntityMembers = $scope.MemberLists;
            SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;
            SaveEntity.AssetArr = new Array();
            SaveEntity.IObjectiveEntityValue = [];
            SaveEntity.Entityamountcurrencytype = [];
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({
                    amount: $scope.Entityamountcurrencytypeitem[m].amount,
                    currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                    Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                });
            }
            SaveEntity.Entityamountcurrencytype.push($scope.curram);

            SaveEntity.PredefinedAttrValues = [];
            SaveEntity.AttributeGroupValues = [];
            if ($scope.DynamicAttributeGroupTabs.length > 0) {
                if ($scope.attributeGroupAddedValues != undefined) {

                    for (var i = 0; i < $scope.DynamicAttributeGroupTabs.length; i++) {
                        var req = $scope.DynamicAttributeGroupTabs[i];
                        if (req != undefined) {
                            if (req.Ispredefined == true)
                                SaveEntity.PredefinedAttrValues.push({ "PredefinedattrGrpData": $scope.attributeGroupAddedValues["PredefinedattrGrpDataof_" + req.ID], "GroupID": req.AttributeGroupID });
                            else
                                SaveEntity.AttributeGroupValues.push({ "attrGrpData": $scope.attributeGroupAddedValues["attrGrpDataof_" + req.ID], "GroupID": req.AttributeGroupID });
                        }
                    }
                }
            }

            RootentitytypecreationService.CreateEntity(SaveEntity).then(function (SaveEntityResult) {
                if (SaveEntityResult.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
                    $scope.Entityamountcurrencytypeitem = [];
                } else {
                    $scope.Activity.IsActivitySectionLoad = false;
                    $scope.MemberLists = [];
                    $scope.Entityamountcurrencytypeitem = [];
                    var IDList = new Array();
                    IDList.push(SaveEntityResult.Response);
                    $window.ListofEntityID = IDList;
                    var TrackID = CreateHisory(IDList);
                    $location.path('/mui/planningtool/default/detail/section/' + SaveEntityResult.Response + '/overview').replace();
                }
                timeoutvariableforentitycreation.cancelTimeout = $timeout(function () {
                    $modalInstance.dismiss('cancel');
                }, 100)
            });
        };

        function RefreshCostcentreSource() {
            if ($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear] != undefined) {
                $scope.costCenterList = [];
                var year = (parseInt($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
                var FiscalYearValue = parseInt(($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]) != "" ? $scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear] : 0, 10);
                RootentitytypecreationService.GetCostcentreforEntityCreation($scope.rootEntityID == "" ? 0 : $scope.rootEntityID, FiscalYearValue, 0).then(function (costCenterList) {
                    $scope.costCenterList = costCenterList.Response;
                });
                if ($('#MyWizard').wizard('selectedItem').step == 2) {
                    $scope.treeCategory = [];
                    var FiscalYearValue = parseInt(($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]) != "" ? $scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear] : 0, 10);
                    RootentitytypecreationService.GetCostcentreTreeforPlanCreation($scope.rootEntityID == "" ? 0 : $scope.rootEntityID, FiscalYearValue, 0).then(function (costCenterList1) {
                        if (costCenterList1.Response != null && costCenterList1.Response != false) {
                            $scope.treeCategory = JSON.parse(costCenterList1.Response);
                            $scope.dyn_Cont = '';
                            $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                            $scope.dyn_Cont += '</div>';
                            $("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));
                        }
                    });
                }
            }
        }   

        $scope.changeCostCenterSource = function () {
            $scope.costCenterList = [];
            $scope.costcemtreObject = [];
            var year = (parseInt($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
            var FiscalYearValue = (year == " " ? 0 : year);
            RootentitytypecreationService.GetCostcentreforEntityCreation($scope.rootEntityID, FiscalYearValue, 0).then(function (costCenterList) {
                $scope.costCenterList = costCenterList.Response;
            });
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        if(attrType != 6)
                            $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = 0;
                            //$("#s2id_DropDownTree_" + attrID + "_" + j).find('span').text('');
                            var ID = "DropDownTree_" + attrID + "_" + 1;
                            $("#EntityMetadata .error").find('#' + ID).next().hide();
                            $("#EntityMetadata .error").find('#' + ID).next().addClass('icon-asterisk validationmark margin-right5x');
                            if ($("#EntityMetadata .error").find('#' + ID).parent().parent().hasClass('error') == true)
                                $("#EntityMetadata .error").find('#' + ID).parent().parent().removeClass('error');

                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
            } catch (e) { }
        }

        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        //if (attrType != 6)
                        //$scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            $.each(Children, function (i, el) {
                                $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        var MultiselectId = "TreeMultiSelection_" + attrID + "_" + 1;
                        $("#EntityMetadata .error").find('#' + MultiselectId).next().removeAttr("style");
                        $("#EntityMetadata .error").find('#' + MultiselectId).next().addClass('icon-asterisk validationmark margin-right5x');
                        if ($("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().hasClass('error') == true)
                            $("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().removeClass('error');

                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }

                        //$.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                        //    $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //});
                        //}
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };

        function GetEntityTypeRoleAccess(rootID) {
            RootentitytypecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.MultiSelecttree = {};
        $scope.Options = {};
        $scope.myTree = [];
        $scope.SelectionList = [];
        $scope.setSelected = function (c) {
            var checkopt = $.grep($scope.SelectionList, function (e) {
                return e == c.id;
            });
            if (c.checked1 == true && checkopt.length == 0) {
                $scope.SelectionList.push(c.id);
            } else if (c.checked1 == false && checkopt.length > 0) {
                $scope.SelectionList.splice($.inArray(c.id, $scope.SelectionList), 1);
            }
        }
        $scope.dropDownTreeListMultiSelectionObj = [];
        $scope.DropDownTreeOptionValues = {};

        function loadfinnacialtree() {
            $scope.dyn_Cont = '';
            if ($scope.treeCategory.length == 0) {
                $scope.dyn_Cont += '<input type="text" class="read-only" placeholder="No Cost centre is available"/>';
            } else {
                $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
                $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
                $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                $scope.dyn_Cont += '</div>';
            }
            $("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));
        }
        $scope.LoadAttributesForEntityCreation = function (rootID, rootCaption) {
            $scope.DynamicAttributeGroupTabs = [];
            RootentitytypecreationService.GetEntityTypeAttributeGroupRelation(rootID, 0, 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {

                        var ListOfAttributeGroup = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.IsTabInEntityCreation == true;
                        });

                        for (var i = 0; i < ListOfAttributeGroup.length; i++) {
                            if (ListOfAttributeGroup[i].Ispredefined == true) {
                                $scope.attributeGroupAddedValues["PredefinedattrGrpvalueof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["PredefinedattrGrpDataof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["PredefinedattrGrpHeaderof_" + ListOfAttributeGroup[i].ID] = [];
                            }
                            else {
                                $scope.attributeGroupAddedValues["attrGrpvalueof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["attrGrpDataof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["attrGrpHeaderof" + ListOfAttributeGroup[i].ID] = [];
                            }
                        }

                        $scope.DynamicAttributeGroupTabs = ListOfAttributeGroup;

                        if ($scope.ShowFinancial == "false") {
                            $scope.memberno = $scope.DynamicAttributeGroupTabs.length + 2;
                        }
                        else {
                            $scope.financialno = $scope.DynamicAttributeGroupTabs.length + 2;
                            $scope.memberno = $scope.DynamicAttributeGroupTabs.length + 3;
                        }

                        $timeout(function () {
                            $('#MyWizard').wizard('stepLoaded');
                            $('#btnWizardNext').show();     //intially wizard submit button is enalbed
                            $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled
                            window["tid_wizard_steps_all_complete_count"] = 0;
                            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                        }, 250);
                    }
                }
                else {
                    $('#MyWizard').wizard('stepLoaded');
                    $('#btnWizardNext').show();     //intially wizard submit button is enalbed
                    $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled
                    window["tid_wizard_steps_all_complete_count"] = 0;
                    window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                }
            });
            $scope.DateObject = {};
            $scope.dyn_Cont = '';
            GetEntityTypeRoleAccess(rootID);
            $scope.UserimageNewTime = new Date.create().getTime().toString();
            $scope.fields = {
                usersID: ''
            };
            $scope.varfields = {
                CostcenterInfo: []
            };
            $scope.dynamicEntityValuesHolder = {};
            $scope.fieldKeys = [];
            $scope.costcemtreObject = [];
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.OptionObj = {};
            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];
                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });
            }
            $scope.dropDownTreeListMultiSelectionObj = [];
            $scope.MemberLists = [];
            $scope.tempCount = 1;
            //var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            //$('#MyWizard').wizard('stepLoaded');
            //$('#btnWizardNext').show();
            //$('#btnWizardPrev').hide();
            //window["tid_wizard_steps_all_complete_count"] = 0;
            //window["tid_wizard_steps_all_complete"] = setInterval(function () {
            //    KeepAllStepsMarkedComplete();
            //}, 25);
            $scope.status = true;
            var ownername = $cookies['Username'];
            var ownerid = $cookies['UserId'];
            $scope.OwnerName = ownername;
            $scope.OwnerID = ownerid;
            $scope.ownerEmail = $cookies['UserEmail'];
            $scope.AutoCompleteSelectedObj = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.contrls = '';
            var Version = 1;
            $scope.FinancialRequestObj = [];
            $scope.optionsLists = [];
            $scope.entityNameTyping = "";
            $scope.AttributeData = [];
            $scope.entityName = "";
            $scope.count = 1;
            $scope.treelevels = [];
            $scope.tree = {};
            $scope.MultiSelecttree = {};
            $scope.Options = {};
            $scope.wizard = {
                newval: ''
            };
            $scope.costcemtreObject = [];
            $scope.dyn_Cont = '';
            $scope.rootEntityID = '';
            $scope.rootEntityCaption = rootCaption;
            $scope.rootEntityID = rootID;
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            $scope.items = [];
            $scope.Entityamountcurrencytypeitem = [];
            $scope.tagwordids = [];
            RootentitytypecreationService.GetAttributeToAttributeRelationsByIDForEntity($scope.rootEntityID).then(function (entityAttrToAttrRelation) {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });
            RootentitytypecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
                RootentitytypecreationService.GetEntityStatus(rootID, false, 0).then(function (getentitytypestatus) {
                    $scope.EntityStatusResult = getentitytypestatus.Response;
                    $scope.owner = Getowner.Response;
                    $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
                    $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
                    $scope.OwnerList.push({
                        "Roleid": 1,
                        "RoleName": "Owner",
                        "UserEmail": $scope.ownerEmail,
                        "DepartmentName": $scope.owner.Designation,
                        "Title": $scope.owner.Title,
                        "Userid": parseInt($scope.OwnerID, 10),
                        "UserName": $scope.OwnerName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "QuickInfo1": $scope.owner.QuickInfo1,
                        "QuickInfo2": $scope.owner.QuickInfo2
                    });
                    RootentitytypecreationService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                        $scope.atributesRelationList = entityAttributesRelation.Response;
                        for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                            if ($scope.atributesRelationList[i].IsHelptextEnabled == true) {
                                var attribteHelptext = $scope.atributesRelationList[i].HelptextDecsription == null ? "-" : $scope.atributesRelationList[i].HelptextDecsription.trim() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription;
                                var qtip = 'my-qtip2 qtip-content="' + attribteHelptext + '"';
                            }
                            else
                                var qtip = "";
                            if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                if ($scope.atributesRelationList[i].AttributeID != 70) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                    if ($scope.atributesRelationList[i].InheritFromParent) $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];
                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                        // $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                        if ($scope.atributesRelationList[i].PlaceHolderValue != null) {                                                                                        
                                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                        }
                                        else {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                        }
                                    } else {
                                        //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    }
                                    $scope.setFieldKeys();
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].calenderAttributeID] = new Date.create();
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue > 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else if ($scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),1000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].IsSpecial == true) {
                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                        //$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>"
                                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                        $scope.setFieldKeys();
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    // $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                    else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    $scope.setFieldKeys();
                                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                    else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    $scope.setFieldKeys();
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $translate.instant('LanguageContents.Res_5613.Caption') + "</label><div class=\"controls bgDropdown\"><select ui-select2=\"select2Config\" id=\"OverAllEntityStats\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"OverAllEntityStatus\" data-placeholder=\"Entity Status\">";
                                $scope.dyn_Cont += "<option value=\"0\"> Select " + $translate.instant('LanguageContents.Res_5613.Caption') + "</option>";
                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in EntityStatusResult\" value=\"{{Aitem.ID}}\">{{Aitem.StatusOptions}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = 0;

                                $scope.select2Config = {
                                    formatResult: $scope.overallstatusformat,
                                    formatSelection: $scope.overallstatusformat
                                };
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };

                                    if (j == 0) {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;

                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + "</label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        $scope.settreeSources();

                                    } else {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\">";
                                        $scope.dyn_Cont += "<option ng-repeat=\"item in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{item.id}}\">{{item.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.settreeSources();
                                    }
                                    $scope.setFieldKeys();
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\">";
                                if($scope.atributesRelationList[i].IsReadOnly == true)
                                    $scope.dyn_Cont += "<select  class=\"multiselect\"  data-placeholder=\"Select filter\" disabled ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\'changeval(" + $scope.atributesRelationList[i].AttributeID + ");'\   ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                else
                                    $scope.dyn_Cont += "<select  class=\"multiselect\"  data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\'changeval(" + $scope.atributesRelationList[i].AttributeID + ");'\   ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent && $scope.atributesRelationList[i].ParentValue != null) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                } else {
                                    var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                    if ($scope.atributesRelationList[i].DefaultValue != "") {
                                        for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) {
                                                return e.Id == defaultmultiselectvalue[j];
                                            })[0].Id);
                                        }
                                    } else {
                                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                    }
                                }
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.items.push({
                                    startDate: null,
                                    endDate: null,
                                    comment: '',
                                    sortorder: 0,
                                    calstartopen: false,
                                    calendopen: false
                                });
                                $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                                $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue > 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else if ($scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                                $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                                $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                                $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                                $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                                $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                                $scope.setoptions();
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Amount;
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Currencytypeid;
                                } else {
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                                }
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                    treeTextVisbileflag = false;
                                    if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                    } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                } else {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                }
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                                $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                                $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                $scope.dyn_Cont += '<div class="input-group inlineBlock treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" id="Tree_' + $scope.atributesRelationList[i].AttributeID + '" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                                $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                                $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                $scope.dyn_Cont += '</div><span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                $scope.dyn_Cont += '</div></div>';
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                //$scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"MTextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                                $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group \" drowdowntreepercentagemultiselection data-purpose='entity' data-helptextdescription=" + helptextdesc + " data-ishelptextenabled=" + $scope.atributesRelationList[i].IsHelptextEnabled.toString() + " data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";

                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < totLevelCnt1; j++) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };
                                    if (totLevelCnt1 == 1) {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        //    return item.Caption
                                        //};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        //    return item.Caption
                                        //};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;

                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        $scope.setFieldKeys();
                                    }
                                    else {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                        if (j == 0) {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                            $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                            $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                            $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                            $scope.settreeSources();
                                        } else {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            }
                                            else {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            }
                                        }
                                    }
                                    $scope.setFieldKeys();
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.setoptions();
                                $scope.tempscope = [];
                                var helptext = $scope.atributesRelationList[i].HelptextDecsription == "" ? "" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                                $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                                $scope.dyn_Cont += "<div class=\"controls\">";
                                //$scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                                $scope.dyn_Cont += "<directive-tagwords id = \"Tag_"+$scope.atributesRelationList[i].AttributeID + "\"   item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\" item-isenable=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" item-attributehelptext=\"" + helptext.toString() + "\" item-isreadonly=\"" + $scope.atributesRelationList[i].IsReadOnly + "\"  ></directive-tagwords>";
                                $scope.dyn_Cont += "</div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) { } else { }
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),1000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                var param1 = new Date.create();
                                var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.setFieldKeys();
                                $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                                StrartUpload_UploaderAttr();
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group"><label class="control-label"';
                                $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<div class="width2x inlineBlock va-middle" style="" id="Uploader_' + $scope.atributesRelationList[i].AttributeID + '" >';
                                if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                                    $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                                }
                                $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                                $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl">';
                                $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a></div>';
                                $scope.dyn_Cont += '<span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            }
                            $scope.setFieldKeys();
                            if ($scope.atributesRelationList[i].IsReadOnly == true) {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            } else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        }
                        $scope.setFieldKeys();
                        $scope.dyn_Cont += '<input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                        $("#EntityMetadata").html($compile($scope.dyn_Cont)($scope));
                        var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                        $scope.mindec = "";
                        if (mar.length == 1) {
                            $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                aSep: ' ',
                                vMin: "0",
                                mDec: "1"
                            });
                        }
                        if (mar.length != 1) {
                            if (mar.length < 5) {
                                $scope.mindec = "0.";
                                for (i = 0; i < mar.length; i++) {
                                    $scope.mindec = $scope.mindec + "0";
                                }
                                $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                    aSep: ' ',
                                    vMin: $scope.mindec
                                });
                            } else {
                                $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                    aSep: ' ',
                                    vMin: "0",
                                    mDec: "0"
                                });
                            }
                        }
                        $("[id^='MTextMoney_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "0.0"
                        });
                        $("[id^='MTextMoney_']").keydown(function (event) {
                            if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                                bootbox.alert($translate.instant('LanguageContents.Res_5735.Caption'));
                                event.preventDefault();
                            }                               
                        });
                        setTimeout(function () {
                            $('[id^=TextSingleLine]:enabled:visible:first').focus().select()
                        }, 1000);
                        $("#EntityMetadata").scrollTop(0);
                        $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];
                        timeoutvariableforentitycreation.hideattr = $timeout(function () {
                            HideAttributeToAttributeRelationsOnPageLoad();
                        }, 200);
                        GetValidationList();
                        $("#EntityMetadata").addClass('notvalidate');
                    });
                });
            });
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            } else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = (7).daysAfter(objdateval.endDate);
            }
        };
        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            } else {
                $("#EntityMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null;
                } else {
                    var endDate = new Date.create(startdate);
                    $scope.items[currentindex].endDate = (7).daysAfter(endDate);
                }
            }
        };
        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            var edate = ConvertDateToString(enddate1);
            var sdate = ConvertDateToString(startdate);
            if (enddate1 != null) {
                if (edate < sdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                    $scope.items[currentindex].endDate = null;
                }
            }
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.RootEntityTypeCreationCtrl']"));
        });

        function GetValidationList() {
            RootentitytypecreationService.GetValidationDationByEntitytype($scope.rootEntityID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listValidationResult.length != 0) {
                        for (var i = 0 ; i < $scope.listofValidations.length; i++) {
                            if ($scope.listofValidations[i][1] == "presence") {
                                $($scope.listofValidations[i][0]).parent().addClass('relative');
                                if ($($scope.listofValidations[i][0]).parent().find('ul').length >= 1 && ($scope.listofValidations[i][0]).contains('ListMultiSelection')) {
                                    $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($($scope.listofValidations[i][0]).parent().find('ul'));
                                } else {
                                    if (($scope.listofValidations[i][0]).contains('Tag') == true) {
                                        $("#EntityMetadata").find($scope.listofValidations[i][0]).find('.validationmark').removeClass("ng-hide");
                                    }
                                    else
                                        $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listofValidations[i][0]);
                                }
                            }
                        }
                    }
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    timeoutvariableforentitycreation.listvalidation = $timeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTemp',
                            'silentSubmit': 'true'
                        });
                    }, 100);
                }
            });
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.OnCostCentreTreeSelection = function (branch, parentArr) {
            if (branch.ischecked == true) {
                $scope.varfields.CostcenterInfo.push(branch.id);
            } else {
                $scope.varfields.CostcenterInfo.splice($scope.varfields.CostcenterInfo.indexOf(branch.id), 1);
            }
        };
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            if ($scope.fields["Tree_" + branch.AttributeId].length > 0) {
                if ($("#Tree_" + branch.AttributeId).parent().parent().parent().hasClass('error') == true) {
                    if (branch.AttributeId > 0) {
                        $("#Tree_" + branch.AttributeId).parent().parent().parent().removeClass("error");
                        $("#Tree_" + branch.AttributeId).next().hide();
                    }
                }
            }
            $scope.ShowOrHideAttributeToAttributeRelation(branch.AttributeId + "_0", 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.origninalamount = 0;
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            RootentitytypecreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                }
            });
        }
        RootentitytypecreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.Closeentitycreationpopup = function () {
            $modalInstance.dismiss('cancel');
        }

        function openentitycreationpopup() {
            $scope.LoadAttributesForEntityCreation(params.ID, params.Caption);
        }

        $scope.EntityStatusResult = [];

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id == "0" && data.text != "") {
                return "<div class='select2-chosen-label'style='background-color: #FEFEFE; color: #444444; font-size: 11px;'>" + data.text + "</div>";
            }
            else if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }
            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        openentitycreationpopup();
        $scope.$on("$destroy", function () {
            $timeout.cancel(timeoutvariableforentitycreation);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.RootEntityTypeCreationCtrl']"));
            var remValtimer, IsValExist, getVal, IsValExist, Attributetypename, relationobj, ID, i, o, a, j, x, k, m, keys, extension, resultArr, PreviewID, variable, result, costCentrevalues, memberObj, memberToRempve, totalWizardSteps, currentWizardStep, totalWizardSteps, percentageflag, ownername, ownerid, Version, membervalues, SaveEntity, attributeLevelOptions, valueMatches, value, multiselectiObject, treenodes, dte, tdte, MyDate, MyDateString, IDList, TrackID, FiscalYearValue, year, test, formatlen, defaultdateVal, recursiveAttrID, attributesToShow, attrRelIDs, currntlevel, hideAttributeOtherThanSelected, optionValue, attrval, checkopt, Version, temp, totLevelCnt, defaultmultiselectvalue, totLevelCnt1, param1, param2, mar, enddate1, enddate, edate, sdate, renderContext, treeTextVisbileflag, apple_selected, tree, treedata_avm, treedata_geography, remainRecord = null;
        });
    }
    app.controller("mui.planningtool.RootEntityTypeCreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$translate', '$compile', 'RootentitytypecreationService', '$modalInstance', 'params', muiplanningtoolRootEntityTypeCreationCtrl]);
    
    app.directive('multidropdownpercentage', function () {
        return {
            restrict: 'A',
            transclude: 'element',
            priority: 1000,
            terminal: true,
            compile: function (tElement, tAttrs, transclude) {
                var repeatExpr, childExpr, rootExpr, childrenExpr, branchExpr;
                repeatExpr = tAttrs.multidropdownpercentage.match(/^(.*) in ((?:.*\.)?(.*)) at (.*)$/);
                childExpr = repeatExpr[1];
                rootExpr = repeatExpr[2];
                childrenExpr = repeatExpr[3];
                branchExpr = repeatExpr[4];
                return function link(scope, element, attrs) {
                    var rootElement = element[0].parentNode,
						cache = [];

                    function lookup(child) {
                        var i = cache.length;
                        while (i--) {
                            if (cache[i].scope[childExpr] === child) {
                                return cache.splice(i, 1)[0];
                            }
                        }
                    }
                    scope.$watch(rootExpr, function (root) {
                        var currentCache = [];
                        (function walk(children, parentNode, parentScope, depth) {
                            var i = 0,
								n = children != undefined ? children.length : 0,
								last = n - 1,
								cursor, child, cached, childScope, grandchildren;
                            for (; i < n; ++i) {
                                cursor = parentNode.childNodes[i];
                                child = children[i];
                                cached = lookup(child);
                                if (cached && cached.parentScope !== parentScope) {
                                    cache.push(cached);
                                    cached = null;
                                }
                                if (!cached) {
                                    transclude(parentScope.$new(), function (clone, childScope) {
                                        childScope[childExpr] = child;
                                        cached = {
                                            scope: childScope,
                                            parentScope: parentScope,
                                            element: clone[0],
                                            branch: clone.find(branchExpr)[0]
                                        };
                                        parentNode.insertBefore(cached.element, cursor);
                                    });
                                } else if (cached.element !== cursor) {
                                    parentNode.insertBefore(cached.element, cursor);
                                }
                                childScope = cached.scope;
                                childScope.$depth = depth;
                                childScope.$index = i;
                                childScope.$first = (i === 0);
                                childScope.$last = (i === last);
                                childScope.$middle = !(childScope.$first || childScope.$last);
                                currentCache.push(cached);
                                grandchildren = child[childrenExpr];
                                if (grandchildren && grandchildren.length) {
                                    walk(grandchildren, cached.branch, childScope, depth + 1);
                                }
                            }
                        })(root, rootElement, scope, 0);
                        var i = cache.length;
                        while (i--) {
                            var cached = cache[i];
                            if (cached.scope) {
                                cached.scope.$destroy();
                            }
                            if (cached.element) {
                                cached.element.parentNode.removeChild(cached.element);
                            }
                        }
                        cache = currentCache;
                    }, true);
                };
            }
        };
    });
    app.directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == undefined) return ''
                    if (inputValue == "00" || inputValue == "000") {
                        inputValue == "0";
                        modelCtrl.$setViewValue("0");
                        modelCtrl.$render();
                    };
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });
            }
        };
    });
    app.directive('drowdowntreepercentagemultiselection', ['$compile', '$timeout', function ($compile, $timeout) {
        return {
            priority: 100,
            terminal: true,
            compile: function compile(scope, tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {
                        var htmlText = "";
                        var levels = [];
                        var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
                        var directiveObj = scope.DropDownTreePricing["AttributeId_Levels_" + parseInt(iAttrs.attributeid) + ""];
                        if (directiveObj != null) {
                            htmlText += '  <div id="DrpdwnPrice" ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + (iAttrs.attributeid) + '_0\')\" data-role="formpercentagetotalcontainer"> ';
                            htmlText += '                        <ol  class="oltreelis"> ';
                            htmlText += '                            <li  multidropdownpercentage="child in DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + ' at ol"> ';
                            htmlText += '                                <div class="control-group"> ';
                            htmlText += '                                    <label class="control-label">{{child.LevelName}}</label> ';
                            htmlText += '                                    <div class="controls"> ';
                            htmlText += '                                        <div> ';
                            htmlText += '                                            <select  id="DropDownTreePricing_' + parseInt(iAttrs.attributeid) + '" ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
                            htmlText += '                                                <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
                            htmlText += '                                            </select> ';
                            if (iAttrs.helptextdescription != undefined && iAttrs.ishelptextenabled != undefined) {
                                htmlText += '<span ng-show=\"' + iAttrs.ishelptextenabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + iAttrs.helptextdescription + '\" class=\"icon-info-sign\"></i></span>';
                            }
                            htmlText += '                                        </div> ';
                            htmlText += '                                    </div> ';
                            htmlText += '                                </div> ';
                            htmlText += '                                <form class="form-horizontal">';
                            htmlText += '                                   <div class="oltreelis-child"> ';
                            htmlText += '                                       <div ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '"> ';
                            htmlText += '                                           <div class="control-group" ng-repeat="val in  getPricingRecords(child.LevelOptions,child.selection) track by $index"> ';
                            htmlText += '                                               <label class="control-label">{{val.caption}}:</label> ';
                            htmlText += '                                               <div class="controls"> ';
                            htmlText += '                                                   <input ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '"  type="text" ng-model="val.value" ng-blur="AutoAssignPercentage(child.LevelOptions,child.selection)" max-date="100" min-date="0" size="3" maxlength="3" name="txt" numbers-only="numbers-only"> ';
                            htmlText += '                                                   <span class="perc">%</span> ';
                            htmlText += '                                               </div> ';
                            htmlText += '                                           </div> ';
                            htmlText += '                                       </div> ';
                            htmlText += '                                       <div data-role="percentagetotalcontainer" ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '" class="oltreelis-childTotal"> ';
                            htmlText += '                                           <span data-selection="{{child.selection.length}}" data-isnotfilter="{{PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '}}" data-ispercentage="{{child.isperc}}" data-role="percentageerror" ng-class="ErrorPercentageClass(child.LevelOptions)">{{ getPercentageTotal(child.LevelOptions) }}</span> ';
                            htmlText += '                                           <span class="perc">%</span> ';
                            htmlText += '                                       </div> ';
                            htmlText += '                                   </div> ';
                            htmlText += '                               </form> ';
                            htmlText += '                               <ol></ol> ';
                            htmlText += '                            </li> ';
                            htmlText += '                        </ol> ';
                            htmlText += '               </div> ';
                            var tpl = htmlText;
                            iElement.html(tpl);
                            $compile(iElement.contents())(scope);
                        }
                        scope.OptionManipulation = function (attributeid, attributeLevel, totallevels, attributetypeid) {
                            $timeout(function () {
                                var attributeLevelOptions = [];
                                attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                    return e.level == attributeLevel;
                                }));
                                if (attributeLevelOptions[0] != undefined) {
                                    if (attributeLevelOptions[0].selection != undefined) {
                                        if (attributeLevelOptions[0].selection.length > 0) {
                                            try {
                                                var valueMatches = [];
                                                if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                    return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
                                                });
                                                else valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                                scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
                                            } catch (e) { }
                                            for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
                                                if (attributeLevel <= totallevels) {
                                                    if ((attributeLevel + 1) <= totallevels) {
                                                        var attributenextLevelOptions = [];
                                                        attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                                            return e.level == (attributeLevel + 1);
                                                        }));
                                                        if (attributenextLevelOptions[0] != undefined) {
                                                            if (attributenextLevelOptions[0].selection != undefined) {
                                                                if (attributenextLevelOptions[0].selection.length > 0) {
                                                                    var deletableIds = new Array();
                                                                    for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
                                                                        var childidObj = [];
                                                                        childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function (e) {
                                                                            return e.NodeId == parseInt(childid);
                                                                        }));
                                                                        if (childidObj.length > 0) {
                                                                            var removableLevelOptions = [];
                                                                            removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function (e) {
                                                                                return e == childidObj[0].LevelParent.toString();
                                                                            }));
                                                                            if (removableLevelOptions.length == 0) {
                                                                                childidObj[0].value = "";
                                                                                deletableIds.push(childid.toString());
                                                                            }
                                                                        }
                                                                    }
                                                                    for (var l = 0, id; id = deletableIds[l++];) {
                                                                        attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            try {
                                                var valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                            } catch (e) { }
                                            if (attributeLevel <= totallevels) {
                                                if ((attributeLevel + 1) <= totallevels) {
                                                    var attributenextLevelOptions = [];
                                                    attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                                        return e.level == (attributeLevel + 1);
                                                    }));
                                                    if (attributenextLevelOptions[0] != undefined) {
                                                        if (attributenextLevelOptions[0].selection != undefined) {
                                                            if (attributenextLevelOptions[0].selection.length > 0) {
                                                                attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (attributeLevel <= totallevels) {
                                            if ((attributeLevel + 1) <= totallevels) {
                                                $timeout(function () {
                                                    scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
                                                }, 100)
                                            }
                                        }
                                    }
                                }
                            }, 100);
                        };
                        scope.getPercentageTotal = function (leveloption) {
                            var total = 0;
                            for (var i = 0, obj; obj = leveloption[i++];) {
                                total += parseInt(obj.value != "" ? obj.value : 0);
                            }
                            return total;
                        }
                        scope.ErrorPercentageClass = function (leveloptions) {
                            var remainvalue = 0;
                            remainvalue = scope.getPercentageTotal(leveloptions);
                            if (remainvalue > 100) return "result lapse"
                            else if (remainvalue < 100) return "result lapse"
                            else return "";
                        }
                        scope.AutoAssignPercentage = function (leveloption, levelSelection) {
                            if (levelSelection.length > 1) {
                                var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
                                emptyBoxes = ($.grep(leveloption, function (e) {
                                    return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
                                }));
                                if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
                                    fillObj = emptyBoxes[emptyBoxes.length - 1];
                                    if (fillObj != null) {
                                        remainvalue = 100 - scope.getPercentageTotal(leveloption);
                                        if (remainvalue > 0) fillObj.value = remainvalue.toString();
                                        if (remainvalue == 0) {
                                            var val = 0;
                                            fillObj.value = val.toString();
                                        }
                                    }
                                }
                            }
                        };
                        scope.getOptions = function (level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
                            if (level == 1) return currentoptions;
                            else {
                                var matches = [];
                                if (previousoptions != undefined) {
                                    if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                        return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
                                    });
                                }
                                return matches;
                            }
                        };
                        scope.getPricingRecords = function (currentoptions, currentselection) {
                            var matches = [];
                            if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                return currentselection.indexOf(relation.NodeId.toString()) != -1;
                            });
                            return matches;
                        };
                    },
                    post: function postLink(scope, iElement, iAttrs, controller) { }
                }
            }
        }
    }]);
    app.directive('drowdowntreepercentagemultiselectionasset', ['$compile', '$timeout', function ($compile, $timeout) {
        return {
            priority: 100,
            terminal: true,
            compile: function compile(scope, tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {
                        var htmlText = "";
                        var levels = [];
                        var fileid = iAttrs.fileid;
                        var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
                        var directiveObj = scope.DropDownTreePricing["AttributeId_Levels_" + fileid + parseInt(iAttrs.attributeid) + ""];
                        if (directiveObj != null) {
                            htmlText += '  <div data-role="formpercentagetotalcontainer"> ';
                            htmlText += '                        <ol  class="oltreelis"> ';
                            htmlText += '                            <li  multidropdownpercentage="child in DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + ' at ol"> ';
                            htmlText += '                                <div class="control-group"> ';
                            htmlText += '                                    <label class="control-label">{{child.LevelName}}</label> ';
                            htmlText += '                                    <div class="controls"> ';
                            htmlText += '                                        <div> ';
                            htmlText += '                                            <select ng-disabled="EnableDisableControlsHolder.Selection_' + fileid + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
                            htmlText += '                                                <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
                            htmlText += '                                            </select> ';
                            htmlText += '                                        </div> ';
                            htmlText += '                                    </div> ';
                            htmlText += '                                </div> ';
                            htmlText += '                                <form class="form-horizontal">';
                            htmlText += '                                   <div class="oltreelis-child"> ';
                            htmlText += '                                       <div ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '"> ';
                            htmlText += '                                           <div class="control-group" ng-repeat="val in  getPricingRecords(child.LevelOptions,child.selection) track by $index"> ';
                            htmlText += '                                               <label class="control-label">{{val.caption}}:</label> ';
                            htmlText += '                                               <div class="controls"> ';
                            htmlText += '                                                   <input ng-disabled="EnableDisableControlsHolder.Selection_' + fileid + parseInt(iAttrs.attributeid) + '"  type="text" ng-model="val.value" ng-blur="AutoAssignPercentage(child.LevelOptions,child.selection)" max-date="100" min-date="0" size="3" maxlength="3" name="txt" numbers-only="numbers-only"> ';
                            htmlText += '                                                   <span class="perc">%</span> ';
                            htmlText += '                                               </div> ';
                            htmlText += '                                           </div> ';
                            htmlText += '                                       </div> ';
                            htmlText += '                                       <div data-role="percentagetotalcontainer" ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '" class="oltreelis-childTotal"> ';
                            htmlText += '                                           <span data-selection="{{child.selection.length}}" data-isnotfilter="{{PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '}}" data-ispercentage="{{child.isperc}}" data-role="percentageerror" ng-class="ErrorPercentageClass(child.LevelOptions)">{{ getPercentageTotal(child.LevelOptions) }}</span> ';
                            htmlText += '                                           <span class="perc">%</span> ';
                            htmlText += '                                       </div> ';
                            htmlText += '                                   </div> ';
                            htmlText += '                               </form> ';
                            htmlText += '                               <ol></ol> ';
                            htmlText += '                            </li> ';
                            htmlText += '                        </ol> ';
                            htmlText += '               </div> ';
                            var tpl = htmlText;
                            iElement.html(tpl);
                            $compile(iElement.contents())(scope);
                        }
                        scope.OptionManipulation = function (attributeid, attributeLevel, totallevels, attributetypeid) {
                            $timeout(function () {
                                var attributeLevelOptions = [];
                                attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function (e) {
                                    return e.level == attributeLevel;
                                }));
                                if (attributeLevelOptions[0] != undefined) {
                                    if (attributeLevelOptions[0].selection != undefined) {
                                        if (attributeLevelOptions[0].selection.length > 0) {
                                            try {
                                                var valueMatches = [];
                                                if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                    return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
                                                });
                                                else valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                                scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
                                            } catch (e) { }
                                            for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
                                                if (attributeLevel <= totallevels) {
                                                    if ((attributeLevel + 1) <= totallevels) {
                                                        var attributenextLevelOptions = [];
                                                        attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function (e) {
                                                            return e.level == (attributeLevel + 1);
                                                        }));
                                                        if (attributenextLevelOptions[0] != undefined) {
                                                            if (attributenextLevelOptions[0].selection != undefined) {
                                                                if (attributenextLevelOptions[0].selection.length > 0) {
                                                                    var deletableIds = new Array();
                                                                    for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
                                                                        var childidObj = [];
                                                                        childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function (e) {
                                                                            return e.NodeId == parseInt(childid);
                                                                        }));
                                                                        if (childidObj.length > 0) {
                                                                            var removableLevelOptions = [];
                                                                            removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function (e) {
                                                                                return e == childidObj[0].LevelParent.toString();
                                                                            }));
                                                                            if (removableLevelOptions.length == 0) {
                                                                                childidObj[0].value = "";
                                                                                deletableIds.push(childid.toString());
                                                                            }
                                                                        }
                                                                    }
                                                                    for (var l = 0, id; id = deletableIds[l++];) {
                                                                        attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            try {
                                                var valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                            } catch (e) { }
                                            if (attributeLevel <= totallevels) {
                                                if ((attributeLevel + 1) <= totallevels) {
                                                    var attributenextLevelOptions = [];
                                                    attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function (e) {
                                                        return e.level == (attributeLevel + 1);
                                                    }));
                                                    if (attributenextLevelOptions[0] != undefined) {
                                                        if (attributenextLevelOptions[0].selection != undefined) {
                                                            if (attributenextLevelOptions[0].selection.length > 0) {
                                                                attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (attributeLevel <= totallevels) {
                                            if ((attributeLevel + 1) <= totallevels) {
                                                $timeout(function () {
                                                    scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
                                                }, 100)
                                            }
                                        }
                                    }
                                }
                            }, 100);
                        };
                        scope.getPercentageTotal = function (leveloption) {
                            var total = 0;
                            for (var i = 0, obj; obj = leveloption[i++];) {
                                total += parseInt(obj.value != "" ? obj.value : 0);
                            }
                            return total;
                        }
                        scope.ErrorPercentageClass = function (leveloptions) {
                            var remainvalue = 0;
                            remainvalue = scope.getPercentageTotal(leveloptions);
                            if (remainvalue > 100) return "result lapse"
                            else if (remainvalue < 100) return "result lapse"
                            else return "";
                        }
                        scope.AutoAssignPercentage = function (leveloption, levelSelection) {
                            if (levelSelection.length > 1) {
                                var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
                                emptyBoxes = ($.grep(leveloption, function (e) {
                                    return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
                                }));
                                if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
                                    fillObj = emptyBoxes[emptyBoxes.length - 1];
                                    if (fillObj != null) {
                                        remainvalue = 100 - scope.getPercentageTotal(leveloption);
                                        if (remainvalue > 0) fillObj.value = remainvalue.toString();
                                        if (remainvalue == 0) {
                                            var val = 0;
                                            fillObj.value = val.toString();
                                        }
                                    }
                                }
                            }
                        };
                        scope.getOptions = function (level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
                            if (level == 1) return currentoptions;
                            else {
                                var matches = [];
                                if (previousoptions != undefined) {
                                    if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                        return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
                                    });
                                }
                                return matches;
                            }
                        };
                        scope.getPricingRecords = function (currentoptions, currentselection) {
                            var matches = [];
                            if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                return currentselection.indexOf(relation.NodeId.toString()) != -1;
                            });
                            return matches;
                        };
                    },
                    post: function postLink(scope, iElement, iAttrs, controller) { }
                }
            }
        }
    }]);
    app.directive('drowdowntreepercentagemultiselectionfilter', ['$compile', '$timeout', function ($compile, $timeout) {
        return {
            priority: 100,
            terminal: true,
            compile: function compile(scope, tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) {
                        var htmlText = "";
                        var levels = [];
                        var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
                        var directiveObj = scope.DropDownFilterTreePricing["AttributeId_Levels_" + parseInt(iAttrs.attributeid) + ""];
                        if (directiveObj != null) {
                            htmlText += '  <div data-role="formpercentagetotalcontainer" class=\"control-group\"> ';
                            htmlText += '         <div   ng-repeat ="child in DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '"> ';
                            htmlText += '               <label class="control-label">{{child.LevelName}}</label> ';
                            htmlText += '                <div class="controls"> ';
                            htmlText += '                      <select ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
                            htmlText += '                              <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
                            htmlText += '                      </select> ';
                            htmlText += '                 </div> ';
                            htmlText += '         </div> ';
                            htmlText += '  </div> ';
                            var tpl = htmlText;
                            iElement.html(tpl);
                            $compile(iElement.contents())(scope);
                        }
                        scope.OptionManipulation = function (attributeid, attributeLevel, totallevels, attributetypeid) {
                            $timeout(function () {
                                var attributeLevelOptions = [];
                                attributeLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                    return e.level == attributeLevel;
                                }));
                                if (attributeLevelOptions[0] != undefined) {
                                    if (attributeLevelOptions[0].selection != undefined) {
                                        if (attributeLevelOptions[0].selection.length > 0) {
                                            try {
                                                var valueMatches = [];
                                                if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                    return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
                                                });
                                                else valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                                scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
                                            } catch (e) { }
                                            for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
                                                if (attributeLevel <= totallevels) {
                                                    if ((attributeLevel + 1) <= totallevels) {
                                                        var attributenextLevelOptions = [];
                                                        attributenextLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                                            return e.level == (attributeLevel + 1);
                                                        }));
                                                        if (attributenextLevelOptions[0] != undefined) {
                                                            if (attributenextLevelOptions[0].selection != undefined) {
                                                                if (attributenextLevelOptions[0].selection.length > 0) {
                                                                    var deletableIds = new Array();
                                                                    for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
                                                                        var childidObj = [];
                                                                        childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function (e) {
                                                                            return e.NodeId == parseInt(childid);
                                                                        }));
                                                                        if (childidObj.length > 0) {
                                                                            var removableLevelOptions = [];
                                                                            removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function (e) {
                                                                                return e == childidObj[0].LevelParent.toString();
                                                                            }));
                                                                            if (removableLevelOptions.length == 0) {
                                                                                childidObj[0].value = "";
                                                                                deletableIds.push(childid.toString());
                                                                            }
                                                                        }
                                                                    }
                                                                    for (var l = 0, id; id = deletableIds[l++];) {
                                                                        attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            try {
                                                var valueMatches = attributeLevelOptions[0].LevelOptions;
                                                if (valueMatches != undefined) {
                                                    if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
                                                        level.value = "";
                                                    }
                                                }
                                            } catch (e) { }
                                            if (attributeLevel <= totallevels) {
                                                if ((attributeLevel + 1) <= totallevels) {
                                                    var attributenextLevelOptions = [];
                                                    attributenextLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function (e) {
                                                        return e.level == (attributeLevel + 1);
                                                    }));
                                                    if (attributenextLevelOptions[0] != undefined) {
                                                        if (attributenextLevelOptions[0].selection != undefined) {
                                                            if (attributenextLevelOptions[0].selection.length > 0) {
                                                                attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (attributeLevel <= totallevels) {
                                            if ((attributeLevel + 1) <= totallevels) {
                                                $timeout(function () {
                                                    scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
                                                }, 100)
                                            }
                                        }
                                    }
                                }
                            }, 100);
                        };
                        scope.getPercentageTotal = function (leveloption) {
                            var total = 0;
                            for (var i = 0, obj; obj = leveloption[i++];) {
                                total += parseInt(obj.value != "" ? obj.value : 0);
                            }
                            return total;
                        }
                        scope.ErrorPercentageClass = function (leveloptions) {
                            var remainvalue = 0;
                            remainvalue = scope.getPercentageTotal(leveloptions);
                            if (remainvalue > 100) return "result lapse"
                            else if (remainvalue < 100) return "result lapse"
                            else return "";
                        }
                        scope.AutoAssignPercentage = function (leveloption, levelSelection) {
                            if (levelSelection.length > 1) {
                                var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
                                emptyBoxes = ($.grep(leveloption, function (e) {
                                    return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
                                }));
                                if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
                                    fillObj = emptyBoxes[emptyBoxes.length - 1];
                                    if (fillObj != null) {
                                        remainvalue = 100 - scope.getPercentageTotal(leveloption);
                                        if (remainvalue > 0) fillObj.value = remainvalue.toString();
                                        if (remainvalue == 0) {
                                            var val = 0;
                                            fillObj.value = val.toString();
                                        }
                                    }
                                }
                            }
                        };
                        scope.getOptions = function (level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
                            if (level == 1) return currentoptions;
                            else {
                                var matches = [];
                                if (previousoptions != null) if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                    return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
                                });
                                return matches;
                            }
                        };
                        scope.getPricingRecords = function (currentoptions, currentselection) {
                            var matches = [];
                            if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function (relation) {
                                return currentselection.indexOf(relation.NodeId.toString()) != -1;
                            });
                            return matches;
                        };
                    },
                    post: function postLink(scope, iElement, iAttrs, controller) { }
                }
            }
        }
    }]);

})(angular, app);