﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
  public interface IPredefineObjectives
    {
      int ObjectiveEntityId { get; set; }
      int ObjectiveId { get; set; }
      string ObjectiveName { get; set; }
      string ObjectiveDescription { get; set; }
      int ObjectiveTypeId { get; set; }
      decimal PlannedTarget { get; set; }
      decimal TargetOutcome { get; set; }
      int Rating { get; set; }
      string RatingCaption { get; set; }
      int FulfillmentState { get; set; }
      string UnitName { get; set; }
      int Satus { get; set; }
      int UnitId { get; set; }
      string ObjectiveComments { get; set; }
      bool IsEnableFeedback { get; set; }
      IList<IEntityRatings> ObjectiveRatings { get; set; }
    }
  public interface IEntityRatings
  {
      int RatingId { get; set; }
      string RatingName { get; set; }
      int SortOrder { get; set; }
  }
}
