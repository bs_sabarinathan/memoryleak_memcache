﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CostCenterDao object for table 'PM_CostCenter'.
	/// </summary>
	
	public partial class EntityCostReleationsDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _entityid;
        protected int _costcenterid;
		protected int _sortorder;
        protected bool _isassociate;
        protected bool _isactive;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EntityCostReleationsDao() {}

        public EntityCostReleationsDao(int pId, int pEntityId, int pCostcenterId, int pSortorder, bool pIsassociate, bool pIsactive)
		{
			this._id = pId;
            this._entityid = pEntityId;
            this._costcenterid = pCostcenterId;
            this._sortorder = pSortorder;
            this._isassociate = pIsassociate;
            this._isactive = pIsactive;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual int CostcenterId
        {
            get { return _costcenterid; }
            set { _bIsChanged |= (_costcenterid != value); _costcenterid = value; }

        }

        public virtual int Sortorder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }

        public virtual bool Isassociate
        {
            get { return _isassociate; }
            set { _bIsChanged |= (_isassociate != value); _isassociate = value; }

        }

        public virtual bool Isactive
        {
            get { return _isactive; }
            set { _bIsChanged |= (_isactive != value); _isactive = value; }

        }
		

		public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods
		
		public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string EntityCostReleationsDao = "CostCenterDao";			
			public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string CostcenterId = "CostcenterId";
            public const string Sortorder = "Sortorder";
            public const string Isassociate = "Isassociate";
            public const string Isactive = "Isactive";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string EntityCostReleationsDao = "CostCenterDao";
            public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string CostcenterId = "CostcenterId";
            public const string Sortorder = "Sortorder";
            public const string Isassociate = "Isassociate";
            public const string Isactive = "Isactive";	
		}

		#endregion
		
		
	}
	
}

