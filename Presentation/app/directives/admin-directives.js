﻿(function (ng, app) {
    app.directive("bnsortable", ['$parse', function ($parse) {
        function link($scope, element, attributes) {
            function getNgRepeatExpression() {
                var ngRepeatComment = element.contents().filter(function () {
                    return ((this.nodeType === 8) && (this.nodeValue.indexOf("ngRepeat:") !== -1));
                });
                return (getNgRepeatExpressionFromComment(ngRepeatComment[0]));
            }

            function getNgRepeatExpressionFromComment(comment) {
                var parts = comment.nodeValue.split(":");
                return (parts[1].replace(/^\s+|\s+$/g, ""));
            }

            function getNgRepeatChildren() {
                var attributeVariants = ["[ng-repeat]", "[data-ng-repeat]", "[x-ng-repeat]", "[ng_repeat]", "[ngRepeat]", "[ng\\:repeat]"];
                return (element.children(attributeVariants.join(",")));
            }

            function handleUpdate(event, ui) {
                var children = getNgRepeatChildren();
                var items = $.map(children, function (domItem, index) {
                    var scope = $(domItem).scope();
                    scope[itemName].SortOrder = (index + 1);
                    return (scope[itemName]);
                });
                $scope.$apply(function () {
                    collectionSetter($scope, items);
                });
            }
            var expressionPattern = /^([^\s]+) in (.+)$/i;
            var expression = getNgRepeatExpression();
            if (!expressionPattern.test(expression)) {
                throw (new Error("Expected ITEM in COLLECTION expression."));
            }
            var expressionParts = expression.match(expressionPattern);
            var itemName = expressionParts[1];
            var collectionName = expressionParts[2];
            var collectionGetter = $parse(collectionName);
            var collectionSetter = collectionGetter.assign;
            element.sortable({
                cursor: "move",
                axis: 'y',
                opacity: 0.7,
                stop: handleUpdate
            });
            $scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
        return ({
            link: link,
            restrict: "A"
        });
    }]);
    app.directive('onRepeatLast', [function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var isLast = scope.$last || scope.$parent.$last;
                if (isLast) {
                    scope.$evalAsync(attrs.onRepeatLast);
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        }
    }]);
    app.controller('gridster1Ctrl', ['$window', '$scope', '$element', '$attrs', '$timeout', function ($window, $scope, $element, $attrs, $timeout) {
        var self = this;
        self.$el = $($element);
        self.gridster1 = null;
        self.updateModel = function (event, ui) {
            var serializedGrid = self.gridster1.serialize();
            angular.forEach($scope.widgets, function (widget, idx) {
                widget.grid = serializedGrid[idx];
            });
            $scope.$apply();
        };
        self.defaultOptions = {
            shift_larger_widgets_down: false,
            max_size_x: 6,
            cols: 6,
            margin_ratio: 0.1,
            resize_time: 500,
            serialize_params: function ($w, wgd) {
                return {
                    col: wgd.col,
                    row: wgd.row,
                    sizex: wgd.size_x,
                    sizey: wgd.size_y
                }
            },
            draggable: {
                stop: self.updateModel
            }
        };
        self.options = angular.extend(self.defaultOptions, $scope.$eval($attrs.options));
        self.attachElementTogridster1 = function (li) {
            var $w = li.addClass('gs_w').appendTo(self.gridster1.$el);
            self.gridster1.$widgets = self.gridster1.$widgets.add($w);
            self.gridster1.register_widget($w).add_faux_rows(1).set_dom_grid_height();
            $w.css('opacity', 1);
        };

        function calculateNewDimensions() {
            var containerWidth = self.$el.innerWidth();
            var newMargin = Math.round(containerWidth * self.options.margin_ratio / (self.options.cols * 2));
            var newSize = Math.round(containerWidth * (1 - self.options.margin_ratio) / self.options.cols);
            return [[newSize, newSize], [newMargin, newMargin]];
        }
        self.resizeWidgetDimensions = function () {
            var newDimensions = calculateNewDimensions();
            self.gridster1.resize_widget_dimensions({
                widget_base_dimensions: newDimensions[0],
                widget_margins: newDimensions[1]
            });
            self.updateModel();
        };
        self.hookWidgetResizer = function () {
            self.resizeWidgetDimensions();
            $($window).on('resize', function (evt) {
                evt.preventDefault();
                $window.clearTimeout(self.resizeTimer);
                self.resizeTimer = $window.setTimeout(function () {
                    self.resizeWidgetDimensions();
                }, self.options.resize_time);
            });
        };
        $scope.$watch('widgets.length', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            if (newValue === oldValue + 1) {
                $timeout(function () {
                    var li = self.$el.find('ul').find('li').last();
                    self.attachElementTogridster1(li);
                });
            } else { }
        });
        $scope.removeWidget = function (widget) {
            var id = widget.id;
            var $li = self.$el.find('[data-widget-id="' + id + '"]');
            $li.fadeOut('slow', function () {
                self.gridster1.remove_widget($li, function () {
                    $scope.onremove({
                        widget: widget
                    });
                    self.resizeWidgetDimensions();
                });
            });
        };
    }]);
    app.directive('gridster1', ['$timeout', function ($timeout) {
        return {
            restrict: 'AC',
            scope: {
                widgets: '=',
                onremove: '&'
            },
            templateUrl: 'views/mui/admin/gridster.html',
            controller: 'gridster1Ctrl',
            link: function (scope, elm, attrs, ctrl) {
                elm.css('opacity', 0);
                scope.initGrid = function () {
                    $timeout(function () {
                        var $ul = ctrl.$el.find('ul');
                        ctrl.gridster1 = $ul.gridster1(ctrl.options).data('gridster1');
                        ctrl.hookWidgetResizer();
                        scope.$broadcast('gridReady');
                        ctrl.resizeWidgetDimensions();
                        elm.css('opacity', 1);
                    });
                };
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('widget1', ['$timeout', 'uuid', function ($timeout, uuid) {
        return {
            restrict: 'A',
            require: '^gridster1',
            scope: {
                remove: '&',
                widget: '='
            },
            link: function (scope, element, attrs, ctrl) {
                var $el = $(element);
                if (scope.widget.id === undefined) {
                    scope.widget.id = uuid.generate();
                }
                scope.$on('gridReady', function (event) {
                    var base_size = ctrl.gridster1.options.widget_base_dimensions;
                    var margins = ctrl.gridster1.options.widget_margins;
                    var headerHeight = $el.find('header').outerHeight();
                    var $content = $el.find('.content');
                    $el.resizable({
                        grid: [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)],
                        animate: false,
                        containment: ctrl.$el,
                        autoHide: false,
                        start: function (event, ui) {
                            var base_size = ctrl.gridster1.options.widget_base_dimensions;
                            var margins = ctrl.gridster1.options.widget_margins;
                            element.resizable('option', 'grid', [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)]);
                        },
                        resize: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                        },
                        stop: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                            setTimeout(function () {
                                sizeToGrid($el);
                                ctrl.updateModel();
                            }, 300);
                        }
                    });
                    $('.ui-resizable-handle, .no-drag, .disabled, [disabled]', $el).hover(function () {
                        ctrl.gridster1.disable();
                    }, function () {
                        ctrl.gridster1.enable();
                    });
                });

                function sizeToGrid($el) {
                    var base_size = ctrl.gridster1.options.widget_base_dimensions;
                    var margins = ctrl.gridster1.options.widget_margins;
                    var w = ($el.width() - base_size[0]);
                    var h = ($el.height() - base_size[1]);
                    for (var grid_w = 1; w > 0; w -= ((base_size[0] + (margins[0] * 2)))) {
                        grid_w++;
                    }
                    for (var grid_h = 1; h > 0; h -= ((base_size[1] + (margins[1] * 2)))) {
                        grid_h++;
                    }
                    $el.css({
                        width: '',
                        height: '',
                        top: '',
                        left: '',
                        position: ''
                    });
                    ctrl.gridster1.resize_widget($el, grid_w, grid_h);
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('dropdragdir', [function () {
        return function (scope, element, attrs) {
            var toUpdate;
            var startIndex = -1;
            scope.$watch(attrs.dropdragdir, function (value) {
                toUpdate = value;
            }, true);
            $(element[0]).sortable({
                items: 'li',
                start: function (event, ui) {
                    startIndex = ($(ui.item).index());
                },
                stop: function (event, ui) {
                    var newIndex = ($(ui.item).index());
                    var toMove = toUpdate[startIndex];
                    toUpdate.splice(startIndex, 1);
                    toUpdate.splice(newIndex, 0, toMove);
                    scope.$apply(scope.entitytypeattributrearr);
                },
                axis: 'y'
            })
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive("reportblocksort", ['$parse', '$timeout', function ($parse, $timeout) {
        function link($scope, element, attributes) {
            function getNgRepeatExpression() {
                var ngRepeatComment = element.contents().filter(function () {
                    return ((this.nodeType === 8) && (this.nodeValue.indexOf("ngRepeat:") !== -1));
                });
                return (getNgRepeatExpressionFromComment(ngRepeatComment[0]));
            }

            function getNgRepeatExpressionFromComment(comment) {
                var parts = comment.nodeValue.split(":");
                return (parts[1].replace(/^\s+|\s+$/g, ""));
            }

            function getNgRepeatChildren() {
                var attributeVariants = ["[ng-repeat]", "[data-ng-repeat]", "[x-ng-repeat]", "[ng_repeat]", "[ngRepeat]", "[ng\\:repeat]"];
                return (element.children(attributeVariants.join(",")));
            }

            function handleUpdate(event, ui) {
                var children = getNgRepeatChildren();
                var items = $.map(children, function (domItem, index) {
                    var scope = $(domItem).scope();
                    return (scope[itemName]);
                });
                $scope.$apply(function () {
                    if ($scope.reportJSONObject.root != undefined) {
                        for (var i = 0; i < $scope.reportJSONObject.root.block.length; i++) {
                            if ($scope.reportJSONObject.root.block[i].id == $scope.blocks.id) {
                                $scope.reportJSONObject.root.block[i].columns[0].column = items;
                            }
                        }
                    }
                    collectionSetter($scope, items);
                });
            }
            var expressionPattern = /^([^\s]+) in (.+)$/i;
            var expression = getNgRepeatExpression();
            if (!expressionPattern.test(expression)) {
                throw (new Error("Expected ITEM in COLLECTION expression."));
            }
            var expressionParts = expression.match(expressionPattern);
            var itemName = expressionParts[1];
            var collectionName = expressionParts[2];
            var collectionGetter = $parse(collectionName);
            var collectionSetter = collectionGetter.assign;
            element.sortable({
                cursor: "move",
                update: handleUpdate,
                opacity: 0.7,
                vertical: true,
                axis: 'y',
            });
            $scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
        return ({
            link: link,
            restrict: "A"
        });
    }]);
    app.directive('detailblockcolumns', [function () {
        return {
            restrict: 'A',
            template: '<li class="active" ng-repeat="column in columnvalue.column">' + '<a href="JavaScript: void(0);">' + '<label class="checkbox checkbox-custom pull-left">' + '<input id="Checkbox2" ng-click="columnmanipulation(column,$event)" ng-checked="column.isselected==\'1\'" type="checkbox"><i class="checkbox" ng-class="{\'checkbox checked\': column.isselected==\'1\', \'checkbox\': column.isselected==\'0\'}"></i>' + ' </label>' + '<input type="text" ng-model="column.caption" placeholder="{{column.caption}}" class="input-medium">' + '</a>' + '</li>',
            transclude: true,
            scope: {
                columnvalue: '=',
                columncontent: '=',
                blocktype: '=',
                blockstaticcolumn: '=',
                blockentitytyperelation: '=',
                blockentitytype: '='
            },
            link: function (scope, elem, attrs) {
                var updateStars = function () {
                    if (scope.blockentitytype != "" && scope.blockentitytype != undefined) {
                        if (scope.columnvalue.column != undefined) {
                            var alreadyAvailed = [];
                            for (var i = 0, columndata; columndata = scope.blockstaticcolumn[i++];) {
                                alreadyAvailed = $.grep(scope.columnvalue.column, function (e) {
                                    return e.id == columndata.id;
                                });
                                if (alreadyAvailed.length == 0) {
                                    scope.columnvalue.column.push({
                                        id: columndata.id,
                                        caption: columndata.caption,
                                        isselected: columndata.isselected
                                    });
                                }
                            }
                            var dynamicattributes = [];
                            var dynamicattributesres = [];
                            dynamicattributesres = jQuery.grep(scope.blockentitytyperelation, function (relation) {
                                return scope.blockentitytype.indexOf(relation.EntityTypeID.toString()) != -1;
                            });
                            if (dynamicattributesres.length > 0) {
                                for (var j = 0, columntyperesdata; columntyperesdata = dynamicattributesres[j++];) {
                                    alreadyAvailed = [];
                                    alreadyAvailed = $.grep(scope.columnvalue.column, function (e) {
                                        return e.id == columntyperesdata.strAttributeID;
                                    });
                                    if (alreadyAvailed.length == 0) {
                                        scope.columnvalue.column.push({
                                            id: columntyperesdata.strAttributeID,
                                            caption: columntyperesdata.AttributeCaption,
                                            isselected: '0'
                                        });
                                    }
                                }
                            }
                        } else {
                            var defaultcolumns = [];
                            scope.columnvalue.column = scope.blockstaticcolumn;
                        }
                    } else {
                        if (scope.columnvalue != undefined) {
                            if (scope.columnvalue.column != undefined) {
                                scope.columnvalue.column = [];
                            }
                        } else {
                            var columns = [{
                                "column": []
                            }];
                            scope.columncontent["columns"] = columns;
                        }
                    }
                };
                scope.columnmanipulation = function (column, $event) {
                    var checkbox = $event.target;
                    column.isselected = checkbox.checked == true ? '1' : '0';
                };
                scope.$watch('blocktype', function (oldVal, newVal) {
                    if (newVal) {
                        if (newVal == 3) {
                            updateStars();
                        }
                    }
                });
                scope.$watch('blockentitytype', function (oldVal, newVal) {
                    if (newVal) {
                        if (scope.blocktype == 3) {
                            updateStars();
                        }
                    } else if (newVal == null) {
                        updateStars();
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        }
    }]);
    app.directive('ngblur', [function () {
        return {
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function () {
                    var id = attrs.langtypeid;
                    var typeid = attrs.contentid;
                    var iterater = attrs.iterater;
                    var oldvalue = scope.actualfields["Caption_" + iterater + ""];
                    var newvalue = element[0].value;
                    if (oldvalue != newvalue) {
                        $(element).addClass('success');
                        scope.newlanguagecontent(attrs.langtypeid, attrs.contentid, newvalue, element);
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablelanguagename', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var LanguageID = parseInt(attrs.languageid, 10);
                var IsName = attrs.isname;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            if (value == "") value = "-";
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.UpdateLanguageName(LanguageID, IsName, params.newValue);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablereports', ['$timeout', '$translate', function ($timeout, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var ISIDCaption = parseInt(attrs.isidcaption, 10);
                var ID = parseInt(attrs.isid, 10);
                var OID = parseInt(attrs.isoid, 10);
                var Name = attrs.isname;
                var Caption = attrs.iscaption;
                var Description = attrs.isdescription;
                var preview = attrs.ispreview;
                var Show = attrs.isshow;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        },
                        validate: function (value) {
                            if ($.trim(value) == '') {
                                if (parseInt(attrs.isidcaption, 10) == 1) {
                                    return 'Please enter the caption';
                                }
                                if (parseInt(attrs.isidcaption, 10) == 0) {
                                    return 'Please enter the description';
                                }
                            }
                            if ($.trim(value) == '-') {
                                if (parseInt(attrs.isidcaption, 10) == 1) {
                                    return 'Please enter the valid text';
                                }
                            }
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    if (editable != undefined) {
                        $timeout(function () {
                            $(".input-medium").focus().select()
                        }, 500);
                        $('[class=input-medium]').keyup(function (e) {
                            if (e.keyCode == 32) {
                                if (($('[class=input-medium]').val().length == 1)) {
                                    var value = $.trim($('[class=input-medium]').val());
                                    $('[class=input-medium]').val(value);
                                    bootbox.alert($translate.instant('LanguageContents.Res_1841.Caption'));
                                }
                            }
                        });
                    }
                });
                angular.element(element).on('save', function (e, params) {
                    scope.UpdateReportValues(parseInt(attrs.isidcaption, 10), parseInt(attrs.isid, 10), parseInt(attrs.isoid, 10), attrs.isname, attrs.iscaption, attrs.isdescription, attrs.ispreview, attrs.isshow, attrs.iscategoryid, attrs.isentitylevel, attrs.issublevel, params.newValue);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive("entitytypetreetooltip", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            var attrslst = attrs;
            $(element).qtip({
                content: {
                    text: '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',
                },
                events: {
                    show: function (event, api) {
                        if (attrslst.tooltiptype == "ENTITYTYPETREE") {
                            var qtipContent_id = api.elements.content.attr('id');
                            scope.LoadtQtipForEntityTypeTreeStructure(qtipContent_id, attrslst.entitytypeid);
                        }
                    }
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom center',
                        tooltip: 'bottom center',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('dropdragdir', [function () {
        return function (scope, element, attrs) {
            var toUpdate;
            var startIndex = -1;
            scope.$watch(attrs.dropdragdir, function (value) {
                toUpdate = value;
            }, true);
            $(element[0]).sortable({
                items: 'li',
                start: function (event, ui) {
                    startIndex = ($(ui.item).index());
                },
                stop: function (event, ui) {
                    var newIndex = ($(ui.item).index());
                    var toMove = toUpdate[startIndex];
                    toUpdate.splice(startIndex, 1);
                    toUpdate.splice(newIndex, 0, toMove);
                    scope.$apply(scope.entitytypeattributrearr);
                },
                axis: 'y'
            })
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('contenteditable', ['$sce', function ($sce) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) return;
                ngModel.$render = function () {
                    element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                };
                element.on('blur keyup change', function () {
                    scope.$evalAsync(read);
                });
                read();

                function read() {
                    var html = element.html();
                    if (attrs.stripBr && html == '<br>') {
                        html = '';
                    }
                    ngModel.$setViewValue(html);
                }
            }
        };
    }]);
    //app.directive('uicolorpicker', function () {
    //    return {
    //        restrict: 'E',
    //        require: 'ngModel',
    //        replace: true,
    //        template: '<span><input  class="input-small" /></span>',
    //        link: function (scope, $element, attrs, $ngModel) {
    //            var $input = $element.find('input');
    //            var fallbackValue = scope.$eval(attrs.fallbackValue);
    //            var format = scope.$eval(attrs.format) || undefined;

    //            function setViewValue(color) {
    //                var value = fallbackValue;
    //                if (color) {
    //                    value = color.toString(format);
    //                } else if (angular.isUndefined(fallbackValue)) {
    //                    value = color;
    //                }
    //                $ngModel.$setViewValue(value.replace("#", ""));
    //                scope.colorchange(value);
    //            }
    //            var onChange = function (color) {
    //                scope.$apply(function () {
    //                    setViewValue(color);
    //                });
    //            };
    //            var onToggle = function () {
    //                $input.spectrum('toggle');
    //                return false;
    //            };
    //            var options = angular.extend({
    //                color: $ngModel.$viewValue,
    //                change: onChange,
    //            }, scope.$eval(attrs.options));
    //            $ngModel.$render = function () {
    //                $input.spectrum('set', $ngModel.$viewValue || '');
    //            };
    //            if (options.color) {
    //                $input.spectrum('set', options.color || '');
    //                setViewValue(options.color);
    //            }
    //            $input.spectrum(options);
    //            scope.$on('$destroy', function () {
    //                $input.spectrum('destroy');
    //            });
    //        }
    //    };
    //});
    app.directive('intentbuttonsRadio', function () {
        return {
            restrict: 'E',
            scope: {
                model: '=',
                options: '=',
                changeselection: "&"
            },
            controller: function ($scope) {
                $scope.activate = function (option) {
                    $scope.model = option.Lable;
                };
                $scope.Returnclass = function (model) {
                    if ($scope.model == model.Lable) {
                        return "btn";
                    } else return "btn";
                }
            },
            template: "<button type='button' class='{{Returnclass(option)}}' " + "ng-class='{active: option.Lable == model}'" + "ng-repeat='option in options' " + "ng-click='activate(option);changeselection(option);'>" + "<i ng-class='option.class'></i>" + "</button>"
        };
    });
})(angular, app);