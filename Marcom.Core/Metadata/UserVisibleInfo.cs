/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// UserVisibleInfo object for table 'MM_UserVisibleInfo'.
	/// </summary>
	
	internal class UserVisibleInfo : IUserVisibleInfo
	{
		#region Member Variables

		protected int _id;
		protected int _attributeid;
		
		#endregion
		
		#region Constructors
		public UserVisibleInfo() {}

        public UserVisibleInfo(int pId, int pAttribtueId)
		{
			this._id = pId;
            this._attributeid = pAttribtueId; 
		}

        public UserVisibleInfo(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
        public int AttributeId
		{
			get { return _attributeid; }
            set { _attributeid = value; }
			
		}
		
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            UserVisibleInfo castObj = null;
			try
			{
                castObj = (UserVisibleInfo)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
