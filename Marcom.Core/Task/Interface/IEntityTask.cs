﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;



namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IEntityTask
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        int MemberID
        {
            get;
            set;

        }



        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }


        int TaskStatus
        {
            get;
            set;

        }



        DateTime? DueDate
        {
            get;
            set;
        }


        int EntityTaskListID
        {
            get;
            set;

        }


        int TaskListID
        {
            get;
            set;

        }
        int TaskType
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;
        }

        int AssetId
        {
            get;
            set;
        }

        string Note
        {
            get;
            set;

        }

        int DalimID
        {
            get;
            set;

        }

        int? TaskFlowID
        {
            get;
            set;
        }

        bool IsWorkStart { get; set; }

        string TaskTypeName { get; set; }
        int TotalTaskAttachment { get; set; }
        dynamic AttributeData { get; set; }
        string StatusName { get; set; }
        string strDate { get; set; }
        dynamic taskAssigness { get; set; }
        IList<ITaskMembers> taskMembers { get; set; }
        dynamic taskAttachment { get; set; }
        string AssigneeName { get; set; }
        int AssigneeID { get; set; }
        int totalDueDays { get; set; }
        string MemberFlagColorCode { get; set; }
        dynamic TotaltaskAssigness { get; set; }
        string ProgressCount { get; set; }
        dynamic TaskCheckList { get; set; }
        int TaskTypeID { get; set; }
        string colorcode { get; set; }
        string shortdesc { get; set; }
        #endregion
    }
}
