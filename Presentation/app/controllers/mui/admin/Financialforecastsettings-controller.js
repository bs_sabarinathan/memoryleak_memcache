﻿(function (ng, app) {
    "use strict";

    function muiadminfinancialforecastsettingsctrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        $scope.DefaultCurrencySelected = "";
        $scope.DefaultCurrency = [{
            Id: '1',
            name: 'AlbaniaLek',
            code: 'ALL',
            symbol: 'Lek'
        }, {
            Id: '2',
            name: 'AfghanistanAfghani',
            code: 'AFN',
            symbol: '؋'
        }, {
            Id: '3',
            name: 'ArgentinaPeso',
            code: 'ARS',
            symbol: '$'
        }, {
            Id: '4',
            name: 'ArubaGuilder',
            code: 'AWG',
            symbol: 'ƒ'
        }, {
            Id: '5',
            name: 'AustraliaDollar',
            code: 'AUD',
            symbol: '$'
        }, {
            Id: '6',
            name: 'AzerbaijanNewManat',
            code: 'AZN',
            symbol: 'ман'
        }, {
            Id: '7',
            name: 'BahamasDollar',
            code: 'BSD',
            symbol: '$'
        }, {
            Id: '8',
            name: 'BarbadosDollar',
            code: 'BBD',
            symbol: '$'
        }, {
            Id: '9',
            name: 'BelarusRuble',
            code: 'BYR',
            symbol: 'p.'
        }, {
            Id: '10',
            name: 'BelizeDollar',
            code: 'BZD',
            symbol: 'BZ$'
        }, {
            Id: '11',
            name: 'BermudaDollar',
            code: 'BMD',
            symbol: '$'
        }, {
            Id: '12',
            name: 'BoliviaBoliviano',
            code: 'BOB',
            symbol: '$b'
        }, {
            Id: '13',
            name: 'BosniaandHerzegovinaConvertibleMarka',
            code: 'BAM',
            symbol: 'KM'
        }, {
            Id: '14',
            name: 'BotswanaPula',
            code: 'BWP',
            symbol: 'P'
        }, {
            Id: '15',
            name: 'BulgariaLev',
            code: 'BGN',
            symbol: 'лв'
        }, {
            Id: '16',
            name: 'BrazilReal',
            code: 'BRL',
            symbol: 'R$'
        }, {
            Id: '17',
            name: 'BruneiDarussalamDollar',
            code: 'BND',
            symbol: '$'
        }, {
            Id: '18',
            name: 'CambodiaRiel',
            code: 'KHR',
            symbol: '៛'
        }, {
            Id: '19',
            name: 'CanadaDollar',
            code: 'CAD',
            symbol: '$'
        }, {
            Id: '20',
            name: 'CaymanIslandsDollar',
            code: 'KYD',
            symbol: '$'
        }, {
            Id: '21',
            name: 'ChilePeso',
            code: 'CLP',
            symbol: '$'
        }, {
            Id: '22',
            name: 'ChinaYuanRenminbi',
            code: 'CNY',
            symbol: '¥'
        }, {
            Id: '23',
            name: 'ColombiaPeso',
            code: 'COP',
            symbol: '$'
        }, {
            Id: '24',
            name: 'CostaRicaColon',
            code: 'CRC',
            symbol: '₡'
        }, {
            Id: '25',
            name: 'CroatiaKuna',
            code: 'HRK',
            symbol: 'kn'
        }, {
            Id: '26',
            name: 'CubaPeso',
            code: 'CUP',
            symbol: '₱'
        }, {
            Id: '27',
            name: 'CzechRepublicKoruna',
            code: 'CZK',
            symbol: 'Kč'
        }, {
            Id: '28',
            name: 'DenmarkKrone',
            code: 'DKK',
            symbol: 'kr'
        }, {
            Id: '29',
            name: 'DominicanRepublicPeso',
            code: 'DOP',
            symbol: 'RD$'
        }, {
            Id: '30',
            name: 'EastCaribbeanDollar',
            code: 'XCD',
            symbol: '$'
        }, {
            Id: '31',
            name: 'EgyptPound',
            code: 'EGP',
            symbol: '£'
        }, {
            Id: '32',
            name: 'ElSalvadorColon',
            code: 'SVC',
            symbol: '$'
        }, {
            Id: '33',
            name: 'EstoniaKroon',
            code: 'EEK',
            symbol: 'kr'
        }, {
            Id: '34',
            name: 'EuroMemberCountries',
            code: 'EUR',
            symbol: '€'
        }, {
            Id: '35',
            name: 'FalklandIslands(Malvinas)Pound',
            code: 'FKP',
            symbol: '£'
        }, {
            Id: '36',
            name: 'FijiDollar',
            code: 'FJD',
            symbol: '$'
        }, {
            Id: '37',
            name: 'GhanaCedi',
            code: 'GHC',
            symbol: '¢'
        }, {
            Id: '38',
            name: 'GibraltarPound',
            code: 'GIP',
            symbol: '£'
        }, {
            Id: '39',
            name: 'GuatemalaQuetzal',
            code: 'GTQ',
            symbol: 'Q'
        }, {
            Id: '40',
            name: 'GuernseyPound',
            code: 'GGP',
            symbol: '£'
        }, {
            Id: '41',
            name: 'GuyanaDollar',
            code: 'GYD',
            symbol: '$'
        }, {
            Id: '42',
            name: 'HondurasLempira',
            code: 'HNL',
            symbol: 'L'
        }, {
            Id: '43',
            name: 'HongKongDollar',
            code: 'HKD',
            symbol: '$'
        }, {
            Id: '44',
            name: 'HungaryForint',
            code: 'HUF',
            symbol: 'Ft'
        }, {
            Id: '45',
            name: 'IcelandKrona',
            code: 'ISK',
            symbol: 'kr'
        }, {
            Id: '46',
            name: 'IndiaRupee',
            code: 'INR',
            symbol: '-'
        }, {
            Id: '47',
            name: 'IndonesiaRupiah',
            code: 'IDR',
            symbol: 'Rp'
        }, {
            Id: '48',
            name: 'IranRial',
            code: 'IRR',
            symbol: '﷼'
        }, {
            Id: '49',
            name: 'IsleofManPound',
            code: 'IMP',
            symbol: '£'
        }, {
            Id: '50',
            name: 'IsraelShekel',
            code: 'ILS',
            symbol: '₪'
        }, {
            Id: '51',
            name: 'JamaicaDollar',
            code: 'JMD',
            symbol: 'J$'
        }, {
            Id: '52',
            name: 'JapanYen',
            code: 'JPY',
            symbol: '¥'
        }, {
            Id: '53',
            name: 'JerseyPound',
            code: 'JEP',
            symbol: '£'
        }, {
            Id: '54',
            name: 'KazakhstanTenge',
            code: 'KZT',
            symbol: 'лв'
        }, {
            Id: '55',
            name: 'Korea(North)Won',
            code: 'KPW',
            symbol: '₩'
        }, {
            Id: '56',
            name: 'Korea(South)Won',
            code: 'KRW',
            symbol: '₩'
        }, {
            Id: '57',
            name: 'KyrgyzstanSom',
            code: 'KGS',
            symbol: 'лв'
        }, {
            Id: '58',
            name: 'LaosKip',
            code: 'LAK',
            symbol: '₭'
        }, {
            Id: '59',
            name: 'LatviaLat',
            code: 'LVL',
            symbol: 'Ls'
        }, {
            Id: '60',
            name: 'LebanonPound',
            code: 'LBP',
            symbol: '£'
        }, {
            Id: '61',
            name: 'LiberiaDollar',
            code: 'LRD',
            symbol: '$'
        }, {
            Id: '62',
            name: 'LithuaniaLitas',
            code: 'LTL',
            symbol: 'Lt'
        }, {
            Id: '63',
            name: 'MacedoniaDenar',
            code: 'MKD',
            symbol: 'ден'
        }, {
            Id: '64',
            name: 'MalaysiaRinggit',
            code: 'MYR',
            symbol: 'RM'
        }, {
            Id: '65',
            name: 'MauritiusRupee',
            code: 'MUR',
            symbol: '₨'
        }, {
            Id: '66',
            name: 'MexicoPeso',
            code: 'MXN',
            symbol: '$'
        }, {
            Id: '67',
            name: 'MongoliaTughrik',
            code: 'MNT',
            symbol: '₮'
        }, {
            Id: '68',
            name: 'MozambiqueMetical',
            code: 'MZN',
            symbol: 'MT'
        }, {
            Id: '69',
            name: 'NamibiaDollar',
            code: 'NAD',
            symbol: '$'
        }, {
            Id: '70',
            name: 'NepalRupee',
            code: 'NPR',
            symbol: '₨'
        }, {
            Id: '71',
            name: 'NetherlandsAntillesGuilder',
            code: 'ANG',
            symbol: 'ƒ'
        }, {
            Id: '72',
            name: 'NewZealandDollar',
            code: 'NZD',
            symbol: '$'
        }, {
            Id: '73',
            name: 'NicaraguaCordoba',
            code: 'NIO',
            symbol: 'C$'
        }, {
            Id: '74',
            name: 'NigeriaNaira',
            code: 'NGN',
            symbol: '₦'
        }, {
            Id: '75',
            name: 'Korea(North)Won',
            code: 'KPW',
            symbol: '₩'
        }, {
            Id: '76',
            name: 'NorwayKrone',
            code: 'NOK',
            symbol: 'kr'
        }, {
            Id: '77',
            name: 'OmanRial',
            code: 'OMR',
            symbol: '﷼'
        }, {
            Id: '78',
            name: 'PakistanRupee',
            code: 'PKR',
            symbol: '₨'
        }, {
            Id: '79',
            name: 'PanamaBalboa',
            code: 'PAB',
            symbol: 'B/.'
        }, {
            Id: '80',
            name: 'ParaguayGuarani',
            code: 'PYG',
            symbol: 'Gs'
        }, {
            Id: '81',
            name: 'PeruNuevoSol',
            code: 'PEN',
            symbol: 'S/.'
        }, {
            Id: '82',
            name: 'PhilippinesPeso',
            code: 'PHP',
            symbol: '₱'
        }, {
            Id: '83',
            name: 'PolandZloty',
            code: 'PLN',
            symbol: 'zł'
        }, {
            Id: '84',
            name: 'QatarRiyal',
            code: 'QAR',
            symbol: '﷼'
        }, {
            Id: '85',
            name: 'RomaniaNewLeu',
            code: 'RON',
            symbol: 'lei'
        }, {
            Id: '86',
            name: 'RussiaRuble',
            code: 'RUB',
            symbol: 'руб'
        }, {
            Id: '87',
            name: 'SaintHelenaPound',
            code: 'SHP',
            symbol: '£'
        }, {
            Id: '88',
            name: 'SaudiArabiaRiyal',
            code: 'SAR',
            symbol: '﷼'
        }, {
            Id: '89',
            name: 'SerbiaDinar',
            code: 'RSD',
            symbol: 'Дин.'
        }, {
            Id: '90',
            name: 'SeychellesRupee',
            code: 'SCR',
            symbol: '₨'
        }, {
            Id: '91',
            name: 'SingaporeDollar',
            code: 'SGD',
            symbol: '$'
        }, {
            Id: '92',
            name: 'SolomonIslandsDollar',
            code: 'SBD',
            symbol: '$'
        }, {
            Id: '93',
            name: 'SomaliaShilling',
            code: 'SOS',
            symbol: 'S'
        }, {
            Id: '94',
            name: 'SouthAfricaRand',
            code: 'ZAR',
            symbol: 'R'
        }, {
            Id: '95',
            name: 'Korea(South)Won',
            code: 'KRW',
            symbol: '₩'
        }, {
            Id: '96',
            name: 'SriLankaRupee',
            code: 'LKR',
            symbol: '₨'
        }, {
            Id: '97',
            name: 'SwedenKrona',
            code: 'SEK',
            symbol: 'kr'
        }, {
            Id: '98',
            name: 'SwitzerlandFranc',
            code: 'CHF',
            symbol: 'CHF'
        }, {
            Id: '99',
            name: 'SurinameDollar',
            code: 'SRD',
            symbol: '$'
        }, {
            Id: '100',
            name: 'SyriaPound',
            code: 'SYP',
            symbol: '£'
        }, {
            Id: '101',
            name: 'TaiwanNewDollar',
            code: 'TWD',
            symbol: 'NT$'
        }, {
            Id: '102',
            name: 'ThailandBaht',
            code: 'THB',
            symbol: '฿'
        }, {
            Id: '103',
            name: 'TrinidadandTobagoDollar',
            code: 'TTD',
            symbol: 'TT$'
        }, {
            Id: '104',
            name: 'TurkeyLira',
            code: 'TRY',
            symbol: '-'
        }, {
            Id: '105',
            name: 'TurkeyLira',
            code: 'TRL',
            symbol: '₤'
        }, {
            Id: '106',
            name: 'TuvaluDollar',
            code: 'TVD',
            symbol: '$'
        }, {
            Id: '107',
            name: 'UkraineHryvnia',
            code: 'UAH',
            symbol: '₴'
        }, {
            Id: '108',
            name: 'UnitedKingdomPound',
            code: 'GBP',
            symbol: '£'
        }, {
            Id: '109',
            name: 'UnitedStatesDollar',
            code: 'USD',
            symbol: '$'
        }, {
            Id: '110',
            name: 'UruguayPeso',
            code: 'UYU',
            symbol: '$U'
        }, {
            Id: '111',
            name: 'UzbekistanSom',
            code: 'UZS',
            symbol: 'лв'
        }, {
            Id: '112',
            name: 'VenezuelaBolivar',
            code: 'VEF',
            symbol: 'Bs'
        }, {
            Id: '113',
            name: 'VietNamDong',
            code: 'VND',
            symbol: '₫'
        }, {
            Id: '114',
            name: 'YemenRial',
            code: 'YER',
            symbol: '﷼'
        }, {
            Id: '115',
            name: 'ZimbabweDollar',
            code: 'ZWD',
            symbol: 'Z$'
        }, {
            Id: '0',
            name: 'Other',
            code: '',
            symbol: ''
        }];
        $scope.openCurrencyTypeFFSettings = function () {
            $("#currencyTypeFFSettingsModal").modal('show');
            $scope.Isvalidate = true;
            $scope.currencyTypeName = '';
            $scope.currencyTypeShorttext = '';
            $scope.currencyTypeSymbol = '';
            $scope.showSave = true;
            $scope.showUpdate = false;
            $timeout(function () {
                $('#currencyTypeName').focus();
            }, 1000);
        }
        $scope.CurrencyData = [];
        $scope.currencyTypeResponse = [];
        $scope.CurrencyFormatsList = [];
        var ListDeleteCurrency = {};
        ListDeleteCurrency.Id = '@Id';
        var CurrenclyListServ = AdminService.GetCurrencyListFFsettings(ListDeleteCurrency).then(function (CurrencyListResult) {
            $scope.CurrencyData = CurrencyListResult.Response;
            $scope.currencyTypeResponse = $scope.CurrencyData;
            for (var i = 0; i < CurrencyListResult.Response.length; i++) {
                $scope.CurrencyFormatsList.push({
                    "ID": CurrencyListResult.Response[i].Id,
                    "Name": CurrencyListResult.Response[i].Name,
                    "ShortName": CurrencyListResult.Response[i].ShortName,
                    "Symbol": CurrencyListResult.Response[i].Symbol
                });
            }
            AdminService.GetAdditionalSettings().then(function (GetAdditionalresult) {
                if (GetAdditionalresult.StatusCode == 200) {
                    $scope.CurrencyFormat = parseInt(GetAdditionalresult.Response[1].SettingValue);
                } else {
                    $scope.CurrencyFormat = parseInt(1);
                }
            });
        });
        $scope.SetDivison = function (value) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2044.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var DivisonFFSettingsSave = {};
                        DivisonFFSettingsSave.DivisionId = value.DivisionId;
                        AdminService.SetDivisonsFFSettings(DivisonFFSettingsSave).then(function (setDivisons) {
                            if (setDivisons.Response == true) {
                                NotifySuccess("" + value.DivisionName + $translate.instant('LanguageContents.Res_4951.Caption'));
                                $scope.DivisionSelected = value.DivisionName;
                                $scope.FinancialCurrentDivisionID.ID = value.DivisionId;
                            };
                        });
                    }, 100);
                }
            });
        }
        $scope.DivisonIdsList = [];
        AdminService.getDivisonIds().then(function (DivisonIdsData) {
            $scope.DivisonIdsList = DivisonIdsData.Response;
        });
        var DivisonSettings = '';
        AdminService.GetDivisonName().then(function (getdivisonsettings) {
            if (getdivisonsettings.Response == false) {
                $scope.DivisionSelected = $scope.DivisonIdsList[0].DivisionName;
            } else {
                $scope.DivisionSelected = getdivisonsettings.Response;
            }
        });
        $scope.listdata = CurrenclyListServ;
        $scope.DeleteCurrencyTypeList = (function (row) {
            if (row.Id != $scope.DefaultSettings.CurrencyFormat.Id) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2043.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            var ID = row.Id;
                            AdminService.DeleteCurrencyListFFSettings(ID).then(function (deletecurrencybyid) {
                                if (deletecurrencybyid.StatusCode == 405) {
                                    NotifyError($translate.instant('LanguageContents.Res_4293.Caption'));
                                } else {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                    var CurrenclyListServ = AdminService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                                        $scope.CurrencyData = CurrencyListResult.Response;
                                        $scope.currencyTypeResponse = $scope.CurrencyData;
                                        $scope.CurrencyFormatsList = [];
                                        for (var i = 0; i < $scope.currencyTypeResponse.length; i++) {
                                            $scope.CurrencyFormatsList.push({
                                                "ID": CurrencyListResult.Response[i].Id,
                                                "Name": CurrencyListResult.Response[i].Name,
                                                "ShortName": CurrencyListResult.Response[i].ShortName,
                                                "Symbol": CurrencyListResult.Response[i].Symbol
                                            });
                                        }
                                    });
                                }
                            });
                        }, 100);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5617.Caption'));
            }
        });
        $scope.EditCurrencyTypeList = function (row) {
            $scope.Isvalidate = false;
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.currencyTypeName = row.Name;
            $scope.currencyTypeShorttext = row.ShortName;
            $scope.currencyTypeSymbol = row.Symbol;
            $scope.Id = row.Id;
            $("#currencyTypeFFSettingsModal").modal('show');
            $timeout(function () {
                $('#currencyTypeName').focus().select();
            }, 1000);
        }
        $scope.SaveCurrencyTypeFFSettings = function () {
            var addCurrencyTypeListFFSettings = {};
            addCurrencyTypeListFFSettings.Id = 0;
            addCurrencyTypeListFFSettings.Name = $scope.currencyTypeName;
            addCurrencyTypeListFFSettings.Shorttext = $scope.currencyTypeShorttext;
            addCurrencyTypeListFFSettings.Symbol = $scope.currencyTypeSymbol;
            AdminService.InsertUpdateCurrencyListFFSettings(addCurrencyTypeListFFSettings).then(function (SaveCurrencyTypeFFSettings) {
                if (SaveCurrencyTypeFFSettings.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4335.Caption'));
                } else {
                    AdminService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                        $scope.CurrencyData = CurrencyListResult.Response;
                        $scope.currencyTypeResponse = $scope.CurrencyData;
                        $scope.CurrencyFormatsList = [];
                        for (var i = 0; i < $scope.currencyTypeResponse.length; i++) {
                            $scope.CurrencyFormatsList.push({
                                "ID": CurrencyListResult.Response[i].Id,
                                "Name": CurrencyListResult.Response[i].Name,
                                "ShortName": CurrencyListResult.Response[i].ShortName,
                                "Symbol": CurrencyListResult.Response[i].Symbol
                            });
                        }
                    });
                    $('#currencyTypeFFSettingsModal').modal('hide');
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                }
            });
        }
        AdminService.GetFinancialforecastData().then(function (ResponseGetFinancialforecastData) {
            var response = ResponseGetFinancialforecastData.Response;
            if (ResponseGetFinancialforecastData.Response[0] == true) {
                $('#CheckFF').next('i').addClass('checked');
            }
            if (ResponseGetFinancialforecastData.Response[0] == false) {
                $('#CheckFF').next('i').removeClass('checked');
            }
            if (ResponseGetFinancialforecastData.Response[1] == true) {
                $scope.CheckMoneytransfer = true;
            }
            if (ResponseGetFinancialforecastData.Response[1] == false) {
                $scope.CheckMoneytransfer = false;
            }
            if (ResponseGetFinancialforecastData.Response[2] == true) {
                $scope.CheckExternalsource = true;
            }
            if (ResponseGetFinancialforecastData.Response[2] == false) {
                $scope.CheckExternalsource = false;
            }
            if (ResponseGetFinancialforecastData.Response[3] == true) {
                $scope.CheckEnablefundingrequest = true;
            }
            if (ResponseGetFinancialforecastData.Response[3] == false) {
                $scope.CheckEnablefundingrequest = false;
            }
            if (ResponseGetFinancialforecastData.Response[4] == true) {
                $scope.CheckEnablerequestplannedfunds = true;
            }
            if (ResponseGetFinancialforecastData.Response[4] == false) {
                $scope.CheckEnablerequestplannedfunds = false;
            }
            if (ResponseGetFinancialforecastData.Response[5] == true) {
                $scope.CheckEnablerelasefunds = true;
            }
            if (ResponseGetFinancialforecastData.Response[5] == false) {
                $scope.CheckEnablerelasefunds = false;
            }
            if (ResponseGetFinancialforecastData.Response[6] == true) {
                $scope.CheckEnableadjustapprovedallocations = true;
            }
            if (ResponseGetFinancialforecastData.Response[6] == false) {
                $scope.CheckEnableadjustapprovedallocations = false;
            }
        });

        function getFinancailData(val, event) {
            var objGetFinancialforecastDataByStatus = {};
            if (val == "Financialforecast") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            } else if (val == "Moneytransfer") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            } else if (val == "Linkedresource") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            }
            else if (val == "Enablefundingrequest") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            }
            else if (val == "Enablerequestplannedfunds") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            }
            else if (val == "Enablerelasefunds") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            }
            else if (val == "Enableadjustapprovedallocations") {
                objGetFinancialforecastDataByStatus.Status = event.target.checked;
            }
            objGetFinancialforecastDataByStatus.FinancialData = val;
            AdminService.UpdateFFData(objGetFinancialforecastDataByStatus).then(function (res) {
                if (res.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption'));
                }
            });
        };
        $scope.getFFDataByStatus = function (val, event) {
            getFinancailData(val, event);
        };
        $scope.UpdateFormat = function (FormatType) {
            var Formatparms = {};
            Formatparms.Settingname = FormatType
            if (FormatType == 'DateFormat') Formatparms.settingValue = $scope.DateFormat;
            else Formatparms.settingValue = $('#ddlcurrencyformat option:selected').val();
            Formatparms.ID = 2;
            AdminService.InsertUpdateAdditionalSettings(Formatparms).then(function (Result) {
                if (Result.StatusCode == 200) {
                    AdminService.GetAdditionalSettings().then(function (GetAdditionalresult) {
                        if (GetAdditionalresult.StatusCode == 200) {
                            $scope.DefaultSettings.DateFormat = GetAdditionalresult.Response[0].SettingValue;
                            for (var i = 0; i < GetAdditionalresult.Response[1].CurrencyFormatvalue.length; i++) {
                                $scope.DefaultSettings.CurrencyFormat.Id = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Id;
                                $scope.DefaultSettings.CurrencyFormat.Name = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].ShortName;
                                $scope.DefaultSettings.CurrencyFormat.ShortName = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Name;
                                $scope.DefaultSettings.CurrencyFormat.Symbol = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Symbol;
                            }
                        }
                    });
                    NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption') + FormatType);
                }
            });
        }
        $scope.UpdateCurrencyTypeFFSettings = function () {
            var addCurrencyTypeListFFSettings = {};
            addCurrencyTypeListFFSettings.Id = $scope.Id;
            addCurrencyTypeListFFSettings.Name = $scope.currencyTypeName;
            addCurrencyTypeListFFSettings.Shorttext = $scope.currencyTypeShorttext;
            addCurrencyTypeListFFSettings.Symbol = $scope.currencyTypeSymbol;
            $scope.DivisonIdsList = [];
            AdminService.getDivisonIds().then(function (DivisonIdsData) {
                if ($scope.DivisonIdsList.length == false) {
                    $scope.DivisonIdsList.DivisionName = "Yearly"
                } else {
                    $scope.DivisonIdsList = DivisonIdsData.Response;
                }
                AdminService.InsertUpdateCurrencyListFFSettings(addCurrencyTypeListFFSettings).then(function (SaveCurrencyTypeFFSettings) {
                    if (SaveCurrencyTypeFFSettings.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4335.Caption'));
                    } else {
                        AdminService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                            $scope.CurrencyData = CurrencyListResult.Response;
                            $scope.currencyTypeResponse = $scope.CurrencyData;
                            $scope.CurrencyFormatsList = [];
                            for (var i = 0; i < $scope.currencyTypeResponse.length; i++) {
                                $scope.CurrencyFormatsList.push({
                                    "ID": CurrencyListResult.Response[i].Id,
                                    "Name": CurrencyListResult.Response[i].Name,
                                    "ShortName": CurrencyListResult.Response[i].ShortName,
                                    "Symbol": CurrencyListResult.Response[i].Symbol
                                });
                            }
                        });
                        $('#currencyTypeFFSettingsModal').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4883.Caption'));
                    }
                });
            });
        }
        $scope.SetDefaultCurrencyValues = function () {
            if ($scope.Isvalidate == true) {
                var seletedCurry = $.grep($scope.DefaultCurrency, function (e) {
                    return e.Id == parseInt(JSON.parse($scope.DefaultCurrencySelected).Id)
                });
                if (seletedCurry[0].name == "Other") $scope.currencyTypeName = "";
                else $scope.currencyTypeName = seletedCurry[0].name;
                $scope.currencyTypeShorttext = seletedCurry[0].code;
                $scope.currencyTypeSymbol = seletedCurry[0].symbol;
            } else {
                $scope.EditCurrencyTypeList();
            }
        }
    }
    app.controller("mui.admin.financialforecastsettingsctrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminfinancialforecastsettingsctrl]);
})(angular, app);