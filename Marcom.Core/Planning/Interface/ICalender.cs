using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjective interface for table 'PM_Calender'.
    /// </summary>
    public interface ICalender : IBaseEntity
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }
        string Description
        {
            get;
            set;

        }

        bool? CalenderStatus
        {
            get;
            set;

        }

        DateTime? PublishedOn
        {
            get;
            set;

        }

        bool IsExternal
        {
            get;
            set;

        }

        int Visibilityperiod
        {
            get;
            set;

        }

        int Visibilitytype
        {
            get;
            set;

        }

        int OwnerID
        {
            get;
            set;

        }

        DateTime CreatedOn
        {
            get;
            set;

        }
        string PublishedData
        {
            get;
            set;
        }

        #endregion
    }
}
