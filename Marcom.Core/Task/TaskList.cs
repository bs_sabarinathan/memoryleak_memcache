﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class TaskList : ITaskList
    {
        #region Member Variables

        protected int _id;
        protected string _Caption;
        protected string _description;
        protected int _Sortorder;
        #endregion

        #region Constructors
        public TaskList() { }

        public TaskList(string pCaption, string pDescription, int pSortorder)
        {
            this._Caption = pCaption;
            this._description = pDescription;
            this._Sortorder = pSortorder;
        }

        #endregion

        #region Public Properties

        public  int Id
        {
            get { return _id; }
            set {_id = value; }

        }


        public  string Description
        {
            get { return _description; }
            set
            {
               
                _description = value;
            }

        }

        public  string Caption
        {
            get { return _Caption; }
            set
            {
                _Caption = value;
            }

        }


        public  int SortOder
        {
            get { return _Sortorder; }
            set { _Sortorder = value; }

        }

        public int TotalAttachment { get; set; }

        public IList<IAdminTask> TaskCollections { get; set; }

        public int TotalTasks { get; set; }

       

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskList castObj = null;
            try
            {
                castObj = (TaskList)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


        
    }
}


