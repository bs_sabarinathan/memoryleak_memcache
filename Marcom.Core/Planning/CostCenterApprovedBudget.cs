﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// CostCenterApprovedBudget object for table 'PM_CostCenterApprovedBudget'.
    /// </summary>

    internal class CostCenterApprovedBudget : ICostCenterApprovedBudget
    {
        #region Member Variables

        protected int _id;
        protected int _costCentID;
        protected int _userID;
        protected DateTime _approveTime;


        #endregion

        #region Constructors
        public CostCenterApprovedBudget() { }

        public CostCenterApprovedBudget(int pCostCentID, int pUserID, DateTime pApproveTime)
        {
            this._costCentID = pCostCentID;
            this._userID = pUserID;
            this._approveTime = pApproveTime;

        }

        public CostCenterApprovedBudget(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int CostCentID
        {
            get { return _costCentID; }
            set {  _costCentID = value; }

        }



        public virtual int UserID
        {
            get { return _userID; }
            set {  _userID = value; }

        }




        public virtual DateTime ApproveTime
        {
            get { return _approveTime; }
            set { _approveTime = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            CostCenterApprovedBudget castObj = null;
            try
            {
                castObj = (CostCenterApprovedBudget)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
