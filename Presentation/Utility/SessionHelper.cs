﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Utility
{
    public class SessionHelper
    {
        protected static Guid _id;

        public static Guid UserSession
        {
            get { return _id; }
            set { _id = value; }

        }


        public Guid GetSessionGuid(string sessioncookieval)
        {
            Guid sessionapi = new Guid(sessioncookieval);
            return sessionapi;
        }
    }
}