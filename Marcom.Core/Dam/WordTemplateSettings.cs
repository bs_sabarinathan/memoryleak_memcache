﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    internal class WordTemplateSettings : IWordTemplateSettings
    {
       #region "Member variables"

        protected int _id;
        protected string _name;
        protected int _categoryid;
        protected string _description;
        protected string _worddocpath;
        protected string _previewimage;

        #endregion


         #region "Constructor"

        public WordTemplateSettings() { }


        public WordTemplateSettings(int pid, string pname, int pcategoryid, string pdescription, string pworddocpath, string ppreviewimage) 
        {
            this._id = pid;
            this._name = pname;
            this._categoryid = pcategoryid;
            this._description = pdescription;
            this._worddocpath = pworddocpath;
            this._previewimage = ppreviewimage;
        }

        #endregion

        #region "Public propertiers"

        public  int ID
        {
            get { return _id; }
            set { _id = value; }

        }



        public  string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }



        public  int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }


        public  string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public  string Worddocpath
        {
            get { return _worddocpath; }
            set { _worddocpath = value; }

        }

        public  string PreviewImage
        {
            get { return _previewimage; }
            set { _previewimage = value; }

        }
        #endregion
    }
}
