﻿(function (ng, app) {
    function muiplanningtooldefaultdetailCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $location, $templateCache, $cookies, $translate, PlandetailService, $modal) {
        setTimeout(function () {   $scope.page_resize();   }, 100);
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName).replace(/\\/g, "\/");
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {

            $scope.reportPreviewImg = cloudpath + "/Files/ReportFiles/Images/Preview/";
        }
        else {
            $scope.reportPreviewImg = "/Files/ReportFiles/Images/Preview/";
        }
        var Timeoutvar_forDeatil = {};
        var model;
        var timer;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.Ganttenddate1 = false;
            model = model1;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.Ganttenddate1 = true;
        };
        $scope.WizardPosition = 1;
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        var CollectionIds = "";
        if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") {
            $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID));
        }
        CollectionIds = $.cookie('ListofEntityID').split(",");
        $scope.CurrentTrackingUrl = $stateParams.TrackID;
        $window.ListofEntityID = CollectionIds;
        $scope.EntityLevelfrFin.levelfrFinancial = 0;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.SelectedCostCentreCurrency = {
            TypeId: $scope.DefaultSettings.CurrencyFormat.Id,
            Name: $scope.DefaultSettings.CurrencyFormat.ShortName,
            ShortName: $scope.DefaultSettings.CurrencyFormat.Name,
            Rate: 1
        };
        ReportCurrency.CurrencyRate = 1;
        ReportCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
        $scope.Level = SearchEntityLevel;
        $scope.IsSingleID = false;
        $scope.IsSortFromGanttView = false;
        $scope.SortOrderColumnId = 0;
        $scope.IsDescFromGanttView = false;
        $scope.ExpandingEntityID = {
            EntityID: '0'
        };
        $scope.FilterType = 1;
        $("#plandetailfilter").on('loadfiltersettings', function (event, TypeID) {
            $("#plandetailfiltersettings").trigger('loaddetailfiltersettings', 6);
        });
        $("#plandetailfilter").on('applyplandetailfilter', function (event, filterid, filtername, IsDetailFilter) {
            $("#plandetailfiltersettings").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        });
        $("#plandetailfilter").on('EditFilterSettingsByFilterID', function (event, filterid, typeid, IsDetailFilter) {
            $("#plandetailfiltersettings").trigger('EditFilterSettingsByFilterID', [filterid, typeid, IsDetailFilter]);
        });
        $("#plandetailfilter").on('ClearScope', function (event, filterid) {
            $("#plandetailfiltersettings").trigger('ClearScope', [filterid]);
        });
        $("#plandetailfilter").on('ClearAndReApply', function (event, filterid) {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.SelectedFilterType = 1;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        var FilterID = 0;
        var AplyFilterObj = [];
        $scope.ListViewDetails = [{
            data: "",
            PageIndex: 0,
            UniqueID: ""
        }];
        $scope.ListTemplate = "";
        $scope.GanttViewDataPageTemplate = '';
        $scope.GanttViewBlockPageTemplate = '';
        $scope.GanttViewHeaders = {};
        var GlobalChildNode = new Array();
        GlobalChildNode = $window.ListofEntityID;
        $scope.EntityHighlight = false;
        $scope.LocalIsWorkspaces = false;
        $scope.ShowHideExpandAll = true;
        PlandetailService.ListSetting("GanttView", 6).then(function (getGanttlistsetting) {
            if (getGanttlistsetting.Response.Attributes != null) {
                $scope.GanttViewHeaders = getGanttlistsetting.Response.Attributes;
            }
        });
        if (GlobalIsWorkspaces) {
            $scope.LocalIsWorkspaces = true;
            $scope.ShowHideExpandAll = false;
            GlobalIsWorkspaces = false;
            $scope.IsSingleID = false;
        } else {
            $scope.LocalIsWorkspaces = false;
            $scope.IsSingleID = false;
        }
        if ($stateParams.ID != undefined && $stateParams.ID != 0) {
            GlobalChildNode = new Array();
            GlobalChildNode.push($stateParams.ID)
            if ($scope.LocalIsWorkspaces == true) {
                $scope.ExpandingEntityID.EntityID = $stateParams.ID;
            } else $scope.ExpandingEntityID.EntityID = '0';
            $scope.Level = "100";
            $scope.EntityHighlight = true;
            $('#TreeCtrlCss').remove();
        } else {
            $('#TreeCtrlCss').remove();
            GetTreeCount();
            $scope.FilterType = 1;
        }
        $scope.loadactivityfromsearchByID = function (id, level) {
            $scope.Level = level;
            FilterID = 0;
            AplyFilterObj = [];
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: ""
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            GlobalChildNode = new Array();
            GlobalChildNode.push(id)
            $scope.ExpandingEntityID.EntityID = '0';
            $('#TreeCtrlCss').remove();
            $scope.EntityHighlight = true;
            GetTreeCount();
            $scope.FilterType = 1;
        }
        $("#EntitiesTree").on("loadactivityfromfilter", function (event, filterid, applyFilterobj, filtertype) {
            if (filtertype == undefined) $scope.FilterType = 1;
            $scope.FilterType = filtertype;
            $scope.Level = "0";
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: ""
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            if (filterid == undefined) {
                FilterID = 0;
            } else {
                FilterID = filterid;
            }
            AplyFilterObj = applyFilterobj;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) $scope.ExpandingEntityID.EntityID = $stateParams.ID;
            else GlobalChildNode = $window.ListofEntityID;
            $('#TreeCtrlCss').remove();
            if ($scope.PlanExpandtext == $translate.instant('LanguageContents.Res_4940.Caption')) {
                $scope.Level = "100";
            } else $scope.Level = "0";
            if (AplyFilterObj.length > 0 || FilterID > 0) {
                $scope.Level = "100";
            }
            GetTreeCount();
        });
        $scope.$on('LoadSectionDetail', function (event, ID) {
            if ($("#EntitiesTree li a[data-entityid=" + ID + "]").length > 0) {
                $("#EntitiesTree li.active").removeClass('active');
                $("#EntitiesTree li a[data-entityid=" + ID + "]").parents("li").addClass('active');
            } else {
                $scope.ExpandingEntityID.EntityID = ID;
            }
        });

        function GetTreeCount(gattsortval, IsDesc, levelcnt) {
            $('#EntitiesTree').scrollTop(0);
            $scope.TreeClassList = [];
            $scope.EntitiesfrCalfromPlan = [];
            var TreeItem = "";
            $scope.Tempattrgroupfilter = [];
            if ($scope.FilterType == undefined) $scope.FilterType = 1;
            if (GlobalChildNode.length > 0) {
                PlandetailService.getParentID(GlobalChildNode[0]).then(function (ParentID) {
                    GlobalChildNode[0] = ParentID.Response;
                    var Node = {};
                    Node.StartRowNo = 0;
                    Node.MaxNoofRow = 30, Node.FilterID = FilterID;
                    if (gattsortval == undefined) {
                        Node.SortOrderColumn = "null";
                        $scope.SortOrderColumnId = 0;
                    } else Node.SortOrderColumn = gattsortval;
                    if (IsDesc == undefined) {
                        Node.IsDesc = false;
                        $scope.IsSortFromGanttView = false;
                        $scope.IsDescFromGanttView = false;
                    } else {
                        Node.IsDesc = IsDesc;
                        $scope.IsDescFromGanttView = IsDesc;
                    }
                    Node.IncludeChildren = (FilterID > 0 ? false : true);
                    Node.EntityID = '0';
                    if (levelcnt == undefined) Node.Level = $scope.Level;
                    else Node.Level = levelcnt;
                    Node.ExpandingEntityID = $scope.ExpandingEntityID.EntityID;
                    Node.IsSingleID = $scope.IsSingleID;
                    Node.FilterType = $scope.FilterType;
                    Node.IDArr = GlobalChildNode;
                    Node.FilterAttributes = AplyFilterObj;
                    PlandetailService.ActivityDetail(Node).then(function (GetActivityList) {
                        if (GetActivityList.Response != null && GetActivityList.Response.Data != null) {
                            var optID = "";
                            var totcnt = 0;
                            fnPageTemplate(parseInt(GetActivityList.Response.DataCount));
                            var LoadedPage = 0;
                            if ($scope.FilterType == 5) {
                                for (var i = 0; i < GetActivityList.Response.Data.length; i++) {
                                    if (optID != GetActivityList.Response.Data[i].OptionCaption) {
                                        totcnt = $.grep(GetActivityList.Response.Data, function (e) {
                                            return e.OptionID == GetActivityList.Response.Data[i].OptionID
                                        });
                                        $scope.Tempattrgroupfilter.push({
                                            1: "",
                                            69: "",
                                            71: "",
                                            ColorCode: "000000",
                                            EntityID: "",
                                            EntityStateID: "",
                                            Id: 0,
                                            IsLock: false,
                                            Level: -1,
                                            MileStone: "<root/>",
                                            Name: GetActivityList.Response.Data[i]["OptionCaption"],
                                            ParentID: 0,
                                            Period: "",
                                            Permission: 0,
                                            ShortDescription: "G",
                                            TempPeriod: "",
                                            TotalChildrenCount: totcnt.length + 1,
                                            TypeID: 0,
                                            TypeName: "@Option",
                                            UniqueKey: "-0" + i,
                                            Parenentitytname: "-"
                                        });
                                    }
                                    $scope.Tempattrgroupfilter.push({
                                        1: GetActivityList.Response.Data[i]["1"],
                                        69: GetActivityList.Response.Data[i]["69"],
                                        71: GetActivityList.Response.Data[i]["71"],
                                        ColorCode: GetActivityList.Response.Data[i]["ColorCode"],
                                        EntityID: GetActivityList.Response.Data[i]["EntityID"],
                                        EntityStateID: GetActivityList.Response.Data[i]["EntityStateID"],
                                        Id: GetActivityList.Response.Data[i]["Id"],
                                        IsLock: GetActivityList.Response.Data[i]["IsLock"],
                                        Level: GetActivityList.Response.Data[i]["Level"],
                                        MileStone: GetActivityList.Response.Data[i]["MileStone"],
                                        Name: GetActivityList.Response.Data[i]["Name"],
                                        ParentID: GetActivityList.Response.Data[i]["ParentID"],
                                        Period: GetActivityList.Response.Data[i]["Period"],
                                        Permission: GetActivityList.Response.Data[i]["Permission"],
                                        ShortDescription: GetActivityList.Response.Data[i]["ShortDescription"],
                                        TempPeriod: GetActivityList.Response.Data[i]["TempPeriod"],
                                        TotalChildrenCount: GetActivityList.Response.Data[i]["TotalChildrenCount"],
                                        TypeID: GetActivityList.Response.Data[i]["TypeID"],
                                        TypeName: GetActivityList.Response.Data[i]["TypeName"],
                                        UniqueKey: GetActivityList.Response.Data[i]["UniqueKey"],
                                        Parenentitytname: GetActivityList.Response.Data[i]["Parenentitytname"]
                                    });
                                    optID = GetActivityList.Response.Data[i].OptionCaption;
                                }
                                GetActivityList.Response.Data = $scope.Tempattrgroupfilter
                                $scope.ParenttreeList = $scope.Tempattrgroupfilter;
                                if ($scope.ParenttreeList != null) {
                                    if ($scope.ListViewDetails[0].data == "") {
                                        $scope.ListViewDetails[0].data = GetActivityList;
                                        $scope.ListViewDetails[0].PageIndex = 0
                                        $scope.ListViewDetails[0].UniqueID = ""
                                    } else {
                                        $scope.ListViewDetails.push({
                                            data: GetActivityList,
                                            PageIndex: 0,
                                            UniqueID: "",
                                        });
                                    }
                                    for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                        $scope.EntitiesfrCalfromPlan.push($scope.ParenttreeList[i].Id);
                                        var UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                                        var ClassName = GenateClass(UniqueId);
                                        ClassName += " mo" + UniqueId;
                                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                        if ($scope.ParenttreeList[i]["TypeName"] != "@Option") TreeItem += "<i class='icon-'></i>";
                                        TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                                        if ($scope.ParenttreeList[i]["TotalChildrenCount"] <= 0 || $scope.IsSingleID == true) {
                                            TreeItem += "<i class='icon-'></i>";
                                            $scope.IsSingleID = false;
                                        } else {
                                            if (UniqueId.split("-").length <= parseInt($scope.Level) || $scope.ParenttreeList[i]["ParentID"] == 0) {
                                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                            } else {
                                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                            }
                                        }
                                        TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                        TreeItem += "<span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                        if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                        } else if ($scope.ParenttreeList[i].Permission == 3) {
                                            var EntityTypesWithPermission = "";
                                            if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                                                EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                                    return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                                })
                                            }
                                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                        } else if ($scope.ParenttreeList[i].Permission == 0) { }
                                        TreeItem += "</a> </li>";
                                    }
                                    $scope.$broadcast('onTreePageCreation', [GetActivityList, 0, 0]);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append(TreeItem);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done').removeAttr('style');
                                    if (FilterID > 0 || AplyFilterObj.length > 0) {
                                        if ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length == 0) {
                                            $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').parent().css('display', 'none');
                                            $('#EntitiesTree li[data-page=' + LoadedPage + ']').css('display', 'none');
                                            clearTimeout(timer);
                                            timer = setTimeout(function () {
                                                StartPageLoad(false)
                                            }, 1);
                                        }
                                        if (($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length > 0) && ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length < 10)) {
                                            clearTimeout(timer);
                                            timer = setTimeout(function () {
                                                StartPageLoad(false)
                                            }, 1);
                                        }
                                    }
                                    if ($scope.EntityHighlight == true) {
                                        $("#EntitiesTree li.active").removeClass('active')
                                        $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                                        $scope.EntityHighlight = false;
                                    }
                                    if (SearchEntityID != 0) {
                                        $scope.EntityTypeID = EntityTypeID;
                                        $location.path('/mui/planningtool/default/detail/section/' + SearchEntityID + '/overview');
                                        SearchEntityID = 0;
                                        EntityTypeID = 0;
                                        SearchEntityLevel = "0";
                                    }
                                }
                            } else {
                                $scope.ParenttreeList = GetActivityList.Response.Data;
                                if ($scope.ParenttreeList != null) {
                                    if ($scope.ListViewDetails[0].data == "") {
                                        $scope.ListViewDetails[0].data = GetActivityList;
                                        $scope.ListViewDetails[0].PageIndex = 0
                                        $scope.ListViewDetails[0].UniqueID = ""
                                        $scope.LoadGantHeader();
                                    } else {
                                        $scope.ListViewDetails.push({
                                            data: GetActivityList,
                                            PageIndex: 0,
                                            UniqueID: ""
                                        });
                                    }
                                    for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                        $scope.EntitiesfrCalfromPlan.push($scope.ParenttreeList[i].Id);
                                        var UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                                        var ClassName = GenateClass(UniqueId);
                                        ClassName += " mo" + UniqueId;
                                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                        TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                                        if ($scope.ParenttreeList[i]["TotalChildrenCount"] <= 0 || $scope.IsSingleID == true) {
                                            TreeItem += "<i class='icon-'></i>";
                                            $scope.IsSingleID = false;
                                        } else {
                                            if (UniqueId.split("-").length <= parseInt($scope.Level) || $scope.ParenttreeList[i]["ParentID"] == 0) {
                                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                            } else {
                                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                            }
                                        }
                                        TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                        TreeItem += "<span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                        if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                        } else if ($scope.ParenttreeList[i].Permission == 3) {
                                            var EntityTypesWithPermission = "";
                                            if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                                                EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                                    return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                                })
                                            }
                                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                        } else if ($scope.ParenttreeList[i].Permission == 0) { }
                                        TreeItem += "</a> </li>";
                                    }
                                    $scope.$broadcast('onTreePageCreation', [GetActivityList, 0, 0]);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append(TreeItem);
                                    $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done').removeAttr('style');
                                    if (FilterID > 0 || AplyFilterObj.length > 0) {
                                        if ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length == 0) {
                                            $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').parent().css('display', 'none');
                                            $('#EntitiesTree li[data-page=' + LoadedPage + ']').css('display', 'none');
                                            clearTimeout(timer);
                                            timer = setTimeout(function () {
                                                StartPageLoad(false)
                                            }, 1);
                                        }
                                        if (($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length > 0) && ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length < 10)) {
                                            clearTimeout(timer);
                                            timer = setTimeout(function () {
                                                StartPageLoad(false)
                                            }, 1);
                                        }
                                    }
                                    if ($scope.EntityHighlight == true) {
                                        $("#EntitiesTree li.active").removeClass('active')
                                        $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                                        $scope.EntityHighlight = false;
                                    }
                                    if (SearchEntityID != 0) {
                                        $scope.EntityTypeID = EntityTypeID;
                                        $location.path('/mui/planningtool/default/detail/section/' + SearchEntityID + '/overview');
                                        SearchEntityID = 0;
                                        EntityTypeID = 0;
                                        SearchEntityLevel = "0";
                                    }
                                }
                            }
                        } else {
                            $("#GanttDataContainer").html("");
                            $("#GanttBlockContainer").html("");
                            $("#EntitiesTree").html("");
                            $("#ListHolder").html("");
                            if (FilterID > 0 || AplyFilterObj.length > 0) {
                                $(window).AdjustHeightWidth();
                            }
                        }
                        $scope.disableShowAll = false;
                    });
                });
            }
        }
        $scope.LoadGantHeader = function () {
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            for (var j = 0; j < $scope.ListViewDetails.length; j++) {
                if ($scope.ListViewDetails[j].data.Response.Data != null) {
                    for (var i = 0; i < $scope.ListViewDetails[j].data.Response.Data.length; i++) {
                        $($.parseXML($scope.ListViewDetails[j].data.Response.Data[i]['Period'])).find('p').each(function () {
                            tempstartdates.push(new Date.create($(this).attr('s')));
                            tempenddates.push(new Date.create($(this).attr('e')));
                        })
                        if (data != null) {
                            $($.parseXML(data['MileStone'])).find('p').each(function () {
                                tempstartdates.push($(this).attr('s'));
                            });
                        }
                    }
                }
            }
            var maxstartDate = "";
            var maxendDate = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            maxstartDate = ConvertDateToString(new Date.create(Math.min.apply(null, tempstartdates)));
            maxendDate = ConvertDateToString(new Date.create(Math.max.apply(null, tempenddates)));
            if (maxstartDate != "") start = maxstartDate;
            if (maxendDate != "") end = maxendDate;
            if (end == "NaN-NaN-NaN") end = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            var currentYear = new Date.create().getFullYear();
            var strtDate = maxendDate.substring(0, 4);
            var endingDate = maxendDate.substring(0, 4);
            if (currentYear < endingDate) {
                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                    day: 1,
                    month: 0
                }).toString('yyyy-MM-dd');
                $scope.Gnttsettings.EndDate = Date.parse(maxendDate).set({
                    day: 31,
                    month: 11
                }).toString('yyyy-MM-dd');;
            } else {
                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                    day: 1,
                    month: 0
                }).toString('yyyy-MM-dd');
                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                    day: 31,
                    month: 11
                }).toString('yyyy-MM-dd');
            }
            if (currentYear == strtDate && currentYear == endingDate) {
                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                    day: 1,
                    month: 0
                }).toString('yyyy-MM-dd');
                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                    day: 31,
                    month: 11
                }).toString('yyyy-MM-dd');
            }
            if (end == "NaN-NaN-NaN" && start == "NaN-NaN-NaN") {
                $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                    day: 1,
                    month: 0
                }).toString('yyyy-MM-dd');
                $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                    day: 31,
                    month: 11
                }).toString('yyyy-MM-dd');
            }
            if (start == "NaN-NaN-NaN" || start == "") start = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            if (end == "NaN-NaN-NaN" || end == "") end = Date.parse('+1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            start = Date.parse(start);
            end = Date.parse(end);
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start];
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) {
                    dates[next.getFullYear()] = [];
                }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }
            $scope.dates = dates;
            return dates;
        }
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            $scope.GanttViewDataPageTemplate = "";
            $scope.GanttViewBlockPageTemplate = "";
            var PageSize = 1;
            var itemsPerPage = 30;
            if (ItemCnt > itemsPerPage) {
                PageSize = ItemCnt / itemsPerPage;
            }
            var TreeTemplate = '';
            var ListTemplate = '';
            var height = 34 * 30;
            for (var i = 0; i < PageSize; i++) {
                TreeTemplate += "<li appear-top-offset='" + height + "' class='pending listviewBGDiv' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list nav-list-menu'></ul></li>"
                ListTemplate += "<tbody class='listviewBGDiv pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>";
                $scope.GanttViewDataPageTemplate += '<tbody class="widthcent" data-page="' + i + '"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>';
                $scope.GanttViewBlockPageTemplate += '<div data-page="' + i + '" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            }
            if (TreeTemplate.length > 0) {
                $scope.GanttViewDataPageTemplate += '<tbody class="blankTreeNode"></tbody>';
                $scope.ListTemplate = ListTemplate;
                $("#EntitiesTree").html($compile(TreeTemplate + '<li  class="blankTreeNode"></li>')($scope));
                $scope.$broadcast('templateCreation');
            }
        }

        var pageQueue = [];
        var ScrollTop = true;
        var iScrollPos = 0;
        $('#EntitiesTree').scroll(function (e) {
            $("#ListHolder").scrollTop($(this).scrollTop());
            $("#GanttDataContainer").scrollTop($(this).scrollTop());
            clearTimeout(timer);
            timer = setTimeout(function () {
                StartPageLoad(false)
            }, 1);
        });
        $(window).scroll(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                StartPageLoad(true)
            }, 1);
        });

        function StartPageLoad(windowScroll) {
            var obj = $('#EntitiesTree');
            var areaHeight = 0;
            var areaTop = 0;
            if (windowScroll) {
                areaHeight = $(window).height();
                areaTop = $(window).scrollTop();
            } else {
                areaHeight = $(obj).height();
                areaTop = $(obj).position().top;
            }
            $('li.pending', obj).each(function () {
                var top = $(this).position().top - areaTop;
                var height = $(this).height();
                if (top + height < 0) { } else if (top > areaHeight) { } else {
                    $(this).removeClass('pending').addClass('inprogress');
                    pageQueue.push($(this).attr('data-page'));
                }
            });
            obj = null;
        }
        setTimeout(LoadPageContent, 1);

        function LoadPageContent() {
            var PageNo = pageQueue.pop();
            if (PageNo == undefined) {
                setTimeout(LoadPageContent, 1);
            } else {
                var StartRowNo = PageNo * 30;
                var MaxNoofRow = 30;
                TreeLoadPage(PageNo, StartRowNo, MaxNoofRow);
            }
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = [];

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function Space(UniKey) {
            var substr = UniKey.split('.');
            var itag = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 1; i < row; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }
        $scope.entitytpesdata = [];
        $scope.LoadEntityTitle = function (typeID) {
            $scope.entitytpesdata = [];
            $scope.entitytpesdata = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                return e.ParentTypeId == parseInt(typeID);
            });
            $("#moduleContextmenu").modal("show");
        };

        function LoadChildTreeNodes(data, ChildCount, UniqueId) {
            if (ChildCount > 0) {
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right");
                    Collapse(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false");
                    $('#EntitiesTree').scroll();
                } else {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down");
                    Expand(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true");
                }
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") {
                    $scope.ExpandingEntityID.EntityID = data;
                    TreeLoad(UniqueId, false);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false");
                }
            }
        }
        var PathforBreadCrum = "";
        $scope.array = [];
        $(document).click(function () {
            $('#moduleContextmenu').hide();
        })
        $scope.EntityTypeID;
        $("#treeSettings").click(function (e) {
            $(window).AdjustHeightWidth();
            var TargetControl = $(e.target);
            $scope.EntityLevelfrFin.levelfrFinancial = TargetControl.attr('data-EntityLevel');
            $scope.muiiscalender.Iscalender = false;
            $scope.muiiscalender.CalenderID = 0;
            var col = "";
            $scope.EntityTypeID = parseInt(TargetControl.find("I[data-role='Overview']").attr('data-typeID'), 10);
            if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') {
                LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'))
            } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
                    $scope.SelectedCostCentreCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
                    $scope.SelectedCostCentreCurrency.ShortName = $scope.DefaultSettings.CurrencyFormat.Name;
                    $scope.SelectedCostCentreCurrency.Rate = 1;
                    ReportCurrency.CurrencyRate = 1;
                    ReportCurrency.Name = $scope.DefaultSettings.CurrencyFormat.ShortName;
                    $scope.entitytpesdata = [];
                    $('#moduleContextmenu').hide();
                    $scope.ContextEntityID = TargetControl.attr('data-entityID');
                    $scope.ContextEntityLevel = TargetControl.parent().attr('data-EntityLevel');
                    $scope.ContextEntityName = TargetControl.parent().attr('data-entityname');
                    $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                    $window.GlobalUniquekey = TargetControl.parents('li').attr('data-uniquekey');
                    $(window).trigger("GloblaUniqueKeyAccess", [$window.GlobalUniquekey]);
                    $scope.ShowDeleteOption = false;
                    $scope.ShowDuplicateOption = false;
                    $scope.ShowDeleteOption = false;
                    if ($scope.ContextEntityLevel == 0) $scope.ShowDeleteOption = true;
                    else $scope.ShowDeleteOption = true;
                    $scope.IsOwner = false;
                    if (parseInt(TargetControl.attr('data-AccessValue')) == 1 || parseInt(TargetControl.attr('data-AccessValue')) == 2) {
                        $scope.ShowDeleteOption = true;
                        $scope.ShowDuplicateOption = true;
                    } else if (parseInt(TargetControl.attr('data-AccessValue')) == 3) {
                        $scope.ShowDeleteOption = false;
                        $scope.ShowDuplicateOption = false;
                    }
                    Timeoutvar_forDeatil.context = $timeout(function () {
                        if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                            if (parseInt(TargetControl.attr('data-AccessValue')) == 1 || parseInt(TargetControl.attr('data-AccessValue')) == 2) {
                                $scope.entitytpesdata = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                    return e.ParentTypeId == parseInt(TargetControl.attr('data-typeID'))
                                });
                            } else {
                                $scope.entitytpesdata = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                    return e.ParentTypeId == parseInt(TargetControl.attr('data-typeID')) && e.EntityTypeAccessIsEnabled == true
                                });
                            }
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        } else if ($scope.ShowDeleteOption == true || $scope.ShowDeleteOption == true) {
                            $('#moduleContextmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                            e.preventDefault();
                            return false;
                        }
                    }, 50);
                } else e.preventDefault();
            } else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    $scope.EntityTypeID = TargetControl.attr('data-entitytypeid');
                    localScope.$apply(function () {
                        var id = TargetControl.attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parents('li').attr('data-uniquekey');
                        $window.SubscriptionEntityID = id;
                        if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                        $(TargetControl).parents('li').addClass("active");
                        if ($stateParams.tabID != undefined)
                            $location.path('/mui/planningtool/default/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                        else
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/default/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                            else {
                                if (localStorage.getItem('lasttab') == "attachment")
                                    $scope.entitylockstatus.isUpdatedfrmsection = false;
                                $location.path('/mui/planningtool/default/detail/section/' + TargetControl.attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                            }
                        //$location.path('/mui/planningtool/default/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                    });
                } else e.preventDefault();
            } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    localScope.$apply(function () {
                        var id = TargetControl.parent().attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.next().attr('data-typeid');
                        $window.SubscriptionEntityID = id;
                        $window.GlobalUniquekey = TargetControl.parent().parent().attr('data-uniquekey');
                        $scope.EntityTypeID = TargetControl.parent().attr('data-entitytypeid');
                        if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                        $(TargetControl).parents('li').addClass("active");
                        if ($stateParams.tabID != undefined)
                            $location.path('/mui/planningtool/default/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview');
                        else
                            if (!localStorage.lasttab)
                                $location.path('/mui/planningtool/default/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/planningtool/default/detail/section/' + TargetControl.parent().attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                    });
                } else e.preventDefault();
            }
            $(window).AdjustHeightWidth();
        });
        $scope.UniqueKey = "";
        $(window).on("GloblaUniqueKeyAccess", function (event, UniqueKey) {
            $scope.UniqueKey = UniqueKey;
        });
        $("#EntitiesTree").on("ActivityLoadEntity", function (event, EntityID) {
            $scope.ExpandingEntityID.EntityID = EntityID;
            TreeLoad($scope.UniqueKey, true);
        });

        function TreeLoad(IdUnique, Actvitycreation) {
            var StartRowNo = 0;
            var MaxNoofRow = 30;
            var TreeItem = "";
            if (Actvitycreation == false) {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = false;
                parentNode.EntityID = '0';
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID.EntityID;
                parentNode.IsSingleID = false;
                parentNode.FilterType = 1;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = AplyFilterObj;
                var dataval = {
                    StartRowNo: StartRowNo,
                    MaxNoofRow: MaxNoofRow,
                    FilterID: 0,
                    SortOrderColumn: "null",
                    IsDesc: false,
                    IncludeChildren: false,
                    EntityID: '0',
                    Level: $scope.Level,
                    ExpandingEntityID: $scope.ExpandingEntityID.EntityID,
                    IsSingleID: false,
                    FilterType: 1,
                    IDArr: GlobalChildNode,
                    FilterAttributes: AplyFilterObj
                };
                var DetailData = JSON.stringify(dataval);
                $.ajax({
                    type: "POST",
                    url: "api/Metadata/ActivityDetail/",
                    contentType: "application/json",
                    data: DetailData,
                    async: false,
                    success: function (ParentNode) {
                        if (ParentNode.Response != null) {
                            $scope.ParenttreeList = ParentNode.Response.Data;
                            if ($scope.ParenttreeList != null) {
                                $scope.EntityLevelfrFin.levelfrFinancial = $scope.ParenttreeList[0].Level;
                                if ($scope.ListViewDetails[0].data == "") {
                                    $scope.ListViewDetails[0].data = ParentNode;
                                    $scope.ListViewDetails[0].PageIndex = 0
                                    $scope.ListViewDetails[0].UniqueID = IdUnique
                                } else {
                                    $scope.ListViewDetails.push({
                                        data: ParentNode,
                                        PageIndex: 0,
                                        UniqueID: IdUnique
                                    });
                                }
                                var SubEntityUniquekey = '';
                                var UniqueId = '';
                                for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                                    UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                                    var ClassName = GenateClass(UniqueId);
                                    ClassName += " mo" + UniqueId;
                                    SubEntityUniquekey = UniqueId;
                                    TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                                    TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                                    if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                        if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                            TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                        } else {
                                            TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                        }
                                    } else {
                                        TreeItem += "<i class='icon-'></i>";
                                    }
                                    TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                                    TreeItem += "<span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                                    if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                                        TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                    } else if ($scope.ParenttreeList[i].Permission == 3) {
                                        var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                            return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                        })
                                        if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                                    } else if ($scope.ParenttreeList[i].Permission == 0) { }
                                    TreeItem += "</a> </li>";
                                }
                                setTimeout(function () {
                                    $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                                }, 100);
                                if (Actvitycreation == true) {
                                    $("#EntitiesTree li.active").removeClass('active')
                                    $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                                    if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                        $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                        $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                                    }
                                }
                                $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                                if ($scope.EntityHighlight == true) {
                                    $("#EntitiesTree li.active").removeClass('active')
                                    $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                                    $scope.EntityHighlight = false;
                                }
                            }
                        }
                    }
                });
            } else {
                var data = {};
                data.StartRowNo = StartRowNo;
                data.MaxNoofRow = MaxNoofRow;
                data.FilterID = 0;
                data.SortOrderColumn = "null";
                data.IsDesc = false;
                data.IncludeChildren = true;
                data.EntityID = '0';
                data.Level = $scope.Level;
                data.IDArr = GlobalChildNode;
                data.FilterAttributes = AplyFilterObj;
                data.ExpandingEntityID = $scope.ExpandingEntityID.EntityID;
                PlandetailService.GetActivityByID(data).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        $scope.EntityLevelfrFin.levelfrFinancial = $scope.ParenttreeList[0].Level;
                        if ($scope.ListViewDetails[0].data == "") {
                            $scope.ListViewDetails[0].data = ParentNode;
                            $scope.ListViewDetails[0].PageIndex = 0
                            $scope.ListViewDetails[0].UniqueID = IdUnique
                        } else {
                            $scope.ListViewDetails.push({
                                data: ParentNode,
                                PageIndex: 0,
                                UniqueID: IdUnique
                            });
                        }
                        var SubEntityUniquekey = '';
                        var UniqueId = '';
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                            var ClassName = GenateClass(UniqueId);
                            ClassName += " mo" + UniqueId;
                            SubEntityUniquekey = UniqueId;
                            TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                            if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                } else {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                }
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                                TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            } else if ($scope.ParenttreeList[i].Permission == 3) {
                                var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                    return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                })
                                if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            } else if ($scope.ParenttreeList[i].Permission == 0) { }
                            TreeItem += "</a> </li>";
                        }
                        setTimeout(function () {
                            $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        }, 100);
                        if (Actvitycreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                    }
                });
            }
            $(window).AdjustHeightWidth();
        }
        $scope.LoadContextMemnuPopup = function (typeid) {
            $("#moduleContextmenu").modal("show");
        }

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="TreeCtrlCss">';
            $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }
        var CurrentDuplicateIdList = new Array();

        function DuplicateTreeLoad(IdUnique, IncludeChildren) {
            var StartRowNo = 0;
            var MaxNoofRow = 100;
            var TreeItem = "";
            var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = 0;
            parentNode.SortOrderColumn = "null";
            parentNode.IsDesc = false;
            parentNode.IncludeChildren = IncludeChildren;
            parentNode.EntityID = '0';
            parentNode.Level = $scope.Level;
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID.EntityID;
            parentNode.IsSingleID = false;
            parentNode.FilterType = 1;
            parentNode.IDArr = CurrentDuplicateIdList;
            parentNode.FilterAttributes = AplyFilterObj;
            PlandetailService.ActivityDetail(parentNode).then(function (ParentNode) {
                $scope.ParenttreeList = ParentNode.Response.Data;
                if ($scope.ParenttreeList != null) {
                    if ($scope.ListViewDetails[0].data == "") {
                        $scope.ListViewDetails[0].data = ParentNode;
                        $scope.ListViewDetails[0].PageIndex = 0
                        $scope.ListViewDetails[0].UniqueID = IdUnique
                    } else {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: 0,
                            UniqueID: IdUnique
                        });
                    }
                    var SubEntityUniquekey = '';
                    for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                        var UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                        var ClassName = GenateClass(UniqueId);
                        ClassName += " mo" + UniqueId;
                        SubEntityUniquekey = UniqueId;
                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "'  data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "' data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                        TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                        if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                            if (UniqueId.split("-").length <= parseInt($scope.Level) || $scope.ParenttreeList[i]["ParentID"] == 0) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span data-icon='ActivityIcon' data-uniqueKey='" + UniqueId + "' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                        TreeItem += "<span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\">" + $scope.ParenttreeList[i]["Name"] + "</span>";
                        if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeList[i].Permission == 3) {
                            var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                            })
                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeList[i].Permission == 0) { }
                        TreeItem += "</a> </li>";
                    }
                    $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                    for (var i = 0; i < CurrentDuplicateIdList.length; i++) {
                        GlobalChildNode.push(CurrentDuplicateIdList[i]);
                    }
                    if ($scope.EntityHighlight == true) {
                        $("#EntitiesTree li.active").removeClass('active')
                        $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                        $scope.EntityHighlight = false;
                    }
                    $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                }
            });
            $(window).AdjustHeightWidth();
        }

        function CreateSubEntityType(Id, Caption, ContextentityID) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/SubEntityCreation.html',
                controller: "mui.planningtool.SubEntityCreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            'ID': Id,
                            'Caption': Caption,
                            'CurrentEntityId': ContextentityID
                        };
                    }
                },
                scope: $scope,
                windowClass: 'SubEntityCreationModal popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.CreateSubEntity = function (event, data) {
            CreateSubEntityType(data.Id, data.Caption, $scope.ContextEntityID);
        };
        $scope.$on('DETAILEVENT', function (event, duplicatedata) {
            CurrentDuplicateIdList = new Array();
            for (var i = 0; i < duplicatedata.length; i++)
                CurrentDuplicateIdList.push(duplicatedata[i]);
            var splitArray = $window.GlobalUniquekey.split('-');
            if (splitArray.length > 0) {
                var uniquekey = $("li.p" + splitArray[0]).last().attr('data-uniquekey');
                if (uniquekey == undefined) {
                    uniquekey = splitArray[0];
                }
                $("#duplicateEnitynames").html("");
                $scope.ExpandingEntityID.EntityID = 0;
                DuplicateTreeLoad(uniquekey, true);
            }
        });
        $scope.ContextEntityDelete = function () {
            var itemms = $('#EntitiesTree LI[data-uniquekey=' + $window.GlobalUniquekey + ']');
            var previousEntityID = itemms.prev('li').find('a').attr('data-entityid');
            var UniqId = $('#EntitiesTree LI[data-uniquekey=' + $window.GlobalUniquekey + ']').attr('data-uniquekey').replace(/\-/g, '.');
            PlandetailService.DeleteApprovedAlloc(UniqId).then(function (result) {
                $scope.ApprovedAlloc = result.Response;
                if ($scope.ApprovedAlloc == null || parseInt($scope.ApprovedAlloc) == 0) {
                    var object = {};
                    var IDList = new Array();
                    IDList.push($scope.ContextEntityID);
                    object.ID = IDList;
                    PlandetailService.MemberAvailable(object).then(function (memberavailability) {
                        if (memberavailability.Response == false) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5117.Caption'));
                            return true;
                        }
                        bootbox.confirm($translate.instant('LanguageContents.Res_2039.Caption'), function (result) {
                            if (result) {
                                Timeoutvar_forDeatil.deleteentity = $timeout(function () {
                                    PlandetailService.DeleteEntity(object).then(function (result) {
                                        if (result.Response == true) {
                                            $('#EntitiesTree LI[data-uniquekey=' + $window.GlobalUniquekey + ']').remove();
                                            $('.p' + $window.GlobalUniquekey).remove();
                                            if ($scope.ContextEntityID == parseInt($stateParams.ID, 10)) {
                                                if (previousEntityID != undefined || previousEntityID != null) $location.path('/mui/planningtool/default/detail/section/' + parseInt(previousEntityID, 10) + '/overview');
                                                else $location.path('/Plans');
                                            }
                                            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                                for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                                    var UniqueKey = UniqueKEY($scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"]);
                                                    if (UniqueKey == $window.GlobalUniquekey) {
                                                        var entparentID = $scope.ListViewDetails[i].data.Response.Data[j].ParentID;
                                                        $scope.ListViewDetails[i].data.Response.Data.splice(j, 1);
                                                        var allentitydata = [];
                                                        for (var k = 0; k < $scope.ListViewDetails.length; k++) {
                                                            var dataobj = $scope.ListViewDetails[k].data.Response.Data;
                                                            for (l = 0; l < $scope.ListViewDetails[k].data.Response.Data.length; l++) {
                                                                allentitydata.push($scope.ListViewDetails[k].data.Response.Data[l]);
                                                            }

                                                        }

                                                        var childentitycount = $.grep(allentitydata, function (s) { return s.ParentID == entparentID });
                                                        if (childentitycount.length == 0) {
                                                            $('#EntitiesTree lI > a[data-entityid=' + entparentID + '] i[data-parentid=' + entparentID + ']').hide();
                                                        }
                                                        $('#GanttBlockContainer > div > div[data-uniquekey=' + $window.GlobalUniquekey + ']').remove();
                                                        $('#GanttDataContainer tr[data-uniquekey=' + $window.GlobalUniquekey + ']').remove();
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }, 100);
                            }
                        });
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_5721.Caption'));
                }
            });
        }
        $scope.ContextEntityDuplicate = function () {
            var entityId = [];
            entityId.push($scope.ContextEntityID);
            $('#DuplicateModels').modal('show');
            $scope.DuplicateEntity(entityId, $scope.ContextEntityLevel, "DETAIL");
        }

        function TreeLoadPage(PageNo, StartRowNo, MaxNoofRow) {
            if (AplyFilterObj.length > 0) {
                $scope.Level = "100";
            }
            var TreeItem = "";
            var ChildInclude = true;
            var listval = [];
            var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = FilterID;
            if ($scope.SortOrderColumnId == 0) {
                parentNode.SortOrderColumn = "null";
                parentNode.Level = $scope.Level;
                parentNode.IsDesc = false;
            } else {
                parentNode.SortOrderColumn = $scope.SortOrderColumnId;
                parentNode.Level = "100";
                parentNode.IsDesc = $scope.IsDescFromGanttView;
            }
            parentNode.IncludeChildren = ChildInclude;
            parentNode.EntityID = '0';
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID.EntityID;
            parentNode.IsSingleID = false;
            parentNode.FilterType = $scope.FilterType;
            parentNode.IDArr = GlobalChildNode;
            parentNode.FilterAttributes = AplyFilterObj;
            PlandetailService.ActivityDetail(parentNode).then(function (ParentNode) {
                var optID = "";
                var totcnt = 0;
                var IsAppendVal = false;
                var preval = "";
                if ($scope.FilterType == 5) {
                    $scope.Tempattrgroupfilter = [];
                    for (var i = 0; i < ParentNode.Response.Data.length; i++) {
                        IsAppendVal = false;
                        preval = "";
                        if (optID != ParentNode.Response.Data[i].OptionCaption) {
                            totcnt = $.grep(ParentNode.Response.Data, function (e) {
                                return e.OptionID == ParentNode.Response.Data[i].OptionID
                            });
                            for (var x = 0; x < $scope.ListViewDetails.length; x++) {
                                preval = $.grep($scope.ListViewDetails[x].data.Response.Data, function (e) {
                                    return e.Name == ParentNode.Response.Data[i]["OptionCaption"]
                                });
                                if (preval.length > 0) {
                                    IsAppendVal = true;
                                    break;
                                }
                            }
                            if (IsAppendVal == false) {
                                $scope.Tempattrgroupfilter.push({
                                    1: "",
                                    69: "",
                                    71: "",
                                    ColorCode: "000000",
                                    EntityID: "",
                                    EntityStateID: "",
                                    Id: 0,
                                    IsLock: false,
                                    Level: -1,
                                    MileStone: "<root/>",
                                    Name: ParentNode.Response.Data[i]["OptionCaption"],
                                    ParentID: 0,
                                    Period: "",
                                    Permission: 0,
                                    ShortDescription: "G",
                                    TempPeriod: "",
                                    TotalChildrenCount: totcnt.length + 1,
                                    TypeID: 0,
                                    TypeName: "@Option",
                                    UniqueKey: "-0" + i,
                                    Parenentitytname: "-"
                                });
                            }
                        }
                        $scope.Tempattrgroupfilter.push({
                            1: ParentNode.Response.Data[i]["1"],
                            69: ParentNode.Response.Data[i]["69"],
                            71: ParentNode.Response.Data[i]["71"],
                            ColorCode: ParentNode.Response.Data[i]["ColorCode"],
                            EntityID: ParentNode.Response.Data[i]["EntityID"],
                            EntityStateID: ParentNode.Response.Data[i]["EntityStateID"],
                            Id: ParentNode.Response.Data[i]["Id"],
                            IsLock: ParentNode.Response.Data[i]["IsLock"],
                            Level: ParentNode.Response.Data[i]["Level"],
                            MileStone: ParentNode.Response.Data[i]["MileStone"],
                            Name: ParentNode.Response.Data[i]["Name"],
                            ParentID: ParentNode.Response.Data[i]["ParentID"],
                            Period: ParentNode.Response.Data[i]["Period"],
                            Permission: ParentNode.Response.Data[i]["Permission"],
                            ShortDescription: ParentNode.Response.Data[i]["ShortDescription"],
                            TempPeriod: ParentNode.Response.Data[i]["TempPeriod"],
                            TotalChildrenCount: ParentNode.Response.Data[i]["TotalChildrenCount"],
                            TypeID: ParentNode.Response.Data[i]["TypeID"],
                            TypeName: ParentNode.Response.Data[i]["TypeName"],
                            UniqueKey: ParentNode.Response.Data[i]["UniqueKey"],
                            Parenentitytname: ParentNode.Response.Data[i]["Parenentitytname"]
                        });
                        optID = ParentNode.Response.Data[i].OptionCaption;
                    }
                    $scope.ParenttreeNewList = $scope.Tempattrgroupfilter;
                    ParentNode.Response.Data = $scope.Tempattrgroupfilter;
                } else $scope.ParenttreeNewList = ParentNode.Response.Data;
                $scope.ParenttreeNewList = ParentNode.Response.Data;
                if ($scope.ParenttreeNewList != null) {
                    $scope.ListViewDetails.push({
                        data: ParentNode,
                        PageIndex: PageNo,
                        UniqueID: ""
                    });
                    for (var i = 0; i < $scope.ParenttreeNewList.length; i++) {
                        var UniqueId = UniqueKEY($scope.ParenttreeNewList[i]["UniqueKey"]);
                        var ClassName = GenateClass(UniqueId);
                        ClassName += " mo" + UniqueId;
                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeNewList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + " data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\"> ";
                        TreeItem += Space($scope.ParenttreeNewList[i]["UniqueKey"]);
                        if ($scope.ParenttreeNewList[i]["TotalChildrenCount"] > 0) {
                            if (UniqueId.split("-").length <= parseInt($scope.Level) || $scope.ParenttreeNewList[i]["ParentID"] == 0) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span data-icon='ActivityIcon' data-uniqueKey='" + UniqueId + "' data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " style='background-color: #" + $scope.ParenttreeNewList[i]["ColorCode"] + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeNewList[i]["ShortDescription"] + "</span>";
                        TreeItem += " <span class='treeItemName' data-name='text' data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeNewList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\">" + $scope.ParenttreeNewList[i]["Name"] + "</span>";
                        if ($scope.ParenttreeNewList[i].Permission == 1 || $scope.ParenttreeNewList[i].Permission == 2) {
                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeNewList[i].Permission + " data-typeID=" + $scope.ParenttreeNewList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeNewList[i].Permission == 3) {
                            var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == parseInt($scope.ParenttreeNewList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                            })
                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeNewList[i].Permission + " data-typeID=" + $scope.ParenttreeNewList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeNewList[i].Permission == 0) { }
                        TreeItem += "</a> </li>";
                    }
                    if (TreeItem.length > 0) {
                        var IsScrollvalid = true
                        $('#EntitiesTree li[data-page=' + PageNo + '] > ul').html(TreeItem);
                        $('#EntitiesTree li[data-page=' + PageNo + ']').removeClass('inprogress').addClass('done').removeAttr('style');
                        if ($scope.EntityHighlight == true && IsScrollvalid == false) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        if ($scope.EntityHighlight == false && IsScrollvalid == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, PageNo, ""]);
                        setTimeout(LoadPageContent, 1);
                    }
                }
                $(window).AdjustHeightWidth();
            });
        };
        $scope.listofreportEntityID = '';
        $scope.ListofReportRecords = function () {

            $("#ReportPageModel").modal('show');
            $scope.chkFinancialDetl = true;
            $scope.chkMetadataDetl = true;
            $scope.chkTaskDetl = true;
            $scope.chkMemberDetl = true;
            $scope.StructuralReportDiv = false;
            $scope.ReportGenerate = false;
            $scope.ReportGanttGenerate = false;
            $scope.ReportEdit = false;
            $scope.ReportTypes = [];
            $scope.SelectedReport = "";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            var IDLIST = new Array();
            var ExpandingEntityID = 0;
            var IncludeChildren = true;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                ExpandingEntityID = $stateParams.ID;
            }
            if (FilterID > 0) {
                IncludeChildren = false;
            }
            PlandetailService.ShowReports(0, true).then(function (saveviewscomment) {
                if (saveviewscomment.Response != null) {
                    var resultcustom = saveviewscomment.Response;
                    for (var i = 0, reportDatacustom; reportDatacustom = resultcustom[i++];) {
                        $scope.ReportTypes.push({
                            "OID": reportDatacustom.OID.toString(),
                            "Caption": reportDatacustom.Caption,
                            "ImageURl": "",
                            "Description": ""
                        });
                    }
                } else { }
            });
            PlandetailService.GetFinancialReportSettings().then(function (ReportResultData) {
                var result = ReportResultData.Response;
                for (var i = 0, reportData; reportData = result[i++];) {
                    $scope.ReportTypes.push({
                        "OID": "F_" + reportData.ID.toString(),
                        "Caption": reportData.ReportSettingsName,
                        "ImageURl": reportData.ReportImage,
                        "Description": reportData.Description
                    });
                }
            });
            PlandetailService.GettableauReportSettings(0).then(function (TableauReportResultData) {
                var resultset = TableauReportResultData.Response;
                for (var i = 0, reportDataset; reportDataset = resultset[i++];) {
                    $scope.ReportTypes.push({
                        "OID": "T_" + reportDataset.ID.toString(),
                        "Caption": reportDataset.ReportSettingsName,
                        "ImageURl": reportDataset.ReportImage,
                        "Description": reportDataset.Description
                    });
                }
            });
        }
        $scope.changeReportType = function () {
            var eidlist = $scope.listofreportEntityID;
            $scope.Reportreset = [];
            $scope.selectvalue = 0;
            $scope.selectvalue = $scope.SelectedReport;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.StructuralReportDiv = false;
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            if ($scope.selectvalue.contains('F_') || $scope.selectvalue.contains('T_')) {
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                var ReportTypeData = $.grep($scope.ReportTypes, function (e) {
                    return e.OID == $scope.selectvalue;
                });
                $scope.ReportImageURL = ReportTypeData[0].ImageURl;
                $scope.reportDescription = ReportTypeData[0].Description;
            }
                //else if ($scope.selectvalue.contains('T_')) {
                //    $scope.ReportGenerate = true;
                //    $scope.ReportGanttGenerate = false;
                //    $scope.CustomReportDiv = false;
                //    var ReportTypeData = $.grep($scope.ReportTypes, function (e) {
                //        return e.OID == $scope.selectvalue;
                //    });
                //    $scope.ReportImageURL = ReportTypeData[0].ImageURl;
                //    $scope.reportDescription = ReportTypeData[0].Description;
                //}
            else if ($scope.selectvalue == -200) {
                $scope.reportDescription = "Structure report"
                $scope.ReportImageURL = "Structure.jpg";
                $scope.StructuralReportDiv = true;
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    $scope.SelectedEntityCheckDivStr = true;
                    $scope.SubLevelsCheckDivStr = true;
                } else {
                    $scope.SelectedEntityCheckDivStr = false;
                    $scope.SubLevelsCheckDivStr = false;
                }
            } else {
                if ($scope.selectvalue > 0) {
                    $scope.ReportEdit = true;
                    $scope.ReportGenerate = true;
                    $scope.ReportGanttGenerate = false;
                    $scope.GanttReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.CustomReportDiv = true;
                    } else {
                        $scope.CustomReportDiv = false;
                    }
                    PlandetailService.ShowReports($scope.selectvalue, true).then(function (BindReports) {
                        if (BindReports.Response != null) {
                            $scope.Reportreset = BindReports.Response;
                            var count = BindReports.Response.length;
                            $scope.showCustomEntityCheck = $scope.Reportreset[count - 1].EntityLevel;
                            $scope.showCustomSubLevels = $scope.Reportreset[count - 1].SubLevel;
                            $scope.reportDescription = $scope.Reportreset[count - 1].Description;
                            $scope.ReportValue = $scope.Reportreset[count - 1].OID;
                            $scope.ReportImageURL = $scope.Reportreset[count - 1].Preview;
                            $scope.ReportEdit = "devexpress.reportserver:open?reportId=" + $scope.ReportValue + "&revisionId=0&preview=0";
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4523.Caption'));
                        }
                    });
                } else if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.ReportGanttGenerate = true;
                    $scope.reportDescription = "Generate an excel report of the gantt view. Use filters to filter out desired data before generating a report.";
                    $scope.ReportImageURL = "GanttReport.png";
                    $scope.GanttReportDiv = true;
                    $scope.Ganttoption = '';
                    $scope.GanttEntityType = '';
                    $scope.GanttstartDate = "";
                    $scope.Ganttenddate = "";
                    $scope.GanttSelectedEntityCheck = "";
                    $scope.GanttAllSubLevelsCheck = "";
                    $scope.CustomReportDiv = false;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        $scope.SelectedEntityCheckDiv = true;
                        $scope.SubLevelsCheckDiv = true;
                    } else {
                        $scope.SelectedEntityCheckDiv = false;
                        $scope.SubLevelsCheckDiv = false;
                    }
                    PlandetailService.GetEntityTypefromDB().then(function (getEntityTypes) {
                        $scope.GanttEntityTypes = getEntityTypes.Response;
                    });
                    PlandetailService.GetAdminSettingselemntnode('ReportSettings', 'GanttViewReport', 0).then(function (getattribtuesResult) {
                        var getattribtues = getattribtuesResult.Response;
                        if ((getattribtues != undefined || getattribtues != null) && (getattribtues.GanttViewReport != undefined || getattribtues.GanttViewReport != null)) {
                            if (getattribtues.GanttViewReport.Attributes.Attribute.length > 0) {
                                $scope.Ganttoptions = getattribtues.GanttViewReport.Attributes.Attribute;
                            } else if (getattribtues.GanttViewReport.Attributes.Attribute.Id != "") {
                                $scope.Ganttoptionsdata = [];
                                $scope.Ganttoptionsdata.push({
                                    "Id": getattribtues.GanttViewReport.Attributes.Attribute.Id,
                                    "Field": getattribtues.GanttViewReport.Attributes.Attribute.Field,
                                    "DisplayName": getattribtues.GanttViewReport.Attributes.Attribute.DisplayName
                                });
                                $scope.Ganttoptions = $scope.Ganttoptionsdata;
                            }
                        }
                    });
                } else {
                    $scope.ReportEdit = false;
                    $scope.ReportGenerate = false;
                    $scope.GanttReportDiv = false;
                }
            }
        }
        $scope.changeEntityType = function () {
            if ($scope.GanttEntityType.length > 0) {
                $scope.SelectedEntityCheckDiv = false;
                $scope.SubLevelsCheckDiv = false;
                $scope.GanttSelectedEntityCheck = "";
                $scope.GanttAllSubLevelsCheck = "";
            } else if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                $scope.SelectedEntityCheckDiv = true;
                $scope.SubLevelsCheckDiv = true;
            }
        };
        $scope.ChangeEntitylevelCheck = function () {
            if ($scope.GanttSelectedEntityCheck == true) {
                $scope.GanttAllSubLevelsCheck = false;
            }
        };
        $scope.ChangeSubLevelsCheck = function () {
            if ($scope.GanttAllSubLevelsCheck == true) {
                $scope.GanttSelectedEntityCheck = false;
            }
        };
        $scope.AddDefaultEndDate = function (startdate) {
            $("#EntityMetadata").addClass('notvalidate');
            var startDate = new Date.create(startdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
            if (startDate == null) {
                $scope.Ganttenddate = null
            } else {
                $scope.Ganttenddate = startDate.addDays(7);
            }
        };
        $scope.CustomChangeEntitylevelCheck = function () {
            if ($scope.CustomEntityCheck == true) {
                $scope.CustomSubLevelsCheck = false;
            }
        };
        $scope.CustomChangeSubLevelsCheck = function () {
            if ($scope.CustomSubLevelsCheck == true) {
                $scope.CustomEntityCheck = false;
            }
        };
        $scope.ChangeEntitylevelCheckStr = function () {
            if ($scope.GanttSelectedEntityCheckStr == true) {
                $scope.GanttAllSubLevelsCheckStr = false;
            }
        };
        $scope.ChangeSubLevelsCheckStr = function () {
            if ($scope.GanttAllSubLevelsCheckStr == true) {
                $scope.GanttSelectedEntityCheckStr = false;
            }
        };

        $scope.hideReportPopup = function () {
            if ($scope.selectvalue.contains('F_')) {
                var reportid = $scope.selectvalue.split('F_')[1];
                var mypage = "reports/FinancialReport.html?id=" + parseInt(reportid) + "&session=" + $cookies["Session"] + "&crate=" + (ReportCurrency.CurrencyRate) + "&csrt=" + (ReportCurrency.Name) + "";
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, mypage, winprops)
            } else if ($scope.selectvalue.contains('T_')) {
                var reportid = $scope.selectvalue.split('T_')[1];
                var mypage = "views/TableauReport.html?id=" + parseInt(reportid) + "&EntityIds=" + $window.ListofEntityID.join(",") + "&session=" + $cookies["Session"] + "";
                var w = 1200;
                var h = 800;
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, mypage, winprops);
            } else if ($scope.selectvalue == -200) {
                $scope.CustomReportDiv = false;
                $scope.showCustomEntityCheck = false;
                $scope.showCustomSubLevels = false;
                $scope.CustomEntityCheck = "";
                $scope.CustomSubLevelsCheck = "";
                $scope.ProcessReport = "Structure Report";
                $("#ReportPageModel").modal("hide");
                $('#loadingReportPageModel').modal("show");
                var ExpandingEntityIDStr = 0;
                var IncludeChildrenStr = true;
                if ($stateParams.ID != undefined && $stateParams.ID != 0 && ($scope.GanttSelectedEntityCheckStr == true)) {
                    ExpandingEntityIDStr = $stateParams.ID;
                }
                if ((FilterID == 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = true;
                }
                if ((FilterID > 0) && (ExpandingEntityIDStr > 0) && ($scope.GanttSelectedEntityCheckStr == "") && ($scope.GanttAllSubLevelsCheckStr == "")) {
                    ExpandingEntityIDStr = 0;
                    IncludeChildrenStr = false;
                } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheckStr == true)) {
                    IncludeChildrenStr = false;
                }
                PlandetailService.GetStrucuralRptDetail(($scope.chkFinancialDetl != undefined ? $scope.chkFinancialDetl : false), ($scope.chkMetadataDetl != undefined ? $scope.chkMetadataDetl : false), ($scope.chkTaskDetl != undefined ? $scope.chkTaskDetl : false), ($scope.chkMemberDetl != undefined ? $scope.chkMemberDetl : false), ExpandingEntityIDStr, ($scope.GanttAllSubLevelsCheckStr != undefined ? $scope.GanttAllSubLevelsCheckStr : false)).then(function (RptResult) {
                    if (RptResult.StatusCode == 200 && RptResult.Response != null) {
                        var a = document.createElement('a'),
							fileid = RptResult.Response,
							extn = '.xlsx';
                        var filename = 'StructuralReport.xlsx';
                        a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                        a.download = fileid + extn;
                        document.body.appendChild(a);
                        $('#loadingReportPageModel').modal("hide");
                        a.click();
                    }
                });
            } else {
                if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                    var GetActivityListrepose = [];
                    if ($scope.GanttstartDate.toString().length > 0 && $scope.Ganttenddate.toString().length > 0) {
                        var sdate = new Date.create($scope.GanttstartDate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var edate = new Date.create($scope.Ganttenddate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                        var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                        if (diffval < 0) {
                            $scope.GanttstartDate = "";
                            $scope.Ganttenddate = "";
                            bootbox.alert($translate.instant('LanguageContents.Res_4767.Caption'));
                            return false;
                        }
                    } else if ($scope.GanttstartDate.toString().length > 0 || $scope.Ganttenddate.toString().length > 0) {
                        $scope.GanttstartDate = "";
                        $scope.Ganttenddate = "";
                        bootbox.alert($translate.instant('LanguageContents.Res_4577.Caption'));
                        return false;
                    }
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.GanttSelectedEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $("#ReportPageModel").modal("hide");
                    $('#loadingReportPageModel').modal("show");
                    $scope.ProcessReport = "Gantt View Report";
                    var IsMonthlyGantt = $scope.selectvalue == -100 ? false : true;
                    var Node2 = {};
                    Node2.IDArr = GlobalChildNode;
                    Node2.FilterAttributes = AplyFilterObj;
                    Node2.EntityTypeArr = $scope.GanttEntityType;
                    Node2.OptionAttributeArr = $scope.Ganttoption;
                    Node2.GanttstartDate = $scope.GanttstartDate;
                    Node2.Ganttenddate = $scope.Ganttenddate;
                    Node2.FilterID = FilterID;
                    Node2.SortOrderColumn = "null";
                    Node2.IsDesc = false;
                    Node2.IncludeChildren = IncludeChildren;
                    Node2.EntityID = '0';
                    Node2.Level = 100;
                    Node2.ExpandingEntityID = ExpandingEntityID;
                    Node2.IsMonthly = IsMonthlyGantt;
                    PlandetailService.ActivityDetailReportSystem(Node2).then(function (GetActivityList2) {
                        if (GetActivityList2.Response != null) {
                            GetActivityListrepose = GetActivityList2.Response;
                            var a = document.createElement('a'),
								fileid = GetActivityListrepose,
								extn = '.xlsx';
                            var datetimetdy = ConvertDateToString(new Date.create(), $scope.DefaultSettings.DateFormat);
                            var filename = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy + extn;
                            a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                            a.download = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy;
                            document.body.appendChild(a);
                            $('#loadingReportPageModel').modal("hide");
                            a.click();
                        }
                    });
                } else if ($scope.selectvalue > 0) {
                    var IDLIST = new Array();
                    var ExpandingEntityID = 0;
                    var IncludeChildren = true;
                    if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                        ExpandingEntityID = $stateParams.ID;
                    }
                    if ((FilterID == 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = true;
                    }
                    if ((FilterID > 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                        ExpandingEntityID = 0;
                        IncludeChildren = false;
                    } else if (FilterID > 0 || ($scope.CustomEntityCheck == true)) {
                        IncludeChildren = false;
                    }
                    $scope.listofreportEntityID = '';
                    var Node1 = {};
                    Node1.FilterID = FilterID;
                    Node1.SortOrderColumn = "null";
                    Node1.IsDesc = false;
                    Node1.IncludeChildren = IncludeChildren;
                    Node1.EntityID = '0';
                    Node1.Level = 100;
                    Node1.ExpandingEntityID = ExpandingEntityID;
                    Node1.IDArr = GlobalChildNode;
                    Node1.FilterAttributes = AplyFilterObj;
                    PlandetailService.ActivityDetailReport(Node1).then(function (GetActivityList1) {
                        if (GetActivityList1.Response != null) {
                            $scope.listofreportEntityID = '';
                            if (GetActivityList1.Response.length > 0) {
                                for (var i = 0; i < GetActivityList1.Response.length; i++) {
                                    IDLIST.push(GetActivityList1.Response[i]);
                                }
                            }
                            $scope.listofreportEntityID = IDLIST.join(",");
                            var param = {
                                'ID': $scope.ReportValue,
                                'SID': $scope.listofreportEntityID
                            };
                            var w = 1200;
                            var h = 800
                            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                            OpenWindowWithPost("reportviewer.aspx", winprops, "NewFile", param);
                        }
                    });
                }
            }
        }
        $scope.SelectedFilterID = 0;
        $scope.SelectedFilterType = 1;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        PlandetailService.GetFilterSettingsForDetail(6).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });

        function OpenWindowWithPost(url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("report.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow");
                $("#plandetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            } else {
                $("#plandetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 6, $scope.SelectedFilterType]);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow");
            }
        };
        $scope.ApplyFilter = function (filterid, filtername, IsDetailFilter) {
            $scope.SelectedFilterID = filterid;
            $scope.SelectedFilterType = IsDetailFilter;
            if (filterid != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $scope.planDetailFilterID = filterid;
                $scope.planDetailFilterName = filtername;
            } else {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $('.FilterHolder').slideUp("slow");
                $("#rootlevelfilter").trigger('ClearScope', $scope.EntityTypeID);
            }
            $("#plandetailfilter").trigger('applyplandetailfilter', [filterid, filtername, IsDetailFilter]);
        }
        $("#plandetailfilter").on('reloaddetailfilter', function (event, typeId, filtername, filterid, filtertype) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#plandetailfilter").trigger('EditFilterSettingsByFilterID', [filterid, 6, filtertype]);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = 'Edit filter';
                $scope.planDetailFilterID = filterid;
                $scope.planDetailFilterName = filtername;
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $scope.SelectedFilterID = 0;
                $scope.SelectedFilterType = 1;
                $('.FilterHolder').slideUp("slow")
            }
            PlandetailService.GetFilterSettingsForDetail(typeId).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.atributesRelationList = [];
            });
        });
        $("EntitiesTree").on("ontreeNodeCreation", function (EntityID, TotalChildrenCount, ParentUniqueKey, UniqueKey, TypeID, Textdescription, ColorCode, ShortDescription) {
            var UniqueId = UniqueKEY(UniqueKey);
            var ClassName = GenateClass(UniqueId);
            var TreeItem = "";
            ClassName += " mo" + UniqueId;
            TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-entityID=" + EntityID + "> ";
            TreeItem += Space(UniqueKey);
            if (TotalChildrenCount > 0) {
                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + EntityID + " data-TotalChildrenCount=" + TotalChildrenCount + " data-UniqueId=\"" + UniqueId + "\"></i>";
            } else {
                TreeItem += "<i class='icon-'></i>";
            }
            TreeItem += " <span style='background-color: #" + ColorCode + ";'  class='eicon-s margin-right5x'>" + ShortDescription + "</span>";
            TreeItem += Textdescription;
            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-typeID=" + TypeID + "></i>";
            TreeItem += "</a> </li>";
            $('#treeSettings li[data-uniqueKey=' + ParentUniqueKey + ']').after($compile(TreeItem)($scope));
        });
        $scope.PlanExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
        $scope.PlanExpndClass = 'icon-caret-right';
        $scope.disableShowAll = false;
        $scope.ExpandAll = function () {
            if ($scope.disableShowAll == false) {
                $scope.disableShowAll = true;
                $scope.ExpandingEntityID.EntityID = '0';
                $scope.ListViewDetails = [{
                    data: "",
                    PageIndex: 0,
                    UniqueID: ""
                }];
                $scope.ListTemplate = "";
                $scope.GanttViewDataPageTemplate = '';
                $scope.GanttViewBlockPageTemplate = '';
                $('#TreeCtrlCss').remove();
                if ($scope.PlanExpandtext == $translate.instant('LanguageContents.Res_4939.Caption')) {
                    $scope.Level = "100";
                    GetTreeCount();
                    $scope.PlanExpandtext = $translate.instant('LanguageContents.Res_4940.Caption');
                    $scope.PlanExpndClass = 'icon-caret-down';
                } else {
                    $scope.Level = "0";
                    GetTreeCount();
                    $scope.PlanExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
                    $scope.PlanExpndClass = 'icon-caret-right';
                }
                $('.ganttview-data').scrollTop(0);
                $('.list_rightblock').scrollTop(0);
            }
        }
        PlandetailService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) {
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
            }
        });
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.$broadcast('callbackstoptimer');
        $('#dynamicControls').on('shown.bs.modal', function () {
            $scope.$broadcast('callbackacivatetimer');
        });
        $('#dynamicControls').on('hidden.bs.modal', function (event) {
            $scope.$broadcast('callbackstoptimer');
        });
        $scope.$on('OnGanttViewSorting', function (event, ganttSortedData, IsDesc) {
            $scope.IsSortFromGanttView = true;
            $scope.SortOrderColumnId = ganttSortedData;
            GetTreeCount(ganttSortedData, IsDesc, "100");
        });
        $scope.OpenCalenderCreationPopup = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/calender/calendercreation.html',
                controller: "mui.planningtool.calender.calendercreationCtrl",
                resolve: {
                    params: function () {
                        return {
                        };
                    }
                },
                scope: $scope,
                windowClass: 'newCalenderModal popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on("$destroy", function () {
            $timeout.cancel(Timeoutvar_forDeatil);
            $("#GanttDataContainer").html("");
            $("#GanttBlockContainer").html("");
            $("#EntitiesTree").html("");
            $("#ListHolder").html("");
            $('#EntitiesTree').html("");
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detailCtrl']"));
            $scope.$broadcast('callbackstoptimer');
        });
    }
    app.controller("mui.planningtool.default.detailCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$location', '$templateCache', '$cookies', '$translate', 'PlandetailService', '$modal', muiplanningtooldefaultdetailCtrl]);
})(angular, app);