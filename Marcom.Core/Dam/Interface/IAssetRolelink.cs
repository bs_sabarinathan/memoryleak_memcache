﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetRolelink
    {
        int ID { get; set; }
        int Roleid{ get; set; }
        int Assetid { get; set; }
    }
}
