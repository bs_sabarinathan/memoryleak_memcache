﻿using BrandSystems.MediaHandler.Input;
using BrandSystems.MediaHandler;
using BrandSystems.MediaHandler.Output;
using BrandSystems.MediaHandler.CustomType;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using BrandSystems.Marcom.Core.Utility;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections;
using System.Xml.Serialization;
using System.Threading;
using System.Xml.Linq;




namespace BrandSystems.Marcom.Core.Utility.MediaHandler
{
    class MediaHandlerClient
    {
        private MediaHandlerService.WorkflowService _proxy = new MediaHandlerService.WorkflowService();
         MediaHandlerService.WorkflowService objProxyForPing = new MediaHandlerService.WorkflowService();
        private ExecuteAsynchronousTaskEventHandler objCallBack;
        private ExecuteAsynchronousTasksEventHandler objCallBacks;
        private string[] _mimeType = null;
        private bool CallForFirstTime = true;
        private string _serverIPAddress;
        private int _portCount;
        Thread Thread1 = default(Thread);
        private XElement _propertyDefault = null;
        public XElement PropertyDefault
        {
            get { return _propertyDefault; }
            set { _propertyDefault = value; }
        }
        public string[] MimeType
        {
            get { return _mimeType; }
            set { _mimeType = value; }
        }

        #region "Deligates and Events"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public delegate void ExecuteAsynchronousTaskEventHandler(object sender, MediaHandlerResponseObject e);
        public event ExecuteAsynchronousTaskEventHandler ExecuteAsynchronousTaskEvent;
        public delegate void ExecuteAsynchronousTasksEventHandler(object sender, MediaHandlerResponseObject[] e);
        public event ExecuteAsynchronousTasksEventHandler ExecuteAsynchronousTasksEvent;
        #endregion

        #region "Constructor"


/// <summary>
/// 
/// </summary>
/// <remarks></remarks>
 public MediaHandlerClient(string MediaHandlerService, ExecuteAsynchronousTaskEventHandler objCallBackArg, ExecuteAsynchronousTasksEventHandler objCallBacksArg, string strIPAddress = null, int intMaxport = 0, int intMinport = 0)
{
	objCallBack = objCallBackArg;
	objCallBacks = objCallBacksArg;
	

	_proxy.Timeout = 1800000;
	_proxy.Url = MediaHandlerService;
	//My.MySettings.Default("MediaHandlerClient_MediaHandlerService_MediaHandlerService")
	objProxyForPing.Url = MediaHandlerService;
	try {
		Ping();
	} catch (Exception ex) {
		try {
			Ping();
		} catch (Exception ex1) {
			CallForFirstTime = true;
		}
	}

	if ((strIPAddress != null)) {
		_serverIPAddress = strIPAddress;
		//Else
		//    _serverIPAddress = My.MySettings.Default("IPAddress")
	}

	//************************************* TCP *************************************//
	// using TCP protocol
	int MaxPort = 0;
	int MinPort = 0;
	bool status = false;

	
	_portCount = 0;
}
///' <summary>
///' 
///' </summary>
///' <remarks></remarks>
//Public Sub New(ByVal appPath As String, ByVal objCallBackArg As ExecuteAsynchronousTaskEventHandler, ByVal objCallBacksArg As ExecuteAsynchronousTasksEventHandler)
//    Me.New(appPath)
//    objCallBack = objCallBackArg
//    objCallBacks = objCallBacksArg
//End Sub


#endregion
        #region "Functions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="argStr"></param>
        /// <param name="argType"></param>
        /// <remarks></remarks>

        private string argStrObj = null;
        public void GetStr(string argStr, string argType)
        {
            

            argStrObj = argStr;
            switch (argType)
            {
                case "ImageResponseObject":
                    Thread1 = new Thread(FireEventForImage);
                    break;
                case "OfficeResponseObject":
                    Thread1 = new Thread(FireEventForOffice);
                    break;
                case "VideoResponseObject":
                    Thread1 = new Thread(FireEventForVideo);
                    break;
            }

            Thread1.IsBackground = true;
            //Thread1.TrySetApartmentState(ApartmentState.STA)
            Thread1.Start();

        }

        private void FireEventForImage()
        {
            string argStr = argStrObj;

            StringReader Input = new StringReader(argStr);
            XmlSerializer xs = new XmlSerializer(typeof(ImageResponseObject[]));
            ImageResponseObject[] Obj = (ImageResponseObject[])xs.Deserialize(Input);

            if (Obj.Length == 1)
            {
                if ((objCallBack != null))
                {
                    objCallBack.Invoke(this, Obj[0]);
                }
                else
                {
                    if (ExecuteAsynchronousTaskEvent != null)
                    {
                        ExecuteAsynchronousTaskEvent(this, Obj[0]);
                    }
                }

            }
            else if (Obj.Length > 1)
            {
                if ((objCallBacks != null))
                {
                    objCallBacks.Invoke(this, Obj);
                }
                else
                {
                    if (ExecuteAsynchronousTasksEvent != null)
                    {
                        ExecuteAsynchronousTasksEvent(this, Obj);
                    }
                }
            }
            else
            {
                // TODO: Need to handle the situation
            }

        }

        private void FireEventForOffice()
        {
            string argStr = argStrObj;

            StringReader Input = new StringReader(argStr);
            XmlSerializer xs = new XmlSerializer(typeof(OfficeResponseObject[]));
            OfficeResponseObject[] Obj = (OfficeResponseObject[])xs.Deserialize(Input);

            if (Obj.Length == 1)
            {
                if ((objCallBack != null))
                {
                    objCallBack.Invoke(this, Obj[0]);
                }
                else
                {
                    if (ExecuteAsynchronousTaskEvent != null)
                    {
                        ExecuteAsynchronousTaskEvent(this, Obj[0]);
                    }
                }

            }
            else if (Obj.Length > 1)
            {
                if ((objCallBacks != null))
                {
                    objCallBacks.Invoke(this, Obj);
                }
                else
                {
                    if (ExecuteAsynchronousTasksEvent != null)
                    {
                        ExecuteAsynchronousTasksEvent(this, Obj);
                    }
                }
            }
            else
            {
                // TODO: Need to handle the situation
            }

        }

        private void FireEventForVideo()
        {
            string argStr = argStrObj;

            StringReader Input = new StringReader(argStr);
            XmlSerializer xs = new XmlSerializer(typeof(VideoResponseObject[]));
            VideoResponseObject[] Obj = (VideoResponseObject[])xs.Deserialize(Input);

            if (Obj.Length == 1)
            {
                if ((objCallBack != null))
                {
                    objCallBack.Invoke(this, Obj[0]);
                }
                else
                {
                    if (ExecuteAsynchronousTaskEvent != null)
                    {
                        ExecuteAsynchronousTaskEvent(this, Obj[0]);
                    }
                }

            }
            else if (Obj.Length > 1)
            {
                if ((objCallBacks != null))
                {
                    objCallBacks.Invoke(this, Obj);
                }
                else
                {
                    if (ExecuteAsynchronousTasksEvent != null)
                    {
                        ExecuteAsynchronousTasksEvent(this, Obj);
                    }
                }
            }
            else
            {
                // TODO: Need to handle the situation
            }

        }
        //Private Sub FireEventForPPT(ByVal argStr As String)
        //    Dim Input As New StringReader(argStr)
        //    Dim xs As XmlSerializer = New XmlSerializer(GetType(PPTResponseObject()))
        //    Dim Obj() As PPTResponseObject = CType(xs.Deserialize(Input), PPTResponseObject())

        //    If Obj.Length = 1 Then
        //        If objCallBack IsNot Nothing Then
        //            objCallBack.Invoke(Me, Obj(0))
        //        Else
        //            RaiseEvent ExecuteAsynchronousTaskEvent(Me, Obj(0))
        //        End If

        //    ElseIf Obj.Length > 1 Then
        //        If objCallBacks IsNot Nothing Then
        //            objCallBacks.Invoke(Me, Obj)
        //        Else
        //            RaiseEvent ExecuteAsynchronousTasksEvent(Me, Obj)
        //        End If
        //    Else
        //        ' TODO: Need to handle the situation
        //    End If

        //End Sub

        #region "CheckStatus Functions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string CheckStatusForTask(System.Guid jobID)
        {
            return _proxy.CheckStatusForTask(jobID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string CheckStatusForTasks(System.Guid groupID)
        {
            return _proxy.CheckStatusForTasks(groupID);
        }
        #endregion

        #region "Asynchronous Functions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ExecuteAsynchronousTask(MediaHandlerRequestObject requestObject)
        {

            try
            {
                Validate(requestObject);
                requestObject.IPAddress = _serverIPAddress;
                requestObject.PortNo = _portCount;
                if (_portCount == 0)
                {
                    throw new System.ApplicationException("Validation Fail : Port within the range is not avalable.");
                }
                else
                {
                    return _proxy.ExecuteAsynchronousTask(Cast(requestObject));
                }
            }
            catch (MHValidationFailException MHex)
            {
                throw new System.ApplicationException("Validation Fail : " + MHex.Message + " For Job Id: " + MHex.JobID.ToString());

            }
            catch (Exception ex)
            {
            }


            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ExecuteAsynchronousTask(MediaHandlerRequestObject[] requestObject)
        {

            try
            {
                Validate(requestObject);
                for (int i = 0; i <= requestObject.Length - 1; i++)
                {
                    requestObject[i].IPAddress = _serverIPAddress;
                    requestObject[i].PortNo = _portCount;
                }
                if (_portCount == 0)
                {
                    throw new System.ApplicationException("Validation Fail : Port within the range is not avalable.");
                }
                else
                {
                    return _proxy.ExecuteAsynchronousTasks(Cast(requestObject));
                }
            }
            catch (MHValidationFailException MHex)
            {
                throw new System.ApplicationException("Validation Fail : " + MHex.Message + " For Job Id: " + MHex.JobID.ToString());

            }
            catch (Exception ex)
            {
            }

            return false;
        }

        #endregion

        #region "Synchronous Functions"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// 

        public MediaHandlerResponseObject ExecuteSynchronousTask(MediaHandlerRequestObject requestObject)
        {


            try
            {
                if (MimeType == null)
                {
                    MimeType = this.GetMimeType();
                }

                Validate(requestObject);

                switch (requestObject.GetType().Name)
                {
                    //needtoworke
                    case "ImageRequestObject":
                        if ((((ImageRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();
                            string dExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).DestinationPath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt) & SearchExt(MimeType, dExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, "", requestObject.JobID, 25);
                            }

                        }
                        else
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, " ", requestObject.JobID, 25);
                            }
                        }

                        MediaHandlerResponseObject objMHResObject1 = Cast(_proxy.ExecuteSynchronousTask(Cast(requestObject)));


                        if ((objMHResObject1.ErrorDetail != null))
                        {
                            objMHResObject1.AditionalInfo = requestObject.AditionalInfo;
                            objMHResObject1.ErrorDetail = "Error Code : 26 Error Msg : " + objMHResObject1.ErrorDetail;
                        }


                        return objMHResObject1;

                    case "ImageRequestObject[]":
                        if ((((ImageRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();
                            string dExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).DestinationPath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt) & SearchExt(MimeType, dExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null," ", requestObject.JobID, 25);
                            }

                        }
                        else
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null," ", requestObject.JobID, 25);
                            }
                        }

                        MediaHandlerResponseObject objMHResObject = Cast(_proxy.ExecuteSynchronousTask(Cast(requestObject)));


                        if ((objMHResObject.ErrorDetail != null))
                        {
                            objMHResObject.AditionalInfo = requestObject.AditionalInfo;
                            objMHResObject.ErrorDetail = "Error Code : 26 Error Msg : " + objMHResObject.ErrorDetail;
                        }


                        return objMHResObject;
                    case "OfficeRequestObject":
                        if ((((OfficeRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                        {
                            string sExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();
                            string dExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject).DestinationPath).Replace(".", "").ToUpper();

                            //If Not (SearchExt(MimeType, sExt) And SearchExt(MimeType, dExt)) Then
                            //    Throw New MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", Nothing, Nothing, requestObject.JobID, 25)
                            //End If

                        }
                        else
                        {
                            string sExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();

                            //If Not (SearchExt(MimeType, sExt)) Then
                            //    Throw New MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", Nothing, Nothing, requestObject.JobID, 25)
                            //End If
                        }
                        //need to workes SetProperyDefault
                        // requestObject = SetProperyDefault(requestObject);

                        MediaHandlerResponseObject objMHResObjectOffice = Cast(_proxy.ExecuteSynchronousTask(Cast(requestObject)));


                        if ((objMHResObjectOffice.ErrorDetail != null))
                        {
                            objMHResObjectOffice.AditionalInfo = requestObject.AditionalInfo;
                            objMHResObjectOffice.ErrorDetail = "Error Code : 26 Error Msg : " + objMHResObjectOffice.ErrorDetail;
                        }


                        return objMHResObjectOffice;

                    case "VideoRequestObject":
                        MediaHandlerResponseObject responseObject = default(MediaHandlerResponseObject);
                        MediaHandlerRequestObject[] validRequestObject = null;
                        int l = 0;
                        VideoRequestObject VideoValidation = new VideoRequestObject();

                        if (MimeType == null)
                        {
                            MimeType = this.GetMimeType();
                        }
                        if ((((VideoRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                        {
                            string sExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();
                            //'Dim dExt As String = System.IO.Path.GetExtension(CType(requestObject, VideoRequestObject).DestinationPath).Replace(".", "").ToUpper()
                            if (((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count > 0)
                            {
                                string[] dExt = new string[((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count];
                                for (int i = 0; i <= ((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count - 1; i++)
                                {
                                    //string ext = System.IO.Path.GetExtension(((VideoRequestObject)requestObject).ThumbnailDestinationPath[i]).Replace(".", "").ToUpper();
                                    dExt[i] = System.IO.Path.GetExtension(((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString()).Replace(".", "").ToUpper();
                                    if (SearchExt(MimeType, sExt) == false | SearchExt(MimeType, dExt[i]) == false)
                                    {
                                        throw new MHValidationFailException("SourcePath or Destination extension missing in XML setting file", null, "SourcePath", ((VideoRequestObject)requestObject).JobID, 17);
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(((VideoRequestObject)requestObject).ConverstionDestinationPath))
                            {
                                string dExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject).ConverstionDestinationPath).Replace(".", "").ToUpper();
                                if (SearchExt(MimeType, sExt) == false | SearchExt(MimeType, dExt) == false)
                                {
                                    throw new MHValidationFailException("SourcePath or Destination extension missing in XML setting file", null, "SourcePath", ((VideoRequestObject)requestObject).JobID, 17);
                                }
                            }

                        }
                        else
                        {
                            string sExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt)))
                            {
                                throw new MHValidationFailException("Media Handler Can't Handle provided File Format", null, "", requestObject.JobID, 25);
                            }
                        }

                        MediaHandlerResponseObject objMHResObjectVideo = Cast(_proxy.ExecuteSynchronousTask(Cast(requestObject)));


                        if ((objMHResObjectVideo.ErrorDetail != null))
                        {
                            objMHResObjectVideo.AditionalInfo = requestObject.AditionalInfo;
                            if ((objMHResObjectVideo.ErrorDetail).Length > 0)
                            {
                                objMHResObjectVideo.ErrorDetail = "Error Code : 26 Error Msg : " + objMHResObjectVideo.ErrorDetail;
                            }

                        }

                        return objMHResObjectVideo;
                }



            }
            catch (MHValidationFailException MHex)
            {
                //Throw New System.ApplicationException("Validation Fail : " & MHex.Message & " For Job Id: " & MHex.JobID.ToString())
                throw MHex;
            }
            catch (Exception ex)
            {
                //Select Case requestObject.GetType().Name
                //    Case "ImageRequestObject"
                //        Dim objImageResObj As New ImageResponseObject
                //        objImageResObj.AditionalInfo = requestObject.AditionalInfo
                //        objImageResObj.JobID = requestObject.JobID
                //        objImageResObj.ErrorDetail = ex.Message
                //        Return objImageResObj
                //    Case "PDFRequestObject"

                //    Case "PPTRequestObject"

                //    Case "VideoRequestObject"

                //    Case Else
                //End Select
                if (ex.Message == "The operation has timed-out.")
                {
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Timeout", null, "", requestObject.JobID, 27);
                }
                else
                {
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", ex, null, requestObject.JobID, 26);
                }
            }



            return null;
        }
       
        private bool SearchExt(string[] MimeTypeList, string Ext)
        {

            bool Status = false;

            foreach (string Item in MimeTypeList)
            {
                if (Item == Ext)
                {
                    Status = true;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return Status;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public MediaHandlerResponseObject[] ExecuteSynchronousTask(MediaHandlerRequestObject[] requestObject)
        {

            try
            {
                //Validate(requestObject)

                switch (requestObject.GetType().Name)
                {

                    case "ImageRequestObject[]":

                        if (MimeType == null)
                        {
                            MimeType = this.GetMimeType();
                        }

                        MediaHandlerResponseObject[] responseObject = new MediaHandlerResponseObject[requestObject.Length];
                        MediaHandlerRequestObject[] validRequestObject = null;
                        int l = 0;

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {

                            if ((((ImageRequestObject)requestObject[i]).TaskToPerform ==BrandSystems.MediaHandler.CustomType.Task.Convert))
                            {
                                string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();
                                string dExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject[i]).DestinationPath).Replace(".", "").ToUpper();

                                if (SearchExt(MimeType, sExt) & SearchExt(MimeType, dExt))
                                {
                                    try
                                    {
                                        Validate(requestObject[i]);
                                        Array.Resize(ref validRequestObject, l + 1);
                                        validRequestObject[l] = requestObject[i];
                                        l = l + 1;
                                    }
                                    catch (MHValidationFailException ex)
                                    {
                                        responseObject[i] = new ImageResponseObject(requestObject[i].JobID);
                                        responseObject[i].AditionalInfo = requestObject[i].AditionalInfo;
                                        responseObject[i].GroupID = requestObject[i].GroupID;
                                        responseObject[i].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                    }

                                }

                            }
                            else
                            {
                                string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();

                                if (SearchExt(MimeType, sExt))
                                {
                                    try
                                    {
                                        Validate(requestObject[i]);
                                        Array.Resize(ref validRequestObject, l + 1);
                                        validRequestObject[l] = requestObject[i];
                                        l = l + 1;
                                    }
                                    catch (MHValidationFailException ex)
                                    {
                                        responseObject[l] = new ImageResponseObject(requestObject[i].JobID);
                                        responseObject[l].AditionalInfo = requestObject[i].AditionalInfo;
                                        responseObject[l].GroupID = requestObject[i].GroupID;
                                        responseObject[l].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                    }

                                }

                            }


                        }


                        MediaHandlerResponseObject[] validResponseObject = null;
                        if ((validRequestObject != null))
                        {
                            validResponseObject = Cast(_proxy.ExecuteSynchronousTasks(Cast(validRequestObject)));
                        }


                        //Dim responseObject() As MediaHandlerResponseObject = Cast(_proxy.ExecuteSynchronousTasks(Cast(requestObject)))


                        if ((validRequestObject != null))
                        {
                            for (int i = 0; i <= validResponseObject.Length - 1; i++)
                            {
                                if ((validResponseObject[i].ErrorDetail != null))
                                {
                                    validResponseObject[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    if ((validResponseObject[i].ErrorDetail).Length > 0)
                                    {
                                        validResponseObject[i].ErrorDetail = "Error Code : 26 Error Msg : " + validResponseObject[i].ErrorDetail;
                                    }

                                }
                            }
                        }



                        int index = 0;

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {
                            if (validRequestObject == null)
                            {
                                if (responseObject[i] == null)
                                {
                                    responseObject[i] = new ImageResponseObject(requestObject[i].JobID);
                                    responseObject[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObject[i].GroupID = requestObject[i].GroupID;
                                    responseObject[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                }

                            }
                            else
                            {
                                if (requestObject[i].JobID.ToString() == validResponseObject[index].JobID.ToString())
                                {
                                    responseObject[i] = validResponseObject[index];
                                    index = index + 1;
                                    if (index > validResponseObject.Length - 1)
                                    {
                                        index = validResponseObject.Length - 1;
                                    }
                                }
                                else
                                {
                                    if (responseObject[i] == null)
                                    {
                                        responseObject[i] = new ImageResponseObject(requestObject[i].JobID);
                                        responseObject[i].AditionalInfo = requestObject[i].AditionalInfo;
                                        responseObject[i].GroupID = requestObject[i].GroupID;
                                        responseObject[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                    }

                                }


                            }
                        }



                        return responseObject;
                    case "OfficeRequestObject[]":

                        if (MimeType == null)
                        {
                            MimeType = this.GetMimeType();
                        }

                        MediaHandlerResponseObject[] responseObjectOffice = new MediaHandlerResponseObject[requestObject.Length];
                        MediaHandlerRequestObject[] validRequestObjectOffice = null;
                        int l1 = 0;

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {

                            if ((((OfficeRequestObject)requestObject[i]).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                            {
                                string sExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();
                                string dExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject[i]).DestinationPath).Replace(".", "").ToUpper();
                                //If SearchExt(MimeType, sExt) And SearchExt(MimeType, dExt) Then

                                try
                                {
                                    Validate(requestObject[i]);
                                    Array.Resize(ref validRequestObjectOffice, l1 + 1);
                                    validRequestObjectOffice[l1] = requestObject[i];
                                    l1 = l1 + 1;
                                }
                                catch (MHValidationFailException ex)
                                {
                                    responseObjectOffice[i] = new OfficeResponseObject(requestObject[i].JobID);
                                    responseObjectOffice[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectOffice[i].GroupID = requestObject[i].GroupID;
                                    responseObjectOffice[i].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                }

                                //End If

                            }
                            else
                            {
                                string sExt = System.IO.Path.GetExtension(((OfficeRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();
                                //If SearchExt(MimeType, sExt) Then

                                try
                                {
                                    Validate(requestObject[i]);
                                    Array.Resize(ref validRequestObjectOffice, l1 + 1);
                                    validRequestObjectOffice[l1] = requestObject[i];
                                    l1 = l1 + 1;
                                }
                                catch (MHValidationFailException ex)
                                {
                                    responseObjectOffice[i] = new OfficeResponseObject(requestObject[i].JobID);
                                    responseObjectOffice[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectOffice[i].GroupID = requestObject[i].GroupID;
                                    responseObjectOffice[i].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                }

                                //End If

                            }


                        }


                        MediaHandlerResponseObject[] validResponseObjectOffice = null;
                        if ((validRequestObjectOffice != null))
                        {
                            validResponseObjectOffice = Cast(_proxy.ExecuteSynchronousTasks(Cast(validRequestObjectOffice)));
                        }


                        //Dim responseObject() As MediaHandlerResponseObject = Cast(_proxy.ExecuteSynchronousTasks(Cast(requestObject)))


                        if ((validRequestObjectOffice != null))
                        {
                            for (int i = 0; i <= validResponseObjectOffice.Length - 1; i++)
                            {
                                if ((validResponseObjectOffice[i].ErrorDetail != null))
                                {
                                    validResponseObjectOffice[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    if ((validResponseObjectOffice[i].ErrorDetail).Length > 0)
                                    {
                                        validResponseObjectOffice[i].ErrorDetail = "Error Code : 26 Error Msg : " + validResponseObjectOffice[i].ErrorDetail;
                                    }
                                }
                            }
                        }



                        int index1 = 0;

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {
                            if (validRequestObjectOffice == null)
                            {
                                if (responseObjectOffice[i] == null)
                                {
                                    responseObjectOffice[i] = new OfficeResponseObject(requestObject[i].JobID);
                                    responseObjectOffice[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectOffice[i].GroupID = requestObject[i].GroupID;
                                    responseObjectOffice[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                }

                            }
                            else
                            {
                                if (requestObject[i].JobID.ToString() == validResponseObjectOffice[index1].JobID.ToString())
                                {
                                    responseObjectOffice[i] = validResponseObjectOffice[index1];
                                    index1 = index1 + 1;
                                    if (index1 > validResponseObjectOffice.Length - 1)
                                    {
                                        index1 = validResponseObjectOffice.Length - 1;
                                    }
                                }
                                else
                                {
                                    if (responseObjectOffice[i] == null)
                                    {
                                        responseObjectOffice[i] = new OfficeResponseObject(requestObject[i].JobID);
                                        responseObjectOffice[i].AditionalInfo = requestObject[i].AditionalInfo;
                                        responseObjectOffice[i].GroupID = requestObject[i].GroupID;
                                        responseObjectOffice[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                    }

                                }


                            }
                        }




                        return responseObjectOffice;
                    case "VideoRequestObject[]":

                        MediaHandlerResponseObject[] responseObjectVideo = new MediaHandlerResponseObject[requestObject.Length];
                        MediaHandlerRequestObject[] validRequestObjectVideo = null;
                        int l2 = 0;
                        VideoRequestObject VideoValidation = new VideoRequestObject();

                        if (MimeType == null)
                        {
                            MimeType = this.GetMimeType();
                        }

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {
                            if ((((VideoRequestObject)requestObject[i]).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                            {
                                try
                                {
                                    string sExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();

                                    if (((VideoRequestObject)requestObject[i]).ThumbnailDestinationPath.Count > 0 & !string.IsNullOrEmpty(((VideoRequestObject)requestObject[i]).SourcePath))
                                    {
                                        string[] dExt = new string[((VideoRequestObject)requestObject[i]).ThumbnailDestinationPath.Count];
                                        int j = 0;
                                        for (j = 0; j <= ((VideoRequestObject)requestObject[i]).ThumbnailDestinationPath.Count - 1; j++)
                                        {
                                            dExt[j] = System.IO.Path.GetExtension(((VideoRequestObject)requestObject[i]).ThumbnailDestinationPath[j].ToString()).Replace(".", "").ToUpper();
                                            if (SearchExt(MimeType, sExt) == false | SearchExt(MimeType, dExt[j]) == false)
                                            {
                                                throw new MHValidationFailException("SourcePath or Destination extension missing in XML setting file", null, "SourcePath", ((VideoRequestObject)requestObject[i]).JobID, 17);
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(((VideoRequestObject)requestObject[i]).ConverstionDestinationPath) & !string.IsNullOrEmpty(((VideoRequestObject)requestObject[i]).SourcePath))
                                    {
                                        string dExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject[i]).ConverstionDestinationPath).Replace(".", "").ToUpper();
                                        if (SearchExt(MimeType, sExt) == false | SearchExt(MimeType, dExt) == false)
                                        {
                                            throw new MHValidationFailException("SourcePath or Destination extension missing in XML setting file", null, "SourcePath", ((VideoRequestObject)requestObject[i]).JobID, 17);
                                        }
                                    }


                                    requestObject[i] = SetProperyDefault(requestObject[i]);
                                    VideoValidation.ValidateConvert((VideoRequestObject)requestObject[i]);
                                    Validate(requestObject[i]);
                                    Array.Resize(ref validRequestObjectVideo, l2 + 1);
                                    validRequestObjectVideo[l2] = requestObject[i];
                                    l2 = l2 + 1;
                                }
                                catch (MHValidationFailException ex)
                                {
                                    responseObjectVideo[i] = new VideoResponseObject(requestObject[i].JobID);
                                    responseObjectVideo[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectVideo[i].GroupID = requestObject[i].GroupID;
                                    responseObjectVideo[i].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                }
                            }
                            else
                            {
                                try
                                {
                                    string sExt = System.IO.Path.GetExtension(((VideoRequestObject)requestObject[i]).SourcePath).Replace(".", "").ToUpper();
                                    if (!SearchExt(MimeType, sExt))
                                    {
                                        throw new MHValidationFailException("SourcePath extension missing in XML setting file", null, "SourcePath", ((VideoRequestObject)requestObject[i]).JobID, 17);
                                    }

                                    VideoValidation.ValidateGetInfo((VideoRequestObject)requestObject[i]);
                                    Validate(requestObject[i]);
                                    Array.Resize(ref validRequestObjectVideo, l2 + 1);
                                    validRequestObjectVideo[l2] = requestObject[i];
                                    l2 = l2 + 1;
                                }
                                catch (MHValidationFailException ex)
                                {
                                    responseObjectVideo[i] = new VideoResponseObject(requestObject[i].JobID);
                                    responseObjectVideo[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectVideo[i].GroupID = requestObject[i].GroupID;
                                    responseObjectVideo[i].ErrorDetail = "Error Code : " + ex.ErrorCode + " Error Msg : " + ex.Message;
                                }
                            }
                        }


                        MediaHandlerResponseObject[] validResponseObjectVideo = null;
                        if ((validRequestObjectVideo != null))
                        {
                            validResponseObjectVideo = Cast(_proxy.ExecuteSynchronousTasks(Cast(validRequestObjectVideo)));
                        }


                        //Dim responseObject() As MediaHandlerResponseObject = Cast(_proxy.ExecuteSynchronousTasks(Cast(requestObject)))


                        if ((validRequestObjectVideo != null))
                        {
                            for (int i = 0; i <= validResponseObjectVideo.Length - 1; i++)
                            {
                                if ((validResponseObjectVideo[i].ErrorDetail != null))
                                {
                                    validResponseObjectVideo[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    if (!string.IsNullOrEmpty(validResponseObjectVideo[i].ErrorDetail))
                                    {
                                        validResponseObjectVideo[i].ErrorDetail = "Error Code : 26 Error Msg : " + validResponseObjectVideo[i].ErrorDetail;
                                    }
                                }
                            }
                        }



                        int index2 = 0;

                        for (int i = 0; i <= requestObject.Length - 1; i++)
                        {
                            if (validRequestObjectVideo == null)
                            {
                                if (responseObjectVideo[i] == null)
                                {
                                    responseObjectVideo[i] = new VideoResponseObject(requestObject[i].JobID);
                                    responseObjectVideo[i].AditionalInfo = requestObject[i].AditionalInfo;
                                    responseObjectVideo[i].GroupID = requestObject[i].GroupID;
                                    responseObjectVideo[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                }

                            }
                            else
                            {
                                if (requestObject[i].JobID.ToString() == validResponseObjectVideo[index2].JobID.ToString())
                                {
                                    responseObjectVideo[i] = validResponseObjectVideo[index2];
                                    index2 = index2 + 1;
                                    if (index2 > validResponseObjectVideo.Length - 1)
                                    {
                                        index2 = validResponseObjectVideo.Length - 1;
                                    }
                                }
                                else
                                {
                                    if (responseObjectVideo[i] == null)
                                    {
                                        responseObjectVideo[i] = new VideoResponseObject(requestObject[i].JobID);
                                        responseObjectVideo[i].AditionalInfo = requestObject[i].AditionalInfo;
                                        responseObjectVideo[i].GroupID = requestObject[i].GroupID;
                                        responseObjectVideo[i].ErrorDetail = "Error Code : 25 Error Msg : Media Handler Can't Handle provided File Format";
                                    }

                                }


                            }
                        }




                        return responseObjectVideo;

                }


            }
            catch (MHValidationFailException MHex)
            {
                //Throw New System.ApplicationException("Validation Fail : " & MHex.Message & " For Job Id: " & MHex.JobID.ToString())
                throw MHex;
            }
            catch (Exception ex)
            {
                if (ex.Message == "The operation has timed-out.")
                {
                    //throw new MediaHandler.MHValidationFailException("Timeout", null, null, null, 27);
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Timeout", null);
                }
                else
                {
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null);
                    //throw new MediaHandler.MHValidationFailException("MediaHandler Internal Exception", ex, null, null, 26);
                }
            }

            return null;
        }
        #endregion

        #region "Validation Functions"

        public void ValidateSourceDestination(MediaHandlerRequestObject requestObject)
        {
            string FileAvailability = null;
            //Dim ValidationInfo As New StringBuilder()

            switch (requestObject.GetType().Name)
            //switch (requestObject.GetType.Name)
            {
                case "ImageRequestObject":
                    if (((ImageRequestObject)requestObject).SourcePath == null | string.IsNullOrEmpty(((ImageRequestObject)requestObject).SourcePath))
                    {
                        throw new MHValidationFailException("SourcePath is not provided", null, "SourcePath", requestObject.JobID, 15);

                    }

                    if (!((ImageRequestObject)requestObject).SourcePath.StartsWith("\\\\"))
                    {
                        throw new MHValidationFailException("SourcePath should start with \\\\", null, "SourcePath", requestObject.JobID, 16);

                    }

                    if (string.IsNullOrEmpty(System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath)))
                    {
                        throw new MHValidationFailException("SourcePath is not valid", null, "SourcePath", requestObject.JobID, 17);

                    }

                    FileAvailability = ((ImageRequestObject)requestObject).SourcePath;
                    string strPath = ConvertPath(FileAvailability);
                    if (! requestObject.ISs3amazonbucket)
                    {

                        if (!File.Exists(strPath))
                        {
                            throw new MHValidationFailException("Source File does not Exists", null, "SourcePath", requestObject.JobID, 18);

                        }
                    }

                    if (((ImageRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert)
                    {
                        if (((ImageRequestObject)requestObject).DestinationPath == null | string.IsNullOrEmpty(((ImageRequestObject)requestObject).DestinationPath))
                        {
                            throw new MHValidationFailException("DestinationPath is not provided", null, "DestinationPath", requestObject.JobID, 19);
                        }

                        if (!((ImageRequestObject)requestObject).DestinationPath.StartsWith("\\\\"))
                        {
                            throw new MHValidationFailException("DestinationPath should start with \\\\", null, "DestinationPath", requestObject.JobID, 20);
                        }

                        if (string.IsNullOrEmpty(System.IO.Path.GetExtension(((ImageRequestObject)requestObject).DestinationPath)))
                        {
                            throw new MHValidationFailException("DestinationPath is not valid", null, "DestinationPath", requestObject.JobID, 21);
                        }


                        if (((ImageRequestObject)requestObject).SourcePath == ((ImageRequestObject)requestObject).DestinationPath)
                        {
                            throw new MHValidationFailException("Sourcepath and Destinationpath should not be same", null, "DestinationPath", requestObject.JobID, 22);
                            //ValidationInfo.Append("DestinationPath:Sourcepath and Destinationpath should not be same" + ",")

                        }
                    }
                    break;
                case "OfficeRequestObject":
                    OfficeRequestObject officeReqTask = (OfficeRequestObject)requestObject;

                    if (officeReqTask.SourcePath == null | string.IsNullOrEmpty(officeReqTask.SourcePath))
                    {
                        throw new MHValidationFailException("SourcePath is not provided", null, "SourcePath", requestObject.JobID, 46);
                    }

                    if (!officeReqTask.SourcePath.StartsWith("\\\\"))
                    {
                        throw new MHValidationFailException("SourcePath should start with \\\\", null, "SourcePath", requestObject.JobID, 47);
                    }

                    if (string.IsNullOrEmpty(System.IO.Path.GetExtension(officeReqTask.SourcePath)))
                    {
                        throw new MHValidationFailException("SourcePath is not valid", null, "SourcePath", requestObject.JobID, 48);
                    }
                    FileAvailability = officeReqTask.SourcePath;
                    string strPath2 = ConvertPath(FileAvailability);
                    //If Not File.Exists(strPath2) Then
                    //    Throw New MHValidationFailException("Source File does not Exists", Nothing, "SourcePath", requestObject.JobID, 49)
                    //End If
                    string dExt = string.Empty;
                    string sExt = System.IO.Path.GetExtension(officeReqTask.SourcePath).Replace(".", "").ToUpper();
                    officeReqTask.FileFormat = sExt;
                    switch (sExt)
                    {
                        case "DOC":
                        case "DOCX":
                            officeReqTask.FileCategory = OfficeFileCategory.Word;
                            if ((sExt == "DOC"))
                            {
                                officeReqTask.IsOpenXMLFile = false;
                            }
                            else
                            {
                                officeReqTask.IsOpenXMLFile = true;
                            }
                            break;
                        case "XLS":
                        case "XLSX":
                            officeReqTask.FileCategory = OfficeFileCategory.Excel;
                            if ((sExt == "XLS"))
                            {
                                officeReqTask.IsOpenXMLFile = false;
                            }
                            else
                            {
                                officeReqTask.IsOpenXMLFile = true;
                            }
                            break;
                        case "PPT":
                        case "PPTX":
                            officeReqTask.FileCategory = OfficeFileCategory.PowerPoint;
                            if ((sExt == "PPT"))
                            {
                                officeReqTask.IsOpenXMLFile = false;
                            }
                            else
                            {
                                officeReqTask.IsOpenXMLFile = true;
                            }
                            break;
                        default:
                            throw new MHValidationFailException("Invalid source file", null, "SourcePath", officeReqTask.JobID, 63);
                    }

                    if (officeReqTask.TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert)
                    {
                        if (officeReqTask.DestinationPath == null | string.IsNullOrEmpty(officeReqTask.DestinationPath))
                        {
                            throw new MHValidationFailException("DestinationPath is not provided", null, "DestinationPath", requestObject.JobID, 50);
                        }

                        if (!officeReqTask.DestinationPath.StartsWith("\\\\"))
                        {
                            throw new MHValidationFailException("DestinationPath should start with \\\\", null, "DestinationPath", requestObject.JobID, 51);
                        }

                        if (string.IsNullOrEmpty(System.IO.Path.GetExtension(officeReqTask.DestinationPath)))
                        {
                            throw new MHValidationFailException("DestinationPath is not valid", null, "DestinationPath", requestObject.JobID, 52);
                        }
                        if (officeReqTask.SourcePath == officeReqTask.DestinationPath)
                        {
                            throw new MHValidationFailException("Sourcepath and Destinationpath should not be same", null, "DestinationPath", requestObject.JobID, 53);
                            //ValidationInfo.Append("DestinationPath:Sourcepath and Destinationpath should not be same" + ",")

                        }

                        dExt = System.IO.Path.GetExtension(officeReqTask.DestinationPath).Replace(".", "").ToUpper();
                        officeReqTask.ThumbnailFormat = dExt;
                        if (!(dExt == "JPG" | dExt == "JPEG" | dExt == "GIF" | dExt == "PNG"))
                        {
                            throw new MHValidationFailException("Invalid thumbnail format", null, "thumbnail format", officeReqTask.JobID, 65);
                        }
                        if (officeReqTask.ThumbnailHeight < 1 & officeReqTask.ThumbnailWidth < 1)
                        {
                            throw new MHValidationFailException("Thumnail Width or Height not provided", null, "thumbnail size", officeReqTask.JobID, 66);
                        }
                    }
                    break;
                case "VideoRequestObject":
                    if (((VideoRequestObject)requestObject).SourcePath == null | string.IsNullOrEmpty(((VideoRequestObject)requestObject).SourcePath))
                    {
                        throw new MHValidationFailException("SourcePath is not provided", null, "SourcePath", requestObject.JobID, 54);

                    }

                    if (!((VideoRequestObject)requestObject).SourcePath.StartsWith("\\\\"))
                    {
                        throw new MHValidationFailException("SourcePath should start with \\\\", null, "SourcePath", requestObject.JobID, 55);

                    }

                    if (string.IsNullOrEmpty(System.IO.Path.GetExtension(((VideoRequestObject)requestObject).SourcePath)))
                    {
                        throw new MHValidationFailException("SourcePath is not valid", null, "SourcePath", requestObject.JobID, 56);

                    }
                    //timeing()
                    FileAvailability = ((VideoRequestObject)requestObject).SourcePath;
                    string strPath1 = ConvertPath(FileAvailability);
                    if (!requestObject.ISs3amazonbucket)
                    {
                        if (!File.Exists(strPath1))
                        {
                            throw new MHValidationFailException("Source File does not Exists", null, "SourcePath", requestObject.JobID, 57);

                        }
                    }


                    if (((VideoRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert)
                    {

                        if (((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count > 0)
                        {

                            for (int i = 0; i <= ((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count - 1; i++)
                            {

                                if (((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString() == null | string.IsNullOrEmpty(((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString()))
                                {
                                    throw new MHValidationFailException("DestinationPath is not provided", null, "DestinationPath", requestObject.JobID, 58);
                                }

                                if (!((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString().StartsWith("\\\\"))
                                {
                                    throw new MHValidationFailException("DestinationPath should start with \\\\", null, "DestinationPath", requestObject.JobID, 59);
                                }

                                if (string.IsNullOrEmpty(System.IO.Path.GetExtension(((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString())))
                                {
                                    throw new MHValidationFailException("DestinationPath is not valid", null, "DestinationPath", requestObject.JobID, 60);
                                }


                                if (((VideoRequestObject)requestObject).SourcePath == ((VideoRequestObject)requestObject).ThumbnailDestinationPath[i].ToString())
                                {
                                    throw new MHValidationFailException("Sourcepath and Destinationpath should not be same", null, "DestinationPath", requestObject.JobID, 61);
                                    //ValidationInfo.Append("DestinationPath:Sourcepath and Destinationpath should not be same" + ",")

                                }

                            }


                        }




                    }

                    break;

            }

            //If ValidationInfo.Length > 0 Then
            //    ValidationInfo.Remove(ValidationInfo.Length - 1, 1)
            //    Return ValidationInfo.ToString()
            //Else
            //    Return Nothing
            //End If
        }

        public void Validate(MediaHandlerRequestObject requestObject)
        {
            ValidateBaseObject(requestObject);
            ValidateSourceDestination(requestObject);
        }


        public void ValidateBaseObject(MediaHandlerRequestObject requestObject)
        {
            if (requestObject.ClientID <= 0)
            {
                throw new MHValidationFailException("ClientID is not provided", null, "ClientID", requestObject.JobID, 23);
            }
            if (requestObject.ApplicationID <= 0)
            {
                throw new MHValidationFailException("ApplicationID is not provided", null, "ApplicationID", requestObject.JobID, 24);
            }
            //If ValidationInfo.Length > 0 Then
            //    ValidationInfo.Remove(ValidationInfo.Length - 1, 1)
            //    Return ValidationInfo.ToString()
            //Else
            //    Return Nothing
            //End If
        }

        /// <summary>
        /// This function converts the UNC path to local system path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string ConvertPath(string path)
        {
            string[] Splittedstr = null;
            string strPath = null;
            StringBuilder sb = new StringBuilder();
            Splittedstr = path.Split('\\');
            for (int i = 3; i <= Splittedstr.Length - 1; i++)
            {
                sb.Append(Splittedstr[i] + "\\");
            }
            strPath = sb.ToString();
            strPath = strPath.Remove(strPath.LastIndexOf("\\"), 1);
            strPath = strPath.Replace("$", ":");
            return strPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private void Validate(MediaHandlerRequestObject[] requestObject)
        {
            for (int i = 0; i <= requestObject.Length - 1; i++)
            {
                Validate(requestObject[i]);
            }
        }

        #endregion

        #region "Cast Functions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private MediaHandlerService.MediaHandlerRequestObject Cast(BrandSystems.MediaHandler.Input.MediaHandlerRequestObject arg)
        {

            switch (arg.GetType().Name)
            {
                case "ImageRequestObject":
                    ImageRequestObject MHReqObj = new ImageRequestObject();
                    MediaHandlerService.ImageRequestObject MHSReqObj = new MediaHandlerService.ImageRequestObject();
                    MHReqObj = (ImageRequestObject)arg;

                    MHSReqObj.AditionalInfo = MHReqObj.AditionalInfo;
                    MHSReqObj.ApplicationID = MHReqObj.ApplicationID;
                    MHSReqObj.ClientID = MHReqObj.ClientID;
                    MHSReqObj.ColourSpace = (MediaHandlerService.ColourSpace)MHReqObj.ColourSpace;
                    MHSReqObj.CropHeight = MHReqObj.CropHeight;
                    MHSReqObj.CropLeft = MHReqObj.CropLeft;
                    MHSReqObj.CropTop = MHReqObj.CropTop;
                    MHSReqObj.CropWidth = MHReqObj.CropWidth;
                    MHSReqObj.DestinationPath = MHReqObj.DestinationPath;
                    MHSReqObj.GroupID = MHReqObj.GroupID;
                    MHSReqObj.ImageType = (MediaHandlerService.Type)MHReqObj.ImageType;
                    MHSReqObj.IPAddress = MHReqObj.IPAddress;
                    MHSReqObj.JobID = MHReqObj.JobID;

                    if ((MHReqObj.MetaDataNeeded != null))
                    {
                        MediaHandlerService.MetaData[] MetaDataNeeded = new MediaHandlerService.MetaData[MHReqObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = (MediaHandlerService.MetaData)MHReqObj.MetaDataNeeded[i];
                        }
                        MHSReqObj.MetaDataNeeded = MetaDataNeeded;
                    }

                    MHSReqObj.PageNo = MHReqObj.PageNo;
                    MHSReqObj.PortNo = MHReqObj.PortNo;
                    MHSReqObj.Quality = MHReqObj.Quality;
                    MHSReqObj.ResizeHeight = MHReqObj.ResizeHeight;
                    MHSReqObj.ResizeInPercentage = MHReqObj.ResizeInPercentage;
                    MHSReqObj.ResizeWidth = MHReqObj.ResizeWidth;
                    MHSReqObj.ScaleHeight = MHReqObj.ScaleHeight;
                    MHSReqObj.ScaleInPercentage = MHReqObj.ScaleInPercentage;
                    MHSReqObj.ScaleWidth = MHReqObj.ScaleWidth;
                    MHSReqObj.SourcePath = MHReqObj.SourcePath;
                    MHSReqObj.TaskToPerform = (MediaHandlerService.Task)MHReqObj.TaskToPerform;
                    MHSReqObj.WorkPriority = (MediaHandlerService.PriorityLevel)MHReqObj.WorkPriority;
                    MHSReqObj.XResolution = MHReqObj.XResolution;
                    MHSReqObj.YResolution = MHReqObj.YResolution;
                    MHSReqObj.Thumbnail = MHReqObj.Thumbnail;
                    MHSReqObj.Units = (MediaHandlerService.Units)MHReqObj.Units;
                    MHSReqObj.FlipDirection = (MediaHandlerService.Direction)MHReqObj.FlipDirection;
                    MHSReqObj.RotationAngle = MHReqObj.RotationAngle;
                    MHSReqObj.TopLeftRadius = MHReqObj.TopLeftRadius;
                    MHSReqObj.TopRightRadius = MHReqObj.TopRightRadius;
                    MHSReqObj.BottomLeftRadius = MHReqObj.BottomLeftRadius;
                    MHSReqObj.BottomRightRadius = MHReqObj.BottomRightRadius;
                    MHSReqObj.CornerColor = MHReqObj.CornerColor;
                    MHSReqObj.ISs3amazonbucket = MHReqObj.ISs3amazonbucket;
                    MHSReqObj.s3accesskey = MHReqObj.s3accesskey;
                    MHSReqObj.s3securitykey = MHReqObj.s3securitykey;
                    MHSReqObj.s3region = MHReqObj.s3region;
                    MHSReqObj.s3Foldername = MHReqObj.s3Foldername;
                    MHSReqObj.s3Filename = MHReqObj.s3Filename;
                    MHSReqObj.s3FileSourcePath = MHReqObj.s3FileSourcePath;
                    MHSReqObj.s3FileDestinationPath = MHReqObj.s3FileDestinationPath;
                    return MHSReqObj;
                case "PDFRequestObject":

                    break;
                case "OfficeRequestObject":

                    OfficeRequestObject MHReqOfficeObj = new OfficeRequestObject();
                    MediaHandlerService.OfficeRequestObject MHSReqOfficeObj = new MediaHandlerService.OfficeRequestObject();
                    MHReqOfficeObj = (OfficeRequestObject)arg;

                    MHSReqOfficeObj.AditionalInfo = MHReqOfficeObj.AditionalInfo;
                    MHSReqOfficeObj.ApplicationID = MHReqOfficeObj.ApplicationID;
                    MHSReqOfficeObj.ClientID = MHReqOfficeObj.ClientID;
                    MHSReqOfficeObj.DestinationPath = MHReqOfficeObj.DestinationPath;
                    MHSReqOfficeObj.FileCategory = (MediaHandlerService.OfficeFileCategory)MHReqOfficeObj.FileCategory;
                    MHSReqOfficeObj.FileFormat = MHReqOfficeObj.FileFormat;
                    MHSReqOfficeObj.GroupID = MHReqOfficeObj.GroupID;
                    MHSReqOfficeObj.IPAddress = MHReqOfficeObj.IPAddress;
                    MHSReqOfficeObj.JobID = MHReqOfficeObj.JobID;
                    if ((MHReqOfficeObj.MetaDataNeeded != null))
                    {
                        MediaHandlerService.OfficeMetaData[] MetaDataNeeded = new MediaHandlerService.OfficeMetaData[MHReqOfficeObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqOfficeObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = (MediaHandlerService.OfficeMetaData)MHReqOfficeObj.MetaDataNeeded[i];
                        }
                        MHSReqOfficeObj.MetaDataNeeded = MetaDataNeeded;
                    }

                    MHSReqOfficeObj.PortNo = MHReqOfficeObj.PortNo;
                    MHSReqOfficeObj.SourcePath = MHReqOfficeObj.SourcePath;
                    MHSReqOfficeObj.TaskToPerform = (MediaHandlerService.Task)MHReqOfficeObj.TaskToPerform;
                    MHSReqOfficeObj.ThumbnailFormat = MHReqOfficeObj.ThumbnailFormat;
                    MHSReqOfficeObj.ThumbnailHeight = MHReqOfficeObj.ThumbnailHeight;
                    MHSReqOfficeObj.ThumbnailWidth = MHReqOfficeObj.ThumbnailWidth;
                    MHSReqOfficeObj.WorkPriority = (MediaHandlerService.PriorityLevel)MHReqOfficeObj.WorkPriority;
                    MHSReqOfficeObj.IsOpenXMLFile = MHReqOfficeObj.IsOpenXMLFile;

                    return (MHSReqOfficeObj);

                case "VideoRequestObject":
                    VideoRequestObject MHReqVideoObj = new VideoRequestObject();
                    MediaHandlerService.VideoRequestObject MHSReqVideoObj = new MediaHandlerService.VideoRequestObject();
                    MHReqVideoObj = (VideoRequestObject)arg;

                    MHSReqVideoObj.AditionalInfo = MHReqVideoObj.AditionalInfo;
                    MHSReqVideoObj.ApplicationID = MHReqVideoObj.ApplicationID;
                    MHSReqVideoObj.ClientID = MHReqVideoObj.ClientID;
                    MHSReqVideoObj.GroupID = MHReqVideoObj.GroupID;
                    MHSReqVideoObj.IPAddress = MHReqVideoObj.IPAddress;
                    MHSReqVideoObj.JobID = MHReqVideoObj.JobID;
                    MHSReqVideoObj.PortNo = MHReqVideoObj.PortNo;
                    MHSReqVideoObj.WorkPriority = (MediaHandlerService.PriorityLevel)MHReqVideoObj.WorkPriority;

                    if ((MHReqVideoObj.MetaDataNeeded != null))
                    {
                        MediaHandlerService.VideoMetaData[] MetaDataNeeded = new MediaHandlerService.VideoMetaData[MHReqVideoObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqVideoObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = (MediaHandlerService.VideoMetaData)MHReqVideoObj.MetaDataNeeded[i];
                        }
                        MHSReqVideoObj.MetaDataNeeded = MetaDataNeeded;
                    }

                    MHSReqVideoObj.ThumbnailDestinationPath = MHReqVideoObj.ThumbnailDestinationPath.ToArray();
                    MHSReqVideoObj.VideoPreviewEndTime = MHReqVideoObj.VideoPreviewEndTime;
                    MHSReqVideoObj.Format = MHReqVideoObj.Format;
                    MHSReqVideoObj.SourcePath = MHReqVideoObj.SourcePath;
                    MHSReqVideoObj.VideoPreviewStartTime = MHReqVideoObj.VideoPreviewStartTime;
                    MHSReqVideoObj.TaskToPerform = (MediaHandlerService.Task)MHReqVideoObj.TaskToPerform;
                    MHSReqVideoObj.ThumbnailFormat = MHReqVideoObj.ThumbnailFormat;
                    MHSReqVideoObj.ThumbnailFrameTimeList = MHReqVideoObj.ThumbnailFrameTimeList.ToArray();
                    MHSReqVideoObj.VideoPreviewFormat = MHReqVideoObj.VideoPreviewFormat;
                    MHSReqVideoObj.ConverstionDestinationPath = MHReqVideoObj.ConverstionDestinationPath;
                    MHSReqVideoObj.ThumbnailHeight = MHReqVideoObj.ThumbnailHeight;
                    MHSReqVideoObj.ThumbnailWidth = MHReqVideoObj.ThumbnailWidth;

                    MHSReqVideoObj.Title = MHReqVideoObj.Title;
                    MHSReqVideoObj.Author = MHReqVideoObj.Author;
                    MHSReqVideoObj.Composer = MHReqVideoObj.Composer;
                    MHSReqVideoObj.Album = MHReqVideoObj.Album;
                    MHSReqVideoObj.Year = MHReqVideoObj.Year;
                    MHSReqVideoObj.Track = MHReqVideoObj.Track;
                    MHSReqVideoObj.Comment = MHReqVideoObj.Comment;
                    MHSReqVideoObj.Copyright = MHReqVideoObj.Copyright;
                    MHSReqVideoObj.Description = MHReqVideoObj.Description;
                    MHSReqVideoObj.MetadataUpdateOption = (MediaHandlerService.VideoMetadataUpdateOption)MHReqVideoObj.MetadataUpdateOption;


                    MHSReqVideoObj.SpecificTimeOrTimeRange = MHReqVideoObj.SpecificTimeOrTimeRange;
                    MHSReqVideoObj.NumberOfThumbnail = MHReqVideoObj.NumberOfThumbnail;
                    MHSReqVideoObj.MultiThumbnailStartTime = MHReqVideoObj.MultiThumbnailStartTime;
                    MHSReqVideoObj.MultiThumbnailEndTime = MHReqVideoObj.MultiThumbnailEndTime;
                    MHSReqVideoObj.VideoResolutionHeight = MHReqVideoObj.VideoResolutionHeight;
                    MHSReqVideoObj.VideoResolutionWidth = MHReqVideoObj.VideoResolutionWidth;

                    MHSReqVideoObj.EnableGIF = MHReqVideoObj.EnableGIF;
                    MHSReqVideoObj.EnableRAWImage = MHReqVideoObj.EnableRAWImage;
                    MHSReqVideoObj.OutputVideoCodec = MHReqVideoObj.OutputVideoCodec;
                    MHSReqVideoObj.OutputAudioCodec = MHReqVideoObj.OutputAudioCodec;
                    MHSReqVideoObj.AudioFileBitrate = MHReqVideoObj.AudioFileBitrate;
                    MHSReqVideoObj.IsAudioFile = MHReqVideoObj.IsAudioFile;

                    MHSReqVideoObj.ISs3amazonbucket = MHReqVideoObj.ISs3amazonbucket;
                    MHSReqVideoObj.s3accesskey = MHReqVideoObj.s3accesskey;
                    MHSReqVideoObj.s3securitykey = MHReqVideoObj.s3securitykey;
                    MHSReqVideoObj.s3region = MHReqVideoObj.s3region;
                    MHSReqVideoObj.s3Foldername = MHReqVideoObj.s3Foldername;
                    MHSReqVideoObj.s3Filename = MHReqVideoObj.s3Filename;
                    MHSReqVideoObj.s3FileSourcePath = MHReqVideoObj.s3FileSourcePath;
                    MHSReqVideoObj.s3FileDestinationPath = MHReqVideoObj.s3FileDestinationPath;

                    return MHSReqVideoObj;
                default:

                    break;
            }

            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private MediaHandlerService.MediaHandlerResponseObject Cast(BrandSystems.MediaHandler.Output.MediaHandlerResponseObject arg)
        {

            switch (arg.GetType().Name)
            {
                case "ImageResponseObject":
                    MediaHandlerService.ImageResponseObject MHSResObj = new MediaHandlerService.ImageResponseObject();
                    ImageResponseObject MHResObj = new ImageResponseObject();
                    MHResObj = (ImageResponseObject)arg;

                    MHSResObj.AditionalInfo = MHResObj.AditionalInfo;
                    MHSResObj.ColourSpace = (MediaHandlerService.ColourSpace)MHResObj.ColourSpace;
                    MHSResObj.Compression = MHResObj.Compression;
                    MHSResObj.Depth = MHResObj.Depth;
                    MHSResObj.DestinationPath = MHResObj.DestinationPath;
                    MHSResObj.ErrorDetail = MHResObj.ErrorDetail;
                    MHSResObj.EXIFProfile = MHResObj.EXIFProfile;
                    MHSResObj.Format = MHResObj.Format;
                    MHSResObj.GroupID = MHResObj.GroupID;
                    MHSResObj.Height = MHResObj.Height;
                    MHSResObj.ICCProfile = MHResObj.ICCProfile;
                    MHSResObj.ImageClass = MHResObj.ImageClass;
                    MHSResObj.ImageType = (MediaHandlerService.Type)MHResObj.ImageType;
                    MHSResObj.JobID = MHResObj.JobID;
                    MHSResObj.PageNo = MHResObj.PageNo;
                    MHSResObj.Quality = MHResObj.Quality;
                    MHSResObj.Width = MHResObj.Width;
                    MHSResObj.XResolution = MHResObj.XResolution;
                    MHSResObj.YResolution = MHResObj.YResolution;
                    MHSResObj.IPTCProfile = MHResObj.IPTCProfile;
                    MHSResObj.Units = (MediaHandlerService.Units)MHResObj.Units;

                    return MHSResObj;
                case "PDFResponseObject":

                    break;
                case "OfficeResponseObject":

                    MediaHandlerService.OfficeResponseObject MHSResOfficeObj = new MediaHandlerService.OfficeResponseObject();
                    OfficeResponseObject MHResOfficeObj = new OfficeResponseObject();
                    MHResOfficeObj = (OfficeResponseObject)arg;

                    MHSResOfficeObj.AditionalInfo = MHResOfficeObj.AditionalInfo;
                    MHSResOfficeObj.Author = MHResOfficeObj.Author;
                    MHSResOfficeObj.Category = MHResOfficeObj.Category;
                    MHSResOfficeObj.Comments = MHResOfficeObj.Comments;
                    MHSResOfficeObj.DestinationPath = MHResOfficeObj.DestinationPath;
                    MHSResOfficeObj.ErrorDetail = MHResOfficeObj.ErrorDetail;
                    MHSResOfficeObj.FileCategory = (MediaHandlerService.OfficeFileCategory)MHResOfficeObj.FileCategory;
                    MHSResOfficeObj.FileFormat = MHResOfficeObj.FileFormat;
                    MHSResOfficeObj.GroupID = MHResOfficeObj.GroupID;
                    MHSResOfficeObj.JobID = MHResOfficeObj.JobID;
                    MHSResOfficeObj.Keywords = MHResOfficeObj.Keywords;
                    MHSResOfficeObj.Subject = MHResOfficeObj.Subject;
                    MHSResOfficeObj.Tags = MHResOfficeObj.Tags;
                    MHSResOfficeObj.ThumbnailHeight = MHResOfficeObj.ThumbnailHeight;
                    MHSResOfficeObj.ThumbnailWidth = MHResOfficeObj.ThumbnailWidth;
                    MHSResOfficeObj.Title = MHResOfficeObj.Title;
                    MHSResOfficeObj.TotalCount = MHResOfficeObj.TotalCount;
                    MHSResOfficeObj.IsOpenXMLFile = MHResOfficeObj.IsOpenXMLFile;

                    return MHSResOfficeObj;
                case "VideoResponseObject":

                    MediaHandlerService.VideoResponseObject MHSResVideoObj = new MediaHandlerService.VideoResponseObject();
                    VideoResponseObject MHResVideoObj = new VideoResponseObject();
                    MHResVideoObj = (VideoResponseObject)arg;

                    MHSResVideoObj.ErrorDetail = MHResVideoObj.ErrorDetail;
                    MHSResVideoObj.GroupID = MHResVideoObj.GroupID;
                    MHSResVideoObj.JobID = MHResVideoObj.JobID;

                    MHSResVideoObj.AudioBitrate = MHResVideoObj.AudioBitrate;
                    MHSResVideoObj.AudioChannels = MHResVideoObj.AudioChannels;
                    MHSResVideoObj.AudioSampleRate = MHResVideoObj.AudioSampleRate;
                    MHSResVideoObj.DataRate = MHResVideoObj.DataRate;
                    //MHSResObj.DestinationPath = MHResObj.DestinationPath
                    MHSResVideoObj.FrameHeight = MHResVideoObj.FrameHeight;
                    MHSResVideoObj.FrameWidth = MHResVideoObj.FrameWidth;
                    MHSResVideoObj.FrameRate = MHResVideoObj.FrameRate;
                    MHSResVideoObj.TotalDuration = MHResVideoObj.TotalDuration;

                    MHSResVideoObj.Title = MHResVideoObj.Title;
                    MHSResVideoObj.Author = MHResVideoObj.Author;
                    MHSResVideoObj.Composer = MHResVideoObj.Composer;
                    MHSResVideoObj.Album = MHResVideoObj.Album;
                    MHSResVideoObj.Year = MHResVideoObj.Year;
                    MHSResVideoObj.Track = MHResVideoObj.Track;
                    MHSResVideoObj.Comment = MHResVideoObj.Comment;
                    MHSResVideoObj.Copyright = MHResVideoObj.Copyright;
                    MHSResVideoObj.Description = MHResVideoObj.Description;
                    MHSResVideoObj.MetadataString = MHResVideoObj.MetadataString;

                    return MHSResVideoObj;
                default:

                    break;
            }

            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private BrandSystems.MediaHandler.Input.MediaHandlerRequestObject Cast(MediaHandlerService.MediaHandlerRequestObject arg)
        {

            switch (arg.GetType().Name)
            {
                case "ImageRequestObject":
                    MediaHandlerService.ImageRequestObject MHSReqObj = new MediaHandlerService.ImageRequestObject();
                    ImageRequestObject MHReqObj = new ImageRequestObject();
                    MHSReqObj = (MediaHandlerService.ImageRequestObject)arg;

                    MHReqObj.AditionalInfo = MHSReqObj.AditionalInfo;
                    MHReqObj.ApplicationID = MHSReqObj.ApplicationID;
                    MHReqObj.ClientID = MHSReqObj.ClientID;
                    MHReqObj.ColourSpace = (BrandSystems.MediaHandler.CustomType.ColourSpace)MHSReqObj.ColourSpace;
                    MHReqObj.CropHeight = MHSReqObj.CropHeight;
                    MHReqObj.CropLeft = MHSReqObj.CropLeft;
                    MHReqObj.CropTop = MHSReqObj.CropTop;
                    MHReqObj.CropWidth = MHSReqObj.CropWidth;
                    MHReqObj.DestinationPath = MHSReqObj.DestinationPath;
                    MHReqObj.GroupID = MHSReqObj.GroupID;
                    MHReqObj.ImageType = (BrandSystems.MediaHandler.CustomType.Type)MHSReqObj.ImageType;
                    MHReqObj.IPAddress = MHSReqObj.IPAddress;
                    MHReqObj.JobID = MHSReqObj.JobID;
                    if ((MHReqObj.MetaDataNeeded != null))
                    {
                        BrandSystems.MediaHandler.CustomType.MetaData[] MetaDataNeeded = new BrandSystems.MediaHandler.CustomType.MetaData[MHReqObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = MHReqObj.MetaDataNeeded[i];
                        }
                        MHReqObj.MetaDataNeeded = MetaDataNeeded;
                    }
                    MHReqObj.PageNo = MHSReqObj.PageNo;
                    MHReqObj.PortNo = MHSReqObj.PortNo;
                    MHReqObj.Quality = MHSReqObj.Quality;
                    MHReqObj.ResizeHeight = MHSReqObj.ResizeHeight;
                    MHReqObj.ResizeInPercentage = MHSReqObj.ResizeInPercentage;
                    MHReqObj.ResizeWidth = MHSReqObj.ResizeWidth;
                    MHReqObj.ScaleHeight = MHSReqObj.ScaleHeight;
                    MHReqObj.ScaleInPercentage = MHSReqObj.ScaleInPercentage;
                    MHReqObj.ScaleWidth = MHSReqObj.ScaleWidth;
                    MHReqObj.SourcePath = MHSReqObj.SourcePath;
                    MHReqObj.TaskToPerform = (BrandSystems.MediaHandler.CustomType.Task)MHSReqObj.TaskToPerform;
                    MHReqObj.WorkPriority = (BrandSystems.MediaHandler.CustomType.PriorityLevel)MHSReqObj.WorkPriority;
                    MHReqObj.XResolution = MHSReqObj.XResolution;
                    MHReqObj.YResolution = MHSReqObj.YResolution;
                    MHReqObj.Thumbnail = MHSReqObj.Thumbnail;
                    MHReqObj.Units = (BrandSystems.MediaHandler.CustomType.Units)MHSReqObj.Units;
                    MHReqObj.FlipDirection = (BrandSystems.MediaHandler.CustomType.Direction)MHSReqObj.FlipDirection;
                    MHReqObj.RotationAngle = MHSReqObj.RotationAngle;
                    MHReqObj.TopLeftRadius = MHSReqObj.TopLeftRadius;
                    MHReqObj.TopRightRadius = MHSReqObj.TopRightRadius;
                    MHReqObj.BottomLeftRadius = MHSReqObj.BottomLeftRadius;
                    MHReqObj.BottomRightRadius = MHSReqObj.BottomRightRadius;
                    MHReqObj.CornerColor = MHSReqObj.CornerColor;
                    MHReqObj.ISs3amazonbucket = MHSReqObj.ISs3amazonbucket;
                    MHReqObj.s3accesskey = MHSReqObj.s3accesskey;
                    MHReqObj.s3securitykey = MHSReqObj.s3securitykey;
                    MHReqObj.s3region = MHSReqObj.s3region;
                    MHReqObj.s3Foldername = MHSReqObj.s3Foldername;
                    MHReqObj.s3Filename = MHSReqObj.s3Filename;
                    MHReqObj.s3FileSourcePath = MHSReqObj.s3FileSourcePath;
                    MHReqObj.s3FileDestinationPath = MHSReqObj.s3FileDestinationPath;
                    return MHReqObj;
                case "PDFRequestObject":

                    break;
                case "OfficeRequestObject":

                    MediaHandlerService.OfficeRequestObject MHSReqOfficeObj = new MediaHandlerService.OfficeRequestObject();
                    OfficeRequestObject MHReqOfficeObj = new OfficeRequestObject();
                    MHSReqOfficeObj = (MediaHandlerService.OfficeRequestObject)arg;

                    MHReqOfficeObj.AditionalInfo = MHSReqOfficeObj.AditionalInfo;
                    MHReqOfficeObj.ApplicationID = MHSReqOfficeObj.ApplicationID;
                    MHReqOfficeObj.ClientID = MHSReqOfficeObj.ClientID;
                    MHReqOfficeObj.DestinationPath = MHSReqOfficeObj.DestinationPath;
                    MHReqOfficeObj.FileCategory = (OfficeFileCategory)MHSReqOfficeObj.FileCategory;
                    MHReqOfficeObj.FileFormat = MHSReqOfficeObj.FileFormat;
                    MHReqOfficeObj.GroupID = MHSReqOfficeObj.GroupID;
                    MHReqOfficeObj.IPAddress = MHSReqOfficeObj.IPAddress;
                    MHReqOfficeObj.JobID = MHSReqOfficeObj.JobID;
                    if ((MHReqOfficeObj.MetaDataNeeded != null))
                    {

                        BrandSystems.MediaHandler.CustomType.OfficeMetaData[] MetaDataNeeded = new BrandSystems.MediaHandler.CustomType.OfficeMetaData[MHReqOfficeObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqOfficeObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = MHReqOfficeObj.MetaDataNeeded[i];
                        }
                        MHReqOfficeObj.MetaDataNeeded = MetaDataNeeded;
                    }
                    MHReqOfficeObj.PortNo = MHSReqOfficeObj.PortNo;
                    MHReqOfficeObj.SourcePath = MHSReqOfficeObj.SourcePath;
                    MHReqOfficeObj.TaskToPerform = (BrandSystems.MediaHandler.CustomType.Task)MHSReqOfficeObj.TaskToPerform;
                    MHReqOfficeObj.ThumbnailFormat = MHSReqOfficeObj.ThumbnailFormat;
                    MHReqOfficeObj.ThumbnailHeight = MHSReqOfficeObj.ThumbnailHeight;
                    MHReqOfficeObj.ThumbnailWidth = MHSReqOfficeObj.ThumbnailWidth;
                    MHReqOfficeObj.WorkPriority = (PriorityLevel)MHSReqOfficeObj.WorkPriority;
                    MHReqOfficeObj.IsOpenXMLFile = MHSReqOfficeObj.IsOpenXMLFile;

                    return MHReqOfficeObj;
                case "VideoRequestObject":
                    MediaHandlerService.VideoRequestObject MHSReqVideoObj = new MediaHandlerService.VideoRequestObject();
                    VideoRequestObject MHReqVideoObj = new VideoRequestObject();
                    MHSReqVideoObj = (MediaHandlerService.VideoRequestObject)arg;

                    MHReqVideoObj.AditionalInfo = MHSReqVideoObj.AditionalInfo;
                    MHReqVideoObj.ApplicationID = MHSReqVideoObj.ApplicationID;
                    MHReqVideoObj.ClientID = MHSReqVideoObj.ClientID;
                    MHReqVideoObj.GroupID = MHSReqVideoObj.GroupID;
                    MHReqVideoObj.IPAddress = MHSReqVideoObj.IPAddress;
                    MHReqVideoObj.JobID = MHSReqVideoObj.JobID;
                    MHReqVideoObj.PortNo = MHSReqVideoObj.PortNo;
                    MHReqVideoObj.WorkPriority = (PriorityLevel)MHSReqVideoObj.WorkPriority;

                    if ((MHReqVideoObj.MetaDataNeeded != null))
                    {
                        BrandSystems.MediaHandler.CustomType.VideoMetaData[] MetaDataNeeded = new BrandSystems.MediaHandler.CustomType.VideoMetaData[MHReqVideoObj.MetaDataNeeded.Length];
                        for (int i = 0; i <= MHReqVideoObj.MetaDataNeeded.Length - 1; i++)
                        {
                            MetaDataNeeded[i] = (BrandSystems.MediaHandler.CustomType.VideoMetaData)MHReqVideoObj.MetaDataNeeded[i];
                        }
                        MHReqVideoObj.MetaDataNeeded = MetaDataNeeded;
                    }
                    ArrayList ThumbnailDestinationPath = new ArrayList(MHSReqVideoObj.ThumbnailDestinationPath);
                    MHReqVideoObj.ThumbnailDestinationPath = ThumbnailDestinationPath;
                    MHReqVideoObj.VideoPreviewEndTime = MHSReqVideoObj.VideoPreviewEndTime;
                    MHReqVideoObj.Format = MHSReqVideoObj.Format;
                    MHReqVideoObj.SourcePath = MHSReqVideoObj.SourcePath;
                    MHReqVideoObj.VideoPreviewStartTime = MHSReqVideoObj.VideoPreviewStartTime;
                    MHReqVideoObj.TaskToPerform = (BrandSystems.MediaHandler.CustomType.Task)MHSReqVideoObj.TaskToPerform;
                    MHReqVideoObj.ThumbnailFormat = MHSReqVideoObj.ThumbnailFormat;
                    ArrayList ThumbnailFrameTimeList = new ArrayList(MHSReqVideoObj.ThumbnailFrameTimeList);
                    MHReqVideoObj.ThumbnailFrameTimeList = ThumbnailFrameTimeList;
                    MHReqVideoObj.VideoPreviewFormat = MHSReqVideoObj.VideoPreviewFormat;
                    MHReqVideoObj.ConverstionDestinationPath = MHSReqVideoObj.ConverstionDestinationPath;
                    MHReqVideoObj.ThumbnailHeight = MHSReqVideoObj.ThumbnailHeight;
                    MHReqVideoObj.ThumbnailWidth = MHSReqVideoObj.ThumbnailWidth;

                    MHReqVideoObj.SpecificTimeOrTimeRange = MHSReqVideoObj.SpecificTimeOrTimeRange;
                    MHReqVideoObj.NumberOfThumbnail = MHSReqVideoObj.NumberOfThumbnail;
                    MHReqVideoObj.MultiThumbnailStartTime = MHSReqVideoObj.MultiThumbnailStartTime;
                    MHReqVideoObj.MultiThumbnailEndTime = MHSReqVideoObj.MultiThumbnailEndTime;
                    MHReqVideoObj.VideoResolutionHeight = MHSReqVideoObj.VideoResolutionHeight;
                    MHReqVideoObj.VideoResolutionWidth = MHSReqVideoObj.VideoResolutionWidth;

                    MHReqVideoObj.EnableGIF = MHSReqVideoObj.EnableGIF;
                    MHReqVideoObj.EnableRAWImage = MHSReqVideoObj.EnableRAWImage;
                    MHReqVideoObj.OutputVideoCodec = MHSReqVideoObj.OutputVideoCodec;
                    MHReqVideoObj.OutputAudioCodec = MHSReqVideoObj.OutputAudioCodec;
                    MHReqVideoObj.AudioFileBitrate = MHSReqVideoObj.AudioFileBitrate;
                    MHReqVideoObj.IsAudioFile = MHSReqVideoObj.IsAudioFile;


                    MHReqVideoObj.Title = MHSReqVideoObj.Title;
                    MHReqVideoObj.Author = MHSReqVideoObj.Author;
                    MHReqVideoObj.Composer = MHSReqVideoObj.Composer;
                    MHReqVideoObj.Album = MHSReqVideoObj.Album;
                    MHReqVideoObj.Year = MHSReqVideoObj.Year;
                    MHReqVideoObj.Track = MHSReqVideoObj.Track;
                    MHReqVideoObj.Comment = MHSReqVideoObj.Comment;
                    MHReqVideoObj.Copyright = MHSReqVideoObj.Copyright;
                    MHReqVideoObj.Description = MHSReqVideoObj.Description;
                    MHReqVideoObj.MetadataUpdateOption = (BrandSystems.MediaHandler.CustomType.VideoMetadataUpdateOption)MHSReqVideoObj.MetadataUpdateOption;

                    MHReqVideoObj.ISs3amazonbucket = MHSReqVideoObj.ISs3amazonbucket;
                    MHReqVideoObj.s3accesskey = MHSReqVideoObj.s3accesskey;
                    MHReqVideoObj.s3securitykey = MHSReqVideoObj.s3securitykey;
                    MHReqVideoObj.s3region = MHSReqVideoObj.s3region;
                    MHReqVideoObj.s3Foldername = MHSReqVideoObj.s3Foldername;
                    MHReqVideoObj.s3Filename = MHSReqVideoObj.s3Filename;
                    MHReqVideoObj.s3FileSourcePath = MHSReqVideoObj.s3FileSourcePath;
                    MHReqVideoObj.s3FileDestinationPath = MHSReqVideoObj.s3FileDestinationPath;


                    return MHReqVideoObj;
                default:

                    break;
            }

            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private BrandSystems.MediaHandler.Output.MediaHandlerResponseObject Cast(MediaHandlerService.MediaHandlerResponseObject arg)
        {

            switch (arg.GetType().Name)
            {
                case "ImageResponseObject":
                    MediaHandlerService.ImageResponseObject MHSResObj = new MediaHandlerService.ImageResponseObject();
                    ImageResponseObject MHResObj = new ImageResponseObject();
                    MHSResObj = (MediaHandlerService.ImageResponseObject)arg;

                    MHResObj.AditionalInfo = MHSResObj.AditionalInfo;
                    MHResObj.ColourSpace = (BrandSystems.MediaHandler.CustomType.ColourSpace)MHSResObj.ColourSpace;
                    MHResObj.Compression = MHSResObj.Compression;
                    MHResObj.Depth = MHSResObj.Depth;
                    MHResObj.DestinationPath = MHSResObj.DestinationPath;
                    MHResObj.ErrorDetail = MHSResObj.ErrorDetail;
                    MHResObj.EXIFProfile = MHSResObj.EXIFProfile;
                    MHResObj.Format = MHSResObj.Format;
                    MHResObj.GroupID = MHSResObj.GroupID;
                    MHResObj.Height = MHSResObj.Height;
                    MHResObj.ICCProfile = MHSResObj.ICCProfile;
                    MHResObj.ImageClass = MHSResObj.ImageClass;
                    MHResObj.ImageType = (BrandSystems.MediaHandler.CustomType.Type)MHSResObj.ImageType;
                    MHResObj.JobID = MHSResObj.JobID;
                    MHResObj.PageNo = MHSResObj.PageNo;
                    MHResObj.Quality = MHSResObj.Quality;
                    MHResObj.Width = MHSResObj.Width;
                    MHResObj.XResolution = MHSResObj.XResolution;
                    MHResObj.YResolution = MHSResObj.YResolution;
                    MHResObj.IPTCProfile = MHSResObj.IPTCProfile;
                    MHResObj.Units = (BrandSystems.MediaHandler.CustomType.Units)MHSResObj.Units;
                    return MHResObj;
                case "PDFResponseObject":

                    break;
                case "OfficeResponseObject":
                    MediaHandlerService.OfficeResponseObject MHSResOfficeObj = new MediaHandlerService.OfficeResponseObject();
                    OfficeResponseObject MHResOfficeObj = new OfficeResponseObject();
                    MHSResOfficeObj = (MediaHandlerService.OfficeResponseObject)arg;

                    MHResOfficeObj.AditionalInfo = MHSResOfficeObj.AditionalInfo;
                    MHResOfficeObj.Author = MHSResOfficeObj.Author;
                    MHResOfficeObj.Category = MHSResOfficeObj.Category;
                    MHResOfficeObj.Comments = MHSResOfficeObj.Comments;
                    MHResOfficeObj.DestinationPath = MHSResOfficeObj.DestinationPath;
                    MHResOfficeObj.ErrorDetail = MHSResOfficeObj.ErrorDetail;
                    MHResOfficeObj.FileCategory = (OfficeFileCategory)MHSResOfficeObj.FileCategory;
                    MHResOfficeObj.FileFormat = MHSResOfficeObj.FileFormat;
                    MHResOfficeObj.GroupID = MHSResOfficeObj.GroupID;
                    MHResOfficeObj.JobID = MHSResOfficeObj.JobID;
                    MHResOfficeObj.Keywords = MHSResOfficeObj.Keywords;
                    MHResOfficeObj.Subject = MHSResOfficeObj.Subject;
                    MHResOfficeObj.Tags = MHSResOfficeObj.Tags;
                    MHResOfficeObj.ThumbnailHeight = MHSResOfficeObj.ThumbnailHeight;
                    MHResOfficeObj.ThumbnailWidth = MHSResOfficeObj.ThumbnailWidth;
                    MHResOfficeObj.Title = MHSResOfficeObj.Title;
                    MHResOfficeObj.TotalCount = MHSResOfficeObj.TotalCount;
                    MHResOfficeObj.IsOpenXMLFile = MHSResOfficeObj.IsOpenXMLFile;


                    return MHResOfficeObj;
                case "VideoResponseObject":

                    MediaHandlerService.VideoResponseObject MHSResVideoObj = new MediaHandlerService.VideoResponseObject();
                    VideoResponseObject MHResVideoObj = new VideoResponseObject();
                    MHSResVideoObj = (MediaHandlerService.VideoResponseObject)arg;

                    MHResVideoObj.ErrorDetail = MHSResVideoObj.ErrorDetail;
                    MHResVideoObj.GroupID = MHSResVideoObj.GroupID;
                    MHResVideoObj.JobID = MHSResVideoObj.JobID;

                    MHResVideoObj.AudioBitrate = MHSResVideoObj.AudioBitrate;
                    MHResVideoObj.AudioChannels = MHSResVideoObj.AudioChannels;
                    MHResVideoObj.AudioSampleRate = MHSResVideoObj.AudioSampleRate;
                    MHResVideoObj.DataRate = MHSResVideoObj.DataRate;
                    //MHResObj.DestinationPath = MHSResObj.DestinationPath
                    MHResVideoObj.FrameHeight = MHSResVideoObj.FrameHeight;
                    MHResVideoObj.FrameWidth = MHSResVideoObj.FrameWidth;
                    MHResVideoObj.FrameRate = MHSResVideoObj.FrameRate;
                    MHResVideoObj.TotalDuration = MHSResVideoObj.TotalDuration;

                    MHResVideoObj.Title = MHSResVideoObj.Title;
                    MHResVideoObj.Author = MHSResVideoObj.Author;
                    MHResVideoObj.Composer = MHSResVideoObj.Composer;
                    MHResVideoObj.Album = MHSResVideoObj.Album;
                    MHResVideoObj.Year = MHSResVideoObj.Year;
                    MHResVideoObj.Track = MHSResVideoObj.Track;
                    MHResVideoObj.Comment = MHSResVideoObj.Comment;
                    MHResVideoObj.Copyright = MHSResVideoObj.Copyright;
                    MHResVideoObj.Description = MHSResVideoObj.Description;
                    MHResVideoObj.MetadataString = MHSResVideoObj.MetadataString;

                    return MHResVideoObj;
                default:

                    break;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private MediaHandlerService.MediaHandlerRequestObject[] Cast(BrandSystems.MediaHandler.Input.MediaHandlerRequestObject[] arg)
        {
            MediaHandlerService.MediaHandlerRequestObject[] MHSReqObj = new MediaHandlerService.MediaHandlerRequestObject[arg.Length];

            for (int i = 0; i <= arg.Length - 1; i++)
            {
                MHSReqObj[i] = Cast(arg[i]);

            }

            return MHSReqObj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private MediaHandlerService.MediaHandlerResponseObject[] Cast(BrandSystems.MediaHandler.Output.MediaHandlerResponseObject[] arg)
        {
            MediaHandlerService.MediaHandlerResponseObject[] MHSResObj = new MediaHandlerService.MediaHandlerResponseObject[arg.Length];

            for (int i = 0; i <= arg.Length - 1; i++)
            {
                MHSResObj[i] = Cast(arg[i]);

            }

            return MHSResObj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private BrandSystems.MediaHandler.Input.MediaHandlerRequestObject[] Cast(MediaHandlerService.MediaHandlerRequestObject[] arg)
        {
            MediaHandlerRequestObject[] MHReqObj = new MediaHandlerRequestObject[arg.Length];

            for (int i = 0; i <= arg.Length - 1; i++)
            {
                MHReqObj[i] = Cast(arg[i]);

            }

            return MHReqObj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private BrandSystems.MediaHandler.Output.MediaHandlerResponseObject[] Cast(MediaHandlerService.MediaHandlerResponseObject[] arg)
        {
            MediaHandlerResponseObject[] MHResObj = new MediaHandlerResponseObject[arg.Length];

            for (int i = 0; i <= arg.Length - 1; i++)
            {
                MHResObj[i] = Cast(arg[i]);

            }

            return MHResObj;
        }

        #endregion      


        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>
        //Public Sub ResetSettings()
        //    Dim StrAppPath As String = _CurrentPath & "\MediaHandlerClient.dll.config"
        //    Dim Value As String = GetFileContents(StrAppPath)
        //    If Not Value Is Nothing Then
        //        ReadXml(Value)
        //    End If
        //End Sub

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        //Private Function GetFileContents(ByVal fullPath As String) As String

        //    Dim StrContents As String = Nothing
        //    Dim ObjReader As IO.StreamReader
        //    Try
        //        ObjReader = New IO.StreamReader(fullPath)
        //        StrContents = ObjReader.ReadToEnd()
        //        ObjReader.Close()
        //        Return StrContents
        //    Catch ex As Exception
        //        Return StrContents
        //    End Try
        //End Function

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserData"></param>
        /// <remarks></remarks>
        //'Private Sub ReadXml(ByVal strUserData As String)
        //'    Dim OXmlUserData As System.Xml.XmlTextReader
        //'    If strUserData.IndexOf("<applicationSettings>") > 0 Then
        //'        strUserData = strUserData.Substring(strUserData.IndexOf("<applicationSettings>"))
        //'        strUserData = strUserData.Replace("</configuration>", "")
        //'    End If
        //'    Dim Name As String = Nothing
        //'    Dim Value As String = Nothing

        //'    OXmlUserData = New System.Xml.XmlTextReader(strUserData, System.Xml.XmlNodeType.Element, New System.Xml.XmlParserContext(Nothing, Nothing, Nothing, System.Xml.XmlSpace.Default))

        //'    Dim StrElementName As String

        //'    While OXmlUserData.Read()
        //'        If OXmlUserData.IsStartElement() Then
        //'            If Not OXmlUserData.IsEmptyElement Then
        //'                StrElementName = OXmlUserData.Name
        //'                If Not StrElementName.Equals("applicationSettings") Then
        //'                    Select Case StrElementName
        //'                        Case "setting"
        //'                            If OXmlUserData.HasAttributes Then
        //'                                While OXmlUserData.MoveToNextAttribute
        //'                                    If OXmlUserData.Name = "name" Then
        //'                                        Name = OXmlUserData.Value
        //'                                        OXmlUserData.Read()
        //'                                        OXmlUserData.Read()
        //'                                        Value = OXmlUserData.ReadString()
        //'                                        My.Settings.Item(Name) = Value

        //'                                        'My.Settings.MySet(Name) = Value
        //'                                    End If
        //'                                End While
        //'                            End If
        //'                    End Select
        //'                End If
        //'            End If
        //'        End If
        //'    End While
        //'End Sub

        #endregion

    

        public string[] GetMimeType()
        {
            try
            {
                return _proxy.GetMimeType();
            }
            catch (Exception ex)
            {
                if (ex.Message == "The operation has timed-out.")
                {
                    //throw new BrandSystems.MediaHandler.MHValidationFailException("Timeout", null, null, null, 27);
                   // throw new MediaHandler.MHValidationFailException.MHValidationFailException("Timeout", null, null, null, 27);
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Timeout", null);
                }
                else
                {
                    //throw new MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null, null, null, 26);
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null);
                }
            }

            return null;

        }

        public string GetPropertyDefault()
        {
            try
            {
                return _proxy.GetPropertyDefault();
            }
            catch (Exception ex)
            {
                if (ex.Message == "The operation has timed-out.")
                {
                    //throw new MediaHandler.MHValidationFailException("Timeout", null, null, null, 27);
                }
                else
                {
                    //throw new MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null, null, null, 26);
                }
            }
            return null;
        }




        public ClassInfo[] GetMediaHandlerObjects()
        {

            ClassInfo[] obj = null;
            int i = 0;
            System.Reflection.Assembly ass = null;
            ass = System.Reflection.Assembly.GetAssembly(new BrandSystems.MediaHandler.CustomType.PriorityLevel().GetType());
            foreach (System.Type item in ass.GetTypes())
            {
                if (item.Namespace == "BrandSystems.MediaHandler.Input" & item.IsClass & !item.IsAbstract)
                {
                    Array.Resize(ref obj, i + 1);
                    ClassInfo objClassInfo = new ClassInfo();
                    objClassInfo.Name = item.Name;

                    PropertyInfo[] properties = null;

                    int j = 0;
                    foreach (System.Reflection.PropertyInfo ProItem in item.GetProperties())
                    {
                        Array.Resize(ref properties, j + 1);
                        PropertyInfo objPropertyInfo = new PropertyInfo();
                        objPropertyInfo.Name = ProItem.Name;
                        objPropertyInfo.Type = ProItem.PropertyType.Name;
                        objPropertyInfo.IsArray = ProItem.PropertyType.IsArray;

                        System.Type EnumInfo = ass.GetType(ProItem.PropertyType.FullName);
                        if ((EnumInfo != null))
                        {
                            objPropertyInfo.IsComplex = true;
                            try
                            {
                                if (!ProItem.PropertyType.IsArray)
                                {
                                    objPropertyInfo.ComplexTypeValue = Enum.GetNames(EnumInfo);

                                }
                                else
                                {
                                    System.Type ComplexInfo = ass.GetType(ProItem.PropertyType.FullName.Replace("[]", ""));

                                    if (ComplexInfo.IsEnum)
                                    {
                                        objPropertyInfo.ComplexTypeValue = Enum.GetNames(ComplexInfo);

                                    }
                                    else
                                    {
                                    }

                                }


                            }
                            catch (Exception ex)
                            {
                            }

                        }

                        properties[j] = objPropertyInfo;
                        j = j + 1;
                    }
                    objClassInfo.PropertyInfo = properties;
                    obj[i] = objClassInfo;
                    i = i + 1;
                }
            }

            return obj;


            return null;
        }


        private MediaHandlerRequestObject SetProperyDefault(MediaHandlerRequestObject requestObject)
        {
            try
            {
                if (PropertyDefault == null)
                    PropertyDefault = XElement.Parse(GetPropertyDefault());
                switch (requestObject.GetType().Name)
                {
                    case "VideoRequestObject":

                        if (((VideoRequestObject)requestObject).ThumbnailFrameTimeList.Count < 1 & ((VideoRequestObject)requestObject).ThumbnailDestinationPath.Count == 1 & ((VideoRequestObject)requestObject).NumberOfThumbnail < 1 & ((VideoRequestObject)requestObject).MultiThumbnailStartTime < 0)
                        {    //need to workes 
                            //XElement element = (from el in PropertyDefault  where el == "FFMpegProcessor" & el == "ThumbnailExtractTimeStamp" el).FirstOrDefault();
                            //((VideoRequestObject)requestObject).ThumbnailFrameTimeList.Add(element.Attribute("PropertyValue").Value);
                        }
                        break;

                }

            }
            catch (Exception ex)
            {
            }
            return requestObject;
        }
        public object GetObjectFromXML(string arg)
        {

            //Dim obj As Object = Nothing

            System.Xml.XmlDocument _doc = new System.Xml.XmlDocument();
            _doc.LoadXml(arg);


            if (_doc.ChildNodes.Count > 0)
            {
                if (_doc.ChildNodes[0].Name == "ImageRequestObject")
                {
                    BrandSystems.MediaHandler.Input.ImageRequestObject imgReq = new BrandSystems.MediaHandler.Input.ImageRequestObject();

                    var _with1 = imgReq;

                    try
                    {
                        _with1.AditionalInfo = _doc.SelectNodes("/ImageRequestObject/AditionalInfo")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ApplicationID = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/ApplicationID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ClientID = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/ClientID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {

                        _with1.ColourSpace = (BrandSystems.MediaHandler.CustomType.ColourSpace)Enum.Parse(new BrandSystems.MediaHandler.CustomType.ColourSpace().GetType(), _doc.SelectNodes("/ImageRequestObject/ColourSpace")[0].InnerText.ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.CropHeight = double.Parse(_doc.SelectNodes("/ImageRequestObject/CropHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.CropLeft = double.Parse(_doc.SelectNodes("/ImageRequestObject/CropLeft")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.CropTop = double.Parse(_doc.SelectNodes("/ImageRequestObject/CropTop")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.CropWidth = double.Parse(_doc.SelectNodes("/ImageRequestObject/CropWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.DestinationPath = _doc.SelectNodes("/ImageRequestObject/DestinationPath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.FlipDirection = (BrandSystems.MediaHandler.CustomType.Direction)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Direction().GetType(), _doc.SelectNodes("/ImageRequestObject/FlipDirection")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.GroupID = new Guid(_doc.SelectNodes("/ImageRequestObject/GroupID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ImageType = (BrandSystems.MediaHandler.CustomType.Type)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Type().GetType(), _doc.SelectNodes("/ImageRequestObject/ImageType")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.IPAddress = _doc.SelectNodes("/ImageRequestObject/IPAddress")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.JobID = new Guid(_doc.SelectNodes("/ImageRequestObject/JobID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        if (_doc.SelectNodes("/ImageRequestObject/MetaDataNeeded").Count > 0)
                        {
                            BrandSystems.MediaHandler.CustomType.MetaData[] MetaData = new BrandSystems.MediaHandler.CustomType.MetaData[_doc.SelectNodes("/ImageRequestObject/MetaDataNeeded").Count + 1];
                            for (int i = 0; i <= _doc.SelectNodes("/ImageRequestObject/MetaDataNeeded").Count - 1; i++)
                            {
                                MetaData[i] = (BrandSystems.MediaHandler.CustomType.MetaData)Enum.Parse(new BrandSystems.MediaHandler.CustomType.MetaData().GetType(), _doc.SelectNodes("/ImageRequestObject/MetaDataNeeded")[i].InnerText);
                            }
                        }

                        //.MetaDataNeeded = _doc.SelectNodes("/ImageRequestObject/MetaDataNeeded")(0).InnerText
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.PageNo = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/PageNo")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.PortNo = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/PortNo")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.Quality = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/Quality")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ResizeHeight = double.Parse(_doc.SelectNodes("/ImageRequestObject/ResizeHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ResizeInPercentage = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/ResizeInPercentage")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ResizeWidth = double.Parse(_doc.SelectNodes("/ImageRequestObject/ResizeWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.RotationAngle = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/RotationAngle")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ScaleHeight = double.Parse(_doc.SelectNodes("/ImageRequestObject/ScaleHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ScaleInPercentage = Int32.Parse(_doc.SelectNodes("/ImageRequestObject/ScaleInPercentage")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.ScaleWidth = double.Parse(_doc.SelectNodes("/ImageRequestObject/ScaleWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.SourcePath = _doc.SelectNodes("/ImageRequestObject/SourcePath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.TaskToPerform = (BrandSystems.MediaHandler.CustomType.Task)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Task().GetType(), _doc.SelectNodes("/ImageRequestObject/TaskToPerform")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.Thumbnail = bool.Parse(_doc.SelectNodes("/ImageRequestObject/Thumbnail")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.Units = (BrandSystems.MediaHandler.CustomType.Units)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Units().GetType(), _doc.SelectNodes("/ImageRequestObject/Units")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.WorkPriority = (BrandSystems.MediaHandler.CustomType.PriorityLevel)Enum.Parse(new BrandSystems.MediaHandler.CustomType.PriorityLevel().GetType(), _doc.SelectNodes("/ImageRequestObject/WorkPriority")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.XResolution = double.Parse(_doc.SelectNodes("/ImageRequestObject/XResolution")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.YResolution = double.Parse(_doc.SelectNodes("/ImageRequestObject/YResolution")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.TopLeftRadius = (int)double.Parse(_doc.SelectNodes("/ImageRequestObject/TopLeftRadius")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.TopRightRadius = (int)double.Parse(_doc.SelectNodes("/ImageRequestObject/TopRightRadius")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.BottomLeftRadius = (int)double.Parse(_doc.SelectNodes("/ImageRequestObject/BottomLeftRadius")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.BottomRightRadius = (int)double.Parse(_doc.SelectNodes("/ImageRequestObject/BottomRightRadius")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with1.CornerColor = double.Parse(_doc.SelectNodes("/ImageRequestObject/CornerColor")[0].InnerText).ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    return imgReq;
                }
                else if (_doc.ChildNodes[0].Name == "OfficeRequestObject")
                {
                    BrandSystems.MediaHandler.Input.OfficeRequestObject OffReq = new BrandSystems.MediaHandler.Input.OfficeRequestObject();
                    var _with2 = OffReq;
                    try
                    {
                        _with2.AditionalInfo = _doc.SelectNodes("/OfficeRequestObject/AditionalInfo")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with2.ApplicationID = Int32.Parse(_doc.SelectNodes("/OfficeRequestObject/ApplicationID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.ClientID = Int32.Parse(_doc.SelectNodes("/OfficeRequestObject/ClientID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.DestinationPath = _doc.SelectNodes("/OfficeRequestObject/DestinationPath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }


                    try
                    {
                        _with2.FileCategory = (BrandSystems.MediaHandler.CustomType.OfficeFileCategory) Int32.Parse(_doc.SelectNodes("/OfficeRequestObject/FileCategory")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }


                    try
                    {
                        _with2.FileFormat = _doc.SelectNodes("/OfficeRequestObject/FileFormat")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.GroupID = new Guid(_doc.SelectNodes("/OfficeRequestObject/GroupID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.IPAddress = _doc.SelectNodes("/OfficeRequestObject/IPAddress")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with2.JobID = new Guid(_doc.SelectNodes("/OfficeRequestObject/JobID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        if (_doc.SelectNodes("/OfficeRequestObject/MetaDataNeeded").Count > 0)
                        {
                            BrandSystems.MediaHandler.CustomType.OfficeMetaData[] OfficeMetaData = new BrandSystems.MediaHandler.CustomType.OfficeMetaData[_doc.SelectNodes("/OfficeRequestObject/MetaDataNeeded").Count + 1];
                            for (int i = 0; i <= _doc.SelectNodes("/OfficeRequestObject/MetaDataNeeded").Count - 1; i++)
                            {
                                OfficeMetaData[i] =(BrandSystems.MediaHandler.CustomType.OfficeMetaData) Enum.Parse(new BrandSystems.MediaHandler.CustomType.OfficeMetaData().GetType(), _doc.SelectNodes("/OfficeRequestObject/MetaDataNeeded")[i].InnerText);
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.PortNo = Int32.Parse(_doc.SelectNodes("/OfficeRequestObject/PortNo")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with2.SourcePath = _doc.SelectNodes("/OfficeRequestObject/SourcePath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with2.TaskToPerform =(BrandSystems.MediaHandler.CustomType.Task)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Task().GetType(),_doc.SelectNodes("/OfficeRequestObject/TaskToPerform")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.ThumbnailFormat = _doc.SelectNodes("/OfficeRequestObject/ThumbnailFormat")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.WorkPriority = (BrandSystems.MediaHandler.CustomType.PriorityLevel)Enum.Parse(new BrandSystems.MediaHandler.CustomType.PriorityLevel().GetType(), _doc.SelectNodes("/OfficeRequestObject/WorkPriority")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.ThumbnailHeight =(int) double.Parse(_doc.SelectNodes("/OfficeRequestObject/ThumbnailHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.ThumbnailWidth = (int)double.Parse(_doc.SelectNodes("/OfficeRequestObject/ThumbnailWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with2.IsOpenXMLFile = bool.Parse(_doc.SelectNodes("/OfficeRequestObject/IsOpenXMLFile")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }



                    return OffReq;

                }
                else if (_doc.ChildNodes[0].Name == "VideoRequestObject")
                {
                    BrandSystems.MediaHandler.Input.VideoRequestObject vidReq = new BrandSystems.MediaHandler.Input.VideoRequestObject();
                    var _with3 = vidReq;
                    try
                    {
                        _with3.AditionalInfo = _doc.SelectNodes("/VideoRequestObject/AditionalInfo")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.SourcePath = _doc.SelectNodes("/VideoRequestObject/SourcePath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        if (_doc.SelectNodes("/VideoRequestObject/ThumbnailDestinationPath").Count > 0)
                        {
                            string[] ThumbnailDestinationPath = new string[_doc.SelectNodes("/VideoRequestObject/ThumbnailDestinationPath").Count + 1];
                            for (int i = 0; i <= _doc.SelectNodes("/VideoRequestObject/ThumbnailDestinationPath").Count - 1; i++)
                            {
                                ThumbnailDestinationPath[i] = _doc.SelectNodes("/VideoRequestObject/ThumbnailDestinationPath")[i].InnerText;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.ApplicationID = Int32.Parse(_doc.SelectNodes("/VideoRequestObject/ApplicationID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.ClientID = Int32.Parse(_doc.SelectNodes("/VideoRequestObject/ClientID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.Format = _doc.SelectNodes("/VideoRequestObject/Format")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.ThumbnailFormat = _doc.SelectNodes("/VideoRequestObject/ThumbnailFormat")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.VideoPreviewFormat = _doc.SelectNodes("/VideoRequestObject/VideoPreviewFormat")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.GroupID = new Guid(_doc.SelectNodes("/VideoRequestObject/GroupID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.IPAddress = _doc.SelectNodes("/VideoRequestObject/IPAddress")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        _with3.JobID = new Guid(_doc.SelectNodes("/VideoRequestObject/JobID")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        if (_doc.SelectNodes("/VideoRequestObject/MetaDataNeeded").Count > 0)
                        {
                            BrandSystems.MediaHandler.CustomType.VideoMetaData[] VideoMetaData = new BrandSystems.MediaHandler.CustomType.VideoMetaData[_doc.SelectNodes("/VideoRequestObject/MetaDataNeeded").Count + 1];
                            for (int i = 0; i <= _doc.SelectNodes("/VideoRequestObject/MetaDataNeeded").Count - 1; i++)
                            {
                                VideoMetaData[i] = (BrandSystems.MediaHandler.CustomType.VideoMetaData)Enum.Parse(new BrandSystems.MediaHandler.CustomType.VideoMetaData().GetType(), _doc.SelectNodes("/VideoRequestObject/MetaDataNeeded")[i].InnerText);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.PortNo = Int32.Parse(_doc.SelectNodes("/VideoRequestObject/PortNo")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.VideoPreviewStartTime = int.Parse(_doc.SelectNodes("/VideoRequestObject/VideoPreviewStartTime")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.VideoPreviewEndTime = int.Parse(_doc.SelectNodes("/VideoRequestObject/VideoPreviewEndTime")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        if (_doc.SelectNodes("/VideoRequestObject/ThumbnailFrameTimeList").Count > 0)
                        {
                            string[] ThumbnailFrameTimeList = new string[_doc.SelectNodes("/VideoRequestObject/ThumbnailFrameTimeList").Count + 1];
                            for (int i = 0; i <= _doc.SelectNodes("/VideoRequestObject/ThumbnailFrameTimeList").Count - 1; i++)
                            {
                                ThumbnailFrameTimeList[i] = _doc.SelectNodes("/VideoRequestObject/ThumbnailFrameTimeList")[i].InnerText;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    //Try
                    //    . = _doc.SelectNodes("/VideoRequestObject/EndTime")(0).InnerText
                    //Catch ex As Exception
                    //End Try
                    try
                    {
                        _with3.TaskToPerform = (BrandSystems.MediaHandler.CustomType.Task)Enum.Parse(new BrandSystems.MediaHandler.CustomType.Task().GetType(), _doc.SelectNodes("/VideoRequestObject/TaskToPerform")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.WorkPriority = (BrandSystems.MediaHandler.CustomType.PriorityLevel)Enum.Parse(new BrandSystems.MediaHandler.CustomType.PriorityLevel().GetType(), _doc.SelectNodes("/VideoRequestObject/WorkPriority")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.ThumbnailHeight = double.Parse(_doc.SelectNodes("/VideoRequestObject/ThumbnailHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.ThumbnailWidth = double.Parse(_doc.SelectNodes("/VideoRequestObject/ThumbnailWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.ConverstionDestinationPath = _doc.SelectNodes("/VideoRequestObject/ConverstionDestinationPath")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }
                    //--------------------------------------
                    try
                    {
                        _with3.Title = _doc.SelectNodes("/VideoRequestObject/Title")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Author = _doc.SelectNodes("/VideoRequestObject/Author")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Composer = _doc.SelectNodes("/VideoRequestObject/Composer")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Album = _doc.SelectNodes("/VideoRequestObject/Album")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Year = int.Parse(_doc.SelectNodes("/VideoRequestObject/Year")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Track = int.Parse(_doc.SelectNodes("/VideoRequestObject/Track")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Comment = _doc.SelectNodes("/VideoRequestObject/Comment")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Copyright = _doc.SelectNodes("/VideoRequestObject/Copyright")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.Description = _doc.SelectNodes("/VideoRequestObject/Description")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.MetadataUpdateOption = (BrandSystems.MediaHandler.CustomType.VideoMetadataUpdateOption)Enum.Parse(new BrandSystems.MediaHandler.CustomType.VideoMetadataUpdateOption().GetType(), _doc.SelectNodes("/VideoRequestObject/MetadataUpdateOption")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }


                    try
                    {
                        _with3.VideoResolutionHeight = int.Parse(_doc.SelectNodes("/VideoRequestObject/VideoResolutionHeight")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.VideoResolutionWidth = int.Parse(_doc.SelectNodes("/VideoRequestObject/VideoResolutionWidth")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.OutputVideoCodec = int.Parse(_doc.SelectNodes("/VideoRequestObject/OutputVideoCodec")[0].InnerText).ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.AudioFileBitrate = int.Parse(_doc.SelectNodes("/VideoRequestObject/AudioBitrate")[0].InnerText);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        _with3.OutputAudioCodec = _doc.SelectNodes("/VideoRequestObject/OutputAudioCodec")[0].InnerText;
                    }
                    catch (Exception ex)
                    {
                    }


                    return vidReq;
                }

            }


            return null;

        }


        public void Ping()
        {
            try
            {
                int ServerStatus = 0;
                if (CallForFirstTime)
                {
                    objProxyForPing.Timeout = 10000;
                    CallForFirstTime = false;
                }
                else
                {
                    objProxyForPing.Timeout = 5000;
                }
                ServerStatus = objProxyForPing.Ping();
                if (ServerStatus > 0)
                {
                    if (ServerStatus == 1)
                    {
                        //new BrandSystems.MediaHandler.MHValidationFailException("hi", null, null, null, 26);
                       // throw new MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null, null, null, 26);
                        throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null);
                    }
                    else
                    {
                        //throw new MediaHandler.MHValidationFailException("Server is running out of memory", null, null, null, 33);
                        throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Server is running out of memory", null);
                    }


                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                CallForFirstTime = true;
                if (ex.Message == "The operation has timed-out.")
                {
                    //throw new MediaHandler.MHValidationFailException("Timeout", null, null, null, 27);
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Timeout", null);
                }
                else
                {
                    //throw new MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null, null, null, 26);
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", null);
                }


            }


        }
    }
  
public class ClassInfo
{

	private string strName;
	public string Name {
		get { return strName; }
		set { strName = value; }
	}


	private PropertyInfo[] objPropertyInfo;
	public PropertyInfo[] PropertyInfo {
		get { return objPropertyInfo; }
		set { objPropertyInfo = value; }
	}


}

public class PropertyInfo
{

	private string strName;
	public string Name {
		get { return strName; }
		set { strName = value; }
	}


	private string strType;
	public string Type {
		get { return strType; }
		set { strType = value; }
	}


	private bool bIsComplex;
	public bool IsComplex {
		get { return bIsComplex; }
		set { bIsComplex = value; }
	}


	private bool bIsArray;
	public bool IsArray {
		get { return bIsArray; }
		set { bIsArray = value; }
	}


	private string[] strComplexTypeValue;
	public string[] ComplexTypeValue {
		get { return strComplexTypeValue; }
		set { strComplexTypeValue = value; }
	}





}

}
