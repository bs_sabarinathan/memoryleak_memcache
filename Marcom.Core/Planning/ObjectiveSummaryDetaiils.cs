﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class ObjectiveSummaryDetails : IObjectiveSummaryDetails
    {
      public  int Id { get; set; }
      public bool ObjectiveStatus { get; set; }
      public int ObjectiveTypeId { get; set; }
      public string ObjectiveTypeName { get; set; }
      public string ObjectiveDescription { get; set; }
      public decimal GlobalBaseLine { get; set; }
      public decimal GlobalTarget { get; set; }
      public decimal PlannedAmount { get; set; }
      public decimal TargetOutcome { get; set; }
      public bool EnableCommnets { get; set; }
      public int Fulfilled { get; set; }
      public int NotFulfilled { get; set; }
      public int LevelsInvolved { get; set; }
      public int ObjectiveUnitId { get; set; }
      public int ObjectiveUnitValue { get; set; }
      public string ObjectiveUnitCaption { get; set; }
      public List<IObjectiveRatingSummary> ObjectiveRating { get; set; }
    }
    internal class ObjectiveRatingSummary : IObjectiveRatingSummary
    {
        public int? RatingId { get; set; }
        public string RatingCaption { get; set; }
        public int? RatinCount { get; set; }
        public int? ObjectiveRatingID { get; set; }
    }
}
