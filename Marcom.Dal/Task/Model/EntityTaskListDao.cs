﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public partial class EntityTaskListDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _Name;
        protected string _Description;
        protected int _Sortorder;
        protected int _EntityID;
        protected int _TaskListID;
        protected int _OnTimeStatus;
        protected string _OnTimeComment;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EntityTaskListDao() { }

        public EntityTaskListDao(int pTaskListID, int pEntityID, string pDescription, string pName, int pSortorder)
        {
            this._TaskListID = pTaskListID;
            this._EntityID = pEntityID;
            this._Description = pDescription;
            this._Name = pName;
            this._Sortorder = pSortorder;

        }

        public EntityTaskListDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual string Name
        {
            get { return _Name; }
            set { _bIsChanged |= (_Name != value); _Name = value; }

        }

        public virtual int EntityID
        {
            get { return _EntityID; }
            set { _bIsChanged |= (_EntityID != value); _EntityID = value; }

        }

        public virtual int Sortorder
        {
            get { return _Sortorder; }
            set { _bIsChanged |= (_Sortorder != value); _Sortorder = value; }

        }

        public virtual string Description
        {
            get { return _Description; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 100 characters");
                _bIsChanged |= (_Description != value);
                _Description = value;
            }

        }

     
        public virtual int TaskListID
        {
            get { return _TaskListID; }
            set { _bIsChanged |= (_TaskListID != value); _TaskListID = value; }

        }

        public virtual int OnTimeStatus
        {
            get { return _OnTimeStatus; }
            set { _bIsChanged |= (_OnTimeStatus != value); _OnTimeStatus = value; }

        }

        public virtual string OnTimeComment
        {
            get { return _OnTimeComment; }
            set { _bIsChanged |= (_OnTimeComment != value); _OnTimeComment = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityTaskListDao = "EntityTaskListDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string EntityID = "EntityID";
            public const string TaskListID = "TaskListID";
            public const string Sortorder = "Sortorder";
            public const string OnTimeStatus = "OnTimeStatus";
            public const string OnTimeComment = "OnTimeComment";
      
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityTaskListDao = "EntityTaskListDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string EntityID = "EntityID";
            public const string TaskListID = "TaskListID";
            public const string Sortorder = "Sortorder";
            public const string OnTimeStatus = "OnTimeStatus";
            public const string OnTimeComment = "OnTimeComment";
        }

        #endregion


    }
}
