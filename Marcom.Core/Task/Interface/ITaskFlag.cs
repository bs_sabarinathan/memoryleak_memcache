﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface ITaskFlag
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string ColorCode
        {
            get;
            set;

        }


        int SortOder
        {
            get;
            set;

        }

        #endregion
    }
}
