﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope) {
        //var renderContext = requestContext.getRenderContext("mui.cms.default");
        //$scope.subview = renderContext.getNextSection();
        //$scope.$on("requestContextChanged", function () {
        //    if (!renderContext.isChangeRelevant()) {
        //        return;
        //    }
        //    $scope.subview = renderContext.getNextSection();
        //});
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.defaultCtrl']"));
        });
    }
    app.controller('mui.cms.defaultCtrl', ['$scope', muiplanningtooldefaultCtrl]);
})(angular, app);