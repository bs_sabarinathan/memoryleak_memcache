﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;

namespace Presentation.Handlers
{
    /// <summary>
    /// Summary description for UploadFiles
    /// </summary>
    public class UploadFiles : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            Decimal chunk = context.Request["chunk"] != null ? Decimal.Parse(context.Request["chunk"]) : 0;
            string fileName = context.Request["name"] != null ? context.Request["name"] : string.Empty;
            string TempName = context.Request["TemplateType"];
            //TODO: change as needed for your application

            string uploadPath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"]) + "UploadedImages\\AttributeGroupImport\\";

           System.IO.Directory.CreateDirectory(uploadPath);
            //HttpContext.Current.Request.Files[0];
            HttpPostedFile fileUpload = HttpContext.Current.Request.Files[0];

            fileName = Guid.NewGuid().ToString() + new FileInfo(fileName).Extension;

            using (var fs = new FileStream(Path.Combine(uploadPath, fileName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);

                fs.Write(buffer, 0, buffer.Length);
            }
            context.Response.Write(fileName);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}