﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

    /// <summary>
    /// EntityRoleUser object for table 'AM_Entity_Role_User'.
    /// </summary>
    internal class TaskMember : ITaskMember
    {
        #region Member Variables

        protected int _id;
        protected int _taskid;
        protected int _roleid;
        protected int _userid;
        protected int _approvalrount;
        protected Boolean _approvalstatus;
      
        #endregion

        #region Constructors
        public TaskMember() { }

        public TaskMember(int pId, int pTaskID, int pRoleid, int pUserid, int pApprovalRount, Boolean pApprovalStatus)
        {
            this._id = pId;
            this._taskid = pTaskID;
            this._roleid = pRoleid;
            this._userid = pUserid;
            this._approvalrount = pApprovalRount;
            this._approvalstatus = pApprovalStatus;
        }

        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int TaskID
        {
            get { return _taskid; }
            set { _taskid = value; }

        }

        public int Roleid
        {
            get { return _roleid; }
            set { _roleid = value; }

        }

        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public int ApprovalRount
        {
            get { return _approvalrount; }
            set { _approvalrount = value; }

        }

        public Boolean ApprovalStatus
        {
            get { return _approvalstatus; }
            set { _approvalstatus = value; }

        }
        public string UserName
        {
            get;
            set;

        }

        public string DepartmentName
        {
            get;
            set;

        }

        public string Role
        {
            get;
            set;

        }

        public string Title
        {
            get;
            set;

        }

        public string UserEmail
        {
            get;
            set;

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskMember castObj = null;
            try
            {
                castObj = (TaskMember)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
