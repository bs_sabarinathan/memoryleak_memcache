﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class EntityTypeAttributeRelationImport
    {

        /// <summary>
        /// EntityPeriod object for table 'MM_EntityTypeAttributeRelation'.
        /// </summary>
        #region Member Variables
        protected int _id;
        protected int _newid;
        protected int _entitytypeid;
        protected int _attributeid;
        protected string _validationid;
        protected int _sortorder;
        protected string _defaultvalue;
        protected bool _inheritfromparent;
        protected bool _isreadonly;
        protected bool _choosefromparentonly;
        protected bool _isvalidationneeded;
        protected string _caption;
        protected bool _issystemdefined;
        protected string _PlaceHolderValue;
        protected int _minvalue;
        protected int _maxvalue;
        protected bool _ishelptextenabled;
        protected string _helptextdescription;
        #endregion

        #region Constructors
        public EntityTypeAttributeRelationImport() { }

        public EntityTypeAttributeRelationImport(int pID, int pEnitytypeId, int pAttributeId, string pValidationId, int pSortOrder, string pDefaultValue, bool pInheritFromParent, bool pIsReadOnly, bool pChooseFromParentOnly, bool pIsValidationNeeded, string pCaption, bool pIsSystemDefined, string pPlaceHolderValue, int pNewId, int MinValue, int MaxValue, bool pIsHelptextEnabled, string pHelptextDecsription)
        {
            this._id = pID;
            this._entitytypeid = pEnitytypeId;
            this._attributeid = pAttributeId;
            this._validationid = pValidationId;
            this._sortorder = pSortOrder;
            this._defaultvalue = pDefaultValue;
            this._inheritfromparent = pInheritFromParent;
            this._isreadonly = pIsReadOnly;
            this._choosefromparentonly = pChooseFromParentOnly;
            this._isvalidationneeded = pIsValidationNeeded;
            this._caption = pCaption;
            this._issystemdefined = pIsSystemDefined;
            this._PlaceHolderValue = pPlaceHolderValue;
            this._newid = pNewId;
            this._minvalue = MinValue;
            this._maxvalue = MaxValue; 
            this._ishelptextenabled = pIsHelptextEnabled;
            this._helptextdescription = pHelptextDecsription;
        }

        public EntityTypeAttributeRelationImport(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }


        public virtual string ValidationID
        {
            get { return _validationid; }
            set { _validationid = value; }
        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        public virtual string DefaultValue
        {
            get { return _defaultvalue; }
            set { _defaultvalue = value; }

        }

        public virtual string PlaceHolderValue
        {
            get { return _PlaceHolderValue; }
            set { _PlaceHolderValue = value; }

        }
        public virtual int MinValue
        {
            get { return _minvalue; }
            set { _minvalue = value; }
        }
        public virtual int MaxValue
        {
            get { return _maxvalue; }
            set { _maxvalue = value; }
        }
        public virtual bool IsHelptextEnabled
        {
            get { return _ishelptextenabled; }
            set { _ishelptextenabled = value; }
        }

        public virtual string HelptextDecsription
        {
            get { return _helptextdescription; }
            set { _helptextdescription = value; }
        }

        public virtual bool InheritFromParent
        {
            get { return _inheritfromparent; }
            set { _inheritfromparent = value; }

        }

        public virtual bool IsReadOnly
        {
            get { return _isreadonly; }
            set { _isreadonly = value; }

        }

        public virtual bool ChooseFromParentOnly
        {
            get { return _choosefromparentonly; }
            set { _choosefromparentonly = value; }

        }

        public virtual bool IsValidationNeeded
        {
            get { return _isvalidationneeded; }
            set { _isvalidationneeded = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual bool IsSystemDefined
        {
            get
            {
                return _issystemdefined;
            }
            set
            {
                _issystemdefined = value;
            }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeAttributeRelation castObj = null;
            try
            {
                castObj = (EntityTypeAttributeRelation)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }

}
