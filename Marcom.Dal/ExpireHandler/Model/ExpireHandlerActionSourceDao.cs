﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.ExpireHandler.Model
{
    public partial class ExpireHandlerActionSourceDao : BaseDao, ICloneable 
    {
        #region Member Variables
		protected int _id;
        protected int _actionID;
        protected int _sourceID;
        protected int _sourceEnityID;
        protected int _sourceFrom;
        protected string _actionexutedays;       
        protected DateTime _actionexutedate;
        protected bool _actionexute;
        protected int _actionownerId;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ExpireHandlerActionSourceDao() {}

        public ExpireHandlerActionSourceDao(int pactionID, int psourceID, int psourceEnityID, int psourceFrom, string pactionexutedays, DateTime pactionexutedate, bool pactionexute, int pactionownerId)
		{
            this._actionID = pactionID;
            this._sourceID = psourceID;
            this._sourceEnityID = psourceEnityID;
            this._sourceFrom = psourceFrom;
            this._actionexutedays = pactionexutedays;
            this._actionexutedate = pactionexutedate;
            this._actionexute = pactionexute;
            this._actionownerId = pactionownerId;
		}

        public ExpireHandlerActionSourceDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int ActionID
        {
            get { return _actionID; }
            set { _actionID = value; }

        }
        public virtual int SourceID
        {
            get { return _sourceID; }
            set { _sourceID = value; }

        }

        public virtual int SourceEnityID
        {
            get { return _sourceEnityID; }
            set { _sourceEnityID = value; }

        }
        public virtual int SourceFrom
        {
            get { return _sourceFrom; }
            set { _sourceFrom = value; }

        }

        public virtual string Actionexutedays
        {
            get { return _actionexutedays; }
            set { _actionexutedays = value; }
        }

        public virtual DateTime Actionexutedate
        {
            get { return _actionexutedate; }
            set { _actionexutedate = value; }
        }

        public virtual bool Actionexute
        {
            get { return _actionexute; }
            set { _actionexute = value; }
        }

        public virtual int ActionownerId
        {
            get { return _actionownerId; }
            set { _actionownerId = value; }

        }
		
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string ExpireHandlerActionSourceDao = "ExpireHandlerActionSourceDao";			
			public const string Id = "Id";
            public const string ActionID = "ActionID";
            public const string SourceID = "SourceID";
            public const string SourceEnityID = "SourceEnityID";
            public const string SourceFrom = "SourceFrom";
            public const string Actionexutedays = "Actionexutedays";
            public const string Actionexutedate = "Actionexutedate";
            public const string Actionexute = "Actionexute";
            public const string ActionownerId = "ActionownerId";
           
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string ExpireHandlerActionSourceDao = "ExpireHandlerActionSourceDao";
            public const string Id = "Id";
            public const string ActionID = "ActionID";
            public const string SourceID = "SourceID";
            public const string SourceEnityID = "SourceEnityID";
            public const string SourceFrom = "SourceFrom";
            public const string Actionexutedays = "Actionexutedays";
            public const string Actionexutedate = "Actionexutedate";
            public const string Actionexute = "Actionexute";
            public const string ActionownerId = "ActionownerId";	
		}

		#endregion
		
    }
}
