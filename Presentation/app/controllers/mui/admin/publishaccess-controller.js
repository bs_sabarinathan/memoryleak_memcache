﻿(function (ng, app) {
    "use strict";
    function muiadminpublishaccessCtrl($scope, $location, $resource, $timeout, $compile, $translate, AdminService) {           
        $scope.publishedobj = { "AccessEnable": [] };
        $scope.getData = function () {
            AdminService.Getpublishaccess().then(function (res) {
                $scope.getroles = res.Response;              
            });
        };
        $scope.getData();          
        $scope.GetChckedView = function (item) {          
            var checkbox = event.target;
            var res1 = $.grep($scope.getroles, function (e) {
                return e.ID == item.ID;
            });
            if (res1.length > 0) {
                var tempres = $.grep($scope.publishedobj.AccessEnable, function (e) {
                    return e.ID == item.ID;
                });
                if (res1[0].AccessPermission == true) {
                    $timeout(function () {                      
                        res1[0].AccessPermission = false;
                    }, 10);
                    if (tempres.length > 0) {
                        tempres[0].AccessPermission = false;                       
                    }
                    else
                        $scope.publishedobj.AccessEnable.push({ AccessPermission: false, Role: item.ID});
                }
                else {
                    $timeout(function () {
                        res1[0].AccessPermission = true;                       
                    }, 10);
                    if (tempres.length > 0) {
                        tempres[0].AccessPermission = true;                      
                    }
                    else
                        $scope.publishedobj.AccessEnable.push({ AccessPermission: true, Role: item.ID });
                }
            }
        }
        $scope.updatepublishaccess = function () {
            var updatepublishaccessdtls = $scope.publishedobj;
            AdminService.Updatepublishaccess(updatepublishaccessdtls).then(function (getresult) {
                if (getresult.Response == true || getresult.Response == null) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    var res = $.grep($scope.getroles, function (e) {
                        return e.ID == ID;
                    });
                    if (res.length > 0) {
                        res1[0].AccessPermission = newvalue;
                    }
                }
                else {
                    NotifyError($translate.instant('LanguageContents.Res_4941.Caption'));
                }
            });
        }
    }
    app.controller("mui.admin.publishaccessCtrl", ['$scope', '$location', '$resource', '$timeout', '$compile', '$translate', 'AdminService', muiadminpublishaccessCtrl]);
})(angular, app);