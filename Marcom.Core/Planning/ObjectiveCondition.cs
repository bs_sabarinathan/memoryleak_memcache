using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// ObjectiveCondition object for table 'PM_ObjectiveCondition'.
	/// </summary>
	
	internal class ObjectiveCondition : IObjectiveCondition 
	{
		#region Member Variables

		protected int _id;
		protected int _objectiveid;
		protected int? _entitytypeid;
		protected int? _attributeid;
		protected int _conditiontype;
		protected int _sortorder;
        protected int _attributelevel;
		
		
		#endregion
		
		#region Constructors
		public ObjectiveCondition() {}
			
		public ObjectiveCondition(int pId, int pObjectiveid, int? pEntityTypeid, int? pAttributeid, int pConditionType, int pSortOrder,int pAttributeLevel)
		{
			this._id = pId; 
			this._objectiveid = pObjectiveid; 
			this._entitytypeid = pEntityTypeid; 
			this._attributeid = pAttributeid; 
			this._conditiontype = pConditionType; 
			this._sortorder = pSortOrder;
            this._attributelevel = pAttributeLevel;
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Objectiveid
		{
			get { return _objectiveid; }
			set { _objectiveid = value; }
			
		}
		
		public int? EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
		
		public int? Attributeid
		{
			get { return _attributeid; }
			set { _attributeid = value; }
			
		}
		
		public int ConditionType
		{
			get { return _conditiontype; }
			set { _conditiontype = value; }
			
		}
		
		public int SortOrder
		{
			get { return _sortorder; }
			set 
			{
			  _sortorder = value; 
			}
			
		}

        public int AttributeLevel
        {
            get { return _attributelevel; }
            set { _attributelevel = value; }
        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			ObjectiveCondition castObj = null;
			try
			{
				castObj = (ObjectiveCondition)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
}
