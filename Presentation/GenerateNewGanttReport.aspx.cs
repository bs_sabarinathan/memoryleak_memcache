﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using System.IO;
using System.Collections;
using System.Data;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Web;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using System.Globalization;
using Presentation.Utility;

namespace Presentation
{
    public partial class GenerateNewGanttReport : System.Web.UI.Page
    {
        private int RowNo = 7;
        private int ColumnNo = 1;
        private int LastColumnNo = 0;
        private int tempcolmno = 19;
        private System.DateTime CalenderStartDate = new System.DateTime(System.DateTime.Now.Year - 1, 1, 1);
        private System.DateTime CalenderEndDate = new System.DateTime(System.DateTime.Now.Year + 1, 12, 31);


        private bool IsObjectiveOrCostCenterPresent = false;
        private int FilterID = 0;
        private string ListOfEntityID = "";
        private int TypeID = 0;
        private string GlobalAccess;


        protected void Page_Load(object sender, EventArgs e)
        {

            TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
            Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
            IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);


            string FileID = Request.QueryString["xid"];

            try
            {

                if (FileID == null)
                {


                    string something = HttpContext.Current.Request.QueryString["EID"];


                    ListOfEntityID = Request.QueryString["EID"];

                    IList<BrandSystems.Marcom.Core.Planning.Gantt> GV = new List<BrandSystems.Marcom.Core.Planning.Gantt>();

                    int intUserID = 0;

                    intUserID = 12;

                    string NewGuid = Guid.NewGuid().ToString();

                    dynamic fullpath = Server.MapPath("Files/ReportFiles/GanttViewReportTemp/") + intUserID + "_" + NewGuid + ".xlsx";

                    FileInfo newFile = new FileInfo(fullpath);

                    ExcelPackage pck = new ExcelPackage(newFile);
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Quaterly View");


                    List<string> AttributeList = new List<string>();
                    GenerateTitle(ws, 1);


                    //AttributeList.Add("ID#") ' need to check with prbhu

                    // need to check with prbhu
                    AttributeList.Add("Type");
                    AttributeList.Add("ID#");
                    AttributeList.Add("Entity Name");
                    AttributeList.Add("Owner");
                    AttributeList.Add("Start/End Date");
                    AttributeList.Add("Country");
                    AttributeList.Add("Budget");
                    AttributeList.Add("End customer size");
                    AttributeList.Add("End customer vertical");

                    AttributeList.Add("Sales Channel");
                    AttributeList.Add("Strategy");
                    AttributeList.Add("Offer");
                    AttributeList.Add("Sponsring");
                    AttributeList.Add("Ringi");
                    AttributeList.Add("Planned budget");
                    AttributeList.Add("Approved allocations");
                    AttributeList.Add("Committed");
                    AttributeList.Add("Spent");
                    GenerateDynamicColumn(ws, AttributeList);

                    LastColumnNo = GenerateMonthlyViewHeader(ws, ColumnNo);

                    GV = managers.PlanningManager.getReportData(ListOfEntityID);
                    GenerateGanttView(ws, GV);
                    int check = mergeQuarter(ws, 18);
                    ws.View.ShowGridLines = false;


                    pck.Workbook.Properties.Title = "Quaterly Gantt View";
                    pck.Workbook.Properties.Author = "Marcom Plarform";
                    pck.Workbook.Properties.Subject = "Quaterly Gantt View";
                    pck.Workbook.Properties.Keywords = "Quaterly Gantt View";

                    pck.Save();

                    string strFriendlyName = "Gantt-view-(Monthly)-" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";

                    dynamic name = System.IO.Path.GetFileName(fullpath);
                    name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
                    dynamic ext = System.IO.Path.GetExtension(fullpath);
                    string type = "";
                    type = "application/vnd.ms-excel";
                    Response.AppendHeader("content-disposition", "attachment; filename=\"" + strFriendlyName + "\"");
                    if (!string.IsNullOrEmpty(type))
                    {
                        Response.ContentType = type;
                        Response.WriteFile(fullpath);
                        Response.End();
                    }
                }

            }
            catch (Exception ex)
            {
                //Log("Page_Load Exception", ex.Message + Constants.vbNewLine + ex.StackTrace);
            }
        }
        protected int HexStrToBase10Int(string hex)
        {
            int base10value = 0;


            try
            {
                return (Convert.ToInt32(hex, 16));
            }
            catch
            { }


            return base10value;
        }
        public bool GenerateTitle(ExcelWorksheet ws, int StartColumnNo)
        {
            ws.Row(2).Height = 35;



            var _with1 = ws.Cells[1, 1];

            _with1.Value = "Gantt view Quaterly report";

            _with1.Style.Font.Name = "Arial";
            _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(64, 64, 64));
            _with1.Style.Font.Size = 20;



            return true;
        }
        public bool GenerateDynamicColumn(ExcelWorksheet ws, List<string> ColumnName)
        {

            foreach (var item in ColumnName)
            {

                var _with1 = ws.Cells[6, ColumnNo, 6, ColumnNo];
                _with1.Merge = true;
                _with1.Value = item.ToString();
                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));

                _with1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Font.Name = "Arial";
                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 11;
                _with1.Style.Font.Bold = true;
                _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                if (ColumnNo == 1)
                {
                    ws.Column(ColumnNo).Width = 18;
                }
                else if (ColumnNo == 2)
                {
                    ws.Column(ColumnNo).Width = 6;
                }
                else if (ColumnNo == 3)
                {
                    ws.Column(ColumnNo).Width = 31;
                }
                else if (ColumnNo == 4)
                {
                    ws.Column(ColumnNo).Width = 18;
                }
                else if (ColumnNo == 5)
                {
                    ws.Column(ColumnNo).Width = 18;
                }

                else
                {
                    ws.Column(ColumnNo).Width = 18;
                }

                ColumnNo = ColumnNo + 1;
            }



            return true;
        }
        public int mergeQuarter(ExcelWorksheet ws, int StartColumnNo)
        {

            try
            {

                DateTime startDate = new DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day);
                DateTime stopDate = new DateTime(CalenderEndDate.Year, CalenderEndDate.Month, CalenderEndDate.Day);
                int interval = 1;

                int isoddcolor = 0;

                int startq = 18;
                int endq = 18;

                DateTime dateTime = startDate;
                bool StartMonthEnd = true;


                while (dateTime <= stopDate)
                {

                    try
                    {
                        if (ws.Cells[3, tempcolmno - 1].Value == ws.Cells[3, tempcolmno].Value)
                        {
                            endq = tempcolmno;
                        }
                        else
                        {
                            var _with1 = ws.Cells[3, startq, 3, endq];

                            _with1.Merge = true;
                            _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            if (isoddcolor == 0)
                            {
                                _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
                                isoddcolor = 1;
                            }
                            else
                            {
                                _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                                isoddcolor = 0;
                            }

                            startq = endq + 1;
                        }



                    }
                    catch (Exception ex)
                    {
                    }

                    dateTime += TimeSpan.FromDays(interval);
                    tempcolmno = tempcolmno + 1;
                }

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GenerateMonthlyViewHeader(ExcelWorksheet ws, int StartColumnNo)
        {
            int Column = StartColumnNo;
            int StartColumn = Column;
            ws.Row(2).Height = 12;
            //ws.Row(3).Height = 10
            //ws.Row(4).Height = 10
            ws.Row(3).Height = 12;
            ws.Row(4).Height = 12;
            ws.Row(5).Height = 20;
            ws.Row(6).Height = 20;

            int IsColorOdd = 0;
            Calendar myCalendar = new GregorianCalendar();

            DateTime startDate = new DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day);
            DateTime stopDate = new DateTime(CalenderEndDate.Year, CalenderEndDate.Month, CalenderEndDate.Day);
            int interval = 1;

            DateTime dateTime = startDate;
            bool StartMonthEnd = true;
            int DaysInMonth = System.DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
            bool OnlyForFirstMonth = true;

            int endweeknum = 0;
            endweeknum = myCalendar.GetWeekOfYear(stopDate, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);


            while (dateTime <= stopDate)
            {
                //With ws.Cells[3, Column)
                var _with1 = ws.Cells[5, Column];
                int weeknum = 0;
                //  Dim WeekNumber As Integer = DatePart(DateInterval.WeekOfYear, dateTime, FirstDayOfWeek.Monday, FirstWeekOfYear.System)
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int WeekNumber = ciCurr.Calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);

                // int WeekNumber = myCalendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                //weeknum = Format(DatePart(DateInterval.WeekOfYear, New Date(dateTime.Year, dateTime.Month, dateTime.Day)))
                weeknum = WeekNumber;
                _with1.Value = weeknum;
                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));
                _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));
                _with1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                _with1.Style.Font.Name = "arial";
                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 8;
                _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                _with1.Merge = false;






                //With ws.Cells[4, Column)
                var _with2 = ws.Cells[6, Column];
                int wknum = 0;

                wknum = ciCurr.Calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);


                //wknum = String.Format(DatePart(DateInterval.WeekOfYear, new System.DateTime(dateTime.Year, dateTime.Month, dateTime.Day)));

                int Days = dateTime.DayOfWeek - CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
                Days = (6 + Days) % 7;

                dynamic weekStart = dateTime.AddDays(-Days);
                int currentweeknum = 0;
                currentweeknum = ciCurr.Calendar.GetWeekOfYear(weekStart, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                //currentweeknum = String.Format(Date(DateInterval.WeekOfYear, new System.DateTime(weekStart.Year, weekStart.Month, weekStart.Day)));
                //Dim weekStart = dateTime
                dynamic weekEnd = weekStart.AddDays(6);
                if (endweeknum == currentweeknum)
                {
                    //.Value = dateTime.Date.ToString("dd") + "/" + dateTime.Month.ToString() + " - " + weekEnd.Date.ToString("dd") + "/" + weekEnd.Month.ToString()
                    _with2.Value = dateTime.Date.ToString("dd") + "-" + dateTime.ToString("MMM");
                }
                else
                {
                    //.Value = dateTime.Date.ToString("dd") + "/" + dateTime.Month.ToString() + " - " + weekEnd.Date.ToString("dd") + "/" + weekEnd.Month.ToString()
                    _with2.Value = dateTime.Date.ToString("dd") + "-" + dateTime.ToString("MMM");

                }


                //.Value = New Date(dateTime.Year, dateTime.Month, dateTime.Day)
                _with2.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                _with2.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                _with2.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                _with2.Style.Font.Name = "arial";
                _with2.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with2.Style.Font.Size = 6;
                _with2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //.Style.Numberformat.Format = "dd / mm - dd / mm"


                int CurrentMonthinDays = System.DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
                DateTime EndMonthDate = new DateTime(dateTime.Year, dateTime.Month, CurrentMonthinDays);


                int q = 0;
                q = ((dateTime.Month - 1) / 3) + 1;


                dateTime += TimeSpan.FromDays(interval);
                Column = Column + 1;


                if (((EndMonthDate == dateTime | stopDate == dateTime | startDate == dateTime) & (stopDate >= dateTime)))
                {
                    if ((stopDate == dateTime) & EndMonthDate != dateTime)
                    {
                        CurrentMonthinDays = stopDate.Day;
                    }

                    if ((OnlyForFirstMonth == true))
                    {
                        if ((startDate.Day != 1 & startDate.Month == dateTime.Month & startDate.Year == dateTime.Year))
                        {
                            CurrentMonthinDays = (CurrentMonthinDays - startDate.Day + 1);
                            OnlyForFirstMonth = false;
                        }
                    }



                    //With ws.Cells[2, Column - (CurrentMonthinDays - 1), 2, Column)
                    var _with3 = ws.Cells[4, Column - (CurrentMonthinDays - 1), 4, Column];
                    _with3.Merge = true;
                    if (CurrentMonthinDays > 5)
                    {
                        _with3.Value = new System.DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
                    }
                    _with3.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                    _with3.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                    _with3.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                    _with3.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));



                    _with3.Style.Font.Name = "arial";
                    _with3.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                    _with3.Style.Font.Size = 10;
                    _with3.Style.Font.Bold = false;




                    if ((CurrentMonthinDays < 15))
                    {
                        if ((dateTime.Month == CalenderEndDate.Month))
                        {
                            _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                        else
                        {
                            _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }


                    }
                    else
                    {
                        _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }


                    // .Style.HorizontalAlignment = ExcelHorizontalAlignment.Center



                    _with3.Style.Numberformat.Format = "mmm-yy";
                    //---Background generating based on quarter

                    _with3.Style.Numberformat.Format = "mmm-yy";


                    if (IsColorOdd == 0)
                    {
                        _with3.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with3.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
                        IsColorOdd = 1;
                    }
                    else
                    {
                        _with3.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with3.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                        IsColorOdd = 0;
                    }




                    ///''''''''''''''''''''''''''''''''''''''''''''''''''


                    var _with4 = ws.Cells[3, Column - (CurrentMonthinDays - 1), 3, Column];
                    //.Merge = True
                    string msg = "";
                    //If CurrentMonthinDays > 5 Then
                    //.Value = New Date(dateTime.Year, dateTime.Month, dateTime.Day)
                    q = ((dateTime.Month - 1) / 3) + 1;
                    _with4.Value = "Q" + q;
                    msg = "Q" + q;
                    //End If
                    _with4.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    _with4.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));
                    _with4.Style.WrapText = true;
                    _with4.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    _with4.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                    _with4.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    _with4.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));



                    _with4.Style.Font.Name = "arial";
                    _with4.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                    _with4.Style.Font.Size = 10;
                    _with4.Style.Font.Bold = false;




                    if ((CurrentMonthinDays < 15))
                    {
                        if ((dateTime.Month == CalenderEndDate.Month))
                        {
                            _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                        else
                        {
                            _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }



                    }
                    else
                    {
                        _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }


                    ///''''''''''''''''''''''''''''''''''''''''''''''''''''

                }
                else
                {
                    // If they select Last day of the month in Start Date. this will get executed. 

                    if ((startDate == EndMonthDate))
                    {

                        //With ws.Cells[2, Column - 1, 2, Column - 1)
                        var _with5 = ws.Cells[4, Column - 1, 4, Column - 1];
                        _with5.Merge = true;

                        _with5.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        _with5.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                        _with5.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with5.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                        _with5.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with5.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                        _with5.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with5.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));



                        _with5.Style.Font.Name = "arial";
                        _with5.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                        _with5.Style.Font.Size = 10;
                        _with5.Style.Font.Bold = false;
                        _with5.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        _with5.Style.Numberformat.Format = "mmm-yy";

                        if (IsColorOdd == 0)
                        {
                            _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
                            IsColorOdd = 1;
                        }
                        else
                        {
                            _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                            IsColorOdd = 0;
                        }




                        ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        var _with6 = ws.Cells[3, Column - 1, 3, Column - 1];
                        string msg = "";
                        q = ((dateTime.Month - 1) / 3) + 1;
                        _with6.Value = "Q" + q;
                        msg = "Q" + q;
                        //End If
                        _with6.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));
                        _with6.Style.WrapText = true;
                        _with6.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));

                        _with6.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(0, 0, 0));



                        _with6.Style.Font.Name = "arial";
                        _with6.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                        _with6.Style.Font.Size = 10;
                        _with6.Style.Font.Bold = false;


                        ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    }
                }


            }

            var _with7 = ws.Column(StartColumn);
            _with7.ColumnMax = Column - 1;
            _with7.Width = 3.5;


            return Column - 1;
        }


        public int GenerateGanttView(ExcelWorksheet ws, IList<BrandSystems.Marcom.Core.Planning.Gantt> Data)
        {


            //code align



            foreach (var Item in Data)
            {

                var _with1 = ws.Cells[RowNo, 1];




                //.Value = ETResult.ToList()(0).FriendlyName
                _with1.Value = Item.TypeName;

                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));




                if (Item.Level == 0)
                {
                    _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                }

                _with1.Style.Font.Name = "arial";

                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 10;

                _with1.Style.Font.Name = "arial";
                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 10;

                _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                int StartDynColNo = 2;


                var _with2 = ws.Cells[RowNo, StartDynColNo];
                //.Value = Item.Name
                _with2.Value = Item.ID;
                _with2.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with2.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with2.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with2.Style.Font.Name = "arial";
                _with2.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with2.Style.Font.Size = 10;


                _with2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with2.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                StartDynColNo = StartDynColNo + 1;



                var _with3 = ws.Cells[RowNo, StartDynColNo];

                _with3.Value = HttpUtility.HtmlDecode(Item.Name);

                _with3.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with3.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with3.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with3.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with3.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with3.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with3.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with3.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with3.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with3.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with3.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with3.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with3.Style.Font.Name = "arial";
                _with3.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with3.Style.Font.Size = 10;


                _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with3.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with4 = ws.Cells[RowNo, StartDynColNo];

                _with4.Value = Item.Owner;

                _with4.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with4.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with4.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with4.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with4.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with4.Style.Font.Name = "arial";
                _with4.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with4.Style.Font.Size = 10;


                _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with4.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with5 = ws.Cells[RowNo, StartDynColNo];

                _with5.Value = Item.StartEndForDate;

                _with5.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with5.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with5.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with5.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with5.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with5.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with5.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with5.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with5.Style.Font.Name = "arial";
                _with5.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with5.Style.Font.Size = 10;


                _with5.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with5.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with6 = ws.Cells[RowNo, StartDynColNo];

                _with6.Value = HttpUtility.HtmlDecode(Item.Country);

                _with6.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with6.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with6.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with6.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with6.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with6.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with6.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with6.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with6.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with6.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with6.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with6.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with6.Style.Font.Name = "arial";
                _with6.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with6.Style.Font.Size = 10;


                _with6.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with6.Style.VerticalAlignment = ExcelVerticalAlignment.Center;



                StartDynColNo = StartDynColNo + 1;

                //remaing extra column added

                var _with7 = ws.Cells[RowNo, StartDynColNo];

                _with7.Value = Item.Budget;

                _with7.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with7.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with7.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with7.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with7.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with7.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with7.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with7.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                if (IsObjectiveOrCostCenterPresent)
                {
                    if (Item.TypeID == 21 | Item.TypeID == 48)
                    {
                        _with7.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with7.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }
                else
                {
                    if (Item.TypeID == 22)
                    {
                        _with7.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with7.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }
                }

                _with7.Style.Font.Name = "arial";
                _with7.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with7.Style.Font.Size = 10;


                _with7.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with7.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                //Dheeraj going to paste this //

                var _with8 = ws.Cells[RowNo, StartDynColNo];

                _with8.Value = Item.EndCustomerSize;

                _with8.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with8.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with8.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with8.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with8.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with8.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with8.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with8.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with8.Style.Font.Name = "arial";
                _with8.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with8.Style.Font.Size = 10;


                _with8.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with8.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with9 = ws.Cells[RowNo, StartDynColNo];

                _with9.Value = Item.EndCustomerVertical;

                _with9.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with9.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with9.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with9.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with9.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with9.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with9.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with9.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with9.Style.Font.Name = "arial";
                _with9.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with9.Style.Font.Size = 10;


                _with9.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with9.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;
                var _with10 = ws.Cells[RowNo, StartDynColNo];

                _with10.Value = Item.Saleschannel;

                _with10.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with10.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with10.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with10.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with10.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with10.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with10.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with10.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with10.Style.Font.Name = "arial";
                _with10.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with10.Style.Font.Size = 10;


                _with10.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with10.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with11 = ws.Cells[RowNo, StartDynColNo];

                _with11.Value = Item.Strategy;

                _with11.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with11.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with11.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with11.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with11.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with11.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with11.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with11.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with11.Style.Font.Name = "arial";
                _with11.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with11.Style.Font.Size = 10;


                _with11.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with11.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with12 = ws.Cells[RowNo, StartDynColNo];

                _with12.Value = Item.Offer;

                _with12.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Font.Name = "arial";
                _with12.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with12.Style.Font.Size = 10;


                _with12.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with12.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with13 = ws.Cells[RowNo, StartDynColNo];

                _with13.Value = Item.Sponsoring;

                _with13.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with13.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with13.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with13.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with13.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with13.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with13.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with13.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with13.Style.Font.Name = "arial";
                _with13.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with13.Style.Font.Size = 10;


                _with13.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with13.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with14 = ws.Cells[RowNo, StartDynColNo];

                _with14.Value = Item.Ringi;

                _with14.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with14.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with14.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with14.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with14.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with14.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with14.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with14.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with14.Style.Font.Name = "arial";
                _with14.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with14.Style.Font.Size = 10;


                _with14.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with14.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with15 = ws.Cells[RowNo, StartDynColNo];

                _with15.Value = Item.PlannedBudget;

                _with15.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with15.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with15.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with15.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with15.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with15.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with15.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with15.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with15.Style.Font.Name = "arial";
                _with15.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with15.Style.Font.Size = 10;


                _with15.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with15.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with16 = ws.Cells[RowNo, StartDynColNo];

                _with16.Value = Item.ApprovedAllocations;

                _with16.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with16.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with16.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with16.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with16.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with16.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with16.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with16.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with16.Style.Font.Name = "arial";
                _with16.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with16.Style.Font.Size = 10;


                _with16.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with16.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with17 = ws.Cells[RowNo, StartDynColNo];

                _with17.Value = Item.Commited;

                _with17.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with17.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with17.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with17.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with17.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with17.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with17.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with17.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with17.Style.Font.Name = "arial";
                _with17.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with17.Style.Font.Size = 10;


                _with17.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with17.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with18 = ws.Cells[RowNo, StartDynColNo];

                _with18.Value = Item.Spent;

                _with18.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with18.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with18.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with18.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with18.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with18.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with18.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with18.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with18.Style.Font.Name = "arial";
                _with18.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with18.Style.Font.Size = 10;


                _with18.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                _with18.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;
                //Dheerak going to End Here //
                if ((Item.ParentID == 0))
                {
                    var _with31 = ws.Cells[RowNo, ColumnNo, RowNo, LastColumnNo];
                    _with31.Merge = true;
                    _with31.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    _with31.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with31.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    _with31.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with31.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    _with31.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    if (IsObjectiveOrCostCenterPresent)
                    {
                        if (Item.TypeID == 21 | Item.TypeID == 48)
                        {
                            _with31.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with31.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                        }
                    }
                    else
                    {
                        if (Item.TypeID == 22)
                        {
                            _with31.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with31.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                        }
                    }

                    _with31.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with31.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));

                    int StartColumnNo = ColumnNo;
                    int EndColumnNo = ColumnNo;
                    switch (new System.DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day).DayOfWeek)
                    {
                        // need to take StartDate

                        case System.DayOfWeek.Monday:
                            EndColumnNo = EndColumnNo + 6;
                            break;
                        case System.DayOfWeek.Tuesday:
                            EndColumnNo = EndColumnNo + 5;
                            break;
                        case System.DayOfWeek.Wednesday:
                            EndColumnNo = EndColumnNo + 4;
                            break;
                        case System.DayOfWeek.Thursday:
                            EndColumnNo = EndColumnNo + 3;
                            break;
                        case System.DayOfWeek.Friday:
                            EndColumnNo = EndColumnNo + 2;
                            break;
                        case System.DayOfWeek.Saturday:
                            EndColumnNo = EndColumnNo + 1;
                            break;
                        case System.DayOfWeek.Sunday:
                            EndColumnNo = EndColumnNo + 0;
                            break;
                    }

                    var _with32 = ws.Cells[5, StartColumnNo, 5, EndColumnNo];
                    _with32.Merge = true;
                    _with32.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    _with32.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    var _with33 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                    _with33.Merge = true;
                    _with33.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    _with33.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    bool IsOdd = true;

                    while (StartColumnNo <= LastColumnNo)
                    {


                        var _with34 = ws.Cells[RowNo, StartColumnNo, RowNo, EndColumnNo];
                        // .Style.Border.Left.Style = ExcelBorderStyle.Thin
                        //.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165))
                        if (EndColumnNo - StartColumnNo <= 1)
                        {
                            _with34.AutoFitColumns(3.0);
                        }
                        else
                        {
                            _with34.AutoFitColumns(1.0);
                        }
                        //.Style.Border.Right.Style = ExcelBorderStyle.Thin
                        //.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165))

                        _with34.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with34.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with34.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with34.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        //.Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.FromArgb(165, 165, 165))

                        if (IsOdd)
                        {
                            _with34.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with34.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                            // .Merge = True




                            var _with35 = ws.Cells[5, StartColumnNo, 5, EndColumnNo];
                            _with35.Merge = true;
                            _with35.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with35.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            var _with36 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                            _with36.Merge = true;
                            _with36.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with36.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            IsOdd = false;

                        }
                        else
                        {

                            var _with37 = ws.Cells[5, StartColumnNo, 5, EndColumnNo];
                            _with37.Merge = true;
                            _with37.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with37.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            var _with38 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                            _with38.Merge = true;
                            _with38.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with38.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            IsOdd = true;
                            //.Merge = True
                        }


                        var _with39 = ws.Cells[RowNo, EndColumnNo, RowNo, EndColumnNo];
                        _with39.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with39.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        StartColumnNo = EndColumnNo + 1;
                        EndColumnNo = StartColumnNo + 6;

                        if (EndColumnNo > LastColumnNo)
                        {
                            EndColumnNo = LastColumnNo;
                        }
                    }


                }
                else
                {

                    int StartColumnNo = ColumnNo;
                    int EndColumnNo = ColumnNo;


                    switch (new System.DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day).DayOfWeek)
                    {
                        // need to take StartDate

                        case System.DayOfWeek.Monday:
                            EndColumnNo = EndColumnNo + 6;
                            break;
                        case System.DayOfWeek.Tuesday:
                            EndColumnNo = EndColumnNo + 5;
                            break;
                        case System.DayOfWeek.Wednesday:
                            EndColumnNo = EndColumnNo + 4;
                            break;
                        case System.DayOfWeek.Thursday:
                            EndColumnNo = EndColumnNo + 3;
                            break;
                        case System.DayOfWeek.Friday:
                            EndColumnNo = EndColumnNo + 2;
                            break;
                        case System.DayOfWeek.Saturday:
                            EndColumnNo = EndColumnNo + 1;
                            break;
                        case System.DayOfWeek.Sunday:
                            EndColumnNo = EndColumnNo + 0;
                            break;
                    }

                    var _with40 = ws.Cells[5, StartColumnNo, 5, EndColumnNo];
                    _with40.Merge = true;
                    _with40.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    _with40.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    var _with41 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                    _with41.Merge = true;
                    _with41.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    _with41.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    bool IsOdd = true;

                    while (StartColumnNo <= LastColumnNo)
                    {


                        var _with42 = ws.Cells[RowNo, StartColumnNo, RowNo, EndColumnNo];
                        // .Style.Border.Left.Style = ExcelBorderStyle.Thin
                        //.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165))
                        if (EndColumnNo - StartColumnNo <= 1)
                        {
                            _with42.AutoFitColumns(3.0);
                        }
                        else
                        {
                            _with42.AutoFitColumns(1.0);
                        }
                        //.Style.Border.Right.Style = ExcelBorderStyle.Thin
                        //.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165))

                        _with42.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with42.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with42.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with42.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        //.Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.FromArgb(165, 165, 165))

                        if (IsOdd)
                        {
                            _with42.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with42.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                            // .Merge = True




                            //With ws.Cells[5, StartColumnNo, 5, EndColumnNo]
                            //    .Merge = True
                            //    .Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                            //    .Style.VerticalAlignment = ExcelVerticalAlignment.Center
                            //End With
                            var _with43 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                            _with43.Merge = true;
                            _with43.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with43.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            IsOdd = false;

                        }
                        else
                        {

                            //With ws.Cells[5, StartColumnNo, 5, EndColumnNo]
                            //    .Merge = True
                            //    .Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                            //    .Style.VerticalAlignment = ExcelVerticalAlignment.Center
                            //End With
                            var _with44 = ws.Cells[6, StartColumnNo, 6, EndColumnNo];
                            _with44.Merge = true;
                            _with44.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            _with44.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            IsOdd = true;
                            //.Merge = True
                        }


                        var _with45 = ws.Cells[RowNo, EndColumnNo, RowNo, EndColumnNo];
                        _with45.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with45.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        StartColumnNo = EndColumnNo + 1;
                        EndColumnNo = StartColumnNo + 6;

                        if (EndColumnNo > LastColumnNo)
                        {
                            EndColumnNo = LastColumnNo;
                        }
                    }




                    foreach (var bar in Item.StartEndDate.Split(new string[] { " ~~!~~ " }, StringSplitOptions.None))
                    {

                        if (bar.Length > 5)
                        {
                            System.DateTime StartDate = DateTime.Parse(bar.Split(':')[0]);
                            System.DateTime EndDate = DateTime.Parse(bar.Split(':')[1]);
                            string Startdatecomment = bar.Split(':')[2].Replace("~~!~~", "");

                            CultureInfo ciCurr = CultureInfo.CurrentCulture;

                            //Calendar myCalendar = new GregorianCalendar();
                            //int EndWeekNumber = myCalendar.GetWeekOfYear(EndDate, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                            int EndWeekNumber = ciCurr.Calendar.GetWeekOfYear(EndDate, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);

                            //  int startweeknum = myCalendar.GetWeekOfYear(StartDate, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                            int startweeknum = ciCurr.Calendar.GetWeekOfYear(StartDate, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                            int BarStartColumn = StartDate.Subtract(CalenderStartDate).Days + ColumnNo;
                            int BarEndColumn = EndDate.Subtract(StartDate).Days + BarStartColumn;

                            int endcolmn = EndWeekNumber + startweeknum;




                            if (BarStartColumn < ColumnNo)
                            {
                                BarStartColumn = ColumnNo;
                            }

                            if (BarEndColumn > LastColumnNo)
                            {
                                BarEndColumn = LastColumnNo;
                            }



                            if (BarStartColumn < LastColumnNo & BarEndColumn > ColumnNo)
                            {

                                try
                                {


                                    var _with46 = ws.Cells[RowNo, BarStartColumn, RowNo, BarEndColumn];
                                    _with46.Merge = true;
                                    _with46.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    _with46.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with46.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    _with46.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with46.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    _with46.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with46.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    _with46.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with46.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    System.Drawing.Color colorcode = default(System.Drawing.Color);
                                    _with46.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(HexStrToBase10Int(Item.ColorCode)));



                                    dynamic Comment = ws.Comments.Add(ws.Cells[RowNo, BarStartColumn, RowNo, BarEndColumn], "Name: " + HttpUtility.HtmlDecode(Item.Name) + Environment.NewLine + "Start: " + StartDate.ToString("yyyy-MM-dd") + Environment.NewLine + "End: " + EndDate.ToString("yyyy-MM-dd") + Environment.NewLine + "Start/End Comment: " + HttpUtility.HtmlDecode(Startdatecomment.ToString()) + Environment.NewLine, "Marcom Platform");
                                    Comment.Font.FontName = "Calibri";
                                    Comment.Font.Size = 10;
                                    Comment.From.Column = 1;
                                    Comment.From.Row = 1;
                                    Comment.To.Column = 3;
                                    Comment.To.Row = 8;
                                    Comment.BackgroundColor = System.Drawing.Color.FromArgb(231, 242, 245);

                                }
                                catch (Exception ex)
                                {
                                    //Log("Date overlaping for", Item.Name);

                                }
                            }

                        }

                    }

                }


                RowNo = RowNo + 1;


            }






            return 0;
        }

    }
}