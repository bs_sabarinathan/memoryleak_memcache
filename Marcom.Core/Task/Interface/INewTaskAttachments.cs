﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    /// <summary>
    /// IAttachments interface for table 'PM_Attachments'.
    /// </summary>
    public interface INewTaskAttachments
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int ActiveVersionNo
        {
            get;
            set;

        }

        int ActiveFileid
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        DateTime Createdon
        {
            get;
            set;

        }


        string Description
        {
            get;
            set;

        }


        #endregion
    }
}
