﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;

namespace BrandSystems.Marcom.Core.ExpireHandler
{
    internal class ExpireActionSourcedatas : IExpireActionSourcedatas
    {
        #region Member Variables
        protected int _id;
        protected int _actionSourceID;
        protected int _attributeID;
        protected int _attributeTypeID;
        protected string _attributeCaption;       
        protected string _nodeID;
        protected int _level;
        protected int _splValue;
        protected bool _ispublish;
        protected string _taskinputvalue;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public ExpireActionSourcedatas() { }

        public ExpireActionSourcedatas(int pactionSourceID, int pattributeID, int pattributeTypeID, string pattributeCaption, string pnodeID, int plevel, int psplValue, bool pispublish, string ptaskinputvalue)
        {
            this._actionSourceID = pactionSourceID;
            this._attributeID = pattributeID;
            this._attributeTypeID = pattributeTypeID;
            this._attributeCaption = pattributeCaption;
            this._nodeID = pnodeID;
            this._level = plevel;
            this._splValue = psplValue;
            this._ispublish = pispublish;
            this._taskinputvalue = ptaskinputvalue;
        }

        public ExpireActionSourcedatas(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int ActionSourceID
        {
            get { return _actionSourceID; }
            set { _actionSourceID = value; }

        }
        public virtual int AttributeID
        {
            get { return _attributeID; }
            set { _attributeID = value; }

        }

        public virtual int AttributeTypeID
        {
            get { return _attributeTypeID; }
            set { _attributeTypeID = value; }

        }
       

        public virtual string AttributeCaption
        {
            get { return _attributeCaption; }
            set { _attributeCaption = value; }
        }

        public virtual string NodeID
        {
            get { return _nodeID; }
            set { _nodeID = value; }
        }
            

        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual int SplValue
        {
            get { return _splValue; }
            set { _splValue = value; }

        }
        public virtual bool Ispublish
        {
            get { return _ispublish; }
            set { _ispublish = value; }
        }

        public virtual string Taskinputvalue
        {
            get { return _taskinputvalue; }
            set { _taskinputvalue = value; }
        }

        #endregion
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            ExpireActionSourcedatas castObj = null;
            try
            {
                castObj = (ExpireActionSourcedatas)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
 
    }
}
