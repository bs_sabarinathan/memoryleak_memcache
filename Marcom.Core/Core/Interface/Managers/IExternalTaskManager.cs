﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IExternalTaskManager
    {
        //bool UpdateExpireActionDate(int SourceID, string DateActionexpiredate, int SourcetypeID, int ActionType, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID);
        void TestDalimRPCConnector(int taskId, int assetId);

        Dictionary<string, string> GetDocumentViewerInfo(int taskId, string usrname);

        #region Instance of Classes In ServiceLayer reference

        #endregion

        #region Dalim User call back

        bool UpdateDalimUserTaskStatus(int DalimID, int dalimstepId, int stepStatus, string stepName);

        //bool UpdateDalimUserTaskStatus(int documentID, string userEmail, int stepId, string status, bool isExternalTask = true);
        #endregion

        bool UpdateDalimDocumentFile(int taskId, string filePath);
    }
}
