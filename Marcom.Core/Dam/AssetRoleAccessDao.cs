﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetRoleAccess : IAssetRoleAccess
    {
        #region "Member variables"

        protected int _id;
        protected int _roleID;
        protected int _assetID;

        #endregion

        #region "Constructor"

        public AssetRoleAccess() { }


        public AssetRoleAccess(int pid,int pRoleID,int pAssetID)
        {
            this._id = pid;
            this._roleID = pRoleID;
            this._assetID = pAssetID;
        }

        #endregion

        #region "Public propertiers"

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int RoleID
        {
            get { return _roleID; }
            set { _roleID = value; }
        }

        public int AssetID
        {
            get { return _assetID; }
            set { _assetID = value; }
        }

        #endregion
    }
}
