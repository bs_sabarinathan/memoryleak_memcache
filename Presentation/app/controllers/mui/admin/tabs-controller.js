﻿(function (ng, app) {
    "use strict";

    function muiadmintabsCtrl($scope, $resource, $location, $timeout, $translate, AdminService) {
        $(function () {
            $("#sortableAttribute1").sortable({
                connectWith: ".connectedAttributeSortable",
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                helper: 'clone',
                update: function (event, ui) {
                    var listItems = $("li", this);
                    var uiArray = new Array();
                    listItems.each(function (idx, li) {
                        uiArray.push($(li).attr("data-ID"));
                    });
                    for (var i = 0; i < uiArray.length; i++) {
                        var taskListResObj = $.grep($scope.TabCollectionsList, function (e) {
                            return e.Id == parseInt(uiArray[i]);
                        });
                        if (taskListResObj.length > 0) {
                            taskListResObj[0].SortOrder = uiArray.indexOf(taskListResObj[0].Id.toString()) + 1;
                            AdminService.UpdateCustomTabSortOrder(taskListResObj[0].Id, taskListResObj[0].SortOrder).then(function () { });
                        }
                    }
                    $scope.$apply();
                }
            }).disableSelection();
        });
        $scope.CurrenttabID = 0;
        $scope.EntityType = 6;
        $scope.ScreenText = "Add tab";
        $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
        $scope.TabCollectionsList = null;
        $scope.TabsEncrptionList = null;
        $scope.EncryptionkeyTemp = "";
        $scope.InitialVectorTemp = "";
        $scope.AlgorithmTemp = "";
        $scope.PaddingModeTemp = "";
        $scope.CipherModeTemp = "";
        $scope.ReassignMembersData = [];
        $scope.GlobalRolevalue = [];
        $scope.EntitytypeId = [];
        $scope.TabsAccessList = null;
        ClearAll();
        loadssodetails();

        var planTypes = [];
        var objectiveTypes = [];

        AdminService.getPlanandObjTypes().then(function (Result) {
            if (Result.Response != null) {
                planTypes = Result.Response.m_Item1;
                objectiveTypes = Result.Response.m_Item2;
            }
        })
        function loadssodetails() {
            AdminService.GetCustomTabSettingDetails().then(function (res) {
                if (res != null) {
                    $scope.EncryptionkeyTemp = res.Response[0].key;
                    $scope.InitialVectorTemp = res.Response[0].IV;
                    $scope.AlgorithmTemp = res.Response[0].AlgorithmValue;
                    $scope.PaddingModeTemp = res.Response[0].PaddingModeValue;
                    $scope.CipherModeTemp = res.Response[0].CipherModeValue;
                    $scope.Encryptionkey = res.Response[0].key;
                    $scope.InitialVector = res.Response[0].IV;
                    $scope.Algorithm = res.Response[0].AlgorithmValue;
                    $scope.PaddingMode = res.Response[0].PaddingModeValue;
                    $scope.CipherMode = res.Response[0].CipherModeValue;
                    $scope.Algorithms = res.Response[0].Algorithmoption;
                    $scope.PaddingModes = res.Response[0].PaddingModeoption;
                    $scope.CipherModes = res.Response[0].CipherModeoption;
                }
            });
        }
        GetAllTabCollections();

        function GetAllTabCollections() {
            AdminService.GetCustomTabsByTypeID($scope.EntityType).then(function (res) {
                if (res.Response != null) {
                    $scope.TabCollectionsList = res.Response;
                }
            });
            AdminService.GetCustomTabEncryptionByID().then(function (res) {
                if (res.Response != null) {
                    $scope.TabsEncrptionList = res.Response;
                }
            });
        }
        $scope.hideentitytypefrCalender = false;
        $scope.GetAllTabCollections = function () {
            $scope.hideentitytypefrCalender = ($scope.EntityType == 35 || $scope.EntityType == 5) ? true : false;
            GetAllTabCollections();
        }
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        //AdminService.GetEntityTypeIsAssociate().then(function (res) {
        //    if (res.Response != null) {
        //        $scope.entitytpesdata = res.Response;
        //        if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
        //        $.each(res.Response, function (i, el) {
        //            $scope.tagAllOptions.data.push({
        //                "id": el.Id,
        //                "text": el.Caption,
        //                "ShortDescription": el.ShortDescription,
        //                "ColorCode": el.ColorCode
        //            });
        //        });
        //    }
        //});
        AdminService.GetGlobalRole().then(function (res) {
            $scope.globalrolesData = res.Response;
        });
        $scope.AddTabs = function () {

            drawEntityType($scope.EntityType);

            $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
            $scope.ScreenText = "Add tab";
            $("#Tabmodal").modal("show");
            ClearAll();
            $scope.EncrypID = 0;
            $scope.Encryptionkey = $scope.EncryptionkeyTemp;
            $scope.InitialVector = $scope.InitialVectorTemp;
            $scope.Algorithm = $scope.AlgorithmTemp;
            $scope.PaddingMode = $scope.PaddingModeTemp;
            $scope.CipherMode = $scope.CipherModeTemp;
            $("#tabsEncryptionform").addClass('notvalidate');
            $timeout(function () {
                ValidatingTabsEncryption();
            }, 200);
        }

        function ValidatingTabsEncryption() {
            var entitytypeValues1 = [
				['#key', 'presence', 'Please enter the Encryption Key'],
				['#InitialVector', 'presence', 'Please enter the Initial vector'],
				['#Algorithm', 'presence', 'Please select the Elgorithm type'],
				['#PaddingMode', 'presence', 'Please select the Padding mode'],
				['#CipherMode', 'presence', 'Please select the CipherMode']
            ];
            $("#tabsEncryptionform").nod(entitytypeValues1, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp4',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
        }
        $scope.SaveTabDetails = function () {
            $("#btnTemp4").click();
            $("#tabsEncryptionform").removeClass('notvalidate');
            if ($("#tabsEncryptionform .error").length > 0) {
                return false;
            }
            if (TabValidation()) {
                var tabParam = {};
                tabParam.Id = $scope.CurrenttabID;
                tabParam.Name = $scope.tabname;
                tabParam.Typeid = $scope.EntityType;
                tabParam.ExternalUrl = $scope.tabexternalurl;
                tabParam.AddEntityID = $scope.tabaddentityid;
                tabParam.AddLanguageCode = $scope.tabaddlanguagecode;
                tabParam.AddUserEmail = $scope.tabaddUseremail;
                tabParam.AddUserName = $scope.tabaddUserName;
                tabParam.AddUserID = $scope.tabaddUserID;
                tabParam.tabEncryID = $scope.EncrypID;
                tabParam.encryKey = $scope.Encryptionkey;
                tabParam.encryIV = $scope.InitialVector;
                tabParam.algorithm = $scope.Algorithm;
                tabParam.paddingMode = $scope.PaddingMode;
                tabParam.cipherMode = $scope.CipherMode;
                var arrentyid = [];
                for (var i = 0; i < $scope.EntitytypeId.length; i++) {
                    arrentyid.push($scope.EntitytypeId[i].id);
                }
                tabParam.EntityTypeIds = arrentyid.join(",");
                tabParam.GlobalIds = $scope.GlobalRolevalue.join(",");
                AdminService.InsertUpdateCustomTab(tabParam).then(function (res) {
                    if (res.Response != 0) {
                        if ($scope.CurrenttabID == 0) {
                            $scope.TabCollectionsList.push({
                                Id: res.Response[0],
                                Name: $scope.tabname,
                                Typeid: $scope.EntityType,
                                ExternalUrl: $scope.tabexternalurl,
                                AddEntityID: $scope.tabaddentityid,
                                AddLanguageCode: $scope.tabaddlanguagecode,
                                AddUserEmail: $scope.tabaddUseremail,
                                AddUserName: $scope.tabaddUserName,
                                AddUserID: $scope.tabaddUserID,
                                IsSytemDefined: false
                            });
                            $scope.TabsEncrptionList.push({
                                ID: res.Response[1],
                                CustomTabID: res.Response[0],
                                EncryKey: $scope.Encryptionkey,
                                EncryIV: $scope.InitialVector,
                                Algorithm: $scope.Algorithm,
                                PaddingMode: $scope.PaddingMode,
                                CipherMode: $scope.CipherMode
                            });
                        } else {
                            var currentobj = $.grep($scope.TabCollectionsList, function (item, i) {
                                return item.Id == $scope.CurrenttabID
                            });
                            currentobj[0].Name = $scope.tabname;
                            currentobj[0].Typeid = $scope.EntityType;
                            currentobj[0].ExternalUrl = $scope.tabexternalurl;
                            currentobj[0].AddEntityID = $scope.tabaddentityid;
                            currentobj[0].AddLanguageCode = $scope.tabaddlanguagecode;
                            currentobj[0].AddUserEmail = $scope.tabaddUseremail;
                            currentobj[0].AddUserName = $scope.tabaddUserName;
                            currentobj[0].AddUserID = $scope.tabaddUserID;
                            currentobj[0].IsSytemDefined = false;
                            var currentEncryobj = $.grep($scope.TabsEncrptionList, function (item, i) {
                                return item.CustomTabID == $scope.CurrenttabID
                            });
                            if (currentEncryobj.length == 0) {
                                $scope.TabsEncrptionList.push({
                                    ID: res.Response[1],
                                    CustomTabID: res.Response[0],
                                    EncryKey: $scope.Encryptionkey,
                                    EncryIV: $scope.InitialVector,
                                    Algorithm: $scope.Algorithm,
                                    PaddingMode: $scope.PaddingMode,
                                    CipherMode: $scope.CipherMode
                                });
                            } else {
                                currentEncryobj[0].EncryKey = $scope.Encryptionkey;
                                currentEncryobj[0].EncryIV = $scope.InitialVector;
                                currentEncryobj[0].Algorithm = $scope.Algorithm;
                                currentEncryobj[0].PaddingMode = $scope.PaddingMode;
                                currentEncryobj[0].CipherMode = $scope.CipherMode;
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4199.Caption'));
                        $("#Tabmodal").modal('hide');
                    }
                });
            }
        }
        $scope.LoadOnUpdate = function (ID) {
            $scope.buttonText = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.ScreenText = "Edit tab";
            $scope.tabname = $scope.TabCollectionsList[ID].Name;
            $scope.EntityType = $scope.TabCollectionsList[ID].Typeid;
            $scope.tabexternalurl = $scope.TabCollectionsList[ID].ExternalUrl;
            $scope.tabaddentityid = $scope.TabCollectionsList[ID].AddEntityID;
            $scope.tabaddlanguagecode = $scope.TabCollectionsList[ID].AddLanguageCode;
            $scope.tabaddUseremail = $scope.TabCollectionsList[ID].AddUserEmail;
            $scope.tabaddUserName = $scope.TabCollectionsList[ID].AddUserName;
            $scope.tabaddUserID = $scope.TabCollectionsList[ID].AddUserID;
            var encrpres = $.grep($scope.TabsEncrptionList, function (e) {
                return e.CustomTabID == $scope.CurrenttabID;
            })[0];
            if (encrpres != undefined) {
                $scope.EncrypID = encrpres.ID;
                $scope.Encryptionkey = encrpres.EncryKey;
                $scope.InitialVector = encrpres.EncryIV;
                $scope.Algorithm = encrpres.Algorithm;
                $scope.PaddingMode = encrpres.PaddingMode;
                $scope.CipherMode = encrpres.CipherMode;
            } else {
                $scope.EncrypID = 0;
                $scope.Encryptionkey = $scope.EncryptionkeyTemp;
                $scope.InitialVector = $scope.InitialVectorTemp;
                $scope.Algorithm = $scope.AlgorithmTemp;
                $scope.PaddingMode = $scope.PaddingModeTemp;
                $scope.CipherMode = $scope.CipherModeTemp;
            }
            $("#Tabmodal").modal("show");
            $("#tabsEncryptionform").addClass('notvalidate');
            $timeout(function () {
                ValidatingTabsEncryption();
            }, 200);
        };
        $scope.TabByID = function (ID) {
            drawEntityType($scope.EntityType);
            $scope.CurrenttabID = $scope.TabCollectionsList[ID].Id;
            $scope.EntitytypeId = [];
            $scope.GlobalRolevalue = [];
            $timeout(function () {
                AdminService.GetCustomTabAccessByID($scope.CurrenttabID).then(function (res) {
                    if (res.Response != null) {
                        $scope.TabsAccessList = res.Response;
                        var enty = "";
                        var entytids = [];
                        var roleids = [];
                        var globalrole = [];

                        for (var uti = 0; uti < res.Response.length; uti++) {                                                                               
                            for (var k = 0; k < $scope.tagAllOptions.data.length; k++) {
                                if ($scope.tagAllOptions.data[k].id == res.Response[uti].EntityTypeID) {                                   
                                    if ($.inArray(res.Response[uti].GlobalRoleID, roleids) == -1) roleids.push(res.Response[uti].GlobalRoleID);
                                    enty = $.grep(entytids, function (e) {
                                        return e.id == $scope.tagAllOptions.data[k].id
                                    });
                                    if (enty == null || enty == "") entytids.push($scope.tagAllOptions.data[k]);
                                    $scope.GlobalRolevalue = jQuery.unique(roleids);
                                } else if (res.Response[uti].EntityTypeID == 35) {
                                    if ($.inArray(res.Response[uti].GlobalRoleID, roleids) == -1) roleids.push(res.Response[uti].GlobalRoleID);
                                    enty = $.grep(entytids, function (e) {
                                        return e.id == $scope.tagAllOptions.data[k].id
                                    });
                                    if (enty == null || enty == "") entytids.push($scope.tagAllOptions.data[k]);
                                } else if (res.Response[uti].EntityTypeID == 0) {
                                    if ($.inArray(res.Response[uti].GlobalRoleID, roleids) == -1) roleids.push(res.Response[uti].GlobalRoleID);
                                }
                            }
                            if ($scope.tagAllOptions.data.length == 0) {
                                roleids.push(res.Response[uti].GlobalRoleID);
                                $scope.GlobalRolevalue = jQuery.unique(roleids);
                            }
                        }
                        $scope.EntitytypeId = jQuery.unique(entytids);
                       //$scope.GlobalRolevalue = jQuery.unique(roleids);
                        $timeout(function () {
                            $scope.LoadOnUpdate(ID);
                        }, 100);
                    } else $timeout(function () {
                        $scope.LoadOnUpdate(ID);
                    }, 100);
                });
            }, 300);
        }
        $scope.DeleteTabByID = function (Index, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2006.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteCustomtabByID(ID, 0, 0).then(function (res) {
                            if (res.Response != false) {
                                $scope.TabCollectionsList.splice(Index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }

        function ClearAll(ID) {
            $scope.tabname = "";
            $scope.CurrenttabID = 0;
            $scope.tabexternalurl = "";
            $scope.tabaddentityid = false;
            $scope.tabaddlanguagecode = false;
            $scope.tabaddUseremail = false;
            $scope.tabaddUserName = false;
            $scope.tabaddUserID = false;
            $scope.EncrypID = 0;
            $scope.Encryptionkey = "";
            $scope.InitialVector = "";
            $scope.Algorithm = "";
            $scope.PaddingMode = "";
            $scope.CipherMode = "";
            $scope.EntitytypeId = [];
            $scope.GlobalRolevalue = [];
        }

        function TabValidation() {
            if ($scope.tabname == undefined || $scope.tabname == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4579.Caption'));
                return false;
            }
            if ($scope.tabexternalurl == undefined || $scope.tabexternalurl == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4575.Caption'));
                return false;
            }
            if ($scope.Algorithm.toString().toLocaleLowerCase() == '--select--') {
                bootbox.alert($translate.instant('LanguageContents.Res_4605.Caption'));
                return false;
            }
            if ($scope.PaddingMode.toString().toLocaleLowerCase() == '--select--') {
                bootbox.alert($translate.instant('LanguageContents.Res_4624.Caption'));
                return false;
            }
            if ($scope.CipherMode.toString().toLocaleLowerCase() == '--select--') {
                bootbox.alert($translate.instant('LanguageContents.Res_4616.Caption'));
                return false;
            }
            if ($scope.EntitytypeId.length > 0 && $scope.GlobalRolevalue.length == 0) {
                $scope.GlobalRolevalue.push("0");
            }
            if ($scope.EntitytypeId.length == 0 && $scope.GlobalRolevalue.length > 0) {
                $scope.EntitytypeId.push({
                    "id": 0
                });
            }
            return true;
        }
        $scope.ChangeCss = function (IsSystemDefined) {
            if (!IsSystemDefined) {
                return "sysDefined";
            }
        }

        function drawEntityType(entityID) {
            var availEntityTypes = [];
            if (entityID == SystemDefinedEntityTypes.Plan) {
                availEntityTypes = planTypes;
            }

            else if (entityID == SystemDefinedEntityTypes.Objective) {
                availEntityTypes = objectiveTypes;
            }

            $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
            $.each(availEntityTypes, function (i, el) {
                $scope.tagAllOptions.data.push({
                    "id": el.Id,
                    "text": el.Caption,
                    "ShortDescription": el.ShortDescription,
                    "ColorCode": el.ColorCode
                });
            });

        }
    }
    app.controller("mui.admin.tabsCtrl", ['$scope', '$resource', '$location', '$timeout', '$translate', 'AdminService', muiadmintabsCtrl]);
})(angular, app);