using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

    /// <summary>
    /// GlobalRole object for table 'AM_GlobalRole'.
    /// </summary>
    public class GlobalRole : IGlobalRole
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        protected string _description;
        protected bool _isassetaccess;

        #endregion

        #region Constructors
        public GlobalRole() { }

        public GlobalRole(string pCaption, string pDescription, bool pIsAssetAccess)
        {
            this._caption = pCaption;
            this._description = pDescription;
            this._isassetaccess = pIsAssetAccess;
        }

        public GlobalRole(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 4000)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 4000 characters");
                _description = value;
            }

        }
        public bool IsAssetAccess
        {
            get { return _isassetaccess; }
            set { _isassetaccess = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            GlobalRole castObj = null;
            try
            {
                castObj = (GlobalRole)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
