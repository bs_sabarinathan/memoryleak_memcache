﻿(function (ng, app) {
    "use strict";

    function muiadminoptimakerCtrl($scope, $location, $resource, $timeout, $translate, $compile, AdminService) {
        $scope.OptimakerSettingCollection = {};
        $scope.OptimakerCatagories = {};
        $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
        $scope.ScreenText = "Add Media Generator"
        $scope.OptimakerID = 0;
        $scope.Wordtemplates = {};
        $scope.WordTempID = 0;
        $scope.PrevImage = "";
        $scope.PrevDoc = "";
        $scope.ImgPath = TenantFilePath + "DAMFiles/Preview/NoPreview.jpg";
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            $scope.ImgPath = cloudpath + "DAMFiles/Preview/NoPreview.jpg";
        }
        $scope.BtnSaveShow = false;
        $scope.AssetTypeListdata = [];
        var availableattrs = [];
        var allEntityTypes = [];
        var allAttributes = [];
        var AdminLogoSettings = 'AssetCreation';
        var ListView = 'AssetType';
        $scope.clearFilter = function () {
            $scope.mediaGenFiltr = "";
            $scope.mediaGenfilt();
            $scope.wordTempFilter = "";
        };
        $scope.filteringmediaGenEventHandler = function (e) {
            $timeout(function () {
                $('.footable').trigger('footable_resize');
            }, 500);
        };
        $scope.mediaGenfilt = function () {
            $scope.mediaGenFilter = $scope.mediaGenFiltr;
            $timeout(function () {
                $("#mediaGenTbl").footable();
                $("#mediaGenTbl").trigger('footable_initialized');
                $("#mediaGenTbl").trigger('footable_resize');
                $("#mediaGenTbl").data('footable').redraw();
            }, 10);
        }
        $scope.clearTempFilter = function () {
            $scope.wordTempFilter = "";
            $scope.wordTempfilt();
        };
        $scope.filteringEventHandler = function (e) {
            $timeout(function () {
                $('.footable').trigger('footable_resize');
            }, 500);
        };
        $scope.wordTempfilt = function () {
            $scope.wordTempFilter = $scope.wordTempFilter;
            $timeout(function () {
                $("#wordTemplateTbl").footable();
                $("#wordTemplateTbl").trigger('footable_initialized');
                $("#wordTemplateTbl").trigger('footable_resize');
                $("#wordTemplateTbl").data('footable').redraw();
            }, 10);
        }
        $scope.sortEnable = function sortEnable(columnID) {
            $scope.tempColID = columnID;
            if ($scope.colName == columnID) {
                if ($scope.IsSortAsc != true) {
                    $scope.IsSortAsc = true;
                    $scope.IsSortDsc = true;
                } else {
                    $scope.IsSortAsc = false;
                }
            } else {
                $scope.IsSortAsc = true;
            }
            $scope.colName = columnID;
        }

        function setSort() {
            $("#" + $scope.tempColID).click();
            if ($scope.tempColID != undefined) if ($scope.tempColID.contains('th0')) $("#" + $scope.tempColID).click();
            if ($scope.tempColID != undefined) if (!$scope.tempColID.contains('th0') && ($scope.IsSortAsc == false || $scope.IsSortDsc == true)) {
                $("#" + $scope.tempColID).click();
            }
        }
        AdminService.getServerUrls().then(function (serverUrl) {
            var allServerUrl = serverUrl.Response;
            $scope.optimakerServerUrls = $.grep(allServerUrl, function (e) {
                return e.EngineType == 1
            });
            $scope.ImagineServerUrls = $.grep(allServerUrl, function (e) {
                return e.EngineType == 2
            });
        });
        AdminService.GetEntityType(5).then(function (assetTypes) {
            $scope.Damentitytype = assetTypes.Response;
            $scope.assetType = $scope.Damentitytype[0].Id;
            for (var i = 0; i < $scope.Damentitytype.length; i++) {
                var el = $scope.Damentitytype[i];
                $scope.AssetTypeListdata.push({
                    "id": el.Id,
                    "text": el.Caption,
                    "ShortDescription": el.ShortDescription,
                    "ColorCode": el.ColorCode,
                    "Caption": el.Caption
                });
            }
            GetOptimakerSettings();
        });
        $scope.AssetType = '';
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="">' + item.Caption + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="">' + item.Caption + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptionsEntitytypelist = {
            multiple: false,
            allowClear: true,
            data: $scope.AssetTypeListdata,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function GetOptimakerSettings() {
            GetCatagories();
            AdminService.GetOptimakerSettings().then(function (data) {
                var OptimakerSettingCollection = data.Response;
                if (OptimakerSettingCollection != null) {
                    for (var i = 0; i < OptimakerSettingCollection.length; i++) {
                        OptimakerSettingCollection[i].TemplateEngineType = OptimakerSettingCollection[i].TemplateEngineType == 1 ? $translate.instant('LanguageContents.Res_5566.Caption') : $translate.instant('LanguageContents.Res_5567.Caption');
                        OptimakerSettingCollection[i].AssetTypeName = $.grep($scope.Damentitytype, function (e) {
                            return e.Id == OptimakerSettingCollection[i].AssetType
                        })[0].Caption;
                        OptimakerSettingCollection[i].ShortDescription = $.grep($scope.Damentitytype, function (e) {
                            return e.Id == OptimakerSettingCollection[i].AssetType
                        })[0].ShortDescription;
                        OptimakerSettingCollection[i].ColorCode = $.grep($scope.Damentitytype, function (e) {
                            return e.Id == OptimakerSettingCollection[i].AssetType
                        })[0].ColorCode;
                    }
                    $scope.OptimakerSettingCollection = OptimakerSettingCollection;
                }
            });
            AdminService.GetWordTemplateSettings().then(function (data) {
                $scope.Wordtemplates = data.Response;
            });
        }

        function GetCatagories() {
            AdminService.GetOptimakerCatagories().then(function (data) {
                $scope.OptimakerCatagories = data.Response;
            });
        }
        $scope.AddSpace = function (CurrentLength, Name) {
            var stringName = "";
            for (var i = 0; i < CurrentLength; i++) {
                stringName += " ";
            }
            return stringName += Name;
        }
        $scope.OptCategory = 0;
        $scope.OpenOptimakerSettingPopUp = function () {
            $("#OptimakerSettings").modal('show');
            $scope.AssetTypesCollection = $scope.Damentitytype;
            $scope.EnableHighResPDF = true;
            $scope.templEnginType = 0;
            $scope.AssetType = 0;
            ClearOptimakerSettings();
            $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
            $scope.ScreenText = "Add Media Generator";
        }
        $scope.SaveOptimakerSettings = function (ID) {
            if ($scope.OptimakerID > 0) {
                ID = $scope.OptimakerID;
            }
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                if (OptimakerSettingValidation()) {
                    AdminService.copydeleteTemplateAsset($scope.PrevImage).then(function (result) {
                        if (result != null) {
                            $scope.PrevImage = result.Response;
                            var AddInsertData = {};
                            AddInsertData.ID = ID;
                            AddInsertData.Name = $scope.OptSettingname;
                            AddInsertData.CategoryId = $scope.OptCategory;
                            AddInsertData.Description = $scope.Templatedesc;
                            AddInsertData.DocId = $scope.DocId;
                            AddInsertData.DeptId = $scope.DeptId == "" ? 0 : $scope.DeptId;
                            AddInsertData.DocType = $scope.DocType == "" ? 0 : $scope.DocType;
                            AddInsertData.DocVersionId = $scope.DocversionId == "" ? 0 : $scope.DocversionId;
                            AddInsertData.PreviewImage = $scope.PrevImage;
                            AddInsertData.AutoApproval = ($('#AutoApprove')[0].checked) ? 1 : 0;
                            AddInsertData.TemplateEngineType = $scope.templEnginType;
                            AddInsertData.AssetType = $scope.AssetType.id == undefined ? $scope.AssetType : $scope.AssetType.id;
                            AddInsertData.ServerURL = $scope.ServerURL;
                            AddInsertData.EnableHighResPDF = ($('#EnableHighResPDF')[0].checked) ? 1 : 0;;
                            AdminService.InsertUpdateOptimakerSettings(AddInsertData).then(function (data) {
                                if (data.Response > 0) {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                                    RefreshScopeValues(ID, data.Response);
                                    $("#OptimakerSettings").modal('hide');
                                    GetOptimakerSettings();
                                }
                            });
                        }
                    })
                }
            } else {
                var AddInsertData = {};
                AddInsertData.ID = ID;
                AddInsertData.Name = $scope.OptSettingname;
                AddInsertData.CategoryId = $scope.OptCategory;
                AddInsertData.Description = $scope.Templatedesc;
                AddInsertData.DocId = $scope.DocId;
                AddInsertData.DeptId = $scope.DeptId == "" ? 0 : $scope.DeptId;
                AddInsertData.DocType = $scope.DocType == "" ? 0 : $scope.DocType;
                AddInsertData.DocVersionId = $scope.DocversionId == "" ? 0 : $scope.DocversionId;
                AddInsertData.PreviewImage = $scope.PrevImage;
                AddInsertData.AutoApproval = ($('#AutoApprove')[0].checked) ? 1 : 0;
                AddInsertData.TemplateEngineType = $scope.templEnginType;
                AddInsertData.AssetType = $scope.AssetType.id == undefined ? $scope.AssetType : $scope.AssetType.id;
                AddInsertData.ServerURL = $scope.ServerURL;
                AddInsertData.EnableHighResPDF = ($('#EnableHighResPDF')[0].checked) ? 1 : 0;;
                if (OptimakerSettingValidation()) {
                    AdminService.InsertUpdateOptimakerSettings(AddInsertData).then(function (data) {
                        if (data.Response > 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            RefreshScopeValues(ID, data.Response);
                            $("#OptimakerSettings").modal('hide');
                            GetOptimakerSettings();
                        }
                    });
                }
            }
        }

        function RefreshScopeValues(ID, NewID) {
            if (ID > 0) {
                var tempColl = $.grep($scope.OptimakerSettingCollection, function (item, i) {
                    return item.ID == ID;
                });
                tempColl[0].Name = $scope.OptSettingname;
                tempColl[0].CategoryID = $scope.OptCategory;
                tempColl[0].DocID = $scope.DocId;
                tempColl[0].DepartmentID = $scope.DeptId;
                tempColl[0].Description = $scope.Templatedesc;
                tempColl[0].DocType = $scope.DocType;
                tempColl[0].DocVersionID = $scope.DocversionId;
                tempColl[0].TemplateEngineType = $scope.templEnginType.Caption;
                tempColl[0].assetType = $scope.AssetType.Caption;
            } else {
                $scope.OptimakerSettingCollection.push({
                    ID: NewID,
                    Name: $scope.OptSettingname,
                    CategoryID: $scope.OptCategory,
                    DocID: $scope.DocId,
                    DepartmentID: $scope.DeptId,
                    Description: $scope.Templatedesc,
                    DocType: $scope.DocType,
                    DocVersionID: $scope.DocversionId,
                    TemplateEngineType: $scope.templEnginType.Caption,
                    assetType: $scope.AssetType.Caption
                });
            }
        }
        $scope.GetOptimakerSettingByIndex = function (Index) {
            ClearOptimakerSettings();
            $scope.AssetTypesCollection = $scope.Damentitytype;
            $("#OptimakerSettings").modal('show');
            $scope.ScreenText = "Edit Media Generator";
            $scope.buttonText = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.OptSettingname = $scope.OptimakerSettingCollection[Index].Name;
            $scope.OptCategory = $scope.OptimakerSettingCollection[Index].CategoryID;
            $scope.DocId = $scope.OptimakerSettingCollection[Index].DocID;
            $scope.DeptId = $scope.OptimakerSettingCollection[Index].DepartmentID;
            $scope.Templatedesc = $scope.OptimakerSettingCollection[Index].Description;
            $scope.DocType = $scope.OptimakerSettingCollection[Index].DocType;
            $scope.DocversionId = $scope.OptimakerSettingCollection[Index].DocVersionID;
            $scope.PrevImage = $scope.OptimakerSettingCollection[Index].PreviewImage;
            $scope.AutoApproval = Boolean($scope.OptimakerSettingCollection[Index].AutoApproval.toString().toLowerCase() == "true" ? 1 : 0);
            $scope.ImgPath = TenantFilePath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;           
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $scope.ImgPath = cloudpath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;
            }
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $scope.ImgPath = cloudpath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;
            }
            $scope.templEnginType = $scope.OptimakerSettingCollection[Index].TemplateEngineType == '2Imagine' ? 2 : 1;
            $scope.AssetType = $scope.OptimakerSettingCollection[Index].AssetType;
            $scope.OptimakerID = $scope.OptimakerSettingCollection[Index].ID;
            $scope.ServerURL = $scope.OptimakerSettingCollection[Index].ServerURL;
            $scope.EnableHighResPDF = $scope.OptimakerSettingCollection[Index].EnableHighResPDF;
        }
        $scope.DeleteOptimakerSettingsByID = function (Index, ID) {
            var result = bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                if (result) {
                    if (ID > 0) {
                        AdminService.DeleteOptimakeSetting(ID).then(function (data) {
                            if (data.Response == true) {
                                $scope.OptimakerSettingCollection.splice(Index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    } else {
                        $scope.OptimakerSettingCollection.splice(Index, 1);
                    }
                }
            });
        }

        function ClearOptimakerSettings() {
            $scope.OptimakerID = 0;
            $scope.OptSettingname = "";
            $scope.OptCategory = 0;
            $scope.DocId = "";
            $scope.DeptId = "";
            $scope.Templatedesc = "";
            $scope.DocType = "";
            $scope.DocversionId = "";
            $scope.OptimakerID = 0;
            $scope.AutoApproval = false;
            $scope.ServerURL = 0;
            $scope.ImgPath = TenantFilePath + "DAMFiles/Preview/NoPreview.jpg";
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $scope.ImgPath = cloudpath + "DAMFiles/Preview/NoPreview.jpg";
            }
        }

        function OptimakerSettingValidation() {
            if ($scope.OptSettingname.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4581.Caption'));
                return false;
            }
            if ($scope.OptCategory.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4615.Caption'));
                return false;
            }
            if ($scope.DocId.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4571.Caption'));
                return false;
            }
            if ($scope.DeptId.toString().trim().length == 0 && $scope.templEnginType == 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4569.Caption'));
                return false;
            }
            if ($scope.Templatedesc.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4570.Caption'));
                return false;
            }
            if ($scope.DocType.toString().trim().length == 0 && $scope.templEnginType == 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4572.Caption'));
                return false;
            }
            if ($scope.DocversionId.toString().trim().length == 0 && $scope.templEnginType == 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4573.Caption'));
                return false;
            }
            return true;
        }
        $scope.OpenOptimakerCategoryPopUp = function () {
            $("#modalCategory").modal('show');
            AdminService.GetCategoryTreeCollection().then(function (data) {
                $scope.tree = JSON.parse(data.Response);
            });
        }
        $scope.SaveWordTemplateSettings = function (ID) {
            if ($scope.WordTempID > 0) {
                ID = $scope.WordTempID;
            }
            var InsertUpdateData = {};
            InsertUpdateData.ID = ID;
            InsertUpdateData.Name = $scope.OptSettingname;
            InsertUpdateData.CategoryId = $scope.OptCategory;
            InsertUpdateData.Description = $scope.Templatedesc;
            InsertUpdateData.Worddocpath = $scope.PrevDoc;
            InsertUpdateData.PreviewImage = $scope.PrevImage;
            if (WordtemplateValidation()) {
                AdminService.InsertUpdateWordTemplateSettings(InsertUpdateData).then(function (data) {
                    if (data.Response > 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        RefreshWordTemplateScopeValues(ID, data.Response);
                        $("#WordtemplateSettings").modal('hide');
                    }
                });
            }
        }

        function WordtemplateValidation() {
            if ($scope.OptSettingname != undefined) {
                if ($scope.OptSettingname.toString().trim().length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4581.Caption'));
                    return false;
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4581.Caption'));
                return false;
            }
            if ($scope.OptCategory.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4615.Caption'));
                return false;
            }
            if ($scope.Templatedesc.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4570.Caption'));
                return false;
            }
            if ($scope.PrevDoc.toString().trim().length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4619.Caption'));
                return false;
            }
            return true;
        }

        function RefreshWordTemplateScopeValues(ID, NewID) {
            if (ID > 0) {
                var tempColl = $.grep($scope.Wordtemplates, function (item, i) {
                    return item.ID == ID;
                });
                tempColl[0].Name = $scope.OptSettingname;
                tempColl[0].CategoryID = $scope.OptCategory;
                tempColl[0].Description = $scope.Templatedesc;
                tempColl[0].Worddocpath = $scope.PrevDoc;
                tempColl[0].PreviewImage = $scope.PrevImage;
            } else {
                $scope.Wordtemplates.push({
                    ID: NewID,
                    Name: $scope.OptSettingname,
                    CategoryID: $scope.OptCategory,
                    Description: $scope.Templatedesc,
                    Worddocpath: $scope.PrevDoc,
                    PreviewImage: $scope.PrevImage
                });
            }
        }
        $scope.OpenWordtemplatePopUp = function () {
            $("#WordtemplateSettings").modal('show');
            $scope.ScreenText = "Add Word Template";
            ClearWordtemplate();
        }
        $scope.GetWordTemplateByIndex = function (Index) {
            ClearWordtemplate();
            $scope.ScreenText = "Edit Word Template";
            $scope.buttonText = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.OptSettingname = $scope.Wordtemplates[Index].Name;
            $scope.OptCategory = $scope.Wordtemplates[Index].CategoryID;
            $scope.Templatedesc = $scope.Wordtemplates[Index].Description;
            $scope.PrevImage = $scope.Wordtemplates[Index].PreviewImage;
            $scope.WordTempID = $scope.Wordtemplates[Index].ID;
            $scope.PrevDoc = $scope.Wordtemplates[Index].Worddocpath;
            $scope.ImgPath = TenantFilePath + 'DAMFiles/Templates/Word/Images/ThumbNail/' + $scope.PrevImage;
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $scope.ImgPath = cloudpath + 'DAMFiles/Templates/Word/Images/ThumbNail/' + $scope.PrevImage;
            }
            var splitArr = $scope.PrevDoc.split(".");
            $scope.DocPath = TenantFilePath + 'DAMFiles/StaticPreview/' + splitArr[1].toString().trim().toUpperCase() + ".png";
            $("#WordtemplateSettings").modal('show');
        }

        function ClearWordtemplate() {
            $scope.WordDocShow = true;
            $scope.ScreenText = "Add Word Template";
            $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
            $scope.OptSettingname = "";
            $scope.OptCategory = 0;
            $scope.Templatedesc = "";
            $scope.DocPath = "";
            $scope.PrevImage = "";
            $scope.WordTempID = 0;
            $scope.PrevDoc = "";
            $scope.ImgPath = TenantFilePath + "DAMFiles/Preview/NoPreview.jpg";
            $scope.DocPath = TenantFilePath + "DAMFiles/Preview/NoPreview.jpg";
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $scope.ImgPath = cloudpath + "DAMFiles/Preview/NoPreview.jpg";
                $scope.DocPath = cloudpath + "DAMFiles/Preview/NoPreview.jpg";
            }
        }
        $scope.DeleteWordTemplateByID = function (Index, ID) {
            var result = bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                if (result) {
                    if (ID > 0) {
                        AdminService.DeleteWordtempSetting(ID).then(function (data) {
                            if (data.Response == true) {
                                $scope.Wordtemplates.splice(Index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    } else {
                        $scope.Wordtemplates.splice(Index, 1);
                    }
                }
            });
        }
        $(function () {
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus',
                    browse_button: 'mediapickfiles',
                    container: 'container',
                    max_file_size: '10mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    resize: {
                        width: 320,
                        height: 240,
                        quality: 90
                    },
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader.bind('Init', function (up, params) {
                    $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('Error', function (up, err) {
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    $scope.PrevImage = response.response.split(',')[0] + response.response.split(',')[2];
                    $timeout(function () {
                        cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
                        $scope.ImgPath = cloudpath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;
                    }, 20);
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    GetOptimakerSettings();
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    console.log("File upload about to start.");
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + file.id + fileext).replace(/\\/g, "\/");;
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus',
                    browse_button: 'mediapickfiles',
                    container: 'container',
                    max_file_size: '10mb',
                    url: 'Handlers/UserTitleImageHandler.ashx?Type=Attachment&TemplateType=media',
                    flash_swf_url: 'assets/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    resize: {
                        width: 320,
                        height: 240,
                        quality: 90
                    }
                });
                uploader.bind('Init', function (up, params) {
                    $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('Error', function (up, err) {
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    $scope.PrevImage = response.response.split(',')[0];
                    $timeout(function () {
                        $scope.ImgPath = TenantFilePath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;
                        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                            $scope.ImgPath = cloudpath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + $scope.PrevImage;
                        }
                    }, 20);
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    GetOptimakerSettings();
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $scope.BtnSaveShow = true;
                    $scope.PrevShow = true;
                });
            }
        });
        $(function () {
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus',
                    browse_button: 'wordpickfiles',
                    container: 'container',
                    max_file_size: '10mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    resize: {
                        width: 320,
                        height: 240,
                        quality: 90
                    },
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader.bind('Init', function (up, params) {
                    $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('Error', function (up, err) {
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    $scope.PrevImage = response.response.split(',')[0] + response.response.split(',')[2];
                    $timeout(function () {
                        cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
                        $scope.ImgPath = cloudpath + 'DAMFiles/Templates/Word/Images/ThumbNail/' + $scope.PrevImage;
                    }, 20);
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    console.log("File upload about to start.");
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + 'DAMFiles/Templates/Word/Images/ThumbNail/' + file.id + fileext).replace(/\\/g, "\/");;
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus',
                    browse_button: 'wordpickfiles',
                    container: 'container',
                    max_file_size: '10mb',
                    url: 'Handlers/UserTitleImageHandler.ashx?Type=Attachment&TemplateType=word',
                    flash_swf_url: 'assets/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    resize: {
                        width: 320,
                        height: 240,
                        quality: 90
                    }
                });
                uploader.bind('Init', function (up, params) {
                    $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('Error', function (up, err) {
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                    $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    $scope.PrevImage = response.response.split(',')[0];
                    $timeout(function () {
                        $scope.ImgPath = TenantFilePath + 'DAMFiles/Templates/Word/Images/ThumbNail/' + $scope.PrevImage;
                    }, 20);
                    $scope.PrevShow = false;
                    $scope.BtnSaveShow = false;
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $scope.BtnSaveShow = true;
                    $scope.PrevShow = true;
                });
            }
        });
        $(function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'docfiles',
                container: 'container',
                max_file_size: '10000mb',
                url: 'Handlers/DamFileHandler.ashx?Type=Attachment&TemplateType=worddocument',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                chunk_size: '64Kb',
                multipart_params: {}
            });
            uploader.bind('Init', function (up, params) {
                uploader.splice();
            });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader.start();
            });
            uploader.bind('Error', function (up, err) {
                $scope.DocShow = false;
                $scope.BtnSaveShow = false;
                $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                var splitarr = response.response.split(',');
                $scope.PrevDoc = splitarr[0] + splitarr[2];
                var splitNewArr = $scope.PrevDoc.split(".");
                $timeout(function () {
                    $scope.DocPath = TenantFilePath + 'DAMFiles/StaticPreview/' + splitNewArr[1].toString().trim().toUpperCase() + ".png";
                }, 20);
                $scope.BtnSaveShow = false;
                $scope.DocShow = false;
            });
            uploader.bind('BeforeUpload', function (up, file) {
                $scope.BtnSaveShow = true;
                $scope.DocShow = true;
                $.extend(up.settings.multipart_params, {
                    id: file.id,
                    size: file.size
                });
            });
        });
        $scope.SaveAndUpdateCategory = function () {
            console.log($scope.tree);
            if ($scope.tree[0].Children.length > 0) {
                var test = {};
                test.jobj = $scope.tree;
                AdminService.InsertUpdateCatergory(test).then(function (data) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                    $("#modalCategory").modal('hide');
                    GetCatagories();
                    $timeout(function () {
                        setSort();
                    }, 50);
                });
            }
        }
        $scope.SelectCatagoryName = function (ID) {
            var currentObj = $.grep($scope.OptimakerCatagories, function (item, i) {
                return item.id == ID;
            });
            return currentObj[0].Caption;
        }
        $scope.wizard = {
            attributeType: 0,
            AttributeId: 0,
            Description: '',
            Caption: '',
            newOption: '',
            EditOption: '',
            OptionID: 0,
            TreeLevelNumberByNode: [],
            TreeLevels: [],
            IsDisableAttributeType: false,
            TotalPopulartags: 5,
            TagwordsSearch: ''
        };
        $scope.AddtreeOptionsNewChild = function (data) {
            var post = data.Children.length + 1;
            var newName = data.Caption + '-' + post;
            newName = "";
            var child_node = {
                Caption: newName,
                Level: data.Level + 1,
                id: 0,
                IsDeleted: false,
                Children: []
            };
            if (data.Caption != 'Categories') {
                child_node.Level = data.Level + 1;
            }
            if (data.Children) {
                data.Children.push(child_node);
                var newLevel = $.inArray(child_node.Level, $scope.wizard.TreeLevelNumberByNode);
                $scope.wizard.TreeLevelNumberByNode.push(child_node.Level);
                if (newLevel == -1) {
                    var level = {
                        Level: child_node.Level,
                        Caption: '',
                        ID: 0
                    }
                    $scope.wizard.TreeLevels.push(level);
                }
            } else {
                data.Children = [child_node];
            }
        };
        $scope.deletetreeOptionsNewChild = function (data, ParentTree) {
            $timeout(function () {
                var result = bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                    if (result) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                        if (data.id == 0) {
                            if (data.Children.length > 0) {
                                for (var i = 0; i <= data.Children.length - 1; i++) {
                                    var level = data.Children[i].Level;
                                    data.Children[i].IsDeleted = true;
                                    $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                    if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                        $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                                    }
                                    data.Children.splice($.inArray(data.Children[i], data.Children), 1);
                                }
                            }
                            var level = data.Level;
                            data.IsDeleted = true;
                            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                            }
                            ParentTree.Children.splice($.inArray(data, ParentTree.Children), 1);
                        } else {
                            AdminService.DeleteOptimakerCategory(data.id).then(function (newdata) {
                                if (newdata.Response == true) {
                                    if (data.Children.length > 0) {
                                        for (var i = 0; i <= data.Children.length - 1; i++) {
                                            if (data.Children[i].ID == 0) {
                                                var level = data.Children[i].Level;
                                                data.Children[i].IsDeleted = true;
                                                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                                    $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                                                }
                                                data.Children.splice($.inArray(data.Children[i], data.Children), 1);
                                            } else if (data.Children[i].IsDeleted == false) {
                                                var level = data.Children[i].Level;
                                                data.Children[i].IsDeleted = true;
                                                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                                    $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                                                }
                                            }
                                        }
                                        var level = data.Level;
                                        data.IsDeleted = true;
                                        $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                        if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                            $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                                        }
                                    } else {
                                        var level = data.Level;
                                        data.IsDeleted = true;
                                        $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                        if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                            $scope.wizard.TreeLevels.splice($.inArray(level, $scope.wizard.TreeLevels), 1);
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }, 20)
        };
        $scope.set_bgcolor = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
    }
    app.controller("mui.admin.optimakerCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', '$compile', 'AdminService', muiadminoptimakerCtrl]);
})(angular, app);