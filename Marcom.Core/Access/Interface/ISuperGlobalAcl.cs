﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{ 
    public interface ISuperGlobalAcl
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int GlobalRoleid
        {
            get;
            set;

        }

        int Menuid
        {
            get;
            set;

        }

    

        int Featureid
        {
            get;
            set;

        }

        bool AccessPermission
        {
            get;
            set;

        }




        #endregion
    }
}
