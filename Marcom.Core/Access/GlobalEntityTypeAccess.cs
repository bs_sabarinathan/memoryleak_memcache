﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{
    public class GlobalEntityTypeAccess : IGlobalEntityTypeAccess
    {
        #region Member Variables

        protected int _id;
		protected int _globalroleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected bool _accesspermission;
		
		
		#endregion
		
		#region Constructors
		public GlobalEntityTypeAccess() {}

        public GlobalEntityTypeAccess(int pId, int pGlobalRoleid, int pModuleid, int pEntityTypeid, bool pAccessPermission)
		{
            this._id = pId;
			this._globalroleid = pGlobalRoleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        
		public int GlobalRoleid
		{
			get { return _globalroleid; }
			set { _globalroleid = value; }
			
		}
		
		public int Moduleid
		{
			get { return _moduleid; }
			set { _moduleid = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}

        public bool AccessPermission
		{
			get { return _accesspermission; }
			set { _accesspermission = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			GlobalAcl castObj = null;
			try
			{
				castObj = (GlobalAcl)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
		
    }
}
