﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{

    public class LuceneResponse
    {
        private int IntCode;
        public int StatusCode
        {
            get { return IntCode; }
            set { IntCode = value; }
        }

        private dynamic DynResponse;

        public dynamic Response
        {
            get { return DynResponse; }
            set { DynResponse = value; }
        }

    }
}
