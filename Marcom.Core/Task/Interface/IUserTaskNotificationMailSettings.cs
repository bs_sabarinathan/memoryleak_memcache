using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    /// <summary>
    /// IUserNotificationMailSettings for table 'TM_UserNotificationMailSettings'.
    /// </summary>
    public interface IUserTaskNotificationMailSettings
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

        DateTimeOffset LastSentOn
        {
            get;
            set;

        }

        DateTimeOffset LastUpdatedOn
        {
            get;
            set;

        }

        bool IsNotificationEnable
        {
            get;
            set;

        }

        int NoOfDays
        {
            get;
            set;

        }

        TimeSpan NotificationTiming
        {
            get;
            set;

        }

        bool IsEmailEnable
        {
            get;
            set;

        }


        TimeSpan MailTiming
        {
            get;
            set;

        }

        bool IsEmailAssignedEnable
        { get; set; }

        #endregion
    }
}
