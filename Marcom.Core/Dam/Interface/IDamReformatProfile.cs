﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IDamReformatProfile
    {
        #region Public Properties

        int ID
        {
            get;
            set;
        }
        string Name
        {
            get;
            set;

        }       
        int Units
        {
            get;
            set;
        }
        string ProfileSrcValue
        {
            get;
            set;
        }




        #endregion
    }
}
