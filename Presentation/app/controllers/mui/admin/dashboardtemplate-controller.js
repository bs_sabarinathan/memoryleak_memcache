﻿(function (ng, app) {
    "use strict";

    function muiadmindashboardtemplateCtrl($window, $scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.DashboardView = "DashboardMain";
        $scope.DashboardTemplateName = '';
        $scope.DashboardTemplateId = 0;
        $scope.ISTemplate = true;
        $window.ISTemplate = true;
        $scope.dashboard = {
            inputTemplateName: '',
            inputTemplateDescription: '',
            ddlGlobalRole: 0,
            TemplateID: 0
        };
        AdminService.GetGlobalRole().then(function (res) {
            $scope.GlobalRoles = res.Response;
        });
        AdminService.GetWidgetTemplateByID(0).then(function (res) {
            $scope.listdata = res.Response;
            $scope.TemplateList = $scope.listdata;
            $(window).AdjustHeightWidth();
        });
        $scope.editableInPopup = '<a class="iconLink"  ng-click="GetModuleByID(row)" data-target="#moduleModal"><i class="icon-edit"></i></a> '
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridOptions = {
            data: 'TemplateList',
            enablePinning: false,
            enableColumnReordering: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60
            }, {
                field: "TemplateName",
                displayName: $translate.instant('LanguageContents.Res_1.Caption'),
                width: 120
            }, {
                field: "TemplateDescription",
                displayName: $translate.instant('LanguageContents.Res_22.Caption'),
                width: 250
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 60
            }]
        };
        $scope.GetModuleByID = function GetModuleByID(row) {

            $scope.dashboard.TemplateID = row.entity.Id;
            $scope.dashboard.inputTemplateName = row.entity.TemplateName;
            $scope.dashboard.inputTemplateDescription = row.entity.TemplateDescription;
            $scope.EnableUpdate = true;
            $scope.EnableAdd = false;
            $timeout(function () {
                $('#inputTemplateName').focus().select();
            }, 1000);
            AdminService.GetGlobalRole().then(function (res) {
                $scope.GlobalRoles = res.Response;
                AdminService.GetWidgetTemplateRolesByTemplateID(row.entity.Id).then(function (res) {
                    $scope.dashboard.ddlGlobalRole = res.Response;
                });
            });
            $('#moduleModal').modal('show');
        };
        $scope.addModule = function () {
            $('#moduleModal').modal('show');
            $scope.dashboard.TemplateID = 0;
            $scope.dashboard.inputTemplateName = "";
            $scope.dashboard.inputTemplateDescription = "";
            $scope.dashboard.ddlGlobalRole = 0;
            $scope.EnableUpdate = false;
            $scope.EnableAdd = true;
            $timeout(function () {
                $('#inputTemplateName').focus();
            }, 1000);
            
        };
        $scope.save = function () {
            if ($scope.dashboard.inputTemplateName != undefined && $scope.dashboard.inputTemplateName.length > 0) {
                var data = {};
                data.TemplateName = $scope.dashboard.inputTemplateName;
                $scope.DashboardTemplateName = $scope.dashboard.inputTemplateName;
                data.TemplateDescription = $scope.dashboard.inputTemplateDescription;
                AdminService.InsertWidgetTemplate(data).then(function (SaveWidgetTemplateDataResult) {
                    $scope.DashboardTemplateId = SaveWidgetTemplateDataResult.Response;
                    if ($scope.dashboard.ddlGlobalRole.length > 0) {
                        var temp = {};
                        temp.WidgetTemplateID = $scope.DashboardTemplateId;
                        temp.roleID = $scope.dashboard.ddlGlobalRole;
                        AdminService.InsertWidgetTemplateRoles(temp);
                    }
                    $('#moduleModal').modal('hide');
                    $scope.DashboardView = "DashboardCustomize";
                });
            } else (NotifyError($translate.instant('LanguageContents.Res_4647.Caption')))
        };
        $scope.update = function () {
            var data = {};
            data.ID = $scope.dashboard.TemplateID;
            data.TemplateName = $scope.dashboard.inputTemplateName;
            data.TemplateDescription = $scope.dashboard.inputTemplateDescription;
            $scope.DashboardTemplateName = $scope.dashboard.inputTemplateName;
            $scope.DashboardTemplateId = $scope.dashboard.TemplateID;
            AdminService.UpdateWidgetTemplateByID(data).then(function (res) {
                if (res.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4377.Caption'));
                } else {
                    if ($scope.dashboard.ddlGlobalRole.length > 0) {
                        var temp = {};
                        temp.WidgetTemplateID = $scope.dashboard.TemplateID;
                        temp.roleID = $scope.dashboard.ddlGlobalRole;
                        AdminService.InsertWidgetTemplateRoles(temp);
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                }
                var updateTemplateList = $.grep($scope.listdata, function (e) {
                    return e.Id == $scope.dashboard.TemplateID;
                });
                updateTemplateList[0].TemplateName = $scope.dashboard.inputTemplateName;
                updateTemplateList[0].TemplateDescription = $scope.dashboard.inputTemplateDescription;
                $('#moduleModal').modal('hide');
                $scope.DashboardView = "DashboardCustomize";
            });
        };

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.dashboardtemplateCtrl']"));
        });
    }
    app.controller("mui.admin.dashboardtemplateCtrl", ['$window', '$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadmindashboardtemplateCtrl]);
})(angular, app);