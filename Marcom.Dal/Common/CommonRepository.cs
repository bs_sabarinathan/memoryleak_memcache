﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;
using System.IO;
using System.Xml.Linq;
using System.Web;
using BrandSystems.Marcom.Dal.Common.Model;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using NHibernate.Criterion;
using BrandSystems.Marcom.Dal.Tenants;
using System.Configuration;
using BrandSystems.Marcom.Dal.Utility;

namespace BrandSystems.Marcom.Dal.Common
{
    public class CommonRepository : GenericRepository
    {
        public CommonRepository(ISessionFactory sessionFactory)
            : base(sessionFactory)
        {
        }
        public string GetXmlPath(int templateID, bool IsUserCustomized, int TenantID)
        {
            TenantSelection tfp = new TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePath(TenantID);
            string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + TenantFilePath;
            if (!IsUserCustomized)
                mappingfilesPath = mappingfilesPath + "DashboardXML" + @"\DashboardTemplate_" + templateID + ".xml";
            else
                mappingfilesPath = mappingfilesPath + "DashboardXML" + @"\UserDashboardTemplate_" + templateID + ".xml";

            if (File.Exists(mappingfilesPath))
            {
                return mappingfilesPath;
            }
            else
            {
                return null;
            }
        }

        public IList<T> GetObject<T>(XDocument xDoc)
        {
            try
            {
                //if (File.Exists(xmlpath))
                //{
                //    XDocument xDoc = XDocument.Load(xmlpath);
                IList<T> obj = null;
                if (typeof(T).Name == "WidgetDao" && xDoc.Descendants("Widget").Count() > 0)
                {
                    obj = xDoc.Root.Elements("Widget_Table").Elements("Widget").Select(e => new WidgetDao
                    {
                        ID = e.Element("ID").Value,
                        WidgetTypeID = Convert.ToInt32(e.Element("WidgetTypeID").Value),
                        IsDynamic = (e.Element("IsDynamic").Value == "true" ? true : false), //Convert.ToBoolean(Convert.ToInt32(e.Element("IsDynamic").Value)),
                        Row = Convert.ToInt32(e.Element("Row").Value),
                        Column = Convert.ToInt32(e.Element("Column").Value),
                        SizeX = Convert.ToInt32(e.Element("SizeX").Value),
                        SizeY = Convert.ToInt32(e.Element("SizeY").Value),
                        Caption = e.Element("Caption").Value,
                        VisualType = Convert.ToInt32(e.Element("VisualType").Value),
                        NoOfItem = Convert.ToInt32(e.Element("NoOfItem").Value),
                        NoOfYear = (e.Element("NoOfYear") != null ? Convert.ToInt32(e.Element("NoOfYear").Value) : 0),
                        NoOfMonth = (e.Element("NoOfMonth") != null ? Convert.ToInt32(e.Element("NoOfMonth").Value) : 0),
                        DimensionID = Convert.ToInt32(e.Element("DimensionID").Value),
                        MatrixID = e.Element("MatrixID").Value
                    }).Cast<T>().ToList();
                }

                return obj;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IList<T> GetDashboardObject<T>(string xmlpath)
        {
            try
            {
                if (File.Exists(xmlpath))
                {
                    XDocument xDoc = XDocument.Load(xmlpath);
                    IList<T> obj = null;
                    if (typeof(T).Name == "WidgetDao" && xDoc.Descendants("Widget").Count() > 0)
                    {
                        obj = xDoc.Root.Elements("Widget_Table").Elements("Widget").Select(e => new WidgetDao
                        {
                            ID = e.Element("ID").Value,
                            WidgetTypeID = Convert.ToInt32(e.Element("WidgetTypeID").Value),
                            IsDynamic = (e.Element("IsDynamic").Value == "true" ? true : false), //Convert.ToBoolean(Convert.ToInt32(e.Element("IsDynamic").Value)),
                            Row = Convert.ToInt32(e.Element("Row").Value),
                            Column = Convert.ToInt32(e.Element("Column").Value),
                            SizeX = Convert.ToInt32(e.Element("SizeX").Value),
                            SizeY = Convert.ToInt32(e.Element("SizeY").Value),
                            Caption = e.Element("Caption").Value,
                            VisualType = Convert.ToInt32(e.Element("VisualType").Value),
                            NoOfItem = Convert.ToInt32(e.Element("NoOfItem").Value),
                            NoOfYear = (e.Element("NoOfYear") != null ? Convert.ToInt32(e.Element("NoOfYear").Value) : 0),
                            NoOfMonth = (e.Element("NoOfMonth") != null ? Convert.ToInt32(e.Element("NoOfMonth").Value) : 0),
                            DimensionID = Convert.ToInt32(e.Element("DimensionID").Value),
                            MatrixID = e.Element("MatrixID").Value
                        }).Cast<T>().ToList();
                    }

                    return obj;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetDynamicWidgetQuery(string widgetID, int widgetTypeId, int dimensionid, string xmlPath, int TenantID)
        {
            try
            {
                if (File.Exists(xmlPath))
                {
                    XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlPath, TenantID);

                    var z = from t in docx.Root.Elements("Widget_Table").Elements("Widget").ToList() where Convert.ToInt32(t.Element("WidgetTypeID").Value) == widgetTypeId && Convert.ToInt32(t.Element("DimensionID").Value) == dimensionid && t.Element("ID").Value.ToString().Trim() == widgetID.ToString().Trim() select t.Element("WidgetQuery").Value;
                    return z.ElementAt(0);
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public string GetListofSelectEntityID(string widgetID, int widgetTypeId, int dimensionid, string xmlPath, int TenantID)
        {
            try
            {
                if (File.Exists(xmlPath))
                {
                    XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlPath, TenantID);
                    var z = from t in docx.Root.Elements("Widget_Table").Elements("Widget").ToList() where Convert.ToInt32(t.Element("WidgetTypeID").Value) == widgetTypeId && Convert.ToInt32(t.Element("DimensionID").Value) == dimensionid && t.Element("ID").Value.ToString().Trim() == widgetID.ToString().Trim() select t.Element("ListofSelectEntityID").Value;
                    return z.ElementAt(0).Replace("\r", "")
                               .Replace("\n", "");
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                return "";
            }

        }


        public XDocument SaveObject<T>(XDocument document1, BaseDao obj)
        {
            XElement subjectElement2 = null;
            XElement PoSettingXML = null;
            if (typeof(T).Name == "PurchaseOrderSettingsDao")
            {
                PurchaseOrderSettingsDao PoDao = (PurchaseOrderSettingsDao)obj;

                string Prefix = PoDao.Prefix;
                string DateFormat = PoDao.DateFormat;
                string DigitFormat = PoDao.DigitFormat;
                DateTime CreateDate = PoDao.CreateDate;

                //XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath,TenantID);


                //We need to add by default MaxId = 0 while creating the table
                //XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath,TenantID);
                XElement subjectElementworking = document1.Descendants("PurchaseOrderSettings_Table").FirstOrDefault();


                //subjectElementworking

                //Here we need to increase and read the max id
                //EntityDao.Id = maxid
                var maxid = subjectElementworking.Attribute("maxid");
                int maxids = Convert.ToInt32(maxid.Value) + 1;
                subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                PoDao.ID = maxids;
                PoSettingXML = GetPoOrderXElement(PoDao);
                subjectElement2 = PoSettingXML.Descendants("PurchaseOrderSettings_Table").FirstOrDefault();

                subjectElementworking.Add(subjectElement2.Nodes());
                //if (File.Exists(xmlpath))
                //{
                //    System.IO.File.Delete(xmlpath);
                //    System.Text.RegularExpressions.Regex.Replace(document1.ToString(), @"\s+", "");
                //    System.IO.File.WriteAllText(xmlpath, document1.ToString());
                //}


            }

            return document1;
        }

        public bool SaveObject<T>(int TenantID, string xmlpath, BaseDao obj)
        {
            XElement subjectElement2 = null;
            XElement PoSettingXML = null;
            if (typeof(T).Name == "PurchaseOrderSettingsDao")
            {
                PurchaseOrderSettingsDao PoDao = (PurchaseOrderSettingsDao)obj;

                string Prefix = PoDao.Prefix;
                string DateFormat = PoDao.DateFormat;
                string DigitFormat = PoDao.DigitFormat;
                DateTime CreateDate = PoDao.CreateDate;

                XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);


                //We need to add by default MaxId = 0 while creating the table
                XDocument document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                XElement subjectElementworking = document1.Descendants("PurchaseOrderSettings_Table").FirstOrDefault();
                if (subjectElementworking == null)
                {
                    CreateTableElement("PoSetting_Table", xmlpath, TenantID);
                    document1 = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);
                    subjectElementworking = document1.Descendants("PurchaseOrderSettings_Table").FirstOrDefault();
                }

                //subjectElementworking

                //Here we need to increase and read the max id
                //EntityDao.Id = maxid
                var maxid = subjectElementworking.Attribute("maxid");
                int maxids = Convert.ToInt32(maxid.Value) + 1;
                subjectElementworking.SetAttributeValue("maxid", maxids.ToString());
                PoDao.ID = maxids;
                PoSettingXML = GetPoOrderXElement(PoDao);
                subjectElement2 = PoSettingXML.Descendants("PurchaseOrderSettings_Table").FirstOrDefault();

                subjectElementworking.Add(subjectElement2.Nodes());
                if (File.Exists(xmlpath))
                {
                    System.IO.File.Delete(xmlpath);
                    System.Text.RegularExpressions.Regex.Replace(document1.ToString(), @"\s+", "");
                    System.IO.File.WriteAllText(xmlpath, document1.ToString());
                }


            }

            return true;
        }



        public XElement GetPoOrderXElement(BaseDao obj)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<root>");

            sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
            sb.AppendLine("<" + obj.GetType().Name.Replace("Dao", "") + ">");

            foreach (var item in obj.GetType().GetProperties())
            {
                sb.AppendLine("<" + item.Name + ">");
                sb.AppendLine(item.GetValue(obj, null).ToString());
                sb.AppendLine("</" + item.Name + ">");
            }

            sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + ">");
            sb.AppendLine("</" + obj.GetType().Name.Replace("Dao", "") + "_Table" + ">");
            sb.AppendLine("</root>");
            return XElement.Parse(sb.ToString().Trim(new char[] { '\n', '\r' }));

        }

        public void CreateTableElement(string TableName, string xmlpath, int TenantID)
        {
            XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath, TenantID);

            System.Xml.XmlDocument resXml = new System.Xml.XmlDocument();
            string xmlcontent = "";

            XmlElement attribRoot = resXml.CreateElement("root");
            resXml.AppendChild(attribRoot);

            XmlElement attribModTableRoot = resXml.CreateElement(TableName);
            //Prabhu
            attribModTableRoot.SetAttribute("maxid", "0");

            attribRoot.AppendChild(attribModTableRoot);
            xmlcontent = resXml.OuterXml.ToString();


            XElement subjectElementworking1 = docx.Descendants("root").FirstOrDefault();
            XDocument rootdoc = XDocument.Parse(xmlcontent);
            XElement subjectElement1 = rootdoc.Descendants("root").FirstOrDefault();


            subjectElementworking1.Add(subjectElement1.Nodes());

            if (File.Exists(xmlpath))
            {
                System.IO.File.Delete(xmlpath);
                System.IO.File.WriteAllText(xmlpath, docx.ToString());
            }
        }

        public XDocument CreateTableElement(string TableName, XDocument docx)
        {
            //XDocument docx = MarcomCacheDal<XDocument>.ReadXML(xmlpath,TenantID);

            System.Xml.XmlDocument resXml = new System.Xml.XmlDocument();
            string xmlcontent = "";

            XmlElement attribRoot = resXml.CreateElement("root");
            resXml.AppendChild(attribRoot);

            XmlElement attribModTableRoot = resXml.CreateElement(TableName);
            //Prabhu
            attribModTableRoot.SetAttribute("maxid", "0");

            attribRoot.AppendChild(attribModTableRoot);
            xmlcontent = resXml.OuterXml.ToString();


            XElement subjectElementworking1 = docx.Descendants("root").FirstOrDefault();
            XDocument rootdoc = XDocument.Parse(xmlcontent);
            XElement subjectElement1 = rootdoc.Descendants("root").FirstOrDefault();


            subjectElementworking1.Add(subjectElement1.Nodes());

            //if (File.Exists(xmlpath))
            //{
            //    System.IO.File.Delete(xmlpath);
            //    System.IO.File.WriteAllText(xmlpath, docx.ToString());
            //}

            return docx;
        }


        public string XmlToJSON(XmlDocument xmlDoc)
        {
            StringBuilder sbJSON = new StringBuilder();
            sbJSON.Append("{ ");
            XmlToJSONnode(sbJSON, xmlDoc.DocumentElement, true);
            sbJSON.Append("}");
            return sbJSON.ToString();
        }

        //  XmlToJSONnode:  Output an XmlElement, possibly as part of a higher array
        public void XmlToJSONnode(StringBuilder sbJSON, XmlElement node, bool showNodeName)
        {
            bool childAdded = false;
            if (showNodeName)
                sbJSON.Append("\"" + SafeJSON(node.Name) + "\": ");
            sbJSON.Append("{");
            // Build a sorted list of key-value pairs
            //  where   key is case-sensitive nodeName
            //          value is an ArrayList of string or XmlElement
            //  so that we know whether the nodeName is an array or not.
            SortedList childNodeNames = new SortedList();

            //  Add in all node attributes
            if (node.Attributes != null)
                foreach (XmlAttribute attr in node.Attributes)
                    StoreChildNode(childNodeNames, attr.Name, attr.InnerText);

            //  Add in all nodes
            foreach (XmlNode cnode in node.ChildNodes)
            {
                //childAdded = true;
                //if (cnode is XmlText)
                //    StoreChildNode(childNodeNames, "value", cnode.InnerText);
                //else if (cnode is XmlElement)
                //    StoreChildNode(childNodeNames, cnode.Name, cnode);

                childAdded = true;
                if (cnode.ChildNodes[0] is XmlCDataSection)
                    StoreChildNode(childNodeNames, cnode.Name, cnode.ChildNodes[0].InnerText);
                else if (cnode is XmlText)
                    StoreChildNode(childNodeNames, "value", cnode.InnerText);
                else if (cnode is XmlElement)
                    StoreChildNode(childNodeNames, cnode.Name, cnode);
            }



            // Now output all stored info
            foreach (string childname in childNodeNames.Keys)
            {
                childAdded = true;
                ArrayList alChild = (ArrayList)childNodeNames[childname];
                bool bFlag = false;
                foreach (object oChild in alChild) bFlag = true;
                if (alChild.Count == 1)
                    OutputNode(childname, alChild[0], sbJSON, true);
                else
                {
                    sbJSON.Append(" \"" + SafeJSON(childname) + "\": [ ");
                    foreach (object Child in alChild)
                        OutputNode(childname, Child, sbJSON, false);
                    sbJSON.Remove(sbJSON.Length - 2, 2);
                    sbJSON.Append(" ], ");
                }
            }
            sbJSON.Remove(sbJSON.Length - 2, 2);
            if (childAdded)
            {
                sbJSON.Append(" }");
            }
            else
            {
                sbJSON.Append(" null");
            }
        }

        //  StoreChildNode: Store data associated with each nodeName
        //                  so that we know whether the nodeName is an array or not.
        private void StoreChildNode(SortedList childNodeNames, string nodeName, object nodeValue)
        {
            // Pre-process contraction of XmlElement-s
            if (nodeValue is XmlElement)
            {
                // Convert  <aa></aa> into "aa":null
                //          <aa>xx</aa> into "aa":"xx"
                XmlNode cnode = (XmlNode)nodeValue;
                if (cnode.Attributes.Count == 0)
                {
                    XmlNodeList children = cnode.ChildNodes;
                    if (children.Count == 0)
                        nodeValue = null;
                    else if (children.Count == 1 && (children[0] is XmlText))
                        nodeValue = ((XmlText)(children[0])).InnerText;
                }
            }
            // Add nodeValue to ArrayList associated with each nodeName
            // If nodeName doesn't exist then add it
            object oValuesAL = childNodeNames[nodeName];
            ArrayList ValuesAL;
            if (oValuesAL == null)
            {
                ValuesAL = new ArrayList();
                childNodeNames[nodeName] = ValuesAL;
            }
            else
                ValuesAL = (ArrayList)oValuesAL;
            ValuesAL.Add(nodeValue);
        }

        private void OutputNode(string childname, object alChild, StringBuilder sbJSON, bool showNodeName)
        {


            if (alChild == null)
            {
                if (showNodeName)
                    sbJSON.Append("\"" + SafeJSON(childname) + "\": ");
                sbJSON.Append("null");
            }
            else if (alChild is string)
            {
                if (showNodeName)
                    sbJSON.Append("\"" + SafeJSON(childname) + "\": ");
                string sChild = (string)alChild;
                sChild = sChild.Trim();
                sbJSON.Append("\"" + SafeJSON(sChild) + "\"");
            }
            else
                XmlToJSONnode(sbJSON, (XmlElement)alChild, showNodeName);
            string temp2 = sbJSON.ToString().Trim();
            if (temp2.Substring(temp2.Length - 1) != ",")
                sbJSON.Append(", ");
        }

        // Make a string safe for JSON
        private string SafeJSON(string sIn)
        {
            StringBuilder sbOut = new StringBuilder(sIn.Length);
            foreach (char ch in sIn)
            {
                if (Char.IsControl(ch) || ch == '\'')
                {
                    int ich = (int)ch;
                    sbOut.Append(@"\u" + ich.ToString("x4"));
                    continue;
                }
                else if (ch == '\"' || ch == '\\' || ch == '/')
                {
                    sbOut.Append('\\');
                }
                sbOut.Append(ch);
            }
            return sbOut.ToString();
        }

    }
}
