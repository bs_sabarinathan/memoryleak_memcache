﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class AttributeGroupAttributeRelation : IAttributeGroupAttributeRelation
    {
        #region Member Variables
        protected int _id;
        protected int _attributegroupid;
        protected string _caption;
        protected int _attributeid;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected bool _issearchable;
        protected bool _showascolumn;

        #endregion

        #region Constructors
        public AttributeGroupAttributeRelation() { }

        public AttributeGroupAttributeRelation(int pID, int pAttributeGroupId, int pAttributeId, string pCaption, bool pIsSearchable, bool pShowAsColumn)
        {
            this._id = pID;
            this._attributegroupid = pAttributeGroupId;
            this._attributeid = pAttributeId;
            this._caption = pCaption;
            this._issearchable = pIsSearchable;
            this._showascolumn = pShowAsColumn;
        }

        public AttributeGroupAttributeRelation(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int AttributeGroupID
        {
            get { return _attributegroupid; }
            set { _attributegroupid = value; }
        }

        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }
        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }
        }

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }
        }

        public bool IsSearchable
        {
            get { return _issearchable; }
            set { _issearchable = value; }
        }

        public bool ShowAsColumn
        {
            get { return _showascolumn; }
            set { _showascolumn = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeAttributeRelation castObj = null;
            try
            {
                castObj = (EntityTypeAttributeRelation)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }
}
