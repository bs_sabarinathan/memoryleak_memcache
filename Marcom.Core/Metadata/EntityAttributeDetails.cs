﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class EntityAttributeDetails : IEntityAttributeDetails
    {
        #region Public Properties
        public int EntityId { get; set; }
        public bool IsLock { get; set; }
        public string ActivityName { get; set; }
        public string Owner { get; set; }
        #endregion
    }
}
