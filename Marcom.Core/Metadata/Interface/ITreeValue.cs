using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// ITreeValue interface for table 'MM_TreeValue'.

    /// </summary>
    public interface ITreeValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;
        }

        int Attributeid
        {
            get;
            set;

        }
        int Nodeid
        {
            get;
            set;

        }
        int Level
        {
            get;
            set;
        }

        string Value
        {
            get;
            set;
        }

        int ParentNode
        {
            get;
            set;
        }

        #endregion
    }
}
