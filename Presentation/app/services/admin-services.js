﻿app.service('AdminService', function ($http, $q) {
    $http.defaults.headers.common.sessioncookie = $.cookie('Session');
    return ({

        GetGlobalRoleUserByID: GetGlobalRoleUserByID,
        GetSuperglobalacl: GetSuperglobalacl,

        //super admin
        GetGlobalRole: GetGlobalRole,
        GetSuperAdminModule: GetSuperAdminModule,
        DeleteGlobalByID: DeleteGlobalByID,
        SaveUpdateSuperAdminRoleFeatures: SaveUpdateSuperAdminRoleFeatures,

        //general setting 
        GetAdminSettingselemntnode: GetAdminSettingselemntnode,
        GetTitleLogoSettings: GetTitleLogoSettings,
        UpdateTitleSettings: UpdateTitleSettings,
        GetNavigationByIDandParentid: GetNavigationByIDandParentid,
        UpdateNavigationSortOrder: UpdateNavigationSortOrder,
        UpdateNavigationByID: UpdateNavigationByID,
        InsertNavigation: InsertNavigation,
        DeleteNavigation: DeleteNavigation,
        GetModuleFeaturesForNavigation: GetModuleFeaturesForNavigation,
        GetModule: GetModule,
        GetEmailIds: GetEmailIds,
        InsertAdminSettingXML: InsertAdminSettingXML,
        InsertUpdateAdditionalSettings: InsertUpdateAdditionalSettings,
        UpdatePopularTagWordsToShow: UpdatePopularTagWordsToShow,
        UpdateExpirytime: UpdateExpirytime,
        Getexpirytime: Getexpirytime,
        GetTotalPopularTagWordsToShow: GetTotalPopularTagWordsToShow,
        InsertNotifydisplaytime: InsertNotifydisplaytime,
        Getnotifytimesetbyuser: Getnotifytimesetbyuser,
        Getnotifyrecycletimesetbyuser: Getnotifyrecycletimesetbyuser,
        InsertNotifycycledisplaytime: InsertNotifycycledisplaytime,
        GetDecimalSettingsValue: GetDecimalSettingsValue,
        InsertUpdateDecimalSettings: InsertUpdateDecimalSettings,
        updateDefaultFolderInfo: updateDefaultFolderInfo,
        UpdateNewFeedconfig: UpdateNewFeedconfig,
        GetNewsFeedConfigInfo: GetNewsFeedConfigInfo,
        getDefaultFolderInfo: getDefaultFolderInfo,
        GetAllCurrencyType: GetAllCurrencyType,
        GetLanguageTypes: GetLanguageTypes,
        SetDefaultLanguage: SetDefaultLanguage,
        GetObjectLanguageContent: GetObjectLanguageContent,
        GetLanguageContent: GetLanguageContent,
        LanguageSearchs: LanguageSearchs,
        SaveNewLanguage: SaveNewLanguage,
        UpdateLanguageContent: UpdateLanguageContent,
        UpdateLanguageName: UpdateLanguageName,
        GetlanguagetypeByID: GetlanguagetypeByID,
        InsertNewLanguage: InsertNewLanguage,
        GetLanguageExport: GetLanguageExport,
        InsertLanguageImport: InsertLanguageImport,
        UpdateJsonLanguageContent: UpdateJsonLanguageContent,
        GetAllSubscriptionType: GetAllSubscriptionType,
        InsertAdminNotificationSettings: InsertAdminNotificationSettings,
        GetNavigationType: GetNavigationType,
        GetUserById: GetUserById,
        Updateadditionalsettingsstartpage: Updateadditionalsettingsstartpage,

        GetFeedTemplates: GetFeedTemplates,
        DeleteFeedGroupByid: DeleteFeedGroupByid,
        InsertUpdateFeedFilterGroup: InsertUpdateFeedFilterGroup,
        UpdateSearchEngine: UpdateSearchEngine,
        GetAllGanttHeaderBar: GetAllGanttHeaderBar,
        InsertUpdateGanttHeaderBar: InsertUpdateGanttHeaderBar,
        DeleteGanttHeaderBar: DeleteGanttHeaderBar,
        GetUniqueuserhit: GetUniqueuserhit,
        GetApplicationhit: GetApplicationhit,
        GetBrowserStatistic: GetBrowserStatistic,
        GetBrowserVersionStatistic: GetBrowserVersionStatistic,
        GetUserStatistic: GetUserStatistic,
        GetOSStatistic: GetOSStatistic,
        GetstartpageStatistic: GetstartpageStatistic,
        GetUserRoleStatistic: GetUserRoleStatistic,
        GetEnityStatistic: GetEnityStatistic,
        GetEnityCreateationStatistic: GetEnityCreateationStatistic,
        GetbandwidthStatistic: GetbandwidthStatistic,
        GetProofInitiatorStatistic: GetProofInitiatorStatistic,
        GetBroadcastMessages: GetBroadcastMessages,
        InsertBroadcastMessages: InsertBroadcastMessages,
        UpdateCustomTabSortOrder: UpdateCustomTabSortOrder,
        GetCustomTabSettingDetails: GetCustomTabSettingDetails,
        GetCustomTabsByTypeID: GetCustomTabsByTypeID,
        GetCustomTabEncryptionByID: GetCustomTabEncryptionByID,
        InsertUpdateCustomTab: InsertUpdateCustomTab,
        GetCustomTabAccessByID: GetCustomTabAccessByID,
        DeleteCustomtabByID: DeleteCustomtabByID,
        GetEntityTypeIsAssociate: GetEntityTypeIsAssociate,
        GetUpdateSettings: GetUpdateSettings,
        GetPasswordPolicyDetails: GetPasswordPolicyDetails,
        UpdatePasswordPolicy: UpdatePasswordPolicy,
        GetAttributeSearchCriteria: GetAttributeSearchCriteria,
        GetSearchCriteriaAdminSettings: GetSearchCriteriaAdminSettings,
        SearchadminSettingsforRootLevelInsertUpdate: SearchadminSettingsforRootLevelInsertUpdate,
        SaveHolidayDetails: SaveHolidayDetails,
        InsertHolidayDetails: InsertHolidayDetails,
        GetNonBusinessDays: GetNonBusinessDays,
        GetHolidaysDetails: GetHolidaysDetails,
        DeleteHoliday: DeleteHoliday,
        getcloudsettings: getcloudsettings,
        savecloudsettings: savecloudsettings,
        DeleteAssetTypeFileExtension: DeleteAssetTypeFileExtension,
        //reports
        GetDataviewbyusername: GetDataviewbyusername,
        GetReportCredentialByID: GetReportCredentialByID,
        validateReportCredential: validateReportCredential,
        ReportLogin: ReportLogin,
        GetReportByOID: GetReportByOID,
        InsertUpdateReport: InsertUpdateReport,
        UpdateReportImage: UpdateReportImage,
        GetReportViewSchemaResponse: GetReportViewSchemaResponse,
        DeleteView: DeleteView,
        GetViews: GetViews,
        InsertCustomViews: InsertCustomViews,
        GetViewsValidate: GetViewsValidate,
        pushviewSchema: pushviewSchema,
        CustomViews_Update: CustomViews_Update,
        GetAlltableswithmetadata: GetAlltableswithmetadata,
        GetAlltablesnames: GetAlltablesnames,
        ManipulateQueryEditorQuery: ManipulateQueryEditorQuery,
        GetAllEntityTypeAttributeRelationWithLevels: GetAllEntityTypeAttributeRelationWithLevels,
        GetFinancialReportSettings: GetFinancialReportSettings,
        GetReportJSONData: GetReportJSONData,
        DeletefinancialreportByID: DeletefinancialreportByID,
        insertupdatefinancialreportsettings: insertupdatefinancialreportsettings,
        UpdateFinancialSettingsReportImage: UpdateFinancialSettingsReportImage,
        GetAttributefromDB: GetAttributefromDB,
        AdminSettingsforReportInsertUpdate: AdminSettingsforReportInsertUpdate,
        GetTaskFulfillmentEntityTypes: GetTaskFulfillmentEntityTypes,
        GetAllCustomList: GetAllCustomList,
        DeleteCustomList: DeleteCustomList,
        ValidateCustomList: ValidateCustomList,
        InsertUpdateCustomlist: InsertUpdateCustomlist,
        GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
        GetFulfillmentAttribute: GetFulfillmentAttribute,
        GetAttributeTypeByEntityTypeID: GetAttributeTypeByEntityTypeID,
        GetAdminSettings: GetAdminSettings,
        AdminSettingsforRootLevelInsertUpdate: AdminSettingsforRootLevelInsertUpdate,
        GettingEntityTypeHierarchyForAdminTree: GettingEntityTypeHierarchyForAdminTree,
        GetAttributesforDetailFilter: GetAttributesforDetailFilter,
        GetPlantabsettings: GetPlantabsettings,
        UpdatePlanTabIds: UpdatePlanTabIds,
        insertupdatetabsettings: insertupdatetabsettings,
        GetLayoutData: GetLayoutData,
        LayoutSettingsApplyChanges: LayoutSettingsApplyChanges,
        GetCurrencyListFFsettings: GetCurrencyListFFsettings,
        getDivisonIds: getDivisonIds,
        SetDivisonsFFSettings: SetDivisonsFFSettings,
        GetDivisonName: GetDivisonName,
        DeleteCurrencyListFFSettings: DeleteCurrencyListFFSettings,
        InsertUpdateCurrencyListFFSettings: InsertUpdateCurrencyListFFSettings,
        GetFinancialforecastData: GetFinancialforecastData,
        UpdateFFData: UpdateFFData,
        GetAdditionalSettings: GetAdditionalSettings,
        getfinancialForecastIds: getfinancialForecastIds,
        GetFinancialForecastsettings: GetFinancialForecastsettings,
        updatefinancialforecastsettings: updatefinancialforecastsettings,
        GetPoSSettings: GetPoSSettings,
        GetCurrentPONumber: GetCurrentPONumber,
        InsertPoSettingXML: InsertPoSettingXML,
        GetExchangesratesbyCurrencytype: GetExchangesratesbyCurrencytype,
        GetCurrencyconverter: GetCurrencyconverter,
        Insertupdatecurrencyconverter: Insertupdatecurrencyconverter,
        DeleteCurrencyconverter: DeleteCurrencyconverter,
        GetFinancialAttribute: GetFinancialAttribute,
        DeleteFinancialOption: DeleteFinancialOption,
        DeleteFinancialAttribute: DeleteFinancialAttribute,
        addFinancialAttribute: addFinancialAttribute,
        addFinancialOption: addFinancialOption,
        GetFinancialAttributeOptions: GetFinancialAttributeOptions,
        UpdateFinancialMetadata: UpdateFinancialMetadata,
        UpdateFinMetadataSortOrder: UpdateFinMetadataSortOrder,
        GetAttributeDAMCreate: GetAttributeDAMCreate,
        GetAttributetype: GetAttributetype,
        DAMGetAdminSettings: DAMGetAdminSettings,
        GetDAMViewAdminSettings: GetDAMViewAdminSettings,
        DAMadminSettingsforRootLevelInsertUpdate: DAMadminSettingsforRootLevelInsertUpdate,
        UpdateDamViewStatus: UpdateDamViewStatus,
        GetEntityType: GetEntityType,
        GetReformatProfile: GetReformatProfile,
        GetMimeType: GetMimeType,
        CreateReformatProfile: CreateReformatProfile,
        getServerUrls: getServerUrls,
        GetOptimakerSettings: GetOptimakerSettings,
        GetWordTemplateSettings: GetWordTemplateSettings,
        GetOptimakerCatagories: GetOptimakerCatagories,
        copydeleteTemplateAsset: copydeleteTemplateAsset,
        InsertUpdateOptimakerSettings: InsertUpdateOptimakerSettings,
        DeleteOptimakeSetting: DeleteOptimakeSetting,
        GetCategoryTreeCollection: GetCategoryTreeCollection,
        InsertUpdateWordTemplateSettings: InsertUpdateWordTemplateSettings,
        DeleteWordtempSetting: DeleteWordtempSetting,
        InsertUpdateCatergory: InsertUpdateCatergory,
        DeleteOptimakerCategory: DeleteOptimakerCategory,
        GetAssetTypeAssetCategoryRelation: GetAssetTypeAssetCategoryRelation,
        GetAssetCategoryTreeCollection: GetAssetCategoryTreeCollection,
        InsertUpdateAssetCatergory: InsertUpdateAssetCatergory,
        DeleteAssetCategory: DeleteAssetCategory,
        GetSelectedCategoryTreeCollection: GetSelectedCategoryTreeCollection,
        InsertUpdateAssetCategoryRelation: InsertUpdateAssetCategoryRelation,
        InsertAssetTypeFileExtension: InsertAssetTypeFileExtension,
        getsnippethoturl: getsnippethoturl,
        getassetSnippetData: getassetSnippetData,
        GetAssetSnippetTemplates: GetAssetSnippetTemplates,
        saveTemplateEngine: saveTemplateEngine,
        deleteTemplateEngine: deleteTemplateEngine,
        getAllTemplateEngines: getAllTemplateEngines,
        GetUnits: GetUnits,
        InsertUpdateUnits: InsertUpdateUnits,
        DeleteUnitsByid: DeleteUnitsByid,
        GetAllCmsSnippetTemplates: GetAllCmsSnippetTemplates,
        InsertCmsSnippetTemplate: InsertCmsSnippetTemplate,
        UpdateSnippetTemplate: UpdateSnippetTemplate,
        DuplicateCmsSnippetTemplate: DuplicateCmsSnippetTemplate,
        SaveImageFromBaseFormat: SaveImageFromBaseFormat,
        getTenantClientPath: getTenantClientPath,
        GetListOfCmsCustomFont: GetListOfCmsCustomFont,
        InsertUpdateCmsCustomFont: InsertUpdateCmsCustomFont,
        DeleteCmsCustomFont: DeleteCmsCustomFont,
        InsertUpdateCmsCustomStyle: InsertUpdateCmsCustomStyle,
        DeleteCmsCustomStyle: DeleteCmsCustomStyle,
        GetListOfCmsCustomStyle: GetListOfCmsCustomStyle,
        GetCmsTreeStyle: GetCmsTreeStyle,
        InsertUpdateCmsTreeStyleSettings: InsertUpdateCmsTreeStyleSettings,
        GetAssetTypeFileExtensionRelation: GetAssetTypeFileExtensionRelation,
        GetObjectiveTypeAttributeByEntityType: GetObjectiveTypeAttributeByEntityType,
        GetThemeValues: GetThemeValues,
        GetThemeData: GetThemeData,
        UpdateThemeSettings: UpdateThemeSettings,
        RestoreDefaultTheme: RestoreDefaultTheme,

        //-------user-controller----------//

        //AdminService
        GetUserDetailsAttributes: GetUserDetailsAttributes,
        GetNavigationType: GetNavigationType,
        GetAssetAccess: GetAssetAccess,
        GetValidationDationByEntitytype: GetValidationDationByEntitytype,
        //UserService
        CheckUserInvolvement: CheckUserInvolvement,
        DeleteUser: DeleteUser,
        GetApprovalRoleUserByID: GetApprovalRoleUserByID,
        GetUsers: GetUsers,
        GetApprovalrole: GetApprovalrole,
        InsertUser: InsertUser,
        InsertApprovalUserRoles: InsertApprovalUserRoles,
        UpdateUser: UpdateUser,

        //AccessService

        GetAssetAccessByID: GetAssetAccessByID,
        GetGlobalRoleUserByID: GetGlobalRoleUserByID,
        InsertGlobalRoleUser: InsertGlobalRoleUser,
        InsertAssetAccess: InsertAssetAccess,

        //-----------apiuser------------//
        //userservice
        GenerateGuidforSelectedAPI: GenerateGuidforSelectedAPI,
        GetAPIusersDetails: GetAPIusersDetails,
        GetAccLockDetails: GetAccLockDetails,
        //-------Acclock----------------//

        UpdateAccLockPasswordPolicy: UpdateAccLockPasswordPolicy,
        SendPasswordResetMail: SendPasswordResetMail,
        GetLockuserDetails: GetLockuserDetails,

        //---------role-controller----------//
        DeleteGlobalRole: DeleteGlobalRole,

        GetSelectedFeedFilter: GetSelectedFeedFilter,
        GetSelectedNotificationFilter: GetSelectedNotificationFilter,
        InsertUpdateGlobalRole: InsertUpdateGlobalRole,
        InsertUpdateGlobalEntitTypeACL: InsertUpdateGlobalEntitTypeACL,
        GetGlobalEntityTypeAcl: GetGlobalEntityTypeAcl,
        GettingEntityTypeTreeStructure: GettingEntityTypeTreeStructure,
        GetRoleFeatures: GetRoleFeatures,
        SaveUpdateRoleFeatures: SaveUpdateRoleFeatures,
        GetFeedFilter: GetFeedFilter,
        SaveUpdateNewsfeedRoleFeatures: SaveUpdateNewsfeedRoleFeatures,
        SaveUpdateNotificationRoleFeatures: SaveUpdateNotificationRoleFeatures,

        //--pendinguser--//
        GetPendingUsers: GetPendingUsers,
        ApproveRejectRegisteredUsers: ApproveRejectRegisteredUsers,

        //--module--//

        GetAllModuleFeatures: GetAllModuleFeatures,
        UpdateFeature: UpdateFeature,
        Module: Module,

        //-----sso---///
        GetSSODetails: GetSSODetails,
        UpdateSSOSettings: UpdateSSOSettings,

        //---asset access--////
        AssetAccess: AssetAccess,
        DeleteAssetAccessOption: DeleteAssetAccessOption,
        GetAssestAccessSaved: GetAssestAccessSaved,

        //-----approvalrole------////
        GetApprovalRoles: GetApprovalRoles,
        InsertApprovalRole: InsertApprovalRole,
        DeleteApprovalRole: DeleteApprovalRole,

        //---dalim----///
        SaveUpdateDalimUser: SaveUpdateDalimUser,
        DeleteDalimUser: DeleteDalimUser,
        GetDalimUser: GetDalimUser,

        //=----dashboard--//
        GetWidgetTemplateByID: GetWidgetTemplateByID,
        GetWidgetTypesByID: GetWidgetTypesByID,
        GetWidgetTypeRolesByID: GetWidgetTypeRolesByID,
        InsertWidgetTemplate: InsertWidgetTemplate,

        //--widgettype--//
        UpdateWidgetTemplateByID: UpdateWidgetTemplateByID,
        InsertWidgetTypeRoles: InsertWidgetTypeRoles,
        InsertWidgetTemplateRoles: InsertWidgetTemplateRoles,
        GetWidgetTemplateRolesByTemplateID: GetWidgetTemplateRolesByTemplateID,

        //---dalimcredential--//
        GetDalimSettings: GetDalimSettings,
        UpdateDalimSettings: UpdateDalimSettings,
        GetApprovalFlowLibraryList: GetApprovalFlowLibraryList,

        DeleteTaskFlagByID: DeleteTaskFlagByID,
        GetTaskFlags: GetTaskFlags,
        AddUpdateTaskFlag: AddUpdateTaskFlag,
        gettaskflagstatus: gettaskflagstatus,
        EnableDisableTaskflag: EnableDisableTaskflag,
        GetAllApprovalFlowPhasesSteps: GetAllApprovalFlowPhasesSteps,
        InsertUpdateTaskTemplate: InsertUpdateTaskTemplate,
        GetAllApprovalFlowTemplates: GetAllApprovalFlowTemplates,
        DeleteApprovalTemplate: DeleteApprovalTemplate,
        InsertUpdateTaskTemplateStep: InsertUpdateTaskTemplateStep,
        InsertUpdateTaskTemplatePhase: InsertUpdateTaskTemplatePhase,
        UpdatetaskTemplateSortOrder: UpdatetaskTemplateSortOrder,
        DeleteApprovalTaskStep: DeleteApprovalTaskStep,
        UpdateProofHQSSettings: UpdateProofHQSSettings,
        GetProofHQSSettings: GetProofHQSSettings,
        GetAssetCategoryTree: GetAssetCategoryTree,
        GetAssetCategoryPathInAssetEdit: GetAssetCategoryPathInAssetEdit,
        //---- publish access---------------//
        Getpublishaccess: Getpublishaccess,
        Updatepublishaccess: Updatepublishaccess,


        //------attributegrouptooltip----------//

        GetAllAttributeGroup: GetAllAttributeGroup,
        GetAttrGroupTooltipValues: GetAttrGroupTooltipValues,
        UpdateAttributeGroupTooltip: UpdateAttributeGroupTooltip,


        GetEntityTypeAttributes: GetEntityTypeAttributes,
        updateentitytypehelptext: updateentitytypehelptext,
        GetAllFinancialAttribute: GetAllFinancialAttribute,
        UpdateFinancialHelptext: UpdateFinancialHelptext,
        InsertEntityHelpTextImport: InsertEntityHelpTextImport,
        ExportEntityHelpTextListtoExcel: ExportEntityHelpTextListtoExcel,

        //--------------EntityStatus-controller---------------//
        GetEntityTemplateDetails: GetEntityTemplateDetails,
        InsertUpdateEntityTemplate: InsertUpdateEntityTemplate,
        GetEntityTypeStatus: GetEntityTypeStatus,
        DeleteEntityTemplateListById: DeleteEntityTemplateListById,
        UpdateEntityTypeOverallStatus: UpdateEntityTypeOverallStatus,
        getEntityToSetOverallStauts: getEntityToSetOverallStauts,

        //Tableau Report
        GettableauReportSettings: GettableauReportSettings,
        DeletetableaureportByID: DeletetableaureportByID,
        insertupdatetableaureportsettings: insertupdatetableaureportsettings,
        UpdatetableauSettingsReportImage: UpdatetableauSettingsReportImage,


        //Financial view admin settings controller
        GetFinancialViews: GetFinancialViews,
        GetFinancialViewColumns: GetFinancialViewColumns,
        InsertFinacialColumns: InsertFinacialColumns,
        UpdateFinancialViews: UpdateFinancialViews,
        DeleteFinancialViews: DeleteFinancialViews,
        EditFinancialView: EditFinancialView,
        GetListFinancialView: GetListFinancialView,

        getPlanandObjTypes: getPlanandObjTypes,
        GetUserRegAdminSettings: GetUserRegAdminSettings,
        GetUserRegLogoSettings: GetUserRegLogoSettings,
        UpdateUserRegSettings: UpdateUserRegSettings,


        // Cache Related Services

        getmetadataLockStatus: getmetadataLockStatus,
        setmetadataLockStatus: setmetadataLockStatus,
        clearCache: clearCache
    });
    

    function GetAdminSettingselemntnode(LogoSettings, elemntnode, typeid) {
        var request = $http({
            method: "get",
            url: "api/common/GetAdminSettingselemntnode/" + LogoSettings + "/" + elemntnode + "/" + typeid,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //-------user-controller----------//
    //AdminService
    function GetUserDetailsAttributes(TypeID, UserID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetUserDetailsAttributes/" + TypeID + "/" + UserID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetNavigationType() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetNavigationType/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetAccess() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAssetAccess/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetValidationDationByEntitytype(EntityTypeID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //UserService
    function CheckUserInvolvement(ID) {
        var request = $http({
            method: "get",
            url: "api/user/CheckUserInvolvement/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetApprovalRoleUserByID(ID) {
        var request = $http({
            method: "get",
            url: "api/User/GetApprovalRoleUserByID/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteUser(ID) {
        var request = $http({
            method: "delete",
            url: "api/user/DeleteUser/" + ID,
            params: {
                action: "delete",
            },
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetUsers() {
        var request = $http({
            method: "get",
            url: "api/user/GetUsers/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetApprovalrole() {
        var request = $http({
            method: "get",
            url: "api/User/GetApprovalrole/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUser(jobj) {
        var request = $http({
            method: "post",
            url: "api/user/InsertUser/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertApprovalUserRoles(jobj) {
        var request = $http({
            method: "post",
            url: "api/User/InsertApprovalUserRoles/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateUser(jobj) {
        var request = $http({
            method: "put",
            url: "api/user/UpdateUser/",
            params: {
                action: "update"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //AccessService

    function GetAssetAccessByID(ID) {
        var request = $http({
            method: "get",
            url: "api/access/GetAssetAccessByID/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetGlobalRoleUserByID(ID) {
        var request = $http({
            method: "get",
            url: "api/access/GetGlobalRoleUserByID/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertGlobalRoleUser(jobj) {
        var request = $http({
            method: "post",
            url: "api/access/InsertGlobalRoleUser/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertAssetAccess(jobj) {
        var request = $http({
            method: "post",
            url: "api/access/InsertAssetAccess/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }


    //----------apiuser--------------//
    function GenerateGuidforSelectedAPI(jobj) {
        var request = $http({
            method: "post",
            url: "api/user/GenerateGuidforSelectedAPI/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAPIusersDetails() {
        var request = $http({
            method: "get",
            url: "api/user/GetAPIusersDetails/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //----------acclock-----------//
    function GetAccLockDetails() {
        var request = $http({
            method: "get",
            url: "api/Common/GetAccLockDetails/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateAccLockPasswordPolicy(jobj) {
        var request = $http({
            method: "post",
            url: "api/Common/UpdateAccLockPasswordPolicy/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function SendPasswordResetMail(jobj) {
        var request = $http({
            method: "post",
            url: "api/user/SendPasswordResetMail/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetLockuserDetails() {
        var request = $http({
            method: "get",
            url: "api/user/GetLockuserDetails/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //---------role-controller----------//
    function DeleteGlobalRole(ID) {
        var request = $http({
            method: "delete",
            url: "api/access/DeleteGlobalRole/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveUpdateNewsfeedRoleFeatures(formobj) {
        var request = $http({
            method: "post",
            url: "api/access/SaveUpdateNewsfeedRoleFeatures/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetSelectedFeedFilter(GlobalRoleID) {
        var request = $http({
            method: "get",
            url: "api/access/GetSelectedFeedFilter/" + GlobalRoleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetSelectedNotificationFilter(GlobalRoleID) {
        var request = $http({
            method: "get",
            url: "api/access/GetSelectedNotificationFilter/" + GlobalRoleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveUpdateNotificationRoleFeatures(formobj) {
        var request = $http({
            method: "post",
            url: "api/access/SaveUpdateNotificationRoleFeatures/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateGlobalEntitTypeACL(jobj) {
        var request = $http({
            method: "post",
            url: "api/access/InsertUpdateGlobalEntitTypeACL/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetGlobalEntityTypeAcl(RoleID, moduleID) {
        var request = $http({
            method: "get",
            url: "api/access/GetGlobalEntityTypeAcl/" + RoleID + "/" + moduleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GettingEntityTypeTreeStructure(EntityTypeID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GettingEntityTypeTreeStructure/" + EntityTypeID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetRoleFeatures(GlobalRoleID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetRoleFeatures/" + GlobalRoleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveUpdateRoleFeatures(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/SaveUpdateRoleFeatures/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    //--pendinguser--//
    function GetPendingUsers() {
        var request = $http({
            method: "get",
            url: "api/user/GetPendingUsers/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function ApproveRejectRegisteredUsers(jobj) {
        var request = $http({
            method: "post",
            url: "api/user/ApproveRejectRegisteredUsers/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    //--module--//


    function GetAllModuleFeatures(moduleID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAllModuleFeatures/" + moduleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateFeature(formobj) {
        var request = $http({
            method: "put",
            url: "api/Metadata/UpdateFeature/",
            params: {
                action: "update"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function Module(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/Module/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    //-----sso---///
    function GetSSODetails() {
        var request = $http({
            method: "get",
            url: "api/common/GetSSODetails/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateSSOSettings(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateSSOSettings/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }


    //---asset access--////

    function AssetAccess(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/AssetAccess/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteAssetAccessOption(ID) {
        var request = $http({
            method: "delete",
            url: "api/Metadata/DeleteAssetAccessOption/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssestAccessSaved() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAssestAccessSaved/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //-----approvalrole------////
    function GetApprovalRoles() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetApprovalRoles/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertApprovalRole(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/InsertApprovalRole/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteApprovalRole(ID) {
        var request = $http({
            method: "delete",
            url: "api/Metadata/DeleteApprovalRole/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //---dalim----///
    function SaveUpdateDalimUser(jobj) {
        var request = $http({
            method: "post",
            url: "api/User/SaveUpdateDalimUser/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteDalimUser(ID) {
        var request = $http({
            method: "delete",
            url: "api/user/DeleteDalimUser/" + ID,
            params: {
                action: "delete",
            },
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetDalimUser() {
        var request = $http({
            method: "get",
            url: "api/User/GetDalimUser",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //=----dashboard--//
    function GetWidgetTemplateByID(TemplateID) {
        var request = $http({
            method: "get",
            url: "api/common/GetWidgetTemplateByID/" + TemplateID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetWidgetTypesByID(UserId, ISAdmin, TypeID) {
        var request = $http({
            method: "get",
            url: "api/common/GetWidgetTypesByID/" + UserId + "/" + ISAdmin + "/" + TypeID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetWidgetTypeRolesByID(WidgetTypeID) {
        var request = $http({
            method: "get",
            url: "api/common/GetWidgetTypeRolesByID/" + WidgetTypeID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertWidgetTemplate(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertWidgetTemplate/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }



    //--widgettype--////---dashboardtemplate//



    function UpdateWidgetTemplateByID(jobj) {
        var request = $http({
            method: "put",
            url: "api/common/UpdateWidgetTemplateByID/",
            params: {
                action: "update",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertWidgetTypeRoles(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertWidgetTypeRoles/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertWidgetTemplateRoles(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertWidgetTemplateRoles/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetWidgetTemplateRolesByTemplateID(WidgetTemplateID) {
        var request = $http({
            method: "get",
            url: "api/common/GetWidgetTemplateRolesByTemplateID/" + WidgetTemplateID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //---dalimdalimcredential//


    function GetDalimSettings() {
        var request = $http({
            method: "get",
            url: "api/common/GetDalimSettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateDalimSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateDalimSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetApprovalFlowLibraryList(typeid, IsAdminSide) {
        var request = $http({
            method: "get",
            url: "api/task/GetApprovalFlowLibraryList/" + typeid + "/" + IsAdminSide,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //-------taskflag--------//

    function GetTaskFlags() {
        var request = $http({
            method: "get",
            url: "api/task/GetTaskFlags/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function AddUpdateTaskFlag(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/AddUpdateTaskFlag/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteTaskFlagByID(ID) {
        var request = $http({
            method: "delete",
            url: "api/task/DeleteTaskFlagByID/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function gettaskflagstatus() {
        var request = $http({
            method: "get",
            url: "api/task/gettaskflagstatus/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function EnableDisableTaskflag(status) {
        var request = $http({
            method: "post",
            url: "api/task/EnableDisableTaskflag/" + status,
            params: {
                action: "add"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //--------approvaltask----------//
    function GetAllApprovalFlowPhasesSteps(templateid) {
        var request = $http({
            method: "get",
            url: "api/task/GetAllApprovalFlowPhasesSteps/" + templateid,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateTaskTemplate(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/InsertUpdateTaskTemplate/",
            params: {
                action: "add"
            },
            data: {
                "ID": formobj.ID,
                "Name": formobj.Name,
                "Description": formobj.Description
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAllApprovalFlowTemplates() {
        var request = $http({
            method: "get",
            url: "api/task/GetAllApprovalFlowTemplates/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteApprovalTemplate(TempID) {
        var request = $http({
            method: "delete",
            url: "api/task/DeleteApprovalTemplate/" + TempID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateTaskTemplateStep(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/InsertUpdateTaskTemplateStep/",
            params: {
                action: "add"
            },
            data: {
                "Name": formobj.Name,
                "Description": formobj.Description,
                "Stepduration": formobj.Stepduration,
                "StepMinApproval": formobj.StepMinApproval,
                "Steproles": formobj.Steproles,
                "IsMandatory": formobj.IsMandatory,
                "PhaseId": formobj.PhaseId,
                "StepId": formobj.StepID,
                "IsUpdate": formobj.IsUpdate
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateTaskTemplatePhase(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/InsertUpdateTaskTemplatePhase/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdatetaskTemplateSortOrder(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/UpdatetaskTemplateSortOrder/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateTemplatePhaseSortOrder(formobj) {
        var request = $http({
            method: "post",
            url: "api/task/UpdateTemplatePhaseSortOrder/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteApprovalTaskStep(StepId) {
        var request = $http({
            method: "delete",
            url: "api/task/DeleteApprovalTaskStep/" + StepId,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }


    //---------proofHQCreadintials------------//
    function UpdateProofHQSSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateProofHQSSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetProofHQSSettings() {
        var request = $http({
            method: "get",
            url: "api/common/GetProofHQSSettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }


    ///Sowmiya
    //listsettings-controller
    function GetAttributeTypeByEntityTypeID(EntityTypeID, IsAdmin) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAttributeTypeByEntityTypeID/" + EntityTypeID + "/" + IsAdmin,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetObjectiveTypeAttributeByEntityType(EntityTypeID, IsAdmin) {

        var request = $http({
            method: "get",
            url: "api/Metadata/GetObjectiveTypeAttributeByEntityType/" + EntityTypeID + "/" + IsAdmin,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAdminSettings(LogoSettings, typeid) {
        var request = $http({
            method: "get",
            url: "api/common/GetAdminSettings/" + LogoSettings + "/" + typeid,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function AdminSettingsforRootLevelInsertUpdate(listSettings) {
        var request = $http({
            method: "post",
            url: "api/common/AdminSettingsforRootLevelInsertUpdate/",
            params: {
                action: "add",
            },
            data: listSettings
        });
        return (request.then(handleSuccess, handleError));
    }
    //treesettings-controller
    function GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GettingEntityTypeHierarchyForAdminTree/" + EntityTypeID + "/" + ModuleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //listviewsettings,rootlevelfiltersettings
    //detailfiltersettings-controller
    function GetAttributesforDetailFilter() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAttributesforDetailFilter/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //myworkspacesettings-controller
    //plantabsettings-controller
    function GetPlantabsettings() {
        var request = $http({
            method: "get",
            url: "api/common/GetPlantabsettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdatePlanTabIds(jsondata) {
        var request = $http({
            method: "post",
            url: "api/common/UpdatePlanTabIds/",
            params: {
                action: "add",
            },
            data: jsondata
        });
        return (request.then(handleSuccess, handleError));
    }
    //designlayout-controller
    function insertupdatetabsettings(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/insertupdatetabsettings/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetLayoutData(TabType, TabLocation) {
        var request = $http({
            method: "get",
            url: "api/Report/GetLayoutData/" + TabType + "/" + TabLocation,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function LayoutSettingsApplyChanges(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/LayoutSettingsApplyChanges/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //financialforecastsettings-controller
    function GetCurrencyListFFsettings() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetCurrencyListFFsettings/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getDivisonIds() {
        var request = $http({
            method: "get",
            url: "api/Planning/getDivisonIds/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SetDivisonsFFSettings(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/SetDivisonsFFSettings/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetDivisonName() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetDivisonName/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCurrencyListFFSettings(ID) {
        var request = $http({
            method: "delete",
            url: "api/Planning/DeleteCurrencyListFFSettings/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCurrencyListFFSettings(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/InsertUpdateCurrencyListFFSettings/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFinancialforecastData() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetFinancialforecastData/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateFFData(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/UpdateFFData/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAdditionalSettings() {
        var request = $http({
            method: "get",
            url: "api/common/GetAdditionalSettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    //financialforecast-controller
    function getfinancialForecastIds() {
        var request = $http({
            method: "get",
            url: "api/Planning/getfinancialForecastIds/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFinancialForecastsettings() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetFinancialForecastsettings/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function updatefinancialforecastsettings(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/updatefinancialforecastsettings/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    //purchaseordersettings-controller
    function GetPoSSettings(PoSettings) {
        var request = $http({
            method: "get",
            url: "api/common/GetPoSSettings/" + PoSettings,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCurrentPONumber() {
        var request = $http({
            method: "get",
            url: "api/common/GetCurrentPONumber/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertPoSettingXML(formobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertPoSettingXML/",
            params: {
                action: "add",
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //Currencyconvertersettings-controller
    function GetExchangesratesbyCurrencytype(ID) {
        var request = $http({
            method: "get",
            url: "api/common/GetExchangesratesbyCurrencytype/" + ID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCurrencyconverter() {
        var request = $http({
            method: "get",
            url: "api/common/GetCurrencyconverter/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function Insertupdatecurrencyconverter(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/Insertupdatecurrencyconverter/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCurrencyconverter(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteCurrencyconverter/" + ID,
            params: {
                action: "delete",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //financialmetadata-controller
    function GetFinancialAttribute() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetFinancialAttribute/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteFinancialOption(ID) {
        var request = $http({
            method: "delete",
            url: "api/Metadata/DeleteFinancialOption/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteFinancialAttribute(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/DeleteFinancialAttribute/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function addFinancialAttribute(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/addFinancialAttribute/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function addFinancialOption(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/addFinancialOption/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFinancialAttributeOptions(ID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetFinancialAttributeOptions/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateFinancialMetadata(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/UpdateFinancialMetadata/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateFinMetadataSortOrder(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/UpdateFinMetadataSortOrder/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAttributetype() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAttributetype/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //assetcreationsettings-controller
    function GetAttributeDAMCreate(EntityTypeID) {
        var request = $http({
            method: "get",
            url: "api/dam/GetAttributeDAMCreate/" + EntityTypeID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DAMGetAdminSettings(LogoSettings, Key, typeid) {
        var request = $http({
            method: "get",
            url: "api/dam/DAMGetAdminSettings/" + LogoSettings + "/" + Key + "/" + typeid,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DAMadminSettingsforRootLevelInsertUpdate(DamAdminData, DamAdminID) {
        var request = $http({
            method: "post",
            url: "api/dam/DAMadminSettingsforRootLevelInsertUpdate/",
            params: {
                action: "add"
            },
            data: {
                DamAdminData: DamAdminData,
                Key: DamAdminID.Key,
                LogoSettings: DamAdminID.LogoSettings,
                TypeId: DamAdminID.TypeId
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //damviewsettings-controller
    function GetDAMViewAdminSettings(ViewType, DamType) {
        var request = $http({
            method: "get",
            url: "api/dam/GetDAMViewAdminSettings/" + ViewType + "/" + DamType,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateDamViewStatus(jobj) {
        var request = $http({
            method: "put",
            url: "api/dam/UpdateDamViewStatus/",
            params: {
                action: "update"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEntityType(ModuleID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetEntityType/" + ModuleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //ReformatProfilesettings-controller
    function GetReformatProfile(ProfileID) {
        var request = $http({
            method: "get",
            url: "api/dam/GetReformatProfile/" + ProfileID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetMimeType() {
        var request = $http({
            method: "get",
            url: "api/dam/GetMimeType/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function CreateReformatProfile(jobj) {
        var request = $http({
            method: "post",
            url: "api/dam/CreateReformatProfile/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //optimaker-controller
    function getServerUrls() {
        var request = $http({
            method: "get",
            url: "api/dam/getServerUrls",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetOptimakerSettings() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetOptimakerSettings/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetWordTemplateSettings() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetWordTemplateSettings/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetOptimakerCatagories() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetOptmakerCatagories/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function copydeleteTemplateAsset(tempGuid) {
        var request = $http({
            method: "post",
            url: "api/dam/copydeleteTemplateAsset/",
            params: {
                action: "add"
            },
            data: { tempGuid: tempGuid }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateOptimakerSettings(objdata) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertUpdateOptimakerSettings/",
            params: {
                action: "add"
            },
            data: objdata
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteOptimakeSetting(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/DeleteOptimakeSetting/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCategoryTreeCollection() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetCategoryTreeCollection/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateWordTemplateSettings(objdata) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertUpdateWordTemplateSettings/",
            params: {
                action: "add"
            },
            data: objdata
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteWordtempSetting(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/DeleteWordtempSetting/" + ID,
            params: {
                action: "delete"
            },
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCatergory(CategoryColl) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertUpdateCatergory/",
            params: {
                action: "add"
            },
            data: CategoryColl
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteOptimakerCategory(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/DeleteOptimakerCategory/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //assetcategory-controller
    function GetAssetTypeAssetCategoryRelation() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetAssetTypeAssetCategoryRelation/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetCategoryTreeCollection() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetAssetCategoryTreeCollection/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateAssetCatergory(CategoryColl) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertUpdateAssetCatergory",
            params: {
                action: "add"
            },
            data: CategoryColl,
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteAssetCategory(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/DeleteAssetCategory/" + ID,
            params: {
                action: "delete"
            },
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetSelectedCategoryTreeCollection(CategoryID) {
        var request = $http({
            method: "Get",
            url: "api/dam/GetSelectedCategoryTreeCollection/" + CategoryID,
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateAssetCategoryRelation(AssetCategoryRelation) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertUpdateAssetCategoryRelation/",
            params: {
                action: "add"
            },
            data: AssetCategoryRelation
        });
        return (request.then(handleSuccess, handleError));
    }
    //assetTypeFileExtensionRelation-controller
    function DeleteAssetTypeFileExtension(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/DeleteAssetTypeFileExtension/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertAssetTypeFileExtension(AssetFileExtRelation) {
        var request = $http({
            method: "post",
            url: "api/dam/InsertAssetTypeFileExtension/",
            params: {
                action: "add"
            },
            data: AssetFileExtRelation
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetTypeFileExtensionRelation() {
        var request = $http({
            method: "Get",
            url: "api/dam/GetAssetTypeFileExtensionRelation/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetCategoryPathInAssetEdit(AssetTypeID) {
        var request = $http({
            method: "Get",
            url: "api/dam/GetAssetCategoryPathInAssetEdit/" + AssetTypeID,
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEntityTypeAttributes() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetEntityTypeAttributes",
            parms: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function updateentitytypehelptext(fromobj) {
        var request = $http({
            method: "post",
            url: 'api/Metadata/updateentitytypehelptext',
            parms: {
                action: "add"
            },
            data: fromobj

        });
        return (request.then(handleSuccess, handleError));
    }
    function UpdateFinancialHelptext(fromobj) {
        var request = $http({
            method: "post",
            url: 'api/Metadata/UpdateFinancialHelptext',
            parms: {
                action: "add"
            },
            data: fromobj

        });
        return (request.then(handleSuccess, handleError));
    }
    function GetAllFinancialAttribute() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAllFinancialAttribute",
            parms: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //assetsnippet-controller
    function getsnippethoturl() {
        var request = $http({
            method: "Get",
            url: "api/common/getsnippethoturl",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getassetSnippetData() {
        var request = $http({
            method: "Get",
            url: "api/dam/getassetSnippetData/",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetSnippetTemplates(assetSnippetData) {
        var request = $http({
            method: "post",
            url: "api/dam/GetAssetSnippetTemplates/",
            params: {
                action: "add"
            },
            data: assetSnippetData
        });
        return (request.then(handleSuccess, handleError));
    }

    function saveTemplateEngine(jObj) {
        var request = $http({
            method: "post",
            url: "api/dam/saveTemplateEngine",
            params: {
                action: "post"
            },
            data: jObj
        });
        return (request.then(handleSuccess, handleError));
    }

    function deleteTemplateEngine(ID) {
        var request = $http({
            method: "delete",
            url: "api/dam/deleteTemplateEngine/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getAllTemplateEngines() {
        var request = $http({
            method: "get",
            url: "api/dam/getAllTemplateEngines",
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //Units-controller
    function GetUnits() {
        var request = $http({
            method: "get",
            url: "api/common/GetUnits/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateUnits(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertUpdateUnits/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteUnitsByid(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteUnitsByid/" + ID,
            params: {
                action: "delete",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //cmstemplate-controller
    function GetAllCmsSnippetTemplates() {
        var request = $http({
            method: "get",
            url: "api/cms/GetAllCmsSnippetTemplates/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertCmsSnippetTemplate(snippetObj) {
        var request = $http({
            method: "post",
            url: "api/cms/InsertCmsSnippetTemplate/",
            params: {
                action: "add"
            },
            data: snippetObj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateSnippetTemplate(snippetObj) {
        var request = $http({
            method: "post",
            url: "api/cms/UpdateSnippetTemplate/",
            params: {
                action: "add"
            },
            data: snippetObj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DuplicateCmsSnippetTemplate(snippetObj) {
        var request = $http({
            method: "post",
            url: "api/cms/DuplicateCmsSnippetTemplate/",
            params: {
                action: "add"
            },
            data: snippetObj
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveImageFromBaseFormat(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/SaveImageFromBaseFormat/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function getTenantClientPath() {
        var request = $http({
            method: "get",
            url: "api/Common/getTenantClientPath/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //CmsCustomFontSettings-controller
    function GetListOfCmsCustomFont() {
        var request = $http({
            method: 'get',
            url: 'api/metadata/GetListOfCmsCustomFont',
            params: {
                action: 'get'
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCmsCustomFont(customfonts) {
        var request = $http({
            method: "post",
            url: "api/Metadata/InsertUpdateCmsCustomFont/",
            params: {
                action: "add"
            },
            data: customfonts
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCmsCustomFont(ProviderId) {
        var request = $http({
            method: "delete",
            url: "api/Metadata/DeleteCmsCustomFont/" + ProviderId,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //CmsCustomStyleSettings-controller
    function GetListOfCmsCustomStyle() {
        var request = $http({
            method: 'get',
            url: 'api/metadata/GetListOfCmsCustomStyle',
            params: {
                action: 'get'
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCmsCustomStyle(customstyle) {
        var request = $http({
            method: "post",
            url: "api/Metadata/InsertUpdateCmsCustomStyle/",
            params: {
                action: "add"
            },
            data: customstyle
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCmsCustomStyle(Id) {
        var request = $http({
            method: "delete",
            url: "api/Metadata/DeleteCmsCustomStyle/" + Id,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //CmsTreeStyleSettings-controller
    function GetCmsTreeStyle() {
        var request = $http({
            method: 'get',
            url: 'api/metadata/GetCmsTreeStyle',
            params: {
                action: 'get'
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCmsTreeStyleSettings(treestyle) {
        var request = $http({
            method: "post",
            url: "api/Metadata/InsertUpdateCmsTreeStyleSettings/",
            params: {
                action: "add"
            },
            data: treestyle
        });
        return (request.then(handleSuccess, handleError));
    }

    //Barath
    //Super Admin
    function GetGlobalRole() {
        var request = $http({
            method: "get",
            url: "api/access/GetGlobalRole/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetSuperAdminModule(Id) {
        var request = $http({
            method: "get",
            url: "api/access/GetSuperAdminModule/" + Id,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteGlobalByID(ID, Menu) {
        var request = $http({
            method: "delete",
            url: "api/access/DeleteGlobalByID/" + ID + "/" + Menu,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveUpdateSuperAdminRoleFeatures(jobj) {
        var request = $http({
            method: "post",
            url: "api/access/SaveUpdateSuperAdminRoleFeatures/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateGlobalRole(jobj) {
        var request = $http({
            method: "post",
            url: "api/access/InsertUpdateGlobalRole/",
            params: {
                action: "add"
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetModule() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetModule/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //Title Logo

    function GetTitleLogoSettings(TitleSettings) {
        var request = $http({
            method: "get",
            url: "api/common/GetTitleLogoSettings/" + TitleSettings,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateTitleSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateTitleSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //navigation
    //commonservice
    function GetNavigationByIDandParentid(IsParentID, UserID, Flag) {
        var request = $http({
            method: "get",
            url: "api/common/GetNavigationByIDandParentid/" + IsParentID + "/" + UserID + "/" + Flag,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateNavigationSortOrder(AttributeArray) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateNavigationSortOrder/",
            params: {
                action: "add",
            },
            data: AttributeArray
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateNavigationByID(formobj) {
        var request = $http({
            method: "put",
            url: "api/common/UpdateNavigationByID/",
            params: {
                action: "update"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertNavigation(addmodule) {
        var request = $http({
            method: "post",
            url: "api/common/InsertNavigation/",
            params: {
                action: "add"
            },
            data: addmodule
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteNavigation(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteNavigation/" + ID,
            params: {
                action: "delete",
            },
        });
        return (request.then(handleSuccess, handleError));
    }
    //metadataservice
    function GetModuleFeaturesForNavigation(moduleID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetModuleFeaturesForNavigation/" + moduleID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //additional setting
    //common service
    function GetEmailIds() {
        var request = $http({
            method: "get",
            url: "api/common/GetEmailIds/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertAdminSettingXML(emailids) {
        var request = $http({
            method: "post",
            url: "api/common/InsertAdminSettingXML/",
            params: {
                action: "add",
            },
            data: emailids
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateAdditionalSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertUpdateAdditionalSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdatePopularTagWordsToShow(TotalTagWords) {
        var request = $http({
            method: "post",
            url: "api/common/UpdatePopularTagWordsToShow/",
            params: {
                action: "add",
            },
            data: TotalTagWords
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateExpirytime(Time) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateExpirytime/",
            params: {
                action: "add",
            },
            data: Time
        });
        return (request.then(handleSuccess, handleError));
    }

    function Getexpirytime() {
        var request = $http({
            method: "get",
            url: "api/common/Getexpirytime/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetTotalPopularTagWordsToShow() {
        var request = $http({
            method: "get",
            url: "api/common/GetTotalPopularTagWordsToShow/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertNotifydisplaytime(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertNotifydisplaytime/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function Getnotifytimesetbyuser() {
        var request = $http({
            method: "get",
            url: "api/common/Getnotifytimesetbyuser/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function Getnotifyrecycletimesetbyuser() {
        var request = $http({
            method: "get",
            url: "api/common/Getnotifyrecycletimesetbyuser/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertNotifycycledisplaytime(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertNotifycycledisplaytime/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetDecimalSettingsValue() {
        var request = $http({
            method: "get",
            url: "api/common/GetDecimalSettingsValue/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateDecimalSettings(DecimalSettingsVal) {
        var request = $http({
            method: "post",
            url: "api/Common/InsertUpdateDecimalSettings/",
            params: {
                action: "add",
            },
            data: DecimalSettingsVal
        });
        return (request.then(handleSuccess, handleError));
    }

    function updateDefaultFolderInfo(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/updateDefaultFolderInfo/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateNewFeedconfig(obj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateNewFeedconfig/",
            params: {
                action: "add"
            },
            data: obj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetNewsFeedConfigInfo() {
        var request = $http({
            method: "get",
            url: "api/common/GetNewsFeedConfigInfo/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getDefaultFolderInfo() {
        var request = $http({
            method: "get",
            url: "api/common/getDefaultFolderInfo/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //planning service
    function GetAllCurrencyType() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetAllCurrencyType/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //Language settings
    function GetLanguageTypes() {
        var request = $http({
            method: "get",
            url: "api/common/GetLanguageTypes/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function SetDefaultLanguage(LangID) {
        var request = $http({
            method: "post",
            url: "api/common/SetDefaultLanguage/",
            params: {
                action: "add",
            },
            data: LangID
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetObjectLanguageContent(LangEditID) {
        var request = $http({
            method: "get",
            url: "api/Common/GetObjectLanguageContent/" + LangEditID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetLanguageContent(StartRows, NextRows) {
        var request = $http({
            method: "get",
            url: "api/common/GetLanguageContent/" + StartRows + "/" + NextRows,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function LanguageSearchs(formobj) {
        var request = $http({
            method: "post",
            url: "api/common/LanguageSearchs/",
            params: {
                action: "add",
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function SaveNewLanguage(formobj) {
        var request = $http({
            method: "post",
            url: "api/common/SaveNewLanguage/",
            params: {
                action: "add",
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateLanguageContent(formobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateLanguageContent/",
            params: {
                action: "add",
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateLanguageName(formobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateLanguageName/",
            params: {
                action: "add",
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetlanguagetypeByID(languageTypeID) {
        var request = $http({
            method: "get",
            url: "api/Common/GetlanguagetypeByID/" + languageTypeID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertNewLanguage(LangContentType) {
        var request = $http({
            method: "post",
            url: "api/Common/InsertNewLanguage/",
            params: {
                action: "add",
            },
            data: LangContentType
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetLanguageExport(ID, languageTypename, Filename) {
        var request = $http({
            method: "get",
            url: "api/common/GetLanguageExport/" + ID + "/" + languageTypename + "/" + Filename,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertLanguageImport(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertLanguageImport/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateJsonLanguageContent(LangContentObj) {
        var request = $http({
            method: "post",
            url: "api/Common/UpdateJsonLanguageContent/",
            params: {
                action: "add",
            },
            data: LangContentObj
        });
        return (request.then(handleSuccess, handleError));
    }
    //notification filter settings
    function GetAllSubscriptionType() {
        var request = $http({
            method: "get",
            url: "api/common/GetAllSubscriptionType/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertAdminNotificationSettings(subscriptionObject) {
        var request = $http({
            method: "post",
            url: "api/common/InsertAdminNotificationSettings/",
            params: {
                action: "add",
            },
            data: subscriptionObject
        });
        return (request.then(handleSuccess, handleError));
    }
    //News Feed Filter
    function GetFeedFilter() {
        var request = $http({
            method: "get",
            url: "api/common/GetFeedFilter/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFeedTemplates() {
        var request = $http({
            method: "get",
            url: "api/common/GetFeedTemplates/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteFeedGroupByid(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteFeedGroupByid/" + ID,
            params: {
                action: "delete",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateFeedFilterGroup(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertUpdateFeedFilterGroup/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //search Engine
    function UpdateSearchEngine(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/UpdateSearchEngine/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //gantt view header
    function GetAllGanttHeaderBar() {
        var request = $http({
            method: "get",
            url: "api/common/GetAllGanttHeaderBar/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateGanttHeaderBar(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertUpdateGanttHeaderBar/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteGanttHeaderBar(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteGanttHeaderBar/" + ID,
            params: {
                action: "delete",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //statistics
    function GetUniqueuserhit(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetUniqueuserhit/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetApplicationhit(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetApplicationhit/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetBrowserStatistic(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetBrowserStatistic/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetBrowserVersionStatistic(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetBrowserVersionStatistic/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetUserStatistic() {
        var request = $http({
            method: "get",
            url: "api/common/GetUserStatistic/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetOSStatistic() {
        var request = $http({
            method: "get",
            url: "api/common/GetOSStatistic/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetstartpageStatistic() {
        var request = $http({
            method: "get",
            url: "api/common/GetstartpageStatistic/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetUserRoleStatistic() {
        var request = $http({
            method: "get",
            url: "api/common/GetUserRoleStatistic/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEnityStatistic() {
        var request = $http({
            method: "get",
            url: "api/common/GetEnityStatistic/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEnityCreateationStatistic(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetEnityCreateationStatistic/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetbandwidthStatistic(year, month) {
        var request = $http({
            method: "get",
            url: "api/common/GetbandwidthStatistic/" + year + "/" + month,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetProofInitiatorStatistic(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/GetProofInitiatorStatistic/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //broadcast message
    function GetBroadcastMessages() {
        var request = $http({
            method: "get",
            url: "api/common/GetBroadcastMessages/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertBroadcastMessages(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertBroadcastMessages/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //tabs
    function UpdateCustomTabSortOrder(ID, SortOrder) {
        var request = $http({
            method: "put",
            url: "api/common/UpdateCustomTabSortOrder/" + ID + "/" + SortOrder,
            params: {
                action: "update",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCustomTabSettingDetails() {
        var request = $http({
            method: "get",
            url: "api/common/GetCustomTabSettingDetails/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCustomTabsByTypeID(TypeID) {
        var request = $http({
            method: "get",
            url: "api/common/GetCustomTabsByTypeID/" + TypeID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCustomTabEncryptionByID() {
        var request = $http({
            method: "get",
            url: "api/common/GetCustomTabEncryptionByID/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCustomTab(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/InsertUpdateCustomTab/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetCustomTabAccessByID(TabID) {
        var request = $http({
            method: "get",
            url: "api/common/GetCustomTabAccessByID/" + TabID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCustomtabByID(ID, AttributeTypeID, EntityTypeID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteCustomtabByID/" + ID + "/" + AttributeTypeID + "/" + EntityTypeID,
            params: {
                action: "delete",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEntityTypeIsAssociate() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetEntityTypeIsAssociate/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getPlanandObjTypes() {
        var request = $http({
            method: "get",
            url: "api/Metadata/getPlanandObjTypes/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //updates
    function GetUpdateSettings() {
        var request = $http({
            method: "get",
            url: "api/common/GetUpdateSettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    //Password policies
    function GetPasswordPolicyDetails() {
        var request = $http({
            method: "get",
            url: "api/common/GetPasswordPolicyDetails/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdatePasswordPolicy(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdatePasswordPolicy/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //search criteria
    function GetAttributeSearchCriteria(EntityTypeID) {
        var request = $http({
            method: "get",
            url: "api/common/GetAttributeSearchCriteria/" + EntityTypeID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function GetSearchCriteriaAdminSettings(LogoSettings, Key, typeid) {
        var request = $http({
            method: "get",
            url: "api/Dam/GetSearchCriteriaAdminSettings/" + LogoSettings + "/" + Key + "/" + typeid, params: { action: "get" }
        });
        return (request.then(handleSuccess, handleError));
    }
    function SearchadminSettingsforRootLevelInsertUpdate(jobj) {
        var request = $http({
            method: "post",
            url: "api/common/SearchadminSettingsforRootLevelInsertUpdate/",
            params: {
                action: "add",
            },
            data: jobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //buisness days
    function SaveHolidayDetails(day) {
        var request = $http({
            method: "post",
            url: "api/common/SaveHolidayDetails/",
            params: {
                action: "add",
            },
            data: {
                "nonbusinessday": day
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertHolidayDetails(holiday) {
        var request = $http({
            method: "post",
            url: "api/Common/InsertHolidayDetails/",
            params: {
                action: "add",
            },
            data: holiday
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetNonBusinessDays() {
        var request = $http({
            method: "get",
            url: "api/Common/GetNonBusinessDays/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetHolidaysDetails() {
        var request = $http({
            method: "get",
            url: "api/Common/GetHolidaysDetails/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteHoliday(ID) {
        var request = $http({
            method: "delete",
            url: "api/common/DeleteHoliday/" + ID,
            params: {
                action: "delete",
            },
        });
        return (request.then(handleSuccess, handleError));
    }
    //cloud settings
    function getcloudsettings() {
        var request = $http({
            method: "get",
            url: "api/common/getcloudsettings/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function savecloudsettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/savecloudsettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //reports
    //credential management
    function GetDataviewbyusername(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/GetDataviewbyusername/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetReportCredentialByID(ID) {
        var request = $http({
            method: "get",
            url: "api/Report/GetReportCredentialByID/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function validateReportCredential(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/validateReportCredential/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function ReportLogin(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/ReportLogin/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //report
    function GetReportByOID(OID) {
        var request = $http({
            method: "get",
            url: "api/Report/GetReportByOID/" + OID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateReport(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/InsertUpdateReport/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateReportImage(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/UpdateReportImage/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //report view
    function GetReportViewSchemaResponse() {
        var request = $http({
            method: "get",
            url: "api/Report/GetReportViewSchemaResponse/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteView(ID) {
        var request = $http({
            method: "delete",
            url: "api/Report/DeleteView/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetViews() {
        var request = $http({
            method: "get",
            url: "api/Report/GetViews/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertCustomViews(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/InsertCustomViews/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetViewsValidate(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/GetViewsValidate/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function CustomViews_Update(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/CustomViews_Update/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function pushviewSchema(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/pushviewSchema/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAlltablesnames() {
        var request = $http({
            method: "get",
            url: "api/common/GetAlltablesnames/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAlltableswithmetadata(Tablename) {
        var request = $http({
            method: "get",
            url: "api/common/GetAlltableswithmetadata/" + Tablename,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function ManipulateQueryEditorQuery(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/ManipulateQueryEditorQuery/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //financial reports
    function GetAllEntityTypeAttributeRelationWithLevels() {
        var request = $http({
            method: "get",
            url: "api/Report/GetAllEntityTypeAttributeRelationWithLevels/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFinancialReportSettings() {
        var request = $http({
            method: "get",
            url: "api/Report/GetFinancialReportSettings/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetReportJSONData(ReportID) {
        var request = $http({
            method: "get",
            url: "api/Report/GetReportJSONData/" + ReportID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeletefinancialreportByID(ID) {
        var request = $http({
            method: "delete",
            url: "api/Report/DeletefinancialreportByID/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function insertupdatefinancialreportsettings(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/insertupdatefinancialreportsettings/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateFinancialSettingsReportImage(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/UpdateFinancialSettingsReportImage/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }
    //gantview report
    function GetAttributefromDB() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetAttributefromDB/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function AdminSettingsforReportInsertUpdate(key) {
        var request = $http({
            method: "post",
            url: "api/common/AdminSettingsforReportInsertUpdate/",
            params: {
                action: "add",
            },
            data: key
        });
        return (request.then(handleSuccess, handleError));
    }
    //custom list
    function GetTaskFulfillmentEntityTypes() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetTaskFulfillmentEntityTypes/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAllCustomList() {
        var request = $http({
            method: "get",
            url: "api/Report/GetAllCustomList/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteCustomList(ID) {
        var request = $http({
            method: "delete",
            url: "api/Report/DeleteCustomList/" + ID,
            params: {
                action: "delete"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function ValidateCustomList(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/ValidateCustomList/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateCustomlist(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/InsertUpdateCustomlist/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
        var request = $http({
            method: "get",
            url: "api/Report/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetFulfillmentAttribute(formobj) {
        var request = $http({
            method: "post",
            url: "api/Report/GetFulfillmentAttribute/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetGlobalRoleUserByID(ID) {
        var request = $http({
            method: "get",
            url: "api/access/GetGlobalRoleUserByID/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetSuperglobalacl() {
        var request = $http({
            method: "get",
            url: "api/access/GetSuperglobalacl/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAssetCategoryTree(AsetTypeID) {
        var request = $http({
            method: "Get",
            url: "api/dam/GetAssetCategoryTree/" + AsetTypeID,
            params: {
                action: "Get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAllAttributeGroup() {
        var request = $http({
            method: "get",
            url: "api/common/GetAllAttributeGroup/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetAttrGroupTooltipValues(GroupID) {
        var request = $http({
            method: "get",
            url: "api/common/GetAttrGroupTooltipValues/" + GroupID,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function UpdateAttributeGroupTooltip(fromdata) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateAttributeGroupTooltip/",
            params: {
                action: "add",
            },
            data: fromdata
        });
        return (request.then(handleSuccess, handleError));
    }


    function InsertEntityHelpTextImport(formobj) {
        var request = $http({
            method: "post",
            url: "api/Metadata/InsertEntityHelpTextImport/",
            params: {
                action: "add"
            },
            data: formobj
        });

        return (request.then(handleSuccess, handleError));
    }

    function ExportEntityHelpTextListtoExcel() {
        var request = $http({
            method: "get",
            url: "api/Metadata/ExportEntityHelpTextListtoExcel",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function GetEntityTemplateDetails() {
        var request = $http({
            method: "get",
            url: "api/Planning/GetEntityTemplateDetails/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function InsertUpdateEntityTemplate(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/InsertUpdateEntityTemplate/",
            params: {
                action: "add"
            },
            data: formobj
        });

        return (request.then(handleSuccess, handleError));
    }

    function GetEntityTypeStatus(EntityTypeId) {
        var request = $http({
            method: "get",
            url: "api/Planning/GetEntityTypeStatus/" + EntityTypeId,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function DeleteEntityTemplateListById(ID) {
        var request = $http({
            method: "delete",
            url: "api/Planning/DeleteEntityTemplateListById/" + ID,
            params: {
                action: "delete"
            }
        });

        return (request.then(handleSuccess, handleError));
    }

    function UpdateEntityTypeOverallStatus(formobj) {
        var request = $http({
            method: "post",
            url: "api/Planning/UpdateEntityTypeOverallStatus/",
            params: {
                action: "add"
            },
            data: formobj
        });
        return (request.then(handleSuccess, handleError));
    }

    function getEntityToSetOverallStauts(ID) {
        var request = $http({
            method: "get",
            url: "api/Metadata/getEntityToSetOverallStauts/" + ID,
            parms: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function Updatepublishaccess(fromdata) {
        var request = $http({
            method: "post",
            url: "api/access/Updatepublishaccess/",
            params: {
                action: "add",
            },
            data: fromdata
        });
        return (request.then(handleSuccess, handleError));
    }

    function Getpublishaccess() {
        var request = $http({
            method: "get",
            url: "api/access/Getpublishaccess/",
            params: {
                action: "get",
            },

        });
        return (request.then(handleSuccess, handleError));
    }

    function GettableauReportSettings(ID) { var request = $http({ method: "get", url: "api/Report/GettableauReportSettings/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function DeletetableaureportByID(ID) { var request = $http({ method: "delete", url: "api/Report/DeletetableaureportByID/" + ID, params: { action: "delete", }, }); return (request.then(handleSuccess, handleError)); }
    function insertupdatetableaureportsettings(jobj) { var request = $http({ method: "post", url: "api/Report/insertupdatetableaureportsettings/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
    function UpdatetableauSettingsReportImage(jobj) { var request = $http({ method: "post", url: "api/Report/UpdatetableauSettingsReportImage/", params: { action: "update" }, data: jobj }); return (request.then(handleSuccess, handleError)); }



    //Financial view controller

    function GetFinancialViews() {
        var request = $http({ method: "get", url: "api/Common/GetFinancialViews", params: { action: "get" } });
        return (request.then(handleSuccess, handleError));
    }
    function GetFinancialViewColumns(viewId) {
        var request = $http({ method: "get", url: "api/Common/GetFinancialViewColumns/" + viewId, params: { action: "get" } });
        return (request.then(handleSuccess, handleError));
    }
    function InsertFinacialColumns(jobj) {
        var request = $http({ method: "post", url: "api/Common/InsertFinacialColumns", data: jobj, params: { action: "add" } });
        return (request.then(handleSuccess, handleError));
    }
    function UpdateFinancialViews(jobj) {
        var request = $http({ method: "post", url: "api/Common/UpdateFinancialViews", data: jobj, params: { action: "add" } });
        return (request.then(handleSuccess, handleError));
    }
    function DeleteFinancialViews(jobj) {
        var request = $http({ method: "post", url: "api/Common/DeleteFinancialViews", data: jobj, params: { action: "add" } });
        return (request.then(handleSuccess, handleError));
    }
    function EditFinancialView(viewid) {
        var request = $http({ method: "get", url: "api/Common/EditFinancialView/" + viewid, params: { action: "get" } });
        return (request.then(handleSuccess, handleError));
    }
    function GetListFinancialView() {
        var request = $http({ method: "get", url: "api/Common/GetListFinancialView", params: { action: "get" } });
        return (request.then(handleSuccess, handleError));
    }
    function GetNavigationType() {
        var request = $http({
            method: "get",
            url: "api/Metadata/GetNavigationType/",
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }


    function GetUserById(ID) {
        var request = $http({
            method: "get",
            url: "api/user/GetUserById/" + ID,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function Updateadditionalsettingsstartpage(ID) {
        var request = $http({
            method: "post",
            url: "api/user/Updateadditionalsettingsstartpage/",
            params: {
                action: "add"
            },
            data: ID
        });
        return (request.then(handleSuccess, handleError));
    }

    function RestoreDefaultTheme() {
        var request = $http({
            method: "post",
            url: "api/common/RestoreDefaultTheme/",
            params: {
                action: "add",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function GetThemeData(Theme) {
        var request = $http({
            method: "get",
            url: "api/common/GetThemeData/" + Theme,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function GetThemeValues() {
        var request = $http({
            method: "get",
            url: "api/common/GetThemeValues/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function UpdateThemeSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateThemeSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }
    function GetUserRegAdminSettings(LogoSettings, typeid) {
        var request = $http({
            method: "get",
            url: "api/common/GetUserRegAdminSettings/" + LogoSettings + "/" + typeid,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function GetUserRegLogoSettings(TitleSettings) {
        var request = $http({
            method: "get",
            url: "api/common/GetUserRegLogoSettings/" + TitleSettings,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }
    function UpdateUserRegSettings(dataobj) {
        var request = $http({
            method: "post",
            url: "api/common/UpdateUserRegSettings/",
            params: {
                action: "add",
            },
            data: dataobj
        });
        return (request.then(handleSuccess, handleError));
    }    

    function getmetadataLockStatus() {
        var request = $http({
            method: "get",
            url: "api/metadata/GetMetadataLockStatus/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function setmetadataLockStatus(status) {
        var request = $http({
            method: "get",
            url: "api/metadata/SetMetadataLockStatus/" + status,
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function clearCache() {
        var request = $http({
            method: "get",
            url: "api/metadata/ClearCache/",
            params: {
                action: "get",
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data.message));
    }

    function handleSuccess(response) {
        return (response.data);
    }
});