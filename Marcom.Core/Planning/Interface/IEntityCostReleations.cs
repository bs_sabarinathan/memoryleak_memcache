﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICostCenter interface for table 'PM_CostCenter'.
    /// </summary>
    public interface IEntityCostReleations
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityId
        {
            get;
            set;

        }

        int CostcenterId
        {
            get;
            set;

        }

        int Sortorder
        {
            get;
            set;

        }

        bool Isassociate
        {
            get;
            set;

        }

        bool Isactive
        {
            get;
            set;

        }


        string CCName
        {
            get;
            set;
        }

        #endregion
    }
}
