﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
  internal class ObjectivesToEntitySelect : IObjectivesToEntitySelect
    {
      public int ObjectiveID { get; set; }
      public string ObjectiveName { get; set; }
      public string ObjectiveOwner { get; set; }
      public int ObjectiveTypeId { get; set; }
      public string ObjectiveDescription { get; set; }
    }
}
