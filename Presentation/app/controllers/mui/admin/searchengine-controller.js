﻿(function (ng, app) {
    "use strict"; function muiadminsearchengineCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.loadimage = false; $scope.UpdateSearch = function () {
            $scope.loadimage = true; AdminService.UpdateSearchEngine().then(function (result) {
                if (result.Response == false || result.StatusCode == 405)
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption')); else
                    NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption')); $scope.loadimage = false;
            });
        }
    }
    app.controller("mui.admin.searchengineCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminsearchengineCntrl]);
})(angular, app);