﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marcom.API.Models
{
    public class Formatted
    {

        #region "Member Variables"

        private string weekday;
        private string month;
        private string time;
        private string shorttime;
        private string hand;
        private string slim;
        private string handtime;
        private string longhand;
        private string longhandtime;
        private int full;
        private string fullslim;

        #endregion

        #region "Members"

        public string Weekday
        {
            get { return weekday; }
            set { weekday = value; }
        }

      

        public string Month
        {
            get { return month; }
            set { month = value; }
        }

       

        public string Time
        {
            get { return time; }
            set { time = value; }
        }

       

        public string Shorttime 
        {
            get { return shorttime; }
            set { shorttime = value; }

        }

      

        public string Hand
        {
            get { return hand; }
            set { hand = value; }
        }


       

        public string Slim
        {
            get { return slim; }
            set { slim = value; }
        }


       

        public string HandTime
        {
            get { return handtime; }
            set { handtime = value; }
        }

       

        public string LongHand
        {
            get { return longhand; }
            set { longhand = value; }
        }


        

        public string LongHandTime
        {
            get { return longhandtime; }
            set { longhandtime = value; }
        }

       

        public int Full
        {
            get { return full; }
            set { full = value; }
        }

        

        public string FullSlim
        {
            get { return fullslim; }
            set { fullslim = value; }
        }

        #endregion

    }
}