﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IFinancialOverview
    {
        decimal TotalAssignedAmount { get; set; }
        decimal PlannedAmount { get; set; }
        decimal InRequests { get; set; }
        decimal ApprovedBudget { get; set; }
        decimal BudgetDeviation { get; set; }
        decimal ApprovedAllocation { get; set; }
        decimal UnAllocatedAmount { get; set; }
        decimal Committed { get; set; }
        decimal Spent { get; set; }
        decimal AvailabletoSpent { get; set; }
        decimal EntityTypeId { get; set; }
        string LastUpdatedon { get; set; }
        decimal SubAssignedAmount { get; set; }
        decimal AvailableAssignedAmount { get; set; }
    }
}
