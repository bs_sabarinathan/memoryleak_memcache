///#source 1 1 /app/controllers/mui/cms/default/detail/section/content-controller.js
(function (ng, app) {
    function muicmscontentCtrl($scope, $location, $window, $timeout, $compile, $resource, $stateParams, $translate, ContentService) {
        var redSave = '';
        var redRestore = '';
        $scope.PageNoobj = {
            pageno: 1
        };
        $scope.TaskStatusObj = [{
            "Name": "Thumbnail",
            ID: 1
        }, {
            "Name": "Summary",
            ID: 2
        }, {
            "Name": "List",
            ID: 3
        }];
        $scope.SettingsDamAttributes = {};
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Creation date (Ascending)",
            ID: 3
        }, {
            "Name": "Creation date (Descending)",
            ID: 4
        }];
        $scope.FilterStatus = {
            filterstatus: 1
        };
        $scope.DamViewName = {
            viewName: "Thumbnail"
        };
        $scope.OrderbyName = {
            orderbyname: "Creation date (Descending)"
        };
        $scope.OrderBy = {
            order: 4
        };
        $scope.PageAsset = {
            CmsAssetSelectionFiles: []
        };
        $scope.processed = {
            index: 0
        };
        $scope.AttrLock = true;
        $scope.attrgrpImagefileName = '';
        $scope.agroup = [];
        $scope.customattrgroup = [];
        $scope.insertImageOptions = {
            header: {
                title: 'Add asset from computer',
                callback: function (obj) {
                    obj.insertHtml('Page 3');
                }
            },
            footer: {
                title: 'Add asset from page attachment',
                callback: function (obj) {
                    obj.insertHtml('Page 4');
                }
            }
        };
        $scope.ShowHideSave = false;
        $scope.tempContent = "";
        $scope.AttributeGroupList = [];
        $scope.snippettemplates = [];
        $.Redactor.prototype.advanced = function () {
            return {
                init: function () {
                    var dropdown = {};
                    dropdown.point1 = {
                        title: 'Add asset from computer',
                        func: this.advanced.addfrmComputer
                    };
                    dropdown.point2 = {
                        title: 'Add asset from page attachment',
                        func: this.advanced.AddassetsfromPageAttachments
                    };
                    dropdown.point3 = {
                        title: 'Add asset from asset library',
                        func: this.advanced.AddFilesFromAssetLib
                    };
                    var button = this.button.add('advanced', 'Asset to page');
                    this.button.setAwesome('advanced', 'icon-tasks');
                    this.button.addDropdown(button, dropdown);
                },
                addfrmComputer: function () {
                    this.selection.save();
                    addfrmComputer1(this);
                },
                AddassetsfromPageAttachments: function () {
                    this.selection.save();
                    AddassetsfromPageAttachments1(this);
                },
                AddFilesFromAssetLib: function () {
                    redactorCurPoint = this.caret.getOffset();
                    AddFilesFromAssetLib1(this);
                }
            };
        };
        FnUpload();

        function CallbackFromAsstLibrary(obj) {
            if ($scope.contentDatafrCMS.attachType != undefined) {
                switch ($scope.contentDatafrCMS.attachType) {
                    case 'link':
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            obj.insert.htmlWithoutClean('&nbsp;<a data-assetId=' + item.AssetID + ' href="DAMDownload.aspx?FileID=' + item.FileGuid + '&FileFriendlyName=' + item.AssetName + item.Ext + '&Ext=' + item.Ext + '&token=' + token + '">' + item.AssetName + '</a>');
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                        break;
                    case 'image':
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            var ste = item.Ext.split('.').join("");
                            if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                            } else {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'DAMFiles/StaticPreview/&amp;name=' + ste + '.png" alt="Image">');
                            }
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                        break;
                    default:
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            var ste = item.Ext.split('.').join("");
                            if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                            } else {
                                var videofilepath = TenantFilePath + "CMSFiles/Templates/Preview/" + item.FileGuid + ".mp4";
                                var vidImagePath = TenantFilePath + "CMSFiles/Templates/Preview/Big_" + item.FileGuid + ".jpg"
                                obj.insert.htmlWithoutClean('<div class="iv-p-videoPlayer embed-responsive embed-responsive-16by9 tmplt-video"><video class="video-js" controls="" preload="none" name="media" poster="' + vidImagePath + '"><source type="video/mp4" src="' + videofilepath + '"></video></div>');
                            }
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                }
            }
        }
        $scope.$on('processnext', function (event, data) {
            var nextGroupToProcess = $scope.agroup.indexOf(($.grep($scope.agroup, function (e) {
                return e.ID == data
            }))[0]) + 1;
            if (nextGroupToProcess + 1 <= $scope.agroup.length) $scope.$broadcast("process" + $scope.agroup[nextGroupToProcess].ID);
        });

        function AttributeGroupInRedactor(attrGrpRes) {
            $.Redactor.prototype.plgattributegroup = function () {
                return {
                    init: function () {
                        var dropdown = {};
                        for (var i = 0; i < attrGrpRes.length; i++) {
                            dropdown[attrGrpRes[i].AttributeGroupID] = {
                                title: attrGrpRes[i].Caption,
                                func: function (obj) {
                                    var attrInfo = $.grep(attrGrpRes, function (e) {
                                        return e.AttributeGroupID == parseInt(obj)
                                    });
                                    var attrGroupBody = "";
                                    attrGroupBody += '<br/><br/><div><div class=\"row clearfix\" data-attributegroupId=\"' + attrInfo[0].AttributeGroupID + '\">';
                                    attrGroupBody += '<div class=\"column full display attGrp-editTemplate\">';
                                    attrGroupBody += '<div class=\"attGrp-header\"><h1>Attribute Group</h1></div>';
                                    attrGroupBody += '<p>' + attrInfo[0].Caption + '</p>'
                                    attrGroupBody += '</div></div></div><br/><br/>'
                                    this.insert.html(attrGroupBody, false);
                                }
                            };
                        }
                        var button = this.button.add('plgattributegroup', 'Attribute Group');
                        this.button.setAwesome('plgattributegroup', 'icon-list-alt');
                        this.button.addDropdown(button, dropdown);
                    }
                };
            };
        }

        function CustomFontsInRedactor(fontList) {
            $.Redactor.prototype.plgcustomfonts = function () {
                return {
                    init: function () {
                        var dropdown = {};
                        $.each(fontList, function (i, styl) {
                            dropdown[styl.Id + "=" + styl.CssCode] = {
                                title: styl.ClassName,
                                style: styl.CssCode,
                                func: function (styl) {
                                    this.inline.removeStyle();
                                    this.inline.format('span', 'style', styl.toString().split("=")[1]);
                                }
                            };
                        });
                        var button = this.button.add('plgcustomfonts', 'Custom Styles');
                        this.button.setAwesome('plgcustomfonts', 'icon-font');
                        this.button.addDropdown(button, dropdown);
                    }
                };
            };
        }
        $scope.BindScrollOnNewsfeed = function () {
            if ($('#CmsPageFeedsdiv').height() > 1260) {
                $scope.GetNewsFeedforPaging();
            }
            $timeout(function () {
                $('#CmsPageFeedsdiv').unbind('scroll');
                $('#CmsPageFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        $scope.GetNewsFeedforPaging();
                    }
                });
            }, 500)
        }

        function GetAttributeGroups() {
            ContentService.GetListOfCmsCustomStyle().then(function (ResCustomStyle) {
                if (ResCustomStyle.Response.length > 0) CustomFontsInRedactor(ResCustomStyle.Response);
            });
            $scope.AttributeGroupList.length = 0;
            $scope.LanguageContent = $scope.LanguageContents;
            ContentService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null && attributeGrpRelation.Response != '') {
                    AttributeGroupInRedactor(attributeGrpRelation.Response);
                    for (var i = 0; i < attributeGrpRelation.Response.length; i++) {
                        $scope.AttributeGroupList.push({
                            AttributeGroupId: attributeGrpRelation.Response[i].AttributeGroupID,
                            Name: attributeGrpRelation.Response[i].Caption
                        });
                    }
                    getSnippettemplates();
                } else getSnippettemplates();
            });
        }

        function getSnippettemplates() {
            ContentService.GetAllCmsSnippetTemplates().then(function (result) {
                if (result.Response != null) {
                    $scope.snippettemplates.length = 0;
                    for (var i = 0; i < result.Response.length; i++) {
                        $scope.snippettemplates.push({
                            ID: result.Response[i].ID,
                            SnippetContent: result.Response[i].DefaultFirstContent + result.Response[i].SnippetContent + result.Response[i].DefaultLastContent
                        });
                    }
                    GetCmsRevisedContent();
                } else GetCmsRevisedContent();
            });
        };
        $scope.PopupModalGroupID = 0;

        $scope.$on("$destroy", function () {
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.section.contentCtrl']"));
        });
        GetAttributeGroups();

        function GetCmsRevisedContent() {
            if ($scope.contentDatafrCMS != undefined && $scope.contentDatafrCMS.attachType != undefined && $scope.contentDatafrCMS.attachType != '') {
                $scope.ShowHideContent = false;
                $('#onLoadSnippet').html($scope.contentDatafrCMS.cmsContentData);
                $('#cmsSnippetsContainer').html($('#onLoadSnippet #cmsSnippetsContainer').html());
                $('#cmsLinksContainer').html($('#onLoadSnippet #cmsLinksContainer').html());
                $('#onLoadSnippet').html('');
                $scope.EditCmsHtmlContent1();
            } else {
                ContentService.GetRevisedContentByFeature($stateParams.ID).then(function (data) {
                    if (data.Response != null) {
                        $('#CmsContentEditablePage #cmsSnippetsContainer #cmsLinksContainer').html('');
                        if ($('#CmsContentEditablePage').hasClass("redactor-editor")) $('#CmsContentEditablePage').redactor('core.destroy');
                        if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
                        $timeout(function () {
                            $('#onLoadSnippet').html(data.Response.Content);
                            $('#cmsSnippetsContainer').html($('#onLoadSnippet #cmsSnippetsContainer').html());
                            $('#cmsLinksContainer').html($('#onLoadSnippet #cmsLinksContainer').html());
                            $('#onLoadSnippet').html('');
                            $scope.ShowHideContent = true;
                            $("#cmsSnippetsContainer").contentbuilder({
                                snippetFile: TenantFilePath + 'CMSFiles/contentbuilder/snippets.html',
                                snippetPathReplace: ["CMSFiles", TenantFilePath + 'CMSFiles/'],
                                attributegroup: $scope.AttributeGroupList,
                                cmssnippetList: $scope.snippettemplates
                            });
                        }, 50);
                    }
                });
            }
        }
        $scope.SaveCmsrevContent = function () {
            if ($('#CmsContentEditablePage').html().trim().length == 0) {
                alert("Please enter content");
                return false;
            }
            var RevParams = {
                EntityID: $stateParams.ID,
                Content: $('#CmsContentEditablePage').html()
            };
            ContentService.InsertRevisedEntityContent(RevParams).then(function (data) {
                if (data.Response != null) {
                    NotifySuccess($translate.instant('LanguageContents.Res_1130.Caption'));
                }
            });
        }
        $scope.addimageatCursor = false;

        function FnUpload() {
            $('#contentpickfiles').unbind();
            $('.moxie-shim').remove();
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'contentpickfiles',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/ImageUploader.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png"
                }],
                resize: {
                    width: 320,
                    height: 240,
                    quality: 90
                }
            });
            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader.start();
            });
            uploader.bind('Error', function (up, err) {
                $scope.PrevShow = false;
                $scope.BtnSaveShow = false;
                $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.PrevImage = response.response.split(',')[0];
                if ($scope.addimageatCursor) {
                    redObj.insert.htmlWithoutClean('<img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + $scope.PrevImage + '" alt="Image">');
                    $scope.addimageatCursor = false;
                    NotifySuccess("File attached successfully");
                } else {
                    showImagePreview(file);
                }
            });
            uploader.bind('BeforeUpload', function (up, file) {
                $scope.BtnSaveShow = true;
                $scope.PrevShow = true;
            });

            function showImagePreview(file) {
                var preloader = new mOxie.Image();
                preloader.onload = function () {
                    var imgfile = $("#redactor-image-resizer").next();
                    preloader.downsize(300, 300);
                    if (imgfile.length > 0) {
                        $("#redactor-image-resizer").next().attr('src', preloader.getAsDataURL());
                        var redactorObj = $('#CmsContentEditablePage').redactor('core.getObject');
                        redactorObj.image.hideResize();
                    }
                };
                preloader.load(file.getSource());
            }
        }

        function addfrmComputer1(Obj) {
            redObj = Obj;
            $scope.addimageatCursor = true;
            $("#contentpickfiles").click();
        };

        function AddFilesFromAssetLib1(Obj) {
            redObj = Obj;
            $scope.MediaBankSettings.Redirectfromdam = true;
            $scope.MediaBankSettings.FolderId = 0;
            $scope.MediaBankSettings.EntityId = parseInt($stateParams.ID);
            $scope.MediaBankSettings.ViewType = 1;
            $scope.MediaBankSettings.directfromMediaBank = false;
            $scope.MediaBankSettings.ProductionTypeID = 17;
            $scope.MediaBankSettings.FolderName = '';
            $scope.MediaBankSettings.frmCMS = true;
            $scope.IsNavFromAssetToPage.IsFromAsset = true;
            $scope.contentDatafrCMS.cmsContentData = $("#CmsContentEditablePage").html();
            $scope.$apply($location.path("/mediabank"));
        }
        $scope.AssetTaskentityselection = 'cms';
        $scope.$on('LoadCmsContentTab', function (event, data) {
            $timeout(function () {
                GetAttributeGroups();
            }, 100);
        });
        var sel, range, htmlcood, position;

        function AddassetsfromPageAttachments1(Obj) {
            $scope.PageAsset.CmsAssetSelectionFiles = [];
            redObj = Obj;
            position = redObj.caret.getOffset();
            $("#attachmentsection").modal("show");
            $scope.MediaBankSettings.ViewType = 1;
            $scope.MediaBankSettings.FolderId = 0;
            $scope.MediaBankSettings.EntityId = parseInt($stateParams.ID);
            $scope.MediaBankSettings.ProductionTypeID = 17;
            $scope.MediaBankSettings.FolderName = '';
            $scope.MediaBankSettings.frmCMS = true;
            $scope.$broadcast('callbackforEntitycreation', 0, parseInt($stateParams.ID), 4);
        };
        $scope.ShowHideContent = true;
        $scope.EditCmsHtmlContent = function () {
            $scope.ShowHideContent = false
            if ($('#cmsSnippetsContainer').data('contentbuilder')) {
                $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            }
            $('#CmsContentEditablePage').redactor({
                focus: true,
                paragraphize: false,
                replaceDivs: false,
                plugins: ['plgattributegroup', 'advanced', 'plgcustomfonts'],
            });
        }
        $scope.EditCmsHtmlContent1 = function () {
            $scope.ShowHideContent = false
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            $('#CmsContentEditablePage').redactor({
                focus: true,
                paragraphize: false,
                replaceDivs: false,
                plugins: ['plgattributegroup', 'advanced', 'plgcustomfonts'],
                initCallback: function () {
                    CallbackFromAsstLibrary(this);
                }
            });
        }
        $scope.DragModeContent = function () {
            if ($('#CmsContentEditablePage').hasClass("redactor-editor")) $('#CmsContentEditablePage').redactor('core.destroy');
            $scope.ShowHideContent = true;
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            $("#cmsSnippetsContainer").contentbuilder({
                snippetFile: TenantFilePath + 'CMSFiles/contentbuilder/snippets.html',
                snippetPathReplace: ["CMSFiles", TenantFilePath + 'CMSFiles/'],
                attributegroup: $scope.AttributeGroupList,
                cmssnippetList: $scope.snippettemplates
            });
        }
        $scope.attachFile = function (attachType) {
            var count = $scope.PageAsset.CmsAssetSelectionFiles.length;
            if ($scope.PageAsset.CmsAssetSelectionFiles.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            } else if ($scope.PageAsset.CmsAssetSelectionFiles.length > 1) {
                bootbox.alert('Can not attach more than one asset.');
                return false;
            }
            ContentService.GetAssetActiveFileinfo($scope.PageAsset.CmsAssetSelectionFiles[0]).then(function (attrList) {
                var d = attrList.Response;
                var ste1 = attrList.Response[0].Extension.split('.').join("");
                var fileType = o.Mime.mimes[ste1.toLowerCase()].split('/')[0];
                if (attachType == 'video' && o.Mime.mimes[ste1.toLowerCase()].split('/')[0] != 'video') {
                    bootbox.alert('Can not attach this asset as video.');
                    return false;
                }
                var arr = [];
                for (var i = 0; i < $scope.PageAsset.CmsAssetSelectionFiles.length; i++) {
                    arr.push($scope.PageAsset.CmsAssetSelectionFiles[i]);
                }
                var assetdata = {};
                assetdata.AssetArr = arr;
                assetdata.EntityID = parseInt($stateParams.ID);
                assetdata.FolderID = 0;
                assetdata.AttachType = attachType;
                ContentService.LinkpublishedfilestoCMS(assetdata).then(function (result) {
                    if (result.Response != null) {
                        if (redObj != '' && redObj != undefined) {
                            redObj.selection.restore();
                            angular.forEach(result.Response, function (item, key) {
                                if (attachType == 'image') {
                                    var ste = item.Ext.split('.').join("");
                                    if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                        redObj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                                    } else {
                                        redObj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'DAMFiles/StaticPreview/&amp;name=' + ste + '.png" alt="Image">');
                                    }
                                } else if (attachType == 'link') {
                                    var token = new Date().getTime();
                                    redObj.insert.htmlWithoutClean('&nbsp;<a href="DAMDownload.aspx?FileID=' + item.FileGuid + '&FileFriendlyName=' + item.AssetName + item.Ext + '&Ext=' + item.Ext + '&token=' + token + '">' + item.AssetName + '</a>');
                                } else if (attachType == 'video') {
                                    var videofilepath = TenantFilePath + "CMSFiles/Templates/Preview/" + item.FileGuid + ".mp4";
                                    var vidImagePath = TenantFilePath + "CMSFiles/Templates/Preview/Big_" + item.FileGuid + ".jpg"
                                    redObj.insert.htmlWithoutClean('<div class="iv-p-videoPlayer embed-responsive embed-responsive-16by9 tmplt-video"><video  class="video-js" controls="" preload="none" name="media" poster="' + vidImagePath + '"><source type="video/mp4" src="' + videofilepath + '"></video></div>');
                                }
                            });
                            NotifySuccess("File attached successfully");
                        }
                    } else { }
                    $timeout(function () {
                        $("#attachmentsection").modal("hide");
                        $scope.PageAsset.CmsAssetSelectionFiles = [];
                    }, 500);
                    $scope.MediaBankSettings.ProductionTypeID = 0;
                    $scope.MediaBankSettings.FolderName = '';
                    $scope.MediaBankSettings.frmCMS = false;
                });
            });
        };
    }
    app.controller('mui.cms.default.detail.section.contentCtrl', ['$scope', '$location', '$window', '$timeout', '$compile', '$resource', '$stateParams', '$translate', 'ContentService',muicmscontentCtrl]);
    function ContentService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetListOfCmsCustomStyle: GetListOfCmsCustomStyle,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetAssetActiveFileinfo: GetAssetActiveFileinfo,
            GetAllCmsSnippetTemplates: GetAllCmsSnippetTemplates,
            GetRevisedContentByFeature: GetRevisedContentByFeature,
            InsertRevisedEntityContent: InsertRevisedEntityContent,
            LinkpublishedfilestoCMS: LinkpublishedfilestoCMS
        });
        function GetListOfCmsCustomStyle() { var request = $http({ method: 'get', url: 'api/metadata/GetListOfCmsCustomStyle', params: { action: 'get' } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAssetActiveFileinfo(assetId) { var request = $http({ method: "get", url: "api/dam/GetAssetActiveFileinfo/" + assetId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllCmsSnippetTemplates() { var request = $http({ method: "get", url: "api/cms/GetAllCmsSnippetTemplates/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetRevisedContentByFeature(EntityID) { var request = $http({ method: "get", url: "api/cms/GetRevisedContentByFeature/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertRevisedEntityContent(revisecmsentityobj) { var request = $http({ method: "post", url: "api/cms/InsertRevisedEntityContent/", params: { action: "add" }, data: revisecmsentityobj }); return (request.then(handleSuccess, handleError)); }
        function LinkpublishedfilestoCMS(jobj) { var request = $http({ method: "post", url: "api/cms/LinkpublishedfilestoCMS/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ContentService", ['$http', '$q', ContentService]);
})(angular, app);
///#source 1 1 /assets/js/Redactor/redactor.js
/*
	Redactor 10.2.1
	Updated: July 6, 2015

	http://imperavi.com/redactor/

	Copyright (c) 2009-2015, Imperavi LLC.
	License: http://imperavi.com/redactor/license/

	Usage: $('#content').redactor();
*/

(function($)
{

	'use strict';

	if (!Function.prototype.bind)
	{
		Function.prototype.bind = function(scope)
		{
			var fn = this;
			return function()
			{
				return fn.apply(scope);
			};
		};
	}

	var uuid = 0;

	// Plugin
	$.fn.redactor = function(options)
	{
		var val = [];
		var args = Array.prototype.slice.call(arguments, 1);

		if (typeof options === 'string')
		{
			this.each(function()
			{
				var instance = $.data(this, 'redactor');
				var func;

				if (options.search(/\./) != '-1')
				{
					func = options.split('.');
					if (typeof instance[func[0]] != 'undefined')
					{
						func = instance[func[0]][func[1]];
					}
				}
				else
				{
					func = instance[options];
				}

				if (typeof instance !== 'undefined' && $.isFunction(func))
				{
					var methodVal = func.apply(instance, args);
					if (methodVal !== undefined && methodVal !== instance)
					{
						val.push(methodVal);
					}
				}
				else
				{
					$.error('No such method "' + options + '" for Redactor');
				}
			});
		}
		else
		{
			this.each(function()
			{
				$.data(this, 'redactor', {});
				$.data(this, 'redactor', Redactor(this, options));
			});
		}

		if (val.length === 0) return this;
		else if (val.length === 1) return val[0];
		else return val;

	};

	// Initialization
	function Redactor(el, options)
	{
		return new Redactor.prototype.init(el, options);
	}

	// Functionality
	$.Redactor = Redactor;
	$.Redactor.VERSION = '10.2.1';
	$.Redactor.modules = ['alignment', 'autosave', 'block', 'buffer', 'build', 'button',
						  'caret', 'clean', 'code', 'core', 'dropdown', 'file', 'focus',
						  'image', 'indent', 'inline', 'insert', 'keydown', 'keyup',
						  'lang', 'line', 'link', 'linkify', 'list', 'modal', 'observe', 'paragraphize',
						  'paste', 'placeholder', 'progress', 'selection', 'shortcuts',
						  'tabifier', 'tidy',  'toolbar', 'upload', 'utils'];

	$.Redactor.opts = {

		// settings
		lang: 'en',
		direction: 'ltr', // ltr or rtl

		plugins: false, // array

		focus: false,
		focusEnd: false,

		placeholder: false,

		visual: true,
		tabindex: false,

		minHeight: false,
		maxHeight: false,

		linebreaks: false,
		replaceDivs: true,
		paragraphize: true,
		cleanStyleOnEnter: false,
		enterKey: true,

		cleanOnPaste: true,
		cleanSpaces: true,
		pastePlainText: false,

		autosave: false, // false or url
		autosaveName: false,
		autosaveInterval: 60, // seconds
		autosaveOnChange: false,
		autosaveFields: false,

		linkTooltip: true,
		linkProtocol: 'http',
		linkNofollow: false,
		linkSize: 50,

		imageEditable: true,
		imageLink: true,
		imagePosition: true,
		imageFloatMargin: '10px',
		imageResizable: true,

		imageUpload: null,
		imageUploadParam: 'file',

		uploadImageField: false,

		dragImageUpload: true,

		fileUpload: null,
		fileUploadParam: 'file',

		dragFileUpload: true,

		s3: false,

		convertLinks: true,
		convertUrlLinks: true,
		convertImageLinks: true,
		convertVideoLinks: true,

		preSpaces: 4, // or false
		tabAsSpaces: false, // true or number of spaces
		tabKey: true,

		scrollTarget: false,

		toolbar: true,
		toolbarFixed: true,
		toolbarFixedTarget: document,
		toolbarFixedTopOffset: 0, // pixels
		toolbarExternal: false, // ID selector
		toolbarOverflow: false,

		source: true,
		buttons: ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist',
				  'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule'], // + 'underline'

		buttonsHide: [],
		buttonsHideOnMobile: [],

		formatting: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
		formattingAdd: false,

		tabifier: true,

		deniedTags: ['script', 'style'],
		allowedTags: false, // or array

		paragraphizeBlocks: ['table', 'div', 'pre', 'form', 'ul', 'ol', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'dl', 'blockquote', 'figcaption',
							'address', 'section', 'header', 'footer', 'aside', 'article', 'object', 'style', 'script', 'iframe', 'select', 'input', 'textarea',
							'button', 'option', 'map', 'area', 'math', 'hr', 'fieldset', 'legend', 'hgroup', 'nav', 'figure', 'details', 'menu', 'summary', 'p'],

		removeComments: false,
		replaceTags: [
			['strike', 'del'],
			['b', 'strong']
		],
		replaceStyles: [
            ['font-weight:\\s?bold', "strong"],
            ['font-style:\\s?italic', "em"],
            ['text-decoration:\\s?underline', "u"],
            ['text-decoration:\\s?line-through', 'del']
        ],
        removeDataAttr: false,

		removeAttr: false, // or multi array
		allowedAttr: false, // or multi array

		removeWithoutAttr: ['span'], // or false
		removeEmpty: ['p'], // or false;

		activeButtons: ['deleted', 'italic', 'bold', 'underline', 'unorderedlist', 'orderedlist',
						'alignleft', 'aligncenter', 'alignright', 'justify'],
		activeButtonsStates: {
			b: 'bold',
			strong: 'bold',
			i: 'italic',
			em: 'italic',
			del: 'deleted',
			strike: 'deleted',
			ul: 'unorderedlist',
			ol: 'orderedlist',
			u: 'underline'
		},

		shortcuts: {
			'ctrl+shift+m, meta+shift+m': { func: 'inline.removeFormat' },
			'ctrl+b, meta+b': { func: 'inline.format', params: ['bold'] },
			'ctrl+i, meta+i': { func: 'inline.format', params: ['italic'] },
			'ctrl+h, meta+h': { func: 'inline.format', params: ['superscript'] },
			'ctrl+l, meta+l': { func: 'inline.format', params: ['subscript'] },
			'ctrl+k, meta+k': { func: 'link.show' },
			'ctrl+shift+7':   { func: 'list.toggle', params: ['orderedlist'] },
			'ctrl+shift+8':   { func: 'list.toggle', params: ['unorderedlist'] }
		},
		shortcutsAdd: false,

		// private
		buffer: [],
		rebuffer: [],
		emptyHtml: '<p>&#x200b;</p>',
		invisibleSpace: '&#x200b;',
		imageTypes: ['image/png', 'image/jpeg', 'image/gif'],
		indentValue: 20,
		verifiedTags: 		['a', 'img', 'b', 'strong', 'sub', 'sup', 'i', 'em', 'u', 'small', 'strike', 'del', 'cite', 'ul', 'ol', 'li'], // and for span tag special rule
		inlineTags: 		['strong', 'b', 'u', 'em', 'i', 'code', 'del', 'ins', 'samp', 'kbd', 'sup', 'sub', 'mark', 'var', 'cite', 'small'],
		alignmentTags: 		['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6',  'DL', 'DT', 'DD', 'DIV', 'TD', 'BLOCKQUOTE', 'OUTPUT', 'FIGCAPTION', 'ADDRESS', 'SECTION', 'HEADER', 'FOOTER', 'ASIDE', 'ARTICLE'],
		blockLevelElements: ['PRE', 'UL', 'OL', 'LI'],
		highContrast: false,
		observe: {
			dropdowns: []
		},

		// lang
		langs: {
			en: {
				html: 'HTML',
				video: 'Insert Video',
				image: 'Insert Image',
				table: 'Table',
				link: 'Link',
				link_insert: 'Insert link',
				link_edit: 'Edit link',
				unlink: 'Unlink',
				formatting: 'Formatting',
				paragraph: 'Normal text',
				quote: 'Quote',
				code: 'Code',
				header1: 'Header 1',
				header2: 'Header 2',
				header3: 'Header 3',
				header4: 'Header 4',
				header5: 'Header 5',
				bold: 'Bold',
				italic: 'Italic',
				fontcolor: 'Font Color',
				backcolor: 'Back Color',
				unorderedlist: 'Unordered List',
				orderedlist: 'Ordered List',
				outdent: 'Outdent',
				indent: 'Indent',
				cancel: 'Cancel',
				insert: 'Insert',
				save: 'Save',
				_delete: 'Delete',
				insert_table: 'Insert Table',
				insert_row_above: 'Add Row Above',
				insert_row_below: 'Add Row Below',
				insert_column_left: 'Add Column Left',
				insert_column_right: 'Add Column Right',
				delete_column: 'Delete Column',
				delete_row: 'Delete Row',
				delete_table: 'Delete Table',
				rows: 'Rows',
				columns: 'Columns',
				add_head: 'Add Head',
				delete_head: 'Delete Head',
				title: 'Title',
				image_position: 'Position',
				none: 'None',
				left: 'Left',
				right: 'Right',
				center: 'Center',
				image_web_link: 'Image Web Link',
				text: 'Text',
				mailto: 'Email',
				web: 'URL',
				video_html_code: 'Video Embed Code or Youtube/Vimeo Link',
				file: 'Insert File',
				upload: 'Upload',
				download: 'Download',
				choose: 'Choose',
				or_choose: 'Or choose',
				drop_file_here: 'Drop file here',
				align_left: 'Align text to the left',
				align_center: 'Center text',
				align_right: 'Align text to the right',
				align_justify: 'Justify text',
				horizontalrule: 'Insert Horizontal Rule',
				deleted: 'Deleted',
				anchor: 'Anchor',
				link_new_tab: 'Open link in new tab',
				underline: 'Underline',
				alignment: 'Alignment',
				filename: 'Name (optional)',
				edit: 'Edit',
				upload_label: 'Drop file here or '
			}
		},

		linkify: {
			regexps: {
				youtube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.\-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig,
				vimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
				image: /((https?|www)[^\s]+\.)(jpe?g|png|gif)(\?[^\s-]+)?/ig,
				url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/ig,
			}
		},

		codemirror: false
	};

	// Functionality
	Redactor.fn = $.Redactor.prototype = {

		keyCode: {
			BACKSPACE: 8,
			DELETE: 46,
			UP: 38,
			DOWN: 40,
			ENTER: 13,
			SPACE: 32,
			ESC: 27,
			TAB: 9,
			CTRL: 17,
			META: 91,
			SHIFT: 16,
			ALT: 18,
			RIGHT: 39,
			LEFT: 37,
			LEFT_WIN: 91
		},

		// Initialization
		init: function(el, options)
		{
			this.$element = $(el);
			this.uuid = uuid++;

			// if paste event detected = true
			this.rtePaste = false;
			this.$pasteBox = false;

			this.loadOptions(options);
			this.loadModules();

			// formatting storage
			this.formatting = {};

			// block level tags
			$.merge(this.opts.blockLevelElements, this.opts.alignmentTags);
			this.reIsBlock = new RegExp('^(' + this.opts.blockLevelElements.join('|' ) + ')$', 'i');

			// setup allowed and denied tags
			this.tidy.setupAllowed();

			// setup denied tags
			if (this.opts.deniedTags !== false)
			{
				var tags = ['html', 'head', 'link', 'body', 'meta', 'applet'];
				for (var i = 0; i < tags.length; i++)
				{
					this.opts.deniedTags.push(tags[i]);
				}
			}

			// load lang
			this.lang.load();

			// extend shortcuts
			$.extend(this.opts.shortcuts, this.opts.shortcutsAdd);

			// start callback
			this.core.setCallback('start');

			// build
			this.start = true;
			this.build.run();
		},

		loadOptions: function(options)
		{
			this.opts = $.extend(
				{},
				$.extend(true, {}, $.Redactor.opts),
				this.$element.data(),
				options
			);
		},
		getModuleMethods: function(object)
		{
			return Object.getOwnPropertyNames(object).filter(function(property)
			{
				return typeof object[property] == 'function';
			});
		},
		loadModules: function()
		{
			var len = $.Redactor.modules.length;
			for (var i = 0; i < len; i++)
			{
				this.bindModuleMethods($.Redactor.modules[i]);
			}
		},
		bindModuleMethods: function(module)
		{
			if (typeof this[module] == 'undefined') return;

			// init module
			this[module] = this[module]();

			var methods = this.getModuleMethods(this[module]);
			var len = methods.length;

			// bind methods
			for (var z = 0; z < len; z++)
			{
				this[module][methods[z]] = this[module][methods[z]].bind(this);
			}
		},
		alignment: function()
		{
			return {
				left: function()
				{
					this.alignment.set('');
				},
				right: function()
				{
					this.alignment.set('right');
				},
				center: function()
				{
					this.alignment.set('center');
				},
				justify: function()
				{
					this.alignment.set('justify');
				},
				set: function(type)
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					// get blocks
					this.alignment.blocks = this.selection.getBlocks();
					this.alignment.type = type;

					// set alignment
					if (this.alignment.isLinebreaksOrNoBlocks())
					{
						this.alignment.setText();
					}
					else
					{
						this.alignment.setBlocks();
					}

					// sync
					this.selection.restore();
					this.code.sync();
				},
				setText: function()
				{
					var wrapper = this.selection.wrap('div');
					$(wrapper).attr('data-tagblock', 'redactor').css('text-align', this.alignment.type);
				},
				setBlocks: function()
				{
					$.each(this.alignment.blocks, $.proxy(function(i, el)
					{
						var $el = this.utils.getAlignmentElement(el);
						if (!$el) return;

						if (this.alignment.isNeedReplaceElement($el))
						{
							this.alignment.replaceElement($el);
						}
						else
						{
							this.alignment.alignElement($el);
						}

					}, this));
				},
				isLinebreaksOrNoBlocks: function()
				{
					return (this.opts.linebreaks && this.alignment.blocks[0] === false);
				},
				isNeedReplaceElement: function($el)
				{
					return (this.alignment.type === '' && typeof($el.data('tagblock')) !== 'undefined');
				},
				replaceElement: function($el)
				{
					$el.replaceWith($el.html());
				},
				alignElement: function($el)
				{
					$el.css('text-align', this.alignment.type);
					this.utils.removeEmptyAttr($el, 'style');
				}
			};
		},
		autosave: function()
		{
			return {
				html: false,
				enable: function()
				{
					if (!this.opts.autosave) return;

					this.autosave.name = (this.opts.autosaveName) ? this.opts.autosaveName : this.$textarea.attr('name');

					if (this.opts.autosaveOnChange) return;
					this.autosaveInterval = setInterval(this.autosave.load, this.opts.autosaveInterval * 1000);
				},
				onChange: function()
				{
					if (!this.opts.autosaveOnChange) return;
					this.autosave.load();
				},
				load: function()
				{
					this.autosave.source = this.code.get();

					if (this.autosave.html === this.autosave.source) return;

					// data
					var data = {};
					data['name'] = this.autosave.name;
					data[this.autosave.name] = this.autosave.source;
					data = this.autosave.getHiddenFields(data);

					// ajax
					var jsxhr = $.ajax({
						url: this.opts.autosave,
						type: 'post',
						data: data
					});

					jsxhr.done(this.autosave.success);
				},
				getHiddenFields: function(data)
				{
					if (this.opts.autosaveFields === false || typeof this.opts.autosaveFields !== 'object')
					{
						return data;
					}

					$.each(this.opts.autosaveFields, $.proxy(function(k, v)
					{
						if (v !== null && v.toString().indexOf('#') === 0) v = $(v).val();
						data[k] = v;

					}, this));

					return data;

				},
				success: function(data)
				{
					var json;
					try
					{
						json = $.parseJSON(data);
					}
					catch(e)
					{
						//data has already been parsed
						json = data;
					}

					var callbackName = (typeof json.error == 'undefined') ? 'autosave' :  'autosaveError';

					this.core.setCallback(callbackName, this.autosave.name, json);
					this.autosave.html = this.autosave.source;
				},
				disable: function()
				{
					clearInterval(this.autosaveInterval);
				}
			};
		},
		block: function()
		{
			return {
				formatting: function(name)
				{
					this.block.clearStyle = false;
					var type, value;

					if (typeof this.formatting[name].data != 'undefined') type = 'data';
					else if (typeof this.formatting[name].attr != 'undefined') type = 'attr';
					else if (typeof this.formatting[name]['class'] != 'undefined') type = 'class';

					if (typeof this.formatting[name].clear != 'undefined')
					{
						this.block.clearStyle = true;
					}

					if (type) value = this.formatting[name][type];

					this.block.format(this.formatting[name].tag, type, value);

				},
				format: function(tag, type, value)
				{
					if (tag == 'quote') tag = 'blockquote';

					var formatTags = ['p', 'pre', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
					if ($.inArray(tag, formatTags) == -1) return;

					this.block.isRemoveInline = (tag == 'pre' || tag.search(/h[1-6]/i) != -1);

					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					var html = $.trim(this.$editor.html());
					this.block.isEmpty = this.utils.isEmpty(html);

					// FF focus
					if (this.utils.browser('mozilla') && !this.focus.isFocused())
					{
						if (this.block.isEmpty)
						{
							var $first;
							if (!this.opts.linebreaks)
							{
								$first = this.$editor.children().first();
								this.caret.setEnd($first);
							}
						}
					}

					this.block.blocks = this.selection.getBlocks();

					this.block.blocksSize = this.block.blocks.length;
					this.block.type = type;
					this.block.value = value;

					this.buffer.set();
					this.selection.save();

					this.block.set(tag);

					this.selection.restore();
					this.code.sync();

				},
				set: function(tag)
				{

					this.selection.get();
					this.block.containerTag = this.range.commonAncestorContainer.tagName;

					if (this.range.collapsed)
					{
						this.block.setCollapsed(tag);
					}
					else
					{
						this.block.setMultiple(tag);
					}
				},
				setCollapsed: function(tag)
				{
					if (this.opts.linebreaks && this.block.isEmpty && tag != 'p')
					{
						var node = document.createElement(tag);
						this.$editor.html(node);
						this.caret.setEnd(node);

						return;
					}


					var block = this.block.blocks[0];
					if (block === false) return;

					if (block.tagName == 'LI')
					{
						if (tag != 'blockquote') return;

						this.block.formatListToBlockquote();
						return;
					}

					var isContainerTable = (this.block.containerTag  == 'TD' || this.block.containerTag  == 'TH');
					if (isContainerTable && !this.opts.linebreaks)
					{
						document.execCommand('formatblock', false, '<' + tag + '>');

						block = this.selection.getBlock();
						this.block.toggle($(block));

					}
					else if (block.tagName.toLowerCase() != tag)
					{
						if (this.opts.linebreaks && tag == 'p')
						{
							$(block).append('<br>');
							this.utils.replaceWithContents(block);
						}
						else
						{
							var $formatted = this.utils.replaceToTag(block, tag);

							this.block.toggle($formatted);

							if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();
							if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
							if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();

							this.block.formatTableWrapping($formatted);
						}
					}
					else if (tag == 'blockquote' && block.tagName.toLowerCase() == tag)
					{
						// blockquote off
						if (this.opts.linebreaks)
						{
							$(block).append('<br>');
							this.utils.replaceWithContents(block);
						}
						else
						{
							var $el = this.utils.replaceToTag(block, 'p');
							this.block.toggle($el);
						}
					}
					else if (block.tagName.toLowerCase() == tag)
					{
						this.block.toggle($(block));
					}



					if (typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
					{
						$(block).removeAttr('class').removeAttr('style');
					}

				},
				setMultiple: function(tag)
				{
					var block = this.block.blocks[0];
					var isContainerTable = (this.block.containerTag  == 'TD' || this.block.containerTag  == 'TH');

					if (block !== false && this.block.blocksSize === 1)
					{
						if (block.tagName.toLowerCase() == tag &&  tag == 'blockquote')
						{
							// blockquote off
							if (this.opts.linebreaks)
							{
								$(block).append('<br>');
								this.utils.replaceWithContents(block);
							}
							else
							{
								var $el = this.utils.replaceToTag(block, 'p');
								this.block.toggle($el);
							}
						}
						else if (block.tagName == 'LI')
						{
							if (tag != 'blockquote') return;

							this.block.formatListToBlockquote();
						}
						else if (this.block.containerTag == 'BLOCKQUOTE')
						{
							this.block.formatBlockquote(tag);
						}
						else if (this.opts.linebreaks && ((isContainerTable) || (this.range.commonAncestorContainer != block)))
						{
							this.block.formatWrap(tag);
						}
						else
						{
							if (this.opts.linebreaks && tag == 'p')
							{
								$(block).prepend('<br>').append('<br>');
								this.utils.replaceWithContents(block);
							}
							else if (block.tagName === 'TD')
							{
								this.block.formatWrap(tag);
							}
							else
							{
								var $formatted = this.utils.replaceToTag(block, tag);

								this.block.toggle($formatted);

								if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
								if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();
							}
						}
					}
					else
					{
						if (this.opts.linebreaks || tag != 'p')
						{
							if (tag == 'blockquote')
							{
								var count = 0;
								for (var i = 0; i < this.block.blocksSize; i++)
								{
									if (this.block.blocks[i].tagName == 'BLOCKQUOTE') count++;
								}

								// only blockquote selected
								if (count == this.block.blocksSize)
								{
									$.each(this.block.blocks, $.proxy(function(i,s)
									{
										var $formatted = false;
										if (this.opts.linebreaks)
										{
											$(s).prepend('<br>').append('<br>');
											$formatted = this.utils.replaceWithContents(s);
										}
										else
										{
											$formatted = this.utils.replaceToTag(s, 'p');
										}

										if ($formatted && typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
										{
											$formatted.removeAttr('class').removeAttr('style');
										}

									}, this));

									return;
								}

							}

							this.block.formatWrap(tag);
						}
						else
						{
							var classSize = 0;
							var toggleType = false;
							if (this.block.type == 'class')
							{
								toggleType = 'toggle';
								classSize = $(this.block.blocks).filter('.' + this.block.value).length;

								if (this.block.blocksSize == classSize) toggleType = 'toggle';
								else if (this.block.blocksSize > classSize) toggleType = 'set';
								else if (classSize === 0) toggleType = 'set';

							}

							var exceptTags = ['ul', 'ol', 'li', 'td', 'th', 'dl', 'dt', 'dd'];
							$.each(this.block.blocks, $.proxy(function(i,s)
							{
								if ($.inArray(s.tagName.toLowerCase(), exceptTags) != -1) return;

								var $formatted = this.utils.replaceToTag(s, tag);

								if (toggleType)
								{
									if (toggleType == 'toggle') this.block.toggle($formatted);
									else if (toggleType == 'remove') this.block.remove($formatted);
									else if (toggleType == 'set') this.block.setForce($formatted);
								}
								else this.block.toggle($formatted);

								if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();
								if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
								if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();

								if (typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
								{
									$formatted.removeAttr('class').removeAttr('style');
								}


							}, this));
						}
					}
				},
				setForce: function($el)
				{
					// remove style and class if the specified setting
					if (this.block.clearStyle)
					{
						$el.removeAttr('class').removeAttr('style');
					}

					if (this.block.type == 'class')
					{
						$el.addClass(this.block.value);
						return;
					}
					else if (this.block.type == 'attr' || this.block.type == 'data')
					{
						$el.attr(this.block.value.name, this.block.value.value);
						return;
					}
				},
				toggle: function($el)
				{
					// remove style and class if the specified setting
					if (this.block.clearStyle)
					{
						$el.removeAttr('class').removeAttr('style');
					}

					if (this.block.type == 'class')
					{
						$el.toggleClass(this.block.value);
						return;
					}
					else if (this.block.type == 'attr' || this.block.type == 'data')
					{
						if ($el.attr(this.block.value.name) == this.block.value.value)
						{
							$el.removeAttr(this.block.value.name);
						}
						else
						{
							$el.attr(this.block.value.name, this.block.value.value);
						}

						return;
					}
					else
					{
						$el.removeAttr('style class');
						return;
					}
				},
				remove: function($el)
				{
					$el.removeClass(this.block.value);
				},
				formatListToBlockquote: function()
				{
					var block = $(this.block.blocks[0]).closest('ul, ol', this.$editor[0]);

					$(block).find('ul, ol').contents().unwrap();
					$(block).find('li').append($('<br>')).contents().unwrap();

					var $el = this.utils.replaceToTag(block, 'blockquote');
					this.block.toggle($el);
				},
				formatBlockquote: function(tag)
				{
					document.execCommand('outdent');
					document.execCommand('formatblock', false, tag);

					this.clean.clearUnverified();
					this.$editor.find('p:empty').remove();

					var formatted = this.selection.getBlock();

					if (tag != 'p')
					{
						$(formatted).find('img').remove();
					}

					if (!this.opts.linebreaks)
					{
						this.block.toggle($(formatted));
					}

					this.$editor.find('ul, ol, tr, blockquote, p').each($.proxy(this.utils.removeEmpty, this));

					if (this.opts.linebreaks && tag == 'p')
					{
						this.utils.replaceWithContents(formatted);
					}

				},
				formatWrap: function(tag)
				{
					if (this.block.containerTag == 'UL' || this.block.containerTag == 'OL')
					{
						if (tag == 'blockquote')
						{
							this.block.formatListToBlockquote();
						}
						else
						{
							return;
						}
					}

					var formatted = this.selection.wrap(tag);
					if (formatted === false) return;

					var $formatted = $(formatted);

					this.block.formatTableWrapping($formatted);

					var $elements = $formatted.find(this.opts.blockLevelElements.join(',') + ', td, table, thead, tbody, tfoot, th, tr');

					$elements.contents().unwrap();

					if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();

					$.each(this.block.blocks, $.proxy(this.utils.removeEmpty, this));

					$formatted.append(this.selection.getMarker(2));

					if (!this.opts.linebreaks)
					{
						this.block.toggle($formatted);
					}

					this.$editor.find('ul, ol, tr, blockquote, p').each($.proxy(this.utils.removeEmpty, this));
					$formatted.find('blockquote:empty').remove();

					if (this.block.isRemoveInline)
					{
						this.utils.removeInlineTags($formatted);
					}

					if (this.opts.linebreaks && tag == 'p')
					{
						this.utils.replaceWithContents($formatted);
					}

					if (this.opts.linebreaks)
					{
						var $next = $formatted.next().next();
						if ($next.size() != 0 && $next[0].tagName === 'BR')
						{
							$next.remove();
						}
					}



				},
				formatTableWrapping: function($formatted)
				{
					if ($formatted.closest('table', this.$editor[0]).length === 0) return;

					if ($formatted.closest('tr', this.$editor[0]).length === 0) $formatted.wrap('<tr>');
					if ($formatted.closest('td', this.$editor[0]).length === 0 && $formatted.closest('th').length === 0)
					{
						$formatted.wrap('<td>');
					}
				},
				removeData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeAttr('data-' + name);

					this.code.sync();
				},
				setData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).attr('data-' + name, value);

					this.code.sync();
				},
				toggleData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$.each(blocks, function()
					{
						if ($(this).attr('data-' + name))
						{
							$(this).removeAttr('data-' + name);
						}
						else
						{
							$(this).attr('data-' + name, value);
						}
					});
				},
				removeAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeAttr(attr);

					this.code.sync();
				},
				setAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).attr(attr, value);

					this.code.sync();
				},
				toggleAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$.each(blocks, function()
					{
						if ($(this).attr(name))
						{
							$(this).removeAttr(name);
						}
						else
						{
							$(this).attr(name, value);
						}
					});
				},
				removeClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeClass(className);

					this.utils.removeEmptyAttr(blocks, 'class');

					this.code.sync();
				},
				setClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).addClass(className);

					this.code.sync();
				},
				toggleClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).toggleClass(className);

					this.code.sync();
				}
			};
		},
		buffer: function()
		{
			return {
				set: function(type)
				{
					if (typeof type == 'undefined' || type == 'undo')
					{
						this.buffer.setUndo();
					}
					else
					{
						this.buffer.setRedo();
					}
				},
				setUndo: function()
				{
					this.selection.save();
					this.opts.buffer.push(this.$editor.html());
					this.selection.restore();
				},
				setRedo: function()
				{
					this.selection.save();
					this.opts.rebuffer.push(this.$editor.html());
					this.selection.restore();
				},
				getUndo: function()
				{
					this.$editor.html(this.opts.buffer.pop());
				},
				getRedo: function()
				{
					this.$editor.html(this.opts.rebuffer.pop());
				},
				add: function()
				{
					this.opts.buffer.push(this.$editor.html());
				},
				undo: function()
				{
					if (this.opts.buffer.length === 0) return;

					this.buffer.set('redo');
					this.buffer.getUndo();

					this.selection.restore();

					setTimeout($.proxy(this.observe.load, this), 50);
				},
				redo: function()
				{
					if (this.opts.rebuffer.length === 0) return;

					this.buffer.set('undo');
					this.buffer.getRedo();

					this.selection.restore();

					setTimeout($.proxy(this.observe.load, this), 50);
				}
			};
		},
		build: function()
		{
			return {
				run: function()
				{
					this.build.createContainerBox();
					this.build.loadContent();
					this.build.loadEditor();
					this.build.enableEditor();
					this.build.setCodeAndCall();
				},
				isTextarea: function()
				{
					return (this.$element[0].tagName === 'TEXTAREA');
				},
				createContainerBox: function()
				{
					this.$box = $('<div class="redactor-box" role="application" />');
				},
				createTextarea: function()
				{
					this.$textarea = $('<textarea />').attr('name', this.build.getTextareaName());
				},
				getTextareaName: function()
				{
					return ((typeof(name) == 'undefined')) ? 'content-' + this.uuid : this.$element.attr('id');
				},
				loadContent: function()
				{
					var func = (this.build.isTextarea()) ? 'val' : 'html';
					this.content = $.trim(this.$element[func]());
				},
				enableEditor: function()
				{
					this.$editor.attr({ 'contenteditable': true, 'dir': this.opts.direction });
				},
				loadEditor: function()
				{
					var func = (this.build.isTextarea()) ? 'fromTextarea' : 'fromElement';
					this.build[func]();
				},
				fromTextarea: function()
				{
					this.$editor = $('<div />');
					this.$textarea = this.$element;
					this.$box.insertAfter(this.$element).append(this.$editor).append(this.$element);
					this.$editor.addClass('redactor-editor');

					this.$element.hide();
				},
				fromElement: function()
				{
					this.$editor = this.$element;
					this.build.createTextarea();
					this.$box.insertAfter(this.$editor).append(this.$editor).append(this.$textarea);
					this.$editor.addClass('redactor-editor');

					this.$textarea.hide();
				},
				setCodeAndCall: function()
				{
					// set code
					this.code.set(this.content);

					this.build.setOptions();
					this.build.callEditor();

					// code mode
					if (this.opts.visual) return;
					setTimeout($.proxy(this.code.showCode, this), 200);
				},
				callEditor: function()
				{
					this.build.disableMozillaEditing();
					this.build.disableIeLinks();
					this.build.setEvents();
					this.build.setHelpers();

					// load toolbar
					if (this.opts.toolbar)
					{
						this.opts.toolbar = this.toolbar.init();
						this.toolbar.build();
					}

					// modal templates init
					this.modal.loadTemplates();

					// plugins
					this.build.plugins();

					// observers
					setTimeout($.proxy(this.observe.load, this), 4);

					// init callback
					this.core.setCallback('init');
				},
				setOptions: function()
				{
					// textarea direction
					$(this.$textarea).attr('dir', this.opts.direction);

					if (this.opts.linebreaks) this.$editor.addClass('redactor-linebreaks');

					if (this.opts.tabindex) this.$editor.attr('tabindex', this.opts.tabindex);

					if (this.opts.minHeight) this.$editor.css('minHeight', this.opts.minHeight);
					if (this.opts.maxHeight) this.$editor.css('maxHeight', this.opts.maxHeight);

				},
				setEventDropUpload: function(e)
				{
					e.preventDefault();

					if (!this.opts.dragImageUpload || !this.opts.dragFileUpload) return;

					var files = e.dataTransfer.files;
					this.upload.directUpload(files[0], e);
				},
				setEventDrop: function(e)
				{
					this.code.sync();
					setTimeout(this.clean.clearUnverified, 1);
					this.core.setCallback('drop', e);
				},
				setEvents: function()
				{
					// drop
					this.$editor.on('drop.redactor', $.proxy(function(e)
					{
						e = e.originalEvent || e;

						if (window.FormData === undefined || !e.dataTransfer) return true;

						if (e.dataTransfer.files.length === 0)
						{
							return this.build.setEventDrop(e);
						}
						else
						{
							this.build.setEventDropUpload(e);
						}

						setTimeout(this.clean.clearUnverified, 1);
						this.core.setCallback('drop', e);

					}, this));


					// click
					this.$editor.on('click.redactor', $.proxy(function(e)
					{
						var event = this.core.getEvent();
						var type = (event == 'click' || event == 'arrow') ? false : 'click';

						this.core.addEvent(type);
						this.utils.disableSelectAll();
						this.core.setCallback('click', e);

					}, this));

					// paste
					this.$editor.on('paste.redactor', $.proxy(this.paste.init, this));

					// cut
					this.$editor.on('cut.redactor', $.proxy(this.code.sync, this));

					// keydown
					this.$editor.on('keydown.redactor', $.proxy(this.keydown.init, this));

					// keyup
					this.$editor.on('keyup.redactor', $.proxy(this.keyup.init, this));

					// textarea keydown
					if ($.isFunction(this.opts.codeKeydownCallback))
					{
						this.$textarea.on('keydown.redactor-textarea', $.proxy(this.opts.codeKeydownCallback, this));
					}

					// textarea keyup
					if ($.isFunction(this.opts.codeKeyupCallback))
					{
						this.$textarea.on('keyup.redactor-textarea', $.proxy(this.opts.codeKeyupCallback, this));
					}

					// focus
					if ($.isFunction(this.opts.focusCallback))
					{
						this.$editor.on('focus.redactor', $.proxy(this.opts.focusCallback, this));
					}

					$(document).on('mousedown.redactor.' + this.uuid, $.proxy(function(e) { this.blurClickedElement = e.target; }, this));


					// blur
					this.$editor.on('blur.redactor', $.proxy(function(e)
					{
						if (this.start) return;
						if (this.rtePaste) return;
						if (!this.build.isBlured()) return;

						this.utils.disableSelectAll();
						if ($.isFunction(this.opts.blurCallback)) this.core.setCallback('blur', e);

					}, this));
				},
				isBlured: function()
				{
					if (this.blurClickedElement === true) return true;

					var $el = $(this.blurClickedElement);

					return (!$el.hasClass('redactor-toolbar, redactor-dropdown') && !$el.is('#redactor-modal') && $el.parents('.redactor-toolbar, .redactor-dropdown, #redactor-modal').length === 0);
				},
				setHelpers: function()
				{
					// linkify
					if (this.linkify.isEnabled())
					{
						this.linkify.format();
					}

					// placeholder
					this.placeholder.enable();

					// focus
					if (this.opts.focus) setTimeout(this.focus.setStart, 100);
					if (this.opts.focusEnd) setTimeout(this.focus.setEnd, 100);

				},
				plugins: function()
				{
					if (!this.opts.plugins) return;

					$.each(this.opts.plugins, $.proxy(function(i, s)
					{
						var func = (typeof RedactorPlugins !== 'undefined' && typeof RedactorPlugins[s] !== 'undefined') ? RedactorPlugins : Redactor.fn;

						if (!$.isFunction(func[s]))
						{
							return;
						}

						this[s] = func[s]();

						// get methods
						var methods = this.getModuleMethods(this[s]);
						var len = methods.length;

						// bind methods
						for (var z = 0; z < len; z++)
						{
							this[s][methods[z]] = this[s][methods[z]].bind(this);
						}

						if ($.isFunction(this[s].init))
						{
							this[s].init();
						}


					}, this));

				},
				disableMozillaEditing: function()
				{
					if (!this.utils.browser('mozilla')) return;

					// FF fix
					try {
						document.execCommand('enableObjectResizing', false, false);
						document.execCommand('enableInlineTableEditing', false, false);
					} catch (e) {}
				},
				disableIeLinks: function()
				{
					if (!this.utils.browser('msie')) return;

					// IE prevent converting links
					document.execCommand("AutoUrlDetect", false, false);
				}
			};
		},
		button: function()
		{
			return {
				build: function(btnName, btnObject)
				{
					var $button = $('<a href="#" class="re-icon re-' + btnName + '" rel="' + btnName + '" />').attr({'role': 'button', 'aria-label': btnObject.title, 'tabindex': '-1'});

					// click
					if (btnObject.func || btnObject.command || btnObject.dropdown)
					{
						this.button.setEvent($button, btnName, btnObject);
					}

					// dropdown
					if (btnObject.dropdown)
					{
						$button.addClass('redactor-toolbar-link-dropdown').attr('aria-haspopup', true);

						var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + ' redactor-dropdown-box-' + btnName + '" style="display: none;">');
						$button.data('dropdown', $dropdown);
						this.dropdown.build(btnName, $dropdown, btnObject.dropdown);
					}

					// tooltip
					if (this.utils.isDesktop())
					{
						this.button.createTooltip($button, btnName, btnObject.title);
					}

					return $button;
				},
				setEvent: function($button, btnName, btnObject)
				{
					$button.on('touchstart click', $.proxy(function(e)
					{
						if ($button.hasClass('redactor-button-disabled')) return false;

						var type = 'func';
						var callback = btnObject.func;

						if (btnObject.command)
						{
							type = 'command';
							callback = btnObject.command;
						}
						else if (btnObject.dropdown)
						{
							type = 'dropdown';
							callback = false;
						}

						this.button.onClick(e, btnName, type, callback);

					}, this));
				},
				createTooltip: function($button, name, title)
				{
					var $tooltip = $('<span>').addClass('redactor-toolbar-tooltip redactor-toolbar-tooltip-' + this.uuid + ' redactor-toolbar-tooltip-' + name).hide().html(title);
					$tooltip.appendTo('body');

					$button.on('mouseover', function()
					{
						if ($(this).hasClass('redactor-button-disabled')) return;

						var pos = $button.offset();

						$tooltip.show();
						$tooltip.css({
							top: (pos.top + $button.innerHeight()) + 'px',
							left: (pos.left + $button.innerWidth()/2 - $tooltip.innerWidth()/2) + 'px'
						});
					});

					$button.on('mouseout', function()
					{
						$tooltip.hide();
					});

				},
				onClick: function(e, btnName, type, callback)
				{
					this.button.caretOffset = this.caret.getOffset();

					e.preventDefault();

					if (this.utils.browser('msie')) e.returnValue = false;

					if (type == 'command') this.inline.format(callback);
					else if (type == 'dropdown') this.dropdown.show(e, btnName);
					else this.button.onClickCallback(e, callback, btnName);
				},
				onClickCallback: function(e, callback, btnName)
				{
					var func;

					// blur
					this.blurClickedElement = true;

					if ($.isFunction(callback)) callback.call(this, btnName);
					else if (callback.search(/\./) != '-1')
					{
						func = callback.split('.');
						if (typeof this[func[0]] == 'undefined') return;

						this[func[0]][func[1]](btnName);
					}
					else this[callback](btnName);

					this.observe.buttons(e, btnName);
				},
				get: function(key)
				{
					return this.$toolbar.find('a.re-' + key);
				},
				setActive: function(key)
				{
					this.button.get(key).addClass('redactor-act');
				},
				setInactive: function(key)
				{
					this.button.get(key).removeClass('redactor-act');
				},
				setInactiveAll: function(key)
				{
					if (typeof key === 'undefined')
					{
						this.$toolbar.find('a.re-icon').removeClass('redactor-act');
					}
					else
					{
						this.$toolbar.find('a.re-icon').not('.re-' + key).removeClass('redactor-act');
					}
				},
				setActiveInVisual: function()
				{
					this.$toolbar.find('a.re-icon').not('a.re-html').removeClass('redactor-button-disabled');
				},
				setInactiveInCode: function()
				{
					this.$toolbar.find('a.re-icon').not('a.re-html').addClass('redactor-button-disabled');
				},
				changeIcon: function(key, classname)
				{
					this.button.get(key).addClass('re-' + classname);
				},
				removeIcon: function(key, classname)
				{
					this.button.get(key).removeClass('re-' + classname);
				},
				setAwesome: function(key, name)
				{
					var $button = this.button.get(key);
					$button.removeClass('redactor-btn-image').addClass('fa-redactor-btn');
					$button.html('<i class="' + name + '"></i>');
				},
				addCallback: function($btn, callback)
				{
					if ($btn == "buffer") return;

					var type = (callback == 'dropdown') ? 'dropdown' : 'func';
					var key = $btn.attr('rel');
					$btn.on('touchstart click', $.proxy(function(e)
					{
						if ($btn.hasClass('redactor-button-disabled')) return false;
						this.button.onClick(e, key, type, callback);

					}, this));
				},
				addDropdown: function($btn, dropdown)
				{
					$btn.addClass('redactor-toolbar-link-dropdown').attr('aria-haspopup', true);

					var key = $btn.attr('rel');
					this.button.addCallback($btn, 'dropdown');

					var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + ' redactor-dropdown-box-' + key + '" style="display: none;">');
					$btn.data('dropdown', $dropdown);

					// build dropdown
					if (dropdown) this.dropdown.build(key, $dropdown, dropdown);

					return $dropdown;
				},
				add: function(key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');

					this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				addFirst: function(key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					this.$toolbar.prepend($('<li>').append(btn));

					return btn;
				},
				addAfter: function(afterkey, key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					var $btn = this.button.get(afterkey);

					if ($btn.length !== 0) $btn.parent().after($('<li>').append(btn));
					else this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				addBefore: function(beforekey, key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					var $btn = this.button.get(beforekey);

					if ($btn.length !== 0) $btn.parent().before($('<li>').append(btn));
					else this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				remove: function(key)
				{
					this.button.get(key).remove();
				},
				isMobileUndoRedo: function(key)
				{
					return (key == "undo" || key == "redo") && !this.utils.isDesktop();
				}
			};
		},
		caret: function()
		{
			return {
				setStart: function(node)
				{
					// inline tag
					if (!this.utils.isBlock(node))
					{
						var space = this.utils.createSpaceElement();

						$(node).prepend(space);
						this.caret.setEnd(space);
					}
					else
					{
						this.caret.set(node, 0, node, 0);
					}
				},
				setEnd: function(node)
				{
					node = node[0] || node;
					if (node.lastChild.nodeType == 1)
					{
						return this.caret.setAfter(node.lastChild);
					}

					this.caret.set(node, 1, node, 1);

				},
				set: function(orgn, orgo, focn, foco)
				{
					// focus
					// disabled in 10.0.7
					// if (!this.utils.browser('msie')) this.$editor.focus();

					orgn = orgn[0] || orgn;
					focn = focn[0] || focn;

					if (this.utils.isBlockTag(orgn.tagName) && orgn.innerHTML === '')
					{
						orgn.innerHTML = this.opts.invisibleSpace;
					}

					if (orgn.tagName == 'BR' && this.opts.linebreaks === false)
					{
						var parent = $(this.opts.emptyHtml)[0];
						$(orgn).replaceWith(parent);
						orgn = parent;
						focn = orgn;
					}

					this.selection.get();

					try
					{
						this.range.setStart(orgn, orgo);
						this.range.setEnd(focn, foco);
					}
					catch (e) {}

					this.selection.addRange();
				},
				setAfter: function(node)
				{
					try
					{
						var tag = $(node)[0].tagName;

						// inline tag
						if (tag != 'BR' && !this.utils.isBlock(node))
						{
							var space = this.utils.createSpaceElement();

							$(node).after(space);
							this.caret.setEnd(space);
						}
						else
						{
							if (tag != 'BR' && this.utils.browser('msie'))
							{
								this.caret.setStart($(node).next());
							}
							else
							{
								this.caret.setAfterOrBefore(node, 'after');
							}
						}
					}
					catch (e)
					{
						var space = this.utils.createSpaceElement();
						$(node).after(space);
						this.caret.setEnd(space);
					}
				},
				setBefore: function(node)
				{
					// block tag
					if (this.utils.isBlock(node))
					{
						this.caret.setEnd($(node).prev());
					}
					else
					{
						this.caret.setAfterOrBefore(node, 'before');
					}
				},
				setAfterOrBefore: function(node, type)
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					node = node[0] || node;

					this.selection.get();

					if (type == 'after')
					{
						try {

							this.range.setStartAfter(node);
							this.range.setEndAfter(node);
						}
						catch (e) {}
					}
					else
					{
						try {
							this.range.setStartBefore(node);
							this.range.setEndBefore(node);
						}
						catch (e) {}
					}


					this.range.collapse(false);
					this.selection.addRange();
				},
				getOffsetOfElement: function(node)
				{
					node = node[0] || node;

					this.selection.get();

					var cloned = this.range.cloneRange();
					cloned.selectNodeContents(node);
					cloned.setEnd(this.range.endContainer, this.range.endOffset);

					return $.trim(cloned.toString()).length;
				},
				getOffset: function()
				{
					var offset = 0;
				    var sel = window.getSelection();

				    if (sel.rangeCount > 0)
				    {
				        var range = window.getSelection().getRangeAt(0);
				        var caretRange = range.cloneRange();
				        caretRange.selectNodeContents(this.$editor[0]);
				        caretRange.setEnd(range.endContainer, range.endOffset);
				        offset = caretRange.toString().length;
				    }

					return offset;
				},
				setOffset: function(start, end)
				{
					if (typeof end == 'undefined') end = start;
					if (!this.focus.isFocused()) this.focus.setStart();

					var sel = this.selection.get();
					var node, offset = 0;
					var walker = document.createTreeWalker(this.$editor[0], NodeFilter.SHOW_TEXT, null, null);

					walker.firstChild();
					if (walker.currentNode.nodeValue != null) {
					    offset = walker.currentNode.nodeValue.length;
					    while (walker.nextSibling()) {
					        offset += walker.currentNode.nodeValue.length;
					        if (offset > start) {
					            this.range.setStart(walker.currentNode, walker.currentNode.nodeValue.length + start - offset);
					            start = Infinity;
					        }

					        if (offset >= end) {
					            this.range.setEnd(walker.currentNode, walker.currentNode.nodeValue.length + end - offset);
					            break;
					        }
					    }
					}
					this.range.collapse(false);
					this.selection.addRange();
					
				},
				// deprecated
				setToPoint: function(start, end)
				{
					this.caret.setOffset(start, end);
				},
				getCoords: function()
				{
					return this.caret.getOffset();
				}
			};
		},
		clean: function()
		{
			return {
				onSet: function(html)
				{
					html = this.clean.savePreCode(html);

					// convert script tag
					html = html.replace(/<script(.*?[^>]?)>([\w\W]*?)<\/script>/gi, '<pre class="redactor-script-tag" style="display: none;" $1>$2</pre>');

					// replace dollar sign to entity
					html = html.replace(/\$/g, '&#36;');

					// replace special characters in links
					html = html.replace(/<a href="(.*?[^>]?)®(.*?[^>]?)">/gi, '<a href="$1&reg$2">');

					if (this.opts.replaceDivs) html = this.clean.replaceDivs(html);
					if (this.opts.linebreaks)  html = this.clean.replaceParagraphsToBr(html);

					// save form tag
					html = this.clean.saveFormTags(html);

					// convert font tag to span
					var $div = $('<div>');
					$div.html(html);
					var fonts = $div.find('font[style]');
					if (fonts.length !== 0)
					{
						fonts.replaceWith(function()
						{
							var $el = $(this);
							var $span = $('<span>').attr('style', $el.attr('style'));
							return $span.append($el.contents());
						});

						html = $div.html();
					}

					$div.remove();

					// remove font tag
					html = html.replace(/<font(.*?[^<])>/gi, '');
					html = html.replace(/<\/font>/gi, '');

					// tidy html
					html = this.tidy.load(html);

					// paragraphize
					if (this.opts.paragraphize) html = this.paragraphize.load(html);

					// verified
					html = this.clean.setVerified(html);

					// convert inline tags
					html = this.clean.convertInline(html);

					html = html.replace(/&amp;/g, '&');

					return html;
				},
				onSync: function(html)
				{
					// remove spaces
					html = html.replace(/\u200B/g, '');
					html = html.replace(/&#x200b;/gi, '');

					if (this.opts.cleanSpaces)
					{
						html = html.replace(/&nbsp;/gi, ' ');
					}

					if (html.search(/^<p>(||\s||<br\s?\/?>||&nbsp;)<\/p>$/i) != -1)
					{
						return '';
					}

					// reconvert script tag
					html = html.replace(/<pre class="redactor-script-tag" style="display: none;"(.*?[^>]?)>([\w\W]*?)<\/pre>/gi, '<script$1>$2</script>');

					// restore form tag
					html = this.clean.restoreFormTags(html);

					var chars = {
						'\u2122': '&trade;',
						'\u00a9': '&copy;',
						'\u2026': '&hellip;',
						'\u2014': '&mdash;',
						'\u2010': '&dash;'
					};
					// replace special characters
					$.each(chars, function(i,s)
					{
						html = html.replace(new RegExp(i, 'g'), s);
					});

					// remove last br in FF
					if (this.utils.browser('mozilla'))
					{
						html = html.replace(/<br\s?\/?>$/gi, '');
					}

					// remove br in the of li
					html = html.replace(new RegExp('<br\\s?/?></li>', 'gi'), '</li>');
					html = html.replace(new RegExp('</li><br\\s?/?>', 'gi'), '</li>');

					// remove empty attributes
					html = html.replace(/<(.*?)rel="\s*?"(.*?[^>]?)>/gi, '<$1$2">');
					html = html.replace(/<(.*?)style="\s*?"(.*?[^>]?)>/gi, '<$1$2">');
					html = html.replace(/="">/gi, '>');
					html = html.replace(/""">/gi, '">');
					html = html.replace(/"">/gi, '">');

					// remove verified
					html = html.replace(/<div(.*?[^>]) data-tagblock="redactor"(.*?[^>])>/gi, '<div$1$2>');
					html = html.replace(/<(.*?) data-verified="redactor"(.*?[^>])>/gi, '<$1$2>');

					var $div = $("<div/>").html($.parseHTML(html, document, true));
					$div.find("span").removeAttr("rel");

					$div.find('pre .redactor-invisible-space').each(function()
					{
						$(this).contents().unwrap();
					});

					html = $div.html();

					// remove rel attribute from img
					html = html.replace(/<img(.*?[^>])rel="(.*?[^>])"(.*?[^>])>/gi, '<img$1$3>');
					html = html.replace(/<span class="redactor-invisible-space">(.*?)<\/span>/gi, '$1');

					html = html.replace(/ data-save-url="(.*?[^>])"/gi, '');

					// remove image resize
					html = html.replace(/<span(.*?)id="redactor-image-box"(.*?[^>])>([\w\W]*?)<img(.*?)><\/span>/gi, '$3<img$4>');
					html = html.replace(/<span(.*?)id="redactor-image-resizer"(.*?[^>])>(.*?)<\/span>/gi, '');
					html = html.replace(/<span(.*?)id="redactor-image-editter"(.*?[^>])>(.*?)<\/span>/gi, '');

					// remove font tag
					html = html.replace(/<font(.*?[^<])>/gi, '');
					html = html.replace(/<\/font>/gi, '');

					// tidy html
					html = this.tidy.load(html);

					// link nofollow
					if (this.opts.linkNofollow)
					{
						html = html.replace(/<a(.*?)rel="nofollow"(.*?[^>])>/gi, '<a$1$2>');
						html = html.replace(/<a(.*?[^>])>/gi, '<a$1 rel="nofollow">');
					}

					// reconvert inline
					html = html.replace(/\sdata-redactor-(tag|class|style)="(.*?[^>])"/gi, '');
					html = html.replace(new RegExp('<(.*?) data-verified="redactor"(.*?[^>])>', 'gi'), '<$1$2>');
					html = html.replace(new RegExp('<(.*?) data-verified="redactor">', 'gi'), '<$1>');

					html = html.replace(/&amp;/g, '&');

					return html;
				},
				onPaste: function(html, setMode)
				{
					html = $.trim(html);
					html = html.replace(/\$/g, '&#36;');

					// convert dirty spaces
					html = html.replace(/<span class="s[0-9]">/gi, '<span>');
					html = html.replace(/<span class="Apple-converted-space">&nbsp;<\/span>/gi, ' ');
					html = html.replace(/<span class="Apple-tab-span"[^>]*>\t<\/span>/gi, '\t');
					html = html.replace(/<span[^>]*>(\s|&nbsp;)<\/span>/gi, ' ');

					if (this.opts.pastePlainText)
					{
						return this.clean.getPlainText(html);
					}

					if (!this.utils.isSelectAll() && typeof setMode == 'undefined')
					{
						if (this.utils.isCurrentOrParent(['FIGCAPTION', 'A']))
						{
							return this.clean.getPlainText(html, false);
						}

						if (this.utils.isCurrentOrParent('PRE'))
						{
							html = html.replace(/”/g, '"');
							html = html.replace(/“/g, '"');
							html = html.replace(/‘/g, '\'');
							html = html.replace(/’/g, '\'');

							return this.clean.getPreCode(html);
						}

						if (this.utils.isCurrentOrParent(['BLOCKQUOTE', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6']))
						{
							html = this.clean.getOnlyImages(html);

							if (!this.utils.browser('msie'))
							{
								var block = this.selection.getBlock();
								if (block && block.tagName == 'P')
								{
									html = html.replace(/<img(.*?)>/gi, '<p><img$1></p>');
								}
							}

							return html;
						}

						if (this.utils.isCurrentOrParent(['TD']))
						{
							html = this.clean.onPasteTidy(html, 'td');

							if (this.opts.linebreaks) html = this.clean.replaceParagraphsToBr(html);

							html = this.clean.replaceDivsToBr(html);

							return html;
						}


						if (this.utils.isCurrentOrParent(['LI']))
						{
							return this.clean.onPasteTidy(html, 'li');
						}
					}


					html = this.clean.isSingleLine(html, setMode);

					if (!this.clean.singleLine)
					{
						if (this.opts.linebreaks)  html = this.clean.replaceParagraphsToBr(html);
						if (this.opts.replaceDivs) html = this.clean.replaceDivs(html);

						html = this.clean.saveFormTags(html);
					}


					html = this.clean.onPasteWord(html);
					html = this.clean.onPasteExtra(html);

					html = this.clean.onPasteTidy(html, 'all');


					// paragraphize
					if (!this.clean.singleLine && this.opts.paragraphize)
					{
						html = this.paragraphize.load(html);
					}

					html = this.clean.removeDirtyStyles(html);
					html = this.clean.onPasteRemoveSpans(html);
					html = this.clean.onPasteRemoveEmpty(html);


					html = this.clean.convertInline(html);

					return html;
				},
				onPasteWord: function(html)
				{
					// comments
					html = html.replace(/<!--[\s\S]*?-->/gi, '');

					// style
					html = html.replace(/<style[^>]*>[\s\S]*?<\/style>/gi, '');

					// op
					html = html.replace(/<o\:p[^>]*>[\s\S]*?<\/o\:p>/gi, '');

					if (html.match(/class="?Mso|style="[^"]*\bmso-|style='[^'']*\bmso-|w:WordDocument/i))
					{
						// comments
						html = html.replace(/<!--[\s\S]+?-->/gi, '');

						// scripts
						html = html.replace(/<(!|script[^>]*>.*?<\/script(?=[>\s])|\/?(\?xml(:\w+)?|img|meta|link|style|\w:\w+)(?=[\s\/>]))[^>]*>/gi, '');

						// Convert <s> into <strike>
						html = html.replace(/<(\/?)s>/gi, "<$1strike>");

						// Replace nbsp entites to char since it's easier to handle
						html = html.replace(/ /gi, ' ');

						// Convert <span style="mso-spacerun:yes">___</span> to string of alternating
						// breaking/non-breaking spaces of same length
						html = html.replace(/<span\s+style\s*=\s*"\s*mso-spacerun\s*:\s*yes\s*;?\s*"\s*>([\s\u00a0]*)<\/span>/gi, function(str, spaces) {
							return (spaces.length > 0) ? spaces.replace(/./, " ").slice(Math.floor(spaces.length/2)).split("").join("\u00a0") : '';
						});

						html = this.clean.onPasteIeFixLinks(html);

						// shapes
						html = html.replace(/<img(.*?)v:shapes=(.*?)>/gi, '');
						html = html.replace(/src="file\:\/\/(.*?)"/, 'src=""');

						// lists
						var $div = $("<div/>").html(html);

						var lastList = false;
						var lastLevel = 1;
						var listsIds = [];

						$div.find("p[style]").each(function()
						{
							var matches = $(this).attr('style').match(/mso\-list\:l([0-9]+)\slevel([0-9]+)/);

							if (matches)
							{
								var currentList = parseInt(matches[1]);
								var currentLevel = parseInt(matches[2]);
								var listType = $(this).html().match(/^[\w]+\./) ? "ol" : "ul";

								var $li = $("<li/>").html($(this).html());

								$li.html($li.html().replace(/^([\w\.]+)</, '<'));
								$li.find("span:first").remove();

								if (currentLevel == 1 && $.inArray(currentList, listsIds) == -1)
								{
									var $list = $("<" + listType + "/>").attr({"data-level": currentLevel,
																			   "data-list": currentList})
																	  .html($li);

									$(this).replaceWith($list);

									lastList = currentList;
									listsIds.push(currentList);
								}
								else
								{
									if (currentLevel > lastLevel)
									{
										var $prevList = $div.find('[data-level="' + lastLevel + '"][data-list="' + lastList + '"]');

										var $lastList = $prevList;

										for(var i = lastLevel; i < currentLevel; i++)
										{
											$list = $("<" + listType + "/>");

											$list.appendTo($lastList.find("li").last());

											$lastList = $list;
										}

										$lastList.attr({"data-level": currentLevel,
														"data-list": currentList})
												 .html($li);

									}
									else
									{
										var $prevList = $div.find('[data-level="' + currentLevel + '"][data-list="' + currentList + '"]').last();

										$prevList.append($li);
									}

									lastLevel = currentLevel;
									lastList = currentList;

									$(this).remove();
								}
							}
						});

						$div.find('[data-level][data-list]').removeAttr('data-level data-list');
						html = $div.html();

						// remove ms word's bullet
						html = html.replace(/·/g, '');
						html = html.replace(/<p class="Mso(.*?)"/gi, '<p');

						// classes
						html = html.replace(/ class=\"(mso[^\"]*)\"/gi,	"");
						html = html.replace(/ class=(mso\w+)/gi, "");

						// remove ms word tags
						html = html.replace(/<o:p(.*?)>([\w\W]*?)<\/o:p>/gi, '$2');

						// ms word break lines
						html = html.replace(/\n/g, ' ');

						// ms word lists break lines
						html = html.replace(/<p>\n?<li>/gi, '<li>');
					}

					return html;
				},
				onPasteExtra: function(html)
				{
					// remove google docs markers
					html = html.replace(/<b\sid="internal-source-marker(.*?)">([\w\W]*?)<\/b>/gi, "$2");
					html = html.replace(/<b(.*?)id="docs-internal-guid(.*?)">([\w\W]*?)<\/b>/gi, "$3");

					// google docs styles
			 		html = html.replace(/<span[^>]*(font-style: italic; font-weight: bold|font-weight: bold; font-style: italic)[^>]*>/gi, '<span style="font-weight: bold;"><span style="font-style: italic;">');
			 		html = html.replace(/<span[^>]*font-style: italic[^>]*>/gi, '<span style="font-style: italic;">');
					html = html.replace(/<span[^>]*font-weight: bold[^>]*>/gi, '<span style="font-weight: bold;">');
					html = html.replace(/<span[^>]*text-decoration: underline[^>]*>/gi, '<span style="text-decoration: underline;">');

					html = html.replace(/<img>/gi, '');
					html = html.replace(/\n{3,}/gi, '\n');
					html = html.replace(/<font(.*?)>([\w\W]*?)<\/font>/gi, '$2');

					// remove dirty p
					html = html.replace(/<p><p>/gi, '<p>');
					html = html.replace(/<\/p><\/p>/gi, '</p>');
					html = html.replace(/<li>(\s*|\t*|\n*)<p>/gi, '<li>');
					html = html.replace(/<\/p>(\s*|\t*|\n*)<\/li>/gi, '</li>');

					// remove space between paragraphs
					html = html.replace(/<\/p>\s<p/gi, '<\/p><p');

					// remove safari local images
					html = html.replace(/<img src="webkit-fake-url\:\/\/(.*?)"(.*?)>/gi, '');

					// bullets
					html = html.replace(/<p>•([\w\W]*?)<\/p>/gi, '<li>$1</li>');

					// FF fix
					if (this.utils.browser('mozilla'))
					{
						html = html.replace(/<br\s?\/?>$/gi, '');
					}

					return html;
				},
				onPasteTidy: function(html, type)
				{
					// remove all tags except these
					var tags = ['span', 'a', 'pre', 'blockquote', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'address', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del',
								'ol', 'ul', 'li', 'dl', 'dt', 'dd', 'p', 'br', 'video', 'audio', 'iframe', 'embed', 'param', 'object', 'img', 'table',
								'td', 'th', 'tr', 'tbody', 'tfoot', 'thead', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
					var tagsEmpty = false;
					var attrAllowed =  [
							['a', '*'],
							['img', ['src', 'alt']],
							['span', ['class', 'rel', 'data-verified']],
							['iframe', '*'],
							['video', '*'],
							['audio', '*'],
							['embed', '*'],
							['object', '*'],
							['param', '*'],
							['source', '*']
						];

					if (type == 'all')
					{
						tagsEmpty = ['p', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
						attrAllowed =  [
							['table', 'class'],
							['td', ['colspan', 'rowspan']],
							['a', '*'],
							['img', ['src', 'alt', 'data-redactor-inserted-image']],
							['span', ['class', 'rel', 'data-verified']],
							['iframe', '*'],
							['video', '*'],
							['audio', '*'],
							['embed', '*'],
							['object', '*'],
							['param', '*'],
							['source', '*']
						];
					}
					else if (type == 'td')
					{
						// remove all tags except these and remove all table tags: tr, td etc
						tags = ['ul', 'ol', 'li', 'span', 'a', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del',
								'ol', 'ul', 'li', 'dl', 'dt', 'dd', 'br', 'iframe', 'video', 'audio', 'embed', 'param', 'object', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

					}
					else if (type == 'li')
					{
						// only inline tags and ul, ol, li
						tags = ['ul', 'ol', 'li', 'span', 'a', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del', 'br',
								'iframe', 'video', 'audio', 'embed', 'param', 'object', 'img'];
					}

					var options = {
						deniedTags: (this.opts.deniedTags) ? this.opts.deniedTags : false,
						allowedTags: (this.opts.allowedTags) ? this.opts.allowedTags : tags,
						removeComments: true,
						removePhp: true,
						removeAttr: (this.opts.removeAttr) ? this.opts.removeAttr : false,
						allowedAttr: (this.opts.allowedAttr) ? this.opts.allowedAttr : attrAllowed,
						removeEmpty: tagsEmpty
					};

					return this.tidy.load(html, options);
				},
				onPasteRemoveEmpty: function(html)
				{
					html = html.replace(/<(p|h[1-6])>(|\s|\n|\t|<br\s?\/?>)<\/(p|h[1-6])>/gi, '');

					// remove br in the end
					if (!this.opts.linebreaks) html = html.replace(/<br>$/i, '');

					return html;
				},
				onPasteRemoveSpans: function(html)
				{
					html = html.replace(/<span>(.*?)<\/span>/gi, '$1');
					html = html.replace(/<span[^>]*>\s|&nbsp;<\/span>/gi, ' ');

					return html;
				},
				onPasteIeFixLinks: function(html)
				{
					if (!this.utils.browser('msie')) return html;

					var tmp = $.trim(html);
					if (tmp.search(/^<a(.*?)>(.*?)<\/a>$/i) === 0)
					{
						html = html.replace(/^<a(.*?)>(.*?)<\/a>$/i, "$2");
					}

					return html;
				},
				isSingleLine: function(html, setMode)
				{
					this.clean.singleLine = false;

					if (!this.utils.isSelectAll() && typeof setMode == 'undefined')
					{
						var blocks = this.opts.blockLevelElements.join('|').replace('P|', '').replace('DIV|', '');

						var matchBlocks = html.match(new RegExp('</(' + blocks + ')>', 'gi'));
						var matchContainers = html.match(/<\/(p|div)>/gi);

						if (!matchBlocks && (matchContainers === null || (matchContainers && matchContainers.length <= 1)))
						{
							var matchBR = html.match(/<br\s?\/?>/gi);
							//var matchIMG = html.match(/<img(.*?[^>])>/gi);
							if (!matchBR)
							{
								this.clean.singleLine = true;
								html = html.replace(/<\/?(p|div)(.*?)>/gi, '');
							}
						}
					}

					return html;
				},
				stripTags: function(input, allowed)
				{
				    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
				    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;

				    return input.replace(tags, function ($0, $1) {
				        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
				    });
				},
				savePreCode: function(html)
				{
					html = this.clean.savePreFormatting(html);
					html = this.clean.saveCodeFormatting(html);

					html = this.clean.restoreSelectionMarker(html);

					return html;
				},
				savePreFormatting: function(html)
				{
					var pre = html.match(/<pre(.*?)>([\w\W]*?)<\/pre>/gi);

					if (pre !== null)
					{
						$.each(pre, $.proxy(function(i,s)
						{
							var arr = s.match(/<pre(.*?)>([\w\W]*?)<\/pre>/i);

							arr[2] = arr[2].replace(/<br\s?\/?>/g, '\n');
							arr[2] = arr[2].replace(/&nbsp;/g, ' ');

							if (this.opts.preSpaces)
							{
								arr[2] = arr[2].replace(/\t/g, Array(this.opts.preSpaces + 1).join(' '));
							}

							arr[2] = this.clean.encodeEntities(arr[2]);

							// $ fix
							arr[2] = arr[2].replace(/\$/g, '&#36;');

							html = html.replace(s, '<pre' + arr[1] + '>' + arr[2] + '</pre>');

						}, this));
					}

					return html;
				},
				saveCodeFormatting: function(html)
				{
					var code = html.match(/<code(.*?)>([\w\W]*?)<\/code>/gi);

					if (code !== null)
					{
						$.each(code, $.proxy(function(i,s)
						{
							var arr = s.match(/<code(.*?)>([\w\W]*?)<\/code>/i);

							arr[2] = arr[2].replace(/&nbsp;/g, ' ');
							arr[2] = this.clean.encodeEntities(arr[2]);
							arr[2] = arr[2].replace(/\$/g, '&#36;');

							html = html.replace(s, '<code' + arr[1] + '>' + arr[2] + '</code>');
						}, this));
					}

					return html;
				},
				restoreSelectionMarker: function(html)
				{
					html = html.replace(/&lt;span id=&quot;selection-marker-([0-9])&quot; class=&quot;redactor-selection-marker&quot; data-verified=&quot;redactor&quot;&gt;​&lt;\/span&gt;/g, '<span id="selection-marker-$1" class="redactor-selection-marker" data-verified="redactor">​</span>');

					return html;
				},
				getTextFromHtml: function(html)
				{
					html = html.replace(/<br\s?\/?>|<\/H[1-6]>|<\/p>|<\/div>|<\/li>|<\/td>/gi, '\n');

					var tmp = document.createElement('div');
					tmp.innerHTML = html;
					html = tmp.textContent || tmp.innerText;

					return $.trim(html);
				},
				getPlainText: function(html, paragraphize)
				{
					html = this.clean.getTextFromHtml(html);
					html = html.replace(/\n/g, '<br />');

					if (this.opts.paragraphize && typeof paragraphize == 'undefined' && !this.utils.browser('mozilla'))
					{
						html = this.paragraphize.load(html);
					}

					return html;
				},
				getPreCode: function(html)
				{
					html = html.replace(/<img(.*?) style="(.*?)"(.*?[^>])>/gi, '<img$1$3>');
					html = html.replace(/<img(.*?)>/gi, '&lt;img$1&gt;');
					html = this.clean.getTextFromHtml(html);

					if (this.opts.preSpaces)
					{
						html = html.replace(/\t/g, Array(this.opts.preSpaces + 1).join(' '));
					}

					html = this.clean.encodeEntities(html);

					return html;
				},
				getOnlyImages: function(html)
				{
					html = html.replace(/<img(.*?)>/gi, '[img$1]');

					// remove all tags
					html = html.replace(/<([Ss]*?)>/gi, '');

					html = html.replace(/\[img(.*?)\]/gi, '<img$1>');

					return html;
				},
				getOnlyLinksAndImages: function(html)
				{
					html = html.replace(/<a(.*?)href="(.*?)"(.*?)>([\w\W]*?)<\/a>/gi, '[a href="$2"]$4[/a]');
					html = html.replace(/<img(.*?)>/gi, '[img$1]');

					// remove all tags
					html = html.replace(/<(.*?)>/gi, '');

					html = html.replace(/\[a href="(.*?)"\]([\w\W]*?)\[\/a\]/gi, '<a href="$1">$2</a>');
					html = html.replace(/\[img(.*?)\]/gi, '<img$1>');

					return html;
				},
				encodeEntities: function(str)
				{
					str = String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
					return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
				},
				removeDirtyStyles: function(html)
				{
					if (this.utils.browser('msie')) return html;

					var div = document.createElement('div');
					div.innerHTML = html;

					this.clean.clearUnverifiedRemove($(div));

					html = div.innerHTML;
					$(div).remove();

					return html;
				},
				clearUnverified: function()
				{
					if (this.utils.browser('msie')) return;

					this.clean.clearUnverifiedRemove(this.$editor);

					var headers = this.$editor.find('h1, h2, h3, h4, h5, h6');
					headers.find('span').removeAttr('style');
					headers.find(this.opts.verifiedTags.join(', ')).removeAttr('style');

					this.code.sync();
				},
				clearUnverifiedRemove: function($editor)
				{
					$editor.find(this.opts.verifiedTags.join(', ')).removeAttr('style');
					$editor.find('span').not('[data-verified="redactor"]').removeAttr('style');

					$editor.find('span[data-verified="redactor"], img[data-verified="redactor"]').each(function(i, s)
					{
						var $s = $(s);
						$s.attr('style', $s.attr('rel'));
					});

				},
				cleanEmptyParagraph: function()
				{

				},
				setVerified: function(html)
				{
					if (this.utils.browser('msie')) return html;

					html = html.replace(new RegExp('<img(.*?[^>])>', 'gi'), '<img$1 data-verified="redactor">');
					html = html.replace(new RegExp('<span(.*?[^>])>', 'gi'), '<span$1 data-verified="redactor">');

					var matches = html.match(new RegExp('<(span|img)(.*?)style="(.*?)"(.*?[^>])>', 'gi'));

					if (matches)
					{
						var len = matches.length;
						for (var i = 0; i < len; i++)
						{
							try {

								var newTag = matches[i].replace(/style="(.*?)"/i, 'style="$1" rel="$1"');
								html = html.replace(matches[i], newTag);

							}
							catch (e) {}
						}
					}

					return html;
				},
				convertInline: function(html)
				{
					var $div = $('<div />').html(html);

					var tags = this.opts.inlineTags;
					tags.push('span');

					$div.find(tags.join(',')).each(function()
					{
						var $el = $(this);
						var tag = this.tagName.toLowerCase();
						$el.attr('data-redactor-tag', tag);

						if (tag == 'span')
						{
							if ($el.attr('style')) $el.attr('data-redactor-style', $el.attr('style'));
							else if ($el.attr('class')) $el.attr('data-redactor-class', $el.attr('class'));
						}

					});

					html = $div.html();
					$div.remove();

					return html;
				},
				normalizeLists: function()
				{
					this.$editor.find('li').each(function(i,s)
					{
						var $next = $(s).next();
						if ($next.length !== 0 && ($next[0].tagName == 'UL' || $next[0].tagName == 'OL'))
						{
							$(s).append($next);
						}

					});
				},
				removeSpaces: function(html)
				{
					html = html.replace(/\n/g, '');
					html = html.replace(/[\t]*/g, '');
					html = html.replace(/\n\s*\n/g, "\n");
					html = html.replace(/^[\s\n]*/g, ' ');
					html = html.replace(/[\s\n]*$/g, ' ');
					html = html.replace( />\s{2,}</g, '> <'); // between inline tags can be only one space
					html = html.replace(/\n\n/g, "\n");
					html = html.replace(/\u200B/g, '');

					return html;
				},
				replaceDivs: function(html)
				{
					if (this.opts.linebreaks)
					{
						html = html.replace(/<div><br\s?\/?><\/div>/gi, '<br />');
						html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, '$2<br />');
					}
					else
					{
						html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, '<p$1>$2</p>');
					}

					html = html.replace(/<div(.*?[^>])>/gi, '');
					html = html.replace(/<\/div>/gi, '');

					return html;
				},
				replaceDivsToBr: function(html)
				{
					html = html.replace(/<div\s(.*?)>/gi, '<p>');
					html = html.replace(/<div><br\s?\/?><\/div>/gi, '<br /><br />');
					html = html.replace(/<div>([\w\W]*?)<\/div>/gi, '$1<br /><br />');

					return html;
				},
				replaceParagraphsToBr: function(html)
				{
					html = html.replace(/<p\s(.*?)>/gi, '<p>');
					html = html.replace(/<p><br\s?\/?><\/p>/gi, '<br />');
					html = html.replace(/<p>([\w\W]*?)<\/p>/gi, '$1<br /><br />');
					html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, '</blockquote>');

					return html;
				},
				saveFormTags: function(html)
				{
					return html.replace(/<form(.*?)>([\w\W]*?)<\/form>/gi, '<section$1 rel="redactor-form-tag">$2</section>');
				},
				restoreFormTags: function(html)
				{
					return html.replace(/<section(.*?) rel="redactor-form-tag"(.*?)>([\w\W]*?)<\/section>/gi, '<form$1$2>$3</form>');
				}
			};
		},
		code: function()
		{
			return {
				set: function(html)
				{
					html = $.trim(html.toString());

					// clean
					html = this.clean.onSet(html);

					this.$editor.html(html);
					this.code.sync();

					if (html !== '') this.placeholder.remove();

					setTimeout($.proxy(this.buffer.add, this), 15);
					if (this.start === false) this.observe.load();

				},
				get: function()
				{
					var code = this.$textarea.val();

					if (this.opts.replaceDivs) code = this.clean.replaceDivs(code);
					if (this.opts.linebreaks) code = this.clean.replaceParagraphsToBr(code);

					// indent code
					code = this.tabifier.get(code);

					return code;
				},
				sync: function()
				{
					setTimeout($.proxy(this.code.startSync, this), 10);
				},
				startSync: function()
				{
					var html = this.$editor.html();

					// is there a need to synchronize
					if (this.code.syncCode && this.code.syncCode == html)
					{
						// do not sync
						return;
					}

					// save code
					this.code.syncCode = html;

					// before clean callback
					html = this.core.setCallback('syncBefore', html);

					// clean
					html = this.clean.onSync(html);

					// set code
					this.$textarea.val(html);

					// after sync callback
					this.core.setCallback('sync', html);

					if (this.start === false)
					{
						this.core.setCallback('change', html);
					}

					this.start = false;

					if (this.autosave.html == false)
					{
						this.autosave.html = this.code.get();
					}

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							el.CodeMirror.setValue(html);
						});
					}

					//autosave
					this.autosave.onChange();
					this.autosave.enable();
				},
				toggle: function()
				{
					if (this.opts.visual)
					{
						this.code.showCode();
					}
					else
					{
						this.code.showVisual();
					}
				},
				showCode: function()
				{
					this.selection.save();

					this.code.offset = this.caret.getOffset();
					var scroll = $(window).scrollTop();

					var	width = this.$editor.innerWidth(),
						height = this.$editor.innerHeight();

					this.$editor.hide();

					var html = this.$textarea.val();

					this.modified = this.clean.removeSpaces(html);

					// indent code
					html = this.tabifier.get(html);

					// caret position sync
					var start = 0, end = 0;
					var $editorDiv = $("<div/>").append($.parseHTML(this.clean.onSync(this.$editor.html()), document, true));
					var $selectionMarkers = $editorDiv.find("span.redactor-selection-marker");

					if ($selectionMarkers.length > 0)
					{
						var editorHtml = this.tabifier.get($editorDiv.html()).replace(/&amp;/g, '&');

						if ($selectionMarkers.length == 1)
						{
							start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML"));
							end   = start;
						}
						else if ($selectionMarkers.length == 2)
						{
							start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML"));
							end	 = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-2").prop("outerHTML")) - $editorDiv.find("#selection-marker-1").prop("outerHTML").toString().length;
						}
					}

					this.selection.removeMarkers();
					this.$textarea.val(html);

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							$(el).show();
							el.CodeMirror.setValue(html);
							el.CodeMirror.setSize('100%', height);
							el.CodeMirror.refresh();

							if (start == end)
							{
								el.CodeMirror.setCursor(el.CodeMirror.posFromIndex(start).line, el.CodeMirror.posFromIndex(end).ch);
							}
							else
							{
								el.CodeMirror.setSelection({line: el.CodeMirror.posFromIndex(start).line,
															ch: el.CodeMirror.posFromIndex(start).ch},
														  {line: el.CodeMirror.posFromIndex(end).line,
														   ch:  el.CodeMirror.posFromIndex(end).ch});
							}

							el.CodeMirror.focus();
						});
					}
					else
					{
						this.$textarea.height(height).show().focus();
						this.$textarea.on('keydown.redactor-textarea-indenting', this.code.textareaIndenting);

						$(window).scrollTop(scroll);

						if (this.$textarea[0].setSelectionRange)
						{
							this.$textarea[0].setSelectionRange(start, end);
						}

						this.$textarea[0].scrollTop = 0;
					}

					this.opts.visual = false;

					this.button.setInactiveInCode();
					this.button.setActive('html');
					this.core.setCallback('source', html);
				},
				showVisual: function()
				{
					var html;

					if (this.opts.visual) return;

					var start = 0, end = 0;

					if (this.opts.codemirror)
					{
						var selection;

						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							selection = el.CodeMirror.listSelections();

							start = el.CodeMirror.indexFromPos(selection[0].anchor);
							end = el.CodeMirror.indexFromPos(selection[0].head);

							html = el.CodeMirror.getValue();
						});
					}
					else
					{
						start = this.$textarea.get(0).selectionStart;
						end = this.$textarea.get(0).selectionEnd;

						html = this.$textarea.hide().val();
					}

					// if selection starts from end
					if (start > end && end > 0)
					{
						var tempStart = end;
						var tempEnd = start;

						start = tempStart;
						end = tempEnd;
					}

					start = this.code.enlargeOffset(html, start);
					end = this.code.enlargeOffset(html, end);

					html = html.substr(0, start) + this.selection.getMarkerAsHtml(1) + html.substr(start);

					if (end > start)
					{
						var markerLength = this.selection.getMarkerAsHtml(1).toString().length;

						html = html.substr(0, end + markerLength) + this.selection.getMarkerAsHtml(2) + html.substr(end + markerLength);
					}

					if (this.modified !== this.clean.removeSpaces(html))
					{
						this.code.set(html);
					}

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').hide();
					}

					this.$editor.show();

					if (!this.utils.isEmpty(html))
					{
						this.placeholder.remove();
					}

					this.selection.restore();

					this.$textarea.off('keydown.redactor-textarea-indenting');

					this.button.setActiveInVisual();
					this.button.setInactive('html');
					this.observe.load();
					this.opts.visual = true;
					this.core.setCallback('visual', html);
				},
				textareaIndenting: function(e)
				{
					if (e.keyCode !== 9) return true;

					var $el = this.$textarea;
					var start = $el.get(0).selectionStart;
					$el.val($el.val().substring(0, start) + "\t" + $el.val().substring($el.get(0).selectionEnd));
					$el.get(0).selectionStart = $el.get(0).selectionEnd = start + 1;

					return false;
				},
				enlargeOffset: function(html, offset)
				{
					var htmlLength = html.length;
					var c = 0;

					if (html[offset] == '>')
					{
						c++;
					}
					else
					{
						for(var i = offset; i <= htmlLength; i++)
						{
							c++;

							if (html[i] == '>')
							{
								break;
							}
							else if (html[i] == '<' || i == htmlLength)
							{
								c = 0;
								break;
							}
						}
					}

					return offset + c;
				}
			};
		},
		core: function()
		{
			return {
				getObject: function()
				{
					return $.extend({}, this);
				},
				getEditor: function()
				{
					return this.$editor;
				},
				getBox: function()
				{
					return this.$box;
				},
				getElement: function()
				{
					return this.$element;
				},
				getTextarea: function()
				{
					return this.$textarea;
				},
				getToolbar: function()
				{
					return (this.$toolbar) ? this.$toolbar : false;
				},
				addEvent: function(name)
				{
					this.core.event = name;
				},
				getEvent: function()
				{
					return this.core.event;
				},
				setCallback: function(type, e, data)
				{
					var eventName = type + 'Callback';
					var eventNamespace = 'redactor';
					var callback = this.opts[eventName];

					if (this.$textarea)
					{
						var returnValue = false;
						var events = $._data(this.$textarea[0], 'events');

						if (typeof events != 'undefined' && typeof events[eventName] != 'undefined')
						{
							$.each(events[eventName], $.proxy(function(key, value)
							{
								if (value['namespace'] == eventNamespace)
								{
									var data = (typeof data == 'undefined') ? [e] : [e, data];

									returnValue = (typeof data == 'undefined') ? value.handler.call(this, e) : value.handler.call(this, e, data);
								}
							}, this));
						}

						if (returnValue) return returnValue;
					}

					if ($.isFunction(callback))
					{
						return (typeof data == 'undefined') ? callback.call(this, e) : callback.call(this, e, data);
					}
					else
					{
						return (typeof data == 'undefined') ? e : data;
					}
				},
				destroy: function()
				{
					this.opts.destroyed = true;

					this.core.setCallback('destroy');

					// off events and remove data
					this.$element.off('.redactor').removeData('redactor');
					this.$editor.off('.redactor');

					$(document).off('mousedown.redactor.' + this.uuid);
					$(document).off('click.redactor-image-delete.' + this.uuid);
					$(document).off('click.redactor-image-resize-hide.' + this.uuid);
					$(document).off('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid);
					$("body").off('scroll.redactor.' + this.uuid);
					$(this.opts.toolbarFixedTarget).off('scroll.redactor.' + this.uuid);

					// common
					this.$editor.removeClass('redactor-editor redactor-linebreaks redactor-placeholder');
					this.$editor.removeAttr('contenteditable');

					var html = this.code.get();

					if (this.opts.toolbar)
					{
						// dropdowns off
						this.$toolbar.find('a').each(function()
						{
							var $el = $(this);
							if ($el.data('dropdown'))
							{
								$el.data('dropdown').remove();
								$el.data('dropdown', {});
							}
						});
					}

					if (this.build.isTextarea())
					{
						this.$box.after(this.$element);
						this.$box.remove();
						this.$element.val(html).show();
					}
					else
					{
						this.$box.after(this.$editor);
						this.$box.remove();
						this.$element.html(html).show();
					}

					// paste box
					if (this.$pasteBox) this.$pasteBox.remove();

					// modal
					if (this.$modalBox) this.$modalBox.remove();
					if (this.$modalOverlay) this.$modalOverlay.remove();

					// buttons tooltip
					$('.redactor-toolbar-tooltip-' + this.uuid).remove();

					// autosave
					clearInterval(this.autosaveInterval);
				}
			};
		},
		dropdown: function()
		{
			return {
				build: function(name, $dropdown, dropdownObject)
				{
					if (name == 'formatting' && this.opts.formattingAdd)
					{
						$.each(this.opts.formattingAdd, $.proxy(function(i,s)
						{
							var name = s.tag,
								func;

							if (typeof s['class'] != 'undefined')
							{
								name = name + '-' + s['class'];
							}
							else
							    name = name + '-' + i;

							s.type = (this.utils.isBlockTag(s.tag)) ? 'block' : 'inline';

							if (typeof s.func !== "undefined")
							{
								func = s.func;
							}
							else
							{
								func = (s.type == 'inline') ? 'inline.formatting' : 'block.formatting';
							}

							if (this.opts.linebreaks && s.type == 'block' && s.tag == 'p') return;

							this.formatting[name] = {
								tag: s.tag,
								style: s.style,
								'class': s['class'],
								attr: s.attr,
								data: s.data,
								clear: s.clear
							};

							dropdownObject[name] = {
								func: func,
								title: s.title,
								style: s.style
							};

						}, this));
					}

					$.each(dropdownObject, $.proxy(function(btnName, btnObject)
					{
						var $item = '';
					    if (btnObject.func == 'inline.formatting')
					        $item = $('<a href="#" style="' + btnObject.style + '" role="button">' + btnObject.title + '</a>');
					    else
					        $item = $('<a href="#" class="redactor-dropdown-' + btnName + '" role="button">' + btnObject.title + '</a>');

						if (name == 'formatting') $item.addClass('redactor-formatting-' + btnName);

						$item.on('click', $.proxy(function(e)
						{
							e.preventDefault();

							var type = 'func';
							var callback = btnObject.func;
							if (btnObject.command)
							{
								type = 'command';
								callback = btnObject.command;
							}
							else if (btnObject.dropdown)
							{
								type = 'dropdown';
								callback = btnObject.dropdown;
							}

							if ($(e.target).hasClass('redactor-dropdown-link-inactive')) return;

							this.button.onClick(e, btnName, type, callback);
							this.dropdown.hideAll();

						}, this));

						this.observe.addDropdown($item, btnName, btnObject);

						$dropdown.append($item);

					}, this));
				},
				show: function(e, key)
				{
					if (!this.opts.visual)
					{
						e.preventDefault();
						return false;
					}

					var $button = this.button.get(key);

					// Always re-append it to the end of <body> so it always has the highest sub-z-index.
					var $dropdown = $button.data('dropdown').appendTo(document.body);

					if (this.opts.highContrast)
					{
						$dropdown.addClass("redactor-dropdown-contrast");
					}

					if ($button.hasClass('dropact'))
					{
						this.dropdown.hideAll();
					}
					else
					{
						this.dropdown.hideAll();
						this.observe.dropdowns();

						this.core.setCallback('dropdownShow', { dropdown: $dropdown, key: key, button: $button });

						this.button.setActive(key);

						$button.addClass('dropact');

						var keyPosition = $button.offset();

						// fix right placement
						var dropdownWidth = $dropdown.width();
						if ((keyPosition.left + dropdownWidth) > $(document).width())
						{
							keyPosition.left = Math.max(0, keyPosition.left - dropdownWidth);
						}

						var left = keyPosition.left + 'px';
						if (this.$toolbar.hasClass('toolbar-fixed-box'))
						{
							var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset;
							var position = 'fixed';
							if (this.opts.toolbarFixedTarget !== document)
							{
								top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top) + this.opts.toolbarFixedTopOffset;
								position = 'absolute';
							}

							$dropdown.css({ position: position, left: left, top: top + 'px' }).show();
						}
						else
						{
							var top = ($button.innerHeight() + keyPosition.top) + 'px';

							$dropdown.css({ position: 'absolute', left: left, top: top }).show();
						}

						this.core.setCallback('dropdownShown', { dropdown: $dropdown, key: key, button: $button });

						this.$dropdown = $dropdown;
					}


					$(document).one('click.redactor-dropdown', $.proxy(this.dropdown.hide, this));
					this.$editor.one('click.redactor-dropdown', $.proxy(this.dropdown.hide, this));
					$(document).one('keyup.redactor-dropdown', $.proxy(this.dropdown.closeHandler, this));

					// disable scroll whan dropdown scroll
					$dropdown.on('mouseover.redactor-dropdown', $.proxy(this.utils.disableBodyScroll, this)).on('mouseout.redactor-dropdown', $.proxy(this.utils.enableBodyScroll, this));


					e.stopPropagation();
				},
				closeHandler: function(e)
				{
					if (e.which != this.keyCode.ESC) return;

					this.dropdown.hideAll();
					this.$editor.focus();
				},
				hideAll: function()
				{
					this.$toolbar.find('a.dropact').removeClass('redactor-act').removeClass('dropact');

					this.utils.enableBodyScroll();

					$('.redactor-dropdown-' + this.uuid).hide();
					$('.redactor-dropdown-link-selected').removeClass('redactor-dropdown-link-selected');


					if (this.$dropdown)
					{
						this.$dropdown.off('.redactor-dropdown');
						this.core.setCallback('dropdownHide', this.$dropdown);

						this.$dropdown = false;
					}
				},
				hide: function (e)
				{
					var $dropdown = $(e.target);

					if (!$dropdown.hasClass('dropact') && !$dropdown.hasClass('redactor-dropdown-link-inactive'))
					{
						$dropdown.removeClass('dropact');
						$dropdown.off('mouseover mouseout');

						this.dropdown.hideAll();
					}
				}
			};
		},
		file: function()
		{
			return {
				show: function()
				{
					this.modal.load('file', this.lang.get('file'), 700);
					this.upload.init('#redactor-modal-file-upload', this.opts.fileUpload, this.file.insert);

					this.selection.save();

					this.selection.get();
					var text = this.sel.toString();

					$('#redactor-filename').val(text);

					this.modal.show();
				},
				insert: function(json, direct, e)
				{
					// error callback
					if (typeof json.error != 'undefined')
					{
						this.modal.close();
						this.selection.restore();
						this.core.setCallback('fileUploadError', json);
						return;
					}

					var link;
					if (typeof json == 'string')
					{
						link = json;
					}
					else
					{
						var text = $('#redactor-filename').val();
						if (typeof text == 'undefined' || text === '') text = json.filename;

						link = '<a href="' + json.filelink + '" id="filelink-marker">' + text + '</a>';
					}

					if (direct)
					{
						this.selection.removeMarkers();
						var marker = this.selection.getMarker();
						this.insert.nodeToCaretPositionFromPoint(e, marker);
					}
					else
					{
						this.modal.close();
					}

					this.selection.restore();
					this.buffer.set();

					this.insert.htmlWithoutClean(link);

					if (typeof json == 'string') return;

					var linkmarker = $(this.$editor.find('a#filelink-marker'));
					if (linkmarker.length !== 0)
					{
						linkmarker.removeAttr('id').removeAttr('style');
					}
					else linkmarker = false;

					this.core.setCallback('fileUpload', linkmarker, json);

				}
			};
		},
		focus: function()
		{
			return {
				setStart: function()
				{
					this.$editor.focus();

					var first = this.$editor.children().first();

					if (first.length === 0) return;
					if (first[0].length === 0 || first[0].tagName == 'BR' || first[0].nodeType == 3)
					{
						return;
					}

					if (first[0].tagName == 'UL' || first[0].tagName == 'OL')
					{
						var child = first.find('li').first();
						if (!this.utils.isBlock(child) && child.text() === '')
						{
							// empty inline tag in li
							this.caret.setStart(child);
							return;
						}
					}

					if (this.opts.linebreaks && !this.utils.isBlockTag(first[0].tagName))
					{
						this.selection.get();
						this.range.setStart(this.$editor[0], 0);
						this.range.setEnd(this.$editor[0], 0);
						this.selection.addRange();

						return;
					}

					// if node is tag
					this.caret.setStart(first);
				},
				setEnd: function()
				{
					var last = this.$editor.children().last();
					this.$editor.focus();

					if (last.size() === 0) return;
					if (this.utils.isEmpty(this.$editor.html()))
					{

						this.selection.get();
						this.range.collapse(true);
						this.range.setStartAfter(last[0]);
						this.range.setEnd(last[0], 0);
						this.selection.addRange();
					}
					else
					{
						this.selection.get();
						this.range.selectNodeContents(last[0]);
						this.range.collapse(false);
						this.selection.addRange();

					}
				},
				isFocused: function()
				{
					var focusNode = document.getSelection().focusNode;
					if (focusNode === null) return false;

					if (this.opts.linebreaks && $(focusNode.parentNode).hasClass('redactor-linebreaks')) return true;
					else if (!this.utils.isRedactorParent(focusNode.parentNode)) return false;

					return this.$editor.is(':focus');
				}
			};
		},
		image: function()
		{
			return {
				show: function()
				{
					this.modal.load('image', this.lang.get('image'), 700);
					this.upload.init('#redactor-modal-image-droparea', this.opts.imageUpload, this.image.insert);

					this.selection.save();
					this.modal.show();

				},
				showEdit: function($image)
				{

				    $("#contentpickfiles").click();
				    //this.image.hideResize();

					//var $link = $image.closest('a', this.$editor[0]);

					//this.modal.load('imageEdit', this.lang.get('edit'), 705);

					//this.modal.createCancelButton();
					//this.image.buttonDelete = this.modal.createDeleteButton(this.lang.get('_delete'));
					//this.image.buttonSave = this.modal.createActionButton(this.lang.get('save'));

					//this.image.buttonDelete.on('click', $.proxy(function()
					//{
					//	this.image.remove($image);

					//}, this));

					//this.image.buttonSave.on('click', $.proxy(function()
					//{
					//	this.image.update($image);

					//}, this));

					//// hide link's tooltip
					//$('.redactor-link-tooltip').remove();

					//$('#redactor-image-title').val($image.attr('alt'));

					//if (!this.opts.imageLink) $('.redactor-image-link-option').hide();
					//else
					//{
					//	var $redactorImageLink = $('#redactor-image-link');

					//	$redactorImageLink.attr('href', $image.attr('src'));
					//	if ($link.length !== 0)
					//	{
					//		$redactorImageLink.val($link.attr('href'));
					//		if ($link.attr('target') == '_blank') $('#redactor-image-link-blank').prop('checked', true);
					//	}
					//}

					//if (!this.opts.imagePosition) $('.redactor-image-position-option').hide();
					//else
					//{
					//	var floatValue = ($image.css('display') == 'block' && $image.css('float') == 'none') ? 'center' : $image.css('float');
					//	$('#redactor-image-align').val(floatValue);
					//}

					//this.modal.show();
					//$('#redactor-image-title').focus();

				},
				setFloating: function($image)
				{
					var floating = $('#redactor-image-align').val();

					var imageFloat = '';
					var imageDisplay = '';
					var imageMargin = '';

					switch (floating)
					{
						case 'left':
							imageFloat = 'left';
							imageMargin = '0 ' + this.opts.imageFloatMargin + ' ' + this.opts.imageFloatMargin + ' 0';
						break;
						case 'right':
							imageFloat = 'right';
							imageMargin = '0 0 ' + this.opts.imageFloatMargin + ' ' + this.opts.imageFloatMargin;
						break;
						case 'center':
							imageDisplay = 'block';
							imageMargin = 'auto';
						break;
					}

					$image.css({ 'float': imageFloat, display: imageDisplay, margin: imageMargin });
					$image.attr('rel', $image.attr('style'));
				},
				update: function($image)
				{
					this.image.hideResize();
					this.buffer.set();

					var $link = $image.closest('a', this.$editor[0]);

					$image.attr('alt', $('#redactor-image-title').val());

					this.image.setFloating($image);

					// as link
					var link = $.trim($('#redactor-image-link').val());
					if (link !== '')
					{
						// test url (add protocol)
						var pattern = '((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}';
						var re = new RegExp('^(http|ftp|https)://' + pattern, 'i');
						var re2 = new RegExp('^' + pattern, 'i');

						if (link.search(re) == -1 && link.search(re2) === 0 && this.opts.linkProtocol)
						{
							link = this.opts.linkProtocol + '://' + link;
						}

						var target = ($('#redactor-image-link-blank').prop('checked')) ? true : false;

						if ($link.length === 0)
						{
							var a = $('<a href="' + link + '">' + this.utils.getOuterHtml($image) + '</a>');
							if (target) a.attr('target', '_blank');

							$image.replaceWith(a);
						}
						else
						{
							$link.attr('href', link);
							if (target)
							{
								$link.attr('target', '_blank');
							}
							else
							{
								$link.removeAttr('target');
							}
						}
					}
					else if ($link.length !== 0)
					{
						$link.replaceWith(this.utils.getOuterHtml($image));

					}

					this.modal.close();
					this.observe.images();
					this.code.sync();


				},
				setEditable: function($image)
				{
					if (this.opts.imageEditable)
					{
						$image.on('dragstart', $.proxy(this.image.onDrag, this));
					}

					var handler = $.proxy(function(e)
					{

						this.observe.image = $image;

						this.image.resizer = this.image.loadEditableControls($image);

						$(document).on('mousedown.redactor-image-resize-hide.' + this.uuid, $.proxy(this.image.hideResize, this));

						// resize
						if (!this.opts.imageResizable) return;

						this.image.resizer.on('mousedown.redactor touchstart.redactor', $.proxy(function(e)
						{
							this.image.setResizable(e, $image);
						}, this));

					}, this);


					$image.off('mousedown.redactor').on('mousedown.redactor', $.proxy(this.image.hideResize, this));
					$image.off('click.redactor touchstart.redactor').on('click.redactor touchstart.redactor', handler);
				},
				setResizable: function(e, $image)
				{
					e.preventDefault();

				    this.image.resizeHandle = {
				        x : e.pageX,
				        y : e.pageY,
				        el : $image,
				        ratio: $image.width() / $image.height(),
				        h: $image.height()
				    };

				    e = e.originalEvent || e;

				    if (e.targetTouches)
				    {
				         this.image.resizeHandle.x = e.targetTouches[0].pageX;
				         this.image.resizeHandle.y = e.targetTouches[0].pageY;
				    }

					this.image.startResize();


				},
				startResize: function()
				{
					$(document).on('mousemove.redactor-image-resize touchmove.redactor-image-resize', $.proxy(this.image.moveResize, this));
					$(document).on('mouseup.redactor-image-resize touchend.redactor-image-resize', $.proxy(this.image.stopResize, this));
				},
				moveResize: function(e)
				{
					e.preventDefault();

					e = e.originalEvent || e;

					var height = this.image.resizeHandle.h;

		            if (e.targetTouches) height += (e.targetTouches[0].pageY -  this.image.resizeHandle.y);
		            else height += (e.pageY -  this.image.resizeHandle.y);

					var width = Math.round(height * this.image.resizeHandle.ratio);

					if (height < 50 || width < 100) return;

					var height = Math.round(this.image.resizeHandle.el.width() / this.image.resizeHandle.ratio);

					this.image.resizeHandle.el.attr({width: width, height: height});
		            this.image.resizeHandle.el.width(width);
		            this.image.resizeHandle.el.height(height);

		            this.code.sync();
				},
				stopResize: function()
				{
					this.handle = false;
					$(document).off('.redactor-image-resize');

					this.image.hideResize();
				},
				onDrag: function(e)
				{
					if (this.$editor.find('#redactor-image-box').length !== 0)
					{
						e.preventDefault();
						return false;
					}

					this.$editor.on('drop.redactor-image-inside-drop', $.proxy(function()
					{
						setTimeout($.proxy(this.image.onDrop, this), 1);

					}, this));
				},
				onDrop: function()
				{
					this.image.fixImageSourceAfterDrop();
					this.observe.images();
					this.$editor.off('drop.redactor-image-inside-drop');
					this.clean.clearUnverified();
					this.code.sync();
				},
				fixImageSourceAfterDrop: function()
				{
					this.$editor.find('img[data-save-url]').each(function()
					{
						var $el = $(this);
						$el.attr('src', $el.attr('data-save-url'));
						$el.removeAttr('data-save-url');
					});
				},
				hideResize: function(e)
				{
					if (e && $(e.target).closest('#redactor-image-box', this.$editor[0]).length !== 0) return;
					if (e && e.target.tagName == 'IMG')
					{
						var $image = $(e.target);
						$image.attr('data-save-url', $image.attr('src'));
					}

					var imageBox = this.$editor.find('#redactor-image-box');
					if (imageBox.length === 0) return;

					$('#redactor-image-editter').remove();
					$('#redactor-image-resizer').remove();

					imageBox.find('img').css({
						marginTop: imageBox[0].style.marginTop,
						marginBottom: imageBox[0].style.marginBottom,
						marginLeft: imageBox[0].style.marginLeft,
						marginRight: imageBox[0].style.marginRight
					});

					imageBox.css('margin', '');
					imageBox.find('img').css('opacity', '');
					imageBox.replaceWith(function()
					{
						return $(this).contents();
					});

					$(document).off('mousedown.redactor-image-resize-hide.' + this.uuid);


					if (typeof this.image.resizeHandle !== 'undefined')
					{
						this.image.resizeHandle.el.attr('rel', this.image.resizeHandle.el.attr('style'));
					}

					this.code.sync();

				},
				loadResizableControls: function($image, imageBox)
				{
					if (this.opts.imageResizable && !this.utils.isMobile())
					{
						var imageResizer = $('<span id="redactor-image-resizer" data-redactor="verified"></span>');

						if (!this.utils.isDesktop())
						{
							imageResizer.css({ width: '15px', height: '15px' });
						}

						imageResizer.attr('contenteditable', false);
						imageBox.append(imageResizer);
						imageBox.append($image);

						return imageResizer;
					}
					else
					{
						imageBox.append($image);
						return false;
					}
				},
				loadEditableControls: function($image)
				{
					var imageBox = $('<span id="redactor-image-box" data-redactor="verified">');
					imageBox.css('float', $image.css('float')).attr('contenteditable', false);

					if ($image[0].style.margin != 'auto')
					{
						imageBox.css({
							marginTop: $image[0].style.marginTop,
							marginBottom: $image[0].style.marginBottom,
							marginLeft: $image[0].style.marginLeft,
							marginRight: $image[0].style.marginRight
						});

						$image.css('margin', '');
					}
					else
					{
						imageBox.css({ 'display': 'block', 'margin': 'auto' });
					}

					$image.css('opacity', '.5').after(imageBox);


					if (this.opts.imageEditable)
					{
						// editter
						this.image.editter = $('<span id="redactor-image-editter" data-redactor="verified">' + this.lang.get('edit') + '</span>');
						this.image.editter.attr('contenteditable', false);
						this.image.editter.on('click', $.proxy(function()
						{
							this.image.showEdit($image);
						}, this));

						imageBox.append(this.image.editter);

						// position correction
						var editerWidth = this.image.editter.innerWidth();
						this.image.editter.css('margin-left', '-' + editerWidth/2 + 'px');
					}

					return this.image.loadResizableControls($image, imageBox);

				},
				remove: function(image)
				{
					var $image = $(image);
					var $link = $image.closest('a', this.$editor[0]);
					var $figure = $image.closest('figure', this.$editor[0]);
					var $parent = $image.parent();
					if ($('#redactor-image-box').length !== 0)
					{
						$parent = $('#redactor-image-box').parent();
					}

					var $next;
					if ($figure.length !== 0)
					{
						$next = $figure.next();
						$figure.remove();
					}
					else if ($link.length !== 0)
					{
						$parent = $link.parent();
						$link.remove();
					}
					else
					{
						$image.remove();
					}

					$('#redactor-image-box').remove();

					if ($figure.length !== 0)
					{
						this.caret.setStart($next);
					}
					else
					{
						this.caret.setStart($parent);
					}

					// delete callback
					this.core.setCallback('imageDelete', $image[0].src, $image);

					this.modal.close();
					this.code.sync();
				},
				insert: function(json, direct, e)
				{
					// error callback
					if (typeof json.error != 'undefined')
					{
						this.modal.close();
						this.selection.restore();
						this.core.setCallback('imageUploadError', json);
						return;
					}

					var $img;
					if (typeof json == 'string')
					{
						$img = $(json).attr('data-redactor-inserted-image', 'true');
					}
					else
					{
						$img = $('<img>');
						$img.attr('src', json.filelink).attr('data-redactor-inserted-image', 'true');
					}


					var node = $img;
					var isP = this.utils.isCurrentOrParent('P');
					if (isP)
					{
						// will replace
						node = $('<blockquote />').append($img);
					}

					if (direct)
					{
						this.selection.removeMarkers();
						var marker = this.selection.getMarker();
						this.insert.nodeToCaretPositionFromPoint(e, marker);
					}
					else
					{
						this.modal.close();
					}

					this.selection.restore();
					this.buffer.set();

					this.insert.html(this.utils.getOuterHtml(node), false);

					var $image = this.$editor.find('img[data-redactor-inserted-image=true]').removeAttr('data-redactor-inserted-image');

					if (isP)
					{
						$image.parent().contents().unwrap().wrap('<p />');
					}
					else if (this.opts.linebreaks)
					{
						if (!this.utils.isEmpty(this.code.get()))
						{
							$image.before('<br>');
						}

						$image.after('<br>');
					}

					if (typeof json == 'string') return;

					this.core.setCallback('imageUpload', $image, json);

				}
			};
		},
		indent: function()
		{
			return {
				increase: function()
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					var block = this.selection.getBlock();

					if (block && block.tagName == 'LI')
					{
						this.indent.increaseLists();
					}
					else if (block === false && this.opts.linebreaks)
					{
						this.indent.increaseText();
					}
					else
					{
						this.indent.increaseBlocks();
					}

					this.selection.restore();
					this.code.sync();
				},
				increaseLists: function()
				{
					document.execCommand('indent');

					this.indent.fixEmptyIndent();
					this.clean.normalizeLists();
					this.clean.clearUnverified();
				},
				increaseBlocks: function()
				{
					$.each(this.selection.getBlocks(), $.proxy(function(i, elem)
					{
						if (elem.tagName === 'TD' || elem.tagName === 'TH') return;

						var $el = this.utils.getAlignmentElement(elem);

						var left = this.utils.normalize($el.css('margin-left')) + this.opts.indentValue;
						$el.css('margin-left', left + 'px');

					}, this));
				},
				increaseText: function()
				{
					var wrapper = this.selection.wrap('div');
					$(wrapper).attr('data-tagblock', 'redactor');
					$(wrapper).css('margin-left', this.opts.indentValue + 'px');
				},
				decrease: function()
				{
					this.buffer.set();
					this.selection.save();

					var block = this.selection.getBlock();
					if (block && block.tagName == 'LI')
					{
						this.indent.decreaseLists();
					}
					else
					{
						this.indent.decreaseBlocks();
					}

					this.selection.restore();
					this.code.sync();
				},
				decreaseLists: function()
				{
					document.execCommand('outdent');

					var current = this.selection.getCurrent();
					var $item = $(current).closest('li', this.$editor[0]);

					this.indent.fixEmptyIndent();

					if (!this.opts.linebreaks && $item.length === 0)
					{
						document.execCommand('formatblock', false, 'p');
						this.$editor.find('ul, ol, blockquote, p').each($.proxy(this.utils.removeEmpty, this));
					}

					this.clean.clearUnverified();
				},
				decreaseBlocks: function()
				{
					$.each(this.selection.getBlocks(), $.proxy(function(i, elem)
					{
						var $el = this.utils.getAlignmentElement(elem);
						var left = this.utils.normalize($el.css('margin-left')) - this.opts.indentValue;

						if (left <= 0)
						{
							if (this.opts.linebreaks && typeof($el.data('tagblock')) !== 'undefined')
							{
								$el.replaceWith($el.html() + '<br />');
							}
							else
							{
								$el.css('margin-left', '');
								this.utils.removeEmptyAttr($el, 'style');
							}
						}
						else
						{
							$el.css('margin-left', left + 'px');
						}

					}, this));
				},
				fixEmptyIndent: function()
				{
					var block = this.selection.getBlock();

					if (this.range.collapsed && block && block.tagName == 'LI' && this.utils.isEmpty($(block).text()))
					{
						var $block = $(block);
						$block.find('span').not('.redactor-selection-marker').contents().unwrap();
						$block.append('<br>');
					}
				}
			};
		},
		inline: function()
		{
			return {
				formatting: function(name)
				{
					var type, value;

					if (typeof this.formatting[name].style != 'undefined') type = 'style';
					else if (typeof this.formatting[name]['class'] != 'undefined') type = 'class';

					if (type) value = this.formatting[name][type];

					this.inline.format(this.formatting[name].tag, type, value);

				},
				format: function(tag, type, value)
				{
					var current = this.selection.getCurrent();
					if (current && current.tagName === 'TR') return;

					// blur
					this.blurClickedElement = true;

					// Stop formatting pre and headers
					if (this.utils.isCurrentOrParent('PRE') || this.utils.isCurrentOrParentHeader()) return;

					var tags = ['b', 'bold', 'i', 'italic', 'underline', 'strikethrough', 'deleted', 'superscript', 'subscript'];
					var replaced = ['strong', 'strong', 'em', 'em', 'u', 'del', 'del', 'sup', 'sub'];

					for (var i = 0; i < tags.length; i++)
					{
						if (tag == tags[i]) tag = replaced[i];
					}

					if (this.opts.allowedTags)
					{
						if ($.inArray(tag, this.opts.allowedTags) == -1) return;
					}
					else
					{
						if ($.inArray(tag, this.opts.deniedTags) !== -1) return;
					}

					this.inline.type = type || false;
					this.inline.value = value || false;

					this.buffer.set();

					if (!this.utils.browser('msie'))
					{
						this.$editor.focus();
					}

					this.selection.get();

					if (this.range.collapsed)
					{
						this.inline.formatCollapsed(tag);
					}
					else
					{
						this.inline.formatMultiple(tag);
					}
				},
				formatCollapsed: function(tag)
				{
					var current = this.selection.getCurrent();
					var $parent = $(current).closest(tag + '[data-redactor-tag=' + tag + ']', this.$editor[0]);

					// inline there is
					if ($parent.length !== 0 && (this.inline.type != 'style' && $parent[0].tagName != 'SPAN'))
					{
						// remove empty
						if (this.utils.isEmpty($parent.text()))
						{
							this.caret.setAfter($parent[0]);

							$parent.remove();
							this.code.sync();
						}
						else if (this.utils.isEndOfElement($parent))
						{
							this.caret.setAfter($parent[0]);
						}

						return;
					}

					// create empty inline
					var node = $('<' + tag + '>').attr('data-verified', 'redactor').attr('data-redactor-tag', tag);
					node.html(this.opts.invisibleSpace);

					node = this.inline.setFormat(node);

					var node = this.insert.node(node);
					this.caret.setEnd(node);

					this.code.sync();
				},
				formatMultiple: function(tag)
				{
					this.inline.formatConvert(tag);

					this.selection.save();
					document.execCommand('strikethrough');

					this.$editor.find('strike').each($.proxy(function(i,s)
					{
						var $el = $(s);

						this.inline.formatRemoveSameChildren($el, tag);

						var $span;
						if (this.inline.type)
						{
							$span = $('<span>').attr('data-redactor-tag', tag).attr('data-verified', 'redactor');
							$span = this.inline.setFormat($span);
						}
						else
						{
							$span = $('<' + tag + '>').attr('data-redactor-tag', tag).attr('data-verified', 'redactor');
						}

						$el.replaceWith($span.html($el.contents()));

						if (tag == 'span')
						{
							var $parent = $span.parent();
							if ($parent && $parent[0].tagName == 'SPAN' && this.inline.type == 'style')
							{
								var arr = this.inline.value.split(';');

								for (var z = 0; z < arr.length; z++)
								{
									if (arr[z] === '') return;
									var style = arr[z].split(':');
									$parent.css(style[0], '');

									if (this.utils.removeEmptyAttr($parent, 'style'))
									{
										$parent.replaceWith($parent.contents());
									}

								}

							}
						}

					}, this));

					// clear text decoration
					if (tag != 'span')
					{
						this.$editor.find(this.opts.inlineTags.join(', ')).each($.proxy(function(i,s)
						{
							var $el = $(s);
							var property = $el.css('text-decoration');
							if (property == 'line-through')
							{
								$el.css('text-decoration', '');
								this.utils.removeEmptyAttr($el, 'style');
							}
						}, this));
					}

					if (tag != 'del')
					{
						var _this = this;
						this.$editor.find('inline').each(function(i,s)
						{
							_this.utils.replaceToTag(s, 'del');
						});
					}

					this.selection.restore();
					this.code.sync();

				},
				formatRemoveSameChildren: function($el, tag)
				{
					var self = this;
					$el.children(tag).each(function()
					{
						var $child = $(this);

						if (!$child.hasClass('redactor-selection-marker'))
						{
							if (self.inline.type == 'style')
							{
								var arr = self.inline.value.split(';');

								for (var z = 0; z < arr.length; z++)
								{
									if (arr[z] === '') return;

									var style = arr[z].split(':');
									$child.css(style[0], '');

									if (self.utils.removeEmptyAttr($child , 'style'))
									{
										$child.replaceWith($child.contents());
									}

								}
							}
							else
							{
								$child.contents().unwrap();
							}
						}

					});
				},
				formatConvert: function(tag)
				{
					this.selection.save();

					var find = '';
					if (this.inline.type == 'class') find = '[data-redactor-class=' + this.inline.value + ']';
					else if (this.inline.type == 'style')
					{
						find = '[data-redactor-style="' + this.inline.value + '"]';
					}

					var self = this;
					if (tag != 'del')
					{
						this.$editor.find('del').each(function(i,s)
						{
							self.utils.replaceToTag(s, 'inline');
						});
					}

					if (tag != 'span')
					{
						this.$editor.find(tag).each(function()
						{
							var $el = $(this);
							$el.replaceWith($('<strike />').html($el.contents()));

						});
					}

					this.$editor.find('[data-redactor-tag="' + tag + '"]' + find).each(function()
					{
						if (find === '' && tag == 'span' && this.tagName.toLowerCase() == tag) return;

						var $el = $(this);
						$el.replaceWith($('<strike />').html($el.contents()));

					});

					this.selection.restore();
				},
				setFormat: function(node)
				{
					switch (this.inline.type)
					{
						case 'class':

							if (node.hasClass(this.inline.value))
							{
								node.removeClass(this.inline.value);
								node.removeAttr('data-redactor-class');
							}
							else
							{
								node.addClass(this.inline.value);
								node.attr('data-redactor-class', this.inline.value);
							}


						break;
						case 'style':

							node[0].style.cssText = this.inline.value;
							node.attr('data-redactor-style', this.inline.value);

						break;
					}

					return node;
				},
				removeStyle: function()
				{
					this.buffer.set();
					var current = this.selection.getCurrent();
					var nodes = this.selection.getInlines();

					this.selection.save();

					if (current && current.tagName === 'SPAN')
					{
						var $s = $(current);

						$s.removeAttr('style');
						if ($s[0].attributes.length === 0)
						{
							$s.replaceWith($s.contents());
						}
					}

					$.each(nodes, $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.removeAttr('style');
							if ($s[0].attributes.length === 0)
							{
								$s.replaceWith($s.contents());
							}
						}
					}, this));

					this.selection.restore();
					this.code.sync();

				},
				removeStyleRule: function(name)
				{
					this.buffer.set();
					var parent = this.selection.getParent();
					var nodes = this.selection.getInlines();

					this.selection.save();

					if (parent && parent.tagName === 'SPAN')
					{
						var $s = $(parent);

						$s.css(name, '');
						this.utils.removeEmptyAttr($s, 'style');
						if ($s[0].attributes.length === 0)
						{
							$s.replaceWith($s.contents());
						}
					}

					$.each(nodes, $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.css(name, '');
							this.utils.removeEmptyAttr($s, 'style');
							if ($s[0].attributes.length === 0)
							{
								$s.replaceWith($s.contents());
							}
						}
					}, this));

					this.selection.restore();
					this.code.sync();
				},
				removeFormat: function()
				{
					this.buffer.set();
					var current = this.selection.getCurrent();

					this.selection.save();

					document.execCommand('removeFormat');

					if (current && current.tagName === 'SPAN')
					{
						$(current).replaceWith($(current).contents());
					}


					$.each(this.selection.getNodes(), $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.replaceWith($s.contents());
						}
					}, this));

					this.selection.restore();
					this.code.sync();

				},
				toggleClass: function(className)
				{
					this.inline.format('span', 'class', className);
				},
				toggleStyle: function(value)
				{
					this.inline.format('span', 'style', value);
				}
			};
		},
		insert: function()
		{
			return {
				set: function(html, clean)
				{
					this.placeholder.remove();

					html = this.clean.setVerified(html);

					if (typeof clean == 'undefined')
					{
						html = this.clean.onPaste(html, false);
					}

					this.$editor.html(html);
					this.selection.remove();
					this.focus.setEnd();
					this.clean.normalizeLists();
					this.code.sync();
					this.observe.load();

					if (typeof clean == 'undefined')
					{
						setTimeout($.proxy(this.clean.clearUnverified, this), 10);
					}
				},
				text: function(text)
				{
					this.placeholder.remove();

					text = text.toString();
					text = $.trim(text);
					text = this.clean.getPlainText(text, false);

					this.$editor.focus();

					if (this.utils.browser('msie'))
					{
						this.insert.htmlIe(text);
					}
					else
					{
						this.selection.get();

						this.range.deleteContents();
						var el = document.createElement("div");
						el.innerHTML = text;
						var frag = document.createDocumentFragment(), node, lastNode;
						while ((node = el.firstChild))
						{
							lastNode = frag.appendChild(node);
						}

						this.range.insertNode(frag);

						if (lastNode)
						{
							var range = this.range.cloneRange();
							range.setStartAfter(lastNode);
							range.collapse(true);
							this.sel.removeAllRanges();
							this.sel.addRange(range);
						}
					}

					this.code.sync();
					this.clean.clearUnverified();
				},
				htmlWithoutClean: function(html)
				{
					this.insert.html(html, false);
				},
				html: function(html, clean)
				{
					this.placeholder.remove();

					if (typeof clean == 'undefined') clean = true;

					this.$editor.focus();

					html = this.clean.setVerified(html);

					if (clean)
					{
						html = this.clean.onPaste(html);
					}

					if (this.utils.browser('msie'))
					{
						this.insert.htmlIe(html);
					}
					else
					{
						if (this.clean.singleLine) this.insert.execHtml(html);
						else document.execCommand('insertHTML', false, html);

						this.insert.htmlFixMozilla();

					}

					this.clean.normalizeLists();

					// remove empty paragraphs finaly
					if (!this.opts.linebreaks)
					{
						this.$editor.find('p').each($.proxy(this.utils.removeEmpty, this));
					}

					this.code.sync();
					this.observe.load();

					if (clean)
					{
						this.clean.clearUnverified();
					}

				},
				htmlFixMozilla: function()
				{
					// FF inserts empty p when content was selected dblclick
					if (!this.utils.browser('mozilla')) return;

					var $next = $(this.selection.getBlock()).next();
					if ($next.length > 0 && $next[0].tagName == 'P' && $next.html() === '')
					{
						$next.remove();
					}

				},
				htmlIe: function(html)
				{
					if (this.utils.isIe11())
					{
						var parent = this.utils.isCurrentOrParent('P');
						var $html = $('<div>').append(html);
						var blocksMatch = $html.contents().is('p, :header, dl, ul, ol, div, table, td, blockquote, pre, address, section, header, footer, aside, article');

						if (parent && blocksMatch) this.insert.ie11FixInserting(parent, html);
						else this.insert.ie11PasteFrag(html);

						return;
					}

					document.selection.createRange().pasteHTML(html);

				},
				execHtml: function(html)
				{
					html = this.clean.setVerified(html);

					this.selection.get();

					this.range.deleteContents();

					var el = document.createElement('div');
					el.innerHTML = html;

					var frag = document.createDocumentFragment(), node, lastNode;
					while ((node = el.firstChild))
					{
						lastNode = frag.appendChild(node);
					}

					this.range.insertNode(frag);

					this.range.collapse(true);
					this.caret.setAfter(lastNode);

				},
				node: function(node, deleteContents)
				{
					node = node[0] || node;

					var html = this.utils.getOuterHtml(node);
					html = this.clean.setVerified(html);

					if (html.match(/</g) !== null)
					{
						node = $(html)[0];
					}

					this.selection.get();

					if (deleteContents !== false)
					{
						this.range.deleteContents();
					}

					this.range.insertNode(node);
					this.range.collapse(false);
					this.selection.addRange();

					return node;
				},
				nodeToPoint: function(node, x, y)
				{
					node = node[0] || node;

					this.selection.get();

					var range;
					if (document.caretPositionFromPoint)
					{
					    var pos = document.caretPositionFromPoint(x, y);

					    this.range.setStart(pos.offsetNode, pos.offset);
					    this.range.collapse(true);
					    this.range.insertNode(node);
					}
					else if (document.caretRangeFromPoint)
					{
					    range = document.caretRangeFromPoint(x, y);
					    range.insertNode(node);
					}
					else if (typeof document.body.createTextRange != "undefined")
					{
				        range = document.body.createTextRange();
				        range.moveToPoint(x, y);
				        var endRange = range.duplicate();
				        endRange.moveToPoint(x, y);
				        range.setEndPoint("EndToEnd", endRange);
				        range.select();
					}
				},
				nodeToCaretPositionFromPoint: function(e, node)
				{
					node = node[0] || node;

					var range;
					var x = e.clientX, y = e.clientY;
					if (document.caretPositionFromPoint)
					{
					    var pos = document.caretPositionFromPoint(x, y);
					    var sel = document.getSelection();
					    range = sel.getRangeAt(0);
					    range.setStart(pos.offsetNode, pos.offset);
					    range.collapse(true);
					    range.insertNode(node);
					}
					else if (document.caretRangeFromPoint)
					{
					    range = document.caretRangeFromPoint(x, y);
					    range.insertNode(node);
					}
					else if (typeof document.body.createTextRange != "undefined")
					{
				        range = document.body.createTextRange();
				        range.moveToPoint(x, y);
				        var endRange = range.duplicate();
				        endRange.moveToPoint(x, y);
				        range.setEndPoint("EndToEnd", endRange);
				        range.select();
					}

				},
				ie11FixInserting: function(parent, html)
				{
					var node = document.createElement('span');
					node.className = 'redactor-ie-paste';
					this.insert.node(node);

					var parHtml = $(parent).html();

					parHtml = '<p>' + parHtml.replace(/<span class="redactor-ie-paste"><\/span>/gi, '</p>' + html + '<p>') + '</p>';
					parHtml = parHtml.replace(/<p><\/p>/gi, '');
					$(parent).replaceWith(parHtml);
				},
				ie11PasteFrag: function(html)
				{
					this.selection.get();
					this.range.deleteContents();

					var el = document.createElement("div");
					el.innerHTML = html;

					var frag = document.createDocumentFragment(), node, lastNode;
					while ((node = el.firstChild))
					{
						lastNode = frag.appendChild(node);
					}

					this.range.insertNode(frag);
					this.range.collapse(false);
					this.selection.addRange();
				}
			};
		},
		keydown: function()
		{
			return {
				init: function(e)
				{
					if (this.rtePaste) return;

					var key = e.which;
					var arrow = (key >= 37 && key <= 40);

					this.keydown.ctrl = e.ctrlKey || e.metaKey;
					this.keydown.current = this.selection.getCurrent();
					this.keydown.parent = this.selection.getParent();
					this.keydown.block = this.selection.getBlock();

			        // detect tags
					this.keydown.pre = this.utils.isTag(this.keydown.current, 'pre');
					this.keydown.blockquote = this.utils.isTag(this.keydown.current, 'blockquote');
					this.keydown.figcaption = this.utils.isTag(this.keydown.current, 'figcaption');

					// shortcuts setup
					this.shortcuts.init(e, key);

					if (this.utils.isDesktop())
					{
						this.keydown.checkEvents(arrow, key);
						this.keydown.setupBuffer(e, key);
					}

					this.keydown.addArrowsEvent(arrow);
					this.keydown.setupSelectAll(e, key);

					// callback
					var keydownStop = this.core.setCallback('keydown', e);
					if (keydownStop === false)
					{
						e.preventDefault();
						return false;
					}

					// ie and ff exit from table
					if (this.opts.enterKey && (this.utils.browser('msie') || this.utils.browser('mozilla')) && (key === this.keyCode.DOWN || key === this.keyCode.RIGHT))
					{
						var isEndOfTable = false;
						var $table = false;
						if (this.keydown.block && this.keydown.block.tagName === 'TD')
						{
							$table = $(this.keydown.block).closest('table', this.$editor[0]);
						}

						if ($table && $table.find('td').last()[0] === this.keydown.block)
						{
							isEndOfTable = true;
						}

						if (this.utils.isEndOfElement() && isEndOfTable)
						{
							var node = $(this.opts.emptyHtml);
							$table.after(node);
							this.caret.setStart(node);
						}
					}

					// down
					if (this.opts.enterKey && key === this.keyCode.DOWN)
					{
						this.keydown.onArrowDown();
					}

					// turn off enter key
					if (!this.opts.enterKey && key === this.keyCode.ENTER)
					{
						e.preventDefault();
						// remove selected
						if (!this.range.collapsed) this.range.deleteContents();
						return;
					}

					// on enter
					if (key == this.keyCode.ENTER && !e.shiftKey && !e.ctrlKey && !e.metaKey)
					{
						var stop = this.core.setCallback('enter', e);
						if (stop === false)
						{
							e.preventDefault();
							return false;
						}

						if (this.keydown.blockquote && this.keydown.exitFromBlockquote(e) === true)
						{
							return false;
						}

						var current, $next;
						if (this.keydown.pre)
						{
							return this.keydown.insertNewLine(e);
						}
						else if (this.keydown.blockquote || this.keydown.figcaption)
						{
							current = this.selection.getCurrent();
							$next = $(current).next();

							if ($next.length !== 0 && $next[0].tagName == 'BR')
							{
								return this.keydown.insertBreakLine(e);
							}
							else if (this.utils.isEndOfElement() && (current && current != 'SPAN'))
							{
								return this.keydown.insertDblBreakLine(e);
							}
							else
							{
								return this.keydown.insertBreakLine(e);
							}
						}
						else if (this.opts.linebreaks && !this.keydown.block)
						{
							current = this.selection.getCurrent();
							$next = $(this.keydown.current).next();

							if ($next.length !== 0 && $next[0].tagName == 'BR')
							{
								return this.keydown.insertBreakLine(e);
							}
							else if (current !== false && $(current).hasClass('redactor-invisible-space'))
							{
								this.caret.setAfter(current);
								$(current).contents().unwrap();

								return this.keydown.insertDblBreakLine(e);
							}
							else
							{
								if (this.utils.isEndOfEditor())
								{
									return this.keydown.insertDblBreakLine(e);
								}
								else if ($next.length === 0 && current === false && typeof $next.context != 'undefined')
								{
									return this.keydown.insertBreakLine(e);
								}

								return this.keydown.insertBreakLine(e);
							}

						}
						else if (this.opts.linebreaks && this.keydown.block)
						{
							setTimeout($.proxy(this.keydown.replaceDivToBreakLine, this), 1);
						}
						// paragraphs
						else if (!this.opts.linebreaks && this.keydown.block)
						{
							setTimeout($.proxy(this.keydown.replaceDivToParagraph, this), 1);

							if (this.keydown.block.tagName === 'LI')
							{
								current = this.selection.getCurrent();
								var $parent = $(current).closest('li', this.$editor[0]);
								var $list = $parent.closest('ul,ol', this.$editor[0]);

								if ($parent.length !== 0 && this.utils.isEmpty($parent.html()) && $list.next().length === 0 && this.utils.isEmpty($list.find("li").last().html()))
								{
									$list.find("li").last().remove();

									var node = $(this.opts.emptyHtml);
									$list.after(node);
									this.caret.setStart(node);

									return false;
								}
							}
						}
						else if (!this.opts.linebreaks && !this.keydown.block)
						{
							return this.keydown.insertParagraph(e);
						}
					}

					// Shift+Enter or Ctrl+Enter
					if (key === this.keyCode.ENTER && (e.ctrlKey || e.shiftKey))
					{
						return this.keydown.onShiftEnter(e);
					}


					// tab or cmd + [
					if (key === this.keyCode.TAB || e.metaKey && key === 221 || e.metaKey && key === 219)
					{
						return this.keydown.onTab(e, key);
					}

					// image delete and backspace
					if (key === this.keyCode.BACKSPACE || key === this.keyCode.DELETE)
					{
						var nodes = this.selection.getNodes();

						if (nodes)
						{
							var len = nodes.length;
							var last;
							for (var i = 0; i < len; i++)
							{
								var children = $(nodes[i]).children('img');
								if (children.length !== 0)
								{
									var self = this;
									$.each(children, function(z,s)
									{
										var $s = $(s);
										if ($s.css('float') != 'none') return;

										// image delete callback
										self.core.setCallback('imageDelete', s.src, $s);
										last = s;
									});
								}
								else if (nodes[i].tagName == 'IMG')
								{
									if (last != nodes[i])
									{
										// image delete callback
										this.core.setCallback('imageDelete', nodes[i].src, $(nodes[i]));
										last = nodes[i];
									}
								}
							}
						}
					}

					// backspace
					if (key === this.keyCode.BACKSPACE)
					{
						// backspace as outdent
						var block = this.selection.getBlock();
						var indented = ($(block).css('margin-left') !== '0px');
						if (block && indented && this.range.collapsed && this.utils.isStartOfElement())
						{
							this.indent.decrease();
							e.preventDefault();
							return;
						}


						// remove hr in FF
						if (this.utils.browser('mozilla'))
						{
							var prev = this.selection.getPrev();
							var prev2 = $(prev).prev()[0];
							if (prev && prev.tagName === 'HR') $(prev).remove();
							if (prev2 && prev2.tagName === 'HR') $(prev2).remove();
						}

						this.keydown.removeInvisibleSpace();
						this.keydown.removeEmptyListInTable(e);
					}

					this.code.sync();
				},
				checkEvents: function(arrow, key)
				{
					if (!arrow && (this.core.getEvent() == 'click' || this.core.getEvent() == 'arrow'))
					{
						this.core.addEvent(false);

						if (this.keydown.checkKeyEvents(key))
						{
							this.buffer.set();
						}
					}
				},
				checkKeyEvents: function(key)
				{
					var k = this.keyCode;
					var keys = [k.BACKSPACE, k.DELETE, k.ENTER, k.SPACE, k.ESC, k.TAB, k.CTRL, k.META, k.ALT, k.SHIFT];

					return ($.inArray(key, keys) == -1) ? true : false;

				},
				addArrowsEvent: function(arrow)
				{
					if (!arrow) return;

					if ((this.core.getEvent() == 'click' || this.core.getEvent() == 'arrow'))
					{
						this.core.addEvent(false);
						return;
					}

				    this.core.addEvent('arrow');
				},
				setupBuffer: function(e, key)
				{
					if (this.keydown.ctrl && key === 90 && !e.shiftKey && !e.altKey && this.opts.buffer.length) // z key
					{
						e.preventDefault();
						this.buffer.undo();
						return;
					}
					// undo
					else if (this.keydown.ctrl && key === 90 && e.shiftKey && !e.altKey && this.opts.rebuffer.length !== 0)
					{
						e.preventDefault();
						this.buffer.redo();
						return;
					}
					else if (!this.keydown.ctrl)
					{
						if (key == this.keyCode.BACKSPACE || key == this.keyCode.DELETE || (key == this.keyCode.ENTER && !e.ctrlKey && !e.shiftKey) || key == this.keyCode.SPACE)
						{
							this.buffer.set();
						}
					}
				},
				setupSelectAll: function(e, key)
				{
					if (this.keydown.ctrl && key === 65)
					{
						this.utils.enableSelectAll();
					}
					else if (key != this.keyCode.LEFT_WIN && !this.keydown.ctrl)
					{
						this.utils.disableSelectAll();
					}
				},
				onArrowDown: function()
				{
					var tags = [this.keydown.blockquote, this.keydown.pre, this.keydown.figcaption];

					for (var i = 0; i < tags.length; i++)
					{
						if (tags[i])
						{
							this.keydown.insertAfterLastElement(tags[i]);
							return false;
						}
					}
				},
				onShiftEnter: function(e)
				{
					this.buffer.set();

					if (this.utils.isEndOfElement())
					{
						return this.keydown.insertDblBreakLine(e);
					}

					return this.keydown.insertBreakLine(e);
				},
				onTab: function(e, key)
				{
					if (!this.opts.tabKey) return true;
					if (this.utils.isEmpty(this.code.get()) && this.opts.tabAsSpaces === false) return true;

					e.preventDefault();

					var node;
					if (this.keydown.pre && !e.shiftKey)
					{
						node = (this.opts.preSpaces) ? document.createTextNode(Array(this.opts.preSpaces + 1).join('\u00a0')) : document.createTextNode('\t');
						this.insert.node(node);
						this.code.sync();
					}
					else if (this.opts.tabAsSpaces !== false)
					{
						node = document.createTextNode(Array(this.opts.tabAsSpaces + 1).join('\u00a0'));
						this.insert.node(node);
						this.code.sync();
					}
					else
					{
						if (e.metaKey && key === 219) this.indent.decrease();
						else if (e.metaKey && key === 221) this.indent.increase();
						else if (!e.shiftKey) this.indent.increase();
						else this.indent.decrease();
					}

					return false;
				},
				replaceDivToBreakLine: function()
				{
					var blockElem = this.selection.getBlock();
					var blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, '');
					if ((blockElem.tagName === 'DIV' || blockElem.tagName === 'P') && blockHtml === '' && !$(blockElem).hasClass('redactor-editor'))
					{
						var br = document.createElement('br');

						$(blockElem).replaceWith(br);
						this.caret.setBefore(br);

						this.code.sync();

						return false;
					}
				},
				replaceDivToParagraph: function()
				{
					var blockElem = this.selection.getBlock();
					var blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, '');
					if (blockElem.tagName === 'DIV' && this.utils.isEmpty(blockHtml) && !$(blockElem).hasClass('redactor-editor'))
					{
						var p = document.createElement('p');
						p.innerHTML = this.opts.invisibleSpace;

						$(blockElem).replaceWith(p);
						this.caret.setStart(p);

						this.code.sync();

						return false;
					}
					else if (this.opts.cleanStyleOnEnter && blockElem.tagName == 'P')
					{
						$(blockElem).removeAttr('class').removeAttr('style');
					}
				},
				insertParagraph: function(e)
				{
					e.preventDefault();

					this.selection.get();

					var p = document.createElement('p');
					p.innerHTML = this.opts.invisibleSpace;

					this.range.deleteContents();
					this.range.insertNode(p);

					this.caret.setStart(p);

					this.code.sync();

					return false;
				},
				exitFromBlockquote: function(e)
				{
					if (!this.utils.isEndOfElement()) return;

					var tmp = $.trim($(this.keydown.block).html());
					if (tmp.search(/(<br\s?\/?>){2}$/i) != -1)
					{
						e.preventDefault();

						if (this.opts.linebreaks)
						{
							var br = document.createElement('br');
							$(this.keydown.blockquote).after(br);

							this.caret.setBefore(br);
							$(this.keydown.block).html(tmp.replace(/<br\s?\/?>$/i, ''));
						}
						else
						{
							var node = $(this.opts.emptyHtml);
							$(this.keydown.blockquote).after(node);
							this.caret.setStart(node);
						}

						return true;

					}

					return;

				},
				insertAfterLastElement: function(element)
				{
					if (!this.utils.isEndOfElement()) return;

					this.buffer.set();

					if (this.opts.linebreaks)
					{
						var contents = $('<div>').append($.trim(this.$editor.html())).contents();
						var last = contents.last()[0];
						if (last.tagName == 'SPAN' && last.innerHTML === '')
						{
							last = contents.prev()[0];
						}

						if (this.utils.getOuterHtml(last) != this.utils.getOuterHtml(element)) return;

						var br = document.createElement('br');
						$(element).after(br);
						this.caret.setAfter(br);

					}
					else
					{
						if (this.$editor.contents().last()[0] !== element) return;

						var node = $(this.opts.emptyHtml);
						$(element).after(node);
						this.caret.setStart(node);
					}
				},
				insertNewLine: function(e)
				{
					e.preventDefault();

					var node = document.createTextNode('\n');

					this.selection.get();

					this.range.deleteContents();
					this.range.insertNode(node);

					this.caret.setAfter(node);

					this.code.sync();

					return false;
				},
				insertBreakLine: function(e)
				{
					return this.keydown.insertBreakLineProcessing(e);
				},
				insertDblBreakLine: function(e)
				{
					return this.keydown.insertBreakLineProcessing(e, true);
				},
				insertBreakLineProcessing: function(e, dbl)
				{
					e.stopPropagation();

					this.selection.get();

					var br1 = document.createElement('br');

					if (this.utils.browser('msie'))
					{
						this.range.collapse(false);
						this.range.setEnd(this.range.endContainer, this.range.endOffset);
					}
					else
					{
						this.range.deleteContents();
					}

					this.range.insertNode(br1);

					// move br outside A tag
					var $parentA = $(br1).parent("a");

					if ($parentA.length > 0)
					{
						$parentA.find(br1).remove();
						$parentA.after(br1);
					}

					if (dbl === true)
					{
						var $next = $(br1).next();
						if ($next.length !== 0 && $next[0].tagName === 'BR' && this.utils.isEndOfEditor())
						{
							this.caret.setAfter(br1);
							this.code.sync();
							return false;
						}

						var br2 = document.createElement('br');

						this.range.insertNode(br2);
						this.caret.setAfter(br2);
					}
					else
					{
						// caret does not move after the br visual
						if (this.utils.browser('msie'))
						{
							var space = document.createElement('span');
							space.innerHTML = '&#x200b;';

							$(br1).after(space);
							this.caret.setAfter(space);
							$(space).remove();
						}
						else
						{
							var range = document.createRange();
							range.setStartAfter(br1);
							range.collapse(true);
							var selection = window.getSelection();
							selection.removeAllRanges();
							selection.addRange(range);

						}
					}

					this.code.sync();
					return false;
				},
				removeInvisibleSpace: function()
				{
					var $current = $(this.keydown.current);
					if ($current.text().search(/^\u200B$/g) === 0)
					{
						$current.remove();
					}
				},
				removeEmptyListInTable: function(e)
				{
					var $current = $(this.keydown.current);
					var $parent = $(this.keydown.parent);
					var td = $current.closest('td', this.$editor[0]);

					if (td.length !== 0 && $current.closest('li', this.$editor[0]) && $parent.children('li').length === 1)
					{
						if (!this.utils.isEmpty($current.text())) return;

						e.preventDefault();

						$current.remove();
						$parent.remove();

						this.caret.setStart(td);
					}
				}
			};
		},
		keyup: function()
		{
			return {
				init: function(e)
				{
					if (this.rtePaste) return;

					var key = e.which;

					this.keyup.current = this.selection.getCurrent();
					this.keyup.parent = this.selection.getParent();
					var $parent = this.utils.isRedactorParent($(this.keyup.parent).parent());

					// callback
					var keyupStop = this.core.setCallback('keyup', e);
					if (keyupStop === false)
					{
						e.preventDefault();
						return false;
					}

					// replace to p before / after the table or body
					if (!this.opts.linebreaks && this.keyup.current.nodeType == 3 && this.keyup.current.length <= 1 && (this.keyup.parent === false || this.keyup.parent.tagName == 'BODY'))
					{
						this.keyup.replaceToParagraph();
					}

					// replace div after lists
					if (!this.opts.linebreaks && this.utils.isRedactorParent(this.keyup.current) && this.keyup.current.tagName === 'DIV')
					{
						this.keyup.replaceToParagraph(false);
					}


					if (!this.opts.linebreaks && $(this.keyup.parent).hasClass('redactor-invisible-space') && ($parent === false || $parent[0].tagName == 'BODY'))
					{
						$(this.keyup.parent).contents().unwrap();
						this.keyup.replaceToParagraph();
					}

					// linkify
					if (this.linkify.isEnabled() && this.linkify.isKey(key)) this.linkify.format();

					if (key === this.keyCode.DELETE || key === this.keyCode.BACKSPACE)
					{
						if (this.utils.browser('mozilla'))
						{
							var td = $(this.keydown.current).closest('td', this.$editor[0]);
							if (td.size() !== 0 && td.text() !== '')
							{
								e.preventDefault();
								return false;
							}
						}

						// clear unverified
						this.clean.clearUnverified();

						if (this.observe.image)
						{
							e.preventDefault();

							this.image.hideResize();

							this.buffer.set();
							this.image.remove(this.observe.image);
							this.observe.image = false;

							return false;
						}

						// remove empty paragraphs
						this.$editor.find('p').each($.proxy(function(i, s)
						{
							this.utils.removeEmpty(i, $(s).html());
						}, this));

						// remove invisible space
						if (this.opts.linebreaks && this.keyup.current && this.keyup.current.tagName == 'DIV' && this.utils.isEmpty(this.keyup.current.innerHTML))
						{
							$(this.keyup.current).after(this.selection.getMarkerAsHtml());
							this.selection.restore();
							$(this.keyup.current).remove();
						}

						// if empty
						return this.keyup.formatEmpty(e);
					}
				},
				replaceToParagraph: function(clone)
				{
					var $current = $(this.keyup.current);

					var node;
					if (clone === false)
					{
						node = $('<p>').append($current.html());
					}
					else
					{
						node = $('<p>').append($current.clone());
					}

					$current.replaceWith(node);
					var next = $(node).next();
					if (typeof(next[0]) !== 'undefined' && next[0].tagName == 'BR')
					{
						next.remove();
					}

					this.caret.setEnd(node);
				},
				formatEmpty: function(e)
				{
					var html = $.trim(this.$editor.html());

					if (!this.utils.isEmpty(html)) return;

					e.preventDefault();

					if (this.opts.linebreaks)
					{
						this.$editor.html(this.selection.getMarkerAsHtml());
						this.selection.restore();
					}
					else
					{
						this.$editor.html(this.opts.emptyHtml);
						this.focus.setStart();
					}

					this.code.sync();

					return false;
				}
			};
		},
		lang: function()
		{
			return {
				load: function()
				{
					this.opts.curLang = this.opts.langs[this.opts.lang];
				},
				get: function(name)
				{
					return (typeof this.opts.curLang[name] != 'undefined') ? this.opts.curLang[name] : '';
				}
			};
		},
		line: function()
		{
			return {
				insert: function()
				{
					this.buffer.set();

					var blocks = this.selection.getBlocks();
 					if (blocks[0] !== false && this.line.isExceptLastOrFirst(blocks))
	 				{
	 					if (!this.utils.browser('msie')) this.$editor.focus();
	 					return;
					}

					if (this.utils.browser('msie'))
					{
						this.line.insertInIe();
					}
					else
					{
						this.line.insertInOthersBrowsers();
					}
				},
				isExceptLastOrFirst: function(blocks)
				{
					var exceptTags = ['li', 'td', 'th', 'blockquote', 'figcaption', 'pre', 'dl', 'dt', 'dd'];

					var first = blocks[0].tagName.toLowerCase();
					var last = this.selection.getLastBlock();

					last = (typeof last == 'undefined') ? first : last.tagName.toLowerCase();

					var firstFound = $.inArray(first, exceptTags) != -1;
					var lastFound = $.inArray(last, exceptTags) != -1;

					if ((firstFound && lastFound) || firstFound)
					{
						return true;
					}
				},
				insertInIe: function()
				{
					this.utils.saveScroll();
					this.buffer.set();

					this.insert.node(document.createElement('hr'));

					this.utils.restoreScroll();
					this.code.sync();
				},
				insertInOthersBrowsers: function()
				{
					this.buffer.set();

					var extra = '<p id="redactor-insert-line"><br /></p>';
					if (this.opts.linebreaks) extra = '<br id="redactor-insert-line">';

					document.execCommand('insertHtml', false, '<hr>' + extra);

					this.line.setFocus();
					this.code.sync();
				},
				setFocus: function()
				{
					var node = this.$editor.find('#redactor-insert-line');
					var next = $(node).next()[0];
					var target = next;
					if (this.utils.browser('mozilla') && next && next.innerHTML === '')
					{
						target = $(next).next()[0];
						$(next).remove();
					}

					if (target)
					{
						node.remove();

						if (!this.opts.linebreaks)
						{
							this.$editor.focus();
							this.line.setStart(target);
						}

					}
					else
					{

						node.removeAttr('id');
						this.line.setStart(node[0]);
					}
				},
				setStart: function(node)
				{
					if (typeof node === 'undefined') return;

					var textNode = document.createTextNode('\u200B');

					this.selection.get();
					this.range.setStart(node, 0);
					this.range.insertNode(textNode);
					this.range.collapse(true);
					this.selection.addRange();

				}
			};
		},
		link: function()
		{
			return {
				show: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault) e.preventDefault();

					if (!this.observe.isCurrent('a'))
					{
						this.modal.load('link', this.lang.get('link_insert'), 600);
					}
					else
					{
						this.modal.load('link', this.lang.get('link_edit'), 600);
					}

					this.modal.createCancelButton();

					var buttonText = !this.observe.isCurrent('a') ? this.lang.get('insert') : this.lang.get('edit');

					this.link.buttonInsert = this.modal.createActionButton(buttonText);

					this.selection.get();

					this.link.getData();
					this.link.cleanUrl();

					if (this.link.target == '_blank') $('#redactor-link-blank').prop('checked', true);

					this.link.$inputUrl = $('#redactor-link-url');
					this.link.$inputText = $('#redactor-link-url-text');

					this.link.$inputText.val(this.link.text);
					this.link.$inputUrl.val(this.link.url);

					this.link.buttonInsert.on('click', $.proxy(this.link.insert, this));

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					// show modal
					this.selection.save();
					this.modal.show();
					this.link.$inputUrl.focus();
				},
				cleanUrl: function()
				{
					var thref = self.location.href.replace(/\/$/i, '');

					if (typeof this.link.url !== "undefined")
					{
						this.link.url = this.link.url.replace(thref, '');
						this.link.url = this.link.url.replace(/^\/#/, '#');
						this.link.url = this.link.url.replace('mailto:', '');

						// remove host from href
						if (!this.opts.linkProtocol)
						{
							var re = new RegExp('^(http|ftp|https)://' + self.location.host, 'i');
							this.link.url = this.link.url.replace(re, '');
						}
					}
				},
				getData: function()
				{
					this.link.$node = false;

					var $el = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
					if ($el.length !== 0 && $el[0].tagName === 'A')
					{
						this.link.$node = $el;

						this.link.url = $el.attr('href');
						this.link.text = $el.text();
						this.link.target = $el.attr('target');
					}
					else
					{
						this.link.text = this.sel.toString();
						this.link.url = '';
						this.link.target = '';
					}

				},
				insert: function()
				{
					this.placeholder.remove();

					var target = '';
					var link = this.link.$inputUrl.val();
					var text = this.link.$inputText.val();

					if ($.trim(link) === '')
					{
						this.link.$inputUrl.addClass('redactor-input-error').on('keyup', function()
						{
							$(this).removeClass('redactor-input-error');
							$(this).off('keyup');

						});

						return;
					}

					// mailto
					if (link.search('@') != -1 && /(http|ftp|https):\/\//i.test(link) === false)
					{
						link = 'mailto:' + link;
					}
					// url, not anchor
					else if (link.search('#') !== 0)
					{
						if ($('#redactor-link-blank').prop('checked'))
						{
							target = '_blank';
						}

						// test url (add protocol)
						var pattern = '((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}';
						var re = new RegExp('^(http|ftp|https)://' + pattern, 'i');
						var re2 = new RegExp('^' + pattern, 'i');
						var re3 = new RegExp('\.(html|php)$', 'i');
						if (link.search(re) == -1 && link.search(re3) == -1 && link.search(re2) === 0 && this.opts.linkProtocol)
						{
							link = this.opts.linkProtocol + '://' + link;
						}
					}

					this.link.set(text, link, target);
					this.modal.close();
				},
				set: function(text, link, target)
				{
					text = $.trim(text.replace(/<|>/g, ''));

					this.selection.restore();
					var blocks = this.selection.getBlocks();

					if (this.utils.browser('mozilla') && !this.focus.isFocused())
					{
						this.focus.setStart();
					}

					if (text === '' && link === '') return;
					if (text === '' && link !== '') text = link;

					if (this.link.$node)
					{
						this.buffer.set();

						var $link = this.link.$node,
							$el   = $link.children();

						if ($el.length > 0)
						{
							while ($el.length)
							{
								$el = $el.children();
							}

							$el = $el.end();
						}
						else
						{
							$el = $link;
						}

						$link.attr('href', link);
						$el.text(text);

						if (target !== '')
						{
							$link.attr('target', target);
						}
						else
						{
							$link.removeAttr('target');
						}

						this.selection.selectElement($link);

						this.code.sync();
					}
					else
					{
						if (this.utils.browser('mozilla') && this.link.text === '')
						{
							var $a = $('<a />').attr('href', link).text(text);
							if (target !== '') $a.attr('target', target);

							this.insert.node($a);
							this.selection.selectElement($a);
						}
						else
						{
							var $a;
							if (this.utils.browser('msie'))
							{
								$a = $('<a href="' + link + '">').text(text);
								if (target !== '') $a.attr('target', target);

								$a = $(this.insert.node($a));

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								this.selection.selectElement($a);
							}
							else
							{
								document.execCommand('createLink', false, link);

								$a = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
								if (this.utils.browser('mozilla'))
								{
									$a = $('a[_moz_dirty=""]');
								}

								if (target !== '') $a.attr('target', target);
								$a.removeAttr('style').removeAttr('_moz_dirty');

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								if (this.link.text !== '' || this.link.text != text)
								{
									if (!this.opts.linebreaks && blocks && blocks.length <= 1)
									{
										$a.text(text);
									}

									this.selection.selectElement($a);
								}
							}
						}

						this.code.sync();
						this.core.setCallback('insertedLink', $a);

					}

					// link tooltip
					setTimeout($.proxy(function()
					{
						this.observe.links();

					}, this), 5);
				},
				unlink: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault)
					{
						e.preventDefault();
					}

					var nodes = this.selection.getNodes();
					if (!nodes) return;

					this.buffer.set();

					var len = nodes.length;
					for (var i = 0; i < len; i++)
					{
						var $node = $(nodes[i]).closest('a', this.$editor[0]);
						$node.replaceWith($node.contents());
					}

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					this.code.sync();

				},
				toggleClass: function(className)
				{
					this.link.setClass(className, 'toggleClass');
				},
				addClass: function(className)
				{
					this.link.setClass(className, 'addClass');
				},
				removeClass: function(className)
				{
					this.link.setClass(className, 'removeClass');
				},
				setClass: function(className, func)
				{
					var links = this.selection.getInlinesTags(['a']);
					if (links === false) return;

					$.each(links, function()
					{
						$(this)[func](className);
					});
				}
			};
		},
		linkify: function()
		{
			return {
				isKey: function(key)
				{
					return key == this.keyCode.ENTER || key == this.keyCode.SPACE;
				},
				isEnabled: function()
				{
					return this.opts.convertLinks && (this.opts.convertUrlLinks || this.opts.convertImageLinks || this.opts.convertVideoLinks) && !this.utils.isCurrentOrParent('PRE');
				},
				format: function()
				{
					var linkify = this.linkify,
						opts    = this.opts;

					this.$editor
						.find(":not(iframe,img,a,pre)")
						.addBack()
						.contents()
						.filter(function()
						{
							return this.nodeType === 3 && $.trim(this.nodeValue) != "" && !$(this).parent().is("pre") && (this.nodeValue.match(opts.linkify.regexps.youtube) || this.nodeValue.match(opts.linkify.regexps.vimeo) || this.nodeValue.match(opts.linkify.regexps.image) || this.nodeValue.match(opts.linkify.regexps.url));
						})
						.each(function()
						{
							var text = $(this).text(),
								html = text;

							if (opts.convertVideoLinks && (html.match(opts.linkify.regexps.youtube) || html.match(opts.linkify.regexps.vimeo)) )
							{
								html = linkify.convertVideoLinks(html);
							}
							else if (opts.convertImageLinks && html.match(opts.linkify.regexps.image))
							{
								html = linkify.convertImages(html);
							}
							else if (opts.convertUrlLinks)
							{
								html = linkify.convertLinks(html);
							}

							$(this).before(text.replace(text, html))
								   .remove();
						});


					var objects = this.$editor.find('.redactor-linkify-object').each(function()
					{
						var $el = $(this);
						$el.removeClass('redactor-linkify-object');
						if ($el.attr('class') === '') $el.removeAttr('class');

						return $el[0];

					});

					// callback
					this.core.setCallback('linkify', objects);

					// sync
					this.code.sync();
				},
				convertVideoLinks: function(html)
				{
					var iframeStart = '<iframe class="redactor-linkify-object" width="500" height="281" src="',
						iframeEnd = '" frameborder="0" allowfullscreen></iframe>';

					if (html.match(this.opts.linkify.regexps.youtube))
					{
						html = html.replace(this.opts.linkify.regexps.youtube, iframeStart + '//www.youtube.com/embed/$1' + iframeEnd);
					}

					if (html.match(this.opts.linkify.regexps.vimeo))
					{
						html = html.replace(this.opts.linkify.regexps.vimeo, iframeStart + '//player.vimeo.com/video/$2' + iframeEnd);
					}

					return html;
				},
				convertImages: function(html)
				{
					var matches = html.match(this.opts.linkify.regexps.image);

					if (matches)
					{
						html = html.replace(html, '<img src="' + matches + '" class="redactor-linkify-object" />');

						if (this.opts.linebreaks)
						{
							if (!this.utils.isEmpty(this.code.get()))
							{
								html = '<br>' + html;
							}
						}

						html += '<br>';
					}

					return html;
				},
				convertLinks: function(html)
				{
					var matches = html.match(this.opts.linkify.regexps.url);

					if (matches)
					{
						matches = $.grep(matches, function(v, k) { return $.inArray(v, matches) === k; });

						var length = matches.length;

						for (var i = 0; i < length; i++)
						{
							var href = matches[i],
								text = href,
								linkProtocol = this.opts.linkProtocol + '://';

							if (href.match(/(https?|ftp):\/\//i) !== null)
							{
								linkProtocol = "";
							}

							if (text.length > this.opts.linkSize)
							{
								text = text.substring(0, this.opts.linkSize) + '...';
							}

							text = decodeURIComponent(text);

							var regexB = "\\b";

							if ($.inArray(href.slice(-1), ["/", "&", "="]) != -1)
							{
								regexB = "";
							}

							// escaping url
							var regexp = new RegExp('(' + href.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + regexB + ')', 'g');

							html = html.replace(regexp, '<a href="' + linkProtocol + $.trim(href) + '" class="redactor-linkify-object">' + $.trim(text) + '</a>');
						}
					}

					return html;
				}
			};
		},
		list: function()
		{
			return {
				toggle: function(cmd)
				{
					this.placeholder.remove();
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					var parent = this.selection.getParent();
					var $list = $(parent).closest('ol, ul', this.$editor[0]);

					if (!this.utils.isRedactorParent($list) && $list.length !== 0)
					{
						$list = false;
					}

					var isUnorderedCmdOrdered, isOrderedCmdUnordered;
					var remove = false;
					if ($list && $list.length)
					{
						remove = true;
						var listTag = $list[0].tagName;

						isUnorderedCmdOrdered = (cmd === 'orderedlist' && listTag === 'UL');
						isOrderedCmdUnordered = (cmd === 'unorderedlist' && listTag === 'OL');
					}

					if (isUnorderedCmdOrdered)
					{
						this.utils.replaceToTag($list, 'ol');
					}
					else if (isOrderedCmdUnordered)
					{
						this.utils.replaceToTag($list, 'ul');
					}
					else
					{
						if (remove)
						{
							this.list.remove(cmd);
						}
						else
						{
							this.list.insert(cmd);
						}
					}

					this.selection.restore();
					this.code.sync();
				},
				insert: function(cmd)
				{
					var parent = this.selection.getParent();
					var current = this.selection.getCurrent();
					var $td = $(current).closest('td, th', this.$editor[0]);

					if (this.utils.browser('msie') && this.opts.linebreaks)
					{
						this.list.insertInIe(cmd);
					}
					else
					{
						document.execCommand('insert' + cmd);
					}

					var parent = this.selection.getParent();
					var $list = $(parent).closest('ol, ul', this.$editor[0]);
					if ($td.length !== 0)
					{
						var prev = $td.prev();
						var html = $td.html();
						$td.html('');
						if (prev && prev.length === 1 && (prev[0].tagName === 'TD' || prev[0].tagName === 'TH'))
						{
							$(prev).after($td);
						}
						else
						{
							$(parent).prepend($td);
						}

						$td.html(html);
					}

					if (this.utils.isEmpty($list.find('li').text()))
					{
						var $children = $list.children('li');
						$children.find('br').remove();
						$children.append(this.selection.getMarkerAsHtml());

						if (this.opts.linebreaks && this.utils.browser('mozilla') && $children.size() == 2 && this.utils.isEmpty($children.eq(1).text()))
						{
							$children.eq(1).remove();
						}
					}

					if ($list.length)
					{
						// remove block-element list wrapper
						var $listParent = $list.parent();
						if (this.utils.isRedactorParent($listParent) && $listParent[0].tagName != 'LI' && this.utils.isBlock($listParent[0]))
						{
							$listParent.replaceWith($listParent.contents());
						}
					}

					if (!this.utils.browser('msie'))
					{
						this.$editor.focus();
					}

					this.clean.clearUnverified();
				},
				insertInIe: function(cmd)
				{
					var wrapper = this.selection.wrap('div');
					var wrapperHtml = $(wrapper).html();

					var tmpList = (cmd == 'orderedlist') ? $('<ol>') : $('<ul>');
					var tmpLi = $('<li>');

					if ($.trim(wrapperHtml) === '')
					{
						tmpLi.append(this.selection.getMarkerAsHtml());
						tmpList.append(tmpLi);
						this.$editor.find('#selection-marker-1').replaceWith(tmpList);
					}
					else
					{
						var items = wrapperHtml.split(/<br\s?\/?>/gi);
						if (items)
						{
							for (var i = 0; i < items.length; i++)
							{
								if ($.trim(items[i]) !== '')
								{
									tmpList.append($('<li>').html(items[i]));
								}
							}
						}
						else
						{
							tmpLi.append(wrapperHtml);
							tmpList.append(tmpLi);
						}

						$(wrapper).replaceWith(tmpList);
					}
				},
				remove: function(cmd)
				{
					if ($.inArray('ul', this.selection.getBlocks())) cmd = 'unorderedlist';


					document.execCommand('insert' + cmd);

					var $current = $(this.selection.getCurrent());

					this.indent.fixEmptyIndent();

					if (!this.opts.linebreaks && $current.closest('li, th, td', this.$editor[0]).length === 0)
					{
						document.execCommand('formatblock', false, 'p');
						this.$editor.find('ul, ol, blockquote').each($.proxy(this.utils.removeEmpty, this));
					}

					var $table = $(this.selection.getCurrent()).closest('table', this.$editor[0]);
					var $prev = $table.prev();
					if (!this.opts.linebreaks && $table.length !== 0 && $prev.length !== 0 && $prev[0].tagName == 'BR')
					{
						$prev.remove();
					}

					this.clean.clearUnverified();

				}
			};
		},
		modal: function()
		{
			return {
				callbacks: {},
				loadTemplates: function()
				{
					this.opts.modal = {
						imageEdit: String()
						+ '<section id="redactor-modal-image-edit">'
							+ '<label>' + this.lang.get('title') + '</label>'
							+ '<input type="text" id="redactor-image-title" />'
							+ '<label class="redactor-image-link-option">' + this.lang.get('link') + '</label>'
							+ '<input type="text" id="redactor-image-link" class="redactor-image-link-option" aria-label="' + this.lang.get('link') + '" />'
							+ '<label class="redactor-image-link-option"><input type="checkbox" id="redactor-image-link-blank" aria-label="' + this.lang.get('link_new_tab') + '"> ' + this.lang.get('link_new_tab') + '</label>'
							+ '<label class="redactor-image-position-option">' + this.lang.get('image_position') + '</label>'
							+ '<select class="redactor-image-position-option" id="redactor-image-align" aria-label="' + this.lang.get('image_position') + '">'
								+ '<option value="none">' + this.lang.get('none') + '</option>'
								+ '<option value="left">' + this.lang.get('left') + '</option>'
								+ '<option value="center">' + this.lang.get('center') + '</option>'
								+ '<option value="right">' + this.lang.get('right') + '</option>'
							+ '</select>'
						+ '</section>',

						image: String()
						+ '<section id="redactor-modal-image-insert">'
							+ '<div id="redactor-modal-image-droparea"></div>'
 						+ '</section>',

						file: String()
						+ '<section id="redactor-modal-file-insert">'
							+ '<div id="redactor-modal-file-upload-box">'
								+ '<label>' + this.lang.get('filename') + '</label>'
								+ '<input type="text" id="redactor-filename" aria-label="' + this.lang.get('filename') + '" /><br><br>'
								+ '<div id="redactor-modal-file-upload"></div>'
							+ '</div>'
						+ '</section>',

						link: String()
						+ '<section id="redactor-modal-link-insert">'
							+ '<label>URL</label>'
							+ '<input type="url" id="redactor-link-url" aria-label="URL" />'
							+ '<label>' + this.lang.get('text') + '</label>'
							+ '<input type="text" id="redactor-link-url-text" aria-label="' + this.lang.get('text') + '" />'
							+ '<label><input type="checkbox" id="redactor-link-blank"> ' + this.lang.get('link_new_tab') + '</label>'
						+ '</section>'
					};


					$.extend(this.opts, this.opts.modal);

				},
				addCallback: function(name, callback)
				{
					this.modal.callbacks[name] = callback;
				},
				createTabber: function($modal)
				{
					this.modal.$tabber = $('<div>').attr('id', 'redactor-modal-tabber');

					$modal.prepend(this.modal.$tabber);
				},
				addTab: function(id, name, active)
				{
					var $tab = $('<a href="#" rel="tab' + id + '">').text(name);
					if (active)
					{
						$tab.addClass('active');
					}

					var self = this;
					$tab.on('click', function(e)
					{
						e.preventDefault();
						$('.redactor-tab').hide();
						$('.redactor-' + $(this).attr('rel')).show();

						self.modal.$tabber.find('a').removeClass('active');
						$(this).addClass('active');

					});

					this.modal.$tabber.append($tab);
				},
				addTemplate: function(name, template)
				{
					this.opts.modal[name] = template;
				},
				getTemplate: function(name)
				{
					return this.opts.modal[name];
				},
				getModal: function()
				{
					return this.$modalBody.find('section');
				},
				load: function(templateName, title, width)
				{
					this.modal.templateName = templateName;
					this.modal.width = width;

					this.modal.build();
					this.modal.enableEvents();
					this.modal.setTitle(title);
					this.modal.setDraggable();
					this.modal.setContent();

					// callbacks
					if (typeof this.modal.callbacks[templateName] != 'undefined')
					{
						this.modal.callbacks[templateName].call(this);
					}

				},
				show: function()
				{
					this.utils.disableBodyScroll();

					if (this.utils.isMobile())
					{
						this.modal.showOnMobile();
					}
					else
					{
						this.modal.showOnDesktop();
					}

					if (this.opts.highContrast)
					{
						this.$modalBox.addClass("redactor-modal-contrast");
					}

					this.$modalOverlay.show();
					this.$modalBox.show();

					this.$modal.attr('tabindex', '-1');
					this.$modal.focus();

					this.modal.setButtonsWidth();

					this.utils.saveScroll();

					// resize
					if (!this.utils.isMobile())
					{
						setTimeout($.proxy(this.modal.showOnDesktop, this), 0);
						$(window).on('resize.redactor-modal', $.proxy(this.modal.resize, this));
					}

					// modal shown callback
					this.core.setCallback('modalOpened', this.modal.templateName, this.$modal);

					// fix bootstrap modal focus
					$(document).off('focusin.modal');

					// enter
					this.$modal.find('input[type=text],input[type=url],input[type=email]').on('keydown.redactor-modal', $.proxy(this.modal.setEnter, this));
				},
				showOnDesktop: function()
				{
					var height = this.$modal.outerHeight();
					var windowHeight = $(window).height();
					var windowWidth = $(window).width();

					if (this.modal.width > windowWidth)
					{
						this.$modal.css({
							width: '96%',
							marginTop: (windowHeight/2 - height/2) + 'px'
						});
						return;
					}

					if (height > windowHeight)
					{
						this.$modal.css({
							width: this.modal.width + 'px',
							marginTop: '20px'
						});
					}
					else
					{
						this.$modal.css({
							width: this.modal.width + 'px',
							marginTop: (windowHeight/2 - height/2) + 'px'
						});
					}
				},
				showOnMobile: function()
				{
					this.$modal.css({
						width: '96%',
						marginTop: '2%'
					});

				},
				resize: function()
				{
					if (this.utils.isMobile())
					{
						this.modal.showOnMobile();
					}
					else
					{
						this.modal.showOnDesktop();
					}
				},
				setTitle: function(title)
				{
					this.$modalHeader.html(title);
				},
				setContent: function()
				{
					this.$modalBody.html(this.modal.getTemplate(this.modal.templateName));
				},
				setDraggable: function()
				{
					if (typeof $.fn.draggable === 'undefined') return;

					this.$modal.draggable({ handle: this.$modalHeader });
					this.$modalHeader.css('cursor', 'move');
				},
				setEnter: function(e)
				{
					if (e.which != 13) return;

					e.preventDefault();
					this.$modal.find('button.redactor-modal-action-btn').click();
				},
				createCancelButton: function()
				{
					var button = $('<button>').addClass('redactor-modal-btn redactor-modal-close-btn').html(this.lang.get('cancel'));
					button.on('click', $.proxy(this.modal.close, this));

					this.$modalFooter.append(button);
				},
				createDeleteButton: function(label)
				{
					return this.modal.createButton(label, 'delete');
				},
				createActionButton: function(label)
				{
					return this.modal.createButton(label, 'action');
				},
				createButton: function(label, className)
				{
					var button = $('<button>').addClass('redactor-modal-btn').addClass('redactor-modal-' + className + '-btn').html(label);
					this.$modalFooter.append(button);

					return button;
				},
				setButtonsWidth: function()
				{
					var buttons = this.$modalFooter.find('button');
					var buttonsSize = buttons.length;
					if (buttonsSize === 0) return;

					buttons.css('width', (100/buttonsSize) + '%');
				},
				build: function()
				{
					this.modal.buildOverlay();

					this.$modalBox = $('<div id="redactor-modal-box"/>').hide();
					this.$modal = $('<div id="redactor-modal" role="dialog" aria-labelledby="redactor-modal-header" />');
					this.$modalHeader = $('<header id="redactor-modal-header"/>');
					this.$modalClose = $('<button type="button" id="redactor-modal-close" tabindex="1" aria-label="Close" />').html('&times;');
					this.$modalBody = $('<div id="redactor-modal-body" />');
					this.$modalFooter = $('<footer />');

					this.$modal.append(this.$modalHeader);
					this.$modal.append(this.$modalClose);
					this.$modal.append(this.$modalBody);
					this.$modal.append(this.$modalFooter);
					this.$modalBox.append(this.$modal);
					this.$modalBox.appendTo(document.body);
				},
				buildOverlay: function()
				{
					this.$modalOverlay = $('<div id="redactor-modal-overlay">').hide();
					$('body').prepend(this.$modalOverlay);
				},
				enableEvents: function()
				{
					this.$modalClose.on('click.redactor-modal', $.proxy(this.modal.close, this));
					$(document).on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
					this.$editor.on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
					this.$modalBox.on('click.redactor-modal', $.proxy(this.modal.close, this));
				},
				disableEvents: function()
				{
					this.$modalClose.off('click.redactor-modal');
					$(document).off('keyup.redactor-modal');
					this.$editor.off('keyup.redactor-modal');
					this.$modalBox.off('click.redactor-modal');
					$(window).off('resize.redactor-modal');
				},
				closeHandler: function(e)
				{
					if (e.which != this.keyCode.ESC) return;

					this.modal.close(false);
				},
				close: function(e)
				{
					if (e)
					{
						if (!$(e.target).hasClass('redactor-modal-close-btn') && e.target != this.$modalClose[0] && e.target != this.$modalBox[0])
						{
							return;
						}

						e.preventDefault();
					}

					if (!this.$modalBox) return;

					this.modal.disableEvents();
					this.utils.enableBodyScroll();

					this.$modalOverlay.remove();

					this.$modalBox.fadeOut('fast', $.proxy(function()
					{
						this.$modalBox.remove();

						setTimeout($.proxy(this.utils.restoreScroll, this), 0);

						if (e !== undefined) this.selection.restore();

						$(document.body).css('overflow', this.modal.bodyOveflow);
						this.core.setCallback('modalClosed', this.modal.templateName);

					}, this));

				}
			};
		},
		observe: function()
		{
			return {
				load: function()
				{
					if (typeof this.opts.destroyed != "undefined") return;

					this.observe.images();
					this.observe.links();
				},
				toolbar: function(e, btnName)
				{
					this.observe.buttons(e, btnName);
					this.observe.dropdowns();
				},
				isCurrent: function($el, $current)
				{
					if (typeof $current == 'undefined')
					{
						var $current = $(this.selection.getCurrent());
					}

					return $current.is($el) || $current.parents($el).length > 0;
				},
				dropdowns: function()
				{
					var $current = $(this.selection.getCurrent());

					$.each(this.opts.observe.dropdowns, $.proxy(function(key, value)
					{
						var observe = value.observe,
							element = observe.element,
							$item   = value.item,
							inValues = typeof observe['in'] != 'undefined' ? observe['in'] : false,
							outValues = typeof observe['out'] != 'undefined' ? observe['out'] : false;

						if ($current.closest(element).size() > 0)
						{
							this.observe.setDropdownProperties($item, inValues, outValues);
						}
						else
						{
							this.observe.setDropdownProperties($item, outValues, inValues);
						}
					}, this));
				},
				setDropdownProperties: function($item, addProperties, deleteProperties)
				{
					if (deleteProperties && typeof deleteProperties['attr'] != 'undefined')
					{
						this.observe.setDropdownAttr($item, deleteProperties.attr, true);
					}

					if (typeof addProperties['attr'] != 'undefined')
					{
						this.observe.setDropdownAttr($item, addProperties.attr);
					}

					if (typeof addProperties['title'] != 'undefined')
					{
						$item.text(addProperties['title']);
					}
				},
				setDropdownAttr: function($item, properties, isDelete)
				{
					$.each(properties, function(key, value)
					{
						if (key == 'class')
						{
							if (!isDelete)
							{
								$item.addClass(value);
							}
							else
							{
								$item.removeClass(value);
							}
						}
						else
						{
							if (!isDelete)
							{
								$item.attr(key, value);
							}
							else
							{
								$item.removeAttr(key);
							}
						}
					});
				},
				addDropdown: function($item, btnName, btnObject)
				{
					if (typeof btnObject.observe == "undefined") return;

					btnObject.item = $item;

					this.opts.observe.dropdowns.push(btnObject);
				},
				buttons: function(e, btnName)
				{
					var current = this.selection.getCurrent();
					var parent = this.selection.getParent();

					if (e !== false)
					{
						this.button.setInactiveAll();
					}
					else
					{
						this.button.setInactiveAll(btnName);
					}

					if (e === false && btnName !== 'html')
					{
						if ($.inArray(btnName, this.opts.activeButtons) != -1) this.button.toggleActive(btnName);
						return;
					}

					//var linkButtonName = (this.utils.isCurrentOrParent('A')) ? this.lang.get('link_edit') : this.lang.get('link_insert');
					//$('body').find('a.redactor-dropdown-link').text(linkButtonName);

					$.each(this.opts.activeButtonsStates, $.proxy(function(key, value)
					{
						var parentEl = $(parent).closest(key, this.$editor[0]);
						var currentEl = $(current).closest(key, this.$editor[0]);

						if (parentEl.length !== 0 && !this.utils.isRedactorParent(parentEl)) return;
						if (!this.utils.isRedactorParent(currentEl)) return;
						if (parentEl.length !== 0 || currentEl.closest(key, this.$editor[0]).length !== 0)
						{
							this.button.setActive(value);
						}

					}, this));

					var $parent = $(parent).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
					if (this.utils.isRedactorParent(parent) && $parent.length)
					{
						var align = ($parent.css('text-align') === '') ? 'left' : $parent.css('text-align');
						this.button.setActive('align' + align);
					}
				},
				addButton: function(tag, btnName)
				{
					this.opts.activeButtons.push(btnName);
					this.opts.activeButtonsStates[tag] = btnName;
				},
				images: function()
				{
					this.$editor.find('img').each($.proxy(function(i, img)
					{
						var $img = $(img);

						// IE fix (when we clicked on an image and then press backspace IE does goes to image's url)
						$img.closest('a', this.$editor[0]).on('click', function(e) { e.preventDefault(); });

						if (this.utils.browser('msie')) $img.attr('unselectable', 'on');

						this.image.setEditable($img);

					}, this));

					$(document).on('click.redactor-image-delete.' + this.uuid, $.proxy(function(e)
					{
						this.observe.image = false;
						if (e.target.tagName == 'IMG' && this.utils.isRedactorParent(e.target))
						{
							this.observe.image = (this.observe.image && this.observe.image == e.target) ? false : e.target;
						}

					}, this));

				},
				links: function()
				{
					if (!this.opts.linkTooltip) return;

					this.$editor.find('a').on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.showTooltip, this));
					this.$editor.on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.closeTooltip, this));
					$(document).on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.closeTooltip, this));
				},
				getTooltipPosition: function($link)
				{
					return $link.offset();
				},
				showTooltip: function(e)
				{
					var $el = $(e.target);

					if ($el[0].tagName == 'IMG')
						return;

					if ($el[0].tagName !== 'A')
						$el = $el.closest('a', this.$editor[0]);

					if ($el[0].tagName !== 'A')
						return;

					var $link = $el;

					var pos = this.observe.getTooltipPosition($link);
					var tooltip = $('<span class="redactor-link-tooltip"></span>');

					var href = $link.attr('href');
					if (href === undefined)
					{
						href = '';
					}

					if (href.length > 24) href = href.substring(0, 24) + '...';
					var aLink = $('<a href="' + $link.attr('href') + '" target="_blank" />').html(href).addClass('redactor-link-tooltip-action');
					if ($link.attr('href').contains('DAMDownload.aspx')) {
					    aLink = $('<a href="' + $link.attr('href') + '" target="_blank" />').html('Download').addClass('redactor-link-tooltip-action');
					}
					
					var aEdit = $('<a href="#" />').html(this.lang.get('edit')).on('click', $.proxy(this.link.show, this)).addClass('redactor-link-tooltip-action');
					var aUnlink = $('<a href="#" />').html(this.lang.get('unlink')).on('click', $.proxy(this.link.unlink, this)).addClass('redactor-link-tooltip-action');

					tooltip.append(aLink).append(' | ').append(aEdit).append(' | ').append(aUnlink);
					tooltip.css({
						top: (pos.top + parseInt($link.css('line-height'), 10)) + 'px',
						left: pos.left + 'px'
					});

					$('.redactor-link-tooltip').remove();
					$('body').append(tooltip);
				},
				closeTooltip: function(e)
				{
					e = e.originalEvent || e;

					var target = e.target;
					var $parent = $(target).closest('a', this.$editor[0]);
					if ($parent.length !== 0 && $parent[0].tagName === 'A' && target.tagName !== 'A')
					{
						return;
					}
					else if ((target.tagName === 'A' && this.utils.isRedactorParent(target)) || $(target).hasClass('redactor-link-tooltip-action'))
					{
						return;
					}

					$('.redactor-link-tooltip').remove();
				}

			};
		},
		paragraphize: function()
		{
			return {
				load: function(html)
				{
					if (this.opts.linebreaks) return html;
					if (html === '' || html === '<p></p>') return this.opts.emptyHtml;

					html = html + "\n";

					this.paragraphize.safes = [];
					this.paragraphize.z = 0;

					html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, '</blockquote>');

					html = this.paragraphize.getSafes(html);
					html = this.paragraphize.getSafesComments(html);
					html = this.paragraphize.replaceBreaksToNewLines(html);
					html = this.paragraphize.replaceBreaksToParagraphs(html);
					html = this.paragraphize.clear(html);
					html = this.paragraphize.restoreSafes(html);

					html = html.replace(new RegExp('<br\\s?/?>\n?<(' + this.opts.paragraphizeBlocks.join('|') + ')(.*?[^>])>', 'gi'), '<p><br /></p>\n<$1$2>');

					return $.trim(html);
				},
				getSafes: function(html)
				{
					var $div = $('<div />').append(html);

					// remove paragraphs in blockquotes
					$div.find('blockquote p').replaceWith(function()
					{
						return $(this).append('<br />').contents();
					});

					html = $div.html();

					$div.find(this.opts.paragraphizeBlocks.join(', ')).each($.proxy(function(i,s)
					{
						this.paragraphize.z++;
						this.paragraphize.safes[this.paragraphize.z] = s.outerHTML;
						html = html.replace(s.outerHTML, '\n{replace' + this.paragraphize.z + '}');

					}, this));

					return html;
				},
				getSafesComments: function(html)
				{
					var commentsMatches = html.match(/<!--([\w\W]*?)-->/gi);

					if (!commentsMatches) return html;

					$.each(commentsMatches, $.proxy(function(i,s)
					{
						this.paragraphize.z++;
						this.paragraphize.safes[this.paragraphize.z] = s;
						html = html.replace(s, '\n{replace' + this.paragraphize.z + '}');
					}, this));

					return html;
				},
				restoreSafes: function(html)
				{
					$.each(this.paragraphize.safes, function(i,s)
					{
						s = (typeof s !== 'undefined') ? s.replace(/\$/g, '&#36;') : s;
						html = html.replace('{replace' + i + '}', s);

					});

					return html;
				},
				replaceBreaksToParagraphs: function(html)
				{
					var htmls = html.split(new RegExp('\n', 'g'), -1);

					html = '';
					if (htmls)
					{
						var len = htmls.length;
						for (var i = 0; i < len; i++)
						{
							if (!htmls.hasOwnProperty(i)) return;

							if (htmls[i].search('{replace') == -1)
							{
								htmls[i] = htmls[i].replace(/<p>\n\t?<\/p>/gi, '');
								htmls[i] = htmls[i].replace(/<p><\/p>/gi, '');

								if (htmls[i] !== '')
								{
									html += '<p>' +  htmls[i].replace(/^\n+|\n+$/g, "") + "</p>";
								}
							}
							else html += htmls[i];
						}
					}

					return html;
				},
				replaceBreaksToNewLines: function(html)
				{
					html = html.replace(/<br \/>\s*<br \/>/gi, "\n\n");
					html = html.replace(/<br\s?\/?>\n?<br\s?\/?>/gi, "\n<br /><br />");

					html = html.replace(new RegExp("\r\n", 'g'), "\n");
					html = html.replace(new RegExp("\r", 'g'), "\n");
					html = html.replace(new RegExp("/\n\n+/"), 'g', "\n\n");

					return html;
				},
				clear: function(html)
				{
					html = html.replace(new RegExp('</blockquote></p>', 'gi'), '</blockquote>');
					html = html.replace(new RegExp('<p></blockquote>', 'gi'), '</blockquote>');
					html = html.replace(new RegExp('<p><blockquote>', 'gi'), '<blockquote>');
					html = html.replace(new RegExp('<blockquote></p>', 'gi'), '<blockquote>');

					html = html.replace(new RegExp('<p><p ', 'gi'), '<p ');
					html = html.replace(new RegExp('<p><p>', 'gi'), '<p>');
					html = html.replace(new RegExp('</p></p>', 'gi'), '</p>');
					html = html.replace(new RegExp('<p>\\s?</p>', 'gi'), '');
					html = html.replace(new RegExp("\n</p>", 'gi'), '</p>');
					html = html.replace(new RegExp('<p>\t?\t?\n?<p>', 'gi'), '<p>');
					html = html.replace(new RegExp('<p>\t*</p>', 'gi'), '');

					return html;
				}
			};
		},
		paste: function()
		{
			return {
				init: function(e)
				{
					if (!this.opts.cleanOnPaste)
					{
						setTimeout($.proxy(this.code.sync, this), 1);
						return;
					}

					this.rtePaste = true;

					this.buffer.set();
					this.selection.save();
					this.utils.saveScroll();

					this.paste.createPasteBox();

					$(window).on('scroll.redactor-freeze', $.proxy(function()
					{
						$(window).scrollTop(this.saveBodyScroll);

					}, this));

					setTimeout($.proxy(function()
					{
						var html = this.$pasteBox.html();

						this.$pasteBox.remove();

						this.selection.restore();
						this.utils.restoreScroll();

						this.paste.insert(html);

						$(window).off('scroll.redactor-freeze');

						if (this.linkify.isEnabled())
							this.linkify.format();

					}, this), 1);
				},
				createPasteBox: function()
				{
					this.$pasteBox = $('<div>').html('').attr('contenteditable', 'true').css({ position: 'fixed', width: 0, top: 0, left: '-9999px' });

					if (this.utils.browser('msie'))
					{
						this.$box.append(this.$pasteBox);
					}
					else
					{
						// bootstrap modal
						if ($('.modal-body').length > 0)
						{
							$('.modal-body').append(this.$pasteBox);
						}
						else
						{
							$('body').append(this.$pasteBox);
						}

					}

					this.$pasteBox.focus();
				},
				insert: function(html)
				{
					html = this.core.setCallback('pasteBefore', html);

					// clean
					html = (this.utils.isSelectAll()) ? this.clean.onPaste(html, false) : this.clean.onPaste(html);

					html = this.core.setCallback('paste', html);

					if (this.utils.isSelectAll())
					{
						this.insert.set(html, false);
					}
					else
					{
						this.insert.html(html, false);
					}

					this.utils.disableSelectAll();
					this.rtePaste = false;

					setTimeout($.proxy(this.clean.clearUnverified, this), 10);

					// clean empty spans
					setTimeout($.proxy(function()
					{
						var spans = this.$editor.find('span');
						$.each(spans, function(i,s)
						{
							var html = s.innerHTML.replace(/\u200B/, '');
							if (html === '' && s.attributes.length === 0) $(s).remove();

						});

					}, this), 10);
				}
			};
		},
		placeholder: function()
		{
			return {
				enable: function()
				{
					if (!this.placeholder.is()) return;

					this.$editor.attr('placeholder', this.$element.attr('placeholder'));

					this.placeholder.toggle();
					this.$editor.on('keydown.redactor-placeholder', $.proxy(this.placeholder.toggle, this));
				},
				toggle: function()
				{
					setTimeout($.proxy(function()
					{
						var func = this.utils.isEmpty(this.$editor.html(), false) ? 'addClass' : 'removeClass';
						this.$editor[func]('redactor-placeholder');

					}, this), 5);
				},
				remove: function()
				{
					this.$editor.removeClass('redactor-placeholder');
				},
				is: function()
				{
					if (this.opts.placeholder)
					{
						return this.$element.attr('placeholder', this.opts.placeholder);
					}
					else
					{
						return !(typeof this.$element.attr('placeholder') == 'undefined' || this.$element.attr('placeholder') === '');
					}
				}
			};
		},
		progress: function()
		{
			return {
				show: function()
				{
					$(document.body).append($('<div id="redactor-progress"><span></span></div>'));
					$('#redactor-progress').fadeIn();
				},
				hide: function()
				{
					$('#redactor-progress').fadeOut(1500, function()
					{
						$(this).remove();
					});
				}

			};
		},
		selection: function()
		{
			return {
				get: function()
				{
					this.sel = document.getSelection();

					if (document.getSelection && this.sel.getRangeAt && this.sel.rangeCount)
					{
						this.range = this.sel.getRangeAt(0);
					}
					else
					{
						this.range = document.createRange();
					}
				},
				addRange: function()
				{
					try {
						this.sel.removeAllRanges();
					} catch (e) {}

					this.sel.addRange(this.range);
				},
				getCurrent: function()
				{
					var el = false;

					this.selection.get();

					if (this.sel && this.sel.rangeCount > 0)
					{
						el = this.sel.getRangeAt(0).startContainer;
					}

					return this.utils.isRedactorParent(el);
				},
				getParent: function(elem)
				{
					elem = elem || this.selection.getCurrent();
					if (elem)
					{
						return this.utils.isRedactorParent($(elem).parent()[0]);
					}

					return false;
				},
				getPrev: function()
				{
					return  window.getSelection().anchorNode.previousSibling;
				},
				getNext: function()
				{
					return window.getSelection().anchorNode.nextSibling;
				},
				getBlock: function(node)
				{
					node = node || this.selection.getCurrent();

					while (node)
					{
						if (this.utils.isBlockTag(node.tagName))
						{
							return ($(node).hasClass('redactor-editor')) ? false : node;
						}

						node = node.parentNode;
					}

					return false;
				},
				getInlines: function(nodes, tags)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return false;
					}

					var inlines = [];
					nodes = (typeof nodes == 'undefined' || nodes === false) ? this.selection.getNodes() : nodes;
					var inlineTags = this.opts.inlineTags;
					inlineTags.push('span');

					if (typeof tags !== 'undefined')
					{
						for (var i = 0; i < tags.length; i++)
						{
							inlineTags.push(tags[i]);
						}
					}

					$.each(nodes, $.proxy(function(i,node)
					{
						if ($.inArray(node.tagName.toLowerCase(), inlineTags) != -1)
						{
							inlines.push(node);
						}

					}, this));

					return (inlines.length === 0) ? false : inlines;
				},
				getInlinesTags: function(tags)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return false;
					}

					var inlines = [];
					var nodes =  this.selection.getNodes();
					$.each(nodes, $.proxy(function(i,node)
					{
						if ($.inArray(node.tagName.toLowerCase(), tags) != -1)
						{
							inlines.push(node);
						}

					}, this));

					return (inlines.length === 0) ? false : inlines;
				},
				getBlocks: function(nodes)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return [this.selection.getBlock()];
					}

					var blocks = [];
					nodes = (typeof nodes == 'undefined') ? this.selection.getNodes() : nodes;
					$.each(nodes, $.proxy(function(i,node)
					{
						if (this.utils.isBlock(node))
						{
							this.selection.lastBlock = node;
							blocks.push(node);
						}

					}, this));

					return (blocks.length === 0) ? [this.selection.getBlock()] : blocks;
				},
				getLastBlock: function()
				{
					return this.selection.lastBlock;
				},
				getNodes: function()
				{
					this.selection.get();

					var startNode = this.selection.getNodesMarker(1);
					var endNode = this.selection.getNodesMarker(2);

					if (this.range.collapsed === false)
					{
					   if (window.getSelection) {
					        var sel = window.getSelection();
					        if (sel.rangeCount > 0) {

					            var range = sel.getRangeAt(0);
					            var startPointNode = range.startContainer, startOffset = range.startOffset;

					            var boundaryRange = range.cloneRange();
					            boundaryRange.collapse(false);
					            boundaryRange.insertNode(endNode);
					            boundaryRange.setStart(startPointNode, startOffset);
					            boundaryRange.collapse(true);
					            boundaryRange.insertNode(startNode);

					            // Reselect the original text
					            range.setStartAfter(startNode);
					            range.setEndBefore(endNode);
					            sel.removeAllRanges();
					            sel.addRange(range);
					        }
					    }
					}
					else
					{
						this.selection.setNodesMarker(this.range, startNode, true);
						endNode = startNode;
					}

					var nodes = [];
					var counter = 0;

					var self = this;
					this.$editor.find('*').each(function()
					{
						if (this == startNode)
						{
							var parent = $(this).parent();
							if (parent.length !== 0 && parent[0].tagName != 'BODY' && self.utils.isRedactorParent(parent[0]))
							{
								nodes.push(parent[0]);
							}

							nodes.push(this);
							counter = 1;
						}
						else
						{
							if (counter > 0)
							{
								nodes.push(this);
								counter = counter + 1;
							}
						}

						if (this == endNode)
						{
							return false;
						}

					});

					var finalNodes = [];
					var len = nodes.length;
					for (var i = 0; i < len; i++)
					{
						if (nodes[i].id != 'nodes-marker-1' && nodes[i].id != 'nodes-marker-2')
						{
							finalNodes.push(nodes[i]);
						}
					}

					this.selection.removeNodesMarkers();

					return finalNodes;

				},
				getNodesMarker: function(num)
				{
					return $('<span id="nodes-marker-' + num + '" class="redactor-nodes-marker" data-verified="redactor">' + this.opts.invisibleSpace + '</span>')[0];
				},
				setNodesMarker: function(range, node, type)
				{
					var range = range.cloneRange();

					try {
						range.collapse(type);
						range.insertNode(node);
					}
					catch (e) {}
				},
				removeNodesMarkers: function()
				{
					$(document).find('span.redactor-nodes-marker').remove();
					this.$editor.find('span.redactor-nodes-marker').remove();
				},
				fromPoint: function(start, end)
				{
					this.caret.setOffset(start, end);
				},
				wrap: function(tag)
				{
					this.selection.get();

					if (this.range.collapsed) return false;

					var wrapper = document.createElement(tag);
					wrapper.appendChild(this.range.extractContents());
					this.range.insertNode(wrapper);

					return wrapper;
				},
				selectElement: function(node)
				{
					this.caret.set(node, 0, node, 1);
				},
				selectAll: function()
				{
					this.selection.get();
					this.range.selectNodeContents(this.$editor[0]);
					this.selection.addRange();
				},
				remove: function()
				{
					this.selection.get();
					this.sel.removeAllRanges();
				},
				save: function()
				{
					this.selection.createMarkers();
				},
				createMarkers: function()
				{
					this.selection.get();


					var node1 = this.selection.getMarker(1);

					this.selection.setMarker(this.range, node1, true);
					if (this.range.collapsed === false)
					{
						var node2 = this.selection.getMarker(2);
						this.selection.setMarker(this.range, node2, false);
					}

					this.savedSel = this.$editor.html();
				},
				getMarker: function(num)
				{
					if (typeof num == 'undefined') num = 1;

					return $('<span id="selection-marker-' + num + '" class="redactor-selection-marker"  data-verified="redactor">' + this.opts.invisibleSpace + '</span>')[0];
				},
				getMarkerAsHtml: function(num)
				{
					return this.utils.getOuterHtml(this.selection.getMarker(num));
				},
				setMarker: function(range, node, type)
				{
					range = range.cloneRange();

					try {
						range.collapse(type);
						range.insertNode(node);

					}
					catch (e)
					{
						this.focus.setStart();
					}

				},
				restore: function()
				{
					var node1 = this.$editor.find('span#selection-marker-1');
					var node2 = this.$editor.find('span#selection-marker-2');

					if (node1.length !== 0 && node2.length !== 0)
					{
						this.caret.set(node1, 0, node2, 0);
					}
					else if (node1.length !== 0)
					{
						this.caret.set(node1, 0, node1, 0);
					}
					else
					{
						this.$editor.focus();
					}

					this.selection.removeMarkers();
					this.savedSel = false;

				},
				removeMarkers: function()
				{
					this.$editor.find('span.redactor-selection-marker').each(function(i,s)
					{
						var text = $(s).text().replace(/\u200B/g, '');
						if (text === '') $(s).remove();
						else $(s).replaceWith(function() { return $(this).contents(); });
					});
				},
				getText: function()
				{
					this.selection.get();

					return this.sel.toString();
				},
				getHtml: function()
				{
					var html = '';

					this.selection.get();
					if (this.sel.rangeCount)
					{
						var container = document.createElement('div');
						var len = this.sel.rangeCount;
						for (var i = 0; i < len; ++i)
						{
							container.appendChild(this.sel.getRangeAt(i).cloneContents());
						}

						html = container.innerHTML;
					}

					return this.clean.onSync(html);
				},
				replaceSelection: function(html)
				{
					this.selection.get();
					this.range.deleteContents();
					var div = document.createElement("div");
					div.innerHTML = html;
					var frag = document.createDocumentFragment(), child;
					while ((child = div.firstChild)) {
						frag.appendChild(child);
					}

					this.range.insertNode(frag);
				},
				replaceWithHtml: function(html)
				{
					html = this.selection.getMarkerAsHtml(1) + html + this.selection.getMarkerAsHtml(2);

					this.selection.get();

					if (window.getSelection && window.getSelection().getRangeAt)
					{
						this.selection.replaceSelection(html);
					}
					else if (document.selection && document.selection.createRange)
					{
						this.range.pasteHTML(html);
					}

					this.selection.restore();
					this.code.sync();
				}
			};
		},
		shortcuts: function()
		{
			return {
				init: function(e, key)
				{
					// disable browser's hot keys for bold and italic
					if (!this.opts.shortcuts)
					{
						if ((e.ctrlKey || e.metaKey) && (key === 66 || key === 73)) e.preventDefault();
						return false;
					}

					$.each(this.opts.shortcuts, $.proxy(function(str, command)
					{
						var keys = str.split(',');
						var len = keys.length;
						for (var i = 0; i < len; i++)
						{
							if (typeof keys[i] === 'string')
							{
								this.shortcuts.handler(e, $.trim(keys[i]), $.proxy(function()
								{
									var func;
									if (command.func.search(/\./) != '-1')
									{
										func = command.func.split('.');
										if (typeof this[func[0]] != 'undefined')
										{
											this[func[0]][func[1]].apply(this, command.params);
										}
									}
									else
									{
										this[command.func].apply(this, command.params);
									}

								}, this));
							}

						}

					}, this));
				},
				handler: function(e, keys, origHandler)
				{
					// based on https://github.com/jeresig/jquery.hotkeys
					var hotkeysSpecialKeys =
					{
						8: "backspace", 9: "tab", 10: "return", 13: "return", 16: "shift", 17: "ctrl", 18: "alt", 19: "pause",
						20: "capslock", 27: "esc", 32: "space", 33: "pageup", 34: "pagedown", 35: "end", 36: "home",
						37: "left", 38: "up", 39: "right", 40: "down", 45: "insert", 46: "del", 59: ";", 61: "=",
						96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7",
						104: "8", 105: "9", 106: "*", 107: "+", 109: "-", 110: ".", 111 : "/",
						112: "f1", 113: "f2", 114: "f3", 115: "f4", 116: "f5", 117: "f6", 118: "f7", 119: "f8",
						120: "f9", 121: "f10", 122: "f11", 123: "f12", 144: "numlock", 145: "scroll", 173: "-", 186: ";", 187: "=",
						188: ",", 189: "-", 190: ".", 191: "/", 192: "`", 219: "[", 220: "\\", 221: "]", 222: "'"
					};


					var hotkeysShiftNums =
					{
						"`": "~", "1": "!", "2": "@", "3": "#", "4": "$", "5": "%", "6": "^", "7": "&",
						"8": "*", "9": "(", "0": ")", "-": "_", "=": "+", ";": ": ", "'": "\"", ",": "<",
						".": ">",  "/": "?",  "\\": "|"
					};

					keys = keys.toLowerCase().split(" ");
					var special = hotkeysSpecialKeys[e.keyCode],
						character = String.fromCharCode( e.which ).toLowerCase(),
						modif = "", possible = {};

					$.each([ "alt", "ctrl", "meta", "shift"], function(index, specialKey)
					{
						if (e[specialKey + 'Key'] && special !== specialKey)
						{
							modif += specialKey + '+';
						}
					});


					if (special) possible[modif + special] = true;
					if (character)
					{
						possible[modif + character] = true;
						possible[modif + hotkeysShiftNums[character]] = true;

						// "$" can be triggered as "Shift+4" or "Shift+$" or just "$"
						if (modif === "shift+")
						{
							possible[hotkeysShiftNums[character]] = true;
						}
					}

					for (var i = 0, len = keys.length; i < len; i++)
					{
						if (possible[keys[i]])
						{
							e.preventDefault();
							return origHandler.apply(this, arguments);
						}
					}
				}
			};
		},
		tabifier: function()
		{
			return {
				get: function(code)
				{
					if (!this.opts.tabifier) return code;

					// clean setup
					var ownLine = ['area', 'body', 'head', 'hr', 'i?frame', 'link', 'meta', 'noscript', 'style', 'script', 'table', 'tbody', 'thead', 'tfoot'];
					var contOwnLine = ['li', 'dt', 'dt', 'h[1-6]', 'option', 'script'];
					var newLevel = ['p', 'blockquote', 'div', 'dl', 'fieldset', 'form', 'frameset', 'map', 'ol', 'pre', 'select', 'td', 'th', 'tr', 'ul'];

					this.tabifier.lineBefore = new RegExp('^<(/?' + ownLine.join('|/?' ) + '|' + contOwnLine.join('|') + ')[ >]');
					this.tabifier.lineAfter = new RegExp('^<(br|/?' + ownLine.join('|/?' ) + '|/' + contOwnLine.join('|/') + ')[ >]');
					this.tabifier.newLevel = new RegExp('^</?(' + newLevel.join('|' ) + ')[ >]');

					var i = 0,
					codeLength = code.length,
					point = 0,
					start = null,
					end = null,
					tag = '',
					out = '',
					cont = '';

					this.tabifier.cleanlevel = 0;

					for (; i < codeLength; i++)
					{
						point = i;

						// if no more tags, copy and exit
						if (-1 == code.substr(i).indexOf( '<' ))
						{
							out += code.substr(i);

							return this.tabifier.finish(out);
						}

						// copy verbatim until a tag
						while (point < codeLength && code.charAt(point) != '<')
						{
							point++;
						}

						if (i != point)
						{
							cont = code.substr(i, point - i);
							if (!cont.match(/^\s{2,}$/g))
							{
								if ('\n' == out.charAt(out.length - 1)) out += this.tabifier.getTabs();
								else if ('\n' == cont.charAt(0))
								{
									out += '\n' + this.tabifier.getTabs();
									cont = cont.replace(/^\s+/, '');
								}

								out += cont;
							}

							if (cont.match(/\n/)) out += '\n' + this.tabifier.getTabs();
						}

						start = point;

						// find the end of the tag
						while (point < codeLength && '>' != code.charAt(point))
						{
							point++;
						}

						tag = code.substr(start, point - start);
						i = point;

						var t;

						if ('!--' == tag.substr(1, 3))
						{
							if (!tag.match(/--$/))
							{
								while ('-->' != code.substr(point, 3))
								{
									point++;
								}
								point += 2;
								tag = code.substr(start, point - start);
								i = point;
							}

							if ('\n' != out.charAt(out.length - 1)) out += '\n';

							out += this.tabifier.getTabs();
							out += tag + '>\n';
						}
						else if ('!' == tag[1])
						{
							out = this.tabifier.placeTag(tag + '>', out);
						}
						else if ('?' == tag[1])
						{
							out += tag + '>\n';
						}
						else if (t = tag.match(/^<(script|style|pre)/i))
						{
							t[1] = t[1].toLowerCase();
							tag = this.tabifier.cleanTag(tag);
							out = this.tabifier.placeTag(tag, out);
							end = String(code.substr(i + 1)).toLowerCase().indexOf('</' + t[1]);

							if (end)
							{
								cont = code.substr(i + 1, end);
								i += end;
								out += cont;
							}
						}
						else
						{
							tag = this.tabifier.cleanTag(tag);
							out = this.tabifier.placeTag(tag, out);
						}
					}

					return this.tabifier.finish(out);
				},
				getTabs: function()
				{
					var s = '';
					for ( var j = 0; j < this.tabifier.cleanlevel; j++ )
					{
						s += '\t';
					}

					return s;
				},
				finish: function(code)
				{
					code = code.replace(/\n\s*\n/g, '\n');
					code = code.replace(/^[\s\n]*/, '');
					code = code.replace(/[\s\n]*$/, '');
					code = code.replace(/<script(.*?)>\n<\/script>/gi, '<script$1></script>');

					this.tabifier.cleanlevel = 0;

					return code;
				},
				cleanTag: function (tag)
				{
					var tagout = '';
					tag = tag.replace(/\n/g, ' ');
					tag = tag.replace(/\s{2,}/g, ' ');
					tag = tag.replace(/^\s+|\s+$/g, ' ');

					var suffix = '';
					if (tag.match(/\/$/))
					{
						suffix = '/';
						tag = tag.replace(/\/+$/, '');
					}

					var m;
					while (m = /\s*([^= ]+)(?:=((['"']).*?\3|[^ ]+))?/.exec(tag))
					{
						if (m[2]) tagout += m[1].toLowerCase() + '=' + m[2];
						else if (m[1]) tagout += m[1].toLowerCase();

						tagout += ' ';
						tag = tag.substr(m[0].length);
					}

					return tagout.replace(/\s*$/, '') + suffix + '>';
				},
				placeTag: function (tag, out)
				{
					var nl = tag.match(this.tabifier.newLevel);

					if (tag.match(this.tabifier.lineBefore) || nl)
					{
						out = out.replace(/\s*$/, '');
						out += '\n';
					}

					if (nl && '/' == tag.charAt(1)) this.tabifier.cleanlevel--;
					if ('\n' == out.charAt(out.length - 1)) out += this.tabifier.getTabs();
					if (nl && '/' != tag.charAt(1)) this.tabifier.cleanlevel++;

					out += tag;

					if (tag.match(this.tabifier.lineAfter) || tag.match(this.tabifier.newLevel))
					{
						out = out.replace(/ *$/, '');
						//out += '\n';
					}

					return out;
				}
			};
		},
		tidy: function()
		{
			return {
				setupAllowed: function()
				{
					if (this.opts.allowedTags) this.opts.deniedTags = false;
					if (this.opts.allowedAttr) this.opts.removeAttr = false;

					if (this.opts.linebreaks) return;

					var tags = ['p', 'section'];
					if (this.opts.allowedTags) this.tidy.addToAllowed(tags);
					if (this.opts.deniedTags) this.tidy.removeFromDenied(tags);

				},
				addToAllowed: function(tags)
				{
					var len = tags.length;
					for (var i = 0; i < len; i++)
					{
						if ($.inArray(tags[i], this.opts.allowedTags) == -1)
						{
							this.opts.allowedTags.push(tags[i]);
						}
					}
				},
				removeFromDenied: function(tags)
				{
					var len = tags.length;
					for (var i = 0; i < len; i++)
					{
						var pos = $.inArray(tags[i], this.opts.deniedTags);
						if (pos != -1)
						{
							this.opts.deniedTags.splice(pos, 1);
						}
					}
				},
				load: function(html, options)
				{
					this.tidy.settings = {
						deniedTags: this.opts.deniedTags,
						allowedTags: this.opts.allowedTags,
						removeComments: this.opts.removeComments,
						replaceTags: this.opts.replaceTags,
						replaceStyles: this.opts.replaceStyles,
						removeDataAttr: this.opts.removeDataAttr,
						removeAttr: this.opts.removeAttr,
						allowedAttr: this.opts.allowedAttr,
						removeWithoutAttr: this.opts.removeWithoutAttr,
						removeEmpty: this.opts.removeEmpty
					};

					$.extend(this.tidy.settings, options);

					html = this.tidy.removeComments(html);

					// create container
					this.tidy.$div = $('<div />').append(html);

					// clean
					this.tidy.replaceTags();
					this.tidy.replaceStyles();
					this.tidy.removeTags();

					this.tidy.removeAttr();
					this.tidy.removeEmpty();
					this.tidy.removeParagraphsInLists();
					this.tidy.removeDataAttr();
					this.tidy.removeWithoutAttr();

					html = this.tidy.$div.html();
					this.tidy.$div.remove();

					return html;
				},
				removeComments: function(html)
				{
					if (!this.tidy.settings.removeComments) return html;

					return html.replace(/<!--[\s\S]*?-->/gi, '');
				},
				replaceTags: function(html)
				{
					if (!this.tidy.settings.replaceTags) return html;

					var len = this.tidy.settings.replaceTags.length;
					var replacement = [], rTags = [];
					for (var i = 0; i < len; i++)
					{
						rTags.push(this.tidy.settings.replaceTags[i][1]);
						replacement.push(this.tidy.settings.replaceTags[i][0]);
					}

					$.each(replacement, $.proxy(function(key, value)
					{
						this.tidy.$div.find(value).replaceWith(function()
						{
							return $("<" + rTags[key] + " />", {html: $(this).html()});
						});
					}, this));
				},
				replaceStyles: function()
				{
					if (!this.tidy.settings.replaceStyles) return;

					var len = this.tidy.settings.replaceStyles.length;
					this.tidy.$div.find('span').each($.proxy(function(n,s)
					{
						var $el = $(s);
						var style = $el.attr('style');
						for (var i = 0; i < len; i++)
						{
							if (style && style.match(new RegExp('^' + this.tidy.settings.replaceStyles[i][0], 'i')))
							{
								var tagName = this.tidy.settings.replaceStyles[i][1];
								$el.replaceWith(function()
								{
									var tag = document.createElement(tagName);
									return $(tag).append($(this).contents());
								});
							}
						}

					}, this));

				},
				removeTags: function()
				{
					if (!this.tidy.settings.deniedTags && this.tidy.settings.allowedTags)
					{
						this.tidy.$div.find('*').not(this.tidy.settings.allowedTags.join(',')).each(function(i, s)
						{
							if (s.innerHTML === '') $(s).remove();
							else $(s).contents().unwrap();
						});
					}

					if (this.tidy.settings.deniedTags)
					{
						this.tidy.$div.find(this.tidy.settings.deniedTags.join(',')).each(function(i, s)
						{
							if ($(s).hasClass('redactor-script-tag')) return;

							if (s.innerHTML === '') $(s).remove();
							else $(s).contents().unwrap();
						});
					}
				},
				removeAttr: function()
				{
					var len;
					if (!this.tidy.settings.removeAttr && this.tidy.settings.allowedAttr)
					{

						var allowedAttrTags = [], allowedAttrData = [];
						len = this.tidy.settings.allowedAttr.length;
						for (var i = 0; i < len; i++)
						{
							allowedAttrTags.push(this.tidy.settings.allowedAttr[i][0]);
							allowedAttrData.push(this.tidy.settings.allowedAttr[i][1]);
						}


						this.tidy.$div.find('*').each($.proxy(function(n,s)
						{
							var $el = $(s);
							var pos = $.inArray($el[0].tagName.toLowerCase(), allowedAttrTags);
							var attributesRemove = this.tidy.removeAttrGetRemoves(pos, allowedAttrData, $el);

							if (attributesRemove)
							{
								$.each(attributesRemove, function(z,f) {
									$el.removeAttr(f);
								});
							}
						}, this));
					}

					if (this.tidy.settings.removeAttr)
					{
						len = this.tidy.settings.removeAttr.length;
						for (var i = 0; i < len; i++)
						{
							var attrs = this.tidy.settings.removeAttr[i][1];
							if ($.isArray(attrs)) attrs = attrs.join(' ');

							this.tidy.$div.find(this.tidy.settings.removeAttr[i][0]).removeAttr(attrs);
						}
					}

				},
				removeAttrGetRemoves: function(pos, allowed, $el)
				{
					var attributesRemove = [];

					// remove all attrs
					if (pos == -1)
					{
						$.each($el[0].attributes, function(i, item)
						{
							attributesRemove.push(item.name);
						});

					}
					// allow all attrs
					else if (allowed[pos] == '*')
					{
						attributesRemove = [];
					}
					// allow specific attrs
					else
					{
						$.each($el[0].attributes, function(i, item)
						{
							if ($.isArray(allowed[pos]))
							{
								if ($.inArray(item.name, allowed[pos]) == -1)
								{
									attributesRemove.push(item.name);
								}
							}
							else if (allowed[pos] != item.name)
							{
								attributesRemove.push(item.name);
							}

						});
					}

					return attributesRemove;
				},
				removeAttrs: function (el, regex)
				{
					regex = new RegExp(regex, "g");
					return el.each(function()
					{
						var self = $(this);
						var len = this.attributes.length - 1;
						for (var i = len; i >= 0; i--)
						{
							var item = this.attributes[i];
							if (item && item.specified && item.name.search(regex)>=0)
							{
								self.removeAttr(item.name);
							}
						}
					});
				},
				removeEmpty: function()
				{
					if (!this.tidy.settings.removeEmpty) return;

					this.tidy.$div.find(this.tidy.settings.removeEmpty.join(',')).each(function()
					{
						var $el = $(this);
						var text = $el.text();
						text = text.replace(/\u200B/g, '');
						text = text.replace(/&nbsp;/gi, '');
						text = text.replace(/\s/g, '');

		    	    	if (text === '' && $el.children().length === 0)
		    	    	{
			    	    	$el.remove();
		    	    	}
					});
				},
				removeParagraphsInLists: function()
				{
					this.tidy.$div.find('li p').contents().unwrap();
				},
				removeDataAttr: function()
				{
					if (!this.tidy.settings.removeDataAttr) return;

					var tags = this.tidy.settings.removeDataAttr;
					if ($.isArray(this.tidy.settings.removeDataAttr)) tags = this.tidy.settings.removeDataAttr.join(',');

					this.tidy.removeAttrs(this.tidy.$div.find(tags), '^(data-)');

				},
				removeWithoutAttr: function()
				{
					if (!this.tidy.settings.removeWithoutAttr) return;

					this.tidy.$div.find(this.tidy.settings.removeWithoutAttr.join(',')).each(function()
					{
						if (this.attributes.length === 0)
						{
							$(this).contents().unwrap();
						}
					});
				}
			};
		},
		toolbar: function()
		{
			return {
				init: function()
				{
					return {
						html:
						{
							title: this.lang.get('html'),
							func: 'code.toggle'
						},
						formatting:
						{
							title: this.lang.get('formatting'),
							dropdown:
							{
								p:
								{
									title: this.lang.get('paragraph'),
									func: 'block.format'
								},
								blockquote:
								{
									title: this.lang.get('quote'),
									func: 'block.format'
								},
								pre:
								{
									title: this.lang.get('code'),
									func: 'block.format'
								},
								h1:
								{
									title: this.lang.get('header1'),
									func: 'block.format'
								},
								h2:
								{
									title: this.lang.get('header2'),
									func: 'block.format'
								},
								h3:
								{
									title: this.lang.get('header3'),
									func: 'block.format'
								},
								h4:
								{
									title: this.lang.get('header4'),
									func: 'block.format'
								},
								h5:
								{
									title: this.lang.get('header5'),
									func: 'block.format'
								}
							}
						},
						bold:
						{
							title: this.lang.get('bold'),
							func: 'inline.format'
						},
						italic:
						{
							title: this.lang.get('italic'),
							func: 'inline.format'
						},
						deleted:
						{
							title: this.lang.get('deleted'),
							func: 'inline.format'
						},
						underline:
						{
							title: this.lang.get('underline'),
							func: 'inline.format'
						},
						unorderedlist:
						{
							title: '&bull; ' + this.lang.get('unorderedlist'),
							func: 'list.toggle'
						},
						orderedlist:
						{
							title: '1. ' + this.lang.get('orderedlist'),
							func: 'list.toggle'
						},
						outdent:
						{
							title: '< ' + this.lang.get('outdent'),
							func: 'indent.decrease'
						},
						indent:
						{
							title: '> ' + this.lang.get('indent'),
							func: 'indent.increase'
						},
						image:
						{
							title: this.lang.get('image'),
							func: 'image.show'
						},
						file:
						{
							title: this.lang.get('file'),
							func: 'file.show'
						},
						link:
						{
							title: this.lang.get('link'),
							dropdown:
							{
								link:
								{
									title: this.lang.get('link_insert'),
									func: 'link.show',
									observe: {
										element: 'a',
										in: {
											title: this.lang.get('link_edit'),
										},
										out: {
											title: this.lang.get('link_insert')
										}
									}
								},
								unlink:
								{
									title: this.lang.get('unlink'),
									func: 'link.unlink',
									observe: {
										element: 'a',
										out: {
											attr: {
												'class': 'redactor-dropdown-link-inactive',
												'aria-disabled': true
											}
										}
									}
								}
							}
						},
						alignment:
						{
							title: this.lang.get('alignment'),
							dropdown:
							{
								left:
								{
									title: this.lang.get('align_left'),
									func: 'alignment.left'
								},
								center:
								{
									title: this.lang.get('align_center'),
									func: 'alignment.center'
								},
								right:
								{
									title: this.lang.get('align_right'),
									func: 'alignment.right'
								},
								justify:
								{
									title: this.lang.get('align_justify'),
									func: 'alignment.justify'
								}
							}
						},
						horizontalrule:
						{
							title: this.lang.get('horizontalrule'),
							func: 'line.insert'
						}
					};
				},
				build: function()
				{
					this.toolbar.hideButtons();
					this.toolbar.hideButtonsOnMobile();
					this.toolbar.isButtonSourceNeeded();

					if (this.opts.buttons.length === 0) return;

					this.$toolbar = this.toolbar.createContainer();

					this.toolbar.setOverflow();
					this.toolbar.append();
					this.toolbar.setFormattingTags();
					this.toolbar.loadButtons();
					this.toolbar.setFixed();

					// buttons response
					if (this.opts.activeButtons)
					{
						this.$editor.on('mouseup.redactor keyup.redactor focus.redactor', $.proxy(this.observe.toolbar, this));
					}

				},
				createContainer: function()
				{
					return $('<ul>').addClass('redactor-toolbar').attr({'id': 'redactor-toolbar-' + this.uuid, 'role': 'toolbar'});
				},
				setFormattingTags: function()
				{
					$.each(this.opts.toolbar.formatting.dropdown, $.proxy(function (i, s)
					{
						if ($.inArray(i, this.opts.formatting) == -1) delete this.opts.toolbar.formatting.dropdown[i];
					}, this));

				},
				loadButtons: function()
				{
					$.each(this.opts.buttons, $.proxy(function(i, btnName)
					{
						if (!this.opts.toolbar[btnName]) return;

						if (btnName === 'file')
						{
							 if (this.opts.fileUpload === false) return;
							 else if (!this.opts.fileUpload && this.opts.s3 === false) return;
						}

						if (btnName === 'image')
						{
							 if (this.opts.imageUpload === false) return;
							 else if (!this.opts.imageUpload && this.opts.s3 === false) return;
						}

						var btnObject = this.opts.toolbar[btnName];
						this.$toolbar.append($('<li>').append(this.button.build(btnName, btnObject)));

					}, this));
				},
				append: function()
				{
					if (this.opts.toolbarExternal)
					{
						this.$toolbar.addClass('redactor-toolbar-external');
						$(this.opts.toolbarExternal).html(this.$toolbar);
					}
					else
					{
						this.$box.prepend(this.$toolbar);
					}
				},
				setFixed: function()
				{
					if (!this.utils.isDesktop()) return;
					if (this.opts.toolbarExternal) return;
					if (!this.opts.toolbarFixed) return;

					this.toolbar.observeScroll();
					$(this.opts.toolbarFixedTarget).on('scroll.redactor.' + this.uuid, $.proxy(this.toolbar.observeScroll, this));

				},
				setOverflow: function()
				{
					if (this.utils.isMobile() && this.opts.toolbarOverflow)
					{
						this.$toolbar.addClass('redactor-toolbar-overflow');
					}
				},
				isButtonSourceNeeded: function()
				{
					if (this.opts.source) return;

					var index = this.opts.buttons.indexOf('html');
					if (index !== -1)
					{
						this.opts.buttons.splice(index, 1);
					}
				},
				hideButtons: function()
				{
					if (this.opts.buttonsHide.length === 0) return;

					$.each(this.opts.buttonsHide, $.proxy(function(i, s)
					{
						var index = this.opts.buttons.indexOf(s);
						this.opts.buttons.splice(index, 1);

					}, this));
				},
				hideButtonsOnMobile: function()
				{
					if (!this.utils.isMobile() || this.opts.buttonsHideOnMobile.length === 0) return;

					$.each(this.opts.buttonsHideOnMobile, $.proxy(function(i, s)
					{
						var index = this.opts.buttons.indexOf(s);
						this.opts.buttons.splice(index, 1);

					}, this));
				},
				observeScroll: function()
				{
					var scrollTop = $(this.opts.toolbarFixedTarget).scrollTop();
					var boxTop = 1;

					if (this.opts.toolbarFixedTarget === document)
					{
						boxTop = this.$box.offset().top;
					}

					if (scrollTop > boxTop)
					{
						this.toolbar.observeScrollEnable(scrollTop, boxTop);
					}
					else
					{
						this.toolbar.observeScrollDisable();
					}
				},
				observeScrollEnable: function(scrollTop, boxTop)
				{
					var top = this.opts.toolbarFixedTopOffset + scrollTop - boxTop;
					var left = 0;
					var end = boxTop + this.$box.height() - 32;
					var width = this.$box.innerWidth();

					this.$toolbar.addClass('toolbar-fixed-box');
					this.$toolbar.css({
						position: 'absolute',
						width: width,
						top: top + 'px',
						left: left
					});

					if (scrollTop > end)
						$('.redactor-dropdown-' + this.uuid + ':visible').hide();

					this.toolbar.setDropdownsFixed();
					this.$toolbar.css('visibility', (scrollTop < end) ? 'visible' : 'hidden');
				},
				observeScrollDisable: function()
				{
					this.$toolbar.css({
						position: 'relative',
						width: 'auto',
						top: 0,
						left: 0,
						visibility: 'visible'
					});

					this.toolbar.unsetDropdownsFixed();
					this.$toolbar.removeClass('toolbar-fixed-box');
				},
				setDropdownsFixed: function()
				{
					var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset;
					var position = 'fixed';
					if (this.opts.toolbarFixedTarget !== document)
					{
						top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top) + this.opts.toolbarFixedTopOffset;
						position = 'absolute';
					}

					$('.redactor-dropdown-' + this.uuid).each(function()
					{
						$(this).css({ position: position, top: top + 'px' });
					});
				},
				unsetDropdownsFixed: function()
				{
					var top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top);
					$('.redactor-dropdown-' + this.uuid).each(function()
					{
						$(this).css({ position: 'absolute', top: top + 'px' });
					});
				}
			};
		},
		upload: function()
		{
			return {
				init: function(id, url, callback)
				{
					this.upload.direct = false;
					this.upload.callback = callback;
					this.upload.url = url;
					this.upload.$el = $(id);
					this.upload.$droparea = $('<div id="redactor-droparea" />');

					this.upload.$placeholdler = $('<div id="redactor-droparea-placeholder" />').text(this.lang.get('upload_label'));
					this.upload.$input = $('<input type="file" name="file" />');

					this.upload.$placeholdler.append(this.upload.$input);
					this.upload.$droparea.append(this.upload.$placeholdler);
					this.upload.$el.append(this.upload.$droparea);

					this.upload.$droparea.off('redactor.upload');
					this.upload.$input.off('redactor.upload');

					this.upload.$droparea.on('dragover.redactor.upload', $.proxy(this.upload.onDrag, this));
					this.upload.$droparea.on('dragleave.redactor.upload', $.proxy(this.upload.onDragLeave, this));

					// change
					this.upload.$input.on('change.redactor.upload', $.proxy(function(e)
					{
						e = e.originalEvent || e;
						this.upload.traverseFile(this.upload.$input[0].files[0], e);
					}, this));

					// drop
					this.upload.$droparea.on('drop.redactor.upload', $.proxy(function(e)
					{
						e.preventDefault();

						this.upload.$droparea.removeClass('drag-hover').addClass('drag-drop');
						this.upload.onDrop(e);

					}, this));
				},
				directUpload: function(file, e)
				{
					this.upload.direct = true;
					this.upload.traverseFile(file, e);
				},
				onDrop: function(e)
				{
					e = e.originalEvent || e;
					var files = e.dataTransfer.files;

					this.upload.traverseFile(files[0], e);
				},
				traverseFile: function(file, e)
				{
					if (this.opts.s3)
					{
						this.upload.setConfig(file);
						this.upload.s3uploadFile(file);
						return;
					}

					var formData = !!window.FormData ? new FormData() : null;
					if (window.FormData)
					{
						this.upload.setConfig(file);

						var name = (this.upload.type == 'image') ? this.opts.imageUploadParam : this.opts.fileUploadParam;
						formData.append(name, file);
					}

					this.progress.show();
					this.core.setCallback('uploadStart', e, formData);
					this.upload.sendData(formData, e);
				},
				setConfig: function(file)
				{
					this.upload.getType(file);

					if (this.upload.direct)
					{
						this.upload.url = (this.upload.type == 'image') ? this.opts.imageUpload : this.opts.fileUpload;
						this.upload.callback = (this.upload.type == 'image') ? this.image.insert : this.file.insert;
					}
				},
				getType: function(file)
				{
					this.upload.type = 'image';
					if (this.opts.imageTypes.indexOf(file.type) == -1)
					{
						this.upload.type = 'file';
					}
				},
				getHiddenFields: function(obj, fd)
				{
					if (obj === false || typeof obj !== 'object') return fd;

					$.each(obj, $.proxy(function(k, v)
					{
						if (v !== null && v.toString().indexOf('#') === 0) v = $(v).val();
						fd.append(k, v);

					}, this));

					return fd;

				},
				sendData: function(formData, e)
				{
					// append hidden fields
					if (this.upload.type == 'image')
					{
						formData = this.upload.getHiddenFields(this.opts.uploadImageFields, formData);
						formData = this.upload.getHiddenFields(this.upload.imageFields, formData);
					}
					else
					{
						formData = this.upload.getHiddenFields(this.opts.uploadFileFields, formData);
						formData = this.upload.getHiddenFields(this.upload.fileFields, formData);
					}

					var xhr = new XMLHttpRequest();
					xhr.open('POST', this.upload.url);
					xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

					// complete
					xhr.onreadystatechange = $.proxy(function()
					{
					    if (xhr.readyState == 4)
					    {
					        var data = xhr.responseText;

							data = data.replace(/^\[/, '');
							data = data.replace(/\]$/, '');

							var json;
							try
							{
								json = (typeof data === 'string' ? $.parseJSON(data) : data);
							}
							catch(err)
							{
								json = {
									error: true
								};
							}


							this.progress.hide();

							if (!this.upload.direct)
							{
								this.upload.$droparea.removeClass('drag-drop');
							}

							this.upload.callback(json, this.upload.direct, e);
					    }
					}, this);


					/*
					xhr.upload.onprogress = $.proxy(function(e)
					{
						if (e.lengthComputable)
						{
							var complete = (e.loaded / e.total * 100 | 0);
							//progress.value = progress.innerHTML = complete;
						}

					}, this);
					*/


					xhr.send(formData);
				},
				onDrag: function(e)
				{
					e.preventDefault();
					this.upload.$droparea.addClass('drag-hover');
				},
				onDragLeave: function(e)
				{
					e.preventDefault();
					this.upload.$droparea.removeClass('drag-hover');
				},
				clearImageFields: function()
				{
					this.upload.imageFields = {};
				},
				addImageFields: function(name, value)
				{
					this.upload.imageFields[name] = value;
				},
				removeImageFields: function(name)
				{
					delete this.upload.imageFields[name];
				},
				clearFileFields: function()
				{
					this.upload.fileFields = {};
				},
				addFileFields: function(name, value)
				{
					this.upload.fileFields[name] = value;
				},
				removeFileFields: function(name)
				{
					delete this.upload.fileFields[name];
				},


				// S3
				s3uploadFile: function(file)
				{
					this.upload.s3executeOnSignedUrl(file, $.proxy(function(signedURL)
					{
						this.upload.s3uploadToS3(file, signedURL);
					}, this));
				},
				s3executeOnSignedUrl: function(file, callback)
				{
					var xhr = new XMLHttpRequest();

					var mark = '?';
					if (this.opts.s3.search(/\?/) != '-1') mark = '&';

					xhr.open('GET', this.opts.s3 + mark + 'name=' + file.name + '&type=' + file.type, true);

					// Hack to pass bytes through unprocessed.
					if (xhr.overrideMimeType) xhr.overrideMimeType('text/plain; charset=x-user-defined');

					var that = this;
					xhr.onreadystatechange = function(e)
					{
						if (this.readyState == 4 && this.status == 200)
						{
							that.progress.show();
							callback(decodeURIComponent(this.responseText));
						}
						else if (this.readyState == 4 && this.status != 200)
						{
							//setProgress(0, 'Could not contact signing script. Status = ' + this.status);
						}
					};

					xhr.send();
				},
				s3createCORSRequest: function(method, url)
				{
					var xhr = new XMLHttpRequest();
					if ("withCredentials" in xhr)
					{
						xhr.open(method, url, true);
					}
					else if (typeof XDomainRequest != "undefined")
					{
						xhr = new XDomainRequest();
						xhr.open(method, url);
					}
					else
					{
						xhr = null;
					}

					return xhr;
				},
				s3uploadToS3: function(file, url)
				{
					var xhr = this.upload.s3createCORSRequest('PUT', url);
					if (!xhr)
					{
						//setProgress(0, 'CORS not supported');
					}
					else
					{
						xhr.onload = $.proxy(function()
						{
							if (xhr.status == 200)
							{
								//setProgress(100, 'Upload completed.');

								this.progress.hide();

								var s3file = url.split('?');

								if (!s3file[0])
								{
									 // url parsing is fail
									 return false;
								}


								if (!this.upload.direct)
								{
									this.upload.$droparea.removeClass('drag-drop');
								}

								var json = { filelink: s3file[0] };
								if (this.upload.type == 'file')
								{
									var arr = s3file[0].split('/');
									json.filename = arr[arr.length-1];
								}

								this.upload.callback(json, this.upload.direct, false);


							}
							else
							{
								//setProgress(0, 'Upload error: ' + xhr.status);
							}
						}, this);

						xhr.onerror = function()
						{
							//setProgress(0, 'XHR error.');
						};

						xhr.upload.onprogress = function(e)
						{
							/*
							if (e.lengthComputable)
							{
								var percentLoaded = Math.round((e.loaded / e.total) * 100);
								setProgress(percentLoaded, percentLoaded == 100 ? 'Finalizing.' : 'Uploading.');
							}
							*/
						};

						xhr.setRequestHeader('Content-Type', file.type);
						xhr.setRequestHeader('x-amz-acl', 'public-read');

						xhr.send(file);
					}
				}
			};
		},
		utils: function()
		{
			return {
				isMobile: function()
				{
					return /(iPhone|iPod|BlackBerry|Android)/.test(navigator.userAgent);
				},
				isDesktop: function()
				{
					return !/(iPhone|iPod|iPad|BlackBerry|Android)/.test(navigator.userAgent);
				},
				isString: function(obj)
				{
					return Object.prototype.toString.call(obj) == '[object String]';
				},
				isEmpty: function(html, removeEmptyTags)
				{
					html = html.replace(/[\u200B-\u200D\uFEFF]/g, '');
					html = html.replace(/&nbsp;/gi, '');
					html = html.replace(/<\/?br\s?\/?>/g, '');
					html = html.replace(/\s/g, '');
					html = html.replace(/^<p>[^\W\w\D\d]*?<\/p>$/i, '');
					html = html.replace(/<iframe(.*?[^>])>$/i, 'iframe');
					html = html.replace(/<source(.*?[^>])>$/i, 'source');

					// remove empty tags
					if (removeEmptyTags !== false)
					{
						html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, '');
						html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, '');
					}

					html = $.trim(html);

					return html === '';
				},
				normalize: function(str)
				{
					if (typeof(str) === 'undefined') return 0;
					return parseInt(str.replace('px',''), 10);
				},
				hexToRgb: function(hex)
				{
					if (typeof hex == 'undefined') return;
					if (hex.search(/^#/) == -1) return hex;

					var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
					hex = hex.replace(shorthandRegex, function(m, r, g, b)
					{
						return r + r + g + g + b + b;
					});

					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
					return 'rgb(' + parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) + ')';
				},
				getOuterHtml: function(el)
				{
					return $('<div>').append($(el).eq(0).clone()).html();
				},
				getAlignmentElement: function(el)
				{
					if ($.inArray(el.tagName, this.opts.alignmentTags) !== -1)
					{
						return $(el);
					}
					else
					{
						return $(el).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
					}
				},
				removeEmptyAttr: function(el, attr)
				{
					var $el = $(el);
					if (typeof $el.attr(attr) == 'undefined')
					{
						return true;
					}

					if ($el.attr(attr) === '')
					{
						$el.removeAttr(attr);
						return true;
					}

					return false;
				},
				removeEmpty: function(i, s)
				{
					var $s = $($.parseHTML(s));

					$s.find('.redactor-invisible-space').removeAttr('style').removeAttr('class');

					if ($s.find('hr, br, img, iframe, source').length !== 0) return;
					var text = $.trim($s.text());

					if (this.utils.isEmpty(text, false))
					{
						$s.remove();
					}
				},

				// save and restore scroll
				saveScroll: function()
				{
					this.saveEditorScroll = this.$editor.scrollTop();
					this.saveBodyScroll = $(window).scrollTop();

					if (this.opts.scrollTarget) this.saveTargetScroll = $(this.opts.scrollTarget).scrollTop();
				},
				restoreScroll: function()
				{
					if (typeof this.saveScroll === 'undefined' && typeof this.saveBodyScroll === 'undefined') return;

					$(window).scrollTop(this.saveBodyScroll);
					this.$editor.scrollTop(this.saveEditorScroll);

					if (this.opts.scrollTarget) $(this.opts.scrollTarget).scrollTop(this.saveTargetScroll);
				},

				// get invisible space element
				createSpaceElement: function()
				{
					var space = document.createElement('span');
					space.className = 'redactor-invisible-space';
					space.innerHTML = this.opts.invisibleSpace;

					return space;
				},

				// replace
				removeInlineTags: function(node)
				{
					var tags = this.opts.inlineTags;
					tags.push('span');

					if (node.tagName == 'PRE') tags.push('a');

					$(node).find(tags.join(',')).not('span.redactor-selection-marker').contents().unwrap();
				},
				replaceWithContents: function(node, removeInlineTags)
				{
					var self = this;
					$(node).replaceWith(function()
					{
						if (removeInlineTags === true) self.utils.removeInlineTags(this);

						return $(this).contents();
					});

					return $(node);
				},
				replaceToTag: function(node, tag, removeInlineTags)
				{
					var replacement;
					var self = this;
					$(node).replaceWith(function()
					{
						replacement = $('<' + tag + ' />').append($(this).contents());

						for (var i = 0; i < this.attributes.length; i++)
						{
							replacement.attr(this.attributes[i].name, this.attributes[i].value);
						}

						if (removeInlineTags === true) self.utils.removeInlineTags(replacement);

						return replacement;
					});

					return replacement;
				},

				// start and end of element
				isStartOfElement: function()
				{
					var block = this.selection.getBlock();
					if (!block) return false;

					var offset = this.caret.getOffsetOfElement(block);

					return (offset === 0) ? true : false;
				},
				isEndOfElement: function(element)
				{
					if (typeof element == 'undefined')
					{
						var element = this.selection.getBlock();
						if (!element) return false;
					}

					var offset = this.caret.getOffsetOfElement(element);
					var text = $.trim($(element).text()).replace(/\n\r\n/g, '');

					return (offset == text.length) ? true : false;
				},
				isEndOfEditor: function()
				{
					var block = this.$editor[0];

					var offset = this.caret.getOffsetOfElement(block);
					var text = $.trim($(block).html().replace(/(<([^>]+)>)/gi,''));

					return (offset == text.length) ? true : false;
				},

				// test blocks
				isBlock: function(block)
				{
					block = block[0] || block;

					return block && this.utils.isBlockTag(block.tagName);
				},
				isBlockTag: function(tag)
				{
					if (typeof tag == 'undefined') return false;

					return this.reIsBlock.test(tag);
				},

				// tag detection
				isTag: function(current, tag)
				{
					var element = $(current).closest(tag, this.$editor[0]);
					if (element.length == 1)
					{
						return element[0];
					}

					return false;
				},

				// select all
				isSelectAll: function()
				{
					return this.selectAll;
				},
				enableSelectAll: function()
				{
					this.selectAll = true;
				},
				disableSelectAll: function()
				{
					this.selectAll = false;
				},

				// parents detection
				isRedactorParent: function(el)
				{
					if (!el)
					{
						return false;
					}

					if ($(el).parents('.redactor-editor').length === 0 || $(el).hasClass('redactor-editor'))
					{
						return false;
					}

					return el;
				},
				isCurrentOrParentHeader: function()
				{
					return this.utils.isCurrentOrParent(['H1', 'H2', 'H3', 'H4', 'H5', 'H6']);
				},
				isCurrentOrParent: function(tagName)
				{
					var parent = this.selection.getParent();
					var current = this.selection.getCurrent();

					if ($.isArray(tagName))
					{
						var matched = 0;
						$.each(tagName, $.proxy(function(i, s)
						{
							if (this.utils.isCurrentOrParentOne(current, parent, s))
							{
								matched++;
							}
						}, this));

						return (matched === 0) ? false : true;
					}
					else
					{
						return this.utils.isCurrentOrParentOne(current, parent, tagName);
					}
				},
				isCurrentOrParentOne: function(current, parent, tagName)
				{
					tagName = tagName.toUpperCase();

					return parent && parent.tagName === tagName ? parent : current && current.tagName === tagName ? current : false;
				},


				// browsers detection
				isOldIe: function()
				{
					return (this.utils.browser('msie') && parseInt(this.utils.browser('version'), 10) < 9) ? true : false;
				},
				isLessIe10: function()
				{
					return (this.utils.browser('msie') && parseInt(this.utils.browser('version'), 10) < 10) ? true : false;
				},
				isIe11: function()
				{
					return !!navigator.userAgent.match(/Trident\/7\./);
				},
				browser: function(browser)
				{
					var ua = navigator.userAgent.toLowerCase();
					var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
		            /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		            /(webkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
		            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		            /(msie) ([\w.]+)/.exec( ua ) ||
		            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
		            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		            [];

					if (browser == 'safari') return (typeof match[3] != 'undefined') ? match[3] == 'safari' : false;
					if (browser == 'version') return match[2];
					if (browser == 'webkit') return (match[1] == 'chrome' || match[1] == 'opr' || match[1] == 'webkit');
					if (match[1] == 'rv') return browser == 'msie';
					if (match[1] == 'opr') return browser == 'webkit';

					return browser == match[1];
				},
				strpos: function(haystack, needle, offset)
				{
					var i = haystack.indexOf(needle, offset);
					return i >= 0 ? i : false;
				},
				disableBodyScroll: function()
				{

					var $body = $('html');
					var windowWidth = window.innerWidth;
					if (!windowWidth)
					{
						var documentElementRect = document.documentElement.getBoundingClientRect();
						windowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
					}

					var isOverflowing = document.body.clientWidth < windowWidth;
					var scrollbarWidth = this.utils.measureScrollbar();

					$body.css('overflow', 'hidden');
					if (isOverflowing) $body.css('padding-right', scrollbarWidth);


				},
				measureScrollbar: function()
				{
					var $body = $('body');
					var scrollDiv = document.createElement('div');
					scrollDiv.className = 'redactor-scrollbar-measure';

					$body.append(scrollDiv);
					var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
					$body[0].removeChild(scrollDiv);
					return scrollbarWidth;
				},
				enableBodyScroll: function()
				{
					$('html').css({ 'overflow': '', 'padding-right': '' });
					$('body').remove('redactor-scrollbar-measure');
				}
			};
		}
	};

	$(window).on('load.tools.redactor', function()
	{
		$('[data-tools="redactor"]').redactor();
	});

	// constructor
	Redactor.prototype.init.prototype = Redactor.prototype;

})(jQuery);
///#source 1 1 /assets/ContentBuilder/contentbuilder.js
eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p } ('c 2D=\'\';c 6X=C;c 6J=K.5t("9F");c 48;1T(c i=0;i<6J.N;i++){c 3m=6J[i].1y.1L();d(3m.1C("O-1y.6C")!=-1)48=6J[i].1y.2b(/O-1y.6C/,"");d(3m.1C("O.6C")!=-1)48=6J[i].1y.2b(/O.6C/,"")}c cG=48.41("?");48=cG[0];d((2f.2r.6w(/cE/i))||(2f.2r.6w(/cC/i))){c 9z=K.cr(\'9F\');9z.1y=48+\'gk-1b.6C\';K.5t(\'gj\')[0].cp(9z)}(q(b){c $2P;b.O=q(u,2O){c 4E={D:\'1\',3g:"52,5i,4X,54,4Z,51,p,9x,c7,c6,.5c",3E:\'5g\',4c:q(){},c5:q(){},c4:\'fA/5g/fy.G\',5p:[\'\',\'\'],4h:T,c3:\'2k\',9t:T,7u:[[0,"fr"],[-1,"fm"],[1,"7K"],[2,"7K, bV"],[3,"bU, 7K"],[4,"bU, 7K, bV"],[5,"bT, 7h"],[6,"7h"],[7,"7h, 62 + 7z"],[8,"bT, 7h, 62 + 7z"],[9,"62 + 7z"],[10,"62 + fg 7z"],[11,"62"],[12,"fb 9s"],[13,"eX bI eP"],[14,"eH"],[15,"eG"],[16,"eF"],[17,"ey"],[20,"ev"],[18,"eo"],[19,"em"]],33:\'\',3Q:C,9c:C,27:\'\',7m:C,3S:["#bk","#bf","#bd","#bb","#ba","#b7","#b5","#b4","#93","#aU","#aS","#aO","#aN","#aL","#aK","#aI","#aH","#aG","#aF","#aC","#aB","#aA","#az","#as","#aq","#ap","#am","#ak","#ag","#ae","#a7","#a6","#a5","#a4","#a3","#a2"],4u:\'#6G\',1v:\'1q\',4V:\'5f\',5q:\'\',a1:T};h.E={};c $u=b(u),u=u;h.4M=q(){h.E=b.7o({},4E,2O);d(!h.E.7m){2C.eW("D")}d(2C.8T("D")!=1S){h.E.D=2C.D}J{2C.D=h.E.D}$u.m(\'D\',h.E.D);$u.m(\'-6s-8S\',\'8R(\'+h.E.D+\')\');$u.3f(\'7R\');h.E.D=h.E.D+\'\';d(h.E.D.1C(\'%\')!=-1){h.E.D=h.E.D.2b(\'%\',\'\')/22;2C.D=h.E.D}d(h.E.D==\'5R\'){h.E.D=1;2C.D=1}d(2D==\'\'){2D=\'#\'+$u.I(\'F\')}J{2D=2D+\',#\'+$u.I(\'F\')}$u.m({\'8M-1e\':\'eh\'});d(b(\'#2x\').N==0){b(\'2i\').2w(\'<k F="2x"></k>\')}d(b(\'#8K\').N==0){b(\'#2x\').2w(\'<k F="8K" M="1m:1s"></k>\');c 6f=\'\';1T(c i=0;i<h.E.7u.N;i++){6f+=\'<2m 2e="\'+h.E.7u[i][0]+\'">\'+h.E.7u[i][1]+\'</2m>\'}6f=\'<2B F="5B" M="1m:1s;L:83%;4a:5Z;3P:5Z;4a:8H 0 dZ 5Z;1Z-39:30;9H-9G:2R;1e:dV;cJ-1e:1;1p:#dT;1z-2q:3v;1z:1s;1G:#49;5o-dF: 0 0 5Z 72(0, 0, 0,0.2);3u:4x;">\'+6f+\'</2B>\';c s=\'<k F="2F">\'+6f;s+=\'<k F="6G"></k>\';s+=\'\';s+=\'<br><k F="cw"><1u 1M="Y" F="5T" 8M="80" 3i="22" 2e="22"></k>\';s+=\'\';s+=\'<a F="3r" 1O="#"><i w="cb-1a-V-3Y-5e" M="1Z-39: 3s;"></i></a></k>\';b(\'#2x\').2w(s);b(\'#5T\').1d(h.E.D*22);b(\'#2x 1u[1M="Y"]\').dC({dx:q(3w,2e){},dt:T});c 1d=b(\'#5T\').1d()/22;h.D(1d);b(\'#5T\').6x(\'4p\',q(){d($u.l(\'O\').E.7m==C){c 1d=b(\'#5T\').1d()/22;$u.l(\'O\').D(1d)}});d(!h.E.7m&&h.E.4u==\'#6G\'){b(\'#cw\').m(\'1m\',\'1s\');b(\'#6G\').m(\'1e\',\'22%\')}b.do(h.E.c4,q(l){c 6E=\'\';c 6Q=\'\';c i=1;c 6S=T;b(\'<k/>\').G(l).1J(\'k\').1F(q(){c 2o=b(h).G();c cl=b(\'<k/>\').1n(2o).G();6E+=\'<k F="3d\'+i+\'">\'+cl+\'</k>\';d(b(h).l("4e")!=1S)6S=C;c 5M=b(h).l("5M");d($u.l(\'O\').E.5p[0]!=\'\'){5M=5M.2b($u.l(\'O\').E.5p[0],$u.l(\'O\').E.5p[1])}d(6S){6Q+=\'<k M="1m:1s" c1="bO \'+i+\'" l-3d="\'+i+\'" l-4e="\'+b(h).l("4e")+\'"><1j 1y="\'+5M+\'" /></k>\'}J{6Q+=\'<k c1="bO \'+i+\'" l-3d="\'+i+\'" l-4e="\'+b(h).l("4e")+\'"><1j 1y="\'+5M+\'" /></k>\'}i++});d($u.l(\'O\').E.5p[0]!=\'\'){c bN=37 6y($u.l(\'O\').E.5p[0],\'g\');6E=6E.2b(bN,$u.l(\'O\').E.5p[1])}b(\'#8K\').G(6E);b($u.l(\'O\').E.4u).G(6Q);d(6S){c 7i=[];c 8F=T;b($u.l(\'O\').E.4u+\' > k\').1F(q(){1T(c j=0;j<b(h).I(\'l-4e\').41(\',\').N;j++){c 4r=b(h).I(\'l-4e\').41(\',\')[j];d(4r==0){b(h).2T(8E);8F=C}d(b.bu(4r,7i)==-1){7i.7y(4r)}}});b(\'#5B 2m\').1F(q(){c 4r=b(h).I(\'2e\');d(b.bu(4r,7i)==-1){d(4r!=0&&4r!=-1){b("#5B 2m[2e=\'"+4r+"\']").1Q()}}});d(!8F){b($u.l(\'O\').E.4u+\' > k\').m(\'1m\',\'2o\');b("#5B 2m[2e=\'0\']").1Q()}b(\'#5B\').m(\'1m\',\'2o\');b(\'#6G\').m(\'1e\',\'86%\');b("#5B").6x("4p",q(e){c fH=b("2m:3X",h);c 8B=h.2e;d(8B==\'-1\'){b($u.l(\'O\').E.4u+\' > k\').2T(3O)}J{b($u.l(\'O\').E.4u+\' > k\').1t(3O,q(){1T(c j=0;j<b(h).I(\'l-4e\').41(\',\').N;j++){d(8B==b(h).I(\'l-4e\').41(\',\')[j]){b(h).2T(8E)}}})}})}c 8A=T;d(b.Q.fC==\'1.11.0\'){8A=C}d(8A){b($u.l(\'O\').E.4u+\' > k\').1r({3u:\'4q\',3y:q(){R b("<k w=\'fB\'></k>")[0]},bj:2D,1g:q(1A,Q){$u.1J("k").1F(q(){d(b(h).1J("1j").N==1){b(h).1Q()}})}})}J{b($u.l(\'O\').E.4u+\' > k\').1r({3u:\'4q\',3y:"bi",bh:q(1A,Q){b(Q.3y).m("4v","36");b(Q.3y).m("3P-1q","b6");b(Q.3y).m("5o-8y","1z-5o");b(Q.3y).m("L","eZ");b(Q.3y).m("1e","b6");b(Q.3y).m("1z","72(8w,8w,8w,0.9) 5Z 70");c D=2C.D;d(D==\'5l\')D=1;d(D==2J)D=1;D=D+\'\';d(D.1C(\'%\')!=-1){D=D.2b(\'%\',\'\')/22}d(D==\'5R\'){D=1}D=D*1;c 2K=b(1h).4y();c 2Z=b(1A.1K).3G().1q;c 2Q=b(1A.1K).3G().V;c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();c 2Y=C;d(26||1X)2Y=T;d(2Y){c 8v=0;c 8u=60;c 8t=(5*D-1)/4;b(Q.3y).m("4a-1q",(1A.a0+8v-((1A.a0+8v)*8t))+(2K+0-((2K+0)*8t)));b(Q.3y).m("4a-V",1A.9Z+8u-((1A.9Z+8u)*D))}J{d(1X){}d(26){}}},bj:2D,1g:q(1A,Q){$u.1J("k").1F(q(){d(b(h).1J("1j").N==1){b(h).1Q()}})}})}})}$u.1J("*").53("<k w=\'Q-1r\'></k>");$u.1J("*").2w(\'<k w="1k-28">\'+\'<k w="1k-56"><i w="cb-1a-4q"></i></k>\'+\'<k w="1k-G"><i w="cb-1a-1P"></i></k>\'+\'<k w="1k-57"><i w="cb-1a-5w"></i></k>\'+\'<k w="1k-1Q"><i w="cb-1a-5y"></i></k>\'+\'</k>\');d(b(\'#2c-O\').N==0){b(\'#2x\').2w(\'<k F="2c-O" M="1m: 1s"></k>\')}c $1h=b(1h);c 7I=$1h.L();c 3B=ek;d(7I<8s){3B=ee}d(h.E.c3==\'2k\'){b(\'#2F\').m(\'L\',3B+\'1x\');b(\'#2F\').m(\'2k\',\'-\'+3B+\'1x\');b("#3r").W(\'H\');b("#3r").H(q(e){$u.l(\'O\').5P();d(2g(b(\'#2F\').m(\'2k\'))==0){b(\'#2F\').4W({2k:\'-=\'+3B+\'1x\'},3O);b("#3r i").I(\'w\',\'cb-1a-V-3Y-5e\')}J{b(\'#2F\').4W({2k:\'+=\'+3B+\'1x\'},3O);b("#3r i").I(\'w\',\'cb-1a-2k-3Y-5e\')}e.2a()});b(\'.1k-28\').m(\'2k\',\'5f\');d(7I<8s){b(\'.1k-28\').m(\'V\',\'-6R\')}J{b(\'.1k-28\').m(\'V\',\'-9V\')}d(h.E.9t){d(b(\'#2F\').I(\'l-3d-3Y\')!=1){b(\'#2F\').I(\'l-3d-3Y\',1);b(\'#2F\').4W({2k:\'+=\'+3B+\'1x\'},9S);b("#3r i").I(\'w\',\'cb-1a-2k-3Y-5e\')}}}J{b(\'#2F\').m(\'L\',3B+\'1x\');b(\'#2F\').m(\'V\',\'-\'+3B+\'1x\');b(\'#3r\').3f(\'e8\');b("#3r").W(\'H\');b("#3r").H(q(e){$u.l(\'O\').5P();d(2g(b(\'#2F\').m(\'V\'))==0){b(\'#2F\').4W({V:\'-=\'+(3B+0)+\'1x\'},3O);b("#3r i").I(\'w\',\'cb-1a-2k-3Y-5e\')}J{b(\'#2F\').4W({V:\'+=\'+(3B+0)+\'1x\'},3O);b("#3r i").I(\'w\',\'cb-1a-V-3Y-5e\')}e.2a()});b(\'.1k-28\').m(\'V\',\'5f\');d(7I<8s){b(\'.1k-28\').m(\'2k\',\'-6R\')}J{b(\'.1k-28\').m(\'2k\',\'-9V\')}d(h.E.9t){d(b(\'#2F\').I(\'l-3d-3Y\')!=1){b(\'#2F\').I(\'l-3d-3Y\',1);b(\'#2F\').4W({V:\'+=\'+3B+\'1x\'},9S);b("#3r i").I(\'w\',\'cb-1a-V-3Y-5e\')}}}h.4n();h.E.4c();$u.6W({e3:q(1A,Q){c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();d(26){Q.3y.m({\'1m\':\'1s\'})}d($u.l(\'O\').E.a1){Q.3y.m({\'1m\':\'1s\'})}},dY:\'.Q-1r\',dW:\'.7R\',\'dU\':5,5q:\'y\',9R:\'4x\',56:\'.1k-56\',dK:3O,3u:\'4q\',74:\'2o-74\',75:q(e,Q){b(Q.74).dE(80);6X=T},4p:q(e,Q){b(Q.74).2E().dn(80)},dm:q(1A,Q){6X=C;c 8p=T;d(Q.5z.U(\'.1k-28\').N==0){8p=C}d(Q.5z.3V().I(\'F\')==$u.I(\'F\')){Q.5z.4R(Q.5z.G());$u.1J("*").1F(q(){d(!b(h).8o(\'Q-1r\')){b(h).53("<k w=\'Q-1r\'></k>")}});$u.1J(\'.Q-1r\').1F(q(){d(b(h).U(\'.1k-28\').N==0){b(h).2w(\'<k w="1k-28">\'+\'<k w="1k-56"><i w="cb-1a-4q"></i></k>\'+\'<k w="1k-G"><i w="cb-1a-1P"></i></k>\'+\'<k w="1k-57"><i w="cb-1a-5w"></i></k>\'+\'<k w="1k-1Q"><i w="cb-1a-5y"></i></k>\'+\'</k>\')}});$u.1J(\'.Q-1r\').1F(q(){d(b(h).1J(\'*\').N==1){b(h).1Q()}d(b(h).1J(\'*\').N==2){d(b(h).1J(0).3C("3T").1L()==\'1j\'&&b(h).1J(0).I(\'1y\').1C(\'gD/\')!=-1){b(h).1Q()}}})}$u.l(\'O\').4n();$u.l(\'O\').E.4c();d(8p)$u.l(\'O\').E.c5(1A,Q)}});d(2D.1C(\',\')!=-1){b(2D).6W(\'2m\',\'5q\',T)}d(h.E.5q!=\'\'){b(2D).6W(\'2m\',\'5q\',h.E.5q)}b.Q.9I=q(x,8n,39){R(x>=8n)&&(x<(8n+39))};b.Q.fO=q(y,x,1q,V,1e,L){R b.Q.9I(y,1q,1e)&&b.Q.fI(x,V,L)};$u.ff({eK:q(1A,Q){d(b(Q.1r).l(\'3d\')){c 3d=b(Q.1r).l(\'3d\');c aj=b(\'#3d\'+3d).1n();b(Q.1r).l(\'3d\',1S);R Q.1r.G(aj);1A.2a()}},9R:\'4x\',eb:C});b(K).1I(\'5a\',q(1A){d(b(1A.1K).I("w")==\'4S\'){b(1A.1K).m(\'z-4m\',\'-1\')}d(b(1A.1K).X(\'.Q-1r\').N>0&&b(1A.1K).X(2D).N>0){c 26=2f.2r.1L().1C(\'3q\')>-1;b(".Q-1r").3L(\'1P\');d(b(1A.1K).X("[l-1W=\'1P\']").N>0){b(1A.1K).X(".Q-1r").3f(\'1P\')}d(b(1A.1K).X("[l-1W=\'3D\']").N>0){b(1A.1K).X(".Q-1r").3f(\'1P\')}b(".Q-1r").3L(\'Q-5Y-5X\');b(1A.1K).X(".Q-1r").3f(\'Q-5Y-5X\');d(26)b(1A.1K).X(".Q-1r").3f(\'3q\');b(\'.1k-28\').1g(C,C).1t(0);d(b(1A.1K).X(".Q-1r").U("[l-G-5c=\'9X\']").N>0||!$u.l(\'O\').E.9c){b(1A.1K).X(".Q-1r").U(\'.1k-28 .1k-G\').m({1m:\'1s\'})}b(1A.1K).X(".Q-1r").U(\'.1k-28\').1g(C,C).m({1m:\'1s\'}).2T(7J);R}d(b(1A.1K).3V().I(\'F\')==\'S-1v\'||b(1A.1K).3V().3V().I(\'F\')==\'S-1v\'){R}d(b(1A.1K).5j(\'[25]\')||b(1A.1K).m(\'3w\')==\'4U\'||b(1A.1K).m(\'3w\')==\'7O\'){R}b(1A.1K).X().1F(q(e){d(b(h).5j(\'[25]\')||b(h).m(\'3w\')==\'4U\'||b(h).m(\'3w\')==\'7O\'){R}});$u.l(\'O\').5P()})};h.G=q(){c 3g=h.E.3g;b(\'#2c-O\').G($u.G());b(\'#2c-O\').U(\'.1k-28\').1Q();b(\'#2c-O\').U(\'.4S\').1Q();b(\'#2c-O\').U(\'[25]\').2U(\'25\');b(\'*[w=""]\').2U(\'w\');b(\'#2c-O\').U(\'.Q-1r\').4R(q(){R b(h).G()});b("#2c-O").U("[l-1W=\'1P\']").1F(q(){d(b(h).I("l-G")!=2J){b(h).G(8d(b(h).I("l-G")))}});c G=b(\'#2c-O\').G().5n();G=G.2b(/<1Z/g,\'<1V\').2b(/<\\/1Z/g,\'</1V\');R G};h.D=q(n){h.E.D=n;b(2D).m(\'D\',n);b(2D).m(\'-6s-8S\',\'8R(\'+n+\')\');2C.D=n;h.5P()};h.5P=q(){b(\'.1k-28\').1g(C,C).1t(0);b(".Q-1r").3L(\'1P\');b(".Q-1r").3L(\'Q-5Y-5X\');c 3g=h.E.3g;$u.U(3g).4g()};h.9L=q(){b(\'#B-G\').m(\'L\',\'45%\');b(\'#B-G\').1i();b(\'#B-G\').l(\'1i\').2s();b(\'#5k\').1d(h.G());b(\'#5V\').W(\'H\');b(\'#5V\').1I(\'H\',q(e){$u.G(b(\'#5k\').1d());b(\'#B-G\').l(\'1i\').2E();$u.1J("*").53("<k w=\'Q-1r\'></k>");$u.1J("*").2w(\'<k w="1k-28">\'+\'<k w="1k-56"><i w="cb-1a-4q"></i></k>\'+\'<k w="1k-G"><i w="cb-1a-1P"></i></k>\'+\'<k w="1k-57"><i w="cb-1a-5w"></i></k>\'+\'<k w="1k-1Q"><i w="cb-1a-5y"></i></k>\'+\'</k>\');$u.l(\'O\').4n();$u.l(\'O\').E.4c()})};h.gs=q(G){$u.G(G);$u.1J("*").53("<k w=\'Q-1r\'></k>");$u.1J("*").2w(\'<k w="1k-28">\'+\'<k w="1k-56"><i w="cb-1a-4q"></i></k>\'+\'<k w="1k-G"><i w="cb-1a-1P"></i></k>\'+\'<k w="1k-57"><i w="cb-1a-5w"></i></k>\'+\'<k w="1k-1Q"><i w="cb-1a-5y"></i></k>\'+\'</k>\');$u.l(\'O\').4n();$u.l(\'O\').E.4c()};h.4n=q(){$u.U(\'a\').H(q(){R T});$u.U("[l-1W=\'1P\']").1F(q(){d(b(h).I("l-G")!=2J){b(h).G(8d(b(h).I("l-G")))}});c 3g=h.E.3g;c 8b=h.E.4h;c 3Q=h.E.3Q;c 3S=h.E.3S;c 3E=h.E.3E;c 1v=h.E.1v;c 4V=h.E.4V;c 33=h.E.33;c 27=h.E.27;$u.1w({27:27,4d:3g,3S:3S,3E:3E,1v:1v,4V:4V});$u.l(\'1w\').47();$u.U(\'1j\').1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;b(h).2M({4h:8b,33:33,27:27,3Q:3Q});d(b(h).X(\'63\').N!=0){d(b(h).X(\'63\').U(\'89\').m(\'3w\')==\'4U\'){b(h).X(\'63\').2M({4h:8b,33:33,27:27,3Q:3Q})}}});$u.U(".5u-76").1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;d(b(h).U(\'.4S\').N==0){b(h).2w(\'<k w="4S" M="3w:4U;1G:#49;88:0.2;3u:4x;1q:0;V:3v;L:22%;1e:22%;z-4m:-1"></k>\')}});$u.U(".5u-76").38(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;d(b(h).X(".Q-1r").m(\'3p-M\')==\'1s\'){b(h).U(\'.4S\').m(\'z-4m\',\'1\')}},q(){b(h).U(\'.4S\').m(\'z-4m\',\'-1\')});$u.U(3g).W(\'87\');$u.U(3g).87(q(){c D=$u.l(\'O\').E.D;c 3g=$u.l(\'O\').E.3g;c 26=2f.2r.1L().1C(\'3q\')>-1;b(".Q-1r").3L(\'1P\');d(b(h).X("[l-1W=\'1P\']").N>0){b(h).X(".Q-1r").3f(\'1P\')}d(b(h).X("[l-1W=\'3D\']").N>0){b(h).X(".Q-1r").3f(\'1P\')}b(".Q-1r").3L(\'Q-5Y-5X\');b(h).X(".Q-1r").3f(\'Q-5Y-5X\');d(26)b(h).X(".Q-1r").3f(\'3q\');b(\'.1k-28\').1g(C,C).1t(0);d(b(h).X(".Q-1r").U("[l-G-5c=\'9X\']").N>0||!$u.l(\'O\').E.9c){b(h).X(".Q-1r").U(\'.1k-28 .1k-G\').m({1m:\'1s\'})}b(h).X(".Q-1r").U(\'.1k-28\').1g(C,C).m({1m:\'1s\'}).2T(7J)});$u.1J("k").U(\'.1k-1Q\').W();$u.1J("k").U(\'.1k-1Q\').H(q(){b(\'#B-5J\').m(\'3i-L\',\'ax\');b(\'#B-5J\').1i();b(\'#B-5J\').l(\'1i\').2s();$2P=b(h).X(\'.Q-1r\');b(\'#82\').W(\'H\');b(\'#82\').1I(\'H\',q(e){b(\'#B-5J\').l(\'1i\').2E();$2P.1t(8E,q(){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b("#2W").1g(C,C).1t(0);b("#3F").1g(C,C).1t(0);$2P.1Q();$u.l(\'O\').E.4c()})});b(\'#81\').W(\'H\');b(\'#81\').1I(\'H\',q(e){b(\'#B-5J\').l(\'1i\').2E()})});$u.1J("k").U(\'.1k-57\').W();$u.1J("k").U(\'.1k-57\').H(q(){$2P=b(h).X(\'.Q-1r\');b(\'#2c-O\').G($2P.G());b(\'#2c-O\').U(\'[25]\').2U(\'25\');b(\'#2c-O *[w=""]\').2U(\'w\');b(\'#2c-O *[M=""]\').2U(\'M\');b(\'#2c-O .4S\').1Q();b(\'#2c-O .1k-28\').1Q();c G=b(\'#2c-O\').G().5n();$2P.gN(G);$u.1J("*").1F(q(){d(!b(h).8o(\'Q-1r\')){b(h).53("<k w=\'Q-1r\'></k>")}});$u.1J(\'.Q-1r\').1F(q(){d(b(h).U(\'.1k-28\').N==0){b(h).2w(\'<k w="1k-28">\'+\'<k w="1k-56"><i w="cb-1a-4q"></i></k>\'+\'<k w="1k-G"><i w="cb-1a-1P"></i></k>\'+\'<k w="1k-57"><i w="cb-1a-5w"></i></k>\'+\'<k w="1k-1Q"><i w="cb-1a-5y"></i></k>\'+\'</k>\')}});$u.1J(\'.Q-1r\').1F(q(){d(b(h).1J(\'*\').N==1){b(h).1Q()}});$u.l(\'O\').4n();$u.l(\'O\').E.4c()});$u.1J("k").U(\'.1k-G\').W();$u.1J("k").U(\'.1k-G\').H(q(){b(\'#B-G\').m(\'L\',\'45%\');b(\'#B-G\').1i();b(\'#B-G\').l(\'1i\').2s();$2P=b(h).X(\'.Q-1r\').1J(\'*\').6i(\'.1k-28\');d($2P.l(\'1W\')==\'1P\'&&$2P.I(\'l-G\')!=2J){b(\'#5k\').1d(8d($2P.I(\'l-G\')))}J{b(\'#2c-O\').G($2P.G());b(\'#2c-O\').U(\'[25]\').2U(\'25\');b(\'#2c-O *[w=""]\').2U(\'w\');b(\'#2c-O *[M=""]\').2U(\'M\');b(\'#2c-O .4S\').1Q();c G=b(\'#2c-O\').G().5n();G=G.2b(/<1Z/g,\'<1V\').2b(/<\\/1Z/g,\'</1V\');b(\'#5k\').1d(G)}b(\'#5V\').W(\'H\');b(\'#5V\').1I(\'H\',q(e){d($2P.l(\'1W\')==\'1P\'){$2P.I(\'l-G\',fd(b(\'#5k\').1d()));$2P.G(\'\')}J{$2P.G(b(\'#5k\').1d())}b(\'#B-G\').l(\'1i\').2E();$u.l(\'O\').4n();$u.l(\'O\').E.4c()})})};h.9O=q(){d(!$u.l(\'O\'))R;c cn=$u.l(\'O\').G();$u.G(cn);$u.6W("9O");c 7n=2D.41(","),7Z=[];1T(c i=0;i<7n.N;i++){d(7n[i]!="#"+$u.I("F")){7Z.7y(7n[i])}}2D=7Z.di(",");$u.3L(\'7R\');$u.m({\'8M-1e\':\'\'});d(2D==""){b(\'#2x\').1Q()}$u.bp(\'O\');$u.bp(\'1w\');$u.W();b(K).W(\'5a\')};h.4M()};b.fn.O=q(2O){R h.1F(q(){d(2J==b(h).l(\'O\')){c 4G=37 b.O(h,2O);b(h).l(\'O\',4G)}})}})(b);(q(b){c $1U;c $3I;c $6q;c 2t=[];q en(){};b.fn.7W=q(){};b.1w=q(u,2O){c 4E={4d:"52,5i,4X,54,4Z,51,p,9x,c7,c6,.5c",3E:"5g",3o:T,4c:q(){},3p:T,27:\'\',1v:\'1q\',4V:\'5f\',3S:["#bk","#bf","#bd","#bb","#ba","#b7","#b5","#b4","#93","#aU","#aS","#aO","#aN","#aL","#aK","#aI","#aH","#aG","#aF","#aC","#aB","#aA","#az","#as","#aq","#ap","#am","#ak","#ag","#ae","#a7","#a6","#a5","#a4","#a3","#a2"]};h.E={};c $u=b(u),u=u;h.4M=q(){h.E=b.7o({},4E,2O);c 4C=T;d(h.E.27!=\'\')4C=C;d(b(\'#2x\').N==0){b(\'2i\').2w(\'<k F="2x"></k>\')}c 7A=\'\';d(h.E.1v==\'V\')7A=\' w="S-at"\';d(h.E.1v==\'2k\')7A=\' w="S-at 2k"\';c 7C=\'<k F="S-1v"\'+7A+\'>\'+\'<a 1O="#" l-S-1B="7V"> <i w="cb-1a-7V"></i> </a>\'+\'<a 1O="#" l-S-1B="7U"> <i w="cb-1a-7U"></i> </a>\'+\'<a 1O="#" l-S-1B="5A"> <i w="cb-1a-5A"></i> </a>\'+\'<a 1O="#" l-S-1B="bm"> <i w="cb-1a-fa"></i> </a>\'+\'<a 1O="#" l-S-1B="1p"> <i w="cb-1a-1p"></i> </a>\'+\'<a 1O="#" l-S-1B="3R"> <i w="cb-1a-3R"></i> </a>\'+\'<a 1O="#" l-S-1B="6D"> <i w="cb-1a-aM"></i> </a>\'+\'<a 1O="#" l-S-1B="8N"> <i w="cb-1a-dP"></i> </a>\'+\'<a 1O="#" l-S-1B="1Z"> <i w="cb-1a-1Z"></i> </a>\'+\'<a 1O="#" l-S-1B="2l"> <i w="cb-1a-2l-8U"></i> </a>\'+\'<a 1O="#" l-S-1B="2G"> <i w="cb-1a-2G-aw" M="1Z-39:dD;cJ-1e:1.3"></i> </a>\'+\'<a 1O="#" l-S-1B="7Q"> <i w="cb-1a-3N"></i> </a>\'+\'<a 1O="#" l-S-1B="6I"> <i w="cb-1a-6I"></i> </a>\'+\'<a 1O="#" l-S-1B="G"> <i w="cb-1a-1P"></i> </a>\'+\'</k>\'+\'\'+\'<k F="2W">\'+\'<i w="cb-1a-3N"></i> aR bz\'+\'</k>\'+\'\'+\'<k F="3F">\'+\'<i w="cb-1a-3N"></i> aR bz\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-4l">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<k w="B-4z">7T:</k>\'+(4C?\'<1u 1M="1n" F="3W" w="42" M="4i:V;L:50%;" 2e="3c:/\'+\'/"></1u><i w="cb-1a-3N B-7X" F="7Y" M="L:10%;"></i>\':\'<1u 1M="1n" F="3W" w="42" 2e="3c:/\'+\'/" M="4i:V;L:60%"></1u>\')+\'<br M="7w:7q">\'+\'<k w="B-4z">7p:</k>\'+\'<1u 1M="1n" F="6l" w="42" M="4i:2k;L:60%"></1u>\'+\'<br M="7w:7q">\'+\'<k w="B-4z">dN:</k>\'+\'<4z M="4i:V;" 1T="4H" w="ea"><1u 1M="9J" F="4H"></1u> f0 f3</4z>\'+\'</k>\'+\'<k w="B-7l">\'+\'<1f F="6h"> 5r </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-6b">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<1u 1M="1n" F="7b" w="42" 2e="3c:/\'+\'/"></1u>\'+\'</k>\'+\'<k w="B-7l">\'+\'<1f F="84"> 5r </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-2l" M="1G:#49;3P:3s 3v 3s 3s;1z-2q:30">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<1f w="B-5Q" l-2l="V"> <i w="cb-1a-2l-V"></i> <1V>ei</1V> </1f>\'+\'<1f w="B-5Q" l-2l="66"> <i w="cb-1a-2l-66"></i> <1V>eC</1V> </1f>\'+\'<1f w="B-5Q" l-2l="2k"> <i w="cb-1a-2l-2k"></i> <1V>eS</1V> </1f>\'+\'<1f w="B-5Q" l-2l="8U"> <i w="cb-1a-2l-8U"></i> <1V>eV</1V> </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-2G" M="1G:#49;3P:3s 3v 3s 3s;1z-2q:30">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<1f w="B-59 bw" l-2G="8a" M="4a-2k:3v"> <i w="cb-1a-8a-V"></i> </1f>\'+\'<1f w="B-59 bw" l-2G="5h"> <i w="cb-1a-8a-2k"></i> </1f>\'+\'<1f w="B-59" l-2G="fx"> <i w="cb-1a-2G-aw"></i> <1V>fD</1V> </1f>\'+\'<1f w="B-59" l-2G="fE"> <i w="cb-1a-2G-fJ"></i> <1V>fR</1V> </1f>\'+\'<1f w="B-59" l-2G="5l"> <i w="cb-1a-5y"></i> <1V>fW</1V> </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-4T" M="1z-2q:30">\'+\'<k w="B-34" M="1z-2q:30">\'+\'<k w="B-2i">\'+\'<2S F="8c" M="L:22%;1e:dq;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\'+\'<1f w="B-8e" l-1Z-5x="" l-7v="" M="1m:1s"></1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-3R" M="1z-2q:30">\'+\'<k w="B-34" M="1z-2q:30">\'+\'<k w="B-2i">\'+\'<2S F="8f" M="L:22%;1e:es;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\'+\'<1f w="B-8g" l-1Z-39="" M="1m:1s"></1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-58" M="1z-2q:30">\'+\'<k w="B-34" M="1z-2q:30">\'+\'<k w="B-2i">\'+\'<2S F="8h" M="L:22%;1e:eT;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\'+\'<1f w="B-8i" l-bl="" M="1m:1s"></1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-1p" M="1G:#49;3P:3s 3v 3s 3s;1z-2q:30">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<k M="L:22%">\'+\'<2B F="8j" M="L:85%"><2m 2e="1">7p f8</2m><2m 2e="2">bn</2m><2m 2e="3">fc bn</2m></2B>\'+\'<1f F="8k" M="3u: 4x;1G: #fe;1z: 1s;4a: 0 0 0 7t;fp-2l: fs;"><i w="cb-1a-aM" M="1p:#fu;1Z-39:bq"></i></1f>\'+\'</k>\'+\'[bv]\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-G">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<8l F="5k" w="42" M="1e:fF;"></8l>\'+\'</k>\'+\'<k w="B-7l">\'+\'<1f F="5V"> 5r </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-27">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+(4C?\'<2S F="7s" M="L:22%;1e:8m;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\':\'\')+\'</k>\'+\'</k>\'+\'</k>\'+\'<1u 1M="36" F="5v-1u" />\'+\'\'+\'<k w="B-2X" F="B-5J">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<k M="3P:30 64 bq;1n-2l:66;">\'+\'<p>fU cv g1 cv gc bI gm h 2o?</p>\'+\'<1f w="5s 5s-5g" F="81"> gy </1f>\'+\'<1f w="5s 5s-gB" F="82" M="4a-V:30"> gC </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k F="2c-1w"></k>\'+\'\';c 7j=\'\';1T(c i=0;i<h.E.3S.N;i++){d(h.E.3S[i]==\'#93\'){7j+=\'<1f w="B-7g" M="1G:\'+h.E.3S[i]+\';1z:#gO 2R 70"></1f>\'}J{7j+=\'<1f w="B-7g" M="1G:\'+h.E.3S[i]+\';1z:\'+h.E.3S[i]+\' 2R 70"></1f>\'}}7C=7C.2b(\'[bv]\',7j);d(b(\'#S-1v\').N==0){b(\'#2x\').2w(7C);h.55(\'7V\');h.55(\'7U\');h.55(\'5A\');h.55(\'bm\');h.55(\'dk\');h.55(\'dl\')}c 79=T;$u.1I(\'78\',q(e){$u.l(\'1w\').8q()});$u.1I(\'dz\',q(e){$u.l(\'1w\').8q()});b(K).6x("dA",\'#\'+$u.I(\'F\'),q(e){8r($3I)});$u.1I(\'6a\',q(e){d(e.5I==46||e.5I==8){c el;3K{d(1h.1o){el=1h.1o().21(0).2j.1Y}J d(K.1c){el=K.1c.24().2u()}d(el.1E.1L()==\'p\'){c t=\'\';d(1h.1o){t=1h.1o().9Y()}J d(K.1o){t=K.1o().9Y()}J d(K.1c){t=K.1c.24().1n}d(t==el.ep){b(el).G(\'<br>\');R T}}}3J(e){}}d(e.5I==17){79=C;R}d((e.5I==86&&79==C)||(e.5I==86&&e.eA)){8r($3I)}d(e.eB){d(e.4Y==65||e.4Y==97){e.2a();c 1X=3A();c el;3K{d(1h.1o){el=1h.1o().21(0).2j.1Y}J d(K.1c){el=K.1c.24().2u()}}3J(e){R}d(1X){c Y=K.2i.eM();Y.eO(el);Y.2B()}J{c Y=K.24();Y.eQ(el);c 1R=1h.1o();1R.6p();1R.6H(Y)}}}}).78(q(e){d(e.5I==17){79=T}});b(K).6x(\'5a\',q(1A){c 6Y=T;d(b(\'#S-1v\').m(\'1m\')==\'1s\')R;c el=b(1A.1K).3C("3T").1L();b(1A.1K).X().1F(q(e){d(b(h).5j(\'[25]\')||b(h).8o(\'B-2X\')||b(h).I(\'F\')==\'2x\'){6Y=C;R}});d(b(1A.1K).5j(\'[25]\')){6Y=C;R}d(!6Y){$3I=1S;d($u.l(\'1w\').E.4V==\'5f\'){b(\'#S-1v\').m(\'1m\',\'1s\')}d($u.l(\'1w\').E.3p){1T(c i=0;i<2t.N;i++){b(2t[i]).m(\'3p\',\'\');b(2t[i]).U(\'*\').m(\'3p\',\'\')}}$u.l(\'O\').5P()}})};h.8q=q(){c 1X=3A();c el;3K{d(1h.1o){el=1h.1o().21(0).2j.1Y}J d(K.1c){el=K.1c.24().2u()}}3J(e){R}d(b(el).X("[l-1W=\'1P\']").N>0)R;d(b(el).X("[l-1W=\'3D\']").N>0)R;d(el.1E.1L()==\'a\'){d(1X){}J{}b("#2W").3f(\'8x\')}J{b("#2W").3L(\'8x\')}};h.47=q(){c D;d(2C.8T("D")!=1S){D=2C.D}J{D=$u.m(\'D\')}d(D==2J)D=1;2C.D=D;c 4d=$u.l(\'1w\').E.4d;d(4d==\'\'){$u.I(\'25\',\'C\');$u.W(\'5a\');$u.1I(\'5a\',q(e){$3I=b(h);b("#S-1v").1g(C,C).2T(3O);d($u.l(\'1w\').E.3p){1T(c i=0;i<2t.N;i++){b(2t[i]).m(\'3p\',\'\');b(2t[i]).U(\'*\').m(\'3p\',\'\')}b(h).m(\'3p\',\'72(0, 0, 0, 0.43) aQ 2R\')}})}J{$u.U(4d).1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;c 3E=$u.l(\'1w\').E.3E;d(3E==\'5g\'){}J{c I=b(h).I(\'25\');d(5D I!==5D 2J&&I!==T){}J{b(h).I(\'25\',\'C\')}}});$u.U(4d).W(\'5a\');$u.U(4d).1I(\'5a\',q(e){$3I=b(h);b("#S-1v").1g(C,C).2T(3O);d($u.l(\'1w\').E.3p){1T(c i=0;i<2t.N;i++){b(2t[i]).m(\'3p\',\'\');b(2t[i]).U(\'*\').m(\'3p\',\'\')}b(h).m(\'3p\',\'72(0, 0, 0, 0.43) aQ 2R\')}});$u.U(\'.5c\').U(4d).2U(\'25\')}$u.U(\'a\').1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;b(h).I(\'25\',\'C\')});c 3E=$u.l(\'1w\').E.3E;d(3E==\'5g\'){$u.U("52,5i,4X,54,4Z,51").W(\'6a\');$u.U("52,5i,4X,54,4Z,51").1I(\'6a\',q(e){d(e.4Y==13){c 1X=3A();d(1X&&1X<=10){c 1R=K.1c.24();d(1R.2u){1R.aT(\'<br>\');e.aV=C;e.aX=T;1R.2B();1R.aY("6V",1);1R.bc("6V",1);1R.5C(T);R T}}J{c 1R=1h.1o();c Y=1R.21(0);Y.7S();Y.5C(C);c 4t=Y.6z(\'<br>\');c 3j=4t.8z;Y.6K(4t);Y.6j(3j);Y.5S(3j);d(Y.7B.8C==1){d(Y.bs==Y.7B.bt.N-1){Y.6K(Y.6z("<br />"));Y.6j(3j);Y.5S(3j)}}c 3t=Y.2j;d(3t&&3t.1Y){3K{3t.1Y.8D()}3J(e){}}1R.6p();1R.6H(Y);R T}}});$u.U("52,5i,4X,54,4Z,51,p,1j").1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;b(h).3V().I(\'25\',C)});$u.U(".g0").1F(q(){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;b(h).I(\'25\',C)});$u.U("k").W(\'78\');$u.U("k").1I(\'78\',q(e){c el;c 1H;d(1h.1o){1H=1h.1o().21(0).2j;el=1h.1o().21(0).2j.1Y}J d(K.1c){1H=K.1c.24();el=K.1c.24().2u()}d(e.4Y==13){c 1X=3A();d(1X>0){}J{c by=/gh/.5d(2f.2r)&&/gn gq/.5d(2f.bD);c gu=/gv/.5d(2f.2r)&&/gw gx/.5d(2f.bD);c bH=1h.bK;c bL=2f.2r.1L().1C(\'3q\')>-1;d(by||bH){d(b(el).3C("3T").1L()==\'p\'||b(el).3C("3T").1L()==\'k\'){K.2h(\'7a\',T,\'<p>\')}}d(bL){d(!b(1H).G())K.2h(\'7a\',T,\'<p>\')}}}d(e.4Y==8||e.4Y==46){d(b(el).3C("3T").1L()==\'k\'){K.2h(\'7a\',T,\'<p>\')}}})}J{$u.U("p").W(\'6a\');$u.U("p").1I(\'6a\',q(e){d(e.4Y==13&&$u.U("bS").N==0){c bY=2f.2r.1L();c c0=(bY.1C(\'6F\')>=0)?C:T;d(c0){c 1R=K.1c.24();d(1R.2u){1R.aT(\'<br>\');e.aV=C;e.aX=T;1R.2B();1R.aY("6V",1);1R.bc("6V",1);1R.5C(T);R T}}J{c 1R=1h.1o();c Y=1R.21(0);Y.7S();Y.5C(C);c 4t=Y.6z(\'<br>\');c 3j=4t.8z;Y.6K(4t);Y.6j(3j);Y.5S(3j);d(Y.7B.8C==1){d(Y.bs==Y.7B.bt.N-1){Y.6K(Y.6z("<br />"));Y.6j(3j);Y.5S(3j)}}c 3t=Y.2j;d(3t&&3t.1Y){3K{3t.1Y.8D()}3J(e){}}1R.6p();1R.6H(Y);R T}}})}b(\'#S-1v a[l-S-1B="cg"]\').W(\'H\');b(\'#S-1v a[l-S-1B="cg"]\').H(q(e){$3I.1Q();$u.l(\'1w\').E.3o=C;$u.l(\'1w\').47();e.2a()});b(\'#S-1v a[l-S-1B="1p"]\').W(\'H\');b(\'#S-1v a[l-S-1B="1p"]\').H(q(e){c 2n=4A();b(\'#B-1p\').m(\'3i-L\',\'dp\');b(\'#B-1p\').1i();b(\'#B-1p\').l(\'1i\').2s();e.2a();c 1n=8G();b(\'.B-7g\').W(\'H\');b(\'.B-7g\').H(q(){4s(2n);c el;c 1H;d(1h.1o){1H=1h.1o().21(0).2j;el=1h.1o().21(0).2j.1Y}J d(K.1c){1H=K.1c.24();el=K.1c.24().2u()}c 2N=b(\'#8j\').1d();d(b.5n(1n)!=\'\'&&b(1H).1n()!=1n){d(2N==1){K.2h("cs",T,b(h).m("1G-1p"))}d(2N==2){K.2h("cu",T,b(h).m("1G-1p"))}c 2V=K.5t("1Z");1T(c i=0,3z=2V.N;i<3z;++i){c s=2V[i].1p;d(s!=\'\'){2V[i].7e("1p");2V[i].M.1p=s}}c 1X=3A();d(1X){$3I.U(\'1V\').1F(q(){d(b(h).U(\'1V\').N==1){d(b(h).1n()==b(h).U(\'1V:3n\').1n()){c cz=b(h).U(\'1V:3n\').I(\'M\');b(h).G(b(h).U(\'1V:3n\').G());c cF=b(h).I(\'M\')+\';\'+cz;b(h).I(\'M\',cF)}}})}}J d(b(1H).1n()==1n){d(2N==1){d(b(1H).G()){b(1H).m(\'1p\',b(h).m("1G-1p"))}J{b(1H).3V().m(\'1p\',b(h).m("1G-1p"))}}d(2N==2){d(b(1H).G()){b(1H).m(\'1G-1p\',b(h).m("1G-1p"))}J{b(1H).3V().m(\'1G-1p\',b(h).m("1G-1p"))}}}J{d(2N==1){b(el).m(\'1p\',b(h).m("1G-1p"))}d(2N==2){b(el).m(\'1G-1p\',b(h).m("1G-1p"))}};d(2N==3){b(el).X(\'.Q-1r\').1J().3n().m(\'1G-1p\',b(h).m("1G-1p"))}b(\'#B-1p\').l(\'1i\').2E()});b(\'#8k\').W(\'H\');b(\'#8k\').H(q(){4s(2n);c el;c 1H;d(1h.1o){1H=1h.1o().21(0).2j;el=1h.1o().21(0).2j.1Y}J d(K.1c){1H=K.1c.24();el=K.1c.24().2u()}c 2N=b(\'#8j\').1d();d(b.5n(1n)!=\'\'&&b(1H).1n()!=1n){d(2N==1){K.2h("cs",T,\'\')}d(2N==2){K.2h("cu",T,\'\')}c 2V=K.5t("1Z");1T(c i=0,3z=2V.N;i<3z;++i){c s=2V[i].1p;2V[i].7e("1p");2V[i].M.1p=s}}J d(b(1H).1n()==1n){d(2N==1){d(b(1H).G()){b(1H).m(\'1p\',\'\')}J{b(1H).3V().m(\'1p\',\'\')}}d(2N==2){d(b(1H).G()){b(1H).m(\'1G-1p\',\'\')}J{b(1H).3V().m(\'1G-1p\',\'\')}}}J{d(2N==1){b(el).m(\'1p\',\'\')}d(2N==2){b(el).m(\'1G-1p\',\'\')}};d(2N==3){b(1H).X(\'.Q-1r\').1J().3n().m(\'1G-1p\',\'\')}b(\'#B-1p\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="3R"]\').W(\'H\');b(\'#S-1v a[l-S-1B="3R"]\').H(q(e){c 2n=4A();b(\'#B-3R\').m(\'3i-L\',\'dO\');b(\'#B-3R\').1i();b(\'#B-3R\').l(\'1i\').2s();e.2a();d(b(\'#8f\').I(\'1y\').1C(\'3R.G\')==-1){b(\'#8f\').I(\'1y\',48+\'3R.G\')}c 1n=8G();b(\'.B-8g\').W(\'H\');b(\'.B-8g\').H(q(){4s(2n);c el;c 1H;d(1h.1o){1H=1h.1o().21(0).2j;el=1h.1o().21(0).2j.1Y}J d(K.1c){1H=K.1c.24();el=K.1c.24().2u()}c s=b(h).I(\'l-1Z-39\');d(b.5n(1n)!=\'\'&&b(1H).1n()!=1n){K.2h("cH",T,"7");c 2V=K.5t("1Z");1T(c i=0,3z=2V.N;i<3z;++i){d(2V[i].39=="7"){2V[i].7e("39");2V[i].M.cH=s}}}J d(b(1H).1n()==1n){d(b(1H).G()){b(1H).m(\'1Z-39\',s)}J{b(1H).3V().m(\'1Z-39\',s)}}J{b(el).m(\'1Z-39\',s)};b(h).4g();$u.l(\'1w\').E.3o=C;e.2a();b(\'#B-3R\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="8N"]\').W(\'H\');b(\'#S-1v a[l-S-1B="8N"]\').H(q(e){c 2n=4A();b(\'#B-58\').m(\'3i-L\',\'dQ\');b(\'#B-58\').1i();b(\'#B-58\').l(\'1i\').2s();e.2a();d(b(\'#8h\').I(\'1y\').1C(\'58.G\')==-1){b(\'#8h\').I(\'1y\',48+\'58.G\')}b(\'.B-8i\').W(\'H\');b(\'.B-8i\').H(q(){4s(2n);c s=b(h).I(\'l-bl\');$u.I(\'25\',C);K.2h(\'7a\',T,\'<\'+s+\'>\');$u.2U(\'25\');$u.l(\'1w\').47();b(h).4g();$u.l(\'1w\').E.3o=C;e.2a();b(\'#B-58\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="6D"]\').W(\'H\');b(\'#S-1v a[l-S-1B="6D"]\').H(q(e){K.2h(\'6D\',T,1S);K.2h(\'6D\',T,1S);b(h).4g();$u.l(\'1w\').E.3o=C;e.2a()});b(\'#S-1v a[l-S-1B="6I"]\').W(\'H\');b(\'#S-1v a[l-S-1B="6I"]\').H(q(e){K.2h(\'6I\',T,1S);b("#2W").3L(\'8x\');b(h).4g();$u.l(\'1w\').E.3o=C;e.2a()});b(\'#S-1v a[l-S-1B="G"]\').W(\'H\');b(\'#S-1v a[l-S-1B="G"]\').H(q(e){c el;d(1h.1o){el=1h.1o().21(0).2j.1Y}J d(K.1c){el=K.1c.24().2u()}b(el).X().1F(q(){d(b(h).l(\'O\')){b(h).l(\'O\').9L()}})});b(\'#S-1v a[l-S-1B="1Z"]\').W(\'H\');b(\'#S-1v a[l-S-1B="1Z"]\').H(q(e){c 2n=4A();b(\'#B-4T\').m(\'3i-L\',\'dR\');b(\'#B-4T\').1i();b(\'#B-4T\').l(\'1i\').2s();e.2a();d(b(\'#8c\').I(\'1y\').1C(\'4T.G\')==-1){b(\'#8c\').I(\'1y\',48+\'4T.G\')}b(\'.B-8e\').W(\'H\');b(\'.B-8e\').H(q(){4s(2n);c el;d(1h.1o){el=1h.1o().21(0).2j.1Y;d(el.1E!=\'6N\'&&el.1E!=\'6L\'&&el.1E!=\'7P\'&&el.1E!=\'7M\'&&el.1E!=\'7L\'&&el.1E!=\'7F\'&&el.1E!=\'P\'){el=el.1Y}}J d(K.1c){el=K.1c.24().2u();d(el.1E!=\'6N\'&&el.1E!=\'6L\'&&el.1E!=\'7P\'&&el.1E!=\'7M\'&&el.1E!=\'7L\'&&el.1E!=\'7F\'&&el.1E!=\'P\'){el=el.2u()}}c s=b(h).I(\'l-1Z-5x\');b(el).m(\'1Z-5x\',s);c 3M=s.41(\',\')[0];c 7v=b(h).I(\'l-7v\');d(7v==\'e7\'){c 8I=T;c 8J=K.5t("3N");1T(c i=0;i<8J.N;i++){c 3m=8J[i].1O.1L();3m=3m.2b(/\\+/g,\' \').2b(/%20/g,\' \');d(3m.1C(3M.1L())!=-1)8I=C}d(!8I)$u.2w(\'<3N 1O="3c://4T.9K.6g/m?5x=\'+3M+\'" 6Z="9M" ej="9M" 1M="1n/m">\')}$u.U(\'3N\').1F(q(){c 3m=b(h).I(\'1O\').1L();d(3m.1C(\'9K\')!=-1){3m=3m.2b(/\\+/g,\' \').2b(/%20/g,\' \');c 3M=3m.9N(3m.1C(\'5x=\')+7);d(3M.1C(\':\')!=-1){3M=3M.41(\':\')[0]}d(3M.1C(\'|\')!=-1){3M=3M.41(\'|\')[0]}c 8L=\'\';b(2D).1F(q(){8L+=b(h).l(\'O\').G().1L()});c 7W=8L.41(3M).N;d(7W<3){b(h).I(\'6Z\',\'9P\')}}});$u.U(\'[6Z="9P"]\').1Q();b(h).4g();$u.l(\'1w\').E.3o=C;e.2a();b(\'#B-4T\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="2l"]\').W(\'H\');b(\'#S-1v a[l-S-1B="2l"]\').H(q(e){c 2n=4A();b(\'#B-2l\').m(\'3i-L\',\'9Q\');b(\'#B-2l\').1i();b(\'#B-2l\').l(\'1i\').2s();e.2a();b(\'.B-5Q\').W(\'H\');b(\'.B-5Q\').H(q(){4s(2n);c el;d(1h.1o){el=1h.1o().21(0).2j.1Y;d(el.1E!=\'6N\'&&el.1E!=\'6L\'&&el.1E!=\'7P\'&&el.1E!=\'7M\'&&el.1E!=\'7L\'&&el.1E!=\'7F\'&&el.1E!=\'P\'){el=el.1Y}}J d(K.1c){el=K.1c.24().2u();d(el.1E!=\'6N\'&&el.1E!=\'6L\'&&el.1E!=\'7P\'&&el.1E!=\'7M\'&&el.1E!=\'7L\'&&el.1E!=\'7F\'&&el.1E!=\'P\'){el=el.2u()}}c s=b(h).l(\'2l\');el.M.eu=s;b(h).4g();$u.l(\'1w\').E.3o=C;e.2a();b(\'#B-2l\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="2G"]\').W(\'H\');b(\'#S-1v a[l-S-1B="2G"]\').H(q(e){c 2n=4A();b(\'#B-2G\').m(\'3i-L\',\'9Q\');b(\'#B-2G\').1i();b(\'#B-2G\').l(\'1i\').2s();e.2a();b(\'.B-59\').W(\'H\');b(\'.B-59\').H(q(){4s(2n);c s=b(h).l(\'2G\');3K{d(s==\'5l\'){K.2h(\'5h\',T,1S);K.2h(\'5h\',T,1S);K.2h(\'5h\',T,1S)}J{K.2h(s,T,1S)}}3J(e){$3I.X(\'k\').3f(\'5c\');c el;d(1h.1o){el=1h.1o().21(0).2j.1Y;el=el.1Y}J d(K.1c){el=K.1c.24().2u();el=el.2u()}el.ew(\'25\',C);d(s==\'5l\'){K.2h(\'5h\',T,1S);K.2h(\'5h\',T,1S);K.2h(\'5h\',T,1S)}J{K.2h(s,T,1S)}el.7e(\'25\');$u.l(\'1w\').47()}b(h).4g();$u.l(\'1w\').E.3o=C;e.2a();b(\'#B-2G\').l(\'1i\').2E()})});b(\'#S-1v a[l-S-1B="7Q"]\').W(\'H\');b(\'#S-1v a[l-S-1B="7Q"]\').H(q(e){c G="";d(5D 1h.1o!="2J"){c 3x=1h.1o();d(3x.7N){c 8O=K.cr("k");1T(c i=0,3z=3x.7N;i<3z;++i){8O.cp(3x.21(i).eD())}G=8O.eE}}J d(5D K.1c!="2J"){d(K.1c.1M=="7p"){G=K.1c.24().eI}}d(G==\'\'){9T(\'9U 2B eL 1n.\');e.2a();e.8P();R}c el;d(1h.1o){el=1h.1o().21(0).2j}J d(K.1c){el=K.1c.24()}d(el.1E.1L()==\'a\'){$1U=b(el)}J{K.2h(\'7Q\',T,\'3c://9W\');$1U=b("a[1O=\'3c://9W\']").3n();$1U.I(\'1O\',\'3c://\')}b(\'#B-4l\').m(\'3i-L\',\'8Q\');b(\'#B-4l\').1i({7H:q(){d($1U.I(\'1O\')==\'3c://\')$1U.4R($1U.G())}});b(\'#B-4l\').l(\'1i\').2s();b(\'#3W\').1d($1U.I(\'1O\'));b(\'#6l\').1d($1U.G());d($1U.I(\'1K\')==\'7G\'){b(\'#4H\').3C(\'5b\',C)}J{b(\'#4H\').2U(\'5b\')}b(\'#6h\').W(\'H\');b(\'#6h\').1I(\'H\',q(e){$1U.I(\'1O\',b(\'#3W\').1d());d(b(\'#3W\').1d()==\'3c://\'||b(\'#3W\').1d()==\'\'){$1U.4R($1U.G())}$1U.G(b(\'#6l\').1d());d(b(\'#4H\').5j(":5b")){$1U.I(\'1K\',\'7G\')}J{$1U.2U(\'1K\')}b(\'#B-4l\').l(\'1i\').2E();1T(c i=0;i<2t.N;i++){b(2t[i]).l(\'1w\').E.3o=C;b(2t[i]).l(\'1w\').47()}});e.2a()});$u.U(".5u-76").W(\'38\');$u.U(".5u-76").38(q(e){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;c D=2C.D;d(D==\'5l\')D=1;d(D==2J)D=1;D=D+\'\';d(D.1C(\'%\')!=-1){D=D.2b(\'%\',\'\')/22}d(D==\'5R\'){D=1}D=D*1;c 29;c 2p;c 2K=b(1h).4y();c 2Z=b(h).3G().1q;c 2Q=b(h).3G().V;c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();c 2Y=C;d(26||1X)2Y=T;d(2Y){29=((2Z-20)*D)+(2K-2K*D);2p=2Q*D}J{d(1X){c 3a=$u.3k().1q;c 3h=(-3a/1.1)*D+3a/1.1;c 32=$u.3k().V;c 3e=-32*D+32;c p=b(h).3k();29=((p.1q-20)*D)+3h;2p=(p.V*D)+3e}d(26){29=2Z-20;2p=2Q}}b("#3F").m("1q",29+"1x");b("#3F").m("V",2p+"1x");b("#3F").1g(C,C).m({1m:\'1s\'}).2T(20);$6q=b(h).U(\'2S\');b("#3F").W(\'H\');b("#3F").1I(\'H\',q(e){b(\'#B-6b\').m(\'3i-L\',\'8Q\');b(\'#B-6b\').1i();b(\'#B-6b\').l(\'1i\').2s();b(\'#7b\').1d($6q.I(\'1y\'));b(\'#84\').W(\'H\');b(\'#84\').1I(\'H\',q(e){c 5E=b(\'#7b\').1d();c a8=/^3c[s]?:\\/\\/(((a9.aa.6g\\/fh\\?(fj=fk&)?)v=)|(fl.be\\/))([^#\\&\\?]*)/;c ab=/^.*(ac\\.6g\\/)((ft\\/[A-z]+\\/)|(fv\\/[A-z]+\\/fw\\/)|(ad\\/))?([0-9]+)\\/?/;c 6B=a8.af(5E);c 6A=ab.af(5E);d(6B!=1S||6A!=1S){d(6B!=1S&&6B.N>=7){c cL=6B[6];5E=\'//a9.aa.6g/5u/\'+cL+\'?6Z=0\'}d(6A!=1S&&6A.N>=7){c ai=6A[6];5E=\'//fG.ac.6g/ad/\'+ai}}$6q.I(\'1y\',5E);d(b(\'#7b\').1d()==\'\'){$6q.I(\'1y\',\'\')}b(\'#B-6b\').l(\'1i\').2E();1T(c i=0;i<2t.N;i++){b(2t[i]).l(\'1w\').E.3o=C;b(2t[i]).l(\'1w\').47()}})});b("#3F").38(q(e){b(h).1g(C,C).m("1m","2o")},q(){b(h).1g(C,C).1t(0)})},q(e){b("#3F").1g(C,C).1t(0)});$u.U(\'a\').6i(\'.6i-a\').W(\'38\');$u.U(\'a\').6i(\'.6i-a\').38(q(e){d(b(h).X("[l-1W=\'1P\']").N>0)R;d(b(h).X("[l-1W=\'3D\']").N>0)R;d(b(h).1J(\'1j\').N==1&&b(h).1J().N==1)R;c D=2C.D;d(D==\'5l\')D=1;d(D==2J)D=1;D=D+\'\';d(D.1C(\'%\')!=-1){D=D.2b(\'%\',\'\')/22}d(D==\'5R\'){D=1}D=D*1;c 29;c 2p;c 2K=b(1h).4y();c 2Z=b(h).3G().1q;c 2Q=b(h).3G().V;c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();c 2Y=C;d(26||1X)2Y=T;d(2Y){29=((2Z-23)*D)+(2K-2K*D);2p=2Q*D}J{d(1X){c 3a=$u.3k().1q;c 3h=(-3a/1.1)*D+3a/1.1;c 32=$u.3k().V;c 3e=-32*D+32;c p=b(h).3k();29=((p.1q-23)*D)+3h;2p=(p.V*D)+3e}d(26){29=2Z-23;2p=2Q}}b("#2W").m("1q",29+"1x");b("#2W").m("V",2p+"1x");b("#2W").1g(C,C).m({1m:\'1s\'}).2T(20);$1U=b(h);b("#2W").W(\'H\');b("#2W").1I(\'H\',q(e){b(\'#B-4l\').m(\'3i-L\',\'ax\');b(\'#B-4l\').1i({7H:q(){d($1U.I(\'1O\')==\'3c://\')$1U.4R($1U.G())}});b(\'#B-4l\').l(\'1i\').2s();b(\'#3W\').1d($1U.I(\'1O\'));b(\'#6l\').1d($1U.G());d($1U.I(\'1K\')==\'7G\'){b(\'#4H\').3C(\'5b\',C)}J{b(\'#4H\').2U(\'5b\')}b(\'#6h\').W(\'H\');b(\'#6h\').1I(\'H\',q(e){$1U.I(\'1O\',b(\'#3W\').1d());d(b(\'#3W\').1d()==\'3c://\'||b(\'#3W\').1d()==\'\'){$1U.4R($1U.G())}$1U.G(b(\'#6l\').1d());d(b(\'#4H\').5j(":5b")){$1U.I(\'1K\',\'7G\')}J{$1U.2U(\'1K\')}b(\'#B-4l\').l(\'1i\').2E();1T(c i=0;i<2t.N;i++){b(2t[i]).l(\'1w\').E.3o=C;b(2t[i]).l(\'1w\').47()}})});b("#2W").38(q(e){b(h).1g(C,C).m("1m","2o")},q(){b(h).1g(C,C).1t(0)})},q(e){b("#2W").1g(C,C).1t(0)});b("#7Y").W(\'H\');b("#7Y").1I(\'H\',q(e){b(\'#7s\').I(\'1y\',$u.l(\'1w\').E.27);b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b("#2W").1g(C,C).1t(0);b("#3F").1g(C,C).1t(0);b(\'#5v-1u\').1d(\'3W\');b(\'#B-27\').m(\'L\',\'65%\');b(\'#B-27\').1i();b(\'#B-27\').l(\'1i\').2s()});$u.l(\'1w\').E.4c()};h.55=q(s){b(\'#S-1v a[l-S-1B="\'+s+\'"]\').W(\'H\');b(\'#S-1v a[l-S-1B="\'+s+\'"]\').H(q(e){3K{K.2h(s,T,1S)}3J(e){$u.I(\'25\',C);K.2h(s,T,1S);$u.2U(\'25\');$u.l(\'1w\').47()}b(h).4g();$u.l(\'1w\').E.3o=C;e.2a()})};h.4M()};b.fn.1w=q(2O){R h.1F(q(){2t.7y(h);d(2J==b(h).l(\'1w\')){c 4G=37 b.1w(h,2O);b(h).l(\'1w\',4G)}})}})(b);q 8r($3I){c 2n=4A();b(\'#2A\').1Q();c al=$3I.3G().1q;b(\'#2x\').2w("<k M=\'3w:4U;z-4m:-fK;1q:"+al+"1x;V:-fN;L:2R;1e:2R;4v:5f;\' 3Z=\'2A\' F=\'2A\' 25=\'C\'></k>");c ao=K.6v("2A");ao.87();fV(q(){3K{4s(2n);c $2I=b(ar());d(b(\'#2A\').N==0)R;c 4B=\'\';c 8V=T;d(b(\'#2A gf\').N>0||b(\'#2A 1j\').N>0||b(\'#2A p\').N>0||b(\'#2A a\').N>0){8V=C}d(8V){4B=b(\'#2A\').G();4B=au(4B);b(\'#2A\').G(4B);d(b(\'#2A\').1J(\'p,52,5i,4X,54,4Z,51,9x,bS\').N>1){b(\'#2A\').gl().av(q(){R(h.8C==3&&b.5n(h.gS)!=\'\')}).53("<p></p>").go().av("br").1Q()}4B=\'<k w="5c">\'+b(\'#2A\').G()+\'</k>\'}J{b(\'#2A\').U(\'p,52,5i,4X,54,4Z,51\').1F(q(){b(h).G(b(h).G()+\' \')});4B=b(\'#2A\').1n()}b(\'#2A\').1Q();c 1R=1h.1o();c Y=1R.21(0);Y.7S();Y.5C(C);c 4t=Y.6z(4B);c 3j=4t.8z;Y.6K(4t);Y.6j(3j);Y.5S(3j);Y.5C(T);c 3t=Y.2j;d(3t&&3t.1Y){3K{3t.1Y.8D()}3J(e){}}1R.6p();1R.6H(Y)}3J(e){b(\'#2A\').1Q()}},3O)}c 2n;q 4A(){d(1h.1o){3x=1h.1o();d(3x.21&&3x.7N){c 8W=[];1T(c i=0,3z=3x.7N;i<3z;++i){8W.7y(3x.21(i))}R 8W}}J d(K.1c&&K.1c.24){R K.1c.24()}R 1S};q 4s(2n){d(2n){d(1h.1o){3x=1h.1o();3x.6p();1T(c i=0,3z=2n.N;i<3z;++i){3x.6H(2n[i])}}J d(K.1c&&2n.2B){2n.2B()}}};q ar(){c 2I,1c;d(1h.1o){1c=1o();2I=1c.ay}d(!2I&&K.1c){1c=K.1c;c Y=1c.21?1c.21(0):1c.24();2I=Y.2j?Y.2j:Y.2u?Y.2u():Y.5z(0)}d(2I){R(2I.1E=="#1n"?2I.1Y:2I)}};c gt=q(){c 2I,1c;d(1h.1o){1c=1o();2I=1c.ay}d(!2I&&K.1c){1c=K.1c;c Y=1c.21?1c.21(0):1c.24();2I=Y.2j?Y.2j:Y.2u?Y.2u():Y.5z(0)}d(2I){R(2I.1E=="#1n"?2I.1Y:2I)}};q 8G(){d(1h.1o){R 1h.1o()}J d(K.1o){R K.1o()}J{c 1c=K.1c&&K.1c.24();d(1c.1n){R 1c.1n}R T}R T};(q(b){c 2L;c 6u;c 6t;c $1D;b.2M=q(u,2O){c 4E={4h:T,33:\'\',27:\'\',3Q:C,aD:C,D:0,aE:q(){}};h.E={};c $u=b(u),u=u;h.4M=q(){h.E=b.7o({},4E,2O);d(b(\'#2x\').N==0){b(\'2i\').2w(\'<k F="2x"></k>\')}c 7d=\'\';c 7c=\'\';d(h.E.3Q){d(2f.gH.1C(\'gI\')!=-1){7d=\'<k F="1N"><k w="gM"><1u 1M="2y" 3Z="2y" w="1l-2y" /><k w="gP"><1j 1y="l:1b/3l;5m,gR/cM+cN+cO+cP/cQ/cR++cS+cT+cU/cV+cW/cX/cY/cZ/d0+d1+f+d2+d3/t+d4/s+d5+d6+d7+d8/d9/da+db+dc+dd/de" /></k></k></k>\';7c=\'\'}J{7d=\'<k M="1m:1s"><1u 1M="2y" 3Z="2y" w="1l-2y"></k>\';7c=\'<k F="1N">\'+\'<i F="df" w="cb-1a-dg"></i>\'+\'</k>\'}}c aJ=\'<k F="dj" M="1m:1s"></k>\'+\'<k w="31-bg" M="3w:7O;1q:0;V:0;L:1;1e:1;z-4m:8X;D 1;1G:#49;88:0.8"></k>\'+\'<k F="4F" M="3w:4U;1m:1s;z-4m:8X">\'+\'<k F="1l-2z" M="L:aP;1e:aP;4v:36;">\'+\'<1j F="1l-1b" 1y="" M="3i-L:1s" />\'+\'</k>\'+\'<k F="1j-8Y" M="4a-1q:2R;3w:4U;1q:-dr;V:3v;L:ds;88:0.8">\'+\'<1f F="8Z" 1M="1f" 2e="du" ><i w="cb-1a-dv"></i></1f>\'+\'<1f F="91" 1M="1f" 2e="-" ><i w="cb-1a-dy"></i></1f>\'+\'<1f F="6o" 1M="1f" 2e="+" ><i w="cb-1a-5w"></i></1f>\'+\'<1f F="92" 1M="1f" 2e="5r" ><i w="cb-1a-dB"></i> 5r</1f>\'+\'</k>\'+\'</k>\'+\'<k M="1m:1s">\'+\'<3b F="6O"></3b>\'+\'<3b F="aW"></3b>\'+\'</k>\'+\'<94 F="dG" dH="dI" dJ="" 1K="95" dL="dM/94-l">\'+7d+\'<1u F="aZ" 3Z="aZ" 1M="36" />\'+\'<1u F="b0" 3Z="b0" 1M="36" />\'+\'<1u F="b1" 3Z="b1" 1M="36" />\'+\'<1u F="b2" 3Z="b2" 1M="36" />\'+\'<1u F="b3" 3Z="b3" 1M="36" />\'+\'</94>\'+\'<2S F="95" 3Z="95" M="L:2R;1e:2R;1z:1s;dS:36;3w:4U"></2S>\';c 6M=T;d(h.E.33!=\'\')6M=C;c 4C=T;d(h.E.27!=\'\')4C=C;c 2H=7c+\'<k F="2v">\'+\'<i F="96" w="cb-1a-3N"></i>\'+\'</k>\'+\'<k F="6n">\'+\'<i F="dX" w="cb-1a-b8 4W-b8"></i>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-1j">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+\'<k M="1G:#49;1z-b9:#e0;1Z-5x: e1-e2;1p: #7E;1Z-39:30;9H-9G: e4;">\'+\'<k M="1n-2l:66;3P:3s;5o-8y:1z-5o;1G:#e5;1z-b9:#e6 2R 70;">\'+\'<1V F="6m" M="3P: 8H 64;1z-2q:6R;1G:#98;1n-5G:1s;1p:#49;4a-2k:3s">e9</1V>\'+\'<1V F="6k" M="3P: 8H 64;1z-2q:6R;1G:#99;1n-5G:5A;1p:#7E;3u:4x">ec ed</1V>\'+\'</k>\'+\'</k>\'+\'<k F="9a" M="4v-y:5f;4v-x:36;1m:1s;5o-8y:1z-5o;3P:7t 7t 7t">\';2H+=\'<k M="3P:30 64 64;L:22%;1n-2l:66;">\';2H+=\'ef (eg): &9b; <2B F="4I">\';c 4w=50;1T(c i=0;i<9d;i++){c 3X=\'\';d(i==90)3X=\' 3X="3X"\';2H+=\'<2m 2e="\'+4w+\'"\'+3X+\'>\'+4w+\'1x</2m>\';4w+=5}2H+=\'</2B> &9b; \';2H+=\'<2B F="4b">\';c 4K=50;1T(c i=0;i<9e;i++){c 3X=\'\';d(i==40)3X=\' 3X="3X"\';2H+=\'<2m 2e="\'+4K+\'"\'+3X+\'>\'+4K+\'1x</2m>\';4K+=5}2H+=\'</2B> &9b; \';2H+=\'<2B F="4L">\';2H+=\'<2m 2e="bo">eq</2m>\';2H+=\'<2m 2e="5K">er</2m>\';2H+=\'</2B>\';2H+=\'<1f w="5s 5s-5g" F="9f" M="4a-V:30"> et </1f>\';2H+=\'</k>\'+\'</k>\'+\'<k F="9g">\'+\'<k w="B-4z">9s 7T:</k>\'+(6M?\'<1u 1M="1n" F="5L" w="42" M="4i:V;L:50%"></1u><i w="cb-1a-3N B-7X" F="9h" M="L:10%;"></i>\':\'<1u 1M="1n" F="5L" w="42" M="4i:V;L:60%"></1u>\')+\'<br M="7w:7q">\'+\'<k w="B-4z">ex 7p:</k>\'+\'<1u 1M="1n" F="6P" w="42" M="4i:2k;L:60%"></1u>\'+\'<br M="7w:7q">\'+\'<k w="B-4z">ez 7T:</k>\'+(4C?\'<1u 1M="1n" F="4o" w="42" M="4i:V;L:50%"></1u><i w="cb-1a-3N B-7X" F="9i" M="L:10%;"></i>\':\'<1u 1M="1n" F="4o" w="42" M="4i:V;L:60%"></1u>\')+\'</k>\'+\'</k>\'+\'<k F="9j" w="B-7l">\'+\'<1f F="9k"> 5r </1f>\'+\'</k>\'+\'</k>\'+\'</k>\'+\'\'+\'<k w="B-2X" F="B-33">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+(6M?\'<2S F="bx" M="L:22%;1e:8m;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\':\'\')+\'</k>\'+\'</k>\'+\'</k>\'+\'\';d(b(\'#B-27\').N==0){2H+=\'<k w="B-2X" F="B-27">\'+\'<k w="B-34">\'+\'<k w="B-2i">\'+(4C?\'<2S F="7s" M="L:22%;1e:8m;1z: 1s;1m: 2o;" 1y="l:1b/3l;5m,5H="></2S>\':\'\')+\'</k>\'+\'</k>\'+\'</k>\'}d(b(\'#5v-1u\').N==0){2H+=\'<1u 1M="36" F="5v-1u" />\'}d(b(\'#1N\').N==0){d(h.E.3Q){b(\'#2x\').2w(aJ)}b(\'#2x\').2w(2H)}2L=K.6v(\'aW\');$u.38(q(e){c D;d(2C.8T("D")!=1S){D=2C.D}J{D=$u.X(\'[M*="D"]\').m(\'D\');d(D==\'5l\')D=1;d(D==2J)D=1}c 26=2f.2r.1L().1C(\'3q\')>-1;D=D+\'\';d(D.1C(\'%\')!=-1){D=D.2b(\'%\',\'\')/22}d(D==\'5R\'){D=1}2C.D=D;D=D*1;d(2D==\'\')D=1;d($u.l("2M").E.D==1){D=1}c 29;c 6e;c 2p;c 2K=b(1h).4y();c 2Z=b(h).3G().1q;c 2Q=b(h).3G().V;c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();c 2Y=C;d(26||1X)2Y=T;c 7D=!b(h).l("2M").E.3Q?9:-35;d(2Y){29=((2Z+2g(b(h).m(\'1e\'))/2)-15)*D+(2K-2K*D);2p=((2Q+2g(b(h).m(\'L\'))/2)-15)*D;6e=29+7D}J{d(1X){c 3a=0;c 32=0;$u.X().1F(q(){d(b(h).l(\'O\')){3a=b(h).3k().1q;32=b(h).3k().V}});c 3h=-3a*D+3a;c 3e=-32*D+32;c p=b(h).3k();29=((p.1q-15+2g(b(h).m(\'1e\'))/2))*D+3h;2p=((p.V-15+2g(b(h).m(\'L\'))/2))*D+3e;6e=29+7D}d(26){c 6d=2g(b(h).m(\'L\'));c 6c=2g(b(h).m(\'1e\'));29=2Z-15+6c*D/2;2p=2Q-15+6d*D/2;6e=29+7D}}c 9l=T;$1D=b(h);d($1D.I(\'l-7O\')==1){9l=C}d(6X&&!9l){b("#1N").m("1q",29+"1x");b("#1N").m("V",2p+"1x");b("#1N").1g(C,C).m({1m:\'1s\'}).2T(20);d(b(h).l("2M").E.aD){b("#2v").m("1q",6e+"1x");b("#2v").m("V",2p+"1x");b("#2v").1g(C,C).m({1m:\'1s\'}).2T(20)}J{b("#2v").m("1q","-eJ")}}b("#1N").W(\'H\');b("#1N").1I(\'H\',q(e){b(h).l(\'1b\',$1D);b(\'1u.1l-2y[1M=2y]\').H();e.2a();e.8P()});b("#1N").W(\'38\');b("#1N").38(q(e){b("#1N").1g(C,C).m("1m","2o");b("#2v").1g(C,C).m("1m","2o")},q(){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0)});$u.U(\'89\').W(\'38\');$u.U(\'89\').38(q(e){b("#1N").1g(C,C).m("1m","2o");b("#2v").1g(C,C).m("1m","2o")},q(){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0)});b("#2v").W(\'38\');b("#2v").38(q(e){b("#1N").1g(C,C).m("1m","2o");b("#2v").1g(C,C).m("1m","2o")},q(){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0)});b("#96").W(\'H\');b("#96").1I(\'H\',q(e){b(h).l(\'1b\',$1D);b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b(\'#B-1j\').m(\'3i-L\',\'8Q\');b(\'#B-1j\').1i();b(\'#B-1j\').l(\'1i\').2s();c $1j=$u;d($u.3C("3T").1L()==\'63\'){$1j=$u.U(\'1j:3n\')}b(\'#5L\').1d($1j.I(\'1y\'));b(\'#6P\').1d($1j.I(\'9m\'));b(\'#4o\').1d(\'\');d($1j.X(\'a:3n\')!=2J){b(\'#4o\').1d($1j.X(\'a:3n\').I(\'1O\'))}b(\'#9k\').W(\'H\');b(\'#9k\').1I(\'H\',q(e){c 4N;$u.X().1F(q(){d(b(h).l(\'O\')){4N=b(h).l(\'O\')}});d($1j.I(\'1y\').1C(\'bA/1b.3l\')!=-1&&b(\'#5L\').1d().1C(\'bA/1b.3l\')==-1){$1j.m(\'L\',\'\');$1j.m(\'1e\',\'\')}$1j.I(\'1y\',b(\'#5L\').1d());$1j.I(\'9m\',b(\'#6P\').1d());d(b(\'#4o\').1d()==\'3c://\'||b(\'#4o\').1d()==\'\'){$1j.X(\'a:3n\').4R($1j.X(\'a:3n\').G())}J{d($1j.X(\'a:3n\').N==0){$1j.53(\'<a 1O="\'+b(\'#4o\').1d()+\'"></a>\')}J{$1j.X(\'a:3n\').I(\'1O\',b(\'#4o\').1d())}}d(4N)4N.4n();b(\'#B-1j\').l(\'1i\').2E()});c 4w=50;1T(c i=0;i<9d;i++){c bB=2g($1j.m(\'L\'));d(4w>=bB){i=9d;b(\'#4I\').1d(4w)}4w+=5}c 4K=50;1T(c i=0;i<9e;i++){c bC=2g($1j.m(\'1e\'));d(4K>=bC){i=9e;b(\'#4b\').1d(4K)}4K+=5}d(2g($1j.m(\'1z-2q\'))==eN){b(\'#4L\').1d(\'5K\');b(\'#4b\').m(\'1m\',\'1s\')}J{b(\'#4L\').1d(\'bo\');b(\'#4b\').m(\'1m\',\'9n\')}b(\'#4L\').W(\'4p\');b(\'#4L\').1I(\'4p\',q(e){d(b(\'#4L\').1d()==\'5K\'){b(\'#4b\').m(\'1m\',\'1s\');b(\'#4b\').1d(b(\'#4I\').1d())}J{b(\'#4b\').m(\'1m\',\'9n\');b(\'#4b\').1d(b(\'#4I\').1d())}});b(\'#4I\').W(\'4p\');b(\'#4I\').1I(\'4p\',q(e){d(b(\'#4L\').1d()==\'5K\'){b(\'#4b\').1d(b(\'#4I\').1d())}});b(\'#9f\').W(\'H\');b(\'#9f\').1I(\'H\',q(e){c 4N;$u.X().1F(q(){d(b(h).l(\'O\')){4N=b(h).l(\'O\')}});$1j.I(\'1y\',48+\'1b.3l\');$1j.I(\'9m\',b(\'#6P\').1d());$1j.m(\'L\',b(\'#4I\').1d()+\'1x\');$1j.m(\'1e\',b(\'#4b\').1d()+\'1x\');d(b(\'#4L\').1d()==\'5K\'){$1j.m(\'1z-2q\',\'7k\')}J{$1j.m(\'1z-2q\',\'\')}d(4N)4N.4n();b(\'#B-1j\').l(\'1i\').2E()});e.2a();e.8P()});b("#9h").W(\'H\');b("#9h").1I(\'H\',q(e){b(\'#bx\').I(\'1y\',$u.l(\'2M\').E.33);b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b("#2W").1g(C,C).1t(0);b("#3F").1g(C,C).1t(0);b(\'#5v-1u\').1d(\'5L\');b(\'#B-33\').m(\'L\',\'65%\');b(\'#B-33\').1i();b(\'#B-33\').l(\'1i\').2s()});b("#9i").W(\'H\');b("#9i").1I(\'H\',q(e){b(\'#7s\').I(\'1y\',$u.l(\'2M\').E.27);b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b("#2W").1g(C,C).1t(0);b("#3F").1g(C,C).1t(0);b(\'#5v-1u\').1d(\'4o\');b(\'#B-27\').m(\'L\',\'65%\');b(\'#B-27\').1i();b(\'#B-27\').l(\'1i\').2s()});b(\'.1l-2y[1M=2y]\').W(\'4p\');b(\'.1l-2y[1M=2y]\').1I(\'4p\',q(e){bE(e);b(\'#1l-1b\').I(\'1y\',\'\');d(!$1D.3V().I(\'l-eR\')){b(h).bF()}});b(\'#6m\').W(\'H\');b(\'#6m\').1I(\'H\',q(e){b(\'#6m\').m({\'1n-5G\':\'\',\'3u\':\'\',\'1G\':\'#98\',\'1p\':\'#49\'});b(\'#6k\').m({\'1n-5G\':\'5A\',\'3u\':\'4x\',\'1G\':\'#99\',\'1p\':\'#7E\'});b(\'#9a\').1t(7J,q(){b(\'#9g\').2T(0);b(\'#9j\').2T(0)})});b(\'#6k\').W(\'H\');b(\'#6k\').1I(\'H\',q(e){b(\'#6m\').m({\'1n-5G\':\'5A\',\'3u\':\'4x\',\'1G\':\'#99\',\'1p\':\'#7E\'});b(\'#6k\').m({\'1n-5G\':\'\',\'3u\':\'\',\'1G\':\'#98\',\'1p\':\'#49\'});b(\'#9g\').1t(0);b(\'#9j\').1t(0,q(){b(\'#9a\').2T(7J)})})},q(e){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0)})};c bE=q(e){d(5D bG=="2J")R C;c eU=b(h);c 9o=e.1K.9o;c 4h=T;3K{4h=$u.l(\'2M\').E.4h}3J(e){};1T(c i=0,2y;2y=9o[i];i++){c 5F=2y.3Z;c 4O=5F.9N((5F.eY(\'.\')+1)).1L();d(4O==\'bJ\'||4O==\'9p\'||4O==\'3l\'||4O==\'f1\'||4O==\'f2\'){}J{9T(\'9U 2B an 1b\');R}d(2y.1M.6w(\'1b.*\')){b("#1N").1g(C,C).1t(0);b("#2v").1g(C,C).1t(0);b(\'.31-bg\').m(\'L\',\'22%\');b(\'.31-bg\').m(\'1e\',\'22%\');b(\'2i\').m(\'4v\',\'36\');b("#6n").m(\'1q\',b(\'#1N\').m(\'1q\'));b("#6n").m(\'V\',b(\'#1N\').m(\'V\'));b("#6n").m(\'1m\',\'2o\');c 9q=37 bG();9q.bM=(q(f4){R q(e){c 1b=e.1K.f5;$1D=b("#1N").l(\'1b\');c D=2C.D;d($u.l(\'2M\').E.D==1){D=1}d($1D.3C("3T").1L()==\'1j\'){b("#1l-2z").m(\'L\',$1D.L()+\'1x\');b("#1l-2z").m(\'1e\',$1D.1e()+\'1x\')}J{b("#1l-2z").m(\'L\',$1D.f6()+\'1x\');b("#1l-2z").m(\'1e\',$1D.f7()+\'1x\')}b("#1l-2z").m(\'D\',D);b("#1l-2z").m(\'-6s-8S\',\'8R(\'+D+\')\');c 9r=37 9s();9r.bM=q(f9){$1D=b("#1N").l(\'1b\');6u=h.L;6t=h.1e;c 4P;c 4Q;c 5N=$1D.L();c 5O=$1D.1e();c bP=6u/6t;c bQ=5N/5O;d(bP<bQ){4P=5N;4Q=(6t*5N)/6u}J{4P=(6u*5O)/6t;4Q=5O}h.L=4P;h.1e=4Q;b(\'#1l-1b\').I(\'1y\',1b);b(\'#1l-1b\').6x(\'bR\',q(){b(\'.31-bg\').m(\'L\',\'22%\');b(\'.31-bg\').m(\'1e\',\'22%\');b(\'2i\').m(\'4v\',\'36\');$1D=b("#1N").l(\'1b\');b("#1l-1b").m(\'1q\',\'3v\');b("#1l-1b").m(\'V\',\'3v\');b("#1l-1b").m(\'L\',4P+\'1x\');b("#1l-1b").m(\'1e\',4Q+\'1x\');c D=2C.D;D=D*1;d($u.l(\'2M\').E.D==1){D=1}c 29;c 2p;c 69;c 61;c 2K=b(1h).4y();c 2Z=$1D.3G().1q;c 2Q=$1D.3G().V;c 26=2f.2r.1L().1C(\'3q\')>-1;c 1X=3A();c 2Y=C;d(26||1X)2Y=T;d(2Y){29=(2Z*D)+(2K-2K*D);2p=2Q*D;69=((2Z+5)*D)+(2K-2K*D);61=(2Q+5)*D}J{d(1X){c 3a=0;c 32=0;$u.X().1F(q(){d(b(h).l(\'O\')){3a=b(h).3k().1q;32=b(h).3k().V}});c 3h=-3a*D+3a;c 3e=-32*D+32;c p=$1D.3k();29=(p.1q*D)+3h;2p=(p.V*D)+3e;69=((p.1q+5)*D)+3h;61=((p.V+5)*D)+3e}d(26){c 6d=2g($1D.m(\'L\'));c 6c=2g($1D.m(\'1e\'));c 3e=6d/2-(6d/2)*D;c 3h=6c/2-(6c/2)*D;b(\'#1j-8Y\').m(\'1q\',5+3h+\'1x\');b(\'#1j-8Y\').m(\'V\',7+3e+\'1x\');29=2Z-3h;2p=2Q-3e;69=2Z-3h+5;61=2Q-3e+5}}b(\'#4F\').m(\'1m\',\'9n-2o\');d($1D.I(\'w\')==\'1j-fi\'){b("#4F").m("1q",69+"1x");b("#4F").m("V",61+"1x")}J{b("#4F").m("1q",29+"1x");b("#4F").m("V",2p+"1x")}7f();2L.L=4P;2L.1e=4Q;c 3H=b("#1l-1b")[0];c 4k=2L.6U(\'2d\');c 26=2f.2r.1L().1C(\'3q\')>-1;d(26)bW(fo);d((2f.2r.6w(/cE/i))||(2f.2r.6w(/cC/i))){3K{c bX=37 fq(3H);bX.47(2L,{L:3H.L,1e:3H.1e})}3J(e){4k.5W(3H,0,0,4P,4Q)}}J{4k.5W(3H,0,0,4P,4Q)}5U();d($1D.I(\'w\')==\'1j-5K\'){b(\'#1l-2z\').m(\'-bZ-1z-2q\',\'7k\');b(\'#1l-2z\').m(\'-6s-1z-2q\',\'7k\');b(\'#1l-2z\').m(\'1z-2q\',\'7k\')}J{b(\'#1l-2z\').m(\'-bZ-1z-2q\',\'3v\');b(\'#1l-2z\').m(\'-6s-1z-2q\',\'3v\');b(\'#1l-2z\').m(\'1z-2q\',\'3v\')}b(\'#1l-1b\').W(\'bR\');d($1D.3C("3T").1L()==\'1j\'){}J{b(\'#6o\').H();b(\'#6o\').H()}b("#6n").m(\'1m\',\'1s\')});b(\'#92\').W(\'H\');b(\'#92\').1I(\'H\',q(){c 3b=K.6v(\'6O\');$1D=b("#1N").l(\'1b\');c 1b;d(4h==T){d(4O==\'bJ\'||4O==\'9p\'){1b=3b.9u("1b/9p",0.9)}J{1b=3b.9u("1b/3l",1)}}J{1b=3b.9u("1b/3l",1)}d($1D.3C("3T").1L()==\'1j\'){$1D.I(\'1y\',1b);$1D.l(\'9v\',5F)}J d($1D.3C("3T").1L()==\'63\'){$1D.U(\'1j\').I(\'1y\',1b);$1D.U(\'1j\').l(\'9v\',5F)}J{$1D.m(\'1G-1b\',\'c2(l:\'+1b+\')\');$1D.l(\'9v\',5F)}b(\'#4F\').m(\'1m\',\'1s\');b(\'.31-bg\').m(\'L\',\'2R\');b(\'.31-bg\').m(\'1e\',\'2R\');b(\'2i\').m(\'4v\',\'\');$1D.m(\'L\',\'\');$1D.m(\'1e\',\'\');$u.l(\'2M\').E.aE()});b(\'#8Z\').W(\'H\');b(\'#8Z\').1I(\'H\',q(){c 3b=K.6v(\'6O\');$1D=b("#1N").l(\'1b\');b(\'#4F\').m(\'1m\',\'1s\');b(\'.31-bg\').m(\'L\',\'2R\');b(\'.31-bg\').m(\'1e\',\'2R\');b(\'2i\').m(\'4v\',\'\')});b(\'#6o\').W(\'H\');b(\'#6o\').1I(\'H\',q(){c 4j=2g(b("#1l-1b").m(\'L\'));c 4f=2g(b("#1l-1b").m(\'1e\'));b("#1l-1b").m(\'L\',(4j/0.9)+\'1x\');b("#1l-1b").m(\'1e\',(4f/0.9)+\'1x\');7f();2L.L=(4j/0.9);2L.1e=(4f/0.9);c 3H=b("#1l-1b")[0];c 4k=2L.6U(\'2d\');4k.5W(3H,0,0,(4j/0.9),(4f/0.9));5U()});b(\'#91\').W(\'H\');b(\'#91\').1I(\'H\',q(){c 4j=2g(b("#1l-1b").m(\'L\'));c 4f=2g(b("#1l-1b").m(\'1e\'));d((4j/1.1)<b("#1l-2z").L())R;d((4f/1.1)<b("#1l-2z").1e())R;b("#1l-1b").m(\'L\',(4j/1.1)+\'1x\');b("#1l-1b").m(\'1e\',(4f/1.1)+\'1x\');7f();2L.L=(4j/1.1);2L.1e=(4f/1.1);c 3H=b("#1l-1b")[0];c 4k=2L.6U(\'2d\');4k.5W(3H,0,0,(4j/1.1),(4f/1.1));5U()})};9r.1y=1b}})(2y);9q.fz(2y)}}};c 5U=q(){c x=2g(b("#1l-1b").m(\'V\'));c y=2g(b("#1l-1b").m(\'1q\'));c dw=2g(b("#1l-2z").m(\'L\'));c dh=2g(b("#1l-2z").m(\'1e\'));c 3b=K.6v(\'6O\');c 4k=3b.6U(\'2d\');3b.L=dw;3b.1e=dh;c 3H=b("#1l-1b")[0];c 71=-1*x;c 77=-1*y;d(77>(2L.1e-dh))77=2L.1e-dh;d(71>(2L.L-dw))71=2L.L-dw;4k.5W(2L,71,77,dw,dh,0,0,dw,dh)};c 7f=q(){b("#1l-1b").m({1q:0,V:0});c 5N=b("#1l-2z").L();c 5O=b("#1l-2z").1e();c 67=b("#1l-1b").3G();c c8=b("#1l-1b").L();c c9=b("#1l-1b").1e();c ca=(67.V+5N)-c8;c cc=(67.1q+5O)-c9;c cd=67.V;c ce=67.1q;b("#1l-1b").1r({fL:T,fM:[ca,cc,cd,ce],bh:q(){5U()}});b("#1l-1b").m({3u:\'4q\'})};h.4M()};b.fn.2M=q(2O){R h.1F(q(){d(2J==b(h).l(\'2M\')){c 4G=37 b.2M(h,2O);b(h).l(\'2M\',4G)}})}})(b);q cf(){c 1n="";c 9w="fP";1T(c i=0;i<5;i++)1n+=9w.fQ(ch.fS(ch.fT()*9w.N));R 1n}q bW(ci){c 75=37 cj().ck();1T(c i=0;i<fX;i++){d((37 cj().ck()-75)>ci){fY}}}b.fn.fZ=b.fn.bF=q(68){c cm=/^(?:1p|g2|g3|g4|g5|g6|g7|Y|g8|g9|1n|ga|c2|gb)$/i;R h.1F(q(){c t=h.1M,9y=h.3T.1L();d(cm.5d(t)||9y==\'8l\'){h.2e=\'\'}J d(t==\'9J\'||t==\'gd\'){h.5b=T}J d(9y==\'2B\'){h.ge=-1}J d(t=="2y"){d(/co/.5d(2f.2r)){b(h).4R(b(h).bi(C))}J{b(h).1d(\'\')}}J d(68){d((68===C&&/36/.5d(t))||(5D 68==\'gg\'&&b(h).5j(68)))h.2e=\'\'}})};c 3U=8X;(q(b){b.1i=q(u,2O){c 4E={7H:q(){}};h.E={};c $u=b(u),u=u;c $7x;h.4M=q(){h.E=b.7o({},4E,2O);d(b(\'#2x\').N==0){b(\'2i\').2w(\'<k F="2x"></k>\')}};h.2E=q(){$u.m(\'1m\',\'1s\');$u.3L(\'B-2s\');$7x.1Q();3U=3U-2};h.2s=q(){3U=3U+1;c 6r=cf();c cq=\'<k F="B-31-\'+6r+\'" w="B-31" M="z-4m:\'+3U+\'"></k>\';b(\'#2x\').2w(cq);$7x=b(\'#B-31-\'+6r);3U=3U+1;$u.m(\'z-4m\',3U);$u.3f(\'B-2s\');$u.1g(C,C).m(\'1m\',\'1s\').2T(3O);b(\'#B-31-\'+6r).W();b(\'#B-31-\'+6r).H(q(){$u.1g(C,C).1t(22,q(){$u.3L(\'B-2s\')});$7x.1Q();3U=3U-2;$u.l(\'1i\').E.7H()})};h.4M()};b.fn.1i=q(2O){R h.1F(q(){d(2J==b(h).l(\'1i\')){c 4G=37 b.1i(h,2O);b(h).l(\'1i\',4G)}})}})(b);b.fn.3k=q(){c o=h[0];c V=0,1q=0,1Y=1S,4D=1S;4D=o.4D;c ct=o;c el=o;gp(el.1Y!=1S){el=el.1Y;d(el.4D!=1S){c 9A=C;d(1h.bK){d(el==ct.1Y||el.1E=="gr"){9A=T}}d(9A){d(el.4y&&el.4y>0){1q-=el.4y}d(el.9B&&el.9B>0){V-=el.9B}}}d(el==4D){V+=o.9C;d(el.cx&&el.1E!="cy"){V+=el.cx}1q+=o.9D;d(el.cA&&el.1E!="cy"){1q+=el.cA}o=el;d(o.4D==1S){d(o.9C){V+=o.9C}d(o.9D){1q+=o.9D}}4D=o.4D}}R{V:V,1q:1q}};q au(1u){c cB=/(\\n|\\r| w=(")?gz[a-gA-Z]+(")?)/g;c 44=1u.2b(cB,\' \');c cD=37 6y(\'<!--(.*?)-->\',\'g\');c 44=44.2b(cD,\'\');c 73=37 6y(\'<(/)*(gE|3N|1V|\\\\?gF:|gG:|o:|1Z)(.*?)>\',\'gi\');44=44.2b(73,\'\');c 6T=[\'M\',\'9F\',\'gJ\',\'5u\',\'gK\',\'gL\'];1T(c i=0;i<6T.N;i++){73=37 6y(\'<\'+6T[i]+\'.*?\'+6T[i]+\'(.*?)>\',\'gi\');44=44.2b(73,\'\')}c 9E=[\'M\',\'75\'];1T(c i=0;i<9E.N;i++){c cI=37 6y(\' \'+9E[i]+\'="(.*?)"\',\'gi\');44=44.2b(cI,\'\')}R 44}q 3A(){c 4J=1h.2f.2r;c 6F=4J.1C(\'co \');c cK=4J.1C(\'gQ/\');d(6F>0){R 2g(4J.ah(6F+5,4J.1C(\'.\',6F)),10)}d(cK>0){c 7r=4J.1C(\'7r:\');R 2g(4J.ah(7r+3,4J.1C(\'.\',7r)),10)}R T}', 62, 1047, '|||||||||||jQuery|var|if||||this|||div|data|css||||function||||element||class|||||md|true|zoom|settings|id|html|click|attr|else|document|width|style|length|contentbuilder||ui|return|rte|false|find|left|unbind|parents|range||||||||||||icon|image|selection|val|height|button|stop|window|simplemodal|img|row|my|display|text|getSelection|color|top|draggable|none|fadeOut|input|toolbar|contenteditor|px|src|border|event|cmd|indexOf|imgActive|nodeName|each|background|curr|bind|children|target|toLowerCase|type|divToolImg|href|code|remove|oSel|null|for|activeLink|span|mode|is_ie|parentNode|font||getRangeAt|100||createRange|contenteditable|is_firefox|fileselect|tool|_top|preventDefault|replace|temp||value|navigator|parseInt|execCommand|body|commonAncestorContainer|right|align|option|savedSel|block|_left|radius|userAgent|show|instances|parentElement|divToolImgSettings|append|divCb|file|mask|idContentWord|select|localStorage|cb_list|hide|divTool|list|html_hover_icons|node|undefined|scrolltop|tmpCanvas|imageembed|selColMode|options|activeRow|offsetleft|1px|iframe|fadeIn|removeAttr|fontElements|divRteLink|modal|browserok|offsettop|12px|overlay|space2|imageselect|content||hidden|new|hover|size|space|canvas|http|snip|adjx_val|addClass|selectable|adjy_val|max|lastNode|getPos|png|sSrc|first|hasChanged|outline|firefox|lnkToolOpen|15px|comCon|cursor|0px|position|sel|helper|len|detectIE|toolwidth|prop|readonly|editMode|divFrameLink|offset|imageObj|activeElement|catch|try|removeClass|fontname|link|200|padding|imageEmbed|fontsize|colors|tagName|zindex|parent|txtLink|selected|open|name||split|inptxt||output|||render|sScriptPath|fff|margin|selImgH|onRender|editable|cat|nCurrentHeight|blur|hiquality|float|nCurrentWidth|context|createlink|index|applyBehavior|txtLinkUrl|change|move|catid|restoreSelection|docFrag|snippetList|overflow|valW|pointer|scrollTop|label|saveSelection|sPastedText|bUseCustomFileSelect|offsetParent|defaults|divImageEdit|plugin|chkNewWindow|selImgW|ua|valH|selImgStyle|init|builder|extension|newW|newY|replaceWith|ovl|fonts|absolute|toolbarDisplay|animate|h3|keyCode|h5||h6|h1|wrap|h4|prepareRteCommand|handle|copy|headings|picklist|mousedown|checked|edit|test|big|auto|default|outdent|h2|is|txtHtml|normal|base64|trim|box|snippetPathReplace|axis|Ok|btn|getElementsByTagName|embed|active|plus|family|cancel|item|underline|selSnips|collapse|typeof|srcUrl|imgname|decoration|iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAFElEQVQYV2P8DwQMBADjqCKiggAAmZsj5vuXmnUAAAAASUVORK5CYII|which|delrowconfirm|circle|txtImgUrl|thumb|maskWidth|maskHeight|clearControls|pickalign|NaN|setEndAfter|inpZoom|crop|btnHtmlOk|drawImage|outlined|dragbox|5px||_left_polaroid|Images|figure|20px||center|imgPos|includeHidden|_top_polaroid|keydown|createsrc|imgheight|imgwidth|_top2|html_catselect|com|btnLinkOk|not|setStartAfter|tabImgPl|txtLinkText|tabImgLnk|divToolImgLoader|btnZoomIn|removeAllRanges|activeFrame|rnd|moz|nInitialHeight|nInitialWidth|getElementById|match|on|RegExp|createContextualFragment|vimeoRegexMatches|youRegexMatches|js|removeFormat|htmlData|msie|divSnippetList|addRange|unlink|oScripts|insertNode|H2|bUseCustomImageSelect|H1|myCanvas|txtAltText|htmlThumbs|30px|bUseSnippetsFilter|badTags|getContext|character|sortable|cb_edit|bEditable|rel|solid|sourceX|rgba|tagStripper|placeholder|start|responsive|sourceY|keyup|isCtrl|formatBlock|txtSrc|html_photo_file2|html_photo_file|removeAttribute|panSetup|pick|Paragraph|cats|html_colors|500px|footer|enableZoom|cbarr|extend|Text|both|rv|ifrFileBrowse|10px|snippetCategories|provider|clear|ovlid|push|Caption|toolbar_attr|endContainer|html_rte|_top_adj|333|H6|_blank|onCancel|windowsize|300|Title|H5|H4|rangeCount|fixed|H3|createLink|connectSortable|extractContents|URL|italic|bold|count|btnbrowse|btnLinkBrowse|newcbarr||btnDelRowCancel|btnDelRowOk||btnSrcOk|||focus|opacity|figcaption|indent|hq|ifrFonts|decodeURIComponent|pickfontfamily|ifrFontSize|pickfontsize|ifrHeadings|pickheading|selColorApplyTo|btnCleanColor|textarea|400px|reference|hasClass|bDrop|realtime|pasteContent|600|newzoom|adjx|adjy|225|forceshow|sizing|lastChild|bJUIStable|valueSelected|nodeType|normalize|400|defaultExists|getSelected|3px|bExist|links|divSnippets|tmp|min|formatPara|container|stopImmediatePropagation|800px|scale|transform|getItem|justify|bRichPaste|ranges|10000|control|btnImageCancel||btnZoomOut|btnChangeImage|ececec|form|canvasframe|lnkImageSettings||515151|fafafa|divImgPl|nbsp|sourceEditor|231|111|btnInsertPlh|divImgLnk|btnImageBrowse|btnFileBrowse|divImgLnkOk|btnImgOk|fixedimage|alt|inline|files|jpeg|reader|oimg|Image|snippetOpen|toDataURL|filename|possible|ul|tag|sc|considerScroll|scrollLeft|offsetLeft|offsetTop|badAttributes|script|spacing|letter|isOverAxis2|checkbox|googleapis|viewHtml|stylesheet|substr|destroy|_del|185px|tolerance|900|alert|Please|37px|dummy|off|toString|clientX|clientY|hideDragPreview|333333|ae19b4|1aa71b|0bbfc5|1b83df|f51f58|youRegex|www|youtube|vimeoRegex|vimeo|video|c31313|exec|955705|substring|vimeoMatch|snipHtml|ff9c26|tmptop|888888||pasteFrame|d24fd7|35d037|getSelectionStartNode|0edce2|side|cleanHTML|filter|bullet|550px|anchorNode|00b8ff|fa4273|ff0e0e|cc7f18|linkDialog|onChanged|fed229|d4d4d4|ef97f3|94dd95|html_photo_tool|7eeaed|63d3ff|eraser|ff80aa|ff8d8d|200px|dashed|Edit|d09f5e|pasteHTML|f7e97a|cancelBubble|myTmpCanvas|returnValue|moveEnd|hidImage|hidPath|hidFile|hidRefId|hidImgType|fcd1fe|c6f5c6|60px|b4fdff|spin|bottom|c5efff|ffd4df|moveStart|ffd5d5||e9d4a7||drag|clone|connectToSortable|ffffc5|heading|strikethrough|Background|square|removeData|25px||endOffset|childNodes|inArray|COLORS|half|ifrImageBrowse|isChrome|Link|scripts|actualW|actualH|vendor|changeImage|clearInputs|FileReader|isOpera|to|jpg|opera|isFirefox|onload|regex|Snippet|photoAspectRatio|canvasAspectRatio|load|li|Heading|Info|Subtitle|sleep|mpImg|UA|webkit|LiveEditor_isIE|title|url|snippetTool|snippetFile|onDrop|small|ol|imgWidth|imgHeight|x1||y1|x2|y2|makeid|removeElement|Math|milliseconds|Date|getTime|blockEncoded|re|sHTML|MSIE|appendChild|html_overlay|createElement|ForeColor|original|BackColor|you|divRange|clientLeft|TABLE|innerspanstyle|clientTop|stringStripper|iPod|commentSripper|iPhone|newstyle|sScriptPathArray|fontSize|attributeStripper|line|trident|youMatch|NlyAAAC|klEQVRoQ|2au24aQRSGz|ySkEvPA9AQubNEhXgCSogEShmZGkSQpTS8AjUNSAjXlCRNStpQ8QK8AI6UOLazM5lZvGRvswsz43hYz0iWZe3uzPnOf25rQOVymcAzWsgAZ1xto3DGBQajsFE4Yx4wIZ0xQSM4RmGjcMY8YEI6Y4LKFy0H|9TCJ7b1VsiOo0PaAAv5Wf4ho|CBPjQhneYokRyezWZQKpW4WzuOA71eD5bLZdrx|vahnSz2YRutwu5XC4RZrPZQL1eP33g4XAI1Wo1FeRYlbVQ|FA1U|kfblitVtBut2Nvf3LgQqEAk8kE2G9VC2MM4|EYRqNRZMsnBy4WizCdTiGfz6vidffhqaw98Ha7hU6nA|v1OuCQfr8PLBV46ySB|bAeoL8qJ0GfHLA|D8P9OOmap|jJAXvq1mq12NB1lW404LL|GVqtD5QTPfwwZEJz|DtcXHwEDPf0z3|2mbw17oxvZjhIBgGz71LqFSqcQ6xK8wgT|AyZ0L|AMflNz3MiNYZXpXkKI2SDhfKw3V67xYwXAdGQJhT6lj77SqgbHP3ywMLMITeB8GIn84C9PJ3P5|vYPdGbxYLGAwGABv3k4aPkSIBYAZMg0tfBs4L6kP|yvy7OoKzt6dg3|UTJrQtABmpOHQThs8PGjbeuMrSuDmbdLLhTbAYZXTgJmTEMrBj|sbbs6yPb1KzMIewOJOWiLh7Nog85UH|7vxobO0bb12QYJrV4jCxZA56OuXb26Oq1pSwOGwTgtPz2gLvaRqv9gzOORXpAiyiywN3jdagXtlwaWACbnf9UWBxdRjbWmnLA1l3qK92kYs79UsOeCYaq3GrOAuokNGnC1SwLRWg4NpT37kpREwHUIwzb9HXs8LWKccZsKK|Nv24IBwYdkIGm5jB|8QuVEyh|WA2XDBqjVygfyvheJAaU9KA6cdoNt1A6ybIqrtMQqr9qhu|xmFdVNEtT1GYdUe1W0|o7Buiqi2xyis2qO67WcU1k0R1fb8BZv85KDCNGIQAAAAAElFTkSuQmCC|lnkEditImage|camera||join|divTempContent|undo|redo|deactivate|slideDown|get|465px|371px|27px|160px|polyfill|Cancel|back||onSlide|minus|mouseup|paste|ok|rangeslider|14px|slideUp|shadow|canvasform|method|post|action|delay|enctype|multipart|Target|190px|header|225px|300px|visibility|454545|distance|28px|connectWith|lnkImageLoader|items|13px|eee|sans|serif|sort|2px|f3f3f3|ddd|google|leftside|IMAGE|inpchk|greedy|BLANK|PLACEHOLDER|150|DIMENSION|WxH|50px|Left|property|260||Separator|instances_count|Social|innerText|Square|Circle|319px|REPLACE|textAlign|Video|setAttribute|Alternate|Map|Navigate|metaKey|ctrlKey|Center|cloneContents|innerHTML|Profile|Quotes|List|htmlText|10000px|drop|some|createTextRange|500|moveToElementText|Action|selectNodeContents|gal|Right|335px|elem|Full|removeItem|Call|lastIndexOf|150px|New|gif|bmp|Window|theFile|result|innerWidth|innerHeight|Color|evt|strike|Single|Block|encodeURIComponent|FFFFFF|droppable|Long|watch|polaroid|feature|player_detailpage|youtu|All||700|vertical|MegaPixImage|Default|middle|channels|555|groups|videos|insertUnorderedList|snippets|readAsDataURL|assets|dynamic|version|Bullet|insertOrderedList|350px|player|optionSelected|isOverAxis|numbered|1000|revert|containment|1000px|isOver|ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789|charAt|Numbered|floor|random|Are|setTimeout|None|1e7|break|clearFields|column|sure|date|datetime|email|month|number|password|search|tel|time|week|want|radio|selectedIndex|table|string|Chrome||head|megapix|contents|delete|Google|end|while|Inc|TR|loadHTML|getSelectedNode|isSafari|Safari|Apple|Computer|CANCEL|Mso|zA|primary|OK|thumbnails|meta|xml|st1|appName|Microsoft|applet|noframes|noscript|fileinputs|after|e7e7e7|fakefile|Trident|iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6|nodeValue'.split('|'), 0, {}));

/*! rangeslider.js - v0.3.1 | (c) 2014 @andreruffert | MIT license | https://github.com/andreruffert/rangeslider.js */
eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p } ('\'1Z 2c\';(4(13){7(v 1j===\'4\'&&1j.28){1j([\'1K\'],13)}1h 7(v 2d===\'2r\'){13(2t(\'1K\'))}1h{13(d)}}(4(d){4 1E(){f Z=o.2u(\'Z\');Z.2S(\'1w\',\'h\');9 Z.1w!==\'2m\'}f 8=\'18\',M=[],1r=1E(),17={O:1T,1Q:\'18\',U:\'18--1F\',1v:\'2D\',15:\'1W\',z:[\'23\',\'25\',\'27\'],A:[\'29\',\'2a\',\'2b\'],B:[\'2e\',\'2f\',\'2l\']};4 1o(k,1m){f Q=1G.a.1C.1X(1A,2);9 1u(4(){9 k.19(1M,Q)},1m)}4 1x(k,V){V=V||1q;9 4(){7(!k.14){f Q=1G.a.1C.19(1A);k.1B=k.19(N,Q);k.14=1T}2i(k.1P);k.1P=1u(4(){k.14=1n},V);9 k.1B}}4 b(c,6){3.$N=d(N);3.$o=d(o);3.$c=d(c);3.6=d.2E({},17,6);3.2H=17;3.2R=8;3.z=3.6.z.1f(\'.\'+8+\' \')+\'.\'+8;3.A=3.6.A.1f(\'.\'+8+\' \')+\'.\'+8;3.B=3.6.B.1f(\'.\'+8+\' \')+\'.\'+8;3.O=3.6.O;3.I=3.6.I;3.G=3.6.G;3.C=3.6.C;7(3.O){7(1r){9 1n}}3.R=\'24-\'+8+\'-\'+(+1U 26());3.l=S(3.$c[0].1k(\'l\')||0);3.q=S(3.$c[0].1k(\'q\')||1q);3.5=S(3.$c[0].5||3.l+(3.q-3.l)/2);3.u=S(3.$c[0].1k(\'u\')||1);3.$1a=d(\'<1b 1c="\'+3.6.1v+\'" />\');3.$K=d(\'<1b 1c="\'+3.6.15+\'" />\');3.$h=d(\'<1b 1c="\'+3.6.1Q+\'" 2n="\'+3.R+\'" />\').2o(3.$c).2p(3.$1a,3.$K);3.$c.2q({\'T\':\'2s\',\'1H\':\'1I\',\'2v\':\'1I\',\'2w\':\'2x\',\'2y\':\'0\'});3.J=d.1g(3.J,3);3.H=d.1g(3.H,3);3.F=d.1g(3.F,3);3.1l();f P=3;3.$N.E(\'1Y\'+\'.\'+8,1x(4(){1o(4(){P.16()},21)},20));3.$o.E(3.z,\'#\'+3.R+\':22(.\'+3.6.U+\')\',3.J);3.$c.E(\'1p\'+\'.\'+8,4(e,m){7(m&&m.1s===8){9}f 5=e.1t.5,j=P.W(5);P.D(j)})}b.a.1l=4(){7(3.I&&v 3.I===\'4\'){3.I()}3.16()};b.a.16=4(){3.X=3.$K[0].1y;3.1z=3.$h[0].1y;3.Y=3.1z-3.X;3.w=3.X/2;3.T=3.W(3.5);7(3.$c[0].1F){3.$h.2g(3.6.U)}1h{3.$h.2h(3.6.U)}3.D(3.T)};b.a.J=4(e){e.1d();3.$o.E(3.A,3.H);3.$o.E(3.B,3.F);7((\' \'+e.1t.2j+\' \').2k(/[\\n\\t]/g,\' \').1D(3.6.15)>-1){9}f p=3.10(3.$h[0],e),11=3.12(3.$K[0])-3.12(3.$h[0]);3.D(p-3.w);7(p>=11&&p<11+3.X){3.w=p-11}};b.a.H=4(e){e.1d();f p=3.10(3.$h[0],e);3.D(p-3.w)};b.a.F=4(e){e.1d();3.$o.L(3.A,3.H);3.$o.L(3.B,3.F);f p=3.10(3.$h[0],e);7(3.C&&v 3.C===\'4\'){3.C(p-3.w,3.5)}};b.a.1J=4(j,l,q){7(j<l){9 l}7(j>q){9 q}9 j};b.a.D=4(j){f 5,r;5=(3.1L(3.1J(j,0,3.Y))/3.u)*3.u;r=3.W(5);3.$1a[0].1i.1H=(r+3.w)+\'1N\';3.$K[0].1i.r=r+\'1N\';3.1O(5);3.T=r;3.5=5;7(3.G&&v 3.G===\'4\'){3.G(r,5)}};b.a.12=4(s){f i=0;2z(s!==1M){i+=s.2A;s=s.2B}9 i};b.a.10=4(s,e){9(e.2C||e.1R.1S||e.1R.2F[0].1S||e.2G.x)-3.12(s)};b.a.W=4(5){f y,j;y=(5-3.l)/(3.q-3.l);j=y*3.Y;9 j};b.a.1L=4(j){f y,5;y=((j)/(3.Y||1));5=3.u*2I.2J((((y)*(3.q-3.l))+3.l)/3.u);9 2K((5).2L(2))};b.a.1O=4(5){7(5!==3.5){3.$c.2M(5).2N(\'1p\',{1s:8})}};b.a.2O=4(){3.$o.L(3.z,\'#\'+3.R,3.J);3.$c.L(\'.\'+8).2P(\'1i\').2Q(\'1e\'+8);7(3.$h&&3.$h.1V){3.$h[0].2T.2U(3.$h[0])}M.2V(M.1D(3.$c[0]),1);7(!M.1V){3.$N.L(\'.\'+8)}};d.k[8]=4(6){9 3.2W(4(){f $3=d(3),m=$3.m(\'1e\'+8);7(!m){$3.m(\'1e\'+8,(m=1U b(3,6)));M.2X(3)}7(v 6===\'2Y\'){m[6]()}})}}));', 62, 185, '|||this|function|value|options|if|pluginName|return|prototype|Plugin|element|jQuery||var||range||pos|fn|min|data||document|posX|max|left|node||step|typeof|grabX||percentage|startEvent|moveEvent|endEvent|onSlideEnd|setPosition|on|handleEnd|onSlide|handleMove|onInit|handleDown|handle|off|pluginInstances|window|polyfill|_this|args|identifier|parseFloat|position|disabledClass|debounceDuration|getPositionFromValue|handleWidth|maxHandleX|input|getRelativePosition|handleX|getPositionFromNode|factory|debouncing|handleClass|update|defaults|rangeslider|apply|fill|div|class|preventDefault|plugin_|join|proxy|else|style|define|getAttribute|init|wait|false|delay|change|100|inputrange|origin|target|setTimeout|fillClass|type|debounce|offsetWidth|rangeWidth|arguments|lastReturnVal|slice|indexOf|supportsRange|disabled|Array|width|1px|cap|jquery|getValueFromPosition|null|px|setValue|debounceTimeout|rangeClass|originalEvent|clientX|true|new|length|rangeslider__handle|call|resize|use||300|not|mousedown|js|touchstart|Date|pointerdown|amd|mousemove|touchmove|pointermove|strict|exports|mouseup|touchend|addClass|removeClass|clearTimeout|className|replace|pointerup|text|id|insertAfter|prepend|css|object|absolute|require|createElement|height|overflow|hidden|opacity|while|offsetLeft|offsetParent|pageX|rangeslider__fill|extend|touches|currentPoint|_defaults|Math|ceil|Number|toFixed|val|trigger|destroy|removeAttr|removeData|_name|setAttribute|parentNode|removeChild|splice|each|push|string'.split('|'), 0, {}));

/*! jQuery UI Touch Punch 0.2.3 | Copyright 2011–2014, Dave Furfero | Dual licensed under the MIT or GPL Version 2 licenses. */
eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p } ('(7(4){4.w.8=\'H\'G p;c(!4.w.8){f}d 6=4.U.D.L,g=6.g,h=6.h,a;7 5(2,r){c(2.k.F.J>1){f}2.B();d 8=2.k.q[0],l=p.N(\'O\');l.S(r,i,i,V,1,8.W,8.X,8.Y,8.A,b,b,b,b,0,C);2.z.E(l)}6.m=7(2){d 3=e;c(a||!3.I(2.k.q[0])){f}a=i;3.j=b;5(2,\'K\');5(2,\'s\');5(2,\'M\')};6.n=7(2){c(!a){f}e.j=i;5(2,\'s\')};6.o=7(2){c(!a){f}5(2,\'P\');5(2,\'Q\');c(!e.j){5(2,\'R\')}a=b};6.g=7(){d 3=e;3.u.T({v:4.9(3,\'m\'),x:4.9(3,\'n\'),y:4.9(3,\'o\')});g.t(3)};6.h=7(){d 3=e;3.u.Z({v:4.9(3,\'m\'),x:4.9(3,\'n\'),y:4.9(3,\'o\')});h.t(3)}})(4);', 62, 62, '||event|self|jQuery|simulateMouseEvent|mouseProto|function|touch|proxy|touchHandled|false|if|var|this|return|_mouseInit|_mouseDestroy|true|_touchMoved|originalEvent|simulatedEvent|_touchStart|_touchMove|_touchEnd|document|changedTouches|simulatedType|mousemove|call|element|touchstart|support|touchmove|touchend|target|clientY|preventDefault|null|mouse|dispatchEvent|touches|in|ontouchend|_mouseCapture|length|mouseover|prototype|mousedown|createEvent|MouseEvents|mouseup|mouseout|click|initMouseEvent|bind|ui|window|screenX|screenY|clientX|unbind'.split('|'), 0, {}));
