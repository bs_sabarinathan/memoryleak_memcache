﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;



namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface ITaskMembers
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int RoleID
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }

        int TaskID
        {
            get;
            set;

        }

        int? ApprovalStatus
        {
            get;
            set;

        }

        int ApprovalRount
        {
            get;
            set;

        }

        string FlagColorCode
        {
            get;
            set;
        }

        string UserName
        {
            get;
            set;

        }

        string DepartmentName
        {
            get;
            set;

        }

        string Role
        {
            get;
            set;

        }

        string Title
        {
            get;
            set;

        }

        string UserEmail
        {
            get;
            set;

        }
        DateTime? OverdueMailSent
        {
            get;
            set;

        }

        bool IsTaskalertSent
        {
            get;
            set;

        }
        int PhaseID
        {
            get;
            set;
        }
        int StepId
        {
            get;
            set;
        }
        int VersionID
        {
            get;
            set;
        }
        bool IsActive
        {
            get;
            set;
        }
        int StepRoleID
        {
            get;
            set;
        }
        bool IsGlobal
        {
            get;
            set;
        }
        int EntityRoleID
        {
            get;
            set;
        }

        #endregion
    }
}
