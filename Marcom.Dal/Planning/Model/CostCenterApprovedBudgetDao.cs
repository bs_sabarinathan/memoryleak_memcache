﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// CostCenterApprovedBudgetDao object for table 'PM_CostCenterApprovedBudget'.
    /// </summary>

    public partial class CostCenterApprovedBudgetDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _costCentID;
        protected int _userID;
        protected DateTime _approveTime;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public CostCenterApprovedBudgetDao() { }

        public CostCenterApprovedBudgetDao(int pCostCentID, int pUserID, DateTime pApproveTime)
        {
            this._costCentID = pCostCentID;
            this._userID = pUserID;
            this._approveTime = pApproveTime;

        }

        public CostCenterApprovedBudgetDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int CostCentreID
        {
            get { return _costCentID; }
            set { _bIsChanged |= (_costCentID != value); _costCentID = value; }

        }



        public virtual int UserID
        {
            get { return _userID; }
            set { _bIsChanged |= (_userID != value); _userID = value; }

        }




        public virtual DateTime ApproveTime
        {
            get { return _approveTime; }
            set { _bIsChanged |= (_approveTime != value); _approveTime = value; }

        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttachmentsDao = "CostCenterApprovedBudgetDao";
            public const string ID = "ID";
            public const string CostCentreID = "CostCentreID";
            public const string UserID = "UserID";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string ApproveTime = "ApproveTime";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttachmentsDao = "CostCenterApprovedBudgetDao";
            public const string ID = "ID";
            public const string CostCentreID = "CostCentreID";
            public const string UserID = "UserID";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string ApproveTime = "ApproveTime";
        }

        #endregion


    }

}
