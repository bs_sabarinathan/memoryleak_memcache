﻿(function (ng, app) {
    "use strict";

    function entitydamselectionCtrl($window, $location, $timeout, $scope, $cookies, $resource, DamentityselectService, $compile) {
        var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
        $scope.AssetSelectionFiles = [];
        $scope.callbackid = 0;
        $scope.FolderID = 1;
        var apple_selected, tree;
        $scope.listViewselectall = {
            IsChecked: false
        }
        $scope.Assetobj = [{ "AssetFiles": [], "AssetDynData": [], "Response": {} }];
        $scope.my_tree_handler = function (branch) {
            if ($scope.callbackid > 0) {
                $scope.PageNoobj.pageno = 1;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus.filterstatus;
                });
                $scope.selectedTree = branch;
                $scope.EntityAssetCreation.selectedTree = $scope.selectedTree;
                if ($scope.callbackid > 0) $timeout(function () {
                    GetAssetsData(branch.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }, 100);
            } else {
                $scope.callbackid = $scope.callbackid + 1;
            }
        };
        $scope.FilterByOptionChange = function (id) {
            if (id != $scope.FilterStatus.filterstatus) {
                $scope.PageNoobj.pageno = 1;
                $scope.FilterStatus.filterstatus = id;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.DamViewName.viewName = res[0].Name;
                var b;
                b = tree.get_selected_branch();
                if (b != null) {
                    GetAssetsData(b.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }
            }
        }
        $scope.OrderbyOption = function (orderid) {
            var branch;
            branch = tree.get_selected_branch();
            if (orderid != $scope.OrderBy.order) {
                $scope.PageNoobj.pageno = 1;
                $scope.OrderBy.order = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $scope.OrderbyName.orderbyname = orderobj[0].Name;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus.filterstatus;
                });
                if (branch != null) {
                    GetAssetsData(branch.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                }
            }
        }
        $scope.my_data = [];
        $scope.EntityAssetCreation.Dam_Directory = [];
        $scope.my_tree = tree = {};
        $scope.ParentEntityId = 0;
        $scope.DamViewName.viewName = "Thumbnail";
        $scope.OrderbyName.orderbyname = "Creation date (Descending)";
        $scope.$on("callbackforEntitycreation", function (event, folderid, eid, status) {
            $scope.ParentEntityId = eid;
            $scope.DamViewName.viewName = "Thumbnail";
            $scope.OrderbyName.orderbyname = "Creation date (Descending)";
            $scope.AssetSelectionFiles.splice(0, $scope.AssetSelectionFiles.length);
            refreshViewObj();
            PageLoad();
        });

        function PageLoad() {
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.my_tree = tree;
            if ($scope.my_data.length > 0) {
                $scope.my_data.splice(0, $scope.my_data.length);
            }
            $scope.selectedTree = {};
            $scope.EntityAssetCreation.selectedTree = {};
            $scope.subview = "Thumbnail";
            $scope.PageNoobj.pageno = 1;
            GetDAMViewSettings();
        }

        function GetDAMViewSettings() {
            DamentityselectService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsDamAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsDamAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SettingsDamAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                    $scope.try_async_load();
                }
            });
        }
        $scope.try_async_load = function () {
            $scope.doing_async = true;
            return $timeout(function () {
                DamentityselectService.GetEntityDamFolders($scope.ParentEntityId).then(function (data) {
                    var res = [];
                    res = JSON.parse(data.Response);
                    var Children = [];
                    if (res != null) {
                        for (var i = 0, child; child = res.Children[i++];) {
                            Children.push(child);
                        }
                        $timeout(function () {
                            if (Children.length > 0) $scope.initialTreeSelect = Children[0].Caption;
                            $scope.my_data = Children;
                            $scope.FolderID = GenarateUniqueId();
                            if ($scope.my_data == null || $scope.my_data.length == 0) {
                                var obj = [];
                                obj = {
                                    "AttributeId": 0, "Caption": "Default", "Children": [], "ColorCode": "ffd300", "Description": "Auto Generated", "IsDeleted": false, "Key": "51", "Level": 0,
                                    "SortOrder": 1, "id": $scope.FolderID, "isShow": false, "ischecked": false
                                };
                                $scope.my_data.push(obj);
                                $scope.initialTreeSelect = "Default";
                            }
                            $scope.EntityAssetCreation.Dam_Directory = $scope.my_data;
                            $scope.doing_async = false;
                            var treefolder = '<dam-task-tree tree-data="my_data" tree-control="my_tree" on-select="my_tree_handler(branch)" expand-level="2" initial-selection="initialTreeSelect"></dam-task-tree>';
                            $("#entityDamFolder").html($compile(treefolder)($scope));
                            var res = $.grep($scope.TaskStatusObj, function (e) {
                                return e.ID == $scope.MulitipleFilterStatus;
                            });
                            $scope.doing_async = false;
                            tree.expand_all();
                            $timeout(function () {
                                var b;
                                b = tree.get_selected_branch();
                                if (b != null) LoadAssetsData(b.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno);
                            }, 200);
                        }, 100);
                    }
                });
            }, 10);
        };

        function GetAssetsData(folderid, eid, filter, orderby, pageno) {
            DamentityselectService.GetEntityAsset(folderid, eid, filter, orderby, pageno, false).then(function (data) {
                var res = [];
                $scope.Assetobj[0].AssetFiles = [];
                $scope.Assetobj[0].AssetDynData = [];
                res = data.Response;
                if (res != null) {
                    $scope.Assetobj[0].AssetFiles.push(res[0].AssetFiles);
                    $scope.Assetobj[0].AssetDynData.push(res[0].AssetDynData);
                }
                else if ($scope.Assetobj[0].AssetFiles.length == 0) {
                    $scope.Assetobj[0].AssetFiles = [];
                    $scope.Assetobj[0].AssetDynData = [];
                }
                // if ($scope.Assetobj[0].AssetFiles.length > 0) {
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView($scope.Assetobj);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView($scope.Assetobj);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView($scope.Assetobj);
                loadScrollSettings();
                //   }
            });
        }

        function LoadAssetsData(folderid, eid, filter, orderby, pageno) {
            DamentityselectService.GetEntityAsset(folderid, eid, filter, orderby, pageno, false).then(function (data) {
                var res = [];
                res = data.Response;
                if (res != null) {
                    $scope.Assetobj[0].AssetFiles.push(res[0].AssetFiles);
                    $scope.Assetobj[0].AssetDynData.push(res[0].AssetDynData);
                } else {
                    $scope.Assetobj[0].AssetFiles = [];
                    $scope.Assetobj[0].AssetDynData = [];
                }
                // if ($scope.Assetobj[0].AssetFiles.length > 0) {
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView($scope.Assetobj);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView($scope.Assetobj);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView($scope.Assetobj);
                loadScrollSettings();
                //  }
            });
        }

        function getMoreListItems() {
            $scope.PageNoobj.pageno = $scope.PageNoobj.pageno + 1;
            var b;
            b = tree.get_selected_branch();
            DamentityselectService.GetEntityAsset(b.id, $scope.ParentEntityId, $scope.FilterStatus.filterstatus, $scope.OrderBy.order, $scope.PageNoobj.pageno, false).then(function (data) {
                var res = [];
                res = data.Response;
                if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView(res);
                else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView(res);
                else if ($scope.FilterStatus.filterstatus == 3) LoadListView(res);
            });
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }

        function loadScrollSettings() {
            $(".imglistSection").bind("scroll", function (e) {
                if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
                    checkListItemContents();
                }
            });
        }
        $scope.AssetChkClass = {};
        $scope.AssetSelectionClass = {};
        $scope.AssetFiles = [];
        var DynamicData = [];

        function refreshViewObj() {
            $(".imglistSection").scrollTop(0);
            var html = '';
            $("#entityassetdata").html(html);
            $scope.AssetSelection = {};
            $scope.AssetChkClass = {};
            $scope.AssetSelectionClass = {};
            $scope.AssetFiles = [];
            $scope.AssetDynamicData = [];
        }

        function IsAssetSelected(assetid) {
            var remainRecord = [];
            var res = [];
            res = $.grep($scope.SelectedAssetIdFolderID.SelectedAssetIDs, function (e) {
                return e.AssetId == assetid && e.FolderId == (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id);
            });
            if (res.length > 0) {
                return true;
            }
            return false;
        }

        function LoadThumbnailView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null && res[0].AssetFiles.length > 0) {
                var assets = [];
                assets = res[0].AssetFiles[0];
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData[0];
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    html += '<div class="th-Box">';
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = false;

                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID) && $scope.MediaBankSettings.frmCMS == false) {
                        assetselection = "th-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] == true ? "th-Selection selected" : "th-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = assetselection;
                    html += '<div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                    asset.FromPlace = asset.FromPlace == null ? "AssetCreation" : asset.FromPlace;
                    if (asset.FromPlace == "SubEntityCreation")
                        html += '<i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\"  class="checkbox checked"></i>';
                    else
                        html += '<i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-Block">';
                    html += '<div class="th-ImgBlock">';
                    //html += '<div  class="th-ImgContainer">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '\">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType == 1 || asset.ProcessType == 3) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.format) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $("#entityassetdata").append($compile(html)($scope));
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $("#entityassetdata").html(emptyHtml);
                }
            }
        }
        $scope.SelectedAssetIdFolderID.SelectedAssetIDs = [];
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target == null ? event : event.target;
            if ($scope.FilterStatus.filterstatus == 1) $scope.AssetSelectionClass["asset_" + assetid + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id)] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            else if ($scope.FilterStatus.filterstatus == 2) $scope.AssetSelectionClass["asset_" + assetid + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id)] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            else if ($scope.FilterStatus.filterstatus == 3) $scope.AssetSelectionClass["asset_" + assetid + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id)] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    if (res[0] != undefined) {
                        $scope.PageAsset.CmsAssetSelectionFiles.push(assetid);
                        $scope.AssetSelectionFiles.push(assetid);
                    } else {
                        $scope.PageAsset.CmsAssetSelectionFiles.push(assetid);
                        $scope.AssetSelectionFiles.push(assetid);
                    }
                }
                var res = [];
                res = $.grep($scope.SelectedAssetIdFolderID.SelectedAssetIDs, function (e) {
                    return e.AssetId == assetid && e.FolderId == (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id);
                });
                if (res.length == 0) {
                    if (res[0] != undefined) $scope.SelectedAssetIdFolderID.SelectedAssetIDs.push({
                        AssetId: assetid,
                        FolderId: (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id)
                    });
                    else $scope.SelectedAssetIdFolderID.SelectedAssetIDs.push({
                        AssetId: assetid,
                        FolderId: (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id)
                    });
                }
            } else {
                var remainRecord = [];
                $scope.listViewselectall.IsChecked = false;
                $("#SelectAllAssets").removeClass('checked');
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetSelectionFiles), 1);
                    $scope.PageAsset.CmsAssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.PageAsset.CmsAssetSelectionFiles), 1);
                }
                var res = [];
                res = $.grep($scope.SelectedAssetIdFolderID.SelectedAssetIDs, function (e) {
                    return e.AssetId == assetid && e.FolderId == (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id);
                });
                if (res.length > 0) {
                    $scope.SelectedAssetIdFolderID.SelectedAssetIDs.splice($.inArray(res[0], $scope.SelectedAssetIdFolderID.SelectedAssetIDs), 1);
                }
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                        else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function LoadSummaryView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null && res[0].AssetFiles.length > 0) {
                var assets = [];
                assets = res[0].AssetFiles[0];
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData[0];
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                for (var i = 0, asset; asset = assets[i++];) {
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-sum-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox checked";
                    } else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = assetselection;
                    html += '<div class="th-sum-Box"> ';
                    html += '        <div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + i + '" type="checkbox" /> ';
                    asset.FromPlace = asset.FromPlace == null ? "AssetCreation" : asset.FromPlace;
                    if (asset.FromPlace == "SubEntityCreation")
                        html += '                    <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\" class="checkbox checked"></i> ';
                    else
                        html += '                    <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer"> ';
                    if (asset.Category == 0) {
                        if (asset.ProcessType > 0) {
                            if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.AssetUniqueID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if (asset.Status == 3) {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '                                   <i id="Publishicon_' + asset.AssetID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                    } else {
                        if (asset.IsPublish == true) html += '                                   <i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetUniqueID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    html += GenereteSummaryDetailBlock(asset);
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                }
                $("#entityassetdata").append($compile(html)($scope));
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $("#entityassetdata").html(emptyHtml);
                }
            }
        }

        function GenereteSummaryDetailBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="' + cls.toString() + '">-</span>';
                        } else html += '<span class="' + cls.toString() + '">-</span>';
                    } else html += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += '                                    <label class="control-label">' + attr.Caption + '</label> ';
                    html += '                                    <div class="controls"> ';
                    if ($scope.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="control-label">-</span>';
                        } else {
                            html += '<label class="control-label">-</label> ';
                        }
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += '                                    </div> ';
                    html += '                                </div> ';
                }
            }
            return html;
        }

        function LoadListView(res) {
            var html = '';
            if ($scope.PageNoobj.pageno == 1) {
                refreshViewObj();
            }
            if (res != null && res[0].AssetFiles.length > 0) {
                var assets = [];
                assets = res[0].AssetFiles[0];
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData[0];
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetDynamicData.push(dyn);
                }
                if ($scope.PageNoobj.pageno == 1) {
                    html += '<div>';
                    html += '    <table id="AssetListViewTableTask" class="table table-normal-left ListviewTable">';
                    html += '        <thead>';
                    html += GenerateHeader();
                    html += '        </thead>';
                    html += '        <tbody>';
                    html += GenerateList(assets);
                    html += '        </tbody>';
                    html += '    </table>';
                    html += '</div>';
                    $("#entityassetdata").append($compile(html)($scope));
                } else {
                    html += GenerateList(assets);
                    $("#AssetListViewTableTask tbody").append($compile(html)($scope));
                }
                if ($scope.listViewselectall.IsChecked)
                    $("#SelectAllAssets").addClass('checked');
                else
                    $("#SelectAllAssets").removeClass('checked');
            } else {
                if ($scope.PageNoobj.pageno == 1) {
                    var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    $("#entityassetdata").html(emptyHtml);
                }
            }
        }

        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input ng-click="SelectAllAssetFiles($event)" type="checkbox" />';
            html += '           <i id="SelectAllAssets" class="checkbox"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                var assetselection = "";
                if (IsAssetSelected(asset.AssetUniqueID)) {
                    assetselection = "li-Selection selected";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox checked";
                } else {
                    assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] == true ? "li-Selection selected" : "li-Selection";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = "checkbox";
                }
                $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + ""] = assetselection;
                html += '<tr ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\">';
                html += '   <td>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                asset.FromPlace = asset.FromPlace == null ? "AssetCreation" : asset.FromPlace;
                if (asset.FromPlace == "SubEntityCreation")
                    html += '           <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\" class="checkbox checked"></i>';
                else
                    html += '           <i id="chkBoxIcon_' + i + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + "_" + (tree.get_selected_branch() == null ? $scope.FolderID : tree.get_selected_branch().id) + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td>';
                html += '       <span class="thmbListviewImgSpan">';
                html += '           <div class="thmbListview-eIconContainer"> ';
                html += '               <span class="thmbListview-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                        html += '<td>';
                        if ($scope.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetDynamicData, function (e) {
                                return e.ID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                if (data[0]["" + attr.ID + ""] != "") html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                else html += '<span class="thmbListviewDescSpan">-</span>';
                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '</tr>';
            }
            return html;
        }

        function CheckPreviewGenerator() {
            getPreviewIDs();
        }
        var PreviewGeneratorTimer;

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                var data = {};
                data.AssetIDs = assetids;
                DamentityselectService.CheckPreviewGeneratorID(data).then(function (result) {
                    if (result.Response != null) {
                        var generatedids = result.Response.split(',');
                        for (var j = 0; j < generatedids.length; j++) {
                            if (generatedids[j] != "") {
                                var src = $('#' + generatedids[j]).attr('data-src');
                                try {
                                    var upd = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                        return e.ActiveFileID == generatedids[j]
                                    });
                                    if (upd.length > 0) {
                                        $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                            return e.ActiveFileID == generatedids[j]
                                        })[0].Status = 2;
                                    }
                                    $scope.EntityAssetCreation.AssetFilesObj = $scope.AssetFilesObj;
                                    var lightboxid = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    });
                                    if (lightboxid.length > 0) {
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].Status = 2;
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].AssetStatus = 2;
                                    }
                                } catch (e) { }
                                var extn = '.jpg';
                                if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                    if (GlobalAssetEditID == generatedids[j]) {
                                        $('#asseteditimage').attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Big_' + src + '' + extn + '?' + generateUniqueTracker());
                                    }
                                }
                                $('#' + generatedids[j]).removeClass('loadingImg');
                                $('#' + generatedids[j]).attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
                            }
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator();
            }, 3000);
        }
        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamentityselectService.GetAttributeDetails(assetid).then(function (attrList) {
                    if (attrList.Response != null) {
                        $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                            return e.ID == attrList.Response.ActiveFileID;
                        })[0];
                        var html = '';
                        html += '<div class="th-AssetInfoTooltip">';
                        html += DrawThumbnailBlock(attrList);
                        html += '</div> ';
                        $('#' + qtip_id).html(html);
                    }
                    else {
                        if ($scope.Assetobj[0] != null) {
                            $scope.Assetobj[0].Response = $.grep($scope.Assetobj[0].AssetFiles[0], function (e) { return e.AssetID == assetid; })[0];
                            $scope.ActiveVersion = 1;
                            var html = '';
                            html += '<div class="th-AssetInfoTooltip">';
                            html += DrawThumbnailBlock($scope.Assetobj[0]);
                            html += '</div> ';
                            $('#' + qtip_id).html(html);
                        }
                    }
                });
            }
        };
        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamentityselectService.GetAttributeDetails(assetid).then(function (attrList) {
                    if (attrList.Response != null) {
                        $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                            return e.ID == attrList.Response.ActiveFileID;
                        })[0];
                    } else {
                        attrList = $scope.Assetobj[0];
                        $scope.ActiveVersion = attrList.Response;
                    }
                    var html = '';
                    html += '       <div class="lv-ImgDetailedPreview">';
                    html += '           <div class="idp-Box"> ';
                    html += '               <div class="idp-Block"> ';
                    html += '                   <div class="idp-leftSection"> ';
                    html += '                       <div class="idp-ImgBlock"> ';
                    html += '                           <div class="idp-ImgDiv"> ';
                    html += '                               <div class="idp-ImgContainer"> ';
                    if (attrList.Response.Category == 0) {
                        if ($scope.ActiveVersion.ProcessType > 0) {
                            if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 3) {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (attrList.Response.Category == 1) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (attrList.Response.Category == 2) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                               </div> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                       <div class="idp-baiscDetailBlock"> ';
                    html += '                           <div class="idp-ActionButtonContainer"> ';
                    html += '                               <span> ';
                    html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (attrList.Response.Category == 0) {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (attrList.Response.LinkedAssetID != 0) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                               </span> ';
                    html += '                           </div> ';
                    html += '                           <div class="idp-detail-eIconContainer"> ';
                    html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '                   <div class="idp-rightSection"> ';
                    html += '                       <div class="idp-expandedDetailBlock"> ';
                    html += '                           <div class="idp-expandedDetails"> ';
                    html += '                               <form class="form-horizontal"> ';
                    try {
                        html += DrawListBlock(attrList);
                    } catch (e) { }
                    html += '                               </form> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '               </div> ';
                    html += '           </div> ';
                    html += '       </div>';
                    $('#' + qtip_id).html(html);
                });
            }
        };
        function GetDAMToolTipSettings() {
            DamentityselectService.GetDAMToolTipSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsAttributes["ThumbnailToolTipSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsAttributes["ListViewToolTipSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }
        $scope.SettingsAttributes = {};
        GetDAMToolTipSettings();
        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ThumbnailToolTipSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.SettingsAttributes["ThumbnailToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    if (attrList.Response.FromPlace == null) {
                        attrList.Response.FromPlace = "AssetCreation"
                    }
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var isexist = {};
                        if (attrList.Response.FromPlace == "SubEntityCreation") {
                            isexist = $.grep(attrList.Response.AttributeData, function (e) {
                                return e.AttributeID == tooltipattrs[l].ID
                            })[0];
                            if (isexist != null) { isexist.TypeID = isexist.AttributeTypeID; isexist.Lable = isexist.AttributeCaption; isexist.Caption = isexist.NodeID; }
                        } else {
                            isexist = $.grep(attrList.Response.AttributeData, function (e) {
                                return e.ID == tooltipattrs[l].ID
                            })[0];
                        }
                        if (isexist != null) $scope.attributedata.push(isexist);
                    }
                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span></div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datstartval = new Date.create($scope.attributedata[i].Value[j].Startdate);
                                datendval = new Date.create($scope.attributedata[i].Value[j].EndDate);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datstartval = new Date.create($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            html += '<div class=\"th-AssetInfoRow\"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span> {{fields.DateTime_' + $scope.attributedata[i].ID + '}}</div></div>';
                        } else {
                            html += '<div class=\"th-AssetInfoRow\"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    }
                }
            }
            return html;
        }
        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ListViewToolTipSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.SettingsAttributes["ListViewToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attrlist = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (attrlist != null) $scope.attributedata.push(attrlist);
                    }
                }
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Value + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.$on('callbackstoptimer', function (event) {
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.$on("callbackaftercreation", function () {
            $timeout.cancel();
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.$on("callbackacivatetimercall", function () {
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 5000);
        });

        $scope.SelectAllAssetFiles = function (event) {
            var status = event.target.checked;
            if (status)
                $scope.listViewselectall.IsChecked = true;
            else
                $scope.listViewselectall.IsChecked = false;
            $('#AssetListViewTableTask > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = true;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                } else {
                    $(this).next('i').removeClass('checked');
                    var b;
                    for (var a = 0; b = $scope.AssetFiles[a]; a++) {
                        b["Checked"] = false;
                        $scope.checkSelection(b.AssetID, b.ActiveFileID, event);
                    }
                }
            });
        }
        function GenarateUniqueId() {
            var date = new Date();
            var components = [date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()];
            return components.join('');
        }
        $scope.$on('LoadAssetTOEntityCreation', function (event, SaveAssetObj, fileid) {
            var assetid = GenarateUniqueId();
            SaveAssetObj[0].AssetAccesscount = 0; SaveAssetObj[0].AssetName = SaveAssetObj[0].Name; SaveAssetObj[0].FileUniqueID = assetid; SaveAssetObj[0].AssetUniqueID = assetid; SaveAssetObj[0].ActiveFileID = assetid;
            SaveAssetObj[0].FolderID = $scope.EntityAssetCreation.Dam_Directory[0].id; SaveAssetObj[0].OwnerID = $scope.UserId; SaveAssetObj[0].AssetTypeid = SaveAssetObj[0].Typeid; SaveAssetObj[0].Category = 0; SaveAssetObj[0].AssetID = assetid;
            SaveAssetObj[0].IsPublish = false; SaveAssetObj[0].LinkURL = null; SaveAssetObj[0].LinkUserDetail = ""; SaveAssetObj[0].LinkedAssetID = 0; SaveAssetObj[0].EntityID = $scope.ParentEntityId;
            SaveAssetObj[0].CreatedOn = (new Date.create()).toString(); SaveAssetObj[0].IsPublish = false; SaveAssetObj[0].publishedON = ""; SaveAssetObj[0].PublisherID = 0; SaveAssetObj[0].publishedby = null;
            SaveAssetObj[0].FromPlace = "SubEntityCreation";
            $scope.Assetobj[0].Response = (SaveAssetObj[0]);
            var dynobj = {};
            if ($scope.Assetobj[0].AssetFiles.length > 0) {
                $scope.Assetobj[0].AssetFiles[0].push(SaveAssetObj[0]);
                for (var i = 0; i < SaveAssetObj[0].AttributeData.length; i++) {
                    dynobj[SaveAssetObj[0].AttributeData[i].AttributeID] = SaveAssetObj[0].AttributeData[i].NodeID;
                }
                dynobj["ID"] = SaveAssetObj[0].AssetID;
                dynobj["68"] = SaveAssetObj[0].AssetName;
                $scope.Assetobj[0].AssetDynData[0].push(dynobj);
                $scope.EntityAssetCreation.AssetFilesObj = $scope.Assetobj[0].AssetFiles;
            } else {
                $scope.Assetobj[0].AssetFiles[0] = ([SaveAssetObj[0]]);
                for (var i = 0; i < SaveAssetObj[0].AttributeData.length; i++) {
                    dynobj[SaveAssetObj[0].AttributeData[i].AttributeID] = SaveAssetObj[0].AttributeData[i].NodeID;
                }
                dynobj["ID"] = SaveAssetObj[0].AssetID;
                dynobj["68"] = SaveAssetObj[0].AssetName;
                $scope.Assetobj[0].AssetDynData[0] = ([dynobj]);
                $scope.EntityAssetCreation.AssetFilesObj = $scope.Assetobj[0].AssetFiles;
            }
            if ($scope.FilterStatus.filterstatus == 1) LoadThumbnailView($scope.Assetobj);
            else if ($scope.FilterStatus.filterstatus == 2) LoadSummaryView($scope.Assetobj);
            else if ($scope.FilterStatus.filterstatus == 3) LoadListView($scope.Assetobj);
            var checkbox = { "checked": true };
            $scope.checkSelection(SaveAssetObj[0].AssetID, SaveAssetObj[0].ActiveFileID, checkbox);
            loadScrollSettings();
        });
    }
    app.controller("mui.DAM.entitydamselectionCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', 'DamentityselectService', '$compile', entitydamselectionCtrl]);
})(angular, app);