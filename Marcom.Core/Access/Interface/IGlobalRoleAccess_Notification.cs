﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    public interface IGlobalRoleAccess_Notification
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int GlobalRoleID
        {
            get;
            set;

        }

        int NotificationTempID
        {
            get;
            set;

        }

        #endregion
    }
}
