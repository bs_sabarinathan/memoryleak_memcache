using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// FundingRequest object for table 'PM_FundingRequest'.
    /// </summary>

    internal class FundingRequest : IFundingRequest
    {
        #region Member Variables

        protected int _id;
        protected int _requestedby;
        protected int _costcenterid;
        protected int _entityid;
        protected int _FundRequestSTATUS;
        protected DateTimeOffset _lastupdatedon;
        protected string _duedate;
        protected string _ownwrName;
        protected string _costcenterName;
        protected string _description;
        protected string _requestamount;
        protected string _requestdate;


        #endregion

        #region Constructors
        public FundingRequest() { }

        public FundingRequest(int pRequestedBy, int pCostCenterid, int pEntityid, int pFundRequestSTATUS, DateTimeOffset pLastUpdatedOn)
        {
            this._requestedby = pRequestedBy;
            this._costcenterid = pCostCenterid;
            this._entityid = pEntityid;
            this._FundRequestSTATUS = FundRequestSTATUS;

        }

        public FundingRequest(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }


        public int RequestedBy
        {
            get { return _requestedby; }
            set { _requestedby = value; }

        }

        public int CostCenterid
        {
            get { return _costcenterid; }
            set { _costcenterid = value; }

        }

        public int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual int FundRequestSTATUS
        {
            get { return _FundRequestSTATUS; }
            set { _FundRequestSTATUS = value; }

        }

        public DateTimeOffset LastUpdatedOn
        {
            get { return _lastupdatedon; }
            set { _lastupdatedon = value; }

        }

        public string DueDate
        {
            get { return _duedate; }
            set { _duedate = value; }

        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public string CostCenterName
        {
            get { return _costcenterName; }
            set { _costcenterName = value; }

        }

        public string CostCenterOwnerName
        {
            get { return _ownwrName; }
            set { _ownwrName = value; }

        }


        public string RequestAmount
        {
            get { return _requestamount; }
            set { _requestamount = value; }

        }
        public string RequestDate
        {
            get { return _requestdate; }
            set { _requestdate = value; }

        }

        public int Status { get; set; }
        public int CostCentreOwnerID { get; set; }
        public string RequestUser { get; set; }
        public int Duedates { get; set; }
        public decimal CCPreviousAllApprovedAmount { get; set; }
        public decimal CCTotalApprovedAmountThisLevel { get; set; }
        public decimal CCAvailableAmount { get; set; }
        public decimal CCAssignedAmount { get; set; }
        public decimal AvailableAssigneAmount { get; set; }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FundingRequest castObj = null;
            try
            {
                castObj = (FundingRequest)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
