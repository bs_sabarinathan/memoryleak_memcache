﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Report.Model
{
    /// <summary>
	/// ReportCredentialDao object for table 'RM_ReportCredential'.
	/// </summary>
	public partial class ReportCredentialDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected int _dataViewID;
		protected string _reporturl;
		protected string _adminusername;
		protected string _adminpassword;
		protected string _viewerusername;
        protected string _viewerpassword;
		protected int _category;
        protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ReportCredentialDao() {}

        public ReportCredentialDao(string Preporturl, string padminusername, string padminpassword, string pviewerusername, string pviewerpassword, int pcategory, int PdataViewID)
		{
			this._reporturl = Preporturl; 
			this._adminusername = padminusername; 
			this._adminpassword = padminpassword; 
			this._viewerusername = pviewerusername; 
			this._viewerpassword = pviewerpassword; 
			this._category = pcategory; 
            this._dataViewID = PdataViewID;
		}

				
		public ReportCredentialDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
        public virtual int DataViewID
        {
            get { return _dataViewID; }
            set { _bIsChanged |= (_dataViewID != value); _dataViewID = value; }

        }
		
		public virtual string ReportUrl
		{
			get { return _reporturl; }
			set 
			{
			  if (value != null && value.Length > 200)
			    throw new ArgumentOutOfRangeException("Reporturl", "Reporturl value, cannot contain more than 200 characters");
			  _bIsChanged |= (_reporturl != value); 
			  _reporturl = value; 
			}
			
		}
		public virtual string AdminUsername
		{
			get { return _adminusername; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("AdminUsername", "AdminUsername value, cannot contain more than 100 characters");
			  _bIsChanged |= (_adminusername != value); 
			  _adminusername = value; 
			}
			
		}
		public virtual string AdminPassword
		{
			get { return _adminpassword; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("AdminPassword", "AdminPassword value, cannot contain more than 100 characters");
			  _bIsChanged |= (_adminpassword != value); 
			  _adminpassword = value; 
			}
			
		}
        public virtual string ViewerUsername
        {
            get { return _viewerusername; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("ViewerUsername", "ViewerUsername value, cannot contain more than 100 characters");
                _bIsChanged |= (_viewerusername != value);
                _viewerusername = value;
            }

        }

		public virtual string ViewerPassword
		{
			get { return _viewerpassword; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("viewerPassword", "Email value, cannot contain more than 100 characters");
			  _bIsChanged |= (_viewerpassword != value); 
			  _viewerpassword = value; 
			}
			
		}
		
		public virtual int Category
		{
			get { return _category; }
            set { _bIsChanged |= (_category != value); _category = value; }
			
			}
			
		
	#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ReportCredentialDao = "ReportCredentialDao";			
			public const string Id = "Id";			
			public const string ReportUrl = "ReportUrl";			
			public const string AdminUsername = "AdminUsername";			
			public const string AdminPassword = "AdminPassword";			
			public const string ViewerUsername = "ViewerUsername";
            public const string ViewerPassword = "ViewerPassword";
			public const string Category = "Category";	
            public const string DataViewID = "DataViewID";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ReportCredentialDao = "ReportCredentialDao";			
			public const string Id = "Id";			
			public const string ReportUrl = "ReportUrl";			
			public const string AdminUsername = "AdminUsername";			
			public const string AdminPassword = "AdminPassword";			
			public const string ViewerUsername = "ViewerUsername";
            public const string ViewerPassword = "ViewerPassword";
			public const string Category = "Category";	
            public const string DataViewID = "DataViewID";
		}

		#endregion
		
		
	}
	
}


