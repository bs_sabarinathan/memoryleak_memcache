using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// Ilinks interface for table 'CM_Links'.
    /// </summary>
    public interface Ilinks
    {
        #region Public Properties

         int ID
        {
            get;
            set;

        }
        
         int EntityID
         {
             get;
             set;

         }

         string Name
         {
             get;
             set;

         }

         string URL
         {
             get;
             set;
         }

         int ActiveVersionNo
         {
             get;
             set;
         }


         int TypeID
         {
             get;
             set;
         }

         int LinkType
         {
             get;
             set;
         }

         string CreatedOn
         {
             get;
             set;
         }
         int OwnerID
         {
             get;
             set;
         }

         int ModuleID
         {
             get;
             set;
         }
         Guid LinkGuid
         {
             get;
             set;
         }

         string Description
         {
             get;
             set;
         }

        #endregion
    }
}
