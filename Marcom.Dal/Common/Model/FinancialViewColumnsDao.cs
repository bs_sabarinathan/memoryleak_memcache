﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    public partial class FinancialViewColumnsDao : BaseDao 
    {
        public virtual int Id { get; set; }
        public virtual int ViewId { get; set; }
        public virtual string Caption { get; set; }
        public virtual int StateID { get; set; }
        public virtual int CatId { get; set; }
    }
}
