﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using BrandSystems.Library.Threading;
using BrandSystems.Library.Threading.Internal;
using System.Reflection;
using System.Configuration;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Utility;
using System.Drawing;
using System.Net.Mail;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Dal.Common.Model;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Configuration;
using System.Net.Configuration;
using System.IO;
using System.Xml.Linq;

using BrandSystems.Marcom.Core.Common;
using System.Data.SqlClient;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.DAM.Model;
using BrandSystems.Marcom.Core.MediaHandlerService;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.Utility;

namespace BrandSystems.Marcom.Core.SearchServer
{
    public class SearchEngineServer
    {

        public SearchEngineServer()
        {

        }

        private static SearchEngineServer _instance = new SearchEngineServer();
        //private static string _header;
        //public static Image _logo;

        public static SearchEngineServer Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }


        public bool Initialize(String smtpServer)
        {
            try
            {


                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        //Create properties for Logo, Header 

        public Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();
        private Dictionary<Guid, IWorkItemsGroup> _workItemGroupList = new Dictionary<Guid, IWorkItemsGroup>();
        private List<Guid> _workItemGroupListIDs = new List<Guid>();

        private bool isIteminloop = false;

        private static BSThreadPool _bsThreadPool;

        #region "Start and Stop Engine"

        /// <summary>
        /// For Starting the Engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StartEngine()
        {
            try
            {
                BSTPStartInfo bstpStartInfo = new BSTPStartInfo();
                bstpStartInfo.IdleTimeout = 30 * 1000; //30 Sec
                bstpStartInfo.MaxWorkerThreads = 30; //Max 30 thread we will create                
                bstpStartInfo.MinWorkerThreads = 0;
                bstpStartInfo.PerformanceCounterInstanceName = "Marcom SearchEngine BSThreadPool";
                _bsThreadPool = new BSThreadPool(bstpStartInfo);

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        /// <summary>
        /// For Stoping the engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StopEngine()
        {


            try
            {
                _bsThreadPool.Shutdown();
                _bsThreadPool.Dispose();
                _bsThreadPool = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        #endregion

        /// <summary>
        /// Checking memory For Synchronous and ASynchronous tasks
        /// </summary>
        /// <param name="obj">MediaHandlerObject</param>
        /// <returns>True or false</returns>
        /// <remarks></remarks>
        public bool AddToQueque(TestHolder obj, string flag)
        {

            try
            {
                var pworkPriority = WorkItemPriority.Highest;
                if (flag == "insert")
                {


                }

                else
                {
                    var workPriority = WorkItemPriority.Lowest;
                    workPriority = pworkPriority;

                    var workItemCallBack = new WorkItemCallback(SendMailEvent);
                    var postExeWorkItemCallBack = new PostExecuteWorkItemCallback(PostSendMailEvent);
                    _bsThreadPool.QueueWorkItem(workItemCallBack, obj, postExeWorkItemCallBack, workPriority);

                }

                return true;
            }
            catch (Exception)
            {

                return true;
            }


        }
        /// <summary>
        /// For executing single Job.
        /// </summary>
        /// <param name="state"></param>
        /// <returns>Media Handler Object</returns>
        /// <remarks></remarks>
        public object SendMailEvent(object state)
        {

            var PreviewHolder = (TestHolder)state;

            try
            {
                object Preview = new object();



            }
            catch (Exception)
            {
                throw;
            }

            return PreviewHolder;
        }
        /// <summary>
        ///  For a single job if  response is nothing it will provide some basic information like error details ,AditionalInfo,GroupID,JobID
        /// </summary>
        /// <param name="result">Provides details like Exception ,In progress,Done.. ,</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public void PostSendMailEvent(IWorkItemResult result)
        {

            if (result.Exception != null)
            {
            }
            else
            {

            }

        }

        public void UpdateLuceneIndex(int TenantID) //send task Preview , Preview for actions that in MH .
        {

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));

                ClsDb clsDb = new ClsDb(marcomManager.User.TenantHost);


                // get the base directory
                string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();

                // search the file below the current directory
                string retFilePath = baseDir + "//" + "LuceneLogwritter.txt";



                IList<Assets> listofglobalaccess = new List<Assets>();
                listofglobalaccess = clsDb.getAssetSearchFiles();

                if (!SearchEngineScheduler.isIteminloop)
                {

                    if (listofglobalaccess.Count > 0)
                    {
                        if (!SearchEngineScheduler.isIteminloop)

                        {


                            foreach (var previewfile in listofglobalaccess)
                            {

                                SearchEngineScheduler.isIteminloop =  true;

                                try
                                {



                                    ErrorLog.LogFilePath = retFilePath;
                                    //false for writing log entry to customized text file
                                    ErrorLog.CustomErrorRoutine(false, "Search index started for id " + previewfile.ID + ", active file id" + previewfile.ActiveFileID + "", DateTime.Now);

                                    try
                                    {

                                        marcomManager.PlanningManager.AddEntityAsyncDamNew(previewfile.ID, previewfile.ActiveFileID, previewfile.searchtype);
                                        ErrorLog.CustomErrorRoutine(false, "Search index done for id " + previewfile.ID + ", active file id" + previewfile.ActiveFileID + "", DateTime.Now);

                                        int status = clsDb.InsertUpdateAssetIndexData("update dam_asset set isIndexed=1 where ID=" + previewfile.ID + "", CommandType.Text);

                                        SearchEngineScheduler.isIteminloop = true;

                                    }

                                    catch (Exception ex)
                                    {
                                        SearchEngineScheduler.isIteminloop = true;
                                        ErrorLog.CustomErrorRoutine(false, "exception is " + ex.Message, DateTime.Now);

                                    }


                                }
                                catch (Exception ex)
                                {
                                    SearchEngineScheduler.isIteminloop = true;

                                    ErrorLog.LogFilePath = retFilePath;
                                    //false for writing log entry to customized text file
                                    ErrorLog.ErrorRoutine(false, ex);

                                }

                                SearchEngineScheduler.isIteminloop = true;

                            }


                           

                            SearchEngineScheduler.isIteminloop = false;

                        }

                    }



                }
                else
                    SearchEngineScheduler.isIteminloop = false;

                //return obj1;
            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("exception generated SE engine" + ex.Message, LogHandler.LogType.Notify);
                // return null;
            }
        }




        public class TestHolder
        {
            public int MaxWidth { get; set; }

        }


    }


}
