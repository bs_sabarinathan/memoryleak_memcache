﻿using BrandSystems.Marcom.Core.Dam;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Dal;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Utility;
using Microsoft.Owin.Hosting;
using System.Configuration;
using BrandSystems.Marcom.Core.ExpireHandler;
using System.Globalization;
using System.Web.Http;
using Presentation.App_Start;
using BrandSystems.Marcom.Core.SearchServer;
using BrandSystems.Marcom.Core.AmazonStorageHelper;

namespace Presentation
{
    public class Global : System.Web.HttpApplication
    {
        private Scheduler objScheduler = null;
        private TaskReminderScheduler objTaskScheduler = null;
        private PreviewScheduler objPreviewScheduler = null;
        private SearchEngineScheduler objseacrhScheduler = null;
        private ExpireScheduler objExpireScheduler = null;
        private EntityStatusScheduler objEntityStatusScheduler = null;
        private Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();


        void Application_Start(object sender, EventArgs e)
        {

            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("global asax file hit happen at" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

            //intialize all schedulers
            InitializeSchedulers();

            //register MVC Web API Attribute routing
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //intialize all marcom tenants
            MarcomManagerFactory.InitializeSystem();

            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("global asax file hit finished  at" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

            Exception ex = Server.GetLastError().GetBaseException();
            string ErrorMessage = ex.Message;
            string StackTrace = ex.StackTrace;
            string ExceptionType = ex.GetType().FullName;

            string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            string retFilePath = baseDir + "//" + "sessionCrashErrorLog.txt";



        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            Exception ex = Server.GetLastError().GetBaseException();
            string ErrorMessage = ex.Message;
            string StackTrace = ex.StackTrace;
            string ExceptionType = ex.GetType().FullName;

            string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            string retFilePath = baseDir + "//" + "sessionCrashErrorLog.txt";

        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        void Session_Start(object sender, EventArgs e)
        {

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        //Function to loop through each tenant and initalize schedulers for mail, task, preview etc
        void InitializeSchedulers()
        {
            
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlElement xelRoot = xdcDocument.DocumentElement;
            System.Xml.XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
            int tenantid;
            string TenantsFilePath = string.Empty;
            //loop through each tenant 
            foreach (System.Xml.XmlNode xndNode in xnlNodes)
            {
                ApplicationSettings.Clear();
                TenantsFilePath = xndNode["FilePath"].InnerText;
                tenantid = Int16.Parse(xndNode["TenantID"].InnerText);


                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantid);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);

                string xelementName = "MailConfig";
                XElement xelementFilepath;
                string xmlpath = "";
                XDocument adminXmlDoc;

                if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                    adminXmlDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                    xelementFilepath = AWSHelper.ReadAdminXElement(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                }
                else
                {
                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                    adminXmlDoc = XDocument.Load(xmlpath);
                    xelementFilepath = XElement.Load(xmlpath);
                }
                //The Key is root node current Settings


                //var xelementFilepath = XElement.Load(xmlpath);
                var xmlElement = xelementFilepath.Element(xelementName);
                foreach (var ele in xmlElement.Descendants())
                {
                    if (ele.Name.ToString() == "SenderConfig")
                    {
                        foreach (var cc in ele.Descendants())
                        {
                            ApplicationSettings.Add(cc.Name.ToString(), cc.Value.ToString());
                        }
                    }
                }
                objScheduler = new Scheduler(ApplicationSettings);
                objScheduler.Start(60, tenantid);

                objTaskScheduler = new TaskReminderScheduler(ApplicationSettings);
                objTaskScheduler.Start(120, tenantid);

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(tenantid));
                objPreviewScheduler = new PreviewScheduler(ApplicationSettings);
                objPreviewScheduler.Start(3, tenantid, marcomManager);
                objEntityStatusScheduler = new EntityStatusScheduler(ApplicationSettings);
                objEntityStatusScheduler.Start(90, tenantid);
                objExpireScheduler = new ExpireScheduler(ApplicationSettings);
                objExpireScheduler.Start(120, tenantid);

                //objseacrhScheduler = new SearchEngineScheduler(ApplicationSettings);
                //objseacrhScheduler.Start(120, tenantid);


                ////Expire handler scheduler logic
                //var DefaultExpirysetting = adminXmlDoc.Descendants("Expirysetting").FirstOrDefault();
                //if (DefaultExpirysetting != null)
                //{
                //    var DefaultExpiryActionON = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionON").FirstOrDefault();
                //    if (DefaultExpiryActionON != null)
                //    {
                //        var ExecutionActionONResponse = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionON").ElementAt(0).Value;
                //        if (ExecutionActionONResponse == "1")
                //        {
                //            var DefaultExpirysettingExecutionTime = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionDateExecutionTime").FirstOrDefault();
                //            if (DefaultExpirysettingExecutionTime != null)
                //            {
                //                var ExecutionTimeResponse = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionDateExecutionTime").ElementAt(0).Value;
                //                //string a = "05:30";
                //                string a = ExecutionTimeResponse;

                //                DateTime Schedulstarttime = new DateTime();
                //                Schedulstarttime = DateTime.ParseExact(a, "H:m", null);

                //                DateTime time = DateTime.Now;
                //                TimeSpan span = time.Subtract(Schedulstarttime);
                //                if (span.Hours <= 1 && span.Hours >= 0)
                //                {
                //                    //time.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                //                    objExpireScheduler = new ExpireScheduler(ApplicationSettings);
                //                    objExpireScheduler.Start(600, tenantid);
                //                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireScheduler start" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                //                }

                //            }
                //        }
                //    }
                //}
            }
        }


    }
}
