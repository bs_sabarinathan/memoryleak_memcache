﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class EntityTaskList : IEntityTaskList
    {
        #region Member Variables

        protected int _id;
        protected string _Name;
        protected string _Description;
        protected int _Sortorder;
        protected int _EntityID;
        protected int _TaskListID;
        protected int _OnTimeStatus;
        protected string _OnTimeComment;
        protected int _activeEntityStateId;
        #endregion

        #region Constructors
        public EntityTaskList() { }

        public EntityTaskList(int pTaskListID, int pEntityID, string pDescription, string pName, int pSortorder)
        {
            this._TaskListID = pTaskListID;
            this._EntityID = pEntityID;
            this._Name = pName;
            this._Description = pDescription;
            this._Sortorder = pSortorder;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }



        public int TaskListID
        {
            get { return _TaskListID; }
            set { _TaskListID = value; }

        }

        public int EntityID
        {
            get { return _EntityID; }
            set { _EntityID = value; }

        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
            }

        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }

        }

        public int Sortorder
        {
            get { return _Sortorder; }
            set { _Sortorder = value; }

        }

        public int OnTimeStatus
        {
            get { return _OnTimeStatus; }
            set { _OnTimeStatus = value; }

        }

        public string OnTimeComment
        {
            get { return _OnTimeComment; }
            set { _OnTimeComment = value; }

        }


        public int TaskInProgress
        {
            get;
            set;

        }

        public int UnAssignedTasks
        {
            get;
            set;

        }


        public int OverDueTasks
        {
            get;
            set;

        }


        public int UnableToComplete
        {
            get;
            set;

        }

        public IList<ITaskList> TaskListDetails
        {
            get;
            set;
        }

        public IList<ITasktemplateCondition> TempCondDetails
        {
            get;
            set;
        }

        public int ActiveEntityStateID
        {
            get { return _activeEntityStateId; }
            set { _activeEntityStateId = value; }
        }

        public int EntityParentID { get; set; }

       
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskTemplate castObj = null;
            try
            {
                castObj = (TaskTemplate)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}