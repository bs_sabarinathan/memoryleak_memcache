﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.planningtool.subscriptionCtrl",
    function muiplanningtoolsubscriptionCtrl($scope, $location, $resource, $timeout, $cookies, requestContext, $compile, $window, $translate, _, CommonService, AccessService) {

        // Get the render context local to this controller (and relevant params).

        // --- Define Scope Variables. ---------------------- //

        // The subview indicates which view is going to be rendered on the page.

        // --- Bind To Scope Events. ------------------------ //


        $scope.SubsrEntitiyID = '';
        $scope.SubscrEntityTypeid = '';
        $scope.SubscrUserId = '';
        $scope.settingsstatus = true;
        //	    var IsSubscribedFromSettings = $resource('common/GetIsSubscribedFromSettings');
        CommonService.GetIsSubscribedFromSettings().then(function (issubscribedfromsettings) {
            //	    var issubscribedfromsettings = IsSubscribedFromSettings.get(function () {
            if (issubscribedfromsettings.Response == false) {
                $scope.settingsstatus = false;
            }
        });

        $scope.SingleLevelSubscription = function () {

            if ($scope.Suscibetext == 'Unsubscribe') {
                //          var DeleteSubscription = $resource('common/GetUnsubscribeMultiSubscriptionById/:EntitiyID/:UserID', { EntitiyID: $scope.SubsrEntitiyID, UserID: $scope.SubscrUserId });
                CommonService.GetUnsubscribeMultiSubscriptionById($scope.SubsrEntitiyID, $scope.SubscrUserId).then(function (deleteSubscription) {
                    //          var deleteSubscription = DeleteSubscription.delete(function () {
                    if (deleteSubscription.Response == true) {
                        LoadSubscriptionAtPageLoad();
                    }
                });
                $scope.Suscibetext = 'Subscribe';

            }
            else {

                //          var AddSubscription = $resource('common/InsertUserSingleEntitySubscription');
                var addSubscription = {};
                //          var addSubscription = new AddSubscription();

                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day + ' ' + hrs;

                addSubscription.UserId = $scope.SubscrUserId;
                addSubscription.EntityId = $scope.SubsrEntitiyID;
                addSubscription.EntitytypeId = $scope.SubscrEntityTypeid;
                addSubscription.SubscribedOn = output;
                addSubscription.LastUpdatedOn = output;
                addSubscription.issubscribe = "true";
                addSubscription.FilterRoleOption = 0;
                if ($scope.settingsstatus == false) {
		                bootbox.alert($translate.instant('LanguageContents.Res_1988.Caption'));
                    return;
                }
                CommonService.InsertUserSingleEntitySubscription(addSubscription).then(function (getaddSubscription) {
                    //          var getaddSubscription = AddSubscription.save(addSubscription, function () {
                    if (getaddSubscription.Response == true) {
		                    NotifySuccess($translate.instant('LanguageContents.Res_4250.Caption'));
                        $scope.Suscibetext = 'Unsubscribe';
                        Loadautosubscriptiondetails($scope.SubscrUserId, $scope.SubsrEntitiyID);
                    }
                });
            }
        }



        //	    var GetRole = $resource('access/Role/', { get: { method: 'GET' } });
        AccessService.Role().then(function (role) {
            //	    var role = GetRole.get(function () {

            $scope.Roles = role.Response;
        });

        $scope.subentity = [];
        $scope.SubscrEntityTypeids = [];
        $scope.subids = [];
        $(window).on('LoadSubscription', function (event, data) {
            LoadSubscriptionAtPageLoad();
        });
        function LoadSubscriptionAtPageLoad() {

            $scope.SubsrEntitiyID = $window.SubscriptionEntityID;
            $scope.SubscrEntityTypeid = $window.SubscriptionEntityTypeID;
            $scope.SubscrUserId = parseInt($cookies['UserId'], 10);

            $scope.subscribedrootlevelname = $scope.RootLevelEntityName;
            $scope.subscribedEntityShortText = $scope.EntityShortText;
            $scope.subscribedEntityColorcode = $scope.EntityColorcode;

            $scope.Suscibetext = 'Subscribe';
            $scope.MultilevelSubscriptionSelect = 'singlelevel';

            Loadautosubscriptiondetails($scope.SubscrUserId, $scope.SubsrEntitiyID);

            $scope.SubscrEntityTypeids = [];
            $scope.subentity = [];
            //	        var GetSubscriptionEntityType = $resource('common/GetEntityTypeforSubscription/:ID', { ID: $scope.SubscrEntityTypeid });
            CommonService.GetEntityTypeforSubscription($scope.SubscrEntityTypeid).then(function (getsubscriptionentitytype) {
                //	        var getsubscriptionentitytype = GetSubscriptionEntityType.get(function () {

                $scope.SubscrEntityTypeids = getsubscriptionentitytype.Response;


                //	            var Multisubsriptionids = $resource('common/UserMultiSubscription/:EntitiyID/:UserID', { EntitiyID: $scope.SubsrEntitiyID, UserID: $scope.SubscrUserId });
                CommonService.UserMultiSubscription($scope.SubsrEntitiyID, $scope.SubscrUserId).then(function (getsubscribedids) {
                    //	            var getsubscribedids = Multisubsriptionids.get(function () {

                    if (getsubscribedids.Response != null) {

                        $scope.subids = getsubscribedids.Response.Item1;

                        $('#FilterOption').val(getsubscribedids.Response.Item3);
                        $('#FilterOption').select2();

                        if (getsubscribedids.Response.Item2 == 0) {
                            $scope.MultilevelSubscriptionSelect = 'singlelevel';
                        }
                        else {
                            $scope.MultilevelSubscriptionSelect = 'multilevel';
                        }


                        if ($scope.subids.length > 0) {
                            $scope.Suscibetext = 'Unsubscribe';
                        }
                        else {
                            $scope.Suscibetext = 'Subscribe';
                        }

                    }

                    for (var i = 0; i < $scope.SubscrEntityTypeids.length; i++) {

                        if ($scope.MultilevelSubscriptionSelect == 'singlelevel') {
                            $scope.subentity.push({ "Caption": $scope.SubscrEntityTypeids[i].Caption, "ID": $scope.SubscrEntityTypeids[i].Id, "ColorCode": $scope.SubscrEntityTypeids[i].ColorCode, "ShortDescription": $scope.SubscrEntityTypeids[i].ShortDescription, "Checked": false });
                        }
                        else {
                            var entitysubscribed = $.grep($scope.subids, function (e) { return e == $scope.SubscrEntityTypeids[i].Id; });
                            if (entitysubscribed.length > 0)
                                $scope.subentity.push({ "Caption": $scope.SubscrEntityTypeids[i].Caption, "ID": $scope.SubscrEntityTypeids[i].Id, "ColorCode": $scope.SubscrEntityTypeids[i].ColorCode, "ShortDescription": $scope.SubscrEntityTypeids[i].ShortDescription, "Checked": true });
                            else
                                $scope.subentity.push({ "Caption": $scope.SubscrEntityTypeids[i].Caption, "ID": $scope.SubscrEntityTypeids[i].Id, "ColorCode": $scope.SubscrEntityTypeids[i].ColorCode, "ShortDescription": $scope.SubscrEntityTypeids[i].ShortDescription, "Checked": false });
                        }
                    }


                });


            });
        }

        function Loadautosubscriptiondetails(Userid, parentEntityid) {
            //        var GetAutoSubscriptionDetails = $resource('common/GetAutoSubscriptionDetails/:UserID/:EntitiyID', { UserID: Userid, EntitiyID: parentEntityid });
            CommonService.GetAutoSubscriptionDetails(Userid, parentEntityid).then(function (getautosubscriptiondetails) {
                //       var getautosubscriptiondetails = GetAutoSubscriptionDetails.get(function () {

                var autosubscribeddata = getautosubscriptiondetails.Response;
                $scope.Suscibetext = autosubscribeddata;
                if (autosubscribeddata == 'Subscribe') {
                    $scope.isVisible = false;
                    $scope.MultilevelSubscriptionSelect = 'singlelevel';
                }
                else if (autosubscribeddata == 'Unsubscribe') {
                    $scope.isVisible = true;
                    $scope.MultilevelSubscriptionSelect = 'singlelevel';
                }
                else {
                    $scope.isVisible = false;
                    $scope.MultilevelSubscriptionSelect = 'multilevel';
                }


            });
        }

        $scope.GetUserSubscriptionEntityType = function () {
            //        var GetSubscriptionEntityType = $resource('common/GetEntityTypeforSubscription/:ID', { ID: $scope.SubscrEntityTypeid });
            CommonService.GetEntityTypeforSubscription($scope.SubscrEntityTypeid).then(function (getsubscriptionentitytype) {
                //        var getsubscriptionentitytype = GetSubscriptionEntityType.get(function () {
                $scope.SubscrEntityTypeids = getsubscriptionentitytype.Response;

                if (getsubscriptionentitytype.Response.length > 0) {
                    $scope.Suscibetext = 'Subscribe';
                }
                //          var Multisubsriptionids = $resource('common/UserMultiSubscription/:EntitiyID/:UserID', { EntitiyID: $scope.SubsrEntitiyID, UserID: $scope.SubscrUserId });
                CommonService.UserMultiSubscription($scope.SubsrEntitiyID, $scope.SubscrUserId).then(function (getsubscribedids) {
                    //         var getsubscribedids = Multisubsriptionids.get(function () {
                    $scope.subids = getsubscribedids.Response.Item1;
                    $scope.FilterRoleOption = getsubscribedids.Response.Item3;
                    if (getsubscribedids.Response.Item2 == 0) {
                        $scope.MultilevelSubscriptionSelect = 'singlelevel';
                    }
                    else {
                        $scope.MultilevelSubscriptionSelect = 'multilevel';
                    }

                    for (var i = 0; i < $scope.SubscrEntityTypeids.length; i++) {
                        var entitysubscribed = $.grep($scope.subids, function (e) { return e == $scope.SubscrEntityTypeids[i].Id; });
                        if (entitysubscribed.length > 0)
                            $scope.subentity.push({ "Caption": $scope.SubscrEntityTypeids[i].Caption, "ID": $scope.SubscrEntityTypeids[i].Id, "ColorCode": $scope.SubscrEntityTypeids[i].ColorCode, "ShortDescription": $scope.SubscrEntityTypeids[i].ShortDescription, "Checked": true });
                        else
                            $scope.subentity.push({ "Caption": $scope.SubscrEntityTypeids[i].Caption, "ID": $scope.SubscrEntityTypeids[i].Id, "ColorCode": $scope.SubscrEntityTypeids[i].ColorCode, "ShortDescription": $scope.SubscrEntityTypeids[i].ShortDescription, "Checked": false });
                    }


                });


            });

        }
        $scope.Unsbscribe = function () {

            //        var DeleteSubscription = $resource('common/GetUnsubscribeMultiSubscriptionById/:EntitiyID/:UserID', { EntitiyID: $scope.SubsrEntitiyID, UserID: $scope.SubscrUserId });
            CommonService.GetUnsubscribeMultiSubscriptionById($scope.SubsrEntitiyID, $scope.SubscrUserId).then(function (deleteSubscription) {
                //        var deleteSubscription = DeleteSubscription.delete(function () {
                if (deleteSubscription.Response == true) {
                    $("#subscriptionmodal").modal('hide');
                    LoadSubscriptionAtPageLoad();
                }
            });
        }
        $scope.MultiLevelSubscription = function () {
            if ($('#multisubid').is(':checked')) {
                if ($('#MultiLevelCategory_17  input:checked').length == 0) {
		                bootbox.alert($translate.instant('LanguageContents.Res_1989.Caption'));
                    return;
                }
            }
            if ($scope.settingsstatus == false) {
		            bootbox.alert($translate.instant('LanguageContents.Res_1988.Caption'));
                return;
            }
            if ($('#singlesubid').is(':checked')) {
                //           var AddSubscription = $resource('common/InsertUserSingleEntitySubscription');
                var addSubscription = {};
                //          var addSubscription = new AddSubscription();

                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day + ' ' + hrs;

                addSubscription.UserId = $scope.SubscrUserId;
                addSubscription.EntityId = $scope.SubsrEntitiyID;
                addSubscription.EntitytypeId = $scope.SubscrEntityTypeid;
                addSubscription.SubscribedOn = output;
                addSubscription.LastUpdatedOn = output;
                addSubscription.issubscribe = "true";
                addSubscription.FilterRoleOption = $('#FilterOption>option:selected').val();// $scope.FilterOption;
                //            var getaddSubscription = AddSubscription.save(addSubscription, function () {
                CommonService.InsertUserSingleEntitySubscription(addSubscription).then(function (getaddSubscription) {
                    if (getaddSubscription.Response == true) {
		                    NotifySuccess($translate.instant('LanguageContents.Res_4250.Caption'));
                        $scope.Suscibetext = 'Unsubscribe';
                        Loadautosubscriptiondetails($scope.SubscrUserId, $scope.SubsrEntitiyID);
                    }
                });


            }
            else if ($('#multisubid').is(':checked')) {

                //           var AddMultiSubscription = $resource('common/MultiSubscription');
                var addmultiSubscription = {};
                //          var addmultiSubscription = new AddMultiSubscription();
                var entitytypeidlist = [];

                $('#checkedstates input:checked').each(function () {
                    entitytypeidlist.push($(this).attr('data-id'));
                });
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day + ' ' + hrs;


                for (var i = 0; i < $scope.subentity.length; i++) {

                    if ($scope.subentity[i].Checked == true) {
                        entitytypeidlist.push($scope.subentity[i].ID);
                    }
                }

                addmultiSubscription.UserID = $scope.SubscrUserId;
                addmultiSubscription.EntitiyID = $scope.SubsrEntitiyID;
                addmultiSubscription.EntityTypeID = $scope.SubscrEntityTypeid;
                addmultiSubscription.SubscribedOn = output;
                addmultiSubscription.LastUpdatedOn = output;
                addmultiSubscription.IsMultiLevel = true;
                addmultiSubscription.levels = entitytypeidlist;
                addmultiSubscription.FilterRoleOption = $('#FilterOption>option:selected').val();
                //         var getaddmultiSubscription = AddMultiSubscription.save(addmultiSubscription, function () {
                CommonService.PostMultiSubscription(addmultiSubscription).then(function (getaddmultiSubscription) {
                    if (getaddmultiSubscription.Response == 'Success') {
		                    NotifySuccess($translate.instant('LanguageContents.Res_4250.Caption'));
                        Loadautosubscriptiondetails($scope.SubscrUserId, $scope.SubsrEntitiyID);
                    }
                });

            }
            $("#subscriptionmodal").modal('hide');
        }

        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    //);
    app.controller("mui.planningtool.subscriptionCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', 'requestContext', '$compile', '$window', '$translate', '_', 'CommonService', 'AccessService', muiplanningtoolsubscriptionCtrl]);
})(angular, app);