using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IModuleFeature interface for table 'MM_Module_Feature'.
    /// </summary>
    public interface IModuleFeature
    {
        #region Public Properties

        int ModuleId
        {
            get;
            set;
        }

        int? Featureid
        {
            get;
            set;

        }

        string ModuleCaption
        { get; set; }
        string FeatureCaption
        { get; set; }



        #endregion
    }
}
