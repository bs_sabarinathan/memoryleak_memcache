using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// NavigationDao object for table 'CM_Navigation'.
	/// </summary>
	
	public partial class NavigationDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _parentid;
        protected int _Typeid;
		protected int _moduleid;
		protected int _featureid;
		protected string _caption;
		protected string _description;
		protected string _url;
        protected string _externalurl;
		protected string _javascript;
		protected bool _isactive;
		protected bool _ispopup;
		protected bool _isiframe;
		protected bool _isdynamicpage;
		protected bool _isexternal;
        protected bool _isdefault;
		protected bool _addusername;
		protected bool _adduseremail;
        protected bool _adduserid;
        protected bool _addlanguagecode;
		protected string _imageurl;
        protected int _sortorder;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected int _SearchType;
		#endregion
		
		#region Constructors
		public NavigationDao() {}

        public NavigationDao(int pId, int pParentid, int pTypeid, int pModuleid, int pFeatureid, string pCaption, string pDescription, string pUrl, string pExternalUrl, string pJavaScript, bool pIsActive, bool pIsPopup, bool pIsDefault, bool pIsIframe, bool pIsDynamicPage, bool pIsExternal, bool pAddUserName, bool pAddUserEmail, string pImageurl, bool pAddUserID, bool pAddLanguageCode, int pSortOrder, int pSearchType)
		{
			this._id = pId; 
			this._parentid = pParentid;
            this._Typeid = pTypeid; 
			this._moduleid = pModuleid; 
			this._featureid = pFeatureid; 
			this._caption = pCaption; 
			this._description = pDescription; 
			this._url = pUrl;
            this._externalurl = pExternalUrl; 
			this._javascript = pJavaScript; 
			this._isactive = pIsActive; 
			this._ispopup = pIsPopup;
            this._isdefault = pIsDefault; 
			this._isiframe = pIsIframe; 
			this._isdynamicpage = pIsDynamicPage; 
			this._isexternal = pIsExternal; 
			this._addusername = pAddUserName; 
			this._adduseremail = pAddUserEmail; 
			this._imageurl = pImageurl;
            this._adduserid = pAddUserID;
            this._addlanguagecode = pAddLanguageCode;
            this._sortorder = pSortOrder;
            this._SearchType = pSearchType;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int Typeid
		{
            get { return _Typeid; }
            set { _bIsChanged |= (_Typeid != value); _Typeid = value; }
			
		}

        public virtual int Parentid
        {
            get { return _parentid; }
            set { _bIsChanged |= (_parentid != value); _parentid = value; }

        }
		
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int Featureid
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}
		
		public virtual string Url
		{
			get { return _url; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Url", "Url value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_url != value); 
			  _url = value; 
			}
			
		}

        public virtual string ExternalUrl
        {
            get { return _externalurl; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("ExternalUrl", "ExternalUrl value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_externalurl != value);
                _externalurl = value;
            }

        }
		
		public virtual string JavaScript
		{
			get { return _javascript; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("JavaScript", "JavaScript value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_javascript != value); 
			  _javascript = value; 
			}
			
		}
		
		public virtual bool IsActive
		{
			get { return _isactive; }
			set { _bIsChanged |= (_isactive != value); _isactive = value; }
			
		}
		
		public virtual bool IsPopup
		{
			get { return _ispopup; }
			set { _bIsChanged |= (_ispopup != value); _ispopup = value; }
			
		}

        public virtual bool IsDefault
        {
            get { return _isdefault; }
            set { _bIsChanged |= (_isdefault != value); _isdefault = value; }

        }

		
		public virtual bool IsIframe
		{
			get { return _isiframe; }
			set { _bIsChanged |= (_isiframe != value); _isiframe = value; }
			
		}
		
		public virtual bool IsDynamicPage
		{
			get { return _isdynamicpage; }
			set { _bIsChanged |= (_isdynamicpage != value); _isdynamicpage = value; }
			
		}
		
		public virtual bool IsExternal
		{
			get { return _isexternal; }
			set { _bIsChanged |= (_isexternal != value); _isexternal = value; }
			
		}
		
		public virtual bool AddUserName
		{
			get { return _addusername; }
			set { _bIsChanged |= (_addusername != value); _addusername = value; }
			
		}
		
		public virtual bool AddUserEmail
		{
			get { return _adduseremail; }
			set { _bIsChanged |= (_adduseremail != value); _adduseremail = value; }
			
		}

        public virtual bool AddUserID
        {
            get { return _adduserid; }
            set { _bIsChanged |= (_adduserid != value); _adduserid = value; }

        }

        public virtual bool AddLanguageCode
        {
            get { return _addlanguagecode; }
            set { _bIsChanged |= (_addlanguagecode != value); _addlanguagecode = value; }

        }
		
		public virtual string Imageurl
		{
			get { return _imageurl; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Imageurl", "Imageurl value, cannot contain more than 250 characters");
			  _bIsChanged |= (_imageurl != value); 
			  _imageurl = value; 
			}
			
		}

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
        public virtual int SearchType
        {
            get { return _SearchType; }
            set { _bIsChanged |= (_SearchType != value); _SearchType = value; }
        }
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string NavigationDao = "NavigationDao";			
			public const string Id = "Id";			
			public const string Parentid = "Parentid";
            public const string Typeid = "Typeid";	
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string Url = "Url";
            public const string ExternalUrl = "ExternalUrl";	
			public const string JavaScript = "JavaScript";			
			public const string IsActive = "IsActive";			
			public const string IsPopup = "IsPopup";
            public const string IsDefault = "IsDefault";	
			public const string IsIframe = "IsIframe";			
			public const string IsDynamicPage = "IsDynamicPage";			
			public const string IsExternal = "IsExternal";			
			public const string AddUserName = "AddUserName";			
			public const string AddUserEmail = "AddUserEmail";
            public const string AddUserID = "AddUserID";
            public const string AddLanguageCode = "AddLanguageCode";
			public const string Imageurl = "Imageurl";
            public const string SortOrder = "SortOrder";
            public const string SearchType = "SearchType";	
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string NavigationDao = "NavigationDao";			
			public const string Id = "Id";			
			public const string Parentid = "Parentid";
            public const string Typeid = "Typeid";	
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string Url = "Url";			
			public const string JavaScript = "JavaScript";			
			public const string IsActive = "IsActive";			
			public const string IsPopup = "IsPopup";
            public const string IsDefault = "IsDefault";
			public const string IsIframe = "IsIframe";			
			public const string IsDynamicPage = "IsDynamicPage";			
			public const string IsExternal = "IsExternal";			
			public const string AddUserName = "AddUserName";			
			public const string AddUserEmail = "AddUserEmail";
            public const string AddUserID = "AddUserID";
            public const string AddLanguageCode = "AddLanguageCode";
			public const string Imageurl = "Imageurl";
            public const string SortOrder = "SortOrder";
            public const string SearchType = "SearchType";
		}

		#endregion
		
		
	}
	
}
