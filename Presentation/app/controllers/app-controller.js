﻿(function (ng, app) {
    "use strict";

    function appCtrl($scope, $resource, $localStorage, $timeout, $interval, $location, $cookies, $translate, $http) {

        $scope.FinancialCurrentDivisionID = {
            ID: ForecstDivisonIds.Yearly
        }

        function GetDivisionValue() {
            GetCurrentDivisionId().then(function (res) {
                if (res.Response != 0) {
                    $scope.FinancialCurrentDivisionID.ID = res.Response;
                }
            });
        }
        getFinancialsettings();

        function getFinancialsettings() {
            GetFinancialForecastsettings().then(function (res) {
                if (res.StatusCode == 200) {
                    $scope.FinancialCurrentDivisionID.ID = res.Response.ForecastDivision;
                }
            });
        };

        function GetFinancialForecastsettings() { var request = $http({ method: "get", url: "api/Planning/GetFinancialForecastsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCurrentDivisionId() { var request = $http({ method: "get", url: "api/Planning/GetCurrentDivisionId/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }

        $scope.DefaultSettings = {
            DateFormat: 'yyyy-mm-dd',
            CurrencyFormat: {
                Id: 1,
                Name: "EUR",
                ShortName: "EUR",
                Symbol: "EUR"
            },
            LinkTypes: [{
                "Id": 1,
                "Type": "http://"
            }, {
                "Id": 2,
                "Type": "https://"
            }, {
                "Id": 3,
                "Type": "ftp://"
            }, {
                "Id": 4,
                "Type": "file://"
            }]
        }
        if (AdditionalSettingsObj != null && AdditionalSettingsObj.length > 0) {
            $scope.DefaultSettings.DateFormat = AdditionalSettingsObj[0].SettingValue;
            GlobalUserDateFormat = AdditionalSettingsObj[0].SettingValue.toUpperCase();
            Defaultdate = AdditionalSettingsObj[0].SettingValue;
            for (var i = 0; i < AdditionalSettingsObj[1].CurrencyFormatvalue.length; i++) {
                $scope.DefaultSettings.CurrencyFormat.Id = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Id;
                $scope.DefaultSettings.CurrencyFormat.Name = AdditionalSettingsObj[1].CurrencyFormatvalue[i].ShortName;
                $scope.DefaultSettings.CurrencyFormat.ShortName = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Name;
                $scope.DefaultSettings.CurrencyFormat.Symbol = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Symbol;
            }
        }
        $scope.LoginStatus = function () {
            $scope.UserId = $cookies['UserId'];
        };
        $scope.GrabUserInfo = function () {
            $scope.Username = $cookies['Username'];
            $scope.UserImage = $cookies['UserImage'];
            $scope.UserEmail = $cookies['UserEmail'];
        };
        $scope.Logout = function () {
            Logout().then(function (logout1) {
                if (logout1.Response == null) {
                    $.removeCookie("UserId");
                    $.removeCookie("Username");
                    $.removeCookie("UserImage");
                    $.removeCookie("UserEmail");
                    $.removeCookie("planAttrs" + parseInt($cookies['UserId']));
                    localStorage.clear();
                    window.location.replace("login.html");
                    var date = new Date();
                    if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                        $('#loginpageTitle').attr('src', amazonURL + cloudsetup.BucketName + '/' + TenantFilePath + 'logo.png?ts=' + date.getTime());
                    } else {
                        $('#loginpageTitle').attr('src', 'assets/img/logo.png?ts=' + date.getTime());
                    }
                }
            });
        };


        function Logout() { var request = $http({ method: "get", url: "api/user/Logout/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }

        $scope.SystemSettings = { financialSettings: {}, finDefaultView: 0 };


        $scope.getInstanceTime = function () {
            var now = new Date();
            var timeString = now.toTimeString();
            var instanceTime = timeString.match(/\d+:\d+:\d+/i);
            return (instanceTime[0]);
        };
        $scope.openModalWindow = function (modalType) {
            bootbox.alert(arguments[1] || $translate.instant('LanguageContents.Res_1808.Caption'));
        };
        $scope.setWindowTitle = function (title) {
            $scope.windowTitle = title;
        };
        $scope.ContextMenuRecords = {
            ChildContextData: []
        };



        ChildEntityTypeHierarchy().then(function (res) {
            $scope.ContextMenuRecords.ChildContextData = res.Response;
        });

        function ChildEntityTypeHierarchy() { var request = $http({ method: "get", url: "api/Metadata/ChildEntityTypeHierarchy/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }


        $scope.DecodedTextName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };
        $scope.windowTitle = "Marcom Platform";
        $scope.LoginStatus();
        $scope.GrabUserInfo();

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
            }
            return undefined;
        }
        if (getCookie("SnippetLogin") != undefined) {
            $(".loading gear").hide();
            $(".gearSmall").hide();
            $(".gearMedium").hide();
            $(".gearLarge").hide();
        }
        $scope.lang = {
            isopen: false
        };

        $scope.TenantFileInfo = TenantFilePath;

        GetListFinancialView().then(function (res) {
            $scope.SystemSettings.financialSettings = res.Response;
            var tempObj = $.grep($scope.SystemSettings.financialSettings, function (rel) {
                return rel.IsDefault == true;
            })[0];
            if (tempObj != null)
                $scope.SystemSettings.finDefaultView = tempObj.Id;
        });
        function GetListFinancialView() { var request = $http({ method: "get", url: "api/Common/GetListFinancialView/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }


        function handleError(response) { if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); } return ($q.reject(response.data.message)); }
        function handleSuccess(response) { return (response.data); }

    }
    app.controller('appCtrl', ['$scope', '$resource', '$localStorage', '$timeout', '$interval', '$location', '$cookies', '$translate', '$http', appCtrl]);
})(angular, app);