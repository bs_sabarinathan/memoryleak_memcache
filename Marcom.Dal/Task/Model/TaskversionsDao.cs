﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public class TaskversionsDao : BaseDao, ICloneable
    {
        public virtual int ID { get; set; }
        public virtual int TaskID { get; set; }
        public virtual int VersionNo { get; set; }
        public virtual int InitiatorId { get; set; }

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
