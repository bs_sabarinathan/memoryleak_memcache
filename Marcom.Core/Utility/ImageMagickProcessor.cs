﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.MediaHandler.Input;
using BrandSystems.MediaHandler;
using BrandSystems.MediaHandler.Output;
using BrandSystems.MediaHandler.CustomType;
using Microsoft.VisualBasic;
using System.Diagnostics;
using System.Xml.Linq;
using System.IO;
using System.Configuration;
namespace BrandSystems.Marcom.Core.Utility.MediaHandler
{
    public class ImageMagickProcessor
    {

        #region "Private Data Members"
        private string _ImageMagickPath;
        private string _ProfilePath;
        private string _TempPath;
        #endregion
        public ImageMagickProcessor(string strImageMagickPath, string strProfilePath)
        {
            _ImageMagickPath = strImageMagickPath;
            _ProfilePath = strProfilePath;
        }

        #region "Public Properties"

        /// <summary>
        /// Contains the path of image magic program
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ImageMagickPath
        {
            get { return _ImageMagickPath; }
            set { _ImageMagickPath = value; }
        }

        /// <summary>
        /// Contains the path of the profile folder
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ProfilePath
        {
            get { return _ProfilePath; }
            set { _ProfilePath = value; }
        }

        /// <summary>
        /// Contains the path of the Temp folder
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string TempPath
        {
            get { return _TempPath; }
            set { _TempPath = value; }
        }



        #endregion

        #region "Member Functions"

        /// <summary>
        /// This is the Entry Point of ImageMagickProcessor, execution of the task will start from here.
        /// </summary>
        /// <param name="Obj">Image Request Object</param>
        /// <returns>Image Response Object</returns>
        /// <remarks>No Other Function Will be expose to Out Side</remarks>
        public ImageResponseObject ExecuteTask(ImageRequestObject obj)
        {
            ImageResponseObject functionReturnValue = default(ImageResponseObject);

            ImageResponseObject imgResObj = new ImageResponseObject(obj.JobID);
            string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
           
            BrandSystems.Marcom.Core.Utility.ErrorLog.LogFilePath = retFilePath;
            try
            {
                if (ValidateMimeType(obj) == true)
                {
                    imgResObj.ErrorDetail = "Invalid file format";
                    BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------ExecuteTask- Invalid file format------------->", DateTime.Now);
                    return imgResObj;
                    return functionReturnValue;
                }
                if (obj.TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.GetInfo)
                {
                    imgResObj = ExecuteGetInfo(obj);

                }
                else if (obj.TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert)
                {


                    if ((obj.ResizeWidth == 0 & obj.ResizeHeight != 0) | (obj.ResizeWidth != 0 & obj.ResizeHeight == 0) | (obj.ScaleWidth == 0 & obj.ScaleHeight != 0) | (obj.ScaleWidth != 0 & obj.ScaleHeight == 0) | (!(obj.Units == Units.Pixels)))
                    {
                        obj = ValidateAndUpdateImgReqObj(obj);

                    }

                    //If Not (obj.Units = Units.Pixels) Then

                    //End If


                    imgResObj = ExecuteConvert(obj);

                    if (obj.MetaDataNeeded != null)
                    {
                        if (obj.MetaDataNeeded.Length > 0)
                        {
                            ImageRequestObject tempReq = new ImageRequestObject(obj.JobID);
                            var _with1 = tempReq;
                            _with1.SourcePath = obj.DestinationPath;
                            _with1.GroupID = obj.GroupID;
                            _with1.ClientID = obj.ClientID;
                            _with1.ApplicationID = obj.ApplicationID;
                            _with1.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
                            _with1.MetaDataNeeded = obj.MetaDataNeeded;
                            ImageResponseObject tempRes = new ImageResponseObject(obj.JobID);
                            tempRes = ExecuteGetInfo(tempReq);

                            var _with2 = imgResObj;
                            _with2.ColourSpace = tempRes.ColourSpace;
                            _with2.Compression = tempRes.Depth;
                            _with2.Depth = tempRes.Depth;
                            _with2.EXIFProfile = tempRes.EXIFProfile;
                            _with2.Format = tempRes.Format;
                            _with2.Height = tempRes.Height;
                            _with2.ICCProfile = tempRes.ICCProfile;
                            _with2.ImageClass = tempRes.ImageClass;
                            _with2.ImageType = tempRes.ImageType;
                            _with2.IPTCProfile = tempRes.IPTCProfile;
                            _with2.PageNo = tempRes.PageNo;
                            _with2.Quality = tempRes.Quality;
                            _with2.Units = tempRes.Units;
                            _with2.Width = tempRes.Width;
                            _with2.XResolution = tempRes.XResolution;
                            _with2.YResolution = tempRes.YResolution;

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------ExecuteTask-Exception------------->" + ex.Message, DateTime.Now);
                Trace.TraceError(DateTime.Now.ToString() + " - " + ex.Message);
            }

            return imgResObj;
            return functionReturnValue;

        }

        /// <summary>
        /// When the task to be performed is GetInfo this function will be executed.
        /// </summary>
        /// <param name="Obj">Contains the image request object</param>
        /// <returns>Image Response Object</returns>
        /// <remarks></remarks>
        private ImageResponseObject ExecuteGetInfo(ImageRequestObject obj)
        {

            ImageMagickHelper imgInfo = new ImageMagickHelper();
            imgInfo.ImageMagickPath = ImageMagickPath;
            imgInfo.ProfilePath = ProfilePath;
            ImageResponseObject imgResObj = new ImageResponseObject(obj.JobID);
            imgResObj.AditionalInfo = obj.AditionalInfo;
            imgResObj.GroupID = obj.GroupID;

            List<string> data = new List<string>();
            BrandSystems.MediaHandler.CustomType.MetaData Item;
            foreach (BrandSystems.MediaHandler.CustomType.MetaData Item_loopVariable in obj.MetaDataNeeded)
            {
                Item = Item_loopVariable;
                data.Add(Item.ToString());
            }

            string strResult = null;
            //strResult = imgInfo.GetInfo(obj.SourcePath, data.ToArray(), obj.JobID.ToString())
            if (Path.GetExtension(obj.SourcePath) == ".eps")
            {
                strResult = imgInfo.GetInfo(obj.SourcePath, data.ToArray(), obj.JobID.ToString(), Path.GetExtension(obj.SourcePath));
            }
            else
            {
                strResult = imgInfo.GetInfo(obj.SourcePath + "[0]", data.ToArray(), obj.JobID.ToString(), Path.GetExtension(obj.SourcePath));
            }
            // WriteXml(imgResObj, strResult)

            XDocument ximageinfo = default(XDocument);
            //Try

            XDocument x = XDocument.Parse(strResult);
            ximageinfo = x;
            Trace.TraceInformation(DateTime.Now.ToString() + " - " + "ExecuteGetInfo method  XDocument prase : {0} " + x.ToString());
            string xResolution = "";
            string yResolution = "";
            string colourSpace = "";
            string compression = "";
            string format = "";
            string imageClass = "";
            string depth = "";
            string imageType = "";
            if(ximageinfo != null)
            {
                  var xHeightval = ximageinfo.Descendants("ImageInfo").Descendants("Height").FirstOrDefault();
                if(xHeightval !=null)
                  imgResObj.Height = Int32.Parse(ximageinfo.Descendants("ImageInfo").Descendants("Height").ElementAt(0).Value);
                 var xWidthval = ximageinfo.Descendants("ImageInfo").Descendants("Width").FirstOrDefault();
                  if(xWidthval !=null)
                 imgResObj.Width = Int32.Parse(ximageinfo.Descendants("ImageInfo").Descendants("Width").ElementAt(0).Value);
                 var xResolutionval = ximageinfo.Descendants("ImageInfo").Descendants("XResolution").FirstOrDefault();
                  if(xResolutionval !=null)
                 xResolution=  ximageinfo.Descendants("ImageInfo").Descendants("XResolution").ElementAt(0).Value;
                  var yResolutionval = ximageinfo.Descendants("ImageInfo").Descendants("YResolution").FirstOrDefault();
                  if (yResolutionval != null)
                      yResolution = ximageinfo.Descendants("ImageInfo").Descendants("YResolution").ElementAt(0).Value;
                  var colourSpaceval = ximageinfo.Descendants("ImageInfo").Descendants("ColourSpace").FirstOrDefault();
                  if (colourSpaceval != null)
                      colourSpace = ximageinfo.Descendants("ImageInfo").Descendants("ColourSpace").ElementAt(0).Value;
                  var compressionval = ximageinfo.Descendants("ImageInfo").Descendants("Compression").FirstOrDefault();
                  if (compressionval != null)
                      compression = ximageinfo.Descendants("ImageInfo").Descendants("Compression").ElementAt(0).Value;

                  var Formatval = ximageinfo.Descendants("ImageInfo").Descendants("Format").FirstOrDefault();
                  if (Formatval != null)
                      format = ximageinfo.Descendants("ImageInfo").Descendants("Format").ElementAt(0).Value;
                  var ImageClassval = ximageinfo.Descendants("ImageInfo").Descendants("ImageClass").FirstOrDefault();
                  if (ImageClassval != null)
                      imageClass = ximageinfo.Descendants("ImageInfo").Descendants("ImageClass").ElementAt(0).Value;
                  var depthval = ximageinfo.Descendants("ImageInfo").Descendants("Depth").FirstOrDefault();
                  if (depthval != null)
                      depth = ximageinfo.Descendants("ImageInfo").Descendants("Depth").ElementAt(0).Value;
                  var ImageTypeval = ximageinfo.Descendants("ImageInfo").Descendants("ImageType").FirstOrDefault();
                  if (ImageTypeval != null)
                      imageType = ximageinfo.Descendants("ImageInfo").Descendants("ImageType").ElementAt(0).Value;
                  var Pageval = ximageinfo.Descendants("ImageInfo").Descendants("PageNo").FirstOrDefault();
                  if (Pageval != null)                  
                      imgResObj.PageNo = Int32.Parse(ximageinfo.Descendants("ImageInfo").Descendants("PageNo").ElementAt(0).Value);
                  var Qualityval = ximageinfo.Descendants("ImageInfo").Descendants("Quality").FirstOrDefault();
                  if (Qualityval != null)
                      imgResObj.Quality = Int32.Parse(ximageinfo.Descendants("ImageInfo").Descendants("Quality").ElementAt(0).Value);

                  var ICCProfileval = ximageinfo.Descendants("ImageInfo").Descendants("ICCProfile").FirstOrDefault();
                  if (ICCProfileval != null)
                      imgResObj.ICCProfile = ximageinfo.Descendants("ImageInfo").Descendants("ICCProfile").ElementAt(0).Value;

                  var EXIFProfileval = ximageinfo.Descendants("ImageInfo").Descendants("EXIFProfile").FirstOrDefault();
                  if (EXIFProfileval != null)
                      imgResObj.EXIFProfile = ximageinfo.Descendants("ImageInfo").Descendants("EXIFProfile").ElementAt(0).Value;

                  var IPTCProfileval = ximageinfo.Descendants("ImageInfo").Descendants("IPTCProfile").FirstOrDefault();
                  if (IPTCProfileval != null)
                      imgResObj.IPTCProfile = ximageinfo.Descendants("ImageInfo").Descendants("IPTCProfile").ElementAt(0).Value;            

                 
            }
            
            //dynamic xResolution = x.ToString();
            string strXres = xResolution;
            //Trace.TraceInformation(Now.ToString() & " - " & "strXresval : {0} " & strXres)
            if (strXres != null && strXres.Length > 0)
            {
                //Trace.TraceInformation(Now.ToString() & " - " & "inside1 : {0} " & strXres)
                if (strXres.IndexOf("PixelsPerCentimeter") != -1)
                {
                    //Trace.TraceInformation(Now.ToString() & " - " & "inside2 : {0} " & strXres)
                    strXres = strXres.Replace("PixelsPerCentimeter", "");
                    //Trace.TraceInformation(Now.ToString() & " - " & "inside3 : {0} " & strXres)
                    double xResolutionVal = 0;
                    xResolutionVal = double.Parse(strXres);
                    //Trace.TraceInformation(Now.ToString() & " - " & "before Parse strXres : {0} " & strXres)
                    //xResolutionVal = Convert.ToDouble(strXres, System.Globalization.CultureInfo.InvariantCulture)
                    //Trace.TraceInformation(Now.ToString() & " - " & "after Parse xResolutionVal : {0} " & xResolutionVal)
                    xResolutionVal = xResolutionVal * 2.54;
                    imgResObj.XResolution = double.Parse(xResolutionVal.ToString());
                }
                else if (strXres.IndexOf("PixelsPerInch") != -1)
                {
                    double xResolutionVal = 0;
                    strXres = strXres.Replace("PixelsPerInch", "");
                    xResolutionVal = double.Parse(strXres);
                    imgResObj.XResolution = double.Parse(xResolutionVal.ToString());
                }
                else if (strXres.IndexOf("Undefined") != -1)
                {
                    double xResolutionVal = 0;
                    strXres = strXres.Replace("Undefined", "");
                    xResolutionVal = double.Parse(strXres);
                    imgResObj.XResolution = double.Parse(xResolutionVal.ToString());
                }
                else
                {
                    double xResolutionVal = 0;
                    Trace.TraceInformation(DateTime.Now.ToString() + " - " + "X elsepart : {0} " + strXres);
                    //xResolutionVal = Double.Parse(strXres)
                    //imgResObj.XResolution = Double.Parse(xResolutionVal)
                    string firstpart = null;
                    string secondpart = null;
                    //Trace.TraceInformation(Now.ToString() & " - " & "before Parse strXres : {0} " & strXres)
                    if (strXres.IndexOf(".") != -1)
                    {
                        int intfirstpart = 0;
                        //Trace.TraceInformation(Now.ToString() & " - " & "first part : {0} " & firstpart)
                        firstpart = strXres.Substring(0, strXres.IndexOf("."));
                        intfirstpart = Int32.Parse(firstpart);
                        if ((intfirstpart < 1))
                        {
                            secondpart = strXres.Substring(strXres.IndexOf("."), strXres.Length - strXres.IndexOf("."));
                            Trace.TraceInformation(DateTime.Now.ToString() + " - " + " X second part : {0} " + secondpart);
                            secondpart = secondpart.Replace(".", "");
                            secondpart = secondpart.Replace(",", "");
                            secondpart = secondpart.Replace("0", "");
                            secondpart = secondpart.Substring(0, 2);
                            Trace.TraceInformation(DateTime.Now.ToString() + " - " + "  X after replace  second part : {0} " + secondpart);
                            strXres = secondpart;
                        }

                    }
                    //Trace.TraceInformation(Now.ToString() & " - " & "before Parse strXres : {0} " & strXres)
                    xResolutionVal = Convert.ToDouble(strXres, System.Globalization.CultureInfo.InvariantCulture);
                    //Trace.TraceInformation(Now.ToString() & " - " & "after Parse strXres : {0} " & xResolutionVal)
                    imgResObj.XResolution = double.Parse(xResolutionVal.ToString());
                    //Trace.TraceInformation(Now.ToString() & " - " & "after Parse xResolutionVal : {0} " & imgResObj.XResolution)
                }
            }

            //dynamic yResolution = x.ToString();
            string strYres = yResolution;
            if (strYres != null && strYres.Length >0)
            {
                if (strYres.IndexOf("PixelsPerCentimeter") != -1)
                {
                    strYres = strYres.Replace("PixelsPerCentimeter", "");
                    double yResolutionVal = 0;
                    yResolutionVal = double.Parse(strYres);
                    yResolutionVal = yResolutionVal * 2.54;
                    imgResObj.YResolution = double.Parse(yResolutionVal.ToString());
                }
                else if (strYres.IndexOf("PixelsPerInch") != -1)
                {
                    double yResolutionVal = 0;
                    strYres = strYres.Replace("PixelsPerInch", "");
                    yResolutionVal = double.Parse(strYres);
                    imgResObj.YResolution = double.Parse(yResolutionVal.ToString());
                }
                else if (strYres.IndexOf("Undefined") != -1)
                {
                    double yResolutionVal = 0;
                    strYres = strYres.Replace("Undefined", "");
                    yResolutionVal = double.Parse(strYres);
                    imgResObj.YResolution = double.Parse(yResolutionVal.ToString());
                }
                else
                {
                    double yResolutionVal = 0;
                    Trace.TraceInformation(DateTime.Now.ToString() + " - " + "y elsepart : {0} " + strYres);
                    string firstpart = null;
                    string secondpart = null;
                    //Trace.TraceInformation(Now.ToString() & " - " & "before Parse strXres : {0} " & strXres)
                    if (strYres.IndexOf(".") != -1)
                    {
                        int intfirstpart = 0;
                        //Trace.TraceInformation(Now.ToString() & " - " & "first part : {0} " & firstpart)
                        firstpart = strYres.Substring(0, strYres.IndexOf("."));
                        intfirstpart = Int32.Parse(firstpart);
                        if ((intfirstpart < 1))
                        {
                            secondpart = strYres.Substring(strYres.IndexOf("."), strYres.Length - strYres.IndexOf("."));
                            Trace.TraceInformation(DateTime.Now.ToString() + " - " + " Y second part : {0} " + secondpart);
                            secondpart = secondpart.Replace(".", "");
                            secondpart = secondpart.Replace(",", "");
                            secondpart = secondpart.Replace("0", "");
                            secondpart = secondpart.Substring(0, 2);
                            Trace.TraceInformation(DateTime.Now.ToString() + " - " + " Y  after replace  second part : {0} " + secondpart);
                            strYres = secondpart;
                        }

                    }
                    //yResolutionVal = Double.Parse(strYres)
                    //imgResObj.YResolution = Double.Parse(yResolutionVal)

                    yResolutionVal = Convert.ToDouble(strYres, System.Globalization.CultureInfo.InvariantCulture);
                    imgResObj.YResolution = double.Parse(yResolutionVal.ToString());
                }
            }

            //dynamic colourSpace = x.ToString();
            if (colourSpace == BrandSystems.MediaHandler.CustomType.ColourSpace.CMY.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.CMY;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.CMYK.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.CMYK;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Gray.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Gray;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.HSB.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.HSB;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.HSL.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.HSL;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.HWB.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.HWB;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Lab.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Lab;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Log.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Log;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.OHTA.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.OHTA;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601Luma.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601Luma;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601YCbCr.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Rec601YCbCr;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709Luma.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709Luma;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709YCbCr.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Rec709YCbCr;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.RGB.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.RGB;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.sRGB.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.sRGB;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.Transparent.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.Transparent;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.XYZ.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.XYZ;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.YCbCr.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.YCbCr;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.YCC.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.YCC;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.YIQ.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.YIQ;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.YPbPr.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.YPbPr;
            }
            else if (colourSpace ==BrandSystems.MediaHandler.CustomType.ColourSpace.YUV.ToString())
            {
                imgResObj.ColourSpace =BrandSystems.MediaHandler.CustomType.ColourSpace.YUV;
            }

            //dynamic compression = x.ToString();
            imgResObj.Compression = compression;

            //dynamic format = x.ToString();
            imgResObj.Format = format;

            //dynamic imageClass = x.ToString();
            if (imageClass != null && imageClass.Length >0)
            {
                try
                {
                    imageClass = imageClass.Replace(imageClass.Substring(imageClass.IndexOf("-") + 1, imageClass.Length - imageClass.IndexOf("-") - 1), "").Replace("-", "");
                }
                catch (Exception ex)
                {
                    Trace.TraceError(DateTime.Now.ToString() + " - " + "imageClass Error : {0} " + "\n"  + " imageClass StackTrace : {1}", ex.Message, ex.StackTrace);
                }
            }
            imgResObj.ImageClass = imageClass;

            //dynamic depth = x.ToString();
            imgResObj.Depth = depth;

            //dynamic imageType = x.ToString();
            if (imageType == BrandSystems.MediaHandler.CustomType.Type.Bilevel.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Bilevel;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.ColorSeparation.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.ColorSeparation;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.ColorSeparationMatte.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.ColorSeparationMatte;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.Grayscale.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Grayscale;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.GrayscaleMatte.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.GrayscaleMatte;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.Optimize.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Optimize;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.Palette.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.Palette;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.PaletteMatte.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.PaletteMatte;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.TrueColor.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.TrueColor;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.TrueColorMatte.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.TrueColorMatte;
            }
            else if (imageType == BrandSystems.MediaHandler.CustomType.Type.NULL.ToString())
            {
                imgResObj.ImageType = BrandSystems.MediaHandler.CustomType.Type.NULL;
            }

            

            return imgResObj;
            //Catch ex As Exception
            //    Trace.TraceInformation(Now.ToString() & " - " & "ExecuteGetInfo method Error : {0} " & strResult)
            //    Trace.TraceError(Now.ToString() & " - " & "ExecuteGetInfo method Error : {0} " & vbNewLine & "StackTrace : {1}", ex.Message, ex.StackTrace)


            //End Try


        }


        /// <summary>
        /// When the task to be performed is Convert this function will be executed.
        /// </summary>
        /// <param name="Obj">Contains the image request object</param>
        /// <returns>Image Response Object</returns>
        /// <remarks></remarks>
        public ImageResponseObject ExecuteConvert(ImageRequestObject obj)
        {


            ImageResponseObject imgResObj = new ImageResponseObject(obj.JobID);
            imgResObj.AditionalInfo = obj.AditionalInfo;
            imgResObj.DestinationPath = obj.DestinationPath;
            imgResObj.GroupID = obj.GroupID;

            bool isCrop = false;
            bool isResize = false;
            bool isScaling = false;
            bool isRoundCorner = false;
            bool isThumbnail = obj.Thumbnail;

            bool needToCallImageMagick = true;
            string extension = null;
            extension = System.IO.Path.GetExtension(obj.DestinationPath).ToUpper();

            if (extension == "PDF" | extension == "EPS")
            {
                needToCallImageMagick = false;
            }
            string colourSpace = "";
            if (obj.ColourSpace !=BrandSystems.MediaHandler.CustomType.ColourSpace.NULL)
            {
                colourSpace = obj.ColourSpace.ToString();
            }

            double cropHeight = obj.CropHeight;
            double cropLeft = obj.CropLeft;
            double cropTop = obj.CropTop;
            double cropWidth = obj.CropWidth;

            string imageType = "";
            if (obj.ImageType != BrandSystems.MediaHandler.CustomType.Type.NULL)
            {
                imageType = obj.ImageType.ToString();
            }

            int pageNo = obj.PageNo;
            int quality = obj.Quality;
            double resizeHeight = obj.ResizeHeight;
            int resizeInPercentage = (int) obj.ResizeInPercentage;
            double resizeWidth = obj.ResizeWidth;
            double scaleHeight = obj.ScaleHeight;
            int scaleInPercentage = (int)obj.ScaleInPercentage;
            double scaleWidth = obj.ScaleWidth;

            string sourcePath = null;
            if (Path.GetExtension(obj.SourcePath) == ".eps")
            {
                sourcePath = obj.SourcePath;
            }
            else
            {
                sourcePath = obj.SourcePath + "[" + obj.PageNo + "]";
            }

            string destinationPath = obj.DestinationPath;

            PriorityLevel workPriority = obj.WorkPriority;
            double xResolution = obj.XResolution;
            double yResolution = obj.YResolution;
            string memoryVal = "100mb";

            string flipDirection = obj.FlipDirection.ToString();
            int rotationAngle = (int) obj.RotationAngle;

            int topLeftRadius = obj.TopLeftRadius;
            int topRightRadius = obj.TopRightRadius;
            int bottomLeftRadius = obj.BottomLeftRadius;
            int bottomRightRadius = obj.BottomRightRadius;
            string cornerColor = obj.CornerColor;

            string tempFilePath = "";

            //----------------------------------------------------------------------------------

            bool workPriorityLevel = false;
            // Check the priority level.. 
            if (workPriority == PriorityLevel.AboveNormal)
            {
                workPriorityLevel = true;
            }
            else if (workPriority == PriorityLevel.BelowNormal)
            {
                workPriorityLevel = false;
            }
            else if (workPriority == PriorityLevel.Highest)
            {
                workPriorityLevel = true;
            }
            else if (workPriority == PriorityLevel.Lowest)
            {
                workPriorityLevel = false;
            }
            else if (workPriority == PriorityLevel.Normal)
            {
                workPriorityLevel = true;
            }
            else if (workPriority == null)
            {
                workPriorityLevel = false;
            }


            if (resizeHeight == 0 | resizeWidth == 0)
            {
                if (resizeHeight == 0)
                {
                    if (resizeWidth != 0)
                    {
                        resizeHeight = resizeWidth * 2;
                        resizeInPercentage = 0;
                        isResize = true;
                    }
                }
                else if (resizeWidth == 0)
                {
                    resizeWidth = resizeHeight * 2;
                    resizeInPercentage = 0;
                    isResize = true;
                }
            }
            else
            {
                resizeInPercentage = 0;
                isResize = true;
            }

            if (resizeInPercentage != 0)
            {
                isResize = true;
            }



            if (scaleHeight == 0 | scaleWidth == 0)
            {
                if (scaleHeight == 0)
                {
                    if (scaleWidth != 0)
                    {
                        scaleHeight = scaleWidth * 2;
                        scaleInPercentage = 0;
                        isScaling = true;
                    }
                }
                else if (scaleWidth == 0)
                {
                    scaleWidth = scaleHeight * 2;
                    scaleInPercentage = 0;
                    isScaling = true;
                }
            }
            else
            {
                scaleInPercentage = 0;
                isScaling = true;
            }

            if (scaleInPercentage != 0)
            {
                isScaling = true;
            }

            if (cropHeight != 0 & cropWidth != 0)
            {
                isCrop = true;
            }

            //Not applicable
            //If cropLeft > cropWidth Then
            //    isCrop = False
            //ElseIf cropTop > cropHeight Then
            //    isCrop = False
            //End If

            //To check for Round Corners Task
            if (topLeftRadius > 0 | topRightRadius > 0 | bottomLeftRadius > 0 | bottomRightRadius > 0)
            {
                isRoundCorner = true;
                cornerColor = obj.CornerColor;
                //Set it to some Temporary location
                tempFilePath = TempPath + "\\" + (Guid.NewGuid()).ToString() + Path.GetExtension(destinationPath);
            }
            else
            {
                tempFilePath = obj.DestinationPath;

            }


            ImageMagickHelper imgInfo = new ImageMagickHelper();
            imgInfo.ImageMagickPath = ImageMagickPath;
            imgInfo.ProfilePath = ProfilePath;

            string result = imgInfo.ExecuteConvert(sourcePath, workPriorityLevel, isResize, isCrop, isScaling, isThumbnail, resizeHeight, resizeWidth, resizeInPercentage, cropWidth,
            cropHeight, cropLeft, cropTop, scaleHeight, scaleWidth, scaleInPercentage, colourSpace, imageType, pageNo, quality,
            xResolution, yResolution, memoryVal, tempFilePath, flipDirection, rotationAngle, isRoundCorner);

            if (result != null )
            {
                if(result.Length >0)
                  imgResObj.ErrorDetail = result;
            }

            string RoundCornerResult = "";
            //if (isRoundCorner)
            //{
            //    sourcePath = tempFilePath;
            //    RoundCornerResult = ExecuteRoundCorners(tempFilePath, workPriority, obj, destinationPath, topLeftRadius, topRightRadius, bottomLeftRadius, bottomRightRadius, cornerColor);

            //    //Delete after round corner task
            //    try
            //    {
            //        File.Delete(tempFilePath);
            //    }
            //    catch (Exception ex)
            //    {
            //        Trace.TraceInformation(DateTime.Now.ToString() + " - " + "Exception {0}", ex.Message);
            //    }

            //}

            if (RoundCornerResult != null)
            {
                if (RoundCornerResult.Length > 0)
                imgResObj.ErrorDetail = RoundCornerResult;
            }

            return imgResObj;
        }

        public string ExecuteRoundCorners(string sourcePath, bool workPriority, ImageRequestObject obj, string destPath, int topLeftRadius, int topRightRadius, int bottomLeftRadius, int bottomRightRadius, string cornerColor)
        {
            ImageMagickHelper imgInfo = new ImageMagickHelper();
            imgInfo.ImageMagickPath = ImageMagickPath;
            imgInfo.ProfilePath = ProfilePath;

            ImageRequestObject tempReq = new ImageRequestObject(obj.JobID);
            var _with3 = tempReq;
            _with3.SourcePath = sourcePath;
            _with3.GroupID = obj.GroupID;
            _with3.ApplicationID = obj.ApplicationID;
            _with3.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
            _with3.MetaDataNeeded = new MetaData[] {
			MetaData.Width,
			MetaData.Height
		};

            ImageResponseObject tempRes = new ImageResponseObject(obj.JobID);
            tempRes = ExecuteGetInfo(tempReq);

            int width =(int) tempRes.Width;
            int height = (int)tempRes.Height;

            List<string> cornerCoord = new List<string>();
            cornerCoord = CalculateRoundCornerCoordinate(width, height, topLeftRadius, topRightRadius, bottomLeftRadius, bottomRightRadius);

            dynamic res = imgInfo.ExecuteRoundCorners(sourcePath, workPriority, destPath, width, height, cornerCoord[0], cornerCoord[1], cornerCoord[2], cornerCoord[3], cornerColor);



            return res;
        }

        private List<string> CalculateRoundCornerCoordinate(int height, int width, int topLeftRadius, int topRightRadius, int bottomLeftRadius, int bottomRightRadius)
        {
            List<string> roundCornerCoordinates = new List<string>();

            int Count = 0;
            int x = 0;
            int y = 0;


            while ((Count != 5))
            {
                StringBuilder build = new StringBuilder();

                if (Count == 0)
                {
                    x = 0;
                    y = 0;
                    build = build.Append(x.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(y.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append((height / 2 + topLeftRadius).ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append((width / 2 + topLeftRadius).ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(topLeftRadius.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(topLeftRadius.ToString().Replace(",", "."));
                    roundCornerCoordinates.Insert(Count, build.ToString());

                }
                else if (Count == 1)
                {
                    x = height / 2 - topRightRadius;
                    y = 0;
                    build = build.Append(x.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(y.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(height.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append((width / 2 + topRightRadius).ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(topRightRadius.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(topRightRadius.ToString().Replace(",", "."));
                    roundCornerCoordinates.Insert(Count, build.ToString());

                }
                else if (Count == 2)
                {
                    x = 0;
                    y = width / 2 - bottomLeftRadius;
                    build = build.Append(x.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(y.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append((height / 2 + bottomLeftRadius).ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(width.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(bottomLeftRadius.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(bottomLeftRadius.ToString().Replace(",", "."));
                    roundCornerCoordinates.Insert(Count, build.ToString());

                }
                else if (Count == 3)
                {
                    x = height / 2 - bottomRightRadius;
                    y = width / 2 - bottomRightRadius;
                    build = build.Append(x.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(y.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(height.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(width.ToString().Replace(",", "."));
                    build = build.Append(" ");
                    build = build.Append(bottomRightRadius.ToString().Replace(",", "."));
                    build = build.Append(",");
                    build = build.Append(bottomRightRadius.ToString().Replace(",", "."));
                    roundCornerCoordinates.Insert(Count, build.ToString());

                }

                Count = Count + 1;
            }

            return roundCornerCoordinates;
        }

        private ImageRequestObject ValidateAndUpdateImgReqObj(ImageRequestObject obj)
        {

            ImageRequestObject tempReq = new ImageRequestObject(obj.JobID);
            var _with4 = tempReq;
            _with4.SourcePath = obj.SourcePath;
            _with4.GroupID = obj.GroupID;
            _with4.ClientID = obj.ClientID;
            _with4.ApplicationID = obj.ApplicationID;
            _with4.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
            _with4.MetaDataNeeded = new MetaData[] {
			MetaData.XResolution,
			MetaData.YResolution,
			MetaData.Height,
			MetaData.Width
		};
            ImageResponseObject tempRes = new ImageResponseObject(obj.JobID);
            tempRes = ExecuteGetInfo(tempReq);

            //if (obj.ResizeWidth > 0)
            //{
            //    obj.ResizeWidth = GetPixel(obj.ResizeWidth, tempRes.XResolution, obj.Units.ToString());
            //}
            //if (obj.ResizeHeight > 0)
            //{
            //    obj.ResizeHeight = GetPixel(obj.ResizeHeight, tempRes.YResolution, obj.Units.ToString());
            //}

            if (obj.ResizeHeight == 0)
            {
                if (obj.ResizeWidth != 0)
                {
                    if (obj.CropHeight > 0)
                    {
                        obj.ResizeHeight = obj.CropHeight / (obj.CropWidth / obj.ResizeWidth);
                    }
                    else
                    {
                        obj.ResizeHeight = tempRes.Height / (tempRes.Width / obj.ResizeWidth);
                    }

                }
            }
            else if (obj.ResizeWidth == 0)
            {
                if (obj.ResizeHeight != 0)
                {
                    if (obj.CropWidth > 0)
                    {
                        obj.ResizeWidth = obj.CropWidth / (obj.CropHeight / obj.ResizeHeight);
                    }
                    else
                    {
                        obj.ResizeWidth = tempRes.Width / (tempRes.Height / obj.ResizeHeight);
                    }

                }
            }

            //if (obj.ScaleWidth > 0)
            //{
            //    obj.ScaleWidth = GetPixel(obj.ScaleWidth, tempRes.XResolution, obj.Units.ToString());
            //}
            //if (obj.ScaleHeight > 0)
            //{
            //    obj.ScaleHeight = GetPixel(obj.ScaleHeight, tempRes.YResolution, obj.Units.ToString());
            //}


            if (obj.ScaleHeight == 0)
            {

                if (obj.ScaleWidth != 0)
                {
                    if (obj.CropHeight > 0)
                    {
                        obj.ScaleHeight = obj.CropHeight / (obj.CropWidth / obj.ScaleWidth);
                    }
                    else
                    {
                        obj.ScaleHeight = tempRes.Height / (tempRes.Width / obj.ScaleWidth);
                    }

                }
            }
            else if (obj.ScaleWidth == 0)
            {

                if (obj.ScaleHeight != 0)
                {
                    if (obj.CropWidth > 0)
                    {
                        obj.ScaleWidth = obj.CropWidth / (obj.CropHeight / obj.ScaleHeight);
                    }
                    else
                    {
                        obj.ScaleWidth = tempRes.Width / (tempRes.Height / obj.ScaleHeight);
                    }

                }
            }

            //if (obj.CropWidth > 0)
            //{
            //    obj.CropWidth = GetPixel(obj.CropWidth, tempRes.XResolution, obj.Units.ToString());
            //}
            //if (obj.CropHeight > 0)
            //{
            //    obj.CropHeight = GetPixel(obj.CropHeight, tempRes.YResolution, obj.Units.ToString());
            //}
            //if (obj.CropTop > 0)
            //{
            //    obj.CropTop = GetPixel(obj.CropTop, tempRes.YResolution, obj.Units.ToString());
            //}
            //if (obj.CropLeft > 0)
            //{
            //    obj.CropLeft = GetPixel(obj.CropLeft, tempRes.XResolution, obj.Units.ToString());
            //}

            return obj;
        }

        public bool ValidateMimeType(ImageRequestObject obj)
        {
            try
            {
                ImageMagickHelper imgInfo = new ImageMagickHelper();
                imgInfo.ImageMagickPath = ImageMagickPath;
                imgInfo.ProfilePath = ProfilePath;

                ImageRequestObject tempReq = new ImageRequestObject(obj.JobID);
                var _with5 = tempReq;
                _with5.SourcePath = obj.SourcePath;
                _with5.GroupID = obj.GroupID;
                _with5.ClientID = obj.ClientID;
                _with5.ApplicationID = obj.ApplicationID;
                _with5.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;
                _with5.MetaDataNeeded = new MetaData[] { MetaData.Format };

                List<string> data = new List<string>();
                int MetaDataNeededlen = 0;
                //MetaDataNeededlen = obj.MetaDataNeeded.GetUpperBound(0)
                BrandSystems.MediaHandler.CustomType.MetaData Item; 
                if (obj.MetaDataNeeded != null)
                {
                    foreach (BrandSystems.MediaHandler.CustomType.MetaData Item_loopVariable in obj.MetaDataNeeded)
                    {
                        Item = Item_loopVariable;
                        data.Add(Item.ToString());
                    }
                    if (!data.Contains("Format"))
                    {
                        data.Add("Format".ToString());
                    }
                }
                else
                {
                    data.Add("Format".ToString());

                }




                string strResult = imgInfo.GetMimeTypeInfo(obj.SourcePath + "[0]", data.ToArray(), obj.JobID.ToString());

                XDocument ximageinfores = XDocument.Parse(strResult);
                 string strformat ="";
                if (ximageinfores != null)
                {
                    var Formatval = ximageinfores.Descendants("ImageInfo").Descendants("Format").FirstOrDefault();
                    if (Formatval != null)
                        strformat =ximageinfores.Descendants("ImageInfo").Descendants("Format").ElementAt(0).Value;
                   
                }

                if (strformat == null | string.IsNullOrEmpty(strformat))
                {
                    return true;
                }
                string strextension = null;
                strextension = Path.GetExtension(obj.SourcePath).Remove(0, 1);


                if ((strextension.ToUpper() == "JPG") | (strextension.ToUpper() == "JPEG"))
                {
                    if ((strformat.ToUpper() == "JPG") | (strformat.ToUpper() == "JPEG"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if ((strextension.ToUpper() == "TIF") | (strextension.ToUpper() == "TIFF"))
                {
                    if ((strformat.ToUpper() == "TIF") | (strformat.ToUpper() == "TIFF"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if ((strextension.ToUpper() == "EPS"))
                {
                    if ((strformat.ToUpper() == "EPS") | (strformat.ToUpper() == "PS") | (strformat.ToUpper() == "EPT"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    if (!(strextension.ToUpper() == strformat.ToUpper().ToUpper()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return true;
            }

        }

        #endregion

        #region "Conversion"

        /// <summary>
        /// Converts the Value in Pixels to Specified Unit.
        /// </summary>
        /// <param name="pixels">Pixel that has to be converted </param>
        /// <param name="dpi">its the dpi for the particular image</param>
        /// <param name="unit">its the unit user has choosed</param>
        /// <returns>A double Value</returns>
        /// <remarks></remarks>
        //public double GetUnit(int pixels, int dpi, string unit)
        //{
        //    double result = 0;
        //    double mm = 0;
        //    try
        //    {
        //        if (!(dpi == 0))
        //        {
        //            mm = ((pixels * 25.4) / dpi);
        //            if ((unit ==BrandSystems.MediaHandler.CustomType.Units.Centimeter))
        //            {
        //                result = (mm * 0.1);
        //                return result;
        //            }
        //            else if ((unit == BrandSystems.MediaHandler.CustomType.Units.Millimeter))
        //            {
        //                return mm;
        //            }
        //            else if ((unit == BrandSystems.MediaHandler.CustomType.Units.Inch))
        //            {
        //                result = (mm * 0.0393700787);
        //                return result;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return null;
        //}

        /// <summary>
        /// Converts the Specified Unit Value to Pixels.
        /// </summary>
        /// <param name="dimension">its the diemension in the corresponding unit choosed by the user </param>
        /// <param name="unit">The unit choosed by user in which user has entered the diemension</param>
        /// <returns>A double Value</returns>
        /// <remarks></remarks>
        //public double GetPixel(double dimension, double dpi, string unit)
        //{
        //    try
        //    {
        //        if (!(dpi == 0))
        //        {
        //            if (unit == (BrandSystems.MediaHandler.CustomType.Units.Inch))
        //            {
        //                return (dimension * dpi);
        //            }
        //            else if (unit == BrandSystems.MediaHandler.CustomType.Units.Centimeter)
        //            {
        //                return (dimension * (dpi / 2.54));
        //            }
        //            else if (unit == BrandSystems.MediaHandler.CustomType.Units.Millimeter)
        //            {
        //                return (dimension * (dpi / 25.4));
        //            }
        //            else
        //            {
        //                return dimension;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        #endregion

    }

}
