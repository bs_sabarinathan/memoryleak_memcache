﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
   public interface IFeedSelection
    {
       int FeedId { get; set; }
       string UserName { get; set; }
       string UserEmail { get; set; }
       string UserImage { get; set; }
       string FeedHappendTime { get; set; }
       string FeedText { get; set; }
       int Actor { get; set; }
       IList<IFeedComment> FeedComment { get; set; }
    }
}
