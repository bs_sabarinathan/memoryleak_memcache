﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectiveobjectivecreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, ObjectivecreationService, $modalInstance, params) {
        var model;
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }
        var ObjectivecreationTimout = {};
        $scope.StartDate = false;
        $scope.EndDate = false;
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.StartDate = false;
            $scope[model] = true;
            $scope.EndDate = true;
        };
        $scope.dynCalanderopen = function ($event, model1, isopen) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.StartDate = true;
            $scope.EndDate = false;
            model = model1;
        };
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        $scope.WizardPosition = 1;

        $scope.listAttriToAttriResult = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.ObjectiveCreationVar = {
            objEntityTypes: [],
            IsToShowRootObjEntity: false
        };

        $scope.Dropdown = {};
        $scope.treeSourcesObj = [];
        $scope.treeSources = {};
        $scope.ImageFileName = '';

        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.objectivecreationCtrl']"));
        });

        function ValidObjectiveTab1() {
            var detailTabValues = [
                ['#ObjectiveName', 'presence', 'Please Enter the Name'],
                ['#ObjectiveDescription', 'presence', 'Please Enter the Description']
            ];
            $("#ObjectiveTablMetadata").nod(detailTabValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp',
                'silentSubmit': 'true'
            });
        }

        function ValidObjectiveTab2() { }

        function ValidObjectiveTab3() {
            var DateCompare = function () {
                var st_date = $('#ObjectiveStatDaterange').val();
                var end_date = $('#ObjectiveEndDaterange').val();
                if (ConvertStringToDateByFormat(end_date, $scope.DefaultSettings.DateFormat) < ConvertStringToDateByFormat(st_date, $scope.DefaultSettings.DateFormat)) return false
                else return true
            }
            var fulfillTabValues = [
                ['#ObjectiveStatDaterange', 'presence', 'Please Enter the Start Date'],
                ['#ObjectiveEndDaterange', 'presence', 'Please Enter the End Date'],
                ['#ObjectiveEndDaterange', DateCompare, 'End Date Should be greater than Start Date'],
            ];
            $("#ObjectiveFulfillTab").nod(fulfillTabValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp3',
                'silentSubmit': 'true'
            });
        }

        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            var Attributetypename = '';
            var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
            });
            var ID = attrID.split("_");
            if (relationobj != undefined) {
                if (relationobj.length > 0) {
                    for (var i = 0; i <= relationobj.length - 1; i++) {
                        Attributetypename = '';
                        if (relationobj[i].AttributeTypeID == 3) {
                            Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 4) {
                            Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 6) {
                            Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        }
                        else if (relationobj[i].AttributeTypeID == 7) {
                            Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 12) {
                            Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        }

                        if ($scope.fields[Attributetypename] == undefined) {
                            if (attrTypeID == 1) {
                                $scope.fields["TextSingleLine_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 2) {
                                $scope.fields["TextMultiLine_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 3) {
                                $scope.fields["ListSingleSelection_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 5) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            }
                            else if (attrTypeID == 6) {
                                $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                            }
                            else if (attrTypeID == 12) {
                                $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                            }
                            else if (attrTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                            }
                            continue;
                            //return false;
                        }
                        if (relationobj[i].AttributeTypeID == 4) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                            if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else if (relationobj[i].AttributeTypeID == 7) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else {
                            if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        //return false;
                    }
                    if (attrTypeID == 1) {
                        $scope.fields["TextSingleLine_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 2) {
                        $scope.fields["TextMultiLine_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 3) {
                        $scope.fields["ListSingleSelection_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 5) {
                        $scope.fields["DatePart_" + ID[0]] = null;
                    }
                    else if (attrTypeID == 6) {
                        $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                    }
                    else if (attrTypeID == 12) {
                        $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                    }
                    else if (attrTypeID == 17) {
                        $scope.fields["ListTagwords_" + ID[0]] = [];
                    }
                    try {
                        $scope.DynamicRemoveValidation(parseInt(ID[0]));
                    }
                    catch (e) { };
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        };

        $scope.LoadObjectivePopuP = function () {
            $scope.Objective = {};
        }

        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttr").click();
        }

        $timeout(function () {
            StrartUpload_UploaderAttr();
        }, 100);

        function StrartUpload_UploaderAttr() {
            $('.moxie-shim').remove();
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }

                });

                uploader_Attr.bind('Init', function (up, params) {
                });

                uploader_Attr.init();


                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });

                uploader_Attr.bind('UploadProgress', function (up, file) {

                });

                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh(); // Reposition Flash/Silverlight
                });

                uploader_Attr.bind('FileUploaded', function (up, file, response) {

                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;

                    SaveFileDetails(file, response.response);

                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;

                });
            }
            else {
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }

        var uploaderAttrInfo = [];
        function SaveFileDetails(file, response) {
            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + resultArr[2];
            uploaderAttrInfo.push({ attrID: $scope.UploadAttributeId, attrFileName: ImageFileName });
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + ImageFileName);
        }
        //ObjectivecreationService.GetObjectiveEntityType(false).then(function (ResultPlansList) {
        //    if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
        //        $scope.ObjectiveCreationVar.IsToShowRootObjEntity = true;
        //        $scope.rootValues = ResultPlansList.Response;
        //    }
        //    else
        //        $scope.ObjectiveCreationVar.IsToShowRootObjEntity = false;

        //});

        //Validation part
        function GetValidationList() {
            ObjectivecreationService.GetValidationDationByEntitytype($scope.rootEntityID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }

                    //$("#ObjectiveMetadataModel").nod($scope.listValidationResult, {
                    //    'delay': 200,
                    //    'submitBtnSelector': '#btnTemp',
                    //    'silentSubmit': 'true'
                    //});
                }
            });
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetEntityTypeRoleAccess(rootID) {
            ObjectivecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        $scope.objectiveTypeChange = function () {
            if ($scope.Objective.ngObjectiveType == 1) {
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.numericqunatitativedisplay = true;
            }
            if ($scope.Objective.ngObjectiveType == 2) {
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = true;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
            }
            if ($scope.Objective.ngObjectiveType == 3) {
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.qualitativedisplay = true;

            }
            if ($scope.Objective.ngObjectiveType == 4) {
                $scope.ratingItems = [];
                $scope.ratingItems.push({ ObjRatingText: '' });
                $scope.RatingsHtml = "";
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = true;

                $scope.RatingsHtml += '<tr ng-repeat="item in ratingItems">';
                $scope.RatingsHtml += '<td><input class="nomargin" type="text" ng-model="item.ObjRatingText" maxlength="30" name="item.ObjRatingText" id="item.ObjRatingText"></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="AddRatings($index)" ng-model="AddRatings"><i class="icon-plus"></i></button></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="RemoveRatings(this)" ng-model="RemoveRatings"><i class="icon-remove"></i></button></td></tr>';
                $('#tblRatingRightbody').html($compile($scope.RatingsHtml)($scope));
                $scope.AddRatings1();

            }
            ValidObjectiveTab2($scope.Objective.ngObjectiveType);
        }

        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = 0;
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        var Childrens = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Childrens != undefined) {
                            var subleveloptions = [];
                            $.each(Childrens, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                    }
                    else if (attrType == 12) {

                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }

                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //    $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                        //        $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //    });
                        //}
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }

            }
            catch (e) { }
        }
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: null,
                    endDate: null,
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.LoadAttributesForObjectiveCreation = function (rootID, rootCaption, objBaseType, objEntityTypeId, objUnitTypeId) {
            //$("#ObjectiveMetadataModel").modal('show');
            $scope.Objective.objUnitTypeId = objUnitTypeId;
            $scope.Objective.objEntityTypeId = objEntityTypeId;
            $scope.Objective.ngObjectiveType = objBaseType;
            handleTextChangeEvents();
            $scope.treeCategory = [];

            //$scope.dyn_Cont = '';
            //$scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
            //$scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
            //$scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
            //$scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
            //$scope.dyn_Cont += '</div>';
            //$("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));

            $scope.UserimageNewTime = new Date.create().getTime().toString();
            $scope.fields = { usersID: '' };
            $scope.varfields = { CostcenterInfo: [] };
            $scope.dynamicEntityValuesHolder = {};
            $scope.fieldKeys = [];
            $scope.costcemtreObject = [];
            $scope.setFieldKeys = function () {
                var keys = [];

                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.OptionObj = {};
            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];
                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });
            }

            $scope.dropDownTreeListMultiSelectionObj = [];

            $scope.MemberLists = [];
            $scope.tempCount = 1;
            $scope.WizardPosition = 1;
            $("#btnWizardFinish").removeAttr('disabled');
            $('#ObjectiveWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#btnWizardFinish').hide();    //intially wizard Finish button is disabled
            $('#btnWizardNext').show();     //intially wizard Next button is enalbed
            $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled

            $scope.status = true;
            //User autocomplete Start 
            var ownername = $cookies['Username'];
            var ownerid = $cookies['UserId'];
            $scope.OwnerName = ownername;
            $scope.OwnerID = ownerid;
            $scope.ownerEmail = $cookies['UserEmail'];
            $scope.AutoCompleteSelectedObj = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.contrls = '';
            var Version = 1;
            $scope.FinancialRequestObj = [];
            $scope.optionsLists = [];
            $scope.entityNameTyping = "";
            $scope.AttributeData = [];
            $scope.entityName = "";
            $scope.count = 1;
            $scope.treelevels = [];
            $scope.DropDownTreeOptionValues = {};
            $scope.tree = {};
            $scope.MultiSelecttree = {};
            $scope.Options = {};
            $scope.wizard = { newval: '' };
            $scope.costcemtreObject = [];
            $scope.dyn_Cont = '';
            $scope.rootEntityID = '';
            $scope.rootEntityCaption = rootCaption;
            $scope.rootEntityID = rootID;
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            $scope.items = [];
            $scope.Entityamountcurrencytypeitem = [];
            $scope.Dropdown = {};
            $scope.treeSourcesObj = [];
            $scope.treeSources = {};
            $scope.tagwordids = [];
            $scope.EntityStatusResult = [];
            $scope.treesrcdirec = {};


            //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
            ObjectivecreationService.GetAttributeToAttributeRelationsByIDForEntity($scope.rootEntityID).then(function (entityAttrToAttrRelation) {
                // var resourceAttributeToAttributeRelationByID = $resource('metadata/GetAttributeToAttributeRelationsByIDForEntity/:ID', { ID: $scope.rootEntityID }, { get: { method: 'GET' } });
                // var entityAttrToAttrRelation = resourceAttributeToAttributeRelationByID.get({ ID: $scope.rootEntityID }, function () {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });

            //-----------------------------------------------------------
            ObjectivecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
                //var GetOwner = $resource('user/GetUserById/:ID', { ID: parseInt($scope.OwnerID, 10) }, { get: { method: 'GET' } });
                //var Getowner = GetOwner.get(function () {
                ObjectivecreationService.GetEntityStatus(rootID, false, 0).then(function (getentitytypestatus) {
                    $scope.EntityStatusResult = getentitytypestatus.Response;
                    $scope.owner = Getowner.Response;
                    $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
                    $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
                    if ($scope.OwnerList.length == 0)
                        $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
                    else {
                        var checkOwner = $.grep($scope.OwnerList, function (e) { return e.Userid == parseInt($scope.OwnerID, 10) && e.Roleid == 1 });
                        if(checkOwner.length == 0)
                            $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
                    }

                    ObjectivecreationService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                        //var resourceEntityTypeAttributeRelationByID = $resource('metadata/GetEntityTypeAttributeRelationWithLevelsByID/:ID/:ParentID', { ID: rootID, ParentID: 0 }, { get: { method: 'GET' } });
                        //var entityAttributesRelation = resourceEntityTypeAttributeRelationByID.get({ ID: rootID }, function () {
                        $scope.atributesRelationList = entityAttributesRelation.Response;
                        for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                            if ($scope.atributesRelationList[i].IsHelptextEnabled == true) {
                                var attribteHelptext = $scope.atributesRelationList[i].HelptextDecsription == null ? "-" : $scope.atributesRelationList[i].HelptextDecsription.trim() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription;
                                var qtip = 'my-qtip2 qtip-content="' + attribteHelptext + '"';
                            }
                            else
                                var qtip = "";
                            if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                if ($scope.atributesRelationList[i].AttributeID != 70) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;

                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    } else {
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    }
                                    $scope.setFieldKeys();
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].calenderAttributeID] = new Date.create();
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                // $scope.maxDate.setDate($scope.maxDate.getDate() + $scope.MaxValue);
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\"  ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.setFieldKeys();

                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].IsSpecial == true) {
                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                        $scope.setFieldKeys();
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                                else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                                }
                                else {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                            }

                            else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };

                                    if (j == 0) {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;

                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                    else {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"> ";
                                        $scope.dyn_Cont += "<option ng-repeat=\"item in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{item.id}}\">{{item.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                    $scope.setFieldKeys();
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) { return e.Id == defaultmultiselectvalue[j]; })[0].Id);
                                    }
                                }
                                else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                                $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                                $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                                $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i ng-show=\"$first==true\" my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                                $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";

                                //$scope.item.startDate = null;
                                //$scope.item.endDate = null;

                                $scope.setFieldKeys();

                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                                $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                                $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                                $scope.setoptions();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                    treeTextVisbileflag = false;
                                    if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                    }
                                    else
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                }
                                else {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                }
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                                $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                                $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                                $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                                $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                $scope.dyn_Cont += '</div><span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                $scope.dyn_Cont += '</div></div>';
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                                $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group \" drowdowntreepercentagemultiselection data-purpose='entity' data-helptextdescription=" + helptextdesc + " data-ishelptextenabled=" + $scope.atributesRelationList[i].IsHelptextEnabled.toString() + " data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";


                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < totLevelCnt1; j++) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };
                                    if (totLevelCnt1 == 1) {

                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\"  ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        $scope.setFieldKeys();
                                    }
                                    else {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                        if (j == 0) {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                            $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                            $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                            $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        } else {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            }
                                            else {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                            }
                                        }
                                    }
                                    $scope.setFieldKeys();
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.setoptions();
                                $scope.tempscope = [];
                                var helptext = $scope.atributesRelationList[i].HelptextDecsription == "" ? "" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                //$scope.dyn_Cont += "<div><directive-tagwords item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-caption=" + $scope.atributesRelationList[i].AttributeCaption + "></directive-tagwords></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                                $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                                $scope.dyn_Cont += "<div class=\"controls\">";
                                $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\" item-isenable=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" item-attributehelptext=\"" + helptext.toString() + "\"  ></directive-tagwords>";
                                $scope.dyn_Cont += "</div></div>";
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                var param1 = new Date.create();

                                var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.setFieldKeys();
                                $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group"><label class="control-label"';
                                $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                                $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                                if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                                    $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                                }
                                $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                                $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl">';
                                $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                                $scope.dyn_Cont += '<span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $translate.instant('LanguageContents.Res_5613.Caption') + "</label><div class=\"controls bgDropdown\"><select ui-select2=\"select2Config\" id=\"OverAllEntityStats\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"OverAllEntityStatus\" data-placeholder=\"Entity Status\">";
                                $scope.dyn_Cont += "<option value=\"0\"> Select " + $translate.instant('LanguageContents.Res_5613.Caption') + "</option>";
                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in EntityStatusResult\" value=\"{{Aitem.ID}}\">{{Aitem.StatusOptions}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = 0;

                                $scope.select2Config = {
                                    formatResult: $scope.overallstatusformat,
                                    formatSelection: $scope.overallstatusformat
                                };
                            }
                            $scope.setFieldKeys();
                            if ($scope.atributesRelationList[i].IsReadOnly == true) {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        }
                        $scope.setFieldKeys();
                        $scope.dyn_Cont += '<input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                        $("#ObjectiveTablMetadata").html(
                                     $compile($scope.dyn_Cont)($scope));

                        var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                        $scope.mindec = "";
                        if (mar.length == 1) {
                            $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "1" });
                        }
                        if (mar.length != 1) {
                            if (mar.length < 5) {
                                $scope.mindec = "0.";

                                for (i = 0; i < mar.length; i++) {
                                    $scope.mindec = $scope.mindec + "0";
                                }
                                $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: $scope.mindec });
                            }
                            else {
                                $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                            }
                        }

                        $("[id^='MTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                        $("[id^='MTextSingleLine_']").keydown(function (event) {
                            if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                                bootbox.alert($translate.instant('Please enter only Number'));
                                event.preventDefault(); //stop character from entering input
                            }
                        });
                        setTimeout(function () { $('[id^=TextSingleLine]:enabled:visible:first').focus().select() }, 1000);
                        $("#ObjectiveTablMetadata").scrollTop(0);
                        $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];

                        GetValidationList();
                        $("#ObjectiveTablMetadata").addClass('notvalidate');

                    });
                });
            });

            $scope.objectiveTypeChange();
            GetEntityTypeRoleAccess(rootID);

            //--Objective Units--
            ObjectivecreationService.GetObjectiveUnitsOptionValues(objUnitTypeId).then(function (ObjectiveUnitsResult) {
                $scope.ObjectiveUnitsData = ObjectiveUnitsResult.Response;
            });

            if (objUnitTypeId == 1)
                $scope.UnitLabel = 'Currency';
            else if (objUnitTypeId == 2)
                $scope.UnitLabel = 'Unit';

        }

        $scope.treePreviewObj = {};

        $("#rootLevelEntity").on("onRootObjecitveCreation", function (event, id) {
            $scope.WizardPosition = 1;
            $("#btnWizardFinish").removeAttr('disabled');
            $('#ObjectiveWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#btnWizardFinish').hide();
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $scope.Objective.ngObjectiveName = '';
            $scope.Objective.ngObjectiveDescription = '';
            $scope.Objective.ngObjectiveStatus = 1;
            $scope.Objective.ngObjectiveNumericUnits = 0;
            $scope.Objective.ngObjectiveNumericGlobalbaselineCheck = false;
            $scope.Objective.ngObjectiveNumericGlobalbaseline = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNumericGlobaltargetCheck = false;
            $scope.Objective.ngObjectiveNumericGlobaltarget = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNonNumericGlobalbaselineCheck = false;
            $scope.Objective.ngObjectiveNonNumericGlobalbaseline = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNonNumericGlobaltargetCheck = false;
            $scope.Objective.ngObjectiveNonNumericGlobaltarget = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNumericEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveNumericDescriptionInstructions = '';
            $scope.Objective.ngObjectivenonNumericUnits = 0;
            $scope.Objective.ngObjectivenonNumericEnableCommentsCheck = false;
            $scope.Objective.ngObjectivenonNumericDescriptionInstructions = '';
            $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions = '';
            $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveRatingDescriptionInstructions = '';
            $scope.Objective.ngObjectiveRatingEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveStatDaterange = '';
            $scope.Objective.ngObjectiveEndDaterange = '';
            $scope.Objective.ngObjectiveDaterule = 1;
            $scope.Objective.ngObjectiveMandatoryCheck = false;
            $scope.Objective.ngObjectiveType = 1;
            $scope.numglobalchecked = false;
            $scope.numtargetchecked = false;
            $scope.nonglobalchecked = false;
            $scope.nontargetchecked = false;
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
        });
        var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
        $('#btnWizardFinish').hide();
        $('#btnWizardNext').show();
        $('#btnWizardPrev').hide();
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.UserimageNewTime = new Date.create().getTime().toString();
        $scope.changeTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            } else {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === 2) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
        }
        $scope.changeObjectiveTab2 = function (e) {
            $timeout(function () {
                $("#btnTemp").click();
            }, 100);
            $("#ObjectiveTablMetadata").removeClass('notvalidate');
            if ($("#ObjectiveTablMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 2) {
                if ($scope.Objective.ngObjectiveType == 1) {
                    $timeout(function () {
                        $("#btnnumeric").click();
                    }, 100);
                    if ($("#Numericform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 2) {
                    $timeout(function () {
                        $("#btnnonnumeric").click();
                    }, 100);
                    if ($("#NonNumericform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 3) {
                    $timeout(function () {
                        $("#btnqualitative").click();
                    }, 100);
                    if ($("#Qualitativeform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 4) {
                    $timeout(function () {
                        $("#btnrating").click();
                    }, 100);
                    if ($("#Ratingform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 3) {
                if ($(e.target).attr("data-target") == "#step2") { return false;} else {
                    $timeout(function () {
                        $("#btnTemp3").click();
                    }, 100);
                    if ($("#ObjectiveFulfillTab .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 3) {
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    if ($('#ObjEntityType' + index).val() == "0" || $('#ObjEntityType' + index).val() == null) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        return false;
                    }
                });
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#ObjectiveWizard ul.steps").find("li").addClass("complete");
            $("#ObjectiveWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#ObjectiveWizard').wizard('previous');
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            $scope.WizardPosition = currentWizardStep;
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            } else {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }
        $scope.status = true;
        $("#Div_Objlist").on('loadObjectiveUIControls', function (event) {
            ValidObjectiveTab1();
            ValidObjectiveTab2();
            ValidObjectiveTab3();
            handleTextChangeEvents();
        });
        $scope.cancelmodel = function () {
            $timeout(function () {
                if ($('#ObjectiveTablMetadata').nod() != undefined) $('#ObjectiveTablMetadata').nod().destroy();
                if ($('#NonNumericform').nod() != undefined) $('#NonNumericform').nod().destroy();
                if ($('#ObjectiveFulfillTab').nod() != undefined) $('#ObjectiveFulfillTab').nod().destroy();
            }, 100);
            $scope.closeObjectivecreationpopup();
        }
        $scope.changenexttab = function () {
            $("#btnTemp").click();
            $("#ObjectiveTablMetadata").removeClass('notvalidate');
            if ($("#ObjectiveTablMetadata .error").length > 0) {
                return false;
            }
            if ($scope.WizardPosition == 2) {
                if ($scope.Objective.ngObjectiveType == 1) {
                    setTimeout(function () { $("#btnnumeric").click(); }, 50);
                    if ($("#Numericform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 2) {
                    setTimeout(function () { $("#btnnonnumeric").click(); }, 50);
                    if ($("#NonNumericform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 3) {
                    setTimeout(function () { $("#btnqualitative").click(); }, 50);
                    if ($("#Qualitativeform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 4) {
                    setTimeout(function () { $("#btnrating").click(); }, 50);
                    if ($scope.ratingItems[$scope.ratingItems.length - 1].ObjRatingText == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                        return false;
                    }
                    if ($("#Ratingform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == "4") {
                    if ($scope.ratingItems.length > 0) {
                        for (var i = 0; i < 2; i++) {
                            if ($scope.ratingItems[i].ObjRatingText == "" || $scope.ratingItems[i].ObjRatingText == undefined) {
                                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                                return false;
                            }
                        }
                    }
                }
            }
            if ($scope.WizardPosition == 3) {
                setTimeout(function () { $("#btnTemp3").click(); }, 50);
                var st_date = $('#ObjectiveStatDaterange').val();
                var end_date = $('#ObjectiveEndDaterange').val();
                if (ConvertStringToDateByFormat(end_date, $scope.DefaultSettings.DateFormat) < ConvertStringToDateByFormat(st_date, $scope.DefaultSettings.DateFormat)) return false
                if ($("#ObjectiveFulfillTab .error").length > 0) {
                    return false;
                }
                $scope.ConditionDataArr = [];
                var EntttyTypeArr = new Array();
                var attriArreArr = new Array();
                var optionArr = [];
                var tempval = false;
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    if (index > 0 && $('#ObjConditionType' + index).val() == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_4617.Caption'));
                        tempval = true;
                        return false;
                    }
                    if ($('#ObjEntityType' + index).val() == "0" || $('#ObjEntityType' + index).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        tempval = true;
                        return false;
                    } else if ($('#ObjEntityAttributes' + index).val() != "0" && $('#ObjEntityAttributes' + index).val() != null) {
                        if ($('#ObjAttributeOptions' + index).val() == null) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                            tempval = true;
                            return false;
                        }
                    }
                });
            }
            if (tempval == true) return false;
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#ObjectiveWizard').wizard('next', '');
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            $scope.WizardPosition = currentWizardStep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            } else {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }
        $scope.ObjectiveUnitsData = [];
        $scope.FulfillmentEntityTypes = [];
        $scope.FulfillmentAttributes = [];
        $scope.FulfillmentAttributeOptions = [];
        $scope.fieldKeys = [];
        $scope.fields = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.creatObjectivefullfilmetadataHtml = '';
        $scope.ObjectiveEntityTypeHtml = '';
        $scope.ObjectiveFullfilAttributes = [];
        $scope.FulfillmentAttributeOptions = {};
        $scope.ObjectiveFullfilOptionsHtml = '';
        $scope.AddMulticonditionHtml = '';
        $scope.RatingsHtml = '';
        $scope.ObjectiveFulfillConditions = [];
        $scope.Objective = {
            ngObjectiveName: '',
            ngObjectiveDescription: '',
            ngObjectiveStatus: 1,
            ngObjectiveNumericUnits: 0,
            ngObjectiveNumericGlobalbaselineCheck: false,
            ngObjectiveNumericGlobalbaseline: 0,
            ngObjectiveNumericGlobaltargetCheck: false,
            ngObjectiveNumericGlobaltarget: 0,
            ngObjectiveNonNumericGlobalbaselineCheck: false,
            ngObjectiveNonNumericGlobalbaseline: 0,
            ngObjectiveNonNumericGlobaltargetCheck: false,
            ngObjectiveNonNumericGlobaltarget: 0,
            ngObjectiveNumericEnableCommentsCheck: false,
            ngObjectiveNumericDescriptionInstructions: '',
            ngObjectivenonNumericUnits: 0,
            ngObjectivenonNumericEnableCommentsCheck: false,
            ngObjectivenonNumericDescriptionInstructions: '',
            ngObjectiveQulaitativeDescriptionInstructions: '',
            ngObjectiveQulaitativeEnableCommentsCheck: false,
            ngObjectiveRatingDescriptionInstructions: '',
            ngObjectiveRatingEnableCommentsCheck: false,
            ngObjectiveStatDaterange: '',
            ngObjectiveEndDaterange: '',
            ngObjectiveDaterule: 1,
            ngObjectiveMandatoryCheck: false,
            ngObjectiveType: 1
        }
        $scope.rowCount = 0;
        $scope.ratingRowsCount = 0;
        $scope.numericObjectiveArr = [];
        var objOwnername = $cookies['Username'];
        var objOwnerid = $cookies['UserId'];
        $scope.OwnerName = objOwnername;
        $scope.OwnerID = objOwnerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.MemberLists = [];
        $scope.AutoCompleteSelectedObj = [];
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = [];
        $scope.OwnerList = [];
        $scope.owner = {};
        ObjectivecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            if ($scope.OwnerList.length == 0) {
                $scope.OwnerList.push({
                    "Roleid": 1,
                    "RoleName": "Owner",
                    "UserEmail": $scope.ownerEmail,
                    "DepartmentName": $scope.owner.Designation,
                    "Title": $scope.owner.Title,
                    "Userid": parseInt($scope.OwnerID, 10),
                    "UserName": $scope.OwnerName,
                    "IsInherited": '0',
                    "InheritedFromEntityid": '0',
                    "QuickInfo1": $scope.owner.QuickInfo1,
                    "QuickInfo2": $scope.owner.QuickInfo2
                });
            }
            else {
                var checkOwner = $.grep($scope.OwnerList, function (e) { return e.Userid == parseInt($scope.OwnerID, 10) && e.Roleid == 1 });
                if(checkOwner.length == 0)
                    $scope.OwnerList.push({
                        "Roleid": 1,
                        "RoleName": "Owner",
                        "UserEmail": $scope.ownerEmail,
                        "DepartmentName": $scope.owner.Designation,
                        "Title": $scope.owner.Title,
                        "Userid": parseInt($scope.OwnerID, 10),
                        "UserName": $scope.OwnerName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "QuickInfo1": $scope.owner.QuickInfo1,
                        "QuickInfo2": $scope.owner.QuickInfo2
                    });
            }
        })
        $scope.contrls = '';
        //ObjectivecreationService.GetEntityTypeRoleAccess(10).then(function (role) {
        //    $scope.Roles = role.Response;
        //});

        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10);
            });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.fields['userRoles'])
                })[0];
                if (membervalues != undefined) {
                    $scope.MemberLists.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedObj[0].Email,
                        "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation,
                        "Title": $scope.AutoCompleteSelectedObj[0].Title,
                        "Roleid": parseInt(membervalues.ID, 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10),
                        "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2
                    });
                    $scope.count = $scope.count + 1;
                }
                $scope.AutoCompleteSelectedObj = [];
                $scope.fields.usersID = '';
            } else bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
        };
        $scope.deleteOptions = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                $timeout(function () {
                    if (result) {
                        $scope.MemberLists.splice($.inArray(item, $scope.MemberLists), 1);
                    }
                }, 100);
            });
        };
        $scope.ratingItems = [];
        $scope.ratingItems.push({
            ObjRatingText: ''
        });
        $scope.checklistratingIndex = 0;
        $scope.AddRatings = function (index) {
            if ($scope.ratingItems[index].ObjRatingText == "" || $.grep($scope.ratingItems, function (e) { return e.ObjRatingText == "" }).length > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                return false;
            }
            if ($scope.checklistratingIndex == -1) {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.ratingItems.push({ ObjRatingText: '' });
            }
            else {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.checklistratingIndex = parseInt(index) + 1;
                $scope.ratingItems.splice(parseInt($scope.checklistratingIndex), 0, { ObjRatingText: '' });
            }
        }
        $scope.AddRatings1 = function () {
            $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
        };
        $scope.RemoveRatings = function (item) {
            if ($scope.ratingRowsCount < 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_1917.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount - 1;
                $scope.ratingItems.splice($.inArray(item.item, $scope.ratingItems), 1);
            }
        };
        $scope.numericqunatitativedisplay = true;
        $scope.Objective.ngObjectiveOwner = $scope.OwnerName;

        if ($('#ObjectiveNumericGlobalbaseline').is(':checked')) {
            $('#ObjectiveNumericGlobaltarget').attr("disabled", false);
        }
        $scope.numglobalchecked = false;
        $scope.numtargetchecked = false;
        $scope.nonglobalchecked = false;
        $scope.nontargetchecked = false;
        $scope.objectiveItems = [];
        $scope.objectiveItems.push({
            ObjEntityType: [],
            ObjEntityAttributes: [],
            ObjAttributeOptionns: [],
            ObjCondition: 0
        });
        $scope.addMultiPleConditions = function () {
            if ($scope.objectiveItems[$scope.rowCount].ObjEntityType.length == 0 || $scope.objectiveItems[$scope.rowCount].ObjEntityAttributes.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1981.Caption'));
                return false;
            } else {
                $scope.rowCount = $scope.rowCount + 1;
                $scope.objectiveItems.push({
                    ObjEntityType: [],
                    ObjEntityAttributes: [],
                    ObjAttributeOptionns: [],
                    ObjCondition: []
                });
                $scope.FulfillmentAttributes = [];
                $scope.FulfillmentAttributeOptions = {};
            }
        };
        $scope.objectiveItemsduplicate = [];
        $scope.objectiveItemsduplicate.push({
            ObjCondition: []
        });
        $scope.addMultiPleConditionsduplicate = function (item) {
            $scope.objectiveItemsduplicate.push({
                ObjCondition: []
            });
        };
        $scope.removeFulCondition = function (item) {
            if ($scope.rowCount > 0) {
                $scope.rowCount = $scope.rowCount - 1;
                $scope.objectiveItems.splice($.inArray(item, $scope.items), 1);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                return false;
            }
        };
        FilltFulfillmentEntityTypes("ObjEntityType0");

        function FilltFulfillmentEntityTypes(ControllerID) {
            ObjectivecreationService.GetFulfillmentEntityTypes().then(function (fulFillmentEntityTypesObj) {
                $scope.FulfillmentEntityTypes = fulFillmentEntityTypesObj.Response;
                var objentities = fulFillmentEntityTypesObj.Response;
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option></option>').val(item.Id).html(item.Caption))
                });
            });
        }
        $scope.LoadObjectiveFulfillmentMetadatatbody=function(event){
        //$("#ObjectiveFulfillmentMetadatatbody").click(function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') {
                ResetDropDown("ObjEntityAttributes" + currentUniqueId, currentUniqueId);
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                ObjectivecreationService.GetFulfillmentAttribute(TargetControl.val()).then(function (fulFillmentAttributesObj) {
                    if (fulFillmentAttributesObj.Response.length > 0) {
                        FillEntityTypeAttributes("ObjEntityAttributes" + currentUniqueId, fulFillmentAttributesObj.Response);
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Attributes') {
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                ObjectivecreationService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillEntityTypeAttributes("ObjAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response)
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Equals') { } else if (TargetControl.attr('data-role') == 'Add') {
                if (currentUniqueId > 0 && TargetControl.parents().find('#ObjConditionType' + currentUniqueId).val() == "") {
                    bootbox.alert($scope.LanguageContents_4617);
                    return false;
                }
                if (TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                    return false;
                }
                if ($('#ObjEntityAttributes' + currentUniqueId).val() != "0" && $('#ObjEntityAttributes' + currentUniqueId).val() != null) {
                    if ($('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }
                var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='ObjectiveCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                html += "<select ui-select2 id='ObjConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "'>";
                html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + "-- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_813.Caption') + " </option>";
                html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_813.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='dummymodule_ObjEntityType_" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0' ng-selected='true'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes'>";
                html += "<option value='0'>--  " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "' data-role='Equals'>";
                html += "<option  value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("ObjEntityType" + UniqueId);
            } else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                    return false;
                }
                $("#container" + currentUniqueId).remove();
            }
        };

        function FillEntityTypes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentEntityTypes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                });
            }
        }

        function FillEntityTypeAttributes(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(ControllerID, currentUniqueId) {
            $('#' + ControllerID).html("");
            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#' + ControllerID).select2("val", 0);
            $('#ObjAttributeOptions' + currentUniqueId).html("");
            $('#ObjAttributeOptions' + currentUniqueId).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#ObjAttributeOptions' + currentUniqueId).select2("val", "");
        }
        $scope.ObjDatavalue = {};
        $scope.objectiveConditionsValues = {};
        $scope.createNewObjective = function (event) {
            // $("#btnWizardFinish").click(function (event) {
            $("#btnWizardFinish").attr('disabled', 'disabled');
            if (uploaderAttrInfo.length > 0) {

                ObjectivecreationService.copyuploadedImage(uploaderAttrInfo[0].attrFileName).then(function (newuplAttrName) {
                    var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfo[0].attrID });
                    var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                    $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                    saveobjectiveEntity();
                })
            }
            else {
                saveobjectiveEntity();
            }
        };
        function saveobjectiveEntity() {
            collectionObjectiveCondition();
            $scope.ObjDatavalue = $scope.ConditionDataArr;

            //-------------> DETAIL BLOCK DATA <----------
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                        else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                                else {
                                    $scope.AttributeData.push({

                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]],
                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1)
                                        valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                    else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                            var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }

                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({ amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    if ($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        var dte = new Date.create($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID]);
                        var tdte = ConvertDateToString(dte);

                        //  var dateval = ConvertDateFromStringToString($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID].toString())
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": tdte,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });

                }


                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {

                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null) {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                        }
                        else {
                            //$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString('MM/dd/yyyy');
                            MyDateString = "";
                        }
                    }
                    else {
                        MyDateString = "";
                    }

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].attrFileID == undefined || $scope.atributesRelationList[i].attrFileID == null) {
                        $scope.atributesRelationList[i].attrFileID = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.atributesRelationList[i].attrFileID,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
            }


            //-------------> OBJECTIVE TYPE DATA <--------------
            var ObjectiveEnttiy = {};
            ObjectiveEnttiy.ObjectiveTypeID = $scope.Objective.ngObjectiveType;
            ObjectiveEnttiy.ObjectiveName = $scope.entityName;
            ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveDescription;
            ObjectiveEnttiy.ObjectiveStatus = parseInt($scope.Objective.ngObjectiveStatus, 10);
            ObjectiveEnttiy.ObjectiveStartDate = $scope.Objective.ngObjectiveStatDaterange.toString('MM/dd/yyyy');
            ObjectiveEnttiy.ObjectiveEndDate = $scope.Objective.ngObjectiveEndDaterange.toString('MM/dd/yyyy');
            ObjectiveEnttiy.ObjectiveDateRule = $scope.Objective.ngObjectiveDaterule;
            $scope.CurrentGlobalBaseLine = 0;
            $scope.CurrentGlobalTarget = 0;
            if ($scope.Objective.ngObjectiveNumericGlobalbaseline != 0) $scope.CurrentGlobalBaseLine = $scope.Objective.ngObjectiveNumericGlobalbaseline.replace(/\s/g, '');
            else $scope.CurrentGlobalBaseLine = 0;
            if ($scope.Objective.ngObjectiveNumericGlobaltarget != 0) {
                $scope.CurrentGlobalTarget = $scope.Objective.ngObjectiveNumericGlobaltarget.replace(/\s/g, '');
            }
            if ($scope.Objective.ngObjectiveType == 1) {
                ObjectiveEnttiy.ObjectiveNumericData = [{
                    "UnitID": $scope.Objective.objUnitTypeId == '' ? 0 : parseInt($scope.Objective.objUnitTypeId, 10),
                    "GlobalBaseLine": $scope.CurrentGlobalBaseLine,
                    "GlobalTarget": $scope.CurrentGlobalTarget,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveNumericEnableCommentsCheck,
                    "Instruction": $scope.Objective.ngObjectiveNumericDescriptionInstructions,
                    "UnitValue": $scope.Objective.ngObjectiveNumericUnits == '' ? 0 : parseInt($scope.Objective.ngObjectiveNumericUnits, 10)
                }];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveNumericDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 2) {
                $scope.CurrentNonNumericGlobalBaseLine = 0;
                $scope.CurrentNonNumericGlobalTarget = 0;
                if ($scope.Objective.ngObjectiveNonNumericGlobalbaseline != 0) $scope.CurrentNonNumericGlobalBaseLine = $scope.Objective.ngObjectiveNonNumericGlobalbaseline.replace(/\s/g, '');
                else $scope.CurrentNonNumericGlobalBaseLine = 0;
                if ($scope.Objective.ngObjectiveNonNumericGlobaltarget != 0) {
                    $scope.CurrentNonNumericGlobalTarget = $scope.Objective.ngObjectiveNonNumericGlobaltarget.replace(/\s/g, '');
                }
                ObjectiveEnttiy.ObjectiveNonNumericData = [{
                    "UnitID": $scope.Objective.objUnitTypeId == '' ? 0 : parseInt($scope.Objective.objUnitTypeId, 10),
                    "GlobalBaseLine": $scope.CurrentNonNumericGlobalBaseLine,
                    "GlobalTarget": $scope.CurrentNonNumericGlobalTarget,
                    "IsEnableFeedback": $scope.Objective.ngObjectivenonNumericEnableCommentsCheck,
                    "Instruction": $scope.Objective.ngObjectivenonNumericDescriptionInstructions,
                    "UnitValue": $scope.Objective.ngObjectivenonNumericUnits == '' ? 0 : parseInt($scope.Objective.ngObjectivenonNumericUnits, 10)
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectivenonNumericDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 3) {
                ObjectiveEnttiy.ObjectiveQualitativeData = [{
                    "Instruction": $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 4) {
                var ratingCollectionObject = [];
                if ($scope.ratingItems.length > 0) {
                    for (var i = 0; i < $scope.ratingItems.length; i++) {
                        ratingCollectionObject.push($scope.ratingItems[i].ObjRatingText);
                    }
                }
                ObjectiveEnttiy.ObjectiveRatingData = [{
                    "Instruction": $scope.Objective.ngObjectiveRatingDescriptionInstructions,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveRatingEnableCommentsCheck,
                    "Ratings": ratingCollectionObject
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveRatingDescriptionInstructions;
            }

            //---------------> OBJECTIVE FULLFILLMENT DATA <------------            
            ObjectiveEnttiy.ObjectiveConditonData = [{
                "StartDate": $scope.Objective.ngObjectiveStatDaterange.toString('MM/dd/yyyy'),
                "EndDate": $scope.Objective.ngObjectiveEndDaterange.toString('MM/dd/yyyy'),
                "DateRule": parseInt($scope.Objective.ngObjectiveDaterule, 10),
                "IsMandatory": $scope.Objective.ngObjectiveMandatoryCheck,
            }]
            if ($scope.ObjDatavalue[0].ObjEntityType != "0") {
                ObjectiveEnttiy.ObjectiveFulfillConditonValueData = [{
                    "ObjectiveFulfillValues": $scope.ObjDatavalue
                }]
            } else if ($scope.ObjDatavalue[0].ObjEntityType == "0") {
                ObjectiveEnttiy.ObjectiveFulfillConditonValueData = []
            }
            ObjectiveEnttiy.ObjectiveMembers = $scope.MemberLists;
            ObjectiveEnttiy.ObjectiveOwner = $scope.OwnerList;
            ObjectiveEnttiy.Periods = [];
            $scope.StartEndDate = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.toString().length != 0 && $scope.items[m].endDate.toString().length != null) {
                        $scope.StartEndDate.push({
                            startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                            endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                            comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
            }
            ObjectiveEnttiy.Periods.push($scope.StartEndDate);
            ObjectiveEnttiy.AttributeData = $scope.AttributeData;
            ObjectiveEnttiy.objEntityTypeId = $scope.Objective.objEntityTypeId;
            ObjectivecreationService.CreateObjective(ObjectiveEnttiy).then(function (objetiveCreationResult) {
                if (objetiveCreationResult.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4538.Caption'));
                    var IDList = new Array();
                    IDList.push(objetiveCreationResult.Response);
                    $window.ListofEntityID = IDList;
                    var TrackID = CreateHisory(IDList);
                    if (!$scope.$$phase) {
                        var localScope = $(event.target).scope();
                        localScope.$apply(function () {
                            $location.path('/mui/objective/detail/section/' + objetiveCreationResult.Response + '/overview');
                        });
                    } else {
                        $location.path('/mui/objective/detail/section/' + objetiveCreationResult.Response + '/overview');
                    }
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4282.Caption'));
                }
            });
            $scope.closeObjectivecreationpopup();
        }
        $scope.ConditionDataArr = [];

        function collectionObjectiveCondition() {
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            var optionArr = [];
            $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                if ($('#ObjConditionType' + index).val() == '') {
                    var collectionarr = {
                        'ObjEntityType': $('#ObjEntityType' + index).val(),
                        'ObjEntityAttributes': $('#ObjEntityAttributes' + index).val(),
                        'ObjAttributeOptionns': $('#ObjAttributeOptions' + index).val(),
                        'ConditionType': 0
                    };
                } else {
                    var collectionarr = {
                        'ObjEntityType': $('#ObjEntityType' + index).val(),
                        'ObjEntityAttributes': $('#ObjEntityAttributes' + index).val(),
                        'ObjAttributeOptionns': $('#ObjAttributeOptions' + index).val(),
                        'ConditionType': $('#ObjConditionType' + index).val()
                    };
                }
                $scope.ConditionDataArr.push(collectionarr);
            });
        }
        $scope.SaveObjectiveName = function (objValue) { }

        function SetCursorPosition(ctrID, cursorPosition) {
            var cnt = ctrID.val().length;
            if (cnt > 4) {
                cursorPosition = cursorPosition + 1;
                if (cnt > 5) cursorPosition = cursorPosition - 1;
                if (cnt > 8) cursorPosition = cursorPosition + 1;
                if (cnt > 9) cursorPosition = cursorPosition - 1;
            }
            setTimeout(function () {
                ctrID.caret(cursorPosition);
            }, 0);
        }

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function getMoney(A) {
            var a = new Number(A);
            var b = a.toFixed(2);
            a = parseInt(a);
            b = (b - a).toPrecision(2);
            b = parseFloat(b).toFixed(2);
            a = a.toLocaleString();
            a = a.replace(",", "");
            a = Number(a);
            a = a.formatMoney(0, ' ', ' ');
            if (a < 1 && a.lastIndexOf('.00') == (a.length - 3)) {
                a = a.substring(0, a.length - 3);
            }
            if (b.substring(0, 1) == '-') {
                b = b.substring(2);
            } else b = b.substring(1);
            return a + b;
        }

        function handleTextChangeEvents() {
            $('#ObjectiveNumericGlobalbaseline ,#ObjectiveNumericGlobaltarget ,#ObjectiveNonNumericGlobalbaseline ,#ObjectiveNonNumericGlobaltarget').autoNumeric('init', $scope.DecimalSettings['ObjectiveAutoNumeric']);
        }

        $scope.EntityStatusResult = [];

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id == "0" && data.text != "") {
                return "<div class='select2-chosen-label'style='background-color: #FEFEFE; color: #444444; font-size: 11px;'>" + data.text + "</div>";
            }
            else if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }
            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };
        setTimeout(function () {
            $scope.LoadAttributesForObjectiveCreation(params.rootID, params.rootCaption, params.objBaseType, params.objEntityTypeId, params.objUnitTypeId);
        }, 300);
        $scope.closeObjectivecreationpopup = function () {
            $modalInstance.dismiss('cancel');
        }

        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            if ($scope.fields["Tree_" + branch.AttributeId].length > 0) {
                if ($("#Tree_" + branch.AttributeId).parent().parent().parent().hasClass('error') == true) {
                    if (branch.AttributeId > 0) {
                        $("#Tree_" + branch.AttributeId).parent().parent().parent().removeClass("error");
                        $("#Tree_" + branch.AttributeId).next().hide();
                    }
                }
            }
            $scope.ShowOrHideAttributeToAttributeRelation(branch.AttributeId + "_0", 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};

        $scope.$on("$destroy", function () {
            $timeout.cancel(ObjectivecreationTimout);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.section.additionalobjectiveCtrl']"));
            var model, renderContext, detailTabValues, DateCompare, st_date, end_date, fulfillTabValues, totalWizardSteps, currentWizardStep, totalWizardSteps, EntttyTypeArr, attriArreArr, optionArr, tempval, keys, objOwnername, objOwnerid, result, membervalues, objentities, TargetControl, currentUniqueId, AttributeData, entityAttribtueId, entityAttributeLevel, UniqueId, html, currentId, ObjectiveEnttiy, ratingCollectionObject, i, x, j, a, b, IDList, TrackID, localScope, collectionarr, cnt, retCommaString, getstr, count, commaString = null, treeTextVisbileflag;
        });
    }
    app.controller("mui.planningtool.objective.objectivecreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'ObjectivecreationService', '$modalInstance', 'params', muiplanningtoolobjectiveobjectivecreationCtrl]);

})(angular, app);