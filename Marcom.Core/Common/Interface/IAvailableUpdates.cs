﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IAvailableUpdates
    {
         string NewVersion { get; set; }
         string Feature { get; set; }
         string DateReleased { get; set; }
         int Id { get; set; }
         string DocPath { get; set; }
    }
}
