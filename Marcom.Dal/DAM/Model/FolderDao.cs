﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.DAM.Model
{

    /// <summary>
    /// TreeNodeDao object for table 'DAM_Folder'.
    /// </summary>

    public partial class FolderDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _nodeid;
        protected int _parentpodeid;
        protected int _level;
        protected string _key;
        protected int _entityid;
        protected string _caption;
        protected string _description;
        protected int _sortOrder;
        protected string _colorCode;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public FolderDao() { }

        public FolderDao(int pId, int pNodeid, int pParentnodeid, int pLevel, string pKEY, int pEntityid, string pCaption, int pSortOrder, string pDescription, string pColorCode)
        {
            this._id = pId;
            this._nodeid = pNodeid;
            this._level = pLevel;
            this._key = pKEY;
            this._parentpodeid = pParentnodeid;
            this._entityid = pEntityid;
            this._caption = pCaption;
            this._sortOrder = pSortOrder;
            this._description = pDescription;
            this._colorCode = pColorCode;
        }

        public FolderDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int NodeID
        {
            get { return _nodeid; }
            set { _bIsChanged |= (_nodeid != value); _nodeid = value; }

        }
        public virtual int ParentNodeID
        {
            get { return _parentpodeid; }
            set { _bIsChanged |= (_parentpodeid != value); _parentpodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _bIsChanged |= (_level != value); _level = value; }

        }

        public virtual string KEY
        {
            get { return _key; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("key", "key value, cannot contain more than 250 characters");
                _bIsChanged |= (_key != value);
                _key = value;
            }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }




        public virtual string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
                _bIsChanged |= (_caption != value);
                _caption = value;
            }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 500 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _bIsChanged |= (_sortOrder != value); _sortOrder = value; }

        }

        public virtual string Colorcode
        {
            get { return _colorCode; }
            set
            {
                _colorCode = value;
            }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string FolderDao = "FolderDao";
            public const string Id = "Id";
            public const string NodeID = "NodeID";
            public const string ParentNodeID = "ParentNodeID";
            public const string Level = "Level";
            public const string KEY = "KEY";
            public const string EntityID = "EntityID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string SortOrder = "SortOrder";
            public const string Colorcode = "Colorcode";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string FolderDao = "FolderDao";
            public const string Id = "Id";
            public const string NodeID = "NodeID";
            public const string ParentNodeID = "ParentNodeID";
            public const string Level = "Level";
            public const string KEY = "KEY";
            public const string EntityID = "EntityID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string SortOrder = "SortOrder";
            public const string Colorcode = "Colorcode";
        }

        #endregion


    }

}
