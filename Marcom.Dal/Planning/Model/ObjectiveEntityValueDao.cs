using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveEntityValueDao object for table 'PM_ObjectiveEntityValue'.
	/// </summary>
	
	public partial class ObjectiveEntityValueDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _objectiveid;
		protected int _entityid;
        protected decimal _plannedtarget;
        protected decimal _targetoutcome;
		protected int _ratingobjective;
		protected string _comments;
		protected int _status;
		protected int _fulfilment;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveEntityValueDao() {}

        public ObjectiveEntityValueDao(int pId, int pObjectiveid, int pEntityid, decimal pPlannedTarget, decimal pTargetOutcome, int pRatingObjective, string pComments, int pStatus, int pFulfilment)
		{
            this._id = pId;
			this._objectiveid = pObjectiveid; 
			this._entityid = pEntityid; 
			this._plannedtarget = pPlannedTarget; 
			this._targetoutcome = pTargetOutcome; 
			this._ratingobjective = pRatingObjective; 
			this._comments = pComments; 
			this._status = pStatus; 
			this._fulfilment = pFulfilment; 
		}
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
		public virtual int Objectiveid
		{
			get { return _objectiveid; }
			set { _bIsChanged |= (_objectiveid != value); _objectiveid = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}

        public virtual decimal PlannedTarget
		{
			get { return _plannedtarget; }
			set { _bIsChanged |= (_plannedtarget != value); _plannedtarget = value; }
			
		}

        public virtual decimal TargetOutcome
		{
			get { return _targetoutcome; }
			set { _bIsChanged |= (_targetoutcome != value); _targetoutcome = value; }
			
		}
		
		public virtual int RatingObjective
		{
			get { return _ratingobjective; }
			set { _bIsChanged |= (_ratingobjective != value); _ratingobjective = value; }
			
		}
		
		public virtual string Comments
		{
			get { return _comments; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Comments", "Comments value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_comments != value); 
			  _comments = value; 
			}
			
		}
		
		public virtual int Status
		{
			get { return _status; }
			set { _bIsChanged |= (_status != value); _status = value; }
			
		}
		
		public virtual int Fulfilment
		{
			get { return _fulfilment; }
			set { _bIsChanged |= (_fulfilment != value); _fulfilment = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveEntityValueDao = "ObjectiveEntityValueDao";			
            public const string Id = "Id";
			public const string Objectiveid = "Objectiveid";			
			public const string Entityid = "Entityid";			
			public const string PlannedTarget = "PlannedTarget";			
			public const string TargetOutcome = "TargetOutcome";			
			public const string RatingObjective = "RatingObjective";			
			public const string Comments = "Comments";			
			public const string Status = "Status";			
			public const string Fulfilment = "Fulfilment";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveEntityValueDao = "ObjectiveEntityValueDao";			
            public const string Id = "Id";
			public const string Objectiveid = "Objectiveid";			
			public const string Entityid = "Entityid";			
			public const string PlannedTarget = "PlannedTarget";			
			public const string TargetOutcome = "TargetOutcome";			
			public const string RatingObjective = "RatingObjective";			
			public const string Comments = "Comments";			
			public const string Status = "Status";			
			public const string Fulfilment = "Fulfilment";			
		}

		#endregion
		
		
	}
	
}
