﻿using BrandSystems.Marcom.Core.Planning.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class FinancialForecastSettings : IFinancialForecastSettings
    {    
        #region  Member Variables

        protected int _id;
        protected bool _isFinancialforecast;
        protected int _forecastDivision;
        protected int _forecastBasis;
        protected int _forecastLevel;
        protected int _forecastdeadlines;
       
        #endregion
        #region Constructors
        public  FinancialForecastSettings() { }
        public FinancialForecastSettings(int pid, bool pIsFinancialforecast, int pForecastDivision, int pForecastBasis, int pForecastLevel, int pForecastdeadlines)
        {
            this._id = pid;
            this. _isFinancialforecast = pIsFinancialforecast;
            this._forecastDivision = pForecastDivision;
            this._forecastBasis = pForecastBasis;
            this._forecastLevel = pForecastLevel;
            this._forecastdeadlines =  pForecastdeadlines;
            
        }

         #endregion
         #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set {  _id = value; }

        }

        public virtual bool IsFinancialForecast
        {
            get { return _isFinancialforecast; }
            set { _isFinancialforecast = value; }

        }

        public virtual int ForecastDivision
        {
            get { return _forecastDivision; }
            set {  _forecastDivision = value; }

        }

        public virtual int ForecastBasis
        {
            get { return _forecastBasis; }
            set {  _forecastBasis = value; }

        }

        public virtual int ForecastLevel
        {
            get { return _forecastLevel; }
            set {  _forecastLevel = value; }

        }

        public virtual int ForecastDeadlines
        {
            get { return _forecastdeadlines; }
            set {  _forecastdeadlines = value; }

        }

      
        #endregion
        	#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Financial castObj = null;
			try
			{
				castObj = (Financial)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
    }
}
