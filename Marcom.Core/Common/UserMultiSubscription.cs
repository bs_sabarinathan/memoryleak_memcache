using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserMultiSubscription object for table 'CM_UserMultiSubscription'.
	/// </summary>
	
	internal class UserMultiSubscription : IUserMultiSubscription 
	{
		#region Member Variables

		protected ISubscriptionType _subscriptionid;
		protected int _entitytypeid;
		
		
		#endregion
		
		#region Constructors
		public UserMultiSubscription() {}
			
		public UserMultiSubscription(SubscriptionType pSubscriptionid, int pEntityTypeid)
		{
			this._subscriptionid = pSubscriptionid; 
			this._entitytypeid = pEntityTypeid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public ISubscriptionType Subscriptionid
		{
			get { return _subscriptionid; }
			set { _subscriptionid = value; }
			
		}
		
		public int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _entitytypeid = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserMultiSubscription castObj = null;
			try
			{
				castObj = (UserMultiSubscription)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
