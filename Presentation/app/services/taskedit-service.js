﻿(function (ng, app) {
    "use strict";

    function TaskEditService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetTakUIData: GetTakUIData,
            GetTaskDetails: GetTaskDetails,
            CreateNewVersion: CreateNewVersion,
            ReorderOverviewStructure: ReorderOverviewStructure,
            UpdatetaskEntityTaskDetailsInTaskTab: UpdatetaskEntityTaskDetailsInTaskTab,
            UpdatetaskEntityTaskDueDate: UpdatetaskEntityTaskDueDate,
            UpdateTaskStatusInTaskTab: UpdateTaskStatusInTaskTab,
            AddNewPhase: AddNewPhase,
            AddPhaseStep: AddPhaseStep,
            RemovePhase: RemovePhase,
            DeletePhaseStep: DeletePhaseStep,
            getAllMemberIDbyStepID: getAllMemberIDbyStepID,
            sendReminderNotificationforTaskPhase: sendReminderNotificationforTaskPhase,
            RemovePhaseStepApprovar: RemovePhaseStepApprovar,
            DeleteTaskMemberById: DeleteTaskMemberById,
            getTaskApprovalRolesUsers: getTaskApprovalRolesUsers,
            InsertTaskMembers: InsertTaskMembers,
            InsertUpdateEntityTaskCheckList: InsertUpdateEntityTaskCheckList,
            DeleteEntityCheckListByID: DeleteEntityCheckListByID,
            GetEntityTypeRoleAccess: GetEntityTypeRoleAccess,
            GetDirectEntityMember: GetDirectEntityMember,
            GetEntityDetailsByID: GetEntityDetailsByID,
            GetApprovalRoles: GetApprovalRoles,
            IsApprovalReportEnables: IsApprovalReportEnables
        });

        function GetTakUIData(taskid, versionNo) {
            var request = $http({
                method: "get",
                url: "api/task/GetTakUIData/" + taskid + "/" + versionNo,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskDetails(obj) {
            var request = $http({
                method: "post",
                ignoreLoadingBar: true,
                url: "api/task/GetTaskDetails/",
                data: obj,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateNewVersion(jobj) {
            var request = $http({
                method: "post",
                url: "api/task/CreateNewVersion/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ReorderOverviewStructure(jsonparameter) {
            var request = $http({
                method: "post",
                url: "api/task/ReorderOverviewStructure",
                params: {
                    action: "add"
                },
                data: jsonparameter
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatetaskEntityTaskDetailsInTaskTab(formobj) {
            var request = $http({
                method: "post",
                url: "api/task/UpdatetaskEntityTaskDetailsInTaskTab/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatetaskEntityTaskDueDate(formobj) {
            var request = $http({
                method: "post",
                url: "api/task/UpdatetaskEntityTaskDueDate/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateTaskStatusInTaskTab(dataobj) {
            var request = $http({
                method: "post",
                url: "api/task/UpdateTaskStatus",
                params: {
                    action: "add"
                },
                data: dataobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function AddNewPhase(obj) {
            var request = $http({
                method: "post",
                url: "api/task/AddNewPhase/",
                data: obj,
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AddPhaseStep(obj) {
            var request = $http({
                method: "post",
                url: "api/task/AddPhaseStep/",
                data: obj,
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function RemovePhase(phaseID, EntityID) {
            var request = $http({
                method: "delete",
                url: "api/task/RemovePhase/" + phaseID + "/" + EntityID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeletePhaseStep(stepID, EntityID) {
            var request = $http({
                method: "delete",
                url: "api/task/DeletePhaseStep/" + stepID + "/" + EntityID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getAllMemberIDbyStepID(stepID) {
            var request = $http({
                method: "get",
                url: "api/task/getAllMemberIDbyStepID/" + stepID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function sendReminderNotificationforTaskPhase(jObj) {
            var request = $http({
                method: "post",
                url: "api/task/sendReminderNotificationforTaskPhase/",
                params: {
                    action: "post"
                },
                data: jObj
            });
            return (request.then(handleSuccess, handleError));
        }

        function RemovePhaseStepApprovar(jobj) {
            var request = $http({
                method: "post",
                url: "api/task/RemovePhaseStepApprovar/",
                data: jobj,
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteTaskMemberById(ID, TaskID) {
            var request = $http({
                method: "delete",
                url: "api/task/DeleteTaskMemberById/" + ID + "/" + TaskID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function getTaskApprovalRolesUsers(jsonparameter) {
            var request = $http({
                method: "post",
                url: "api/task/getTaskApprovalRolesUsers",
                params: {
                    action: "add"
                },
                data: jsonparameter
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertTaskMembers(formobj) {
            var request = $http({
                method: "post",
                url: "api/task/InsertTaskMembers/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateEntityTaskCheckList(formobj) {
            var request = $http({
                method: "post",
                url: "api/task/InsertUpdateEntityTaskCheckList/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DeleteEntityCheckListByID(ID) {
            var request = $http({
                method: "delete",
                url: "api/task/DeleteEntityCheckListByID/" + ID,
                params: {
                    action: "delete"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAccess(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDirectEntityMember(EntityId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetDirectEntityMember/" + EntityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityDetailsByID(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetEntityDetailsByID/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetApprovalRoles() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetApprovalRoles/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function IsApprovalReportEnables() {
            var request = $http({
                method: "get",
                url: "api/Task/IsApprovalReportEnables",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("TaskEditService", ['$http', '$q', TaskEditService]);
})(angular, app);