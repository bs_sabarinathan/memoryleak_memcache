﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Amazon.S3.Model;
using System.Net;
using System.Xml.Linq;
using System.IO;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using Amazon;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core;
namespace Presentation
{
    /// <summary>
    /// Summary description for UserImage
    /// </summary>
    public class UserImage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {

                int tenantID;
                tenantID = GetTenantID(context.Request.Url.Host.ToString());
                IMarcomManager marcomManager;
                marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(tenantID));
                string id = context.Request.QueryString["id"];
                string Preview = context.Request.QueryString["preview"];
                int userid;
                bool result = Int32.TryParse(id, out userid);
                if (result)
                {
                    string userimagepath="UserImages/" + id + ".jpg";
                    string usersmallimagepath="UserImages/Preview/Small_" + id + ".jpg";
                    string userBig_imagepath="UserImages/Preview/Big_" + id + ".jpg";
                    string fullpath = "";
                   
                    if (Preview == "2")
                        fullpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(context.Request.Url.Host.ToString()), "UserImages/Preview/Big_" + id + ".jpg");// context.Server.MapPath("UserImages/" + id + ".jpg");
                    else
                        fullpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(context.Request.Url.Host.ToString()), "UserImages/Preview/Small_" + id + ".jpg");// context.Server.MapPath("UserImages/" + id + ".jpg");

                    if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Local)
                    {
                        if(!System.IO.File.Exists(fullpath))
                        {
                            if (Preview == "2")
                                fullpath = fullpath.Replace(userBig_imagepath.ToString(), userimagepath.ToString());
                            else
                                fullpath = fullpath.Replace(usersmallimagepath.ToString(), userimagepath.ToString());
                        }
                        context.Response.ContentType = "image/jpg";
                        context.Response.WriteFile(fullpath);
                        context.Response.End();
                    }
                    else if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        string amazondownloadfilepath = fullpath.Replace(ConfigurationManager.AppSettings["MarcomPresentation"], marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/").Replace("\\", "/");
                        //Create a stream for the file
                        string keyname = amazondownloadfilepath;
                        int index1 = keyname.IndexOf("Tenants");

                        if (index1 != -1)
                        {
                            keyname = keyname.Substring(index1);

                            keyname = keyname.Replace("\\", "/");
                        }

                       
                            if (AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, keyname))
                            {
                                string iskeyname = amazondownloadfilepath;
                            }
                            else
                            { if (Preview == "2")
                                amazondownloadfilepath = amazondownloadfilepath.Replace(userBig_imagepath.ToString(), userimagepath.ToString());
                              else
                                amazondownloadfilepath = amazondownloadfilepath.Replace(usersmallimagepath.ToString(), userimagepath.ToString());

                            }
                        
                        Stream stream = null;

                        try
                        {


                            //This controls how many bytes to read at a time and send to the client
                            int bytesToRead = 10000;

                            // Buffer to read bytes in chunk size specified above
                            byte[] buffer = new Byte[bytesToRead];
                            HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(amazondownloadfilepath);

                            //Create a response for this request
                            HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                            if (fileReq.ContentLength > 0)
                                fileResp.ContentLength = fileReq.ContentLength;

                            //Get the Stream returned from the response
                            stream = fileResp.GetResponseStream();
                            var resp = HttpContext.Current.Response;
                            //Indicate the type of data being sent
                            resp.ContentType = "image/jpg";
                            int length;
                            do
                            {
                                // Verify that the client is connected.
                                if (resp.IsClientConnected)
                                {
                                    // Read data into the buffer.
                                    length = stream.Read(buffer, 0, bytesToRead);

                                    // and write it out to the response's output stream
                                    resp.OutputStream.Write(buffer, 0, length);

                                    // Flush the data
                                    resp.Flush();

                                    //Clear the buffer
                                    buffer = new Byte[bytesToRead];
                                }
                                else
                                {
                                    // cancel the download if client has disconnected
                                    length = -1;
                                }
                            } while (length > 0); //Repeat until no data is read
                        }
                        catch (Exception e)
                        {
                            //Console.WriteLine("Error reading from {0}. Message = {1}", path, e.Message);
                            string fullpath1 = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(context.Request.Url.Host.ToString()), "UserImages/noimage.jpg");// context.Server.MapPath("UserImages/" + id + ".jpg");
                            context.Response.ContentType = "image/jpg";
                            context.Response.WriteFile(fullpath1);
                            context.Response.End();
                        }

                        finally
                        {
                            if (stream != null)
                            {
                                //Close the input stream
                                stream.Close();
                            }
                        }



                    }
                }
            }
            catch
            {
                string fullpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(context.Request.Url.Host.ToString()), "UserImages/noimage.jpg");// context.Server.MapPath("UserImages/" + id + ".jpg");
                context.Response.ContentType = "image/jpg";
                context.Response.WriteFile(fullpath);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public string GetTenantFilePath(string TenantHost)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantFilePathByHostName(TenantHost);
        }
        public int GetTenantID(string TenantHost)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantIDByHostName(TenantHost);
        }
        
    }
}