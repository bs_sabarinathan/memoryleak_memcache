using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// CalenderTab object for table 'PM_CalenderTab'.
	/// </summary>
	
	internal class CalenderTab : ICalenderTab
	{
		#region Member Variables

        protected int _id;
		protected int _calenderid;
		protected int _customtabid;
		#endregion
		
		#region Constructors
		public CalenderTab() {}

        public CalenderTab(int pId, int pCalenderid, int pCustomTabid)
		{
            this._id = pId;
            this._calenderid = pCalenderid; 
			this._customtabid = pCustomTabid; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int Calenderid
		{
			get { return _calenderid; }
            set { _calenderid = value; }
			
		}
		
		public int CustomTabid
		{
			get { return _customtabid; }
            set { _customtabid = value; }
			
		}
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			CalenderTab castObj = null;
			try
			{
                castObj = (CalenderTab)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _calenderid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
