﻿(function (ng, app) {
    function CmsoverviewService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetCmsEntityPublishVersion: GetCmsEntityPublishVersion,
            UpdateCmsEntityDetailsBlockValues: UpdateCmsEntityDetailsBlockValues,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            FetchEntityStatusTree: FetchEntityStatusTree,
            SaveDetailBlockForLevels: SaveDetailBlockForLevels,
            GetAttributeTreeNodeByEntityID: GetAttributeTreeNodeByEntityID,
            UpdateDropDownTreePricing: UpdateDropDownTreePricing,
            GetDropDownTreePricingObjectFromParentDetail: GetDropDownTreePricingObjectFromParentDetail,
            GetDropDownTreePricingObject: GetDropDownTreePricingObject,
            GetTreeNode: GetTreeNode,
            updateOverviewStatus: updateOverviewStatus,
            UpdateEntityStatus: UpdateEntityStatus,
            GetMember: GetMember,
            GetEntityRelatedDataOnLoad: GetEntityRelatedDataOnLoad,
            GetEntityRelatedDataOnLoadSet1: GetEntityRelatedDataOnLoadSet1,
            GetEntityRelatedDataOnLoad_Set2: GetEntityRelatedDataOnLoad_Set2,
            UpdateEntityActiveStatus: UpdateEntityActiveStatus,
            GetMilestoneByEntityID: GetMilestoneByEntityID,
            DeleteEntityPeriod: DeleteEntityPeriod,
            InsertEntityPeriod: InsertEntityPeriod,
            GetEntitiPeriodByIdForGantt: GetEntitiPeriodByIdForGantt,
            PostEntityPeriod1: PostEntityPeriod1,
            UpdateImageName: UpdateImageName,
            CreateMilestone: CreateMilestone,
            GetMilestoneMetadata: GetMilestoneMetadata,
            UpdateMilestone: UpdateMilestone,
            UpdatingMilestoneStatus: UpdatingMilestoneStatus,
            SaveUploaderImage: SaveUploaderImage,
            GetCurrencyListFFsettings: GetCurrencyListFFsettings,
            GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById,
            GetFeedFilter: GetFeedFilter,
            GetEnityFeeds: GetEnityFeeds,
            GetLastEntityFeeds: GetLastEntityFeeds,
            IsActiveEntity: IsActiveEntity,
            InsertFeedComment: InsertFeedComment,
            IsAvailableAsset: IsAvailableAsset,
            PostFeed: PostFeed,
            UpdateOverviewEntityTaskList: UpdateOverviewEntityTaskList,
            copyuploadedImage:copyuploadedImage
        });
        function GetCmsEntityPublishVersion(CmsEntityID) { var request = $http({ method: "get", url: "api/cms/GetCmsEntityPublishVersion/" + CmsEntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateCmsEntityDetailsBlockValues(cmsentityattrvalue) { var request = $http({ method: "post", url: "api/cms/UpdateCmsEntityDetailsBlockValues/", params: { action: "add" }, data: cmsentityattrvalue }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function FetchEntityStatusTree(EntityID) { var request = $http({ method: "get", url: "api/Metadata/FetchEntityStatusTree/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SaveDetailBlockForLevels(jobj) { var request = $http({ method: "post", url: "api/Metadata/SaveDetailBlockForLevels/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
        function GetAttributeTreeNodeByEntityID(AttributeID, EntityID) { var request = $http({ method: "get", url: "api/Metadata/GetAttributeTreeNodeByEntityID/" + AttributeID + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateDropDownTreePricing(formobj) { var request = $http({ method: "post", url: "api/Metadata/UpdateDropDownTreePricing/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) { var request = $http({ method: "get", url: "api/Metadata/GetDropDownTreePricingObjectFromParentDetail/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) { var request = $http({ method: "get", url: "api/Metadata/GetDropDownTreePricingObject/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetTreeNode(AttributeID) { var request = $http({ method: "get", url: "api/Metadata/GetTreeNode/" + AttributeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function updateOverviewStatus(formobj) { var request = $http({ method: "put", url: "api/Planning/updateOverviewStatus/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityStatus(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityStatus/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function GetMember(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMember/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoad(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Planning/GetEntityRelatedDataOnLoad/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoadSet1(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Planning/GetEntityRelatedDataOnLoadSet1/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityRelatedDataOnLoad_Set2(EntityID, UserID, EntityTypeID, IsAdmin, MilestoneEntityTypeID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Planning/GetEntityRelatedDataOnLoad_Set2/" + EntityID + "/" + UserID + "/" + EntityTypeID + "/" + IsAdmin + "/" + MilestoneEntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityActiveStatus(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityActiveStatus/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function GetMilestoneByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityPeriod(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityPeriod/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function InsertEntityPeriod(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertEntityPeriod/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetEntitiPeriodByIdForGantt(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntitiPeriodByIdForGantt/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function PostEntityPeriod1(formobj) { var request = $http({ method: "post", url: "api/Planning/PostEntityPeriod1/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateImageName(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateImageName/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetMilestoneMetadata(EntityID, EntityTypeId) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneMetadata/" + EntityID + "/" + EntityTypeId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdatingMilestoneStatus(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatingMilestoneStatus/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SaveUploaderImage(formobj) { var request = $http({ method: "post", url: "api/Planning/SaveUploaderImage/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCurrencyListFFsettings() { var request = $http({ method: "get", url: "api/Planning/GetCurrencyListFFsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFeedFilter() { var request = $http({ method: "get", url: "api/common/GetFeedFilter/", params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function GetEnityFeeds(EntityID, pageNo, Feedsgroupid) { var request = $http({ method: "get", url: "api/common/GetEnityFeeds/" + EntityID + "/" + pageNo + "/" + Feedsgroupid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function GetLastEntityFeeds(EntityID, Feedsgroupid) { var request = $http({ method: "get", url: "api/common/GetLastEntityFeeds/" + EntityID + "/" + Feedsgroupid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function InsertFeedComment(addfeedcomment) { var request = $http({ method: "post", url: "api/common/InsertFeedComment/", params: { action: "add", }, data: addfeedcomment }); return (request.then(handleSuccess, handleError)); }
        function IsAvailableAsset(AssetID) { var request = $http({ method: "get", url: "api/common/IsAvailableAsset/" + AssetID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function PostFeed(addnewsfeed) { var request = $http({ method: "post", url: "api/common/PostFeed/", params: { action: "add", }, data: addnewsfeed }); return (request.then(handleSuccess, handleError)); }
        function UpdateOverviewEntityTaskList(ID, OnTimeStatus, OnTimeComment) { var request = $http({ method: "put", url: "api/task/UpdateOverviewEntityTaskList/" + ID + "/" + OnTimeStatus + "/" + OnTimeComment, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function copyuploadedImage(filename) { var request = $http({ method: "post", url: "api/Planning/copyuploadedImage/", params: { action: "save" }, data: { filename: filename } }); return (request.then(handleSuccess, handleError)); }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("CmsoverviewService", ['$http', '$q', CmsoverviewService]);
})(angular, app);