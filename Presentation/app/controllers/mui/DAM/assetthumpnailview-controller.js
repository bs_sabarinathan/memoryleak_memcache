﻿(function (ng, app) {
    "use strict";

    function muiDAMassetthumpnailCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, AssetListService, $compile, $translate, $modal) {
        var localfolderid = 0;
        $scope.userpublishaccess = false;
        $scope.IsPublish = false;

        userpublishaccess($stateParams.ID);
        /******** Current user publishacces ********/
        function userpublishaccess(entityID) {
            AssetListService.userpublishaccess(entityID).then(function (result) {
                $scope.userpublishaccess = result.Response;
                $scope.IsPublish = result.Response;
            });
        }

        if ($scope.selectedTree.id != undefined) {
            if ($scope.selectedTree.id == 0) {
                $('#assetthumbnailview').removeClass('assetthumbnailscroll');
                $('#assetthumbnailview').addClass('assetthumbnailscroll');
            } else {
                $('#assetthumbnailview').removeClass('assetthumbnailscroll');
                localfolderid = $scope.selectedTree.id;
            }
        }
        $scope.$on('LoadThumbnailAssetView', function (event, folderID, EID, view) {            
            var currentpath = $location.path().replace('/', '');
            var test = currentpath;
            test = test.split("/");
            if (test[test.length - 1] == "attachment") {
                if ($scope.SelectAllAssetsObj.checkselectall) {
                    $scope.$emit('clickcheckselectall', 1);
                }
            }
            localfolderid = folderID;
            if (test[test.length - 1] == "task" || folderID == 0) {
                $('#assetthumbnailview').removeClass('assetthumbnailscroll');
                $('#assetthumbnailview').addClass('assetthumbnailscroll');
            } else {
                $('#assetthumbnailview').removeClass('assetthumbnailscroll');

            }
            if ($scope.DAMStatusFilter.length == 0) {
                AssetListService.GetEntityAsset(folderID, EID, view, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                    var res = [];
                    res = data.Response;
                    LoadView(res);
                    loadScrollSettings();
                });
            } else {
                AssetListService.GetStatusFilteredEntityAsset(folderID, EID, view, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, $scope.DAMStatusFilter).then(function (data) {
                    var res = [];
                    res = data.Response;
                    LoadView(res);
                    loadScrollSettings();
                });
            }
        });
        if ($scope.selectedTree.id != undefined) {
            AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                res = data.Response;
                LoadView(res);
                loadScrollSettings();
            });
        }
        $scope.$on('refreshSelection', function (event, arr) {
            for (var i = 0, item; item = arr[i++];) {
                $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item.AssetId] = "th-Selection";
                $scope.AssetListSelection.AssetSelection["asset_" + item.AssetId] = false;
                $scope.AssetListSelection.AssetChkClass["asset_" + item.AssetId + ""] = "checkbox";
            }
        });
        var DynamicData = [];
        $scope.AssetListSelection.AssetSelection = {};
        $scope.AssetListSelection.AssetChkClass = {};
        $scope.AssetListSelectionClass.AssetSelectionClass = {};
        $scope.DownloadFileObj.DownloadFilesArr = [];
        $scope.AssetFilesObj.AssetTaskObj = [];

        function LoadView(res) {
            var html = '';
            if ($scope.AssetFilesObj.PageNo == 1) {
                $("#assetthumbnailview").html($compile(html)($scope));
                $scope.AssetSelection = {};
                $scope.AssetSelectionClass = {};
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetSelection = [];
                $scope.AssetFilesObj.AssetFiles = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                $scope.AssetFilesObj.AssetDynamicData = [];
            }
            if (res != null) {
                loading = false;
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFilesObj.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetFilesObj.AssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    html += '<div class="th-Box" id="divthboxid_' + asset.AssetUniqueID + '">';
                    if (!$scope.SelectAllAssetsObj.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                    html += '<i  id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i>';
                    html += '</div>';
                    html += '<div class="th-Block" >';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '  ng-click=\"Editasset(' + asset.AssetUniqueID + ')\">';
                    var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock" >';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.format) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details" id="thid_' + asset.AssetUniqueID + '">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $("#assetthumbnailview").append($compile(html)($scope));
                if ($('#assetview').css('display') == 'none') {
                    $('#assetview').css('display', 'block');
                }
            } else {
                if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length > 0) {
                    var emptyHtml = "";
                    if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    if ($scope.processingsrcobj.processingplace == "task") emptyHtml = '<div class="emptyFolder">There is no asset.</div>';
                    $("#assetthumbnailview").html(emptyHtml);
                    if ($('#assetview').css('display') == 'none') {
                        $('#assetview').css('display', 'block');
                    }
                } else if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length == 0) {
                    var emptyHtml = "";
                    if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">There is no folder.</div>';
                    $("#assetthumbnailview").html(emptyHtml);
                    if ($('#assetview').css('display') == 'none') {
                        $('#assetview').css('display', 'block');
                    }
                }
            }
        }
        $scope.IsActiveEntity = function (event) {
            var linkdetail = event.target.dataset["linkdetails"];
            var entityid = linkdetail.split("###")[4];
            AssetListService.IsActiveLinkEntity(entityid).then(function (data) {
                var objdetail = {};
                if (data.Response[0].isaccess == true) {
                    DirectOverview(data, entityid);
                }
            });
        }

        function DirectOverview(data, EntityID) {
            var isTask = data.Response[0].isTaskObj;
            var entityid = data.Response[0].ParentID,
             taskid = EntityID,
             TypeId = data.Response[0].typeID,
             ModuleId = data.Response[0].moduleID;
            if (isTask == 1) {
                AssetListService.IsActiveEntity(taskid).then(function (result) {
                    if (result.Response == true) {
                        $("#loadNotificationtask").modal("show");
                        $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, TypeId, entityid]);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            } else {
                AssetListService.IsActiveEntity(EntityID).then(function (result) {
                    if (result.Response == true) {
                        if (ModuleId == 6) {
                            $location.path('#/mui/cms/default/list/' + EntityID);
                        } else {
                            if (TypeId == 5) {
                                $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID);
                            } else if (TypeId == 10) {
                                $location.path('/mui/planningtool/objective/detail/section/' + EntityID);
                            } else {
                                $location.path('/mui/planningtool/default/detail/section/' + EntityID + '/overview');
                            }
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                    }
                });
            }
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            var assetselection = "";           
            assetselection = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkbox.checked == true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkbox.checked;
            var res = [];
            res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid, res[0].Category);
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.AssetFilesObj.AssetSelection.push(assetid);
                }
                if ($scope.AssetFilesObj.AssetSelection.length == $scope.AssetFilesObj.AssetFiles.length) {
                    if ($("#icon_selectassets").hasClass("icon-unchecked")) {
                        $("#icon_selectassets").removeClass("icon-unchecked");
                        $("#icon_selectassets").addClass("icon-check");
                        $scope.selectallassets(0);
                    }
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
            if (!checkbox.checked) {
                $scope.$emit('checkselectall', false);
            }
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if (ischecked == true) {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                    return e.ID == assetid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
            }
        }

        function DownloadAssets(checkbox, assetid, name, ext, guid, category) {
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.push({
                        AssetId: assetid,
                        FileName: name,
                        Fileguid: guid,
                        Extension: ext,
                        Category: category
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.splice($.inArray(remainRecord[0], $scope.DownloadFileObj.DownloadFilesArr), 1);
                }
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetFilesObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (attr.ID == 62 || attr.ID ==63) {
                                if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + dateFormat(data[0]["" + attr.ID + ""], $scope.format) + '</span>';
                            }
                            else
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }
        $scope.Editasset = function (AssetID) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: AssetID,
                            IsLock: $scope.AssetFilesObj.IsLock,
                            isNotify: false,
                            viewtype: 'Thumbnail'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        function getMoreListItems() {
            $scope.AssetFilesObj.PageNo = $scope.AssetFilesObj.PageNo + 1;
            AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                res = data.Response;
                if (res != null) {
                    if (res.length > 0)
                        loading = false;
                }
                LoadView(res);
            });
            var loader = $("#loader");
            loader.text("Loading New Items");
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }
        var loading = false;

        function loadScrollSettings() {

            if (localfolderid != 0) {

                $(window).scroll(function () {

                    if (loading) {
                        return;
                    }

                    if ($(window).scrollTop() > $(document).height() - $(window).height() - 200) {
                        loading = true;

                        checkListItemContents();


                    }

                });

            } else {
                $(".assetthumbnailscroll").bind("scroll", function (e) {

                    if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
                        //if (($(this)[0].scrollHeight - 150) < 2000) {
                        checkListItemContents();

                    }
                });
            }
        }
        var previewTimer = $timeout(function () {
            CheckPreviewGenerator()
        }, 5000);

        function CheckPreviewGenerator() {
            getPreviewIDs();
        }

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                AssetListService.CheckPreviewGeneratorID(assetids).then(function (result) {
                    if (result.Response != null) {
                        var generatedids = result.Response.split(',');
                        for (var j = 0; j < generatedids.length; j++) {
                            if (generatedids[j] != "") {
                                var src = $('#' + generatedids[j]).attr('data-src');
                                var ProcessType = 0;
                                var Srcextn = '.jpg';
                                try {
                                    var upd = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                        return e.ActiveFileID == generatedids[j]
                                    });
                                    if (upd.length > 0) {
                                        ProcessType = upd[0].ProcessType;
                                        Srcextn = upd[0].Extension;
                                        $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                            return e.ActiveFileID == generatedids[j]
                                        })[0].Status = 2;
                                    }
                                    var lightboxid = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    });
                                    if (lightboxid.length > 0) {
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].Status = 2;
                                        $.grep($scope.damlightbox.lightboxselection, function (e) {
                                            return e.AssetId == upd[0].AssetID
                                        })[0].AssetStatus = 2;
                                    }
                                } catch (e) { }
                                var previewimageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath);
                                var extn = '.jpg';
                                if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                    if (GlobalAssetEditID == generatedids[j]) {
                                        $('#asseteditimage').attr('src', previewimageBaseUrlcloud + 'DAMFiles/Preview/Big_' + src + '' + extn + '?' + generateUniqueTracker());
                                    }
                                }
                                $('#' + generatedids[j]).removeClass('loadingImg');
                                if (ProcessType == 1 || ProcessType == 3) $('#' + generatedids[j]).attr('src', previewimageBaseUrlcloud + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
                                else $('#' + generatedids[j]).attr('src', previewimageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + Srcextn.substring(1, Srcextn.length).toUpperCase() + '.jpg?' + generateUniqueTracker());
                            }
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 3000);
        }
        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                AssetListService.GetAttributeDetails(assetid).then(function (attrList) {
                    if (attrList.Response != null) {
                        $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                            return e.ID == attrList.Response.ActiveFileID;
                        })[0];
                        var html = '';
                        html += '<div class="th-AssetInfoTooltip">';
                        html += DrawThumbnailBlock(attrList);
                        html += '</div> ';
                        $('#' + qtip_id).html(html);
                    }
                });
            }
        };

        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ThumbnailToolTipSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.SettingsAttributes["ThumbnailToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var isexist = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (isexist != null) $scope.attributedata.push(isexist);
                    }
                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span></div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datstartval = new Date.create($scope.attributedata[i].Value[j].Startdate);
                                datendval = new Date.create($scope.attributedata[i].Value[j].EndDate);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datstartval = new Date.create($scope.attributedata[i].Value);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            html += '<div class=\"th-AssetInfoRow\"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span> {{fields.DateTime_' + $scope.attributedata[i].ID + '}}</div></div>';
                        } else {
                            html += '<div class=\"th-AssetInfoRow\"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel(previewTimer);
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.$on('UpdatePublishIcon', function (event, fileids) {
            for (var i = 0; i < fileids.length; i++) {
                $('#Publishicon_' + fileids[i]).removeClass('displayNone')
            }
        });
        $scope.$on('Loadupdatedassetinthumnail', function (event, AssetID, FileDetails) {
            var id = "file_' + asset.AssetUniqueID + '"
            var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
            var d = FileDetails;
            $('#' + AssetID).attr('src', imageBaseUrlcloud + 'DAMFiles/Original/' + FileDetails.Fileguid + FileDetails.Extension + '?' + generateUniqueTracker());
            AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                res = data.Response;
                LoadView(res);
                loadScrollSettings();
            });
        });
        $scope.SetAssetActionId = function (assetid) {
            $('#chkBoxIcon_' + assetid + '').addClass('checked');
            var checkboxchecked = true;
            var assetselection = "";
            assetselection = checkboxchecked == true ? "th-Selection selected" : "th-Selection";
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
            $scope.AssetFilesObj.selectedAssetId = assetid;
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                return e == $scope.AssetFilesObj.selectedAssetId;
            });
            if (remainRecord.length == 0) {
                $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
            }
            var remdownloadrecord = [];
            remdownloadrecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                return e.AssetId == assetid;
            });
            if (remdownloadrecord.length == 0) {
                var res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid
                });
                if (res.length > 0) $scope.DownloadFileObj.DownloadFilesArr.push({
                    AssetId: assetid,
                    FileName: res[0].FileName,
                    Fileguid: res[0].FileGuid,
                    Extension: res[0].Extension,
                    Category: res[0].Category
                });
            }
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                        return e.Category == 1 && e.AssetUniqueID == assetid;
                    });
                    if (temparr.length > 0) {
                        $scope.IsPublish = false;
                        $scope.UnPublish = false;
                        $scope.showhidepackage = false;
                        $scope.Isblankasset = false;
                        return true;
                    } else {
                        $scope.showhidepackage = true;
                        $scope.Isblankasset = true;
                    }
                }
            }
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    if ($('#Publishicon_' + assetid)[0].className == "icon-arrow-up") {
                        $scope.IsPublish = false;
                        if ($scope.userpublishaccess == true)
                        $scope.UnPublish = true;                       
                        else
                            $scope.UnPublish = false;
                    } else {                       
                        $scope.UnPublish = false;
                    }
                } else {
                   
                    $scope.UnPublish = true;                  
                    $scope.showhidepackage = true;
                }
            } else {
               
                $scope.UnPublish = true;               
                $scope.showhidepackage = true;
                $scope.Isblankasset = true;
            }
        }

        function parseSize(bytes) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
        $scope.CallBackAssetAction = function (assetid, istask, tasktype) {
            var actionName = assetAction.get(assetid);
            if (actionName != undefined) {
                $scope.$emit('DamAssetAction', actionName, istask, tasktype);
                userpublishaccess($stateParams.ID);
            }
        }
        $scope.$on('loadAssetDetailSection_th', function (event, assetList) {
            AssetListService.GetAssetDetsforSelectedAssets($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false, assetList).then(function (data) {
                var res = [];
                res = data.Response;
                LoadViewDetails(res);
            });
        });

        function LoadViewDetails(res) {
            if (res != null) {
                var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
                var assets = [];
                loading = false;
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    var assetData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    var assettempData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    if (assetData != null) {
                        if (assetData.length == 0) {
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                        } else {
                            $scope.AssetFilesObj.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj.AssetFiles), 1);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            if (assettempData.length > 0) {
                                var Indexofasset = $scope.AssetFilesObj_Temp.AssetFiles.indexOf(assettempData[0]);
                                $scope.AssetFilesObj_Temp.AssetFiles.splice(Indexofasset, 1, assetData[0]);
                            }
                        }
                    } else {
                        $scope.AssetFilesObj.AssetFiles.push(asset);
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    var temp = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                        return e.ID == dyn.ID;
                    });
                    if (temp.length > 0) {
                        $scope.AssetFilesObj.AssetDynamicData.splice($.inArray(temp[0], $scope.AssetFilesObj.AssetDynamicData), 1);
                    }
                    $scope.AssetFilesObj.AssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    var html = '';
                    html += '<div class="th-Box"  id="divthboxid_' + asset.AssetUniqueID + '">';
                    if (!$scope.SelectAllAssetsObj.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                    html += '<i  id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i>';
                    html += '</div>';
                    html += '<div class="th-Block" >';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '  ng-click=\"Editasset(' + asset.AssetUniqueID + ')\">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock" >';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.format) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details" id="thid_' + asset.AssetUniqueID + '">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    $('#divthboxid_' + asset.AssetUniqueID).html($compile('')($scope));
                    $('#divthboxid_' + asset.AssetUniqueID).html($compile(html)($scope));
                }
            }
        }
        $scope.GetAssetDetailsforArrayList = function (arrayList) {
            AssetListService.GetAssetDetsforSelectedAssets(folderID, EID, view, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false, $scope.AssetFilesObj.AssetSelection).then(function (data) {
                var res = [];
                res = data.Response;
                LoadViewDetails(res);
                loadScrollSettings();
            });
        };

        $scope.assetMenuActions = function (action) {
            var AssetObj={};
            AssetObj.IsLock= $scope.AssetFilesObj.IsLock,
            AssetObj.isNotify= false,
            AssetObj.viewtype= 'Thumbnail'
            $scope.$emit('callBacktoDam', action, AssetObj);
        }

        $scope.$on('LoadThumbnailAssetViewWithStatusFilter', function (event, folderID, EID, view, statusfilter) { });
    }
    app.controller("mui.DAM.assetthumpnailCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', 'AssetListService', '$compile', '$translate', '$modal', muiDAMassetthumpnailCtrl]);
})(angular, app);