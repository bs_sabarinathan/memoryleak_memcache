using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IEntityRoleUser interface for table 'AM_Entity_Role_User'.
    /// </summary>
    public interface IGlobalRoleUser
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int GlobalRoleId
        {
            get;
            set;

        }

        int UserId
        {
            get;
            set;

        }

        

        
        

        #endregion
    }
}
