using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// AttributeSetAttributeDao object for table 'MM_AttributeSet_Attribute'.
	/// </summary>
	
	public partial class AttributeSetAttributeDao : BaseDao, ICloneable
	{
		#region Member Variables

		protected int _attributesetid;
		protected int _attributeid;
		protected int? _sortorder;
		protected int? _validationid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        public override bool Equals(object obj)
        {
            AttributeSetAttributeDao toCompare = obj as AttributeSetAttributeDao;
            if (toCompare == null)
            {
                return false;
            }

            if (!Object.Equals(this.AttributeSetid, toCompare.AttributeSetid))
                return false;
            if (!Object.Equals(this.Attributeid, toCompare.Attributeid))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + AttributeSetid.GetHashCode();
            hashCode = (hashCode * 7) + Attributeid.GetHashCode();
            return hashCode;
        }

        #endregion
		
		#region Constructors
		public AttributeSetAttributeDao() {}
			
		public AttributeSetAttributeDao(int pAttributeSetid, int pAttributeid, int? pSortOrder, int? pValidationid)
		{
			this._attributesetid = pAttributeSetid; 
			this._attributeid = pAttributeid; 
			this._sortorder = pSortOrder; 
			this._validationid = pValidationid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int AttributeSetid
		{
			get { return _attributesetid; }
			set { _bIsChanged |= (_attributesetid != value); _attributesetid = value; }
			
		}
		
		public virtual int Attributeid
		{
			get { return _attributeid; }
			set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}
		
		public virtual int? SortOrder
		{
			get { return _sortorder; }
			set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
			
		}
		
		public virtual int? Validationid
		{
			get { return _validationid; }
			set { _bIsChanged |= (_validationid != value); _validationid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string AttributeSetAttributeDao = "AttributeSetAttributeDao";			
			public const string AttributeSetid = "AttributeSetid";			
			public const string Attributeid = "Attributeid";			
			public const string SortOrder = "SortOrder";			
			public const string Validationid = "Validationid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string AttributeSetAttributeDao = "AttributeSetAttributeDao";			
			public const string AttributeSetid = "AttributeSetid";			
			public const string Attributeid = "Attributeid";			
			public const string SortOrder = "SortOrder";			
			public const string Validationid = "Validationid";			
		}

		#endregion
		
		
	}
	
}
