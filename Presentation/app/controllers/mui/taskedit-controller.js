﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.taskeditCntrl",
    function muitaskeditCntrl($scope, $cookies, $resource, $route, $routeParams, requestContext, $compile, $window, $timeout, $location, _, $sce, $translate, PlanningService, TaskService, MetadataService, CommonService, AccessService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.temptaskDiv = "";
        $scope.tempTaskID = 0;
        var fundrequestID = 0;
        $scope.TaskIDforFeed = 0;
        $scope.taskEditDivid = '';
        $scope.updatetaskEdit = true;
        var cancelevent;
        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];

            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $("#taskedit").on('EditTask', function (event, Id, taskTypeId, EntityId) {
            $scope.GetTaskDetailFromService(Id, taskTypeId, EntityId);
        });

        $scope.$on('EditTask', function (event, Id, taskTypeId, EntityId) {
            $scope.IsLockTask = false;
            //var GetLockStatus = $resource('planning/GetLockStatus/:EntityID', { EntityID: EntityId }, { get: { method: 'GET' } });
            //var entitylockstatus = GetLockStatus.get({ EntityID: EntityId }, function () {
            //    $scope.IsLockTask = entitylockstatus.Response.Item2;
            PlanningService.GetLockStatus(EntityId).then(function (res) {
                $scope.IsLockTask = res.Response.m_Item2;
                $scope.processingsrcobj.processinglock = $scope.IsLockTask;
                $scope.GetTaskDetailFromService(Id, taskTypeId, EntityId);
            });
        });

        $scope.GetTaskDetailFromService = function (Id, taskTypeId, EntityId) {
            $routeParams.ID = EntityId;
            $scope.editable = '';
            if (NewsFeedUniqueTimerForTask != undefined)
                $timeout.cancel(NewsFeedUniqueTimerForTask);
            $('#mytaskeditAction').on('hide.bs.modal', function () {
                if (NewsFeedUniqueTimerForTask != undefined)
                    $timeout.cancel(NewsFeedUniqueTimerForTask);
            });
            $("#TaskEditWorkTaskPopup").hide();
            $("#TaskEditApprovalTaskPopup").hide();
            $("#TaskEditReviewTaskPopup").hide();
            var cancelevent;
            $scope.TaskIDforFeed = Id;
            refreshModel();
            //var GetEntityTaskDetails = $resource('task/GetEntityTaskDetails/:EntityTaskID', { EntityTaskID: Id }, { get: { method: 'GET' } });
            //var EntityTaskDetails = GetEntityTaskDetails.get({ EntityTaskID: Id }, function () {
            //    $scope.TaskDetails = EntityTaskDetails.Response;
            TaskService.GetEntityTaskDetails(Id).then(function (res) {
                $scope.TaskDetails = res.Response;
                $scope.EntityMemberList = [];
                //var GetEntityMember = $resource('planning/Member/:EntityID', { EntityID: EntityId }, { get: { method: 'GET' } });
                //var EntityMemberList = GetEntityMember.get({ EntityID: EntityId }, function () {
                //    $scope.EntityMemberList = EntityMemberList.Response;
                PlanningService.GetMember(EntityId).then(function (resp) {
                    $scope.EntityMemberList = resp.Response;
                    BindFundRequestTaskDetails(taskTypeId, $scope.TaskDetails);
                    GetEntityLocationPath(Id);
                    $timeout(function () {
                        LoadMemberRoles();
                    }, 300);
                    if (taskTypeId == 2) {
                        $("#TaskEditApprovalTaskPopup").hide();
                        $("#TaskEditReviewTaskPopup").hide();
                        MaxMinPopulate(taskTypeId);
                        $scope.taskEditDivid = "feeddivforworktaskedit";
                        feedforApprovalTask(parseInt(Id), "feeddivforworktaskedit", false);
                        $("#TaskEditWorkTaskPopup").show();
                        var commentbuttinid = "feedcommentworktaskedit";
                        $('#' + commentbuttinid).empty();
                        var temp = '';
                        if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                            temp = 'feedtextholderworktaskedit';
                            document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                        }
                    }
                    else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36) {
                        $("#TaskEditWorkTaskPopup").hide();
                        $("#TaskEditReviewTaskPopup").hide();
                        MaxMinPopulate(taskTypeId);
                        $scope.taskEditDivid = "feeddivapprovaltaskedit";
                        feedforApprovalTask(parseInt(Id), "feeddivapprovaltaskedit", false);
                        $("#TaskEditApprovalTaskPopup").show();
                        var commentbuttinid = "approvaltaskfeedcommentedit";
                        $('#' + commentbuttinid).empty();
                        var temp = '';
                        if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                            temp = 'feedtextholderapprovalforedit';
                            document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                        }
                    }
                    else if (taskTypeId == 31) {
                        $("#TaskEditWorkTaskPopup").hide();
                        $("#TaskEditApprovalTaskPopup").hide();
                        MaxMinPopulate(taskTypeId);
                        $scope.taskEditDivid = "feeddivforreviewtaskforedit";
                        feedforApprovalTask(parseInt(Id), "feeddivforreviewtaskforedit", false);
                        $("#TaskEditReviewTaskPopup").show();
                        var commentbuttinid = "reviewtaskcommentarea";
                        $('#' + commentbuttinid).empty();
                        var temp = '';
                        if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                            temp = 'textholderreviewtask';
                            document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                        }
                    }
                });

            });
            $timeout(function () {
                $scope.Timerforcallback(parseInt($scope.TaskIDforFeed), $scope.taskEditDivid, true);
            }, 30000);
        }

        $scope.DatepickerShow = function (CurrentDueDate, CurrentId) {
            if (CurrentDueDate.length < 2) {
                $('#' + CurrentId).val('');
                $('#' + CurrentId).attr('AlredyDateSelected', 'true');
            }
            else {
                $('#' + CurrentId).attr('AlredyDateSelected', 'false');
            }
            $timeout(function () { $('[id=' + CurrentId + ']:enabled:visible:first').focus().select() }, 100);
            $timeout(function () {
                $scope.fields["DatePart_Calander_Open" + model] = true;
                //$('#' + CurrentId).datepicker('show');
            }, 100);
        }

        // Validates that the input string is a valid date 
        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };



        $scope.saveEmptyDueDate = function (type) {

            $scope.EntityTaskChangesHolder.DueDate = '';

            //var TaskStatusserv = $resource('task/UpdatetaskEntityTaskDueDate');
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
            // var TaskStatusResult = TaskStatusserv.save(TaskStatusData, function () {
            TaskService.UpdatetaskEntityTaskDueDate(TaskStatusData).then(function (res) {
                $scope.fired = false;
                if (res.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $scope.fired = false;
                }
                else {
                    if (res.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $scope.fired = false;
                    }
                    else {
                        if (type == "unassignedue") {
                            if ($scope.EntityTaskChangesHolder.DueDate != '') {
                                $scope.TaskBriefDetails.dueIn = dateDiffBetweenDates($scope.dueDate);
                                $scope.dueDate = dateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
                                $scope.fired = false;
                            } else {
                                $scope.dueDate = '-';
                                $scope.TaskBriefDetails.dueDate = '-';
                                $scope.TaskBriefDetails.dueIn = 0;
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = '-';
                            $scope.TaskBriefDetails.dueDate = '-';
                            $scope.TaskBriefDetails.dueIn = 0;
                            $scope.fired = false;
                        }
                        RefreshCurrentTask();
                        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                        $scope.fired = false;
                    }
                }
            });

        }

        $scope.IsNotVersioning = true;

        $scope.fired = false;
        $scope.approvechangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    // $('#TaskApproveTaskDatepicker').datepicker("hide");

                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#TaskApproveTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#TaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                                $('#TaskApproveTaskDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#TaskApproveTaskDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('due');
                            $scope.editable = '';
                            $('#TaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                            $('#TaskApproveTaskDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            // $('#TaskApproveTaskDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#TaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                        $('#TaskApproveTaskDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        //$('#TaskApproveTaskDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }



        $('#TaskApproveTaskDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#TaskApproveTaskDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#TaskApproveTaskDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }
                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else 
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');

                    $scope.editable = '';
                    $('#TaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');

                    $('#TaskApproveTaskDatepicker').val('');
                    // $('#TaskApproveTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }
            }
        });

        //-------------Approval End


        //-------------Reviewal start

        $scope.ReviewTaskchangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    //$('#TaskReviewTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#TaskReviewTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#TaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');

                                $('#TaskReviewTaskDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#TaskReviewTaskDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('due');
                            $scope.editable = '';
                            $('#TaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
                            $('#TaskReviewTaskDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            //$('#TaskReviewTaskDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#TaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
                        $('#TaskReviewTaskDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        //$('#TaskReviewTaskDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }

        $('#TaskReviewTaskDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#TaskReviewTaskDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#TaskReviewTaskDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }
                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');

                    $scope.editable = '';
                    $('#TaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');

                    $('#TaskReviewTaskDatepicker').val('');
                    //$('#TaskReviewTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }

            }
        });
        //-------------Approval End


        //-------------Worktask start


        $scope.WorkTaskchangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    //   $('#TaskEditDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#TaskEditDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#TaskEditDatepicker').attr('AlredyDateSelected', 'false');

                                $('#TaskEditDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#TaskEditDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('unassignedue');
                            $scope.editable = '';
                            $('#TaskEditDatepicker').attr('AlredyDateSelected', 'false');
                            $('#TaskEditDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            // $('#TaskEditDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#TaskEditDatepicker').attr('AlredyDateSelected', 'false');
                        $('#TaskEditDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        //$('#TaskEditDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;

                    }
                }, 100);
            }
        }

        $('#TaskEditDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#TaskEditDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#TaskEditDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');
                    $scope.editable = '';
                    $('#TaskEditDatepicker').attr('AlredyDateSelected', 'false');
                    $('#TaskEditDatepicker').val('');
                    // $('#TaskEditDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }
            }
        });

        //-------------Worktask End

        $scope.OwnerName = $cookies['Username'];
        $scope.OwnerID = parseInt($cookies['UserId'], 10);
        $scope.ownerEmail = $cookies['UserEmail'];


        $scope.TaskBriefDetails = {
            taskID: 0,
            taskTypeId: 0,
            taskListUniqueID: 0,
            taskTypeName: "",
            EntityID: 0,
            ownerId: 0,
            taskName: "No Name",
            dueIn: 0,
            dueDate: "",
            strDueDate: "",
            status: "",
            statusID: 0,
            taskOwner: "",
            Description: "",
            Note: "",
            taskmembersList: [],
            taskAttachmentsList: [],
            taskProgressCount: "",
            totalTaskMembers: [],
            TaskMembersRoundTripGroup: [],
            taskCheckList: [],
            WorkTaskInprogressStatus: "",
            LatestMaxRound: 0,
            Totalassetcount: 0,
            Totalassetsize: ""
        };

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskListUniqueID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                Description: "",
                Note: "",
                taskmembersList: [],
                taskAttachmentsList: [],
                taskProgressCount: ""
            };
            $scope.FileList = [];
            $scope.AttachmentFilename = [];
            $scope.SelectedTaskID = 0;
        }

        $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;
        var cssToRemove = "";
        $scope.TaskMinMaxList = [];
        function GetTaskMinMaxValue(tasktypeid) {

            //Get entity members
            //var GetEntityMember = $resource('planning/Member/:EntityID', { EntityID: entityID }, { get: { method: 'GET' } });
            PlanningService.GetTaskMinMaxValue(tasktypeid).then(function (TaskMinMaxListres) {
                // var EntityMemberList = GetEntityMember.get({ EntityID: entityID }, function () {
                $scope.TaskMinMaxList = TaskMinMaxListres.Response;

            });
        }
        //Populate task Breif Details
        function BindFundRequestTaskDetails(taskTypeId, TaskObject) {
            GetTaskMinMaxValue(taskTypeId);
            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.ShowRevokeButton = false;
            $("#TaskEditApprovalTaskPopup,#TaskEditReviewTaskPopup,#TaskEditWorkTaskPopup").removeClass(cssToRemove);
            $("#TaskEditApprovalTaskPopup,#TaskEditReviewTaskPopup,#TaskEditWorkTaskPopup").addClass("TaskPopup " + TaskObject.StatusName.replace(/\s+/g, ""));
            cssToRemove = TaskObject.StatusName.replace(/\s+/g, "");
            $scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
            var taskOwnerObj = $.grep(TaskObject.taskMembers, function (e) { return (e.RoleID == 1); })[0];
            if (TaskObject.taskAssigness != null) {
                var isThisMemberPresent = $.grep(TaskObject.taskAssigness, function (e) { return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1); });
                if (taskTypeId == 2) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 8) {
                        if (isThisMemberPresent.length == 1) {
                            if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
                                if ($scope.IsLockTask != true) {
                                    $scope.ShowCompleteBtn = false;
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowWithdrawBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = true;
                                }
                            }
                            else {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = false;
                            }
                        }
                        else {
                            $scope.ShowCompleteBtn = false;
                            $scope.ShowApproveBtn = false;
                            $scope.ShowRejectedBtn = false;
                            $scope.ShowWithdrawBtn = false;
                            $scope.ShowUnabletoCompleteBtn = false;
                            $scope.ShowRevokeButton = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 8) {

                        if (isThisMemberPresent != null)
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
                                    if ($scope.IsLockTask != true) {
                                        $scope.ShowCompleteBtn = false;
                                        $scope.ShowApproveBtn = false;
                                        $scope.ShowRejectedBtn = false;
                                        $scope.ShowWithdrawBtn = false;
                                        $scope.ShowUnabletoCompleteBtn = false;
                                        $scope.ShowRevokeButton = true;
                                    }
                                }
                                else {
                                    $scope.ShowCompleteBtn = false;
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowWithdrawBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = false;
                                }
                            }
                            else {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = false;
                            }
                    }
                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.editAssetfileTemplateLoading = "worktask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
                else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = true;
                                    $scope.ShowRejectedBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 8) {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                        $scope.ShowRevokeButton = false;
                    }
                    if (TaskObject.TaskStatus == 5 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == 5 && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = true;
                                }
                            }
                        }
                    }

                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.editAssetfileTemplateLoading = "approvaltask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
                else if (taskTypeId == 31) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 8) {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                    }
                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.editAssetfileTemplateLoading = "reveiewtask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
            }
            $scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
            $scope.TaskBriefDetails.taskName = TaskObject.Name;
            $scope.TaskBriefDetails.taskTypeId = taskTypeId;
            $scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
            $scope.TaskBriefDetails.dueDate = '-';
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat); //datstartval.toString("yyyy/MM/dd");
            }
            $scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
            $scope.TaskBriefDetails.taskOwner = taskOwnerObj.UserName;
            $scope.TaskBriefDetails.status = TaskObject.StatusName;
            $scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
            $scope.TaskBriefDetails.Description = '-';
            if (TaskObject.Description.length > 0) {
                $scope.TaskBriefDetails.Description = TaskObject.Description;
            }
            $scope.TaskBriefDetails.Note = '-';
            if (TaskObject.Note != null)
                if (TaskObject.Note.length > 0) {
                    $scope.TaskBriefDetails.Note = TaskObject.Note;
                }
            $scope.TaskBriefDetails.taskID = TaskObject.Id;
            $scope.TaskBriefDetails.ownerId = taskOwnerObj.UserID;
            $scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
            $scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
            if (TaskObject.taskAssigness != null) {
                var unresponsedMembers = $.grep(TaskObject.taskAssigness, function (e) { return (e.ApprovalStatus != null); });
                if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.TaskType != 2) {
                    $scope.TaskBriefDetails.taskProgressCount = "";
                }
                else {
                    if (unresponsedMembers != null)
                        $scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";
                }
            }
            $scope.TaskMemberList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
            $scope.name = $scope.TaskBriefDetails.taskName;
            $scope.DescriptionChange = $scope.TaskBriefDetails.Description;
            if (TaskObject.strDate != "") {
                $scope.dueDate = dateFormat(TaskObject.strDate, GlobalUserDateFormat);
                $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            }
            else {
                $scope.dueDate = "-";
                $scope.TaskBriefDetails.strDueDate = "-";
            }
            $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            if (TaskObject.TaskStatus != 8)
                if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36)
                    $scope.groupByTaskRoundTrip('ApprovalRount');
            if (TaskObject.taskAssigness != null)
                SeperateUsers();
            $scope.fromAssignedpopup = true;
            GetEntityAttributesDetails($scope.TaskBriefDetails.taskID);
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            GetTaskCheckLists();
            ReloadTaskAttachments(TaskObject.Id);
            $scope.processingsrcobj.processingid = $scope.TaskBriefDetails.taskID;
            $scope.$broadcast('ReloadAssetView');
        }

        function GetTaskCheckLists() {
            //var GetTaskDetails = $resource('task/getTaskchecklist/:TaskID', { TaskID: $scope.TaskBriefDetails.taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: $scope.TaskBriefDetails.taskID }, function () {
            TaskService.getTaskchecklist($scope.TaskBriefDetails.taskID).then(function (res) {

                $scope.TaskBriefDetails.taskCheckList = [];
                if (res.Response.length > 0) {
                    $scope.TaskBriefDetails.taskCheckList = res.Response;
                }
                if ($scope.TaskBriefDetails.taskTypeId == 2 && $scope.TaskBriefDetails.statusID == 0 && $scope.TaskBriefDetails.taskCheckList.length > 0) {
                    var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Status == true; });
                    $scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
                }
                if ($scope.TaskBriefDetails.statusID == 2 || $scope.TaskBriefDetails.statusID == 3 || $scope.TaskBriefDetails.statusID == 8) {
                    $scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
                }
                else {
                    if ($scope.TaskBriefDetails.taskCheckList.length > 0) {
                        var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Status == true; });
                        $scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
                    }
                    else {
                        $scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
                    }
                }
            });
        }

        function GetadminTaskCheckLists(taskID) {
            $scope.AdminTaskCheckList = [];
            //var GetTaskDetails = $resource('task/getTaskchecklist/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.getTaskchecklist(taskID).then(function (res) {
                $scope.AdminTaskCheckList = res.Response;
                if ($scope.AdminTaskCheckList.length == 0)
                    $scope.AdminTaskCheckList.push({ ID: 0, NAME: "" });

            });
        }

        function RefreshCurrentTask() {
            $scope.TaskDetails = [];
            //var GetEntityTaskDetails = $resource('task/GetEntityTaskDetails/:EntityTaskID', { EntityTaskID: $scope.TaskBriefDetails.taskID }, { get: { method: 'GET' } });
            //var EntityTaskDetails = GetEntityTaskDetails.get({ EntityTaskID: $scope.TaskBriefDetails.taskID }, function () {
            TaskService.GetEntityTaskDetails($scope.TaskBriefDetails.taskID).then(function (res) {
                $scope.TaskDetails = res.Response;
                if ($scope.TaskDetails != null)
                    if ($scope.TaskDetails.TaskStatus != 0)
                        BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                    else {
                        LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                    }

            });

        }
        function MaxMinPopulate(typeid) {
            $scope.MinValue = parseInt(($scope.TaskMinMaxList[0] == undefined ? 0 : $scope.TaskMinMaxList[0].MinValue));
            $scope.MaxValue = parseInt(($scope.TaskMinMaxList[0] == undefined ? 0 : $scope.TaskMinMaxList[0].MaxValue));
            $scope.TaskDueDateMinValue = new Date.create();
            $scope.TaskDueDateMaxValue = new Date.create();
            if ($scope.MinValue < 0) {
                $scope.TaskDueDateMinValue.setDate($scope.TaskDueDateMinValue.getDate() + ($scope.MinValue + 1));
            }
            else {
                $scope.TaskDueDateMinValue.setDate($scope.TaskDueDateMinValue.getDate() + ($scope.MinValue));
            }
            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                $scope.TaskDueDateMaxValue.setDate($scope.TaskDueDateMaxValue.getDate() + ($scope.MaxValue - 1));
            }
            else {
                $scope.TaskDueDateMaxValue.setDate($scope.TaskDueDateMaxValue.getDate() + 100000);
            }
        }

        //TaskMember roundtrip grouping
        //group the members list on the given property.
        function sortOnMemberRound(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        $scope.groupByTaskRoundTrip = function (attribute) {
            // First, reset the groups.
            $scope.TaskBriefDetails.TaskMembersRoundTripGroup = [];

            // Now, sort the collection of member on the
            // grouping-property. This just makes it easier
            // to split the collection.
            sortOnMemberRound($scope.TaskBriefDetails.totalTaskMembers, attribute);

            // determine which group members are currently in.
            var groupValue = "_INVALID_GROUP_VALUE_";

            // As we loop over each member, add it to the
            // current group - we'll create a NEW group every
            // time we come across a new attribute value.
            for (var i = 0 ; i < $scope.TaskBriefDetails.totalTaskMembers.length ; i++) {
                var friend = $scope.TaskBriefDetails.totalTaskMembers[i];

                //based on global access Conditions 

                // Should we create a new group?
                if (friend[attribute] !== groupValue) {
                    var group = {
                        label: friend[attribute],
                        friends: [],
                        rolename: "Round " + friend.ApprovalRount.toString()
                    };
                    groupValue = group.label;
                    $scope.TaskBriefDetails.TaskMembersRoundTripGroup.push(group);
                }

                // Add the friends to the currently active group
                // grouping.
                group.friends.push(friend);
            }
        };

        $scope.popMe = function (e) {
            var TargetControl = $(e.target);
            var mypage = TargetControl.attr('data-Name');
            if (!(mypage.indexOf("http") == 0)) {
                mypage = "http://" + mypage;
            }
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };

        function SeperateUsers() {
            var UniqueTaskMembers = [];
            var dupes = {};
            if ($scope.TaskMemberList != null) {
                $.each($scope.TaskMemberList, function (i, el) {

                    if (!dupes[el.UserID]) {
                        dupes[el.UserID] = true;
                        UniqueTaskMembers.push(el);
                    }
                });
            }
            var UniqueEntityMembers = [];
            dupes = {};
            $.each($scope.EntityMemberList, function (i, el) {

                if (el.IsInherited != true)
                    if (!dupes[el.Userid]) {
                        dupes[el.Userid] = true;
                        UniqueEntityMembers.push(el);
                    }
            });
            $scope.RemainMembers = [];
            $.each(UniqueEntityMembers, function (key, value) {
                var MemberList = $.grep(UniqueTaskMembers, function (e) { return e.UserID == value.Userid; });
                if (MemberList.length == 0) {

                    $scope.RemainMembers.push(value);

                }
            });
        }

        $scope.addAdditionalMember = function () {
            SeperateUsers();
            $scope.globalEntityMemberLists = [];
            $scope.globalTempcount = 1;
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluser = '';
            $scope.AutoCompleteSelectedObj = [];
            $("#AddTaskMemberModalPopup").modal("show");
        }

        $scope.AddTaskAdditionalMembers = function () {
            if ($('#ngMytaskEditAddTaskMember').hasClass('disabled')) { return; }
            $('#ngMytaskEditAddTaskMember').addClass('disabled');
            var memberList = [];
            memberList = GetUserSelectedAll();
            if (memberList.length > 0 || $scope.globalEntityMemberLists.length > 0) {
                //var InsertTaskMemberServ = $resource('task/InsertTaskMembers/');
                //var insertMemberTask = new InsertTaskMemberServ();
                var insertMemberTask = {};
                insertMemberTask.ParentId = $routeParams.ID;
                insertMemberTask.TaskID = $scope.TaskBriefDetails.taskID;
                insertMemberTask.TaskMembers = memberList;
                insertMemberTask.GlobalTaskMembers = $scope.globalEntityMemberLists;
                // var SaveTaskMemberResult = InsertTaskMemberServ.save(insertMemberTask, function () {
                TaskService.InsertTaskMembers(insertMemberTask).then(function (res) {
                    if (res.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4267.Caption'));
                        $('#ngMytaskEditAddTaskMember').removeClass('disabled');
                    }
                    else {
                        $scope.globalEntityMemberLists = [];
                        var taskMemberObj = res.Response.m_Item2;
                        var taskAssignessObj = $.grep(taskMemberObj, function (e) { return e.RoleID > 1; });
                        $scope.TaskBriefDetails.taskmembersList = taskAssignessObj;
                        $scope.TaskMemberList = taskAssignessObj;
                        SeperateUsers();
                        $('#ngMytaskEditAddTaskMember').removeClass('disabled');
                        $('#MytaskAdditionalTaskMembers > tbody input:checked').each(function () {
                            $(this).next('i').removeClass('checked');
                        });
                        $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                        NotifySuccess($translate.instant('LanguageContents.Res_4108.Caption'));
                        $('#AddTaskMemberModalPopup').modal('hide');
                        RefreshCurrentTask();
                    }
                });
            }
            else {
                $('#ngMytaskEditAddTaskMember').removeClass('disabled');
            }
        }

        function GetUserSelectedAll() {
            var IDList = [];
            $('#MytaskAdditionalTaskMembers  > tbody input:checked').each(function () {
                IDList.push({
                    "Userid": ($(this).attr('data-userid')), "Id": ($(this).attr('data-id')), "TaskID": ($(this).attr('data-taskid')), "Roleid": 4
                , "UserName": ($(this).attr('data-username')), "UserEmail": ($(this).attr('data-UserEmail')), "DepartmentName": ($(this).attr('data-departmentname')),
                    "Role": ($(this).attr('data-role')), "Title": ($(this).attr('data-title'))
                });
            });
            return IDList
        }

        function sortOn(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        function sortOnMemberRound(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        //File attachment Related Blocks
        $scope.AttachmentFilename = [];
        $scope.globalAttachment = true;
        $scope.addAditionalattachments = function () {
            $scope.IsNotVersioning = true;
            $scope.globalAttachment = false;
            $scope.fnTimeOut();
        }

        $scope.AddAttachments = function () {
            $scope.IsNotVersioning = true;
            if ($scope.SelectedTaskID > 0)
                $scope.globalAttachment = false;
            else
                $scope.globalAttachment = true;
            $scope.fnTimeOut();
        }

        $scope.fnTimeOut = function () {
            $('#TaskEditfilelistWorkFlow').empty();
            $('#TaskEditdragfilesAttachment').show();
            $("#TaskEdittotalProgress").empty();
            $("#TaskEdittotalProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>'
                                      + '<div class="progress progress-striped active">'
                                      + '<div style="width: 0%" class="bar"></div>'
                                      + '</div>');
            $scope.StrartUpload();
        }

        $scope.StrartUpload = function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'TaskEditpickfilesAlternate',
                drop_element: 'TaskEditdragfilesAttachment',
                container: 'TaskEditfilescontainer',
                max_file_size: '10000mb',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                chunk_size: '64Kb',
                multipart_params: {}
            });

            uploader.bind('Init', function (up, params) {
                uploader.splice();
            });

            $('#TaskEdituploadfiles').click(function (e) {
                uploader.start();
                e.preventDefault();
            });

            $('#TaskEditclearUploader').click(function (e) {
                uploader.destroy();
                $('.moxie-shim').remove();
            });

            uploader.init();

            $('#TaskEditpickfiles').each(function () {
                var input = new mOxie.FileInput({
                    browse_button: this,
                    multiple: $scope.IsNotVersioning
                });
                input.onchange = function (event) {
                    uploader.addFile(input.files);
                };
                input.init();
            });

            uploader.bind('FilesAdded', function (up, files) {
                if (!$scope.IsNotVersioning) {
                    if (files.length > 1) {
                        uploader.splice(1, files.length - 1);
                        bootbox.alert($translate.instant('LanguageContents.Res_4146.Caption'));
                        return false;
                    }
                }

                $.each(files, function (i, file) {
                    var ste = file.name.split('.')[file.name.split('.').length - 1];
                    var stes = [];
                    stes = [{ ID: file.id, Extension: ste }];
                    if ($scope.listPredfWorkflowFilesAttch == undefined)
                        $scope.listPredfWorkflowFilesAttch = [];

                    $scope.listPredfWorkflowFilesAttch.push({ ID: file.id, Name: file.name, Createdon: new Date.create() });
                    $('#TaskEditfilelistWorkFlow').append(
                        '<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' +
                            '<div class="row-fluid">' +
                            '<div class="span12">' +
                            '<div class="info">' +
                            '<span class="name" >' +
                            '<i class="icon-file-alt"></i>' + file.name +
                            '</span>' +
                            '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' +
                            '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' +
                            '</span>' +
                            '</div>' +
                            '<div class="progress progress-striped active">' +
                            '<div style="width: 0%" class="bar"></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="row-fluid">' +
                            '<div class="span12">' +
                            '<form id="Form_' + file.id + '" class="form-inline">' +
                            '<select>' +
                            '<option>' + Mimer(ste) + '</option>' +
                            '<option>Video</option>' +
                            '<option>Document</option>' +
                            '<option>Zip</option>' +
                            '</select>' +
                            '<input type="text" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Description">' +
                            '</form>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    );

                });
                $('#TaskEditdragfilesAttachment').hide();
                up.refresh(); // Reposition Flash/Silverlight
                TotalUploadProgress();
            });

            $('#TaskEditfilelistWorkFlow').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid');
                $.each(uploader.files, function (i, file) {
                    if (file.id == fileToRemove) {
                        uploader.removeFile(file);
                        $('#' + file.id).remove();
                    }
                });
                if ($('#TaskEditfilelistWorkFlow .attachmentBox').length == 0) {
                    $('#TaskEditdragfilesAttachment').show();
                }
                TotalUploadProgress();
            });

            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " .bar").css("width", file.percent + "%");
                $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                TotalUploadProgress();
            });

            uploader.bind('Error', function (up, err) {
                $('#TaskEditfilelistWorkFlow').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );
                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                $('#' + file.id).attr('data-status', 'true');
                var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                SaveFileDetails(file, response.response, fileDescription);
                $scope.globalAttachment = true;
                TotalUploadProgress();
            });

            uploader.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, { id: file.id, size: file.size });
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                var obj = response;
                $('#' + file.id).attr('data-fileId', response.response);
            });

            function TotalUploadProgress() {
                var TotalSize = 0;
                var TotalCount = $('#TaskEditfilelistWorkFlow .attachmentBox').length;
                var UploadedSize = 0;
                var UploadedCount = 0;
                $('#TaskEditfilelistWorkFlow .attachmentBox').each(function () {
                    TotalSize += parseInt($(this).attr('data-size'));
                    UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#TaskEdittotalProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded');
                $('#TaskEdittotalProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
                $('#TaskEdittotalProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
            }

        }
        $scope.FileWizardHeader = "Change friendly name";

        $scope.AddNewLinktoTask = function () {
            $scope.EditLink = false;
            $scope.SaveLink = true;
            $scope.HidecfnTitle = "Add link";
            $scope.txtLinkName = "";
            $scope.txtLinkDesc = "";
            $scope.txtLinkURL = "";
            $scope.linkType = 1;
            $("#TaskEditaddLinkPopup").modal("show");
        }

        $scope.EditAttachLink = function () {
            if ($scope.txtLinkName != "" && $scope.txtLinkURL != "") {
                //var TaskStatusserv = $resource('task/UpdatetaskLinkDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
                //var TaskStatusData = new TaskStatusserv();
                var TaskStatusData = {};
                TaskStatusData.FileID = $scope.contextFileID;
                TaskStatusData.FileName = $scope.txtLinkName;
                TaskStatusData.Description = "";
                TaskStatusData.URL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                TaskStatusData.LinkType = $scope.linkType;
                //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
                TaskService.UpdatetaskLinkDescription(TaskStatusData).then(function (res) {
                    if (res.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        if (res.Response == false) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            $("#TaskEditaddLinkPopup").modal("hide");
                        }
                        else {
                            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID });
                            if (taskListResObj.length > 0) {
                                var taskAttachmentObj = taskListResObj;
                                if (taskAttachmentObj.length > 0) {
                                    taskAttachmentObj[0].Name = $scope.txtLinkName;
                                    taskAttachmentObj[0].Description = $scope.txtLinkDesc;
                                    taskAttachmentObj[0].LinkURL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                                    taskAttachmentObj[0].LinkType = $scope.linkType;
                                    $scope.EditLink = false;
                                    $scope.SaveLink = true;
                                    $scope.HidecfnTitle = "Add link";
                                    $scope.txtLinkName = "";
                                    $scope.txtLinkDesc = "";
                                    $scope.txtLinkURL = "";
                                    $("#TaskEditaddLinkPopup").modal("hide");
                                }
                            }
                            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                            NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        }
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
            }
        }


        $scope.EditLink = false;
        $scope.SaveLink = true;
        $scope.HidecfnTitle = "Add link";
        $scope.TaskEditfileSaveObject = { "TaskEditFileFriendlyName": "", "TaskEditFileDescription": "" };
        $scope.TaskEditlinkSaveObject = { "TaskEditLinkFriendlyName": "", "TaskEditlinkDescription": "" };
        $scope.ChangeFileFriendlyName = function () {
            $('#TaskEditFileControls tbody tr').remove();
            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
            if (taskListResObj.length > 0) {
                var taskAttachmentObj = taskListResObj;
                if (taskAttachmentObj.length > 0) {
                    if (taskAttachmentObj[0].Extension != "Link") {
                        $scope.FileWizardHeader = "Change friendly name";
                        var htmlStr = '';
                        htmlStr += '<tr> ';
                        htmlStr += ' <td>Name:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <input type="text" id="friendlyName" ng-model="TaskEditfileSaveObject.TaskEditFileFriendlyName"  placeholder="Friendly Name">';
                        htmlStr += ' </td> ';
                        htmlStr += ' <td>Description:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <textarea class="small-textarea" id="FileDescriptionVal" ng-model="TaskEditfileSaveObject.TaskEditFileDescription" placeholder="File Description" rows="3"></textarea>';
                        htmlStr += ' </td> ';
                        htmlStr += '</tr> ';
                        var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
                        $scope.TaskEditfileSaveObject.TaskEditFileFriendlyName = trimFileName;
                        $scope.TaskEditfileSaveObject.TaskEditFileDescription = taskAttachmentObj[0].Description;
                        $scope.TaskEditchangeFriendlynameLink = true;
                        $scope.TaskEditchangeLinkFriendlyName = false;
                        $("#TaskEditFileControls tbody").append($compile(htmlStr)($scope));
                        $("#TaskEditFileUpdateModal").modal("show");

                    }
                    else {

                        $scope.HidecfnTitle = "Edit link";
                        $scope.txtLinkName = taskAttachmentObj[0].Name;
                        $scope.txtLinkDesc = taskAttachmentObj[0].Description;
                        $scope.txtLinkURL = taskAttachmentObj[0].LinkURL.replace('http://', '').replace('https://', '').replace("ftp://", "").replace("file://", "");
                        $scope.linkType = taskAttachmentObj[0].LinkType;
                        $("#TaskEditaddLinkPopup").modal("show");
                        $timeout(function () { $('#TaskEdittxtLinkName').focus().select(); }, 1000);
                        $scope.EditLink = true;
                        $scope.SaveLink = false;
                    }
                }
            }
        }


        $scope.name = "";
        $scope.DescriptionChange = "";
        $scope.dueDate = new Date.create();
        $scope.dueDate = null;
        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };

        $scope.saveEntityTaskDetails = function (type) {
            if (type == "nam") {
                if ($scope.name != "") {

                    if ($scope.TaskBriefDetails.taskName != $scope.name) {
                        $scope.EntityTaskChangesHolder.TaskName = $scope.name;
                        $scope.updatetaskEdit = true;
                    }
                    else
                        return false;
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1876.Caption'));
                    return false;
                }
            }
            else if (type == "des") {
                if ($scope.TaskBriefDetails.Description != $scope.DescriptionChange) {
                    $scope.EntityTaskChangesHolder.TaskDescription = $scope.DescriptionChange;
                    $scope.updatetaskEdit = true;
                }
                else
                    return false;

            }
            else if (type == "note") {
                if ($scope.TaskBriefDetails.Note != $scope.Notechange) {
                    $scope.EntityTaskChangesHolder.Note = $scope.Notechange;
                    $scope.updatetaskEdit = true;
                }
                else
                    return false;
            }
            else if (type == "due") {
                if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
                    $scope.EntityTaskChangesHolder.DueDate = dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat);
                    var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
                    if (dateFormat(tempduedate1) == dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
                        $scope.updatetaskEdit = false;
                    } else { $scope.updatetaskEdit = true; }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1878.Caption'));
                    return false;
                }
            }
            else if (type == "unassignedue") {
                if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
                    $scope.EntityTaskChangesHolder.DueDate = dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat);
                    var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
                    if (dateFormat(tempduedate1) == dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
                        $scope.updatetaskEdit = false;
                    } else { $scope.updatetaskEdit = true; }
                } else {
                    $scope.EntityTaskChangesHolder.DueDate = '';
                }
            }
            //var TaskStatusserv = $resource('task/UpdatetaskEntityTaskDetails');
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.TaskName = $scope.EntityTaskChangesHolder.TaskName;
            TaskStatusData.Description = $scope.EntityTaskChangesHolder.TaskDescription;
            TaskStatusData.Note = $scope.EntityTaskChangesHolder.Note;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
            TaskStatusData.actionfor = type;
            // var TaskStatusResult = TaskStatusserv.save(TaskStatusData, function () {
            TaskService.UpdatetaskEntityTaskDetails(TaskStatusData).then(function (res) {
                if (res.StatusCode == 405) {
                    $scope.fired = false;
                    if ($scope.updatetaskEdit != false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                }
                else {
                    if (res.Response == false) {
                        $scope.fired = false;
                        if ($scope.updatetaskEdit != false) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                    }
                    else {
                        if (type == "unassignedue") {
                            if ($scope.EntityTaskChangesHolder.DueDate != '') {
                                $scope.TaskBriefDetails.dueIn = dateDiff($scope.dueDate);
                                $scope.dueDate = formatteddateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
                                $scope.TaskBriefDetails.dueDate = ConvertDateFromStringToString($scope.EntityTaskChangesHolder.DueDate);
                            } else {
                                $scope.dueDate = '-';
                                $scope.TaskBriefDetails.dueDate = '-';
                                $scope.TaskBriefDetails.dueIn = 0;
                            }
                        }
                        RefreshCurrentTask();
                        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };
                        $scope.TaskBriefDetails.taskName = $scope.name;
                        $scope.TaskBriefDetails.Description = $scope.DescriptionChange;
                        $scope.TaskBriefDetails.Note = $scope.Notechange;
                        $scope.fired = false;
                        if ($scope.updatetaskEdit != false) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        }
                        $("#mytaskeditAction").trigger("onChecklisttaskAction");
                    }
                }
            });
            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
        }

        //To select auto populate the name
        $scope.TaskTitleChange = function (taskname) {
            $scope.name = $('<div />').html(taskname).text();
            $timeout(function () { $("#mytaskWorkTaskName").focus().select() }, 10);
            $timeout(function () { $("#mytaskApprovalTaskName").focus().select() }, 10);
            $timeout(function () { $("#mytaskReviewTaskName").focus().select() }, 10);
        };


        //To AutoSelect the Xeditable selected Value
        $scope.entityeditcontrolclick = function () {
            $timeout(function () { $('[class=input-medium]').focus().select() }, 10);
        };

        $scope.TaskNoteChange1 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytasknotechangework").focus().select() }, 10);
        };
        $scope.TaskNoteChange2 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytasknoteapprovaltask").focus().select() }, 10);
        };
        $scope.TaskNoteChange3 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytasknoteReviewTask").focus().select() }, 10);
        };

        $scope.TaskDescChange1 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytaskWorkTaskDesc").focus().select() }, 10);
        };

        $scope.TaskDescChange2 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytaskApprovalTaskDesc").focus().select() }, 10);
        };

        $scope.TaskDescChange3 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#mytaskreviewTaskDesc").focus().select() }, 10);
        };

        $scope.TaskcheckEditable = function () {
            $timeout(function () { $("#MyTaskworktxtChk_" + index + "").focus().select() }, 10);
        };

        $scope.ChangeFileDescription = function () {
            TaskEditFileUpdateModal
            $('#TaskEditFileControls tbody tr').remove();
            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
            if (taskListResObj.length > 0) {
                var taskAttachmentObj = taskListResObj;
                if (taskAttachmentObj.length > 0) {
                    $scope.FileWizardHeader = "Change file description";
                    var htmlStr = '';
                    htmlStr += '<tr> ';
                    htmlStr += ' <td>Description:</td> ';
                    htmlStr += ' <td> ';
                    htmlStr += '    <textarea class="large-textarea" id="FileDescriptionVal" ng-model="TaskEditfileSaveObject.TaskEditFileDescription" placeholder="File Description" rows="3"></textarea>';
                    htmlStr += ' </td> ';
                    htmlStr += '</tr> ';
                    var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
                    $scope.TaskEditfileSaveObject.TaskEditFileFriendlyName = trimFileName;
                    $scope.TaskEditfileSaveObject.TaskEditFileDescription = taskAttachmentObj[0].Description;
                    $("#TaskEditFileControls tbody").append($compile(htmlStr)($scope));
                    $scope.TaskEditchangeFriendlynameLink = true;
                    $scope.TaskEditchangeLinkFriendlyName = false;
                    $("#TaskEditFileUpdateModal").modal("show");
                }
            }
            $("#TaskEditFileUpdateModal").modal("show");
        }

        // Delete the attachment either it is link or file....
        $scope.DeleteFile = function (e) {

            if ($scope.ContextFileExtention == "Link") {
                bootbox.confirm($translate.instant('LanguageContents.Res_2022.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            //var DeleteAttachment = $resource('task/DeleteTaskLinkByid/:ID/:EntityId', { ID: $scope.contextFileID, EntityId: $scope.TaskBriefDetails.taskID });
                            //var deleteattach = DeleteAttachment.delete(function () {
                            TaskService.DeleteTaskLinkByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function (res) {
                                if (res.Response == true) {
                                    var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                                    $scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
                                    $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));

                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
                                }
                            })
                        }, 100);
                    }
                });
            }
            else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2023.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            //var DeleteAttachment = $resource('task/DeleteTaskFileByid/:ID/:EntityId', { ID: $scope.contextFileID, EntityId: $scope.TaskBriefDetails.taskID });
                            //var deleteattach = DeleteAttachment.delete(function () {
                            TaskService.DeleteTaskFileByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function (res) {
                                if (res.Response == true) {
                                    var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                                    $scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
                                    $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
                                }
                            });
                        }, 100);
                    }
                });
            }
        };

        //Delete Attachment before createtask
        $scope.deleteAttachedFiles = function (fileObject) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2027.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (fileObject.Extension != "link") {
                            var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.FileGuid == fileObject.FileGuid });
                            $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            var fileToDelete = $.grep($scope.FileList, function (e) { return e.FileGuid == fileObject.FileGuid });
                            $scope.FileList.splice($.inArray(fileToDelete[0], $scope.FileList), 1);
                            if (attachmentDelete[0].ID > 0) {

                                //var DeletetaskAttachmentServ = $resource("task/DeleteAttachments/:ActiveFileid", { ActiveFileid: attachmentDelete[0].ID });
                                //var result = DeletetaskAttachmentServ.delete(function () {
                                TaskService.DeleteAttachments(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        } else {
                            if (fileObject.ID == 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.LinkID == fileObject.LinkID });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            } else if (fileObject.ID > 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.ID == fileObject.ID });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                                //var DeletetaskAttachmentServ = $resource("task/DeleteLinkByID/:ID", { ID: attachmentDelete[0].ID });
                                //var result = DeletetaskAttachmentServ.delete(function () {
                                TaskService.DeleteLinkByID(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        }
                    }, 100);
                }
            });
        }

        $scope.GetAttachmentResponse = [];
        $scope.openattachment = function () {
            //var GetAttachmentList = $resource('common/GetFilesandLinksByEntityID/:EntityID', { EntityID: $scope.TaskBriefDetails.EntityID });
            //var getattachments = GetAttachmentList.get(function () {
            CommonService.GetFilesandLinksByEntityID($scope.TaskBriefDetails.EntityID).then(function (getattachments) {
                $scope.getattachmentslist = getattachments.Response;
                if ($scope.getattachmentslist.length != 0) {

                    $scope.GetAttachmentResponse = $scope.getattachmentslist;
                    $scope.attachmentsTaskeditTableHeader = true;
                    $scope.attachmentsTaskeditTableData = true;
                    $scope.noattachments = '';
                }
                else {
                    $scope.attachmentsTaskeditTableHeader = false;
                    $scope.attachmentsTaskeditTableData = false;
                    $scope.noattachments = 'No attachment present';
                }
            });
        }


        $scope.LinkSelected = [];
        $scope.FileSelected = [];
        $scope.attachmentid = function (e) {
            var check = e.target.checked;
            var extn = e.target.attributes[1].nodeValue;
            var checkedid = e.target.attributes[2].nodeValue;
            if (check == true) {//check true
                if (extn == "Link") {
                    $scope.LinkSelected.push(checkedid);
                }
                else {
                    $scope.FileSelected.push(checkedid);
                }
            }
            else {
                if (extn == "Link") {
                    $scope.LinkSelected = jQuery.grep($scope.LinkSelected, function (value) {
                        return value != checkedid;
                    });
                }
                else {
                    $scope.FileSelected = jQuery.grep($scope.FileSelected, function (value) {
                        return value != checkedid;
                    });
                }
            }
        }


        $scope.CheckListSelection = function (status) {
            var baseClass = "checkbox";
            if (status)
                baseClass += " checked";
            return baseClass;
        }


        $scope.contextCheckListID = 0;
        $scope.contextCheckListIndex = 0;
        $scope.SetcontextCheckListID = function (id, indexval) {
            $scope.contextCheckListID = id;
            $scope.contextCheckListIndex = indexval;
        };

        $scope.DeleteEntityChecklists = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2027.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if ($scope.contextCheckListID != 0) {
                            //var DeleteChkList = $resource("task/DeleteEntityCheckListByID/:ID", { ID: $scope.contextCheckListID });
                            //var result = DeleteChkList.delete(function () {
                            TaskService.DeleteEntityCheckListByID($scope.contextCheckListID).then(function (result) {
                                if (result.StatusCode == 200) {

                                    var fileObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Id == $scope.contextCheckListID; });
                                    $scope.TaskBriefDetails.taskCheckList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskCheckList), 1);
                                    RefreshCurrentTask();
                                }
                            });
                        } else {
                            $scope.TaskBriefDetails.taskCheckList.splice($scope.contextCheckListIndex, 1);
                        }
                    }, 100);

                }
            });
            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
        }

        $scope.SaveTaskCheckList = function (Index, CheckListID, ChklistName, ChkListStatus, CheCklistExisting) {
            var checkbox;
            if (ChkListStatus.target != undefined) {
                checkbox = ChkListStatus.target.checked;
            }
            else {
                checkbox = ChkListStatus;
            }
            if (ChklistName != '-' && ChklistName.length > 0) {
                //var SaveTaskData = $resource('task/InsertUpdateEntityTaskCheckList/');
                //var SaveTask = new SaveTaskData();
                var SaveTask = {};
                SaveTask.Id = CheckListID;
                SaveTask.taskId = $scope.TaskBriefDetails.taskID;
                SaveTask.ChkListStatus = checkbox;
                SaveTask.ISowner = (CheckListID > 0 ? false : true);
                SaveTask.SortOrder = (Index + 1);
                SaveTask.CheckListName = ChklistName;
                SaveTask.isnew = CheCklistExisting == undefined ? false : true;
                //var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                TaskService.InsertUpdateEntityTaskCheckList(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4263.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4157.Caption'))
                        var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                            if (i == Index) {
                                return $scope.TaskBriefDetails.taskCheckList[i];
                            }
                        });
                        Object[0].Id = SaveTaskResult.Response;
                        Object[0].Status = checkbox;
                        Object[0].IsExisting = true;
                        RefreshCurrentTask();
                        $("#mytaskeditAction").trigger("onChecklisttaskAction");
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1872.Caption'));
                CheCklistExisting = true;
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
        }

        $scope.AddCheckListNext = function () {

            var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                if (n.IsExisting == false) {
                    return $scope.TaskBriefDetails.taskCheckList[i];
                }
            });
            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                } else {
                    Object[0].Name = CurrentCheckListName;
                    Object[0].IsExisting = true;
                }
            }
            if ($scope.contextCheckListIndex == -1) {
                $scope.TaskBriefDetails.taskCheckList.push({ Id: 0, Name: '', OwnerId: 0, OwnerName: 0, SortOrder: 1, Status: false, TaskId: $scope.TaskBriefDetails.taskID, UserId: 0, UserName: "", CompletedOnValue: "", CompletedOn: null, IsExisting: false });
            } else {
                $scope.TaskBriefDetails.taskCheckList.splice(parseInt($scope.contextCheckListIndex) + 1, 0, { Id: 0, Name: '', OwnerId: 0, OwnerName: 0, SortOrder: 1, Status: false, TaskId: $scope.TaskBriefDetails.taskID, UserId: 0, UserName: "", CompletedOnValue: "", CompletedOn: null, IsExisting: false });
            }
            $timeout(function () { $('[datafocus-id =0]').focus(); }, 10);
        }


        $scope.SaveAttachfile = function () {
            if ($scope.LinkSelected.length == 0 && $scope.FileSelected.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1873.Caption'));
                return false;
            }
            var items = GetRootLevelSelectedAll();
            var IDList = new Array();
            IDList = GetRootLevelSelectedAll();
            //var TaskStatusserv = $resource('task/CopyAttachmentsfromtask/:EntityID', { EntityID: $scope.TaskBriefDetails.taskID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.EntityID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.linkIDs = $scope.LinkSelected;
            TaskStatusData.ActivFileIDs = $scope.FileSelected;
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {            
            TaskService.CopyAttachmentsfromtask(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $('#TaskEditgeneralattachmentsPopup').modal('hide');
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#TaskEditFileUpdateModal").modal("hide");
                    }
                    else {
                        if (TaskStatusResult.StatusCode == 200) {
                            if (TaskStatusResult.Extension == "Link") {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                $('#TaskEditgeneralattachmentsPopup').modal('hide');
                            }
                            else {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                $('#TaskEditgeneralattachmentsPopup').modal('hide');
                            }
                        }
                    }
                }
                $scope.LinkSelected = [];
                $scope.FileSelected = [];
            });
        };

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#TaskEditListContainer > tbody input:checked').each(function () {
                IDList.push($(this).attr('data-id'));
            });
            return IDList
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }



        $scope.Clear = function () {
        }

        $scope.FileList = [];
        $scope.UniqueLinkID = 0;

        $scope.InsertableFileList = [];
        $scope.InsertableAttachmentFilename = [];

        function SaveFileDetails(file, response, fileDescription) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");

            $scope.FileList.push({
                "Name": file.name, "VersionNo": 1, "MimeType": resultArr[1], "Extension": extension, "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(), "Checksum": "", "ModuleID": 1, "EntityID": $routeParams.ID, "Size": file.size, "FileGuid": resultArr[0], "FileDescription": fileDescription
            });

            $scope.InsertableFileList.push({
                "Name": file.name, "VersionNo": 1, "MimeType": resultArr[1], "Extension": extension, "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(), "Checksum": "", "ModuleID": 1, "EntityID": $routeParams.ID, "Size": file.size, "FileGuid": resultArr[0], "FileDescription": fileDescription
            });


            $scope.AttachmentFilename.push({ "FileName": file.name, "Extension": extension, "Size": parseSize(file.size), "FileGuid": resultArr[0], "FileDescription": fileDescription, "URL": "", "LinkType": "", "ID": 0, "LinkID": 0 });
            $scope.InsertableAttachmentFilename.push({ "FileName": file.name, "Extension": extension, "Size": parseSize(file.size), "FileGuid": resultArr[0], "FileDescription": fileDescription, "ID": 0 });
            if ($scope.IsNotVersioning == true) {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    //var SaveTaskData = $resource('task/InsertEntityTaskAttachments/');
                    //var SaveTask = new SaveTaskData();
                    var SaveTask = {};
                    SaveTask.TaskID = $scope.TaskBriefDetails.taskID;
                    SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                    SaveTask.TaskFiles = $scope.InsertableFileList;
                    // var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                    TaskService.InsertEntityTaskAttachments(SaveTask).then(function (SaveTaskResult) {
                        if (SaveTaskResult.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                            $scope.globalAttachment = true;
                        }
                        else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                            $scope.InsertableFileList = [];
                            $scope.InsertableAttachmentFilename = [];
                            ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                        }
                    });
                }
            }

            else {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (n, i) {
                        return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
                    });
                    if (objfile != null && objfile.length > 0) {
                        //var SaveTaskData = $resource('task/InsertEntityTaskAttachmentsVersion/');
                        //var SaveTask = new SaveTaskData();
                        var SaveTask = {};
                        SaveTask.TaskID = $scope.TaskBriefDetails.taskID;
                        SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                        SaveTask.TaskFiles = $scope.InsertableFileList;
                        SaveTask.VersioningFileId = objfile[0].VersioningFileId;
                        SaveTask.FileId = $scope.FileID;
                        // var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                        TaskService.InsertEntityTaskAttachmentsVersion(SaveTask).then(function (SaveTaskResult) {
                            if (SaveTaskResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                                $scope.globalAttachment = true;
                            }
                            else {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                ReloadTaskVersionDetails();
                                $scope.contextFileID = SaveTaskResult.Response;
                                $scope.FileID = SaveTaskResult.Response;
                                NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                                $scope.InsertableFileList = [];
                                $scope.InsertableAttachmentFilename = [];
                                RefreshCurrentTask();
                            }
                        });
                    }
                }
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
        }

        function ReloadTaskVersionDetails(TaskID) {
            if ($scope.TaskVersionFileList.length > 0) {
                $scope.TaskVersionFileList = [];
                $scope.ViewAllFiles();
            }
        }

        function ReloadTaskAttachments(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityTaskAttachmentFile/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityTaskAttachmentFile(taskID).then(function (TaskDetailList) {
                $scope.attachedFiles = TaskDetailList.Response;
                $scope.TaskBriefDetails.taskAttachmentsList = TaskDetailList.Response;

            });
        }

        $scope.OpenAttachLink = function () {
            $scope.txtLinkName = "";
            $scope.txtLinkURL = "";
            $("#TaskEditaddLinkPopup").modal("show");

        }

        //Update the taskStatus
        $scope.UpdateTaskStatus = function (taskID, StatusID) {
            //var TaskStatusserv = $resource('task/UpdateTaskStatus/:TaskID/:Status', { TaskID: taskID, Status: StatusID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            //var TaskStatusData = {};
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdateTaskStatus(taskID, StatusID).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                }
                else {
                    if (TaskStatusResult.Response.Item1 == false) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1874.Caption'));
                        $('#TaskEditWorkTaskPopup').hide();
                        $('#TaskEditApprovalTaskPopup').hide();
                        $('#TaskEditReviewTaskPopup').hide();
                        $('#TaskEditloadtaskedit').hide();
                    }
                    else {
                        $('#TaskEditWorkTaskPopup').hide();
                        $('#TaskEditApprovalTaskPopup').hide();
                        $('#TaskEditReviewTaskPopup').hide();
                        $('#TaskEditloadtaskedit').hide();
                        RefreshCurrentTask();
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $("#mytaskeditAction").trigger("ontaskAction");
                    }
                }
            });
        }


        $scope.TaskVersionFileList = [];
        $scope.ViewAllFiles = function () {
            $scope.FileID = $scope.contextFileID;
            var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (n, i) {
                return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
            });
            if (objfile != null && objfile.length > 0) {
                $scope.versionFileId = objfile[0].VersioningFileId;
                var taskid = $scope.TaskBriefDetails.taskID;
                //var ViewFiles = $resource('task/ViewAllFilesByEntityID/:TaskID/:VersionFileID', { TaskID: $scope.TaskBriefDetails.taskID, VersionFileID: objfile[0].VersioningFileId }, { get: { method: 'GET' } });
                //var FileResult = ViewFiles.get({ TaskID: $scope.TaskBriefDetails.taskID }, function () {
                TaskService.ViewAllFilesByEntityID($scope.TaskBriefDetails.taskID, objfile[0].VersioningFileId).then(function (FileResult) {
                    if (FileResult.Response != null) {
                        $scope.TaskVersionFileList = FileResult.Response;
                        $scope.SelectedVersionID = $scope.TaskVersionFileList[0].VersionNo;
                    }
                });
            }
        }


        $scope.TaskEditSaveFileObject = function () {
            //var TaskStatusserv = $resource('task/UpdatetaskAttachmentDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.FileID = $scope.contextFileID;
            TaskStatusData.FileName = $scope.TaskEditfileSaveObject.TaskEditFileFriendlyName;
            TaskStatusData.Description = $scope.TaskEditfileSaveObject.TaskEditFileDescription;
            // var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdatetaskAttachmentDescription(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#TaskEditFileUpdateModal").modal("hide");
                    }
                    else {
                        var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                        if (taskListResObj.length > 0) {
                            var taskAttachmentObj = taskListResObj;
                            if (taskAttachmentObj.length > 0) {
                                taskAttachmentObj[0].Name = $scope.TaskEditfileSaveObject.TaskEditFileFriendlyName;
                                taskAttachmentObj[0].Description = $scope.TaskEditfileSaveObject.TaskEditFileDescription;
                                $("#TaskEditFileUpdateModal").modal("hide");
                            }
                        }
                        $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    }
                }
            });

        }


        $scope.taskEditSaveLinkObject = function () {
            //var TaskStatusserv = $resource('task/UpdatetaskLinkDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.FileID = $scope.contextFileID;
            TaskStatusData.FileName = $scope.TaskEditlinkSaveObject.TaskEditlinkFriendlyName;
            TaskStatusData.Description = $scope.TaskEditlinkSaveObject.TaskEditlinkDescription;
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdatetaskLinkDescription(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#TaskEditFileUpdateModal").modal("hide");
                    }
                    else {
                        var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                        if (taskListResObj.length > 0) {
                            var taskAttachmentObj = taskListResObj;
                            if (taskAttachmentObj.length > 0) {
                                taskAttachmentObj[0].Name = $scope.TaskEditlinkSaveObject.TaskEditlinkFriendlyName;
                                taskAttachmentObj[0].Description = $scope.TaskEditlinkSaveObject.TaskEditlinkDescription;
                                $("#TaskEditFileUpdateModal").modal("hide");
                            }
                        }
                        $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);

                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    }
                }
            });

        }
        var CurrentCheckListName = "";
        $scope.EditCheckList = function (Name, Index) {
            var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                if ((i != Index && n.IsExisting == false)) {
                    return $scope.TaskBriefDetails.taskCheckList[i];

                }
            });

            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                }
                else {
                    Object[0].IsExisting = true;
                    Object[0].Name = CurrentCheckListName;
                }
            }

            CurrentCheckListName = Name;
        }

        $scope.CloseCheckListPopUp = function (Index, CheCklistExisting, ChklistName) {
            if ($scope.TaskBriefDetails.taskCheckList[Index].Id == 0) {
                $scope.TaskBriefDetails.taskCheckList.splice(Index, 1);
            } else {
                $scope.TaskBriefDetails.taskCheckList[Index].Name = CurrentCheckListName;
                $scope.TaskBriefDetails.taskCheckList[Index].IsExisting = true;
            }
            $("#NonEditableCL" + Index).show();
            $("#EditableCL" + Index).hide();
        }


        $scope.TaskEditFund_newsfeed_TEMP = [];
        var fundrequestID = 0;
        var divtypeid = '';
        //approval task feed

        $scope.userimgsrc = '';
        $scope.commentuserimgsrc = '';

        //------------> CALL THE TASK FEEDS ON PAGE LOAD <--------------
        function feedforApprovalTask(entityid, divid, isLatestnewsfeed) {
            var tempPageno = 0;
            if (isLatestnewsfeed == false) {
                $('#' + divid).html('');
                PagenoforScroll = 0;
                $scope.temptaskDiv = divid;
                $scope.TaskFeedReloadOnScroll();
                tempPageno = 0;
            }
            else
                tempPageno = -1;

            fundrequestID = entityid;
            divtypeid = divid;
            //  var fundingreqNewsFeed = $resource('common/GetEnityFeedsForFundingReq/:EntityID/:PageNo/:islatestfeed/');

            //---------------------------  Get the task feed ------------------------------------
            try {
                $scope.tempTaskID = entityid;
                //var getFundingreqNewsFeedResult = fundingreqNewsFeed.get({ EntityID: entityid, PageNo: tempPageno, islatestfeed: isLatestnewsfeed }, function () {
                CommonService.GetEnityFeedsForFundingReq(entityid, tempPageno, isLatestnewsfeed).then(function (getFundingreqNewsFeedResult) {
                    var fundingreqfundingreqfeeddivHtml = '';

                    if (getFundingreqNewsFeedResult.Response.length > 0) {
                        if (!isLatestnewsfeed) {
                            $scope.TaskEditFund_newsfeed_TEMP = [];
                        }
                        $scope.TaskEditFund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
                    }
                    for (var i = 0 ; i < getFundingreqNewsFeedResult.Response.length; i++) {
                        var feedcomCount = getFundingreqNewsFeedResult.Response[i].FeedComment != null ? getFundingreqNewsFeedResult.Response[i].FeedComment.length : 0;

                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedText + '</p></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + divid + '"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" id="Comment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                        //fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo" > ' + feedcomCount + ' Comment(s)<i class="icon-caret-down"    data-DynHTML="mytaskCommentshowHTML" data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></i></span></div></div></div>';
                        if (feedcomCount > 1) {
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                        }
                        else {
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                        }
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';

                        if (getFundingreqNewsFeedResult.Response[i].FeedComment != null && getFundingreqNewsFeedResult.Response[i].FeedComment != '') {
                            var j = 0;
                            if (getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                            }
                            else {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }

                            for (var k = 0; k < getFundingreqNewsFeedResult.Response[i].FeedComment.length; k++) {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                if (k == 0) {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                }
                                else {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                }

                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment_' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                            }
                        }
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul/></li>';

                        $('#' + divid + ' [data-id]').each(function () {
                            if (parseInt($(this).attr('data-id')) == getFundingreqNewsFeedResult.Response[i].FeedId) {
                                $(this).remove();
                            }
                        });

                        if (isLatestnewsfeed == false) {
                            $('#' + divid).append(fundingreqfeeddivHtml);
                            $('#' + divid).scrollTop(0);
                        }
                        else {
                            $('#' + divid).prepend(fundingreqfeeddivHtml);
                        }

                    }
                    commentclick(divid);
                    $('#' + divid).on('click', 'a[data-command="openlink"]', function (event) {
                        var TargetControl = $(this);
                        var mypage = TargetControl.attr('data-Name');
                        var myname = TargetControl.attr('data-Name');
                        var w = 1200;
                        var h = 800
                        var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                        var win = window.open(mypage, myname, winprops)
                    });

                });
            }
            catch (e)
            { }
        }
        $scope.Timerforcallback = function (entityid, divid, stat) {
            feedforApprovalTask(parseInt(entityid), divid, true);
            NewsFeedUniqueTimerForTask = $timeout(function () {
                if ($('#' + divid).css('display') != 'none') {
                    $scope.Timerforcallback(parseInt(entityid), divid, true);
                }
            }, 30000);
        }
        //---------------------------  Get the task feed on page scroll------------------------------------
        var PagenoforScroll = 0;

        $scope.TaskFeedReloadOnScroll = function () {
            try {
                $('#' + $scope.temptaskDiv).scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;

                        //var latestNewsFeed = $resource('common/GetEnityFeedsForFundingReq/:EntityID/:PageNo/:islatestfeed/');
                        //var latestNewsFeedResult = latestNewsFeed.get({ EntityID: $scope.tempTaskID, PageNo: PagenoforScroll, islatestfeed: false }, function () {
                        CommonService.GetEnityFeedsForFundingReq($scope.tempTaskID, PagenoforScroll, false).then(function (latestNewsFeedResult) {
                            var fundingreqfundingreqfeeddivHtml = '';
                            if (latestNewsFeedResult.Response.length > 0) {
                                for (var i = 0 ; i < latestNewsFeedResult.Response.length; i++) {
                                    var feedcomCount = latestNewsFeedResult.Response[i].FeedComment != null ? latestNewsFeedResult.Response[i].FeedComment.length : 0;

                                    var fundingreqfeeddivHtml = '';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + latestNewsFeedResult.Response[i].FeedId + '>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + latestNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].UserEmail + '>' + latestNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedText + '</p></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + $scope.temptaskDiv + '"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedId + '" id="Comment_' + latestNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                                    //fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo" > ' + feedcomCount + ' Comment(s)<i class="icon-caret-down"    data-DynHTML="mytaskCommentshowHTML" data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + latestNewsFeedResult.Response[i].FeedId + '" ></i></span></div></div></div>';

                                    if (feedcomCount > 1) {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + latestNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                    }
                                    else {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + latestNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';

                                    if (latestNewsFeedResult.Response[i].FeedComment != null && latestNewsFeedResult.Response[i].FeedComment != '') {
                                        var j = 0;

                                        if (latestNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                        }
                                        else {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        }

                                        for (var k = 0; k < latestNewsFeedResult.Response[i].FeedComment.length; k++) {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                            if (k == 0) {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                            }
                                            else {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                            }

                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment_' + latestNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + latestNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                                        }
                                    }

                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
                                    $('#' + $scope.temptaskDiv).append(fundingreqfeeddivHtml);

                                    var duplicateChk = {};
                                    $('#' + $scope.temptaskDiv + '[data-id]').each(function () {
                                        if (duplicateChk.hasOwnProperty(this.id)) {

                                            $(this).remove();
                                        } else {
                                            duplicateChk[this.id] = 'true';
                                        }
                                    });
                                }
                            }
                            else
                                PagenoforScroll -= 2;

                            commentclick($scope.temptaskDiv);
                        });
                    }
                });
            }
            catch (e)
            { }
        }


        $('#feeddivforworktaskedit').on('click', 'a[data-DynHTML="mytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.TaskEditFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Mytaskfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });

        $('#feeddivapprovaltaskedit').on('click', 'a[data-DynHTML="mytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.TaskEditFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Mytaskfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });


        $('#feeddivforreviewtaskforedit').on('click', 'a[data-DynHTML="mytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.TaskEditFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Mytaskfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });


        $scope.AddApprovalTaskNewsFeedforedit = function (divid) {
            var temp = '';
            if (fundrequestID != 0) {
                if (commentbuttinid == 'feedcommentworktaskedit')
                    temp = 'feedtextholderworktaskedit';
                else if (commentbuttinid == 'approvaltaskfeedcommentedit')
                    temp = 'feedtextholderapprovalforedit';
                else if (commentbuttinid == 'reviewtaskcommentarea')
                    temp = 'textholderreviewtask';
                else
                    temp = 'textholderunassigned';

                if ($('#' + temp).text() != "") {
                    return false;
                }

                //var AddNewsFeed = $resource('common/Feed/');
                //var addnewsfeed = new AddNewsFeed();
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 40;
                addnewsfeed.EntityID = parseInt(fundrequestID);
                addnewsfeed.TypeName = "Tasks";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#feedcommentworktaskedit').text();

                // var savenewsfeed = AddNewsFeed.save(addnewsfeed, function () {
                CommonService.Feed(addnewsfeed).then(function (savenewsfeed) {
                });

                $('#' + commentbuttinid).empty();
                if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                    document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                }
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
        };

        function commentclick(divtypeid) {
            $('#' + divtypeid).on('click', 'a[data-DynHTML="CommentHTML1_' + divtypeid + '"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var fundingreqfeeddivHtml = '';

                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li class="writeNewComment">';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>'; //should always be current userid
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary"  data-dyncommentbtn="ButtonHTML_' + divtypeid + '" >Comment</button></div></li>';

                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul li').children().length > 0)
                    $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(fundingreqfeeddivHtml);
                else
                    $(this).parents('li[data-parent="NewsParent"]').find('ul').html(fundingreqfeeddivHtml);
                $timeout(function () { $('#feedcomment_' + commentuniqueid).html('').focus(); }, 10);
                buttonclick(divtypeid);
            });

        }

        function buttonclick(divtypeid) {
            $('#' + divtypeid).on('click', 'button[data-dyncommentbtn="ButtonHTML_' + divtypeid + '"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    //var AddFeedComment = $resource('common/InsertFeedComment/');
                    //var addfeedcomment = new AddFeedComment();
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' +
                        (month < 10 ? '0' : '') + month + '-' +
                        (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);

                    //var saveusercomment = AddFeedComment.save(addfeedcomment, function () {
                    CommonService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class=\"newsFeed\">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></div></li>';
                        $('#' + divtypeid).find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(fundingreqfeeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#Comment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
        }


        $scope.AddApprovalTaskNewsFeed = function () {
            if (fundrequestID != 0) {
                //var AddNewsFeed = $resource('common/Feed/');
                //var addnewsfeed = new AddNewsFeed();
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 2;
                addnewsfeed.EntityID = parseInt(fundrequestID);
                addnewsfeed.TypeName = "Tasks";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#Financialfundingreqfeedcomment1').text();

                //var savenewsfeed = AddNewsFeed.save(addnewsfeed, function () {
                CommonService.Feed(addnewsfeed).then(function (savenewsfeed) {
                    var fundingreqfeeddivHtml = '';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-ID=' + savenewsfeed.Response + '>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"> <img src= Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId'], 10) + '&time=' + $scope.NewTime + ' alt="Avatar"></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block" data-parent="parentforcomment"><div class="arrow-left"></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"><a href=' + $cookies['UserEmail'] + '>' + $cookies['Username'] + '</a></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">Added new comments <b>' + addnewsfeed.ToValue + '</b></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">Few seconds ago</span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button class="btn btn-small pull-right" data-DynHTML="CommentHTML1">Comment</button></span></div></li>';
                    $('#TaskEditfinicialfundingrequestfeeddiv1').prepend(fundingreqfeeddivHtml);
                });
                $('#Financialfundingreqfeedcomment1').empty();
            }
        };

        function validateURL(textval) {
            var urlregex = new RegExp(
                  "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
            return urlregex.test(textval);
        }

        $scope.TaskEditAddLink = function () {
            if ($scope.txtLinkName != "" && $scope.txtLinkURL != "") {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    //var AttachmentLink = $resource('task/InsertLink');
                    //var AttachLink = new AttachmentLink();
                    var AttachLink = {};
                    AttachLink.EntityID = $scope.TaskBriefDetails.taskID;
                    AttachLink.Name = $scope.txtLinkName;
                    AttachLink.URL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                    AttachLink.LinkType = $scope.linkType;
                    AttachLink.Description = "";
                    AttachLink.ActiveVersionNo = 1;
                    AttachLink.TypeID = 9;
                    AttachLink.CreatedOn = Date.now();
                    AttachLink.OwnerID = $cookies['UserId'];
                    AttachLink.OwnerName = $cookies['Username'];
                    AttachLink.ModuleID = 1;

                    //var AttachResponse = AttachmentLink.save(AttachLink, function () {
                    TaskService.InsertLink(AttachLink).then(function (AttachResponse) {
                        if (AttachResponse.Response != 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4788.Caption'));
                            $scope.txtLinkName = "";
                            $scope.txtLinkURL = "";
                            $scope.txtLinkDesc = "";
                            $scope.linkType = 1;
                            $scope.EditLink = false;
                            $scope.SaveLink = true;
                            $scope.HidecfnTitle = "Add link";
                            $("#TaskEditaddLinkPopup").modal("hide");
                            ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                        }
                    });

                }
                else {
                    var linkName = ($scope.txtLinkName != undefined || $scope.txtLinkName != null) ? $scope.txtLinkName : "";
                    var url = ($scope.txtLinkURL != undefined || $scope.txtLinkURL != null) ? $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '') : "";
                    var linkdesc = ($scope.txtLinkDesc != undefined || $scope.txtLinkDesc != null) ? $scope.txtLinkDesc : "";
                    $scope.AttachmentFilename.push({ "FileName": $scope.txtLinkName, "Extension": "link", "Size": 0, "FileGuid": 0, "FileDescription": linkdesc, "URL": url, "LinkType": $scope.linkType, "ID": 0, "LinkID": ($scope.UniqueLinkID + 1) });
                    $scope.txtLinkName = "";
                    $scope.txtLinkURL = "";
                    $scope.txtLinkDesc = "";
                    $scope.EditLink = false;
                    $scope.SaveLink = true;
                    $scope.HidecfnTitle = "Add link";
                    $("#TaskEditaddLinkPopup").modal("hide");
                }
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
            }
        }




        $scope.TaskBriefDetailDateFormat = function (dateObj) {
            return dateFormat(dateObj, $scope.DefaultSettings.DateFormat);
        }

        //File details mouse over data
        $scope.callcontent = function (file) {

            var msg = "";
            if (file.Extension != "Link") {
                msg += "<b>Description:</b> " + (file.Description != null ? file.Description.toString() : "-") + "<br/><br/>";
                msg += "<b>Uploaded by:</b> " + (file.OwnerName != null ? file.OwnerName.toString() : "-") + "<br/><br/>";
                msg += "<b>File size: </b> " + (file.Size != null ? parseSize(file.Size) : "-") + "<br/><br/>";
                msg += "<b>Type:</b> " + (file.Extension != null ? file.Extension.toString() : "-") + "<br/><br/>";
                return msg;
            }
            else {
                msg += "<b>Url:</b> " + (file.LinkURL != null ? file.LinkURL.toString() : "-") + "<br/><br/>";
                return msg;
            }
        };

        $scope.loadcontextmenu = function (e, fileid) {
            $scope.contextFileID = $(e.target).attr('data-fileid');
            var fileid = fileid;
            var linkextn = $(e.target).attr('data-extension');
            if (linkextn == "Link") {
                $scope.showmanageverson = false;
                $scope.shownewversion = false;
            }
            else {
                $scope.showmanageverson = true;
                $scope.shownewversion = true;
            }
        }


        $scope.taskMenuOptionText = 'Set to "Not Applicable"';

        $scope.OrganizeTaskMenuOption = function (taskStatus) {
            if (taskStatus == 0) {
                $scope.taskMenuOptionText = 'Set to "Not Applicable"';
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {

                } else {
                    this.$apply();
                }
                $('#context2 li#TM3').show();
            }
            else if (taskStatus == 7) {
                $scope.taskMenuOptionText = 'Set to "Unassigned"';
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {

                } else {
                    this.$apply();
                }
                $('#context2 li#TM3').show();
            }
            else {
                $('#context2 li#TM3').hide();
            }
        }

        $scope.ShowHideSettoComplete = function (taskTypeId) {
        }

        //Populate TaskList Data for Update
        $scope.SetContextTaskListID = function (id) {
            $scope.ContextTaskListID = id;
        };
        $scope.SetContextTaskListEntityID = function (id) {
            if (id != 0)
                $scope.ContextTaskEntityID = id;
            else
                $scope.ContextTaskEntityID = $routeParams.ID;
        };

        $scope.SetContextMemberID = function (id) {
            $scope.ContextMemberID = id;
        };
        $scope.SetContextTaskID = function (id) {
            $scope.contextTaskID = id;
        };

        $scope.contextFileID = 0;
        $scope.SetContextFileID = function (fileid) {
            $scope.contextFileID = fileid;
        };

        $scope.DuplicateEntityTaskListWithTasks = function () {
            //var DuplicateTaskListServ = $resource('task/DuplicateEntityTaskList/:TaskListID/:EntityID', { TaskListID: parseInt($scope.ContextTaskListID), EntityID: $scope.ContextTaskEntityID }, { update: { method: 'PUT' } });
            //var DuplicateTaskListData = new DuplicateTaskListServ();
            var DuplicateTaskListData = {};
            DuplicateTaskListData.TaskListID = parseInt($scope.ContextTaskListID);
            DuplicateTaskListData.EntityID = $scope.ContextTaskEntityID;
            //var DuplicateTaskListDataResult = DuplicateTaskListServ.update(DuplicateTaskListData, function () {
            TaskService.DuplicateEntityTaskList(DuplicateTaskListData).then(function (DuplicateTaskListDataResult) {
                if (DuplicateTaskListDataResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4323.Caption'));
                }
                else {
                    if (DuplicateTaskListDataResult.Response != null) {
                        AppendDuplicatedTaskList(DuplicateTaskListDataResult.Response, parseInt($scope.ContextTaskListID), $scope.ContextTaskEntityID);
                        NotifySuccess($translate.instant('LanguageContents.Res_4835.Caption'));
                    }
                    else {
                        NotifyError($translate.instant('LanguageContents.Res_4322.Caption'));
                    }
                }
            });
        };

        $scope.DeleteEntityTaskList = function () {
            // var Deletetaskflag = $resource("task/DeleteTaskEntityListID/:TaskListID/:EntityID", { TaskListID: $scope.ContextTaskListID, EntityID: 0 });
            var tasklistname = "";
            tasklistname = ReturnDeleteTasklistName();
            bootbox.confirm($translate.instant('LanguageContents.Res_2036.Caption') + tasklistname + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        //  var delresult = Deletetaskflag.delete(function () {
                        TaskService.DeleteTaskEntityListID($scope.ContextTaskListID, 0).then(function (delresult) {
                            if (delresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4310.Caption'));
                            } else {
                                if (delresult.Response == false) {

                                    bootbox.alert($translate.instant('LanguageContents.Res_1880.Caption'));
                                } else {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4834.Caption'));
                                    refreshTaskListObjectAfterDelete();
                                }
                            }
                        });
                    }, 100);
                }
            });
        }

        $scope.SaveActiveVersion = function () {
            $scope.IsNotVersioning = false;
            if ($scope.versionFileId != 0) {
                //var SaveVerionNo = $resource('task/UpdateAttachmentVersionNo/:TaskID/:SelectedVersion/:VersioningFileId', { TaskID: $scope.TaskBriefDetails.taskID, SelectedVersion: $scope.SelectedVersionID, VersioningFileId: $scope.versionFileId });
                //var SaveResult = SaveVerionNo.save({ TaskID: $scope.TaskBriefDetails.taskID, SelectedVersion: $scope.SelectedVersionID }, function () {
                var SaveVerionNo = {};
                SaveVerionNo.TaskID = $scope.TaskBriefDetails.taskID;
                SaveVerionNo.SelectedVersion = $scope.SelectedVersionID;
                SaveVerionNo.VersioningFileId = $scope.versionFileId;
                TaskService.UpdateAttachmentVersionNo(SaveVerionNo).then(function () {
                    ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                    $("#TaskEditAttachmentVersion").modal('hide');
                });
            }
        }

        //-------------Unassigned start

        //$('#UnAssignedDatepicker').datepicker().on('hide', function (e) {
        //    if ($('#UnAssignedDatepickere').attr('AlredyDateSelected') == 'true') {
        //        $('#UnAssignedDatepicker').val('');
        //    }
        //});

        //$('#UnAssignedDatepicker').datepicker().on('changeDate', function (ev) {
        //    $timeout(function () {
        //        $('#UnAssignedDatepicker').datepicker("hide");
        //        if (dateDiffBetweenDates($scope.dueDate) >= 0) {
        //            $scope.saveEntityTaskDetails('unassignedue');

        //            $scope.editable = '';
        //            $('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');

        //            $('#UnAssignedDatepicker').val('');
        //        }
        //        else {
        //            bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
        //        }
        //    }, 100);
        //});

        $('#UnAssignedDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#UnAssignedDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#UnAssignedDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('unassignedue');

                    $scope.editable = '';
                    $('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');

                    $('#UnAssignedDatepicker').val('');
                    $('#UnAssignedDatepicker').datepicker("hide");
                }
            }
        });

        //-------------Unassigned End


        //-------------Worktask start


        function dateDiffBetweenDates(dateTo) {

            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();

            var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());

            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
        }

        $scope.FileID = 0;
        $scope.ChangeVersionNo = function () {
            $("#TaskEditAddAttachmentPopup").modal("show");
            $scope.FileID = $scope.contextFileID;
            $scope.IsNotVersioning = false;
            $scope.fnTimeOut();
        }


        $scope.DeleteAttachmentVersionByID = function (ID, Index, ActiveFileVersionID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (ActiveFileVersionID != 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1881.Caption'));
                            return false;
                        }
                        //var DeleteAttachment = $resource('task/DeleteFileByID/:ID', { ID: ID });
                        //var deleteattach = DeleteAttachment.delete(function () {
                        TaskService.DeleteFileByID(ID).then(function (deleteattach) {
                            if (deleteattach.Response == true) {
                                $scope.TaskVersionFileList.splice(Index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }


        $scope.TaskEditDeletableAssigneeName = "";

        $scope.TaskEditSetDeletebleAssigneName = function (Name) {
            $scope.TaskEditDeletableAssigneeName = Name;
        };


        $scope.DeleteTaskMembers = function () {
            var name = $scope.UserName;
            //  var DeletetaskMembersServ = $resource("task/DeleteTaskMemberById/:ID/:TaskID", { ID: $scope.ContextMemberID, TaskID: $scope.TaskBriefDetails.taskID });
            bootbox.confirm($translate.instant('LanguageContents.Res_2043.Caption') + $scope.TaskEditDeletableAssigneeName + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        // var delresult = DeletetaskMembersServ.delete(function () {
                        TaskService.DeleteTaskMemberById($scope.ContextMemberID, $scope.TaskBriefDetails.taskID).then(function (delresult) {
                            if (delresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4308.Caption'));

                            } else {
                                if (delresult.Response == false) {

                                    bootbox.alert($translate.instant('LanguageContents.Res_1875.Caption'));
                                } else {
                                    $timeout(function () { feedforApprovalTask($scope.TaskIDforFeed, $scope.taskEditDivid, true); }, 2000);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4480.Caption'));
                                    $scope.TaskEditDeletableAssigneeName = "";
                                    RefreshCurrentTask();
                                    $("#mytaskeditAction").trigger("onChecklisttaskAction");

                                }
                            }
                        });
                    }, 100);
                }
            });

        }


        $scope.SendReminderNotification = function () {
            //var Sendreminder = $resource("task/SendReminderNotification/:TaskMemberID/:TaskID", { TaskMemberID: $scope.ContextMemberID, TaskID: $scope.TaskBriefDetails.taskID });
            //var getstatus = Sendreminder.save(function () {
            TaskService.SendReminderNotification($scope.ContextMemberID, $scope.TaskBriefDetails.taskID).then(function (getstatus) {
                var sentstatus = getstatus.Response;
            });
        }

        //Update the taskStatus
        $scope.UpdateTaskStatusNotApplicableUnassigned = function () {
            //var TaskStatusserv = $resource('task/UpdatetasktoNotApplicableandUnassigned/:TaskID', { TaskID: $scope.contextTaskID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdatetasktoNotApplicableandUnassigned($scope.contextTaskID).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                }
                else {
                    RefreshUnApplicapleStatusObject(TaskStatusResult.Response.Item2, TaskStatusResult.Response.Item3);
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                }
            });

        }

        function GetEntityLocationPath(EntityIDVal) {
            //var GetPathforbreadcrum = $resource('metadata/GetPath/:EntityID', { EntityID: EntityIDVal });
            //var getbc = GetPathforbreadcrum.get(function () {
            MetadataService.GetPath(EntityIDVal).then(function (getbc) {
                $scope.array = [];
                if (getbc != null && getbc != undefined) {
                    for (var i = 0; i < getbc.Response.length - 1; i++) {
                        $scope.array.push({ "ID": getbc.Response[i].ID, "Name": getbc.Response[i].Name, "UniqueKey": getbc.Response[i].UniqueKey, "ColorCode": getbc.Response[i].ColorCode, "ShortDescription": getbc.Response[i].ShortDescription, "TypeID": getbc.Response[i].TypeID, "mystyle": "0088CC" });
                    }
                }
            });
        }

        function GetentityMembers(entityID) {
            $scope.EntityMemberList = [];
            //-----------------------Get entity members-----------------------
            //var GetEntityMember = $resource('planning/Member/:EntityID', { EntityID: entityID }, { get: { method: 'GET' } });
            //var EntityMemberList = GetEntityMember.get({ EntityID: entityID }, function () {
            PlanningService.GetMember(entityID).then(function (EntityMemberList) {
                $scope.EntityMemberList = EntityMemberList.Response;
                $scope.EntityDistinctMembers = [];
                var dupes = {};
                $.each($scope.EntityMemberList, function (i, el) {
                    if (el.IsInherited != true) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            $scope.EntityDistinctMembers.push(el);
                        }
                    }
                });

            });
        }

        $scope.BreadCrumOverview = function (ID) {
            $("#mytaskeditAction").hide();
            $('#TaskEditWorkTaskPopup').modal('hide');
            $('#TaskEditApprovalTaskPopup').modal('hide');
            $('#TaskEditReviewTaskPopup').modal('hide');
            $("#mytaskeditAction").trigger("onBreadCrumtaskEditAction");
        }


        //Returns Decoded Name
        $scope.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            }
            else {
                return "";
            }
        };


        /*
         Global memebers from the system to add in tasks logic goes here
         Author: Viniston
    */

        $scope.Roles = [];                          // Global member role scope
        $scope.globalEntityMemberLists = [];        // to hold the global entity task members from the system
        $scope.globalTempcount = 1;                 // temporary id count for global members
        $scope.AutoCompleteSelectedObj = [];        // auto complete member data holder
        // to load the entity roles for adding global members from the system
        function LoadMemberRoles() {
            //var GetRole = $resource('access/Role/', { get: { method: 'GET' } }); 
            //var role = GetRole.get(function () {
            AccessService.Role().then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        $scope.addUsers = function () {                 // adding global members into the scope  logic goes here
            var result = $.grep($scope.globalEntityMemberLists, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); });
            var entitymemberresult = $.grep($scope.EntityMemberList, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); });
            if (result.length == 0 && entitymemberresult.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) { return e.Id == $scope.ddltasklobalrole; })[0];
                $scope.globalEntityMemberLists.push({ "TID": $scope.globalTempcount, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": $scope.ddltasklobalrole, "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "CostCentreID": 0 });
                $scope.globalTempcount = $scope.globalTempcount + 1;
                $scope.ddltasklobalrole = '';
                $scope.taskGlobaluser = '';
                $scope.AutoCompleteSelectedObj = [];
            }
            else {
                $scope.ddltasklobalrole = '';
                $scope.taskGlobaluser = '';
                $scope.AutoCompleteSelectedObj = [];
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };

        $scope.deleteGlobalTaskMembers = function (item) {  // deleting global members from the scope  logic goes here
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
                    }, 100);
                }
            });
        };

        $scope.taskUnassignTypeID = 0;
        function LoadUnassignedTaskDetl(taskTypeId, TaskObject) {

            $scope.taskUnassignTypeID = taskTypeId;
            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.ShowRevokeButton = false;

            $("#TaskEditApprovalTaskPopup,#TaskEditReviewTaskPopup,#TaskEditWorkTaskPopup").removeClass(cssToRemove);
            if (TaskObject != null) {
                $("#TaskEditApprovalTaskPopup,#TaskEditReviewTaskPopup,#TaskEditWorkTaskPopup").addClass("TaskPopup " + TaskObject.StatusName.replace(/\s+/g, ""));
                cssToRemove = TaskObject.StatusName.replace(/\s+/g, "");
            }
            $scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
            var isThisMemberPresent = 0;
            if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
                isThisMemberPresent = $.grep(TaskObject.taskAssigness, function (e) { return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1); });
            }
            var taskOwnerObj = null;
            if (TaskObject.taskMembers != null && TaskObject.taskMembers.length > 0) {
                taskOwnerObj = $.grep(TaskObject.taskMembers, function (e) { return (e.RoleID == 1); })[0];
            }

            if (taskOwnerObj != null && taskOwnerObj.length > 0) {

                if (taskTypeId == 2) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {

                        if ($scope.EntityLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }

                    }
                    if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {

                        if ($scope.EntityLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }

                    }
                    if (TaskObject.TaskStatus == 2 && isThisMemberPresent.length >= 1) {

                        if ($scope.EntityLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }

                    }
                }
                else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36) {

                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }

                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.EntityLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = true;
                                    $scope.ShowRejectedBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }

                }
                else if (taskTypeId == 31) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }

                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.EntityLockTask != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }

                }
            }
            $scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
            $scope.TaskBriefDetails.taskName = TaskObject.Name;
            $scope.TaskBriefDetails.taskTypeId = taskTypeId;
            $scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
            $scope.TaskBriefDetails.dueDate = '-';
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat); //datstartval.toString("yyyy/MM/dd");
            }

            $scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
            $scope.TaskBriefDetails.status = TaskObject.StatusName;
            $scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
            $scope.TaskBriefDetails.Description = '-';
            $scope.TaskBriefDetails.Note = '-';
            if (TaskObject.Description.length > 0) {
                $scope.TaskBriefDetails.Description = TaskObject.Description;
            }
            if (TaskObject.Note != null)
                if (TaskObject.Note.length > 0) {
                    $scope.TaskBriefDetails.Note = TaskObject.Note;
                }
            $scope.TaskBriefDetails.taskID = TaskObject.Id;
            $scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
            $scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
            var unresponsedMembers = null;
            if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
                unresponsedMembers = $.grep(TaskObject.taskAssigness, function (e) { return (e.ApprovalStatus != null); });
            }

            if (TaskObject.TaskType == 2 && TaskObject.TaskStatus == 0) {
                $scope.TaskBriefDetails.taskProgressCount = TaskObject.ProgressCount;
            }

            if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.taskTypeId != 2) {
                $scope.TaskBriefDetails.taskProgressCount = "";
            }
            else {
                if (unresponsedMembers != null && unresponsedMembers.length > 0) {
                    $scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";
                }
            }

            $scope.TaskMemberList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
            $scope.name = $scope.TaskBriefDetails.taskName;
            $scope.DescriptionChange = $scope.TaskBriefDetails.Description;
            $scope.dueDate = "-";
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.dueDate = (TaskObject.strDate);
                $scope.TaskBriefDetails.strDueDate = (TaskObject.strDate);
            }
            if ($scope.taskUnassignTypeID == 2) {
                GetTaskCheckLists();
            }
            $scope.fromAssignedpopup = false;
            GetEntityAttributesDetails($scope.TaskBriefDetails.taskID);
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            ReloadTaskAttachments(TaskObject.Id);
        }


        //task dynamic metadata handling works goes here-->START
        $scope.fields = {
            usersID: ''
        };
        $scope.Dropdown = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.fromAssignedpopup = true;
        $scope.treePreviewObj = {};
        $scope.treeNodeSelectedHolder = [];
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.atributesRelationList = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {

        };
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.items = [];
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeSelection = [];
        $scope.normaltreeSources = {};

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == parent.AttributeId && e.id == parent.id; });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }


        };

        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }


        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended

        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                }
                else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                }
                else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                }
                else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }

        }




        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }


        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON PAGE LOAD <-----------------------
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //Validation part
        function GetValidationList(rootID) {
            //var ValidationAttribute = $resource('metadata/GetValidationDationByEntitytype/:EntityTypeID', { EntityTypeID: rootID });
            //var GetValidationresult = ValidationAttribute.get(function () {
            MetadataService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                        $("#TaskDynamicMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#tskbtnTemp',
                            'silentSubmit': 'true'
                        });
                    }


                }
            });
        }

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0)
                    GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = [];

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }

            //----------------> Calling Attribute to Attribute relation on selecting
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);

        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }
        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended
        $scope.LoadDropDownChildLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;

                //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);

                    if (attrType == 6) {
                        $scope.fields["DropDown_" + attrID + "_" + j] = "";
                    }
                    else if (attrType == 12) {
                        if (j == levelcnt)
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                        else
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                    }
                }

                //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------

                if (attrType == 6) {
                    if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
                else if (attrType == 12) {
                    if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }


        function LoadTaskMetadata(EntAttrDet, ID, tasktype, fromassignedplace) {

            //----------------> LOADING DETAILS BLOCK STARTS HERE <---------------------
            $scope.StopeUpdateStatusonPageLoad = false;
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.dyn_Cont = '';
            $scope.dyn_Cont = "";
            $scope.ClearModelObject($scope.fields);
            $scope.attributedata = EntAttrDet;
            for (var i = 0; i < $scope.attributedata.length; i++) {
                if ($scope.attributedata[i].TypeID == 6) {
                    $scope.dyn_Cont2 = '';
                    //construct inline scope objects

                    var CaptionObj = $scope.attributedata[i].Caption.split(",");

                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if (j == 0) {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            }
                            else {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        }
                        else {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            }
                            else {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        }
                    }
                    $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                    $scope.items = [];


                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                        var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockTask == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditableoptimizedtreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  title=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockTask == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 12) {
                    $scope.dyn_Cont2 = '';
                    var CaptionObj = $scope.attributedata[i].Caption;
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if ($scope.attributedata[i].Lable.length == 1) {
                            var k = j;
                            var treeTexts = [];
                            var fields = [];
                            $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                            if (k == CaptionObj.length) {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                            else {
                                if (CaptionObj[k] != undefined) {
                                    for (k; k < CaptionObj.length; k++) {
                                        treeTexts.push(CaptionObj[k]);
                                        fields.push(CaptionObj[k]);
                                    }
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                }
                                else {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }

                        }
                        else {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                }
                                else {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }
                            else {
                                var k = j;
                                if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                    else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                fields.push(CaptionObj[k]);
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                        }
                                        else {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    }
                                    else {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            }
                        }
                    }
                    $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                    $scope.settreelevels();
                    $scope.items = [];


                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;

                        var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockTask == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  title=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockTask == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockTask == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 2) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";

                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockTask == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 11) {  // inline uploader

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                    $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                    $scope.setUploaderCaption();
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                    $scope.dyn_Cont += '<div class=\"controls\">';
                    if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                        $scope.attributedata[i].Value = "NoThumpnail.jpg";
                    }
                    $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                    $scope.dyn_Cont += 'class="entityDetailImgPreview">';
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else {
                        if ($scope.IsLockTask == false) {
                            $scope.dyn_Cont += "<a ng-model='UploadImage' ng-click='UploadImagefile(" + $scope.attributedata[i].ID + ")' href='#UplaodImagediv' data-toggle='modal' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                            $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                            $scope.dyn_Cont += 'title="' + $scope.attributedata[i].Lable + '"';
                            $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                            $scope.dyn_Cont += '</a></div></div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.dyn_Cont += '</div></div>';
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 3) {

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {

                        if ($scope.attributedata[i].Caption[0] != undefined) {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                        else {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                $scope.dyn_Cont += '</div></div>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                    $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                    $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                    $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '"';
                                    $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                    $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                    $scope.dyn_Cont += '</div></div>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                    $scope.dyn_Cont += '</div></div>';
                                }
                            }
                        }

                    } else {

                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption[0].length > 1) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                                else {
                                    if ($scope.IsLockTask == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLockTask == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            }
                        }
                        else {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 4) {

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        if ($scope.attributedata[i].Caption.length > 1) {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    }
                    else {

                        $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                        $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;


                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockTask == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockTask == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 10) {

                    var inlineEditabletitile = $scope.attributedata[i].Caption;

                    var perioddates = [];

                    $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                    if ($scope.attributedata[i].Value == "-") {
                        $scope.IsStartDateEmpty = true;
                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                        $scope.dyn_Cont += '</div>';
                    }
                    else {

                        for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {

                            var datStartUTCval = "";
                            var datstartval = "";
                            var datEndUTCval = "";
                            var datendval = "";

                            datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                            datstartval = new Date.create(parseInt(datStartUTCval));

                            datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                            datendval = new Date.create(parseInt(datEndUTCval));

                            perioddates.push({ ID: $scope.attributedata[i].Value[j].Id, value: datendval });

                            $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(dateFormat(datstartval));
                            $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(dateFormat(datendval));

                            $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(dateFormat(datstartval));
                            $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(dateFormat(datendval));

                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
                            }
                            else {
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
                            }
                            else {
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
                            }

                            if ($scope.attributedata[i].Value[j].Description == undefined) {
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                            } else {
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                            }

                            $('#fsedateid').css("display", "none");
                            $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                            $scope.dyn_Cont += '<div class="inputHolder span11">';

                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {

                                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  title=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  title=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                    $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                }
                            }

                            $scope.dyn_Cont += '</div></div>';

                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            }
                            else {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  title=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                }
                                else if ($scope.IsLockTask == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                }
                            }

                            $scope.dyn_Cont += '</div></div></div>';

                            if (j != 0) {
                                if ($scope.IsLockTask == false) {
                                    $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                }
                            }

                            $scope.dyn_Cont += '</div>';

                            if (j == ($scope.attributedata[i].Value.length - 1)) {

                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                                $scope.dyn_Cont += '</div>';
                            }
                        }
                    }
                    $scope.dyn_Cont += ' </div>';

                    $scope.dyn_Cont += '<div class="control-group nomargin">';
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">Start date / End date</label>';
                        $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  title=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
                        $scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
                    }
                    else {
                        if ($scope.IsLockTask == false) {
                            if ($scope.attributedata[i].Value == "-") {
                                $scope.dyn_Cont += '<label id="fsedateid" class="control-label" for="label">' + inlineEditabletitile + '</label>';
                            }
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  title=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                            $scope.dyn_Cont += '</div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.dyn_Cont += '<span class="controls">[Add ' + inlineEditabletitile + ' ]</span>';
                        }
                    }
                    $scope.dyn_Cont += '</div>';
                }
                else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                    var datStartUTCval = "";
                    var datstartval = "";
                    var inlineEditabletitile = $scope.attributedata[i].Caption;
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    datStartUTCval = $scope.attributedata[i].Value.substr(6, ($scope.attributedata[i].Value.indexOf('+') - 6));
                    datstartval = new Date.create(parseInt(datStartUTCval));
                    $scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(dateFormat(datstartval));
                    $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(dateFormat(datstartval));
                    if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockTask == false) {
                                $scope.MinValue = $scope.attributedata[i].MinValue;
                                $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                }
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  title=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockTask == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    }
                    else {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    }
                }
                else if ($scope.attributedata[i].TypeID == 7) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                    $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                    $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group">';
                    $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    if ($scope.IsLockTask == false) {
                        $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                        if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                            }
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }
                        else {
                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }

                        $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';

                        $scope.dyn_Cont += ' </div>';
                    }
                    else {
                        if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                            }
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }
                        else {
                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }

                        $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                    }
                    $scope.dyn_Cont += '</div></div>';

                }
                else if ($scope.attributedata[i].TypeID == 8) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockTask == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 13) {


                    $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                    $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                    for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {


                        if (price.selection.length > 0) {
                            var selectiontext = "";
                            var valueMatches = [];
                            if (price.selection.length > 0)
                                valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                });
                            if (valueMatches.length > 0) {
                                selectiontext = "";
                                for (var x = 0, val; val = valueMatches[x++];) {
                                    selectiontext += val.caption;
                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                    else selectiontext += "</br>";
                                }
                            }
                            else
                                selectiontext = "-";
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                        }
                        else {
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                        }

                        if ($scope.IsLockTask == false) {

                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                            $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  title=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                        }
                        else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                            $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                        }

                    }
                }
                else if ($scope.attributedata[i].TypeID == 19) {
                    //$scope.isfromtask = 1;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Value != null) {
                            $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
                            //$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html(($scope.attributedata[i].Value.Amount)).text();
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
                            var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
                            $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) { return e.Id == currtypeid; }));
                            $scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
                        } else {
                            $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
                        }
                        $scope.currencytypeslist = $scope.CurrencyFormatsList;
                        // $scope.setFieldKeys();
                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        //$scope.setNormalDropdownCaption();                     
                        if ($scope.IsLockTask == false) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
                        }
                        else if ($scope.IsLockTask == true) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }

            }

            $("#taskeditworkAssetdynamicdetail").unbind(); $("#taskeditworkAssetdynamicdetail").empty(); $("#taskeditworkAssetdynamicdetail").html($compile("")($scope));
            $("#taskeditapprovalworktaskdynamicHolder").unbind(); $("#taskeditapprovalworktaskdynamicHolder").empty(); $("#taskeditapprovalworktaskdynamicHolder").html($compile("")($scope));
            $("#taskeditreviewtaskdynamicholder").unbind(); $("#taskeditreviewtaskdynamicholder").empty(); $("#taskeditreviewtaskdynamicholder").html($compile("")($scope));
            $("#taskeditunassignedtaskdynamicdataholder").unbind(); $("#taskeditunassignedtaskdynamicdataholder").empty(); $("#taskeditunassignedtaskdynamicdataholder").html($compile("")($scope));

            if (fromassignedplace) {
                if (tasktype == 2)
                    $("#taskeditworkAssetdynamicdetail").html(
                                        $compile($scope.dyn_Cont)($scope));
                if (tasktype == 3)
                    $("#taskeditapprovalworktaskdynamicHolder").html(
                                        $compile($scope.dyn_Cont)($scope));
                if (tasktype == 31)
                    $("#taskeditreviewtaskdynamicholder").html(
                                        $compile($scope.dyn_Cont)($scope));
            }
            else
                $("#taskeditunassignedtaskdynamicdataholder").html(
                                        $compile($scope.dyn_Cont)($scope));


            //----------> HIDE THE ATTRIBUTE THAT ALL ARE HAVING ATTRIBUTE RELATIONS <------------
            HideAttributeToAttributeRelationsOnPageLoad();



            //---------> SHOW THE ATTRIBUTE THAT ALL ARE HAVING THE VALUE TO SHOW THE ATTRIBUTE RELATIONS <----------
            for (i = 0; i < $scope.attributedata.length; i++) {
                if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                    if ($scope.attributedata[i].TypeID == 12) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                if (($scope.attributedata[i].Lable.length - 1) == j) {
                                    var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;

                                    for (k; k < $scope.attributedata[i].Value.length; k++) {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                    }
                                }
                                else {
                                    $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 6) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null)
                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                    }
                    else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                    }
                    else {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, $scope.attributedata[i].TypeID);
                    }
                }
            }

            //---------------->  DETAILS BLOCK ENDS HERE <---------------------

        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }


        //task metadata action methods comes here

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        $scope.deletePeriodDate = function (periodid) {
            //var DeletePeriodByID = $resource('planning/DeleteEntityPeriod/:ID', { ID: periodid }, { delete: { method: 'DELETE' } });
            //var deletePerById = DeletePeriodByID.delete({ ID: periodid }, function () {
            PlanningService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {

                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    UpdateEntityPeriodTree($routeParams.ID);
                    $("#dynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');

                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });

                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }

        //save detail block

        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                // var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));

                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 1 || attributetypeid == 2) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                if (attrID == 68) {
                    $(window).trigger("UpdateEntityName", optionarray);
                    $("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
                                if ($scope.TaskBriefDetails.taskID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $scope.TaskBriefDetails.taskID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
                                    if (Splitarr.length == 0) {
                                        Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
                                }
                            } else {
                                break;
                            }

                        }
                    }
                }
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 3 || attributetypeid == 4) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });

                $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 12) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                    //var updateentityattrib = new UpdateEntityAttributes();
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                    MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405)
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                            $scope.fields['SingleLineText_' + attrID] = optionarray == "" ? 0 : optionarray;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        }
                    });
                    $scope.treeSelection = [];
                }
                else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            }
            else if (attributetypeid == 5) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });

                var sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = dateFormat(formatteddateFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                //var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.DefaultSettings.DateFormat);
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 19) {
                $scope.newamount = optionarray[0];
                $scope.newcurrencytypename = optionarray[2];
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    // $scope.fields["SingleLineTextValue_" + attrID] = $scope.newamount.toFixed($scope.DecimalSettings["FormatMoney"]["Financial_FormatMoney"]);
                    $scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
                    $scope.fields["NormalDropDown_" + attrID] = optionarray[1];

                });
                $scope.treeSelection = [];

            }

            $scope.treeSelection = [];

        };



        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                //var InsertNewEntityPeriod = $resource('planning/InsertEntityPeriod/');
                //var newperiod = new InsertNewEntityPeriod();
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));

                //----------> Start date and end date diff checking
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }

                var tempperioddates = [];

                for (var i = 0 ; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }

                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 1) {

                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }

                newperiod.EntityID = entityid;
                newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;

                //var resultperiod = InsertNewEntityPeriod.save(newperiod, function () {
                Planningservice.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        var newid = resultperiod.Response;

                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }

                    perioddates.push({ ID: newid, value: edate });

                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(dateFormat(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(dateFormat(edate))

                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];


                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    }
                    else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("display", "none");
                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';

                    $scope.dyn_Cont += '<div class="inputHolder span11">';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Start / End Date </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  title=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  title=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment Start / End Date </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + ID + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  title=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';

                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }

                    var divcompile = $compile($scope.dyn_Cont)($scope);

                    if ($scope.fromAssignedpopup) {
                        if ($scope.TaskBriefDetails.taskTypeId == 2)
                            $("#taskeditworkAssetdynamicdetail div[data-addperiodid]").append(divcompile);
                        if ($scope.TaskBriefDetails.taskTypeId == 3)
                            $("#taskeditapprovalworktaskdynamicHolder div[data-addperiodid]").append(divcompile);
                        if ($scope.TaskBriefDetails.taskTypeId == 31)
                            $("#taskeditreviewtaskdynamicholder div[data-addperiodid]").append(divcompile);
                    }
                    else
                        $("#taskeditunassignedtaskdynamicdataholder div[data-addperiodid]").append(divcompile);


                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });
            }
            else {
                //var UpdateperiodByID = $resource('planning/EntityPeriod/');
                //var updateperiod = new UpdateperiodByID();
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {

                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[Add Start / End Date ]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });

                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });


                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = dateFormat(formatteddateFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = dateFormat(formatteddateFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }


                //----------> Start date and end date diff checking
                var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }

                var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
                var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
                if (parseInt(diffvalend) < 1) {


                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }

                var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
                var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));

                    return false;
                }


                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = sdate;
                updateperiod.EndDate = edate;
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];

                // var resultperiod = UpdateperiodByID.save(updateperiod, function () {
                PlanningService.PostEntityPeriod1(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        $scope.fields["PeriodStartDate_" + periodid] = ConvertDateFromStringToString(sdate);
                        $scope.fields["PeriodEndDate_" + periodid] = ConvertDateFromStringToString(edate);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
            }
        }

        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForTreeLevels/:EntityID/:AttributeID', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        //var GetTreeRes = $resource('metadata/GetAttributeTreeNodeByEntityID/:AttributeID/:EntityID');
                        //var GetTree = GetTreeRes.get({ AttributeID: attrID, EntityID: parseInt($scope.TaskBriefDetails.taskID, 10) }, function () {
                        MetedataService.GetAttributeTreeNodeByEntityID(attrID, parseInt($scope.TaskBriefDetails.taskID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0)
                                $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;

                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });

            }
        }

        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                //var UpdateEntityAttributes = $resource('metadata/UpdateDropDownTreePricing/:EntityID/:AttributeID', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                //var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var GetTreeRes;
                        // if (choosefromParent)

                        //    GetTreeRes = $resource('metadata/GetDropDownTreePricingObjectFromParentDetail/:AttributeID/:IsInheritFromParent/:Isfromparent/:Entityid/:ParentId');
                        //else
                        //    GetTreeRes = $resource('metadata/GetDropDownTreePricingObject/:AttributeID/:IsInheritFromParent/:Isfromparent/:Entityid/:ParentId');


                        //  var GetTree = GetTreeRes.get({ AttributeID: attrID, IsInheritFromParent: choosefromParent, Isfromparent: choosefromParent, Entityid: parseInt($scope.TaskBriefDetails.taskID, 10), ParentId: 0 }, function () {
                        MetadataService.GetDropDownTree(choosefromParent, attrID, choosefromParent, choosefromParent, parseInt($scope.TaskBriefDetails.taskID, 10), 0).then(function (GetTree) {

                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) { return e.level == p; }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;

                                        //Updating data in detail block

                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0)
                                                valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            }
                                            else
                                                selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        }
                                        else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }

                            }
                        });
                    }
                });

            }
        }

        function ReturnSelectedTreeNodes(attrID) {
            var selection = [];
            for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {

                if (price.selection.length > 0) {
                    var matches = [];
                    matches = jQuery.grep(price.LevelOptions, function (relation) {
                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                    });
                    if (matches.length > 0)
                        for (var z = 0, node; node = matches[z++];) {
                            selection.push({ "NodeId": node.NodeId, "Level": price.level, "value": node.value != "" ? node.value : "-1" })
                        }

                }
            }
            return selection;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }

        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {

            for (var i = 0, node; node = treeobj[i++];) {

                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    }
                    else {
                        GenerateTreeStructure(node.Children);
                    }
                }
                else
                    GenerateTreeStructure(node.Children);
            }

        }


        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        //task metadata handling methods ends here


        //detail dynamic block works ends here

        function GetEntityAttributesDetails(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityAttributesDetails/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityAttributesDetails(taskID).then(function (TaskDetailList) {
                LoadTaskMetadata(TaskDetailList.Response, $scope.TaskBriefDetails.taskID, $scope.TaskBriefDetails.taskTypeId, $scope.fromAssignedpopup);

            });
        }

        $scope.CloseDampopup = function () {
            if ($scope.editAssetfileTemplateLoading != null) {
                $scope.SetCurrentTabid("Edittask-MetaData-" + $scope.AssetfileTemplateLoading, "Edittask-Attachments-" + $scope.AssetfileTemplateLoading)
                $("#Edittask-Attachments-" + $scope.editAssetfileTemplateLoading + "dam").empty();
                $("#Edittask-Attachments-" + $scope.editAssetfileTemplateLoading + "dam").unbind();
                $scope.editAssetfileTemplateLoading = "Null";
            }
        };

        $scope.editAssetfileTemplateLoading = "Null";

        $scope.SetCurrentTabid = function (activecalss, deactiveclass) {
            $("#li" + activecalss + "").removeClass('active');
            $("#" + activecalss + "").removeClass('tab-pane active');
            $("#" + activecalss + "").removeClass('tab-pane');
            $("#li" + activecalss + "").addClass('active');
            $("#" + activecalss + "").addClass('tab-pane active');

            $("#li" + deactiveclass + "").removeClass('active');
            $("#" + deactiveclass + "").removeClass('tab-pane active');
            $("#" + deactiveclass + "").removeClass('tab-pane');
            $("#" + deactiveclass + "").addClass('tab-pane');

        }

        //task dynamic metadata handling works ends here-->END
        //dam related logic goes here
        $scope.processingsrcobj = { processingid: $scope.TaskBriefDetails.taskID, processingplace: "task", processinglock: $scope.IsLockTask };
        function refreshAssetobjects() {
            $scope.PageNoobj = { pageno: 1 };
            $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 }, { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1 }, { "Name": "Name (Descending)", ID: 2 }, { "Name": "Creation date (Ascending)", ID: 3 }, { "Name": "Creation date (Descending)", ID: 4 }];
            $scope.FilterStatus = { filterstatus: 1 };
            $scope.DamViewName = { viewName: "Thumbnail" };
            $scope.OrderbyName = { orderbyname: "Creation date (Descending)" };
            $scope.OrderBy = { order: 4 };
            $scope.AssetSelectionFiles = [];
        }
        refreshAssetobjects();

        function GetEntityTaskAttachmentinfo(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityTaskAttachmentinfo/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskAttachmentinfo = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityTaskAttachmentinfo(taskID).then(function (TaskAttachmentinfo) {
                if (TaskAttachmentinfo.Response != null) {
                    $scope.TaskBriefDetails.Totalassetcount = TaskAttachmentinfo.Response[0].totalcount;
                    $scope.TaskBriefDetails.Totalassetsize = parseSize(TaskAttachmentinfo.Response[0].totalfilesize);
                }

            });
        }

        $scope.$on('CallBackAttachtak', function (event, ID) {
            refreshAssetobjects();
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            $timeout(function () {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $scope.$broadcast('ReloadAssetView');
            }, 100);

        });

        $scope.$on('Taskassetinfoupdate', function (event) {

            $timeout(function () {
                GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            }, 100);

        });
        // --- Define Controller Variables. ----------------- //

        // Get the render context local to this controller (and relevant params).
        var renderContext = requestContext.getRenderContext("mui");


        // --- Define Scope Variables. ---------------------- //


        // The subview indicates which view is going to be rendered on the page.
        $scope.subview = renderContext.getNextSection();

        // Get the current year for copyright output.
        $scope.copyrightYear = (new Date.create()).getFullYear();


        // --- Bind To Scope Events. ------------------------ //


        // I handle changes to the request context.
        $scope.$on(
            "requestContextChanged",
            function () {
                // Make sure this change is relevant to this controller.
                if (!renderContext.isChangeRelevant()) {
                    return;
                }

                //Make sure the user is logged in
                $scope.LoginStatus();

                // Update the view that is being rendered.
                $scope.subview = renderContext.getNextSection();
            }
        );

        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimerForTask);
            RecursiveUnbindAndRemove($("[ng-controller='mui.taskeditCntrl']"));
        });

        // --- Initialize. ---------------------------------- //

        // ...

        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'color': "#" + clr.toString().trim() };
            else
                return '';
        }
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.TaskDueDate1 = false;
            model = model1;
        };

        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.TaskDueDate1 = true;
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };
        PlanningService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null)
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope['NormalDropDown_' + atrid])
                $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '');
            else
                $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '') / 1 / $scope.currRate;
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            PlanningService.GetCostCentreCurrencyRateById(0, $scope['NormalDropDown_' + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.currRate = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        // $scope['SingleTextValue_' + atrid] = parseFloat($scope['origninalamountvalue_' + atrid]) * $scope.currRate;
                        $scope['SingleTextValue_' + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope.currRate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                        // $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                    //$scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                }
            });
        }
    }
    //);
    app.controller("mui.taskeditCntrl", ['$scope', '$cookies', '$resource', '$route', '$routeParams', 'requestContext', '$compile', '$window', '$timeout', '$location', '_', '$sce', '$translate', 'PlanningService', 'TaskService', 'MetadataService', 'CommonService', 'AccessService', muitaskeditCntrl]);
})(angular, app);