﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcom.API.Models.Interface
{
    interface ISessionModel
    {    
        Guid sessionid { get; set; }
    }
}
