using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access;

namespace BrandSystems.Marcom.Core.User.Interface
{
    /// <summary>
    /// IUser interface for table 'UM_User'.
    /// </summary>
    public interface IUser
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string FirstName
        {
            get;
            set;

        }

        string LastName
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        byte[] Password
        {
            get;
            set;

        }

        string SaltPassword
        {
            get;
            set;

        }

        string Email
        {
            get;
            set;

        }

        string Image
        {
            get;
            set;

        }

        string Language
        {
            get;
            set;

        }

        string TimeZone
        {
            get;
            set;

        }

        int? StartPage
        {
            get;
            set;

        }

        int DashboardTemplateID
        {
            get;
            set;
        }

        int IsDashboardCustomized
        {
            get;
            set;
        }

        string Feedselection
        {
            get;
            set;
        }


        bool Gender
        {
            get;
            set;

        }

        string Phone
        {
            get;
            set;

        }

        string Address
        {
            get;
            set;

        }

        string City
        {
            get;
            set;

        }

        int ZipCode
        {
            get;
            set;

        }

        string Website
        {
            get;
            set;

        }

        string Designation
        {
            get;
            set;
        }

        string CompanyName
        {
            get;
            set;
        }
        string Partners
        {
            get;
            set;
        }

        string Title
        {
            get;
            set;
        }
        string OldPassword
        {
            get;
            set;
        }

        int LanguageSettings
        {
            get;
            set;
        }


        bool IsSSOUser
        {
            get;
            set;

        }

        bool IsAPIUser
        {
            get;
            set;

        }

        IList<GlobalAcl> ListOfUserGlobalRoles
        {
            get;
            set;
        }

        string QuickInfo1
        {
            get;
            set;

        }

        string QuickInfo2
        {
            get;
            set;

        }
        string QuickInfo1AttributeCaption
        {
            get;
            set;

        }

        string QuickInfo2AttributeCaption
        {
            get;
            set;

        }
        int DimensionUnit
        {
            get;
            set;

        }

        int AssetAccess
        {
            get;
            set;

        }

        int TenantID
        {
            get;
            set;

        }
        string TenantHost
        {
            get;
            set;

        }
        string TenantPath
        {
            get;
            set;

        }

        string TenantName
        {
            get;
            set;

        }


        Dictionary<string, bool> ListOfAccess
        {
            get;
            set;
        }

        bool MemberRelated
        {
            get;
            set;

        }
        Guid session { get; set; }
        #endregion

        StorageBlock AwsStorage { get; set; }

    }
}
