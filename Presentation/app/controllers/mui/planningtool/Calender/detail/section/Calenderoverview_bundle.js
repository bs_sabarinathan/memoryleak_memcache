///#source 1 1 /app/controllers/mui/planningtool/calender/detail/section/overview-controller.js
(function (ng, app) {

    function muiplanningtoolcalenderdetailoverviewCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $stateParams, $location, $window, $translate, CalenderoverviewService) {

        $scope.EntityUserListInCalender = [];
        $scope.PersonalUserIdsInCalender = [];
        var cancelNewsTimerevent;
        var FeedInitiated = false;
        $scope.OwnerName = '';
        $scope.CalenderisEdit_mui = $scope.CalenderisEdit;

        $scope.GetEntityMembers = function () {
            $scope.PersonalUserIdsInCalender = [];
            $scope.EntityUserListInCalender = [];
            CalenderoverviewService.GetMember($stateParams.ID).then(function (member) {
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserListInCalender.length > 0) {
                                $.each($scope.EntityUserListInCalender, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true)
                                    $scope.EntityUserListInCalender.push(value);
                            }
                            else
                                $scope.EntityUserListInCalender.push(value);
                        }
                    });
                }
            });
        }
        $('#CalenderOverviewDiv').on('onCalOverViewDetail', function (event, EntityTypeID) {
            $scope.TaskListSummerydetal = [];
            $scope.LoadNewsFeedBlockForCalender();
            $scope.DynamicTaskListDescriptionCal = "-";

            $scope.CalDatavalue = {};

            $scope.TabCollectionsList = null;
            $scope.selectedTabs = [];
            $scope.CalenderOwnerId = 0;
            $scope.calenderRoleId = 0;
            $scope.ObjectiveOwnerName = '';
            $scope.CalenderOwnerName = 0;
            $scope.CurrentCalenderOwnerId = 0;

            $scope.ConditionDataArr = [];
            $scope.CalenderFulfillDeatils = {};
            GetAllTabCollections();
            EnableDisableLoader();
            LoadCalenderFulfillment();
            FilltFulfillmentEntityTypes();
        });
        //---Declaration ---//
        $scope.CalenderFulfillDeatils = {};
        $scope.CalenderDetails = {};
        $scope.FulfillmentEntityTypes = {};
        $scope.FulfillmentAttributes = [];
        $scope.FulfillmentAttributeOptions = [];
        $scope.FulfillmentConditionsList = [];
        $scope.CurrentCalenderId = parseInt($stateParams.ID, 10);
        $scope.userimg = parseInt($cookies['UserId']);
        $scope.TaskListSummerydetail = [];
        if ($scope.isfromExternalLink != undefined && $scope.isfromExternalLink == true) {
            $scope.IsLock = true;
        }
        //$scope.Newsfilter.Feedgrouplistvalues = '-1';
        //---Declaration End--//

        EnableDisableLoader();

        function EnableDisableLoader() {
            $scope.detailsLoader = true;
            $scope.detailsData = false;
        }

        $scope.selectedTabs = [];
        CalenderoverviewService.GetMember($stateParams.ID).then(function (member) {
            $scope.CalenderAssignRole = member.Response;
            var IsViewer = $.grep($scope.CalenderAssignRole, function (e) { return e.Roleid == 56 });
            for (var i = 0; i < IsViewer.length; i++) {
                if (IsViewer[i].Userid == $cookies['UserId']) {
                    $scope.IsLock = true;
                }
            }
            LoadCalenderFulfillment();
        });


        $scope.selectedEntities = [];
        $scope.CalenderOwnerList = {};
        function LoadCalenderFulfillment() {
            CalenderoverviewService.GettingCalenderFulfillmentBlockDetails(parseInt($stateParams.ID, 10))
               .then(function (result) {
                   //only for Calendar as a Link

                   if (result.Response != null && result.Response != undefined) {
                       LoadFulfillMentBlock(result.Response[0], result.Response[2]);
                       $scope.CalenderOwnerList = result.Response[1];
                       LoadTaskSummaryDetlTemp(result.Response[3]);
                       $scope.selectedTabs = result.Response[4];
                       $timeout(selectedTabsforCalenders($scope.selectedTabs), 500);
                   }
               });
        }

        function selectedTabsforCalenders(selectedtabs) {
            $.each(selectedtabs, function (index, value) {
                $("#tabscheckbox_" + value).addClass("checkbox checked")
            });
        };


        $scope.CalDatavalue = {};
        GetAllTabCollections();
        $scope.TabCollectionsList = null;
        function GetAllTabCollections() {
            CalenderoverviewService.GetAllTabCollections(6)
             .then(
             function (result) {
                 if (result.Response != null) {
                     $scope.TabCollectionsList = result.Response;
                 }

             });
        };


        $scope.CalenderOwnerId = 0;
        $scope.calenderRoleId = 0;
        $scope.ObjectiveOwnerName = '';
        $scope.CalenderOwnerName = 0;
        $scope.CurrentCalenderOwnerId = 0;
        function LoadFulfillMentBlock(ObjectiveFulfillDetails, Caldata) {
            $scope.CalenderFulfillDeatils = ObjectiveFulfillDetails;
            $scope.CalenderDetails.VisibilityPeriod = Caldata.Visibilityperiod;
            $scope.CalenderDetails.Visibilitytype = Caldata.Visibilitytype;
            $scope.CalenderDetails.IsExternal = Caldata.IsExternal;
            if ($scope.CalenderDetails.IsExternal == true) {
                $('#CalenderisExternal').checked = true;
            }
            else {
                $('#CalenderisExternal').checked = false;
            }

            var param1 = new Date.create(Caldata.PublishedData);
            var param2 = param1.getDate() + '-' + param1.getMonth() + '-' + param1.getFullYear();
            var dateValue = Caldata.PublishedData;
            dateValue = dateValue.split('-');
            dateValue = dateValue[0] + '-' + dateValue[1] + '-' + dateValue[2];
            $scope.CalenderDetails.PublishedOn = dateFormat(Caldata.PublishedData);
            $scope.objFulfillHtml = '';
            $scope.objDetailHtml = '';
            $scope.CalenderOwnerName = $scope.CalenderFulfillDeatils.OwnerName;
            $scope.CurrentCalenderOwnerId = $scope.CalenderFulfillDeatils.OwnerId;
            $scope.objFulfillHtml += '<div class="control-group ng-scope">';
            $scope.objFulfillHtml += '<label for="label" class="control-label">Condition</label>';
            $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
            if ($scope.CalenderFulfillDeatils.FulfillCondition != '' && $scope.CalenderFulfillDeatils.FulfillCondition != undefined)
                $scope.objFulfillHtml += $scope.CalenderFulfillDeatils.FulfillCondition.replace(/\n|\r/g, "");
            else
                $scope.objFulfillHtml += $scope.CalenderFulfillDeatils.FulfillCondition;
            $scope.objFulfillHtml += '</label></div></div>';
            $('#fulfillmentDiv').html($compile($scope.objFulfillHtml)($scope));
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.objDetailHtml += '<div class="control-group ng-scope">';
            $scope.objDetailHtml += '<label for="label" class="control-label">Id</label>';
            $scope.objDetailHtml += '<div class="controls"><label for="label" class="control-label">';
            $scope.objDetailHtml += $scope.CalenderFulfillDeatils.Id;
            $scope.objDetailHtml += '</label></div></div>';
            $scope.objDetailHtml += '<div class="control-group ng-scope">';
            $scope.objDetailHtml += '<label for="label" class="control-label">Name</label>';
            $scope.objDetailHtml += '<div class="controls">';
            if ($scope.IsLock == false) {
                $scope.objDetailHtml += '<a xeditablecalender href="javascript:;" calenderId="' + parseInt($stateParams.ID, 10) + '" calenderName="' + $scope.CalenderFulfillDeatils.CalenderName + '" calenderDescripton="' + $scope.CalenderFulfillDeatils.CalenderDescription + '" calendereditType="111" ng-click="calendereditcontrolclick()" id="CalenderName" data-ng-model="CalenderName" data-type="text" data-original-`"Calender Name">';
                $scope.objDetailHtml += $('<div />').html($scope.CalenderFulfillDeatils.CalenderName).text();
                $scope.objDetailHtml += '</a>';
            }
            else {
                $scope.objDetailHtml += '<label for="label" class="control-label">' + $scope.CalenderFulfillDeatils.CalenderName + '</label>';
            }
            $scope.objDetailHtml += '</div></div>';
            $scope.objDetailHtml += '<div class="control-group ng-scope">';
            $scope.objDetailHtml += '<label for="label" class="control-label">Owner</label>';
            $scope.objDetailHtml += '<div class="controls">';
            if ($scope.IsLock == false) {
                $scope.objDetailHtml += '<a  xeditablecalenderownerlist href="javascript:;" attributeTypeID="' + AttributeTypes.Owner + '"';
                $scope.objDetailHtml += 'entityid="' + parseInt($stateParams.ID, 10) + '" attributeid="' + SystemDefiendAttributes.Owner + '"';
                $scope.objDetailHtml += 'id="' + SystemDefiendAttributes.Owner + '"  data-ng-model="ngowner" my-qtip2 qtip-content="Select Owner"';
                $scope.objDetailHtml += 'attributename=" calenderownername " data-type="calenderownername" >{{CalenderOwnerName}}';
                $scope.objDetailHtml += '</a>';
            }
            else {
                $scope.objDetailHtml += '<label for="label" class="control-label">' + $scope.CalenderOwnerName + '</label>';
            }
            $scope.objDetailHtml += '</div></div>';


            $scope.objDetailHtml += '<div class="control-group ng-scope">';
            $scope.objDetailHtml += '<label for="label" class="control-label">Description</label>';
            $scope.objDetailHtml += '<div class="controls">';
            if ($scope.IsLock == false) {
                $scope.objDetailHtml += '<a xeditablecalender href="javascript:;" calenderId="' + parseInt($stateParams.ID, 10) + '" calenderName="' + $scope.CalenderFulfillDeatils.CalenderName + '" calenderDescripton="' + $scope.CalenderFulfillDeatils.CalenderDescription + '" calendereditType="112" ng-click="calendereditcontrolclick()" id="CalenderDescription" data-ng-model="CalenderDescription" data-type="text" data-original-title="Calender Description">';
                $scope.objDetailHtml += $scope.CalenderFulfillDeatils.CalenderDescription != '' ? $('<div />').html($scope.CalenderFulfillDeatils.CalenderDescription).text() : '-';
                $scope.objDetailHtml += '</a>';
            }
            else {
                $scope.objDetailHtml += '<label for="label" class="control-label">' + $scope.CalenderFulfillDeatils.CalenderDescription + '</label>';
            }
            $scope.objDetailHtml += '</div></div>';

            $('#caldetailsDiv').html($compile($scope.objDetailHtml)($scope));
        }


        $scope.FulfillEntites = [];
        $scope.CalenderFulfillEdit = function () {
            CalenderoverviewService.GettingEditCalenderFulfillmentDetails(parseInt($stateParams.ID, 10)).then(function (GetObjectiveFullConditionsListObj) {
                var Result = GetObjectiveFullConditionsListObj.Response.m_Item1;
                $scope.selectedEntities = GetObjectiveFullConditionsListObj.Response.m_Item2;
                if (Result != null) {
                    var html = '';
                    $.each(Result, function (val, item) {
                        var UniqueId = val;
                        html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                        html += "<ul class='repeter'>";
                        html += "<li class='form-inline'>";
                        if (val == 0) {
                            html += "<div class='coverFill'></div>";
                        }
                        else {
                            html += "<div class='cover'></div>";
                        }
                        html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>Condition:</label>";
                        html += "<select ui-select2 id='calConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "'>";
                        html += "<option value='' >-- Set condition -- </option>";
                        html += "<option value='1'>OR </option>";
                        html += "<option value='2'>AND </option>";
                        html += "</select>";
                        html += "<label>If type is</label>";
                        html += "<select ui-select2 id='calEntityType" + UniqueId + "' ng-model='dummymodule_calEntityType_" + UniqueId + "'  data-role='EntityType'>";
                        html += "<option value='0'>-- Select --</option>";
                        html += "</select>";
                        html += "<label>where</label>";
                        html += "<select ui-select2 id='calEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'    data-role='Attributes'>";
                        html += "<option value='0'>-- Select --</option>";
                        html += "</select>";
                        html += "<label>equals</label>";
                        html += "<select ui-select2 multiple='multiple' id='calAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "'  data-role='Equals'>";
                        html += "<option  value='0'>-- Select --</option>";
                        html += "</select>";
                        html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                        html += "</li>";
                        html += "</ul>";
                        html += "</div>";
                    });
                    $('#CalenderFulfillmentMetadatatbody').html($compile(html)($scope));
                    $.each(Result, function (val, item) {
                        var UniqueId = val;
                        $scope["dummymodule_ConditionType_" + UniqueId] = item.ConditionType;

                        FillEntityTypes("calEntityType" + UniqueId);
                        $timeout(function () {
                            $("#calEntityType" + UniqueId).select2("val", item.EntityTypeid);
                            $scope["dummymodule_calEntityType_" + UniqueId] = item.EntityTypeid;
                            //$scope["dummymodule_ConditionType_" + UniqueId] = item.ConditionType;

                        }, 10);

                        LoadFillEntityTypeAttributes("calEntityAttributes" + UniqueId, item.EntityTypeid, item.Attributeid, item.AttributeLevel, UniqueId);
                        LoadFillEntityTypeOptions("calAttributeOptions" + UniqueId, item.Attributeid, item.AttributeLevel, item.CalenderOptionValue)

                    });

                    $timeout(function () { $scope.getEntities() }, 500);



                }
                else {
                    var html = '';
                    var UniqueId = 0;
                    html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                    html += "<ul class='repeter'>";
                    html += "<li class='form-inline'>";
                    html += "<div class='coverFill'></div>";
                    html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>Condition:</label>";
                    html += "<select ui-select2 id='calConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "'>";
                    html += "<option value='' >-- Set condition -- </option>";
                    html += "<option value='1'>OR </option>";
                    html += "<option value='2' ng-selected='true'>AND </option>";
                    html += "";
                    html += "</select>";
                    html += "<label>If type is</label>";
                    html += "<select ui-select2 id='calEntityType" + UniqueId + "' data-role='EntityType' ng-model='dummymodule_calEntityType_" + UniqueId + "'>";
                    html += "<option value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>where</label>";
                    html += "<select ui-select2 id='calEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes'>";
                    html += "<option value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>equals</label>";
                    html += "<select ui-select2 multiple='multiple' id='calAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "'  data-role='Equals'>";
                    html += "<option  value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                    html += "";
                    html += "</li>";
                    html += "</ul>";
                    html += "</div>";
                    $('#CalenderFulfillmentMetadatatbody').html($compile(html)($scope));
                    FillEntityTypes("calEntityType" + UniqueId);
                }
            });
        }

        $scope.getEntities = function () {
            collectionCalenderCondition();
            var CalenderFulfill = [];
            var CalDatavaluefrEntities = $scope.ConditionDataArr;
            if (CalDatavaluefrEntities[0].CalEntityType != "0") {
                CalenderFulfill.push({ "CalenderFulfillValues": CalDatavaluefrEntities });
            }
            else if (CalDatavaluefrEntities[0].CalEntityType == "0") {
                CalenderFulfill.push({ "CalenderFulfillValues": CalDatavaluefrEntities });
            }
            CalenderoverviewService.getEntitiesfrCalender(CalenderFulfill)
           .then(function (result) {
               if (result.StatusCode == 200) {
                   $scope.FulfillEntites = result.Response;
                   $timeout(checktheCalenderEnttites, 500);
                   //checktheCalenderEnttites();
               }
               else {
               }
           });
        }
        function checktheCalenderEnttites() {
            $.each($scope.selectedEntities, function (val, item) {
                $("#CalSelEntity_" + item).addClass("checkbox checked")
            });
        }
        function LoadFillEntityTypeAttributes(ControllerID, EntityTypeID, Attributeid, AttributeLevel, UniqueId) {
            if (ControllerID != undefined) {
                if (EntityTypeID != null) {
                    CalenderoverviewService.GetFulfillmentAttribute(EntityTypeID).then(function (fulfillmentAttributeOptionsObj) {
                        if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                            if (ControllerID != undefined) {
                                $('#' + ControllerID).html("");
                                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                                if (fulfillmentAttributeOptionsObj.Response != null) {
                                    $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                        var currentId = item.Id.toString() + "_" + item.Level;
                                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                                    });
                                    $timeout(function () {
                                        if (Attributeid == 0 && AttributeLevel == 0) {
                                            $scope["dummymodule_attribute_" + UniqueId] = 0;

                                        }
                                        else {
                                            $('#' + ControllerID).select2("val", Attributeid + "_" + AttributeLevel);

                                            $scope["dummymodule_attribute_" + UniqueId] = Attributeid + "_" + AttributeLevel;
                                        }

                                    }, 10);
                                }
                            }
                        }
                    });
                }
            }
        }



        function LoadFillEntityTypeOptions(ControllerID, entityAttribtueId, entityAttributeLevel, selectedAttributeOption) {
            if (entityAttribtueId != null) {
                CalenderoverviewService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        if (ControllerID != undefined) {
                            $('#' + ControllerID).html("");
                            $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                            if (fulfillmentAttributeOptionsObj.Response != null) {
                                $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                    $('#' + ControllerID).append($("<option></option>").val(item.Id).html(item.Caption));
                                });
                                var Values = selectedAttributeOption.split(",");
                                $timeout(function () {
                                    $('#' + ControllerID).select2("val", Values);
                                }, 10);
                            }
                        }
                    }
                });
            }
        }


        FilltFulfillmentEntityTypes();
        function FilltFulfillmentEntityTypes() {
            CalenderoverviewService.GetFulfillmentEntityTypesfrCal().then(function (fulFillmentEntityTypesObj) {
                $scope.FulfillmentEntityTypes = fulFillmentEntityTypesObj.Response;

            });


        }



        $scope.calendereditcontrolclick = function () {
            $timeout(function () { $('[class=input-medium]').focus().select() }, 10);
        };


        $("#CalenderFulfillmentMetadatatbody").click(function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') {
                if (TargetControl.val() == 0) {
                    ResetDropDown("calEntityAttributes" + currentUniqueId, currentUniqueId);
                    return false;
                }
                CalenderoverviewService.GetFulfillmentAttribute(TargetControl.val()).then(function (fulFillmentAttributesObj) {
                    if (fulFillmentAttributesObj.Response.length >= 0) {
                        FillEntityTypeAttributes("calEntityAttributes" + currentUniqueId, fulFillmentAttributesObj.Response);
                        ResetDropDown("calAttributeOptions" + currentUniqueId, currentUniqueId);
                    }
                });
            }
            else if (TargetControl.attr('data-role') == 'Attributes') {
                if (TargetControl.val() == 0) {
                    ResetDropDown("calAttributeOptions" + currentUniqueId, currentUniqueId);
                    return false;
                }
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                CalenderoverviewService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillEntityTypeAttributes("calAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response);
                    }
                });

            }
            else if (TargetControl.attr('data-role') == 'Equals') {

            }
            else if (TargetControl.attr('data-role') == 'Add') {

                if (TargetControl.parents().find('#calEntityType' + currentUniqueId).val() == "0" || TargetControl.parents().find('#calEntityType' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                    return false;
                }

                if ($('#calEntityAttributes' + currentUniqueId).val() != "0" && $('#calEntityAttributes' + currentUniqueId).val() != null) {
                    if ($('#calAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }

                var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='ObjectiveCondition'>Condition:</label>";
                html += "<select ui-select2 id='calConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "' style='width:120px;'>";
                html += "<option value='' >-- Set condition -- </option>";
                html += "<option value='1'>OR </option>";
                html += "<option value='2' ng-selected='true'>AND </option>";
                html += "";
                html += "</select>";
                html += "<label>If type is</label>";
                html += "<select ui-select2 id='calEntityType" + UniqueId + "' ng-model='dummymodule_calEntityType_" + UniqueId + "' data-role='EntityType'    style='width: 120px;'>";
                html += "<option value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<label>where</label>";
                html += "<select ui-select2 id='calEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes' style='width: 120px;'>";
                html += "<option value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<label>equals</label>";
                html += "<select ui-select2 multiple='multiple' id='calAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "' data-role='Equals' style='width: 120px;'>";
                html += "<option  value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("calEntityType" + UniqueId);

            }
            else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                    return false;
                }
                $("#container" + currentUniqueId).remove();
            }
            $scope.TimerForLatestFeed();
        });

        function FillEntityTypes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentEntityTypes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                });
            }
        }

        function FillEntityTypeAttributes(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).select2("val", "");
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(ControllerID, currentUniqueId) {
            $('#' + ControllerID).html("");
            $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
            $('#' + ControllerID).select2("val", "");
            $('#calAttributeOptions' + currentUniqueId).html("");
            $('#calAttributeOptions' + currentUniqueId).html("<option value='0'>-- Select --</option>");
            $('#calAttributeOptions' + currentUniqueId).select2("val", "");
        }

        $scope.ConditionDataArr = [];
        function collectionCalenderCondition() {
            $scope.ConditionDataArr = [];
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            var optionArr = [];
            $('#CalenderFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                if ($('#calConditionType' + index).val() == '' || $('#calConditionType' + index).val() == null || $('#calConditionType' + index).val() == undefined) {
                    var collectionarr = { 'CalEntityType': $('#calEntityType' + index).val(), 'CalEntityAttributes': $('#calEntityAttributes' + index).val(), 'CalAttributeOptionns': $('#calAttributeOptions' + index).val(), 'CalConditionType': 0 };
                }
                else {
                    var collectionarr = { 'CalEntityType': $('#calEntityType' + index).val(), 'CalEntityAttributes': $('#calEntityAttributes' + index).val(), 'CalAttributeOptionns': $('#calAttributeOptions' + index).val(), 'CalConditionType': $('#calConditionType' + index).val() };
                }
                $scope.ConditionDataArr.push(collectionarr);

            });

        }

        $scope.selectAllEntities = function (e) {
            $scope.selectedEntities = [];
        }
        $scope.selectEntity = function (entityid, e) {
            if (e.target.checked) {
                $scope.selectedEntities.push(entityid);
            }
            else {
                $scope.selectedEntities.splice($.inArray(entityid, $scope.selectedEntities), 1);
            }
        }

        $scope.updateCalenderFulFillment = function () {
            var tempval = false;
            $('#CalenderFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                if ($('#calEntityType' + index).val() == "0" || $('#calEntityType' + index).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                    tempval = true;
                    return false;
                }
                else if ($('#calEntityAttributes' + index).val() != "0" && $('#calEntityAttributes' + index).val() != null) {
                    if ($('#calAttributeOptions' + index).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        tempval = true;
                        return false;
                    }
                }

            });
            if (tempval == true)
                return false;

            collectionCalenderCondition();
            $scope.CalDatavalue = $scope.ConditionDataArr;
            var CalenderCondition = {};
            CalenderCondition.CalenderID = parseInt($stateParams.ID, 10);
            var selectedentitiesfrCal = [];
            $.each($scope.FulfillEntites, function (index, value) {
                if ($("#CalSelEntity_" + value.EntityID).attr("class") == "checkbox checked" || $("#CalSelEntity_" + value.EntityID).attr("class") == "checked") {
                    selectedentitiesfrCal.push(value.EntityID);
                }
            });
            CalenderCondition.selectedentities = selectedentitiesfrCal;
            //Handling the newsfeed for fullfllcodiiton while page is loading...
            if ($scope.CalenderFulfillDeatils.FulfillCondition != '' && $scope.CalenderFulfillDeatils.FulfillCondition != undefined)
                CalenderCondition.CalenderFulfillDeatils = $scope.CalenderFulfillDeatils.FulfillCondition.replace(/\n|\r/g, "");
            else
                CalenderCondition.CalenderFulfillDeatils = "";
            CalenderCondition.CalenderFulfillConditonValueData = [{
                "CalenderFulfillValues": $scope.CalDatavalue
            }];
            $('#EditCalenderFulfillment').modal('hide');
            CalenderoverviewService.UpdateCalenderFulfillmentCondition(CalenderCondition).then(function (calenderFulfillConditionUpdateResult) {
                if (calenderFulfillConditionUpdateResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_1132.Caption'));
                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4138.Caption'));
                    CalenderoverviewService.GettingCalenderFulfillmentBlockDetails(calenderFulfillConditionUpdateResult.Response)
                     .then(function (result) {
                         if (result.Response != null) {
                             LoadFulfillMentBlock(result.Response[0], result.Response[2]);
                         }
                     });
                    $scope.$emit("OnFullfillmentChangecalender", calenderFulfillConditionUpdateResult.CalenderID);

                }
            });
            $scope.TimerForLatestFeed();
        }



        $scope.SaveCalenderDeatils = function (calenderId, calenderName, calenderedittype, calenderDescription, calendernewValue) {
            var saveObjectiveNameObj = {};
            if (calenderedittype == 111) {
                saveObjectiveNameObj.CalenderID = $scope.CurrentCalenderId;
                saveObjectiveNameObj.CalenderName = calendernewValue;
                saveObjectiveNameObj.CalenderDescription = calenderDescription;
                saveObjectiveNameObj.Typeid = calenderedittype;
                $(window).trigger("UpdateCalenderEntityName", calendernewValue);
                $('#breadcrumlink').text(calendernewValue);
                $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([calendernewValue]);
                $("ul li a[data-entityid='" + $stateParams.ID + "']").attr('data-entityname', calendernewValue);

            }
            else {
                saveObjectiveNameObj.CalenderID = $scope.CurrentCalenderId;
                saveObjectiveNameObj.CalenderName = calenderName;
                saveObjectiveNameObj.CalenderDescription = calendernewValue;
                saveObjectiveNameObj.Typeid = calenderedittype;
            }
            CalenderoverviewService.UpdatingCalenderOverDetails(saveObjectiveNameObj).then(function (saveObjectiveNameDataResult) {
                if (saveObjectiveNameDataResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_1131.Caption'));
                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4140.Caption'));
                    saveObjectiveNameDataResult.CalenderID = '';
                    saveObjectiveNameDataResult.CalenderName = '';
                    saveObjectiveNameDataResult.CalenderDescription = '';
                }
            });
            $scope.TimerForLatestFeed();
        }


        $scope.UpdateCalenderOwner = function (CalenderOwnerId, roleId, CalenderOwnerName) {
            var UpdateObjectiveOwnerObj = {};
            UpdateObjectiveOwnerObj.EntityID = parseInt($stateParams.ID, 10);
            UpdateObjectiveOwnerObj.UserId = CalenderOwnerId;
            UpdateObjectiveOwnerObj.RoleId = roleId;
            UpdateObjectiveOwnerObj.OldUserId = $scope.CurrentCalenderOwnerId;
            UpdateObjectiveOwnerObj.ObjectiveOwnerName = CalenderOwnerName;
            UpdateObjectiveOwnerObj.EntityTypeID = 35;
            CalenderoverviewService.UpdateObjectiveOwner(UpdateObjectiveOwnerObj).then(function (UpdateObjectiveOwnerResult) {

            });
        };



        $scope.CalenderDetEdit = function () {
            var selectedTabsfredit = [];
            $.each($scope.TabCollectionsList, function (index, value) {
                if ($("#tabscheckbox_" + value.Id).attr("class") == "checkbox checked" || $("#tabscheckbox_" + value.Id).attr("class") == "checked") {
                    selectedTabsfredit.push(value.Id);
                }
            });
            if (selectedTabsfredit.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_5033.Caption'));
                return false;
            }
            var d = $scope.CalenderDetails.PublishedOn;
            var savecalData = {};
            savecalData.CalenderID = parseInt($stateParams.ID, 10);
            savecalData.VisibilityPeriod = $scope.CalenderDetails.VisibilityPeriod;
            savecalData.VisibilityType = $scope.CalenderDetails.Visibilitytype;
            savecalData.IsExternal = $scope.CalenderDetails.IsExternal;
            savecalData.PublishedOn = dateFormat($scope.CalenderDetails.PublishedOn, "yyyy/MM/dd");
            savecalData.SelectedTabs = selectedTabsfredit;
            CalenderoverviewService.SaveCalenderDetails(savecalData).then(function (CalRelatedData) {
                if (CalRelatedData.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_1131.Caption'));
                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4140.Caption'));
                }
            });
        };

        function LoadTaskSummaryDetlTemp(EntTaskList) {
            ClearSummaryDetl();
            $scope.TaskListSummerydetail = [];
            $scope.DynamicTaskListDescriptionCal = "-";
            var TaskSummaryresult = EntTaskList;
            if (TaskSummaryresult.length > 0) {
                $scope.ShowEmptyTaskSummary = false;
                $scope.ShowLoadedTaskSummary = true;
                $scope.TaskListSummerydetal = TaskSummaryresult;
                $scope.OverAllStatus = $scope.TaskListSummerydetal[0].ActiveEntityStateID;
                $scope.Ontimecomment = ($scope.TaskListSummerydetal[0].OnTimeComment != null ? $scope.TaskListSummerydetal[0].OnTimeComment : '-');
                $scope.TasksInprgress = $scope.TaskListSummerydetal[0].TaskInProgress;
                $scope.Unassigned = $scope.TaskListSummerydetal[0].UnAssignedTasks;
                $scope.Overduetasks = $scope.TaskListSummerydetal[0].OverDueTasks;
                $scope.Unabletocomplete = $scope.TaskListSummerydetal[0].UnableToComplete;
                FindmaxWidth();
            }
            else {

                $scope.ShowEmptyTaskSummary = true;
                $scope.ShowLoadedTaskSummary = false;
            }
        }


        $scope.DynamicClass = 'btn-success';
        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = [];
            $scope.DynamicTaskListDescriptionCal = "-";
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();

        }

        function FindmaxWidth() {

            var maxwidth = $scope.TasksInprgress;

            if (maxwidth < $scope.Unassigned)
                maxwidth = $scope.Unassigned;

            if (maxwidth < $scope.Overduetasks)
                maxwidth = $scope.Overduetasks;

            if (maxwidth < $scope.Unabletocomplete)
                maxwidth = $scope.Unabletocomplete;


            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;

                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;

                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }

        $scope.isexternalselect = function (flag) {
            if (flag) {

                return "checkbox checked";
            }
            else {

                return "checkbox";
            }
        }


        $scope.checkIsExternal = function (isexternal, e) {
            $scope.CalenderDetails.IsExternal = (isexternal == true) ? false : true;
        };

        $scope.isTabSelected_setclass = function (tabid) {
            if ($scope.selectedTabs.length > 0) {
                var d = $.grep($scope.selectedTabs, function (e) { return e == parseInt(tabid); });
                if (d.length > 0) {
                    return "checkbox checked";
                }
                else {
                    return "checkbox";
                }
            }
        };

        $scope.LoadNewsFeedBlockForCalender = function () {
            var ID = $stateParams.ID;
            $scope.FeedHtml = '';
            $scope.entityNewsFeeds = {};
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            var PagenoforScroll = 0;
            //-------------------------------- Get Feeds for Pagescroll-----------------------------------------
            try {
                $('#CalenderFeedsdiv').unbind('scroll');
                $('#CalenderFeedsdiv').scroll(function () {

                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;
                        CalenderoverviewService.GetEnityFeeds(ID, PagenoforScroll, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResultForScroll) {
                            var feeddivHtml = '';
                            for (var i = 0 ; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';

                                if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                }
                                else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor;
                                }

                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >Comment</a></span>';
                                //feeddivHtml = feeddivHtml + '<span class="cmntInfo" > ' + feedcomCount + ' Comment(s)<i class="icon-caret-down"    data-DynHTML="CommentshowHTML" data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></i></span></div></div>';
                                if (feedcomCount > 1) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                }
                                else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment">';
                                if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {

                                    var j = 0;

                                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    }
                                    else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }

                                    for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        //feeddivHtml = feeddivHtml + '<li style = "display:none;"';


                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        }
                                        else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }


                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#CalenderFeedsdiv').append(feeddivHtml);
                            }


                        });
                    }
                });
            }
            catch (e) {
            }

            //--------------------------------------------------------------------------------------------------

            //---------------------------  Get the initial feed ------------------------------------
            try {
                $('#CalenderFeedsdiv').html('');
                //var getEntityNewsFeedResult = enityNewsFeed.get({ EntityID: ID, PageNo: 0, Feedsgroupid: $scope.Newsfilter.Feedgrouplistvalues }, function () {
                CalenderoverviewService.GetEnityFeeds(ID, 0, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                    var feeddivHtml = '';
                    $('#CalenderFeedsdiv').html('');
                    if (getEntityNewsFeedResult.Response != null) {
                        for (var i = 0 ; i < getEntityNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            feeddivHtml = '';

                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            }
                            else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                            }
                            else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment">';

                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {

                                var j = 0;

                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                }
                                else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }

                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    }
                                    else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            feedcomCount = 0;
                            $('#CalenderFeedsdiv').append(feeddivHtml);
                            feeddivHtml = '';
                        }
                    }

                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 30000);

                });
            }
            catch (e)
            { }



            //---------------------------  Getting the latest feed ------------------------------------
            try {

                $scope.TimerForLatestFeed = function () {
                    FeedInitiated = true;
                    if (NewsFeedUniqueTimer != undefined)
                        $timeout.cancel(NewsFeedUniqueTimer);

                    $scope.FeedHtml = '';
                    $scope.entityNewsFeeds = {};
                    CalenderoverviewService.GetLastEntityFeeds(ID, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult.Response != null) {
                            for (var i = 0 ; i < getEntityNewsFeedResult.Response.length; i++) {
                                var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                                feeddivHtml = '';
                                if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                }
                                else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                                }

                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                                if (feedcomCount > 1) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                }
                                else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment"';
                                if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {

                                    var j = 0;

                                    if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    }
                                    else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }

                                    for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        }
                                        else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#CalenderFeedsdiv [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });

                                $('#CalenderFeedsdiv').prepend(feeddivHtml);
                            }
                        }
                        FeedInitiated = false;
                        $timeout(function () {
                            $scope.TimerTocallBackNewsFeed();  //to startntimer after 30 seconds once page loads(to give delay)
                        }, 30000);
                    });
                }
            }
            catch (e)
            { }
            $scope.TimerTocallBackNewsFeed = function () {
                $scope.TimerForLatestFeed();
                NewsFeedUniqueTimer = $timeout(function () {
                    $scope.TimerTocallBackNewsFeed();
                }, 30000);
            }

            $('#CalenderFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = $.grep(getEntityNewsFeedResult.Response, function (e) { return e.FeedId == parseInt(feedid); });
                $("#Overviewfeed_" + feedid).next('div').hide();
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            });

            $('#CalenderFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>'; //should always be current userid
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';

                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0)
                    $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else
                    $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                $timeout(function () { $('#feedcomment_' + commentuniqueid).html('').focus(); }, 10);
            });

            $('#CalenderFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' +
                        (month < 10 ? '0' : '') + month + '-' +
                        (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);

                    CalenderoverviewService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#CalenderFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });

            $scope.AddNewsFeed = function () {
                if ($("#feedtextholder").text() != "") {
                    return false;
                }
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 2;
                addnewsfeed.EntityID = parseInt($stateParams.ID);
                addnewsfeed.TypeName = "";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#feedcomment').text();
                addnewsfeed.PersonalUserIds = $scope.PersonalUserIdsInCalender;

                CalenderoverviewService.PostFeed(addnewsfeed).then(function (savenewsfeed) {

                });
                $scope.PersonalUserIdsInCalender = [];
                $('#feedcomment').empty();
                if (document.getElementById('feedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                    document.getElementById('feedcomment').innerHTML = '<span id=\'feedtextholder\' class=\'placeholder\'>Write a comment...</span>';
                }

                $scope.TimerForLatestFeed();
            }

        }


        $scope.LoadNewsFeedBlockForCalender();

        //Calender related code ends here

        //Objective related




        GlistFeedgroup();
        function GlistFeedgroup() {
            CalenderoverviewService.GetFeedFilter().then(function (GetFeedglistvalue) {
                $scope.FeedgResult = GetFeedglistvalue.Response;
                $scope.Feedglist = [];
                if ($scope.FeedgResult.length > 0) {
                    for (var i = 0; i < $scope.FeedgResult.length; i++) {
                        $scope.Feedglist.push({ ID: $scope.FeedgResult[i].Id, Name: $scope.FeedgResult[i].FeedGroup });
                    }


                }

            });
        };
        //$scope.CurrentFilterStatus = "All";


        $scope.FeedgroupFilterByOptionChange = function () {
            if ($scope.Newsfilter.FeedgroupMulitipleFilterStatus.length == 0 || $scope.Newsfilter.FeedgroupMulitipleFilterStatus == undefined) {

                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = [];
                $scope.Newsfilter.Feedgrouplistvalues = '-1';
                $scope.LoadNewsFeedBlockForCalender();
            }
            else {

                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = $scope.Newsfilter.FeedgroupMulitipleFilterStatus;
                $scope.Newsfilter.Feedgrouplistvalues = '';
                $scope.Newsfilter.Feedgrouplistvalues = $scope.Newsfilter.FeedgroupMulitipleFilterStatus.join(',');
                $scope.LoadNewsFeedBlockForCalender();

            }

        }



        $('#CalenderFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {

            var entityid = $(this).attr('data-entityid');
            var taskid = $(this).attr('data-taskid');
            var typeid = $(this).attr('data-typeid');
            CalenderoverviewService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    $("#loadNotificationtask").modal("show");
                    $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, entityid, $stateParams.ID]);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });

        $('#CalenderFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
            event.stopImmediatePropagation();

            var entityid = $(this).attr('data-entityid');
            var typeid = $(this).attr('data-typeid');
            var parentid = $(this).attr('data-parentid');
            CalenderoverviewService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                if (result.Response == true) {
                    if (typeid == 11 || typeid == 10)//objective & additional objective
                        $location.path('/mui/planningtool/default/detail/section/' + parentid + '/' + 'objective' + '');
                    else
                        $location.path('/mui/planningtool/default/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }

            });

        });


        $('#CalenderFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
            var TargetControl = $(this);
            var mypage = TargetControl.attr('data-Name');
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        });


        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");

        });


        $('#CalenderFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
            /* Respond to click here */

            var EntityID = $(this).attr('data-parentid');
            var funderqid = $(this).attr('data-entityid');
            CalenderoverviewService.IsActiveEntity(funderqid).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    $('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                }
                else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }

            });
        });


        $('#CalenderFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-costcentreid');
            CalenderoverviewService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                if (result.Response == true) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }

            });
        });


        // Set and Get caret position
        // Written by Viniston, 2014

        //handle negative value in objective creation STARTS HERE

        function SetCursorPosition(ctrID, cursorPosition) {
            var cnt = ctrID.val().length;
            if (cnt > 4) {
                cursorPosition = cursorPosition + 1;
                if (cnt > 5)
                    cursorPosition = cursorPosition - 1;
                if (cnt > 8)
                    cursorPosition = cursorPosition + 1;
                if (cnt > 9)
                    cursorPosition = cursorPosition - 1;
            }
            setTimeout(function () {
                ctrID.caret(cursorPosition);
            }, 0);
        }

        function FinancialValidation(event) {

            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            }
            else {

                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {

            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" "))
                return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;

            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);


        }


        function handleTextChangeEvents() {

            var allocatedamountList = $('#ObjectiveNumericGlobalbaseline ,#ObjectiveNumericGlobaltarget');
            for (var i = 0; i < allocatedamountList.length; i++) {

                $(allocatedamountList[i]).keydown(function (event) {



                    var caretPos = $(this).caret();
                    if ($(this).val().contains("-")) {
                        if (event.keyCode === 189 || event.key === "-") {
                            return false;
                            event.preventDefault();
                        }

                    }


                    if ($(this).val().contains(".")) {
                        if (event.keyCode === 190) {
                            return false;
                            event.preventDefault();
                        }
                        else {
                            var dotindex = $(this).val().indexOf(".")
                            if (caretPos > (dotindex + 2)) {
                                return false;
                                event.preventDefault();
                            }
                        }
                    }

                    return FinancialValidation(event);



                }).change(function (event) {
                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                })

                    .focusout(function (event) {



                        if ($(this).val().length == 0 || $(this).val() == 'NaN') {

                            $(this).val('0');
                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));

                            event.stopPropagation();
                            return false;
                        }

                        else {

                            if (event.keyCode == 9) {

                                $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                event.stopPropagation();
                                return false;

                            } else {
                                $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));

                            }
                        }

                    }).keyup(function (event) {

                        if ($(this).val().length == 0 || $(this).val() == 'NaN') {

                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                            $(this).val('');
                            event.stopPropagation();
                            return false;
                        }
                        else if ($(this).val().length == 0 || $(this).val() == '-') {

                            $(this).val('-');
                            //$(event.target).val(CurrencyFormat(Number($(event.target).val().replace(/ /g, ''))));

                            event.stopPropagation();
                            return false;
                        }
                        else {

                            if (event.keyCode != 9) {

                                var caretPos = $(this).caret();

                                $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                                SetCursorPosition($(this), caretPos);
                                if ($(this).val().length == 11) {
                                    $(this).val($(this).val().slice(0, -1));
                                    if (event.keyCode != 9) {
                                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                                    } else {
                                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                    }
                                }
                                else if ($(this).val().length == 13) {
                                    $(this).val($(this).val().slice(0, -2));
                                    if (event.keyCode != 9) {
                                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                                    } else {
                                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                    }
                                }

                            }
                            else {
                                $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                event.stopPropagation();
                                return false;
                            }
                        }

                    });


            }

        }


        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detail.overviewCtrl']"));
        });

    }
    app.controller("mui.planningtool.calender.detail.overviewCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$stateParams', '$location', '$window', '$translate', 'CalenderoverviewService', muiplanningtoolcalenderdetailoverviewCtrl]);
})(angular, app);
///#source 1 1 /app/services/calenderoverview-service.js
(function(ng,app){function CalenderoverviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GettingCalenderFulfillmentBlockDetails:GettingCalenderFulfillmentBlockDetails,GetAllTabCollections:GetAllTabCollections,getEntitiesfrCalender:getEntitiesfrCalender,GetFulfillmentAttribute:GetFulfillmentAttribute,GetFulfillmentAttributeOptions:GetFulfillmentAttributeOptions,GetFulfillmentEntityTypesfrCal:GetFulfillmentEntityTypesfrCal,GetMember:GetMember,GettingEditCalenderFulfillmentDetails:GettingEditCalenderFulfillmentDetails,UpdateCalenderFulfillmentCondition:UpdateCalenderFulfillmentCondition,UpdatingCalenderOverDetails:UpdatingCalenderOverDetails,UpdateObjectiveOwner:UpdateObjectiveOwner,SaveCalenderDetails:SaveCalenderDetails,GetEnityFeeds:GetEnityFeeds,GetLastEntityFeeds:GetLastEntityFeeds,InsertFeedComment:InsertFeedComment,PostFeed:PostFeed,GetFeedFilter:GetFeedFilter,IsActiveEntity:IsActiveEntity})
function GettingCalenderFulfillmentBlockDetails(CalID){var request=$http({method:"get",url:"api/planning/GettingCalenderFulfillmentBlockDetail/"+CalID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllTabCollections(TypeID){var request=$http({method:"get",url:"api/common/GetCustomTabsByTypeID/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function getEntitiesfrCalender(CalenderFulfill){var request=$http({method:"post",url:"api/planning/GetEntitiesfrCalender/",params:{action:"add"},data:CalenderFulfill});return(request.then(handleSuccess,handleError));}
function GetFulfillmentAttribute(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetFulfillmentAttribute/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFulfillmentAttributeOptions(AttributeID,AttributeLevel){var request=$http({method:"get",url:"api/Metadata/GetFulfillmentAttributeOptions/"+AttributeID+"/"+AttributeLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFulfillmentEntityTypesfrCal(){var request=$http({method:"get",url:"api/Metadata/GetFulfillmentEntityTypesfrCal/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMember(EntityID){var request=$http({method:"get",url:"api/Planning/GetMember/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingEditCalenderFulfillmentDetails(CalenderId){var request=$http({method:"get",url:"api/Planning/GettingEditCalenderFulfillmentDetails/"+CalenderId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateCalenderFulfillmentCondition(formobj){var request=$http({method:"post",url:"api/Planning/UpdateCalenderFulfillmentCondition/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdatingCalenderOverDetails(formobj){var request=$http({method:"post",url:"api/Planning/UpdatingCalenderOverDetails/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateObjectiveOwner(formobj){var request=$http({method:"post",url:"api/Planning/UpdateObjectiveOwner/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function SaveCalenderDetails(formobj){var request=$http({method:"post",url:"api/Planning/SaveCalenderDetails/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateNavigationByID(formobj){var request=$http({method:"put",url:"api/common/UpdateNavigationByID/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetLastEntityFeeds(EntityID,Feedsgroupid){var request=$http({method:"get",url:"api/common/GetLastEntityFeeds/"+EntityID+"/"+Feedsgroupid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function InsertFeedComment(addfeedcomment){var request=$http({method:"post",url:"api/common/InsertFeedComment/",params:{action:"add",},data:addfeedcomment});return(request.then(handleSuccess,handleError));}
function PostFeed(addnewsfeed){var request=$http({method:"post",url:"api/common/PostFeed/",params:{action:"add",},data:addnewsfeed});return(request.then(handleSuccess,handleError));}
function GetFeedFilter(){var request=$http({method:"get",url:"api/common/GetFeedFilter/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
function GetEnityFeeds(EntityID, pageNo, Feedsgroupid) { var request = $http({ method: "get", url: "api/common/GetEnityFeeds/" + EntityID + "/" + pageNo + "/" + Feedsgroupid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CalenderoverviewService",['$http','$q',CalenderoverviewService]);})(angular,app);
