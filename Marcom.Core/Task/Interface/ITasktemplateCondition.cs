﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface ITasktemplateCondition
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }


        int TaskTempID
        {
            get;
            set;

        }

        int TypeID
        {
            get;
            set;

        }

        int AttributeID
        {
            get;
            set;

        }

        int AttributeTypeID
        {
            get;
            set;

        }
        int AttributeLevel
        {
            get;
            set;

        }

        string Value
        {
            get;
            set;

        }


        int SortOder
        {
            get;
            set;

        }

        int ConditionType
        {
            get;
            set;

        }

       

        #endregion
    }
}
