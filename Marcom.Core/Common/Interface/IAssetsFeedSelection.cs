﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IAssetsFeedSelection
    {
        int FeedId { get; set; }
        string UserName { get; set; }
        string UserEmail { get; set; }
        string UserImage { get; set; }
        string FeedHappendTime { get; set; }
        string FeedText { get; set; }
        int Actor { get; set; }
        int EntityID { get; set; }
        int AssetId { get; set; }
        string Entityname { get; set; }
        string AssetName { get; set; }
        int Versionno { get; set; }
        int ActiveFileID { get; set; }
        string Assetimagepath { get; set; }
        int ProcessType { get; set; }
        int Category { get; set; }
        int EntitytypeId { get; set; }
        bool Isobjective { get; set; }
        //IList<IFeedComment> FeedComment { get; set; }
    }
}
