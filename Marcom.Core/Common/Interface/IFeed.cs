using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFeed interface for table 'CM_Feed'.
    /// </summary>
    public interface IFeed
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Actor
        {
            get;
            set;

        }

        int Templateid
        {
            get;
            set;

        }

        DateTimeOffset HappenedOn
        {
            get;
            set;

        }

        DateTimeOffset CommentedUpdatedOn
        {
            get;
            set;

        }

        int? Entityid
        {
            get;
            set;

        }

        string TypeName
        {
            get;
            set;

        }

        string AttributeName
        {
            get;
            set;

        }

        string FromValue
        {
            get;
            set;

        }

        string ToValue
        {
            get;
            set;

        }
        string AttributeGroupRecordName
        {
            get;
            set;

        }
        IList<IFeedComment> FeedComment
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }
        int? AssocitedEntityID
        {
            get;
            set;

        }

        int? Version
        {
            get;
            set;

        }
        int? PhaseID
        {
            get;
            set;

        }
        string  StepID
        {
            get;
            set;

        }
        #endregion
    }
}
