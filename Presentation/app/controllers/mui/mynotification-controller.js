﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.mynotificationCtrl",
    function muimynotificationCtrl($scope, $cookies, $resource, $compile, $window, $timeout, $location, $translate, MynotificationService, $modal) {

        //---------------------------  Get the initial feed ------------------------------------
        // --- Define Scope Variables. ---------------------- //
        // --- Bind To Scope Events. ------------------------ //
        // I handle changes to the request context.
        // --- Initialize. ---------------------------------- //
        //---------------------------  Get the initial feed ------------------------------------

        $scope.Allnotifications = [];
        $(window).AdjustHeightWidth();
        GetAllNotifications();
        $scope.muiiscalender.Iscalender = false;
        $scope.muiiscalender.CalenderID = 0;
        function GetAllNotifications() {
            NotificationPagenoforScroll = 0;
            $('#mypagenotificationinfo').html('');
            MynotificationService.GetNotificationForMypageSettings(1, 0).then(function (getnotification) {
                if (getnotification.Response != null) {

                    if (getnotification.Response.m_Item1 == '') {
                        $('#nonotificationforviewall').show();
                    }
                    else {
                        $('#nonotificationforviewall').hide();

                        var notificationdivHtml = '';
                        for (var k = 0; k < getnotification.Response.m_Item1.length; k++) {
                            notificationdivHtml = notificationdivHtml + '<ul id="notifyulformypage" class="feedcontent registerevent"><li><span class="avatar">';
                            notificationdivHtml = notificationdivHtml + '<img alt="Avatar" src="Handlers/UserImage.ashx?id=' + getnotification.Response.m_Item1[k].UserId + '"></span>';
                            notificationdivHtml = notificationdivHtml + '<div data-parent="parentforcomment" class="block">';
                            notificationdivHtml = notificationdivHtml + '<div class="arrow-left"></div>';
                            notificationdivHtml = notificationdivHtml + '<span class="header"><a href="">' + getnotification.Response.m_Item1[k].UserName + '</a></span>';
                            notificationdivHtml = notificationdivHtml + '<span class="body">' + getnotification.Response.m_Item1[k].NotificationText + '</span>';
                            notificationdivHtml = notificationdivHtml + '<span class="footer"><span class="time">' + getnotification.Response.m_Item1[k].NotificationHappendTime + '</span>';
                            notificationdivHtml = notificationdivHtml + '</span></div></li></ul>';
                        }
                        $('#mypagenotificationinfo').html($compile(notificationdivHtml)($scope));
                        $('#mypagenotificationinfo').scrollTop(0);
                        $scope.NotificationPagenoforScrollReloadOnScroll();
                        registerclickevents();
                    }
                }
            });
        };

        var NotificationPagenoforScroll = 0;
        $scope.NotificationPagenoforScrollReloadOnScroll = function () {
            try {
                $('#mypagenotification').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        NotificationPagenoforScroll += 1;
                        MynotificationService.GetNotificationForMypageSettings(1, NotificationPagenoforScroll).then(function (getnotification) {
                            if (getnotification.Response != null) {
                                var notificationdivHtml = '';
                                for (var k = 0; k < getnotification.Response.m_Item1.length; k++) {
                                    notificationdivHtml = notificationdivHtml + '<ul id="notifyulformypage" class="feedcontent registerevent"><li><span class="avatar">';
                                    notificationdivHtml = notificationdivHtml + '<img alt="Avatar" src="Handlers/UserImage.ashx?id=' + getnotification.Response.m_Item1[k].UserId + '"></span>';
                                    notificationdivHtml = notificationdivHtml + '<div data-parent="parentforcomment" class="block">';
                                    notificationdivHtml = notificationdivHtml + '<div class="arrow-left"></div>';
                                    notificationdivHtml = notificationdivHtml + '<span class="header"><a href="">' + getnotification.Response.m_Item1[k].UserName + '</a></span>';
                                    notificationdivHtml = notificationdivHtml + '<span class="body">' + getnotification.Response.m_Item1[k].NotificationText + '</span>';
                                    notificationdivHtml = notificationdivHtml + '<span class="footer"><span class="time">' + getnotification.Response.m_Item1[k].NotificationHappendTime + '</span>';
                                    notificationdivHtml = notificationdivHtml + '</span></div></li></ul>';
                                }
                                $('#mypagenotificationinfo').append($compile(notificationdivHtml)($scope));
                                //$scope.NotificationPagenoforScrollReloadOnScroll();
                                registerclickevents();
                            }
                        });
                    }
                });
            } catch (e) {
            }
        };

        function registerclickevents() {
            $('.registerevent').on('click', 'a[data-id="notifypath"]', function (event) {
                var IsObjective = false;
                if ($(event.target).attr('data-IsObjective') != undefined)
                    IsObjective = $(event.target).attr('data-IsObjective');
                event.stopImmediatePropagation();
                var entitytypeid = $(this).attr('data-typeid');
                var entityId = $(this).attr('data-entityid');
                MynotificationService.GetModuleID(entityId).then(function (CheckModule) {
                    $scope.Moduleid = CheckModule.Response[0].ModuleID;
                    MynotificationService.CheckUserPermissionForEntity(entityId).then(function (CheckUserPermissionForEntity) {
                        if (CheckUserPermissionForEntity.Response == true && $scope.Moduleid != 6) {
                            if (entitytypeid == 35) {
                                $location.path('/mui/calender/detail/section/' + entityId + '/overview');
                            }
                            else if (IsObjective == "True")
                                $location.path('/mui/objective/detail/section/' + entityId + '/overview');
                            else
                                $location.path('/mui/planningtool/default/detail/section/' + entityId + '/overview');
                        }
                        else if (CheckUserPermissionForEntity.Response == true && $scope.Moduleid == 6) {
                            MynotificationService.GetCmsEntitiesByID(entityId).then(function (resNavID) {
                                if (resNavID.Response != null) {
                                    $scope.GlobalNavigationID.NavigationID = resNavID.Response[0].NavID.toString();
                                    $cookies["GlobalNavigationID"] = resNavID.Response[0].NavID.toString();
                                    $location.path('/mui/cms/detail/list/' + entityId + '/entitycontent');
                                }
                            });
                        }
                        else
                            bootbox.alert($translate.instant('LanguageContents.Res_1867.Caption'));
                    });
                });
            });

            $('.registerevent').on('click', 'a[data-id="openpopupfortaskeditforviewall"]', function (event) {
                event.stopImmediatePropagation();

                var taskID = $(this).attr('data-taskid');
                var entityID = $(this).attr('data-entityid');
                /************** Load taskedit page **************/

                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskEdit.html',
                    controller: "mui.task.muitaskFlowEditCtrl",
                    resolve: {
                        items: function () {
                            return { TaskId: taskID, EntityId: entityID };
                        }
                    },
                    scope: $scope,
                    windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                    backdrop: "static"
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    //console.log('Modal dismissed at: ' + new Date());
                });

            });

            $('.registerevent').on('click', 'a[data-id="openpopupfortaskedit"]', function (event) {
                event.stopImmediatePropagation();
                /************** Load taskedit page **************/

                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskEdit.html',
                    controller: "mui.task.muitaskFlowEditCtrl",
                    resolve: {
                        items: function () {
                            return { TaskId: $(this).attr('data-taskid'), EntityId: $(this).attr('data-entityid') };
                        }
                    },
                    scope: $scope,
                    windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                    backdrop: "static"
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    //console.log('Modal dismissed at: ' + new Date());
                });
            });

            $('.registerevent').on('click', 'a[data-id="costcenterlink"]', function (event) {
                event.stopImmediatePropagation();
                var EntityID = $(this).attr('data-costcentreid');
                MynotificationService.CheckUserPermissionForEntity(EntityID).then(function (CheckUserPermissionForEntity) {
                    if (CheckUserPermissionForEntity.Response == true && $scope.Moduleid != 6)
                        $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID + '/overview');
                });
            });

            $('.registerevent').on('click', 'a[data-id="openpopupforfundrequestforviewall"]', function (event) {
                event.stopImmediatePropagation();

                var funderqid = $(this).attr('data-entityid');
                $("#feedFundingRequestModal").modal("show");
                //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);

            });
            $('.registerevent').on('click', 'a[data-id="Assetpath"]', function (event) {
                event.stopImmediatePropagation();
                var Assetid = $(this).attr('data-Assetid');
                MynotificationService.IsAvailableAsset(Assetid).then(function (result) {
                    if (result.Response == true) {
                        /********** Load assetedit page **********/
                        var modalInstance = $modal.open({
                            templateUrl: 'views/mui/DAM/assetedit.html',
                            controller: "mui.DAM.asseteditCtrl",
                            resolve: {
                                params: function () {
                                    return { AssetID: Assetid, IsLock: false, isNotify: true, viewtype: 'ViewType' };
                                }
                            },
                            scope: $scope,
                            windowClass: 'iv-Popup',
                            backdrop: "static"
                        });
                        modalInstance.result.then(function (selectedItem) {
                            $scope.selected = selectedItem;
                        }, function () { });
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $(window).on("ontaskActionnotification", function (event) {
                $("#loadNotificationtask").modal("hide");
            });
        }
        //----- notification upDATE
        $scope.updateallnotifications = function () {

            var updatestae = {};
            updatestae.UserID = $cookies['UserId'];
            updatestae.FirstFiveNotifications = '';
            updatestae.Flag = 1;
            MynotificationService.UpdateIsviewedStatusNotification(updatestae).then(function (updateviewednotifications) {
                var a = updateviewednotifications.Response;
                $(document).trigger("OnViewedAllNotifications");
            });

        };


        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.mynotificationCtrl']"));
        });
    }
    //);
    app.controller("mui.mynotificationCtrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$timeout', '$location', '$translate', 'MynotificationService', '$modal', muimynotificationCtrl]);
    function MynotificationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetNotificationForMypageSettings: GetNotificationForMypageSettings,
            IsAvailableAsset: IsAvailableAsset,
            CheckUserPermissionForEntity: CheckUserPermissionForEntity,
            GetModuleID: GetModuleID,
            UpdateIsviewedStatusNotification: UpdateIsviewedStatusNotification,
            GetCmsEntitiesByID: GetCmsEntitiesByID
        });
        function GetNotificationForMypageSettings(flag, pageNo) {
            var request = $http({
                method: "get",
                url: "api/common/GetNotificationForMypageSettings/" + flag + "/" + pageNo,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function IsAvailableAsset(AssetID) {
            var request = $http({
                method: "get",
                url: "api/common/IsAvailableAsset/" + AssetID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function CheckUserPermissionForEntity(EntitiyID) {
            var request = $http({
                method: "get",
                url: "api/common/CheckUserPermissionForEntity/" + EntitiyID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetModuleID(EntityId) {
            var request = $http({
                method: "get",
                url: "api/common/GetModuleID/" + EntityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function UpdateIsviewedStatusNotification(updatestae) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateIsviewedStatusNotification/",
                params: {
                    action: "add"
                },
                data: {
                    UserID: updatestae.UserID,
                    FirstFiveNotifications: updatestae.FirstFiveNotifications,
                    Flag: updatestae.Flag
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetCmsEntitiesByID(CmsEntityID) { var request = $http({ method: "get", url: "api/cms/GetCmsEntitiesByID/" + CmsEntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("MynotificationService", ['$http', '$q', MynotificationService]);
})(angular, app);