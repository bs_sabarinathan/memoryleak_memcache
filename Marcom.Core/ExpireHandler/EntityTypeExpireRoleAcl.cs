﻿using BrandSystems.Marcom.Core.Access;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ExpireHandler
{
    public class EntityTypeExpireRoleAcl : IEntityTypeExpireRoleAcl 
    {
        	#region Member Variables

        protected int _id;
        protected int _entityroleid;
        protected int _moduleid;
        protected int _entitytypeid;
        protected int _sortorder;
        protected string _caption;
		
		
		#endregion
		
		#region Constructors
		public EntityTypeExpireRoleAcl() {}

        public EntityTypeExpireRoleAcl(int pId, string _pCaption, int pEntityroleid, int pModuleid, int pEntityTypeid, int pSortOrder)
		{
            this._id = pId;
            this._entityroleid = pEntityroleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._sortorder = pSortOrder;
            this._caption = _pCaption;
		}
		
		#endregion
		
		#region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set {  _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual int EntityRoleID
        {
            get { return _entityroleid; }
            set {  _entityroleid = value; }

        }

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set {  _moduleid = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set {  _entitytypeid = value; }

        }

        public virtual int Sortorder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
        public int ExpireRoleID
        {
            get;
            set;
        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			GlobalAcl castObj = null;
			try
			{
				castObj = (GlobalAcl)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
    }
}
