﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtool.myworkspacesCtrl",
    function muiplanningtoolmyworkspacesCtrl($scope, $cookies, $resource, $compile, $window, $timeout, $location) {
        //------------------------------------------------------------Ended By Rajkumar------------------------------------------------------------------------------
        // --- Define Controller Variables. ----------------- //
        $scope.workspaceEntityColor = '';
        $scope.workspaceEntityTypeID = '';
        $scope.workspaceEntityShortDesc = '';
        $scope.muiiscalender.Iscalender = false;
        $scope.muiiscalender.CalenderID = 0;
        //-------------- user defined functions ---------

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.myworkspacesCtrl']"));
        });
    }
    //);
    app.controller("mui.planningtool.myworkspacesCtrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$timeout', '$location', muiplanningtoolmyworkspacesCtrl]);
})(angular, app);