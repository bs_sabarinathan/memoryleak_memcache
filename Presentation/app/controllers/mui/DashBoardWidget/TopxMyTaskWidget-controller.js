﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetTopxMyTaskWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, $translate, TopxmytaskwidgetService) {
        $scope.TopxMyTaskdivID = "Wizarddiv_" + $scope.WizardID;
        $scope.TopxMyTasks = [];
        $scope.TopxNoOfItem = $scope.WizardNoOfItem;
        $scope.TaskByIDDetail = [];
        $timeout(function () { LoadTopxMyTask(); }, 100);

        function LoadTopxMyTask() {
            TopxmytaskwidgetService.GetTopMyTaskByID(parseInt($cookies['UserId']), $scope.TopxNoOfItem).then(function (Gettopxmytask) {
                $scope.TopxMyTasks = Gettopxmytask.Response;
                $('#' + $scope.TopxMyTaskdivID + '').parents('.box-content').find('.InProgress').remove();
            });
            $scope.OpenPopUpAction = function (TaskID, taskTypeId, taskTypeName, PEnityname, PEnityTypename, PEntityId) {
                if (taskTypeId > 0 && taskTypeId != 7) {
                    $("#loadNotificationtask").modal("show");
                    $("#Notificationtaskedit").trigger('TopMyTaskAction', [TaskID, PEntityId]);
                } else if (taskTypeId > 0 && taskTypeId == 7) {
                    $("#feedFundingRequestModal").modal("show");
                    $('#feedFundingRequestModal').trigger("TopMyTaskCostCentreFundingRequestsAction", [TaskID]);
                }
            }
        }
        $scope.TaskBriefDetails = {
            taskID: 0,
            taskTypeId: 0,
            taskTypeName: "",
            PEnityname: "",
            PEnityTypename: "",
            EntityID: 0,
            ownerId: 0,
            stepID: 0,
            taskName: "",
            dueIn: 0,
            dueDate: "",
            status: "",
            statusID: 0,
            taskOwner: "",
            Description: "",
            taskmembersList: [],
            taskAttachmentsList: []
        };
        $scope.listPredfWorkflowFilesAttch = [];
        $scope.TaskMemberList = [];
        $scope.ShowCompleteBtn = false;
        $scope.ShowApproveBtn = false;
        $scope.attachedFiles = [];

        function BindTaskDetails(TaskObject, taskTypeId, taskTypeName, PEnityname, PEnityTypename) {
            for (var i = 0; i < TaskObject.length; i++) {
                $scope.listPredfWorkflowFilesAttch = TaskObject[i].TaskAttachment;
                var isThisMemberPresent = $.grep(TaskObject[i].TaskMembers, function (e) {
                    return e.Userid == parseInt($cookies['UserId']);
                });
                if (taskTypeId == 2) {
                    if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 0) {
                        $scope.ShowCompleteBtn = true;
                    }
                    $scope.ShowApproveBtn = false;
                } else if (taskTypeId == 3) {
                    if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 0) {
                        $scope.ShowApproveBtn = true;
                    }
                    $scope.ShowCompleteBtn = false;
                }
                $scope.TaskBriefDetails.taskName = TaskObject[i].Name;
                $scope.TaskBriefDetails.taskTypeId = taskTypeId;
                $scope.TaskBriefDetails.taskTypeName = taskTypeName;
                $scope.TaskBriefDetails.PEnityname = PEnityname;
                $scope.TaskBriefDetails.PEnityTypename = PEnityTypename;
                var dateval = "";
                var datStartUTCval = "";
                var datstartval = "";
                if (TaskObject[i].DueDate != undefined) dateval = TaskObject[i].DueDate;
                datStartUTCval = dateval.substr(6, (dateval.indexOf('+') - 6));
                datstartval = new Date(parseInt(datStartUTCval));
                $scope.TaskBriefDetails.dueDate = dateFormat(datstartval, $scope.DefaultSettings.DateFormat).toString();
                $scope.TaskBriefDetails.dueIn = TaskObject[i].Duedates;
                $scope.TaskBriefDetails.taskOwner = TaskObject[i].TaskOwnerName;
                $scope.TaskBriefDetails.status = TaskObject[i].TaskStatusName;
                $scope.TaskBriefDetails.statusID = TaskObject[i].Status;
                $scope.TaskBriefDetails.Description = TaskObject[i].Description;
                $scope.TaskBriefDetails.taskID = TaskObject[i].Id;
                $scope.TaskBriefDetails.ownerId = TaskObject[i].MemberID;
                $scope.TaskBriefDetails.EntityID = TaskObject[i].EntityId;
                $scope.TaskBriefDetails.stepID = TaskObject[i].StepID;
                var membersWithoutOwner = $.grep(TaskObject[i].TaskMembers, function (e) {
                    return e.Roleid != 1;
                });
                $scope.TaskBriefDetails.taskmembersList = membersWithoutOwner;
                $scope.TaskMemberList = TaskObject[i].TaskMembers;
                $scope.TaskBriefDetails.taskAttachmentsList = TaskObject[i].TaskAttachment;
                $scope.attachedFiles = TaskObject[i].TaskAttachment;
                $scope.groupBy('Roleid', false);
            }

            function sortOn(collection, name) { }
            $scope.sortby = function (collection, name) {
                collection.sort(function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                });
            }
        }
        $scope.groupBy = function (attribute, ISglobal) {
            $scope.TaskMembergroups = [];
            var groupValue = "_INVALID_GROUP_VALUE_";
            for (var i = 0; i < $scope.TaskMemberList.length; i++) {
                var friend = $scope.TaskMemberList[i];
                if (friend[attribute] !== groupValue) {
                    var group = {
                        label: friend[attribute],
                        friends: [],
                        rolename: friend.Role == "Owner" ? "Task Owner" : "Task Assignee"
                    };
                    groupValue = group.label;
                    $scope.TaskMembergroups.push(group);
                }
                group.friends.push(friend);
            }
        };
        $scope.TaskMembergroups = [];
        $scope.UpdateTaskStatus = function (taskID, StatusID) {
            var TaskStatusData = {};
            TopxmytaskwidgetService.UpdateTaskStatus(taskID, StatusID).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $('#WorkTaskModal').modal('hide');
                    cacheReault();
                }
            });
            $timeout(function () { LoadTopxMyTask(); }, 100);
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.TopxMyTaskWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.TopxMyTaskWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', '$translate', 'TopxmytaskwidgetService', muiDashBoardWidgetTopxMyTaskWidgetCtrl]);
    function TopxmytaskwidgetService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetTopMyTaskByID: GetTopMyTaskByID,
            UpdateTaskStatus: UpdateTaskStatus
        });
        function GetTopMyTaskByID(UserID, Topx) { var request = $http({ method: "get", url: "api/Metadata/GetTopMyTaskByID/" + UserID + "/" + Topx, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateTaskStatus(EntityID, Status) { var request = $http({ method: "put", url: "api/Planning/UpdateTaskStatus/" + EntityID + "/" + Status, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("TopxmytaskwidgetService", ['$http', '$q', TopxmytaskwidgetService]);



})(angular, app);