﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IEntityTypeAttributeGroupRelationsWithValues
    {
        #region Public Properties

        int ID
        {
            get;
            set;
        }

        int EntityTypeID
        {
            get;
            set;

        }
        string EntityTypeCaption
        {
            get;
            set;

        }
        int AttributeID
        {
            get;
            set;

        }

        string AttributeCaption
        {
            get;
            set;

        }
        int AttributeTypeID
        {
            get;
            set;
        }
        string ValidationID
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }
        string DefaultValue
        {
            get;
            set;

        }

        bool InheritFromParent
        {
            get;
            set;

        }

        bool IsReadOnly
        {
            get;
            set;

        }

        bool IsSpecial
        {
            get;
            set;

        }

        bool ChooseFromParentOnly
        {
            get;
            set;

        }

        bool IsValidationNeeded
        {
            get;
            set;

        }
        string Caption
        {
            get;
            set;

        }

        bool IsSystemDefined
        {
            get;
            set;

        }
        string PlaceHolderValue
        {
            get;
            set;

        }
        dynamic AttributeValue
        {
            get;
            set;
        }

        dynamic ParentValue { get; set; }
        dynamic ParentTreeLevelValueCaption { get; set; }
        dynamic Lable { get; set; }
        string strAttributeID { get; set; }
        #endregion
    }

    public interface IEntityTypeAttributeGroupRelationwithLevels : IEntityTypeAttributeGroupRelationsWithValues
    {
        IList<ITreeLevel> Levels { get; set; }
        IList<IOption> Options { get; set; }
        string tree { get; set; }
    }

    public interface IAttributeGroupTreeDropDownLabel
    {
        int Level { get; set; }
        string Label { get; set; }
    }

    public interface IAttributeGroupTreeDropDownCaption
    {
        int Level { get; set; }
        string Caption { get; set; }
    }
}
