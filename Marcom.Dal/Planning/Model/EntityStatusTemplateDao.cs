﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    public partial class EntityStatusTemplateDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected string _templateName;
        protected string _templateDescription;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors

        public EntityStatusTemplateDao() { }


        public EntityStatusTemplateDao(int pID)
        {
            this._id = pID;
        }

        public EntityStatusTemplateDao(int pID, string pTemplateName, string pTemplateDescription)
        {
            this._id = pID;
            this._templateName = pTemplateName;
            this._templateDescription = pTemplateDescription;
        }
        #endregion
        #region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
        public virtual string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }

        }
        public virtual string TemplateDescription
        {
            get { return _templateDescription; }
            set { _templateDescription = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            // public const string EntityOverallStatus = "EntityOverallStatus";
            public const string ID = "ID";
            public const string TemplateName = "TemplateName";
            public const string TemplateDescription = "TemplateDescription";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            // public const string EntityOverallStatus = "EntityOverallStatus";
            public const string ID = "ID";
            public const string TemplateName = "TemplateName";
            public const string TemplateDescription = "TemplateDescription";
        }


        #endregion

    }
}
