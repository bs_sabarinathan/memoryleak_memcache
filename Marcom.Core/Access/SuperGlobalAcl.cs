﻿using BrandSystems.Marcom.Core.Access.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access
{
    public class SuperGlobalAcl : ISuperGlobalAcl
    {

        #region Member Variables

        protected int _id;
        protected int _globalroleid;
        protected int _menuid;
        protected int _featureid;
        protected bool _accesspermission;


        #endregion

        #region Constructors
		public SuperGlobalAcl() {}

        public SuperGlobalAcl(int pId, int pGlobalRoleid, int pMenuid, int pEntityTypeid, int pFeatureid, bool pAccessPermission)
		{
            this._id = pId;
			this._globalroleid = pGlobalRoleid; 
			this._menuid = pMenuid;            
			this._featureid = pFeatureid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion


        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int GlobalRoleid
        {
            get { return _globalroleid; }
            set { _globalroleid = value; }

        }

        public int Menuid
        {
            get { return _menuid; }
            set { _menuid = value; }

        }

        //public int EntityTypeid
        //{
        //    get { return _entitytypeid; }
        //    set { _entitytypeid = value; }

        //}

        public int Featureid
        {
            get { return _featureid; }
            set { _featureid = value; }

        }

        public bool AccessPermission
        {
            get { return _accesspermission; }
            set { _accesspermission = value; }

        }


        #endregion 

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            GlobalAcl castObj = null;
            try
            {
                castObj = (GlobalAcl)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
