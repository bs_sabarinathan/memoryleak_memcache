///#source 1 1 /app/controllers/mui/planningtool/costcentre/list-controller.js
(function(ng,app){"use strict";function muiplanningtoolcostcentrelistCtrl($scope,$timeout,$http,$compile,$resource,$location,$window,$cookies,$cookieStore,$translate,CclistService,$modal){$scope.TreeClassList=[];$scope.CostCentreEntityID=0;$scope.CostCentreEntityType="";$scope.PageSize=1;$scope.IsSelectAllChecked=false;$scope.createCostCentre=function(){var modalInstance=$modal.open({templateUrl:'views/mui/planningtool/costcentre/costcentrecreation.html',controller:"mui.planningtool.costcentre.costcentrecreationCtrl",resolve:{params:function(){return 0;}},scope:$scope,windowClass:'popup-widthL',backdrop:"static"});modalInstance.result.then(function(selectedItem){$scope.selected=selectedItem;},function(){});$timeout(function(){$("#Div_listCC").trigger('loadAttributeMetadata',[0,0]);},100)}
$scope.appliedfilter="No filter applied";$scope.SelectedFilterID=0;$scope.AddEditFilter="Add new filter";CclistService.GetFilterSettings(5).then(function(filterSettings){$scope.filterValues=filterSettings.Response;});$timeout(function(){$("#rootlevelfilter").trigger('LoadRootLevelFilter',[5,RememberCCFilterAttributes]);},500);$scope.DetailFilterCreation=function(event){if($scope.SelectedFilterID==0){$('.FilterHolder').slideDown("slow")
$scope.deletefiltershow=false;$("#rootlevelfilter").trigger('ClearScope',$scope.EntityTypeID);}
else{$timeout(function(){$("#rootlevelfilter").trigger('EditFilterSettingsByFilterID',[$scope.SelectedFilterID,5]);},20);$scope.showSave=false;$scope.showUpdate=true;$('.FilterHolder').slideDown("slow")}};$scope.hideFilterSettings=function(){$('.FilterHolder').slideUp("slow")
$timeout(function(){$(window).AdjustHeightWidth();},500);}
$scope.ApplyFilter=function(filterid,filtername){$scope.SelectedFilterID=filterid;if(filterid!=0){$scope.AddEditFilter='Edit filter';$scope.appliedfilter=filtername;$('.FilterHolder').slideUp("slow")}
else{$scope.appliedfilter=filtername;$('.FilterHolder').slideUp("slow")
$scope.AddEditFilter=$translate.instant('LanguageContents.Res_402.Caption');}
$("#rootlevelfilter").trigger('ApplyRootLevelFilter',[filterid,filtername]);}
function GetCostCenterEntityType(){CclistService.GetEntityTypeByID(5).then(function(res){if(res.Response!=null){$scope.CostCentreEntityType=res.Response;}});}
GetCostCenterEntityType();$("#Div_listCC").on('ReloadFilterSettings',function(event,TypeID,filtername,filterid){$scope.SelectedFilterID=filterid;if(filtername!=''){$scope.appliedfilter=filtername;$("#rootlevelfilter").trigger('EditFilterSettingsByFilterID',[filterid,TypeID]);$scope.AddEditFilter='Edit filter';}
if(filtername==$translate.instant('LanguageContents.Res_401.Caption')){$scope.appliedfilter=filtername;$scope.AddEditFilter=$translate.instant('LanguageContents.Res_402.Caption');}
CclistService.GetFilterSettings(TypeID).then(function(filterSettings){$scope.filterValues=filterSettings.Response;$scope.appliedfilter=filtername;});});$("#Div_listCC").on('ClearAndReApply',function(){$scope.appliedfilter=$translate.instant('LanguageContents.Res_401.Caption');$scope.SelectedFilterID=0;$scope.AddEditFilter=$translate.instant('LanguageContents.Res_402.Caption');});$scope.Level="0";$scope.FilterID={selectedFilterID:0,selectedfilterattribtues:[]};$scope.AppllyFilterObj={selectedattributes:[]};$scope.EntityTypeID=5;var TypeID=5;var CurrentuserId=$scope.UserId;$scope.onTreeDataMouseEnter=function(){var id=this.item.Id;setTimeout('$(\'#treeHolder > li > a[data-entityID="'+id+'"]\').addClass(\'hover\');',50);};$scope.onTreeDataMouseLeave=function(){var id=this.item.Id;setTimeout('$(\'#treeHolder > li > a[data-entityID="'+id+'"]\').removeClass(\'hover\');',50);};$scope.onTreeMouseEnter=function(){var id=this.item.Id;setTimeout('$(\'#listdataHolder > tr[data-entityID="'+id+'"]\').addClass(\'hover\');',50);};$scope.onTreeMouseLeave=function(){var id=this.item.Id;setTimeout('$(\'#listdataHolder > tr[data-entityID="'+id+'"]\').removeClass(\'hover\');',50);};$scope.listColumnDefsdata={};$scope.NameExcludeFilter=function(columndefs){if(columndefs.Field!="Name"){return true;}};$scope.load=function(parameters){};if(ShowAllRemember.CostCentre==true){$scope.DisablePart=true;CurrentuserId=$scope.UserId;$("#btnParticipated").removeClass("active").addClass("active");$("#btnViewAll").removeClass("active");}else{$scope.DisablePart=false;CurrentuserId=0;$("#btnViewAll").removeClass("active").addClass("active");$("#btnParticipated").removeClass("active");}
$scope.tglHide=true;$scope.ShowAll=function(IsActive){$scope.DisablePart=!$scope.DisablePart;$("#ListContainer table").html('');$("#treeHolder ul").html('');$('#RootLevelSelectAll').next('i').removeClass('checked');ClearCheckBoxes();if(ShowAllRemember.CostCentre==true){CurrentuserId=$scope.UserId}
if(IsActive){$("#btnViewAll").removeClass("active").addClass("active");$("#btnParticipated").removeClass("active");CurrentuserId=0;ShowAllRemember.CostCentre=false;}
else{$("#btnParticipated").removeClass("active").addClass("active");$("#btnViewAll").removeClass("active");CurrentuserId=$scope.UserId;ShowAllRemember.CostCentre=true;}
GetRootLevelActivityListCount();if($("#ListColumn > thead tr th").length==1)
$('#ListColumn > thead').html("");};function ClearCheckBoxes(){$scope.IsSelectAllChecked=false;$('#ListContainer > table > tbody input:checkbox').each(function(){this.checked=false;$(this).next('i').removeClass('checked');});}
$scope.DeleteEntity=function(){var IDList=new Array();var ID=0;IDList=GetRootLevelSelectedAll();if(IDList.length!=0){var object={};var deletesearch={};deletesearch.ID=IDList;object.ID=IDList;CclistService.MemberAvailable(object).then(function(memberavailability){if(memberavailability.Response==false){bootbox.alert($translate.instant('LanguageContents.Res_2100.Caption'));return true;}
bootbox.confirm($translate.instant('LanguageContents.Res_16.Caption'),function(result){if(result){$timeout(function(){CclistService.DeleteCostcentre(object).then(function(data){if(data.Response==1){for(var i=0;i<IDList.length;i++){var entityId=IDList[i];$('#treeHolder a[data-entityID='+entityId+']').remove();$('#ListContainer tr[data-entityID='+entityId+']').remove();}
$('#RootLevelSelectAll').next('i').removeClass('checked');ClearCheckBoxes();}
if(data.Response==2){bootbox.alert($translate.instant('LanguageContents.Res_1912.Caption'));}
if(data.Response==3){bootbox.alert($translate.instant('LanguageContents.Res_4850.Caption'));}});},100);}});});}};function SelectEntityIDs(){var IDList=new Array();$('#listdataHolder input:checked').each(function(){IDList.push($(this).parents('tr').attr('data-entityID'));});return IDList}
var IsDesc=false;var sortOrder="";$scope.FilterID.selectedFilterID=0;$scope.FilterID.selectedfilterattribtues=[];$scope.LoadRootLevelCostCenterActivity=function(PageIndex,StartRowNo,MaxNoofRow,filterid,filterattributes){$(window).AdjustHeightWidth();var FilterID=filterid;var CCAttrs=$cookieStore.get('CCAttrs'+parseInt($cookies['UserId'],10));var CCFilterID=$cookieStore.get('CCFilterID'+parseInt($cookies['UserId'],10));var CCFilterName=$cookieStore.get('CCFilterName'+parseInt($cookies['UserId'],10));RemeberCCFilterName=CCFilterName;if(CCAttrs!=undefined){RememberCCFilterAttributes=CCAttrs;}
if(CCFilterID!=undefined){RememberCCFilterID=CCFilterID;}
if(RememberCCFilterID!=0||RememberCCFilterAttributes.length!=0){FilterID=RememberCCFilterID;$scope.FilterID.selectedfilterattribtues=RememberCCFilterAttributes;if(RememberCCFilterAttributes.length>0){}
if(RememberCCFilterID!=0){$scope.appliedfilter=RemeberCCFilterName;$scope.AddEditFilter='Edit filter';$scope.SelectedFilterID=FilterID;}}
if(StartRowNo==0)
$timeout(function(){$("#rootlevelfilter").trigger('LoadRootLevelFilter',[5,RememberCCFilterAttributes]);},10);$scope.FilterID.selectedFilterID=filterid;$scope.FilterID.selectedfilterattribtues=filterattributes;$window.CostCentreFilterName='';var getactivities={};getactivities.StartRowNo=StartRowNo;getactivities.MaxNoofRow=MaxNoofRow;getactivities.FilterID=FilterID;getactivities.SortOrderColumn=$scope.sortOrder;getactivities.IsDesc=$scope.IsDesc;getactivities.IncludeChildren=false;getactivities.EntityID='0';getactivities.UserID=CurrentuserId;getactivities.Level=$scope.Level;getactivities.IDArr=[];getactivities.FilterAttributes=$scope.FilterID.selectedfilterattribtues;CclistService.CostCentreRootLevel(getactivities).then(function(getactivitylist){if(getactivitylist.Response!=null&&getactivitylist.Response.Data!=null){if(getactivitylist.Response.ColumnDefs.length>0){var listColumnDefsdata=getactivitylist.Response.ColumnDefs
var listContent=getactivitylist.Response.Data;var contentnHtml="";var columnHtml="<tr >";var treeHtml="";var colStatus=false;var TreeItem="";for(var i=0;i<listContent.length;i++){if(i!=undefined){var UniqueId=UniqueKEY(listContent[i]["UniqueKey"]);var ClassName=GenateClass(UniqueId);ClassName+=" mo"+UniqueId;contentnHtml+="<tr data-over='true' class='ng-scope"+ClassName+"' data-EntityLevel='"+listContent[i]["Level"]+"' data-entityID="+listContent[i]["Id"]+" data-uniqueKey="+UniqueId+">"
for(var j=0;j<listColumnDefsdata.length;j++){if(j!=undefined&&listColumnDefsdata[j].Field!="68"){if(colStatus==false){if(j!=undefined&&listColumnDefsdata[j].Field==SystemDefiendAttributes.AssignedAmount){columnHtml+="<th>";}else{columnHtml+="<th>";}
columnHtml+="<a  data-Column="+listColumnDefsdata[j].Field+" >";columnHtml+="    <span>"+listColumnDefsdata[j].DisplayName+"</span>";columnHtml+="</a>";columnHtml+="</th>";}
if(j!=undefined&&listColumnDefsdata[j].Field==SystemDefiendAttributes.AssignedAmount)
contentnHtml+="<td><span class='currColumn ng-binding'>"+(listContent[i][listColumnDefsdata[j].Field]!=undefined?listContent[i][listColumnDefsdata[j].Field].formatMoney(0,' ',' '):"-")+" "+listContent[i].CurrencyType.toUpperCase()+"</span></td>";else if(AttributeTypes.DateTime==listColumnDefsdata[j].Type)
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i][listColumnDefsdata[j].Field]==null?"-":dateFormat(listContent[i][listColumnDefsdata[j].Field],$scope.DefaultSettings.DateFormat))+"</span></td>";else
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i][listColumnDefsdata[j].Field]!=undefined?listContent[i][listColumnDefsdata[j].Field]:"-")+"</span></td>";}}
TreeItem+="<li class='ng-scope"+ClassName+"' data-over='true' data-uniqueKey="+UniqueId+"><a data-role='Activity' data-EntityLevel='"+listContent[i]["Level"]+"' data-EntityTypeID='"+listContent[i]["TypeID"]+"'  data-entityID="+listContent[i]["Id"]+"  data-colorcode="+listContent[i]["ColorCode"]+" data-shorttext="+listContent[i]["ShortDescription"]+" data-Permission="+listContent[i].Permission+" data-entityname=\""+listContent[i]["Name"]+"\"> ";TreeItem+=Space(listContent[i]["UniqueKey"]);if(listContent[i]["TotalChildrenCount"]<=0||$scope.IsSingleID==true){TreeItem+="<i class='icon-'></i>";$scope.IsSingleID=false;}
else{TreeItem+="<i data-role='Arrow' data-icon='"+UniqueId+"' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID="+listContent[i]["Id"]+" data-TotalChildrenCount="+listContent[i]["TotalChildrenCount"]+" data-Permission="+listContent[i].Permission+"  data-UniqueId=\""+UniqueId+"\"></i>";}
TreeItem+=" <span data-icon='ActivityIcon' data-ParentID="+listContent[i]["Id"]+" data-uniqueKey='"+UniqueId+"' style='background-color: #"+listContent[i]["ColorCode"].trim()+";'  class='eicon-s margin-right5x'>"+listContent[i]["ShortDescription"]+"</span>";TreeItem+="<span data-name='text' data-EntityLevel='"+listContent[i]["Level"]+"' data-EntityTypeID='"+listContent[i]["TypeID"]+"'  data-entityID="+listContent[i]["Id"]+"  data-colorcode="+listContent[i]["ColorCode"]+" data-shorttext="+listContent[i]["ShortDescription"]+" data-Permission="+listContent[i].Permission+" data-entityname=\""+listContent[i]["Name"]+"\">"+listContent[i]["Name"]+"</span>";if(listContent[i].Permission==1||listContent[i].Permission==2){TreeItem+="<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue="+listContent[i].Permission+" data-typeID="+listContent[i]["TypeID"]+" data-entityID="+listContent[i]["Id"]+"></i>";}
else if(listContent[i].Permission==3){var EntityTypesWithPermission="";if($scope.ContextMenuRecords.ChildContextData!=null&&$scope.ContextMenuRecords.ChildContextData!=""){EntityTypesWithPermission=$.grep($scope.ContextMenuRecords.ChildContextData,function(e){return e.ParentTypeId==parseInt(listContent[i]["TypeID"])&&e.EntityTypeAccessIsEnabled==true;})}
if(EntityTypesWithPermission.length>0)
TreeItem+="<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue="+listContent[i].Permission+" data-typeID="+listContent[i]["TypeID"]+" data-entityID="+listContent[i]["Id"]+"></i>";}
else if(listContent[i].Permission==0){}
TreeItem+="</a> </li>";var status=$scope.IsSelectAllChecked;if(listContent[i]["Id"]!=null){contentnHtml+="<td id='mos"+listContent[i]["Id"]+"'><span class='pull-right'><label class='checkbox checkbox-custom pull-right' id="+listContent[i]["Id"]+"><input type='checkbox' ><i class='checkbox "+($scope.IsSelectAllChecked?'checked':'')+"'></i></label></span></td>"}
colStatus=true;contentnHtml+="</tr>";colStatus=true;}}
GetApprovedBugetIcon(listContent,$scope.IsSelectAllChecked);if(PageIndex==0){$("#ListContainer > table tbody").html('');$("#treeHolder ul").html('');fnPageTemplate(parseInt(getactivitylist.Response.DataCount));$('#ListContainer').scrollTop(0);}
if($("#ListColumn > thead tr").length<1){columnHtml+="<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";columnHtml+="</tr>";$('#ListColumn > thead').html(columnHtml);}
if($("#ListColumn > thead tr").length>0){$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').html(contentnHtml);$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').removeClass('pending').removeAttr('style');$('#treeHolder  li[data-page="'+PageIndex+'"] > ul').html(TreeItem);$('#treeHolder li[data-page="'+PageIndex+'"]').removeClass('pending').removeAttr('style');ListCheckBoxitemClick();}}}
else{bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));}
$(window).AdjustHeightWidth();});}
$scope.sortOrder="null";$scope.IsDesc=false;GetRootLevelActivityListCount();function GetRootLevelActivityListCount(){var CCAttrs=$cookieStore.get('CCAttrs'+parseInt($cookies['UserId'],10));var CCFilterID=$cookieStore.get('CCFilterID'+parseInt($cookies['UserId'],10));var CCFilterName=$cookieStore.get('CCFilterName'+parseInt($cookies['UserId'],10));RemeberCCFilterName=CCFilterName;if(CCAttrs!=undefined){RememberCCFilterAttributes=CCAttrs;}
if(CCFilterID!=undefined){RememberCCFilterID=CCFilterID;}
if(RememberCCFilterID!=0||RememberCCFilterAttributes.length!=0){$scope.FilterID.selectedFilterID=RememberCCFilterID;$scope.FilterID.selectedfilterattribtues=RememberCCFilterAttributes;if(RememberCCFilterAttributes.length>0){}
if(RememberCCFilterID!=0){$scope.appliedfilter=RemeberCCFilterName;$scope.AddEditFilter='Edit filter';$scope.SelectedFilterID=$scope.FilterID.selectedFilterID;}}
$scope.noData=false;$window.CostCentreFilterName='';var Node={};Node.StartRowNo=0;Node.MaxNoofRow=20;Node.FilterID=$scope.FilterID.selectedFilterID;Node.SortOrderColumn=$scope.sortOrder;Node.IsDesc=$scope.IsDesc;Node.IncludeChildren=false;Node.EntityID='0';Node.UserID=CurrentuserId;Node.Level=$scope.Level;Node.IDArr=[];Node.FilterAttributes=$scope.FilterID.selectedfilterattribtues;CclistService.CostCentreRootLevel(Node).then(function(getactivitylist){if(getactivitylist.Response!=null&&getactivitylist.Response.Data!=null){fnPageTemplate(parseInt(getactivitylist.Response.DataCount));var PageIndex=0;if(getactivitylist.Response.ColumnDefs.length>0){var listColumnDefsdata=getactivitylist.Response.ColumnDefs
var listContent=getactivitylist.Response.Data;var contentnHtml="";var columnHtml="<tr >";var treeHtml="";var TreeItem="";var colStatus=false;for(var i=0;i<listContent.length;i++){if(i!=undefined){var UniqueId=UniqueKEY(listContent[i]["UniqueKey"]);var ClassName=GenateClass(UniqueId);ClassName+=" mo"+UniqueId;contentnHtml+="<tr data-over='true' class='ng-scope"+ClassName+"' data-EntityLevel='"+listContent[i]["Level"]+"' data-entityID="+listContent[i]["Id"]+" data-uniqueKey="+UniqueId+">"
for(var j=0;j<listColumnDefsdata.length;j++){if(j!=undefined&&listColumnDefsdata[j].Field!=SystemDefiendAttributes.Name){if(colStatus==false){if(j!=undefined&&listColumnDefsdata[j].Field==SystemDefiendAttributes.AssignedAmount){columnHtml+="<th>";}else{columnHtml+="<th>";}
columnHtml+="<a  data-Column="+listColumnDefsdata[j].Field+" >";columnHtml+="    <span>"+listColumnDefsdata[j].DisplayName+"</span>";columnHtml+="</a>";columnHtml+="</th>";}
if(j!=undefined&&listColumnDefsdata[j].Field==SystemDefiendAttributes.AssignedAmount)
contentnHtml+="<td><span class='currColumn ng-binding'>"+(listContent[i][listColumnDefsdata[j].Field]!=undefined?listContent[i][listColumnDefsdata[j].Field].formatMoney(0,' ',' '):"-")+" "+listContent[i].CurrencyType.toUpperCase()+"</span></td>";else
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i][listColumnDefsdata[j].Field]!=undefined?listContent[i][listColumnDefsdata[j].Field]:"-")+"</span></td>";}}
TreeItem+="<li class='ng-scope"+ClassName+"' data-over='true' data-uniqueKey="+UniqueId+"><a  data-role='Activity' data-EntityLevel='"+listContent[i]["Level"]+"' data-EntityTypeID='"+listContent[i]["TypeID"]+"'  data-entityID="+listContent[i]["Id"]+"  data-colorcode="+listContent[i]["ColorCode"]+" data-shorttext="+listContent[i]["ShortDescription"]+" data-Permission="+listContent[i].Permission+" data-entityname=\""+listContent[i]["Name"]+"\"> ";TreeItem+=Space(listContent[i]["UniqueKey"]);if(listContent[i]["TotalChildrenCount"]<=0||$scope.IsSingleID==true){TreeItem+="<i class='icon-'></i>";$scope.IsSingleID=false;}
else{TreeItem+="<i data-role='Arrow' data-icon='"+UniqueId+"' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID="+listContent[i]["Id"]+" data-TotalChildrenCount="+listContent[i]["TotalChildrenCount"]+" data-Permission="+listContent[i].Permission+"  data-UniqueId=\""+UniqueId+"\"></i>";}
TreeItem+=" <span data-icon='ActivityIcon' data-ParentID="+listContent[i]["Id"]+" data-uniqueKey='"+UniqueId+"' style='background-color: #"+listContent[i]["ColorCode"].trim()+";'  class='eicon-s margin-right5x'>"+listContent[i]["ShortDescription"]+"</span>";TreeItem+="<span data-name='text' data-EntityLevel='"+listContent[i]["Level"]+"' data-EntityTypeID='"+listContent[i]["TypeID"]+"'  data-entityID="+listContent[i]["Id"]+"  data-colorcode="+listContent[i]["ColorCode"]+" data-shorttext="+listContent[i]["ShortDescription"]+" data-Permission="+listContent[i].Permission+" data-entityname=\""+listContent[i]["Name"]+"\">"+listContent[i]["Name"]+"</span>";if(listContent[i].Permission==1||listContent[i].Permission==2){TreeItem+="<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue="+listContent[i].Permission+" data-typeID="+listContent[i]["TypeID"]+" data-entityID="+listContent[i]["Id"]+"></i>";}
else if(listContent[i].Permission==3){var EntityTypesWithPermission="";if($scope.ContextMenuRecords.ChildContextData!=null&&$scope.ContextMenuRecords.ChildContextData!=""){EntityTypesWithPermission=$.grep($scope.ContextMenuRecords.ChildContextData,function(e){return e.ParentTypeId==parseInt(listContent[i]["TypeID"])&&e.EntityTypeAccessIsEnabled==true;})}
if(EntityTypesWithPermission.length>0)
TreeItem+="<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue="+listContent[i].Permission+" data-typeID="+listContent[i]["TypeID"]+" data-entityID="+listContent[i]["Id"]+"></i>";}
treeHtml+="<li><a data-over='true' class='mo"+listContent[i]["Id"]+"' href='javascript:void(0)' data-id="+listContent[i]["Id"]+"><i class='icon-'></i><span style='background-color: #"+listContent[i]["ColorCode"]+"' class='eicon-s margin-right5x'>"+listContent[i]["ShortDescription"]+"</span><span class='treeItemName'>"+listContent[i]["Name"]+"</span></a></li>";var status="";if(listContent[i]["Id"]!=null){contentnHtml+="<td id='mos"+listContent[i]["Id"]+"'><span class='pull-right'><label class='checkbox checkbox-custom pull-right' id="+listContent[i]["Id"]+"><input type='checkbox' ><i class='checkbox "+status+"'></i></label></span></td>"}
colStatus=true;}}
GetApprovedBugetIcon(listContent);if($("#ListColumn > thead tr").length<1){columnHtml+="<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";columnHtml+="</tr>";$('#ListColumn > thead').html(columnHtml);}
if($("#ListColumn > thead tr").length>0){$("#ListContainer > table tbody").html('');$("#treeHolder ul").html('');$('#ListContainer').scrollTop(0);$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').html(contentnHtml);$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').removeClass('pending').removeAttr('style');$('#treeHolder  li[data-page="'+PageIndex+'"] > ul').html(TreeItem);$('#treeHolder li[data-page="'+PageIndex+'"]').removeClass('pending').removeAttr('style');ListCheckBoxitemClick();}
if($scope.PageSize>1){$scope.LoadRootLevelCostCenterActivity(1,20,20,$scope.FilterID.selectedFilterID,$scope.AppllyFilterObj.selectedattributes);}}}
else{$scope.noData=true;}
$(window).AdjustHeightWidth();});}
function UniqueKEY(UniKey){var substr=UniKey.split('.');var id="";var row=substr.length;if(row>1){for(var i=0;i<row;i++){id+=substr[i];if(i!=row-1){id+="-";}}
return id;}
return UniKey;}
function GenateClass(UniKey){var ToLength=UniKey.lastIndexOf('-');if(ToLength==-1){return"";}
var id=UniKey.substring(0,ToLength);var afterSplit=id.split('-');var result="";for(var i=0;i<afterSplit.length;i++){result+=SplitClss(id,i+1);}
return result;}
function SplitClss(NewId,lengthofdata){var afterSplit=NewId.split('-');var finalresult=" p";for(var i=0;i<lengthofdata;i++){finalresult+=afterSplit[i];if(i!=lengthofdata-1){finalresult+="-"}}
return finalresult;}
function Space(UniKey){var substr=UniKey.split('.');var itag="";var row=substr.length;if(row>1){for(var i=1;i<row;i++){itag+="<i class='icon-'></i>";}}
return itag;}
var LoadedPage=0;function fnPageTemplate(ItemCnt){var PageSize=1;var itemsPerPage=20;var height=34*ItemCnt;if(ItemCnt>itemsPerPage){height=34*itemsPerPage;PageSize=Math.ceil(ItemCnt/itemsPerPage);}
$scope.PageSize=PageSize;var TreeTemplate='';var ListTemplate='';for(var i=0;i<=PageSize;i++){if(i!=undefined){TreeTemplate+="<li class='pending' style='min-height: "+height+"px;' data-page='"+i+"'><ul class='nav nav-list CCnav-list nav-list-menu'></ul></li>"
ListTemplate+="<tbody class='widthcent pending' style='min-height: "+height+"px;' data-page='"+i+"'></tbody>"}}
if(TreeTemplate.length>0){$("#ListContainer > table").html(ListTemplate);$("#treeHolder").html(TreeTemplate);}}
$('#ListContainer').scroll(function(){$("#treeHolder").scrollTop($(this).scrollTop());var areaHeight=$('#ListContainer').height();var areaTop=$('#ListContainer').position().top;var top=$(this).position().top-areaTop;var height=$('#ListContainer table').height();$('tbody.pending','#ListContainer').each(function(){var datapage=parseInt($(this).attr('data-page'));if((($(this).height()*datapage)/3)<$("#treeHolder").scrollTop()){log("Page "+$(this).attr('data-page')+" is loaded");$('#ListContainer > table tbody[data-page="'+$(this).attr('data-page')+'"]').removeClass('pending');var StartRowNo=datapage*20;var MaxNoofRow=20;$(this).removeClass('pending');$(this).removeAttr('style');var filterattributes=[];$('#treeHolder li[data-page="'+datapage+'"]').removeClass('pending');$('#treeHolder li[data-page="'+datapage+'"]').removeAttr('style');$scope.LoadRootLevelCostCenterActivity(datapage,StartRowNo,MaxNoofRow,$scope.FilterID.selectedFilterID,$scope.FilterID.selectedfilterattribtues);}});});$("#ListColumn").click(function(e){var filterattributes=[];var TargetControl=$(e.target);var col="";if(TargetControl[0].tagName=="SPAN"){$("#ListContainer table").html('');$("#treeHolder ul").html('');col=TargetControl.parent().attr("data-Column");}
else if(TargetControl[0].tagName=="A"){$("#ListContainer table").html('');$("#treeHolder ul").html('');col=TargetControl.attr("data-Column");}
if(col!=""){$scope.sortOrder=col;$('#RootLevelSelectAll').next('i').removeClass('checked');ClearCheckBoxes();if($scope.sortOrder===col){$scope.IsDesc=!$scope.IsDesc;}else{$scope.sortOrder=col;$scope.IsDesc=false;}
$scope.LoadRootLevelCostCenterActivity(0,0,20,0,filterattributes)}});$("#rootLevelEntity").click(function(event){$("#rootLevelEntity").trigger("onRootCostCentreCreation",[$scope.EntityTypeID])});$window.ListofEntityID=[]
$("#treeHolder").click(function(e){$(window).AdjustHeightWidth();var TargetControl=$(e.target);$scope.EntityTypeID=parseInt(TargetControl.find("I[data-role='Overview']").attr('data-typeID'),10);if(TargetControl[0].tagName=="I"&&TargetControl.attr('data-role')=='Arrow'){LoadChildTreeNodes(TargetControl.attr('data-ParentID'),TargetControl.attr('data-TotalChildrenCount'),TargetControl.attr('data-UniqueId'))}
else if(TargetControl[0].tagName=="I"&&TargetControl.attr('data-role')=='Overview'){if(TargetControl.attr('data-entityID')!=undefined&&TargetControl.attr('data-entityID')!="0"){$scope.CostCentreEntityID=TargetControl.attr('data-entityID');$('#moduleContextCCRootmenu').css('top',$(TargetControl[0]).offset().top+22+'px').show();e.preventDefault();return false;}
else
e.preventDefault();}
else if(TargetControl[0].tagName=="A"&&TargetControl.attr('data-role')=='Activity'){if(TargetControl.attr('data-entityID')!=undefined&&TargetControl.attr('data-entityID')!="0"){e.preventDefault();ViewRootLevelEntity(TargetControl.children().attr('data-TotalChildrenCount'),TargetControl.attr('data-entityID'),TargetControl.attr('data-entitylevel'),e);}
else
e.preventDefault();}
else if(TargetControl[0].tagName=="SPAN"&&TargetControl.attr('data-name')=='text'){if(TargetControl.attr('data-entityID')!=undefined&&TargetControl.attr('data-entityID')!="0"){e.preventDefault();ViewRootLevelEntity(TargetControl.children().attr('data-TotalChildrenCount'),TargetControl.attr('data-entityID'),TargetControl.attr('data-entitylevel'),e);}
else
e.preventDefault();}
$(window).AdjustHeightWidth();});$(document).on('click','.checkbox-custom > input[id=RootLevelSelectAll]',function(e){var status=this.checked;$('#ListContainer > table > tbody input:checkbox').each(function(){this.checked=status;if(status){$scope.IsSelectAllChecked=true;$(this).next('i').addClass('checked');}else{$scope.IsSelectAllChecked=false;$(this).next('i').removeClass('checked');}});});$("#ViewSelect").click(function(event){ViewRootLevelEntity(1,null,0,event);});function GetRootLevelSelectedAll(){var IDList=new Array();$('#ListContainer > table > tbody input:checked').each(function(){IDList.push($(this).parents('tr').attr('data-entityID'));});return IDList}
function ViewRootLevelEntity(TotalChildren,ID,Level,event){var IDList=new Array();if($scope.IsSelectAllChecked==false){if(ID!=null){IDList.push(ID);LoadDetailPart(event,IDList,Level,TotalChildren);}
else{IDList=GetRootLevelSelectedAll();if(IDList.length==1){$('#ListContainer > table > tbody input:checked').each(function(){var selectedUniqkey=($(this).parents('tr').attr('data-uniquekey'));TotalChildren=$('#treeHolder li[data-uniquekey="'+selectedUniqkey+'"] a i').attr('data-TotalChildrenCount');});}
LoadDetailPart(event,IDList,Level,TotalChildren);}}
else{$scope.IsSelectAllChecked=false;CclistService.GetAllEntityIds().then(function(Entityresult){if(Entityresult.Response!=null){IDList=Entityresult.Response;$window.TreeLevel="100";LoadSelectAllDetailPart(event,IDList);}});}}
function LoadDetailPart(event,IDList,Level,TotalChildren){if(IDList.length!==0){$window.ListofEntityID=[];$window.TreeLevel="";$window.ListofEntityID=IDList;$window.TotalChildrenForSelecedCC=0;if(IDList.length>1)
$window.TreeLevel="100";else{$window.TreeLevel=Level;$window.TotalChildrenForSelecedCC=TotalChildren==undefined?0:TotalChildren;}
var TrackID=CreateHisory(IDList);event.preventDefault();var localScope=$(event.target).scope();localScope.$apply(function(){if($("#ViewSelect").attr('data-viewtype')=="ganttview"){$location.path("mui/planningtool/costcentre/detail/ganttview/"+TrackID);}
else{$location.path("mui/planningtool/costcentre/detail/listview/"+TrackID);}});}
else{bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));}}
function ListCheckBoxitemClick(){$('#ListContainer > table > tbody input:checkbox').click(function(){$("#RootLevelSelectAll").next().removeClass('checked')
$scope.IsSelectAllChecked=false;});}
function LoadSelectAllDetailPart(event,IDList){if(IDList.length!==0){$window.ListofEntityID=[];$window.ListofEntityID=IDList;var TrackID=CreateHisory(IDList);if($("#ViewSelect").attr('data-viewtype')=="ganttview"){$location.path("mui/planningtool/costcentre/detail/ganttview/"+TrackID);}
else{$location.path("mui/planningtool/costcentre/detail/listview/"+TrackID);}}
else{bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));}}
$scope.UpdateCostCentreApprovedBudget=function(){var IDList=new Array();var ID=0;IDList=GetRootLevelSelectedAll();if(IDList.length>0){bootbox.confirm($translate.instant('LanguageContents.Res_2028.Caption'),function(result){if(result){$timeout(function(){if(IDList.length>0){var CostCentreApprovedBudget={};CostCentreApprovedBudget.CCIDs=IDList;CclistService.UpdateCostCentreApprovedBudget(CostCentreApprovedBudget).then(function(UpdateCostCentreApprovedBudget){if(UpdateCostCentreApprovedBudget.StatusCode==200){GetApprovedBugetIconDtls(IDList);NotifySuccess($translate.instant('LanguageContents.Res_4086.Caption'));$('#RootLevelSelectAll').next('i').removeClass('checked');}else{NotifyError($translate.instant('LanguageContents.Res_4378.Caption'));}});}else{bootbox.alert($translate.instant('LanguageContents.Res_2133.Caption'))}},100);}});}
else{bootbox.alert($translate.instant('LanguageContents.Res_2133.Caption'))}};function GetApprovedBugetIconDtls(IDList,IsChecked){CclistService.GetApprovedBudgetDate(IDList.join(",")).then(function(ApprovedDates){if(ApprovedDates.Response!=null){for(var i=0;i<ApprovedDates.Response.length;i++){$scope.ApprovedDateTime=[];for(var i=0;i<ApprovedDates.Response.length;i++){var BugetDts=ApprovedDates.Response[i].ApproveTime;if(BugetDts!=null){var status=IsChecked;$('#mos'+ApprovedDates.Response[i].CostCentreID).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox"><i class="checkbox '+(status?"checked":"")+'"></i></label></span><img id="ApprovedBudge'+ApprovedDates.Response[i].CostCentreID+'" src="assets/img/AppBudget.png"></td></tr>');var colStatus=true;}
var datStartUTCval="";var datstartval="";datStartUTCval=BugetDts.substr(6,(BugetDts.indexOf('+')-6));datstartval=new Date(parseInt(datStartUTCval));$scope.fields=BugetDts;var id=ApprovedDates.Response[i].CostCentreID;$("#ApprovedBudge"+id).show();$("#ApprovedBudge"+ApprovedDates.Response[i].CostCentreID).attr("title","ApprovedBudgetDate: "+$scope.fields).qtip({content:{text:$scope.fields,},style:{classes:"qtip-dark qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},events:{hide:function(event,api){event:'unfocus click mouseleave mouseup mousedown'}},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(100);},event:'unfocus click mouseleave mouseup mousedown'},position:{my:'bottom left',at:'top left',viewport:$(window),adjust:{method:'shift',mouse:true},corner:{target:'bottom left',tooltip:'bottom left',mimic:'top'},target:'mouse'}});}}}});}
function GetApprovedBugetIcon(IDList,IsChecked){var ids=$.map(IDList,function(e){return e.Id});CclistService.GetApprovedBudgetDate(ids.join(",")).then(function(ApprovedDates){if(ApprovedDates.Response!=null){for(var i=0;i<ApprovedDates.Response.length;i++){$scope.ApprovedDateTime=[];for(var i=0;i<ApprovedDates.Response.length;i++){var BugetDts=ApprovedDates.Response[i].ApproveTime;if(BugetDts!=null){var status=IsChecked;$('#mos'+ApprovedDates.Response[i].CostCentreID).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox"><i class="checkbox '+(status?"checked":"")+'"></i></label></span><img id="ApprovedBudge'+IDList+'" src="assets/img/AppBudget.png"></td></tr>');var colStatus=true;}
var datStartUTCval="";var datstartval="";datStartUTCval=BugetDts.substr(6,(BugetDts.indexOf('+')-6));datstartval=new Date(parseInt(datStartUTCval));$scope.fields=BugetDts;$("#ApprovedBudge"+ApprovedDates.Response[i].CostCentreID).attr("title","ApprovedBudgetDate: "+$scope.fields).qtip({content:{text:$scope.fields,},style:{classes:"qtip-dark qtip-shadow qtip-rounded",title:{'display':'none'}},show:{event:'mouseenter click unfocus',solo:true},events:{hide:function(event,api){event:'unfocus click mouseleave mouseup mousedown'}},hide:{delay:0,fixed:false,effect:function(){$(this).fadeOut(100);},event:'unfocus click mouseleave mouseup mousedown'},position:{my:'bottom left',at:'top left',viewport:$(window),adjust:{method:'shift',mouse:true},corner:{target:'bottom left',tooltip:'bottom left',mimic:'top'},target:'mouse'}});}}}
else{var status="";$('#mos'+IDList).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox" ><i class="checkbox '+status+'"></i></label></span></td></tr>');}});}
$(document).click(function(){$('#moduleContextCCRootmenu').hide();})
$("#moduleContextCCRootmenu").click(function(){CclistService.GetCostCentreAssignedAmount($scope.CostCentreEntityID).then(function(ResAssgnAmnt){$timeout(function(){$("#Div_listCC").trigger("loadAttributeMetadata",[$scope.CostCentreEntityID,ResAssgnAmnt.Response]);},100);});});function GenerateCss(){var row=$scope.TreeClassList.length;var cssStyle='<style type="text/css" id="TreeCtrlCss">';$('#TreeCtrlCss').remove()
for(var i=0;i<row;i++){cssStyle+='.'+$scope.TreeClassList[i].trim();cssStyle+="{display: none;}";}
$('head').append(cssStyle+"</style>");}
function Collapse(value){if($scope.TreeClassList.indexOf("p"+value)==-1){$scope.TreeClassList.push("p"+value);}
GenerateCss();}
function Expand(value){var index=$scope.TreeClassList.indexOf("p"+value)
if(index!=-1){$scope.TreeClassList.splice(index,1);}
GenerateCss();}
function LoadChildTreeNodes(data,ChildCount,UniqueId){if(ChildCount>0){if($('#treeHolder li i[data-icon='+UniqueId+']').attr("data-isExpanded")=="true"){$('#treeHolder li i[data-icon='+UniqueId+']').attr("class","icon-caret-right");Collapse(UniqueId);$('#treeHolder li i[data-icon='+UniqueId+']').attr("data-isExpanded","false");$('#EntitiesTree').scroll();}
else{$('#treeHolder li i[data-icon='+UniqueId+']').attr("class","icon-caret-down");Expand(UniqueId);$('#treeHolder li i[data-icon='+UniqueId+']').attr("data-isExpanded","true");}
if($('#treeHolder li i[data-icon='+UniqueId+']').attr("data-isChildrenPresent")=="true"){$scope.ExpandingEntityID=data;TreeLoad(UniqueId,false);$('#treeHolder li i[data-icon='+UniqueId+']').attr("data-isChildrenPresent","false");}}}
$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.listCtrl']"));});$timeout(function(){$scope.load();},0);$scope.visible=false;$scope.fields={};$scope.dynamicEntityValuesHolder={};$scope.dyn_Cont='';$scope.fieldKeys=[];$scope.options={};$scope.setFieldKeys=function(){var keys=[];angular.forEach($scope.fields,function(key){keys.push(key);$scope.fieldKeys=keys;});}
$scope.createcostcentre=function(){var modalInstance=$modal.open({templateUrl:'views/mui/planningtool/costcentre/costcentrecreation.html',controller:"mui.planningtool.costcentre.costcentrecreationCtrl",resolve:{params:function(){return 0;}},scope:$scope,windowClass:'popup-widthL',backdrop:"static"});modalInstance.result.then(function(selectedItem){$scope.selected=selectedItem;},function(){});}}
app.controller("mui.planningtool.costcentre.listCtrl",['$scope','$timeout','$http','$compile','$resource','$location','$window','$cookies','$cookieStore','$translate','CclistService','$modal',muiplanningtoolcostcentrelistCtrl]);})(angular,app);
///#source 1 1 /app/controllers/mui/planningtool/filtersettings-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, PlanfiltersettingsService) {
        $scope.treeNodeSelectedHolderValues = [];
        $scope.appliedfilter = "No filter applied";
        var IsUpdate = 0;
        var IsSave = 0;
        $scope.deletefiltershow = false;
        $scope.FilterFields = {};
        var TypeID = $scope.EntityTypeID;
        $scope.visible = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.rememberAttrValue = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {};
        $scope.TreePricing = [];
        $scope.treeTexts = {};
        $scope.FilterDataXML = [];
        $("#rootlevelfilter").on('LoadRootLevelFilter', function (event, typeid, data) {
            $scope.rememberAttrValue = data;
            $scope.FilterSettingsLoad();
        });
        var rememberfilter = [];
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilterbutton = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsUpdate = 0;
            IsSave = 0;
        };
        $("#rootlevelfilter").on('ClearScope', function (event, TypeID) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
            }
        });
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearScopeModle = function () {
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationListWithTree, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            IsSave = 0;
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            $scope.deletefiltershow = false;
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
                var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 13
                });
                if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                    for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                        if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                    }
                }
            }
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattributes = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $("#Div_list").trigger('ClearAndReApply');
            $("#Div_listCC").trigger('ClearAndReApply');
            if (TypeID == 6) {
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            } else if (TypeID == 5) {
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
        }
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            if ($scope.dyn_Cont.length == 0) {
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                $scope.EntityHierarchyTypesResult = [];
                $scope.dyn_Cont = '';
                $scope.Updatefilter = false;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $("#dynamic_Controls").html('');
                $scope.atributesRelationList = [];
                $scope.atributesRelationListWithTree = [];
                $scope.ngsaveasfilter = '';
                $scope.ngKeywordtext = '';
                var OptionFrom = 0;
                var IsKeyword = "false";
                var IsEntityType = "false";
                var CurrentActiveVersion = 0;
                var elementNode = 'Filter';
                var KeywordOptionsresponse;
                var CheckingPricicingAttr = [];
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    PlanfiltersettingsService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                        KeywordOptionsresponse = KeywordOptionsResult.Response;
                        OptionFrom = KeywordOptionsresponse.split(',')[0];
                        IsKeyword = KeywordOptionsresponse.split(',')[1];
                        IsEntityType = KeywordOptionsresponse.split(',')[3];
                        CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                        var IDList = new Array();
                        var filterattids = {};
                        filterattids.IDList = IDList;
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'Filter';
                        filterattids.OptionFrom = OptionFrom;
                        PlanfiltersettingsService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                    if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                        if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                            var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                            $scope.tagAllOptionsEntityStatus.data = [];
                                            if (EntityStatusOptions != null) {
                                                $.each(EntityStatusOptions, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.dyn_Cont += "<div class='control-group'>";
                                            $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                            $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Name  for ndata in  OntimeStatusLists \"></select></div>";
                                        } else {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption  for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                        if ($scope.atributesRelationList[i].AttributeId != 75 && $scope.atributesRelationList[i].AttributeId != 74) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"  id=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].LevelTreeNodes \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                        $scope.dyn_Cont += "<select multiple='multiple' ui-select2 data-placeholder='Select " + $scope.atributesRelationList[i].DisplayName + " options' ng-model='FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "'\ >";
                                        $scope.dyn_Cont += "<option ng-repeat='ndata in atributesRelationList[" + i + "].LevelTreeNodes' value='{{ndata.Id}}'>{{ndata.Caption}}</option>";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                        var k = $scope.TreePricing.length;
                                        var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                        });
                                        if (treecount12.length == 0) {
                                            var mm = $scope.atributesRelationList[i].AttributeId;
                                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                            $scope.FilterDataXML.push({
                                                "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                            });
                                        }
                                    }
                                }
                                if ($scope.EntityTypeID == 6 && IsEntityType == "True") {
                                    $scope.dyn_Cont += "<div class='control-group'>";
                                    $scope.dyn_Cont += "<span>EntityType : </span>";
                                    $scope.dyn_Cont += "<input id='ddlChildren' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' style='width: 200px;' />";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                    return e.AttributeTypeId == 7
                                })
                                for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                    if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                        $scope.treePreviewObj = {};
                                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        }
                                        $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                        $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_Cont += '</div></div>';
                                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                        $scope.dyn_Cont += '<div class="controls">';
                                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_Cont += '</div></div></div>';
                                    }
                                }
                                if (IsKeyword == "True") {
                                    $scope.dyn_Cont += "<div class=\"control-group\">";
                                    $scope.dyn_Cont += "<span>Keyword : </span>";
                                    $scope.dyn_Cont += "<input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'>";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                                $scope.Applyfilterbutton = false;
                                $scope.Updatefilter = false;
                                $scope.Deletefilter = false;
                                $scope.Savefilter = true;
                            }
                            RememberFilterOnReLoad($scope.rememberAttrValue);
                        });
                    });
                });
            }
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k].id,
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            if (orgLevel != undefined) {
                                for (var k = 0; k < orgLevel.length; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = [];
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    whereConditionData = [];
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = FilterData.FilterName;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = FilterData.FilterName;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterId, typeId) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId) {
            $scope.Updatefilter = false;
            $scope.Applyfilterbutton = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.ClearScopeModle();
            $scope.FilterID.selectedFilterID = filterId;
            PlanfiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if ($scope.filterSettingValues.FilterValues.length > 0) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                    return e.AttributeId == parseInt(71);
                                })[0].EntityStatusOptionValues;
                                var seletedoptionvalue = [];
                                for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
									return e.AttributeId == parseInt(71);
                                }).length; sts++) {
                                    seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                        return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                    })[0]);
                                    if (seletedoptionvalue != null) {
                                        $.each(seletedoptionvalue, function (i, el) {
                                            $scope.tagAllOptionsEntityStatus.data.push({
                                                "id": el.ID,
                                                "text": el.StatusOptions,
                                                "ShortDescription": el.ShortDesc,
                                                "ColorCode": el.ColorCode
                                            });
                                        });
                                    }
                                    if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                }
                            } else {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        }
                    }
                }
                if ($scope.EntityTypeID == 6) {
                    PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                        if (filterSettingsValues.Response.EntityTypeID != "") {
                            for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                                if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                                    if ($.grep($scope.tagAllOptions.data, function (e) {
										return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                        return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0]);
                                }
                            }
                            $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                        }
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                    for (var k = 0; k < dataVal.length; k++) {
                        if ($scope.atributesRelationList[i].AttributeId == 71) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k].id,
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        } else {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            var FilterData = {};
            FilterData.FilterId = $scope.FilterID.selectedFilterID;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = $scope.appliedfilter;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = $scope.appliedfilter;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $scope.ApplyFilter = function (FilterID, FilterName) {
            if (FilterID != undefined && FilterID != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                if ($scope.atributesRelationList.length > 0) {
                    if ($scope.atributesRelationList.length == 0) {
                        $scope.atributesRelationListtemp = $scope.atributesRelationList;
                    } else $scope.atributesRelationListtemp = $scope.atributesRelationList;
                }
            }
            if ($scope.atributesRelationList != undefined) if ($scope.atributesRelationList.length == 0) $scope.atributesRelationList = $scope.atributesRelationListtemp;
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var StartDate = '01/01/1990';
            var EndDate = '01/01/1990';
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            if ($scope.ngKeywordtext != '') whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': $scope.ngKeywordtext
                            });
                            else whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': ''
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    }
                }
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            } else {
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            $scope.FilterID.filterattributes = $scope.AppllyFilterObj.selectedattributes;
            if (TypeID == 6) {
                RememberPlanFilterID = FilterID;
                RememberPlanFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberPlanFilterName = $scope.appliedfilter;
                $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            } else if (TypeID == 5) {
                RememberCCFilterID = FilterID;
                RememberCCFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberCCFilterName = $scope.appliedfilter;
                $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
            $scope.FilterID.filterattributes = [];
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            $scope.filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        PlanfiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4398.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                $("#Div_list").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                $scope.FilterID.selectedFilterID = 0;
                                $scope.FilterID = {
                                    selectedFilterID: 0
                                };
                                $scope.AppllyFilterObj = {};
                                $scope.ApplyFilter(0, 'No filter applied');
                                if (TypeID == 6) {
                                    $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 5) {
                                    $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 6 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            } else if ($scope.EntityTypeID == 5 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            }
            if (RememberPlanFilterAttributes.length != 0) {
                $scope.ddlParententitytypeId = [];
                $scope.filterSettingValues.FilterAttributes = RememberPlanFilterAttributes;
                for (var i = 0; i < $scope.filterSettingValues.FilterAttributes.length; i++) {
                    if ($scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 4) {
                        if ($scope.filterSettingValues.FilterAttributes[i].AttributeID == 71) {
                            var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeId == parseInt(71);
                            })[0].EntityStatusOptionValues;
                            var seletedoptionvalue = [];
                            for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterAttributes, function (e) {
								return e.AttributeID == parseInt(71);
                            }).length; sts++) {
                                seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                    return e.id == parseInt($scope.filterSettingValues.FilterAttributes[sts].SelectedValue);
                                })[0]);
                                if (seletedoptionvalue != null) {
                                    $.each(seletedoptionvalue, function (i, el) {
                                        $scope.tagAllOptionsEntityStatus.data.push({
                                            "id": el.ID,
                                            "text": el.StatusOptions,
                                            "ShortDescription": el.ShortDesc,
                                            "ColorCode": el.ColorCode
                                        });
                                    });
                                }
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] = seletedoptionvalue;
                            }
                        } else {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                        }
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                    }
                }
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.length == undefined) {
                    if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs != '0') {
                        if ($.grep($scope.tagAllOptions.data, function (e) {
							return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0]);
                    }
                } else {
                    for (var l = 0; l < $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',').length; l++) {
                        if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l] != '0') {
                            if ($.grep($scope.tagAllOptions.data, function (e) {
								return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0]);
                        }
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                $scope.ngKeywordtext = $scope.filterSettingValues.FilterAttributes[0].Keyword;
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.filtersettingsCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            if ($scope.treesrcdirec["Attr_" + attributeid].length != undefined) {
                for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                    branch.ischecked = false;
                    if (branch.Children.length > 0) {
                        ClearRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        };
    }
    app.controller("mui.planningtool.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'PlanfiltersettingsService', muiplanningtoolfiltersettingsCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/costcentre/costcentrecreation-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentrecostcentrecreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, CostcentrecreationService, $modalInstance, params) {
        $scope.GetNonBusinessDaysforDatePicker();
        $("#rootLevelEntity").on("onRootCostCentreCreation", function (event, id) {
            $("#btnWizardFinish").removeAttr('disabled');
            $('#MyWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $scope.ClearModelObject($scope.fields);
            $('#btnWizardFinish').hide();
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $timeout(function () {
                HideAttributeToAttributeRelationsOnPageLoad();
            });
            $scope.UserimageNewTime = new Date.create().getTime().toString();
        });
        $scope.SelectedCostCentreCurrency = {
            TypeId: 0,
            Name: '',
            Rate: 1
        };
        $scope.ImageFileName = '';
        $scope.EnableDisableControlsHolder = {};
        $scope.treeSourcesObj = [];
        $scope.treeSources = {};
        $scope.Dropdown = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.ParentID = 0;
        $scope.AvailableAssignAmount = 0;
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            GetTreeCheckedNodes(parentArr, branch.AttributeId);
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttr").click();
        }

        function StrartUpload_UploaderAttr() {
            $('.moxie-shim').remove();
            var uploader_Attr = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttr',
                container: 'filescontaineroo',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}
            });
            uploader_Attr.bind('Init', function (up, params) { });
            uploader_Attr.init();
            uploader_Attr.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_Attr.start();
            });
            uploader_Attr.bind('UploadProgress', function (up, file) { });
            uploader_Attr.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader_Attr.bind('FileUploaded', function (up, file, response) {
                SaveFileDetails(file, response.response);
            });
            uploader_Attr.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, {
                    id: file.id,
                    size: file.size
                });
            });
        }

        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            $scope.ImageFileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreviewcc_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', 'UploadedImages/' + $scope.ImageFileName);
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            $scope.MemberLists = [];
            $scope.ngfinancialAmount = "0";
            $scope.$apply();
        }
        var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
        $('#btnWizardFinish').hide();
        $('#btnWizardNext').show();
        $('#btnWizardPrev').hide();
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.changeTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            } else {
                $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
                $('[id=ngfinancialAmount]').css('text-align', 'right');
                setTimeout(function () {
                    $('[id=ngfinancialAmount]:enabled:visible:first').focus().select()
                }, 100);
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
        }
        $scope.changeCostcentreTab2 = function (e) {
            $("#btnTemp").click();
            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#MyWizard ul.steps").find("li").addClass("complete");
            $("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#MyWizard').wizard('previous');
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
                $('[id=ngfinancialAmount]').css('text-align', 'right');
                setTimeout(function () {
                    $('[id=ngfinancialAmount]:enabled:visible:first').focus().select()
                }, 100);
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            } else {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }
        $scope.status = true;
        $scope.changenexttab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $("#btnTemp").click();
            if ($("#EntityMetadata .error").length > 0) {
                return false;
            }
            $('#MyWizard').wizard('next', '');
            $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
            $('[id=ngfinancialAmount]').css('text-align', 'right');
            setTimeout(function () {
                $('[id=ngfinancialAmount]:enabled:visible:first').focus().select()
            }, 100);
            var currentWizardStep = $('#MyWizard').wizard('selectedItem').step
            var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            } else {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.AutoCompleteSelectedObj = [];
        $scope.atributesRelationList = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};
        CostcentrecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({
                "Roleid": 1,
                "RoleName": "Owner",
                "UserEmail": $scope.ownerEmail,
                "DepartmentName": $scope.owner.Designation,
                "Title": $scope.owner.Title,
                "Userid": parseInt($scope.OwnerID, 10),
                "UserName": $scope.OwnerName,
                "IsInherited": '0',
                "InheritedFromEntityid": '0',
                "QuickInfo1": $scope.owner.QuickInfo1,
                "QuickInfo2": $scope.owner.QuickInfo2
            });
        })
        $scope.UserLists = [];
        $scope.MemberLists = [];
        $scope.contrls = '';
        GetEntityTypeRoleAccess(5);

        function GetEntityTypeRoleAccess(rootID) {
            CostcentrecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }
        var Version = 1;
        $scope.wizard = {
            newval: ''
        };
        $scope.dyn_Cont = '';
        $scope.items = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            $scope.items.push({
                startDate: [],
                endDate: [],
                comment: '',
                sortorder: 0
            });
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.entityObjectString = '{';
        $scope.fields = {
            usersID: ''
        };
        $scope.dynamicEntityValuesHolder = {};
        $scope.fieldKeys = [];
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.Entityamountcurrencytypeitem = [];
        $scope.optionsLists = [];
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.CostcenteAssignedAmount = 0;
        $scope.count = 1;
        $scope.addUsers = function () {
            var membervalues = $.grep($scope.Roles, function (e) {
                return e.ID == parseInt($scope.fields.userRoles, 10)
            })[0];
            if (membervalues != undefined) {
                if ($scope.AutoCompleteSelectedObj.length > 0) {
                    $scope.MemberLists.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedObj[0].Email,
                        "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation,
                        "Title": $scope.AutoCompleteSelectedObj[0].Title,
                        "Roleid": parseInt(membervalues.ID, 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10),
                        "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2
                    });
                    $scope.fields.usersID = '';
                    $scope.count = $scope.count + 1;
                    $scope.AutoCompleteSelectedObj = [];
                    $('#ddlrole').select2('val', '');
                }
            }
        };
        $scope.deleteOptions = function (item) {
            $scope.MemberLists.splice($.inArray(item.Userid, $scope.MemberLists), 1);
        };
        if ($('#ngfinancialAmount').length != 0) {
            $('#ngfinancialAmount').autoNumeric('init', $scope.DecimalSettings['FinancialAutoNumeric']);
            $('[id=ngfinancialAmount]').css('text-align', 'right');
            setTimeout(function () {
                $('[id=ngfinancialAmount]:enabled:visible:first').focus().select()
            }, 100);
        }
        $scope.saveCostcentreEntity = function () {
            if ($scope.ParentID > 0) if (parseInt($('#ngfinancialAmount').val().replace(/\s/g, ''), 10) > $scope.AvailableAssignAmount) {
                bootbox.alert("Assigned amount is greater than Available amount.");
                return false;
            }
            if ($scope.SelectedCostCentreCurrency.TypeId == 0 || $scope.SelectedCostCentreCurrency.TypeId == "") {
                bootbox.alert("Please select currency type.");
                return false;
            }
            $("#btnWizardFinish").attr('disabled', 'disabled');
            var saveCostcentre = {};
            $scope.AttributeData.length = 0;
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10)
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                });
                            }
                        } else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                        });
                                    }
                                } else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CurrencyInCostCentre) {
                            var fiscalyear = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined > 0 ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt(fiscalyear, 10),
                                "Level": 0
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == null) {
                            MyDate.setDate(MyDate.getDate());
                            MyDateString = null;
                        } else {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString('MM/dd/yyyy');
                        }
                    } else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.AssignedAmount) {
                    $scope.CostcenteAssignedAmount = 0;
                    if (isNaN(parseFloat($('#ngfinancialAmount').val(), 10)) == false && parseInt($('#ngfinancialAmount').val(), 10) != 0) $scope.CostcenteAssignedAmount = parseFloat((parseFloat($('#ngfinancialAmount').val().replace(/\s/g, ''), 10)) / $scope.SelectedCostCentreCurrency.Rate);
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.CostcenteAssignedAmount,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.AvailableAssignedAmount) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.CostcenteAssignedAmount,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] == undefined ? 0 : $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({
                        amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                        currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                        Attributeid: $scope.atributesRelationList[i].AttributeID
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.ImageFileName,
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    if ($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        var dte = new Date.create($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID]);
                        var tdte = ConvertDateToString(dte);
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": tdte,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
            }
            saveCostcentre.ParentId = $scope.ParentID;
            saveCostcentre.TypeId = 5;
            saveCostcentre.Name = $scope.entityName;
            saveCostcentre.AssignedAmount = $scope.CostcenteAssignedAmount;
            saveCostcentre.EntityMembers = [];
            saveCostcentre.Periods = [];
            saveCostcentre.CurrencyFormat = $scope.SelectedCostCentreCurrency.TypeId;
            saveCostcentre.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                $scope.items[m].startDate = ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : '');
                $scope.items[m].endDate = ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : '');
            }
            saveCostcentre.Periods.push($scope.items);
            saveCostcentre.AttributeData = $scope.AttributeData;
            saveCostcentre.EntityMembers = $scope.MemberLists;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({
                    amount: $scope.Entityamountcurrencytypeitem[m].amount,
                    currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                    Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                });
            }
            saveCostcentre.Entityamountcurrencytype.push($scope.curram);
            CostcentrecreationService.CreateCostcentre(saveCostcentre).then(function (costcentreCreationResult) {
                if (costcentreCreationResult.StatusCode == 405 || costcentreCreationResult.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4278.Caption'));
                    $scope.Entityamountcurrencytypeitem = [];
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4175.Caption'));
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.MemberLists = [];
                    var IDList = new Array();
                    IDList.push(costcentreCreationResult.Response);
                    $window.ListofEntityID = IDList;
                    var TrackID = CreateHisory(IDList);
                    $location.path('/mui/planningtool/costcentre/detail/section/' + costcentreCreationResult.Response);
                }
            });
            $scope.closecostcentrecreationPopup();
        }
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.Options = {};
        $scope.OptionObj = {};
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $("#Div_listCC").on('loadAttributeMetadata', function (event, ParentID, ParentAssgnAmount) {
            $("#btnWizardFinish").removeAttr('disabled');
            $('#MyWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $scope.ClearModelObject($scope.fields);
            $('#btnWizardFinish').hide();
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $scope.ParentID = ParentID;
            $scope.AvailableAssignAmount = ParentAssgnAmount;
            $scope.dyn_Cont = '';
            $scope.treePreviewObj = {};
            loadAttributeMetadata();
        });

        function loadAttributeMetadata() {
            $scope.SelectedCostCentreCurrency.TypeId = $scope.DefaultSettings.CurrencyFormat.Id;
            CostcentrecreationService.GetAttributeToAttributeRelationsByIDForEntity(5).then(function (entityAttrToAttrRelation) {
                $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
            });
            CostcentrecreationService.GetEntityTypeAttributeRelationWithLevelsByID(5, $scope.ParentID).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"> <input type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " : </label><div class=\"controls\"> <select ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ui-select2 ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        var MyDate = new Date.create();
                        var MyDateString;
                        MyDate.setDate(MyDate.getDate() + 1);
                        MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '/' + MyDate.getFullYear();
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" data-date-format=\"dd-mm-yyyy\" type=\"text\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"   class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        } else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select class=\"multiselect\"   data-placeholder=\"Select filter\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"   multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\" ></select></div></div>";
                        var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                        if ($scope.atributesRelationList[i].DefaultValue != "") {
                            for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) {
                                    return e.Id == defaultmultiselectvalue[j];
                                })[0].Id);
                            }
                        } else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.items.push({
                            startDate: [],
                            endDate: [],
                            comment: '',
                            sortorder: 0
                        });
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span4\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.startDate" + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\" /><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.endDate" + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\" /><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"   ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                        $scope.fields["DatePart_Calander_Open" + "items.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "items.endDate"] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if (totLevelCnt1 == 1) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.setFieldKeys();
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.settreeSources();
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"MultiSelectDropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  type=\"hidden\" /></div></div>";
                                    }
                                }
                                $scope.setFieldKeys();
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        StrartUpload_UploaderAttr();
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div class="control-group"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        $scope.dyn_Cont += '<img id="UploaderPreviewcc_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
                        $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" data-target="#EntityCreationUplaodImagediv" data-toggle="modal" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.dTextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\" class=\"currencySelector\" ><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div>";
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        $scope.setFieldKeys();
                    }
                    $scope.setFieldKeys();
                    if ($scope.atributesRelationList[i].IsReadOnly == true) {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    } else {
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
                $scope.dyn_Cont += ' <input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#EntityMetadata").html($compile($scope.dyn_Cont)($scope));
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', {
                        aSep: ' ',
                        vMin: "0",
                        mDec: "1"
                    });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";
                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: $scope.mindec
                        });
                    } else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "0"
                        });
                    }
                }
                setTimeout(function () {
                    $('[id^=TextSingleLine]:enabled:visible:first').focus().select()
                }, 1000);
                GetValidationList();
                if ($scope.ParentID > 0) UpdateInheritFromParentScopes();
                $timeout(function () {
                    HideAttributeToAttributeRelationsOnPageLoad();
                }, 200);
            });
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            } else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) { } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                return e.Id == $scope.atributesRelationList[i].ParentValue[0]
                            }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        } else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                        return e.Id == $scope.atributesRelationList[i].ParentValue[0][j]
                                    }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["Selection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }
                                }
                            }
                        } else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) {
                                        return e.Id == defaultmultiselectvalue[j];
                                    })[0].Id);
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                        return e.Caption == $scope.treeTexts[dropdown_text]
                                    }))[0];
                                }
                            } else {
                                if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                    return e.Caption == $scope.treeTexts[dropdown_text]
                                }))[0];
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                            return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                        }))[0];
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, j, levelCount, 12);
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        } else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                            }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }
                }
            }
        }

        function GetValidationList() {
            CostcentrecreationService.GetValidationDationByEntitytype(5).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#EntityMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTemp',
                        'silentSubmit': 'true'
                    });
                }
            });
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        CostcentrecreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.GetCostCentreCurrencyRateById = function () {
            CostcentrecreationService.GetCostCentreCurrencyRateById(0, $scope.SelectedCostCentreCurrency.TypeId, true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    $scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                }
            });
        }
        CostcentrecreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            if (atrid != undefined) {
                CostcentrecreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                    if (resCurrencyRate.Response != null) {
                        $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                        if ($scope['origninalamountvalue_' + atrid] != 0) {
                            $scope.fields["dTextSingleLine_" + atrid] = parseInt((parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid])).formatMoney(0, ' ', ' ');
                            $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                        }
                    }
                });
            }
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };
        $scope.closecostcentrecreationPopup = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.costcentrecreationCtrl']"));
        });
    }
    app.controller("mui.planningtool.costcentre.costcentrecreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'CostcentrecreationService', '$modalInstance', 'params', muiplanningtoolcostcentrecostcentrecreationCtrl]);
})(angular, app);
///#source 1 1 /app/services/cclist-service.js
(function(ng,app){"use strict";function CclistService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetFilterSettings:GetFilterSettings,MemberAvailable:MemberAvailable,DeleteCostcentre:DeleteCostcentre,UpdateCostCentreApprovedBudget:UpdateCostCentreApprovedBudget,GetApprovedBudgetDate:GetApprovedBudgetDate,GetCostCentreAssignedAmount:GetCostCentreAssignedAmount,GetEntityTypeByID:GetEntityTypeByID,CostCentreRootLevel:CostCentreRootLevel,GetAllEntityIds:GetAllEntityIds});function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function MemberAvailable(formobj){var request=$http({method:"post",url:"api/Planning/MemberAvailable/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteCostcentre(formobj){var request=$http({method:"post",url:"api/Planning/DeleteCostcentre/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateCostCentreApprovedBudget(formobj){var request=$http({method:"post",url:"api/Planning/UpdateCostCentreApprovedBudget/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetApprovedBudgetDate(ListId){var request=$http({method:"get",url:"api/Planning/GetApprovedBudgetDate/"+ListId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreAssignedAmount(CostCentreID){var request=$http({method:"get",url:"api/Planning/GetCostCentreAssignedAmount/"+CostCentreID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeByID(ID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CostCentreRootLevel(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreRootLevel/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllEntityIds(){var request=$http({method:"get",url:"api/Metadata/GetAllEntityIds/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CclistService",['$http','$q',CclistService]);})(angular,app);
///#source 1 1 /app/services/planfiltersettings-service.js
(function(ng,app){function PlanfiltersettingsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GettingEntityForRootLevel:GettingEntityForRootLevel,GetOptionsFromXML:GetOptionsFromXML,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings})
function GettingEntityForRootLevel(IsRootLevel){var request=$http({method:"get",url:"api/Metadata/GettingEntityForRootLevel/"+IsRootLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanfiltersettingsService",['$http','$q',PlanfiltersettingsService]);})(angular,app);
///#source 1 1 /app/services/costcentrecreation-service.js
(function(ng,app){"use strict";function CostcentrecreationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetUserById:GetUserById,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,CreateCostcentre:CreateCostcentre,GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetAttributeToAttributeRelationsByIDForEntity:GetAttributeToAttributeRelationsByIDForEntity,GetEntityTypeAttributeRelationWithLevelsByID:GetEntityTypeAttributeRelationWithLevelsByID,GetValidationDationByEntitytype:GetValidationDationByEntitytype});function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateCostcentre(formobj){var request=$http({method:"post",url:"api/Planning/CreateCostcentre/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeToAttributeRelationsByIDForEntity(ID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(ID,ParentID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/"+ID+"/"+ParentID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetValidationDationByEntitytype(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetValidationDationByEntitytype/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CostcentrecreationService",['$http','$q',CostcentrecreationService]);})(angular,app);
