﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Dal;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    class DynamicAttributesDao : BaseDao, ICloneable
    {
        #region Member Variables
		
        protected Guid _id;
        protected int _entityId;
      //  protected Iesi.Collections.Generic.ISet<DynamicnewAttributes> _attributes;
		protected List<KeyValuePair<String, String>> _attributes;

		#endregion
		
		#region Constructors
		public DynamicAttributesDao() {}

        public DynamicAttributesDao(Guid pId, int pEntityId, List<KeyValuePair<String, String>> pAttributes)
		{
			this._id = pId;
            this._entityId = pEntityId;
			this._attributes = pAttributes;  
		}

        public  DynamicAttributesDao(Guid pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual Guid Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public virtual int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }
        public virtual List<KeyValuePair<String, String>> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }

        }
		
		#endregion
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string DynamicAttributesDao = "DynamicAttributesDao";			
			public const string Id = "Id";
            public const string EnityId = "EntityId";
            public const string Attributes = "Attributes";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string DynamicAttributesDao = "DynamicAttributesDao";
            public const string Id = "Id";
            public const string EnityId = "EntityId";
            public const string Attributes = "Attributes";					
		}

		#endregion
    }
}
