/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// Attribute object for table 'MM_Attribute'.
	/// </summary>
	
	internal class Attribute : IAttribute 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
        protected string _description;
		protected int _attributetypeid;
		protected bool _issystemdefined;
        protected bool _isspecial;
        protected int _level;
        protected string _type;
		
		#endregion
		
		#region Constructors
		public Attribute() {}

        public Attribute(int pId, string pCaption, string pDescription, int pAttributeTypeid, bool pIsSystemDefined, bool pIsSpecial,string pType)
		{
			this._id = pId; 
			this._caption = pCaption;
            this._description = pDescription;
			this._attributetypeid = pAttributeTypeid; 
			this._issystemdefined = pIsSystemDefined;
            this._isspecial = pIsSpecial;
            this._type = pType; 

		}
				
		public Attribute(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
        public string Type
        {
            get { return _type; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Type", "Type value, cannot contain more than 50 characters");
                _type = value;
            }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }

        public int AttributeTypeID
		{
			get { return _attributetypeid; }
			set { _attributetypeid = value; }
			
		}
		
		public bool IsSystemDefined
		{
			get { return _issystemdefined; }
			set { _issystemdefined = value; }
			
		}
		
		
        public bool IsSpecial
        {
            get { return _isspecial; }
            set { _isspecial = value; }

        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }
     
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Attribute castObj = null;
			try
			{
				castObj = (Attribute)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
