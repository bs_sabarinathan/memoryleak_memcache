﻿(function (ng, app) {
    "use strict ";
    function attributegrouptooltipCntrl($scope, $resource, $location, $compile, $timeout, $translate, AdminService) {
        $scope.AttributeGrouptype = 0;
        $scope.attributeRelationarr = [];
        $scope.attributeGrpDataArr = [];
        $scope.groupattribtues = [];
        $scope.getAttributeGroupData = function () {
            AdminService.GetAllAttributeGroup().then(function (res) {
                var Attr_result = res.Response;
                if (Attr_result.length > 0)
                {
                    var attr = res.Response;
                    for (var i = 0; i < attr.length; i++) {
                        $scope.attributeRelationarr.push({
                            "Caption": attr[i].Caption,
                            "Id": attr[i].Id
                        });
                    }

                    $timeout(function () {
                        $scope.AttributeGrouptype = $scope.attributeRelationarr[0].Id
                    }, 100);

                    AdminService.GetAttrGroupTooltipValues($scope.attributeRelationarr[0].Id).then(function (res) {

                        $scope.groupattribtues = res.Response[0];
                    });
                }
            });
        };

        $scope.getAttributeGroupData();

        $scope.loadattributes = function (GroupID) {
            var Id = $scope.AttributeGrouptype;

            AdminService.GetAttrGroupTooltipValues(Id).then(function (res) {
                
                $scope.groupattribtues = res.Response[0];
            });
        }

        $scope.GetChckedView = function (item) {
            var attributedetails = $scope.groupattribtues;
            if (item.IsToolTip == false)
                $.grep(attributedetails, function (e) { return e.ID == item.ID })[0].IsToolTip = true;
            else if(item.IsToolTip == true)
                $.grep(attributedetails, function (e) { return e.ID == item.ID })[0].IsToolTip = false;

            var data = {};
            data.GroupID = $scope.AttributeGrouptype;
            data.AttributeID = $.grep(attributedetails, function (e) { return e.ID == item.ID })[0].ID;
            data.ToolTip = $.grep(attributedetails, function (e) { return e.ID == item.ID })[0].IsToolTip;
            data.attributedetails = $.grep(attributedetails, function (e) { return e.IsToolTip == true });
            AdminService.UpdateAttributeGroupTooltip(data).then(function (res) {
                if (res.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption'));
                    if ($('#attributegrouptypeId').val() != null && $('#attributegrouptypeId').val() != 0) {
                        AdminService.GetAttrGroupTooltipValues($('#attributegrouptypeId').val())
                            .then(
                                function (data) {
                                    var res = [];
                                    res = data.Response;
                                    $scope.groupattribtues = res[0];
                                }
                            );
                    }
                }
                else
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'))
            });
        }

    }
    app.controller("mui.admin.attributegrouptooltipCntrl", ['$scope', '$resource', '$location', '$compile', '$timeout', '$translate', 'AdminService', attributegrouptooltipCntrl]);
})(angular, app);