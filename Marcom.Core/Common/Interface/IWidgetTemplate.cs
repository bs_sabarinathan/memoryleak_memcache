﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;
namespace BrandSystems.Marcom.Core.Common.Interface
{
    public  interface IWidgetTemplate
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string TemplateName
        {
            get;
            set;

        }

        string TemplateDescription
        {
            get;
            set;

        }



        #endregion
    }
}
