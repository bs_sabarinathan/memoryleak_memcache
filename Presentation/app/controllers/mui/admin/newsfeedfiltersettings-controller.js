﻿(function (ng, app) {
    "use strict";

    function muiadminnewsfeedfiltersettingsCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.FeedActionsSelected = '';
        $scope.Id = 0;
        $scope.groupadd = true;
        $scope.GetFeedGroups = function () {
            AdminService.GetFeedFilter().then(function (res) {
                $scope.feedfiltergroupData = $.grep(res.Response, function (e) {
                    return e.Id != 10;
                });
            });
        }
        $scope.GetFeedGroups();
        AdminService.GetFeedTemplates().then(function (res) {
            $scope.feedactions = res.Response;
        });
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.editableInPopup = '<a class="iconLink"  ng-click="GetFeedGroupsById(row)"  data-target="#FeedFilterModal"><i class="icon-edit"></i></a> '
        $scope.Delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteFeedGroupsById(row)" data-toggle="modal"><i class="icon-remove"></i></a> '
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.addNewFeedFilterGroup = function () {
            $('#FeedFilterModal').modal('show');
            $scope.MulitipleFeedFilterStatus = '';
            $scope.FeedGroup = '';
            $scope.Id = 0;
            $scope.groupadd = true;
            $scope.groupupdate = false;
        };
        $scope.DeleteFeedGroupsById = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2009.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteFeedGroupByid(row.entity.Id).then(function (res) {
                            if (res.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                            } else {
                                $scope.GetFeedGroups();
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.GetFeedGroupsById = function (row) {
            $('#FeedFilterModal').modal('show');
            $scope.Id = row.entity.Id;
            $scope.FeedGroup = row.entity.FeedGroup;
            var arr = row.entity.Template.split(',');
            $scope.MulitipleFeedFilterStatus = [];
            for (var i = 0, temp; temp = arr[i++];) {
                $scope.MulitipleFeedFilterStatus.push(parseInt(temp));
            }
            $scope.groupadd = false;
            $scope.groupupdate = true;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        };
        $scope.SaveNewsFeedFilterGroup = function () {
            var addfeedfilter = {};
            addfeedfilter.ID = $scope.Id;
            addfeedfilter.FeedGroup = $scope.FeedGroup;
            addfeedfilter.Template = $scope.MulitipleFeedFilterStatus.join(',');
            AdminService.InsertUpdateFeedFilterGroup(addfeedfilter).then(function (res) {
                if (res.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4330.Caption'));
                } else {
                    $scope.GetFeedGroups();
                    $('#FeedFilterModal').modal('hide');
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                }
            });
        }
        $scope.gridGetFeedFilterGroup = {
            data: 'feedfiltergroupData',
            enablePinning: false,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "FeedGroup",
                displayName: $translate.instant('LanguageContents.Res_4980.Caption'),
                width: 180
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.Delete,
                width: 40
            }]
        };
    }
    app.controller("mui.admin.newsfeedfiltersettingsCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminnewsfeedfiltersettingsCntrl]);
})(angular, app);