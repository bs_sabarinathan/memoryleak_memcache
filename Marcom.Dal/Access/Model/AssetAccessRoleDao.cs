﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class AssetAccessRoleDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _AssetAccessID;
        protected int _userid;
        #endregion

        #region Constructors

        public AssetAccessRoleDao() { }

        public AssetAccessRoleDao(int pId, int pAssetAccessID, int pUserid)
        {
            this._id = pId;
            this._AssetAccessID = pAssetAccessID;
            this._userid = pUserid; 
        }
        #endregion

        #region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int AssetAccessID
        {
            get { return _AssetAccessID; }
            set { _AssetAccessID = value; }

        }

        public virtual int Userid
        {
            get { return _userid; }
            set { _userid = value; }

        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {


            public const string Id = "Id";
            public const string AssetAccessRoleDao = "AssetAccessRoleDao";

            public const string AssetAccessID = "AssetAccessID";
            public const string Userid = "Userid";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {

            public const string Id = "Id";
            public const string AssetAccessRoleDao = "AssetAccessRoleDao";

            public const string AssetAccessID = "AssetAccessID";
            public const string Userid = "Userid";

        }

        #endregion
    }
        
}
