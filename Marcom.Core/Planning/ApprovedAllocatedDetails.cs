﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// PurchaseOrder object for table 'PM_PurchaseOrder'.
    /// </summary>

    internal class ApprovedAllocatedDetails : IApprovedAllocatedDetails
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _cosrcentreid;
        protected DateTime _plandate;
        protected string _description;
        protected decimal _planAmount;
        protected int _currencytype;
        protected int _userid;


        #endregion



        #region Constructors
        public ApprovedAllocatedDetails() { }

        public ApprovedAllocatedDetails(int pEntityid, int pCosrcentreId, DateTime pPlanDate, string pDescription, int pCurrencyType, int pUserID, decimal pPlanAmount)
        {
            this._entityid = pEntityid;
            this._cosrcentreid = pCosrcentreId;
            this._plandate = pPlanDate;
            this._description = pDescription;
            this._currencytype = pCurrencyType;
            this._userid = pUserID;
            this._planAmount = pPlanAmount;

        }

        public ApprovedAllocatedDetails(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual int CostCentreID
        {
            get { return _cosrcentreid; }
            set { _cosrcentreid = value; }

        }

        public virtual DateTime PlanDate
        {
            get { return _plandate; }
            set { _plandate = value; }

        }

        public virtual decimal PlanAmount
        {
            get { return _planAmount; }
            set { _planAmount = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }

        }


        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set { _currencytype = value; }

        }

        public virtual int UserID
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public virtual string RequesterName { get; set; }
        public virtual string currencytypeName { get; set; }
        public virtual string strPlanDate { get; set; }
        public virtual string strAmount { get; set; }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            ApprovedAllocatedDetails castObj = null;
            try
            {
                castObj = (ApprovedAllocatedDetails)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
