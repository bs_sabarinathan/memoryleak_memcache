﻿using Amazon.S3;
using Amazon.S3.Model;
using CSRedis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace BrandSystems.Marcom.Dal.Utility
{
    class MarcomCacheDal<T>
    {
        #region Initialization

        private static string RedisHost = ConfigurationManager.AppSettings["RedisServer"].ToString();
        private static string RedisPassword = ConfigurationManager.AppSettings["RedisPassword"].ToString();
        private static TimeSpan CacheExpireTime = TimeSpan.Parse(ConfigurationManager.AppSettings["RedisExpireTime"]);
        private static string AppPath = ConfigurationManager.AppSettings["MarcomPresentation"];
        private static bool IsAmazonEC = ConfigurationManager.AppSettings["AmazonEC"] == "1";

        /*** Redis Initialization ***/
        RedisClient marcomCache = new RedisClient(RedisHost);

        #endregion

        #region Communicate with CacheServer

        /// <summary>
        /// Get the XMLString from MarcomCache based on the CacheKey
        /// </summary>
        /// <param name="CacheKey"> </param>
        /// <returns></returns>
        private string GetXML(string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 

                string result = marcomCache.Get(CacheKey);
                string status = (result == null || result == "") ? "Failed" : "Success";

                return result;
            }
            catch (Exception Ex)
            {
                //string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                //writeLog("", CacheKey.Split('_')[0], "Failed to GetXML", error);
                return null;
            }

        }

        /// <summary>
        /// Store (CacheKey,XMLString) pair to MarcomCache
        /// </summary>
        /// <param name="CacheObj"> </param>
        /// <param name="CacheKey"> </param>
        private void SetXML(string CacheObj, string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 

                marcomCache.Del(CacheKey);
                marcomCache.Set(CacheKey, CacheObj); // For CsRedis only, not for ElasticCache 
            }
            catch (Exception Ex)
            {
                //string error = " CacheKey :" + CacheKey.Split('_')[1] + " \n Error Message : " + Ex.ToString();
                //writeLog("", CacheKey.Split('_')[0], "SetXML", error);
            }
        }

        #endregion

        #region XML Cache

        public static XDocument ReadXML(string xmlPath, int TenantID)
        {
            try
            {
                XDocument XDoc = null;
                TenantsDal TenantsInfo = getTenantsInfo(TenantID);
                string XML = xmlPath.Replace(AppPath, "").Replace(TenantsInfo.TenantPath, "");
                XML = XML.Replace("MetadataXML/", "").Replace("MetadataXML\\", "");

                if (XML.Contains("MetadataVersion_V"))
                {
                    string version = XML.Replace("MetadataVersion_V", "").Replace(".xml", "");
                    XDoc = ReadXDocument(XmlType.Metadata, TenantID, version);
                }
                else
                {
                    switch (XML)
                    {
                        case "AdminSettings.xml":
                            XDoc = ReadXDocument(XmlType.Admin, TenantID);
                            break;
                        case "CurrentMetadataWorking.xml":
                            XDoc = ReadXDocument(XmlType.CurrentWorking, TenantID);
                            break;
                        case "CurrentSyncDBXML.xml":
                            XDoc = ReadXDocument(XmlType.CurrentSyncDB, TenantID);
                            break;
                        case "FutureMetadataWorking.xml":
                            XDoc = ReadXDocument(XmlType.FutureMetadata, TenantID);
                            break;
                        case "FutureSyncDBXML.xml":
                            XDoc = ReadXDocument(XmlType.FutureSyncDB, TenantID);
                            break;
                        case "InitialWorkingXML.xml":
                            XDoc = ReadXDocument(XmlType.InitialWorking, TenantID);
                            break;
                    }
                }
                return XDoc;
            }
            catch
            {
                return null;
            }
        }

        public static bool SaveXML(XDocument XDoc, string xmlPath, int TenantID)
        {
            try
            {
                TenantsDal TenantsInfo = getTenantsInfo(TenantID);
                string XML = xmlPath.Replace(AppPath, "").Replace(TenantsInfo.TenantPath, "");
                XML = XML.Replace("MetadataXML/", "").Replace("MetadataXML\\", "");

                if (XML.Contains("MetadataVersion_V"))
                {
                    string version = XML.Replace("MetadataVersion_V", "").Replace(".xml", "");
                    SaveXDocument(XDoc, XmlType.Metadata, TenantID, version);
                }
                else
                {
                    switch (XML)
                    {
                        case "AdminSettings.xml":
                            SaveXDocument(XDoc, XmlType.Admin, TenantID);
                            break;
                        case "CurrentMetadataWorking.xml":
                            SaveXDocument(XDoc, XmlType.CurrentWorking, TenantID);
                            break;
                        case "CurrentSyncDBXML.xml":
                            SaveXDocument(XDoc, XmlType.CurrentSyncDB, TenantID);
                            break;
                        case "FutureMetadataWorking.xml":
                            SaveXDocument(XDoc, XmlType.FutureMetadata, TenantID);
                            break;
                        case "FutureSyncDBXML.xml":
                            SaveXDocument(XDoc, XmlType.FutureSyncDB, TenantID);
                            break;
                        case "InitialWorkingXML.xml":
                            SaveXDocument(XDoc, XmlType.InitialWorking, TenantID);
                            break;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static XDocument ReadXDocument(XmlType Type, int TenantID, string metadataVersion = null)
        {
            MarcomCacheDal<XDocument> Cache = new MarcomCacheDal<XDocument>();
            TenantsDal TenantInfo = getTenantsInfo(TenantID);
            string CacheKey = TenantInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == XmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }
                string CacheString = string.Empty;
                XDocument Result = new XDocument();
                try
                {
                    CacheString = Cache.GetXML(CacheKey);
                }
                catch
                {
                    Result = ReadXDocFromFile(Type, TenantInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                if (CacheString == null || CacheString == "")
                {
                    Result = ReadXDocFromFile(Type, TenantInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                return XDocument.Parse(CacheString);
            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public static bool SaveXDocument(XDocument XDoc, XmlType Type, int TenantID, string metadataVersion = null)
        {
            MarcomCacheDal<XDocument> Cache = new MarcomCacheDal<XDocument>();
            TenantsDal TenantInfo = getTenantsInfo(TenantID);
            string CacheKey = TenantInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == XmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }

                string desireKey = string.Empty;
                switch (Type)
                {
                    case XmlType.Admin:
                        desireKey = TenantInfo.TenantPath + "AdminSettings.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }
                        break;
                    case XmlType.Metadata:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case XmlType.CurrentWorking:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case XmlType.CurrentSyncDB:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case XmlType.FutureMetadata:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case XmlType.FutureSyncDB:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case XmlType.InitialWorking:
                        desireKey = TenantInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                        if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                        {
                            uploadXdocContent(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                }

                Cache.SetXML(XDoc.ToString(), CacheKey);
                return true;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }

        private static XDocument ReadXDocFromFile(XmlType Type, TenantsDal TenantInfo, string metadataVersion)
        {
            XDocument Result = new XDocument();
            string desireKey = string.Empty;

            switch (Type)
            {
                case XmlType.Admin:
                    desireKey = TenantInfo.TenantPath + "AdminSettings.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.Metadata:

                    desireKey = TenantInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.CurrentWorking:
                    desireKey = TenantInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.CurrentSyncDB:
                    desireKey = TenantInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.FutureMetadata:
                    desireKey = TenantInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.FutureSyncDB:
                    desireKey = TenantInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case XmlType.InitialWorking:
                    desireKey = TenantInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                    if (TenantInfo.AwsStorage.storageType == (int)StorageAreaDal.Amazon)
                    {
                        Result = ReadXDocumentFromBucket(TenantInfo.AwsStorage.S3, TenantInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;

            }
            return Result;
        }

        #endregion

        #region Get TenatsInfo

        private static TenantsDal getTenantsInfo(int TenantID)
        {
            TenantsDal tenantsInfo = new TenantsDal();
            StorageBlock _awsStorage = new StorageBlock();

            string TenantInfoPath = AppPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            tenantsInfo.TenantPath = xn["FilePath"].InnerText;
            tenantsInfo.TenantName = xn["TenantName"].InnerText;
            tenantsInfo.TenantHost = xn.Attributes["TenantHost"].Value;

            System.Xml.XmlNode _aws = xn.SelectSingleNode("AWSSetup");
            _awsStorage.AWSAccessKeyID = _aws.SelectSingleNode("AWSAccessKeyID").InnerText;
            _awsStorage.AWSSecretAccessKey = _aws.SelectSingleNode("AWSSecretAccessKey").InnerText;
            _awsStorage.BucketName = _aws.SelectSingleNode("BucketName").InnerText;
            _awsStorage.ServiceURL = _aws.SelectSingleNode("ServiceURL").InnerText;
            _awsStorage.RegionEndpoint = _aws.SelectSingleNode("RegionEndpoint").InnerText;
            _awsStorage.Uploaderurl = _aws.SelectSingleNode("UploaderUrl").InnerText;
            AmazonS3Config S3Config = new AmazonS3Config()
            {
                ServiceURL = _awsStorage.ServiceURL,
                RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_awsStorage.RegionEndpoint)
            };
            AmazonS3Client client = new AmazonS3Client(_awsStorage.AWSAccessKeyID, _awsStorage.AWSSecretAccessKey, S3Config);
            _awsStorage.S3 = client;
            _awsStorage.storageType = Convert.ToInt32(xn["FileSystem"].InnerText);

            tenantsInfo.AwsStorage = _awsStorage;

            return tenantsInfo;
        }

        #endregion

        #region AmazonStorage Helper

        /// <summary>
        /// Read XML file from Bucket
        /// </summary>
        /// <param name="client"></param>
        /// <param name="bucketName"></param>
        /// <param name="s3Key"></param>
        /// <returns></returns>
        private static XDocument ReadXDocumentFromBucket(AmazonS3Client client, string bucketName, string s3Key)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/"),
                };

                GetObjectResponse response = client.GetObject(request);
                StreamReader reader = new StreamReader(response.ResponseStream);
                String content = reader.ReadToEnd();
                return XDocument.Parse(content);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Upload the XML file to the Amazon s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="BUCKET_NAME"></param> the amazon bucket name
        /// <param name="desireKey"></param> the amazon file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="content"></param> file content
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This is just a wrapper for the default provider. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        private static System.Net.HttpStatusCode uploadXdocContent(AmazonS3Client client, string bucketname, string desireKey, XDocument xDoc)
        {
            try
            {

                string content = ToOuterXml(xDoc);

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = desireKey.Replace("\\", "/"),
                    ContentBody = content,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse response2 = client.PutObject(request);

                return System.Net.HttpStatusCode.OK;
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// Upload the local file to the Amazon s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="BUCKET_NAME"></param> the amazon bucket name
        /// <param name="desireKey"></param> the amazon file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="content"></param> file content
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This is just a wrapper for the default provider. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        private static System.Net.HttpStatusCode uploadXElemContent(AmazonS3Client client, string bucketname, string desireKey, XElement xDoc)
        {
            try
            {

                string content = ToOuterXml(xDoc);

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = desireKey.Replace("\\", "/"),
                    ContentBody = content,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse response2 = client.PutObject(request);

                return System.Net.HttpStatusCode.OK;
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        private static string ToOuterXml(XDocument xmlDoc)
        {
            return xmlDoc.ToString();
        }

        private static string ToOuterXml(XElement xmlDoc)
        {
            return xmlDoc.ToString();
        }

        #endregion
    }

}
