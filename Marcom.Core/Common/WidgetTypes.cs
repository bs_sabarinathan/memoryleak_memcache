﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;


namespace BrandSystems.Marcom.Core.Common
{
    /// <summary>
    /// FeedDao object for table 'CM_WidgetTypes'.
    /// </summary>
    internal class WidgetTypes : IWidgetTypes
    {

        #region Member Variables

        protected int _id;
        protected string _typename;
        protected int _iSdynamic;
        #endregion
        #region Constructors
		public WidgetTypes() {}
			
		public WidgetTypes(string pTypeName,int piSdynamic)
		{
            this._typename = pTypeName;
            this._iSdynamic = piSdynamic; 
		}

        public WidgetTypes(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		#region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public string TypeName
        {
            get { return _typename; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("TypeName", "TypeName value, cannot contain more than 100 characters");
                _typename = value;
            }

        }

        public int ISDynamic
        {
            get { return _iSdynamic; }
            set { _iSdynamic = value; }

        }
       

        #endregion 
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            WidgetTypes castObj = null;
            try
            {
                castObj = (WidgetTypes)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
