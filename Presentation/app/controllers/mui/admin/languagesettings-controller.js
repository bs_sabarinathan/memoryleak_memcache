﻿(function (ng, app) {
    "use strict";

    function muiadminlanguagesettingsCntrl($scope, $location, $resource, $timeout, $translate, $compile, AdminService) {
        $scope.myDatatobind = [];
        $scope.listdatatobind = [];
        $scope.LanguageContentss = [];
        $scope.Contents = [];
        $scope.JsonLanguageContent = [];
        $scope.PrevJsonLangContent = [];
        $scope.LangKey = '';
        var DateValidate = dateFormat('', $scope.DefaultSettings.DateFormat);
        $scope.search = {
            Date: DateValidate
        };
        $scope.fields = {};
        $scope.actualfields = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.LanguageContentsResources = {};
        $scope.Languages = [];
        $scope.LanguageName = [];
        $scope.LanguageDescription = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        AdminService.GetLanguageTypes().then(function (res) {
            $.each(res.Response, function (i, obj) {
                res.Response[i].AddedOn = dateFormat(new Date.create(res.Response[i].AddedOn, $scope.format), $scope.format);
            });
            $scope.listdatatobind = res.Response;
            $scope.myDatatobind = $scope.listdatatobind;
        });
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.setasDefaultpopup = '<a class="btn btn-small btn-primary pull-right">' + $translate.instant('LanguageContents.Res_549.Caption') + '</a> '
        $scope.ViewPopUp = '<a class="btn btn-small btn-primary pull-right" ng-click="EditLanguage(row)" href=\"javascript:;\" >' + $translate.instant('LanguageContents.Res_550.Caption') + '</a>'
        $scope.gridOptions = {
            data: 'myDatatobind',
            enablePinning: false,
            enableColumnReordering: false,
            columnDefs: [{
                field: "SlNo",
                displayName: 'Sl.No',
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "ID",
                displayName: 'ID',
                visible: false,
                width: 0
            }, {
                field: "Name",
                displayName: 'Caption',
                width: 150
            }, {
                field: "Description",
                displayName: 'Description',
                width: 150
            }, {
                field: "AddedOn",
                displayName: 'CreatedOn',
                width: 150
            }, {
                field: "",
                displayName: 'Translation Pending',
                width: 150
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.setasDefaultpopup,
                width: 160
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.ViewPopUp,
                width: 100
            }]
        };
        $scope.SetDefaultLanguage = function (row) {
            var defaultlang = {};
            defaultlang.LangID = row.ID;
            AdminService.SetDefaultLanguage(defaultlang).then(function (setdefault) {
                if (setdefault.Response == true) {
                    NotifySuccess("" + row.Name + $translate.instant('LanguageContents.Res_4456.Caption'));
                    $timeout(function () {
                        $translate.use(row.LangKey);
                    }, 100);
                    AdminService.GetLanguageTypes().then(function (languagetypes) {
                        $.each(languagetypes.Response, function (i, obj) {
                            languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                        });
                        $scope.listdatatobind = languagetypes.Response;
                        $scope.myDatatobind = $scope.listdatatobind;
                    });
                }
            });
        }
        $scope.LanguageEditID;
        $scope.StartRows = 0;
        $scope.NextRows = 20;
        $scope.LangEditID = 0;
        $scope.LangEditName = '';
        $scope.LangEditDesc = '';
        $scope.EditLanguage = function (row) {
            $('#editlanguageModal').modal('show');
            $scope.Languages = [];
            $scope.totalDisplayed = 30;
            $scope.LoadLanguageOnEdit(row.ID, row.Name, row.Description);
        }
        $scope.searchtexts = '';
        $scope.searchby = {
            caption: ''
        };
        $scope.LoadLanguageOnEdit = function (Id, Name, Description) {
            $scope.search.Date = '';
            $("#Dynamic_Controls1").append('');
            $("#dynamicName").append('');
            $("#Dynamic_Controls1").append('');
            $scope.searchtexts = '';
            $scope.StartRows = 0;
            $scope.NextRows = 20;
            $scope.LangEditID = Id;
            $scope.LangEditName = Name;
            $scope.LanguageTypeName = "";
            $scope.LanguageTypeDescription = "";
            if (Description == "") $scope.LangEditDesc = null;
            else $scope.LangEditDesc = Description;
            $scope.dyn_Cont = '';
            $scope.dyn_Cont2 = '';
            $scope.searchtext = '';
            $scope.namesearch = '';
            $("#dynamicName").html("");
            $("#editlanguageModal").modal("show");
            AdminService.GetObjectLanguageContent($scope.LangEditID).then(function (langcon) {
                if (langcon.Response != null) {
                    $scope.JsonLanguageContent = JSON.parse(langcon.Response[0].Languagecontents);
                    $scope.PrevJsonLangContent = JSON.parse(langcon.Response[0].Languagecontents);
                    $scope.LangKey = langcon.Response[0].LangKey;
                    $scope.LanguageEditID = $scope.LangEditID;
                    $scope.editlanguagetype = $scope.LangEditID;
                    $scope.LanguageTypeName = $scope.LangEditName;
                    for (var key in $scope.JsonLanguageContent.LanguageContents) {
                        if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) {
                            $scope.LanguageContentsResources[key] = $scope.JsonLanguageContent.LanguageContents[key].Caption;
                            $scope.Languages.push({
                                Caption: $scope.JsonLanguageContent.LanguageContents[key].Caption,
                                Description: $scope.JsonLanguageContent.LanguageContents[key].Description,
                                id: key,
                                LastUpdated: dateFormat($scope.JsonLanguageContent.LanguageContents[key].LastUpdated, $scope.format)
                            });
                        }
                    }
                    var tempdata = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == $scope.editlanguagetype;
                    });
                    $scope.LanguageTypeDescription = tempdata[0].Description;
                    $scope.dyn_Cont2 = '';
                    $scope.LanguageName = $scope.LanguageTypeName;
                    $scope.LanguageDescription = $scope.LanguageTypeDescription;
                    if ($scope.LanguageEditID != 1) {
                        $scope.dyn_Cont2 += '<h3><a xeditablelanguagename href=\"javascript:;\" ng-click="entityeditcontrolclick()" IsName=1 LanguageID="' + $scope.LanguageEditID + '"  id=\"LanguageName\" data-ng-model=\"LanguageName"\   data-mode=\"inline\" data-type=\"text\" data-original-title=\"LanguageName\">' + $scope.LanguageTypeName + '</a></h3>';
                        $scope.dyn_Cont2 += '<span><a xeditablelanguagename href=\"javascript:;\" ng-click="entityeditcontrolclick()" IsName=0 LanguageID="' + $scope.LanguageEditID + '"  id=\"LanguageDescription\" data-ng-model=\"LanguageDescription"\   data-mode=\"inline\" data-type=\"text\" data-original-title=\"LanguageDescription\">' + $scope.LanguageTypeDescription + '</a></span>';
                    } else {
                        $scope.dyn_Cont2 += '<h3>' + $scope.LanguageTypeName + '</h3>';
                        $scope.dyn_Cont2 += '<span>' + $scope.LanguageTypeDescription + '</span>';
                    }
                    $("#dynamicName").html($compile($scope.dyn_Cont2)($scope));
                }
            });
        }
        var isloading = false;
        $scope.dyn_ContFullResult = "";

        function loadingScroll() {
            $('#langEditScroll').unbind('scroll');
            $('#langEditScroll').scroll(function (event) {
                if (isloading) {
                    return;
                }
                if (($(this).scrollTop() + $(this).innerHeight()) + 500 >= $(this)[0].scrollHeight) {
                    isloading = true;
                    $scope.StartRows = $scope.NextRows;
                    $scope.NextRows = $scope.NextRows + 20;
                    $scope.dyn_Cont = "";
                    AdminService.GetLanguageContent($scope.StartRows, $scope.NextRows).then(function (languagecontent) {
                        $scope.LanguageContentss = languagecontent.Response;
                        if (languagecontent.Response != null || languagecontent.Response != undefined) {
                            if (languagecontent.Response.length > 0) {
                                $scope.Contents = $scope.LanguageContentss;
                                isloading = false;
                                $scope.LanguageEditID = $scope.LangEditID;
                                $scope.editlanguagetype = $scope.LangEditID;
                                $scope.LanguageTypeName = $scope.LangEditName;
                                $scope.LanguageTypeDescription = $scope.LangEditDesc;
                                if ($scope.LangEditID != 1) {
                                    for (var i = 0; i < $scope.Contents.length; i++) {
                                        $scope.dyn_Cont += "<tr>";
                                        $scope.dyn_Cont += "<td>";
                                        for (var j = 0; j < $scope.myDatatobind.length; j++) {
                                            var langid = $scope.FormatedContents[j].ID;
                                            if (j == 0) {
                                                $scope.dyn_Cont += "<textarea ngblur rows='1' langtypeid=" + langid + " contentid=" + $scope.Contents[i].ID + " iterater=" + $scope.Contents[i].ID + " ng-model=\"fields.Caption_" + $scope.Contents[i].ID + "\"></textarea>";
                                                if ($scope.Contents[i]["Lang" + $scope.LangEditID + "_IsUpdated"] == true) {
                                                    $scope.fields["Caption_" + $scope.Contents[i].ID] = $scope.Contents[i]["Lang" + $scope.LangEditID + "_Caption"];
                                                    $scope.actualfields["Caption_" + $scope.Contents[i].ID] = $scope.Contents[i]["Lang" + $scope.LangEditID + "_Caption"];
                                                } else {
                                                    $scope.fields["Caption_" + $scope.Contents[i].ID] = "";
                                                    $scope.actualfields["Caption_" + $scope.Contents[i].ID] = "";
                                                }
                                            } else if (j == 1) {
                                                $scope.dyn_Cont += "<blockquote>";
                                                $scope.dyn_Cont += "                                <p>" + $scope.FormatedContents[j].Name + "</p>";
                                                $scope.dyn_Cont += "                                <small>" + $scope.Contents[i]["Lang1_Caption"] + "</small>";
                                                $scope.dyn_Cont += "                            </blockquote>";
                                            } else {
                                                $scope.dyn_Cont += "<blockquote>";
                                                $scope.dyn_Cont += "                                <p>" + $scope.FormatedContents[j].Name + "</p>";
                                                $scope.dyn_Cont += "                                <small>" + $scope.Contents[i]["Lang" + langid + "_Caption"] + "</small>";
                                                $scope.dyn_Cont += "                            </blockquote>";
                                            }
                                        }
                                        $scope.dyn_Cont += "</td>";
                                        $scope.dyn_Cont += "<td>" + $scope.Contents[i].Description + "</td>";
                                        $scope.dyn_Cont += "<td>" + $scope.Contents[i].AddedOn + "</td>";
                                        $scope.dyn_Cont += "</tr>";
                                    }
                                    $("#tbodydyn").html($compile($scope.dyn_Cont)($scope));
                                } else {
                                    var selected = $.grep($scope.myDatatobind, function (e) {
                                        return e.ID == $scope.LangEditID;
                                    });
                                    $scope.LanguageTypeName = selected[0].Name;
                                    $scope.LanguageTypeDescription = selected[0].Description;
                                    $scope.dyn_Cont2 = '';
                                    $scope.dyn_Cont_first = '';
                                    $scope.editlanguagetype = $scope.LangEditID;
                                    $scope.FormatedContents = [];
                                    var langcontentid = "Lang" + $scope.LangEditID + "_Caption";
                                    for (var i = 0; i < $scope.Contents.length; i++) {
                                        $scope.dyn_Cont += "<tr>";
                                        $scope.dyn_Cont += "<td>";
                                        for (var j = 0; j < $scope.myDatatobind.length; j++) {
                                            var langid = $scope.myDatatobind[j].ID;
                                            $scope.dyn_Cont += "<blockquote>";
                                            $scope.dyn_Cont += "                                <p>" + $scope.myDatatobind[j].Name + "</p>";
                                            $scope.dyn_Cont += "                                <small>" + $scope.Contents[i]["Lang" + langid + "_Caption"] + "</small>";
                                            $scope.dyn_Cont += "                            </blockquote>";
                                        }
                                        $scope.dyn_Cont += "</td>";
                                        $scope.dyn_Cont += "<td>" + $scope.Contents[i].Description + "</td>";
                                        $scope.dyn_Cont += "<td>" + $scope.Contents[i].AddedOn + "</td>";
                                        $scope.dyn_Cont += "</tr>";
                                    }
                                    if ($scope.dyn_Cont != "") {
                                        $scope.dyn_ContFullResult += $scope.dyn_Cont;
                                    }
                                }
                                if ($scope.dyn_ContFullResult != "") {
                                    $("#tbodydyn").html($compile($scope.dyn_ContFullResult)($scope));
                                }
                            }
                        }
                    });
                }
            });
        }
        $scope.searchinglangs = function () {
            if ($scope.searchtexts != "") $scope.searchlangs();
            else $scope.LoadLanguageOnEdit($scope.LangEditID, $scope.LangEditName, $scope.LangEditDesc);
        };
        $scope.searchlangs = function () {
            var id = $scope.editlanguagetype;
            var searchtext = $scope.searchtexts.toString();
            $scope.SearchLanguageContent = [];
            $scope.SearchData = [];
            $scope.StartRowsSearch = 0;
            var searchdate;
            $scope.fitlertextval = [];
            $scope.fitlertextval.push($scope.searchtexts.toString());
            ($("#searchDate").val(), 'mm/dd/yyyy');
            if (searchdate == "NaN/NaN/NaN") {
                searchdate = '';
            }
            var searchlang = {};
            searchlang.LangID = id;
            searchlang.searchtext = $scope.Searchval;
            searchlang.searchdate = searchdate;
            searchlang.StartRowsSearch = $scope.StartRowsSearch;
            searchlang.id = id;
            searchlang.searchtext = searchtext.toString();
            searchlang.filterval = $scope.fitlertextval;
            AdminService.LanguageSearchs(searchlang).then(function (res) {
                $scope.SearchLanguageContent = res.Response;
                $scope.SearchData = $scope.SearchLanguageContent;
                $scope.dyn_Cont = '';
                $scope.dyn_Cont2 = '';
                $scope.dyn_Cont += "<div class='languageDesc-Header'>";
                $scope.dyn_Cont += "    <table class='table table-striped table-language'>";
                $scope.dyn_Cont += "        <colgroup>";
                $scope.dyn_Cont += "            <col class='width3x'>";
                $scope.dyn_Cont += "            <col>";
                $scope.dyn_Cont += "            <col class='width1x'>";
                $scope.dyn_Cont += "        </colgroup>";
                $scope.dyn_Cont += "        <thead>";
                $scope.dyn_Cont += "            <tr>";
                $scope.dyn_Cont += "                <th><a href='#'>" + $translate.instant('LanguageContents.Res_44.Caption') + "</a></th>";
                $scope.dyn_Cont += "                <th><a href='#'>" + $translate.instant('LanguageContents.Res_1129.Caption') + "</a></th>";
                $scope.dyn_Cont += "                <th><a href='#'>" + $translate.instant('LanguageContents.Res_31.Caption') + "</a></th>";
                $scope.dyn_Cont += "                <th class=\"nowidth\"></th>";
                $scope.dyn_Cont += "";
                $scope.dyn_Cont += "            </tr>";
                $scope.dyn_Cont += "        </thead>";
                $scope.dyn_Cont += "    </table>";
                $scope.dyn_Cont += "</div>";
                $scope.dyn_Cont += "<div  class='languageDesc-Content' id='langEditScroll'>";
                $scope.dyn_Cont += "<table class='table table-striped table-language'>";
                $scope.dyn_Cont += "<colgroup>";
                $scope.dyn_Cont += "            <col class='width3x border-right1x'>";
                $scope.dyn_Cont += "            <col class='border-right1x'>";
                $scope.dyn_Cont += "            <col class='width1x border-right1x'>";
                $scope.dyn_Cont += "        </colgroup>";
                $scope.dyn_Cont += "<tbody  id='tbodydynsearch'>";
                if ($scope.LanguageEditID != 1) {
                    $scope.LanguageEditID = id;
                    $scope.editlanguagetype = id;
                    $scope.FormatedContents = [];
                    var langcontentid = "Lang" + $scope.LanguageEditID + "_Caption";
                    var selected = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == $scope.LanguageEditID;
                    });
                    $scope.FormatedContents.push(selected[0]);
                    var defaultlanguage = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == 1;
                    });
                    $scope.FormatedContents.push(defaultlanguage[0]);
                    for (var k = 0; k < $scope.myDatatobind.length; k++) {
                        if ($.grep($scope.FormatedContents, function (e) {
							return e.ID == $scope.myDatatobind[k].ID;
                        }).length == 0) {
                            $scope.FormatedContents.push($scope.myDatatobind[k]);
                        }
                    }
                    for (var i = 0; i < $scope.SearchData.length; i++) {
                        $scope.dyn_Cont += "<tr>";
                        $scope.dyn_Cont += "<td>";
                        for (var j = 0; j < $scope.myDatatobind.length; j++) {
                            var langid = $scope.FormatedContents[j].ID;
                            if (j == 0) {
                                $scope.dyn_Cont += "<textarea rows='1' ngblur langtypeid=" + langid + " contentid=" + $scope.SearchLanguageContent[i].ID + " iterater=" + i + " ng-model=\"fields.Caption_" + i + "\"></textarea>";
                                if ($scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_IsUpdated"] == true) {
                                    $scope.fields["Caption_" + i] = $scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_Caption"];
                                    $scope.actualfields["Caption_" + i] = $scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_Caption"];
                                } else {
                                    $scope.fields["Caption_" + i] = "";
                                    $scope.actualfields["Caption_" + i] = "";
                                }
                            } else if (j == 1) {
                                $scope.dyn_Cont += "<blockquote>";
                                $scope.dyn_Cont += "           <p>" + $scope.FormatedContents[j].Name + "</p>";
                                $scope.dyn_Cont += "          <small>" + $scope.SearchData[i]["Lang1_Caption"] + "</small>";
                                $scope.dyn_Cont += "           </blockquote>";
                            } else {
                                $scope.dyn_Cont += "<blockquote>";
                                $scope.dyn_Cont += "          <p>" + $scope.FormatedContents[j].Name + "</p>";
                                $scope.dyn_Cont += "          <small>" + $scope.SearchData[i]["Lang" + langid + "_Caption"] + "</small>";
                                $scope.dyn_Cont += "          </blockquote>";
                            }
                        }
                        $scope.dyn_Cont += "</td>";
                        $scope.dyn_Cont += "<td>" + $scope.SearchData[i].Description + "</td>";
                        $scope.dyn_Cont += "<td>" + $scope.SearchData[i].AddedOn + "</td>";
                        $scope.dyn_Cont += "</tr>";
                    }
                } else {
                    $scope.LanguageEditID = id;
                    $scope.editlanguagetype = id;
                    $scope.FormatedContents = [];
                    var langcontentid = "Lang" + $scope.LanguageEditID + "_Caption";
                    var selected = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == $scope.LanguageEditID;
                    });
                    $scope.FormatedContents.push(selected[0]);
                    var defaultlanguage = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == 1;
                    });
                    $scope.FormatedContents.push(defaultlanguage[0]);
                    for (var k = 0; k < $scope.myDatatobind.length; k++) {
                        if ($.grep($scope.FormatedContents, function (e) {
							return e.ID == $scope.myDatatobind[k].ID;
                        }).length == 0) {
                            $scope.FormatedContents.push($scope.myDatatobind[k]);
                        }
                    }
                    for (var i = 0; i < $scope.SearchData.length; i++) {
                        $scope.dyn_Cont += "<tr>";
                        $scope.dyn_Cont += "<td>";
                        for (var j = 0; j < $scope.myDatatobind.length; j++) {
                            var langid = $scope.FormatedContents[j].ID;
                            if (j == 0) {
                                $scope.dyn_Cont += "<textarea rows='1' ngblur langtypeid=" + langid + " contentid=" + $scope.SearchLanguageContent[i].ID + " iterater=" + i + " ng-model=\"fields.Caption_" + i + "\"></textarea>";
                                if ($scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_IsUpdated"] == true) {
                                    $scope.fields["Caption_" + i] = $scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_Caption"];
                                    $scope.actualfields["Caption_" + i] = $scope.SearchData[i]["Lang" + $scope.LanguageEditID + "_Caption"];
                                } else {
                                    $scope.fields["Caption_" + i] = "";
                                    $scope.actualfields["Caption_" + i] = "";
                                }
                            } else if (j == 1) {
                                $scope.dyn_Cont += "<blockquote>";
                                $scope.dyn_Cont += "           <p>" + $scope.FormatedContents[j].Name + "</p>";
                                $scope.dyn_Cont += "          <small>" + $scope.SearchData[i]["Lang1_Caption"] + "</small>";
                                $scope.dyn_Cont += "           </blockquote>";
                            } else {
                                $scope.dyn_Cont += "<blockquote>";
                                $scope.dyn_Cont += "          <p>" + $scope.FormatedContents[j].Name + "</p>";
                                $scope.dyn_Cont += "          <small>" + $scope.SearchData[i]["Lang" + langid + "_Caption"] + "</small>";
                                $scope.dyn_Cont += "          </blockquote>";
                            }
                        }
                        $scope.dyn_Cont += "</td>";
                        $scope.dyn_Cont += "<td>" + $scope.SearchData[i].Description + "</td>";
                        $scope.dyn_Cont += "<td>" + $scope.SearchData[i].AddedOn + "</td>";
                        $scope.dyn_Cont += "</tr>";
                    }
                }
                $scope.dyn_Cont += " </tbody>";
                $scope.dyn_Cont += "</table>";
                $scope.dyn_Cont += "</div>";
                $("#Dynamic_Controls1").html($compile($scope.dyn_Cont)($scope));
                $timeout(function () { loadingScroll(); }, 200);
            });
        }
        $scope.addNewLanguage = function () {
            $('#addlanguageModal').modal('show');
            $scope.Name = '';
            $scope.Description = '';
            $scope.myData = [];
            $scope.listdata = [];
            $timeout(function () {
                $('#Name').focus().select();
            }, 1000);
            AdminService.GetLanguageTypes().then(function (languagetypes) {
                $.each(languagetypes.Response, function (i, obj) {
                    languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                });
                $scope.listdata = languagetypes.Response;
                $scope.myData = $scope.listdata;
            });
        }
        $("#editlanguageModal").keypress(function (e) {
            if (e.keyCode == 27) {
                $('#editlanguageModal').modal('hide');
                AdminService.GetLanguageTypes().then(function (languagetypes) {
                    $.each(languagetypes.Response, function (i, obj) {
                        languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                    });
                    $scope.listdatatobind = languagetypes.Response;
                    $scope.myDatatobind = $scope.listdatatobind;
                });
                AdminService.GetLanguageContent().then(function (languagecontent) {
                    $scope.LanguageContentss = languagecontent.Response;
                    $scope.Contents = $scope.LanguageContentss;
                });
            }
        });
        $scope.totalDisplayed = 20;
        $scope.loadMore = function () {
            $scope.totalDisplayed += 75;
        };
        var typingTimer;
        var doneTypingInterval = 200;
        $scope.startquicksearch = function () {
            $timeout.cancel(typingTimer);
            typingTimer = $timeout(function () { doneTyping(); }, doneTypingInterval);
        }

        function doneTyping() {
            if ($scope.namesearch.toLowerCase() != "") {
                $scope.totalDisplayed = 20;
                var namesearch = $.grep($scope.Languages, function (rel) {
                    return rel.Caption.toLowerCase().indexOf($scope.namesearch.toLowerCase()) > -1;
                });
                if (namesearch.length == 0) {
                    $scope.Languages = [];
                    for (var key in $scope.JsonLanguageContent.LanguageContents) {
                        if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) {
                            $scope.Languages.push({
                                Caption: $scope.LanguageContentsResources[key],
                                Description: $scope.JsonLanguageContent.LanguageContents[key].Description,
                                id: key,
                                LastUpdated: $scope.JsonLanguageContent.LanguageContents[key].LastUpdated
                            });
                        }
                    }
                    namesearch = $.grep($scope.Languages, function (rel) {
                        return rel.Caption.toLowerCase().indexOf($scope.namesearch.toLowerCase()) > -1;
                    });
                    $scope.Languages = [];
                    $scope.Languages = namesearch;
                } else {
                    $scope.Languages = [];
                    $scope.Languages = namesearch;
                }
            } else {
                $scope.totalDisplayed = 20;
                $scope.Languages = [];
                for (var key in $scope.JsonLanguageContent.LanguageContents) {
                    if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) {
                        $scope.Languages.push({
                            Caption: $scope.LanguageContentsResources[key],
                            Description: $scope.JsonLanguageContent.LanguageContents[key].Description,
                            id: key,
                            LastUpdated: $scope.JsonLanguageContent.LanguageContents[key].LastUpdated
                        });
                    }
                }
            }
        }
        $scope.clearlangsearch = function () {
            $scope.namesearch = "";
            $scope.totalDisplayed = 20;
            $scope.Languages = [];
            for (var key in $scope.JsonLanguageContent.LanguageContents) {
                if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) {
                    $scope.Languages.push({
                        Caption: $scope.LanguageContentsResources[key],
                        Description: $scope.JsonLanguageContent.LanguageContents[key].Description,
                        id: key,
                        LastUpdated: $scope.JsonLanguageContent.LanguageContents[key].LastUpdated
                    });
                }
            }
        }
        $scope.savenewlanguage = function () {
            var languagetype = $scope.languagetype;
            var Name = $scope.Name;
            var Description = $scope.Description;
            var newlanguage = {};
            newlanguage.InheritedId = languagetype;
            newlanguage.Name = Name;
            newlanguage.Description = Description;
            AdminService.SaveNewLanguage(newlanguage).then(function (savenewlanguage) {
                if (savenewlanguage.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4789.Caption'));
                    AdminService.GetLanguageTypes().then(function (languagetypes) {
                        $.each(languagetypes.Response, function (i, obj) {
                            languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                        });
                        $scope.listdatatobind = languagetypes.Response;
                        $scope.myDatatobind = $scope.listdatatobind;
                    });
                }
                $('#addlanguageModal').modal('hide');
            });
        }
        $scope.closepopup = function () {
            $('#addlanguageModal').modal('hide');
        }
        $scope.closeeditpopup = function () {
            $('#editlanguageModal').modal('hide');
        }
        $scope.newlanguagecontent = function (langtypeid, contentid, actualvalue, element) {
            var newlanguagecontent = {};
            newlanguagecontent.LangTypeID = langtypeid;
            newlanguagecontent.ContentID = contentid;
            newlanguagecontent.newValue = actualvalue;
            AdminService.UpdateLanguageContent(newlanguagecontent).then(function (savenewlanguagecontent) {
                if (savenewlanguagecontent.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    setTimeout(function () {
                        $(element).removeClass('success');
                    }, 1000);
                }
            });
        }
        $scope.UpdateLanguageName = function (langid, IsName, newvalue) {
            var NameOrDesc = 0;
            if (IsName == "0") {
                NameOrDesc = 0;
                if (newvalue == "") newvalue = "-";
            } else {
                NameOrDesc = 1;
            }
            var updatelanguage = {};
            updatelanguage.LangTypeID = langid;
            updatelanguage.NewValue = newvalue;
            updatelanguage.NameOrDesc = NameOrDesc;
            AdminService.UpdateLanguageName(updatelanguage).then(function (updatelanguagenamedesc) {
                if (updatelanguagenamedesc.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    AdminService.GetLanguageTypes().then(function (languagetypes) {
                        $.each(languagetypes.Response, function (i, obj) {
                            languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                        });
                        $scope.listdatatobind = languagetypes.Response;
                        $scope.myDatatobind = $scope.listdatatobind;
                    });
                }
            });
        }
        $scope.Insertupdatelanguagecontent = function (result) {
            var LangContentObj = {};
            LangContentObj.LangKey = $scope.LangKey;
            LangContentObj.SelectedLangID = $scope.LangEditID;
            var TranslationPending = [];
            var keys = Object.keys($scope.JsonLanguageContent["LanguageContents"]);
            var last = keys[keys.length - 1];
            var rep = last.replace(/Res_/, '');
            var lastValue = parseInt(rep);
            for (var key in $scope.JsonLanguageContent.LanguageContents) {
                if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) if ($scope.JsonLanguageContent.LanguageContents[key].Caption == $scope.LanguageContentsResources[key]) {
                    TranslationPending++;
                } else {
                    $scope.JsonLanguageContent.LanguageContents[key].Caption = $scope.LanguageContentsResources[key];
                }
            }
            LangContentObj.TranslationPending = TranslationPending;
            var langcontent = JSON.stringify($scope.JsonLanguageContent);
            LangContentObj.JsonLanguageData = langcontent;
            $scope.LanguageContentsResources = {};
            AdminService.UpdateJsonLanguageContent(LangContentObj).then(function (Jsonresponse) {
                if (Jsonresponse.Response == true) {
                    $translate.refresh();
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    AdminService.GetLanguageTypes().then(function (languagetypes) {
                        $.each(languagetypes.Response, function (i, obj) {
                            languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                        });
                        $scope.listdatatobind = languagetypes.Response;
                        $scope.myDatatobind = $scope.listdatatobind;
                        $scope.closeeditpopup();
                    });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_5824.Caption'));
                }
            });
        }
        $scope.DecodedLanguage = function (langcontent) {
            if (langcontent != null & langcontent != "") {
                return $('<div />').html(langcontent).text();
            } else {
                return "";
            }
        };
        $scope.changelanguagetype = function (ID) {
            if (ID != null) {
                AdminService.GetlanguagetypeByID(ID).then(function (languagecontentByID) {
                    $scope.LangEditID = ID;
                    if (languagecontentByID.Response != null) {
                        $scope.JsonLanguageContent = JSON.parse(languagecontentByID.Response[0].Languagecontents);
                        $scope.LangKey = languagecontentByID.Response[0].LangKey;
                    }
                    $scope.Languages = [];
                    $scope.LanguageContentsResources = {};
                    for (var key in $scope.JsonLanguageContent.LanguageContents) {
                        if ($scope.JsonLanguageContent.LanguageContents.hasOwnProperty(key)) {
                            $scope.LanguageContentsResources[key] = $scope.JsonLanguageContent.LanguageContents[key].Caption;
                            $scope.Languages.push({
                                Caption: $scope.JsonLanguageContent.LanguageContents[key].Caption,
                                Description: $scope.JsonLanguageContent.LanguageContents[key].Description,
                                id: key,
                                LastUpdated: $scope.JsonLanguageContent.LanguageContents[key].LastUpdated
                            });
                        }
                    }
                    var tempdata = $.grep($scope.myDatatobind, function (e) {
                        return e.ID == $scope.LangEditID;
                    });
                    $scope.dyn_Cont2 = '';
                    $scope.totalDisplayed = 20;
                    $scope.LanguageEditID = $scope.LangEditID;
                    $scope.LanguageTypeDescription = tempdata[0].Description;
                    $scope.LanguageTypeName = tempdata[0].Name;
                    $scope.LanguageName = tempdata[0].Name;
                    $scope.LanguageDescription = tempdata[0].Description;
                    if ($scope.LanguageEditID != 1) {
                        $scope.dyn_Cont2 += '<h3><a xeditablelanguagename href=\"javascript:;\" ng-click="entityeditcontrolclick()" IsName=1 LanguageID="' + $scope.LanguageEditID + '"  id=\"LanguageName\" data-ng-model=\"LanguageName"\   data-mode=\"inline\" data-type=\"text\" data-original-title=\"LanguageName\">' + $scope.LanguageTypeName + '</a></h3>';
                        $scope.dyn_Cont2 += '<span><a xeditablelanguagename href=\"javascript:;\" ng-click="entityeditcontrolclick()" IsName=0 LanguageID="' + $scope.LanguageEditID + '"  id=\"LanguageDescription\" data-ng-model=\"LanguageDescription"\   data-mode=\"inline\" data-type=\"text\" data-original-title=\"LanguageDescription\">' + $scope.LanguageTypeDescription + '</a></span>';
                    } else {
                        $scope.dyn_Cont2 += '<h3>' + $scope.LanguageTypeName + '</h3>';
                        $scope.dyn_Cont2 += '<span>' + $scope.LanguageTypeDescription + '</span>';
                    }
                    $("#dynamicName").html($compile($scope.dyn_Cont2)($scope));
                });
            }
        }
        $scope.Insertnewanguage = function () {
            if ($scope.languagetype == null || $scope.languagetype == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_5542.Caption'));
                return false;
            } else if ($scope.Name == "" || $scope.Name == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_5543.Caption'));
                return false;
            } else {
                var languagetype = $scope.languagetype;
                var Name = $scope.Name;
            }
            if ($scope.Description == "") $scope.Description = "-";
            var Description = $scope.Description;
            var newlanguage = {};
            newlanguage.InheritedId = languagetype;
            newlanguage.Name = Name;
            newlanguage.Description = Description;
            AdminService.InsertNewLanguage(newlanguage).then(function (insertnewlanguage) {
                if (insertnewlanguage.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4789.Caption'));
                    AdminService.GetLanguageTypes().then(function (languagetypes) {
                        $.each(languagetypes.Response, function (i, obj) {
                            languagetypes.Response[i].AddedOn = dateFormat(new Date.create(languagetypes.Response[i].AddedOn, $scope.format), $scope.format);
                        });
                        $scope.listdatatobind = languagetypes.Response;
                        $scope.myDatatobind = $scope.listdatatobind;
                    });
                }
                $('#addlanguageModal').modal('hide');
            });
        }
        $scope.Export = function () {
            $('#ExportTasklistReportPageModel').modal("show");
            $scope.filename = "";
            var dt = new Date();
            $scope.filename = dt.getDate() + "/" + dt.getMonth() + 1 + "/" + dt.getFullYear() + ":" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var time = dt.getUTCDate();
            AdminService.GetLanguageExport($scope.LangEditID, $scope.LangEditName, time).then(function (GetExportValuesDetails) {
                if (GetExportValuesDetails.Response != null) {
                    var a = document.createElement('a'),
						fileid = time,
						extn = '.xml';
                    var filename = "Language " + $scope.filename + '.xml';
                    a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                    a.download = fileid + extn;
                    document.body.appendChild(a);
                    a.click();
                    $('#ExportTasklistReportPageModel').modal("hide");
                };
            });
        }
        var imgfileid = '';
        $(function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'pickfiles',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/CustomHandler.ashx?Path=Files/ImportExportFiles&typeoffile=Doc',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,xml"
                }],
                resize: {
                    width: 320,
                    height: 240,
                    quality: 90
                }
            });
            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader.start();
            });
            uploader.bind('Error', function (up, err) {
                $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.FileID = response.response.split(',')[0];
                Importing($scope.FileID);
            });
        });

        function Importing(val) {
            $('#ImportTasklistReportPageModel').modal("show");
            var rst = {};
            rst.FileImport = val;
            rst.LangTypeId = $scope.LangEditID;
            AdminService.InsertLanguageImport(rst).then(function (addoptionsResponse) {
                if (addoptionsResponse.Response == true) {
                    $('#ImportTasklistReportPageModel').modal("hide");
                    NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                    $scope.Languages = [];
                    $scope.LoadLanguageOnEdit($scope.LangEditID, $scope.LangEditName, $scope.LangEditDesc);
                }
                if (addoptionsResponse.Response != true) {
                    $('#ImportTasklistReportPageModel').modal("hide");
                    NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                }
            });
        }
    }
    app.directive('whenScrolled', function ($timeout) {
        return function (scope, elm, attr) {
            var raw = elm[0];
            elm.bind('scroll', function () {
                if ((raw.scrollTop + raw.offsetHeight) >= (raw.scrollHeight - 200)) {
                    $timeout(function () {
                        scope.$apply(attr.whenScrolled);
                    }, 100);
                }
            });
        };
    });
    app.controller("mui.admin.languagesettingsCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', '$compile', 'AdminService', muiadminlanguagesettingsCntrl]);
})(angular, app);