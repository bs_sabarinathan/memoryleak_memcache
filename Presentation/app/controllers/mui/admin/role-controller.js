﻿(function (ng, app) {
    "use strict";

    function muiadminroleCtrl($scope, $location, $resource, $timeout, $compile, $translate, AdminService) {
        var IsEdit = 0;
        $scope.ModuleforEntityTypeAccess = [];
        $scope.NewsfeedTempData = {};
        $scope.Selected_NewsfeedTempData = {};
        $scope.Notificationaccessdata = {};
        $scope.Selected_NotificationTempData = {};
        $scope.EntityTypeAccessInPopup = $scope.IsSuperAdmin.access == true ? '<a class="iconLink"  ng-click="GetEntityTypeAccessOnClick(row)" ng-hide="row.entity.Id== -1 || row.entity.Id==1001"  ><i class="icon-edit"></i></a> ' : '<a class="iconLink"  ng-click="GetEntityTypeAccessOnClick(row)" ng-hide="row.entity.Id== 1 || row.entity.Id==1001" ><i class="icon-edit"></i></a> ';
        $scope.FeatureAccessInPopup = $scope.IsSuperAdmin.access == true ? '<a class="iconLink" ng-click="GetFeatureAccessOnClick(row)" ng-hide="row.entity.Id== -1  ||  row.entity.Id==1001" ><i class="icon-edit"></i></a> ' : '<a class="iconLink"  ng-click="GetFeatureAccessOnClick(row)" ng-hide="row.entity.Id==1  ||  row.entity.Id==1001"  ><i class="icon-edit"></i></a> ';
        $scope.NewsfeedAccessInPopup = $scope.IsSuperAdmin.access == true ? '<a class="iconLink"  ng-click="GetNewsfeedAccessOnClick(row)" ng-hide="row.entity.Id== -1 || row.entity.Id==1001"><i class="icon-edit"></i></a> ' : '<a class="iconLink"  ng-click="GetNewsfeedAccessOnClick(row)" ng-hide="row.entity.Id== 1 || row.entity.Id==1001"><i class="icon-edit"></i></a> ';
        $scope.NotificatonAccessInPopup = $scope.IsSuperAdmin.access == true ? '<a class="iconLink"  ng-click="GetNotificationAccessOnClick(row)" ng-hide="row.entity.Id== -1 || row.entity.Id==1001"  ><i class="icon-edit"></i></a> ' : '<a class="iconLink"  ng-click="GetNotificationAccessOnClick(row)" ng-hide="row.entity.Id== 1 || row.entity.Id==1001" ><i class="icon-edit"></i></a> ';
        $scope.delete = $scope.IsSuperAdmin.access == true ? '<a class="iconLink" data-toggle="modal" ng-click="DeleteGlobalRolesByID(row)" ng-hide="row.entity.Id== -1 || row.entity.Id==10  || row.entity.Id==1001" data-toggle="modal"><i class="icon-remove"></i></a> ' : '<a class="iconLink" data-toggle="modal" ng-click="DeleteGlobalRolesByID(row)" ng-hide="row.entity.Id== 1 || row.entity.Id==10  || row.entity.Id==1001" data-toggle="modal"><i class="icon-remove"></i></a> '
        $scope.ediglobalroleInPopup = '<div class="ngCellText"><a data-toggle="modal" ng-click="GetGlobalRolesByID(row)" data-toggle="modal" data-target="#globalrolesModal">{{row.entity.Caption}}</a></div>'
        $scope.DeleteGlobalRolesByID = function DeleteGlobalRolesByID(row) {
            AdminService.DeleteGlobalRole(row.entity.Id).then(function (res) {
                if (res.Response == 2) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4147.Caption'));
                    return;
                }
                if (res.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4261.Caption'));
                } else {
                    $scope.listglobalrolesdata = res.Response;
                    NotifySuccess($translate.instant('LanguageContents.Res_4795.Caption'));
                }
                AdminService.GetGlobalRole().then(function (getglobalroles) {
                    $scope.listglobalrolesdata = getglobalroles.Response;
                    $scope.globalrolesData = $scope.listglobalrolesdata;
                });
            });
        }
        $scope.GetGlobalRolesByID = function GetGlobalRolesByID(row) {
            $scope.Id = row.entity.Id;
            $scope.Caption = row.entity.Caption;
            $scope.Description = row.entity.Description;
            $scope.EnableGlobalRoleUpdate = true;
            $scope.EnableGloabalRoleAdd = false;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        };
        AdminService.GetGlobalRole().then(function (getglobalroles) {
            $scope.listglobalrolesdata = getglobalroles.Response;
            $scope.globalrolesData = $scope.listglobalrolesdata;
        });
        $scope.addGlobalRoles = function () {
            $("#globalrolesModal").modal('show');
            $scope.Caption = '';
            $scope.Description = '';
            $scope.EnableGloabalRoleAdd = true;
            $scope.EnableGlobalRoleUpdate = false;
            $timeout(function () {
                $('#Caption').focus();
            }, 1000);
        };
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.filterOptions = {
            filterText: ''
        };
        var linkCellTemplate = '<div class="ngCellText">' + ' <a href="test">Visible text</a>' + '</div>';
        $scope.gridGlobalRole = {
            data: 'globalrolesData',
            enablePinning: false,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60,
                visible: false
            }, {
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60,
                visible: true
            }, {
                field: "Caption",
                displayName: $translate.instant('LanguageContents.Res_44.Caption'),
                cellTemplate: $scope.ediglobalroleInPopup,
                width: 180
            }, {
                field: "Description",
                displayName: $translate.instant('LanguageContents.Res_22.Caption'),
                width: 250
            }, {
                field: "Entity Type",
                displayName: $translate.instant('LanguageContents.Res_42.Caption'),
                cellTemplate: $scope.EntityTypeAccessInPopup,
                width: 80
            }, {
                field: "Feature",
                displayName: $translate.instant('LanguageContents.Res_543.Caption'),
                cellTemplate: $scope.FeatureAccessInPopup,
                width: 80
            }, {
                field: "Newsfeed",
                displayName: $translate.instant('LanguageContents.Res_1201.Caption'),
                cellTemplate: $scope.NewsfeedAccessInPopup,
                width: 80
            }, {
                field: "Notification",
                displayName: $translate.instant('LanguageContents.Res_1607.Caption'),
                cellTemplate: $scope.NotificatonAccessInPopup,
                width: 80
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 40
            }]
        };
        $scope.SaveGlobalRole = function () {
            var addglobalroles = {};
            addglobalroles.ID = 0;
            addglobalroles.Caption = $scope.Caption;
            addglobalroles.Description = $scope.Description;
            AdminService.InsertUpdateGlobalRole(addglobalroles).then(function (res) {
                if (res.Response == 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4339.Caption'));
                } else {
                    AdminService.GetGlobalRole().then(function (getglobalrolesResult) {
                        if (getglobalrolesResult.Response != null) {
                            $scope.listglobalrolesdata = getglobalrolesResult.Response;
                            $scope.globalrolesData = $scope.listglobalrolesdata;
                            $scope.EnableAdd = true;
                        }
                    });
                    NotifySuccess($translate.instant('LanguageContents.Res_4716.Caption'));
                }
            });
            $('#globalrolesModal').modal('hide');
        };
        $scope.UpdateGlobalRole = function () {
            var updateglobalroles = {};
            updateglobalroles.ID = $scope.Id;
            updateglobalroles.Caption = $scope.Caption;
            updateglobalroles.Description = $scope.Description;
            AdminService.InsertUpdateGlobalRole(updateglobalroles).then(function () {
                NotifySuccess($translate.instant('LanguageContents.Res_4874.Caption'));
                AdminService.GetGlobalRole().then(function (getglobalroles) {
                    $scope.listglobalrolesdata = getglobalroles.Response;
                    $scope.globalrolesData = $scope.listglobalrolesdata;
                });
                $('#globalrolesModal').modal('hide');
                AdminService.GetGlobalRole().then(function (getglobalroles) {
                    $scope.listglobalrolesdata = getglobalroles.Response;
                    $scope.globalrolesData = $scope.listglobalrolesdata;
                });
            });
        };
        $scope.GlobalRoleAccess = {};
        $scope.globalaccessdata = {};
        $scope.globalEntityTypeaccessdata = {};
        AdminService.GetGlobalRole().then(function (res) {
            $scope.GlobalRoleforAccess = res.Response;
            $scope.globalaccessdata = $scope.GlobalRoleforAccess;
        });
        $scope.ModuleGlobalAccess = {};
        $scope.moduleglobalaccessdata = {};
        var Version = 1;
        AdminService.GetModule().then(function (res) {
            $scope.ModuleforEntityTypeAccess = res.Response;
            $scope.ModuleforEntityTypeAccess.push({
                Id: -1,
                Caption: "Custom Tab",
                Description: "Custom Tab",
                IsEnable: true
            });
        });
        $scope.EntityGlobalAccess = {};
        $scope.entityglobalaccessdata = {};
        $scope.FeatureGlobalAccess = {};
        $scope.featureglobalaccessdata = {};
        $scope.ID = 0;
        $scope.ShowChatOption = function (features) {
            var d = ($.cookie('UserEmail').contains('brandsystems') == true && IsGlobalAdmin == true && features.Caption == 'Live Chat') ? true : false;
            return d;
        };
        $scope.GetFeatureAccessOnClick = function (row) {
            $scope.GlobalRoleCaption = row.entity.Caption;
            $scope.ModuleforEntityTypeAccess = [];
            $scope.GlobalRole = row.entity.Id;
            $scope.GlobalRoleCaption = row.entity.Caption;
            AdminService.GetModule().then(function (res) {
                $scope.ModuleforEntityTypeAccess = res.Response;
                $scope.ModuleforEntityTypeAccess.push({
                    Id: -1,
                    Caption: "Custom Tab",
                    Description: "Custom Tab",
                    IsEnable: true
                });
                AdminService.GetRoleFeatures(row.entity.Id).then(function (res) {
                    $scope.globalaccessData = res.Response;
                });
            });
            $('#globalaccessModal').modal('show');
        };
        $scope.GetEntityTypeAccessOnClick = function (row) {
           
            $scope.GlobalRole = row.entity.Id;
            $scope.GlobalRoleCaption = row.entity.Caption;
            $scope.GetEntityTypeAccessByID(row.entity.Id);
            $('#EntitytypeAccessPopUp').modal('show');
        };
        $scope.GetEntityTypeAccessByID = function (roleID) {
            AdminService.GetGlobalEntityTypeAcl(roleID, 3).then(function (res) {
                $scope.globalEntityTypeaccessdata = res.Response;
            });
        }
        $scope.SaveEntityTypeAccess = function () {
            var addglobalaccess = {};
            addglobalaccess.entitytypeAccessObj = $scope.globalEntityTypeaccessdata;
            addglobalaccess.ID = "0";
            AdminService.InsertUpdateGlobalEntitTypeACL(addglobalaccess).then(function (accessResult) {
                if (accessResult.Response == null) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4338.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $scope.GetEntityTypeAccessByID($scope.GlobalRole);
                }
                $("#EntitytypeAccessPopUp").modal('hide');
            });
        };
        $scope.LoadtQtipForEntityTypeTreeStructure = function (qtip_id, entitytypeId) {
            $scope.tempEntityTypeTreeText = '';
            AdminService.GettingEntityTypeTreeStructure(parseInt(entitytypeId, 10)).then(function (res) {
                if (res.Response != null && res.Response.length > 0) {
                    var entityTypeTree = jQuery.parseJSON(res.Response).Children;
                    for (var i = 0; i < entityTypeTree.length; i++) {
                        if (entityTypeTree[i].Children.length > 0) {
                            $scope.tempEntityTypeTreeText += entityTypeTree[i].Caption;
                            $scope.RecursiveToGetEntityTypeChildren(entityTypeTree[i].Children);
                            $scope.tempEntityTypeTreeText += "<BR/>";
                        }
                    }
                    if ($scope.tempEntityTypeTreeText == "") $('#' + qtip_id).html("Root Entity");
                    else $('#' + qtip_id).html($scope.tempEntityTypeTreeText);
                }
            });
        };
        $scope.RecursiveToGetEntityTypeChildren = function (entitytypeChildren) {
            $scope.tempEntityTypeTreeText += " > " + entitytypeChildren[0].Caption;
            if (entitytypeChildren[0].Children.length > 0) {
                $scope.RecursiveToGetEntityTypeChildren(entitytypeChildren[0].Children);
            }
        }
        $scope.SaveUpdateRoleFeatures = function SaveUpdateRoleFeatures() {
            $scope.globalaccessData;
            var insertupdglobalfeatures = {};
            insertupdglobalfeatures.GlobalAccessFeatures = $scope.globalaccessData;
            insertupdglobalfeatures.GlobalRoleID = $scope.GlobalRole;
            AdminService.SaveUpdateRoleFeatures(insertupdglobalfeatures).then(function (result) {
                if (result.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $("#globalaccessModal").modal('hide');
                } else NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
            });
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.GetNewsfeedAccessOnClick = function (row) {
            $scope.GlobalRole = row.entity.Id;
            $scope.GlobalRoleCaption = row.entity.Caption;
            AdminService.GetFeedFilter().then(function (newsfeedtemp) {
                if (newsfeedtemp.Response != null) {
                    $scope.NewsfeedTempData = newsfeedtemp.Response;
                    $timeout(function () {
                        AdminService.GetSelectedFeedFilter($scope.GlobalRole).then(function (selectednewsfeedtemp) {
                            if (selectednewsfeedtemp.Response != null) {
                                $scope.Selected_NewsfeedTempData = selectednewsfeedtemp.Response;
                                for (var i = 0; i < $scope.Selected_NewsfeedTempData.length; i++) {
                                    for (var j = 0; j < $scope.NewsfeedTempData.length; j++) {
                                        if ($scope.Selected_NewsfeedTempData[i].NewsFeedGroupID == $scope.NewsfeedTempData[j].Id) {
                                            $scope.NewsfeedTempData[j].IsChecked = true;
                                        }
                                    };
                                }
                            }
                        });
                    });
                }
            });
            $('#NewsfeedAccessPopUp').modal('show');
        }
        $scope.GetNotificationAccessOnClick = function (row) {
            $scope.GlobalRole = row.entity.Id;
            $scope.GlobalRoleCaption = row.entity.Caption;
            AdminService.GetAllSubscriptionType().then(function (res) {
                if (res.Response != null) {
                    $scope.Notificationaccessdata = res.Response;
                    AdminService.GetSelectedNotificationFilter($scope.GlobalRole).then(function (selectednotificationtemp) {
                        if (selectednotificationtemp.Response != null) {
                            $scope.Selected_NotificationTempData = selectednotificationtemp.Response;
                            for (var i = 0; i < $scope.Selected_NotificationTempData.length; i++) {
                                for (var j = 0; j < $scope.Notificationaccessdata.length; j++) {
                                    if ($scope.Selected_NotificationTempData[i].NotificationTempID == $scope.Notificationaccessdata[j].Id) {
                                        $scope.Notificationaccessdata[j].IsChecked = true;
                                    }
                                };
                            }
                        }
                    });
                }
            });
            $('#NotificationAccessPopUp').modal('show');
        }
        $scope.SaveNewsfeedAccess = function () {
            var insertupdnewsfeeddata = {};
            insertupdnewsfeeddata.GlobalAccessFeatures = {};
            insertupdnewsfeeddata.GlobalAccessFeatures = $.grep($scope.NewsfeedTempData, function (e) {
                return e.IsChecked == true;
            });
            insertupdnewsfeeddata.GlobalRoleID = $scope.GlobalRole;
            AdminService.SaveUpdateNewsfeedRoleFeatures(insertupdnewsfeeddata).then(function (result) {
                if (result.Response = true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $("#NewsfeedAccessPopUp").modal('hide');
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            });
        }
        $scope.SaveNotificationAccess = function () {
            var insertupdnotificationdata = {};
            insertupdnotificationdata.GlobalAccessFeatures = {};
            insertupdnotificationdata.GlobalAccessFeatures = $.grep($scope.Notificationaccessdata, function (e) {
                return e.IsChecked == true;
            });
            insertupdnotificationdata.GlobalRoleID = $scope.GlobalRole;
            AdminService.SaveUpdateNotificationRoleFeatures(insertupdnotificationdata).then(function (result) {
                if (result.Response = true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $("#NotificationAccessPopUp").modal('hide');
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            });
        }
    }
    app.controller("mui.admin.roleCtrl", ['$scope', '$location', '$resource', '$timeout', '$compile', '$translate', 'AdminService', muiadminroleCtrl]);
})(angular, app);