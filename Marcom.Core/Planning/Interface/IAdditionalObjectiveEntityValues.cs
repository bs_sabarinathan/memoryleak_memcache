﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IAdditionalObjectiveEntityValues interface for table 'PM_AdditionalObjectiveEntityValues'.
    /// </summary>
  public interface IAdditionalObjectiveEntityValues
    {
        #region Public Properties
        int Id
        {
            get;
            set;
        }
        int EntityId
        {
            get;
            set;

        }

        int TypeId
        {
            get;
            set;

        }
        string Instruction
        {
            get;
            set;
        }

        bool IsEnableFeedback
        {
            get;
            set;
        }
        int UnitID
        {
            get;
            set;
        }

        decimal PlannedTarget
        {
            get;
            set;

        }
        decimal TargetOutcome
        {
            get;
            set;

        }

        int RatingObjective
        {
            get;
            set;

        }

        string Comments
        {
            get;
            set;

        }

        int Fulfillment
        {
            get;
            set;

        }

        bool IsActive
            {
            get;
            set;
            }
        #endregion
    }
}
