﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;


namespace BrandSystems.Marcom.Core.Planning
{
    internal class WorkFlowOverView : IWorkFlowOverView
    {
        #region Public Properties

        public int StepID { get; set; }
        public int ActiveTasks { get; set; }
        public int UnassignedTasks { get; set; }
        public int OverdueTasks { get; set; }
        public string CurrentStepName { get; set; }
        public dynamic OptionList { get; set; }

        #endregion
    }
    internal class StepOptions : IStepOptions
    {
        public int StepID { get; set; }
        public string StepName { get; set; }
    }
}
