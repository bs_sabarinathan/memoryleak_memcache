using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IGlobalAcl interface for table 'AM_GlobalAcl'.
    /// </summary>
    public interface IGlobalAcl
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        
        int GlobalRoleid
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        int Featureid
        {
            get;
            set;

        }

        bool AccessPermission
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
