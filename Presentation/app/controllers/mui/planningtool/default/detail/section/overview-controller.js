﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailoverviewCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $stateParams, $location, $window, $sce, $translate, PlanoverviewService, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.numberValue = { value: '' }
        $scope.minDate = Date.create(new Date());
        $scope.maxDate = Date.create(new Date());
        PlanoverviewService.updateEntityOverviewStatus(0, $stateParams.ID, 10).then(function (statusUpdate) {
            if (statusUpdate.Response) {
                // console.log("Success");
            }
        });
        var milestoneuploaderAttrIDs = [];
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.calanderopened = false;
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
        };
        var Timeoutvar = {};
        $scope.PerioddirectiveCalanderopen = function ($event, attrid, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") $scope.fields["PeriodStartDateopen_" + attrid] = true;
            else $scope.fields["PeriodEndDateopen_" + attrid] = true;
        };
        $scope.EntityUserList = [];
        $scope.PersonalUserIds = [];
        $('#OverAllEntityStats').select2("enable", false);
        $scope.attrgrpImagefileName = '';
        $scope.userimgWidth = 0;
        $scope.userimgHeight = 0;
        $scope.userimgX = 0;
        $scope.userimgY = 0;
        $scope.Dropdown = [];
        var cancelNewsTimerevent;
        var FeedInitiated = false;
        $scope.StopeUpdateStatusonPageLoad = false;
        $scope.EntityStatusResult = [];
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.items = [];
        $scope.treeSources = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.treelevels = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.MilestonePrevStatus = '';
        $scope.normaltreeSources = {};
        $scope.userimg = parseInt($cookies['UserId']);
        $scope.milestoneediting = '';
        $scope.EntityHistoryid = 0;
        $scope.IsStartDateEmpty = false;
        $scope.EntityID = $stateParams.ID;
        $scope.OverAllEntityStatus = '';
        $scope.OverAllEntityInTime = '';
        $scope.OverAllEntityStatusComment = '';
        $scope.processed = {
            index: 0
        };
        $scope.agroup = [];
        $scope.DropDownTreePricing = {};
        $scope.PercentageVisibleSettings = {};
        $scope.agroupDetailBlock = [];
        $scope.agroupNotDetailBlock = [];
        var getEntityNewsFeedResultnew = [];
        var getEntityNewsFeedResult = "";
        $scope.listglobalrolesdata = [];
        $scope.entityGlobalRole = '';
        $scope.IsGlobalAccessToShow = true;
        $scope.ShowHideAttributeOption = {};

        $scope.agroupListDetailBlock = [];
        $scope.agroupListNotDetailBlock = [];
        if ($scope.isfromExternalLink != undefined && $scope.isfromExternalLink == true) {
            $scope.IsLock = true;
        }
        var Attributetypename = '';
        var node = [];
        $scope.ShowOrHideAttributeToAttributeRelationInOverview = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'NormalDropDown_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'NormalMultiDropDown_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'dropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            } else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'multiselectdropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            if (relationobj[i].AttributeTypeID == 3) {
                                if ($scope.fields[Attributetypename] != "" && $scope.fields[Attributetypename] != "-" && ($scope.ShowHideAttributeOption[Attributetypename] == undefined || $scope.ShowHideAttributeOption[Attributetypename] == "")) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        var tempvar = ($.grep($scope.normaltreeSources[Attributetypename], function (e) {
                                            return e.Caption == $scope.fields[Attributetypename]
                                        }))[0];
                                        $scope.ShowHideAttributeOption[Attributetypename] = tempvar.Id;
                                    }
                                }
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if (relationobj[i].AttributeOptionID == $scope.ShowHideAttributeOption[Attributetypename]) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 4) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        $scope['IsChanged_' + relationobj[i].AttributeID] = false;
                                        var captions = $scope.fields[Attributetypename].split(',')
                                        $scope.ShowHideAttributeOption[Attributetypename] = [];
                                        var tempvar = [];
                                        for (var x = 0; x <= captions.length; x++) {
                                            if (captions[x] != undefined) {
                                                tempvar.push(($.grep($scope.normaltreeSources[Attributetypename], function (e) {
                                                    return e.Caption.trim() == captions[x].trim()
                                                }))[0]);                                               
                                                $scope.ShowHideAttributeOption[Attributetypename].push(tempvar[x].Id);
                                            }
                                        }
                                    }
                                }
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if ($.inArray(relationobj[i].AttributeOptionID, $scope.ShowHideAttributeOption[Attributetypename]) != -1) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 6) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == true || $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == undefined) {
                                    if ($scope.treeSources["dropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["dropdown_" + relationobj[i].AttributeID] != "") {
                                        if (relationobj[i].AttributeLevel == 1) {
                                            $scope.ShowHideAttributeOption[Attributetypename] = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim()
                                            }))[0];
                                            $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                        } else {
                                            var node = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel - 1)].toString().trim()
                                            }))[0];
                                            if (node != undefined) {
                                                $scope.ShowHideAttributeOption[Attributetypename] = ($.grep(node.Children, function (e) {
                                                    return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel)].toString().trim()
                                                }))[0];
                                                $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                            }
                                        }
                                    }
                                }
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if ($scope.ShowHideAttributeOption[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 12) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != "") {
                                        $scope.ShowHideAttributeOption[Attributetypename] = ($.grep($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                            return e.Caption.toString() == $scope.treeTexts['multiselectdropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim()
                                        }))[0];
                                    }
                                }
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if ($scope.ShowHideAttributeOption[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 7) {
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    return true;
                                }
                            } else {
                                if ($scope.ShowHideAttributeOption[Attributetypename] != undefined && $scope.ShowHideAttributeOption[Attributetypename] != "") if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    } else return true;
                } else return true;
            } else return true;
        };
        $scope.$on('processnext', function (event, data) {
            var nextGroupToProcess = $scope.agroup.indexOf(($.grep($scope.agroup, function (e) {
                return e.ID == data
            }))[0]) + 1;
            if (nextGroupToProcess + 1 <= $scope.agroup.length) $scope.$broadcast("process" + $scope.agroup[nextGroupToProcess].ID);
            if (nextGroupToProcess == $scope.agroup.length) $scope.BindScrollOnNewsfeed();
        });

        function GetAttributeGroups() {
            $scope.LanguageContent = $scope.LanguageContents;
            $scope.AttrLock = $scope.IsLock;
            if ($scope.agroup.length > 0) {
                $scope.agroup.splice(0, $scope.agroup.length);
                $scope.agroupDetailBlock.splice(0, $scope.agroupDetailBlock.length);
                $scope.agroupDetailBlock.splice(0, $scope.agroupDetailBlock.length);
                $scope.agroupListDetailBlock.splice(0, $scope.agroupListDetailBlock.length);
                $scope.agroupListNotDetailBlock.splice(0, $scope.agroupListNotDetailBlock.length);
            }
            PlanoverviewService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {
                        $scope.agroupDetailBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 1 && e.RepresentationType == false
                        });
                        $scope.agroupNotDetailBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 2 && e.RepresentationType == false
                        });
                        $scope.agroupListDetailBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 1 && e.RepresentationType == true
                        });
                        $scope.agroupListNotDetailBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 2 && e.RepresentationType == true
                        });
                        $scope.agroup = attributeGrpRelation.Response;
                    } else {
                        $scope.BindScrollOnNewsfeed();
                    }
                    if ($scope.agroupDetailBlock.length > 0) Timeoutvar.broadcast = $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupDetailBlock[0].ID))
                    }, 200);
                    if ($scope.agroupNotDetailBlock.length > 0) Timeoutvar.broadcastdetails = $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupNotDetailBlock[0].ID))
                    }, 200);
                    if ($scope.agroupListDetailBlock.length > 0) Timeoutvar.broadcastattrgrp = $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupListDetailBlock[0].ID))
                    }, 200);
                    if ($scope.agroupListNotDetailBlock.length > 0) Timeoutvar.broadcastattrgrpdetail = $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupListNotDetailBlock[0].ID))
                    }, 200);
                } else {
                    $scope.BindScrollOnNewsfeed();
                }
            });
        }
        $scope.StatusAttributeData = [];
        $scope.LoadStatusblock = false;

        function OverviewStatusAttribute() {
            PlanoverviewService.FetchEntityStatusTree($stateParams.ID).then(function (GetStatusvalue) {
                $scope.StatusAttributeData = GetStatusvalue.Response;
                $scope.LoadStatusblock = true;
            });
        }
        OverviewStatusAttribute();
        $scope.changestatusoption = function (attrid, optionid, optioncaption, optionCmt, optionLvl, type, parentnode, optioncolorcode) {
            if (type == "option") {
                if (optionid == $scope.entitystatuscomment["LevelSelectedId_" + parentnode]) return false;
            }
            if (type == "comment") {
                if (optionCmt == $scope.entitystatuscomment["LevelComment_" + parentnode]) return false;
            }
            var metadata = [{
                "AttributeId": attrid,
                "EntityId": $stateParams.ID,
                "Level": optionLvl,
                "NodeId": optionid,
                "comment": optionCmt,
                "type": type,
                "ParentNode": parentnode,
                "optioncaption": optioncaption,
                "optioncolorcode": optioncolorcode
            }];
            UpdateEntityStatusMetadata(metadata);
        }
        $scope.entitystatuscomment = {};
        $scope.setfocusonComment = function (id, comment) {
            $scope.entitystatuscomment["LevelComment_" + id] = comment;
            Timeoutvar.statuscomment = $timeout(function () {
                $("#statuscomment_" + id + "").focus().select()
            }, 10);
        };
        $scope.getSelectedOptionvalue = function (levelid, selectvalue, selectid, colorcode) {
            $scope.entitystatuscomment["LevelSelectedCaption_" + levelid] = selectvalue;
            $scope.entitystatuscomment["LevelSelectedId_" + levelid] = selectid;
            $scope.entitystatuscomment["LevelSelectedColorCode_" + levelid] = colorcode;
        }
        $scope.ClearEntityComment = function (attrid, id, option) {
            refreshStatus(attrid, id, option);
        }

        function refreshStatus(attrid, id, option) {
            var updatecopedata = $.grep($scope.StatusAttributeData, function (rel) {
                return rel.attributeid == attrid && rel.levelid == id;
            })[0];
            if (updatecopedata != null) {
                if (option == "comment") updatecopedata.optioncomment = $scope.entitystatuscomment["LevelComment_" + id];
                else {
                    updatecopedata.selectid = $scope.entitystatuscomment["LevelSelectedId_" + id];
                    updatecopedata.selectvalue = $scope.entitystatuscomment["LevelSelectedCaption_" + id];
                    updatecopedata.optioncolorcode = $scope.entitystatuscomment["LevelSelectedColorCode_" + id];
                }
            }
        }

        function UpdateEntityStatusMetadata(metadata) {
            var upd = {};
            upd.EntityID = $stateParams.ID;
            upd.metadata = metadata;
            PlanoverviewService.updateOverviewStatus(upd).then(function (Result) {
                if (Result.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    $scope.entitystatuscomment["LevelSelectedCaption_" + metadata[0]["ParentNode"]] = metadata[0]["optioncaption"];
                    $scope.entitystatuscomment["LevelSelectedId_" + metadata[0]["ParentNode"]] = metadata[0]["NodeId"];
                    $scope.entitystatuscomment["LevelSelectedColorCode_" + metadata[0]["ParentNode"]] = metadata[0]["optioncolorcode"];
                    $scope.entitystatuscomment["LevelComment_" + metadata[0]["ParentNode"]] = metadata[0]["comment"] != "" ? metadata[0]["comment"] : "-";
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                    Timeoutvar.loadlatestfeed = $timeout(function () {
                        $scope.TimerForLatestFeed();
                    }, 1000);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    refreshStatus(metadata[0]["AttributeId"], metadata[0]["ParentNode"], metadata[0]["type"]);
                }
            });
        }
        $scope.UpdateEntityOverAllStatus = function (OverAllEntityStatus) {
            if (OverAllEntityStatus != "" && OverAllEntityStatus != null && OverAllEntityStatus != undefined) {
                var upd = {};
                upd.EntityID = parseInt($stateParams.ID);
                upd.EntityStatusID = $scope.OverAllEntityStatus;
                upd.OnTimeStatus = $scope.OverAllEntityInTime;
                upd.OnTimeComment = $scope.DynamicTaskListDescriptionObj;
                PlanoverviewService.UpdateEntityStatus(upd).then(function (Result) {
                    if (Result.StatusCode == 200) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                if (EntityId == parseInt($stateParams.ID, 10)) {
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] != undefined) {
                                        $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] = $("#OntimeStatus").text().trim();
                                    } else {
                                        $scope.ListViewDetails[i].data.Response.Data[j]["EntityOnTimeStatus"] = $("#OntimeStatus").text().trim();
                                    }
                                    if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] != undefined) {
                                        var res = $.grep($scope.EntityStatusResult, function (e) { return e.ID == $scope.OverAllEntityStatus });
                                        if (res.length > 0) {
                                            $scope.ListViewDetails[i].data.Response.Data[j].StatusColorCode = res[0].ColorCode;
                                        }

                                        $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] = $('#OverAllEntityStats option:selected').text();
                                    }
                                    else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $('#OverAllEntityStats option:selected').text();
                                    Timeoutvar.loadlatfeed = $timeout(function () {
                                        $scope.TimerForLatestFeed();
                                    }, 2000);
                                    return false;
                                }
                            }
                        }
                    }
                });
            }
        }
        GlistFeedgroup();

        function GlistFeedgroup() {
            PlanoverviewService.GetFeedFilter().then(function (GetFeedglistvalue) {
                $scope.FeedgResult = GetFeedglistvalue.Response;
                $scope.Feedglist = [];
                if ($scope.FeedgResult.length > 0) {
                    for (var i = 0; i < $scope.FeedgResult.length; i++) {
                        $scope.Feedglist.push({
                            ID: $scope.FeedgResult[i].Id,
                            Name: $scope.FeedgResult[i].FeedGroup
                        });
                    }
                }
            });
        };
        $scope.FeedgroupFilterByOptionChange = function () {
            PagenoforScroll = 0;
            if ($scope.Newsfilter.FeedgroupMulitipleFilterStatus.length == 0 || $scope.Newsfilter.FeedgroupMulitipleFilterStatus == undefined) {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = [];
                $scope.Newsfilter.Feedgrouplistvalues = '-1';
                $scope.LoadNewsFeedBlock();
            } else {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = $scope.Newsfilter.FeedgroupMulitipleFilterStatus;
                $scope.Newsfilter.Feedgrouplistvalues = '';
                $scope.Newsfilter.Feedgrouplistvalues = $scope.Newsfilter.FeedgroupMulitipleFilterStatus.join(',');
                $scope.LoadNewsFeedBlock();
            }
        }
        $scope.MilestoneIsLock = true;
        var perioddates = [];
        var Version = 1;
        $scope.OverAllEntityStatusVis = true;
        $scope.BindScrollOnNewsfeed = function () {
            if ($('#EntityFeedsdiv').height() > 1260) {
                $scope.GetNewsFeedforPaging();
            }
            Timeoutvar.newsfeedpaging = $timeout(function () {
                $('#EntityFeedsdiv').unbind('scroll');
                $('#EntityFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        $scope.GetNewsFeedforPaging();
                    }
                });
            }, 500)
        }
        $scope.GetNewsFeedforPaging = function () {
            PagenoforScroll += 2;
            var ID = $stateParams.ID;
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            PlanoverviewService.GetEnityFeeds(ID, PagenoforScroll, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResultForScroll) {
                getEntityNewsFeedResultnew.push(getEntityNewsFeedResultForScroll);
                var feeddivHtml = '';
                for (var i = 0; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                    feeddivHtml = '';
                    var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                    if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                        $scope.userimgsrc = $scope.NewUserImgSrc;
                    } else {
                        $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                    }
                    feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                    feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >Comment</a></span>';
                    if (feedcomCount > 1) {
                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                    } else {
                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></a></span></div></div></div>';
                    }
                    feeddivHtml = feeddivHtml + '<ul class="subComment">';
                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                        var j = 0;
                        if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                        } else {
                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                        }
                        for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            if (k == 0) {
                                feeddivHtml = feeddivHtml + '<li';
                            } else {
                                feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                            }
                            feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                            feeddivHtml = feeddivHtml + '</div></div></li>';
                        }
                    }
                    feeddivHtml = feeddivHtml + '</ul></li>';
                    $('#EntityFeedsdiv').append(feeddivHtml);
                    feeddivHtml = '';
                }
            });
        }
        var PagenoforScroll = 0;
        $scope.LoadNewsFeedBlock = function () {
            var ID = $stateParams.ID;
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            try {
                $('#EntityFeedsdiv').html('');
                PlanoverviewService.GetEnityFeeds(ID, 0, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                    var feeddivHtml = '';
                    $('#EntityFeedsdiv').html('');
                    if (getEntityNewsFeedResult.Response != null) {
                        getEntityNewsFeedResultnew.push(getEntityNewsFeedResult);
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            feeddivHtml = '';
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment">';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            feedcomCount = 0;
                            $('#EntityFeedsdiv').append(feeddivHtml);
                            feeddivHtml = '';
                        }
                    }
                    $('#EntityFeedsdiv').scrollTop(1);
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);
                });
            } catch (e) { }
            $scope.TimerForLastestFeedCallBack = function () {
                if (FeedInitiated != true) {
                    $scope.TimerForLatestFeed();
                }
                if (NewsFeedUniqueTimer != undefined) {
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);
                }
            }
            try {
                $scope.TimerForLatestFeed = function () {
                    FeedInitiated = true;
                    PlanoverviewService.GetLastEntityFeeds(ID, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                        getEntityNewsFeedResultnew.push(getEntityNewsFeedResult);
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult.Response != null) {
                            for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                                feeddivHtml = '';
                                var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                                if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 1) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                } else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment"';
                                if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                    var j = 0;
                                    if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        } else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });
                                $('#EntityFeedsdiv').prepend(feeddivHtml);
                                feedcomCount = 0;
                            }
                            FeedInitiated = false;
                        }
                    });
                }
            } catch (e) { }
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = [];
                for (var j = 0; j < getEntityNewsFeedResultnew.length; j++) {
                    for (var k = 0; k < getEntityNewsFeedResultnew[j].Response.length; k++) {
                        if (getEntityNewsFeedResultnew[j].Response[k].FeedId == parseInt(feedid)) {
                            feedfilter = $.grep(getEntityNewsFeedResultnew[j].Response, function (e) {
                                return e.FeedId == parseInt(feedid);
                            });
                        }
                    }
                }
                $("#Overviewfeed_" + feedid).next('div').hide();
                if (feedfilter != '') {
                    for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                        if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                            $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedfilter[0].FeedComment.length - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption');
                            if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                            else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        } else {
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                            $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4960.Caption');
                        }
                    }
                }
            });
            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                Timeoutvar.feedcomment = $timeout(function () {
                    $('#feedcomment_' + commentuniqueid).html('').focus();
                }, 10);
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-entityid');
                var typeid = $(this).attr('data-typeid');
                var parentid = $(this).attr('data-parentid');
                var active = $(this).attr('data-fromvalue');
                var deactive = $(this).attr('data-tovalue');
                var EntityTypeName = $(this).attr('data-entitytypename');
                PlanoverviewService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                    if (result.Response == true) {
                        if (typeid == 10) {
                            $("#EntitiesTree li.active").removeClass('active');
                            $("#EntitiesTree  li a[data-entityid=" + parentid + "]").parent('li').addClass('active');
                            $("#SectionTabs li.active").removeClass('active');
                            $("#SectionTabs li#Objective").addClass("active");
                            $location.path('/mui/planningtool/objective/detail/section/' + entityid);
                            Timeoutvar.loadbreadcrum = $timeout(function () {
                                $scope.loadBreadCrum_Edit(false)
                            }, 100);
                        } else if (typeid == 11) {
                            $("#EntitiesTree li.active").removeClass('active');
                            $("#EntitiesTree  li a[data-entityid=" + parentid + "]").parent('li').addClass('active');
                            $("#SectionTabs li.active").removeClass('active');
                            $("#SectionTabs li#Objective").addClass("active");
                            $location.path('/mui/planningtool/default/detail/section/' + entityid + '/' + 'overview' + '');
                            Timeoutvar.loadbreadcrum = $timeout(function () {
                                $scope.loadBreadCrum_Edit(false)
                            }, 100);
                        } else $location.path('/mui/planningtool/default/detail/section/' + entityid + '/overview');
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $('#EntityFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
                var TargetControl = $(this);
                var mypage = TargetControl.attr('data-Name');
                var myname = TargetControl.attr('data-Name');
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {
                var entityid = $(this).attr('data-entityid');
                var taskid = $(this).attr('data-taskid');
                var typeid = $(this).attr('data-typeid');
                PlanoverviewService.IsActiveEntity(taskid).then(function (result) {
                    if (result.Response == true) {
                        $scope.$emit("pingMUITaskEdit", {
                            TaskId: taskid,
                            EntityId: entityid
                        });
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $(window).on("ontaskActionnotification", function (event) {
                $("#loadNotificationtask").modal("hide");
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
                var EntityID = $(this).attr('data-parentid');
                var funderqid = $(this).attr('data-entityid');
                PlanoverviewService.IsActiveEntity(funderqid).then(function (result) {
                    if (result.Response == true) {
                        $("#feedFundingRequestModal").modal("show");
                        //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                        $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-costcentreid');
                PlanoverviewService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                    if (result.Response == true) {
                        $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                    if ($('.FundingRequestPopup').length != 0) {
                        $('.FundingRequestPopup').hide()
                    }
                });
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="objectivelink"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-objectiveid');
                PlanoverviewService.IsActiveEntity($(this).attr('data-objectiveid')).then(function (result) {
                    if (result.Response == true) {
                        $location.path('/mui/objective/detail/section/' + entityid + '/overview');
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $('#EntityFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);
                    PlanoverviewService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + $translate.instant('LanguageContents.Res_5055.Caption') + '</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#EntityFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="Assetpath"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-entityid');
                var typeid = $(this).attr('data-typeid');
                var parentid = $(this).attr('data-parentid');
                var Assetid = $(this).attr('data-Assetid');
                PlanoverviewService.IsAvailableAsset($(this).attr('data-Assetid')).then(function (result) {
                    if (result.Response == true) {
                        var modalInstance = $modal.open({
                            templateUrl: 'views/mui/DAM/assetedit.html',
                            controller: "mui.DAM.asseteditCtrl",
                            resolve: {
                                params: function () {
                                    return {
                                        AssetID: Assetid,
                                        IsLock: $scope.IsLock,
                                        isNotify: true,
                                        viewtype: 'ViewType'
                                    };
                                }
                            },
                            scope: $scope,
                            windowClass: 'iv-Popup',
                            backdrop: "static"
                        });
                        modalInstance.result.then(function (selectedItem) {
                            $scope.selected = selectedItem;
                        }, function () { });
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
        };
        $scope.GetEntityMembers = function () {
            PlanoverviewService.GetMember($stateParams.ID).then(function (member) {
                $scope.PersonalUserIds = [];
                $scope.EntityUserList = [];
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserList.length > 0) {
                                $.each($scope.EntityUserList, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserList.push(value);
                            } else $scope.EntityUserList.push(value);
                        }
                    });
                }
            });
        }
        $scope.funloadEntityStatus_Edit = function (EntityStatusByID, EntityStatus) {
            $scope.OverAllEntityStatusVis = true;
            var EntityStatusByIDResult = EntityStatusByID;
            if (EntityStatus != null) {
                $scope.EntityStatusResult = EntityStatus;
                //Timeoutvar.entitystatus = $timeout(function () {
                if (EntityStatusByIDResult != null) {
                    $scope.OverAllEntityStatus = '';
                    $scope.OverAllEntityStatus = EntityStatusByIDResult.Status;
                    $scope.OverAllEntityInTime = EntityStatusByIDResult.TimeStatus;
                    $scope.DynamicTaskListDescriptionObj = (EntityStatusByIDResult.Comment != null ? EntityStatusByIDResult.Comment : '-');
                    $scope.Ontime = OnSelectionChange(EntityStatusByIDResult.TimeStatus);
                }
                //}, 100);
                $timeout(function () {
                    $scope.OverAllEntityStatusVis = false;
                }, 100);
            }
            lockEntityStatus();
        }

        function lockEntityStatus() {
            if ($scope.IsLock == true || IsLock == true) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", false)
                    $('#OntimeStatus').attr('disabled', 'disabled');
                    $("#entitystatuscommentID").attr('readonly', true);
                    $scope.updatecommnet = false;
                    $scope.cancelcommnet = false;
                    $('#entityGlobalAccess').select2("enable", false);
                }, 50);
            }
            else if ($scope.IsLock == false || IsLock == false) {
                $timeout(function () {
                    $('#OverAllEntityStats').select2("enable", true)
                    $('#OntimeStatus').attr('disabled', false);
                    $("#entitystatuscommentID").attr('readonly', false);
                    $scope.updatecommnet = true;
                    $scope.cancelcommnet = true;
                    $('#entityGlobalAccess').select2("enable", true);
                }, 50);
            }
        }
        $scope.GetGlobalAccess = function () {
            if ($("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").attr('data-entitylevel') == "0") {
                $scope.IsGlobalAccessToShow = true;
                PlanoverviewService.GetGlobalRole().then(function (getglobalroles) {
                    if (getglobalroles.Response != null) {
                        $scope.listglobalrolesdata = getglobalroles.Response;
                        $scope.GetEntityGlobalAccessInDetail();
                    }
                });
            } else $scope.IsGlobalAccessToShow = false;
        }
        $scope.GetEntityGlobalAccessInDetail = function () {
            PlanoverviewService.GetEntityGlobalAccessInDetail($stateParams.ID).then(function (res) {
                if (res.Response != null) {
                    $scope.entityGlobalRole = res.Response;
                }
            });
        }
        EnableDisableLoader();

        function EnableDisableLoader() {
            $scope.detailsLoader = true;
            $scope.detailsData = false;
        }
        $scope.tempresultHolder = [];
        $scope.LoadEntityRelatedData = function () {
            $scope.EntityStatusResult = [];
            $scope.DynamicTaskListDescriptionObj = "-";
            PlanoverviewService.GetEntityRelatedDataOnLoad(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false, SystemDefinedEntityTypes.Milestone).then(function (EntityRelatedDataResult) {
                if (EntityRelatedDataResult.StatusCode != 405) {
                    if (EntityRelatedDataResult.Response != null) {
                        $scope.tempresultHolder = EntityRelatedDataResult.Response;
                        $scope.EntityHistoryid = $scope.tempresultHolder[0];
                        $scope.funloadEntityStatus_Edit($scope.tempresultHolder[1], $scope.tempresultHolder[2]);
                        LoadTaskSummaryDetl_Temp($scope.tempresultHolder[3]);
                        loadDetailblockwithdata();
                        $scope.loadEntityRelatedData_Set2();
                        GetAttributeGroups();
                    }
                }
            });
            $scope.GetGlobalAccess();
        }
        $scope.LoadEntityRelatedDataSet1 = function () {
            PlanoverviewService.GetEntityRelatedDataOnLoadSet1(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false, SystemDefinedEntityTypes.Milestone).then(function (EntityRelatedDataResultSet1) {
                if (EntityRelatedDataResultSet1.StatusCode != 405) {
                    if (EntityRelatedDataResultSet1.Response != null) {
                        $scope.LoadMilestone_Temp(EntityRelatedDataResultSet1.Response[0]);
                        $scope.AddMilestone_Temp(EntityRelatedDataResultSet1.Response[1], EntityRelatedDataResultSet1.Response[2]);
                    }
                }
            });
        }
        $scope.tempresultHolder_Set2 = [];


        $scope.loadEntityRelatedData_Set2 = function () {
            PlanoverviewService.GetEntityRelatedDataOnLoad_Set2(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false, SystemDefinedEntityTypes.Milestone).then(function (EntityRelatedDataResult_Set2) {
                if (EntityRelatedDataResult_Set2.StatusCode != 405) {
                    if (EntityRelatedDataResult_Set2.Response != null) {
                        $scope.tempresultHolder_Set2 = EntityRelatedDataResult_Set2.Response;
                        $scope.LoadSectionDetail_Temp($scope.tempresultHolder_Set2[0], $scope.tempresultHolder_Set2[1], $scope.tempresultHolder_Set2[2], $scope.tempresultHolder_Set2[3]);
                        //---------------------- GET THE ENDDATE----------------------------------//
                        $scope.newGetEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                            return ($scope.tempresultHolder_Set2[3][i].TypeID == 5, $scope.tempresultHolder_Set2[3][i].ID == 63);
                        });
                        $scope.newGetDueEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                            return ($scope.tempresultHolder_Set2[3][i].TypeID == 5, $scope.tempresultHolder_Set2[3][i].ID == 56);
                        });

                        //--------------------- GET PERIOD DATE-----------------------------------//
                        $scope.newGetPeriodEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                            return (
                            $scope.tempresultHolder_Set2[3][i].TypeID == 10);
                        });
                        //------------- SERVICE CALL TO GET THE SAVE OVERALL STATUS ------------//
                        //PlanningService.GetEntityTemplateDetails().then(function (res) {
                        //    if (res.Response != undefined || res.Response != null) {
                        //        $scope.EResults = res.Response
                        //        $scope.GetFullResults = $.grep($scope.EResults, function (n, i) {
                        //            return ($scope.EResults[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId)
                        //        });
                        //        for (var i = 0; i < res.Response.length; i++) {
                        //            if (res.Response[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId) {
                        //                if ($scope.newGetEndatesVal.length != 0 || $scope.newGetDueEndatesVal.length != 0) {
                        //                    for (var j = 0; j < $scope.newGetEndatesVal.length; j++) {
                        //                        if (res.Response[i].AttrID == $scope.newGetEndatesVal[j].ID) {
                        //                            var datEndUTCRes = "";
                        //                            var datendtval = "";
                        //                            var datPeriodEndUTCRes = "";
                        //                            var datPeriodendtval = "";
                        //                            datEndUTCRes = $scope.newGetEndatesVal[j].Value;
                        //                            if (datEndUTCRes != '-' && $scope.newGetEndatesVal[j].Value != null) {
                        //                                datEndUTCRes = $scope.newGetEndatesVal[j].Value.substr(0, 10);
                        //                                var currentdate = new Date();
                        //                                //-------------------- Consider only the Due date and Enddate--------------//
                        //                                if (ConvertDateToString(currentdate) >= datEndUTCRes && $scope.newGetEndatesVal[j].ID == 63) {
                        //                                    $scope.OverAllEntityStatus = '';
                        //                                    $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                        //                                }
                        //                            }
                        //                        }
                        //                    }
                        //                    for (var j = 0; j < $scope.newGetDueEndatesVal.length; j++) {
                        //                        if (res.Response[i].AttrID == $scope.newGetDueEndatesVal[j].ID) {
                        //                            var datEndUTCRes = "";
                        //                            var datendtval = "";
                        //                            var datPeriodEndUTCRes = "";
                        //                            var datPeriodendtval = "";

                        //                            datEndUTCRes = $scope.newGetDueEndatesVal[j].Value;

                        //                            if (datEndUTCRes != '-' && $scope.newGetDueEndatesVal[j].Value != null) {
                        //                                datEndUTCRes = $scope.newGetDueEndatesVal[j].Value.substr(0, 10);
                        //                                var currentdate = new Date();
                        //                                //-------------------- Consider only the Due date and Enddate--------------//
                        //                                if (ConvertDateToString(currentdate) >= datEndUTCRes && $scope.newGetDueEndatesVal[j].ID == 56) {
                        //                                    $scope.OverAllEntityStatus = '';
                        //                                    $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                        //                                }
                        //                            }
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //        $scope.MaxPeriodEnddate = [];
                        //        var datPeriodsUTCval = "";
                        //        var datPeriodsval = [];
                        //        if ($scope.newGetPeriodEndatesVal[0].Value[0] != null) {
                        //            if ($scope.newGetPeriodEndatesVal[0].Value != "-") {
                        //                for (var i = 0; i < $scope.newGetPeriodEndatesVal[0].Value.length; i++) {
                        //                    datPeriodsUTCval = $scope.newGetPeriodEndatesVal[0].Value[i].EndDate.substr(0, 10);
                        //                    datPeriodsval.push(new Date((datPeriodsUTCval)));
                        //                }
                        //                for (var i = 0; i < res.Response.length; i++) {
                        //                    if (res.Response[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId && res.Response[i].AttrID == $scope.newGetPeriodEndatesVal[i].ID) {
                        //                        var maxDate = ConvertDateToString(new Date(Math.max.apply(null, datPeriodsval)));
                        //                        datPeriodEndUTCRes = maxDate;
                        //                        if (datPeriodEndUTCRes != '-') {
                        //                            datPeriodendtval = datPeriodEndUTCRes;
                        //                            var currentdate = new Date();
                        //                            if (ConvertDateToString(currentdate) >= datPeriodEndUTCRes && $scope.newGetPeriodEndatesVal[i].TypeID == 10) {
                        //                                $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                        //                            }
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //});
                    }
                }
            });
            $scope.StopeUpdateStatusonPageLoad = true;
        }
        $scope.TaskListSummerydetal = []
        $scope.GetEntityMembers();
        $scope.LoadEntityRelatedData();
        $scope.LoadEntityRelatedDataSet1();
        $scope.LoadNewsFeedBlock();
        $scope.DynamicTaskListDescriptionObj = "-";

        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = []
            $scope.DynamicTaskListDescriptionObj = "-";
            $scope.Ontime = OnSelectionChange(0);
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();
        }
        $scope.DynamicClass = 'btn-success';

        function LoadTaskSummaryDetl_Temp(EntityTaskLists) {
            var TaskSummaryresult = EntityTaskLists;
            if (TaskSummaryresult != null) if (TaskSummaryresult.length > 0) {
                $scope.ShowEmptyTaskSummary = false;
                $scope.ShowLoadedTaskSummary = true;
                $scope.TaskListSummerydetal = TaskSummaryresult;
                $scope.OverAllStatus = $scope.TaskListSummerydetal[0].ActiveEntityStateID;
                $scope.Ontimecomment = ($scope.TaskListSummerydetal[0].OnTimeComment != null ? $scope.TaskListSummerydetal[0].OnTimeComment : '-');
                $scope.TasksInprgress = $scope.TaskListSummerydetal[0].TaskInProgress;
                $scope.Unassigned = $scope.TaskListSummerydetal[0].UnAssignedTasks;
                $scope.Overduetasks = $scope.TaskListSummerydetal[0].OverDueTasks;
                $scope.Unabletocomplete = $scope.TaskListSummerydetal[0].UnableToComplete;
                FindmaxWidth();
            } else {
                $scope.ShowEmptyTaskSummary = true;
                $scope.ShowLoadedTaskSummary = false;
            }
        }

        function FindmaxWidth() {
            var maxwidth = $scope.TasksInprgress;
            if (maxwidth < $scope.Unassigned) maxwidth = $scope.Unassigned;
            if (maxwidth < $scope.Overduetasks) maxwidth = $scope.Overduetasks;
            if (maxwidth < $scope.Unabletocomplete) maxwidth = $scope.Unabletocomplete;
            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }

        function OnSelectionChange(Selectionid) {
            if ($scope.OntimeStatusLists != undefined) {
                var Newvalue = $.grep($scope.OntimeStatusLists, function (n, i) {
                    return ($scope.OntimeStatusLists[i].Id == Selectionid);
                });
                $scope.DynamicClass = Newvalue[0].SelectedClass;
                return Newvalue[0].Name;
            } else return "";
        }

        function UpdateOverVieEntitytasklistVById(newValue, TimeStatusID) {
            var upd = {};
            upd.ID = $scope.OverAllStatus;
            upd.OnTimeStatus = TimeStatusID;
            upd.OnTimeComment = newValue;
            PlanoverviewService.UpdateOverviewEntityTaskList(upd).then(function (UpDateEntityTaskList) {
                if (Result.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    var NewTaskListvalue = $.grep($scope.TaskListSummerydetal, function (n, i) {
                        return ($scope.TaskListSummerydetal[i].Id == $scope.OverAllStatus);
                    });
                    if (newValue != undefined && newValue != '-') {
                        NewTaskListvalue[0].OnTimeComment = newValue
                    } else {
                        NewTaskListvalue[0].OnTimeStatus = TimeStatusID;
                    }
                }
            });
        }
        $scope.ShowCurrentTaskListDetl = function () {
            $scope.UpdateEntityTaskListId($scope.OverAllStatus);
            var newObjectlist = $.grep($scope.TaskListSummerydetal, function (n, i) {
                return ($scope.TaskListSummerydetal[i].Id == $scope.OverAllStatus);
            });
            $scope.DynamicTaskListDescriptionObj = (newObjectlist[0].OnTimeComment != null ? newObjectlist[0].OnTimeComment : '-');
            $scope.Ontimecomment = (newObjectlist[0].OnTimeComment != null ? newObjectlist[0].OnTimeComment : '-');
            $('#ngDynamicTaskCommentHolder').text($scope.DynamicTaskListDescriptionObj);
            $('#ngDynamicTaskCommentHolder').parent().find(".input-large").val($scope.DynamicTaskListDescriptionObj);
            $scope.TasksInprgress = newObjectlist[0].TaskInProgress;
            $scope.Unassigned = newObjectlist[0].UnAssignedTasks;
            $scope.Overduetasks = newObjectlist[0].OverDueTasks;
            $scope.Unabletocomplete = newObjectlist[0].UnableToComplete;
            FindmaxWidth();
        }
        $scope.UpdateEntityTaskListId = function (TaskListID) {
            var TaskStatusData = {};
            TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
            TaskStatusData.Status = TaskListID;
            PlanoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) { } else {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $('#TaskListColl option:selected').text();
                                else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $('#TaskListColl option:selected').text();
                                return false;
                            }
                        }
                    }
                }
            });
        }

        function GetMileStoneByEntityId(entityid) {
            PlanoverviewService.GetMilestoneByEntityID(entityid).then(function (getMilestoneByEntity) {
                if (getMilestoneByEntity.StatusCode == 200) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                $scope.ListViewDetails[i].data.Response.Data[j]["MileStone"] = getMilestoneByEntity.Response;
                                return false;
                            }
                        }
                    }
                }
            });
        };
        $scope.milestoneStatusOptions = [];
        $scope.milestoneStatusOptions = [{
            "Id": 1,
            "Caption": "Reached"
        }, {
            "Id": 0,
            "Caption": "Not Reached"
        }];
        $scope.fiscalyears = [];
        $scope.treeSourcesObj = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.treelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.$on('LoadSectionDetail', function (event, data) {
            $('#OverAllEntityStats').select2("enable", false)
            Timeoutvar.toclear = $timeout(function () {
                PagenoforScroll = 0;
                $scope.processed.index = 0;
                if (NewsFeedUniqueTimer != undefined) {
                    $timeout.cancel(NewsFeedUniqueTimer);
                }
                $scope.OverAllEntityStatusVis = true;
                EnableDisableLoader();
                $scope.EntityStatusResult = [];
                $('#OntimeStatus').attr('disabled', true);
                $scope.DynamicTaskListDescriptionObj = "-";
                $scope.GetEntityMembers();
                $scope.LoadEntityRelatedData();
                $scope.LoadEntityRelatedDataSet1();
                $scope.LoadNewsFeedBlock();
                $scope.StatusAttributeData = [];
                $scope.LoadStatusblock = false;
                Timeoutvar.overviewstatusclear = $timeout(function () { OverviewStatusAttribute(); }, 50);
            }, 100);
        });
        $scope.AddNewsFeed = function () {
            if ($("#feedtextholder").text() != "") {
                return false;
            }
            var addnewsfeed = {};
            addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
            addnewsfeed.TemplateID = 2;
            addnewsfeed.EntityID = parseInt($stateParams.ID);
            addnewsfeed.TypeName = "";
            addnewsfeed.AttributeName = "";
            addnewsfeed.FromValue = "";
            addnewsfeed.ToValue = $('#feedcomment').text();
            addnewsfeed.PersonalUserIds = $scope.PersonalUserIds;
            PlanoverviewService.PostFeed(addnewsfeed).then(function (savenewsfeed) { });
            $('#feedcomment').empty();
            if (document.getElementById('feedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                document.getElementById('feedcomment').innerHTML = '<span id=\'feedtextholder\' class=\'placeholder\'>' + $translate.instant('LanguageContents.Res_1202.Caption') + '...</span>';
            }
            $scope.PersonalUserIds = [];
            $scope.TimerForLatestFeed();
        };
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
        };
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
        $scope.LoadFinancialSummanyBlock = function (CostCentreFin) {
            $scope.financialchartpoints = [];
            var FinancialResult = CostCentreFin;
            $scope.financialchartpoints = [{
                label: $translate.instant('LanguageContents.Res_1702.Caption'),
                y: parseFloat((parseFloat(FinancialResult['PlannedAmount']) / $scope.SelectedCostCentreCurrency.Rate).formatMoney(0, ' ', ' ').replace(/ /g, ''))
            }, {
                label: $translate.instant('LanguageContents.Res_5525.Caption'),
                y: parseFloat((parseFloat(FinancialResult['ApprovedAllocation']) / $scope.SelectedCostCentreCurrency.Rate).formatMoney(0, ' ', ' ').replace(/ /g, ''))
            }, {
                label: $translate.instant('LanguageContents.Res_1164.Caption'),
                y: parseFloat((parseFloat(FinancialResult['Committed']) / $scope.SelectedCostCentreCurrency.Rate).formatMoney(0, ' ', ' ').replace(/ /g, ''))
            }, {
                label: $translate.instant('LanguageContents.Res_1725.Caption'),
                y: parseFloat((parseFloat(FinancialResult['Spent']) / $scope.SelectedCostCentreCurrency.Rate).formatMoney(0, ' ', ' ').replace(/ /g, ''))
            }, {
                label: $translate.instant('LanguageContents.Res_1726.Caption'),
                y: parseFloat((parseFloat(FinancialResult['AvailabletoSpent']) / $scope.SelectedCostCentreCurrency.Rate).formatMoney(0, ' ', ' ').replace(/ /g, ''))
            }];
            CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#E7E796", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
            var chart = new CanvasJS.Chart("financialchart", {
                animationEnabled: true,
                colorSet: "finColorSet1",
                axisX: {
                    tickColor: "white",
                    tickLength: 5,
                    labelFontFamily: "sans-serif",
                    labelFontColor: "#7F7F7F",
                    labelFontSize: 10,
                    labelFontWeight: "normal"
                },
                axisY: {
                    tickLength: 0,
                    labelFontSize: 0,
                    lineThickness: 0,
                    gridThickness: 0,
                    includeZero: true,
                },
                data: [{
                    type: "column",
                    indexLabel: "{y}",
                    indexLabelFontSize: 12,
                    indexLabelFontWeight: "bold",
                    indexLabelPlacement: "outside",
                    indexLabelOrientation: "horizontal",
                    legendMarkerColor: "gray",
                    dataPoints: $scope.financialchartpoints
                }]
            });
            chart.render();
        }
        $scope.LoadSectionDetail_Temp = function (AttrtoAttrRel, EntAttrValDet, CostCentreFin, EntAttrDet) {
            var ID = $stateParams.ID;
            try {
                $scope.listAttriToAttriResult = [];
                try {
                    var entityAttrToAttrRelation = AttrtoAttrRel;
                    $scope.listAttriToAttriResult = [];
                    if (entityAttrToAttrRelation != null) $scope.listAttriToAttriResult = entityAttrToAttrRelation;
                } catch (ex) { }
                var getentityattributesValidationDetails = EntAttrValDet;
                $scope.listAttributeValidationResult = [];
                $scope.listAttributeValidationResult = getentityattributesValidationDetails;
                $scope.LoadFinancialSummanyBlock(CostCentreFin);
            } catch (e) { }
            try {

                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                $scope.WorkFlowDiable = true;
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {
                        var TaskStatusData = {};
                        TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        PlanoverviewService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }
                } catch (exc) { }
            } catch (e) { }
        };



        function loadDetailblockwithdata() {

            var ID = $stateParams.ID;

            PlanoverviewService.GetEntityRelatedDataOnLoad_detailblock(parseInt($stateParams.ID)).then(function (EntityRelatedDataResult_Set2) {
                if (EntityRelatedDataResult_Set2.StatusCode != 405) {

                    console.log("response came");

                    $scope.detailsLoader = false;
                    $scope.detailsData = true;

                    $scope.fields = {
                        usersID: 0,
                    };
                    $scope.tree = {};
                    $scope.fieldKeys = [];
                    $scope.options = {};
                    $scope.setFieldKeys = function () {
                        var keys = [];
                        angular.forEach($scope.fields, function (key) {
                            keys.push(key);
                            $scope.fieldKeys = keys;
                        });
                    }
                    $scope.ownername = $cookies['Username'];
                    $scope.ownerid = $cookies['UserId'];
                    $scope.ownerEmail = $cookies['UserEmail'];
                    $scope.StopeUpdateStatusonPageLoad = false;
                    $scope.dyn_Cont = '';
                    var getentityattributesdetails = EntityRelatedDataResult_Set2.Response[0];
                    $scope.dyn_Cont = "";
                    $scope.attributedata = getentityattributesdetails;
                    $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                    if ($scope.IsLock == false) {
                        $scope.ActivityName = $scope.RootLevelEntityName;
                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_1.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:void(0);\" ng-click="entityeditcontrolclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_1.Caption') + '\"  data-type=\"text\" data-original-title=\"Activity Name\">' + $scope.ActivityName + '</a></div></div>';
                    } else if ($scope.IsLock == true) {
                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_1.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.RootLevelEntityNameTemp + '</label></div></div>';
                    }
                    $scope.ShowHideAttributeOption = {};


                    for (var i = 0, attrdatablock = {}; attrdatablock = $scope.attributedata[i++];) {
                        if (attrdatablock.TypeID == 6) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = attrdatablock.Caption.split(",");
                            for (var j = 0; j < attrdatablock.Lable.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: attrdatablock.Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: attrdatablock.Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: attrdatablock.Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: attrdatablock.Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["DropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                }
                            }
                            $scope.treelevels["dropdown_levels_" + attrdatablock.ID] = $scope.items;
                            //$scope.treeSources["dropdown_" + attrdatablock.ID] = attrdatablock.tree != "" ? JSON.parse(attrdatablock.tree) : [];
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < attrdatablock.Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + attrdatablock.ID][j].caption;
                                if (attrdatablock.IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group\"><label class=\"control-label AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" InheritFromParent="' + attrdatablock.IsInheritFromParent + '" ChooseFromParent="' + attrdatablock.IsChooseFromParent + '" attributeid="' + attrdatablock.ID + '" id="fields.DropDown_' + attrdatablock.ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + attrdatablock.ID + '\" >{{treeTexts.dropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 12) {
                            $scope.dyn_Cont2 = '';
                            var CaptionObj = attrdatablock.Caption;
                            $scope.treeSources["multiselectdropdown_" + attrdatablock.ID] = JSON.parse(attrdatablock.tree);
                            for (var j = 0; j < attrdatablock.Lable.length; j++) {
                                if (attrdatablock.Lable.length == 1) {
                                    var k = j;
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({
                                        caption: attrdatablock.Lable[j].Label,
                                        level: j + 1
                                    });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                    } else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                $scope.settreeTexts();
                                                fields.push(CaptionObj[k]);
                                                $scope.setFieldKeys();
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = fields.join(", ");
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                } else {
                                    if (j == 0) {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: attrdatablock.Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: attrdatablock.Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    } else {
                                        var k = j;
                                        if (j == (attrdatablock.Lable.length - 1)) {
                                            var treeTexts = [];
                                            var fields = [];
                                            $scope.items.push({
                                                caption: attrdatablock.Lable[j].Label,
                                                level: j + 1
                                            });
                                            if (k == CaptionObj.length) {
                                                $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                            } else {
                                                if (CaptionObj[k] != undefined) {
                                                    for (k; k < CaptionObj.length; k++) {
                                                        treeTexts.push(CaptionObj[k]);
                                                        $scope.settreeTexts();
                                                        fields.push(CaptionObj[k]);
                                                        $scope.setFieldKeys();
                                                    }
                                                    $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                    $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = fields.join(", ");
                                                } else {
                                                    $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                                    $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                                }
                                            }
                                        } else {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.items.push({
                                                    caption: attrdatablock.Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = CaptionObj[j];
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.items.push({
                                                    caption: attrdatablock.Lable[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                                $scope.fields["MultiSelectDropDown_" + attrdatablock.ID + "_" + (j + 1)] = "-";
                                                $scope.setFieldKeys();
                                            }
                                        }
                                    }
                                }
                            }
                            $scope.treelevels["multiselectdropdown_levels_" + attrdatablock.ID] = $scope.items;
                            $scope.settreelevels();
                            $scope.items = [];
                            $scope.settreeSources();
                            $scope.settreeTexts();
                            $scope.settreelevels();
                            $scope.setFieldKeys();
                            for (var j = 0; j < attrdatablock.Lable.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID + "_" + (j + 1)] = true;
                                var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + attrdatablock.ID][j].caption;
                                if (attrdatablock.IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" InheritFromParent="' + attrdatablock.IsInheritFromParent + '" ChooseFromParent="' + attrdatablock.IsChooseFromParent + '" attributeid="' + attrdatablock.ID + '" id="fields.MultiSelectDropDown_' + attrdatablock.ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + '\" >{{treeTexts.multiselectdropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_' + (j + 1) + '\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + attrdatablock.ID + '_' + (j + 1) + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 1 && attrdatablock.IsSpecial == false) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = "-";
                            if (attrdatablock.Caption != undefined) {
                                $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = $('<div />').html(attrdatablock.Caption).text();
                            }
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id=\"SingleLineText_' + attrdatablock.ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + attrdatablock.ID + '\"  data-type="' + attrdatablock.ID + '"  my-qtip2 qtip-content=\"' + attrdatablock.Lable + '\"  data-original-title=' + attrdatablock.Lable + '>{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 2) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = "-";
                            if (attrdatablock.Caption != undefined) {
                                $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = $('<div />').html(attrdatablock.Caption).text();
                            }
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id=\"MultiLineText_' + attrdatablock.ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + attrdatablock.ID + '\" data-type="' + attrdatablock.ID + '" my-qtip2 qtip-content=\"' + attrdatablock.Lable + '\" data-original-title=\"' + attrdatablock.Lable + '\">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 11) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["uploader_" + attrdatablock.ID] = attrdatablock.Caption;
                            $scope.setFieldKeys();
                            $scope.UploaderCaption["uploader_" + attrdatablock.ID] = attrdatablock.Lable;
                            $scope.setUploaderCaption();
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group ng-scope AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable.toString() + '</label>';
                            $scope.dyn_Cont += '<div class=\"controls\">';

                            if (attrdatablock.Caption == "" || attrdatablock.Caption == null && attrdatablock.Caption == undefined) {
                                attrdatablock.Caption = attrdatablock.Lable;
                            }
                            if (attrdatablock.Value == "" || attrdatablock.Value == null && attrdatablock.Value == undefined) {
                                attrdatablock.Value = "NoThumpnail.jpg";
                            }
                            $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + attrdatablock.Value + '" alt="' + attrdatablock.Caption + '"';
                            $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview_' + attrdatablock.ID + '">';
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '</div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    var curFiletempobj = {};
                                    var place = 'detail';
                                    curFiletempobj.place = '"' + place.toString() + '"';
                                    curFiletempobj.AttrID = attrdatablock.ID;

                                    $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefile(" + curFiletempobj.AttrID + "," + curFiletempobj.place + ")'  attributeTypeID='" + attrdatablock.TypeID + "'";
                                    $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="Uploader_' + attrdatablock.ID + '"';
                                    $scope.dyn_Cont += 'my-qtip2 qtip-content="' + attrdatablock.Lable + '"';
                                    $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + attrdatablock.ID] + '\">' + $translate.instant('LanguageContents.Res_4729.Caption');
                                    $scope.dyn_Cont += '</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '</div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 3) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            if (attrdatablock.ID == SystemDefiendAttributes.Owner && attrdatablock.IsSpecial == true) {
                                if (attrdatablock.Caption[0] != undefined) {
                                    $scope.fields["NormalDropDown_" + attrdatablock.ID] = attrdatablock.Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                    $scope.setNormalDropdownCaption();
                                    if (attrdatablock.options != null)
                                        if (attrdatablock.options.length > 0)
                                            $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                    if (attrdatablock.IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="NormalDropDown_' + attrdatablock.ID + '"  data-ng-model=\"fields.NormalDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" >{{fields.NormalDropDown_' + attrdatablock.ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                        }
                                    }
                                } else {
                                    $scope.fields["NormalDropDown_" + attrdatablock.ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                    $scope.setNormalDropdownCaption();
                                    if (attrdatablock.options != null)
                                        if (attrdatablock.options.length > 0)
                                            $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                    if (attrdatablock.IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + attrdatablock.ID + '}}</span>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label>';
                                            $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                            $scope.dyn_Cont += 'attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '"';
                                            $scope.dyn_Cont += 'attributeid="' + attrdatablock.ID + '" id="NormalDropDown_' + attrdatablock.ID + '"';
                                            $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '"';
                                            $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\"';
                                            $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" >{{fields.NormalDropDown_' + attrdatablock.ID + '}}</a>';
                                            $scope.dyn_Cont += '</div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label>';
                                            $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label>';
                                            $scope.dyn_Cont += '</div></div>';
                                        }
                                    }
                                }
                            } else {
                                if (attrdatablock.Caption[0] != undefined) {
                                    if (attrdatablock.Caption[0].length > 0) {
                                        $scope.fields["NormalDropDown_" + attrdatablock.ID] = attrdatablock.Caption[0];
                                        $scope.setFieldKeys();
                                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                        //if (attrdatablock.options.length > 0)
                                        //    $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                        $scope.setNormalDropdownCaption();
                                        if (attrdatablock.IsReadOnly == true) {
                                            var attrId = attrdatablock.ID;
                                            PlanoverviewService.GetOptionDetailListByID(attrId, ID).then(function (optionlist) {
                                                if (optionlist.Response.length > 0)
                                                    $scope.normaltreeSources["NormalDropDown_" + optionlist.Response[0].AttributeID] = optionlist.Response;
                                            });
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                        } else {
                                            if ($scope.IsLock == false) {
                                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="NormalDropDown_' + attrdatablock.ID + '"  data-ng-model=\"fields.NormalDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" >{{fields.NormalDropDown_' + attrdatablock.ID + '}}</a></div></div>';
                                            } else if ($scope.IsLock == true) {
                                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                            }
                                        }
                                    }
                                } else {
                                    $scope.fields["NormalDropDown_" + attrdatablock.ID] = "-";
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                    $scope.setNormalDropdownCaption();
                                    //if (attrdatablock.options != null)
                                    //    if (attrdatablock.options.length > 0)
                                    //        $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                    if (attrdatablock.IsReadOnly == true) {
                                        var attrId = attrdatablock.ID;
                                        //if (attrdatablock.options.length > 0)
                                        //    $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                        PlanoverviewService.GetOptionDetailListByID(attrId, ID).then(function (optionlist) {
                                            if (optionlist.Response.length > 0)
                                                $scope.normaltreeSources["NormalDropDown_" + optionlist.Response[0].AttributeID] = optionlist.Response;
                                        });
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="NormalDropDown_' + attrdatablock.ID + '"  data-ng-model=\"fields.NormalDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] + '\" >{{fields.NormalDropDown_' + attrdatablock.ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                        }
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 4) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            if (attrdatablock.Caption[0] != undefined) {
                                if (attrdatablock.Caption.length > 1) {
                                    $scope.fields["NormalMultiDropDown_" + attrdatablock.ID] = attrdatablock.Caption;
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] = attrdatablock.Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    //if (attrdatablock.options != null)
                                    //    if (attrdatablock.options.length > 0)
                                    //        $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                    if (attrdatablock.IsReadOnly == true) {
                                        //if (attrdatablock.options.length > 0)
                                        //    $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                        PlanoverviewService.GetOptionDetailListByID(attrId, ID).then(function (optionlist) {
                                            if (optionlist.Response.length > 0)
                                                $scope.normaltreeSources["NormalMultiDropDown_" + optionlist.Response[0].AttributeID] = optionlist.Response;
                                        });
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="NormalMultiDropDown_' + attrdatablock.ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] + '\" >{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                        }
                                    }
                                }
                            } else {
                                $scope.fields["NormalMultiDropDown_" + attrdatablock.ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] = attrdatablock.Lable;
                                $scope.setNormalMultiDropdownCaption();
                                //if (attrdatablock.options != null)
                                //    if (attrdatablock.options.length > 0)
                                //        $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                if (attrdatablock.IsReadOnly == true) {
                                    //if (attrdatablock.options.length > 0)
                                    //    $scope.normaltreeSources["NormalDropDown_" + attrdatablock.ID] = attrdatablock.options;
                                    PlanoverviewService.GetOptionDetailListByID(attrId, ID).then(function (optionlist) {
                                        if (optionlist.Response.length > 0)
                                            $scope.normaltreeSources["NormalMultiDropDown_" + optionlist.Response[0].AttributeID] = optionlist.Response;
                                    });
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="NormalMultiDropDown_' + attrdatablock.ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + attrdatablock.ID] + '\" >{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + attrdatablock.ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 10) {
                            var inlineEditabletitile = attrdatablock.Caption;
                            perioddates = [];
                            $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                            if (attrdatablock.Value == "-") {
                                $scope.MinValue = attrdatablock.MinValue;
                                $scope.MaxValue = attrdatablock.MaxValue;
                                $scope.fields["DatePartMinDate_0"] = new Date.create();
                                $scope.fields["DatePartMaxDate_0"] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + 100000);
                                }
                                $scope.IsStartDateEmpty = true;
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                $scope.dyn_Cont += '</div>';
                            } else {
                                for (var j = 0; j < attrdatablock.Value.length; j++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = attrdatablock.Value[j].Startdate.substr(0, 10);
                                    datstartval = new Date.create(datStartUTCval);
                                    datEndUTCval = attrdatablock.Value[j].EndDate.substr(0, 10);
                                    datendval = new Date.create(datEndUTCval);
                                    perioddates.push({
                                        ID: attrdatablock.Value[j].Id,
                                        value: datendval
                                    });
                                    $scope.fields["PeriodStartDateopen_" + attrdatablock.Value[j].Id] = false;
                                    $scope.fields["PeriodEndDateopen_" + attrdatablock.Value[j].Id] = false;
                                    $scope.fields["PeriodStartDate_" + attrdatablock.Value[j].Id] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodEndDate_" + attrdatablock.Value[j].Id] = dateFormat(datendval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodStartDate_Dir_" + attrdatablock.Value[j].Id] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.fields["PeriodEndDate_Dir_" + attrdatablock.Value[j].Id] = dateFormat(datendval, $scope.GetDefaultSettings.DateFormat);
                                    if (attrdatablock.Value[j].Description == undefined) {
                                        $scope.fields["PeriodDateDesc_Dir_" + attrdatablock.Value[j].Id] = "-";
                                        $scope.fields["PeriodDateDesc_" + attrdatablock.Value[j].Id] = "";
                                    } else {
                                        $scope.fields["PeriodDateDesc_" + attrdatablock.Value[j].Id] = attrdatablock.Value[j].Description;
                                        $scope.fields["PeriodDateDesc_Dir_" + attrdatablock.Value[j].Id] = attrdatablock.Value[j].Description;
                                    }
                                    $('#fsedateid').css("visibility", "hidden");
                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + attrdatablock.Value[j].Id + '">';
                                    $scope.dyn_Cont += '<div class="inputHolder span11">';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if (attrdatablock.IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + attrdatablock.Value[j].Id + '}}';
                                        $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + attrdatablock.Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.MinValue = attrdatablock.MinValue;
                                            $scope.MaxValue = attrdatablock.MaxValue;
                                            $scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id] = new Date.create();
                                            $scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id] = new Date.create();
                                            if ($scope.MinValue < 0) {
                                                $scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id].setDate($scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id].getDate() + ($scope.MinValue + 1));
                                            } else {
                                                $scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id].setDate($scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id].getDate() + ($scope.MinValue));
                                            }
                                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                                $scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id].getDate() + ($scope.MaxValue - 1));
                                            } else {
                                                $scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id].getDate() + 100000);
                                            }
                                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id], $scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id]);
                                            $scope.fields["DatePartMinDate_" + attrdatablock.Value[j].Id] = (temp.MinDate);
                                            $scope.fields["DatePartMaxDate_" + attrdatablock.Value[j].Id] = (temp.MaxDate);
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + attrdatablock.Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-periodstartdate_id=\"PeriodStartDate_' + attrdatablock.Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + attrdatablock.Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + attrdatablock.Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + attrdatablock.Value[j].Id + '}}</a>';
                                            $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + attrdatablock.Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-periodenddate_id=\"PeriodEndDate_' + attrdatablock.Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + attrdatablock.Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + attrdatablock.Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + attrdatablock.Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + attrdatablock.Value[j].Id + '}}';
                                            $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + attrdatablock.Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5051.Caption') +" " +inlineEditabletitile + '</label>';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    if (attrdatablock.IsReadOnly == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + attrdatablock.Value[j].Id + '}}</label>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + attrdatablock.Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-periodenddate_id=\"PeriodDateDesc_' + attrdatablock.Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + attrdatablock.Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + attrdatablock.Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + attrdatablock.Value[j].Id + '}}</a>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + attrdatablock.Value[j].Id + '}}</label>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div></div></div>';
                                    if (j != 0) {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + attrdatablock.Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                        }
                                    }
                                    $scope.dyn_Cont += '</div>';
                                    if (j == (attrdatablock.Value.length - 1)) {
                                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                        $scope.dyn_Cont += '</div>';
                                    }
                                }
                            }
                            $scope.dyn_Cont += ' </div>';
                            $scope.dyn_Cont += '<div class="control-group nomargin">';
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">' + $translate.instant('LanguageContents.Res_5056.Caption') + '</label>';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[' + $translate.instant('LanguageContents.Res_5065.Caption') + ' ]</a>';
                            } else {
                                if ($scope.IsLock == false) {
                                    if (attrdatablock.Value == "-") {
                                        $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                    }
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                    $scope.dyn_Cont += '</div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<span></span>';
                                }
                            }
                            $scope.dyn_Cont += '</div>';
                        } else if (attrdatablock.TypeID == 5 && attrdatablock.ID != SystemDefiendAttributes.ApproveTime) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            var datStartUTCval = "";
                            var datstartval = "";
                            var inlineEditabletitile = attrdatablock.Caption;
                            if (attrdatablock.Value != null || attrdatablock.Value != undefined) {
                                datStartUTCval = attrdatablock.Value.substr(0, 10);
                                datstartval = new Date.create(datStartUTCval);
                                $scope.fields["DateTime_" + attrdatablock.ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                $scope.fields["DateTime_Dir_" + attrdatablock.ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                            } else {
                                $scope.fields["DateTime_" + attrdatablock.ID] = "-";
                                $scope.fields["DateTime_Dir_" + attrdatablock.ID] = "-";
                            }
                            if (attrdatablock.ID != SystemDefiendAttributes.CreationDate) {
                                if (attrdatablock.IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + attrdatablock.ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.MinValue = attrdatablock.MinValue;
                                        $scope.MaxValue = attrdatablock.MaxValue;
                                        $scope.fields["DatePartMinDate_" + attrdatablock.ID] = new Date.create();
                                        $scope.fields["DatePartMaxDate_" + attrdatablock.ID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.fields["DatePartMinDate_" + attrdatablock.ID].setDate($scope.fields["DatePartMinDate_" + attrdatablock.ID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.fields["DatePartMinDate_" + attrdatablock.ID].setDate($scope.fields["DatePartMinDate_" + attrdatablock.ID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.fields["DatePartMaxDate_" + attrdatablock.ID].setDate($scope.fields["DatePartMaxDate_" + attrdatablock.ID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.fields["DatePartMaxDate_" + attrdatablock.ID].setDate($scope.fields["DatePartMaxDate_" + attrdatablock.ID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + attrdatablock.ID], $scope.fields["DatePartMaxDate_" + attrdatablock.ID]);
                                        $scope.fields["DatePartMinDate_" + attrdatablock.ID] = (temp.MinDate);
                                        $scope.fields["DatePartMaxDate_" + attrdatablock.ID] = (temp.MaxDate);
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + attrdatablock.ID + '" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" data-datetime_id=\"DateTime_' + attrdatablock.ID + '\" data-ng-model=\"DateTime_' + attrdatablock.ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + attrdatablock.ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + attrdatablock.ID + '}}</a></div></div>';
                                        $scope.fields["DatePart_" + attrdatablock.ID] = null;
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + attrdatablock.ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + attrdatablock.ID + '}}</label></div></div>';
                            }
                        } else if (attrdatablock.TypeID == 7) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["Tree_" + attrdatablock.ID] = [];
                            $scope.treesrcdirec["Attr_" + attrdatablock.ID] = JSON.parse(attrdatablock.tree).Children;
                            GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + attrdatablock.ID], attrdatablock.ID);
                            $scope.staticTreesrcdirec["Attr_" + attrdatablock.ID] = JSON.parse(attrdatablock.tree).Children;
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0 relative\">';
                            $scope.dyn_Cont += '<label class="control-label">' + attrdatablock.Lable + ' </label>';
                            $scope.dyn_Cont += '<div class="controls">';
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + attrdatablock.ID + '" attributename=\"' + attrdatablock.Lable + '\" isreadonly="' + attrdatablock.IsReadOnly + '" ChooseFromParent="' + attrdatablock.IsChooseFromParent + '"  data-type="treeType_' + attrdatablock.ID + '" entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" attributeid="' + attrdatablock.ID + '" id=\"' + attrdatablock.ID + '\" data-ng-model=\"tree_' + attrdatablock.ID + '"\    data-original-title=\"' + attrdatablock.Lable + '\">';
                                if ($scope.treesrcdirec["Attr_" + attrdatablock.ID].length > 0) {
                                    treeTextVisbileflag = false;
                                    if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + attrdatablock.ID])) {
                                        $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = true;
                                    } else $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = false;
                                } else {
                                    $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = false;
                                }
                                $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + attrdatablock.ID + '\" tree-data=\"treesrcdirec.Attr_' + attrdatablock.ID + '\" treeplace="detail" node-attributeid="' + attrdatablock.ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + attrdatablock.ID + '\">-<span>';
                                $scope.dyn_Cont += ' </div>';
                            } else {
                                if ($scope.treesrcdirec["Attr_" + attrdatablock.ID].length > 0) {
                                    treeTextVisbileflag = false;
                                    if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + attrdatablock.ID])) {
                                        $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = true;
                                    } else $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = false;
                                } else {
                                    $scope.TreeEmptyAttributeObj["Attr_" + attrdatablock.ID] = false;
                                }
                                $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + attrdatablock.ID + '\" tree-data=\"treesrcdirec.Attr_' + attrdatablock.ID + '\"  node-attributeid="' + attrdatablock.ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + attrdatablock.ID + '\">-<span>';
                            }
                            $scope.dyn_Cont += '</div></div>';
                        } else if (attrdatablock.TypeID == 8) {
                            $scope.fields['SingleLineTextValue_' + attrdatablock.ID] = (attrdatablock.Value == null ? '' : attrdatablock.Value).toString();
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:void(0);\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id=\"SingleLineText_' + attrdatablock.ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + attrdatablock.ID + '\"  data-type="' + attrdatablock.ID + '"   data-original-title=' + attrdatablock.Lable + '>{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 19) {
                            if (attrdatablock.Caption[0] != undefined) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                                $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = "-";
                                if (attrdatablock.Value != null) {
                                    $scope['origninalamountvalue_' + attrdatablock.ID] = attrdatablock.Value.Amount;
                                    $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = $('<div />').html(((attrdatablock.Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                                    $scope.fields["NormalDropDown_" + attrdatablock.ID] = attrdatablock.Value.Currencytypeid;
                                    var currtypeid = attrdatablock.Value.Currencytypeid;
                                    $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) {
                                        return e.Id == currtypeid;
                                    }));
                                    $scope.fields["currtypename_" + attrdatablock.ID] = $scope.currtypenameobj[0]["ShortName"];
                                } else {
                                    $scope['origninalamountvalue_' + attrdatablock.ID] = 0;
                                    $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = "-";
                                    $scope.fields["NormalDropDown_" + attrdatablock.ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                                    $scope.fields["currtypename_" + attrdatablock.ID] = "-";
                                }
                                $scope.currencytypeslist = $scope.CurrencyFormatsList;
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                $scope.setNormalDropdownCaption();
                                if (attrdatablock.IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + attrdatablock.ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id=\"SingleLineText_' + attrdatablock.ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + attrdatablock.ID + '\"  data-type="' + attrdatablock.ID + '"  my-qtip2 qtip-content=\"' + attrdatablock.Lable + '\"  data-original-title=' + attrdatablock.Lable + '>{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}<span class="margin-left5x">{{fields.currtypename_' + attrdatablock.ID + '}}</span></a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + attrdatablock.ID + '}}</span></div></div>';
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 13) {
                            $scope.DropDownTreePricing["AttributeId_Levels_" + attrdatablock.ID + ""] = attrdatablock.DropDownPricing;
                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + attrdatablock.ID + ""] = true;
                            for (var j = 0, price; price = attrdatablock.DropDownPricing[j++];) {
                                if (price.selection.length > 0) {
                                    var selectiontext = "";
                                    var valueMatches = [];
                                    if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                    });
                                    if (valueMatches.length > 0) {
                                        selectiontext = "";
                                        for (var x = 0, val; val = valueMatches[x++];) {
                                            selectiontext += val.caption;
                                            if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                            else selectiontext += "</br>";
                                        }
                                    } else selectiontext = "-";
                                    $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + j] = selectiontext;
                                } else {
                                    $scope.treeTexts["multiselectdropdown_text_" + attrdatablock.ID + "_" + j] = "-";
                                }
                                if (attrdatablock.IsReadOnly == false && $scope.IsLock == false) {
                                    $scope.EnableDisableControlsHolder["Selection_" + attrdatablock.ID] = false;
                                    $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + attrdatablock.TypeID + '" InheritFromParent="' + attrdatablock.IsInheritFromParent + '" ChooseFromParent="' + attrdatablock.IsChooseFromParent + '" attributeid="' + attrdatablock.ID + '" id="fields.MultiSelectDropDown_' + attrdatablock.ID + "_" + j + '" editabletypeid="percentagetype' + attrdatablock.ID + '_' + j + '" data-type="percentagetype' + attrdatablock.ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + attrdatablock.ID + '_' + j + ')"></span></a></div></div>';
                                } else {
                                    $scope.EnableDisableControlsHolder["Selection_" + attrdatablock.ID] = true;
                                    $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + attrdatablock.ID + '_' + j + ')"></span></span></div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 17) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            if (attrdatablock.Caption[0] != undefined) {
                                if (attrdatablock.Caption.length > 0) {
                                    $scope.fields["TagWordsCaption_" + attrdatablock.ID] = attrdatablock.Caption;
                                    $scope.fields["TagWordsSeleted_" + attrdatablock.ID] = attrdatablock.Value;
                                    $scope.setFieldKeys();
                                    $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                    $scope.setNormalMultiDropdownCaption();
                                    if (attrdatablock.IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="TagWordsCaption_' + attrdatablock.ID + '"  data-ng-model=\"fields.TagWordsCaption_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] + '\" >{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</label></div></div>';
                                        }
                                    }
                                }
                            } else {
                                $scope.fields["TagWordsCaption_" + attrdatablock.ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] = attrdatablock.Lable;
                                $scope.setNormalMultiDropdownCaption();
                                $scope.fields["TagWordsSeleted_" + attrdatablock.ID] = [];
                                if (attrdatablock.IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + attrdatablock.TypeID + '" entityid="' + ID + '" attributeid="' + attrdatablock.ID + '" id="TagWordsCaption_' + attrdatablock.ID + '"  data-ng-model=\"fields.TagWordsCaption_' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + attrdatablock.ID] + '\" >{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\" for=\"label\">' + attrdatablock.Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + attrdatablock.ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else if (attrdatablock.TypeID == 16) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            var inlineEditabletitile = attrdatablock.Caption;
                            var dateExpire;
                            if (attrdatablock.Value != null || attrdatablock.Value != undefined) {
                                var dateExpireUTCval = "";
                                var dateExpireval = "";
                                dateExpireUTCval = attrdatablock.Value.substr(0, 10);
                                dateExpireval = new Date.create(dateExpireUTCval);
                                dateExpire = dateFormat((dateExpireUTCval), $scope.GetDefaultSettings.DateFormat);
                                $scope.fields["DateTime_" + attrdatablock.ID] = dateExpire;
                                $scope.fields["DateTime_Dir_" + attrdatablock.ID] = dateExpire;
                            } else {
                                dateExpire = "-"
                                $scope.fields["DateTime_" + attrdatablock.ID] = "-";
                                $scope.fields["DateTime_Dir_" + attrdatablock.ID] = "-";
                            }
                            if (attrdatablock.IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><span class="editable">' + dateExpire + '</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + attrdatablock.ID + '\" my-qtip2 qtip-content="' + attrdatablock.Lable + '" data-expiredate="' + dateExpire + '" data-attrlable="' + attrdatablock.Lable + '" ng-click="openExpireAction($event)">' + dateExpire + '</a></span></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><span class="editable"> ' + dateExpire + '</span></div></div>';
                                }
                            }
                        } else if (attrdatablock.TypeID == 16) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            var inlineEditabletitile = attrdatablock.Caption;
                            var dateExpire;
                            if (attrdatablock.Value != null || attrdatablock.Value != undefined) {
                                var dateExpireUTCval = "";
                                var dateExpireval = "";
                                dateExpireUTCval = attrdatablock.Value.substr(0, 10);
                                dateExpireval = new Date.create(dateExpireUTCval);
                                dateExpire = dateFormat(ConvertDateFromStringToString(ConvertDateToString(dateExpireval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.format);
                                $scope.fields["DateTime_" + attrdatablock.ID] = dateExpire;
                                $scope.fields["DateTime_Dir_" + attrdatablock.ID] = dateExpire;
                            }
                        } else if (attrdatablock.TypeID == 18) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = "-";
                            if (attrdatablock.Caption != undefined) {
                                $scope.fields["SingleLineTextValue_" + attrdatablock.ID] = $('<div />').html(attrdatablock.Caption).text();
                            }
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + attrdatablock.ID + '_0\',' + attrdatablock.TypeID + ') class=\"control-group AttrID_' + attrdatablock.ID + '_0\"><label class=\"control-label\"\>' + attrdatablock.Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + attrdatablock.ID + '}}</label></div></div>';
                        }
                        else if (attrdatablock.TypeID == 9) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrdatablock.ID] = true;
                            $scope.fields["CheckBoxSelection_" + attrdatablock.ID] = attrdatablock.Value;
                            $scope.dyn_Cont += "<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview('" + attrdatablock.ID + "_0'," + attrdatablock.TypeID + ")  class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + attrdatablock.AttributeID + "\">" + attrdatablock.Caption + " : </label><div class=\"controls\"><input ng-click=\"saveDropdownTree(" + attrdatablock.ID + "," + attrdatablock.TypeID + "," + parseInt($stateParams.ID) + "," + $scope.fields["CheckBoxSelection_" + attrdatablock.ID] + "  )\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + attrdatablock.ID + "\" id=\"CheckBoxSelection_" + attrdatablock.ID + "\" placeholder=\"" + attrdatablock.Caption + "\"></div></div>";
                        }
                    }
                    $("#dynamicdetail").empty();
                    $("#dynamicdetail").append($scope.dyn_Cont);
                    $compile($("#dynamicdetail").contents())($scope);
                    Timeoutvar.getattr = $timeout(function () { getentityattributesdetails }, 100);
                }
            });
        }

        $scope.deletePeriodDate = function (periodid) {
            PlanoverviewService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    UpdateEntityPeriodTree($stateParams.ID);
                    $("#dynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');
                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }
        $scope.openExpireAction = function (event) {
            var attrevent = event.currentTarget;
            $scope.SourceAttributeID = attrevent.dataset.attributeid;
            $scope.SourceAttributename = attrevent.dataset.attrlable;
            $scope.dateExpireval = attrevent.dataset.expiredate;
            var sourcefrom = 2;
            Timeoutvar.emit = $timeout(function () {
                $scope.$emit('CallExpireaction', $scope.dateExpireval, parseInt($stateParams.ID), parseInt($stateParams.ID), sourcefrom, $scope.EntityTypeID, parseInt($stateParams.ID), $scope.SourceAttributeID, $scope.SourceAttributename);
            }, 100);
        }
        $scope.$on('updateExpireactionEntityEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            $scope.fields["DateTime_" + AttributeID] = Attributevalue;
            $scope.LoadEntityRelatedData();
            $scope.LoadEntityRelatedDataSet1();
            Timeoutvar.timeforlatestfeeed = $timeout(function () {
                $scope.TimerForLatestFeed();
            }, 1000);
        });
        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        for (var i = 0; i < optionarray.length; i++) {
                            if (optionarray[i] != 0) {
                                var level = i + 1;
                                UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                            }
                        }
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        getCurrentEntityStatus(updateentityattrib.EntityID);
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 1 || attributetypeid == 2) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                if (attrID == 68) {
                    $(window).trigger("UpdateEntityName", optionarray);
                    $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $stateParams.ID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
                                if ($stateParams.ID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $stateParams.ID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
                                    if (Splitarr.length == 0) {
                                        Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 19) {
                $scope.newamount = "";
                $scope.newcurrencytypename = "";
                $scope.newamount = optionarray[0];
                $scope.newcurrencytypename = optionarray[2];
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    UpdateTreeScope(attrID, (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ') + ' ' + optionarray[2]);
                    $scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
                    $scope.fields["NormalDropDown_" + attrID] = optionarray[1];
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 12) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                            //$scope.fields['SingleLineText_' + attrID] = optionarray == "" ? 0 : optionarray;
                            $scope.numberValue.value = optionarray == "" ? 0 : optionarray;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            UpdateTreeScope(attrID, $scope.numberValue.value);
                        }
                    });
                    $scope.treeSelection = [];
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            } else if (attributetypeid == 5) {
                var sdate = "";
                if ($scope.fields['DateTime_Dir_' + attrID].toString() != "" && $scope.fields['DateTime_Dir_' + attrID] != null && $scope.fields['DateTime_Dir_' + attrID].toString() != "-") {
                    sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    if (sdate == 'NaN-NaN-NaN') {
                        sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                    }
                }
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        if (sdate == "") $scope.fields["DateTime_" + attrID] = "-";
                        else {
                            $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.format);
                        }
                        getCurrentEntityStatus(updateentityattrib.EntityID);
                    }
                });
                $scope.treeSelection = [];
                // ------------------- UPDATE THE STATUS IF GIVEN ENDDATE IS CURRENT SYSTEM DATE AND IF RESPECTIVE SETTINGS IS PRESENT IS ADMIN SETTINGS-----//

                //---------------------- GET THE ENDDATE----------------------------------//              
                $scope.newGetEndatesVal = [];
                for (var i = 0; i < $scope.tempresultHolder_Set2[3].length; i++) {
                    if ($scope.tempresultHolder_Set2[3][i].TypeID == 5 && $scope.tempresultHolder_Set2[3][i].Lable == "Due date" || $scope.tempresultHolder_Set2[3][i].Lable == "EndDate")
                        $scope.newGetEndatesVal.push({ "TypeID": $scope.tempresultHolder_Set2[3][i].TypeID, "Caption": $scope.tempresultHolder_Set2[3][i].Caption, "Value": $scope.tempresultHolder_Set2[3][i].Value });

                }

                //------------- SERVICE CALL TO GET THE SAVE OVERALL STATUS ------------//
                //PlanningService.GetEntityTemplateDetails().then(function (res) {
                //    if (res.Response != undefined || res.Response != null) {
                //        $scope.EResults = res.Response
                //        $scope.GetFullResults = $.grep($scope.EResults, function (n, i) {
                //            return ($scope.EResults[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId)

                //        });
                //        var currentdate = new Date();
                //        for (var i = 0; i < res.Response.length; i++) {
                //            if (res.Response[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId) {
                //                if ($scope.newGetEndatesVal.length != 0) {
                //                    for (var j = 0; j < $scope.newGetEndatesVal.length; j++) {
                //                        var datEndUTCRes = "";
                //                        var datendtval = "";
                //                        var datPeriodEndUTCRes = "";
                //                        var datPeriodendtval = "";
                //                        datEndUTCRes = $scope.newGetEndatesVal[0].Value;
                //                        if (ConvertDateToString(currentdate) >= sdate && attrID == 56 || attrID == 63) {
                //                            $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                //                        }
                //                        else if (datEndUTCRes != '-' && $scope.newGetEndatesVal[0].Value != null) {
                //                            datEndUTCRes = $scope.newGetEndatesVal[0].Value.substr(0, 10);
                //                            var currentdate = new Date();

                //                            if (ConvertDateToString(currentdate) >= sdate && attrID == 56 || attrID == 63) {
                //                                $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                //                            } else {
                //                                $scope.funloadEntityStatus_Edit($scope.tempresultHolder[1], $scope.tempresultHolder[2]);
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //});
            }
            else if (attributetypeid == 9) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = ($('#CheckBoxSelection_' + attrID)[0].checked) ? [1] : [0];
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields['CheckBoxSelection_' + attrID] = $('#CheckBoxSelection_' + attrID)[0].checked;
                        getCurrentEntityStatus(updateentityattrib.EntityID);
                    }
                });
            }
            $scope.treeSelection = [];
            Timeoutvar.latestfeedtimer = $timeout(function () {
                $scope.TimerForLatestFeed();
            }, 5000);
        };
        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }
                var tempperioddates = [];
                var tempperioddates = [];
                if (perioddates.length == 2) {
                    for (var i = 0; i < perioddates.length; i++) {
                        tempperioddates.push(perioddates[i].value)
                    }
                }
                else {
                    perioddates = [];
                    perioddates.push({ value: edate }, { value: sdate });
                    for (var i = 0; i < perioddates.length; i++) {
                        tempperioddates.push(perioddates[i].value)
                    }
                }
                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (maxPeriodDate.getTime() - sdate.getTime());
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }
                newperiod.EntityID = entityid;
                newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;
                PlanoverviewService.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else var newid = resultperiod.Response;
                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }
                    perioddates.push({
                        ID: newid,
                        value: edate
                    });
                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate))
                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString()), $scope.format);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString()), $scope.format);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    } else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("visibility", "hidden");
                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';
                    $scope.dyn_Cont += '<div class="inputHolder span11">';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5056.Caption') + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5057.Caption') + '</label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';
                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }
                    var divcompile = $compile($scope.dyn_Cont)($scope);
                    $("#dynamicdetail div[data-addperiodid]").append(divcompile);
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    PlanoverviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                        if (getGanttperiod.StatusCode == 200) {
                            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                    if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                        return false;
                                    } else {
                                        if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                            $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod.Response;
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    });
                });
                $scope.newGetPeriodEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                    return (
                    $scope.tempresultHolder_Set2[3][i].TypeID == 10);
                });

                var tempenddates = [];
                for (var j = 0; j < $scope.ListViewDetails.length; j++) {
                    if ($scope.ListViewDetails[j].data.Response.Data != null) {
                        for (var i = 0 ; i < $scope.ListViewDetails[j].data.Response.Data.length; i++) {
                            $($.parseXML($scope.ListViewDetails[j].data.Response.Data[i]['Period'])).find('p').each(function () {
                                tempenddates.push(new Date.create($(this).attr('e')));
                            });
                        }
                    }
                }
            } else {
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {
                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[" + $translate.instant('LanguageContents.Res_5065.Caption') + "]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });
                var temparryEndate = [];
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), $scope.format);
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), $scope.format))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), $scope.format);
                if (edate == 'NaN-NaN-NaN') {
                    edate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), $scope.format))
                }
                var diffval = ((parseInt((edate).getTime()) - parseInt(((sdate).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }
                $scope.fields["PeriodStartDate_Dir_" + periodid] = dateFormat(new Date.create(sdate.toString()), $scope.format);
                $scope.fields["PeriodEndDate_Dir_" + periodid] = dateFormat(new Date.create(edate.toString()), $scope.format);
                var maxPeriodEndDate = edate;
                var Convertsdate = sdate;
                var diffvalend = ((maxPeriodEndDate.getTime() - Convertsdate.getTime()));
                if (parseInt(diffvalend) < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }
                var diffvalstart = (parseInt(Convertsdate.getTime() - Convertsdate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));
                    return false;
                }
                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = dateFormat(sdate, $scope.format);
                updateperiod.EndDate = dateFormat(edate, $scope.format);
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                PlanoverviewService.PostEntityPeriod1(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        $scope.fields["PeriodStartDate_" + periodid] = dateFormat(sdate, $scope.format);
                        $scope.fields["PeriodEndDate_" + periodid] = dateFormat(edate, $scope.format);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        PlanoverviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                            if (resultperiod.StatusCode == 200) {
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                            return false;
                                        } else {
                                            if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod.Response;
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        getCurrentEntityStatus(updateperiod.EntityID);
                    }
                });

                //-------------------- UPDATE THE STATUS IF GIVEN PERIOD ENDDATE IS CURRENT SYSTEM DATE AND IF RESPECTIVE SETTINGS IS PRESENT IS ADMIN SETTINGS-----//
                //----------------------GET PERIOD DATE-----------------------------------//
                $scope.newGetPeriodEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                    return (
                    $scope.tempresultHolder_Set2[3][i].TypeID == 10);
                });
                var tempenddates = [];
                for (var j = 0; j < $scope.ListViewDetails.length; j++) {
                    if ($scope.ListViewDetails[j].data.Response.Data != null) {
                        for (var i = 0 ; i < $scope.ListViewDetails[j].data.Response.Data.length; i++) {
                            $($.parseXML($scope.ListViewDetails[j].data.Response.Data[i]['Period'])).find('p').each(function () {

                                tempenddates.push(new Date.create($(this).attr('e')));
                            });
                        }
                    }
                }
                $scope.newGetPeriodEndatesVal = $.grep($scope.tempresultHolder_Set2[3], function (n, i) {
                    return (
                    $scope.tempresultHolder_Set2[3][i].TypeID == 10);
                });

                //------------- SERVICE CALL TO GET THE SAVE OVERALL STATUS ------------//
                //PlanningService.GetEntityTemplateDetails().then(function (res) {
                //    if (res.Response != undefined || res.Response != null) {
                //        $scope.pResults = res.Response
                //        $scope.GetFullResults = $.grep($scope.pResults, function (n, i) {
                //            return ($scope.pResults[i].EntityTypeId == $scope.tempresultHolder_Set2[2].EntityTypeId)

                //        });
                //        var currentdate = new Date();
                //        $scope.MaxPeriodEnddate = [];
                //        var datPeriodsUTCval = "";
                //        var datPeriodsUTCvals = "";
                //        var datPeriodsval = [];
                //        var datPeriodEndUTCRes = "";
                //        var datPeriodendtval = "";

                //        if ($scope.newGetPeriodEndatesVal[0].Value[0] != null) {
                //            for (var i = 0; i < $scope.newGetPeriodEndatesVal[0].Value.length; i++) {
                //                datPeriodsUTCval = ConvertDateToString(edate);
                //                for (var j = 0; j < tempenddates.length; j++) {
                //                    datPeriodsUTCvals = ConvertDateToString(tempenddates[j]);
                //                }
                //                datPeriodsval.push(new Date(datPeriodsUTCvals), new Date(datPeriodsUTCval));
                //            }
                //            var maxDate = ConvertDateToString(new Date(Math.max.apply(null, datPeriodsval)));
                //            datPeriodEndUTCRes = maxDate;
                //            if (datPeriodEndUTCRes != '-') {
                //                datPeriodendtval = datPeriodEndUTCRes;
                //                var currentdate = new Date();
                //                if (ConvertDateToString(currentdate) >= maxDate) {
                //                    $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                //                }
                //                else if (ConvertDateToString(currentdate) >= datPeriodEndUTCRes) {
                //                    $scope.OverAllEntityStatus = $scope.GetFullResults[0].OverallStatus;
                //                }
                //                else {
                //                    $scope.funloadEntityStatus_Edit($scope.tempresultHolder[1], $scope.tempresultHolder[2]);
                //                }
                //            }
                //        }
                //    }
                //});

            }
            $scope.TimerForLatestFeed();
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        PlanoverviewService.GetAttributeTreeNodeByEntityID(attrID, parseInt($stateParams.ID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            Timeoutvar.latestfeedtimeout = $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }
        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                PlanoverviewService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var GetTreeRes;
                        if (choosefromParent) {
                            PlanoverviewService.GetDropDownTreePricingObjectFromParentDetail(attrID, choosefromParent, choosefromParent, parseInt($stateParams.ID, 10), 0).then(function (GetTree) {
                                if (GetTree.Response != null) {
                                    var result = GetTree.Response;
                                    for (var p = 0, price; price = result[p++];) {
                                        var attributeLevelOptions = [];
                                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                            return e.level == p;
                                        }));
                                        if (attributeLevelOptions[0] != undefined) {
                                            attributeLevelOptions[0].selection = price.selection;
                                            attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            if (price.selection.length > 0) {
                                                var selectiontext = "";
                                                var valueMatches = [];
                                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                                if (valueMatches.length > 0) {
                                                    selectiontext = "";
                                                    for (var x = 0, val; val = valueMatches[x++];) {
                                                        selectiontext += val.caption;
                                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                        else selectiontext += "</br>";
                                                    }
                                                } else selectiontext = "-";
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            PlanoverviewService.GetDropDownTreePricingObject(attrID, choosefromParent, choosefromParent, parseInt($stateParams.ID, 10), 0).then(function (GetTree) {
                                if (GetTree.Response != null) {
                                    var result = GetTree.Response;
                                    for (var p = 0, price; price = result[p++];) {
                                        var attributeLevelOptions = [];
                                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                            return e.level == p;
                                        }));
                                        if (attributeLevelOptions[0] != undefined) {
                                            attributeLevelOptions[0].selection = price.selection;
                                            attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            if (price.selection.length > 0) {
                                                var selectiontext = "";
                                                var valueMatches = [];
                                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                                if (valueMatches.length > 0) {
                                                    selectiontext = "";
                                                    for (var x = 0, val; val = valueMatches[x++];) {
                                                        selectiontext += val.caption;
                                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                        else selectiontext += "</br>";
                                                    }
                                                } else selectiontext = "-";
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }

        function ReturnSelectedTreeNodes(attrID) {
            var selection = [];
            for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {
                if (price.selection.length > 0) {
                    var matches = [];
                    matches = jQuery.grep(price.LevelOptions, function (relation) {
                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                    });
                    if (matches.length > 0) for (var z = 0, node; node = matches[z++];) {
                        selection.push({
                            "NodeId": node.NodeId,
                            "Level": price.level,
                            "value": node.value != "" ? node.value : "-1"
                        })
                    }
                }
            }
            return selection;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }
        $scope.UploadAttributeId = 0;
        $scope.UploadImagefile = function (attributeId, place) {
            $scope.UploadAttributeId = attributeId;
            $scope.place = place;
            $("#pickfiles100").click();
        }
        Timeoutvar.uploader = $timeout(function () {
            StrartUpload_UploaderAttr();
        }, 100);

        function StrartUpload_UploaderAttr() {
            $('.moxie-shim').remove();
            var uploader_Attr = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttr',
                container: 'filescontaineroo',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}
            });
            uploader_Attr.bind('Init', function (up, params) { });
            uploader_Attr.init();
            uploader_Attr.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_Attr.start();
            });
            uploader_Attr.bind('UploadProgress', function (up, file) { });
            uploader_Attr.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader_Attr.bind('FileUploaded', function (up, file, response) {
                SaveFileDetails(file, response.response);
            });
            uploader_Attr.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, {
                    id: file.id,
                    size: file.size
                });
            });
        }
        //$scope.StrartUpload = function () {
        //    $('.moxie-shim').remove();
        //    var uploader = new plupload.Uploader({
        //        runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
        //        browse_button: 'pickfilesAlternate',
        //        drop_element: 'dragfilesoveruploader',
        //        container: 'filescontainer',
        //        max_file_size: '10000mb',
        //        url: 'Handlers/UploadHandler.ashx?Type=Attachment',
        //        flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
        //        silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
        //        chunk_size: '64Kb',
        //        multipart_params: {}
        //    });
        //    uploader.bind('Init', function (up, params) {
        //        uploader.splice();
        //    });
        //    $('#uploadfiles').click(function (e) {
        //        uploader.start();
        //        e.preventDefault();
        //    });
        //    $('#clearUploader').click(function (e) {
        //        uploader.destroy();
        //        $('.moxie-shim').remove();
        //    });
        //    uploader.init();
        //    $('#pickfiles').each(function () {
        //        var input = new mOxie.FileInput({
        //            browse_button: this,
        //            multiple: true
        //        });
        //        input.onchange = function (event) {
        //            uploader.addFile(input.files);
        //        };
        //        input.init();
        //    });
        //    uploader.bind('FilesAdded', function (up, files) {
        //        $.each(files, function (i, file) {
        //            var ste = file.name.split('.')[file.name.split('.').length - 1];
        //            $('#filelistoveruploader').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="info">' + '<span class="name" >' + '<i class="icon-file-alt"></i>' + file.name + '</span>' + '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' + '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + '</span>' + '</div>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '</div>' + '</div>' + '<div class="row-fluid">' + '<div class="span12">' + '<form class="form-inline">' + '<select>' + '<option>' + Mimer(ste) + '</option>' + '<option>Image</option>' + '<option>Video</option>' + '<option>Document</option>' + '<option>Zip</option>' + '</select>' + '<input type="text" placeholder="Description">' + '</form>' + '</div>' + '</div>' + '</div>')($scope));
        //        });
        //        $('#dragfilesoveruploader').hide();
        //        up.refresh();
        //        TotalUploadProgress();
        //    });
        //    $('#filelistoveruploader').on('click', '.removefile', function () {
        //        var fileToRemove = $(this).attr('data-fileid');
        //        $.each(uploader.files, function (i, file) {
        //            if (file.id == fileToRemove) {
        //                uploader.removeFile(file);
        //                $('#' + file.id).remove();
        //            }
        //        });
        //        if ($('#filelistoveruploader .attachmentBox').length == 0) {
        //            $('#dragfiles').show();
        //        }
        //        TotalUploadProgress();
        //    });
        //    uploader.bind('UploadProgress', function (up, file) {
        //        $('#' + file.id + " .bar").css("width", file.percent + "%");
        //        $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
        //        $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
        //        TotalUploadProgress();
        //    });
        //    uploader.bind('Error', function (up, err) {
        //        $('#filelistoveruploader').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
        //        up.refresh();
        //    });
        //    uploader.bind('FileUploaded', function (up, file, response) {
        //        $('#' + file.id).attr('data-status', 'true');
        //        SaveFileDetails(file, response.response);
        //        TotalUploadProgress();
        //    });
        //    uploader.bind('BeforeUpload', function (up, file) {
        //        $.extend(up.settings.multipart_params, {
        //            id: file.id,
        //            size: file.size
        //        });
        //    });
        //    uploader.bind('FileUploaded', function (up, file, response) {
        //        var obj = response;
        //        $('#' + file.id).attr('data-fileId', response.response);
        //    });

        //    function TotalUploadProgress() {
        //        var TotalSize = 0;
        //        var TotalCount = $('#filelistoveruploader .attachmentBox').length;
        //        var UploadedSize = 0;
        //        var UploadedCount = 0;
        //        $('#filelistoveruploader .attachmentBox').each(function () {
        //            TotalSize += parseInt($(this).attr('data-size'));
        //            UploadedSize += parseInt($(this).attr('data-size-uploaded'));
        //            if ($(this).attr('data-status') == 'true') {
        //                UploadedCount += 1;
        //            }
        //        });
        //        $('#overViewUploaderTotalProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded');
        //        $('#overViewUploaderTotalProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
        //        $('#overViewUploaderTotalProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
        //    }
        //}
        $scope.Clear = function () { }

        function SaveFileDetails(file, response) {

            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.create('today');
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.EntityID = parseInt($stateParams.ID, 10);
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            PlanoverviewService.copyuploadedImage(uplaodImageObject.FileName).then(function (ImgRes) {
                if (ImgRes.Response != null) {
                    uplaodImageObject.FileName = ImgRes.Response;
                    PlanoverviewService.UpdateImageName(uplaodImageObject).then(function (uplaodImageObjectResult) {
                        $('#overviewUplaodImagediv').modal('hide');
                        if (uplaodImageObjectResult.Response != 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                                $('#' + PreviewID).attr('src', cloudpath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }
                            else {
                                $('#' + PreviewID).attr('src', TenantFilePath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }
                        }
                    });
                }
            })

        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToHide = [];
            if (attrLevel > 0) {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide[0].length; i++) {
                    var attrRelIDs = attributesToHide[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, attrVal) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToHide = [];
                var hideAttributeOtherThanSelected = [];
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    optionValue = attrVal;
                    attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    optionValue = attrVal;
                    attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToHide = [];
                        attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToHide[0] != undefined) {
                    for (var i = 0; i < attributesToHide.length; i++) {
                        var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    if ($scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] == false) $scope.fields['SingleLineTextValue_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "-";
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.LoadMilestone_Temp = function (MilestoneMetaData) {
            $scope.MilestoneIsLock = true;
            if ($scope.IsLock == true) {
                $scope.MilestoneIsLock = false;
            }
            var milestonesData = MilestoneMetaData;
            $scope.MilestoneCollection = milestonesData;
            $scope.LoadMilestoneData(milestonesData);
        };
        $scope.MilestoneAttributeHeaderCaption = [];
        $scope.MilestoneAttributeValues = [];
        $scope.MilestoneCollection = {};
        $scope.LoadMilestoneData = function (milestonesData) {
            $scope.milestones = [];
            for (var i = 0; i < milestonesData.length; i++) {
                var milestone = {};
                var milestoneValue = [];
                milestone["MileStoneId"] = milestonesData[i].EntityId;
                for (var j = 0; j < milestonesData[i].AttributeData.length; j++) {
                    if (milestonesData[i].AttributeData[j].ID == SystemDefiendAttributes.MilestoneStatus) {
                        if (i == 0) {
                            $scope.MilestoneAttributeHeaderCaption.push({
                                Caption: milestonesData[i].AttributeData[j].Caption
                            });
                        }
                        if (milestonesData[i].AttributeData[j].Value == true) {
                            $scope.MilestoneAttributeValues.push({
                                Value: "Reached"
                            });
                            milestone[milestonesData[i].AttributeData[j].Caption.replace(/\s/g, '')] = "Reached";
                            milestoneValue.push("Reached");
                        } else {
                            $scope.MilestoneAttributeValues.push({
                                Value: "Not Reached"
                            });
                            milestone[milestonesData[i].AttributeData[j].Caption] = "Not Reached";
                        }
                    } else {
                        if (milestonesData[i].AttributeData[j].ID != SystemDefiendAttributes.MilestoneEntityID) {
                            if (i == 0) {
                                $scope.MilestoneAttributeHeaderCaption.push({
                                    Caption: milestonesData[i].AttributeData[j].Caption
                                });
                            }
                            $scope.MilestoneAttributeValues.push(milestonesData[i].AttributeData[j]);
                            if (milestonesData[i].AttributeData[j].TypeID == 5) {
                                milestone[milestonesData[i].AttributeData[j].Caption] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(milestonesData[i].AttributeData[j].Value)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                                var datStartUTCval = milestonesData[i].AttributeData[j].Value.substr(0, 10);
                                var datstartval = new Date.create(datStartUTCval);
                                milestone[milestonesData[i].AttributeData[j].Caption] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                            } else milestone[milestonesData[i].AttributeData[j].Caption.replace(/\s/g, '')] = milestonesData[i].AttributeData[j].Value;
                        }
                    }
                }
                $scope.milestones.push(milestone);
            }
            if ($scope.milestones[0] != undefined) {
                if ($scope.milestones[0]["Due date"] != undefined) {
                    sortOnMileStoneByDate($scope.milestones, "Due date");
                }
                if ($scope.milestones[0]["Due Date"] != undefined) {
                    sortOnMileStoneByDate($scope.milestones, "Due Date");
                }
            }
            if ($scope.IsLock == true) {
                Timeoutvar.milestonelock = $timeout(function () { $("#MileStoneHolderLock").css("display", "block") }, 100);
                Timeoutvar.milestoneunlock = $timeout(function () { $("#MileStoneHolderUnLock").css("display", "none") }, 100);
            } else {
                Timeoutvar.milestonelock = $timeout(function () { $("#MileStoneHolderLock").css("display", "none") }, 100);
                Timeoutvar.milestoneunlock = $timeout(function () { $("#MileStoneHolderUnLock").css("display", "block") }, 100);
            }
        }
        $scope.duedateFormatforMilestone = function (dateval) {
            if (dateval != undefined) {
                return dateFormat(dateval, $scope.format)
            }
        }

        function sortOnMileStoneByDate(collection, name) {
            collection.sort(function (a, b) {
                if (ConvertStringToDateObj(a[name]) <= ConvertStringToDateObj(b[name])) {
                    return (-1);
                }
                return (1);
            });
        }
        try {
            $scope.MilestoneName = '';
            $scope.milestoneDueDate = null;
            $scope.Description = '';
            $scope.MilestoneID = 0;
            $scope.MilestonePrevStatus = '';
            $scope.editmilestone = function (e) {
                $('#addMilestoneModel').modal('show');
                $('#MileStoneHolder').each(function () { });
            };
            $scope.OptionObj = {};
            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];
                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });
            }
            $scope.fields = [];
            $scope.milestonefields = []
            $scope.dyn_Cont = '';
            $scope.EnableDisableControlsHolder = {};
            $scope.atributesRelationList = {};
            $scope.ClearScopeModle = function () {
                for (var variable in $scope.fields) {
                    if (typeof $scope.fields[variable] === "string") {
                        if (variable !== "ListSingleSelection_69") {
                            $scope.fields[variable] = "";
                        }
                    } else if (typeof $scope.fields[variable] === "number") {
                        $scope.fields[variable] = null;
                    } else if (Array.isArray($scope.fields[variable])) {
                        $scope.fields[variable] = [];
                    } else if (typeof $scope.fields[variable] === "object") {
                        $scope.fields[variable] = {};
                    }
                }
                $scope.costcemtreObject = [];
                $scope.MemberLists = [];
            }
            $scope.tree = {};
            $scope.AddMilestone = function () {
                $("#AddMilestoneModel").modal('show');
                $("#EnityMilestone").addClass('notvalidate');
                ClearMilestoneFields();
                $scope.EnableAdd = true;
                $scope.EnableUpdate = false;

                $timeout(function () {
                    for (var i = 0; i < milestoneuploaderAttrIDs.length; i++)
                        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                            $('#milestoneUploader_' + milestoneuploaderAttrIDs[i]).attr('src', cloudpath + 'UploadedImages/' + 'NoThumpnail.jpg');
                        }
                        else {
                            $('#milestoneUploader_' + milestoneuploaderAttrIDs[i]).attr('src', TenantFilePath + 'UploadedImages/' + 'NoThumpnail.jpg');
                        }
                }, 100);
            }
            $scope.AddMilestone_Temp = function (EntityTypeAttributeRelation, ValidationAttribute) {
                $scope.EnableAdd = true;
                $scope.EnableUpdate = false;
                setTimeout(function () {
                    $('[id^=TextSingleLine]:enabled:visible:first').focus().select()
                }, 1000);
                $scope.atributesRelationList = EntityTypeAttributeRelation;
                $scope.dyn_Cont = '';
                milestoneuploaderAttrIDs = [];
               for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 3 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.MilestoneEntityID) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeCaption.trim() == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"> <input type=\"text\" ng-model=\"milestonefields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.milestonefields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerName;
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " : </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"milestonefields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            if ($scope.atributesRelationList[i].InheritFromParent) $scope.milestonefields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                            else $scope.milestonefields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeCaption.trim() == SystemDefiendAttributes.Name) {
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"milestonefields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        } else {
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"milestonefields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                        }
                        $scope.milestonefields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.dyn_Cont += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"milestonefields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"milestonefields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"milestonefields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.milestonefields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = '';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " : </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"milestonefields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\">  </select></div></div>";
                        $scope.milestonefields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input   type=\"text\" ng-change=\"setTimeout(changeduedate_changed(milestonefields.DatePart_" + $scope.atributesRelationList[i].AttributeID + ",milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),1000)\" ng-model=\"milestonefields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event," + $scope.atributesRelationList[i].AttributeID + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"calanderopened\" min-date=\"milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var MyDate = new Date.create();
                        MyDate = new Date.create(Date.create(MyDate.getFullYear(), MyDate.getMonth(), (MyDate.getDate())));
                        $scope.milestonefields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = MyDate;
                        $scope.milestonefields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        PlanoverviewService.GetTreeNode($scope.atributesRelationList[i].AttributeID).then(function (GetTree) {
                            $scope.tree = JSON.parse(GetTree.Response);
                        });
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if (j == 0) $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> <select ui-select2  ng-model=\"milestonefields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-options=\"ndata as ndata.Caption for ndata in tree.Children\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].Levels[j].LevelName + " </option> </select></div></div>";
                            else $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> <select ui-select2 ng-model=\"milestonefields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-options=\"ndata as ndata.Caption for ndata in milestonefields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + j + ".Children\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].Levels[j].LevelName + " </option> </select></div></div>";
                            $scope.milestonefields["DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = "";
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input  type=\"text\" ng-model=\"milestonefields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.milestonefields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = '';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.MilestoneStatus) {
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input  type=\"checkbox\" ng-model=\"milestonefields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.milestonefields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = '';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.items.push({
                            startDate: '',
                            endDate: '',
                            comment: '',
                            sortorder: 0
                        });
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.milestonefields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.milestonefields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"milestonefields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span11\">";
                        $scope.dyn_Cont += "<input class=\"sdate\" id=\"item.startDate\"type=\"text\" required name=\"startDate\"  ng-change=\"changeperioddate_changed(item.startDate,milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event," + $scope.milestonefields["DatePart_Calander_Open" + "item.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"milestonefields.DatePart_Calander_Open" + "item.startDate" + "\" min-date=\"milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\" /><input class=\"edate\" type=\"text\" required name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.milestonefields["DatePart_Calander_Open" + "item.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"milestonefields.DatePart_Calander_Open" + "item.endDate" + "\" min-date=\"milestonefields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"milestonefields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\" /><input type=\"text\" class=\"dateComment\" required name=\"comment\"   ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.milestonefields["Period_" + $scope.atributesRelationList[i].AttributeID] = '';
                        $scope.milestonefields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.milestonefields["DatePart_Calander_Open" + "item.endDate"] = false;
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        milestoneuploaderAttrIDs.push($scope.atributesRelationList[i].AttributeID);
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div class="control-group ng-scope"><label class="control-label"';
                        $scope.dyn_Cont += 'for="milestonefields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '>' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        //$scope.dyn_Cont += '<img class="entityImgPreview ng-pristine ng-valid"';
                        //$scope.dyn_Cont += ' ng-model="milestonefields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                        if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                            $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                        }

                        var curFiletempobjmile = {};
                        var place = 'milestone';
                        curFiletempobjmile.place = "'" + place.toString() + "'";
                        curFiletempobjmile.AttrID = $scope.atributesRelationList[i].AttributeID;

                        $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview ng-pristine ng-valid" ng-model="milestonefields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="milestoneUploader_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<a class="margin-left10x" ng-model="UploadImage" ng-click="UploadImagefile(' + curFiletempobjmile.AttrID + ',' + curFiletempobjmile.place + ')" href="#EntityCreationUplaodImagediv" data-toggle="modal" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.milestonefields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = '';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        PlanoverviewService.GetTreeNode($scope.atributesRelationList[i].AttributeID).then(function (GetTree) {
                            $scope.tree = JSON.parse(GetTree.Response);
                        });
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if (j == 0) $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> <select ui-select2  ng-model=\"milestonefields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-options=\"ndata as ndata.Caption for ndata in tree.Children\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].Levels[j].LevelName + " </option> </select></div></div>";
                            else $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"milestonefields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> <select ui-select2 ng-model=\"milestonefields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-options=\"ndata as ndata.Caption for ndata in milestonefields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + j + ".Children\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].Levels[j].LevelName + " </option> </select></div></div>";
                            $scope.milestonefields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = "";
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly == false && $scope.IsLock == false) $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                    else $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                }
                $scope.dyn_Cont += '<input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#milestoneDataDiv").html($compile($scope.dyn_Cont)($scope));
                GetValidationList();
                $("#EnityMilestone").addClass('notvalidate');

                function GetValidationList() {
                    if (ValidationAttribute != null) {
                        $scope.listValidationResult = ValidationAttribute;
                        if ($scope.listAttriToAttriResult != null) {
                            for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                                var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                                if (attrRelIDs != undefined) {
                                    for (var j = 0; j < attrRelIDs.length; j++) {
                                        if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                            $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                                return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                            });
                                        } else {
                                            $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                                return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        Timeoutvar.milestonevalidation = $timeout(function () {
                            if ($("#EnityMilestone").length > 0) {
                                $("#EnityMilestone").nod($scope.listValidationResult, {
                                    'delay': 200,
                                    'submitBtnSelector': '#btnTemp',
                                    'silentSubmit': 'true'
                                });
                            }
                        }, 100);
                    }
                }
                $scope.AttributeData = [];
                $scope.saveMilestone = function () {
                    $("#btnTemp").click();
                    $("#EnityMilestone").removeClass('notvalidate');
                    if ($("#EnityMilestone .error").length > 0) {
                        return false;
                    }
                    $scope.AttributeData = [];
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                if ($scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if ($scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID != undefined) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID],
                                            "Level": $scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt($scope.OwnerID, 10),
                                        "Level": 0
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 3 && $scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.MilestoneEntityID) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($stateParams.ID),
                                    "Level": 0
                                });
                            } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                                if ($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                    if ($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                        var value = parseInt(($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": value,
                                            "Level": 0
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.milestonefields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.milestonefields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] == undefined ? "" : $scope.milestonefields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                            var milestoneDate = new Date.create();
                            var milestoneDateString;
                            if ($scope.milestonefields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == "") {
                                milestoneDate.setDate(milestoneDate.getDate());
                                milestoneDateString = ('0' + (milestoneDate.getMonth() + 1)).slice(-2) + '/' + ('0' + milestoneDate.getDate()).slice(-2) + '/' + milestoneDate.getFullYear();
                            } else {
                                milestoneDateString = $scope.milestonefields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString("yyyy-MM-dd");
                            }
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": milestoneDateString,
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.milestonefields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                            if ($scope.atributesRelationList[i].AttributeTypeID == 9 && $scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.MilestoneStatus) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": 0,
                                    "Level": 0
                                });
                            } else {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.milestonefields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                                    "Level": 0
                                });
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            if ($scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if ($scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                    var multiselectiObject = $scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                    for (var k = 0; k < multiselectiObject.length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": multiselectiObject[k].AttributeID,
                                            "AttributeCaption": multiselectiObject[k].Caption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": parseInt(multiselectiObject[k].Id, 10),
                                            "Level": 0
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": milestone_uploader.FileName,
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                if ($scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if ($scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID != undefined) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID],
                                            "Level": $scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                        });
                                    }
                                }
                            }
                        }
                    }
                    var SaveMilestone = {};
                    SaveMilestone.MilestoneEntityTypeID = SystemDefinedEntityTypes.Milestone;
                    SaveMilestone.Name = $scope.entityName;
                    SaveMilestone.AttributeData = $scope.AttributeData;
                    var uploaderNodeID = [];

                    var uploaderArr = $.grep(SaveMilestone.AttributeData, function (e) {
                        return e.AttributeTypeID == 11
                    })
                    if (uploaderArr.length > 0) {
                        for (var i = 0; i < uploaderArr.length; i++) {
                            uploaderNodeID.push(uploaderArr[i].NodeID);
                        }
                        if (uploaderNodeID.length > 0) {
                            PlanoverviewService.savemultipleUploadedImg(uploaderNodeID).then(function (newIDs) {
                                if (newIDs.Response != null) {
                                    for (var i = 0; i < newIDs.Response.length; i++) {
                                        var UploadAttr = $.grep(SaveMilestone.AttributeData, function (e) {
                                            return e.NodeID == newIDs.Response[i].m_Item1;
                                        })[0];
                                        var AttrIndex = SaveMilestone.AttributeData.indexOf(UploadAttr);
                                        SaveMilestone.AttributeData[AttrIndex].NodeID = newIDs.Response[i].m_Item2;
                                    }
                                    addmilestone(SaveMilestone);
                                }
                            });
                        }
                    }
                    else {
                        addmilestone(SaveMilestone);
                    }

                }
            }

            $scope.selectedMilestoneObj = {};
            $scope.SelectedMilestoneStatus = '';
            $scope.GetMilestoneByID = function (milestone) {
                $('#AddMilestoneModel').modal('show');
                $("#EnityMilestone").addClass('notvalidate');
                $scope.SelectedMilestoneStatus = milestone.Status;
                $scope.MilestoneID = milestone.MileStoneId;
                $scope.selectedMilestoneObj = $.grep($scope.MilestoneCollection, function (e) {
                    return e.EntityId == parseInt(milestone.MileStoneId)
                });
                var serer = $scope.selectedMilestoneObj;
                for (var i = 0; i < $scope.selectedMilestoneObj.length; i++) {
                    for (var j = 0; j < $scope.selectedMilestoneObj[i].AttributeData.length; j++) {
                        var milestoneAttrResult = $scope.selectedMilestoneObj[i].AttributeData[j];
                        if (milestoneAttrResult.TypeID == 3 && milestoneAttrResult.ID != SystemDefiendAttributes.MilestoneEntityID) {
                            if (milestoneAttrResult.IsSpecial == true) {
                                if (milestoneAttrResult.ID == SystemDefiendAttributes.Owner) {
                                    $scope.milestonefields["ListSingleSelection_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                                }
                            } else if (milestoneAttrResult.IsSpecial == false) {
                                $scope.milestonefields["ListSingleSelection_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                            }
                        } else if (milestoneAttrResult.TypeID == 1) {
                            var milestoneAttrResult_TypeID_1 = $('<div />').html(milestoneAttrResult.Value).text();
                            $scope.milestonefields["TextSingleLine_" + milestoneAttrResult.ID] = milestoneAttrResult_TypeID_1;
                        } else if (milestoneAttrResult.TypeID == 2) {
                            var milestoneAttrResult_TypeID_2 = $('<div />').html(milestoneAttrResult.Value).text();
                            $scope.milestonefields["TextMultiLine_" + milestoneAttrResult.ID] = milestoneAttrResult_TypeID_2;
                        } else if (milestoneAttrResult.TypeID == 4) {
                            $scope.milestonefields["ListMultiSelection_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 5) {
                            var param1 = new Date.create(milestoneAttrResult.Value);
                            var param2 = param1.getDate() + '-' + param1.getMonth() + '-' + param1.getFullYear();
                            var dateValue = milestoneAttrResult.Value;
                            dateValue = dateValue.split('-');
                            dateValue = dateValue[0] + '-' + dateValue[1] + '-' + dateValue[2];
                            $scope.milestonefields["DatePart_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 6) {
                            $scope.milestonefields["DropDown_" + milestoneAttrResult.ID + "_" + milestoneAttrResult.Level] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 8) {
                            $scope.milestonefields["TextMoney_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 9 && milestoneAttrResult.ID != SystemDefiendAttributes.MilestoneStatus) {
                            $scope.milestonefields["CheckBoxSelection_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 10) {
                            $scope.milestonefields["Period_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                        } else if (milestoneAttrResult.TypeID == 11) {
                            $scope.milestonefields["Uploader_" + milestoneAttrResult.ID] = milestoneAttrResult.Value;
                            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                                $('#milestoneUploader_' + milestoneAttrResult.ID).attr('src', cloudpath + 'UploadedImages/' + milestoneAttrResult.Value);
                            }
                            else {
                                $('#milestoneUploader_' + milestoneAttrResult.ID).attr('src', TenantFilePath + 'UploadedImages/' + milestoneAttrResult.Value);
                            }

                        } else if (milestoneAttrResult.TypeID == 12) {
                            $scope.milestonefields["MultiSelectDropDown_" + milestoneAttrResult.ID + "_" + milestoneAttrResult.Level] = milestoneAttrResult.Value;
                        }
                    }
                }
                $scope.EnableAdd = false;
                if ($scope.IsLock == false) {
                    $scope.EnableUpdate = true;
                } else {
                    $scope.EnableUpdate = false;
                }
            };
            $scope.milestonechecked = function (row, Index) {
                $scope.GetMilestoneByID(row);
                $scope.milestoneediting = row;
                $scope.milestoneRowIndex = Index;
                update
            }
            $scope.update = function () {
                $("#btnTemp").click();
                $("#EnityMilestone").removeClass('notvalidate');
                if ($("#EnityMilestone .error").length > 0) {
                    return false;
                }
                var updateMilestoneData = {};
                updateMilestoneData.MilestoneId = $scope.MilestoneID;
                $scope.AttributeData = [];
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if ($scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID],
                                        "Level": $scope.milestonefields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.OwnerID, 10),
                                    "Level": 0
                                });
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3 && $scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.MilestoneEntityID) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($stateParams.ID),
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            if ($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if ($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != null) {
                                    var value = parseInt(($scope.milestonefields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.milestonefields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.milestonefields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID],
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.milestonefields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.milestonefields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString('dd/MM/yyyy'),
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.milestonefields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        var milestoneStatusValue = 0;
                        if ($scope.SelectedMilestoneStatus.toString().toLowerCase() == "reached") {
                            milestoneStatusValue = 1;
                        } else if ($scope.SelectedMilestoneStatus.toString().toLowerCase() == "not reached") {
                            milestoneStatusValue = 0;
                        }
                        if ($scope.atributesRelationList[i].AttributeTypeID == 9 && $scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.MilestoneStatus) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt(milestoneStatusValue),
                                "Level": 0
                            });
                        } else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.milestonefields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        if ($scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                var multiselectiObject = $scope.milestonefields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": multiselectiObject[k].AttributeID,
                                        "AttributeCaption": multiselectiObject[k].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k].Id, 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": milestone_uploader.FileName,
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if ($scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].ID],
                                        "Level": $scope.milestonefields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    }
                }
                updateMilestoneData.MilestoneId = $scope.MilestoneID;
                updateMilestoneData.MilstoneName = $scope.entityName;
                updateMilestoneData.MilestoneEntityTypeID = SystemDefinedEntityTypes.Milestone;
                updateMilestoneData.EntityID = parseInt($stateParams.ID);
                updateMilestoneData.AttributeData = $scope.AttributeData;

                var uploaderNodeID = [];

                var uploaderArr = $.grep(updateMilestoneData.AttributeData, function (e) {
                    return e.AttributeTypeID == 11
                })
                if (uploaderArr.length > 0) {
                    for (var i = 0; i < uploaderArr.length; i++) {
                        uploaderNodeID.push(uploaderArr[i].NodeID);
                    }
                    if (uploaderNodeID.length > 0) {
                        PlanoverviewService.savemultipleUploadedImg(uploaderNodeID).then(function (newIDs) {
                            if (newIDs.Response != null) {
                                for (var i = 0; i < newIDs.Response.length; i++) {
                                    var UploadAttr = $.grep(updateMilestoneData.AttributeData, function (e) {
                                        return e.NodeID == newIDs.Response[i].m_Item1;
                                    })[0];
                                    var AttrIndex = updateMilestoneData.AttributeData.indexOf(UploadAttr);
                                    updateMilestoneData.AttributeData[AttrIndex].NodeID = newIDs.Response[i].m_Item2;
                                }
                                updatemilestone(updateMilestoneData);
                            }
                        });
                    }
                }
                else {
                    updatemilestone(updateMilestoneData);
                }

                upldatefilestone();
                $scope.DueDate = null;
                $scope.milestoneediting = '';
                $('#MileStoneHolder > table > tbody input:checkbox').each(function () {
                    $(this).next('i').removeClass('checked');
                });
                $scope.milestoneediting = '';
                $scope.TimerForLatestFeed();
            };
            $scope.saveMilestStatus = function (milestoneid, milestonename, milestoneduedate, milestoneshortdescription, optionarray) {
                var updateMilestoneData = {};
                updateMilestoneData.MilestoneId = milestoneid;
                updateMilestoneData.MilestoneStatus = parseInt(optionarray, 10);
                PlanoverviewService.UpdatingMilestoneStatus(updateMilestoneData).then(function (UpdateMilestoneStatusObj) {
                    if (UpdateMilestoneStatusObj.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4362.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_4485.Caption'));
                    GetMileStoneByEntityId(parseInt($stateParams.ID));
                });
                $scope.TimerForLatestFeed();
            };
            $scope.DeleteMilestone = function DeleteMilestoneByID(row) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2020.Caption'), function (result) {
                    if (result) {
                        Timeoutvar.milestoneload = $timeout(function () {
                            var ID = row.milestone.MileStoneId;
                            PlanoverviewService.DeleteMileStone(ID).then(function (deleteMiletestoneById) {
                                if (deleteMiletestoneById.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4301.Caption'));
                                else {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4484.Caption'));
                                    var index = $.inArray(row, $scope.milestones);
                                    $scope.milestones.splice(index, 1);
                                    PlanoverviewService.GetMilestoneMetadata(parseInt($stateParams.ID), SystemDefinedEntityTypes.Milestone).then(function (GetAllMilestones) {
                                        var milestonesData = GetAllMilestones.Response;
                                        $scope.MilestoneCollection = GetAllMilestones.Response;
                                        $scope.LoadMilestoneData(GetAllMilestones.Response);
                                    });
                                    GetMileStoneByEntityId(parseInt($stateParams.ID));
                                    $scope.TimerForLatestFeed();
                                }
                            });
                        }, 100);
                    }
                });
            };
        } catch (e) { }
        $scope.MilstoneStatus = [{
            'StatusId ': 1,
            'StatusDescription': 'Reached'
        }, {
            'StatusId ': 0,
            'StatusDescription': 'Not Reached'
        }]
        $scope.entityeditcontrolclick = function () {
            Timeoutvar.entityeditlock = $timeout(function () {
                $('[class=input-medium]').focus().select()
            }, 10);
        };
        $scope.entitymultilineeditcontrolclick = function () {
            Timeoutvar.entitymultieditlock = $timeout(function () {
                $('[class=input-large]').focus().select()
            }, 10);
        };
        $scope.DecodedText = function (Name) {
            if (Name != null & Name != "") {
                return $('<div />').html(Name).text();
            } else {
                return "";
            }
        };


        function addmilestone(SaveMilestone) {
            milestone_uploader = {};
            PlanoverviewService.CreateMilestone(SaveMilestone).then(function (milestoneCreateObj) {
                $('#AddMilestoneModel').modal('hide');
                if (milestoneCreateObj.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4483.Caption'));
                    $scope.AttributeData = [];
                    PlanoverviewService.GetMilestoneMetadata(parseInt($stateParams.ID), SystemDefinedEntityTypes.Milestone).then(function (GetAllMilestones) {
                        var milestonesData = GetAllMilestones.Response;
                        $scope.MilestoneCollection = GetAllMilestones.Response;
                        $scope.LoadMilestoneData(GetAllMilestones.Response);
                    });
                    GetMileStoneByEntityId(parseInt($stateParams.ID));
                    $scope.TimerForLatestFeed();
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4280.Caption'));
                    $scope.AttributeData = [];
                }
            });
        }

        function updatemilestone(updateMilestoneData) {
            PlanoverviewService.UpdateMilestone(updateMilestoneData).then(function (updateMilestoneResult) {
                $('#AddMilestoneModel').modal('hide');
                if (updateMilestoneResult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4361.Caption'));
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4486.Caption'));
                    PlanoverviewService.GetMilestoneMetadata(parseInt($stateParams.ID), SystemDefinedEntityTypes.Milestone).then(function (GetAllMilestones) {
                        var milestonesData = GetAllMilestones.Response;
                        $scope.MilestoneCollection = GetAllMilestones.Response;
                        $scope.LoadMilestoneData(GetAllMilestones.Response);
                    });
                    GetMileStoneByEntityId(parseInt($stateParams.ID))
                }
            });
        }

        $scope.StartUploaderImage = function () {
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles100',
                    container: 'filescontainer100',
                    max_file_size: '10mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }],
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#uploadprogress' + $scope.PopupModalGroupID + ' .bar').css("width", file.percent + "%");
                });
                uploader.bind('Error', function (up, err) {
                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    if ($scope.place == 'detail') {
                        SaveFileDetails(file, response.response);
                    }
                    else if ($scope.place == 'milestone') {
                        SaveFileDetails_Milestone(file, response.response);
                    }
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles100',
                    container: 'filescontainer100',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }],
                    multipart_params: {}

                });
                uploader.bind('Init', function (up, params) { });

                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) { });
                uploader.bind('Error', function (up, err) {
                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    if ($scope.place == 'detail') {
                        SaveFileDetails(file, response.response);
                    }
                    else if ($scope.place == 'milestone') {
                        SaveFileDetails_Milestone(file, response.response);
                    }
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

        }

        var milestone_uploader = {};
        function SaveFileDetails_Milestone(file, response) {
            var previewID = "milestoneUploader_" + $scope.UploadAttributeId;
            var resultArr = response.split(",");
            milestone_uploader.FileName = resultArr[0] + resultArr[2];

            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('#' + previewID).attr('src', cloudpath + 'UploadedImages/Temp/' + milestone_uploader.FileName);
            }
            else {
                $('#' + previewID).attr('src', TenantFilePath + 'UploadedImages/Temp/' + milestone_uploader.FileName);
            }
        }

        var jcrop_api;
        $scope.AssigneNewImage = function (w, h, path) {
            $('#UserProfilePic100 #userpic100').attr('src', 'UploadedImages/Temp/' + path);
            $('#UserProfilePic100 #userpic100').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#UserProfilePic100 #userpic100').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#UserProfilePic100 #userpic100').parent().css('margin-top', margine + 'px');
            } else {
                $('#UserProfilePic100 #userpic100').css('height', '528px');
                $('#UserProfilePic100 #userpic100').parent().css('margin-top', '0px');
            }
            $('#UserProfilePic100 #userpic100').css('visibility', 'visible');
            initJcrop();
        };

        function initJcrop() {
            if ($('#UserProfilePic100 .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#userpic100').Jcrop({
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.focus();
                var w = $('#UserProfilePic100 #userpic100').width();
                var h = $('#UserProfilePic100 #userpic100').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                $scope.userimgWidth = w;
                $scope.userimgHeight = h;
                jcrop_api.animateTo([0, 0, 0, 0]);
            });

            function showPreview(c) {
                var orgw = $('#UserProfilePic100 #userpic100').attr('data-width');
                var orgh = $('#UserProfilePic100 #userpic100').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $scope.PopupModalGroupID = 0;
        $scope.getgroupid = function (grpid) {
            $scope.PopupModalGroupID = grpid;
        }
        $scope.SaveUploadedImage100 = function () {
            var saveobj = {};
            saveobj.imgsourcepath = $("#userpic100").attr('src');
            saveobj.imgwidth = $scope.userimgWidth;
            saveobj.imgheight = $scope.userimgHeight;
            saveobj.imgX = $scope.userimgX;
            saveobj.imgY = $scope.userimgY;
            var filePath = $("#userpic100").attr('src').toString().replace("Temp", "");
            PlanoverviewService.SaveUploaderImage(saveobj).then(function (getchangeimageResult) {
                $scope.OrgimgWidth = 0;
                $scope.OrgimgHeight = 0;
                $scope.attrgrpImagefileName = $("#userpic100").attr('src').toString().split("/")[2];
                Timeoutvar.uploadmodal = $timeout(function () {
                    $("#UploaderImageControl" + $scope.PopupModalGroupID).attr("src", filePath);
                }, 200);
            });
        }
        Timeoutvar.startuploader = $timeout(function () {
            $scope.StartUploaderImage();
        }, 50);
        $scope.Clearxeditabletreedropdown = function Clearxeditabletreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['dropdown_' + attrid + '_' + i] = "";
            }
        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                            //$scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children;
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0) {
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                }
                                else {
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                                }
                                //$.each($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (i, el) {

                                //    res.push(el.Children);
                                //});
                                //$scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = res;
                            }
                            //$scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children;
                        }
                    }
                }

            }
        }

        $scope.isModelUpdated = function (attrID, levelcnt, attributeLevel, attrType, flag) {
            var currntlevel = attributeLevel + 1;
            var dropdown_text = 'dropdown_text_' + attrID + '_' + currntlevel, modelVal = {};
            for (var j = currntlevel; j <= levelcnt; j++) {
                $scope['dropdown_' + attrID + '_' + currntlevel] = 0;
                modelVal = ($.grep($scope["ddtoption_" + attrID + "_" + currntlevel].data, function (e) {
                    return e.Caption.toString() == $scope.treeTexts[dropdown_text].toString().trim()
                }))[0];
                if (modelVal != undefined) {
                    $scope['dropdown_' + attrID + '_' + currntlevel] = modelVal.id;
                }

            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        $scope.Clearxeditablemultiselecttreedropdown = function Clearxeditablemultiselecttreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['multiselectdropdown_' + attrid + '_' + i] = "";
            }
        }
        $scope.ShowHideAttributeOnRelations = function (attrVal) {
            var optionValue = attrVal;
            var attributesToHide = [];
            attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                return e.AttributeOptionID == optionValue;
            })[0]);
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide.length; i++) {
                    var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j]] = false;
                        }
                    }
                }
            } else {
                attributesToHide = [];
                attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                    return e.AttributeID == attrID;
                })[0]);
                if (attributesToHide[0] != undefined) {
                    for (var i = 0; i < attributesToHide.length; i++) {
                        var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j]] = true;
                            }
                        }
                    }
                }
            }
        }
        $scope.AddDefaultEndDate = function (stratdateval, enddateval) {
            if ($scope.fields[stratdateval] == null) {
                $scope.fields[enddateval] = null;
            } else {
                var objsetenddate = new Date.create($scope.fields[stratdateval]);
                $scope.fields[enddateval] = (7).daysAfter(objsetenddate);
            }
        };

        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }

        function ClearMilestoneFields() {
            try {
                Timeoutvar.datepartctrl = $timeout(function () {
                    $(".DatePartctrl").each(function () { });
                }, 20);
                var IsInherit = "";
                $scope.milestonefields.TextMultiLine_2 = "";
                for (var variable in $scope.milestonefields) {
                    if ($scope.milestonefields.hasOwnProperty(variable)) {
                        IsInherit = $.grep($scope.atributesRelationList, function (e) {
                            return e.AttributeID == parseInt(variable.substring(variable.lastIndexOf("_") + 1), 10)
                        })[0].InheritFromParent;
                        if (variable.indexOf("DatePart") != -1) {
                            if (!IsInherit) $scope.milestonefields[variable] = "";
                        }
                        if (typeof $scope.milestonefields[variable] === "string") {
                            if (variable !== "ListSingleSelection_69") {
                                if (!IsInherit) $scope.milestonefields[variable] = "";
                                else $scope.milestonefields[variable] = $.grep($scope.atributesRelationList, function (e) {
                                    return e.AttributeID == parseInt(variable.substring(variable.lastIndexOf("_") + 1), 10)
                                })[0].ParentValue[0]
                            }
                        } else if (typeof $scope.milestonefields[variable] === "number") {
                            if (!IsInherit) $scope.milestonefields[variable] = null;
                            else $scope.milestonefields[variable] = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeID == parseInt(variable.substring(variable.lastIndexOf("_") + 1), 10)
                            })[0].ParentValue[0]
                        } else if (Array.isArray($scope.milestonefields[variable])) {
                            if (!IsInherit) $scope.milestonefields[variable] = [];
                            else $scope.milestonefields[variable] = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeID == parseInt(variable.substring(variable.lastIndexOf("_") + 1), 10)
                            })[0].ParentValue[0]
                        } else if (typeof $scope.milestonefields[variable] === "object") {
                            if (!IsInherit) $scope.milestonefields[variable] = {};
                            else $scope.milestonefields[variable] = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeID == parseInt(variable.substring(variable.lastIndexOf("_") + 1), 10)
                            })[0].ParentValue[0]
                        }
                    }
                }
            } catch (e) { }
        }

        function UpdateEntityPeriodTree(entityid) {
            PlanoverviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (resultperiod) {
                if (resultperiod.StatusCode == 200) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                return false;
                            } else {
                                if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = resultperiod.Response;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
        }
        $scope.ChangeCostCentreCurrencyType = function () {
            $scope.SelectedCostCentreCurrency.Name = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].Name;
            $scope.SelectedCostCentreCurrency.ShortName = $.grep($scope.CurrencyFormatsList, function (e) {
                return e.Id == $scope.SelectedCostCentreCurrency.TypeId;
            })[0].ShortName;
            PlanoverviewService.GetCostCentreCurrencyRateById(0, $scope.SelectedCostCentreCurrency.TypeId, false).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.SelectedCostCentreCurrency.Rate = parseFloat(resCurrencyRate.Response[1]);
                    ReportCurrency.CurrencyRate = parseFloat(resCurrencyRate.Response[1]);
                    ReportCurrency.Name = $scope.SelectedCostCentreCurrency.ShortName;
                    $scope.LoadFinancialSummanyBlock($scope.tempresultHolder_Set2[2]);
                }
            });
        }
        $scope.InsertUpdateEntityGlobalRole = function () {
            if ($scope.IsLock == false) {
                var ObjGlobalRole = {
                    EntityID: $stateParams.ID,
                    GlobalRoleIDs: $scope.entityGlobalRole
                };
                PlanoverviewService.InsertEntityGlobalAccessInDetail(ObjGlobalRole).then(function (Insres) {
                    if (Insres.Response == true) NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    else NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                });
            }
        };

        /****** Update Entity OverAll  Staus *******/
        function getCurrentEntityStatus(currentEntityID) {
            PlanoverviewService.getCurrentEntityStatus(currentEntityID).then(function (status) {
                if (status.Response != 0) {
                    $scope.OverAllEntityStatus = status.Response;
                }

            })
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimer);
            $timeout.cancel(Timeoutvar);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.overviewCtrl']"));
        });
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }

            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        $scope.select2Config = {
            formatResult: $scope.overallstatusformat,
            formatSelection: $scope.overallstatusformat
        };

        $('#EntityFeedsdiv').scroll(function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                $scope.GetNewsFeedforPaging();
            }
        });
    }
    app.controller("mui.planningtool.default.detail.overviewCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$stateParams', '$location', '$window', '$sce', '$translate', 'PlanoverviewService', '$modal', muiplanningtooldefaultdetailoverviewCtrl]);

    app.directive('xeditablepercentage', ['$timeout', '$compile', '$resource', '$window', '$translate', 'PlanoverviewService', function ($timeout, $compile, $resource, $window, $translate, PlanoverviewService) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var dttype = attrs.editabletypeid;
                var choosefromParent = attrs.choosefromparent;
                var inhritfromParent = attrs.inheritfromparent;
                var isDataTypeExists = false;
                var access = true;
                access = attrs.isreadonly != undefined ? (attrs.isreadonly == "true" ? true : false) : true;
                for (var variable in $.fn.editabletypes) {
                    if (dttype == variable) isDataTypeExists = true;
                }
                var Address = function (options) {
                    this.init(dttype, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                inlinePopup += '<div class="ngcompilePercentage control-group relative">';
                inlinePopup += '<div drowdowntreepercentagemultiselection data-purpose=\'entity\' data-attributeid=' + attributeID.toString() + '></div>'
                inlinePopup += '</div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: $compile(inlinePopup)(scope),
                    inputclass: ''
                });
                $.fn.editabletypes[dttype] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    if ($('#DrpdwnPrice').hasClass('ng-hide')) $('#DrpdwnPrice').removeClass('ng-hide')
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompilePercentage'))(scope);
                            var treeCollection = [],
								treeData = [],
								remainRecord = [];
                            var GetTreeRes;
                            if (attrs.isdam == null) {
                                if (attrs.choosefromparent) PlanoverviewService.GetDropDownTreePricingObjectFromParentDetail(attrs.attributeid, attrs.inheritfromparent, attrs.choosefromparent, parseInt(CurrentEntityID), 0).then(function (GetTree) {
                                    if (GetTree.Response != null) {
                                        var result = GetTree.Response;
                                        if ($('#DrpdwnPrice').hasClass('ng-hide')) $('#DrpdwnPrice').removeClass('ng-hide')
                                        for (var p = 0, price; price = result[p++];) {
                                            var attributeLevelOptions = [];
                                            attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attrs.attributeid + ""], function (e) {
                                                return e.level == p;
                                            }));
                                            if ($('.editable-buttons').find('.span3').length == 0) $('.editable-buttons').prepend("<div class='span3'></div>");
                                            if (attributeLevelOptions[0] != undefined) {
                                                attributeLevelOptions[0].selection = price.selection;
                                                attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            }
                                        }
                                    }
                                });
                                else PlanoverviewService.GetDropDownTreePricingObject(attrs.attributeid, attrs.inheritfromparent, attrs.choosefromparent, parseInt(CurrentEntityID), 0).then(function (GetTree) {
                                    if (GetTree.Response != null) {
                                        var result = GetTree.Response;
                                        for (var p = 0, price; price = result[p++];) {
                                            var attributeLevelOptions = [];
                                            attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attrs.attributeid + ""], function (e) {
                                                return e.level == p;
                                            }));
                                            if (attributeLevelOptions[0] != undefined) {
                                                attributeLevelOptions[0].selection = price.selection;
                                                attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            }
                                        }
                                    }
                                });
                            } else if (attrs.isdam == "true") {
                                if (attrs.choosefromparent) {
                                    PlanoverviewService.GetDropDownTreePricingObjectFromParentDetaildam(attrs.attributeid, attrs.inheritfromparent, attrs.choosefromparent, parseInt(CurrentEntityID), 0).then(function (GetTree) {
                                        getDropDownTree(GetTree);
                                    });
                                } else {
                                    PlanoverviewService.GetDropDownTreePricingObjectdam().then(function (GetTree) {
                                        getDropDownTree(GetTree);
                                    });
                                }
                            }
                        }
                    }, 10);

                    function getDropDownTree(GetTree) {
                        if (GetTree.Response != null) {
                            var result = GetTree.Response;
                            if ($('#DrpdwnPrice').hasClass('ng-hide')) $('#DrpdwnPrice').removeClass('ng-hide')
                            for (var p = 0, price; price = result[p++];) {
                                var attributeLevelOptions = [];
                                attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attrs.attributeid + ""], function (e) {
                                    return e.level == p;
                                }));
                                if (attributeLevelOptions[0] != undefined) {
                                    attributeLevelOptions[0].selection = price.selection;
                                    attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                }
                            }
                        }
                    }
                });
                angular.element(element).on('save', function (e, params) {
                    var percentageflag = false;
                    $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                        if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                            if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                                percentageflag = true;
                            }
                        }
                    });
                    if (percentageflag) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4557.Caption'));
                        return false;
                    } else {
                        scope.saveDropDownTreePricing(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, choosefromParent, attrs.inheritfromparent);
                    }
                });
            }
        };
    }]);
})(angular, app);