using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveNumeric interface for table 'PM_Objective_Numeric'.
    /// </summary>
    public interface IObjectiveNumeric
    {
        #region Public Properties
        int ObjectiveId
        {
            get;
            set;
        }

        decimal GlobalBaseLine
        {
            get;
            set;

        }

        decimal GlobalTarget
        {
            get;
            set;

        }

        int Unitid
        {
            get;
            set;

        }

        int UnitValue
        {
            get;
            set;

        }
        
        

        #endregion
    }
}
