﻿

public enum XmlType
{
    Admin = 101,
    Metadata = 102,
    CurrentWorking = 103,
    CurrentSyncDB = 104,
    FutureMetadata = 105,
    FutureSyncDB = 106,
    InitialWorking = 107
}

public enum StorageAreaDal
{
    Local = 0,
    Amazon = 1
}