using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// AttributeDao object for table 'MM_Attribute'.
	/// </summary>
	
	public partial class AttributeDao : BaseDao, ICloneable
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
        protected string _description;
		protected int _attributetypeid;
		protected bool _issystemdefined;
        protected bool _isspecial;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public AttributeDao() {}

        public AttributeDao(int pId, string pCaption, string pDescription, int pAttributeTypeid, bool pIsSystemDefined, bool pIsSpecial)
		{
			this._id = pId; 
			this._caption = pCaption;
            this._description = pDescription; 
			this._attributetypeid = pAttributeTypeid; 
			this._issystemdefined = pIsSystemDefined; 
            this._isspecial = pIsSpecial;
		}
				
		public AttributeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual int AttributeTypeID
		{
			get { return _attributetypeid; }
			set { _bIsChanged |= (_attributetypeid != value); _attributetypeid = value; }
			
		}
		
		public virtual bool IsSystemDefined
		{
			get { return _issystemdefined; }
			set { _bIsChanged |= (_issystemdefined != value); _issystemdefined = value; }
			
		}
		

        public virtual bool IsSpecial
        {
            get { return _isspecial; }
            set { _bIsChanged |= (_isspecial != value); _isspecial = value; }

        }

		public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}
		
		public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods
		
		public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string AttributeDao = "AttributeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";  
            public const string Description = "Description";
            public const string AttributeTypeID = "AttributeTypeID";			
			public const string IsSystemDefined = "IsSystemDefined";			
            public const string IsSpecial = "IsSpecial";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string AttributeDao = "AttributeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";
            public const string Description = "Description";
            public const string AttributeTypeID = "AttributeTypeID";			
			public const string IsSystemDefined = "IsSystemDefined";			
            public const string IsSpecial = "IsSpecial";
		}

		#endregion    
		
		
	}
	
}
