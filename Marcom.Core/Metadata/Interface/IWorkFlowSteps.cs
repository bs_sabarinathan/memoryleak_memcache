﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata;
namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttribute interface for table 'MM_Attribute'.
    /// </summary>
    public interface IWorkFlowSteps
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int WorkFlowID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string HelpText
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        IList<IWorkFlowStepPredefinedTasks> WorkFlowTasks
        {
            get;
            set;
        }
        #endregion
    }
}
