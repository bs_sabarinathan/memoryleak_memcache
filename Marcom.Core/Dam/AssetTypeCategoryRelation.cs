﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetTypeCategoryRelation : IAssetTypeCategoryRelation
    {
        #region "Member variables"

        protected int _id;
        protected int _assettypeid;
        protected int _categoryid;

        #endregion

        #region "Constructor"

        public AssetTypeCategoryRelation() { }


        public AssetTypeCategoryRelation(int pid, int pAssettypeid, int pCategoryid) 
        {
            this._id = pid;
            this._assettypeid = pAssettypeid;
            this._categoryid = pCategoryid;
        }

        #endregion


        #region "Public propertiers"

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int AssetTypeId
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public int CategoryId
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }

        public bool IsDeleted
        {
            get;
            set;
        }

        #endregion

    }
}
