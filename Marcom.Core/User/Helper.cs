﻿using System;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Utility;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{
    public Helper()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static PasswordSetting GetPasswordSetting(string TenantHst)
    {
        IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null,MarcomManagerFactory.GetSystemSession(0));
        PasswordSetting passwordSetting = new PasswordSetting();
        //Get the tenant related file path
        BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
        string TenantFilePath = tfp.GetTenantFilePathByHostName(TenantHst);
        int TenantID = tfp.GetTenantIDByHostName(TenantHst);
        XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
        string xelementName = "PasswordPolicy";

        var xmlElement = xelementFilepath.Element(xelementName);
        foreach (var val in xmlElement.Descendants())
        {

            if (val.Name.ToString() == "duration")
                passwordSetting.Duration = int.Parse(val.Value);
            if (val.Name.ToString() == "minLength")
                passwordSetting.MinLength = int.Parse(val.Value);
            if (val.Name.ToString() == "maxLength")
                passwordSetting.MaxLength = int.Parse(val.Value);
            if (val.Name.ToString() == "numsLength")
                passwordSetting.NumsLength = int.Parse(val.Value);
            if (val.Name.ToString() == "specialLength")
                passwordSetting.SpecialLength = int.Parse(val.Value);
            if (val.Name.ToString() == "upperLength")
                passwordSetting.UpperLength = int.Parse(val.Value);
            if (val.Name.ToString() == "specialChars")
                passwordSetting.SpecialChars = val.Value;

        }

        return passwordSetting;

    }
}
