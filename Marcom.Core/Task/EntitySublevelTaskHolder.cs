﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;


namespace BrandSystems.Marcom.Core.Task
{
    internal class EntitySublevelTaskHolder : IEntitySublevelTaskHolder
    {
        #region Public Properties

        public int EntityID { get; set; }
        public string EntityName { get; set; }
        public int SortOrder { get; set; }
        public string EntityUniqueKey { get; set; }
        public string EntityTypeShortDescription { get; set; }
        public int EntityTypeID { get; set; }
        public string EntityTypeColorCode { get; set; }
        public IList<ITaskLibraryTemplateHolder> TaskListGroup { get; set; }
       
        #endregion
    }
}
