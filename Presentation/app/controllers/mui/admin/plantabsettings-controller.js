﻿(function (ng, app) {
    "use strict"; function muiadminplantabsettingsCtrl($scope, $resource, $location, $timeout, $translate, AdminService) {
        GetEntityTypeTabCollections(); $scope.PlanTabCollectionsList = [{}]; function GetEntityTypeTabCollections() { AdminService.GetPlantabsettings().then(function (gettabresult) { if (gettabresult.Response != null) { $scope.PlanTabCollectionsList.splice(0, 1); $scope.PlanTabCollectionsList.push({ Name: $translate.instant('LanguageContents.Res_35.Caption'), Value: gettabresult.Response.Attachments }); $scope.PlanTabCollectionsList.push({ Name: $translate.instant('LanguageContents.Res_1796.Caption'), Value: gettabresult.Response.Financials }); $scope.PlanTabCollectionsList.push({ Name: $translate.instant('LanguageContents.Res_701.Caption'), Value: gettabresult.Response.Objectives }); } }); }
        $scope.addClass = function (val) {
            if (val == true || val == "true")
            { return "checkbox checked"; }
            return "checkbox";
        }
        $scope.UpdatePlanTabs = function () {
            var jsonData = ""; for (var i = 0; i < $scope.PlanTabCollectionsList.length; i++)
            { jsonData += "<" + $scope.PlanTabCollectionsList[i].Name + ">" + $scope.PlanTabCollectionsList[i].Value + "</" + $scope.PlanTabCollectionsList[i].Name + ">" }
            var updateparam = {}; updateparam.jsondata = jsonData; AdminService.UpdatePlanTabIds(updateparam).then(function (result) { if (result.Response == true) { NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption')); } });
        }
    }
    app.controller("mui.admin.plantabsettingsCtrl", ['$scope', '$resource', '$location', '$timeout', '$translate', 'AdminService', muiadminplantabsettingsCtrl]);
})(angular, app);