﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class WidgetContainer : IWidgetContainer
    {

        #region Member Variables

        protected int _id;
        protected int _typeid;
        protected bool _IsDynamic;
        protected string _visualType;
        #endregion

        #region Constructors

        public WidgetContainer() { }
        public WidgetContainer(int pTypeid, string pVisualType, bool pIsDynamic)
        {
            this._typeid = pTypeid;
            this._visualType = pVisualType;
            this._IsDynamic = pIsDynamic;

        }

        public WidgetContainer(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public string VisualType
        {
            get { return _visualType; }
            set
            {
                _visualType = value;
            }

        }

        public bool IsDynamic
        {
            get { return _IsDynamic; }
            set
            {

                _IsDynamic = value;
            }

        }

        #endregion

    }

}
