﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

namespace Presentation
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPath = null;

            try
            {

                if (Request.QueryString["Ext"] == "-")
                {
                    strPath = Server.MapPath("~/UploadedImages/" + Request.QueryString["FileID"]);
                }
                else if (Request.QueryString["FileFriendlyName"].ToString() == "forUpgradetool")
                {
                    string path = Request.QueryString["FileID"];
                    strPath = path;
                }
                else if (Request.QueryString["DownloadType"].ToString() == "MetadataSettings")
                {
                    strPath = Server.MapPath("~/Files/ReportFiles/Images/Temp/" + Request.QueryString["FileID"] + Request.QueryString["Ext"]);
                }
                else
                {
                    string nam = Request.QueryString["FileID"];
                    if (nam.Contains("."))
                    {
                        nam = nam.Substring(0, nam.LastIndexOf('.'));
                        strPath = Server.MapPath("~/UploadedImages/" + Request.QueryString["FileID"]);
                    }
                    else
                        strPath = Server.MapPath("~/UploadedImages/" + Request.QueryString["FileID"] + Request.QueryString["Ext"]);
                }


                string fileName = Request.QueryString["FileID"];

                if (Request.QueryString["FileFriendlyName"].ToString() == "forUpgradetool")
                {
                    if ((fileName != null))
                    {
                        if (fileName.Length > 0)
                        {
                            DownloadFileUpgrade(strPath, true);
                        }
                    }
                }
                else if (Request.QueryString["DownloadType"].ToString() == "MetadataSettings")
                {
                    if ((fileName != null))
                    {
                        if (fileName.Length > 0)
                        {
                            if (File.Exists(strPath))
                            {
                                DownloadMetadataSettingsFile(strPath, true);
                            }
                        }
                    }
                }
                else 
                {
                    if ((fileName != null))
                    {
                        if (fileName.Length > 0)
                        {
                            if (File.Exists(strPath))
                            {
                                DownloadFile(strPath, true);

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void DownloadFile(string fname, bool forceDownload)
        {

            string fullpath = System.IO.Path.GetFullPath(fname);
            string strFriendlyName = Request.QueryString["FileFriendlyName"];

            string name = System.IO.Path.GetFileName(fullpath);
            name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
            string ext = System.IO.Path.GetExtension(fullpath);
            string type = "";

            switch (ext)
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;
                case "-":
                    type = "text/HTML";
                    break;
                case ".txt":
                    type = "text/plain";
                    break;
                case ".doc":
                case ".rtf":
                case ".docx":
                    type = "Application/msword";
                    break;
                case ".csv":
                case ".xls":
                case ".xlsx":
                    type = "Application/x-msexcel";
                    break;
                case ".pdf":
                    type = "Application/pdf";
                    break;
                case ".tiff":
                    type = "image/tiff";
                    break;
                case ".bmp":
                    type = "image/bmp";
                    break;
                case ".jpeg":
                    type = "image/jpeg";
                    break;
                case ".jpg":
                    type = "image/jpeg";
                    break;
                case ".png":
                    type = "image/png";
                    break;
                case ".gif":
                    type = "image/gif";
                    break;
                case ".zip":
                    type = "Application/zip";
                    break;
                default:
                    type = "application/octet-stream";
                    break;
            }

            if ((forceDownload))
            {
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + name + "\"");
            }
            if (!string.IsNullOrEmpty(type))
            {
                Response.ContentType = type;
                Response.WriteFile(fullpath);
                Response.End();

            }


        }

        private void DownloadFileUpgrade(string fname, bool forceDownload)
        {

            string fullpath = fname;
            string strFriendlyName = Request.QueryString["FileFriendlyName"];
            FileInfo fileInfo = new FileInfo(fname);
            string name = fileInfo.Name;
            //name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
            // string ext = System.IO.Path.GetExtension(fullpath);
            string ext = ".pdf";
            string type = "";

            switch (ext)
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;
                case "-":
                    type = "text/HTML";
                    break;
                case ".txt":
                    type = "text/plain";
                    break;
                case ".doc":
                case ".rtf":
                case ".docx":
                    type = "Application/msword";
                    break;
                case ".csv":
                case ".xls":
                case ".xlsx":
                    type = "Application/x-msexcel";
                    break;
                case ".pdf":
                    type = "Application/pdf";
                    break;
                case ".tiff":
                    type = "image/tiff";
                    break;
                case ".bmp":
                    type = "image/bmp";
                    break;
                case ".jpeg":
                    type = "image/jpeg";
                    break;
                case ".jpg":
                    type = "image/jpeg";
                    break;
                case ".png":
                    type = "image/png";
                    break;
                case ".gif":
                    type = "image/gif";
                    break;
                case ".zip":
                    type = "Application/zip";
                    break;
                default:
                    type = "application/octet-stream";
                    break;
            }

            if ((forceDownload))
            {
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + name + "\"");
            }
            if (!string.IsNullOrEmpty(type))
            {
                Response.ContentType = type;
                Response.WriteFile(fullpath);
                Response.End();

            }


        }

        private void DownloadMetadataSettingsFile(string fname, bool forceDownload)
        {
            string timeStamp = DateTime.Now.ToString("yyyy-MM-dd_hh_mm_ss");
            string fullpath = System.IO.Path.GetFullPath(fname);
            string strFriendlyName = Request.QueryString["FileFriendlyName"] + "_" + timeStamp + ".xml";

            string name = System.IO.Path.GetFileName(fullpath);
            name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
            string ext = System.IO.Path.GetExtension(fullpath);
            string type = "";

            switch (ext)
            {
                case ".xml":
                    type = "text/xml";
                    break;
                default:
                    type = "application/octet-stream";
                    break;
            }

            if ((forceDownload))
            {
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + name + "\"");
            }
            if (!string.IsNullOrEmpty(type))
            {
                Response.ContentType = type;
                Response.WriteFile(fullpath);
                Response.End();

            }


        }

        public Download()
        {
            Load += Page_Load;
        }
    }


}