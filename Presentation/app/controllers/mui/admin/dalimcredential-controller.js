﻿(function (ng, app) {
    "use strict"; function muidalimcrentialCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        AdminService.GetDalimSettings().then(function (res) { var result = res.Response; if (result != null) { $scope.dalimcustomerId = result[0].UserCredential.CustomerID; $scope.dalimcustomername = result[0].UserCredential.CustomerName; $scope.dalimdefaultusername = result[0].UserCredential.DefaultUserName; $scope.dalimdefaultuserpassword = result[0].UserCredential.DefaultUserPwd; $scope.dalimorgUnit = result[0].UserCredential.orgUnit; $scope.dalimdefaultprofile = result[0].UserCredential.defaultProfile; $scope.dalimprojecttemplatename = result[0].UserCredential.projectTemplateName; $scope.dalimProtocol = result[0].UserCredential.Protocol; $scope.dalimexternalUrl = result[0].UserCredential.ExternalDalimUrl; $scope.dalimDomain = result[0].UserCredential.DomainUrl; } }); $scope.LoadingTemplate = true; $scope.SavedalimRecord = function () {
            if ($scope.dalimdefaultuserpassword == undefined || $scope.dalimdefaultuserpassword == "") { bootbox.alert($translate.instant('LanguageContents.Res_4667.Caption')); return false; }
            if ($scope.dalimcustomerId == undefined || $scope.dalimcustomerId == "") { bootbox.alert($translate.instant('LanguageContents.Res_5603.Caption')); return false; }
            if ($scope.dalimcustomername == undefined || $scope.dalimcustomername == "") { bootbox.alert($translate.instant('LanguageContents.Res_5604.Caption')); return false; }
            if ($scope.dalimdefaultusername == undefined || $scope.dalimdefaultusername == "") { bootbox.alert($translate.instant('LanguageContents.Res_5605.Caption')); return false; }
            if ($scope.dalimorgUnit == undefined || $scope.dalimorgUnit == "") { bootbox.alert($translate.instant('LanguageContents.Res_5606.Caption')); return false; }
            if ($scope.dalimdefaultprofile == undefined || $scope.dalimdefaultprofile == "") { bootbox.alert($translate.instant('LanguageContents.Res_5607.Caption')); return false; }
            if ($scope.dalimprojecttemplatename == undefined || $scope.dalimprojecttemplatename == "") { bootbox.alert($translate.instant('LanguageContents.Res_5608.Caption')); return false; }
            if ($scope.dalimProtocol == undefined || $scope.dalimProtocol == "") { bootbox.alert($translate.instant('LanguageContents.Res_5609.Caption')); return false; }
            if ($scope.dalimexternalUrl == undefined || $scope.dalimexternalUrl == "") { bootbox.alert($translate.instant('LanguageContents.Res_4590.Caption')); return false; }
            if ($scope.dalimDomain == undefined || $scope.dalimDomain == "") { bootbox.alert($translate.instant('LanguageContents.Res_4590.Caption')); return false; }
            var addproofSettings = {}; addproofSettings.dalimcustomerId = $scope.dalimcustomerId; addproofSettings.dalimcustomername = $scope.dalimcustomername; addproofSettings.dalimdefaultusername = $scope.dalimdefaultusername; addproofSettings.dalimdefaultuserpassword = $scope.dalimdefaultuserpassword; addproofSettings.dalimorgUnit = $scope.dalimorgUnit; addproofSettings.dalimdefaultprofile = $scope.dalimdefaultprofile; addproofSettings.dalimprojecttemplatename = $scope.dalimprojecttemplatename; addproofSettings.dalimProtocol = $scope.dalimProtocol; addproofSettings.externalurl = $scope.dalimexternalUrl; addproofSettings.dalimDomain = $scope.dalimDomain; addproofSettings.TemaplateCollection = []; if ($scope.TemaplateCollection != null && $scope.TemaplateCollection != undefined) { addproofSettings.TemaplateCollection = $scope.TemaplateCollection; }
            AdminService.UpdateDalimSettings(addproofSettings).then(function (saveProofSettings) {
                if (saveProofSettings.Response == true)
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); else
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
            });
        }
        $scope.templateDataList = []; AdminService.GetApprovalFlowLibraryList(taskType.dalim_approval_task, true).then(function (Templatesdetails) { if (Templatesdetails.Response != null || Templatesdetails.Response != " ") { $scope.templateDataList = JSON.parse(Templatesdetails.Response); $scope.LoadingTemplate = false; getSelectedTemplate(); } }); function getSelectedTemplate() { AdminService.GetApprovalFlowLibraryList(taskType.dalim_approval_task, false).then(function (SelResult) { if (SelResult.Response != null && SelResult.Response != undefined && SelResult.Response != "") { var SelectedTemplate = JSON.parse(SelResult.Response); for (var i = 0; i < SelectedTemplate.length; i++) { for (var j = 0; j < $scope.templateDataList.length; j++) { if ($scope.templateDataList[j].ID == SelectedTemplate[i].TemplateFlow.ID) { $scope.templateDataList[j].Selected = true; } } } } }); }
        $scope.checkAllTemplates = function () {
            if ($scope.selectedAlltemplates) { $scope.selectedAlltemplates = true; } else { $scope.selectedAlltemplates = false; }
            angular.forEach($scope.templateDataList, function (item) { item.Selected = $scope.selectedAlltemplates; });
        }; $scope.RemoveCheckallSelection = function (list, $event) {
            var checkbox = $event.target; list.Selected = checkbox.checked; var tempTemplateCollection = []; $scope.TemaplateCollection = []; var TemaplateCollection = $.grep($scope.templateDataList, function (e) { return e.Selected == true; }); $scope.TemaplateCollection.push(TemaplateCollection); if ($scope.TemaplateCollection.length != $scope.templateDataList.length)
                $scope.selectedAlltemplates = false; else
                $scope.selectedAlltemplates = true;
        }
    }
    app.controller("mui.admin.muidalimcrentialCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muidalimcrentialCntrl]);
})(angular, app);