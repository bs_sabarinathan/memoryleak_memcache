﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.metadatasettingsctrl",
    function muimetadatasettingsctrl($scope, $state, $timeout, $http, $compile, $resource, $cookies, $location, $window, $translate, MetadatasettingsService) {

        $scope.localvar = {
            VerID: 0,
            SelectedVersionID: 0,
            Name: '',
            Description: '',
            ShowHideVersion: false,
            ShowHideApplyChanges: true,
            Metadataheader: ''
        };
        $scope.dynamicControls = '';
        $scope.FileID = "";
        $scope.ImportMetadataVerID = 0;

        $scope.ItemsToImport = {
            IsAttributes: true,
            IsAttributeGroup: true,
            IsUserDetails: true,
            IsEntityType: true,
            IsAssetType: true,
            IsTaskType: true,
            IsPageType: true
        }


        MetadatasettingsService.GetXmlNodes_CheckIfValueExistsOrNot().then(function (getstats) {
            $scope.localvar.ShowHideApplyChanges = true;
            if (getstats.Response == 0) {
                $scope.localvar.ShowHideApplyChanges = false;
            }


            MetadatasettingsService.GetMetadataVersion().then(function (getlistmetadataver) {
                $scope.listOfMetadataVer = getlistmetadataver;
                $scope.dynamicControls = '';
                var IsAddNewVersionEnable = true;

                for (var i = 0; i < getlistmetadataver.Response.length; i++) {
                    var datStartUTCval = "";
                    var datEndUTCval = "";

                    datStartUTCval = $scope.listOfMetadataVer.Response[i].StartDate.substr(0, $scope.listOfMetadataVer.Response[i].StartDate.indexOf('T') - 0);

                    if ($scope.listOfMetadataVer.Response[i].State == 0) {
                        datEndUTCval = $scope.listOfMetadataVer.Response[i].EndDate.substr(0, $scope.listOfMetadataVer.Response[i].StartDate.indexOf('T') - 0);

                        $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                        $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                        $scope.dynamicControls += '<div class="span1"><span></span></div>';
                        $scope.dynamicControls += '<div class="span5">' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' to ' + dateFormat(new Date(datEndUTCval), $scope.DefaultSettings.DateFormat) + '';
                        $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + ' (' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' Onwards)\');">View metadata</a>';
                        $scope.dynamicControls += '<span class="pull-right">';
                        $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span></div>';
                    }
                    else if ($scope.listOfMetadataVer.Response[i].State == 1) { 
                        $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem current"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                        $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                        $scope.dynamicControls += '<div class="span1"><span data-VerState=' + $scope.listOfMetadataVer.Response[i].ID + '>Active</span></div>';
                        $scope.dynamicControls += '<div data-VerPeriod=' + $scope.listOfMetadataVer.Response[i].ID + ' class="span5">' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' onwards';
                        $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,1,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + ' (' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' Onwards)\');">Edit metadata</a><a data-synctoDBID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-show=' + $scope.localvar.ShowHideApplyChanges + ' ng-click="MakeSynctoDB($event,1,' + $scope.listOfMetadataVer.Response[i].ID + ');">Apply Changes</a>';
                        $scope.dynamicControls += '<span class="pull-right"><a ng-click="ImpoortMetadataSettingsPopUp($event,' + $scope.listOfMetadataVer.Response[i].ID + ')" id="pickimportfiles" class="customIcon btn btn-primary mini primary margin-top0x" title="Import" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-down"></i></span></a>';
                        $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span>';
                        $scope.dynamicControls += '</div></div>';
                    }
                    else if ($scope.listOfMetadataVer.Response[i].State == 2) {
                        IsAddNewVersionEnable = false;
                        $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem next"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                        $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                        $scope.dynamicControls += '<div class="span1"><span data-VerState=' + $scope.listOfMetadataVer.Response[i].ID + '>Next</span></div><div data-VerPeriod=' + $scope.listOfMetadataVer.Response[i].ID + ' class="span5">';
                        $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,2,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\');">Edit metadata</a><a data-synctoDBID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target=""  ng-click="MakeSynctoDB($event,2,' + $scope.listOfMetadataVer.Response[i].ID + ');">Apply Changes And Activate</a>';
                        $scope.dynamicControls += '<span class="pull-right">';
                        $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span>';
                        $scope.dynamicControls += '</div></div>';

                    }
                    $scope.dynamicControls += '</div></li>'
                }

                $scope.dynamicControls += '<div class="text-right"></div>';

                $scope.dynamicControls += '<div id="addnewversionbtn" class="text-right"><a ng-click="NewMetadataVersion($event, 1);" role="button" class="btn btn-small btn-primary" data-toggle="modal">Add new version</a></div>';
                $('#loadMetadataVersions').html($compile($scope.dynamicControls)($scope));

                if (IsAddNewVersionEnable == false) {
                    $('div[id=addnewversionbtn]').hide();
                }
                $(window).AdjustHeightWidth();

                setTimeout($scope.StrartUpload(), 200);
            });
        });

        //-------------------> CLICK ON NEW METADATA BUTTON <-----------------
        $scope.NewMetadataVersion = function (event, stats, verID, VerName, VerDesc) {
            if ((event.target.offsetParent.tagName.toLowerCase() != "a" && event.target.tagName.toLowerCase() != "a") || event.target.parentNode.hasAttribute("id", "addnewversionbtn")) {
                $("#addListItem").modal('show');
                $scope.localvar.VerID = 0;
                $scope.localvar.Metadataheader = '';
                if (stats == 1) {
                    $scope.localvar.Metadataheader = "Add new metadata version";
                    $scope.localvar.ShowHideVersion = true;
                    $scope.listOfMetadataVerForPopUp = [];
                    $scope.listOfMetadataVerForPopUp.push({ ID: 0, Name: 'Initial System Version' });
                    for (var i = 0; i < $scope.listOfMetadataVer.length; i++) {
                        $scope.listOfMetadataVerForPopUp.push($scope.listOfMetadataVer.Response[i]);
                    }
                    var res = $.grep($scope.listOfMetadataVer, function (e) {
                        return e.State == 1;
                    });
                    if (res.length > 0)
                        $scope.localvar.SelectedVersionID = res[0].ID;
                }
                else {
                    $scope.localvar.Metadataheader = "Update metadata version";
                    $scope.localvar.ShowHideVersion = false;
                    $scope.localvar.VerID = verID;
                    $scope.localvar.Name = VerName;
                    $scope.localvar.Description = VerDesc;
                }
            }
        };


        //---------------------> EXPORT METADATA SETTINGS XML
        $scope.ExportMetadataSettingsXml = function (event, VerID, VerCaption) {
            $("#ExportInProgress").modal('show');
            //var ExportMetadataVersion = $resource('metadata/ExportMetadataSettingsXml/');
            //var expmetadataVer = new ExportMetadataVersion();
            var expmetadataVer = {};
            expmetadataVer.MetadataVerId = VerID;
            expmetadataVer.VersionName = VerCaption;

            // var result = ExportMetadataVersion.save(expmetadataVer, function () {
            MetadatasettingsService.ExportMetadataSettingsXml(expmetadataVer).then(function (result) {
                if (result.Response != null) {
                    var exportfile = document.createElement('a'), fileid = result.Response, extn = '.xml';
                    var filename = "MetadataSettings_V" + VerID;
                    exportfile.href = 'Download.aspx?FileID=' + result.Response + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&DownloadType=MetadataSettings';
                    exportfile.download = fileid + extn;
                    document.body.appendChild(exportfile);
                    exportfile.click();
                    $timeout(function () {
                        $("#ExportInProgress").modal('hide');
                    }, 1000);
                }
                else
                    $("#ExportInProgress").modal('hide');

            });

            event.stopImmediatePropagation();
            event.stopPropagation();
        }

        //---------------------> ON CLICK PROPOGATE EDIT METADATA VERSION POP UP <--------------
        $scope.ImpoortMetadataSettingsPopUp = function (event, VerID) {
            $scope.ImportMetadataVerID = VerID;
            $("#importMetadataModel").removeClass('animated shake');
        }
      
        //------------------>  IMPORT METADATA SETTINGS XML <--------------
        $scope.ImportMetadataSettingsXml = function () {
            if ($scope.localvar.ShowHideApplyChanges == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_1146.Caption'));
                return false;
            }

            //---------------> SAVE IMPORT XML FILE TO CURRENT WORKING XML FILE
            $("#importMetadataModel").modal('hide');
            $("#ImportInProgress").modal('show');           
            //var ImportMetadataVersion = $resource('metadata/ImportMetadataSettingsXml/');
            //var impmetadataVer = new ImportMetadataVersion();
            var impmetadataVer = {};
            impmetadataVer.MetadataVerId = $scope.ImportMetadataVerID;
            impmetadataVer.ImportTypes = $scope.ItemsToImport;
            impmetadataVer.ImportFileID = $scope.FileID;
            MetadatasettingsService.ImportMetadataSettingsXml(impmetadataVer).then(function (result) {
                //  var result = ImportMetadataVersion.save(impmetadataVer, function () {
                if (result.StatusCode == 200 && result.Response == "Success") {
                    $scope.LoadMetadataVersion();
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    $("#ImportInProgress").modal('hide');
                }
                else {
                    NotifyError(result.Response);
                    $("#ImportInProgress").modal('hide');
                }

                $scope.localvar.VerID = 0;
                $scope.localvar.Name = '';
                $scope.localvar.Description = '';
                $scope.localvar.ShowHideVersion = false;
            });
        }

        //-----------------> SAVE NEW METADATA VERSION <---------------------
        $scope.SaveNewMetadataVersion = function () {
            //var AddNewMetadataVersion = $resource('metadata/InsertMetadataVersion/');
            //var addmetadataVer = new AddNewMetadataVersion();
            var addmetadataVer = {};
            addmetadataVer.ID = $scope.localvar.VerID;
            addmetadataVer.Name = $scope.localvar.Name;
            addmetadataVer.Description = $scope.localvar.Description;
            addmetadataVer.selectedMetadataVer = $scope.localvar.SelectedVersionID;

            // var result = AddNewMetadataVersion.save(addmetadataVer, function () {
            MetadatasettingsService.InsertMetadataVersion(addmetadataVer).then(function (result) {
                if (result.StatusCode == 200) {
                    $scope.LoadMetadataVersion();
                }
                else {
                }
                $scope.localvar.VerID = 0;
                $scope.localvar.Name = '';
                $scope.localvar.Description = '';
                $scope.localvar.ShowHideVersion = false;
            });
        }
        $scope.SettingsEntityTypeMetadata = {};
        //-----------------> EDIT CURRENT METADATA VERSION <---------------------
        $scope.UpadateCurrentVersion = function (event, IsCurrentOrFutureVersion, verID, VerName) {
            $scope.CurrentMetadataVersionInfo.ID = verID;
            $scope.CurrentMetadataVersionInfo.Name = VerName;
            localStorage.setItem("CurrentMetadataVersionID", verID);
            localStorage.setItem("CurrentMetadataVersionName", VerName);

            if (IsCurrentOrFutureVersion == 0)
                $scope.IsOlderMetadataVersion.IsOlderVersion = false;
            else
                $scope.IsOlderMetadataVersion.IsOlderVersion = true;

            //var SetWorkingVersionFlag = $resource('metadata/SetWorkingVersionFlag/:Flag/:VersionNo', { Flag: IsCurrentOrFutureVersion, VersionNo: verID });
            //var result = SetWorkingVersionFlag.save({ Flag: IsCurrentOrFutureVersion, VersionNo: verID }, function () {
            var SetWork = {};
            SetWork.Flag = IsCurrentOrFutureVersion;
            SetWork.VersionNo = verID;
            MetadatasettingsService.SetWorkingVersionFlag(SetWork).then(function (result) {
                if (result.StatusCode == 200) {
                    //var GetRoleUserByID = $resource('access/GetGlobalRoleUserByID/:ID', { ID: $scope.UserId });
                    //var getroleuserbyid = GetRoleUserByID.get(function () {
                    MetadatasettingsService.GetGlobalRoleUserByID($scope.UserId).then(function (getroleuserbyid) {
                        $scope.RolesOfUser = getroleuserbyid.Response;
                        var res = Math.min.apply(Math, $scope.RolesOfUser)
                        if (res == -1) {
                            $scope.settingsM = { 0: true }
                        }
                        //var getsuperglobalacl = $resource('access/GetSuperglobalacl', { get: { method: 'GET' } });
                        //var objgetgetsuperglobalacl = getsuperglobalacl.get(function () {
                        MetadatasettingsService.GetSuperglobalacl().then(function (objgetgetsuperglobalacl) {
                            $scope.resul = objgetgetsuperglobalacl.Response;
                            var globalrole = [];
                            if (objgetgetsuperglobalacl.Response.length > 0) {
                                var Res = [];
                                var Result = [];
                                var Isvalidation = false;
                                for (var i = 0; i < objgetgetsuperglobalacl.Response.length; i++) {
                                    if (objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true && objgetgetsuperglobalacl.Response[i].GlobalRoleid == res) {
                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 66) {
                                            //$location.path('/mui/metadataconfiguration/entitytypeattributerelation');

                                            $state.go('mui.MetadataConfiguration.entitytypeattributerelation');

                                            return true;
                                        }

                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 67) {
                                            $state.go('mui.metadataconfiguration.Damentitytype');
                                            return true;
                                        }

                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 68) {
                                            $state.go('mui.metadataconfiguration.Taskentitytype');
                                            return true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 69) {
                                            $state.go('mui.metadataconfiguration.cmspagetypeattributerelation');
                                            return true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 70) {
                                            $state.go('mui.metadataconfiguration.attributegroup');
                                            return true;
                                        }

                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 71) {
                                            $state.go('mui.metadataconfiguration.userdetails'); return true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 72) {
                                            $state.go('mui.metadataconfiguration.attribute'); return true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i].Featureid == 87) {
                                            $state.go('mui.metadataconfiguration.objectiveentitytype'); return true;
                                        }
                                    }
                                    if (objgetgetsuperglobalacl.Response[i].GlobalRoleid == res && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == false) {
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 66 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 67 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 68 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 69 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 70 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 71 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 72 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                        if (objgetgetsuperglobalacl.Response[i]["Featureid"] == 87 && objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true) {
                                            Isvalidation = true;
                                        }
                                    }
                                }
                                if (res != -1 && Isvalidation == false) {
                                    bootbox.alert($translate.instant('LanguageContents.Res_5780.Caption'));
                                }
                            }
                            if (objgetgetsuperglobalacl.Response.length == 0)
                                bootbox.alert($translate.instant('LanguageContents.Res_5780.Caption'));
                            if (res == -1) {
                                $state.go('mui.MetadataConfiguration.entitytypeattributerelation');
                                return true;
                            }
                        });
                    });
                }
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        };

        $scope.MakeSynctoDB = function (event, IsCurrentOrFutureVersion, VerID) {

            $("a[data-synctodbid = " + VerID + "]").attr('disabled', true).text('In progress...');

            //var SetWorkingVersionFlag = $resource('metadata/SetWorkingVersionFlag/:Flag/:VersionNo', { Flag: IsCurrentOrFutureVersion, VersionNo: VerID });
            //var result = SetWorkingVersionFlag.save({ Flag: IsCurrentOrFutureVersion, VersionNo: VerID }, function () {
            var SetWork = {};
            SetWork.Flag = IsCurrentOrFutureVersion;
            SetWork.VersionNo = VerID;
            MetadatasettingsService.SetWorkingVersionFlag(SetWork).then(function (result) {
                if (result.StatusCode == 200) {
                    //var SyncToDB = $resource('metadata/SyncToDb/');
                    //var syncdb = new SyncToDB();
                    var syncdb = {};
                    //syncdb.$save(function (result) {
                    MetadatasettingsService.SyncToDb().then(function (result) {
                        //MetadatasettingsService.SetWorkingVersionFlag(syncdb).then(function (result) {
                        if (result.Response == true) {
                            LoadContextEntitycreationMenu();
                            $scope.LoadMetadataVersion();
                            NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                            $("#importMetadataModel").modal('hide');
                        }
                        else {
                            bootbox.alert($translate.instant('LanguageContents.Res_4273.Caption'));
                            $("a[data-synctodbid = " + VerID + "]").attr('disabled', false).text('Apply Changes');
                            $("#importMetadataModel").modal('hide');
                        }
                    });
                }
            });

            event.stopImmediatePropagation();
            event.stopPropagation();
        };
       

        function LoadContextEntitycreationMenu() {

            $scope.ContextMenuRecords.ChildContextData = [];
            if ($scope.ContextMenuRecords.ChildContextData != null) {
                if ($scope.ContextMenuRecords.ChildContextData.length > 0)
                    $scope.ContextMenuRecords.ChildContextData = [];
            }

            var childResult = [];


            //var GetChildEntityHierarachy = $resource('metadata/ChildEntityTypeHierarchy', { get: { method: 'GET' } });
            //var GetChildEntityHierarachyresult = GetChildEntityHierarachy.get(function () {

            MetadatasettingsService.ChildEntityTypeHierarchy().then(function (GetChildEntityHierarachyresult) {

                childResult = GetChildEntityHierarachyresult.Response;

                // looping a result collection
                if (childResult != null) {
                    for (var i = 0, child; child = childResult[i++];) {
                        // push into parent context data
                        $scope.ContextMenuRecords.ChildContextData.push(child);
                    }
                }
            });
        }

        $scope.LoadMetadataVersion = function () {
            //var Getsystodbstatus = $resource('metadata/GetXmlNodes_CheckIfValueExistsOrNot');
            //var getstats = Getsystodbstatus.get(function () {
            MetadatasettingsService.GetXmlNodes_CheckIfValueExistsOrNot().then(function (getstats) {
                $scope.localvar.ShowHideApplyChanges = true;
                if (getstats.Response == 0) {
                    $scope.localvar.ShowHideApplyChanges = false;
                }

                //var Getlistofmetadataversion = $resource('metadata/GetMetadataVersion');
                //var getlistmetadataver = Getlistofmetadataversion.query(function () {
                MetadatasettingsService.GetMetadataVersion().then(function (getlistmetadataver) {
                    $scope.listOfMetadataVer = getlistmetadataver;
                    $scope.dynamicControls = '';
                    var IsAddNewVersionEnable = true;

                    for (var i = 0; i < getlistmetadataver.Response.length; i++) {
                        var datStartUTCval = "";
                        var datEndUTCval = "";

                        datStartUTCval = $scope.listOfMetadataVer.Response[i].StartDate.substr(0, $scope.listOfMetadataVer.Response[i].StartDate.indexOf('T') - 0);

                        if ($scope.listOfMetadataVer.Response[i].State == 0) {
                            datEndUTCval = $scope.listOfMetadataVer.Response[i].EndDate.substr(0, $scope.listOfMetadataVer.Response[i].StartDate.indexOf('T') - 0);

                            $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + '  data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                            $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                            $scope.dynamicControls += '<div class="span2"><span></span></div>';
                            $scope.dynamicControls += '<div class="span4">' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' to ' + dateFormat(new Date(datEndUTCval), $scope.DefaultSettings.DateFormat);
                            $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + ' (' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' Onwards)\');">View metadata</a>';
                            $scope.dynamicControls += '<span class="pull-right">';
                            $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span></div>';
                        }
                        else if ($scope.listOfMetadataVer.Response[i].State == 1) {
                            $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem current"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                            $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                            $scope.dynamicControls += '<div class="span2"><span data-VerState=' + $scope.listOfMetadataVer.Response[i].ID + '>Active</span></div>';
                            $scope.dynamicControls += '<div data-VerPeriod=' + $scope.listOfMetadataVer.Response[i].ID + ' class="span4">' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' Onwards';
                            $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,1,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + ' (' + dateFormat(new Date(datStartUTCval), $scope.DefaultSettings.DateFormat) + ' Onwards)\');">Edit metadata</a><a data-synctoDBID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-show=' + $scope.localvar.ShowHideApplyChanges + ' ng-click="MakeSynctoDB($event,1,' + $scope.listOfMetadataVer.Response[i].ID + ');">Apply Changes</a>';
                            $scope.dynamicControls += '<span class="pull-right"><a ng-click="ImpoortMetadataSettingsPopUp($event,' + $scope.listOfMetadataVer.Response[i].ID + ')" id="pickimportfiles" class="customIcon btn btn-primary mini primary margin-top0x" title="Import" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-down"></i></span></a>';
                            $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span>';
                            $scope.dynamicControls += '</div></div>';

                        }
                        else if ($scope.listOfMetadataVer.Response[i].State == 2) {
                            IsAddNewVersionEnable = false;
                            $scope.dynamicControls += '<li data-verID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" ng-click="NewMetadataVersion($event, 0,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\', \'' + $scope.listOfMetadataVer.Response[i].Description + '\');" class="ListItem next"><div class="row-fluid"><div class="span2"><i class="ListItemNo">' + $scope.listOfMetadataVer.Response[i].ID + '</i><span>' + $scope.listOfMetadataVer.Response[i].Name + '</span></div>';
                            $scope.dynamicControls += '<div class="span4"><span>' + $scope.listOfMetadataVer.Response[i].Description + '</span></div>';
                            $scope.dynamicControls += '<div class="span2"><span data-VerState=' + $scope.listOfMetadataVer.Response[i].ID + '>Next</span></div><div data-VerPeriod=' + $scope.listOfMetadataVer.Response[i].ID + ' class="span4">';
                            $scope.dynamicControls += '<a data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target="" ng-click="UpadateCurrentVersion($event,2,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\');">Edit metadata</a><a data-synctoDBID=' + $scope.listOfMetadataVer.Response[i].ID + ' data-toggle="modal" class="btn btn-small btn-primary pull-right" role="button" data-target=""  ng-click="MakeSynctoDB($event,2,' + $scope.listOfMetadataVer.Response[i].ID + ');">Apply Changes And Activate</a>';
                            $scope.dynamicControls += '<span class="pull-right">';
                            $scope.dynamicControls += '<a ng-click="ExportMetadataSettingsXml($event,' + $scope.listOfMetadataVer.Response[i].ID + ',\'' + $scope.listOfMetadataVer.Response[i].Name + '\')" class="customIcon btn btn-primary mini primary margin-top0x" title="Export" data-toggle="modal"><span class="baseIcon"><i class="icon-file-text-alt"></i></span><span class="foreIcon"><i class="icon-double-angle-up"></i></span></a></span>';
                            $scope.dynamicControls += '</div></div>';
                        }
                        $scope.dynamicControls += '</div></li>'
                    }


                    $scope.dynamicControls += '<div id="addnewversionbtn" class="text-right"><a ng-click="NewMetadataVersion($event, 1);" role="button" class="btn btn-small btn-primary" data-toggle="modal">Add new version</a></div>';
                    $('#loadMetadataVersions').html($compile($scope.dynamicControls)($scope));

                    if (IsAddNewVersionEnable == false) {
                        $('div[id=addnewversionbtn]').hide();
                    }

                    $(window).AdjustHeightWidth();

                    setTimeout($scope.StrartUpload(), 250);

                    // $scope.StrartUpload();
                });
            });
        }

        $scope.GetMetadataVersionId = function (verID) {
            var len = $scope.listOfMetadataVer.length;
            if ($scope.listOfMetadataVer[len - 1].ID != parseInt(verID)) {
                bootbox.alert($translate.instant('LanguageContents.Res_1870.Caption'));
            }
        }

        $scope.ReportServerPushSchema = function ReportServerPushSchema() {
            $("#generateviewsschema").attr('disabled', true).text('Generating...');
            //var generateReportView = $resource('metadata/ReportViewCreationAndPushSchema', { get: { method: 'GET' } });
            //var GetGenerateReportView = generateReportView.get(function () {

            MetadatasettingsService.ReportViewCreationAndPushSchema().then(function (GetGenerateReportView) {
                var result = GetGenerateReportView.Response;
                if (result == 4) {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
                else if (result == 1) {
                    //$("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1861.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1862.Caption'));
                }
                else if (result == 2) {
                    //$("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1863.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1864.Caption'));
                }
                else if (result == 3) {
                    //$("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1863.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1864.Caption'));
                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                }
                $("#generateviewsschema").attr('disabled', false).text('Generate Report Views And Schema');
            });

        }

        var imgfileid = '';
        var uploader;
        $scope.StrartUpload = function () {
            uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'pickimportfiles',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/CustomHandler.ashx?Path=Files/ImportExportFiles&typeoffile=Doc',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [
                    { title: "Xml files", extensions: "xml" }
                ],
                resize: { width: 320, height: 240, quality: 90 }
            });

            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });

            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                up.refresh(); // Reposition Flash/Silverlight
                uploader.start();
            });

            uploader.bind('Error', function (up, err) {
                $('#filelist').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );

                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.FileID = response.response.split(',')[0];
                $("#importMetadataModel").modal('show');              
            });                  
        };
             
        //---------------------------  Get the initial feed ------------------------------------
        // --- Define Scope Variables. ---------------------- //
        // --- Bind To Scope Events. ------------------------ //
        // I handle changes to the request context.
        // --- Initialize. ---------------------------------- //
        //-------------- user defined functions ---------


        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.metadatasettingsctrl']"));
        });
    }
    //);
    app.controller("mui.metadatasettingsctrl", ['$scope', '$state', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', '$translate', 'MetadatasettingsService', muimetadatasettingsctrl]);
    //var myApp = angular.module('myApp', []);
    function MetadatasettingsService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); 
        return ({
            GetXmlNodes_CheckIfValueExistsOrNot:GetXmlNodes_CheckIfValueExistsOrNot,
            GetMetadataVersion:GetMetadataVersion,
            ExportMetadataSettingsXml:ExportMetadataSettingsXml,
            ImportMetadataSettingsXml:ImportMetadataSettingsXml,
            InsertMetadataVersion:InsertMetadataVersion,
            SetWorkingVersionFlag:SetWorkingVersionFlag,
            SyncToDb:SyncToDb,
            ChildEntityTypeHierarchy:ChildEntityTypeHierarchy,
            ReportViewCreationAndPushSchema:ReportViewCreationAndPushSchema,
            GetGlobalRoleUserByID:GetGlobalRoleUserByID,
            GetSuperglobalacl:GetSuperglobalacl
        });
        function GetXmlNodes_CheckIfValueExistsOrNot() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetXmlNodes_CheckIfValueExistsOrNot/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMetadataVersion() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetMetadataVersion/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ExportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ExportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function ImportMetadataSettingsXml(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/ImportMetadataSettingsXml/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertMetadataVersion(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/InsertMetadataVersion/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SetWorkingVersionFlag(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetWorkingVersionFlag/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function SyncToDb() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SyncToDb/",
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ChildEntityTypeHierarchy() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ChildEntityTypeHierarchy/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function ReportViewCreationAndPushSchema() {
            var request = $http({
                method: "get",
                url: "api/Metadata/ReportViewCreationAndPushSchema/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetGlobalRoleUserByID(ID) {
            var request = $http({
                method: "get",
                url: "api/access/GetGlobalRoleUserByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSuperglobalacl() {
            var request = $http({
                method: "get",
                url: "api/access/GetSuperglobalacl/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("MetadatasettingsService", ['$http', '$q', MetadatasettingsService]);
})(angular, app);