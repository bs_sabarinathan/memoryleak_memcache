﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Utility;
using Newtonsoft.Json.Linq;
using Presentation.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using System.Configuration;
using Newtonsoft.Json;
using BrandSystems.Marcom.Core.AmazonStorageHelper;

namespace Presentation.Controllers
{
    public class UserController : ApiController
    {
        ServiceResponse result;
        [Route("api/User/InsertUser")]
        [HttpPost]
        public ServiceResponse InsertUser([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject jobj = JObject.Parse(jsonobj.ToString());
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.Response = marcomManager.UserManager.User_Insert((string)jobj["Email"], (string)jobj["FirstName"], (string)jobj["LastName"], (string)jobj["Imageurl"], (string)jobj["Language"], (string)jobj["Password"], (int?)jobj["StartPage"], (string)jobj["TimeZone"], (string)jobj["UserName"], (int)jobj["DashboardTemplateID"], (bool)jobj["IsApiUser"], listattributevalues, (int)jobj["AssetAccessID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/RegisterUser")]
        [HttpPost]
        public ServiceResponse RegisterUser([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                JObject jobj = JObject.Parse(jsonobj.ToString());
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = objDb.RegisterData(jobj, tenantres.tenanturl);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/UpdateUser")]
        [HttpPut]
        public ServiceResponse UpdateUser([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject jobj = JObject.Parse(jsonobj.ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcommanager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.Response = marcommanager.UserManager.User_Update((int)jobj["ID"], (string)jobj["Email"], (string)jobj["FirstName"], (string)jobj["LastName"], (string)jobj["Imageurl"], (string)jobj["Language"], (int)jobj["StartPage"], (string)jobj["TimeZone"], (int)jobj["DashboardTemplateID"], (bool)jobj["IsSSOUser"], (bool)jobj["IsApiUser"], listattributevalues, (int)jobj["AssetAccess"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/DeleteUser/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteUser(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.User_DeleteByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed; ;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/CheckUserInvolvement/{ID}")]
        [HttpGet]
        public ServiceResponse CheckUserInvolvement(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.User_CheckUserInvolvementByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed; ;
                result.Response = false;
            }
            return result;
        }


        //Will retrive the login details
        [Route("api/User/GetLogoLoginDetails")]
        [HttpGet]
        public ServiceResponse GetLogoLoginDetails()
        {


            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = objDb.GetLoginLogoDetails(tenantres.tenantID);



            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        [Route("api/User/ClientIntranetUrl")]
        [HttpGet]
        public ServiceResponse ClientIntranetUrl()
        {
            result = new ServiceResponse();

            try
            {
                //identify the tenant
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = objDb.GetClientIntranetUrl(tenantres.tenanturl);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //[Route("api/User/ValidUser/")]
        //Will authenticate the user credentials
        [Route("api/User/ValidUser")]
        [HttpPost]
        public ServiceResponse ValidUser([FromBody]JObject jsonString)
        {
            result = new ServiceResponse();
            try
            {
                JObject jobj = JObject.Parse(jsonString.ToString());
                int duration = 0,LoginAttempt = 0;
                if (((string)jobj["Password"]).Length == 0)
                {
                    throw new InvalidExpressionException();
                }
                JObject Data = new JObject();
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,(Guid)systemSession);
                string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                PasswordSetting pp = GettingAccountLocalXMLValues();
                ILoginCheck prevattempts = objDb.GetAttemptByEmail("Select COUNT(Email) as attempt from UM_LoginCheck where Email ='" + ((string)jobj["Email"]).Trim() + "'", CommandType.Text);
                LoginAttempt = (prevattempts == null) ? 0 : (int)prevattempts.Id;
                PasswordSetting minutesdata = objDb.GetMinuteByEmail("SELECT TOP 1 DATEDIFF(minute,LoginTime,Getdate()) as LoginTime from UM_LoginCheck where Email ='" + ((string)jobj["Email"]).Trim() + "' ORDER BY LoginTime DESC; ", CommandType.Text);
                duration = (minutesdata == null) ? 0 : (int)minutesdata.Duration;
                int UserID = objDb.CheckValidUserID("SELECT ID,Password,PasswordSalt FROM UM_User  WHERE Email='" + ((string)jobj["Email"]).Trim() + "'", CommandType.Text, (string)jobj["Password"]);
                if (pp.notifyadmin)
                {
                    #region hardlock
                    if (UserID != 0 && LoginAttempt < pp.attempts)
                    {
                        // if Valid User (this function used to create Session id and give user details)
                        PasswordExpiryValidation(pp.expirevalidation, (string)jobj["Email"], UserID, pp.monthlyexpiration, pp.notifyadmin);
                    }
                    else if (UserID != 0 && LoginAttempt >= pp.attempts && duration > pp.lockouttime)
                    {
                        PasswordExpiryValidation(pp.expirevalidation, (string)jobj["Email"], UserID, pp.monthlyexpiration, true);
                    }
                    else if (UserID == 0 && LoginAttempt >= pp.attempts && duration > pp.lockouttime)
                    {
                        Data.Add("dur", pp.lockouttime);
                        Data.Add("res", 2);
                        result.Response = Data;
                    }
                    else if (UserID == 0 && (LoginAttempt < pp.attempts))
                    {
                        IUser a = objDb.GetUserByID("SELECT * FROM UM_User WHERE Email='" + ((string)jobj["Email"]).Trim() + "'", CommandType.Text);
                        if (a.Email.Equals((string)jobj["Email"]))
                        {
                            bool blinsertlogin = marcommanager.UserManager.InvalidLoginAttemptDetails((string)jobj["Email"], ipAddress, pp.notifyadmin);
                        }
                        result.Response = null;
                    }
                    else
                    {
                        //NotifyUser
                        Data = marcommanager.UserManager.CheckNotifyUser(pp.notifyuser, (string)jobj["Email"], pp.notifyadmin);
                        result.Response = Data;
                    }
                    #endregion
                }
                else
                {
                    #region softlock with notifyuser and pwd expiry
                    if (UserID != 0 && (LoginAttempt >= pp.attempts || LoginAttempt < pp.attempts) && duration > pp.lockouttime)
                    {
                        PasswordExpiryValidation(pp.expirevalidation, (string)jobj["Email"], UserID, pp.monthlyexpiration, false);
                    }
                    else if (UserID != 0 && (LoginAttempt < pp.attempts))
                    {
                        PasswordExpiryValidation(pp.expirevalidation, (string)jobj["Email"], UserID, pp.monthlyexpiration, false);
                    }
                    else if (LoginAttempt < pp.attempts)
                    {
                        IUser a = objDb.GetUserByID("SELECT * FROM UM_User WHERE Email='" + ((string)jobj["Email"]).Trim() + "'", CommandType.Text);
                        if (a.Email.Equals((string)jobj["Email"]))
                        {
                            bool blinsertlogin = marcommanager.UserManager.InvalidLoginAttemptDetails((string)jobj["Email"], ipAddress, pp.notifyadmin);
                        }
                        result.Response = null;
                    }
                    else
                    {
                        #region NotifyUser
                        if (pp.notifyuser)
                        {
                            JObject Userdetails = JObject.Parse(@"{lockouttime:" + pp.lockouttime + ",Email:'" + (string)jobj["Email"] + "',tenanturl:'"+tenantres.tenanturl+"'}");
                            result.Response = SoftLockNotify(Userdetails);
                        }
                        else
                        {
                            Data.Add("dur", pp.lockouttime);
                            Data.Add("res", 1);
                            result.Response = Data;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/MediaBankValidUser")]
        [HttpPost]
        public ServiceResponse MediaBankValidUser([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());


                if (((string)jobj["UserID"]).Length == 0)
                {
                    throw new InvalidExpressionException();
                }

                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                int UserID = objDb.CheckValidUserID("SELECT ID,Password,PasswordSalt FROM UM_User  WHERE id='" + ((int)jobj["UserID"]) + "'", CommandType.Text, "");
                if (UserID != 0)
                {
                    var Req = System.Web.HttpContext.Current.Request.Browser;
                    string Browsername = Req.Browser;
                    string Version = Req.Version;
                    int MajorVersion = Req.MajorVersion;
                    string MinorVersion = Req.MinorVersionString;
                    string osPlatform = Req.Platform;
                    string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    Guid sessionid = MarcomManagerFactory.CreateSession(UserID, tenantres.tenantID, tenantres.tenanturl);
                    if (sessionid == Guid.Empty)
                    {
                        result.Response = null;
                    }
                    //  Session["ID"] = sessionid;
                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                    result.StatusCode = (int)HttpStatusCode.OK;
                    int userid = marcommanager.User.Id;
                    if (userid > 0)
                    {
                        bool blinsertlogin = marcommanager.UserManager.insetlogin(userid, ipAddress, Browsername, Version, MajorVersion, MinorVersion, osPlatform, false);
                    }
                    result.Response = marcommanager.User;
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/SSO")]
        [HttpPost]
        public ServiceResponse SSO([FromBody]JObject jsonobj)
        {

            result = new ServiceResponse();

            try
            {
                //identify the tenant
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                JObject jobj = JObject.Parse(jsonobj.ToString());
                string useremail = objDb.SSORegisterData(jobj, tenantres.tenanturl);

                if (useremail != null)
                {
                    var Req = System.Web.HttpContext.Current.Request.Browser;
                    string Browsername = Req.Browser;
                    string Version = Req.Version;
                    int MajorVersion = Req.MajorVersion;
                    string MinorVersion = Req.MinorVersionString;
                    string osPlatform = Req.Platform;
                    string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    //ClsDb objDb = new ClsDb();
                    int UserID = objDb.CheckValidUserID("SELECT ID,Password,PasswordSalt FROM UM_User  WHERE Email='" + useremail.Trim() + "'", CommandType.Text, "");
                    if (UserID != 0)
                    {
                        Guid sessionid = MarcomManagerFactory.CreateSession(UserID, tenantres.tenantID, tenantres.tenanturl);
                        //Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                        // Session["ID"] = sessionid;
                        IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,sessionid);
                        // MarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                        result.StatusCode = (int)HttpStatusCode.OK;
                        int userid = marcommanager.User.Id;
                        if (userid > 0)
                        {
                            bool blinsertlogin = marcommanager.UserManager.insetlogin(userid, ipAddress, Browsername, Version, MajorVersion, MinorVersion, osPlatform, true);
                        }
                        IList<dynamic> SsoDetails = new List<dynamic>();
                        SsoDetails.Add(marcommanager.User);
                        SsoDetails.Add(sessionid);
                        result.Response = SsoDetails;
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/User/SecureSAMLSSO")]
        [HttpPost]
        public ServiceResponse SecureSAMLSSO([FromBody]JObject jsonobj)
        {

            result = new ServiceResponse();

            try
            {
                //identify the tenant
                TenantResolver tenantres = new TenantResolver(Request.Headers.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                JObject jobj = JObject.Parse(jsonobj.ToString());
                string useremail = (string)jsonobj["Email"];
                string firstname = (string)jsonobj["firstname"];
                string lastname = (string)jsonobj["lastname"];
                List<string> SAMLRoles = Convert.ToString((string)jsonobj["samlroles"]).Split('@').ToList();

                useremail = objDb.SAMLUserRegistration(tenantres.tenanturl, useremail, firstname, lastname, "", SAMLRoles);

                if (useremail != null)
                {
                    var Req = System.Web.HttpContext.Current.Request.Browser;
                    string Browsername = Req.Browser;
                    string Version = Req.Version;
                    int MajorVersion = Req.MajorVersion;
                    string MinorVersion = Req.MinorVersionString;
                    string osPlatform = Req.Platform;
                    string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    //ClsDb objDb = new ClsDb();
                    int UserID = objDb.CheckValidUserID("SELECT ID,Password,PasswordSalt FROM UM_User  WHERE Email='" + useremail.Trim() + "'", CommandType.Text, "");
                    if (UserID != 0)
                    {
                        Guid sessionid = MarcomManagerFactory.CreateSession(UserID, tenantres.tenantID, tenantres.tenanturl);
                        // Session["ID"] = sessionid;
                        IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,sessionid);
                        result.StatusCode = (int)HttpStatusCode.OK;
                        int userid = marcommanager.User.Id;
                        if (userid > 0)
                        {
                            bool blinsertlogin = marcommanager.UserManager.insetlogin(userid, ipAddress, Browsername, Version, MajorVersion, MinorVersion, osPlatform, true);
                        }
                        marcommanager.User.session = sessionid;
                        result.Response = marcommanager.User;
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        //Forget password functionality
        [Route("api/User/ValidUserForForgotPwd")]
        [HttpPost]
        public ServiceResponse ValidUserForForgotPwd([FromBody]JObject jsonString)
        {
            result = new ServiceResponse();
            try
            {
                JObject jobj = JObject.Parse(jsonString.ToString());
                //Marcom.UIService.Utility.TenantResolver tenantres = new Marcom.UIService.Utility.TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                Guid NewId = Guid.NewGuid();
                Guid FileGuId = NewId;
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = objDb.ForgotPwd("SELECT um.Id as Id ,um.IsSSOUser as IsSSOUser FROM UM_User um  WHERE um.Email='" + (string)jobj["Email"] + "'", CommandType.Text, (string)jobj["Email"], (bool)jobj["IsResetPassword"], tenantres.tenantID, FileGuId);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        [Route("api/User/Resetpwdlink")]
        [HttpPost]
        public bool Resetpwdlink([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {

                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                return objdb.Resetpwdlink("select count(*) as count from UM_ResetPasswordTrack pt WHERE TrackingID='" + (Guid)jobj["Guid"] + "' and DATEDIFF(hh, pt.ResetPasswordDate, GETDATE())<24", CommandType.Text, (string)jobj["UserID"]);

            }
            catch
            {

            }
            return false;
        }
        [Route("api/User/IsUserExistsForSetpassword")]
        [HttpPost]
        public bool IsUserExistsForSetpassword([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                return objdb.IsUserExistsForSetpassword("select count(*) as count from UM_SetUpToolUsersRegister pt WHERE TrackingID='" + (Guid)jobj["Guid"] + "' and setpassword=1", CommandType.Text);
            }
            catch
            {

            }
            return false;
        }
        [Route("api/User/UpdateSetPassword")]
        [HttpPost]
        public ServiceResponse UpdateSetPassword([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                result.Response = objdb.UpdateSetpassword("update UM_User set password=@password,PasswordSalt=@saltpassword from UM_User WHERE ID=@userID ; update UM_SetUpToolUsersRegister set SetPassword=1 where UserID=@userID", CommandType.Text, (string)jobj["Guid"], (string)jobj["Password"], (string)jobj["OldPassword"], true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetUserById/{ID}")]
        [HttpGet]
        public ServiceResponse GetUserById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.User_SelectByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/Getstartpages")]
        [HttpGet]
        public ServiceResponse Getstartpages()
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetStartpages();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }

            return result;
        }
        [Route("api/User/AutoCompleteMemberList")]
        [HttpPost]
        public ServiceResponse AutoCompleteMemberList([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //   result.Response = marcommanager.UserManager.GetMemberList(System.Uri.UnescapeDataString(QueryString), GroupID);
                result.Response = marcommanager.UserManager.GetAutoCompleteMemberList(System.Uri.UnescapeDataString((string)jsonobj["QueryString"]));
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/GetUsers")]
        [HttpGet]
        public ServiceResponse GetUsers()
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetUsers();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/GetPendingUsers")]
        [HttpGet]
        public ServiceResponse GetPendingUsers()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetPendingUsers();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/ApproveRejectRegisteredUsers")]
        [HttpPost]
        public ServiceResponse ApproveRejectRegisteredUsers([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject jobj = JObject.Parse(jsonobj.ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.UpdateUsersToRegister((string)jobj["checkedusers"], (bool)jobj["status"], '1');
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetUserByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetUserByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetUserByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/GetMembersByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetMembersByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetMembersByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/UserDateTime")]
        [HttpGet]
        public ServiceResponse UserDateTime()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.UserDateTime();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/IsUserSesstionPresent")]
        [HttpGet]
        public ServiceResponse IsUserSesstionPresent()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = true;
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = ex.Message + ex.StackTrace;
            }
            return result;
        }
        [Route("api/User/UpdateUserByColumn")]
        [HttpPost]
        public ServiceResponse UpdateUserByColumn([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.Userinfo_UpdateByColumn((string)jobj["ColumnName"], (string)jobj["ColumnValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/SaveUserImage")]
        [HttpPost]
        public ServiceResponse SaveUserImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.SaveUserImage((string)jobj["imgsourcepath"], (int)jobj["imgwidth"], (int)jobj["imgheight"], (int)jobj["imgX"], (int)jobj["imgY"], (string)jobj["fileName"], (string)jobj["fileExt"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetAllObjectiveMembers/{EntityTypeId}")]
        [HttpGet]
        public ServiceResponse GetAllObjectiveMembers(int EntityTypeId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetAllObjectiveMembers(EntityTypeId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/UpdateNewLanguageType")]
        [HttpPost]
        public ServiceResponse UpdateNewLanguageType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.UpdateNewLanguageType((int)jobj["UserID"], (int)jobj["newlangid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/IsSSOUser")]
        [HttpGet]
        public ServiceResponse IsSSOUser()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.IsSSOUser();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/Logout")]
        [HttpGet]
        public ServiceResponse Logout()
        {
            result = new ServiceResponse();
            try
            {
                MarcomManagerFactory.EndSession(Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/Resetpassword")]
        [HttpPost]
        public ServiceResponse Resetpassword([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                result.Response = objdb.pwdreset("update UM_User set password=@password,PasswordSalt=@saltpassword WHERE ID=@userID", CommandType.Text, (string)jobj["Guid"], (string)jobj["Password"], (string)jobj["OldPassword"], (bool)jobj["Flag"], false, tenantres.tenantID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/Forgetpassword")]
        [HttpPost]
        public ServiceResponse Forgetpassword([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                result.Response = objdb.pwdreset("update UM_User set password=@password,PasswordSalt=@saltpassword WHERE ID=@userID", CommandType.Text, (string)jobj["Guid"], (string)jobj["Password"], (string)jobj["OldPassword"], (bool)jobj["Flag"], true, tenantres.tenantID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetAPIusersDetails")]
        [HttpGet]
        public ServiceResponse GetAPIusersDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetAPIusersDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/User/GenerateGuidforSelectedAPI")]
        [HttpPost]
        public ServiceResponse GenerateGuidforSelectedAPI([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject jobj = JObject.Parse(jsonobj.ToString());
                JArray arrAPIList = (JArray)jobj["APISelected"];
                int[] APIuserIDs = arrAPIList.Select(jv => (int)jv["UserID"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GenerateGuidforSelectedAPI(APIuserIDs);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/InsertUserRegisterDynamicData")]
        [HttpPost]
        public ServiceResponse InsertUserRegisterDynamicData([FromBody]JArray jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                //  JArray jobj = JArray.Parse(jsonobj.ToString());
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = objdb.InsertUserRegisterDynamicData(jsonobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/User/SetupToolUserRegister")]
        [HttpGet]
        public ServiceResponse SetupToolUserRegister()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = objDb.SetUpToolUserRegister(tenantres.tenanturl);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/CalendarValidUser")]
        [HttpPost]
        public ServiceResponse CalendarValidUser([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();

            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                if (((string)jobj["UserID"]).Length == 0)
                {
                    throw new InvalidExpressionException();
                }

                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                int UserID = ((int)jobj["UserID"]);
                if (UserID != 0)
                {
                    var Req = System.Web.HttpContext.Current.Request.Browser;
                    string Browsername = Req.Browser;
                    string Version = Req.Version;
                    int MajorVersion = Req.MajorVersion;
                    string MinorVersion = Req.MinorVersionString;
                    string osPlatform = Req.Platform;
                    string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    Guid sessionid = MarcomManagerFactory.CreateSession(UserID, tenantres.tenantID, tenantres.tenanturl);
                    if (sessionid == Guid.Empty)
                    {
                        result.Response = null;
                        return null;
                    }
                    // Session["ID"] = sessionid;
                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                    result.StatusCode = (int)HttpStatusCode.OK;
                    int userid = marcommanager.User.Id;
                    if (userid > 0)
                    {
                        bool blinsertlogin = marcommanager.UserManager.insetlogin(userid, ipAddress, Browsername, Version, MajorVersion, MinorVersion, osPlatform, false);
                    }
                    result.Response = marcommanager.User;
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/User/getdimensionunit")]
        [HttpGet]
        public ServiceResponse getdimensionunit()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.getdimensionunit();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetDimensionunitsettings")]
        [HttpGet]
        public ServiceResponse GetDimensionunitsettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetDimensionunitsettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/User/GetTenantPath")]
        [HttpGet]
        public ServiceResponse GetTenantPath()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,(Guid)systemSession);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetTenantFilePath(tenantres.tenantID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/User/GetTenantXMLFile")]
        [HttpGet]
        public ServiceResponse GetTenantXMLFile()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,(Guid)systemSession);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetTenantXMLFile(tenantres.tenantID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/User/GetTitleLogoBGImage")]
        [HttpGet]
        public ServiceResponse GetTitleLogoBGImage()
        {


            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = objDb.GetLoginTitleLogoBGImage(tenantres.tenantID);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        [Route("api/User/LockResetpassword")]
        [HttpPost]
        public ServiceResponse LockResetpassword([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {
                JObject jobj = JObject.Parse(jsonobj.ToString());
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + tenantres.tenantfilepath, "AdminSettings.xml");
                XDocument adminXmlDoc = XDocument.Load(xmlpath);
                string xelementName = "LoginandPassword";
                var xelementFilepath = XElement.Load(xmlpath);
                var xmlElement = xelementFilepath.Element(xelementName);
                string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                IList<PasswordSetting> pplist = new List<PasswordSetting>();
                PasswordSetting pp = new PasswordSetting();
                foreach (var des in xmlElement.Descendants())
                {
                    switch (des.Name.ToString())
                    {
                        case "passwordrepeated_months":
                            pp.monthlyrepetition = Convert.ToInt32(des.Value.ToString());
                            break;
                        case "passwordrepeated":
                            pp.repetitiontime = Convert.ToInt32(des.Value.ToString());
                            break;
                        default:
                            break;
                    }
                }
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,(Guid)systemSession);
                ClsDb objdb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                IUser prevattempts = objdb.GetEmailByID("Select Email from UM_LoginMails where Captcha ='" + ((string)jsonobj["OldPassword"]) + "'", CommandType.Text);
                string userEmail = (prevattempts == null) ? "" : prevattempts.Email;
                int pwdcount = objdb.GetPasswordCountByPwd("SELECT UserID,Password,PasswordSalt FROM UM_InsertUserDetails  WHERE Email='" + userEmail + "'and DATEDIFF(month,GetDate(),CreationTime)<='" + pp.monthlyrepetition + "'", CommandType.Text, (string)jobj["Password"]);
                if (pwdcount <= pp.repetitiontime)
                {
                    result.Response = objdb.lockpwdreset("update UM_User set password=@password,PasswordSalt=@saltpassword WHERE ID=@userID", CommandType.Text, (string)jobj["Guid"], (string)jobj["Password"], (string)jobj["OldPassword"], (bool)jobj["Flag"], false, tenantres.tenantID);
                }
                else
                {
                    result.Response = "exists";
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/User/SendPasswordResetMail")]
        [HttpPost]
        public ServiceResponse SendPasswordResetMail([FromBody]JObject jsonobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject jobj = JObject.Parse(jsonobj.ToString());
                JArray arrUserList = (JArray)jobj["SelectedUser"];
                string[] APIuserMails = arrUserList.Select(jv => (string)jv["Email"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                foreach (var mail in APIuserMails)
                {
                    //result.Response = marcommanager.UserManager.SendPasswordResetMail(mail.ToString());
                    result.Response = marcommanager.UserManager.ValidLoginAfterInvalid(mail.ToString());
                }
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GetLockuserDetails")]
        [HttpGet]
        public ServiceResponse GetLockuserDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetLockeduserDetails();
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        [Route("api/User/GetusermailbyGuid")]
        [HttpPost]
        public ServiceResponse GetusermailbyGuid([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,systemSession);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.GetusermailbyGuid((Guid)jobj["Guid"]);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        [Route("api/User/GetApprovalrole")]
        [HttpGet]
        public ServiceResponse GetApprovalrole()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.UserManager.GetApprovalrole();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/User/InsertApprovalUserRoles")]
        [HttpPost]
        public ServiceResponse InsertApprovalUserRoles([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                if ((jobj["UserID"] != null) && (jobj["ApprovalroleId"] != null) && marcomManager.UserManager.DeleteApprovalRoleUser((int)jobj["UserID"]) == true)
                {
                    JArray jobjApprovalRole = (JArray)jobj["ApprovalroleId"];
                    result.StatusCode = (int)HttpStatusCode.OK;
                    foreach (var val in jobjApprovalRole)
                    {
                        result.Response = marcomManager.UserManager.InsertApprovalRoleUser((int)val, (int)jobj["UserID"]);
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/User/GetApprovalRoleUserByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetApprovalRoleUserByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.UserManager.GetApprovalRoleUserByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        // Password Expiration Validation Checking based on User Requirements(In Months)
        public ServiceResponse PasswordExpiryValidation(bool expirevalidation, string UserEmail, int UserID, int expirationMonth, bool isLock)
        {
            int m_expire = 0;
            result = new ServiceResponse();
            TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
            ClsDb objDb = new ClsDb(tenantres.tenanturl);
            Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,systemSession);
            if (expirevalidation)
            {
                PasswordSetting months = objDb.GetMinuteByEmail("SELECT TOP 1 DATEDIFF(month,CreationTime,Getdate()) as LoginTime from UM_InsertUserDetails where Email ='" + UserEmail + "' ORDER BY CreationTime DESC; ", CommandType.Text);
                m_expire = (months == null) ? 0 : (int)months.Duration;
                if (expirationMonth == m_expire || expirationMonth < m_expire)
                {
                    IUser mailsent = objDb.GetEmailByID("Select Email from UM_LoginMails where Email ='" + UserEmail + "' and Captcha='expired'", CommandType.Text);
                    string mailid = (mailsent == null) ? "" : mailsent.Email;
                    if (mailid.Equals(""))
                    {
                        bool notifypwdreset = marcommanager.UserManager.UserNotificationMail(UserEmail, "expired");
                        result.Response = "expired";
                    }
                    else
                    {
                        //already mail sent
                        result.Response = "expired";
                    }
                }
                else
                {
                    result.Response = marcommanager.UserManager.CreateSession(UserID, tenantres.tenanturl, tenantres.tenantID, UserEmail);
                }
                result.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                result.Response = marcommanager.UserManager.CreateSession(UserID, tenantres.tenanturl, tenantres.tenantID, UserEmail);
            }
            return result;
        }

        [Route("api/User/GetDalimUser")]
        [HttpGet]
        public ServiceResponse GetDalimUser()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.UserManager.GetDalimUser();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/User/SaveUpdateDalimUser")]
        [HttpPost]
        public ServiceResponse SaveUpdateDalimUser([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.UserManager.SaveUpdateDalimUser((string)jobj["Email"], (string)jobj["Password"], (int)jobj["UserID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/User/DeleteDalimUser/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteDalimUser(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.DeleteDalimUser(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed; ;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/Updateadditionalsettingsstartpage")]
        [HttpPost]
        public ServiceResponse Updateadditionalsettingsstartpage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.UserManager.Updateadditionalsettingsstartpage((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/User/GettingAccountLocalXMLValues")]
        [HttpPost]
        public PasswordSetting GettingAccountLocalXMLValues()
        {
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,systemSession);
                return marcommanager.UserManager.GettingAccountLocalXMLValues();
            }
            catch
            {
                return null;
            }
        }
        [Route("api/User/SoftLockNotify")]
        [HttpPost]
        public JObject SoftLockNotify(JObject jobj)
        {
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,systemSession);
                return marcommanager.UserManager.SoftLockNotify(jobj);
            }
            catch
            {
                return null;
            }
        }
        [Route("api/User/GetUserRegLogoBGImage")]
        [HttpGet]
        public ServiceResponse GetUserRegLogoBGImage()
        {


            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = objDb.GetUserRegLogoBGImage(tenantres.tenantID);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        [Route("api/User/GetUserRegLogoDetails")]
        [HttpGet]
        public ServiceResponse GetUserRegLogoDetails()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                ClsDb objDb = new ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = objDb.GetUserRegLogoDetails(tenantres.tenantID);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
    }
}