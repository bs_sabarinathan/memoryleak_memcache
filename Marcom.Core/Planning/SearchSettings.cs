﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net;
using Lucene.Net.Util;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Store;
using System.IO;
using System.Configuration;

namespace BrandSystems.Marcom.Core.Planning
{
    public class SearchSettings
    {

        private static SearchSettings objSearchSettings = new SearchSettings();
        public static SearchSettings Instance
        {
            get { return objSearchSettings; }
        }

        private string strIndexFolderLocation = ConfigurationManager.AppSettings["MarcomPresentation"].ToString()  + "SearchIndexFiles\\LuceneIndex";
        public string IndexFolderLocation
        {
            get { return strIndexFolderLocation + "\\Index"; }
            set { strIndexFolderLocation = value; }
        }

        private string strSearchIndexFolderLocation = ConfigurationManager.AppSettings["MarcomPresentation"].ToString()  + "SearchIndexFiles\\LuceneIndex";
        public string SearchIndexFolderLocation
        {
            get { return strSearchIndexFolderLocation + "\\SearchIndex"; }
            set { strSearchIndexFolderLocation = value; }
        }

        private string strTempIndexFolderLocation = ConfigurationManager.AppSettings["MarcomPresentation"].ToString()  + "SearchIndexFiles\\LuceneIndex";
        public string TempIndexFolderLocation
        {
            get { return strTempIndexFolderLocation + "\\Index"; }
            set { strTempIndexFolderLocation = value; }
        }

        private string strAutoCompleteIndexFolderLocation = ConfigurationManager.AppSettings["MarcomPresentation"].ToString()  + "SearchIndexFiles\\LuceneIndex";
        public string AutoCompleteIndexFolderLocation
        {
            get { return strAutoCompleteIndexFolderLocation + "\\Index"; }
            set { strAutoCompleteIndexFolderLocation = value; }
        }

        private string strAutoCompleteTempIndexFolderLocation = ConfigurationManager.AppSettings["MarcomPresentation"].ToString()  + "SearchIndexFiles\\LuceneIndex";
        public string AutoCompleteTempIndexFolderLocation
        {
            get { return strAutoCompleteTempIndexFolderLocation + "\\Index"; }
            set { strAutoCompleteTempIndexFolderLocation = value; }
        }

        private string strBaseFolderLocation;
        public string BaseFolderLocation
        {
            get { return strBaseFolderLocation; }
            set { strBaseFolderLocation = value; }
        }

        private int intPageSize;
        public int PageSize
        {
            get { return intPageSize; }
            set { intPageSize = value; }
        }

        private int intMaxResultForAutoComplete;
        public int MaxResultForAutoComplete
        {
            get { return intMaxResultForAutoComplete; }
            set { intMaxResultForAutoComplete = value; }
        }
        

        public FSDirectory GetIndexFileDirectory()
        {
            if (!System.IO.Directory.Exists(IndexFolderLocation))
            {
                System.IO.Directory.CreateDirectory(IndexFolderLocation);
            }

            return FSDirectory.Open(new DirectoryInfo(IndexFolderLocation));
        }

        public FSDirectory GetSearchIndexFileDirectory()
        {
            if (!System.IO.Directory.Exists(SearchIndexFolderLocation))
            {
                System.IO.Directory.CreateDirectory(SearchIndexFolderLocation);
            }

            return FSDirectory.Open(new DirectoryInfo(SearchIndexFolderLocation));
        }


        public FSDirectory GetTempIndexFileDirectory()
        {
            if (!System.IO.Directory.Exists(TempIndexFolderLocation))
            {
                System.IO.Directory.CreateDirectory(TempIndexFolderLocation);
            }

            return FSDirectory.Open(new DirectoryInfo(TempIndexFolderLocation));
        }

        public FSDirectory GetAutoCompleteIndexFileDirectory()
        {
            if (!System.IO.Directory.Exists(AutoCompleteIndexFolderLocation))
            {
                System.IO.Directory.CreateDirectory(AutoCompleteIndexFolderLocation);
            }

            return FSDirectory.Open(new DirectoryInfo(AutoCompleteIndexFolderLocation));
        }

        public FSDirectory GetAutoCompleteTempIndexFileDirectory()
        {
            if (!System.IO.Directory.Exists(AutoCompleteTempIndexFolderLocation))
            {
                System.IO.Directory.CreateDirectory(AutoCompleteTempIndexFolderLocation);
            }

            return FSDirectory.Open(new DirectoryInfo(AutoCompleteTempIndexFolderLocation));
        }

        public Analyzer GetAnalyzer()
        {
            return new CustomAnalyzer();
        }
    }
}
