///#source 1 1 /app/controllers/mui/planningtool/calender/detail/section-controller.js
(function (ng, app) {
    function muiplanningtoolcalenderdetailsectionCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, CalendersectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        $scope.TabCollectionsList = null;
        $scope.IsLock = true;
        $scope.EntityLockTask = false;
        
        $scope.set_bgcolor = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            }
            else {
                return '';
            }
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            }
            else {
                return '';
            }
        }
        $scope.load = function (parameters) {
            $scope.LoadTab = function (tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                // Put the object into storage
                localStorage.setItem('lasttab', tab);
                $location.path('/mui/calender/detail/section/' + $stateParams.ID + '/' + tab + '');
            };
        };
        $scope.CustomTabLoad = function (tab, tabUrl, IsSytemDefined, Name, AddEntityID) {
            if (IsSytemDefined == false) {
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                CalendersectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function (gettabresult) {
                    if (gettabresult.Response != null) {
                        $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                    }
                });
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                $location.path('/mui/calender/customtab/' + tab + '/' + $stateParams.ID + '');
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }
        }
        $scope.BreadCrumOverview = function (ID) {
            var actname = $('#breadcrumlink').text();
            actname = actname.substring(0, actname.length);
            $window.EntityTypeID = ID;
            $window.SubscriptionEntityTypeID = $('#breadcrumlink').attr('data-typeid');
            $window.GlobalUniquekey = $('#breadcrumlink').attr('data-uniquekey');
            $scope.RootLevelEntityName = actname;
            $scope.EntityShortText = $('#breadcrumlink').attr('data-shortdesc');
            $scope.EntityColorcode = $('#breadcrumlink').attr('data-colorcode');
            $location.path('/mui/calender/detail/section/' + ID);
            CalendersectionService.GetPath(ID).then(function (getbc) {
                $scope.array = getbc.Response;
            });
            $('#CalenderOverviewDiv').trigger('onObjOverViewDetail', ID);
        }
        $scope.loadBreadCrum = function (isOnload) {
            $("#plandetailfilterdiv").css('display', 'none')
            if ($("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").length == 0) {
                $scope.loadobjectivefromsearchByID($stateParams.ID, "100");
            }
            $("#EntitiesTree li.active").removeClass('active')
            $("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
            $scope.array = [];
            CalendersectionService.GetPath($stateParams.ID).then(function (getbc) {
                for (var i = 0; i < getbc.Response.length; i++) {
                    if (i == (getbc.Response.length - 1)) {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        var decodedEntityShortText = $('<div/>').html(getbc.Response[i].ShortDescription).text();
                        var decodedEntityBreadCrumTypeName = $('<div/>').html(getbc.Response[i].TypeName).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": getbc.Response[i].Name,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "999999"
                        });
                        $scope.EntityShortText = getbc.Response[i].ShortDescription;
                        $scope.EntityBreadCrumTypeName = getbc.Response[i].TypeName;
                        $scope.EntityColorcode = getbc.Response[i].ColorCode;
                        $scope.RootLevelEntityName = getbc.Response[i].Name;
                        $window.GlobalUniquekey = getbc.Response[i].UniqueKey;
                        $window.SubscriptionEntityTypeID = getbc.Response[i].TypeID;
                        $window.SubscriptionEntityID = getbc.Response[i].ID;
                        $scope.EntityTypeID = getbc.Response[i].TypeID;
                        if (getbc.Response[i].IsLock == "True") $scope.IsLock = true;
                        else $scope.IsLock = false;
                        $(window).trigger('LoadSubscription', $stateParams.ID);
                    } else {
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": getbc.Response[i].Name,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "0088CC"
                        });
                    }
                }
                if (isOnload == false) {
                    if ($scope.subview == "overview") {
                        $('#CalenderOverviewDiv').trigger('onCalOverViewDetail', $stateParams.ID);
                    } else if ($scope.subview == "member") {
                        $('#membersDivObj').trigger('CalLoadMembersDetailObj', $stateParams.ID);
                    } else if ($scope.subview == "attachment") {
                        $('#attachmentDivObj').trigger('LoadAttachmentObj', $stateParams.ID);
                    } else if ($scope.subview == "presentation") {
                        $('#PresentationObj').trigger('LoadPresentationsObj', $stateParams.ID);
                    } else if ($scope.subview == "task") {
                        ResetTaskObjects();
                        $scope.$broadcast('LoadTask', $stateParams.ID);
                    }
                }
            });
        }
        $scope.DecodedTaskName = function (Name) {
            if (Name != null & Name != "") {
                return $('<div />').html(Name).text();
            } else {
                return "";
            }
        };
        $scope.loadBreadCrum(true);
        $scope.TaskExpandedDetails = [];
        $scope.SubLevelTaskExpandedDetails = [];
        $scope.SubLevelTaskLibraryListContainer = {
            SubLevelTaskLibraryList: []
        };
        $scope.RootLevelTaskLibraryListContainer = {
            RootLevelTaskLibraryList: []
        };
        $scope.groupbySubLevelTaskObj = [];
        $scope.ngExpandAll = true;
        $scope.ngExpandCollapseStatus = {
            ngExpandAll: true
        };
        $scope.TaskOrderListObj = {
            TaskSortingStatus: "Reorder task lists",
            TaskSortingStatusID: 1,
            TaskOrderHandle: false,
            SortOrderObj: [],
            UniqueTaskSort: "Reorder task",
            UniqueTaskSortingStatusID: 1,
            UniqueTaskOrderHandle: false,
            UniqueSortOrderObj: []
        };
        $scope.SubLevelEnableObject = {
            Enabled: false,
            SublevelText: "Show task(s) from sub-levels",
            SublevelTextStatus: 1
        };
        

        function GetAllTabCollections() {
            CalendersectionService.GetCustomEntityTabsByTypeID(35).then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function (item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID);
                    }
                }
            });
        }
        $scope.BindClass = function (index, tab, ID) {
            if (index == 0 && $scope.subview == null) {
                if ($scope.subview == null) {
                    var tabid = $scope.TabCollectionsList[index].ControleID;
                    $scope.subview = tabid;
                }
                return "active";
            } else if ($scope.subview == tab) {
                return "active";
            } else if (ID == $stateParams.tabID) {
                return "active";
            }
        }
        

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function (event, taskkist) {
            if (taskkist.length > 0) {
                $scope.MainTaskLiveUpdateIndex = 0;
                $scope.subTaskLiveUpdateIndex = 0;
                UpdateTaskData();
                UpdateSubLevelTaskData();
            }
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.MainTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.TaskExpandedDetails, function (e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var taskListResObj = $.grep($scope.groups, function (e) {
                            return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                        });
                        if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                            if (tasklistList[0].TaskList.length > 0) {
                                taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                            }
                        }
                    } catch (e) { }
                }
                $timeout(function () {
                    $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                    if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateTaskData();
                }, 20);
            });
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function (e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function (e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function (e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) { }
                }
                $timeout(function () {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detail.sectionCtrl']"));
        });
        $timeout(function () {
            $scope.load();
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
        $(window).on("UpdateCalenderEntityName", function (event, optionarray) {
            $scope.RootLevelEntityName = optionarray.toString();
        });

        GetAllTabCollections();

        CalendersectionService.GetLockStatus($stateParams.ID).then(function (entitylockstatus) {
            $scope.IsLock = entitylockstatus.Response.m_Item1;
        });

        ResetTaskObjects();
    }
    app.controller("mui.planningtool.calender.detail.sectionCtrl", ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'CalendersectionService', muiplanningtoolcalenderdetailsectionCtrl]);
})(angular, app);
///#source 1 1 /app/services/calendersection-service.js
app.service('CalendersectionService', function ($http, $q) {
    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ GetPath:GetPath,GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation, GetCustomTabUrlTabsByTypeID: GetCustomTabUrlTabsByTypeID, GetCustomEntityTabsByTypeID: GetCustomEntityTabsByTypeID, GetLockStatus: GetLockStatus, GetDataForBreadcrumLoadWithLocalWorkSpacePath: GetDataForBreadcrumLoadWithLocalWorkSpacePath, GetDataForBreadcrumLoadWithPath: GetDataForBreadcrumLoadWithPath }); function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetCustomTabUrlTabsByTypeID(tabID, entityID) { var request = $http({ method: "get", url: "api/common/GetCustomTabUrlTabsByTypeID/" + tabID + "/" + entityID, params: { action: "get", }, }); return (request.then(handleSuccess, handleError)); }
    function GetCustomEntityTabsByTypeID(TypeID, EntityTypeID, EntityID, CalID) {
        if (arguments.length == 1) { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID, params: { action: "get", } }); } else { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID + "/" + CalID + "/" + EntityTypeID + "/" + EntityID, params: { action: "get", }, }); }
        return (request.then(handleSuccess, handleError));
    }
    function GetPath(EntityID) { var request = $http({ method: "get", url: "api/Metadata/GetPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetLockStatus(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetLockStatus/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDataForBreadcrumLoadWithLocalWorkSpacePath(EntityID, IsWorkspace) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithLocalWorkSpacePath/" + EntityID + "/" + IsWorkspace, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDataForBreadcrumLoadWithPath(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
        return ($q.reject(response.data.message));
    }
    function handleSuccess(response) { return (response.data); }
});
