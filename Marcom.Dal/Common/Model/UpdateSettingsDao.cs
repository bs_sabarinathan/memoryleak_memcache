using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
    /// [MarcomUpdateDetails]
	/// </summary>

    public partial class UpdateSettingsDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected string _version;
        protected string _features;
        protected string _status;
        protected DateTime _date;
        protected string _license;
        protected bool _currentversion;
        protected int _versionid;
        protected int _instanceid;
        protected int _memberid;
        protected string _username;
		
		#endregion
		
		#region Constructors
		public UpdateSettingsDao() {}

        public UpdateSettingsDao(string pVersion, string pFeatures, string pStatus, DateTime pDate, string pLicense, bool pCurrentVersion, int pVersionId, int pInstanceId, int pMemberid, string Pusername)
		{
            this._version = pVersion;
            this._features = pFeatures;
            this._status = pStatus;
            this._date = pDate;
            this._license = pLicense;
            this._currentversion = pCurrentVersion;
            this._versionid = pVersionId;
            this._instanceid = pInstanceId;
            this._memberid = pMemberid;
            this._username = Pusername;
		}
        public UpdateSettingsDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public virtual string Version
		{
			get { return _version; }
			set 
			{
                _version = value; 
			}
		}

        public virtual string Features
		{
			get { return _features; }
			set 
			{
                _features = value; 
			}
		}

        public virtual string Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }

        public virtual DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
            }
        }

        public virtual string License
        {
            get { return _license; }
            set
            {
                _license = value;
            }
        }

        public virtual bool CurrentVersion
        {
            get { return _currentversion; }
            set
            {
                _currentversion = value;
            }
        }

        public virtual int VersionId
        {
            get { return _versionid; }
            set
            {
                _versionid = value;
            }
        }
        public virtual int InstanceId
        {
            get { return _instanceid; }
            set
            {
                _instanceid = value;
            }
        }

        public virtual int MemberId
        {
            get { return _memberid; }
            set
            {
                _memberid = value;
            }
        }
       
        public virtual string username
        {
            get { return _username; }
            set
            {
                _username = value;
            }
        }
               
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string UpdateSettingsDao = "UpdateSettingsDao";			
			public const string Id = "Id";
            public const string Version = "Version";
            public const string Features = "Features";
            public const string Status = "Status";
            public const string Date = "Date";
            public const string Licence = "Licence";
            public const string CurrentVersion = "CurrentVersion";			
            public const string VersionId = "VersionId";
            public const string InstanceId = "InstanceId";
            public const string MemberId = "MemberId";
            public const string username = "username";

		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string UpdateSettingsDao = "UpdateSettingsDao";
            public const string Id = "Id";
            public const string Version = "Version";
            public const string Features = "Features";
            public const string Status = "Status";
            public const string Date = "Date";
            public const string Licence = "Licence";
            public const string CurrentVersion = "CurrentVersion";				
            public const string VersionId = "VersionId";
            public const string InstanceId = "InstanceId";
            public const string MemberId = "MemberId";
            public const string username = "username";
		}

		#endregion
		
		
	}
	
}
