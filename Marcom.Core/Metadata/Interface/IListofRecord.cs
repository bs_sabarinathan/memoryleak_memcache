﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Metadata.Interface;
using System.Collections;

namespace BrandSystems.Marcom.Metadata.Interface
{
     public interface IListofRecord
    {
         IList<IAttribute> Attributes { get; set; }
         IList Data { get; set; }
         IList<AttributeSettings> ColumnDefs { get; set; }
         int DataCount { get; set; }
         Tuple<ArrayList, ArrayList> GeneralColumnDefs { get; set; }
    }
}