using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
    /// ValidationDao object for table 'MM_Validation'.
	/// </summary>
	
	public partial class ValidationDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected int _entitytypeid;
        protected int _relationshipid;
        protected string _name;
        protected string _valuetype;
        protected string _errormessage;
        protected string _value;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ValidationDao() {}

        public ValidationDao(int pId, int pentitytypeid, int prelationshipid, string pname, string pvaluetype, string perrormessage, string pvalue)
		{
			this._id = pId; 
            this._name = pname;
            this._entitytypeid = pentitytypeid;
            this._relationshipid = prelationshipid;
            this._valuetype = pvaluetype;
            this._errormessage = perrormessage;
            this._value = pvalue;
		}

        public ValidationDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int EntityTypeID
		{
            get { return _entitytypeid; }
			set 
			{
                _bIsChanged |= (_entitytypeid != value);
                _entitytypeid = value; 
			}
			
		}

        public virtual int RelationShipID
        {
            get { return _relationshipid; }
            set
            {
                _bIsChanged |= (_relationshipid != value);
                _relationshipid = value;
            }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual string ValueType
        {
            get { return _valuetype; }
            set
            {
                _bIsChanged |= (_valuetype != value);
                _valuetype = value;
            }

        }

        public virtual string ErrorMessage
        {
            get { return _errormessage; }
            set
            {
                _bIsChanged |= (_errormessage != value);
                _errormessage = value;
            }

        }

        public virtual string Value
        {
            get { return _value; }
            set
            {
                _bIsChanged |= (_value != value);
                _value = value;
            }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string ValidationDao = "ValidationDao";			
			public const string Id = "Id";
            public const string EntityTypeID = "EntityTypeID";
            public const string RelationShipID = "RelationShipID";
            public const string Name = "Name";
            public const string ValueType = "ValueType";
            public const string ErrorMessage = "ErrorMessage";
            public const string Value = "Value";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string ValidationDao = "ValidationDao";
            public const string Id = "Id";
            public const string EntityTypeID = "EntityTypeID";
            public const string RelationShipID = "RelationShipID";
            public const string Name = "Name";
            public const string ValueType = "ValueType";
            public const string ErrorMessage = "ErrorMessage";
            public const string Value = "Value";
		}

		#endregion
		
		
	}
	
}
