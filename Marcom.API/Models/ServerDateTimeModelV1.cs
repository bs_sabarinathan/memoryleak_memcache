﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marcom.API.Models
{
    public class ServerDateTimeModelV1
    {

        #region "Member Variables"

        private TimeFormat time;

        private Formatted formatted;

        #endregion

        #region "Members"

        public TimeFormat Time
        {
            get { return time; }
            set { time = value; }
        }

        public Formatted Formatted
        {
            get { return formatted; }
            set { formatted = value; }
        }

        #endregion


    }
}