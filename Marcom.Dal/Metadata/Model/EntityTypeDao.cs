using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// EntityTypeDao object for table 'MM_EntityType'.
	/// </summary>
	
	public partial class EntityTypeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _description;
		protected int _moduleid;
		protected bool _issystemdefined;
        //protected int _parententitytypeId;
        protected string _shortdescription;
        protected string _colorcode;
        protected bool _isassociate;
        //protected int _group;
        //protected int? _attributesetid;
        protected int _category;
        protected int? _workFlowID;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected bool _isrootlevel;
		#endregion
		
		#region Constructors
		public EntityTypeDao() {}

        public EntityTypeDao(string pCaption, string pDescription, int pModuleid, int pCategory, string pShortDescription, string pColorCode, bool pIsAssociate, int pWorkFlowID, bool pIsRootLevel)
		{
			this._caption = pCaption; 
			this._description = pDescription; 
			this._moduleid = pModuleid;
            //this._issystemdefined = pSysDefined;
            this._category = pCategory;
            //this._parententitytypeId = pParentEntityTypeId;
            this._shortdescription = pShortDescription;
            this._colorcode = pColorCode;
            this._isassociate = pIsAssociate;
            this._workFlowID = pWorkFlowID;
            //this._group = pGroup; 
            //this._attributesetid = pAttributeSetid; 
            this._isrootlevel = pIsRootLevel;
		}
				
		public EntityTypeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}
		
		public virtual int ModuleID
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}


        public virtual bool IsSystemDefined
        {
            get { return _issystemdefined; }
            set { _bIsChanged |= (_issystemdefined != value); _issystemdefined = value; }

        }
        //public virtual int ParentEntityTypeId
        //{
        //    get { return _parententitytypeId; }
        //    set { _bIsChanged |= (_parententitytypeId != value); _parententitytypeId = value; }

        //}

        public virtual string ShortDescription
        {
            get { return _shortdescription; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("ShortDescription", "Caption value, cannot contain more than 50 characters");
                _bIsChanged |= (ShortDescription != value);
                _shortdescription = value;
            }

        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _colorcode = value; }
        }

        public virtual bool IsAssociate
        {
            get { return _isassociate; }
            set { _bIsChanged |= (_isassociate != value); _isassociate = value; }

        }
        //public virtual int Group
        //{
        //    get { return _group; }
        //    set { _bIsChanged |= (_group != value); _group = value; }
			
        //}
		
        //public virtual int? AttributeSetid
        //{
        //    get { return _attributesetid; }
        //    set { _bIsChanged |= (_attributesetid != value); _attributesetid = value; }
			
        //}
        public virtual int Category
        {
            get { return _category; }
            set { _bIsChanged |= (_category != value); _category = value; }

        }

        public virtual int? WorkFlowID
        {
            get { return _workFlowID; }
            set { _bIsChanged |= (_workFlowID != value); _workFlowID = value; }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

        public virtual bool IsRootLevel
        {
            get { return _isrootlevel; }
            set { _bIsChanged |= (_isrootlevel != value); _isrootlevel = value; }
        }
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string EntityTypeDao = "EntityTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string Moduleid = "Moduleid";			
			public const string IsSystemDefined = "IsSystemDefined";
            //public const string Group = "Group";			
            //public const string AttributeSetid = "AttributeSetid";	
            public const string Category = "Category";
            //public const string ParentEntityTypeId = "ParentEntityTypeId";
            public const string ShortDescription = "ShortDescription";
            public const string ColorCode = "ColorCode";
            public const string IsAssociate = "IsAssociate";
            public const string WorkFlowID = "WorkFlowID";
            public const string IsRootLevel = "IsRootLevel";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string EntityTypeDao = "EntityTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string Moduleid = "Moduleid";			
			public const string IsSystemDefined = "IsSystemDefined";			
            //public const string Group = "Group";			
            //public const string AttributeSetid = "AttributeSetid";		
            public const string Category = "Category";
            //public const string ParentEntityTypeId = "ParentEntityTypeId";
            public const string ShortDescription = "ShortDescription";
            public const string ColorCode = "ColorCode";
            public const string IsAssociate = "IsAssociate";
            public const string WorkFlowID = "WorkFlowID";
            public const string IsRootLevel = "IsRootLevel";
		}

		#endregion
		
		
	}
	
}
