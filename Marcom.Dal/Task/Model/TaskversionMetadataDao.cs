﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'TM_TaskVersionViewdata'.
    /// </summary>

    public partial class  TaskVersionMetadataDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _TaskId;
        protected int _VersionId;
        protected string _JsonViewdata;
        protected string _Jsonmetadata;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TaskVersionMetadataDao() { }

        public TaskVersionMetadataDao(int pTaskId, string pJsonViewdata, int pVersionId, string pJsonmetadata)
        {
            this._TaskId = pTaskId;
            this._JsonViewdata = pJsonViewdata;
            this._VersionId = pVersionId;
            this._Jsonmetadata = pJsonmetadata;

        }

        public TaskVersionMetadataDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }




        public virtual string JsonViewdata
        {
            get { return _JsonViewdata; }
            set { _bIsChanged |= (_JsonViewdata != value); _JsonViewdata = value; }

        }

        public virtual string JsonMetadata
        {
            get { return _Jsonmetadata; }
            set { _bIsChanged |= (_Jsonmetadata != value); _Jsonmetadata = value; }

        }

        public virtual int TaskId
        {
            get { return _TaskId; }
            set { _bIsChanged |= (_TaskId != value); _TaskId = value; }

        }

        public virtual int VersionId
        {
            get { return _VersionId; }
            set { _bIsChanged |= (_VersionId != value); _VersionId = value; }

        }

      
        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TaskVersionViewdataDao = "TaskVersionViewdataDao";
            public const string ID = "ID";
            public const string TaskId = "TaskId";
            public const string VersionId = "VersionId";
            public const string TaskType = "TaskType";
            public const string JsonViewdata = "JsonViewdata";
            public const string JsonMetadata = "JsonMetadata";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TaskVersionViewdataDao = "TaskVersionViewdataDao";
            public const string ID = "ID";
            public const string TaskId = "TaskId";
            public const string VersionId = "VersionId";
            public const string TaskType = "TaskType";
            public const string JsonMetadata = "JsonMetadata";
        }

        #endregion


    }

}
