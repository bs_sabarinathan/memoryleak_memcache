﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class AttributeGroupRoleAccess : IAttributeGroupRoleAccess
    {
         #region Member Variables

		protected int _id;
        protected int _entitytypeid;
        protected int _globalroleid;
        protected int _attrgroupid;
		#endregion
		
		#region Constructors
		public AttributeGroupRoleAccess() {}

        public AttributeGroupRoleAccess(int pId, int pCutomtabid, int pEntitytypeId, int pGlobalroleId, int pAttrGroupId)
		{
			this._id = pId;
            this._entitytypeid = pEntitytypeId;
            this._globalroleid = pGlobalroleId;
            this._attrgroupid = pAttrGroupId;
		}
		
		#endregion
		
		#region Public Properties
		
		public int ID
		{
			get { return _id; }
			set {  _id = value; }
			
		}

        public  int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }
        }

        public  int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _globalroleid = value; }
        }

        public  int AttributeGroupID
        {
            get { return _attrgroupid; }
            set {  _attrgroupid = value; }
        }

		#endregion 			

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttributeGroupRoleAccess castObj = null;
            try
            {
                castObj = (AttributeGroupRoleAccess)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }
}
