﻿(function(ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailsectionCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, CcsectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        setTimeout(function () { $scope.page_resize(); }, 100);
        $scope.TabCollectionsList = null;
        $scope.IsLock = true;
        $scope.EntityLockTask = false;
        CcsectionService.GetLockStatus($stateParams.ID).then(function(entitylockstatus) {
            $scope.IsLock = entitylockstatus.Response.m_Item1;
        });
        $scope.set_bgcolor = function(clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            } 
            else return ''
        }
        $scope.set_color = function(clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            }
            else return ''
        }
        $scope.loadBreadCrum = function(isOnload) {
            $("#plandetailfilterdiv").css('display', 'none')
            if ($("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").length == 0 && $scope.Activity.IsActivitySectionLoad == false) {
                $scope.loadcostcentrefromsearchByID($stateParams.ID, "100");
            }
            $("#EntitiesTree li.active").removeClass('active')
            $("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
            $scope.array = [];
            CcsectionService.GetPath($stateParams.ID).then(function(getbc) {
                $scope.array = [];
                for (var i = 0; i < getbc.Response.length; i++) {
                    if (i == (getbc.Response.length - 1)) {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": decodedName,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "999999"
                        });
                        $scope.EntityShortText = getbc.Response[i].ShortDescription;
                        $scope.EntityColorcode = getbc.Response[i].ColorCode;
                        $scope.RootLevelEntityName = decodedName;
                        $scope.RootLevelEntityNameTemp = getbc.Response[i].Name;
                        $window.GlobalUniquekey = getbc.Response[i].UniqueKey;
                        $scope.EntityTypeID = getbc.Response[i].TypeID;
                        //$timeout(function () {
                            if (getbc.Response[i].IsLock == "True") $scope.IsLock = true;
                            else $scope.IsLock = false;
                        //}, 50)
                        
                    } else {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": decodedName,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "0088CC"
                        });
                    }
                }
            });
            if (isOnload == false) {
                if ($scope.subview == "overview") {
                    $scope.$broadcast('LoadCostcentreSectionDetail', $stateParams.ID);
                } else if ($scope.subview == "financial") {
                    $scope.$broadcast('LoadFinancialDetailCC', $stateParams.ID);
                } else if ($scope.subview == "member") {
                    $scope.$broadcast('LoadMembersDetailCC', $stateParams.ID);
                } else if ($scope.subview == "objective") {} else if ($scope.subview == "attachment") {
                    $scope.$broadcast('LoadAttachment', $stateParams.ID);
                } else if ($scope.subview == "presentation") {
                    $scope.$broadcast('LoadPresentationsCC', $stateParams.ID);
                } else if ($scope.subview == "workflow") {
                    $scope.$broadcast('LoadWorkflowCC', $stateParams.ID);
                } else if ($scope.subview == "task") {
                    ResetTaskObjects();
                    $scope.$broadcast('LoadTask', $stateParams.ID);
                }
            }
        }
        $scope.loadBreadCrum(true);
        $scope.load = function(parameters) {
            $scope.LoadTab = function(tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                // Put the object into storage
                localStorage.setItem('lasttab', tab);
                $location.path('/mui/planningtool/costcentre/detail/section/' + $stateParams.ID + '/' + tab + '');
            };
        };
        $scope.CustomTabLoad = function(tab, tabUrl, IsSytemDefined, Name, AddEntityID) {
            if (IsSytemDefined == false) {
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                CcsectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function(gettabresult) {
                    if (gettabresult.Response != null) {
                        $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                    }
                });
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                $location.path('/mui/planningtool/costcentre/detail/section/customtab/' + tab + '/' + $stateParams.ID + '');
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }
        }
        $scope.BreadCrumOverview = function(ID) {
            var actname = $('#breadcrumlink').text();
            actname = actname.substring(2, actname.length);
            $window.EntityTypeID = ID;
            $window.SubscriptionEntityTypeID = $('#breadcrumlink').attr('data-typeid');
            $window.GlobalUniquekey = $('#breadcrumlink').attr('data-uniquekey');
            $scope.RootLevelEntityName = actname;
            $scope.EntityShortText = $('#breadcrumlink').attr('data-shortdesc');
            $scope.EntityColorcode = $('#breadcrumlink').attr('data-colorcode');
            $location.path('/mui/planningtool/costcentre/detail/section/' + ID + '/overview');
        }
        $scope.TaskExpandedDetails = [];
        $scope.SubLevelTaskExpandedDetails = [];
        $scope.SubLevelTaskLibraryListContainer = {
            SubLevelTaskLibraryList: []
        };
        $scope.RootLevelTaskLibraryListContainer = {
            RootLevelTaskLibraryList: []
        };
        $scope.groupbySubLevelTaskObj = [];
        $scope.ngExpandAll = true;
        $scope.ngExpandCollapseStatus = {
            ngExpandAll: true
        };
        $scope.TaskOrderListObj = {
            TaskSortingStatus: "Reorder task lists",
            TaskSortingStatusID: 1,
            TaskOrderHandle: false,
            SortOrderObj: [],
            UniqueTaskSort: "Reorder task",
            UniqueTaskSortingStatusID: 1,
            UniqueTaskOrderHandle: false,
            UniqueSortOrderObj: []
        };
        $scope.SubLevelEnableObject = {
            Enabled: false,
            SublevelText: "Show task(s) from sub-levels",
            SublevelTextStatus: 1
        };
        GetAllTabCollections();

        function GetAllTabCollections() {
            CcsectionService.GetCustomEntityTabsByTypeID(5, 5, $stateParams.ID, 0).then(function(gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function(item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID);
                    }
                }
            });
        }
        $scope.BindClass = function(index, tab, ID) {
            if (index == 0 && $scope.subview == null) {
                if ($scope.subview == null) {
                    var tabid = $scope.TabCollectionsList[index].ControleID;
                    $scope.subview = tabid;
                }
                return "active";
            } else if ($scope.subview == tab) {
                return "active";
            } else if (ID == $stateParams.tabID) {
                return "active";
            }
        }
        ResetTaskObjects();

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function(event, taskkist) {
            if (taskkist.length > 0) {
                $scope.MainTaskLiveUpdateIndex = 0;
                $scope.subTaskLiveUpdateIndex = 0;
                UpdateTaskData();
                UpdateSubLevelTaskData();
            }
        });

        $scope.$on('RefreshEntityTaskListCollection', function (event, taskobj) {
            $scope.LiveTaskListIDCollection.TasklistIDList = [{
                "TaskLiStID": taskobj.TaskListID,
                "EntityID": taskobj.EntityID
            }];
            InitiateTaskLiveupdateAction();
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.MainTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.TaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var taskListResObj = $.grep($scope.groups, function(e) {
                            return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                        });
                        if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                            if (tasklistList[0].TaskList.length > 0) {
                                taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                    if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateTaskData();
                }, 20);
            });
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }
        $scope.$on("$destroy", function() {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.sectionCtrl']"));
        });
        $timeout(function() {
            $scope.load();
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
        $(window).on("UpdateEntityName", function(event, optionarray) {
            $scope.RootLevelEntityName = optionarray;
        });
    }
    app.controller("mui.planningtool.costcentre.detail.sectionCtrl", ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'CcsectionService', muiplanningtoolcostcentredetailsectionCtrl]);
})(angular, app);