﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;


namespace BrandSystems.Marcom.Core.Task
{
    internal class TaskLibraryTemplateHolder : ITaskLibraryTemplateHolder
    {
        #region Public Properties

        public int ID { get; set; }
        public string LibraryName { get; set; }
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
        public string LibraryDescription { get; set; }
        public dynamic TaskList { get; set; }
        public dynamic TaskAttachment { get; set; }
        public int EntityParentID { get; set; }
        public bool IsGetTasks { get; set; }
        public bool IsExpanded { get; set; }
        public int TaskCount { get; set; }
        #endregion
    }
}
