using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// ObjectiveNumeric object for table 'PM_Objective_Numeric'.
	/// </summary>
	
	internal class ObjectiveNumeric : IObjectiveNumeric 
	{
		#region Member Variables

        protected int _objectiveId;
        protected decimal _globalbaseline;
        protected decimal _globaltarget;
		protected int _unitid;
        protected int _unitvalue;
		
		
		#endregion
		
		#region Constructors
		public ObjectiveNumeric() {}

        public ObjectiveNumeric(int pObjectivId, decimal pGlobalBaseLine, decimal pGlobalTarget, int pUnitid, int pUnitvalue)
        {
            this._objectiveId = pObjectivId;
            this._globalbaseline = pGlobalBaseLine;
            this._globaltarget = pGlobalTarget;
            this._unitid = pUnitid;
            this._unitvalue = pUnitvalue;
        }
		
		#endregion
		
		#region Public Properties
		
        public int ObjectiveId
        {
            get { return _objectiveId; }
            set { _objectiveId = value; }
        }
        public decimal GlobalBaseLine
		{
			get { return _globalbaseline; }
			set { _globalbaseline = value; }
			
		}

        public decimal GlobalTarget
		{
			get { return _globaltarget; }
			set { _globaltarget = value; }
			
		}
		
		public int Unitid
		{
			get { return _unitid; }
			set { _unitid = value; }
			
		}

        public int UnitValue
        {
            get { return _unitvalue; }
            set { _unitvalue = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			ObjectiveNumeric castObj = null;
			try
			{
				castObj = (ObjectiveNumeric)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
