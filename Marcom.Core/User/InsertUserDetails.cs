﻿using BrandSystems.Marcom.Core.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User
{
    public partial class InsertUserDetails : IInsertUserDetails, ICloneable
    {
        #region Member Variables

		protected int _id;
        protected int _IUserID;
        protected string _IEmail;
        protected byte[] _IPassword;
        protected string _IPasswordSalt;
        protected DateTime _ICreationTime;
        protected string _IPAddress;
        protected string _IPNumber;
		protected string _IOS;
	
        protected bool _bIsDeleted;
		protected bool _bIsChanged;
       


		#endregion
		
		#region Constructors
		public InsertUserDetails() {}

        public InsertUserDetails(int UserID, string Email, byte[] Password, string PasswordSalt, DateTime CreationTime, string IPAddress, string IPNumber, string OS)
		{
            this._IUserID = UserID;
            this._IEmail=Email;
            this._IPassword=Password;
            this._IPasswordSalt = PasswordSalt;
            this._ICreationTime = CreationTime;
            this._IPAddress = IPAddress;
            this._IPNumber = IPNumber;
            this._IOS = OS;
           
			
            
		}

        public InsertUserDetails(int Id)
		{
			this._id = Id; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int UserID
        {
            get { return _IUserID; }
            set { _IUserID = value; }

        }

        public virtual string Email
        {
            get { return _IEmail; }
            set
            {
                _bIsChanged |= (_IEmail != value);
                _IEmail = value;
            }

        }

        public virtual byte[] Password
        {
            get { return _IPassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_IPassword != value);
                _IPassword = value;
            }

        }


        public virtual string PasswordSalt
        {
            get { return _IPasswordSalt; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_IPasswordSalt != value);
                _IPasswordSalt = value;
            }

        }
        public virtual DateTime CreationTime
        {
            get { return _ICreationTime; }
            set
            {
                _ICreationTime = value;
            }

        }
       
        public virtual string IPAddress
		{
            get { return _IPAddress; }
			set 
			{
                _bIsChanged |= (_IPAddress != value);
                _IPAddress = value; 
			}
			
		}



        public virtual string IPNumber
        {
            get { return _IPNumber; }
            set
            {
                _bIsChanged |= (_IPNumber != value);
                _IPNumber = value;
            }

        }

        public virtual string OS
        {
            get { return _IOS ; }
            set
            {
                _bIsChanged |= (_IOS != value);
                _IOS = value;
            }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}


		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion

        
    }
}
