﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    public class ListofRecord : IListofRecord
    {
        #region Member Variables

        private IList<IAttribute> _attributes;

        private IList _data;

        private IList<AttributeSettings> _columndefs;

        private int _dataCount;

        private Tuple<ArrayList, ArrayList> _generalcolumndefs;
        #endregion

        #region Constructors
        public ListofRecord() { }

        public ListofRecord(IList<IAttribute> mAttributes, IList mData, IList<AttributeSettings> mColumnDefs, int _mDataCount, Tuple<ArrayList, ArrayList> _mGeneralColumnDefs)
        {
            this._attributes = mAttributes;
            this._data = mData;
            this._columndefs = mColumnDefs;
            this._dataCount = _mDataCount;
            this._generalcolumndefs = _mGeneralColumnDefs;
        }



        #endregion

        #region Public Properties


        public IList<IAttribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }
        public IList Data
        {
            get { return _data; }
            set { _data = value; }

        }

        public IList<AttributeSettings> ColumnDefs
        {
            get { return _columndefs; }
            set { _columndefs = value; }

        }

        public int DataCount
        {
            get { return _dataCount; }
            set { if (value > 0) _dataCount = value; else _dataCount = 0; }

        }

        public Tuple<ArrayList, ArrayList> GeneralColumnDefs
        {
            get { return _generalcolumndefs; }
            set { _generalcolumndefs = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        //public override bool Equals(object obj)
        //{
        //    if (this == obj) return true;
        //    ListofRecord castObj = null;
        //    try
        //    {
        //        castObj = (ListofRecord)obj;
        //    }
        //    catch (Exception) { return false; }
        //    return (castObj != null) &&
        //        (this._attributes == castObj._attributes);
        //}
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _attributes.GetHashCode();
            return hash;
        }
        #endregion
    }
}
