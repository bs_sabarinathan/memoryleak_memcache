﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Cms.Interface;

namespace BrandSystems.Marcom.Core.Cms
{
    public partial class Tags : ITags
    {
        #region Member variables

        protected int _id;
        protected int _entityid;
        protected string _tag;


        #endregion

        #region Constructors

        public Tags()
        {

        }

        public Tags(int pid)
        {
            this._id = pid;
        }

        public Tags(int pid, int pentityid, bool pactive, string ptag)
        {
            this._id = pid;
            this._entityid = pentityid;
            this._tag = ptag;
        }

        #endregion

        #region Public properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }



        #endregion


    }
}
