﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Xml;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Metadata.Interface;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Core.Dam;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Dal.DAM.Model;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;
using BrandSystems.Marcom.Dal.ExpireHandler.Model;


namespace BrandSystems.Marcom.Core.Managers.Proxy
{
    internal partial class ExpireHandlerManagerProxy : IExpireHandlerManager, IManagerProxy
    {
        // Reference to the MarcomManager
        /// <summary>
        /// The _marcom manager      
        /// </summary>
        private MarcomManager _marcomManager = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataManagerProxy" /> class.
        /// </summary>
        /// <param name="marcomManager">The marcom manager.</param>
        internal ExpireHandlerManagerProxy(MarcomManager marcomManager)
        {
            _marcomManager = marcomManager;

            // Do some initialization.... 
            // i.e. cache logged in user specific things (or maybe use lazy loading for that)
        }


        // Reference to the MarcomManager (only internal)
        /// <summary>
        /// Gets the marcom manager.
        /// </summary>
        /// <value>
        /// The marcom manager.
        /// </value>
        internal MarcomManager MarcomManager
        {
            get { return _marcomManager; }
        }


        public IList<EHActionInitiatorDao> GetExpireActions(int SourceID, int AttributeID, int ActionID)
        {
            return ExpireHandlerManager.Instance.GetExpireActions(this, SourceID, AttributeID,ActionID);
        }
        public int CreateExpireAction(int ActionTypeID, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, int ActionsourceId, int attributeid, JObject srcValue)
        {
            return ExpireHandlerManager.Instance.CreateExpireAction(this, ActionTypeID, SourceID, SourceEnityID, SourceFrom, Actionexutedays, DateActionexpiredate, ActionsourceId, attributeid, srcValue);
        }

        public bool UpdateExpireActionDate(int SourceID, string DateActionexpiredate, int SourcetypeID, int ActionType, string Actionexutedays, int SourceAttributeID ,int SourceFrom, int ProductionEntityID)
        {
            return ExpireHandlerManager.Instance.UpdateExpireActionDate(this, SourceID, DateActionexpiredate, SourcetypeID, ActionType, Actionexutedays, SourceAttributeID, SourceFrom, ProductionEntityID);
        }
        public bool DeleteExpireAction(int ActionID)
        {
            return ExpireHandlerManager.Instance.DeleteExpireAction(this, ActionID);
        }

        public IList<IAttribute> GetEntityAttributes(int entityID)
        {
            return ExpireHandlerManager.Instance.GetEntityAttributes(this, entityID);
        }

        //public int CreateExpireActionwithMetadata(int ActionType, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, bool Actionexute, bool ispublish, int ActionsourceId, IList<IAttributeData> listattributevalues, string Taskinputvalue)
        //{
        //    return ExpireHandlerManager.Instance.CreateExpireActionwithMetadata(this, ActionType, SourceID, SourceEnityID, SourceFrom, Actionexutedays, DateActionexpiredate, Actionexute, ispublish, ActionsourceId, listattributevalues, Taskinputvalue);
        //}

        public IList<IExpireActionSourcedatas> GetExpireHandlermetadata(int actionSourceID)
        {
            return ExpireHandlerManager.Instance.GetExpireHandlermetadata(this, actionSourceID);
        }
        public IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(int entityID)
        {
            return ExpireHandlerManager.Instance.GetEntityTypeAttributeRelationWithLevelsByID(this, entityID);
        }
        public bool ExpiryupdateEntityAttribute(IList<IAttributeData> attributes, int entityId, int updaterid)
        {
            return ExpireHandlerManager.Instance.ExpiryupdateEntityAttribute(this, attributes, entityId,updaterid);
        }
        public bool ValidExpireActionDate(int SourceID, string DateActionexpiredate, int SourcetypeID, int ActionType, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID, string DueActionexutedays)
        {
            return ExpireHandlerManager.Instance.ValidExpireActionDate(this, SourceID, DateActionexpiredate, SourcetypeID, ActionType, Actionexutedays, SourceAttributeID, SourceFrom, ProductionEntityID, DueActionexutedays);
        }
        public IList<IEntityTypeExpireRoleAcl> GetEntityTypeExpireRoleAccess(int EntityTypeID)
        {
            return ExpireHandlerManager.Instance.GetEntityTypeExpireRoleAccess(this, EntityTypeID);
        }
    }
}
