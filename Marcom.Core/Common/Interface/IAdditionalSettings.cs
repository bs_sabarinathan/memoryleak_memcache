using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFeed interface for table 'CM_Feed'.
    /// </summary>
    public interface IAdditionalSettings
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string SettingName
        {
            get;
            set;

        }

        string SettingValue
        {
            get;
            set;

        }
        IList<ICurrencyType> CurrencyFormatvalue
        {
            get;
            set;
        }

      

        #endregion
    }
}
