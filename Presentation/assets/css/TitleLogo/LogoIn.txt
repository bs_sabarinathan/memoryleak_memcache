﻿/*#region LOGIN THEME*/

.loginThemed {
    background-color: #@LogoINBgColor;
     @BgimageURL
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

    .loginThemed .loginBlock,
    .loginThemed .forgotPasswordBlock {
        height: 350px;
        width: 330px;
        padding: 10px;
    }

/*#region LOGO IN*/
/*When Logo is In then below CSS*/
.loginThemed .loginBlock,
.loginThemed .forgotPasswordBlock {
    height: 334px;
    border: 6px solid rgba(255, 255, 255, 0.8);
    background-color: rgba(255, 255, 255, 0.7);
}

    .loginThemed .loginBlock .header,
    .loginThemed .forgotPasswordBlock .header {
        text-align: @LogoAlign;
    }

	.loginThemed .loginBlock .header > img#loginpageTitle,
    .loginThemed .forgotPasswordBlock .header > img#loginpageTitle {
        margin: 0 12px;
    }

    .loginThemed .loginBlock fieldset,
    .loginThemed .forgotPasswordBlock fieldset {
        border-color: transparent;
        border-radius: 0 !important;
		background-color: transparent;
        box-shadow: none;
    }
/*#endregion*/


/*#endregion*/

/*#endregion*/
