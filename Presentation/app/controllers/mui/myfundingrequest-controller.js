﻿

(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.myfundrequestCtrl",
    function muimyfundrequestCtrl($scope, $cookies, $resource, $compile, $window, $timeout, $location, $translate, MyfundingrequestService) {

        //---------------------------  Get the initial feed ------------------------------------
        // --- Define Scope Variables. ---------------------- //
        // --- Bind To Scope Events. ------------------------ //
        // I handle changes to the request context.
        // --- Initialize. ---------------------------------- //
        //Forming Complex data

        $scope.ComplexTaskDataStructure = [];
        $scope.ComplexTaskFilterDataStructure = [];
        $scope.ShowTaskStatus = [{ ID: 8, Name: 'In progress' }, { ID: 11, Name: 'Approved' }, { ID: 12, Name: 'Rejected' }];
        $scope.MulitipleStatus = [8];
        $scope.muiiscalender.Iscalender = false;
        $scope.muiiscalender.CalenderID = 0;
        $scope.ShowTasksByAssignCriteria = [{ ID: 1, Name: 'sent BY me' }, { ID: 4, Name: 'sent TO me' }];

        $scope.AssignRole = 4;

        $scope.FilterStatusID = 1;
        $timeout(function () {
            GetMyTaskLists();
        }, 0);
        $scope.TaskAssignCriteria = 'sent TO me';
        $scope.TaskCriteria = 'Main location';
        $scope.CuurenttaskStatus = 'In progress';
        $scope.frtempWizardID = $scope.WizardID;

        if ($scope.frtempWizardID != undefined) {
            if ($scope.frtempWizardID.length > 0) {
                $scope.ngShowfundingrequests = false;
                $scope.ngShowDesc = false;
                $('#container.myfundingrequest.myPage').parents('.box-content').find('.InProgress').remove();
            }
            else {
                $scope.ngShowfundingrequests = true;
                $scope.ngShowDesc = true;
            }

        }
        else {
            $scope.ngShowfundingrequests = true;
            $scope.ngShowDesc = true;
        }

        function GetMyTaskLists() {
            var drawContent = '';
            $("ul#MyFundHolder").html($compile("")($scope));
            var MyTaskParams = {};
            MyTaskParams.AssignRole = $scope.AssignRole;
            MyTaskParams.FilterStatusID = $scope.MulitipleStatus;//[1, 2, 3];
            MyfundingrequestService.GetMyFundingRequests(MyTaskParams).then(function (GetMytasksResult) {

                $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;

                if (GetMytasksResult.StatusCode == 200) {
                    var GetMytasksResultObj = [];
                    GetMytasksResultObj = GetMytasksResult.Response;
                    if (GetMytasksResultObj != null)
                        for (var i = 0; i <= GetMytasksResultObj.length - 1; i++) {
                            var dateval = "";
                            var datStartUTCval = "";
                            var datstartval = "";
                            dateval = GetMytasksResultObj[i].DueDate;
                            datStartUTCval = dateval.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            var FinancialReqduedate = dateFormat(datstartval, $scope.DefaultSettings.DateFormat);
                            drawContent += '<li class="ListItem ' + generateRequestStatusText(GetMytasksResultObj[i].TaskStatus) + '" ng-click="OpenFundRequestPopup(' + GetMytasksResultObj[i].TaskId + ')"  class="ListItem"> ';
                            drawContent += '                     <ul class="inline"> ';
                            drawContent += '                         <li class="col1"> ';
                            drawContent += '                             <span class="manageTextOverflow"> ';
                            drawContent += '                                 <span style="background-color: #' + GetMytasksResultObj[i].EntityTypeCC + ';" class="eicon-s margin-right5x">' + GetMytasksResultObj[i].EntityType + '</span> ';
                            drawContent += '                                 <span class="mainText">' + GetMytasksResultObj[i].EntityName + '</span> ';
                            drawContent += '                             </span> ';
                            drawContent += '                         </li> ';
                            drawContent += '                         <li class="col2"><span>' + dateFormat(GetMytasksResultObj[i].RequestDate, $scope.DefaultSettings.DateFormat) + '</span></li> ';
                            drawContent += '                         <li class="col3"><span class="curreny">' + parseInt(GetMytasksResultObj[i].RequestAmount).formatMoney(0, ' ', ' ') + ' ' + $scope.DefaultSettings.CurrencyFormat.Name + '</span></li> ';
                            drawContent += '                         <li class="col4"> ';
                            drawContent += '                             <span>' + GetMytasksResultObj[i].CCNAME + '</span> ';
                            drawContent += '                         </li> ';
                            if ($scope.ngShowDesc) {
                                drawContent += '                         <li class="col5" > ';
                                drawContent += '                             <span>' + GetMytasksResultObj[i].TaskDescription + ' </span> ';
                                drawContent += '                         </li> ';
                            }
                            drawContent += '                         <li ng-show="AssignRole==4" class="col6" > ';
                            drawContent += '                             <span>' + GetMytasksResultObj[i].RequestUserName + ' </span> ';
                            drawContent += '                         </li> ';
                            drawContent += '                         <li ng-show="AssignRole!=4" class="col6" > ';
                            drawContent += '                             <span>' + GetMytasksResultObj[i].CCOWNERNAME + ' </span> ';
                            drawContent += '                         </li> ';
                            drawContent += '                         <li class="col7" ><span>';
                            drawContent += '                             ' + FinancialReqduedate + ' ';
                            drawContent += '                         </span></li> ';
                            drawContent += '                         <li class="col8 StatusIcon"> ';
                            drawContent += '                             <span class="statusColumn">';
                            //drawContent += '                               <span class="pull-left">' + generateRequestStatusIconText(GetMytasksResultObj[i].TaskStatus) + ' </span> <i my-qtip2="" qtip-content="{{generateRequestStatusTooltip(' + GetMytasksResultObj[i].TaskStatus + ')}}"  class="icon-status" ></i> ';
                            drawContent += '                               ' + generateRequestStatusIconText(GetMytasksResultObj[i].TaskStatus) + ' <i my-qtip2="" qtip-content="{{generateRequestStatusTooltip(' + GetMytasksResultObj[i].TaskStatus + ')}}"  class="icon-status" ></i> ';
                            drawContent += '                             </span>';
                            drawContent += '                         </li> ';
                            drawContent += '                     </ul> ';
                            drawContent += '                 </li>';

                        }
                    $("ul#MyFundHolder").append(
                             $compile(drawContent)($scope));
                    $(window).AdjustHeightWidth();
                }
            });
            $(window).AdjustHeightWidth();
        }

        function generateRequestStatusIcon(statvale) {
            if (statvale == 8) {
                return "ListItem InProgress icon-status";
            }
            else if (statvale == 11) {
                return "TaskPopup Approved icon-status";
            }
            else if (statvale == 12) {
                return "TaskPopup Rejected icon-status";
            }
        }
        function generateRequestStatusText(statvale) {
            if (statvale == 8) {
                return "InProgress";
            }
            else if (statvale == 11) {
                return "Approved";
            }
            else if (statvale == 12) {
                return "Rejected";
            }
        }
        function generateRequestStatusIconText(statvale) {
            if (statvale == 8) {
                return "In progress";
            }
            else if (statvale == 11) {
                return "Approved";
            }
            else if (statvale == 12) {
                return "Rejected";
            }
        }

        $scope.generateRequestStatusTooltip = function (statvale) {
            if (statvale == 8) {
                return "In progress";
            }
            else if (statvale == 11) {
                return "Approved";
            }
            else if (statvale == 12) {
                return "Rejected";
            }
        }

        $scope.OpenFundRequestPopup = function (funderqid) {
            $("#feedFundingRequestModal").modal("show");
            //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
            $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);
        }

        $scope.ShowtasksChangeVal = function (ID) {
            ChangeSelectedShowtasks(ID);
        }

        function ChangeSelectedShowtasks(ID) {
            var Currentitem = $.grep($scope.ShowTasksByAssignCriteria, function (n, i) {
                return $scope.ShowTasksByAssignCriteria[i].ID == ID;
            });
            $scope.AssignRole = ID;
            $scope.TaskAssignCriteria = Currentitem[0].Name;
            $scope.ComplexTaskDataStructure = { Header: '', body: '' };
            GetMyTaskLists();
        }

        $scope.SelctedCriteriaChangeVal = function (ID) {
            Changeselectedval(ID);
        }

        $scope.ShowTaskStatusChangeVal = function () {

            GetMyTaskLists();
        }

        $scope.StatusDivClasses = function (statusCode, overdue, IsAdminTask) {
            var baseClass = 'ListItem ';
            if (statusCode == 0 && overdue >= 0 && IsAdminTask == 0)
                return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue >= 0 && IsAdminTask >= 0)
                return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask > 0)
                return baseClass + 'UnableToComplete OverDue';
            if (statusCode == 0 && overdue == 0 && IsAdminTask > 0)
                return baseClass + 'Unassigned';
            if (statusCode == 0 && overdue < 0 && IsAdminTask >= 0)
                return baseClass + 'Unassigned OverDue';
            if (statusCode == 1)
                return baseClass + 'InProgress';
            if (statusCode == 2)
                return baseClass + 'Completed';
            if (statusCode == 3)
                return baseClass + 'Approved';
            if (statusCode == 4)
                return baseClass + 'UnableToComplete';
            if (statusCode == 5 || statusCode == 6)
                return baseClass + 'Rejected';
            if (statusCode == 7)
                return baseClass + 'NotApplicable';
        }

        $scope.CustomDateFormat = function (date) {

            return dateFormat(date, $scope.DefaultSettings.DateFormat)
        }
        $scope.TaskDetails = [];

        $scope.IsActiveEntity = function (EntityID) {
              MyfundingrequestService.IsActiveEntity(EntityID).then(function (result) {
                if (result.Response == true) {
                    $location.path('/mui/planningtool/default/detail/section/' + EntityID + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }

            });
        }

        $scope.$on('onMyfundingRequestkActions', function (event) {
            GetMyTaskLists();
        });


        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.myfundrequestCtrl']"));
        });
    }
    //);
    app.controller("mui.myfundrequestCtrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$timeout', '$location', '$translate', 'MyfundingrequestService', muimyfundrequestCtrl]);
    function MyfundingrequestService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetMyFundingRequests: GetMyFundingRequests,
            IsActiveEntity: IsActiveEntity
        });
        function GetMyFundingRequests(formobj) { var request = $http({ method: "post", url: "api/task/GetMyFundingRequests/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("MyfundingrequestService", ['$http', '$q', MyfundingrequestService]);
})(angular, app);