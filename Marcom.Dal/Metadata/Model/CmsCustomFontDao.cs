﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class CmsCustomFontDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
        protected int _providerid;
        protected string _kitid;
        protected string _name;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CmsCustomFontDao() {}

        public CmsCustomFontDao(int pId, int pProviderId, string pKitId, string pName)
		{
			this._id = pId;
            this._providerid = pProviderId;
            this._kitid = pKitId;
            this._name = pName; 
		}

        public CmsCustomFontDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 250)
                  throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
              _bIsChanged |= (_name != value);
              _name = value; 
			}
		}
		
		public virtual int ProviderID
		{
            get { return _providerid; }
            set { _bIsChanged |= (_providerid != value); _providerid = value; }
		}

        public virtual string KitID
		{
			get { return _kitid; }
            set { _bIsChanged |= (_kitid != value); _kitid = value; }
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CmsCustomFontDao = "CmsCustomFontDao";			
			public const string Id = "Id";
            public const string ProviderID = "ProviderID";
            public const string KitID = "KitID";			
			public const string IsEnable = "IsEnable";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CmsCustomFontDao = "CmsCustomFontDao";			
			public const string Id = "Id";
            public const string ProviderID = "ProviderID";
            public const string KitID = "KitID";
            public const string Name = "Name";			
		}

		#endregion
    }
}
