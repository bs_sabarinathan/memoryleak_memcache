﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class ExpireHandlerController : ApiController
    {
        ServiceResponse result;

        [Route("api/ExpireHandler/CreateExpireAction")]
        [HttpPost]
        public ServiceResponse CreateExpireAction([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject CreateExpireActionObj = (JObject)jobj["CreateExpireAction"];
                JObject saveObj = (JObject)CreateExpireActionObj["SaveExpireAction"];
                var ActionTypeID = int.Parse(saveObj["ActionType"].ToString());
                var AttributeID = int.Parse(saveObj["AttributeID"].ToString());
                var SourceID = int.Parse(saveObj["SourceID"].ToString());
                var SourceEnityID = int.Parse(saveObj["SourceEnityID"].ToString());
                var SourceFrom = int.Parse(saveObj["SourceFrom"].ToString());
                var Actionexutedays = saveObj["Actionexutedays"].ToString();
                var DateActionexpiredate = saveObj["DateActionexpiredate"].ToString();
                var ActionsourceId = int.Parse(saveObj["ActionsourceId"].ToString());
                var srcInputObj = (JObject)saveObj["ActionSrcValue"];
                var attributedetails = (JObject)srcInputObj["ActionTaskSrcValue"];
                if (attributedetails != null)
                {
                    foreach (var i in attributedetails["AttributeData"])
                    {
                        if ((int)i["AttributeID"] == (int)SystemDefinedAttributes.CreationDate)
                        {
                            i["NodeID"] = DateTime.UtcNow.ToString("yyyy-MM-dd");
                        }
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.CreateExpireAction(ActionTypeID, SourceID, SourceEnityID, SourceFrom, Actionexutedays, DateActionexpiredate, ActionsourceId, AttributeID, srcInputObj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/UpdateExpireActionDate")]
        [HttpPost]
        public ServiceResponse UpdateExpireActionDate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray saveObj = (JArray)jobj["UpdateExpireActionDate"];
                var SourceID = int.Parse(saveObj[0]["SourceID"].ToString());
                var DateActionexpiredate = saveObj[0]["DateActionexpiredate"].ToString();
                var ActionType = int.Parse(saveObj[0]["ActionType"].ToString());
                var Actionexutedays = saveObj[0]["Actionexutedays"].ToString();
                var SourcetypeID = int.Parse(saveObj[0]["SourcetypeID"].ToString());
                var SourceAttributeID = int.Parse(saveObj[0]["SourceAttributeID"].ToString());
                var ProductionEntityID = int.Parse(saveObj[0]["ProductionEntityID"].ToString());
                var SourceFrom = int.Parse(saveObj[0]["SourceFrom"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.UpdateExpireActionDate(SourceID, DateActionexpiredate, SourcetypeID, ActionType, Actionexutedays, SourceAttributeID, SourceFrom, ProductionEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/GetExpireActions/{SourceID}/{AttributeID}/{ActionID}")]
        [HttpGet]
        public ServiceResponse GetExpireActions(int SourceID, int AttributeID, int ActionID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.GetExpireActions(SourceID, AttributeID, ActionID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/DeleteExpireAction/{ActionID}")]
        [HttpDelete]
        public ServiceResponse DeleteExpireAction(int ActionID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.DeleteExpireAction(ActionID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/ExpireHandler/GetEntityAttributes/{entityID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributes(int entityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.GetEntityAttributes(entityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/GetExpireHandlermetadata/{ActionSourceID}")]
        [HttpGet]
        public ServiceResponse GetExpireHandlermetadata(int ActionSourceID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.GetExpireHandlermetadata(ActionSourceID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/GetEntityTypeAttributeRelationWithLevelsByID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelationWithLevelsByID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.GetEntityTypeAttributeRelationWithLevelsByID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/ExpireHandler/ValidExpireActionDate")]
        [HttpPost]
        public ServiceResponse ValidExpireActionDate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray saveObj = (JArray)jobj["ValidExpireActionDate"];
                var SourceID = int.Parse(saveObj[0]["SourceID"].ToString());
                var DateActionexpiredate = saveObj[0]["DateActionexpiredate"].ToString();
                var ActionType = int.Parse(saveObj[0]["ActionType"].ToString());
                var Actionexutedays = saveObj[0]["Actionexutedays"].ToString();
                var SourcetypeID = int.Parse(saveObj[0]["SourcetypeID"].ToString());
                var SourceAttributeID = int.Parse(saveObj[0]["SourceAttributeID"].ToString());
                var ProductionEntityID = int.Parse(saveObj[0]["ProductionEntityID"].ToString());
                var SourceFrom = int.Parse(saveObj[0]["SourceFrom"].ToString());
                var DueActionexutedays = saveObj[0]["DueActionexutedays"].ToString();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.ValidExpireActionDate(SourceID, DateActionexpiredate, SourcetypeID, ActionType, Actionexutedays, SourceAttributeID, SourceFrom, ProductionEntityID, DueActionexutedays);
            }
            catch
            {
                result.Response = null;
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
            }
            return result;
        }

        [Route("api/ExpireHandler/GetEntityTypeExpireRoleAccess/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeExpireRoleAccess(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.ExpireHandlerManager.GetEntityTypeExpireRoleAccess(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
    }
}