using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveRating interface for table 'PM_Objective_Rating'.
    /// </summary>
    public interface IObjectiveRating
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Objectiveid
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
