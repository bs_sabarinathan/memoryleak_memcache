﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface ITaskList
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }


        string Description
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;
        }


        int SortOder
        {
            get;
            set;

        }

        int TotalAttachment { get; set; }

        IList<IAdminTask> TaskCollections { get; set; }

        int TotalTasks { get; set; }

      

        #endregion
    }
}
