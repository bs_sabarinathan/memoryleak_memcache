﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
    {
    internal class EntityTypeAttributeGroupRelation : IEntityTypeAttributeGroupRelation
        {
        #region Member Variables
        protected int _id;
        protected int _attributegroupid;
        protected string _caption;
        protected int _entitytypeid;
        protected int _IsLocationType;
        protected bool _IsRepresentationType;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected int _pagesize;
        protected bool _IsTabInEntityCreation;
        protected bool _IsAttrGrpInheritFromParent;

        #endregion

        #region Constructors
        public EntityTypeAttributeGroupRelation() { }

        public EntityTypeAttributeGroupRelation(int pID, int pAttributeGroupId, int pEnitytypeId, string pCaption, int pIsLocationType, bool pIsRepresentationType, int pSortOrder, int pPageSize, bool pIsTabInEntityCreation, bool pIsAttrGrpInheritFromParent)
        {
            this._id = pID;
            this._attributegroupid = pAttributeGroupId;
            this._entitytypeid = pEnitytypeId;
            this._caption = pCaption;
            this._IsLocationType = pIsLocationType;
            this._IsRepresentationType = pIsRepresentationType;
            this._sortorder = pSortOrder;
            this._pagesize = pPageSize;
            this._IsTabInEntityCreation = pIsTabInEntityCreation;
            this._IsAttrGrpInheritFromParent = pIsAttrGrpInheritFromParent;
        }

        public EntityTypeAttributeGroupRelation(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
            {
            get { return _id; }
            set { _id = value; }
            }

        public int AttributeGroupID
            {
            get { return _attributegroupid; }
            set { _attributegroupid = value; }

            }

        public int EntityTypeID
            {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

            }

        public virtual string Caption
            {
            get { return _caption; }
            set { _caption = value; }

            }

        public virtual int LocationType
            {
            get { return _IsLocationType; }
            set { _IsLocationType = value; }

            }

        public virtual bool RepresentationType
            {
            get { return _IsRepresentationType; }
            set { _IsRepresentationType = value; }

            }

        public virtual int SortOrder
            {
            get { return _sortorder; }
            set { _sortorder = value; }
            }

        public virtual int PageSize
        {
            get { return _pagesize; }
            set { _pagesize = value; }
        }

        public virtual bool IsTabInEntityCreation
        {
            get { return _IsTabInEntityCreation; }
            set { _IsTabInEntityCreation = value; }
        }

        public virtual bool IsAttrGrpInheritFromParent
        {
            get { return _IsAttrGrpInheritFromParent; }
            set { _IsAttrGrpInheritFromParent = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
            {
            if(this == obj) return true;
            EntityTypeAttributeRelation castObj = null;
            try
                {
                castObj = (EntityTypeAttributeRelation)obj;
                }
            catch(Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
            }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {

            return this.GetType().FullName.GetHashCode();

            }
        #endregion
        }
    }
