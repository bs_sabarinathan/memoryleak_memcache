﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Lucene.Net;
using Lucene.Net.Util;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using System.Threading;
using System.IO;

namespace BrandSystems.Marcom.Core.Planning
{
    public class SearchEngine
    {


        private static SearchEngine objSearchEngine = new SearchEngine();
        public static SearchEngine Instance
        {
            get { return objSearchEngine; }
        }


        private ReaderWriterLockSlim IndexLock = new ReaderWriterLockSlim();

        private IndexWriter IndexWriter = null;
        private IndexWriter TempIndexWriter = null;

        private IndexSearcher IndexSearch = null;

        private void InitIndexWriter()
        {
            if (IndexWriter == null)
            {
                IndexWriter = new IndexWriter(SearchSettings.Instance.GetIndexFileDirectory(), SearchSettings.Instance.GetAnalyzer(), new IndexWriter.MaxFieldLength(10000));
            }
        }

        private void DisposeIndexWriter()
        {
            if (IndexWriter != null)
            {
                IndexWriter.Close();
                IndexWriter.Dispose();
                IndexWriter = null;
            }

            if (IndexSearch != null)
            {
                //IndexSearch.Close();
                IndexSearch.Dispose();
                IndexSearch = null;
            }
        }


        private void OptimizeAndCloseIndex()
        {
            if (IndexWriter != null)
            {
                IndexWriter.Optimize();
                IndexWriter.Commit();
                IndexWriter.Close();
                IndexWriter = null;
            }
            if (IndexSearch != null)
            {
                //IndexSearch.Close();
                IndexSearch.Dispose();
                IndexSearch = null;
            }
        }

        private void InitTempIndexWriter()
        {
            if (TempIndexWriter == null)
            {
                TempIndexWriter = new IndexWriter(SearchSettings.Instance.GetTempIndexFileDirectory(), SearchSettings.Instance.GetAnalyzer(), new IndexWriter.MaxFieldLength(10000));
            }
        }


        private void DisposeTempIndexWriter()
        {
            if (TempIndexWriter != null)
            {
                TempIndexWriter.Close();
                TempIndexWriter.Dispose();
                TempIndexWriter = null;
            }

        }


        private void OptimizeAndCloseTempIndex()
        {
            if (TempIndexWriter != null)
            {
                TempIndexWriter.Optimize();
                TempIndexWriter.Commit();
                TempIndexWriter.Close();
                TempIndexWriter = null;
            }

        }

        private bool InitSearch()
        {

            if (IndexSearch == null)
            {
                IndexSearch = new IndexSearcher(SearchSettings.Instance.GetIndexFileDirectory());
            }
            return true;

        }

        public bool UpdateIndexFile()
        {


            try
            {
                IndexLock.EnterWriteLock();

                DirectoryInfo Directory = new DirectoryInfo(SearchSettings.Instance.IndexFolderLocation);

                if (!Directory.Exists)
                {
                    Directory.Create();
                }


                foreach (FileInfo Item in Directory.GetFiles())
                {
                    Item.Delete();

                }

                Directory.Delete();

                System.IO.Directory.Move(SearchSettings.Instance.TempIndexFolderLocation, SearchSettings.Instance.IndexFolderLocation);

                return true;


            }
            catch (Exception ex)
            {
                throw ex;


            }
            finally
            {
                IndexLock.ExitWriteLock();

            }

        }

        public bool AddDocumentToIndex(Document Doc)
        {
            try
            {
                IndexLock.EnterWriteLock();
                //IndexLock.EnterWriteLock()
                InitIndexWriter();
                IndexWriter.AddDocument(Doc);
                OptimizeAndCloseIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool AddDocumentToIndex(List<Document> Doc)
        {
            try
            {
                //IndexLock.EnterWriteLock()
                InitIndexWriter();
                foreach (Document Item in Doc)
                {
                    IndexWriter.AddDocument(Item);
                }
                OptimizeAndCloseIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool AddSearchHistoryDocumentToIndex(List<Document> Doc)
        {
            try
            {

                // init lucene
                var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
                using (var writer = new IndexWriter(SearchSettings.Instance.GetSearchIndexFileDirectory(), analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    // add data to lucene search index (replaces older entries if any)
                    foreach (Document Item in Doc)
                    {
                        var searchQuery = new TermQuery(new Term("TermName", Item.Get("TermName")));
                        writer.DeleteDocuments(searchQuery); //delete if already presence
                        writer.AddDocument(Item);
                    }

                    // close handles
                    analyzer.Close();
                    writer.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool RemoveDocumentFromIndex(List<int> id)
        {

            try
            {
                //IndexLock.EnterWriteLock()
                Term[] term = new Term[id.Count];
                for (int i = 0; i <= id.Count - 1; i++)
                {
                    term[i] = new Term("ID", id[i].ToString());
                }
                InitIndexWriter();
                IndexWriter.DeleteDocuments(term);
                OptimizeAndCloseIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }
        public bool RemoveFileDocumentFromIndex(List<int> id)
        {

            try
            {
                //IndexLock.EnterWriteLock()
                Term[] term = new Term[id.Count];
                for (int i = 0; i <= id.Count - 1; i++)
                {
                    term[i] = new Term("FileID", id[i].ToString());
                }
                InitIndexWriter();
                IndexWriter.DeleteDocuments(term);
                OptimizeAndCloseIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }
        public bool UpdateDocumentInIndex(Document doc)
        {

            try
            {

              

               
                //IndexLock.EnterWriteLock()
                int idToUpdate = 0;
                if (doc.GetField("SearchType").StringValue == "Productions")
                    idToUpdate = Convert.ToInt32(doc.GetField("ID").StringValue);
                else
                    idToUpdate = Convert.ToInt32(doc.GetField("AssetId").StringValue);
                string searchColumn = (doc.GetField("SearchType").StringValue == "Attachments" || doc.GetField("SearchType").StringValue == "Assetlibrary") ? "AssetId" : "ID";
                Term term = new Term(searchColumn, idToUpdate.ToString());
                InitIndexWriter();
                IndexWriter.DeleteDocuments(term);
                IndexWriter.AddDocument(doc);
                OptimizeAndCloseIndex();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool UpdateDocumentInIndex(List<Document> Doc)
        {

            try
            {
                //IndexLock.EnterWriteLock()Dim term(Doc.Count - 1) As Term

                //Term term = new Term((Doc.Count-1).ToString());

                Term[] term = new Term[Doc.Count];
                for (int i = 0; i <= Doc.Count - 1; i++)
                {
                    term[i] = new Term("ID", Doc[i].Get("ID").ToString());
                }

                InitIndexWriter();
                IndexWriter.DeleteDocuments(term);

                foreach (Document Item in Doc)
                {
                    IndexWriter.AddDocument(Item);
                }
                OptimizeAndCloseIndex();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }


        public bool AddDocumentToTempIndex(Document Doc)
        {
            try
            {
                //IndexLock.EnterWriteLock()
                InitTempIndexWriter();
                TempIndexWriter.AddDocument(Doc);
                OptimizeAndCloseTempIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool AddDocumentToTempIndex(List<Document> Doc)
        {
            try
            {
                //IndexLock.EnterWriteLock()
                InitTempIndexWriter();
                foreach (Document Item in Doc)
                {
                    TempIndexWriter.AddDocument(Item);
                }
                OptimizeAndCloseTempIndex();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }

        }

        public bool UpdateAutoCompleteText()
        {
            try
            {
                //IndexLock.EnterWriteLock()
                SearchAutoComplete SAC = new SearchAutoComplete(SearchSettings.Instance.AutoCompleteTempIndexFolderLocation);


                SAC.BuildAutoCompleteIndex(SearchSettings.Instance.GetIndexFileDirectory(), "Name");

                DirectoryInfo Directory = new DirectoryInfo(SearchSettings.Instance.AutoCompleteIndexFolderLocation);

                if (!Directory.Exists)
                {
                    Directory.Create();
                }


                foreach (FileInfo Item in Directory.GetFiles())
                {
                    Item.Delete();

                }

                DirectoryInfo SourceDirectory = new DirectoryInfo(SearchSettings.Instance.AutoCompleteTempIndexFolderLocation);


                foreach (FileInfo Item in SourceDirectory.GetFiles())
                {
                    System.IO.File.Copy(Item.FullName, SearchSettings.Instance.AutoCompleteIndexFolderLocation + "\\" + Item.Name);

                }

                //Directory.Delete()

                //System.IO.Directory.c(SearchSettings.Instance.AutoCompleteTempIndexFolderLocation, SearchSettings.Instance.AutoCompleteIndexFolderLocation)


                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //IndexLock.ExitWriteLock()
            }
        }

        public List<string> GetSuggestedTermsFor(string Text)
        {
            SearchAutoComplete SAC = new SearchAutoComplete(SearchSettings.Instance.AutoCompleteTempIndexFolderLocation);
            SAC.MaxResults = SearchSettings.Instance.MaxResultForAutoComplete;
            List<string> suggetion = SAC.SuggestTermsFor(Text);

            return suggetion;
        }
    }
}
