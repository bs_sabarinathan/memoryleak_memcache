﻿(function (ng, app) {
    "use strict";

    function muiadmincurrencyconvertersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.enddate1 = false;
            model = model1;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.enddate1 = true;
        };
        $(document).ready(function () {
            $(".currencynumeric").keydown(function (e) {
                if ($.inArray(e.keyCode, [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 190, 8, 46, 65, 17, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110]) == -1) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4567.Caption'));
                    e.preventDefault();
                }
            });
        });
        $("#currencyrate").autoNumeric();
        $scope.CurrencyType = 0;
        $scope.Id = 0;
        $scope.IsUpdate = 0;
        $scope.AddOrUpdate = $translate.instant('LanguageContents.Res_8.Caption');
        $scope.ClearAll = function () {
            $scope.AddOrUpdate = $translate.instant('LanguageContents.Res_8.Caption');
            $scope.Id = 0;
            $scope.Startdate = '';
            $scope.Enddate = '';
            $scope.CurrencyType = 0;
            $scope.Currencyrate = '';
        }
        $scope.ClearButton = function () {
            $scope.AddOrUpdate = $translate.instant('LanguageContents.Res_8.Caption');
            $scope.Id = 0;
            $scope.Startdate = '';
            $scope.Enddate = '';
            $scope.CurrencyType = 0;
            $scope.Currencyrate = '';
            getAllvalues();
        }
        $scope.getItemsbyCurrencytype = function (e) {
            $scope.AddOrUpdate = $translate.instant('LanguageContents.Res_8.Caption');
            $scope.IsUpdate = 0;
            $scope.Id = 0;
            $scope.Startdate = '';
            $scope.Enddate = '';
            $scope.Currencyrate = '';
            var objCurrentData1 = [];

            var ID = $scope.CurrencyType;
            AdminService.GetExchangesratesbyCurrencytype(ID).then(function (res) {
                $scope.getEratesbyid = res.Response;
                $scope.Getconverterresponse = [];
                if ($scope.getEratesbyid != null) {
                    for (var i = 0; i < $scope.getEratesbyid.length; i++) {
                        var objCurrentData2 = []
                        objCurrentData2 = $.grep($scope.CurrencyFormatsList, function (e) {
                            return e.Id == parseInt($scope.getEratesbyid[i].Currencytype)
                        });
                        $scope.Getconverterresponse.push({
                            "Id": $scope.getEratesbyid[i].Id,
                            "Startdate": $scope.getEratesbyid[i].Startdate,
                            "Enddate": $scope.getEratesbyid[i].Enddate,
                            "Currencytype": objCurrentData2[0].ShortName,
                            "Currencyrate": $scope.getEratesbyid[i].Currencyrate,
                            "CurrencyId": $scope.getEratesbyid[i].Currencytype
                        });
                    }
                } else { }
            });

            if ($scope.CurrencyType == 0) {
                getAllvalues();
            }
        };

        function getExchangerates() {
            AdminService.GetCurrencyconverter().then(function (getcurrencyconverterresult) {
                if (getcurrencyconverterresult.Response != null) {
                    for (var i = 0; i < getcurrencyconverterresult.Response.length; i++) {
                        objCurrentData = $.grep($scope.CurrencyFormatsList, function (e) {
                            return e.Id == parseInt(getcurrencyconverterresult.Response[i].Currencytype, 10);
                        });
                        $scope.Getconverterresponse.push({
                            "Id": getcurrencyconverterresult.Response[i].Id,
                            "Startdate": getcurrencyconverterresult.Response[i].Startdate,
                            "Enddate": getcurrencyconverterresult.Response[i].Enddate,
                            "Currencytype": objCurrentData[0].ShortName,
                            "Currencyrate": getcurrencyconverterresult.Response[i].Currencyrate,
                            "CurrencyId": getcurrencyconverterresult.Response[i].Currencytype
                        });
                    }
                } else { }
            });
        }
        getAllvalues();

        function getAllvalues() {
            $scope.Getconverterresponse = [];
            $scope.CurrencyFormatsList = [];
            AdminService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                $scope.currencyTypeResponse = CurrencyListResult.Response;
                for (var i = 0; i < CurrencyListResult.Response.length; i++) {
                    $scope.CurrencyFormatsList.push({
                        "Id": CurrencyListResult.Response[i].Id,
                        "ShortName": CurrencyListResult.Response[i].ShortName,
                    });
                }
                getExchangerates();
            });
        }
        $scope.GetCurrrecyconverterdata = {};
        var objCurrentData = [];
        $scope.Getconverterresponse = [];

        function validationsForSavingdata() {
            if ($scope.Startdate == "" || $scope.Enddate == "" || $scope.Startdate == undefined || $scope.Enddate == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_4636.Caption'));
                return false;
            }
            if ($scope.Currencyrate == "" || $scope.Currencyrate == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_4629.Caption'));
                return false;
            }
            if ($scope.CurrencyType == "" || $scope.CurrencyType == undefined || $scope.CurrencyType == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4630.Caption'));
                return false;
            }
            if (ConvertDateToString($scope.Startdate) > ConvertDateToString($scope.Enddate)) {
                bootbox.alert($translate.instant('LanguageContents.Res_4241.Caption'));
                return false;
            }
            //if ($scope.IsUpdate == 1) {
            //    bootbox.alert($translate.instant('LanguageContents.Res_4145.Caption'));
            //    return false;
            //}
            return true;
        }

        function IsDateExist() {
            var IsValid = true;
            for (var i = 0; i < $scope.Getconverterresponse.length; i++) {
                var strtdate = ConvertStringToDate(getdatebystring($scope.Getconverterresponse[i].Startdate));
                var enddate = ConvertStringToDate(getdatebystring($scope.Getconverterresponse[i].Enddate));
                if ((strtdate < $scope.Startdate && enddate > $scope.Startdate) || (strtdate < $scope.Enddate && enddate > $scope.Enddate) || (strtdate > $scope.Startdate && enddate < $scope.Enddate)) {
                    IsValid = false;
                    return false;
                }
            }
            return IsValid;
        }
        $scope.SaveCurrencyconverterData = function () {
            if (validationsForSavingdata() == true) {
                $("#UpdatingCurrencyRate").modal('show');
                var insertupdateFormat = {};
                insertupdateFormat.Id = $scope.Id;
                var sdate = new Date($scope.Startdate).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat(($scope.Startdate).toString(), GlobalUserDateFormat));
                }
                var edate = new Date($scope.Enddate).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = ConvertDateToString(ConvertStringToDateByFormat(($scope.Enddate).toString(), GlobalUserDateFormat));
                }
                insertupdateFormat.Startdate = ConvertDateToString(sdate);
                insertupdateFormat.Enddate = ConvertDateToString(edate);
                insertupdateFormat.Currencytype = $scope.CurrencyType;
                insertupdateFormat.Currencyrate = $scope.Currencyrate;
                var objCurrentData = $.grep($scope.CurrencyFormatsList, function (item, i) {
                    return item.ID == $scope.CurrencyType;
                });
                AdminService.Insertupdatecurrencyconverter(insertupdateFormat).then(function (saveconverterresult) {
                    if (saveconverterresult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4272.Caption'));
                        $("#UpdatingCurrencyRate").modal('hide');
                        return true;
                    } else {
                        $("#UpdatingCurrencyRate").modal('hide');
                        $scope.Getconverterresponse = [];
                        var objCurrentData1 = [];
                        var ID = $scope.CurrencyType;
                        getExchangerates();
                        if ($scope.CurrencyType == 0) {
                            getAllvalues();
                        }
                        $scope.ClearAll();
                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    }
                });
            }
        };
        $scope.DeleteCurrencyconverter = (function (row, Index) {
            $("#UpdatingCurrencyRate").modal('show');
            $scope.ClearAll();
            bootbox.confirm($translate.instant('LanguageContents.Res_4996.Caption'), function (result) {
                if (result) {
                    var ID = row.Id;
                    AdminService.DeleteCurrencyconverter(ID).then(function (deletecurrencybyid) {
                        if (deletecurrencybyid.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4296.Caption'));
                            $("#UpdatingCurrencyRate").modal('hide');
                        } else {
                            $scope.Getconverterresponse.splice(Index, 1);
                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            $("#UpdatingCurrencyRate").modal('hide');
                        }
                    });
                }
            });
        });

        function getdatebystring(Value) {
            var i = Value.substr(5, 3);
            var j;
            switch (i) {
                case 'Jan':
                    j = "01";
                    break;
                case 'Feb':
                    j = "02";
                    break;
                case 'Mar':
                    j = "03";
                    break;
                case 'Apr':
                    j = "04";
                    break;
                case 'May':
                    j = "05";
                    break;
                case 'Jun':
                    j = "06";
                    break;
                case 'Jul':
                    j = "07";
                    break;
                case 'Aug':
                    j = "08";
                    break;
                case 'Sep':
                    j = "09";
                    break;
                case 'Aug':
                    j = "10";
                    break;
                case 'Nov':
                    j = "11";
                    break;
                case 'Dec':
                    j = "12";
            }
            var k = Value.substr(0, 4) + "-" + j + "-" + Value.substr(9, 11);
            return k;
        }
        $scope.UpdateCurrencyconverter = function (row, Index) {
            $scope.AddOrUpdate = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.IsUpdate = 2;
            var currencyval = $.grep($scope.Getconverterresponse, function (e) {
                return parseInt(e.Id) == parseInt(row.Id);
            });
            $scope.Id = currencyval[0].Id;
            $scope.Startdate = currencyval[0].Startdate;
            $scope.Enddate = currencyval[0].Enddate;
            $scope.Currencyrate = currencyval[0].Currencyrate;
            $scope.CurrencyType = currencyval[0].CurrencyId;
        }
    }
    app.controller("mui.admin.currencyconvertersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadmincurrencyconvertersettingsCtrl]);
})(angular, app);