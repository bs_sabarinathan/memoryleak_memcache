﻿using BrandSystems.Marcom.Core.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User
{
    class LoginMails : ILoginMails, ICloneable
    {
  
        #region Member Variables
        
        protected int _id;
		protected string _email;
		protected string _fileguid;
		protected string _captcha;
        protected string _unlockedby;
        protected DateTime _unlockedtime;
        protected bool _bIsChanged;
        #endregion
        	
		#region Constructors
		public LoginMails() {}

        public LoginMails(int Id, string Email, string FileGuId, string Captcha, string Unlockedby, DateTime UnlockedTime)
		{
            this._id = Id;
            this._email = Email;
            this._fileguid = FileGuId;
            this._captcha = Captcha;
            this._unlockedby = Unlockedby;
            this._unlockedtime = UnlockedTime;
		}
                
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        public virtual string Email
        {
            get { return _email; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 50 characters");
                _bIsChanged |= (_email != value);
                _email = value;
            }

        }
        public virtual string FileGuId
        {
            get { return _fileguid; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("FileGuId", "FileGuId value, cannot contain more than 50 characters");
                _bIsChanged |= (_fileguid != value);
                _fileguid = value;
            }

        }
        public virtual string Captcha
        {
            get { return _captcha; }
            set
            {
                if (value != null && value.Length > 30)
                    throw new ArgumentOutOfRangeException("Captcha", "Captcha value, cannot contain more than 15 characters");
                _bIsChanged |= (_captcha != value);
                _captcha = value;
            }

        }
        public virtual string Unlockedby
        {
            get { return _unlockedby; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Unlockedby", "Unlockedby value, cannot contain more than 50 characters");
                _bIsChanged |= (_unlockedby != value);
                _unlockedby = value;
            }

        }
        public virtual DateTime UnlockedTime
        {
            get { return _unlockedtime; }
            set { _bIsChanged |= (_unlockedtime != value); _unlockedtime = value; }

        }
               
        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

      

		#endregion 				
		
        #region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion


    }
}
