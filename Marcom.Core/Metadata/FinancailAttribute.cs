﻿using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
    {
    class FinancailAttribute : IFinancialAttribute
        {
        #region Public Properties

        public int _id { get; set; }
        public int _finTypeID { get; set; }
        public string _caption { get; set; }
        public int _attributeTypeID { get; set; }
        public string _description { get; set; }
        public bool _isSystemDefined { get; set; }
        public bool _isColumn { get; set; }
        public bool _isTooltip { get; set; }
        public bool _isCommitTooltip { get; set; }
        public int _SortOrder { get; set; }
        public bool _IsHelptextEnabled { get; set; }
        public string _HelptextDecsription { get; set; }

        #endregion


           #region Constructors
        public FinancailAttribute() { }
        public FinancailAttribute(int pid, int pfintypeid, string pcaption, int pattributetypeid, bool pissystemdefined, string pdescription, bool piscolumn, bool pistooptip, bool piscommittooltip, int pSortOrder)
            {
            this._id = pid;
            this._finTypeID = pfintypeid;
            this._caption = pcaption;
            this._attributeTypeID = pattributetypeid;
            this._isSystemDefined = pissystemdefined;
            this._description = pdescription;
            this._isColumn = piscolumn;
            this._isTooltip = pistooptip;
            this._isCommitTooltip = piscommittooltip;
            this._SortOrder = pSortOrder;
            }

        public FinancailAttribute(int pId)
            {
            this._id = pId;
            }
        #endregion


        #region Public Properties

        public virtual int ID
            {
            get { return _id; }
            set { _id = value; }

            }

        public virtual int FinTypeID
            {
            get { return _finTypeID; }
            set { _finTypeID = value; }
            }

        public virtual string Caption
            {
            get { return _caption; }
            set { _caption = value; }
            }



        public virtual int AttributeTypeID
            {
            get { return _attributeTypeID; }
            set { _attributeTypeID = value; }
            }

        public virtual bool IsSystemDefined
            {
            get { return _isSystemDefined; }
            set { _isSystemDefined = value; }
            }


        public virtual string Description
            {
            get { return _description; }
            set { _description = value; }
            }


        public virtual bool IsColumn
            {
            get { return _isColumn; }
            set { _isColumn = value; }
            }

        public virtual bool IsTooltip
            {
            get { return _isTooltip; }
            set { _isTooltip = value; }
            }

        public virtual bool IsCommitTooltip
            {
            get { return _isCommitTooltip; }
            set { _isCommitTooltip = value; }
            }

        public virtual int SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; }

        }
        public virtual bool IsHelptextEnabled
        {
            get { return _IsHelptextEnabled; }
            set { _IsHelptextEnabled = value; }
        }

        public virtual string HelptextDecsription
        {
            get { return _HelptextDecsription; }
            set { _HelptextDecsription = value; }
        }


        #endregion


        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
            {
            if (this == obj) return true;
            FinancailAttribute castObj = null;
            try
                {
                castObj = (FinancailAttribute)obj;
                }
            catch (System.Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
            }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {

            return this.GetType().FullName.GetHashCode();

            }
        #endregion

        }
    }
