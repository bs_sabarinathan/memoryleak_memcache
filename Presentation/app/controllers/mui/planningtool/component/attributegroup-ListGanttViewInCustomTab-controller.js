﻿(function (ng, app) {

    "use strict";
    function attributegrouplistganttviewincustomtabCtrl($scope, $timeout, $resource, $compile, $stateParams, $cookies, AttributegrouplistganttviewincustomtabService, $translate) {   // I handle changes to the request context.
        var model;

        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
        };

        $scope.SearchCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.searchfields["DatePart_Calander_Open" + model] = true;

        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.format = 'dd/MM/yyyy';

        $scope.sortColumnBy = "";
        $scope.IsPredefDataPresent = false;
        $scope.loading = true;
        $scope.SortOrder = 0;
        $scope.ShowHideSaveGroup = true;
        $scope.ShowHideEditGroup = false;
        $scope.ShowHideAddAttributeGroupBtn = true;
        $scope.AttributeGroupRecordID = 0;
        $scope.AGRecordList = [];
        $scope.HeaderCaptions = [];
        $scope.IsListView = true;
        $scope.ImportDataColumnNames = [];
        $scope.BindLabelAndColumn = [];
        $scope.ImportFileName = "";
        $scope.ImportedFileId = "";
        $scope.showhideimport = false;
        $scope.LoadingWheelCaption = "";
        $scope.IsUploadshow = true;
        $scope.treesrcdirec = {};
        $scope.treeNodeSelectedHolder = new Array();
        var treeTextVisbileflag = false;
        $scope.treePreviewObj = {};
        $scope.Noofcolumn = $scope.HeaderCaptions.length == undefined ? 0 : $scope.HeaderCaptions.length + 1;
        $scope.pagination = {
            currentPage: 1,
            totalitems: 0
        };
        $scope.reverse = true;
        $scope.sortKey = "Name";



        //------------------> GANTT VIEW DECLARATION
        $scope.GanttDetail = {
            GanttStartDate: "",
            GanttEndDate: "",
            GanttDescription: ""
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
            $scope.sortColumnBy = "Attr" + keyname.ID;
            $scope.loadpredefinedData();
        }

        $scope.data = {
            start: "", end: "", desc: "", SortOid: "", order: "", uniqueId: "", id: ""
        }
        var dataGantEdit = {
            start: "", end: "", desc: "", SortOid: "", order: "", uniqueId: "", id: ""
        }

        $scope.predefinedpagination = {
            currentPage: 1,
            totalitems: 0,
            PageSize: 20
        };

        $scope.checkedAttrGrpInpag = [];

        $scope.PredefinedpageChanged = function (newPage) {
            $scope.predefinedpagination.currentPage = newPage;
            $scope.loadpredefinedData();
        };

        if ($scope.itemPredefined == "false") {
            $scope.IsPredefinedAttrGroup = false;
        }
        else {
            $scope.IsPredefinedAttrGroup = true;
        }

        if ($scope.itemIsLock == "") {
            $scope.itemIsLock = true;
        }

        var IsResized = false;
        $scope.settings = {
            Mode: 'monthly',
            MonthlyDayWidth: 30,
            YearlyDayWidth: 2,
            QuarterlyDayWidth: 6,
            WeeklyDayWidth: 16,
            VerticallyConnected: null,
            HorizontallyConnected: null,
            DataColumnWidth: 100,
            StartDate: Date.parse('-1year').set({ day: 1, month: 0 }).toString('yyyy-MM-dd'), //Date.parse('2013-01-01').toString('yyyy-MM-dd'), //
            EndDate: Date.parse('+1year').set({ day: 31, month: 11 }).toString('yyyy-MM-dd'), //Date.parse('2013-12-31').toString('yyyy-MM-dd'), //
            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                onClick: function (data, BlockID) {
                    if (IsResized == false) {
                        $scope.GanttCurrentDateCollections = {};
                        AttributegrouplistganttviewincustomtabService.GetEntityPeriod(data.id).then(function (ganttDateRes) {
                            if (ganttDateRes.Response != null) {
                                $scope.GanttCurrentDateCollections = [{
                                    Description: "", EndDate: ""
                                    , Entityid: 0
                                    , Id: 0
                                    , SortOrder: 0
                                    , Startdate: ""
                                }]
                                $scope.GanttCurrentDateCollections.splice(0, 1);
                                for (var i = 0; i < ganttDateRes.Response.length; i++) {

                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = ganttDateRes.Response[i].Startdate.substr(6, (ganttDateRes.Response[i].Startdate.indexOf('+') - 6));
                                    datstartval = new Date.create(parseInt(datStartUTCval));
                                    datEndUTCval = ganttDateRes.Response[i].EndDate.substr(6, (ganttDateRes.Response[i].EndDate.indexOf('+') - 6));
                                    datendval = new Date.create(parseInt(datEndUTCval));


                                    $scope.GanttCurrentDateCollections.push({
                                        Description: ganttDateRes.Response[i].Description
                                         , EndDate: ConvertStringToDate(ConvertDateToString(datendval))//ganttDateRes.Response[i].EndDate
                                         , Entityid: ganttDateRes.Response[i].Entityid
                                         , Id: ganttDateRes.Response[i].Id
                                         , SortOrder: ganttDateRes.Response[i].SortOrder
                                         , Startdate: ConvertStringToDate(ConvertDateToString(datstartval))//ganttDateRes.Response[i].Startdate
                                    });
                                }
                            }
                        });


                        getCaptionfromPerod(data.id);

                        dataGantEdit.SortOid = data.SortOid
                        dataGantEdit.order = data.order
                        dataGantEdit.uniqueId = data.uniqueId
                        dataGantEdit.id = data.id
                        dataGantEdit.BlockID = BlockID
                        $("#moduleContextGantt").modal('show');
                    }
                    IsResized = false;
                },
                onDrag: function (data) {

                    EntityPeriodUpdate(data);

                },
                onResize: function (data) {

                    EntityPeriodUpdate(data);
                    IsResized = true;

                }
            },
        };

        $scope.pageChanged = function (newPage) {
            $scope.pagination.currentPage = newPage;
            $scope.InitLoad();
        };

        $scope.IsPredefinedAttrGroup = false;

        //----------------------------------------> ATTRIBUTE GROUP LIST VIEW CODE STARTS HERE <-------------------------
        $scope.InitLoad = function () {
            if ($scope.itemPredefined == "false") {
                $scope.IsPredefinedAttrGroup = false;
            }
            else {
                $scope.IsPredefinedAttrGroup = true;
            }
            AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, $scope.itemGroupId, $scope.itemIsLock, $scope.pagination.currentPage, $scope.itemPagesize).then(function (entitygroupvalues) {
                if (entitygroupvalues.Response != null && entitygroupvalues.Response.length > 0) {
                    $scope.AGRecordList = entitygroupvalues.Response == null ? "" : entitygroupvalues.Response;
                    if ($scope.AGRecordList.length > 0) {
                        $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                        $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                        $scope.HidePagination = true;
                        $scope.showHideItemDiv = false;
                        $scope.ShowHideAddAttributeGroupBtn = false;
                        $scope.HeaderCaptions = entitygroupvalues.Response[0];
                        $scope.LoadListViewHeader();
                        $scope.Noofcolumn = $scope.HeaderCaptions.length + 1;
                        if ($scope.BindLabelAndColumn.length > 0)
                            $scope.BindLabelAndColumn.splice(0, $scope.BindLabelAndColumn.length);
                        for (var i = 0; i < entitygroupvalues.Response[0].length; i++) {
                            if (entitygroupvalues.Response[0][i].TypeID == 6 || entitygroupvalues.Response[0][i].TypeID == 12) {
                                for (var j = 0; j < entitygroupvalues.Response[0][i].Lable.length; j++) {
                                    $scope.BindLabelAndColumn.push({ AttributeID: entitygroupvalues.Response[0][i].ID, AttributeType: entitygroupvalues.Response[0][i].TypeID, Level: entitygroupvalues.Response[0][i].Lable[j].Level, AttributeCaption: entitygroupvalues.Response[0][i].Lable[j].Label, ColumnName: "", Value: "" });
                                }
                            }
                            else
                                $scope.BindLabelAndColumn.push({ AttributeID: entitygroupvalues.Response[0][i].ID, AttributeType: entitygroupvalues.Response[0][i].TypeID, Level: 0, AttributeCaption: entitygroupvalues.Response[0][i].Lable, ColumnName: "", Value: "" });
                        }
                    }
                }
                else {
                    //           var GetEntityAttributesGroupLabelNames = $resource('metadata/GetEntityAttributesGroupLabelNames/:EntityID/:GroupID', { EntityID: parseInt($stateParams.ID), GroupID: $scope.itemGroupId }, { get: { method: 'GET' } });
                    AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupLabelNames(parseInt($stateParams.ID), $scope.itemGroupId).then(function (entitygrouplabelnames) {
                        //             var entitygrouplabelnames = GetEntityAttributesGroupLabelNames.get({ EntityID: parseInt($stateParams.ID), GroupID: $scope.itemGroupId }, function () {
                        if (entitygrouplabelnames.Response != null) {
                            for (var i = 0; i < entitygrouplabelnames.Response[0].length; i++) {
                                if (entitygrouplabelnames.Response[0][i].TypeID == 6 || entitygrouplabelnames.Response[0][i].TypeID == 12)
                                    for (var j = 0; j < entitygrouplabelnames.Response[0][i].Lable.length; j++) {
                                        $scope.BindLabelAndColumn.push({ AttributeID: entitygrouplabelnames.Response[0][i].ID, AttributeType: entitygrouplabelnames.Response[0][i].TypeID, Level: entitygrouplabelnames.Response[0][i].Lable[j].Level, AttributeCaption: entitygrouplabelnames.Response[0][i].Lable[j].Label, ColumnName: "", Value: "" });
                                    }
                                else
                                    $scope.BindLabelAndColumn.push({ AttributeID: entitygrouplabelnames.Response[0][i].ID, AttributeType: entitygrouplabelnames.Response[0][i].TypeID, Level: 0, AttributeCaption: entitygrouplabelnames.Response[0][i].Lable, ColumnName: "", Value: "" });
                            }
                        }
                    });

                    $scope.showHideItemDiv = true;
                    $scope.ShowHideAddAttributeGroupBtn = true;
                    $scope.HidePagination = false;
                }
                $scope.loading = false;
            });
        }

        $scope.$on("processlistganttintab" + $scope.itemGroupId, function (event) {
            $scope.AGRecordList.length = 0;
            $scope.InitLoad();
            if ($scope.itemPredefined == "false") {
                $scope.IsPredefinedAttrGroup = false;
            }
            else {
                $scope.IsPredefinedAttrGroup = true;
            }
        });

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);

                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
        };

        $scope.AddAttributeValues = function (valattrRecID, SortOrder) {
            $("#AddAttributesModel" + $scope.itemGroupId).modal('show');
            $scope.GantColorCodeGlobalObj = {};
            $scope.GantColorCodeGlobalObj.colorcode = 'ffffff';


            $scope.ColorOptions = {
                preferredFormat: "hex",
                showInput: true,  //to show user to type their color code
                showAlpha: false,
                allowEmpty: true,
                showPalette: true, //Spectrum can show a palette below the colorpicker to make it convenient for users to choose from frequently or recently used colors. 
                showPaletteOnly: false, //If you'd like, spectrum can show the palettes you specify, and nothing else.
                togglePaletteOnly: true, //Spectrum can show a button to toggle the colorpicker next to the palette.
                togglePaletteMoreText: 'more', //text for show palette
                togglePaletteLessText: 'less', //text for hide palette
                showSelectionPalette: true, // true by default . Spectrum can keep track of what has been selected by the user
                chooseText: "Choose",
                cancelText: "Cancel",
                showButtons: true,
                clickoutFiresChange: true,

                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            };


            $scope.colorchange = function (color) { }



            $scope.itemImagefilename = "";
            $scope.SortOrder = SortOrder == undefined ? 0 : SortOrder;
            $scope.ShowHideAttributeOnRelation = {};
            $scope.EnableDisableControlsHolder = {};
            $scope.OptionObj = {

            };
            $scope.UserimageNewTime = new Date.create().getTime().toString();
            $scope.items = [];
            // option holder part for dynamic controls START
            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];

                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });

            }
            $scope.dyn_Cont = '';
            $scope.fields = {
                usersID: ''
            };
            $scope.fieldKeys = [];
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.treeSources = {};
            $scope.treeSourcesObj = [];
            $scope.UploadAttributeData = [];
            $scope.settreeSources = function () {
                var keys = [];
                angular.forEach($scope.treeSources, function (key) {
                    keys.push(key);
                    $scope.treeSourcesObj = keys;
                });
            }
            $scope.treeTextsObj = [];
            $scope.settreeTexts = function () {
                var keys2 = [];
                angular.forEach($scope.treeTexts, function (key) {
                    keys2.push(key);
                    $scope.treeTextsObj = keys2;
                });
            }
            $scope.treelevelsObj = [];
            $scope.settreelevels = function () {
                var keys1 = [];
                angular.forEach($scope.Inherritingtreelevels, function (key) {
                    keys1.push(key);
                    $scope.treelevelsObj = keys1;
                });
            }
            $scope.Dropdown = [];
            $scope.optionsLists = [];
            var tempAttributeRecordID = 0;
            if (valattrRecID != undefined) {
                tempAttributeRecordID = valattrRecID;
                $scope.AttributeGroupRecordID = valattrRecID;
                $scope.ShowHideSaveGroup = false;
                $scope.ShowHideEditGroup = true;
            }
            else {
                $scope.AttributeGroupRecordID = 0;
                $scope.ShowHideSaveGroup = true;
                $scope.ShowHideEditGroup = false;
            }

            $timeout(function () {
                $('#AddAttributesModel' + $scope.itemGroupId).modal('show'), 300
            });

            AttributegrouplistganttviewincustomtabService.GetAttributeGroupAttributeOptions($scope.itemGroupId, tempAttributeRecordID, parseInt($stateParams.ID)).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                $scope.dyn_Cont = '';
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;

                                if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                        return e.id == $scope.atributesRelationList[i].AttributeValue[j];
                                    })[0];
                                }
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            }
                            else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                    if ($scope.atributesRelationList[i].AttributeValue[j - 1] != undefined)
                                        $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, j, totLevelCnt, 6);

                                    if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                        $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                            return e.id == $scope.atributesRelationList[i].AttributeValue[j];
                                        })[0];
                                    }
                                }
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) {
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        GetTreeObjecttoSave($scope.atributesRelationList[i].AttributeID);
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                            if (j == 0) {
                                $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                        return e.id == $scope.atributesRelationList[i].AttributeValue[j];
                                    })[0];
                                }
                                $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + $scope.atributesRelationList[i].Levels.length + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            }
                            else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                if ($scope.atributesRelationList[i].AttributeValue[j - 1] != undefined)
                                    $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, j, $scope.atributesRelationList[i].Levels.length, 12);

                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                        var tempval = [];
                                        var k = j;
                                        for (k; k < $scope.atributesRelationList[i].AttributeValue.length; k++) {
                                            tempval.push($.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == $scope.atributesRelationList[i].AttributeValue[k];
                                            })[0]);
                                        }
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = tempval;
                                    }
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + $scope.atributesRelationList[i].Levels.length + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                                else {
                                    if ($scope.atributesRelationList[i].AttributeValue[j] != undefined) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                            return e.id == $scope.atributesRelationList[i].AttributeValue[j];
                                        })[0];
                                    }
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        for (k; k < (CaptionObj.length) ; k++) {
                                            if (CaptionObj[k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push(CaptionObj[k].trim());
                                            }
                                            else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    }
                                    else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                            $scope.settreeTexts();

                                        }
                                        else {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID == 80) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls colorPickerContainer\"><input type='text' class=\"margin-bottom10x\" maxlength='6' name='colorcode' ng-model='GantColorCodeGlobalObj.colorcode' id='Text1' required><uicolorpicker ng-model='GantColorCodeGlobalObj.colorcode' options='ColorOptions'></uicolorpicker></div></div>";
                            if (valattrRecID != undefined) {
                                $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue == null ? "ffffff" : $scope.atributesRelationList[i].AttributeValue;
                                $scope.GantColorCodeGlobalObj.colorcode = $scope.atributesRelationList[i].AttributeValue == null ? "ffffff" : $scope.atributesRelationList[i].AttributeValue;
                                $(".current-color").css('background-color', "#" + $scope.GantColorCodeGlobalObj.colorcode);
                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ></div></div>";
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue == null ? "-" : $scope.atributesRelationList[i].AttributeValue;
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue == null ? "-" : $scope.atributesRelationList[i].AttributeValue;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\"  rows=\"3\"></textarea></div></div>";
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        $scope.dyn_Cont += "<select class=\"multiselect\"  multiselect-dropdown   ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        if ($scope.atributesRelationList[i].AttributeValue == "") {
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        }
                        else {
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = dateFormat($scope.atributesRelationList[i].AttributeValue, GlobalUserDateFormat);
                        }
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" ></div></div>";
                        $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue;
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].AttributeValue;
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeValue != "" && $scope.atributesRelationList[i].AttributeValue != null) {
                            $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "UploadedImages/AttributeGroupImport/" + $scope.atributesRelationList[i].AttributeValue;
                            $scope.itemImagefilename = $scope.atributesRelationList[i].AttributeValue;
                        }
                        else {
                            $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = 'UploadedImages/AttributeGroupImport/noPreview.jpg';
                            $scope.itemImagefilename = "";
                        }
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group attrGrpImgUploadSection ng-scope"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        $scope.dyn_Cont += '<img class="widthauto heightauto nomargin entityDetailImgPreview ng-pristine ng-valid"';
                        $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl' + $scope.itemGroupId + '" ';
                        $scope.dyn_Cont += " src=" + $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] + " alt='No thumbnail present'>";
                        $scope.dyn_Cont += '<div id="uploadprogress' + $scope.itemGroupId + '" style="display: none;" class="attrGrpImgUploadProgress progress progress-striped active"><div style="width:0%" class="bar"></div></div>'
                        $scope.dyn_Cont += '<a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(itemGroupId)" data-toggle="modal" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                    }
                }
                $scope.dyn_Cont += '<input style="display: none;" type="submit" id="btnTempSub" class="ng-scope" invisible>';
                $("#AttributeGroup" + $scope.itemGroupId).html('');
                $("#AttributeGroup" + $scope.itemGroupId).html('<div class="row-fluid"><div data-col="1" class="span4" style="width: 202px;"></div><div data-col="2" class="span8"></div></div> ');
                $("#AttributeGroup" + $scope.itemGroupId).html(
                    $compile($scope.dyn_Cont)($scope));
                $(".savedisable").removeAttr("disabled");
            });

        }

        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.treesrcdirec["Attr_" + attributeid]);
        }

        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {
            if (treeobj.length > 0) {
                for (var i = 0, node; node = treeobj[i++];) {

                    if (node.ischecked == true) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(node);
                        }
                        treeformflag = false;
                        if (ischildSelected(node.Children)) {
                            GenerateTreeStructure(node.Children);
                        }
                        else {
                            GenerateTreeStructure(node.Children);
                        }
                    }
                    else
                        GenerateTreeStructure(node.Children);
                }

            }
        }

        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        $scope.SaveAttribtueGroup = function SaveAttribtueGroup() {
            $(".savedisable").prop("disabled", true);
            $scope.AttributeData = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                    });
                                }
                            }
                            else {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt(value, 10),
                                "Level": 0
                            });

                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else if ($scope.atributesRelationList[i].AttributeID == 80) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.GantColorCodeGlobalObj.colorcode.replace("#", ""),
                            "Level": 0
                        });
                    }
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null || $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            var datestring = formatteddateFormat(($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]), "yyyy/MM/dd");
                            MyDateString = (datestring).toString("yyyy/MM/dd");
                        }
                        else {
                            MyDateString = "";
                        }
                    }
                    else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0
                    });
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": 0,
                                "Level": 0
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.itemImagefilename,
                        "Level": 0
                    });
                }
            }
            var SaveEntity = {};
            SaveEntity.AttributeGroupRecordID = $scope.AttributeGroupRecordID;
            SaveEntity.ParentId = parseInt($stateParams.ID);
            SaveEntity.Typeid = 0;
            SaveEntity.GroupID = $scope.itemGroupId;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.itemTitle;
            SaveEntity.SortOrder = $scope.SortOrder;
            SaveEntity.AttributeData = $scope.AttributeData;
            AttributegrouplistganttviewincustomtabService.CreateAttributeGroupRecord(SaveEntity).then(function (EntityList) {
                if (EntityList.Response > 0) {
                    AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, $scope.itemGroupId, $scope.itemIsLock, $scope.pagination.currentPage, $scope.itemPagesize).then(function (entitygroupvalues) {
                        if (entitygroupvalues.Response != null && entitygroupvalues.Response.length > 0) {
                            $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                            $scope.AGRecordList = entitygroupvalues.Response;
                            $scope.HidePagination = true;
                            if ($scope.HeaderCaptions.length <= 0) {
                                $scope.HeaderCaptions = entitygroupvalues.Response[0];
                                $scope.Noofcolumn = $scope.HeaderCaptions.length + 1;
                                $scope.showHideItemDiv = false;
                            }
                            if (entitygroupvalues.Response.length > 0)
                                $scope.ShowHideAddAttributeGroupBtn = false;
                            else
                                $scope.ShowHideAddAttributeGroupBtn = true;
                            $scope.EntityMemberData = [];
                            $scope.MemberLists = [];
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            $('#AddAttributesModel' + $scope.itemGroupId).modal('hide');
                            $scope.LoadListViewHeader();
                        }
                    });
                    $timeout(function () {
                        setSort();
                    }, 300);

                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $('#AddAttributesModel' + $scope.itemGroupId).modal('hide');
                }
                $scope.itemImagefilename = "";
            });

        }

        $scope.DeleteAttribtueRecord = function DeleteAttribtueRecord(RecordID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    AttributegrouplistganttviewincustomtabService.DeleteEntityAttributeGroupRecord($scope.itemGroupId, RecordID, parseInt($stateParams.ID)).then(function (deleteAttributeById) {
                        if (deleteAttributeById.Response == true) {
                            if ($scope.AGRecordList.length == 1)
                                $scope.pagination.currentPage--;
                            AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, $scope.itemGroupId, $scope.itemIsLock, $scope.pagination.currentPage, $scope.itemPagesize).then(function (entitygroupvalues) {
                                if (entitygroupvalues.Response != null && entitygroupvalues.Response.length > 0) {
                                    $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                                    $scope.AGRecordList = entitygroupvalues.Response;
                                    if (entitygroupvalues.Response.length > 0) {
                                        $scope.ShowHideAddAttributeGroupBtn = false;
                                        $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                                    }
                                    else {
                                        $scope.HeaderCaptions.splice(0, $scope.HeaderCaptions.length);
                                        $scope.AGRecordList.splice(0, $scope.AGRecordList.length);
                                        $scope.ShowHideAddAttributeGroupBtn = true;
                                        $scope.showHideItemDiv = true;
                                        $scope.HidePagination = false;
                                        $("#Listviewheader").html('');
                                    }
                                    //$scope.LoadListViewHeader();
                                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                }
                                else if (entitygroupvalues.Response.length == 0) {
                                    $scope.HeaderCaptions.splice(0, $scope.HeaderCaptions.length);
                                    $scope.AGRecordList.splice(0, $scope.AGRecordList.length);
                                    $scope.ShowHideAddAttributeGroupBtn = true;
                                    $scope.showHideItemDiv = true;
                                    $scope.HidePagination = false;
                                    $("#Listviewheader").html('');
                                }
                            });
                        }
                        else {
                            NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                        }
                    });
                }
            });
        }
        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToHide = [];
                var hideAttributeOtherThanSelected = [];

                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;

                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        try {
                            if ($scope.Dropdown["OptionValues" + attrID + "_" + j] != undefined && $scope.Dropdown["OptionValues" + attrID + "_" + j] != "")
                                $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        } catch (e) { }
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------

                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
            }
            catch (e) { }
        }

        $scope.EditAttributeValues = function (val) {
            $scope.itemImagefilename = "";
            $('#AddAttributesModel' + $scope.itemGroupId).modal('show');
            $scope.AddAttributeValues(val);
        };

        //--------------> UPLOAD IMAGE FILE AND CROP <---------------

        $scope.UploadImagefile = function (grpid) {
            $scope.itemImagecrop(grpid);
            $("#pickfiles100").click();
        };

        //----------------------------------------> ATTRIBUTE GROUP LIST VIEW ENDS HERE <-------------------------

        //----------------------------------------> ATTRIBUTE GROUP GANTT VIEW STARTS HERE <-------------------------

        $scope.GanttViewLoad = function (modeofview) {
            $scope.settings.Mode = modeofview;
            $scope.IsListView = false;
            var height = 34 * 20;
            $scope.GanttViewDataPageTemplate1 = "";
            $scope.GanttViewBlockPageTemplate1 = "";
            $scope.GanttViewDataPageTemplate1 += '<tbody class="widthcent" data-page="0"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>';
            $scope.GanttViewBlockPageTemplate1 += '<div data-page="0" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            $scope.GanttViewDataPageTemplate1 += '<tbody class="blankTreeNode"></tbody>';

            $timeout(function () {

                $("#ganttChart #GanttDataContainer").html($scope.GanttViewDataPageTemplate1);
                $("#ganttChart #GanttBlockContainer").html($scope.GanttViewBlockPageTemplate1);

                $('#ganttChart .ganttview-data').scroll(function () {
                    $("#ganttChart .ganttview-slide-container").scrollLeft($(this).scrollLeft());
                    $("#ganttChart .ganttview-list-container-data").scrollTop($(this).scrollTop());
                });

                var tempdates = getDates($scope.settings.StartDate, $scope.settings.EndDate);
                drawDataHeader();
                drawHeader(tempdates);

                LoadGanttView($scope.AGRecordList, 0, 0);
                ScrollToCurrentDate();

                $(window).AdjustHeightWidth();
            }, 100);
        }

        function drawDataHeader() {
            var HeaderHtml = '';
            var totalColumn = 1;
            HeaderHtml += '<th><span>Name</span></th>';
            $('#ganttChart #ganttviewDataHeader').html(HeaderHtml);

            $('.ganttview-left').width(totalColumn * 200);
            $('.ganttview-right').css('margin-left', (totalColumn * 200) + 'px');
        }

        function drawHeader(dates) {

            var totalW = 0;

            var mainHeaderDivContent = '';
            var subHeaderDivContent = '';
            var rowDivContent = '';
            var ganttHeaderBar = '';

            switch ($scope.settings.Mode) {
                case 'daily':
                    for (var y in dates) {
                        if (dates.hasOwnProperty(y)) {
                            for (var m = 0; m < dates[y].length; m++) {
                                var w = dates[y][m].length * $scope.settings.MonthlyDayWidth;
                                totalW = totalW + w;
                                mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';

                                for (var d in dates[y][m]) {
                                    if (dates[y][m].hasOwnProperty(d)) {
                                        if (isWeekend(dates[y][m][d])) {
                                            subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd">' + dates[y][m][d].getDate() + '</div>';
                                            rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                        } else {
                                            subHeaderDivContent += '<div class="ganttview-header-sub">' + dates[y][m][d].getDate() + '</div>';
                                            rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        if ($scope.GanttHeaderList.hasOwnProperty(x)) {
                            var startDt = $scope.GanttHeaderList[x].Startdate;
                            var endDt = $scope.GanttHeaderList[x].EndDate;
                            var duration = 0;
                            var formLeft = 0;
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth);
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            ganttHeaderBar += '<div class="qtip-header-block" my-qtip2 qtip-content="<b>Name:' + $scope.GanttHeaderList[x].Name.replace(/["']/g, "") + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                        }
                    }
                    break;
                case 'monthly':
                    var possion = 1;
                    //Draw monthly header
                    for (var y in dates) {
                        var yw = 0;
                        if (dates.hasOwnProperty(y)) {
                            for (var m = 0; m < dates[y].length; m++) {
                                var w = dates[y][m].length * $scope.settings.YearlyDayWidth;
                                totalW = totalW + w;
                                yw = yw + w;
                                if ((possion % 2) == 1) {
                                    subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                    rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                } else {
                                    subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                    rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                }
                                possion += 1;
                            }
                            mainHeaderDivContent += '<div my-qtip2 qtip-content="<b>Name: </b></br><b>Milestone Date: </b> </br><b>Descripton: </b>" class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">' + y + '</div>';
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        if ($scope.GanttHeaderList.hasOwnProperty(x)) {
                            if ($scope.GanttHeaderList.hasOwnProperty(x)) {
                                var startDt = $scope.GanttHeaderList[x].Startdate;
                                var endDt = $scope.GanttHeaderList[x].EndDate;
                                var duration = 0;
                                var formLeft = 0;
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth);
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);

                                ganttHeaderBar += '<div class="qtip-header-block" my-qtip2 qtip-content="<b>Name:' + $scope.GanttHeaderList[x].Name.replace(/["']/g, "") + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                            }
                        }
                    }
                    break;
                case 'quarterly':
                    var possion = 1;
                    //Draw monthly header
                    for (var y in dates) {
                        if (dates.hasOwnProperty(y)) {
                            var yw = 0;
                            var Qno = 0;

                            for (var m in dates[y]) {
                                if (dates[y].hasOwnProperty(m)) {
                                    var w = dates[y][m].length * $scope.settings.QuarterlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;

                                    if (Qno != Math.floor((parseInt(m) + 1) / 3)) {
                                        mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">Q' + (Qno + 1) + ' - ' + y + '</div>';
                                        yw = 0;
                                        Qno = Math.floor((parseInt(m) + 1) / 3);
                                    }
                                }
                            }
                        }
                    }

                    for (var x in $scope.GanttHeaderList) {
                        if ($scope.GanttHeaderList.hasOwnProperty(x)) {
                            var startDt = $scope.GanttHeaderList[x].Startdate;
                            var endDt = $scope.GanttHeaderList[x].EndDate;
                            var duration = 0;
                            var formLeft = 0;
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);

                            ganttHeaderBar += '<div class="qtip-header-block" my-qtip2 qtip-content="<b>Name:' + $scope.GanttHeaderList[x].Name.replace(/["']/g, "") + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                        }
                    }
                    break;
                case 'weekly':
                    var possion = 1;
                    var weekno = getWeek(Date.parse($scope.settings.StartDate));
                    var weekLen = 0;

                    for (var y in dates) {
                        if (dates.hasOwnProperty(y)) {
                            for (var m in dates[y]) {
                                if (dates[y].hasOwnProperty(m)) {
                                    var w = dates[y][m].length * $scope.settings.WeeklyDayWidth;
                                    totalW = totalW + w;

                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';

                                    for (var d in dates[y][m]) {
                                        if (dates[y][m].hasOwnProperty(d)) {
                                            if (weekno != getWeek(dates[y][m][d])) {
                                                if ((possion % 2) == 1) {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                } else {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                }
                                                possion += 1;
                                                weekno = getWeek(dates[y][m][d]);
                                                weekLen = 0;
                                            }
                                            weekLen += $scope.settings.WeeklyDayWidth;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Draw final week
                    if ((possion % 2) == 1) {
                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    } else {
                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    }

                    for (var x in $scope.GanttHeaderList) {
                        if ($scope.GanttHeaderList.hasOwnProperty(x)) {
                            var startDt = $scope.GanttHeaderList[x].Startdate;
                            var endDt = $scope.GanttHeaderList[x].EndDate;
                            var duration = 0;
                            var formLeft = 0;
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);

                            ganttHeaderBar += '<div class="qtip-header-block" my-qtip2 qtip-content="<b>Name:' + $scope.GanttHeaderList[x].Name.replace(/["']/g, "") + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                        }
                    }

                    break;
                default:
            }

            $('#ganttChart .ganttview-header .ganttview-header-mains').html(mainHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-subs').html(subHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-block').html(ganttHeaderBar);
            $('#ganttChart .ganttview-header').css("width", totalW + "px");

            $('#ganttChart .ganttview-grid').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid-row').html(rowDivContent);
            $('#ganttChart #GanttBlockContainer').css("width", totalW + "px");

            $(function () {
                $(".qtip-header-block").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded"
                        //title: { 'display': 'none' }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true

                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () { $(this).fadeOut(100); },
                        event: 'unfocus click mouseleave mouseup mousedown'

                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'

                    }

                });
            });


        }

        function drawBlock(data) {



            var blockDivPage = '';
            var blockDivDataPage = '';


            var UniqueId = 0;
            var ClassName = "";
            var attrStartEndDate = "";
            var colorcode = "dddddd";

            for (var i = 0; i < data.length; i++) {


                attrStartEndDate = $.grep(data[i], function (e) { return e.ID == 78 || e.ID == 79 });
                if ($.grep(data[i], function (e) { return e.ID == 80 })[0] != undefined)
                    colorcode = $.grep(data[i], function (e) { return e.ID == 80 })[0].Value == null ? "dddddd" : $.grep(data[i], function (e) { return e.ID == 80 })[0].Value;
                else
                    colorcode = "dddddd";

                ClassName = " mo" + i;

                blockDivPage += "<div data-over='true' data-uniquekey=" + i + " data-GroupRecordId='" + data[i][0].AttributeRecordID + "' class='" + ClassName + " ganttview-data-block-container'>";
                if (attrStartEndDate.length == 2) {
                    if (attrStartEndDate[0].Value != "" && attrStartEndDate[1].Value != "") {

                        var startDt = "", endDt = "";
                        var datstartval = new Date.create(attrStartEndDate[0].Value.toString());
                        if (datstartval == 'NaN-NaN-NaN') {
                            startDt = ConvertDateToString(ConvertStringToDateByFormat(attrStartEndDate[0].Value.toString(), GlobalUserDateFormat))
                        }
                        else
                            startDt = ConvertDateToString(attrStartEndDate[0].Value);

                        var datendval = new Date.create(attrStartEndDate[1].Value.toString());
                        if (datendval == 'NaN-NaN-NaN') {
                            endDt = ConvertDateToString(ConvertStringToDateByFormat(attrStartEndDate[1].Value.toString(), GlobalUserDateFormat))
                        }
                        else
                            endDt = ConvertDateToString(attrStartEndDate[1].Value);

                        var duration = 0;
                        var formLeft = 0;

                        switch ($scope.settings.Mode) {
                            case 'daily':
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                                break;
                            case 'monthly':
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                                break;
                            case 'quarterly':
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                                break;
                            case 'weekly':
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                                break;
                            default:
                                duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                                formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                                break;
                        }

                        if ($scope.GanttLock == true) {
                            if (data.Data[i]['Permission'] == 3 || data[i]['Permission'] == 0) {
                                blockDivPage += "<div data-name='" + data[i]["Caption"].replace(/["']/g, "") + "'  my-qtip2 qtip-content='<b>Name:</b> " + data[i]["Caption"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br>' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({ id: parseInt(data.Data[i]['Id']), order: parseInt($(this).attr("o")), start: $(this).attr("s"), end: $(this).attr("e"), desc: $(this).attr("d"), uniqueId: UniqueId, SortOid: $(this).attr("sid") }) + "'></div>";
                            }
                            else {
                                blockDivPage += "<div data-name='" + data[i]["Caption"].replace(/["']/g, "") + "' my-qtip2 qtip-content='<b>Name:</b> " + data[i]["Caption"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br>' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({ id: parseInt(data.Data[i]['Id']), order: parseInt($(this).attr("o")), start: $(this).attr("s"), end: $(this).attr("e"), desc: $(this).attr("d"), uniqueId: UniqueId, SortOid: $(this).attr("sid") }) + "'></div>";
                            }
                        }
                        else {
                            blockDivPage += "<div data-name='" + data[i][0]["Caption"].replace(/["']/g, "") + "'  my-qtip2 qtip-content='<b>Name:</b> " + data[i][0]["Caption"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br>' class='ganttview-data-block' data-id='" + data[i][0]['ID'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + colorcode.trim() + ";'></div>";
                        }
                    }
                }
                blockDivPage += '</div>';

                var attrCaption = $.grep(data[i], function (e) { return e.ID == 76 })[0].Caption;
                blockDivDataPage += "<tr data-over='true' data-GroupRecordId='" + data[i][0].AttributeRecordID + "' data-uniquekey=" + i + " class='" + ClassName + "' >";
                blockDivDataPage += '<td><span data-icon="ActivityIcon" style="background-color: #' + colorcode + ';" class="eicon">' + data[i][0]["Caption"].toString().charAt(0) + '</span><span class="Gantt-tdName">' + attrCaption + '</span></td>';
                blockDivDataPage += '</tr>';
            }



            return { Block: blockDivPage, Data: blockDivDataPage };
        }

        function ScrollToCurrentDate() {
            var ToDay;
            var DaysToScroll;

            switch ($scope.settings.Mode) {
                case 'daily':
                    ToDay = new Date.create();
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.MonthlyDayWidth;
                    break;
                case 'quarterly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.QuarterlyDayWidth;
                    break;

                case 'weekly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.WeeklyDayWidth;
                    break;
                default:
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.YearlyDayWidth;
                    break;
            }

            $(".ganttview-data").scrollLeft(DaysToScroll);

        }

        function daysBetween(start, end) {
            if (!start || !end) {
                return 0;
            }
            start = Date.parse(start);
            end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }

            return Math.round((end - start) / (1000 * 60 * 60 * 24));
        }

        function isWeekend(date) {
            return date.getDay() % 6 == 0;
        }

        function getWeek(d) {

            // Create a copy of this date object
            var target = new Date.create(d.valueOf());
            // ISO week date weeks start on monday
            // so correct the day number
            var dayNr = (d.getDay() + 6) % 7;

            // Set the target to the thursday of this week so the
            // target date is in the right year
            target.setDate(target.getDate() - dayNr + 3);

            // ISO 8601 states that week 1 is the week
            // with january 4th in it
            var jan4 = new Date.create(target.getFullYear(), 0, 4);

            // Number of days between target date and january 4th
            var dayDiff = (target - jan4) / 86400000;

            // Calculate week number: Week 1 (january 4th) plus the
            // number of weeks between target date and january 4th
            var weekNr = 1 + Math.ceil(dayDiff / 7);

            return weekNr;

        }

        function getDates(start, end) {

            start = Date.parse(start);
            end = Date.parse(end);
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start];
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) { dates[next.getFullYear()] = []; }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }

            return dates;

        }

        function GetCalenderStartDate() {
            return new Date.create(parseInt((new Date.create).getFullYear()) - 1, 0, 1);
        }

        function LoadGanttView(ListContentData, PageIndex, UniqueID) {

            var value = drawBlock(ListContentData);

            if (UniqueID != "") {
                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').after(value.Block);
                $('#ganttChart tr[data-uniquekey="' + UniqueID + '"]').after(value.Data);
            } else {
                $('#ganttChart .ganttview-data-blocks-page[data-page="' + PageIndex + '"]').html(value.Block).removeAttr('style');
                $('#ganttChart tbody[data-page="' + PageIndex + '"]').html(value.Data).removeAttr('style');
            }
            $(window).AdjustHeightWidth();

            $(function () {
                $(".ganttview-data-block, .reached, .notreached ").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: { 'display': 'none' }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true

                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () { $(this).fadeOut(100); },
                        event: 'unfocus click mouseleave mouseup mousedown'

                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'

                    }

                });
            });
        }



        //----------------------------------------> ATTRIBUTE GROUP GANTT VIEW ENDS HERE <-------------------------

        //---------------------------------------> ATTRIBUTE GROUP IMPORT DATA STARTS HERE <-----------------

        //--------> UPLOAD XLS FILE <------------------

        var imgfileid = '';
        $scope.StartUploaderImage = function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'pickfiles',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/UploadAttributeGroupFiles.ashx?Type=Attachment',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [
                    { title: "Excel files", extensions: "xls,xlsx,csv" }
                ],
                resize: { width: 320, height: 240, quality: 90 }
            });

            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });

            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                up.refresh(); // Reposition Flash/Silverlight
                uploader.start();
            });

            uploader.bind('Error', function (up, err) {
                $('#filelist').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );

                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.LoadingWheelCaption = "Uploading...";
                $scope.showhideimport = true;
                $("#importfile > span").addClass("displayNone")
                $("#importfile div > span:eq(0)").removeClass("displayNone")
                AttributegrouplistganttviewincustomtabService.GetAttributeGroupImportedFileColumnName(response.response).then(function (AttrImportedData) {
                    if (AttrImportedData.Response != null) {
                        if (AttrImportedData.Response[0] != "@Exception") {
                            $("#importfile div > span:eq(1)").addClass("displayNone")
                            $scope.ImportedFileId = response.response;
                            $scope.ImportFileName = file.name;
                            $scope.ImportDataColumnNames = AttrImportedData.Response;
                            $scope.showhideimport = false;
                            $scope.IsUploadshow = false;
                        }
                        else {
                            $scope.showhideimport = false;
                            $scope.ImportedFileId = "";
                            $scope.ImportFileName = AttrImportedData.Response[1];
                            $scope.IsUploadshow = true;
                        }
                    }
                    else {
                        $("#importfile div > span:eq(1)").removeClass("displayNone")
                        $scope.showhideimport = false;
                        $scope.ImportedFileId = "";
                        $scope.ImportFileName = "";
                        $scope.IsUploadshow = true;
                    }
                });

            });
        };

        $scope.SaveImportedData = function () {
            $scope.LoadingWheelCaption = "Saving...";
            $scope.showhideimport = true;
            var insertimportdata = {};
            insertimportdata.BindedLabelAndColumn = $scope.BindLabelAndColumn;
            insertimportdata.FileName = $scope.ImportedFileId;
            insertimportdata.AttributeGroupID = $scope.itemGroupId;
            insertimportdata.EntityID = parseInt($stateParams.ID);
            insertimportdata.AttributeGrpName = $scope.itemTitle;
            AttributegrouplistganttviewincustomtabService.InsertImportedAttributeGroupData(insertimportdata).then(function (updateentityattribresult) {
                if (updateentityattribresult.StatusCode == 405) {
                    $scope.showhideimport = false;
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
                else {
                    $scope.loading = true;
                    $scope.AGRecordList.splice(0, $scope.AGRecordList.length);
                    AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, $scope.itemGroupId, $scope.itemIsLock, $scope.pagination.currentPage, $scope.itemPagesize).then(function (entitygroupvalues) {
                        if (entitygroupvalues.Response != null) {
                            $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                            $scope.AGRecordList = entitygroupvalues.Response;
                            if ($scope.HeaderCaptions.length == 0) {
                                $scope.HeaderCaptions = entitygroupvalues.Response[0];
                                $scope.Noofcolumn = $scope.HeaderCaptions.length + 1;
                                if ($scope.BindLabelAndColumn.length > 0)
                                    $scope.BindLabelAndColumn.splice(0, $scope.BindLabelAndColumn.length);

                                for (var i = 0; i < entitygroupvalues.Response[0].length; i++) {
                                    if (entitygroupvalues.Response[0][i].TypeID == 6 || entitygroupvalues.Response[0][i].TypeID == 12) {
                                        for (var j = 0; j < entitygroupvalues.Response[0][i].Lable.length; j++) {
                                            $scope.BindLabelAndColumn.push({ AttributeID: entitygroupvalues.Response[0][i].ID, AttributeType: entitygroupvalues.Response[0][i].TypeID, Level: entitygroupvalues.Response[0][i].Lable[j].Level, AttributeCaption: entitygroupvalues.Response[0][i].Lable[j].Label, ColumnName: "", Value: "" });
                                        }
                                    }
                                    else
                                        $scope.BindLabelAndColumn.push({ AttributeID: entitygroupvalues.Response[0][i].ID, AttributeType: entitygroupvalues.Response[0][i].TypeID, Level: 0, AttributeCaption: entitygroupvalues.Response[0][i].Lable, ColumnName: "", Value: "" });
                                }
                            }
                            $scope.loading = false;
                        }
                        else
                            $scope.loading = false;
                        $scope.showhideimport = false;
                        $scope.showHideItemDiv = false;
                    });
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                }
                $("#attrGrpImportDataPopup").modal('hide');
            });
        };

        $scope.clearImportpopup = function () {
            $scope.IsUploadshow = true;
            $("#importfile > span").removeClass("displayNone")
            $("#importfile div > span:eq(0)").addClass("displayNone")
            $("#importfile div > span:eq(1)").addClass("displayNone")
            $scope.ImportedFileId = "";
            $scope.ImportFileName = "";
            $scope.ImportDataColumnNames.splice(0, $scope.ImportDataColumnNames.length);
            for (var i = 0; i < $scope.BindLabelAndColumn.length; i++) {
                $scope.BindLabelAndColumn[i].ColumnName = "";
                $scope.BindLabelAndColumn[i].Value = "";
            }
        }

        $scope.PredefinedAttributeGroupValues = [];
        $scope.PredefinedAttrGroupshowAsColumn = [];
        $scope.PredefinedAttrGroupIsSearchable = [];
        $scope.AGR = [];
        $scope.PredefinedHeaderCaptions = [];
        $scope.FilterFields = {};
        $scope.searchfields = {};
        var whereCondition = [];
        var orgLevel = [];

        $scope.SelectAllPredefinedValues = function (event, groupID) {
            var status = event.target.checked;
            $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                this.checked = status;
                if ($('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').hasClass('checkbox checked')) {
                    $(this).next('i').removeClass('checked');
                } else {
                    $(this).next('i').addClass('checked');
                }
            });
            if ($('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').hasClass('checkbox checked')) {
                $('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').removeClass('checked');
                $scope.checkedAttrGrpInpag.splice($scope.checkedAttrGrpInpag.length, 0);
            }
            else {
                $('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').addClass('checked');

                var PredefinedAttribute = {};
                PredefinedAttribute.ID = $scope.itemGroupId;
                PredefinedAttribute.entityId = parseInt($stateParams.ID);
                PredefinedAttribute.EntityTypeID = 0;
                PredefinedAttribute.searchattributes = whereCondition;
                PredefinedAttribute.pageNo = 0;
                PredefinedAttribute.PageSize = 0;
                PredefinedAttribute.sortColumnBy = $scope.sortColumnBy == "" ? "" : $scope.sortColumnBy + " " + ($scope.reverse == false ? "ASC" : "DESC");

                PredefinedAttribute.SelectAll = true;
                PredefinedAttribute.SelectedIds = [];

                PredefinedAttribute.IsFromSave = true;

                if (whereCondition.length > 0) {
                    AttributegrouplistganttviewincustomtabService.GetPreDefinedAttributeGroupAttributeOptions(PredefinedAttribute).then(function (PredefinedSearchresult) {
                        if (PredefinedSearchresult.Response != null && PredefinedSearchresult.Response.length > 0) {
                            for (var i = 0; i < PredefinedSearchresult.Response.length; i++) {
                                var res = $.grep($scope.checkedAttrGrpInpag, function (e) { return e == PredefinedSearchresult.Response[i][0].AttributeRecordID });
                                if (res == undefined || res.length == 0)
                                    $scope.checkedAttrGrpInpag.push(PredefinedSearchresult.Response[i][0].AttributeRecordID)
                            }
                        }
                    });
                }
            }
        }

        $scope.SelectPredefinedValues = function (event, groupId, AttrRecordGroupId) {
            if ($('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').hasClass('checkbox checked')) {
                $scope.checkedAttrGrpInpag.splice(0, $scope.checkedAttrGrpInpag.length);
                $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                    if ($(this).next('i').hasClass('checked'))
                        $scope.checkedAttrGrpInpag.push($(this).parents('tr > td').attr('data-id'));
                });

                $('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').removeClass('checked');
            }
            if (event.target.checked) {
                var res = $.grep($scope.checkedAttrGrpInpag, function (e) { return e == AttrRecordGroupId });
                if (res.length == 0)
                    $scope.checkedAttrGrpInpag.push(AttrRecordGroupId);
            }
            else {
                var res = $.grep($scope.checkedAttrGrpInpag, function (e) { return e == AttrRecordGroupId });
                if (res.length > 0)
                    $scope.checkedAttrGrpInpag.splice($scope.checkedAttrGrpInpag.indexOf(AttrRecordGroupId), 1);
            }
        }

        function GetSelectedPredefinedIds() {

            var PredefinedIDList = new Array();
            $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                if ($(this).next('i').hasClass('checked'))
                    PredefinedIDList.push($(this).parents('tr > td').attr('data-id'));
            });

            return PredefinedIDList;
        }

        $scope.IsClicked = {};

        $scope.AddPredefinedAttribteValues = function (Ispredefined) {
            $scope.IsPredefDataPresent = true;
            $(".savedisable").removeAttr("disabled");

            $scope.dyn_Cont = '';
            $scope.PredefinedAttrGroupshowAsColumn = [];
            $scope.PredefinedAttrGroupIsSearchable = [];
            $scope.AGR = [];
            $scope.dyn_Cont = '';
            $("#dynamic_Controls1" + $scope.itemGroupId).html(
                                        $compile($scope.dyn_Cont)($scope));
            $scope.PredefinedHeaderCaptions = [];
            $scope.FilterFields = {};
            $scope.searchfields = {};
            whereCondition = [];
            $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                $(this).next('i').removeClass('checked');
            });
            $('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').removeClass('checked');

            $scope.predefinedpagination.totalitems = 0;
            $scope.predefinedpagination.currentPage = 1;
            //$scope.predefinedpagination.PageSize = 10;
            $scope.predefinedloading = true;
            //$scope.predefinedscroll.pageNumber = 1;

            $timeout(function () {
                $('#PredefinedAddAttributesModel' + $scope.itemGroupId).modal('show'), 300
            });

            AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupLabelNames(0, $scope.itemGroupId).then(function (entitygroupvalues) {
                if (entitygroupvalues.Response != null && entitygroupvalues.Response.length > 0) {
                    var res = $.grep(entitygroupvalues.Response[0], function (e) { return e.ShowAsColumn == 1 });
                    if (res.length > 0) {
                        $scope.PredefinedHeaderCaptions = res;
                    }
                }
            });
            GetPredefinedSearchAttributes();
        }

        function GetPredefinedSearchAttributes() {

            $scope.FilterFields = {};
            $scope.searchfields = {};
            whereCondition = [];

            AttributegrouplistganttviewincustomtabService.GetAttrGroupSearchAttributes($scope.itemGroupId).then(function (Searchresult) {
                if (Searchresult.Response != null) {
                    $scope.PredefinedAttrGroupSearch = Searchresult.Response;
                    $scope.dyn_Cont = '';
                    $("#dynamic_Controls1" + $scope.itemGroupId).html(
                                $compile($scope.dyn_Cont)($scope));
                    for (var i = 0; i < $scope.PredefinedAttrGroupSearch.length; i++) {
                        if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 3) {
                            if ($scope.PredefinedAttrGroupSearch[i].AttributeId == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.PredefinedAttrGroupSearch[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"  id=\"DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  PredefinedAttrGroupSearch[" + i + "].Users \"></select></div>";
                            }
                            else if ($scope.PredefinedAttrGroupSearch[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                var EntityStatusOptions = $scope.PredefinedAttrGroupSearch[i].EntityStatusOptionValues
                                $scope.tagAllOptionsEntityStatus.data = [];
                                if (EntityStatusOptions != null) {
                                    $.each(EntityStatusOptions, function (i, el) {
                                        $scope.tagAllOptionsEntityStatus.data.push({
                                            "id": el.ID,
                                            "text": el.StatusOptions,
                                            "ShortDescription": el.ShortDesc,
                                            "ColorCode": el.ColorCode
                                        });
                                    });
                                }

                                $scope.dyn_Cont += "<div class='control-group'>";
                                $scope.dyn_Cont += "<span>" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " : </span><div class=\"controls\">";
                                $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" />";
                                $scope.dyn_Cont += "</div></div>";
                            }
                            else if ($scope.PredefinedAttrGroupSearch[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {

                                $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.PredefinedAttrGroupSearch[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"  id=\"DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Name  for ndata in  OntimeStatusLists \"></select></div>";
                            }
                            else {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"  id=\"DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption  for ndata in  PredefinedAttrGroupSearch[" + i + "].OptionValues \"></select></div></div>";
                            }
                            $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = [];
                        }
                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 1 || $scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 2) {
                            if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 2) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"searchfields.TextMultiLine_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"><textarea class=\"small-textarea\" name=\"searchfields.TextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" ng-model=\"searchfields.TextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" id=\"SearchTextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"  rows=\"3\"></textarea></div></div>";
                                $scope.searchfields["TextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = '';
                            }
                            else {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"searchfields.TextSingleLine_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"><input type=\"text\" ng-model=\"searchfields.TextSingleLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" id=\"SearchTextSingleLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" ></div></div>";
                                $scope.searchfields["TextSingleLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = '';
                            }
                        }
                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 4) {
                            if ($scope.PredefinedAttrGroupSearch[i].AttributeId != 75 && $scope.PredefinedAttrGroupSearch[i].AttributeId != 74) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"  id=\"DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  PredefinedAttrGroupSearch[" + i + "].OptionValues \"></select></div></div>";
                                $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = [];
                            }
                        }
                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 6) {
                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel + "\"  id=\"FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  PredefinedAttrGroupSearch[" + i + "].LevelTreeNodes \"></select></div></div>";
                            $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel] = [];
                        }

                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 12) {
                            $scope.dyn_Cont += "<div class='control-group'>";
                            $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\">";
                            $scope.dyn_Cont += "<select multiple='multiple' ui-select2 data-placeholder='Select " + $scope.PredefinedAttrGroupSearch[i].DisplayName + " options' ng-model='FilterFields.DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel + "'\ >";
                            $scope.dyn_Cont += "<option ng-repeat='ndata in PredefinedAttrGroupSearch[" + i + "].LevelTreeNodes' value='{{ndata.Id}}'>{{ndata.Caption}}</option>";
                            $scope.dyn_Cont += "</select></div>";
                            $scope.dyn_Cont += "</div>";
                            $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel] = [];
                        }

                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 13) {
                            var k = $scope.TreePricing.length;
                            var treecount12 = $.grep($scope.FilterDataXML, function (e) { return e.AttributeId == $scope.PredefinedAttrGroupSearch[i].AttributeId });
                            if (treecount12.length == 0) {
                                var mm = $scope.PredefinedAttrGroupSearch[i].AttributeId;
                                $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.PredefinedAttrGroupSearch[i].AttributeId.toString() + ""] = false;
                                $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.PredefinedAttrGroupSearch[i].AttributeId.toString() + ""] = $scope.PredefinedAttrGroupSearch[i].DropdowntreePricingAttr;
                                $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.PredefinedAttrGroupSearch[i].AttributeId.toString() + "></div>";
                                $scope.FilterDataXML.push({ "AttributeId": parseInt($scope.PredefinedAttrGroupSearch[i].AttributeId) });
                            }
                        }

                        else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 5) {

                            $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"searchfields.DatePart_ " + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\">" + $scope.PredefinedAttrGroupSearch[i].DisplayName + " </label><div class=\"controls\"><input type=\"text\" ng-model=\"searchfields.DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" ng-click=\"SearchCalanderopen($event," + $scope.searchfields["DatePart_Calander_Open" + $scope.PredefinedAttrGroupSearch[i].AttributeId] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"searchfields.DatePart_Calander_Open" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" id=\"DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "\" ></div></div>";
                            var param1 = new Date.create();
                            var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                            if ($scope.PredefinedAttrGroupSearch[i].AttributeValue == "" || $scope.PredefinedAttrGroupSearch[i].AttributeValue == null) {
                                $scope.searchfields["DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = new Date.create();
                                $scope.searchfields["DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] = null;
                            }
                        }
                    }

                    $scope.PreDefinedAttrGroupSearchRelationListWithTree = $.grep(Searchresult.Response, function (e) { return e.AttributeTypeId == 7 })
                    for (var i = 0; i < $scope.PreDefinedAttrGroupSearchRelationListWithTree.length; i++) {
                        if ($scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeTypeId == 7) {
                            $scope.treePreviewObj = {};
                            $scope.treesrcdirec["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId] = JSON.parse($scope.PreDefinedAttrGroupSearchRelationListWithTree[i].tree).Children;
                            if ($scope.treesrcdirec["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId].length > 0) {
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId])) {
                                    $scope.treePreviewObj["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId] = true;
                                }
                                else
                                    $scope.treePreviewObj["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId] = false;
                            }
                            else {
                                $scope.treePreviewObj["Attr_" + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId] = false;
                            }
                            $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                            $scope.dyn_Cont += '<label class="control-label">' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].DisplayName + '</label>';
                            $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                            $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '"></div>';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                            $scope.dyn_Cont += '</div></div>';
                            $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '\">';
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.PreDefinedAttrGroupSearchRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '</div></div></div>';
                        }
                    }

                    $scope.dyn_Cont += "<button style=\"margin-left: 258px;\" ng-click=\"GetSearchResult()\" ng-show=\"PredefinedAttrGroupSearch.length > 0\" class=\"btn btn-small btn-primary savedisable\">Search</button>";

                    $("#dynamic_Controls1" + $scope.itemGroupId).html(
                                $compile($scope.dyn_Cont)($scope));
                }

            });
        }

        $scope.GetSearchResult = function () {
            $scope.checkedAttrGrpInpag = [];
            whereCondition = [];

            for (var i = 0; i < $scope.PredefinedAttrGroupSearch.length > 0; i++) {

                if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 1 || $scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 2) {
                    if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 1) {
                        if ($scope.searchfields["TextSingleLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].trim() != "")
                            whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': 0, 'SelectedText': $scope.searchfields["TextSingleLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId], 'Level': 0, 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                    }
                    else
                        if ($scope.searchfields["TextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].trim() != "")
                            whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': 0, 'SelectedText': $scope.searchfields["TextMultiLine_" + $scope.PredefinedAttrGroupSearch[i].AttributeId], 'Level': 0, 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                }
                else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 3) {
                    if ($scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].length > 0)
                        whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].length > 1 ? $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].join(",") : $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId].join(','), 'SelectedText': '', 'Level': 0, 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                }
                else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel];
                        if (orgLevel.length > 0)
                            for (var k = 0 ; k < orgLevel.length; k++) {
                                whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel][k], 'Level': $scope.PredefinedAttrGroupSearch[i].TreeLevel, 'SelectedText': '', 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                            }
                    }
                }
                else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel];
                        if (orgLevel.length > 0)
                            for (var k = 0 ; k < orgLevel.length; k++) {
                                whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': $scope.FilterFields["DropDown_" + $scope.PredefinedAttrGroupSearch[i].AttributeId + "_" + $scope.PredefinedAttrGroupSearch[i].TreeLevel][k], 'Level': $scope.PredefinedAttrGroupSearch[i].TreeLevel, 'SelectedText': '', 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                            }
                    }
                }
                else if ($scope.PredefinedAttrGroupSearch[i].AttributeTypeId == 5) {
                    if ($scope.searchfields["DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] != null && $scope.searchfields["DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId] != undefined)
                        whereCondition.push({ 'AttributeID': $scope.PredefinedAttrGroupSearch[i].AttributeId, 'SelectedValue': ConvertDateToString($scope.searchfields["DatePart_" + $scope.PredefinedAttrGroupSearch[i].AttributeId]), 'SelectedText': '', 'Level': 0, 'AttributeTypeId': $scope.PredefinedAttrGroupSearch[i].AttributeTypeId });
                }
            }
            $scope.loadpredefinedData();
        }
        $scope.loadpredefinedData = function () {

            var PredefinedAttribute = {};
            PredefinedAttribute.ID = $scope.itemGroupId;
            PredefinedAttribute.entityId = parseInt($stateParams.ID);
            PredefinedAttribute.EntityTypeID = 0;
            PredefinedAttribute.searchattributes = whereCondition;
            PredefinedAttribute.pageNo = $scope.predefinedpagination.currentPage;
            PredefinedAttribute.PageSize = $scope.predefinedpagination.PageSize;
            PredefinedAttribute.sortColumnBy = $scope.sortColumnBy == "" ? "" : $scope.sortColumnBy + " " + ($scope.reverse == false ? "ASC" : "DESC");
            PredefinedAttribute.SelectedIds = [];
            //PredefinedAttribute.SelectedIds = $scope.itemAddedattrgrps["PredefinedattrGrpDataof_" + $scope.itemGroupId].SelectedIds == undefined ? [] : $scope.itemAddedattrgrps["PredefinedattrGrpDataof_" + $scope.itemGroupId].SelectedIds;
            PredefinedAttribute.SelectAll = false;
            PredefinedAttribute.IsFromSave = false;

            if (whereCondition.length > 0) {
                AttributegrouplistganttviewincustomtabService.GetPreDefinedAttributeGroupAttributeOptions(PredefinedAttribute).then(function (entityPredefinedAttributesRelation) {
                    if (entityPredefinedAttributesRelation.Response != null && entityPredefinedAttributesRelation.Response.length > 0) {
                        $scope.IsPredefDataPresent = true;
                        $scope.predefinedpagination.totalitems = entityPredefinedAttributesRelation.Response[0][0].MaxValue;
                        $scope.AGR = [];
                        $scope.AGR = entityPredefinedAttributesRelation.Response;

                        $timeout(function () {
                            if ($scope.AGR.length > 0) {
                                if ($('#predefinedTable' + $scope.itemGroupId + ' > thead input:checkbox').next('i').hasClass('checkbox checked')) {
                                    $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                                        var id = parseInt($(this).parents('tr > td').attr('data-id'));
                                        $(this).next('i').addClass('checked');
                                    });
                                }
                                else {
                                    if ($scope.checkedAttrGrpInpag.length > 0) {
                                        $('#predefinedTable' + $scope.itemGroupId + ' > tbody input:checkbox').each(function () {
                                            var id = parseInt($(this).parents('tr > td').attr('data-id'));
                                            var res = $.grep($scope.checkedAttrGrpInpag, function (selectedId) { return parseInt(selectedId) == id });
                                            if (res != undefined && res.length > 0) {
                                                $(this).next('i').addClass('checked');
                                            } else {
                                                $(this).next('i').removeClass('checked');
                                            }
                                        });
                                    }
                                }
                            }
                        }, 100);
                    }
                    else {
                        $scope.AGR = [];
                        $scope.IsPredefDataPresent = false;
                        $scope.predefinedpagination.totalitems = 0;
                    }
                });
            }
            else {
                $scope.AGR = [];
                $scope.IsPredefDataPresent = false;
                $scope.predefinedpagination.totalitems = 0;
            }
        }

        $scope.SavePredefinedValuesToAttribtueGroup = function () {
            $(".savedisable").prop("disabled", true);
            var SelectedIds = $scope.checkedAttrGrpInpag;
            var IsselectAll = false;

            var predefinedSelectedValues = {};
            predefinedSelectedValues.SelectedIds = SelectedIds;
            predefinedSelectedValues.GroupID = $scope.itemGroupId;

            predefinedSelectedValues.ParentId = parseInt($stateParams.ID);
            predefinedSelectedValues.Typeid = 0;
            predefinedSelectedValues.IsLock = false;
            predefinedSelectedValues.Name = $scope.itemTitle;
            predefinedSelectedValues.SortOrder = $scope.SortOrder;
            predefinedSelectedValues.SelectedIds = SelectedIds;
            predefinedSelectedValues.SelectAll = false;

            AttributegrouplistganttviewincustomtabService.SavePredefinedValuesToAttribtueGroup(predefinedSelectedValues).then(function (result) {
                if (result.Response == true) {

                    AttributegrouplistganttviewincustomtabService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, $scope.itemGroupId, $scope.itemIsLock, $scope.pagination.currentPage, $scope.itemPagesize).then(function (entitygroupvalues) {
                        if (entitygroupvalues.Response != null && entitygroupvalues.Response.length > 0) {
                            $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                            $scope.AGRecordList = entitygroupvalues.Response;
                            $scope.pagination.totalitems = entitygroupvalues.Response[0][0].MaxValue;
                            $scope.HidePagination = true;
                            if ($scope.HeaderCaptions.length <= 0) {
                                $scope.HeaderCaptions = entitygroupvalues.Response[0];
                                //$scope.attrGrpList();
                                $scope.Noofcolumn = $scope.HeaderCaptions.length + 1;

                            }
                            if (entitygroupvalues.Response.length > 0)
                                $scope.ShowHideAddAttributeGroupBtn = false;
                            else
                                $scope.ShowHideAddAttributeGroupBtn = true;
                            $scope.EntityMemberData = [];
                            $scope.MemberLists = [];
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                            $('#PredefinedAddAttributesModel' + $scope.itemGroupId).modal('hide');
                        }
                        else {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            $('#PredefinedAddAttributesModel' + $scope.itemGroupId).modal('hide');
                        }
                    });

                    $timeout(function () {
                        for (var k = 0; k < SelectedIds.length; k++) {
                            for (var i = 0; i < $scope.AGR.length; i++) {
                                var req_res = $.grep($scope.AGR[i], function (e) { return e.AttributeRecordID == SelectedIds[k] });
                                if (req_res.length > 0) {
                                    $scope.AGR.splice($scope.AGR.indexOf($scope.AGR[i]), 1);
                                }
                            }
                        }
                    }, 50);

                    $scope.showHideItemDiv = false;
                }
                else {
                    if ($scope.AGRecordList.length > 0)
                        $scope.showHideItemDiv = false;
                    else
                        $scope.showHideItemDiv = true;
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $('#PredefinedAddAttributesModel' + $scope.itemGroupId).modal('hide');
                }
            });
        }

        $scope.PredefinedHeaderList = function () {

            if ($scope.PredefinedHeaderCaptions.length > 0) {

                $("#PredefinedHeaderList" + $scope.itemGroupId).html('');

                var dynhtml = '';
                dynhtml += "<th data-sort-ignore=\"true\"><span><label class=\"checkbox checkbox-custom pull-right\"><input ng-click=\"SelectAllPredefinedValues($event, itemGroupId)\" ng-model=\"AllpredefinedvalueChecked\" type=\"checkbox\" /><i class=\"checkbox\"></i></label></span></th>";
                var sub = '';
                for (var i = 0; i < $scope.PredefinedHeaderCaptions.length; i++) {
                    var ColumnId = '"th' + i + '"';
                    if ($scope.PredefinedHeaderCaptions[i].TypeID == 6 || $scope.PredefinedHeaderCaptions[i].TypeID == 12) {
                        sub = '';
                        for (var j = 0; j < $scope.PredefinedHeaderCaptions[i].Lable.length; j++) {
                            sub += "<span><label class=\"bold\">" + $scope.PredefinedHeaderCaptions[i].Lable[j].Label + "</label></span>";
                        }
                        dynhtml += "<th ng-click='sortEnable(" + ColumnId + ")' id=" + ColumnId + ">" + sub + "";
                    }
                    else {
                        dynhtml += " <th id=" + ColumnId + " ng-click='sortEnable(" + ColumnId + ")'><span><label class=\"bold\">" + $scope.PredefinedHeaderCaptions[i].Lable + "</label></span>";
                    }
                    dynhtml += "<span class=\"footable-sort-indicator\"></span></th>";
                }

                $("#PredefinedHeaderList" + $scope.itemGroupId).html($compile(dynhtml)($scope));

                $timeout(function () {
                    $("#predefinedTable" + $scope.itemGroupId).footable();
                    $("#predefinedTable" + $scope.itemGroupId).trigger('footable_initialized');
                    $("#predefinedTable" + $scope.itemGroupId).trigger('footable_resize');
                    $("#predefinedTable" + $scope.itemGroupId).data('footable').redraw();
                }, 10);
            }

        }

        //$timeout(function () {
        //    $scope.StartUploaderImage();
        //}, 50);

        $scope.LoadListViewHeader = function () {

            if ($scope.HeaderCaptions.length > 0) {
                $("#Listviewheader").html('');
                var dynhtml = '';

                for (var i = 0; i < $scope.HeaderCaptions.length; i++) {
                    var colID = '"th' + i + '"';
                    if ($scope.HeaderCaptions[i].TypeID == 6 || $scope.HeaderCaptions[i].TypeID == 12) {
                        for (var j = 0; j < $scope.HeaderCaptions[i].Lable.length; j++) {
                            //dynhtml += '<th ng-click="sortEnable('+ $scope.HeaderCaptions[i].Lable[j].Label + ')"' > +$scope.HeaderCaptions[i].Lable[j].Label + '';
                            dynhtml += "<th id=" + colID + ">" + $scope.HeaderCaptions[i].Lable[j].Label + "";
                        }
                    }
                    else {
                        //dynhtml += ' <th ng-click="sortEnable(' + $scope.HeaderCaptions[i].Lable + ')">' + $scope.HeaderCaptions[i].Lable + "";
                        dynhtml += " <th id=" + colID + " ng-click='sortEnable(" + colID + ")'>" + $scope.HeaderCaptions[i].Lable + "";
                    }
                    dynhtml += "</th>";
                }
                dynhtml += '<th data-sort-ignore="true"></th>';
                $("#Listviewheader").html($compile(dynhtml)($scope));
            }
        }

        $scope.sortEnable = function sortEnable(columnID) {
            $scope.tempColID = columnID;
            if ($scope.colName == columnID) {
                if ($scope.IsSortAsc != true) {
                    $scope.IsSortAsc = true;
                    $scope.IsSortDsc = true;
                }
                else {
                    $scope.IsSortAsc = false;
                }
            }
            else {
                $scope.IsSortAsc = true;
            }

            $scope.colName = columnID;

        }
        function setSort() {
            $("#" + $scope.tempColID).click();
            if ($scope.tempColID.contains('th0'))
                $("#" + $scope.tempColID).click();
            if (!$scope.tempColID.contains('th0') && ($scope.IsSortAsc == false || $scope.IsSortDsc == true)) {
                $("#" + $scope.tempColID).click();

            }
        }

        $timeout(function () {
            $scope.StartUploaderImage();
        }, 50);

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.component.attributegrouplistganttviewincustomtabCtrl']"));
        });

    }
    app.controller('mui.planningtool.component.attributegrouplistganttviewincustomtabCtrl', ['$scope', '$timeout', '$resource', '$compile', '$stateParams', '$cookies', 'AttributegrouplistganttviewincustomtabService', '$translate', attributegrouplistganttviewincustomtabCtrl]);
    // Attributegrouplistganttviewincustomtab-Service 
    function AttributegrouplistganttviewincustomtabService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetEntityPeriod: GetEntityPeriod,
            CreateAttributeGroupRecord: CreateAttributeGroupRecord,
            DeleteEntityAttributeGroupRecord: DeleteEntityAttributeGroupRecord,
            GetEntityAttributesGroupValues: GetEntityAttributesGroupValues,
            GetEntityAttributesGroupLabelNames: GetEntityAttributesGroupLabelNames,
            GetAttributeGroupAttributeOptions: GetAttributeGroupAttributeOptions,
            GetAttributeGroupImportedFileColumnName: GetAttributeGroupImportedFileColumnName,
            InsertImportedAttributeGroupData: InsertImportedAttributeGroupData,
            GetAttrGroupSearchAttributes: GetAttrGroupSearchAttributes,
            GetPreDefinedAttributeGroupAttributeOptions: GetPreDefinedAttributeGroupAttributeOptions,
            SavePredefinedValuesToAttribtueGroup: SavePredefinedValuesToAttribtueGroup
        });
        function GetEntityPeriod(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntityPeriod/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateAttributeGroupRecord(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateAttributeGroupRecord/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityAttributeGroupRecord(GroupID, GroupRecordID, ParentID) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityAttributeGroupRecord/" + GroupID + "/" + GroupRecordID + "/" + ParentID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesGroupValues(EntityID, EntityTypeID, GroupID, IsCmsContent, pageNo, PageSize) { var request = $http({ method: "get", url: "api/Metadata/GetEntityAttributesGroupValues/" + EntityID + "/" + EntityTypeID + "/" + GroupID + "/" + IsCmsContent + "/" + pageNo + "/" + PageSize, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesGroupLabelNames(EntityID, GroupID) { var request = $http({ method: "get", url: "api/Metadata/GetEntityAttributesGroupLabelNames/" + EntityID + "/" + GroupID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAttributeGroupAttributeOptions(ID, EntityID, AttributeRecordID) { var request = $http({ method: "get", url: "api/Metadata/GetAttributeGroupAttributeOptions/" + ID + "/" + EntityID + "/" + AttributeRecordID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAttributeGroupImportedFileColumnName(FileID) { var request = $http({ method: "post", url: "api/Metadata/GetAttributeGroupImportedFileColumnName/", params: { action: "add" }, data: { FileID: FileID } }); return (request.then(handleSuccess, handleError)); }
        function InsertImportedAttributeGroupData(formobj) { var request = $http({ method: "post", url: "api/Metadata/InsertImportedAttributeGroupData/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SavePredefinedValuesToAttribtueGroup(formobj) { var request = $http({ method: "post", url: "api/Planning/SavePredefinedValuesToAttribtueGroup/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAttrGroupSearchAttributes(GroupID) { var request = $http({ method: "get", url: "api/Metadata/GetAttrGroupSearchAttributes/" + GroupID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetPreDefinedAttributeGroupAttributeOptions(formobj) { var request = $http({ method: "post", url: "api/Metadata/GetPreDefinedAttributeGroupAttributeOptions/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("AttributegrouplistganttviewincustomtabService", ['$http', '$q', AttributegrouplistganttviewincustomtabService]);
})(angular, app);
