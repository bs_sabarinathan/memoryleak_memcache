using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Navigation object for table 'CM_Navigation'.
	/// </summary>

    internal class CustomTab : ICustomTab 
	{
		#region Member Variables

        protected int _id;
        protected int _Typeid;
        protected string _name;
        protected string _externalurl;
        protected bool _issytemdefined;
        protected bool _addusername;
        protected bool _adduseremail;
        protected bool _adduserid;
        protected bool _addlanguagecode;
        protected bool _addentityid;
        protected int _entityttypeid;
        protected int _attributegroupid;

        protected int _sortorder;
        protected string _controleid;
		
		#endregion
		
		#region Constructors
        public CustomTab() { }

        public CustomTab(int pId , int pTypeid , string pName , string pExternalUrl , bool pIsSystemDefined , bool pAddUserName , bool pAddUserEmail , bool pAddEntityID , bool pAddUserID , bool pAddLanguageCode , int pSortOrder , int pentitytypeid , int pattributegroupid)
		{
			this._id = pId; 
            this._Typeid = pTypeid; 
			this._name = pName; 
            this._externalurl = pExternalUrl; 
			this._issytemdefined = pIsSystemDefined; 
			this._addusername = pAddUserName; 
			this._adduseremail = pAddUserEmail; 
			this._addentityid = pAddEntityID;
            this._adduserid = pAddUserID;
            this._addlanguagecode = pAddLanguageCode;
            this._entityttypeid = pentitytypeid;
            this._attributegroupid = pattributegroupid;
            this._sortorder = pSortOrder;
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
        public virtual int Typeid
        {
            get { return _Typeid; }
            set { _Typeid = value; }
        }
		
		public string Name
		{
			get { return _name; }
			set 
			{
                if(value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption" , "Caption value, cannot contain more than 50 characters");
              _name = value; 
			}
			
		}
		

        public virtual string ExternalUrl
        {

            get { return _externalurl; }
            set
            {
                if(value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("ExternalUrl" , "ExternalUrl value, cannot contain more than 1073741823 characters");
                _externalurl = value;
            }
        }


        public bool IsSytemDefined
		{
			get { return _issytemdefined; }
            set { _issytemdefined = value; }
			
		}
		
		public bool AddUserName
		{
			get { return _addusername; }
			set { _addusername = value; }
			
		}
		
		public bool AddUserEmail
		{
			get { return _adduseremail; }
			set { _adduseremail = value; }
			
		}

        public bool AddUserID
        {
            get { return _adduserid; }
            set { _adduserid = value; }

        }

        public bool AddLanguageCode
        {
            get { return _addlanguagecode; }
            set { _addlanguagecode = value; }

        }

        public bool AddEntityID
		{
			get { return _addentityid; }
            set { _addentityid = value; }
			
		}

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }
        }

        public int EntityTypeID
            {
            get { return _entityttypeid; }
            set { _entityttypeid = value; }
            }
        public int AttributeGroupID
            {
            get { return _attributegroupid; }
            set { _attributegroupid = value; }
            }



        public virtual string ControleID
        {
            get { return _controleid; }
            set { _controleid = value; }
        }

        public  bool IsChecked;
       
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
        public override bool Equals(object obj)
		{
            if(this == obj) return true;
			Navigation castObj = null;
			try
			{
				castObj = (Navigation)obj;
                }
            catch(Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
