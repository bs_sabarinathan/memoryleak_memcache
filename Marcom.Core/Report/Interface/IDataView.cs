﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access;

namespace BrandSystems.Marcom.Core.Report.Interface
{
    /// <summary>
    /// IUser interface for table 'DataView'.
    /// Database :DevExpressReportServer
    /// </summary>
    public interface IDataView
    {
        #region Public Properties

        int DataviewID
        {
            get;
            set;

        }

        string Dataviewname
        {
            get;
            set;

        }

       
        #endregion
    }
}
