﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace UpgradeTool
{
    [Serializable, System.Xml.Serialization.XmlRoot("marcominstances")]
    public class marcominstances
    {
        [XmlElement]
        public List<instance> instance { get; set; }
    }
}