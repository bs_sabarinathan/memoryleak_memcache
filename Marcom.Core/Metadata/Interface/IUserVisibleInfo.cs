using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IUserVisibleInfo interface for table 'MM_UserVisibleInfo'.
    /// </summary>
    public interface IUserVisibleInfo
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }
        int AttributeId
        {
            get;
            set;
        }
       
        #endregion
    }
}
