/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// SelectableValue object for table 'MM_SelectableValue'.
	/// </summary>
	
	internal class SelectableValue : ISelectableValue 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected int _attributeid;
		
		
		#endregion
		
		#region Constructors
		public SelectableValue() {}
			
		public SelectableValue(int pId, string pCaption, int pAttributeid)
		{
			this._id = pId; 
			this._caption = pCaption; 
			this._attributeid = pAttributeid; 
		}
				
		public SelectableValue(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public int Attributeid
		{
			get { return _attributeid; }
			set { _attributeid = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			SelectableValue castObj = null;
			try
			{
				castObj = (SelectableValue)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
