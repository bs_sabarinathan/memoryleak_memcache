﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface ICmsCustomStyle
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string ClassName
        {
            get;
            set;

        }

        string CssCode
        {
            get;
            set;

        }
        #endregion
    }
}
