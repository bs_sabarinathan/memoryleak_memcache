﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    public partial class CustomTabEntityTypeRoleAclDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
        protected int _customtabid;
        protected int _entitytypeid;
        protected int _globalroleid;

		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CustomTabEntityTypeRoleAclDao() {}

        public CustomTabEntityTypeRoleAclDao(int pId, int pCutomtabid, int pEntitytypeId, int pGlobalroleId)
		{
			this._id = pId;
            this._customtabid = pCutomtabid;
            this._entitytypeid = pEntitytypeId;
            this._globalroleid = pGlobalroleId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int ID
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int CustomTabID
		{
            get { return _customtabid; }
            set { _bIsChanged |= (_customtabid != value); _customtabid = value; }
			
		}

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
        }

        public virtual int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CustomTabEntityTypeRoleAclDao = "CustomTabEntityTypeRoleAclDao";
            public const string ID = "ID";
            public const string CustomTabID = "CustomTabID";
            public const string EntityTypeID = "EntityTypeID";
            public const string GlobalRoleID = "GlobalRoleID";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CustomTabEntityTypeRoleAclDao = "CustomTabEntityTypeRoleAclDao";
            public const string ID = "ID";
            public const string CustomTabID = "CustomTabID";
            public const string EntityTypeID = "EntityTypeID";
            public const string GlobalRoleID = "GlobalRoleID";
		}

		#endregion
    }
}
