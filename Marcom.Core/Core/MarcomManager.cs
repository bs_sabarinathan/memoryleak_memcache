﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.User.Interface;
using System.Threading;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Collections;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Core.Metadata;


namespace BrandSystems.Marcom.Core
{
    public class MarcomManager : IMarcomManager
    {

        // Reference to the managers
        private UserManagerProxy _userManagerProxy = null;
        private ReportManagerProxy _reportManagerProxy = null;
        private PlanningManagerProxy _planningManagerProxy = null;
        private AccessManagerProxy _accessManagerProxy = null;
        private MetadataManagerProxy _metadataManagerProxy = null;
        private CommonManagerProxy _commonManagerProxy = null;
        private TaskManagerProxy _taskManagerProxy = null;
        private DigitalAssetManagerProxy _digitalassetProxy = null;
        private CmsManagerProxy _cmsmanagerproxy = null;
        private ExternalTaskManagerProxy _externaltaskManagerProxy = null;
        private IEventManager _eventManager = null;
        private IPluginManager _pluginManager = null;
        private ExpireHandlerManagerProxy _expireHandlerManagerProxy = null;
        private HashSet<string> powerThreadIds = new HashSet<string>();

        public MarcomManager(IEventManager eventManager, IPluginManager pluginManager)
        {

            // Create the logged in user's specific instance of the user manager proxy.
            // The actual implementation class could be changed in configuration
            // or using dependency injection (if we want to implement that)...

            _eventManager = eventManager;
            _pluginManager = pluginManager;

            _reportManagerProxy = new ReportManagerProxy(this);
            _userManagerProxy = new UserManagerProxy(this);
            _planningManagerProxy = new PlanningManagerProxy(this);
            _accessManagerProxy = new AccessManagerProxy(this);
            _metadataManagerProxy = new MetadataManagerProxy(this);
            _commonManagerProxy = new CommonManagerProxy(this);
            _taskManagerProxy = new TaskManagerProxy(this);
            _digitalassetProxy = new DigitalAssetManagerProxy(this);
            _cmsmanagerproxy = new CmsManagerProxy(this);
            _expireHandlerManagerProxy = new ExpireHandlerManagerProxy(this);
            _externaltaskManagerProxy = new ExternalTaskManagerProxy(this);
        }

        public Guid SessionID { get; set; }

        // Reference to the logged in user
        public IUser User
        {
            get
            {
                IUser userCache = MarcomCache<User.User>.GetCache("_marcomUsersCache", SessionID);
                BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
                userCache.AwsStorage = tfp.GetTenantAmazonStoragebytenantId(userCache.TenantID);
                return userCache;
            }
            set
            {
                value.AwsStorage = null;
                MarcomCache<IUser>.SetCache(value, "_marcomUsersCache", SessionID);
            }
        }

        //Reference filterd Entity Id for the login user
        public List<SortOrderIdsCollection> EntitySortorderIdColle
        {
            get
            {
               return MarcomCache<List<SortOrderIdsCollection>>.GetCache("_EntitySortorderIdColle", SessionID);
                
            }
            set
            {
                MarcomCache<List<SortOrderIdsCollection>>.SetCache(value, "_EntitySortorderIdColle", SessionID);
            }
        }

        public List<SortOrderIdsCollection> EntityFilterSortorderIDColln
        {
            get
            {
                return MarcomCache<List<SortOrderIdsCollection>>.GetCache("_EntityFilterSortorderIDColln", SessionID);
            }
            set
            {
                MarcomCache<List<SortOrderIdsCollection>>.SetCache(value, "_EntityFilterSortorderIDColln", SessionID);
            }
        }
        public int EntitySortorderIdColleHash
        {
            get
            {
                return MarcomCache<int>.GetCache("_EntitySortorderIdColleHash", SessionID);
            }
            set
            {
                MarcomCache<int>.SetCache(value, "_EntitySortorderIdColleHash", SessionID);
            }
        }
        public string EntityMainQuery
        {
            get
            {
                return MarcomCache<string>.GetCache("_EntityMainQuery", SessionID);
            }
            set
            {
                MarcomCache<string>.SetCache(value, "_EntityMainQuery", SessionID);
            }
        }
        public Tuple<ArrayList, ArrayList> GeneralColumnDefs
        {
            get
            {
                return MarcomCache<Tuple<ArrayList, ArrayList>>.GetCache("_GeneralColumnDefs", SessionID);
            }
            set
            {
                MarcomCache<Tuple<ArrayList, ArrayList>>.SetCache(value, "_GeneralColumnDefs", SessionID);
            }
        }

        //reference to keep currency type and date format globally
        public IList<IAdditionalSettings> GlobalAdditionalSettings { get; set; }

        // Retrieve the UserManager
        public IUserManager UserManager
        {
            get { return _userManagerProxy; }
        }

        // Retrieve the UserManager
        public IReportManager ReportManager
        {
            get { return _reportManagerProxy; }
        }

        // Retrieve the Planning manager
        public IPlanningManager PlanningManager
        {
            get { return _planningManagerProxy; }
        }

        // Retrieve the Access manager
        public IAccessManager AccessManager
        {
            get { return _accessManagerProxy; }
        }

        // Retrieve the Common manager
        public ICommonManager CommonManager
        {
            get { return _commonManagerProxy; }
        }

        // Retrieve the Task manager
        public ITaskManager TaskManager
        {
            get { return _taskManagerProxy; }
        }

        // Retrieve the Event manager
        public IEventManager EventManager
        {
            get { return _eventManager; }
        }

        // Retrieve the Plugin manager
        public IPluginManager PluginManager
        {
            get { return _pluginManager; }
        }

        // Retrieve the Metadata manager
        public IMetadataManager MetadataManager
        {
            get { return _metadataManagerProxy; }
        }

        // Retrieve the Digital Asset manager
        public IDigitalAssetManager DigitalAssetManager
        {
            get { return _digitalassetProxy; }
        }
        public ICmsManager CmsManager
        {
            get { return _cmsmanagerproxy; }
        }

        // Retrieve the Digital Asset manager
        public IExpireHandlerManager ExpireHandlerManager
        {
            get { return _expireHandlerManagerProxy; }
        }
        //public ITransaction GetTransaction()
        //{
        //    return GetTransaction(true);
        //}

        #region IExternalTaskManager

        public IExternalTaskManager ExternalTaskManager
        {
            get { return _externaltaskManagerProxy; }
        }

        #endregion


        //public ITransaction GetTransaction(bool create)
        //{
        //    return MarcomManagerFactory.GetTransaction(create);
        //}


        public ITransaction GetTransaction(int tenantid)
        {
            return GetTransaction(true, tenantid);
        }

        public ITransaction GetTransaction(bool create, int tenantid)
        {
            return MarcomManagerFactory.GetTransaction(create, tenantid);
        }

        /// <summary>
        /// Gets or Sets this thread in powermode. This means that this thread has unlimited rights!
        /// </summary>
        /// <param name="powerMode"></param>
        public bool PowerMode
        {
            get { return powerThreadIds.Contains(GetThreadKey()); }
            set
            {
                if (value)
                    powerThreadIds.Add(GetThreadKey());
                else
                    powerThreadIds.Remove(GetThreadKey());
            }
        }

        private static string GetThreadKey()
        {
            return Thread.CurrentThread.GetHashCode().ToString();
        }

    }
}
