﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ExpireHandler.Interface
{
    public interface IEntityTypeExpireRoleAcl
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityRoleID
        {
            get;
            set;

        }

        int ModuleID
        {
            get;
            set;

        }

        int EntityTypeID
        {
            get;
            set;

        }

        int Sortorder
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        int ExpireRoleID
        {
            get;
            set;
        }


        #endregion
    }
}
