﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolCtrl($scope, $resource, AdminService, $timeout, $compile, $translate) {
        $scope.assetTypedata = [];
        $scope.wizard = {
            TreeLevelNumberByNode: [],
            TreeLevels: [],
            DeletedIds: []
        };
        $scope.tree = [];
        $scope.SelectedCategory = [];
        $scope.filterval = "";
        $scope.treeCategory = [];
        $scope.dyn_Cont = "";
        AdminService.GetAssetTypeAssetCategoryRelation().then(function (data) {
            $scope.assetTypedata = data.Response;
            for (var i = 0; i < $scope.assetTypedata.length; i++) {
                if ($scope.assetTypedata[i].CategoryName == "-") {
                    $scope.assetTypedata[i].CategoryName = $translate.instant('LanguageContents.Res_1197.Caption');
                }
            }
        });
        AdminService.GetAssetCategoryTreeCollection().then(function (data) {
            if (data.Response != null) {
                $scope.tree = JSON.parse(data.Response);
            }
        });
        $scope.SaveAndUpdateCategory = function () {
            if ($scope.tree[0].Children.length > 0) {
                var IsEmpty = false;
                $('.TreeNodeEditorRowForm input[type="text"]').each(function () {
                    if ($.trim(this.value) == '') {
                        if ($(this).data('isdeleted') == "false" || $(this).data('isdeleted') == false) {
                            IsEmpty = true;
                        }
                    }
                });
                if (IsEmpty) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1813.Caption'));
                    return false;
                }
                var test = {};
                test.jobj = $scope.tree;
                AdminService.InsertUpdateAssetCatergory(test).then(function (data) {
                    AdminService.GetAssetTypeAssetCategoryRelation().then(function (data) {
                        $scope.assetTypedata = data.Response;
                    });
                    NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                    $("#modalCategory").modal('hide');
                });
            }
        }
        $scope.SelectCatagoryName = function (ID) {
            var currentObj = $.grep($scope.OptimakerCatagories, function (item, i) {
                return item.id == ID;
            });
            return currentObj[0].Caption;
        }
        $scope.AddtreeOptionsNewChild = function (data) {
            var post = data.Children.length + 1;
            var newName = data.Caption + '-' + post;
            newName = "";
            var child_node = {
                Caption: newName,
                Level: data.Level + 1,
                id: 0,
                IsDeleted: false,
                Children: []
            };
            if (data.Caption != 'Categories') {
                child_node.Level = data.Level + 1;
            }
            if (data.Children) {
                data.Children.push(child_node);
                var newLevel = $.inArray(child_node.Level, $scope.wizard.TreeLevelNumberByNode);
                $scope.wizard.TreeLevelNumberByNode.push(child_node.Level);
                if (newLevel == -1) {
                    var level = {
                        Level: child_node.Level,
                        Caption: '',
                        ID: 0
                    }
                    $scope.wizard.TreeLevels.push(level);
                }
            } else {
                data.Children = [child_node];
            }
        };
        $scope.deletetreeOptionsNewChild = function (data, ParentTree) {
            if (data.id == 0) {
                var total_childrens = data.Children;
                var level = data.Level;
                data.IsDeleted = true;
                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                    var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                        return e.Level == level;
                    });
                    for (var r = 0, obj; obj = removableLevels[r++];) {
                        $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                    }
                }
                if (data.Children.length > 0) {
                    RemovalofNewChild(total_childrens, data);
                }
            } else {
                AdminService.DeleteAssetCategory(data.id).then(function (newdata) {
                    if (newdata.Response == 1) {
                        if (data.Children.length > 0) {
                            for (var i = 0; i <= data.Children.length - 1; i++) {
                                if (data.Children[i].id == 0) {
                                    var level = data.Children[i].Level;
                                    data.Children[i].IsDeleted = true;
                                    $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                    if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                        var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                            return e.Level == level;
                                        });
                                        for (var r = 0, obj; obj = removableLevels[r++];) {
                                            $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                        }
                                    }
                                } else if (data.Children[i].IsDeleted == false) {
                                    var level = data.Children[i].Level;
                                    data.Children[i].IsDeleted = true;
                                    $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                    if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                        var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                            return e.Level == level;
                                        });
                                        for (var r = 0, obj; obj = removableLevels[r++];) {
                                            $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                        }
                                    }
                                }
                                RemovalofNewChild(data.Children[i].Children, data.Children[i]);
                            }
                            var level = data.Level;
                            data.IsDeleted = true;
                            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                    return e.Level == level;
                                });
                                for (var r = 0, obj; obj = removableLevels[r++];) {
                                    $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                }
                            }
                        } else {
                            var level = data.Level;
                            data.IsDeleted = true;
                            $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                            if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                    return e.Level == level;
                                });
                                for (var r = 0, obj; obj = removableLevels[r++];) {
                                    $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                }
                            }
                        }
                    } else if (newdata.Response == 2) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5113.Caption'));
                    } else if (newdata.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                    }
                });
            }
        };
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            if (branch.ischecked == true) {
                $scope.SelectedCategory.CategoryID = branch.id;
                $scope.SaveAndUpdateAssetCategoryRel();
            } else {
                $scope.SelectedCategory.CategoryID = 0;
                $scope.SaveAndUpdateAssetCategoryRel();
            }
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.my_tree = tree = {};
        $scope.LaodAssetCategoryRelByID = function (Catgry) {
            $("#AssetCategoryRel").modal('show');
            AdminService.GetSelectedCategoryTreeCollection(Catgry.CategoryId).then(function (data) {
                if (data.Response != null) {
                    $scope.treeCategory = JSON.parse(data.Response);
                    $("#dynamicTree").html('');
                    $scope.dyn_Cont = '<category-treeadmin tree-filter=\"filterval\" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></category-treeadmin>';
                    $("#dynamicTree").html($compile($scope.dyn_Cont)($scope));
                }
            });
            $scope.SelectedCategory = {
                AssetTypeID: Catgry.AssetTypeID,
                CategoryID: Catgry.CategoryId
            };
        }
        $scope.SaveAndUpdateAssetCategoryRel = function () {
            var asset = {};
            asset.AssetTypeID = $scope.SelectedCategory.AssetTypeID;
            asset.CategoryID = $scope.SelectedCategory.CategoryID;
            AdminService.InsertUpdateAssetCategoryRelation(asset).then(function (data) {
                if (data.Response != null) {
                    if (data.Response[0].Name != "-") {
                        $.grep($scope.assetTypedata, function (e) {
                            return e.AssetTypeID == $scope.SelectedCategory.AssetTypeID
                        })[0].CategoryId = data.Response[0].ID;
                        $.grep($scope.assetTypedata, function (e) {
                            return e.AssetTypeID == $scope.SelectedCategory.AssetTypeID
                        })[0].CategoryName = data.Response[0].Name;
                    } else {
                        $.grep($scope.assetTypedata, function (e) {
                            return e.AssetTypeID == $scope.SelectedCategory.AssetTypeID
                        })[0].CategoryId = data.Response[0].ID;
                        $.grep($scope.assetTypedata, function (e) {
                            return e.AssetTypeID == $scope.SelectedCategory.AssetTypeID
                        })[0].CategoryName = $translate.instant('LanguageContents.Res_1197.Caption');
                    }
                }
            });
            $timeout(function () {
                $("#AssetCategoryRel").modal('hide');
            }, 100);
        }

        function RemovalofNewChild(childrens, parent) {
            if (childrens != undefined) {
                if (childrens.length > 0) {
                    for (var z = 0, child; child = childrens[z++];) {
                        if (child.Children.length > 0) {
                            for (var x = 0, grandchild; grandchild = child.Children[x++];) {
                                var level = grandchild.Level;
                                grandchild.IsDeleted = true;
                                $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                                if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                                    var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                        return e.Level == level;
                                    });
                                    for (var r = 0, obj; obj = removableLevels[r++];) {
                                        $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                                    }
                                }
                                RemovalofNewChild(grandchild.Children, grandchild);
                            }
                        }
                        var level = child.Level;
                        child.IsDeleted = true;
                        $scope.wizard.TreeLevelNumberByNode.splice($.inArray(level, $scope.wizard.TreeLevelNumberByNode), 1);
                        if ($.inArray(level, $scope.wizard.TreeLevelNumberByNode) == -1) {
                            var removableLevels = $.grep($scope.wizard.TreeLevels, function (e) {
                                return e.Level == level;
                            });
                            for (var r = 0, obj; obj = removableLevels[r++];) {
                                $scope.wizard.TreeLevels.splice($.inArray(obj, $scope.wizard.TreeLevels), 1);
                            }
                        }
                    }
                }
            }
        }
        $scope.OpenEditCategoryPopUp = function () {
            $("#modalCategory").modal('show');
            AdminService.GetAssetCategoryTreeCollection().then(function (data) {
                if (data.Response != null) {
                    $scope.tree = JSON.parse(data.Response);
                }
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.assetcategoryCtrl']"));
        });
    }
    app.controller('mui.admin.assetcategoryCtrl', ['$scope', '$resource', 'AdminService', '$timeout', '$compile', '$translate', muiplanningtoolCtrl]);
})(angular, app);