﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public class EntityTypeAttributeRelationDao : BaseDao, ICloneable 
    {
        #region Member Variables
        protected  int _id;
        protected int _entitytypeid;
        protected int _attributeid;
        protected string _validationid;
        protected int _sortorder;
        protected string _defaultvalue;
        protected bool _inheritfromparent;
        protected bool _isreadonly;
        protected bool _choosefromparentonly;
        protected bool _isvalidationneeded;
        protected string _caption;
        protected bool _issystemdefined;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected string _PlaceHolderValue;
        protected int _minvalue; 
        protected int _maxvalue;
        protected bool _ishelptextenabled;
        protected string _helptextdescription;
		#endregion
        #region Extensibility Method Definitions

        public override bool Equals(object obj)
        {
            EntityTypeAttributeRelationDao toCompare = obj as EntityTypeAttributeRelationDao;
            if (toCompare == null)
            {
                return false;
            }

            if (!Object.Equals(this.EntityTypeID, toCompare.EntityTypeID))
                return false;
            if (!Object.Equals(this.AttributeID, toCompare.AttributeID))
                return false;
            if (EntityTypeID == this.EntityTypeID && AttributeID == this.AttributeID)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + EntityTypeID.GetHashCode();
            hashCode = (hashCode * 7) + AttributeID.GetHashCode();
            return hashCode;
        }
        //public override bool Equals(object obj)
        //{
        //    if (obj == null)
        //        return false;
        //    EntityTypeAttributeRelationDao id;
        //    id = (EntityTypeAttributeRelationDao)obj;
        //    if (id == null)
        //        return false;
        //    if (EntityTypeID == id.EntityTypeID && AttributeID == id.AttributeID)
        //        return true;
        //    return false;
        //}
        //public override int GetHashCode()
        //{
        //    return (EntityTypeID + "|" + AttributeID).GetHashCode();
        //} 
        #endregion
		#region Constructors
		public EntityTypeAttributeRelationDao() {}

        public EntityTypeAttributeRelationDao(int pID, int pEnitytypeId, int pAttributeId, string pValidationId, int pSortOrder, string pDefaultValue, bool pInheritFromParent, bool pIsReadOnly, bool pChooseFromParentOnly, bool pIsValidationNeeded, string pCaption, bool pIsSystemDefined, string pPlaceHolderValue, int MinValue, int MaxValue, bool pIsHelptextEnabled, string pHelptextDecsription)
		{
            this._id = pID;
            this._entitytypeid = pEnitytypeId;
            this._attributeid = pAttributeId;
            this._validationid = pValidationId;
            this._sortorder = pSortOrder;  
            this._defaultvalue = pDefaultValue;
            this._inheritfromparent = pInheritFromParent;
            this._isreadonly = pIsReadOnly;
            this._choosefromparentonly = pChooseFromParentOnly;
            this._isvalidationneeded = pIsValidationNeeded;
            this._caption = pCaption;
            this._PlaceHolderValue = pPlaceHolderValue;
            this._issystemdefined = pIsSystemDefined;
            this._minvalue = MinValue;
            this._maxvalue = MaxValue;
            this._ishelptextenabled = pIsHelptextEnabled;
            this._helptextdescription = pHelptextDecsription;
		}

        public EntityTypeAttributeRelationDao(int pID)
		{
            this._id = pID;  
		}
		
		#endregion
		
		#region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
        public virtual int EntityTypeID
		{
			get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }


        public virtual string ValidationID
		{
			get { return _validationid; }
			set { _bIsChanged |= (_validationid != value); _validationid = value; }
		}

        public virtual int SortOrder
		{
			get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
			
		}

        public virtual string DefaultValue
        {
            get { return _defaultvalue; }
            set { _bIsChanged |= (_defaultvalue != value); _defaultvalue = value; }

        }

        public virtual string PlaceHolderValue
        {
            get { return _PlaceHolderValue; }
            set { _bIsChanged |= (_PlaceHolderValue != value); _PlaceHolderValue = value; }

        }
        public virtual int MinValue
        {
            get { return _minvalue; }
            set { _bIsChanged |= (_minvalue != value); _minvalue = value; }
        }
        public virtual int MaxValue
        {
            get { return _maxvalue; }
            set { _bIsChanged |= (_maxvalue != value); _maxvalue = value; }
        }
        public virtual bool IsHelptextEnabled
        {
            get { return _ishelptextenabled; }
            set { _bIsChanged |= (_ishelptextenabled != value); _ishelptextenabled = value; }
        }

        public virtual string HelptextDecsription
        {
            get { return _helptextdescription; }
            set { _bIsChanged |= (_helptextdescription != value); _helptextdescription = value; }
        }

        public virtual bool InheritFromParent
        {
            get { return _inheritfromparent; }
            set { _bIsChanged |= (_inheritfromparent != value); _inheritfromparent = value; }

        }

        public virtual bool IsReadOnly
        {
            get { return _isreadonly; }
            set { _bIsChanged |= (_isreadonly != value); _isreadonly = value; }

        }

        public virtual bool ChooseFromParentOnly
        {
            get { return _choosefromparentonly; }
            set { _bIsChanged |= (_choosefromparentonly != value); _choosefromparentonly = value; }

        }

        public virtual bool IsValidationNeeded
        {
            get { return _isvalidationneeded; }
            set { _bIsChanged |= (_isvalidationneeded != value); _isvalidationneeded = value; }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
        public virtual string Caption
        {
            get { return _caption; }
            set { _bIsChanged |= (_caption != value); _caption = value; }

        }

        public virtual bool IsSystemDefined
        {
            get
            {
                return _issystemdefined;
            }
            set
            {
                _issystemdefined = value;
            }
        }
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string EntityTypeAttributeRelationDao = "EntityTypeAttributeRelationDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeID = "AttributeID";
            public const string ValidationID = "ValidationID";
            public const string SortOrder = "SortOrder";			
            public const string DefaultValue = "DefaultValue";
            public const string InheritFromParent = "InheritFromParent";
            public const string IsReadOnly = "IsReadOnly";
            public const string ChooseFromParentOnly = "ChooseFromParentOnly";
            public const string IsValidationNeeded = "IsValidationNeeded";
            public const string PlaceHolderValue = "PlaceHolderValue";
            public const string IsSystemDefined = "IsSystemDefined";
            public const string MinValue = "MinValue";
            public const string MaxValue = "MaxValue";
            public const string IsHelptextEnabled = "IsHelptextEnabled";
            public const string HelptextDecsription = "HelptextDecsription";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string EntityTypeAttributeRelationDao = "EntityTypeAttributeRelationDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeID = "AttributeID";
            public const string ValidationID = "ValidationID";
            public const string SortOrder = "SortOrder";			
            public const string DefaultValue = "DefaultValue";
            public const string InheritFromParent = "InheritFromParent";
            public const string IsReadOnly = "IsReadOnly";
            public const string ChooseFromParentOnly = "ChooseFromParentOnly";
            public const string IsValidationNeeded = "IsValidationNeeded";
            public const string PlaceHolderValue = "PlaceHolderValue";
            public const string IsSystemDefined = "IsSystemDefined";
            public const string MinValue = "MinValue";
            public const string MaxValue = "MaxValue";
            public const string IsHelptextEnabled = "IsHelptextEnabled";
            public const string HelptextDecsription = "HelptextDecsription";
		}

		#endregion
    }
}
