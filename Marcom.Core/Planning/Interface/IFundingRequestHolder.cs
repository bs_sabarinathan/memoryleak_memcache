﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IFundingRequestHolder
    {
         int CostCenterID { get; set; }
         string DueDate { get; set; }
         string Comment { get; set; }
         string Amount { get; set; }
    }
}
