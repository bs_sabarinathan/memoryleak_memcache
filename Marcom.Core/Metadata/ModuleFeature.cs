/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// ModuleFeature object for table 'MM_Module_Feature'.
    /// </summary>

    public class ModuleFeature : IModuleFeature
    {
        #region Member Variables

        protected int _ModuleId;
        protected int? _Featureid;
        protected string _ModuleCaption;
        protected string _FeatureCaption;


        #endregion

        #region Constructors
        public ModuleFeature() { }

        public ModuleFeature(int pModuleid, int? pFeatureid, string pModulecaption, string pFeaturecaption)
        {
            this._ModuleId = pModuleid;
            this._Featureid = pFeatureid;
            this._ModuleCaption = pModulecaption;
            this._FeatureCaption = pFeaturecaption;
        }

        //public ModuleFeature(int pId)
        //{
        //    this._id = pId; 
        //}

        #endregion

        #region Public Properties


        public int ModuleId
        {
            get { return _ModuleId; }
            set { _ModuleId = value; }

        }

        public int? Featureid
        {
            get { return _Featureid; }
            set { _Featureid = value; }

        }
        public string ModuleCaption
        {
            get { return _ModuleCaption; }
            set { _ModuleCaption = value; }
        }
        public string FeatureCaption
        {
            get { return _FeatureCaption; }
            set { _FeatureCaption = value; }
        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        //public override bool Equals( object obj )
        //{
        //    if( this == obj ) return true;
        //    ModuleFeature castObj = null;
        //    try
        //    {
        //        castObj = (ModuleFeature)obj;
        //    }
        //    catch (Exception) { return false; }
        //    return (castObj != null) &&
        //        this._id.Equals(castObj.Id);
        //}
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        //public override int GetHashCode()
        //{


        //    int hash = 57;
        //    hash = 27 * hash * _id.GetHashCode();
        //    return hash;
        //}
        #endregion

    }

}
