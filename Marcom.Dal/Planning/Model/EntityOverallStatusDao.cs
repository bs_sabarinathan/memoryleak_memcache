﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    public partial class EntityOverallStatusDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected int _entitytypeid;
        protected int _overallstatus;
        protected int _attrid;
        protected int _days;
        protected int _templateID;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors

        public EntityOverallStatusDao() { }


        public EntityOverallStatusDao(int pID)
        {
            this._id = pID;
        }

        public EntityOverallStatusDao(int pID, int pentitytypeid, int poverallstatus, int pattrid, string pcaption, string pdescription, int pDays, int pTemplateID)
        {
            this._id = pID;
            this._entitytypeid = pentitytypeid;
            this._overallstatus = poverallstatus;
            this._attrid = pattrid;
            this._days = pDays;
            this._templateID = pTemplateID;
        }
        #endregion
        #region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
        public virtual int EntityTypeId
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }
        public virtual int OverallStatus
        {
            get { return _overallstatus; }
            set
            {
                _overallstatus = value;
            }
        }
        public virtual int AttrID
        {
            get { return _attrid; }
            set
            {
                _attrid = value;
            }

        }
        public virtual int Days
        {
            get { return _days; }
            set { _days = value; }
        }
        public virtual int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
           // public const string EntityOverallStatus = "EntityOverallStatus";
            public const string ID = "ID";
            public const string EntityTypeId = "EntityTypeId";
            public const string OverallStatus = "OverallStatus";
            public const string AttrID = "AttrID";
            public const string Days = "Days";
            public const string TemplateID = "TemplateID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
           // public const string EntityOverallStatus = "EntityOverallStatus";
            public const string ID = "ID";
            public const string EntityTypeId = "EntityTypeId";
            public const string OverallStatus = "OverallStatus";
            public const string AttrID = "AttrID";
            public const string Days = "Days";
            public const string TemplateID = "TemplateID";
        }

        #endregion

    }
}
