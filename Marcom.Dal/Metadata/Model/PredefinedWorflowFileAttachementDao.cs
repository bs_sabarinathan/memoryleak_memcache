﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class PredefinedWorflowFileAttachementDao : BaseDao, ICloneable
    {
         #region Member Variables

        protected int _id;
        protected int _predefinedid;
        protected string _name;
        protected DateTime _createdon;
        protected Guid _fileguid;

        protected string _extension;
        protected long _size;
        protected string _mimetype;
        protected int _ownerid;
        protected int _versionno;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public PredefinedWorflowFileAttachementDao() { }

        public PredefinedWorflowFileAttachementDao(int predefinedid, string pName, DateTime pCreatedon)
        {
            this._predefinedid = predefinedid;
            this._name = pName;
            this._createdon = pCreatedon;

        }

        public PredefinedWorflowFileAttachementDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int PredefinedID
        {
            get { return _predefinedid; }
            set { _bIsChanged |= (_predefinedid != value); _predefinedid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual Guid FileGuid
        {
            get { return _fileguid; }
            set { _bIsChanged |= (_fileguid != value); _fileguid = value; }

        }


        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _bIsChanged |= (_createdon != value); _createdon = value; }

        }

        public virtual string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 100 characters");
                _bIsChanged |= (_extension != value);
                _extension = value;
            }

        }

        public virtual long Size
        {
            get { return _size; }
            set { _bIsChanged |= (_size != value); _size = value; }

        }

        public virtual string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 500 characters");
                _bIsChanged |= (_mimetype != value);
                _mimetype = value;
            }

        }

        public virtual int OwnerID
        {
            get { return _ownerid; }
            set { _bIsChanged |= (_ownerid != value); _ownerid = value; }

        }

        public virtual int VersionNo
        {
            get { return _versionno; }
            set { _bIsChanged |= (_versionno != value); _versionno = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttachmentsDao = "PredefinedWorflowFileAttachementDao";
            public const string ID = "ID";
            public const string PredefinedID = "PredefinedID";
            public const string Name = "Name";
            public const string Createdon = "Createdon";
            public const string FileGuid = "FileGuid";

            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string MimeType = "MimeType";
            public const string OwnerID = "OwnerID";
            public const string VersionNo = "VersionNo";	
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttachmentsDao = "PredefinedWorflowFileAttachementDao";
            public const string ID = "ID";
            public const string PredefinedID = "PredefinedID";
            public const string Createdon = "Createdon";
            public const string Name = "Name";
            public const string FileGuid = "FileGuid";

            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string MimeType = "MimeType";
            public const string OwnerID = "OwnerID";
            public const string VersionNo = "VersionNo";	
        }

        #endregion
    }
}
