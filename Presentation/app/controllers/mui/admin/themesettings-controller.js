﻿(function (ng, app) {
    "use strict";

    function muiadminthemesettingsCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.ColorCodeGlobal = 'ffffff';
        $scope.ThemeData = [];
        $scope.LoginData = [];
        $scope.TopNavigData = [];
        $scope.LogoData = [];
        $scope.BtnAndLinkData = [];
        $scope.FontData = [];
        $scope.ThemeDataBlck = [];
        $scope.Themedata = [];
        $scope.ThemeTypesResult = [];
        $scope.alignmentscope = 1;
        $scope.ColorCodeGlobalObj = {};
        $scope.ColorCodeGlobalObj.colorcode = 'ffffff';
        $scope.greyvariation = {};
        $scope.greyvariationischanged = 0;
        $scope.Basecolorvariations = {};
        $scope.Basecolorvariationsischanged = 0;
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
                ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { }
        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: false,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showHexInput: true,
            showBasicColors: true
        });
        AdminService.GetThemeValues().then(function (GetThemeValuesRes) {
            var Res = [];
            Res = GetThemeValuesRes.Response.m_Item1;
            if (GetThemeValuesRes.Response.m_Item1 != undefined) {
                for (var i = 0; i < Res.length; i++) {
                    $scope.ThemeDataBlcks = GetThemeValuesRes.Response.m_Item1;
                }
                $scope.selected = [];
                $scope.selected = $.grep(GetThemeValuesRes.Response.m_Item1, function (e) {
                    return e.Isactive == 1;
                });
                if ($scope.selected != null && $scope.selected.length != 0) {
                    $scope.ThemeDataBlck = $scope.selected[0].NAME;
                    AdminService.GetThemeData($scope.selected[0].ID).then(function (ThemeResultData) {
                        var rst = [];
                        rst = JSON.parse(ThemeResultData.Response);
                        $scope.Themedata = rst;
                        $scope.Themedata.root.ThemeDataBlck.Theme = $scope.selected[0].ID;
                    });
                }
            }
        });
        $scope.RefreshTheme = function () {
            if ($scope.Themedata.root.ThemeDataBlck.Theme != 0) {
                LoadThemeSettingsXML();
                $scope.greyvariationischanged = 0;
                $scope.Basecolorvariationsischanged = 0;
                $scope.greyvariation = {};
                $scope.Basecolorvariations = {};
            } else {
                $scope.greyvariation = {};
                $scope.Basecolorvariations = {};
                $scope.greyvariationischanged = 0;
                $scope.Basecolorvariationsischanged = 0;
                $scope.Themedata = {
                    root: {
                        ThemeDataBlck: {
                            "Theme": 0,
                            "ThemeColor": "fff",
                            "ThemeName": ""
                        },
                        LoginBlck: {
                            "LoginPosition": "",
                            "LogoAlign": "left"
                        },
                        TopNavBlck: {
                            "BckGrdColor": "1b1b1b",
                            "BckGrdMouseOverColor": "3f3f3f",
                            "Foregroundcolour": "999999",
                            "ForegroundmouseOverColor": "ffffff"
                        },
                        LogoBar: {
                            "LogoBckGrdColor": "fafafa",
                            "LogoForegroundColor": "777777",
                            "LogoForegroundmouseOverColor": "333333"
                        },
                        Buttonlink: {
                            "Primarbtncolour": "007acc",
                            "Defaultbtncolour": "eeeeee",
                            "Linkcolour": "0088cc",
                        },
                        Font: {
                            "Fontsize": 11,
                            "Fontfamilyname": "",
                            "JavaScriptURL": "",
                            "CSSURL": "",
                            "FontURL": ""
                        },
                        Aplbasetheme: {
                            "BaseColor": "0088CC",
                            "lftblkcloro": "DDDDDD"
                        }
                    }
                };
            }
        }

        function LoadThemeSettingsXML() {
            $scope.themeID = "";
            $scope.themeID = $scope.Themedata.root.ThemeDataBlck.Theme;
            AdminService.GetThemeData($scope.Themedata.root.ThemeDataBlck.Theme).then(function (ThemeResultData) {
                var rst = [];
                rst = JSON.parse(ThemeResultData.Response);
                $scope.Themedata = rst;
            });
        }
        $scope.Themedata = {
            root: {
                ThemeDataBlck: {
                    "Theme": "0",
                    "ThemeColor": "fff",
                    "ThemeName": ""
                },
                LoginBlck: {
                    "LoginPosition": "",
                    "LogoAlign": "left"
                },
                TopNavBlck: {
                    "BckGrdColor": "1b1b1b",
                    "BckGrdMouseOverColor": "3f3f3f",
                    "Foregroundcolour": "999999",
                    "ForegroundmouseOverColor": "ffffff"
                },
                LogoBar: {
                    "LogoBckGrdColor": "fafafa",
                    "LogoForegroundColor": "777777",
                    "LogoForegroundmouseOverColor": "333333"
                },
                Buttonlink: {
                    "Primarbtncolour": "007acc",
                    "Defaultbtncolour": "eeeeee",
                    "Linkcolour": "0088cc",
                },
                Font: {
                    "Fontsize": 11,
                    "Fontfamilyname": "",
                    "JavaScriptURL": "",
                    "CSSURL": "",
                    "FontURL": ""
                },
                Aplbasetheme: {
                    "BaseColor": "0088CC",
                    "lftblkcloro": "DDDDDD"
                }
            }
        };
        $scope.Themedata.root.LoginBlck.LoginPosition = 1;
        $scope.shadeColor = function (color, percent, Id) {
            var R = parseInt(color.substring(0, 2), 16);
            var G = parseInt(color.substring(2, 4), 16);
            var B = parseInt(color.substring(4, 6), 16);
            R = parseInt(R * (100 + percent) / 100);
            G = parseInt(G * (100 + percent) / 100);
            B = parseInt(B * (100 + percent) / 100);
            R = (R < 255) ? R : 255;
            G = (G < 255) ? G : 255;
            B = (B < 255) ? B : 255;
            var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
            var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
            var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));
            var BGcolor = "#" + RR + GG + BB;
            if (Id == "darkenTopNavBG") {
                $scope.Themedata["root"]["darkenTopNavBG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenTopNavFG") {
                $scope.Themedata["root"]["darkenTopNavFG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenTopLogoFG") {
                $scope.Themedata["root"]["darkenTopLogoFG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenTopLogoBG") {
                $scope.Themedata["root"]["darkenTopLogoBG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenbtnPrimBG") {
                $scope.Themedata["root"]["darkenbtnPrimBG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenlinkFG") {
                $scope.Themedata["root"]["darkenlinkFG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "darkenbtnBG") {
                $scope.Themedata["root"]["darkenbtnBG"] = {
                    ID: Id,
                    colorcode: BGcolor
                }
            }
            if (Id == "lftBlockBaseBG") {
                if (color != "dddddd" || color != "DDDDDD") {
                    $scope.Themedata["root"]["lftBlockBaseBG"] = {
                        ID: Id,
                        colorcode: BGcolor
                    }
                    $scope.lbc = $scope.Themedata["root"]["lftBlockBaseBG"]["colorcode"];
                    leftblockvariations($scope.lbc);
                } else {
                    $scope.Themedata["root"]["lftBlockBaseBG"] = {
                        ID: Id,
                        colorcode: color
                    }
                    $scope.greyvariationischanged = 1;
                    $scope.greyvariation = {};
                }
            }
            if (Id == "baseAppColor") {
                if (color != "0088cc" || color != "0088CC") {
                    $scope.Themedata["root"]["baseAppColor"] = {
                        ID: Id,
                        colorcode: BGcolor
                    }
                    $scope.selectedbasecolor = $scope.Themedata["root"]["baseAppColor"]["colorcode"];
                    basecolorvaritions($scope.selectedbasecolor);
                } else {
                    $scope.Themedata["root"]["baseAppColor"] = {
                        ID: Id,
                        colorcode: color
                    }
                    $scope.Basecolorvariationsischanged = 1;
                    $scope.Basecolorvariations = {};
                }
            }
        }
        $scope.updateThemeBlock = function () {
            if ($scope.Themedata.root.Aplbasetheme == null || $scope.Themedata.root.Aplbasetheme == 0 || $scope.Themedata.root.Aplbasetheme == '') {
                $scope.bootbox.alert($translate.instant('LanguageContents.Res_5741.Caption'));
            }
            if ($scope.greyvariationischanged == 0) {
                if ($scope.Themedata.root.Aplbasetheme.lftblkcloro == null || $scope.Themedata.root.Aplbasetheme.lftblkcloro == 0 || $scope.Themedata.root.Aplbasetheme.lftblkcloro == '') {
                    $scope.bootbox.alert($translate.instant('LanguageContents.Res_5742.Caption'));
                } else {
                    if ($scope.Themedata.root.Aplbasetheme.lftblkcloro != "DDDDDD" || $scope.Themedata.root.Aplbasetheme.lftblkcloro != "dddddd") {
                        $scope.shadeColor($scope.Themedata.root.Aplbasetheme.lftblkcloro, 0, 'lftBlockBaseBG');
                    }
                }
            }
            if ($scope.Basecolorvariationsischanged == 0) {
                if ($scope.Themedata.root.Aplbasetheme.BaseColor == null || $scope.Themedata.root.Aplbasetheme.BaseColor == 0 || $scope.Themedata.root.Aplbasetheme.BaseColor == '') {
                    $scope.bootbox.alert($translate.instant('LanguageContents.Res_5743.Caption'));
                } else {
                    if ($scope.Themedata.root.Aplbasetheme.BaseColor != "0088CC" || $scope.Themedata.root.Aplbasetheme.BaseColor != "0088cc") {
                        $scope.shadeColor($scope.Themedata.root.Aplbasetheme.BaseColor, 0, 'baseAppColor');
                    }
                }
            }
            if ($scope.Themedata.root.ThemeDataBlck.ThemeName == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_160.Caption'));
            } else {
                var inserttasktempparams = {};
                var rst = $scope.Themedata;
                inserttasktempparams.themeBlockData = rst;
                inserttasktempparams.themeID = $scope.Themedata.root.ThemeDataBlck.Theme;
                inserttasktempparams.themename = $scope.Themedata.root.ThemeDataBlck.ThemeName;
                inserttasktempparams.Greyvariation = $scope.greyvariation;
                inserttasktempparams.objBasecolorvariations = $scope.Basecolorvariations;
                AdminService.UpdateThemeSettings(inserttasktempparams).then(function (result) {
                    if (result != null && result != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        AdminService.GetThemeValues().then(function (GetThemeValuesRes) {
                            var Res = [];
                            Res = GetThemeValuesRes.Response.m_Item1;
                            if (GetThemeValuesRes.Response.m_Item1 != undefined) {
                                for (var i = 0; i < Res.length; i++) {
                                    $scope.ThemeDataBlcks = GetThemeValuesRes.Response.m_Item1;
                                }
                                $scope.selected = [];
                                $scope.selected = $.grep(GetThemeValuesRes.Response.m_Item1, function (e) {
                                    return e.Isactive == 1;
                                });
                                if ($scope.selected != null && $scope.selected.length != 0) {
                                    $scope.ThemeDataBlck = $scope.selected[0].NAME;
                                    AdminService.GetThemeData($scope.selected[0].ID).then(function (ThemeResultData) {
                                        var rst = [];
                                        rst = JSON.parse(ThemeResultData.Response);
                                        $scope.Themedata = rst;
                                        $scope.Themedata.root.ThemeDataBlck.Theme = $scope.selected[0].ID;
                                    });
                                }
                                var Result = "";
                                Result = GetThemeValuesRes.Response.m_Item2;
                                $("#theme-style-id").html(Result);
                            }
                        });
                    }
                });
            }
        }
        $scope.Restorevalues = function () {
            AdminService.RestoreDefaultTheme().then(function (result) {
                if (result != null && result != 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_159.Caption'));
                    $scope.Themedata.root.ThemeDataBlck.Theme = 0;
                    $scope.RefreshTheme();
                    $("#theme-style-id").html("");
                }
            });
        }
        $scope.intentOptions = [{
            "id": 1,
            "class": "icon-align-left",
            "Lable": "left"
        }, {
            "id": 2,
            "class": "icon-align-center",
            "Lable": "center"
        }, {
            "id": 3,
            "class": "icon-align-right",
            "Lable": "right"
        }];

        function leftblockvariations(selectedcolor) {
            var rgb = hex2rgb(selectedcolor);
            var hsl = rgb2hsl(rgb.R, rgb.G, rgb.B);
            $scope.greyvariation.base15perL = hsl2hex(hsl.H, hsl.S, hsl.L + 15);
            $scope.greyvariation.base11perL = hsl2hex(hsl.H, hsl.S, hsl.L + 11);
            $scope.greyvariation.base10perL = hsl2hex(hsl.H, hsl.S, hsl.L + 10);
            $scope.greyvariation.base9perL = hsl2hex(hsl.H, hsl.S, hsl.L + 9);
            $scope.greyvariation.base7perL = hsl2hex(hsl.H, hsl.S, hsl.L + 7);
            $scope.greyvariation.base6perL = hsl2hex(hsl.H, hsl.S, hsl.L + 6);
            $scope.greyvariation.base2perD = hsl2hex(hsl.H, hsl.S, hsl.L - 2);
            $scope.greyvariation.base4perD = hsl2hex(hsl.H, hsl.S, hsl.L - 4);
            $scope.greyvariation.base7perD = hsl2hex(hsl.H, hsl.S, hsl.L - 7);
            $scope.greyvariation.base9perD = hsl2hex(hsl.H, hsl.S, hsl.L - 9);
            $scope.greyvariation.base13perD = hsl2hex(hsl.H, hsl.S, hsl.L - 13);
            $scope.greyvariation.base22perD = hsl2hex(hsl.H, hsl.S, hsl.L - 22);
            $scope.greyvariation.base27perD = hsl2hex(hsl.H, hsl.S, hsl.L - 27);
            $scope.greyvariation.base29perD = hsl2hex(hsl.H, hsl.S, hsl.L - 29);
            $scope.greyvariation.base54perD = hsl2hex(hsl.H, hsl.S, hsl.L - 54);
            $scope.greyvariation.base67perD = hsl2hex(hsl.H, hsl.S, hsl.L - 67);
            $scope.greyvariation.base80perD = hsl2hex(hsl.H, hsl.S, hsl.L - 80);
            $scope.greyvariationischanged = 1;
            return $scope.greyvariation;
        }

        function basecolorvaritions(selectedbasecolor) {
            var rgb = hex2rgb(selectedbasecolor);
            var hsl = rgb2hsl(rgb.R, rgb.G, rgb.B);
            $scope.Basecolorvariations.baseAppNavGradientTop = hsl2hex(hsl.H, hsl.S, 99);
            $scope.Basecolorvariations.baseAppNavGradientbottom = hsl2hex(hsl.H, hsl.S, 92);
            $scope.Basecolorvariations.baseApp57perL = hsl2hex(hsl.H, hsl.S, hsl.L + 57);
            $scope.Basecolorvariations.baseApp54perL = hsl2hex(hsl.H, hsl.S, hsl.L + 54);
            $scope.Basecolorvariations.baseApp45perL = hsl2hex(hsl.H, hsl.S, hsl.L + 45);
            $scope.Basecolorvariations.baseApp35perL = hsl2hex(hsl.H, hsl.S, hsl.L + 35);
            $scope.Basecolorvariations.baseApp15perL = hsl2hex(hsl.H, hsl.S, hsl.L + 15);
            $scope.Basecolorvariations.baseApp5perL = hsl2hex(hsl.H, hsl.S, hsl.L + 5);
            $scope.Basecolorvariations.baseApp2perD = hsl2hex(hsl.H, hsl.S, hsl.L - 2);
            $scope.Basecolorvariations.baseApp5perD = hsl2hex(hsl.H, hsl.S, hsl.L - 5);
            $scope.Basecolorvariations.baseApp10perD = hsl2hex(hsl.H, hsl.S, hsl.L - 10);
            $scope.Basecolorvariations.baseApp15perD = hsl2hex(hsl.H, hsl.S, hsl.L - 15);
            $scope.Basecolorvariations.baseAppOpaque1 = "rgba(" + rgb.R + ", " + rgb.G + ", " + rgb.B + ", 0.1)";
            $scope.Basecolorvariations.baseAppOpaque3 = "rgba(" + rgb.R + ", " + rgb.G + ", " + rgb.B + ", 0.3)";
            $scope.Basecolorvariations.baseAppOpaque5 = "rgba(" + rgb.R + ", " + rgb.G + ", " + rgb.B + ", 0.5)";
            $scope.Basecolorvariationsischanged = 1;
            return $scope.Basecolorvariations;
        }

        function rgb2hsl(r, g, b) {
            var r1 = r / 255;
            var g1 = g / 255;
            var b1 = b / 255;
            var maxColor = Math.max(r1, g1, b1);
            var minColor = Math.min(r1, g1, b1);
            var L = (maxColor + minColor) / 2;
            var S = 0;
            var H = 0;
            if (maxColor != minColor) {
                if (L < 0.5) {
                    S = (maxColor - minColor) / (maxColor + minColor);
                } else {
                    S = (maxColor - minColor) / (2.0 - maxColor - minColor);
                }
                if (r1 == maxColor) {
                    H = (g1 - b1) / (maxColor - minColor);
                } else if (g1 == maxColor) {
                    H = 2.0 + (b1 - r1) / (maxColor - minColor);
                } else {
                    H = 4.0 + (r1 - g1) / (maxColor - minColor);
                }
            }
            L = L * 100;
            S = S * 100;
            H = H * 60;
            if (H < 0) {
                H += 360;
            }
            H = Math.round(H);
            S = Math.round(S);
            L = Math.round(L);
            var result = {
                H: H,
                S: S,
                L: L
            };
            return result;
        }

        function hsl2rgb(h, s, l) {
            var m1, m2, hue;
            var r, g, b
            s /= 100;
            l /= 100;
            if (s == 0)
                r = g = b = (l * 255);
            else {
                if (l <= 0.5)
                    m2 = l * (s + 1);
                else
                    m2 = l + s - l * s;
                m1 = l * 2 - m2;
                hue = h / 360;
                r = HueToRgb(m1, m2, hue + 1 / 3);
                g = HueToRgb(m1, m2, hue);
                b = HueToRgb(m1, m2, hue - 1 / 3);
            }
            r = Math.round(r);
            g = Math.round(g);
            b = Math.round(b);
            return {
                r: r,
                g: g,
                b: b
            };
        }

        function HueToRgb(m1, m2, hue) {
            var v;
            if (hue < 0)
                hue += 1;
            else if (hue > 1)
                hue -= 1;
            if (6 * hue < 1)
                v = m1 + (m2 - m1) * hue * 6;
            else if (2 * hue < 1)
                v = m2;
            else if (3 * hue < 2)
                v = m1 + (m2 - m1) * (2 / 3 - hue) * 6;
            else
                v = m1;
            return 255 * v;
        }

        function hex2rgb(hex) {
            var R = parseInt(hex.substring(1, 3), 16);
            var G = parseInt(hex.substring(3, 5), 16);
            var B = parseInt(hex.substring(5, 7), 16);
            var result = {
                R: R,
                G: G,
                B: B
            };
            return result;
        }

        function rgb2hex(R, G, B) {
            var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
            var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
            var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));
            var result = RR + GG + BB;
            return result;
        }

        function hsl2hex(h, s, l) {
            if (l >= 100) {
                l = 99;
            }
            if (l <= 0) {
                l = 1;
            }
            var hsl = hsl2rgb(h, s, l);
            var result = rgb2hex(hsl.r, hsl.g, hsl.b);
            return result;
        }
        $scope.themeapplychanges = function () {
            $('#themeloading').modal("show");
            $timeout(function () {
                updateThemeBlock();
            }, 500);
            $('#themeloading').modal("hide");
        }
    }
    app.controller("mui.admin.themesettingsCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminthemesettingsCntrl]);
})(angular, app);