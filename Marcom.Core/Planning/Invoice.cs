﻿using BrandSystems.Marcom.Core.Planning.Interface;
using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class Invoice : IInvoice
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected DateTime? _createddate;
        protected string _ponumber;
        protected string _invoiceumber;
        protected int _supplierid;
        protected string _description;
        protected DateTime? _paymentdate;
        protected string _poid;
        protected int? _approverid;
        protected byte[] _invoiceCopy;
        protected bool _IsExternal;
        protected int _userid;

        #endregion

        #region Constructors
        public Invoice() { }

        public Invoice(int pEntityid, DateTime? pCreateddate, string pPonumber, string pInvoiceNumber, int pSupplierid, string pDescription, DateTime? pPaymentdate, int? pApproverId, int pUserID, byte[] pInvoiceCopy, string pPOID, bool pIsExternal)
        {
            this._entityid = pEntityid;
            this._createddate = pCreateddate;
            this._ponumber = pPonumber;
            this._supplierid = pSupplierid;
            this._description = pDescription;
            this._paymentdate = pPaymentdate;
            this._invoiceCopy = pInvoiceCopy;
            this._approverid = pApproverId;
            this._userid = pUserID;
            this._poid = pPOID;
            this._invoiceumber = pInvoiceNumber;

        }

        public Invoice(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual DateTime? CreateDate
        {
            get { return _createddate; }
            set { _createddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");

                _description = value;
            }

        }

        public virtual string PONumber
        {
            get { return _ponumber; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("PONumber", "PONumber value, cannot contain more than 50 characters");

                _ponumber = value;
            }

        }


        public virtual string InvoiceNumber
        {
            get { return _invoiceumber; }
            set
            {
                _invoiceumber = value;
            }

        }

        public virtual string POID
        {
            get { return _poid; }
            set { _poid = value; }

        }

        public virtual int SupplierID
        {
            get { return _supplierid; }
            set { _supplierid = value; }

        }



        public virtual DateTime? PaymentDate
        {
            get { return _paymentdate; }
            set { _paymentdate = value; }

        }

        public virtual int? ApproverID
        {
            get { return _approverid; }
            set { _approverid = value; }

        }

        public virtual byte[] InvoiceCopy
        {
            get { return _invoiceCopy; }
            set { _invoiceCopy = value; }

        }


        public virtual int UserID
        {
            get { return _userid; }
            set { _userid = value; }

        }


        public virtual bool IsExternal
        {
            get { return _IsExternal; }
            set { _IsExternal = value; }
        }


        public int CostCenterID { get; set; }
        public string SupplierName { get; set; }
        public string ApproverName { get; set; }
        public decimal amount { get; set; }
        public string currencytypeName { get; set; }
        public int currencytype { get; set; }
        public string strAppprovalDate { get; set; }
        public string strPaymentDate { get; set; }
        public string strCreatedDate { get; set; }
        public string strAmount { get; set; }
        public string strStatusName { get; set; }
        public bool isApproveSendAuthority { get; set; }

        public IList<IFinancialMetadataAttributewithValues> DynamciData { get; set; }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Invoice castObj = null;
            try
            {
                castObj = (Invoice)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


    }

}
