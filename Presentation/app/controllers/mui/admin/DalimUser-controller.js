﻿(function (ng, app) {
    "use strict";

    function muidalimuserCtrl($scope, AdminService, $translate) {
        $scope.editableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetDalimUserByID(row)" data-toggle="modal" data-target="#dalimuserModal"><i class="icon-edit"></i></a>'
        $scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteDalimUserByID(row)" data-toggle="modal"><i class="icon-remove"></i></a>'
        $scope.DalimUserDetails = [];
        $scope.dalimUserVar = {
            UserID: 0,
            FirstName: "",
            LastName: "",
            Email: "",
            Password: "",
            reset: function () {
                this.UserID = 0;
                this.FirstName = "";
                this.LastName = "";
                this.Email = "";
                this.Password = "";
                $('#dalimuserModal').modal('show');
            }
        };
        $scope.GetListOfDalimUsers = function () {
            AdminService.GetDalimUser().then(function (data) {
                if (data.Response != null) {
                    $scope.DalimUserDetails = data.Response;
                }
            });
        };
        $scope.GetListOfDalimUsers();
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridOptions = {
            data: 'DalimUserDetails',
            enablePinning: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60,
                visible: false
            }, {
                field: "UserName",
                displayName: $translate.instant('LanguageContents.Res_55.Caption'),
                width: 260
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }]
        };
        $scope.GetDalimUserByID = function GetUserByID(row) {
            $scope.dalimUserVar.UserID = row.entity.Id;
            $scope.dalimUserVar.Email = row.entity.UserName;
            $scope.dalimUserVar.Password = row.entity.Password;
        }
        $scope.SaveUpdateUser = function () {
            AdminService.SaveUpdateDalimUser($scope.dalimUserVar).then(function (res) {
                if (res.Response > 0) {
                    $scope.GetListOfDalimUsers();
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                } else NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                $("#dalimuserModal").modal('hide');
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.dalimuserCtrl']"));
        });
    }
    app.controller('mui.admin.dalimuserCtrl', ['$scope', 'AdminService', '$translate', muidalimuserCtrl]);
})(angular, app);