﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class PreviousUpdates : IPreviousUpdates
    {

        protected string _Version;
        protected string _Feature;
        protected string _Status;
        protected string _date;
        protected int _Id;
        protected bool _showButton;
        protected string _firstName;
        protected string _lastName;
        protected string _DocPath;
        protected string _username;

        	#region Constructors
        public PreviousUpdates() { }

        public PreviousUpdates(string pVersion, string pFeature, string pStatus, string pDate, int pId, bool pshowButton, int pmemberID, string pfirstName, string plastName, string pDocPath,string Pusername )
		{
            this._Version = pVersion;
            this._Feature = pFeature;
            this._date = pDate;
            this._Status = pStatus;
            this._Id = pId;
            this._showButton = pshowButton;
            this._firstName = pfirstName;
            this._lastName = plastName;
            this._DocPath = pDocPath;
            this._username = Pusername;
		}
				
	
		#endregion
		
		#region Public Properties
		
		public string Version
		{
            get { return _Version; }
            set { _Version = value; }
			
		}

        public string Feature
        {
            get { return _Feature; }
            set { _Feature = value; }
        }


        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string date
        {
            get { return _date; }
            set { _date = value; }
        }
     
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public bool showbutton
        {
            get { return _showButton; }
            set { _showButton = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string DocPath
        {
            get { return _DocPath; }
            set { _DocPath = value; }
        }
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
		#endregion 

    }
}
