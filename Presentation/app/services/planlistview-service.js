﻿(function(ng,app){"use strict";function PlanlistviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({ListSetting:ListSetting});function ListSetting(elementNode,entitytype){var request=$http({method:"get",url:"api/Metadata/ListSetting/"+elementNode+"/"+entitytype,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanlistviewService",['$http','$q',PlanlistviewService]);})(angular,app);