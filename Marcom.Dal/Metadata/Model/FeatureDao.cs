using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// FeatureDao object for table 'MM_Feature'.
	/// </summary>
	
	public partial class FeatureDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _description;
        protected int _moduleid;
        protected bool _isenable;
        protected bool _istopnavigation;
        
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public FeatureDao() {}

        public FeatureDao(string pCaption, string pDescription, int pModuleID, bool pIsEnable, bool pIsTopNavigation)
		{
			this._caption = pCaption; 
			this._description = pDescription;
            this._moduleid = pModuleID;
            this._isenable = pIsEnable;
            this._istopnavigation = pIsTopNavigation;
		}
				
		public FeatureDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set { _bIsChanged |= (_moduleid != value); _moduleid = value; }

        }

        public virtual bool IsEnable
        {
            get { return _isenable; }
            set { _bIsChanged |= (_isenable != value); _isenable = value; }

        }
        public virtual bool IsTopNavigation
        {
            get { return _istopnavigation; }
            set { _bIsChanged |= (_istopnavigation != value); _istopnavigation = value; }

        }
                 

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FeatureDao = "FeatureDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";
            public const string ModuleID = "ModuleID";
            public const string IsEnable = "IsEnable";
            public const string IsTopNavigation = "IsTopNavigation";	
            
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FeatureDao = "FeatureDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";
            public const string ModuleID = "ModuleID";
            public const string IsEnable = "IsEnable";
            public const string IsTopNavigation = "IsTopNavigation";	
		}

		#endregion
		
		
	}
	
}
