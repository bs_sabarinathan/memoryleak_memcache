///#source 1 1 /app/controllers/mui/planningtool/default/detail/ganttview-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailganttviewCtrl($scope, $timeout, $http, $resource, $compile, $window, $translate, PlanganttviewService) {
        var model;
        $scope.EndDate = false;
        $scope.Startdate = false;
        $scope.Calanderopen = function ($event, fromplace, obj) {
            $event.preventDefault();
            $event.stopPropagation();
            if (fromplace == "Startdate") {
                obj.s_IsOpened = true;
                obj.e_IsOpened = false;
            }
            else {
                obj.s_IsOpened = false;
                obj.e_IsOpened = true;
            }
        };
        $scope.IsPageLoad = false;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.copyrightYear = (new Date.create()).getFullYear();
        $scope.OnganttPeriodChange = function () {
            $scope.IsPageLoad = true;
        }
        $scope.$on("$destroy", function () {
            $(".ganttview-data-block, .reached, .notreached ").qtip('destroy', true);
            $('#ganttChart .ganttview-data-blocks-page').html('');
            $('#ganttChart .ganttview-header .ganttview-header-mains').html('');
            $('#ganttChart .ganttview-header .ganttview-header-subs').html('');
            $('#ganttChart .ganttview-header .ganttview-header-block').html('');
            $('#ganttChart .ganttview-grid-row').html('');
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.ganttviewCtrl']"));
        });
        $scope.GanttDetail = {
            GanttStartDate: "",
            GanttEndDate: "",
            GanttDescription: ""
        }
        $scope.data = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var dataGantEdit = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var IsResized = false;
        $scope.GanttHeaderList = null;
        $scope.DrawHeaderBar = function (zoomlevel) {
            $scope.settings.Mode = zoomlevel;
            $scope.IsPageLoad = false;
            GetAllGanttHeaderBar();
            lazyLoadGantt();
        };

        function GetAllGanttHeaderBar() {
            PlanganttviewService.GetAllGanttHeaderBar().then(function (getGanttresult) {
                if (getGanttresult.Response != null) {
                    $scope.GanttHeaderList = getGanttresult.Response;
                }
                drawHeader($scope.dates);
                ScrollToCurrentDate();
            });
        }

        function getCaptionfromPerod(Id) {
            PlanganttviewService.GetCaptionofPeriod(Id).then(function (CaptionResponse) {
                $scope.CaptionforPeriod = CaptionResponse.Response;
            });
        }
        $scope.GanttCurrentDateCollections = [{}];
        $scope.settings = {
            Mode: 'monthly',
            MonthlyDayWidth: 30,
            YearlyDayWidth: 2,
            QuarterlyDayWidth: 6,
            WeeklyDayWidth: 16,
            VerticallyConnected: null,
            HorizontallyConnected: null,
            DataColumnWidth: 100,
            StartDate: $scope.Gnttsettings.StartDate,
            EndDate: $scope.Gnttsettings.EndDate,
            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                onClick: function (data, BlockID) {
                    if (IsResized == false) {
                        $scope.GanttCurrentDateCollections = {};
                        PlanganttviewService.GetEntityPeriod(data.id).then(function (ganttDateRes) {
                            if (ganttDateRes.Response != null) {
                                $scope.GanttCurrentDateCollections = [{
                                    Description: "",
                                    EndDate: "",
                                    Entityid: 0,
                                    Id: 0,
                                    SortOrder: 0,
                                    Startdate: "",
                                    s_IsOpened: false,
                                    e_IsOpened: false
                                }]
                                $scope.GanttCurrentDateCollections.splice(0, 1);
                                for (var i = 0; i < ganttDateRes.Response.length; i++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = ganttDateRes.Response[i].Startdate.substr(0, 10)
                                    datstartval = new Date.create((datStartUTCval));
                                    datEndUTCval = ganttDateRes.Response[i].EndDate.substr(0, 10)
                                    datendval = new Date.create((datEndUTCval));
                                    $scope.GanttCurrentDateCollections.push({
                                        Description: ganttDateRes.Response[i].Description,
                                        EndDate: (ConvertDateToString(datendval)),
                                        Entityid: ganttDateRes.Response[i].Entityid,
                                        Id: ganttDateRes.Response[i].Id,
                                        SortOrder: ganttDateRes.Response[i].SortOrder,
                                        Startdate: (ConvertDateToString(datstartval)),
                                        s_IsOpened: false,
                                        e_IsOpened: false
                                    });
                                }
                            }
                        });
                        getCaptionfromPerod(data.id);
                        dataGantEdit.SortOid = data.SortOid
                        dataGantEdit.order = data.order
                        dataGantEdit.uniqueId = data.uniqueId
                        dataGantEdit.id = data.id
                        dataGantEdit.BlockID = BlockID
                        $("#moduleContextGantt").modal('show');
                    }
                    IsResized = false;
                },
                onDrag: function (data) {
                    EntityPeriodUpdate(data);
                },
                onResize: function (data) {
                    EntityPeriodUpdate(data);
                    IsResized = true;
                }
            },
        };
        $scope.ConvertGanttDatetoString = function (date) {
            if (typeof date == "string") return "";
            return ConvertDateFromStringToStringByFormat(date);
        }
        $scope.AddNextDate = function (enddate, startdate, index) {
            var ItemCnt = $scope.GanttCurrentDateCollections.length;
            if (enddate == null || startdate == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_4618.Caption'));
                return false;
            }
            var k = $scope.GanttCurrentDateCollections.length;
            $scope.GanttCurrentDateCollections.push({
                Description: "",
                EndDate: [],
                Id: 0,
                SortOrder: k + 1,
                Entityid: $scope.GanttCurrentDateCollections[0].Entityid,
                Startdate: [],
                s_IsOpened: false,
                e_IsOpened: false
            })
        }
        $scope.CheckValidStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.GanttCurrentDateCollections[currentindex - 1].EndDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.GanttCurrentDateCollections[currentindex].Startdate = null;
                $scope.GanttCurrentDateCollections[currentindex].EndDate = null;
            } else {
                if (startdate == null) {
                    $scope.GanttCurrentDateCollections[currentindex].EndDate = null;
                } else {
                    $scope.GanttCurrentDateCollections[currentindex].EndDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };
        $scope.CheckValidEndDate = function (enddate, startdate, currentindex) {
            if ((enddate != null && enddate <= startdate) || (enddate == startdate)) {
                bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                $scope.GanttCurrentDateCollections[currentindex].EndDate = null;
            }
        };
        $scope.RemoveDate = function (index, ID) {
            if ($scope.GanttCurrentDateCollections.length > 1) {
                if (ID > 0) {
                    $scope.deletePeriodDate(ID);
                }
                $scope.GanttCurrentDateCollections.splice(index, 1);
            } else {
                $scope.GanttCurrentDateCollections.splice(index, 1, {
                    Description: "",
                    EndDate: "",
                    Entityid: 0,
                    Id: 0,
                    SortOrder: 0,
                    Startdate: "",
                    s_IsOpened: false,
                    e_IsOpened: false
                });
            }
        }
        $scope.deletePeriodDate = function (periodid, ID) {
            bootbox.confirm(($translate.instant('LanguageContents.Res_4090.Caption')), function (result) {
                if (result) {
                    PlanganttviewService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                        if (deletePerById.StatusCode == 200) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            UpdateEntityPeriodTree(dataGantEdit.id);
                            var data = dataGantEdit;
                            var UpdateEntityPeriod = {};
                            UpdateEntityPeriod.EntityID = data.id;
                            UpdateEntityPeriod.GanttCurrentDateCollections = $scope.GanttCurrentDateCollections;
                            PlanganttviewService.InsertUpdateEntityPeriodLst(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                                if (EntityPeriodResult.Response.length > 0) {
                                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                            var UniqueKey = UniqueKEY($scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"]);
                                            if (UniqueKey == data.uniqueId) {
                                                var UniqueID = UniqueKEY(data.uniqueId);
                                                var Enddd = UpdateEntityPeriod.GanttCurrentDateCollections[UpdateEntityPeriod.GanttCurrentDateCollections.length - 1].EndDate;
                                                var stardd = UpdateEntityPeriod.GanttCurrentDateCollections[0].Startdate;
                                                var Enddd1 = dateFormat(new Date.create(Enddd), 'yyyy-mm-dd');
                                                var stardd1 = dateFormat(new Date.create(stardd), 'yyyy-mm-dd');
                                                $scope.ListViewDetails[i].data.Response.Data[j]["TempPeriod"] = stardd1 + " " + Enddd1;
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                                var ganttDrawHtml = drawSpecifiedBlock($scope.ListViewDetails[i].data.Response.Data[j]);
                                                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').html(ganttDrawHtml);
                                                $timeout(function () {
                                                    CallBehavior();
                                                }, 1000);
                                                $(function () {
                                                    $(".ganttview-data-block, .reached, .notreached ").qtip({
                                                        style: {
                                                            classes: "qtip-dark qtip-shadow qtip-rounded",
                                                            title: {
                                                                'display': 'none'
                                                            }
                                                        },
                                                        show: {
                                                            event: 'mouseenter click unfocus',
                                                            solo: true
                                                        },
                                                        events: {
                                                            hide: function (event, api) {
                                                                event: 'unfocus click mouseleave mouseup mousedown'
                                                            }
                                                        },
                                                        hide: {
                                                            delay: 0,
                                                            fixed: false,
                                                            effect: function () {
                                                                $(this).fadeOut(100);
                                                            },
                                                            event: 'unfocus click mouseleave mouseup mousedown'
                                                        },
                                                        position: {
                                                            my: 'bottom left',
                                                            at: 'top left',
                                                            viewport: $(window),
                                                            adjust: {
                                                                method: 'shift',
                                                                mouse: true
                                                            },
                                                            corner: {
                                                                target: 'bottom left',
                                                                tooltip: 'bottom left',
                                                                mimic: 'top'
                                                            },
                                                            target: 'mouse'
                                                        }
                                                    });
                                                });
                                                return false;
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                        }
                    });
                }
            });
        }

        function UpdateEntityPeriodTree(entityid) {
            PlanganttviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (resultperiod) {
                if (resultperiod.StatusCode == 200) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                return false;
                            } else {
                                if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = resultperiod.Response;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
        }
        $scope.GanttRedrawDetail = function (data) {
            dataGantEdit.start = data.start;
            dataGantEdit.end = data.end;
            dataGantEdit.desc = data.desc;
            var block = $('.ganttview-data-block[data-dateblockuniqueid="' + data.order + '-' + data.uniqueId + '"]');
            block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + ConvertDateFromStringToString(data.start) + "</br><b>End Date:</b> " + ConvertDateFromStringToString(data.end) + " </br><b>Comment:</b> " + data.desc + "").qtip({
                content: {
                    text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                },
                style: {
                    classes: "qtip-dark qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
                        event: 'unfocus click mouseleave mouseup mousedown'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(100);
                    },
                    event: 'unfocus click mouseleave mouseup mousedown'
                },
                position: {
                    my: 'bottom left',
                    at: 'top left',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom left',
                        tooltip: 'bottom left',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
        }
        $scope.GanttsaveDetail = function () {
            var data = dataGantEdit;
            var UpdateEntityPeriod = {};
            UpdateEntityPeriod.EntityID = data.id;
            for (var i = 0; i < $scope.GanttCurrentDateCollections.length; i++) {
                if (typeof ($scope.GanttCurrentDateCollections[i].Startdate) != "string")
                    $scope.GanttCurrentDateCollections[i].Startdate = dateFormat($scope.GanttCurrentDateCollections[i].Startdate, $scope.format);
                if (typeof ($scope.GanttCurrentDateCollections[i].EndDate) != "string")
                    $scope.GanttCurrentDateCollections[i].EndDate = dateFormat($scope.GanttCurrentDateCollections[i].EndDate, $scope.format);
            }
            UpdateEntityPeriod.GanttCurrentDateCollections = $scope.GanttCurrentDateCollections;
            PlanganttviewService.InsertUpdateEntityPeriodLst(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                if (EntityPeriodResult.Response.length > 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $("#moduleContextGantt").modal('hide');
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var UniqueKey = UniqueKEY($scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"]);
                            if (UniqueKey == data.uniqueId) {
                                var UniqueID = UniqueKEY(data.uniqueId);
                                var Enddd = UpdateEntityPeriod.GanttCurrentDateCollections[UpdateEntityPeriod.GanttCurrentDateCollections.length - 1].EndDate;
                                var stardd = UpdateEntityPeriod.GanttCurrentDateCollections[0].Startdate;
                                var Enddd1 = dateFormat(new Date.create(Enddd), 'yyyy-mm-dd');
                                var stardd1 = dateFormat(new Date.create(stardd), 'yyyy-mm-dd');
                                $scope.ListViewDetails[i].data.Response.Data[j]["TempPeriod"] = stardd1 + " " + Enddd1;
                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                var ganttDrawHtml = drawSpecifiedBlock($scope.ListViewDetails[i].data.Response.Data[j]);
                                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').html(ganttDrawHtml);
                                $timeout(function () {
                                    CallBehavior();
                                }, 1000);
                                $(function () {
                                    $(".ganttview-data-block, .reached, .notreached ").qtip({
                                        style: {
                                            classes: "qtip-dark qtip-shadow qtip-rounded",
                                            title: {
                                                'display': 'none'
                                            }
                                        },
                                        show: {
                                            event: 'mouseenter click unfocus',
                                            solo: true
                                        },
                                        events: {
                                            hide: function (event, api) {
                                                event: 'unfocus click mouseleave mouseup mousedown'
                                            }
                                        },
                                        hide: {
                                            delay: 0,
                                            fixed: false,
                                            effect: function () {
                                                $(this).fadeOut(100);
                                            },
                                            event: 'unfocus click mouseleave mouseup mousedown'
                                        },
                                        position: {
                                            my: 'bottom left',
                                            at: 'top left',
                                            viewport: $(window),
                                            adjust: {
                                                method: 'shift',
                                                mouse: true
                                            },
                                            corner: {
                                                target: 'bottom left',
                                                tooltip: 'bottom left',
                                                mimic: 'top'
                                            },
                                            target: 'mouse'
                                        }
                                    });
                                });
                                return false;
                            }
                        }
                    }
                }
            });
        }

        function drawSpecifiedBlock(data) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            var UniqueId = UniqueKEY(data["UniqueKey"]);
            var ClassName = GenateClass(UniqueId);
            ClassName += " mo" + UniqueId;
            blockDivPage += "<div data-over='true' data-EntityLevel='" + data["Level"] + "' data-uniquekey='" + UniqueId + "' class='" + ClassName + " ganttview-data-block-container'>";
            $($.parseXML(data['Period'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var endDt = $(this).attr('e');
                var duration = 0;
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                }
                if (startDt != undefined) {
                    startDt = ConvertDateFromStringToString(startDt);
                    endDt = ConvertDateFromStringToString(endDt);
                }
                if ($scope.GanttLock == true) {
                    if (data['Permission'] == 3 || data['Permission'] == 0) {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    } else {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    }
                } else {
                    blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block Ganttlockdummy' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                        id: parseInt(data['Id']),
                        order: parseInt($(this).attr("o")),
                        start: $(this).attr("s"),
                        end: $(this).attr("e"),
                        desc: $(this).attr("d"),
                        uniqueId: UniqueId,
                        SortOid: $(this).attr("sid")
                    }) + "'></div>";
                }
            });
            $($.parseXML(data['MileStone'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var MileStoneStatus = $(this).attr('ms');
                var MileStoneName = $(this).attr('n');
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                }
                if (startDt != undefined) {
                    var startDtFormat = ConvertDateFromStringToString(startDt);
                }
                var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
            });
            blockDivPage += '</div>';
            return blockDivPage;
        }

        function EntityPeriodUpdate(data) {
            var UpdateEntityPeriod = {};
            UpdateEntityPeriod.ID = data.order;
            UpdateEntityPeriod.StartDate = data.start;
            UpdateEntityPeriod.EndDate = data.end;
            UpdateEntityPeriod.Description = data.desc;
            PlanganttviewService.UpdateEntityPeriod(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                if (EntityPeriodResult.Response.length > 0) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var UniqueKey = UniqueKEY($scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"]);
                            if (UniqueKey == data.uniqueId) {
                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                $scope.GanttRedrawDetail(data);
                                return false;
                            }
                        }
                    }
                }
            });
        }

        function EntityPeriodUpdateLSt(data) {
            var UpdateEntityPeriod = {};
            UpdateEntityPeriod.EntityID = data.id;
            UpdateEntityPeriod.GanttCurrentDateCollections = $scope.GanttCurrentDateCollections;
            PlanganttviewService.InsertUpdateEntityPeriodLst(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                if (EntityPeriodResult.Response.length > 0) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $("#moduleContextGantt").modal('hide');
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var UniqueKey = UniqueKEY($scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"]);
                            if (UniqueKey == data.uniqueId) {
                                var UniqueID = UniqueKEY(data.uniqueId);
                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                var ganttDrawHtml = drawSpecifiedBlock($scope.ListViewDetails[i].data.Response.Data[j]);
                                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').html(ganttDrawHtml);
                                $(function () {
                                    $(".ganttview-data-block, .reached, .notreached ").qtip({
                                        style: {
                                            classes: "qtip-dark qtip-shadow qtip-rounded",
                                            title: {
                                                'display': 'none'
                                            }
                                        },
                                        show: {
                                            event: 'mouseenter click unfocus',
                                            solo: true
                                        },
                                        events: {
                                            hide: function (event, api) {
                                                event: 'unfocus click mouseleave mouseup mousedown'
                                            }
                                        },
                                        hide: {
                                            delay: 0,
                                            fixed: false,
                                            effect: function () {
                                                $(this).fadeOut(100);
                                            },
                                            event: 'unfocus click mouseleave mouseup mousedown'
                                        },
                                        position: {
                                            my: 'bottom left',
                                            at: 'top left',
                                            viewport: $(window),
                                            adjust: {
                                                method: 'shift',
                                                mouse: true
                                            },
                                            corner: {
                                                target: 'bottom left',
                                                tooltip: 'bottom left',
                                                mimic: 'top'
                                            },
                                            target: 'mouse'
                                        }
                                    });
                                });
                                return false;
                            }
                        }
                    }
                }
            });
        }
        $scope.dates = getDates($scope.settings.StartDate, $scope.settings.EndDate);
        var colStatus = false;
        $scope.$on('templateCreation', function (event) {
            GanttViewLoad();
            $(window).AdjustHeightWidth();
        });

        function GanttViewLoad() {
            if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
            if ($scope.ListTemplate.length > 0) {
                $("#ganttChart #GanttDataContainer").html($scope.GanttViewDataPageTemplate);
                $("#ganttChart #GanttBlockContainer").html($scope.GanttViewBlockPageTemplate);
                $('#ganttChart .ganttview-data').scroll(function () {
                    $("#ganttChart .ganttview-slide-container").scrollLeft($(this).scrollLeft());
                    $("#ganttChart .ganttview-list-container-data").scrollTop($(this).scrollTop());
                    $("#EntitiesTree").scrollTop($(this).scrollTop());
                });
                GetAllGanttHeaderBar();
                if ($scope.GanttViewHeaders != null) {
                    drawDataHeader($scope.GanttViewHeaders);
                }
                ScrollToCurrentDate();
            }
            $(window).AdjustHeightWidth();
        }
        if ($scope.GanttViewDataPageTemplate != '') {
            GanttViewLoad();
            lazyLoadGantt();
        }

        function lazyLoadGantt() {
            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                LoadGanttView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID);
            }
        }

        function ScrollToCurrentDate() {
            var ToDay;
            var DaysToScroll;
            switch ($scope.settings.Mode) {
                case 'daily':
                    ToDay = new Date.create();
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.MonthlyDayWidth;
                    break;
                case 'quarterly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.QuarterlyDayWidth;
                    break;
                case 'weekly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.WeeklyDayWidth;
                    break;
                default:
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.YearlyDayWidth;
                    break;
            }
            $(".ganttview-data").scrollLeft(DaysToScroll);
        }

        function GetCalenderStartDate() {
            return new Date.create(parseInt((new Date.create).getFullYear()) - 1, 0, 1);
        }
        $scope.$on('onTreePageCreation', function (event, data) {
            LoadGanttView(data[0], data[1], data[2]);
        });

        function LoadGanttView(ListContentData, PageIndex, UniqueID) {
            var value = drawBlock(ListContentData.Response);
            if (UniqueID != "") {
                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').after(value.Block);
                $('#ganttChart tr[data-uniquekey="' + UniqueID + '"]').after(value.Data);
            } else {
                $('#ganttChart .ganttview-data-blocks-page[data-page="' + PageIndex + '"]').html(value.Block).removeAttr('style');
                $('#ganttChart tbody[data-page="' + PageIndex + '"]').html(value.Data).removeAttr('style');
            }
            $(window).AdjustHeightWidth();
            $(function () {
                $(".ganttview-data-block, .reached, .notreached ").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
        }

        function CallBehavior() {
            apply($('#ganttChart'), $scope.settings);
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function daysBetween(start, end) {
            if (!start || !end) {
                return 0;
            }
            start = Date.parse(start);
            end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }
            return Math.round((end - start) / (1000 * 60 * 60 * 24));
        }

        function isWeekend(date) {
            return date.getDay() % 6 == 0;
        }

        function getWeek(d) {
            var target = new Date.create(d.valueOf());
            var dayNr = (d.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var jan4 = new Date.create(target.getFullYear(), 0, 4);
            var dayDiff = (target - jan4) / 86400000;
            var weekNr = 1 + Math.ceil(dayDiff / 7);
            return weekNr;
        }

        function getDates(start, end) {
            if (start == "NaN-NaN-NaN" || start == "") start = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            if (end == "NaN-NaN-NaN" || end == "") end = Date.parse('+1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            start = Date.parse(start);
            end = Date.parse(end);
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start];
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) {
                    dates[next.getFullYear()] = [];
                }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }
            return dates;
        }

        function drawHeader(dates) {
            var totalW = 0;
            var mainHeaderDivContent = '';
            var subHeaderDivContent = '';
            var rowDivContent = '';
            var ganttHeaderBar = '';
            switch ($scope.settings.Mode) {
                case 'daily':
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.MonthlyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (isWeekend(dates[y][m][d])) {
                                                subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            } else {
                                                subHeaderDivContent += '<div class="ganttview-header-sub">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'monthly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.YearlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                }
                            }
                            mainHeaderDivContent += '<div title="<b>Name: </b></br><b>Milestone Date: </b> </br><b>Descripton: </b>" class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">' + y + '</div>';
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'quarterly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            var Qno = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.QuarterlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                    if (Qno != Math.floor((parseInt(m) + 1) / 3)) {
                                        mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">Q' + (Qno + 1) + ' - ' + y + '</div>';
                                        yw = 0;
                                        Qno = Math.floor((parseInt(m) + 1) / 3);
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'weekly':
                    var possion = 1;
                    var weekno = getWeek(Date.parse($scope.settings.StartDate));
                    var weekLen = 0;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.WeeklyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (weekno != getWeek(dates[y][m][d])) {
                                                if ((possion % 2) == 1) {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                } else {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                }
                                                possion += 1;
                                                weekno = getWeek(dates[y][m][d]);
                                                weekLen = 0;
                                            }
                                            weekLen += $scope.settings.WeeklyDayWidth;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ((possion % 2) == 1) {
                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    } else {
                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                default:
            }
            $('#ganttChart .ganttview-header .ganttview-header-mains').html(mainHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-subs').html(subHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-block').html(ganttHeaderBar);
            $('#ganttChart .ganttview-header').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid-row').html(rowDivContent);
            $('#ganttChart #GanttBlockContainer').css("width", totalW + "px");
            $(function () {
                $(".qtip-header-block").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded"
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
        }

        function drawBlock(data) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = UniqueKEY(data.Data[i]["UniqueKey"]);
                var ClassName = GenateClass(UniqueId);
                ClassName += " mo" + UniqueId;
                blockDivPage += "<div data-over='true' data-EntityLevel='" + data.Data[i]["Level"] + "' data-uniquekey='" + UniqueId + "' class='" + ClassName + " ganttview-data-block-container'>";
                $($.parseXML(data.Data[i]['Period'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var endDt = $(this).attr('e');
                    var duration = 0;
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                        case 'monthly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                            break;
                        case 'quarterly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                            break;
                        case 'weekly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                            break;
                        default:
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                    }
                    startDt = ConvertDateFromStringToString(startDt);
                    endDt = ConvertDateFromStringToString(endDt);
                    if ($scope.GanttLock == true) {
                        if (data.Data[i]['Permission'] == 3 || data.Data[i]['Permission'] == 0) {
                            blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: UniqueId,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        } else {
                            blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "' title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: UniqueId,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        }
                    } else {
                        blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data.Data[i]['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                        updateganttlocksettings();
                    }
                });
                $($.parseXML(data.Data[i]['MileStone'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var MileStoneStatus = $(this).attr('ms');
                    var MileStoneName = $(this).attr('n');
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                        case 'monthly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                            break;
                        case 'quarterly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                            break;
                        case 'weekly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                            break;
                        default:
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                    }
                    if (startDt != undefined) {
                        var startDtFormat = ConvertDateFromStringToString(startDt);
                    }
                    var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                    blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
                });
                blockDivPage += '</div>';
                blockDivDataPage += "<tr data-over='true' data-EntityLevel='" + data.Data[i]["Level"] + "' data-uniquekey='" + UniqueId + "'class='" + ClassName + " noBGimg' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivDataPage += "<td><span><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        var bclr = '';
                        var txtclr = '';
                        if ($scope.GanttViewHeaders[k].Id == "71") {
                            var txtcolor = 'ffffff';
                            var backgroundcolor = data.Data[i].StatusColorCode.trim();
                            txtcolor = data.Data[i].StatusColorCode.trim();
                            if (txtcolor == null || txtcolor.trim() == "" || txtcolor.trim() == "-") {
                                txtcolor = 'ffffff';
                                backgroundcolor = 'ffffff';
                            }
                            var r = parseInt(txtcolor.substr(0, 2), 16);
                            var g = parseInt(txtcolor.substr(2, 2), 16);
                            var b = parseInt(txtcolor.substr(4, 2), 16);
                            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                            txtcolor = (yiq >= 225) ? 'black' : 'white';

                            bclr = 'style="background-color:#' + backgroundcolor + ';"';
                            txtclr = 'style = "color: ' + txtcolor + ';"';
                        }
                        if ($scope.GanttViewHeaders[k].Type != "5")
                            blockDivDataPage += '<td  ' + bclr + ' ><span  ' + txtclr + ' >' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                        else {
                            if (data.Data[i][$scope.GanttViewHeaders[k].Field] == "-") blockDivDataPage += '<td><span>' + "-" + '</span></td>';
                            else blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == (null || "          ") ? "-" : dateFormat(data.Data[i][$scope.GanttViewHeaders[k].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                        }
                    }
                }
                blockDivDataPage += '</tr>';
            }
            return {
                Block: blockDivPage,
                Data: blockDivDataPage
            };
        }

        function drawData(data) {
            var blockDivDataPage = '';
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = UniqueKEY(data.Data[i]["UniqueKey"]);
                var ClassName = GenateClass(UniqueId);
                ClassName += " mo" + UniqueId;
                blockDivDataPage += "<tr data-over='true' data-EntityLevel='" + data.Data[i]["Level"] + "' data-uniquekey='" + UniqueId + "' class='" + ClassName + "' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivDataPage += "<td><span class=\"donno\"><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                    }
                }
                blockDivDataPage += '</tr>';
            }
            return blockDivPage;
        }
        $scope.FilterID = {
            selectedFilterID: 0,
            selectedfilterattribtues: []
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };

        function drawDataHeader(ColumnDefs) {
            var HeaderHtml = '';
            var totalColumn = 0;
            for (var j = 0; j < ColumnDefs.length; j++) {
                if (ColumnDefs[j].Field != "68" && ColumnDefs[j].Type != "10") {
                    if ($scope.IsDesc == true) HeaderHtml += '<th><i class="icon-caret-down cursor-pointer margin-right5x"></i><a data-Column=' + ColumnDefs[j].Field + '><span>' + ColumnDefs[j].DisplayName + '</span></a></th>';
                    else HeaderHtml += '<th><i class="icon-caret-up cursor-pointer margin-right5x"></i><a data-Column=' + ColumnDefs[j].Field + '><span>' + ColumnDefs[j].DisplayName + '</span></a></th>';
                    totalColumn += 1;
                }
            }
            $('#ganttChart #ganttviewDataHeader').html(HeaderHtml);
            $('.ganttview-left').width(totalColumn * 100);
            $('.ganttview-right').css('margin-left', (totalColumn * 100) + 'px');
        }
        $("#ganttviewDataHeader").click(function (e) {
            var TargetControl = $(e.target);
            var GlobalChildNode = new Array();
            GlobalChildNode = $window.ListofEntityID;
            $scope.AppllyFilterObj.selectedattributes = [];
            var col = "";
            if (TargetControl[0].tagName == "SPAN") {
                col = TargetControl.parent().attr("data-Column");
            } else if (TargetControl[0].tagName == "A") {
                col = TargetControl.attr("data-Column");
            }
            if (col != "") {
                $scope.sortOrder = col;
                if ($scope.sortOrder === col) {
                    $scope.IsDesc = !$scope.IsDesc;
                } else {
                    $scope.sortOrder = col;
                    $scope.IsDesc = false;
                }
                if ($.isNumeric($scope.sortOrder)) $scope.$emit('OnGanttViewSorting', $scope.sortOrder, $scope.IsDesc);
            }
        });

        function apply(parent, settings) {
            if (settings.behavior.clickable) {
                bindBlockClick(parent, settings.behavior.onClick);
            }
            if (settings.behavior.resizable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockResize(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'monthly':
                        bindBlockResize(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'quarterly':
                        bindBlockResize(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'weekly':
                        bindBlockResize(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    default:
                }
            }
            if (settings.behavior.draggable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockDrag(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'monthly':
                        bindBlockDrag(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'quarterly':
                        bindBlockDrag(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'weekly':
                        bindBlockDrag(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    default:
                }
            }
        }

        function bindBlockClick(parent, callback) {
            $("div.ganttview-data-block", parent).on("click", function () {
                var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                var block = $(this);
                if (islock == 3 || islock == 0) {
                    $scope.IsLock = true;
                } else {
                    $scope.IsLock = false;
                }
                if ($scope.IsLock == true || $scope.GanttLock == true) {
                    block.css('position', 'absolute');
                    $("#moduleContextGantt").modal('hide');
                } else if (callback) {
                    if ($(this).attr('data-ResizeDrag') == 'true') {
                        $(this).removeAttr('data-ResizeDrag');
                    } else {
                        callback(JSON.parse($(this).attr("block-data")), $(this).attr('data-dateblockuniqueid'));
                    }
                }
            });
        }

        function bindBlockResize(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).resizable({
                grid: cellWidth,
                handles: "e,w",
                start: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    }
                },
                stop: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    } else {
                        var data = JSON.parse($(this).attr("block-data"));
                        updateDataAndPosition(parent, block, cellWidth, startDate);
                        if (DateValidate(JSON.parse($(this).attr("block-data")))) {
                            if (callback) {
                                callback(JSON.parse($(this).attr("block-data")));
                            }
                        } else {
                            bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                            var startDt = data.start;
                            var endDt = data.end;
                            var duration = 0;
                            var formLeft = 0;
                            switch (settings.Mode) {
                                case 'daily':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                                case 'monthly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                                    break;
                                case 'quarterly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                                    break;
                                case 'weekly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                                    break;
                                default:
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                            }
                            block.css('width', duration + 'px');
                            block.css('margin-left', formLeft + 'px');
                            block.attr('block-data', JSON.stringify(data));
                        }
                    }
                }
            });
        }

        function bindBlockDrag(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).draggable({
                axis: "x",
                grid: [cellWidth, cellWidth],
                start: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        block.removeAttr('position', 'relative');
                        return false;
                    } else {
                        block.css('position', 'relative');
                    }
                },
                stop: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        block.removeAttr('position', 'relative');
                    } else {
                        block.css('position', 'relative');
                        ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings);
                    }
                }
            });
        }

        function ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings) {
            block.css('position', 'absolute');
            block.attr('data-ResizeDrag', 'true');
            var data = JSON.parse(block.attr("block-data"));
            updateDataAndPosition(parent, block, cellWidth, startDate);
            if (DateValidate(JSON.parse(block.attr("block-data")))) {
                data = JSON.parse(block.attr("block-data"));
                block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "").qtip({
                    content: {
                        text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                    },
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
                if (callback) {
                    callback(JSON.parse(block.attr("block-data")));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                var startDt = data.start;
                var endDt = data.end;
                var duration = 0;
                var formLeft = 0;
                switch (settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                }
                block.css('width', duration + 'px');
                block.css('margin-left', formLeft + 'px');
                block.attr('block-data', JSON.stringify(data));
            }
        }

        function updateDataAndPosition(parent, block, cellWidth, startDate) {
            startDate = Date.parse(startDate);
            var container = $("div.ganttview-slide-container", parent);
            var scroll = container.scrollLeft();
            var offset = block.offset().left - container.offset().left - 1 + scroll;
            var blockdata = JSON.parse(block.attr("block-data"));
            var daysFromStart = Math.round(offset / cellWidth);
            var newStart = startDate.clone().addDays(daysFromStart);
            blockdata.start = newStart.toString("yyyy-MM-dd");
            var width = block.outerWidth();
            var numberOfDays = Math.round(width / cellWidth) - 1;
            blockdata.end = newStart.clone().addDays(numberOfDays).toString("yyyy-MM-dd");
            block.attr('block-data', JSON.stringify(blockdata));
            block.css("top", "").css("left", "").css("position", "relative").css("margin-left", offset + "px");
        }

        function DateValidate(data) {
            var IsOverlap = false;
            var MainObject = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").parent();
            var currentHtml = MainObject.html();
            var childCount = MainObject.children(".ganttview-data-block").length;
            var NextjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").next().attr('block-data');
            var PrevjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").prev().attr('block-data');
            if (NextjsonData != undefined) {
                var NextObjectData = jQuery.parseJSON(NextjsonData);
            }
            if (PrevjsonData != undefined) {
                var PrevObjectData = jQuery.parseJSON(PrevjsonData);
            }
            if (childCount == 1) {
                IsOverlap = true;
            } else if (data.SortOid == 1) {
                if (data.end < NextObjectData.start) {
                    IsOverlap = true;
                }
            } else if (data.SortOid == childCount) {
                if (data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            } else if (childCount > 1 && data.SortOid < childCount) {
                if (data.end < NextObjectData.start && data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            }
            return IsOverlap
        }
        $scope.SetGanttLock = function () {
            var entitydata = $("div.ganttview-data-block");
            if (entitydata[0] != undefined) {
                var ParentEntity = entitydata[0].attributes["block-data"].value;
                var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse(ParentEntity).id + '"]').attr('data-Permission');
                if (parseInt(islock) > 0) {
                    var ganttlock = $("#SetGanttLock").hasClass("icon-lock");
                    if (ganttlock == true) {
                        $timeout(function () {
                            CallBehavior();
                        }, 1000);
                        $("#SetGanttLock").removeClass("icon-lock");
                        $("#SetGanttLock").addClass("icon-unlock");
                        $scope.GanttLock = false;
                        $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                        $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
                    } else {
                        $("#SetGanttLock").removeClass("icon-unlock");
                        $("#SetGanttLock").addClass("icon-lock");
                        $scope.GanttLock = true;
                        $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                        $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
                    }
                }
            }
        }

        function updateganttlocksettings() {
            var ganttlock = $("#SetGanttLock").hasClass("icon-lock");
            if (ganttlock == false) {
                $timeout(function () {
                    CallBehavior();
                }, 1000);
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-draggable')
            } else {
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-draggable')
            }
        }
    }
    app.controller("mui.planningtool.default.detail.ganttviewCtrl", ['$scope', '$timeout', '$http', '$resource', '$compile', '$window', '$translate', 'PlanganttviewService', muiplanningtooldefaultdetailganttviewCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/SubEntityCreation-controller.js

(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtool.SubEntityCreationCtrl",
    function muiplanningtoolSubEntityCreationCtrl($scope, $location, $resource, $http, $cookies, $compile, $window, $translate, SubentitycreationService, $modalInstance, params) {

        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }

        // --- Define Controller Methods. ------------------- //
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }
        var SubentityTypeCreationTimeout = {};
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, object, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            if (Call_from == "DueDate") {
                object.StartDate = false; object.EndDate = false; object.DateAction = false; object.DueDate = true;
            }
            else if (Call_from == "StartDate") {
                object.EndDate = false; object.DueDate = false; object.DateAction = false; object.StartDate = true;
            }
            else if (Call_from == "EndDate") {
                object.StartDate = false; object.DueDate = false; object.DateAction = false; object.EndDate = true;
            }
            else {
                object.StartDate = false; object.EndDate = false; object.DueDate = false; object.DateAction = true;
            }
        };
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            }
            else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };

        $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];

        $scope.listofValidations = [];
        var remValtimer = "";
        $scope.DynamicAddValidation = function (attrID) {
            var IsValExist = $.grep($scope.listValidationResult, function (e) {
                return parseInt(e[0].split('_')[1]) == attrID;
            });

            if (IsValExist == null || IsValExist.length == 0) {
                var getVal = $.grep($scope.listofValidations, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });
                if (getVal != null && getVal.length > 0) {
                    $scope.listValidationResult.push(getVal[0]);

               //   $("#btnTempSub").click();
                 //  $("#EntityMetadata").removeClass('notvalidate');                
                 $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                       $("#btnTempSub").click();
                    }, 100);
                }
            }
        }

        $scope.DynamicRemoveValidation = function (attrID) {
            if ($scope.listValidationResult != undefined) {
                var IsValExist = $.grep($scope.listValidationResult, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });

                if (IsValExist != null && IsValExist.length > 0) {
                    $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                        return parseInt(e[0].split('_')[1]) != attrID;
                    });

                //  $("#btnTempSub").click();
                 //  $("#EntityMetadata").removeClass('notvalidate');              
                   $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {

                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                        $("#btnTempSub").click();
                    }, 10);
                }
            }
        }  

        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var Attributetypename = '';
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }

                            if ($scope.fields[Attributetypename] == undefined) {
                                if (attrTypeID == 1) {
                                    $scope.fields["TextSingleLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 2) {
                                    $scope.fields["TextMultiLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 3) {
                                    $scope.fields["ListSingleSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 4) {
                                    $scope.fields["ListMultiSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 5) {
                                    $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                }
                                else if (attrTypeID == 6) {
                                    $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = 0;
                                }
                                else if (attrTypeID == 12) {
                                    $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                                }
                                else if (attrTypeID == 17) {
                                    $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                }
                                continue;
                                //return false;
                            }
                            if (relationobj[i].AttributeTypeID == 4) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                                if (parseInt($scope.fields[Attributetypename]) == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else {
                                if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            //return false;
                        }
                        if (attrTypeID == 1) {
                            $scope.fields["TextSingleLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 2) {
                            $scope.fields["TextMultiLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 3) {
                            $scope.fields["ListSingleSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 5) {
                            $scope.fields["DatePart_" + ID[0]] = null;
                        }
                        else if (attrTypeID == 6) {
                            $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = 0;
                        }
                        else if (attrTypeID == 12) {
                            $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                        }
                        else if (attrTypeID == 17) {
                            $scope.fields["ListTagwords_" + ID[0]] = [];
                        }
                        try {
                            $scope.DynamicRemoveValidation(parseInt(ID[0]));
                        }
                        catch (e) { };
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        };

        //Gets all the tabs list in the systems..
        function GetEntityTypeTabCollections(Id, Caption, CurrentEntityId) {
            SubentitycreationService.GetPlantabsettings().then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.ShowFinancial = gettabresult.Response.Financials;
                    $scope.ShowObjective = gettabresult.Response.Objectives;
                    $scope.ShowAttachments = gettabresult.Response.Attachments;


                    if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 5, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }



                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }
                else {
                    $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }

            });
        }

        $scope.attributeGroupDetailAsTab = [];

        function GenerateTabNavigations(Id, Caption, CurrentEntityId) {
            var AttrGroup = false;
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            for (var t = 0, tab; tab = $scope.WizardSteps[t++];) {
                if (tab.StepId == 1) {
                    tabHtml += '<li data-target=\"' + tab.StepDOMID + '\"  ng-click=\"changeSubentityTab2($event)\"><span class="badge badge-info">' + tab.StepId + '</span>' + tab.StepName + '<span class="chevron"></span></li>';
                    AttrGroup = true;
                    tabHtml += '<li ng-repeat=\"grpname in DynamicAttributeGroupTabs\" data-target=\"#step6\" ng-click=\"attributeGroupDetailAsTab.length = 0; attributeGroupDetailAsTab.push(grpname); changeSubentityTab2($event);\">';
                    tabHtml += '<span class="badge badge-info">{{$index + 2}}</span>{{grpname.Caption}}<span class="chevron"></span></li>';
                }
                else
                    tabHtml += '<li data-target=\"' + tab.StepDOMID + '\"  ng-click=\"changeSubentityTab2($event)\"><span class="badge badge-info">' + ($scope.DynamicAttributeGroupTabs.length + tab.StepId) + '</span>' + tab.StepName + '<span class="chevron"></span></li>';
            }
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));

            SubentityTypeCreationTimeout.createdynwiz = setTimeout(function () {
                $('#SubentityMyWizard').wizard();
                $('#SubentityMyWizard').wizard('stepLoaded');
                window["tid_wizard_steps_all_complete_count"] = 0;
                window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                $('#btnWizardNext').show();     //intially wizard Next button is enalbed
                $('#btnWizardPrev').hide();     //intially wizard Previous button is DISABLED
                $scope.CreateDynamicControls(Id, Caption, CurrentEntityId);
            }, 20);
        }

        //Wizard enable/disable setup
        $scope.EntityStatusResult = [];
        $scope.Dropdown = [];
        $scope.DropDownTreeOptionValues = {};

        // handling wizard tab click event in the wizard
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.changeTab = function () {

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;

            if (currentWizardStep === 1) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            }
            else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            }

            else {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if ($scope.DynamicAttributeGroupTabs.length > 0) {
                if (currentWizardStep === ($scope.DynamicAttributeGroupTabs.length + 3)) {
                    EntityMetadata();
                }
            }
            else {
                if (currentWizardStep === 3) {
                    SubentityTypeCreationTimeout.loadmetadata = setTimeout(function () { EntityMetadata(); }, 100);
                }
            }

        }
        $scope.changeSubentityTab2 = function (e) {
            $("#btnTempSub").click();
           $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#SubentityMyWizard ul.steps").find("li").addClass("complete");
            $("#SubentityMyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 4) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }

        // change previous tab in the wizard

        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('previous');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;

            if ($scope.DynamicAttributeGroupTabs.length > 0 && currentWizardStep > 1 && currentWizardStep <= ($scope.DynamicAttributeGroupTabs.length + 1)) {
                $scope.attributeGroupDetailAsTab.length = 0;
                $scope.attributeGroupDetailAsTab.push($scope.DynamicAttributeGroupTabs[currentWizardStep - 2]);
            }

            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
            else {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }

        // change next tab in the wizard

        $scope.changenexttab = function () {           
            $("#btnTempSub").click();
           $("#EntityMetadata").removeClass('notvalidate');
           for (var i = 0; i < $("#EntityMetadata .error").length; i++) {
               if ($($("#EntityMetadata .error")[i]).hasClass("ng-hide") == true)
                   $($("#EntityMetadata .error")[i]).removeClass("error");
           }
            if ($("#EntityMetadata .error").length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().css({ 'color': '#B94A48' });
                }
                if ($("#EntityMetadata .error").find('.va-middle').length > 0) {
                    if (uploaderAttrInfoSub.length > 0) {
                        if (parseInt(($("#EntityMetadata .error").find('.va-middle').attr('id')).split('_')[1]) == uploaderAttrInfoSub[0]["attrID"]) {
                            $("#EntityMetadata .error").find('.va-middle').next().hide();
                            $("#EntityMetadata .error").find('.va-middle').parent().parent().removeClass("error");
                           
                        }
                    }
                }
                if ($("#EntityMetadata .error").find('directive-tagwords').length > 0) {
                    if ($scope.fields["ListTagwords_" + $("#EntityMetadata .error").find('directive-tagwords').attr('item-attrid')].length > 0) {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').parent().parent().removeClass("error");
                    }
                    else {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').removeAttr("style");
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                    }
                }
                return false;
            }
            EntityMetadata();

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('next', '');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }

            if ($scope.DynamicAttributeGroupTabs.length > 0 && currentWizardStep > 1 && currentWizardStep <= ($scope.DynamicAttributeGroupTabs.length + 1)) {
                $scope.attributeGroupDetailAsTab.length = 0;
                $scope.attributeGroupDetailAsTab.push($scope.DynamicAttributeGroupTabs[currentWizardStep - 2]);

                if ($scope.attributeGroupDetailAsTab.length > 0)
                    SubentityTypeCreationTimeout.AttrGroup = setTimeout(function () { $scope.$broadcast("attrprocess" + ($scope.attributeGroupDetailAsTab[0].ID)) }, 200);
            }

            if (currentWizardStep === totalWizardSteps) {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
            else {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }

        //InheritFrom Parent Functionality Start
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.multiselecttreeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.Inherritingtreelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        //InheritFrom Parent Functionality End


        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        //var apple_selected, treedata_avm, treedata_geography;
        var tree;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);

                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }

            //----------------> Calling Attribute to Attribute relation on selecting
            //$scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);

        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended

        $scope.Parentid = 0;
        //getting cost center
        $scope.costCenterList = [];
        $scope.MemberLists = [];
        $scope.costcemtreObject = [];
        $scope.count = 1;
        $scope.createCostCentre = function (CurrentEntityId) {
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreforEntityCreation($scope.EntitytypeID, 2, parseInt(CurrentEntityId, 10)).then(function (costCenterList) {
                    $scope.costCenterList = costCenterList.Response;
                    $scope.costcemtreObject = [];
                    $scope.MemberLists = [];
                    for (var i = 0; i < $scope.costCenterList.length; i++) {
                        $scope.costcemtreObject.push({
                            "CostcenterId": $scope.costCenterList[i].Id, "costcentername": $scope.costCenterList[i].costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": $scope.costCenterList[i].username, "OwnerID": $scope.costCenterList[i].UserID
                        });
                        $scope.count = $scope.count + 1;
                    }
                });
            }

            $scope.treeCategory = [];
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreTreeforPlanCreation($scope.EntitytypeID, 0, parseInt(CurrentEntityId, 10)).then(function (costCenterList1) {
                    $("#dynamicCostCentreTreeOnSubEntity").html = "";
                    if (costCenterList1.Response != null && costCenterList1.Response != false) {
                        $scope.treeCategory = [];
                        $scope.treeCategory = JSON.parse(costCenterList1.Response);

                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                    else {
                        $scope.treeCategory = [];
                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                });
            }

        }
        $scope.GlobalMembersList = [];
        $scope.createGlobalMembers = function (CurrentEntityId) {
            SubentitycreationService.GetGlobalMembers(parseInt(CurrentEntityId, 10)).then(function (GlobalMembersList) {
                $scope.GlobalMembersList = GlobalMembersList.Response;
                for (var i = 0; i < $scope.GlobalMembersList.length; i++) {
                    if (parseInt($scope.GlobalMembersList[i].RoleID, 10) == 1)
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": 2, "RoleName": "Editor", "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    else
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": parseInt($scope.GlobalMembersList[i].RoleID, 10), "RoleName": $scope.GlobalMembersList[i].Caption, "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    $scope.count = $scope.count + 1;
                }
            });
        }

        $scope.addcostCentre = function () {
            if ($scope.costcentreid != undefined && $scope.costcentreid != null && $scope.costcentreid.length > 0) {
                $.each($scope.costcentreid, function (val, item) {
                    var result = $.grep($scope.costcemtreObject, function (e) { return e.CostcenterId == item; });
                    if (result.length == 0) {
                        var costCentrevalues = $.grep($scope.costCenterList, function (e) { return e.Id == parseInt(item) })[0];
                        $scope.costcemtreObject.push({
                            "CostcenterId": item, "costcentername": costCentrevalues.costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": costCentrevalues.username, "OwnerID": costCentrevalues.UserID
                        });
                        var memberObj = $.grep($scope.MemberLists, function (e) { return e.Roleid == 8 && e.Userid == item; });
                        if (memberObj.length == 0) {
                            $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": costCentrevalues.usermail, "DepartmentName": costCentrevalues.Designation, "Title": costCentrevalues.Title, "Roleid": 8, "RoleName": 'BudgetApprover', "Userid": costCentrevalues.UserID, "UserName": costCentrevalues.username, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 1, "CostCentreID": item });
                            $scope.count = $scope.count + 1;
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1985.Caption'));
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
            }
            SubentityTypeCreationTimeout.spliceto = setTimeout(function () {
                $scope.costcentreid.splice(0, $scope.costcentreid.length);
            }, 10);

            if ($scope.treeCategory.length > 0)
                $scope.RecursiveCostCentreTreeClearChecked($scope.treeCategory);
        }

        $scope.RecursiveCostCentreTreeClearChecked = function (Treeval) {
            $.each(Treeval, function (val, item) {
                item.ischecked = false;
                if (item.Children.length > 0) {
                    $scope.RecursiveCostCentreTreeClearChecked(item.Children);
                }
            });
        }

        $scope.deleteCostCentre = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlisto = setTimeout(function () {
                        $scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
                        var memberToRempve = $.grep($scope.MemberLists, function (e) { return e.CostCentreID == item.CostcenterId });
                        if (memberToRempve.length > 0)
                            $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };

        $scope.subEntityName = "";
        $scope.EntitytypeID = 0;
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.AutoCompleteSelectedObj = [];
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};
        $scope.attributeGroupAddedValues = [];
        SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
        });

        SubentitycreationService.GetEntityStatus(params.ID, false, 0).then(function (getentitytypestatus) {
            $scope.EntityStatusResult = getentitytypestatus.Response;
        });


        //User Autocomplete End

        // --- Define Controller Variables. ----------------- //
        $scope.contrls = '';
        function GetEntityTypeRoleAccess(rootID) {
            SubentitycreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        // Get the all the module to bind ng-grid
        $scope.wizard = {
            newval: ''
        };
        $scope.OptionObj = {

        };
        $scope.UserimageNewTime = new Date.create().getTime().toString();

        // option holder part for dynamic controls START
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });

        }
        // option holder part for dynamic controls END

        $scope.dyn_Cont = '';
        //$scope.dyn_ContSpecial = '';

        /// dynamic value holding scope is start
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];

            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        /// dynamic value holding scopes end

        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.EntityMemberData = [];
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10) && e.FromGlobal == 0; });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) { return e.ID == parseInt($scope.fields['userRoles']) })[0];
                if (membervalues != undefined) {
                    if ($scope.AutoCompleteSelectedUserObj.UserSelection != null && $scope.AutoCompleteSelectedUserObj.UserSelection.Email != null || $scope.AutoCompleteSelectedUserObj.UserSelection.Email != "") {
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email, "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation, "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10), "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedUserObj.UserSelection.InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2 });
                        $scope.EntityMemberData.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email, "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation, "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10), "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedUserObj.UserSelection.InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2 });
                        $scope.fields.usersID = '';
                        $scope.count = $scope.count + 1;
                        $scope.AutoCompleteSelectedObj = [];
                        $scope.AutoCompleteSelectedUserObj = [];
                    }
                }
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };

        $scope.deleteOptions = function (users) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlistsplice = setTimeout(function () {
                        $scope.MemberLists.splice($.inArray(users, $scope.MemberLists), 1);
                        $scope.EntityMemberData.splice($.inArray(users, $scope.EntityMemberData), 1);
                    }, 100);
                }
            });
        };
        $scope.Entityamountcurrencytypeitem = [];
        //add period block
        $scope.items = [];
        $scope.subentityperiods = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0 });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.changeval = function (attrid) {
            if ($scope.fields["ListMultiSelection_" + attrid].length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().addClass('icon-asterisk validationmark margin-right5x');
                    $("#EntityMetadata").html($compile());
                }
            }
            else {
                var ID = "ListMultiSelection_" + attrid;
                $("#" + ID).next().find('.multiselect-container').next().removeAttr("style");
                $("#" + ID).next().find('.multiselect-container').next().css({ 'color': '#B94A48' });
            }

        }
        $scope.changevalues = function (attrTypeid, attrid) {
            if (attrid == 12) {
                var MultiselectId = "TreeMultiSelection_" + attrTypeid + "_" + 1;
                $("#EntityMetadata .error").find('#' + MultiselectId).next().removeAttr("style");
                $("#EntityMetadata .error").find('#' + MultiselectId).next().addClass('icon-asterisk validationmark margin-right5x');
                if ($("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().hasClass('error') == true)
                    $("#EntityMetadata .error").find('#' + MultiselectId).parent().parent().removeClass('error');
            }
        }
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.FinancialRequestObj = [];
        $scope.nonMandatoryIDList = [];

        $scope.saveEntity = function () {
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {

                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#btnTempSub").click();
           $("#EntityMetadata").removeClass('notvalidate');
           for (var i = 0; i < $("#EntityMetadata .error").length; i++) {
               if ($($("#EntityMetadata .error")[i]).hasClass("ng-hide") == true)
               $($("#EntityMetadata .error")[i]).removeClass("error");
           }
         
            if ($("#EntityMetadata .error").length > 0) {
                if ($("#EntityMetadata .error").find('.multiselect').hasClass('multiselect') == true) {
                    $("#EntityMetadata .error").find('.multiselect-container').next().removeAttr("style");
                    $("#EntityMetadata .error").find('.multiselect-container').next().css({ 'color': '#B94A48' });
                }
                if ($("#EntityMetadata .error").find('.va-middle').length > 0) {
                    if (uploaderAttrInfoSub.length > 0) {
                        if (parseInt(($("#EntityMetadata .error").find('.va-middle').attr('id')).split('_')[1]) == uploaderAttrInfoSub[0]["attrID"]) {
                            $("#EntityMetadata .error").find('.va-middle').next().hide();
                            $("#EntityMetadata .error").find('.va-middle').parent().parent().removeClass("error");
                            
                        }
                    }

                }
                if ($("#EntityMetadata .error").find('directive-tagwords').length > 0) {
                    if ($scope.fields["ListTagwords_" + $("#EntityMetadata .error").find('directive-tagwords').attr('item-attrid')].length > 0) {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').parent().parent().removeClass("error");
                    }
                    else {
                        $("#EntityMetadata .error").find('directive-tagwords').next().hide();
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').removeAttr("style");
                        $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                    }
                }
                return false;
            }
            $("#btnWizardFinish").attr('disabled', 'disabled');
            $scope.AttributeData = [];
            for (var i = 0 ; i < $scope.nonMandatoryIDList.length; i++) {
                $scope.IDList.push($scope.nonMandatoryIDList[i]);
            }
            if ($scope.IsObjectiveFired == false) {
                var entityMetadataObj = {};
                var metadata = [];
                metadata = FormObjectiveMetadata();
                entityMetadataObj.AttributeData = metadata;
                entityMetadataObj.Periods = [];
                GetObjectivePeriods();
                entityMetadataObj.Periods.push($scope.subentityperiods);
                entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
                SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                    try {

                        UpdateObjectiveScope(entityAttributeDataObjData.Response);

                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            if (uploaderAttrInfoSub.length > 0) {

                                SubentitycreationService.copyuploadedImage(uploaderAttrInfoSub[0].attrFileName).then(function (newuplAttrName) {
                                    var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfoSub[0].attrID });
                                    var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                                    $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                                    ProcessEntityCreation();

                                })
                            }
                            else { ProcessEntityCreation(); }
                        }, 100);


                    }
                    catch (ex) {
                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            if (uploaderAttrInfoSub.length > 0) {

                                SubentitycreationService.copyuploadedImage(uploaderAttrInfoSub[0].attrFileName).then(function (newuplAttrName) {
                                    var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfoSub[0].attrID });
                                    var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                                    $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                                    ProcessEntityCreation();

                                })
                            }
                            else { ProcessEntityCreation(); }
                        }, 100);

                    }

                });
            }
            else {
                SubentityTypeCreationTimeout.processtimeoout = setTimeout(function () {
                    if (uploaderAttrInfoSub.length > 0) {

                        SubentitycreationService.copyuploadedImage(uploaderAttrInfoSub[0].attrFileName).then(function (newuplAttrName) {
                            var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfoSub[0].attrID });
                            var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                            $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                            ProcessEntityCreation();
                        })
                    }
                    else { ProcessEntityCreation(); }
                    
                }, 100);
            }

        };


        function ProcessEntityCreation() {
            var SaveEntity = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1)
                                        valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                        else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                                else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]],
                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                    else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;

                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(value, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });

                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null) {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString("yyyy-MM-dd");
                        } else {
                            MyDateString = "";
                        }
                    }
                    else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields['MTextMoney_' + $scope.atributesRelationList[i].AttributeID].toString()).replace(/ /g, ''),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].attrFileID != undefined && $scope.atributesRelationList[i].attrFileID != null) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.atributesRelationList[i].attrFileID,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    // $scope.Entityamountcurrencytypeitem.push({ amount: parseInt($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({ amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });

                }
            }
            SaveEntity.ParentId = parseInt($scope.Parentid, 10);
            SaveEntity.Typeid = parseInt($scope.EntitytypeID, 10);
            SaveEntity.Active = true;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.entityName;
            SaveEntity.EntityMembers = [];
            SaveEntity.EntityCostRelations = $scope.costcemtreObject;
            SaveEntity.Periods = [];
            $scope.savesubentityperiods = [];
            SaveEntity.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.savesubentityperiods.push({ startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''), endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''), comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''), sortorder: 0 });
                    }
                }
            }
            SaveEntity.Periods.push($scope.savesubentityperiods);
            SaveEntity.AttributeData = $scope.AttributeData;
            SaveEntity.EntityMembers = $scope.EntityMemberData;
            SaveEntity.IObjectiveEntityValue = $scope.IDList;
            SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;

            if ($scope.AssetSelectionFiles.length == 0) {
                var res = [];
                for (var i = 0; i < $scope.SelectedAssetIdFolderID.SelectedAssetIDs.length; i++) {
                    res.push($scope.SelectedAssetIdFolderID.SelectedAssetIDs[i].AssetId);
                }
                SaveEntity.AssetArr = res;
            }
            else
                SaveEntity.AssetArr = $scope.AssetSelectionFiles;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({ amount: $scope.Entityamountcurrencytypeitem[m].amount, currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype, Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid });
            }
            SaveEntity.Entityamountcurrencytype.push($scope.curram);
            SaveEntity.SelectedAssetIdFolderID = $scope.SelectedAssetIdFolderID.SelectedAssetIDs;
            SaveEntity.PredefinedAttrValues = [];
            SaveEntity.AttributeGroupValues = [];

            if ($scope.DynamicAttributeGroupTabs.length > 0) {
                if ($scope.attributeGroupAddedValues != undefined) {

                    for (var i = 0; i < $scope.DynamicAttributeGroupTabs.length; i++) {
                        var req = $scope.DynamicAttributeGroupTabs[i];
                        if (req != undefined) {
                            if (req.Ispredefined == true)
                                SaveEntity.PredefinedAttrValues.push({ "PredefinedattrGrpData": $scope.attributeGroupAddedValues["PredefinedattrGrpDataof_" + req.ID], "GroupID": req.AttributeGroupID });
                            else
                                SaveEntity.AttributeGroupValues.push({ "attrGrpData": $scope.attributeGroupAddedValues["attrGrpDataof_" + req.ID], "GroupID": req.AttributeGroupID });
                        }
                    }
                }
            }
            SubentitycreationService.CreateEntity(SaveEntity).then(function (EntityList) {
                if (EntityList.Response > 0) {
                    $scope.EntityMemberData = [];
                    $scope.MemberLists = [];
                    $scope.fields = [];
                    $scope.entityObjectiveList = {};
                    $scope.entityAllObjectives = {};
                    $scope.MandatoryEntityObjectives = {};
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    $scope.nonMandatoryIDList = [];

                    $('#dynamicControls').modal('hide');
                    $('#moduleContextmenu').modal('hide');
                    $scope.Activity.IsActivitySectionLoad = true;
                    SubentityTypeCreationTimeout.refreshentitylist = setTimeout(function () { $("#EntitiesTree").trigger("ActivityLoadEntity", [EntityList.Response]); }, 50);
                    $location.path('/mui/planningtool/default/detail/section/' + EntityList.Response + '/overview');
                    $scope.OwnerList = [];
                    $scope.treePreviewObj = {};
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": "-", "Title": "-", "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "InheritedFromEntityName": "-" });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
                    $("#btnWizardFinish").attr('disabled', false);
                }
                SubentityTypeCreationTimeout.closepopup = setTimeout(function () { $modalInstance.dismiss('cancel') }, 100);
                $("#EntityMetadata").html('');
            });
        }
        //$(window).on("GloblaUniqueKeyAccess", function (event, UniqueKey) {
        //    $scope.UniqueKey = UniqueKey;
        //});
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.MultiSelecttree = {};
        $scope.Options = {};
        $scope.attributeGroupAddedValues = [];
        function SubentityTypeCreation(Id, Caption, CurrentEntityId) {
            if (Id != undefined) {
                GetEntityTypeRoleAccess(Id);
            }
            $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.owner = {};
            //SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            //    $scope.owner = Getowner.Response;
            //    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
            //});
            $scope.Parentid = CurrentEntityId;
            $scope.createCostCentre(CurrentEntityId);
            $scope.createGlobalMembers(CurrentEntityId);
            $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            $scope.DynamicAttributeGroupTabs = [];
            SubentitycreationService.GetEntityTypeAttributeGroupRelation(Id, 0, 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {

                        var ListOfAttributeGroup = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.IsTabInEntityCreation == true;
                        });

                        for (var i = 0; i < ListOfAttributeGroup.length; i++) {
                            if (ListOfAttributeGroup[i].Ispredefined == true) {
                                $scope.attributeGroupAddedValues["PredefinedattrGrpvalueof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["PredefinedattrGrpDataof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["PredefinedattrGrpHeaderof_" + ListOfAttributeGroup[i].ID] = [];
                            }
                            else {
                                $scope.attributeGroupAddedValues["attrGrpvalueof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["attrGrpDataof_" + ListOfAttributeGroup[i].ID] = [];
                                $scope.attributeGroupAddedValues["attrGrpHeaderof" + ListOfAttributeGroup[i].ID] = [];
                            }
                        }

                        $scope.DynamicAttributeGroupTabs = ListOfAttributeGroup;

                        SubentityTypeCreationTimeout.AttrGroup1 = setTimeout(function () {
                            $('#MyWizard').wizard('stepLoaded');
                            $('#btnWizardNext').show();     //intially wizard submit button is enalbed
                            $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled
                            window["tid_wizard_steps_all_complete_count"] = 0;
                            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                        }, 250);
                    }
                }
                else {
                    $('#MyWizard').wizard('stepLoaded');
                    $('#btnWizardNext').show();     //intially wizard submit button is enalbed
                    $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled
                    window["tid_wizard_steps_all_complete_count"] = 0;
                    window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                }

                GetEntityTypeTabCollections(Id, Caption, CurrentEntityId);

            });
        }

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0]["Currencytypeid"] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0]["Id"];
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = ($scope.atributesRelationList[i].ParentValue[0]["Amount"]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').toString();
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0][j] }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }

                                }
                            }
                        }
                        else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) { return e.Id == defaultmultiselectvalue[j]; })[0].Id);
                                }
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        if ($scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] != undefined) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                    if ($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].length > 0) {
                                        $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0].id;
                                }
                            }
                            else {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = 0;
                                    if ($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j - 1)].length > 0) {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j)] = $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j - 1)], function (e) { return e.id == $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] })[0].Children;
                                        if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null ||$scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != undefined)
                                            $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j)], function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0].id;
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                    }
                                }
                            }
                            else {
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        }
                                        else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
            }
        }

        $scope.CreateDynamicControls = function (Id, Caption, CurrentEntityId) {
            $("#btnWizardFinish").removeAttr('disabled');
            $("#EntityMetadata").html('');
            $("#EntityMetadata").scrollTop(0);
            $("#EntityMetadata").html('<div class="loadingSubPopup"><img src="assets/img/loading.gif"> Loading...</div>');
            $scope.subEntityName = Caption;
            $scope.EntitytypeID = Id;
            $scope.items = [];
            $scope.DateObject = {};
            //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
            SubentitycreationService.GetAttributeToAttributeRelationsByIDForEntity(Id).then(function (entityAttrToAttrRelation) {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });
            SubentitycreationService.GetJSONEntityTypeAttributeRelationWithLevelsByID(Id, CurrentEntityId).then(function (entityAttributesRelation) {
                if (entityAttributesRelation.Response != null) {
                    $scope.atributesRelationList = JSON.parse(entityAttributesRelation.Response);
                $scope.dyn_Cont = '';
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].IsHelptextEnabled == true) {
                        var attribteHelptext = $scope.atributesRelationList[i].HelptextDecsription == null ? "-" : $scope.atributesRelationList[i].HelptextDecsription.trim() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription;
                        var qtip = 'my-qtip2 qtip-content="' + attribteHelptext + '"';
                    }
                    else
                        var qtip = "";

                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                multiple: false,
                                formatResult: function (item) {
                                    return item.text;
                                },
                                formatSelection: function (item) {
                                    return item.text;
                                }
                            };
                            if (j == 0) {
                                $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + "</label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                $scope.settreeSources();
                            }
                            else {
                                $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\">";
                                $scope.dyn_Cont += "<option ng-repeat=\"item in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{item.id}}\">{{item.Caption}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.settreeSources();
                            }

                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) {
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        GetTreeObjecttoSave($scope.atributesRelationList[i].AttributeID);
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group inlineBlock treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div><span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                multiple: false,
                                formatResult: function (item) {
                                    return item.text;
                                },
                                formatSelection: function (item) {
                                    return item.text;
                                }
                            };
                            if (totLevelCnt1 == 1) {

                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.setFieldKeys();
                            }

                            else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                if (j == 0) {
                                    $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                    $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                    $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                    $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    $scope.settreeSources();
                                }
                                else {
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                    else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        //$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                }
                                $scope.setFieldKeys();
                            }
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        for (k; k < ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].length) ; k++) {
                                            if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k].trim());
                                            }
                                            else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    }
                                    else {
                                        if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                            $scope.settreeTexts();
                                        }
                                        else {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                //$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                        }
                        else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $translate.instant('LanguageContents.Res_5613.Caption') + "</label><div class=\"controls bgDropdown\"><select ui-select2=\"select2Config\" id=\"OverAllEntityStats\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"OverAllEntityStatus\" data-placeholder=\"Entity Status\">";
                        $scope.dyn_Cont += "<option value=\"0\"> Select " + $translate.instant('LanguageContents.Res_5613.Caption') + "</option>";
                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in EntityStatusResult\" value=\"{{Aitem.ID}}\">{{Aitem.StatusOptions}}</option>";
                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = 0;

                        $scope.select2Config = {
                            formatResult: $scope.overallstatusformat,
                            formatSelection: $scope.overallstatusformat
                        };
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                //$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input  class=\"valsi\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            } else {
                                //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                            }
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        if ($scope.atributesRelationList[i].IsReadOnly)
                            //  $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown disabled ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown disabled ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        else
                            //$scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ></select></div></div>";
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\'changeval(" + $scope.atributesRelationList[i].AttributeID + ");'\  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        if ($scope.atributesRelationList[i].Caption == "Due Date") {
                            $scope.IsOpend = "DueDate";
                            $scope.DateObject.DueDate = false;
                            var isopenedhtmlstr = "DateObject.DueDate";
                        } else if ($scope.atributesRelationList[i].Caption == "StartDate") {
                            $scope.IsOpend = "StartDate";
                            var isopenedhtmlstr = "DateObject.StartDate";
                            $scope.DateObject.StartDate = false;
                        }
                        else {
                            $scope.IsOpend = "EndDate";
                            $scope.DateObject.EndDate = false;
                            var isopenedhtmlstr = "DateObject.EndDate";
                        }
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MinValue);
                        if ($scope.MaxValue > 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                        } else if ($scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }

                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"\ is-open=\'" + isopenedhtmlstr + "'\ min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";


                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.MTextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"MTextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        //$scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.fields["MTextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";

                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                        if ($scope.atributesRelationList[i].IsReadOnly == true)
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        else
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        // $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";
                        var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-helptextdescription=" + helptextdesc + " data-ishelptextenabled=" + $scope.atributesRelationList[i].IsHelptextEnabled.toString() + " data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";



                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        //$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        try {
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                if ($scope.atributesRelationList[i].ParentValue == "-") {
                                    $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                }
                                else {
                                    if ($scope.atributesRelationList[i].ParentValue.length > 0) {
                                        for (var j = 0 ; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                            var datStartUTCval = "";
                                            var datstartval = "";
                                            var datEndUTCval = "";
                                            var datendval = "";
                                            datStartUTCval = $scope.atributesRelationList[i].ParentValue[0][j].Startdate.substr(0, 10);
                                            datstartval = new Date.create((datStartUTCval));
                                            datEndUTCval = $scope.atributesRelationList[i].ParentValue[0][j].EndDate.substr(0, 10);
                                            datendval = new Date.create((datEndUTCval));
                                            $scope.items.push({ startDate: (ConvertDateToString(datstartval)), endDate: (ConvertDateToString(datendval)), comment: $scope.atributesRelationList[i].ParentValue[0][j].Description, sortorder: 0, calstartopen: false, calendopen: false });
                                        }
                                    }
                                    else {
                                        $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                    }
                                }
                            }
                            else {
                                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                            }
                        }
                        catch (e) { }
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue > 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else if ($scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"startDate\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/>";
                        $scope.dyn_Cont += "<input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\"  ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/>";
                        $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\" ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        //$scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i ng-show=\"$first==true\" my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        StrartUpload_UploaderAttrSubEntity();
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\',' + $scope.atributesRelationList[i].AttributeTypeID + ')\" class="control-group"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div  class="controls">';
                        $scope.dyn_Cont += '<div class="width2x inlineBlock va-middle" style="" id="Uploader_' + $scope.atributesRelationList[i].AttributeID + '">';
                        if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                            $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                        $scope.dyn_Cont += 'class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderPreviewSub_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '<span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        var helptext = $scope.atributesRelationList[i].HelptextDecsription == "" ? "" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        //$scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "<directive-tagwords id = \"Tag_" + $scope.atributesRelationList[i].AttributeID + "\"    item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\" item-isenable=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" item-attributehelptext=\"" + helptext.toString() + "\" ></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";

                        if ($scope.atributesRelationList[i].InheritFromParent) {
                        }
                        else {
                        }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.IsOpend = "DateAction";
                        var isopenedhtmlstr = "DateObject.DateAction";
                        $scope.DateObject = { "DateAction": false };
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.setFieldKeys();
                    }

                    else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\" class=\"currencySelector\" ><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Name}}</option></select></div></div>";
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        $scope.setFieldKeys();

                    }

                    $scope.setFieldKeys();
                    $scope.setoptions();
                }
                $scope.dyn_Cont += '<input style="visibility:hidden" type="submit" id="btnTempSub" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#EntityMetadata").html('<div class="row-fluid"><div data-col="1" class="span4"></div><div data-col="2" class="span8"></div></div> ');
                    
                $("#EntityMetadata").html(
                    $compile($scope.dyn_Cont)($scope));

                //$("[id^='dTextSingleLine_']").autoNumeric('init',$scope.DecimalSettings['FinancialAutoNumeric']);
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "1" });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";

                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: $scope.mindec });
                    }
                    else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                    }
                }
                $("[id^='MTextMoney_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0.0" });
                $("[id^='MTextMoney_']").keydown(function (event) {
                    if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                        bootbox.alert($translate.instant('Please enter only Number'));
                        event.preventDefault(); //stop character from entering input
                    }
                });
                $("#EntityMetadata").scrollTop(0);
                refreshAssetobjects();
                $scope.$broadcast('callbackforEntitycreation', 0, CurrentEntityId, 4);
                GetValidationList(Id);
                $("#EntityMetadata").addClass('notvalidate');

                SubentityTypeCreationTimeout.updateto = setTimeout(function () {
                    //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
                    //HideAttributeToAttributeRelationsOnPageLoad();
                    UpdateInheritFromParentScopes();
                });

                }
            });
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.treesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {
            if (treeobj.length > 0) {
                for (var i = 0, node; node = treeobj[i++];) {

                    if (node.ischecked == true) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(node);
                        }
                        treeformflag = false;
                        if (ischildSelected(node.Children)) {
                            GenerateTreeStructure(node.Children);
                        }
                        else {
                            GenerateTreeStructure(node.Children);
                        }
                    }
                    else
                        GenerateTreeStructure(node.Children);
                }

            }
        }


        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        //Validation functionality--call this for validation
        function GetValidationList(EntitypeId) {
            SubentitycreationService.GetValidationDationByEntitytype(EntitypeId).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listofValidations.length != 0) {
                        for (var i = 0 ; i < $scope.listofValidations.length; i++) {
                            if ($scope.listofValidations[i][1] == "presence") {
                                $($scope.listofValidations[i][0]).parent().addClass('relative');
                                if (($scope.listofValidations[i][0]).contains('_' + (SystemDefiendAttributes.Name).toString()) == false) {
                                    if ($($scope.listofValidations[i][0]).parent().find('ul').length >= 1 && ($scope.listofValidations[i][0]).contains('ListMultiSelection')) {
                                        $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($($scope.listofValidations[i][0]).parent().find('ul'));
                                    }
                                    else if (($scope.listofValidations[i][0]).contains('#Uploader') == true) {
                                        $("#EntityMetadata").find($scope.listofValidations[i][0]).parent().addClass('relative');
                                        $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($("#EntityMetadata").find($scope.listofValidations[i][0]));

                                    }
                                    else if (($scope.listofValidations[i][0]).contains('Tag') == true) {
                                      
                                        $("#EntityMetadata").find($scope.listofValidations[i][0]).find('.validationmark').removeClass("ng-hide");
                                    }
                                    else {
                                        $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listofValidations[i][0]);
                                    }
                                }                               
                                else {
                                    $('.valsi').parent().addClass('relative');
                                    $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter('.valsi');
                                }
                            }
                        }
                    }
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#EntityMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTempSub',
                        'silentSubmit': 'true'
                    });               
                }
            });
        }

        $scope.entityObjectiveList = {};
        $scope.entityAllObjectives = {};
        $scope.MandatoryEntityObjectives = {};
        $scope.MandatoryObjList = [];
        $scope.NonMandatoryObjList = [];
        $scope.IDList = [];
        $scope.nonMandatoryIDList = [];
        $scope.IsObjectiveFired = false;
        function EntityMetadata() {
            $scope.AttributeData = [];
            var entityMetadataObj = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            entityMetadataObj.AttributeData = $scope.AttributeData;
            entityMetadataObj.Periods = [];
            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
            entityMetadataObj.Periods.push($scope.subentityperiods);
            entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
            SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                $scope.IsObjectiveFired = true;
                if (entityAttributeDataObjData.Response != null && entityAttributeDataObjData.Response.length > 0) {
                    $scope.entityAllObjectives = entityAttributeDataObjData.Response;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            });
        }


        function FormObjectiveMetadata() {

            var AttributeData = [];


            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return AttributeData;
        }


        function GetObjectivePeriods() {

            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
        }

        function UpdateObjectiveScope(objResponse) {

            if (objResponse != null) {


                if (objResponse != null && objResponse.length > 0) {
                    $scope.entityAllObjectives = objResponse;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            }
            else {
                $scope.IDList = [];
            }

        }
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrSub").click();
        }

        function StrartUpload_UploaderAttrSubEntity() {
            $('.moxie-shim').remove();
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                var uploader_AttrSub = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrSub',
                    container: 'filescontainerooSub',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }

                });
                uploader_AttrSub.bind('Init', function (up, params) {
                });

                uploader_AttrSub.init();
                uploader_AttrSub.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_AttrSub.start();
                });
                uploader_AttrSub.bind('UploadProgress', function (up, file) {

                });
                uploader_AttrSub.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh(); // Reposition Flash/Silverlight
                });
                uploader_AttrSub.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    UpdateFileDetails(file, response.response);
                });
                uploader_AttrSub.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_AttrSub.settings.multipart_params.key = uniqueKey;
                    uploader_AttrSub.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else{
            var uploader_AttrSub = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttrSub',
                container: 'filescontainerooSub',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                chunk: '64Kb',
                multi_selection: false,
                multipart_params: {}

            });
            uploader_AttrSub.bind('Init', function (up, params) {
            });

            uploader_AttrSub.init();
            uploader_AttrSub.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_AttrSub.start();
            });
            uploader_AttrSub.bind('UploadProgress', function (up, file) {

            });
            uploader_AttrSub.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh(); // Reposition Flash/Silverlight
            });
            uploader_AttrSub.bind('FileUploaded', function (up, file, response) {
                var fileinfo = response.response.split(",");
                response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                UpdateFileDetails(file, response.response);
            });
            uploader_AttrSub.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, { id: file.id, size: file.size });
            });
        }
        }

        var uploaderAttrInfoSub = [];
        function UpdateFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + extension;
            uploaderAttrInfoSub.push({ attrID: $scope.UploadAttributeId, attrFileName: ImageFileName });
            var PreviewID = "UploaderPreviewSub_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + ImageFileName);
            if ( $("#EntityMetadata").find("#Uploader_" + $scope.UploadAttributeId).parent().parent().hasClass('error') == true) {
                if (uploaderAttrInfoSub.length > 0) {                   
                    $("#EntityMetadata").find("#Uploader_" + $scope.UploadAttributeId).parent().parent().removeClass("error");
                    $("#EntityMetadata").find("#Uploader_" + $scope.UploadAttributeId).next().hide();
                    
                }
            }
        }
        $scope.Clear = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.UploadAttributeData = [];
        $scope.EntityObjSelect = function (objectiveId) {
            var IsExist = $.grep($scope.nonMandatoryIDList, function (e) {
                if (e == objectiveId) {
                    return e;
                }
            })
            if (IsExist.length == 0) {
                $scope.nonMandatoryIDList.push(objectiveId);
            }
        }

        $(document).on('click', '.checkbox-custom > input[id=SelectAllObjectives]', function (e) {
            var status = this.checked;
            $('#nonMandatoryObjectiveBody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
            for (var i = 0 ; i < $scope.entityObjectiveList.length; i++) {
                $scope.nonMandatoryIDList.push($scope.entityObjectiveList[i].ObjectiveId);
            }
            for (var i = 0 ; i < $scope.MandatoryEntityObjectives.length; i++) {
                $scope.IDList.push($scope.MandatoryEntityObjectives[i].ObjectiveId);
            }
        });

        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }        

        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {                        
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);                        
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = 0;
                            var ID = "DropDownTree_" + attrID + "_" + 1;
                            if ($("#" + ID).attr('value') != undefined) {
                                //$("#EntityMetadata .error").find('#' + ID).next().addClass("hide");
                                $("#EntityMetadata .error").find('#' + ID).next().hide();
                                $("#EntityMetadata .error").find('#' + ID).next().next().addClass('icon-asterisk validationmark margin-right5x');
                                if ($("#EntityMetadata .error").find('#' + ID).parent().parent().hasClass('error') == true)
                                    $("#EntityMetadata .error").find('#' + ID).parent().parent().removeClass('error');
                            }
                        }
                        else if (attrType == 12) {
                            var ID = "TreeMultiSelection_" + attrID + "_" + 1;
                            if ($("#" + ID).attr('value') != undefined) {
                                $("#EntityMetadata .error").find('#' + ID).next().removeAttr("style");
                                $("#EntityMetadata .error").find('#' + ID).next().next().addClass('icon-asterisk validationmark margin-right5x');
                                if ($("#EntityMetadata .error").find('#' + ID).parent().parent().hasClass('error') == true)
                                    $("#EntityMetadata .error").find('#' + ID).parent().parent().removeClass('error');
                            }
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;

                            $scope.ShowOrHideAttributeToAttributeRelation(attrID + "_" + (attributeLevel), attrType);
                        }
                    }
                    else if (attrType == 12) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //    $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                        //        $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //    });
                        //}
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }

            }
            catch (e) { }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {                        
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            $.each(Children, function (i, el) {
                                $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //    $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                        //        $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //    });
                        //}
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            }
            else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        $scope.CheckPreviousEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            }
            else {
                $("#EntityMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null
                }
                else {
                    $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };

        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;

            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            var edate = ConvertDateToString(enddate1);

            var sdate = ConvertDateToString(startdate);
            if (enddate1 != null) {
                if (edate < sdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                    $scope.items[currentindex].endDate = null;
                }
            }
        };


        //dam file selection part
        function refreshAssetobjects() {
            $scope.PageNoobj = { pageno: 1 };
            $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 }, { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1 }, { "Name": "Name (Descending)", ID: 2 }, { "Name": "Creation date (Ascending)", ID: 3 }, { "Name": "Creation date (Descending)", ID: 4 }];
            $scope.FilterStatus = { filterstatus: 1 };
            $scope.DamViewName = { viewName: "Thumbnail" };
            $scope.OrderbyName = { orderbyname: "Creation date (Descending)" };
            $scope.OrderBy = { order: 4 };
            $scope.AssetSelectionFiles = [];
            $scope.SelectedAssetIdFolderID.SelectedAssetIDs = [];
        }
        refreshAssetobjects();

        $scope.costcentreid = [];

        $scope.OnCostCentreTreesSelection = function (branch, parentArr) {
            if (branch.ischecked == true) {
                $scope.costcentreid.push(branch.id);
            }
            else {
                $scope.costcentreid.splice($scope.costcentreid.indexOf(branch.id), 1);
            }
        };
        $scope.origninalamount = 0;
        $scope.Getamountentered = function (atrid) {

            if (1 == $scope.fields["ListSingleSelection_" + atrid])
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            SubentitycreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {

                        //$scope.fields["dTextSingleLine_" + atrid] = parseInt((parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid])).formatMoney(0, ' ', ' ');
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                    //$scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                }
            });
        }



        SubentitycreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null)
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });

        $scope.OnPageClose = function () {
            $scope.listAttriToAttriResult = [];
            $("#EntityMetadata").html('');
            $modalInstance.dismiss('cancel');
        }



        $scope.$on('callbackstoptimer', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        $scope.$on('callbackacivatetimer', function (event) {
            $scope.$broadcast('callbackacivatetimercall');
        });

        $('#LightBoxPreview').on('hidden.bs.modal', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        //change event for duedate
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                }
                else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        //change event for period date
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate")
                                $scope.items[0].startDate = "";
                            else
                                $scope.item[0].endDate = "";
                        }
                    }
                }
                else {
                    if (datetype == "StartDate")
                        $scope.items[0].startDate = "";
                    else
                        $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };

        $scope.EntityStatusResult = [];

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id == "0" && data.text != "") {
                return "<div class='select2-chosen-label'style='background-color: #FEFEFE; color: #444444; font-size: 11px;'>" + data.text + "</div>";
            }
            else if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }
            return "<div class='select2-chosen-label' style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        SubentityTypeCreation(params.ID, params.Caption, params.CurrentEntityId);

        $scope.$on("$destroy", function () {
            setTimeout.cancel(SubentityTypeCreationTimeout);
            setTimeout.cancel(remValtimer);
            model, tree, ownername, ownerid, treeTextVisbileflag, treeformflag = null;
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.SubEntityCreationCtrl']"));
            //var model, remValtimer, IsValExist, getVal, Attributetypename, relationobj, ID, i, j, o, x, k, m, t, tabHtml, currentWizardStep, totalWizardSteps, keys, keys1, keys2, apple_selected, tree, treedata_avm, treedata_geography, _ref, remainRecord, result, costCentrevalues, memberObj, memberToRempve, ownername, ownerid, membervalues, ItemCnt, percentageflag, entityMetadataObj, metadata, SaveEntity, attributeLevelOptions, levelCount, value, MyDate, MyDateString, treenodes, multiselectiObject, res, totLevelCnt, CaptionObj, LabelObject, totLevelCnt1, temp, param1, param2, datStartUTCval, datstartval, datEndUTCval, datendval, mar, treeTextVisbileflag, treeformflag, remainRecord, attrInheritval, defaultmultiselectvalue, attrRelIDs, multiselectiObject, AttributeData, extension, resultArr, PreviewID, IsExist, status, recursiveAttrID, attributesToShow, currntlevel, optionValue, hideAttributeOtherThanSelected, attrval, enddate1, edate, sdate, test, a, formatlen, defaultdateVal = null;
            $scope.$broadcast('callbackaftercreation');
        });
        // --- Initialize. ---------------------------------- //

    }
    //);
    app.controller("mui.planningtool.SubEntityCreationCtrl", ['$scope', '$location', '$resource', '$http', '$cookies', '$compile', '$window', '$translate', 'SubentitycreationService', '$modalInstance', 'params', muiplanningtoolSubEntityCreationCtrl]);
})(angular, app);
///#source 1 1 /app/services/planganttview-service.js
(function(ng,app){function PlanganttviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetCaptionofPeriod:GetCaptionofPeriod,GetEntityPeriod:GetEntityPeriod,DeleteEntityPeriod:DeleteEntityPeriod,InsertUpdateEntityPeriodLst:InsertUpdateEntityPeriodLst,GetEntitiPeriodByIdForGantt:GetEntitiPeriodByIdForGantt,UpdateEntityPeriod:UpdateEntityPeriod,GetAllGanttHeaderBar:GetAllGanttHeaderBar});function GetCaptionofPeriod(EntityTypeID){var request=$http({method:"get",url:"api/Planning/GetCaptionofPeriod/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityPeriod(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntityPeriod/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteEntityPeriod(ID){var request=$http({method:"delete",url:"api/Planning/DeleteEntityPeriod/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateEntityPeriodLst(formobj){var request=$http({method:"post",url:"api/Planning/InsertUpdateEntityPeriodLst/",params:{action:"add",},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntitiPeriodByIdForGantt(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntitiPeriodByIdForGantt/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateEntityPeriod(formobj){var request=$http({method:"put",url:"api/Planning/UpdateEntityPeriod/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllGanttHeaderBar(){var request=$http({method:"get",url:"api/common/GetAllGanttHeaderBar/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanganttviewService",['$http','$q',PlanganttviewService]);})(angular,app);
///#source 1 1 /app/services/SubEntityCreation-service.js
(function (ng, app) {
    function SubentitycreationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetCostcentreforEntityCreation: GetCostcentreforEntityCreation,
            GetCostcentreTreeforPlanCreation: GetCostcentreTreeforPlanCreation,
            GetGlobalMembers: GetGlobalMembers,
            GettingPredefineObjectivesForEntityMetadata: GettingPredefineObjectivesForEntityMetadata,
            CreateEntity: CreateEntity,
            GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById,
            GetCurrencyListFFsettings: GetCurrencyListFFsettings,
            GetEntityTypeRoleAccess: GetEntityTypeRoleAccess,
            GetUserById: GetUserById,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetJSONEntityTypeAttributeRelationWithLevelsByID: GetJSONEntityTypeAttributeRelationWithLevelsByID,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            GetPlantabsettings: GetPlantabsettings,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetEntityStatus: GetEntityStatus,
            copyuploadedImage: copyuploadedImage
        });

        function GetCostcentreforEntityCreation(EntityTypeID, FiscalYear, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostcentreforEntityCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCostcentreTreeforPlanCreation(EntityTypeID, FiscalYear, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostcentreTreeforPlanCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetGlobalMembers(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetGlobalMembers/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingPredefineObjectivesForEntityMetadata(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/GettingPredefineObjectivesForEntityMetadata/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateEntity(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/CreateEntity/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCurrencyListFFsettings() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCurrencyListFFsettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAccess(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserById(ID) {
            var request = $http({
                method: "get",
                url: "api/user/GetUserById/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetJSONEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetJSONEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidationDationByEntitytype(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPlantabsettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetPlantabsettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function copyuploadedImage(filename) { var request = $http({ method: "post", url: "api/Planning/copyuploadedImage/", params: { action: "save" }, data: { filename: filename } }); return (request.then(handleSuccess, handleError)); }

function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("SubentitycreationService", ['$http', '$q', SubentitycreationService]);
})(angular, app);
