using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IModule interface for table 'MM_Module'.
    /// </summary>
    public interface IModule
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        bool IsEnable
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
