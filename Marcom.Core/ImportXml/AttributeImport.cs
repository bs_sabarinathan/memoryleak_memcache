﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ImportXml
{
    internal class AttributeImport 
    {
        #region Member Variables

        protected int _id;
        protected int _newid;
        protected string _caption;
        protected string _description;
        protected int _attributetypeid;
        protected bool _issystemdefined;
        protected bool _isspecial;

        #endregion

        #region Constructors
        public AttributeImport() { }

        public AttributeImport(int pId, string pCaption, string pDescription, int pAttributeTypeid, bool pIsSystemDefined, bool pIsSpecial, int pNewId)
        {
            this._id = pId;
            this._caption = pCaption;
            this._description = pDescription;
            this._attributetypeid = pAttributeTypeid;
            this._issystemdefined = pIsSystemDefined;
            this._isspecial = pIsSpecial;
            this._newid = pNewId;

        }

        public AttributeImport(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _caption = value;
            }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }

        public int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }

        }

        public bool IsSystemDefined
        {
            get { return _issystemdefined; }
            set { _issystemdefined = value; }

        }

        public bool IsSpecial
        {
            get { return _isspecial; }
            set { _isspecial = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttributeImport castObj = null;
            try
            {
                castObj = (AttributeImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
