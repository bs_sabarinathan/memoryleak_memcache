﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
    {
    public interface IConvertedcurrencies
        {
        int Id{get;set;}
        string Currency { get; set; }
        decimal ConvertedAmount { get; set; }
        string Error { get; set; }
       // dynamic ConvertedCurrrency { get; set; }
        
        }
    }
