﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Presentation.Handlers
{
    /// <summary>
    /// Summary description for UserImage
    /// </summary>
    public class VideoUploadedPath : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string path = context.Request.QueryString["Path"];
            try
            {
                string name = context.Request.QueryString["name"];
                string fullpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], path + name);// context.Server.MapPath("UserImages/" + id + ".jpg");
                context.Response.Buffer = false;
                context.Response.ContentType = "video/mpeg";
                context.Response.WriteFile(fullpath);
                context.Response.End(); 
            }
            catch
            {
                string fullpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], path + "/IMG.png");// context.Server.MapPath("UserImages/" + id + ".jpg");
                context.Response.ContentType = "video/jpg";
                context.Response.WriteFile(fullpath);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}