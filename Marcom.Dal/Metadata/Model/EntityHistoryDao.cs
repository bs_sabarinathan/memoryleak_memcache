﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class EntityHistoryDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
		protected int _EntityID;
        protected DateTimeOffset _VisitedOn;
		protected int _UserID;
	    protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EntityHistoryDao() {}

        public EntityHistoryDao(int pEntityID, DateTimeOffset pVisitedOn, int pUserID)
		{
			this._EntityID = pEntityID; 
			this._VisitedOn = pVisitedOn; 
			this._UserID = pUserID; 
			
        
		}

        public EntityHistoryDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int EntityID
        {
            get { return _EntityID; }
            set { _bIsChanged |= (_EntityID != value); _EntityID = value; }
			

        }

        public virtual DateTimeOffset VisitedOn
        {
            get { return _VisitedOn; }
            set { _bIsChanged |= (_VisitedOn != value); _VisitedOn = value; }

        }

        public virtual int UserID
        {
            get { return _UserID; }
            set { _bIsChanged |= (_UserID != value); _UserID = value; }
		}
		
		

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string EntityHistoryDao = "EntityHistoryDao";			
			public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string VisitedOn = "VisitedOn";
            public const string UserID = "UserID";			
			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string EntityHistoryDao = "EntityHistoryDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string VisitedOn = "VisitedOn";
            public const string UserID = "UserID";			
		}

		#endregion
    }
}
 