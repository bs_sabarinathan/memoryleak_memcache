﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Dam;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.DAM;
using Newtonsoft.Json.Linq;
using Presentation.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class DamController : ApiController
    {
        ServiceResponse result;
        StreamReader reader;

        [Route("api/Dam/GetAttributeDAMCreate/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetAttributeDAMCreate(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAttributeDAMCreate(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetDAMEntityTypes")]
        [HttpGet]
        public ServiceResponse GetDAMEntityTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMEntityTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetDAMExtensions")]
        [HttpGet]
        public ServiceResponse GetDAMExtensions()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMExtensions();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetDAMAttribute/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetDAMAttribute(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMAttribute(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetDAMCommonAttribute")]
        [HttpGet]
        public ServiceResponse GetDAMCommonAttribute()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMCommonAttribute();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/DAMadminSettingsforRootLevelInsertUpdate")]
        [HttpPost]
        public ServiceResponse DAMadminSettingsforRootLevelInsertUpdate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj["DamAdminData"]);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DAMadminSettingsforRootLevelInsertUpdate(jsondata, (string)jobj["LogoSettings"], (string)jobj["Key"], (int)jobj["TypeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Dam/PostDAMadminSettingsforRootLevelInsertUpdate")]
        [HttpPost]
        //method name changed to PostDAMadminSettingsforRootLevelInsertUpdate
        public ServiceResponse PostDAMadminSettingsforRootLevelInsertUpdate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                // IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DAMadminSettingsforRootLevelInsertUpdate(jsondata, (string)jobj["LogoSettings"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/DAMGetAdminSettings/{LogoSettings}/{Key}/{typeid}")]
        [HttpGet]
        public ServiceResponse DAMGetAdminSettings(string LogoSettings, string Key, int typeid)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DAMGetAdminSettings(LogoSettings, Key, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Dam/GetDamAttributeRelation/{damID}")]
        [HttpGet]
        public ServiceResponse GetDamAttributeRelation(int damID)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDamAttributeRelation(damID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/CreateAsset/")]
        [HttpPost]
        public ServiceResponse CreateAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray saveObj = (JArray)jobj["saveAsset"];
                JArray arrperiods = (JArray)saveObj[0]["Periods"];
                JArray arrobjAttibutevalues = (JArray)saveObj[0]["AttributeData"];
                JArray arrentityamountcurrencytype = (JArray)saveObj[0]["Entityamountcurrencytype"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                IList<IDamPeriod> listEntityperiods = new List<IDamPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        //if (entityPeriodObj.Count() > 0)
                        //{
                        //    foreach (var arrp in entityPeriodObj)
                        //    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                        {

                            IDamPeriod enti = marcomManager.DigitalAssetManager.Damperiodservice();
                            enti.Startdate = startDate;
                            enti.EndDate = endDate;
                            enti.Description = (string)arrp["comment"];
                            enti.SortOrder = (int)arrp["sortorder"];
                            listEntityperiods.Add(enti);
                        }
                        //    }
                        //}
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IAssetAmountCurrencyType> listentityamountcurrencytype = new List<IAssetAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            IAssetAmountCurrencyType entamcur = marcomManager.DigitalAssetManager.AssetAmountCurrencyTypeservice();
                            entamcur.Amount = (decimal)item["amount"];
                            entamcur.Currencytypeid = (int)item["currencytype"];
                            entamcur.Attributeid = (int)item["Attributeid"];
                            listentityamountcurrencytype.Add(entamcur);
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }
                //saveObj[0]["Typeid"].ToString()
                var typeid = int.Parse(saveObj[0]["Typeid"].ToString());
                var Name = saveObj[0]["Name"].ToString();
                var FileName = saveObj[0]["FileName"].ToString();
                var VersionNo = int.Parse(saveObj[0]["VersionNo"].ToString());
                var MimeType = saveObj[0]["MimeType"].ToString();
                var Extension = saveObj[0]["Extension"].ToString();
                var Size = int.Parse(saveObj[0]["Size"].ToString());
                var Status = int.Parse(saveObj[0]["Status"].ToString());
                var EntityID = int.Parse(saveObj[0]["EntityID"].ToString());
                var FileGuid = saveObj[0]["FileGuid"].ToString();
                var Description = saveObj[0]["Description"].ToString();
                var Active = bool.Parse(saveObj[0]["Active"].ToString());
                var FolderID = 0;
                if (saveObj[0]["FolderID"] != null)
                    FolderID = int.Parse(saveObj[0]["FolderID"].ToString());

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateAsset(FolderID, typeid, Name, listattributevalues, listEntityperiods, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description, false, 0, 0, null, null, 0, false, 0, false, null, false, listentityamountcurrencytype);
                //result.Response = marcomManager.DigitalAssetManager.CreateAsset(FolderID, typeid, Name, listattributevalues, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateAsset method hits exception  as '" + ex + "' with key " + ((string[])(Request.Headers.GetValues("sessioncookie")))[0], BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/CreateBlankAsset")]
        [HttpPost]
        public ServiceResponse CreateBlankAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray saveObj = (JArray)jobj["saveAsset"];
                JArray arrperiods = (JArray)saveObj[0]["Periods"];
                JArray arrobjAttibutevalues = (JArray)saveObj[0]["AttributeData"];
                JArray arrentityamountcurrencytype = (JArray)saveObj[0]["Entityamountcurrencytype"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                IList<IDamPeriod> listEntityperiods = new List<IDamPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                        {

                            IDamPeriod enti = marcomManager.DigitalAssetManager.Damperiodservice();
                            enti.Startdate = startDate;
                            enti.EndDate = endDate;
                            enti.Description = (string)arrp["comment"];
                            enti.SortOrder = (int)arrp["sortorder"];
                            listEntityperiods.Add(enti);
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IAssetAmountCurrencyType> listentityamountcurrencytype = new List<IAssetAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            IAssetAmountCurrencyType entamcur = marcomManager.DigitalAssetManager.AssetAmountCurrencyTypeservice();
                            entamcur.Amount = (decimal)item["amount"];
                            entamcur.Currencytypeid = (int)item["currencytype"];
                            entamcur.Attributeid = (int)item["Attributeid"];
                            listentityamountcurrencytype.Add(entamcur);
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }
                //saveObj[0]["Typeid"].ToString()
                var typeid = int.Parse(saveObj[0]["Typeid"].ToString());
                var Name = saveObj[0]["Name"].ToString();
                var Status = int.Parse(saveObj[0]["Status"].ToString());
                var EntityID = int.Parse(saveObj[0]["EntityID"].ToString());
                var Active = bool.Parse(saveObj[0]["Active"].ToString());
                var FolderID = 0;
                if (saveObj[0]["FolderID"] != null)
                    FolderID = int.Parse(saveObj[0]["FolderID"].ToString());
                var Category = int.Parse(saveObj[0]["Category"].ToString());
                var url = saveObj[0]["Url"].ToString();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateBlankAsset(FolderID, typeid, Name, listattributevalues, EntityID, Category, listEntityperiods, url, false, 0, null, 0, listentityamountcurrencytype);

            }
            catch
            {

            }
            return result;
        }

        [Route("api/Dam/DAMGetAdminSettings/{LogoSettings}")]
        [HttpGet]
        public ServiceResponse DAMGetAdminSettings(string LogoSettings)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DAMGetAdminSettings(LogoSettings);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Dam/GetEntityDamFolder/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityDamFolder(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetEntityDamFolder(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetAssetAttributesDetails/{ID}")]
        [HttpGet]
        public ServiceResponse GetAssetAttributesDetails(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetAttributesDetails(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetAssets/{FolderID}/{EntityID}/{View}/{orderby}/{pageNo}/{ViewAll}")]
        [HttpGet]
        public ServiceResponse GetAssets(int FolderID, int EntityID, int View, int orderby, int pageNo, bool ViewAll)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssets(FolderID, EntityID, View, orderby, pageNo, ViewAll);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetLightboxAssets")]
        [HttpPost]
        public ServiceResponse GetLightboxAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int View = (int)jobj["View"];
                int orderby = (int)jobj["orderby"];
                int pageNo = (int)jobj["pageNo"];
                JArray typeObj = (JArray)jobj["assetArr"];
                int[] assetIdArr = typeObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetLightboxAssets(assetIdArr, View, orderby, pageNo, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetMediaAssets")]
        [HttpPost]
        public ServiceResponse GetMediaAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray typeObj = (JArray)jobj["assetTypesArr"];
                string filter = (string)jobj["filterschema"];
                int[] assettypeIdArr = typeObj.Select(jv => (int)jv["Id"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetMediaAssets((int)jobj["View"], (int)jobj["orderby"], (int)jobj["pageNo"], assettypeIdArr, filter, (int)jobj["totalRecords"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetDAMViewSettings")]
        [HttpGet]
        public ServiceResponse GetDAMViewSettings()
        {
            result = new ServiceResponse();
            try
            {
              
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMViewSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/CreateFolder")]
        [HttpPost]
        public ServiceResponse CreateFolder([FromBody]JObject saveObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                // JArray saveObj = (JArray)jobj["NewFolderObj"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateFolder((int)saveObj["EntityId"], (int)saveObj["ParentNodeId"], (int)saveObj["Level"], (int)saveObj["Nodeid"], (int)saveObj["id"], (string)saveObj["Key"], (int)saveObj["SortOrder"], (string)saveObj["Caption"], (string)saveObj["Description"], (string)saveObj["Colorcode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/UpdateFolder")]
        [HttpPost]
        public ServiceResponse UpdateFolder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateFolder((int)jobj["id"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Colorcode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/DeleteFolder/")]
        [HttpPost]
        public ServiceResponse DeleteFolder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrFilesObj = (JArray)jobj["FolderArr"];
                int[] folderIdList = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteFolder(folderIdList);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/DAM/DeleteAssets")]
        [HttpPost]
        public ServiceResponse DeleteAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["FolderArr"];
                int[] folderIdList = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteAssets(folderIdList);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/DAM/SaveFiletoAsset")]
        [HttpPost]
        public ServiceResponse SaveFiletoAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray fileObj = (JArray)jobj["fileDetails"];
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = null;

                result.Response = marcomManager.DigitalAssetManager.SaveFiletoAsset((int)fileObj[0]["AssetID"], (int)fileObj[0]["Status"], (string)fileObj[0]["MimeType"], (long)fileObj[0]["Size"], (string)fileObj[0]["FileGuid"], DateTime.Parse(fileObj[0]["CreatedOn"].ToString()), (string)fileObj[0]["Extension"], (string)fileObj[0]["Name"], (int)fileObj[0]["VersionNo"], (string)fileObj[0]["Description"], (int)fileObj[0]["OwnerID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/UpdateAssetVersion")]
        [HttpPost]
        public ServiceResponse UpdateAssetVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetVersion((int)jobj["AssetID"], (int)jobj["fileid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/DAM/DownloadDamZip")]
        [HttpPost]
        public ServiceResponse DownloadDamZip([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv["AssetId"]).ToArray();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                if (arrFilesObj[0]["Iscropped"] != null)
                {
                    int[] Iscropped = arrFilesObj.Select(jv => (int)jv["Iscropped"]).ToArray();
                    for (int i = 0; i < assetIdArr.Length; i++)
                    {
                        list.Add(new KeyValuePair<string, int>(assetIdArr[i].ToString(), Iscropped[i]));
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DownloadDamFiles(assetIdArr, true, 0, list);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/SaveDetailBlockForAssets")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jassetdet = (JArray)jobj["AssetDetails"];
                JArray jrnewValue = (JArray)jassetdet[0]["NewValue"];
                List<object> obj1 = new List<object>();
                foreach (var obj in jrnewValue)
                {
                    if (obj.Type == JTokenType.Boolean)
                    {
                        obj1.Add(Convert.ToBoolean(obj.ToString()));
                    }
                    else if (obj.Type == JTokenType.Date)
                    {
                        obj1.Add(obj.ToString());

                    }
                    else if (obj.Type == JTokenType.Integer)
                    {

                        obj1.Add(int.Parse(obj.ToString()));

                    }
                    else
                        obj1.Add(obj.ToString());
                }
                result.Response = marcomManager.DigitalAssetManager.SaveDetailBlockForAssets((int)jobj["AssetID"], (int)jassetdet[0]["AttributetypeID"], (int)jobj["AttributeID"], obj1, (int)jobj["Level"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/UpdateAssetAccess")]
        [HttpPost]
        public ServiceResponse UpdateAssetAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetAccess((int)jobj["assetid"], (string)jobj["accessid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/AssetRoleAccess")]
        [HttpPost]
        public ServiceResponse AssetRoleAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                if ((jobj["ID"] != null) && (jobj["RoleID"] != null))
                {
                    JArray arrFilesObj = (JArray)jobj["RoleID"];
                    int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                    int[] RoleIDArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                    result.StatusCode = (int)HttpStatusCode.OK;

                    result.Response = marcomManager.DigitalAssetManager.AssetRoleAccess((int)jobj["ID"], RoleIDArr);
                    //foreach (var val in arrFilesObj)
                    //{
                    //    result.Response = marcomManager.DigitalAssetManager.AssetRoleAccess((int)jobj["ID"], (int)val);
                    //}
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/DuplicateAssets")]
        [HttpPost]
        public ServiceResponse DuplicateAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DuplicateAssets(assetIdArr);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/CheckPreviewGenerator")]
        [HttpPost]
        public ServiceResponse CheckPreviewGenerator([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray assetid = (JArray)jobj["AssetIDs"];
                string asset = string.Join(",", assetid.Select(x => x.ToString()).ToArray());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CheckPreviewGenerator(asset);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetOptionDetailListByAssetID/{ID}/{AssetID}")]
        [HttpGet]
        public ServiceResponse GetOptionDetailListByAssetID(int ID, int AssetID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetOptionDetailListByAssetID(ID, AssetID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/PublishAssets")]
        [HttpPost]
        public ServiceResponse PublishAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.PublishAssets(assetIdArr, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/SendMail")]
        [HttpPost]
        public ServiceResponse SendMail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject srcObj = (JObject)jobj["AssetArr"];
                bool IncludeMdata = (bool)srcObj["metadata"];
                JArray assetArr = (JArray)srcObj["AssetArr"];
                int[] assetIdArr = assetArr.Select(jv => (int)jv["AssetId"]).ToArray();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                if (assetArr[0]["Iscropped"] != null)
                {
                    int[] Iscropped = assetArr.Select(jv => (int)jv["Iscropped"]).ToArray();
                    for (int i = 0; i < assetIdArr.Length; i++)
                    {
                        list.Add(new KeyValuePair<string, int>(assetIdArr[i].ToString(), Iscropped[i]));
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.SendMail(IncludeMdata, assetIdArr, (string)srcObj["Toaddress"], (string)srcObj["subject"], list);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/MoveFilesToUploadImagesToCreateTask")]
        [HttpPost]
        public ServiceResponse MoveFilesToUploadImagesToCreateTask([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];
                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        attachval.IsExist = (bool)arm["IsExists"];
                        listFiles.Add(attachval);
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.MoveFilesToUploadImagesToCreateTask(listFiles, (int)jobj["ParentEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/DeleteAttachmentVersionByAssetID/{fileid}")]
        [HttpDelete]
        public ServiceResponse DeleteAttachmentVersionByAssetID(int fileid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteAttachmentVersionByAssetID(fileid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/DAM/UpdateAssetAccessSettings")]
        [HttpPost]
        public ServiceResponse UpdateAssetAccessSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetAccessSettings((int)jobj["RoleID"], (int)jobj["IsChecked"] == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/UpdateAssetDetails")]
        [HttpPost]
        public ServiceResponse UpdateAssetDetails([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray fileObj = (JArray)jobj["assetupdatedets"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetDetails((int)fileObj[0]["AssetID"], (string)fileObj[0]["AssetName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/GetProfileFiles")]
        [HttpGet]
        public ServiceResponse GetProfileFiles()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetProfileFiles();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/InsertUpdateProfileFiles")]
        [HttpPost]
        public ServiceResponse InsertUpdateProfileFiles([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateProfileFiles((int)jobj["ID"], (string)jobj["Name"], (double)jobj["Height"], (double)jobj["Width"], (string)jobj["Extension"], (int)jobj["DPI"], jobj["GlobalRoleID"].ToString());
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/DeleteFileProfile/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFileProfile(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteProfilefile(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetOptimakerSettings")]
        [HttpGet]
        public ServiceResponse GetOptimakerSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetOptimakerSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetOptmakerCatagories")]
        [HttpGet]
        public ServiceResponse GetOptmakerCatagories()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetOptmakerCatagories();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetCategoryTreeCollection")]
        [HttpGet]
        public ServiceResponse GetCategoryTreeCollection()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetCategoryTreeCollection();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/InsertUpdateCatergory")]
        [HttpPost]
        public ServiceResponse InsertUpdateCatergory([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JObject jobj1 = (JObject)jobj["jobj"][0];
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateCatergory(jobj1);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/InsertUpdateOptimakerSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateOptimakerSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateOptimakerSettings((int)jobj["ID"], (string)jobj["Name"], (int)jobj["CategoryId"], (string)jobj["Description"], (int)jobj["DocId"], (int)jobj["DeptId"], (int)jobj["DocType"], (int)jobj["DocVersionId"], (string)jobj["PreviewImage"], (int)jobj["AutoApproval"], (int)jobj["TemplateEngineType"], (int)jobj["AssetType"], (string)jobj["ServerURL"], (bool)jobj["EnableHighResPDF"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/DeleteOptimakeSetting/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteOptimakeSetting(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteOptimakeSetting(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Dam/DeleteOptimakerCategory/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteOptimakerCategory(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteOptimakerCategory(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Dam/GetAllOptimakerSettings/{categorytID}")]
        [HttpGet]
        public ServiceResponse GetAllOptimakerSettings(int categorytID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAllOptimakerSettings(categorytID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetOptimakerSettingsBaseURL")]
        [HttpGet]
        public ServiceResponse GetOptimakerSettingsBaseURL()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetOptimakerSettingsBaseURL();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetMediaGeneratorSettingsBaseURL")]
        [HttpGet]
        public ServiceResponse GetMediaGeneratorSettingsBaseURL()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetMediaGeneratorSettingsBaseURL();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/CreateMediaGeneratorAsset")]
        [HttpPost]
        public ServiceResponse CreateMediaGeneratorAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                //TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                Guid systemSession = MarcomManagerFactory.GetSystemSession((int)jobj["TenantID"]);
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,systemSession);
                result.StatusCode = (int)HttpStatusCode.OK;
                marcomManager.DigitalAssetManager.CreateMediaGeneratorAsset((int)jobj["EntityID"], (int)jobj["FolderID"], (int)jobj["AssetTypeID"], (int)jobj["CreatedBy"], (int)jobj["DocversionID"], (string)jobj["AssetName"], (string)jobj["jpegFilename"], (long)jobj["jpegsize"], (int)jobj["jpegVersion"], (string)jobj["jpegDesc"], (string)jobj["jpegGuid"], (string)jobj["highResGUID"], (string)jobj["lowResGUID"], (int)jobj["DocID"], marcomManager.User.TenantID);
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
                string LogFile = System.Web.HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("EXCEPTION IN service FUNCTION CreateMediaGeneratorAsset" + e);
                    sw.WriteLine("----------------");

                }
            }
            return result;
        }

        [Route("api/Dam/GetDAMViewAdminSettings/{ViewType}/{DamType}")]
        [HttpGet]
        public ServiceResponse GetDAMViewAdminSettings(string ViewType, int DamType)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMViewAdminSettings(ViewType, DamType);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/UpdateDamViewStatus")]
        [HttpPut]
        public ServiceResponse UpdateDamViewStatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray attrlist = (JArray)jobj["attribtueslist"];
                result.StatusCode = (int)HttpStatusCode.OK;
                IList<object> obj = new List<object>();
                IList<IDamViewAttribtues> damattrs = new List<IDamViewAttribtues>();
                if (attrlist != null)
                {
                    foreach (var attr in attrlist)
                    {
                        IDamViewAttribtues attrbs = new DamViewAttributes();
                        attrbs.ID = (int)attr["ID"];
                        attrbs.DisplayName = (string)attr["DisplayName"];
                        attrbs.SortOrder = (int)attr["SortOrder"];
                        attrbs.Level = (int)attr["Level"];
                        attrbs.Type = (int)attr["Type"];
                        attrbs.IsColumn = (bool)attr["IsColumn"];
                        attrbs.IsToolTip = (bool)attr["IsToolTip"];
                        damattrs.Add(attrbs);
                    }
                }
                result.Response = marcomManager.DigitalAssetManager.UpdateDamViewStatus((string)jobj["ViewType"], (int)jobj["DamType"], damattrs);
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/GetDAMToolTipSettings")]
        [HttpGet]
        public ServiceResponse GetDAMToolTipSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDAMToolTipSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetStatusFilteredEntityAsset/{FolderID}/{EntityID}/{View}/{orderby}/{pageNo}/{statusFilter}")]
        [HttpGet]
        public ServiceResponse GetStatusFilteredEntityAsset(int FolderID, int EntityID, int View, int orderby, int pageNo, String statusFilter)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetStatusFilteredEntityAsset(FolderID, EntityID, View, orderby, pageNo, statusFilter);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/CreateDOCGeneratedAsset")]
        [HttpPost]
        public ServiceResponse CreateDOCGeneratedAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                marcomManager.DigitalAssetManager.CreateDOCGeneratedAsset((int)jobj["EntityID"], (int)jobj["FolderID"], (int)jobj["AssetTypeID"], (int)jobj["CreatedBy"], (string)jobj["filename"], (string)jobj["assetname"], marcomManager.User.TenantID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/InsertUpdateWordTemplateSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateWordTemplateSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateWordTemplateSettings((int)jobj["ID"], (string)jobj["Name"], (int)jobj["CategoryId"], (string)jobj["Description"], (string)jobj["Worddocpath"], (string)jobj["PreviewImage"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/DeleteWordtempSetting/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteWordtempSetting(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteWordtempSetting(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Dam/GetWordTemplateSettings")]
        [HttpGet]
        public ServiceResponse GetWordTemplateSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetWordTemplateSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetPublishedAssets/{View}/{orderby}/{pageNo}")]
        [HttpGet]
        public ServiceResponse GetPublishedAssets(int View, int orderby, int pageNo)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetPublishedAssets(View, orderby, pageNo);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/AddPublishedFilesToDAM")]
        [HttpPost]
        public ServiceResponse AddPublishedFilesToDAM([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.AddPublishedFilesToDAM(assetIdArr, (int)jobj["EntityID"], (int)jobj["FolderID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/AddPublishedLinkFiles")]
        [HttpPost]
        public ServiceResponse AddPublishedLinkFiles([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int EntityID = (int)jobj["EntityID"];
                int FolderID = (int)jobj["FolderID"];
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.AddPublishedLinkFiles(assetIdArr, EntityID, FolderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/CreateversionDOCGeneratedAsset")]
        [HttpPost]
        public ServiceResponse CreateversionDOCGeneratedAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                marcomManager.DigitalAssetManager.CreateversionDOCGeneratedAsset((int)jobj["EntityID"], (int)jobj["FolderID"], (int)jobj["AssetTypeID"], (int)jobj["CreatedBy"], (string)jobj["filename"], (string)jobj["assetname"], (int)jobj["assetid"], marcomManager.User.TenantID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/getkeyvaluefromwebconfig/{key}")]
        [HttpGet]
        public ServiceResponse getkeyvaluefromwebconfig(string key)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.getkeyvaluefromwebconfig(key);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetAssetActiveFileinfo/{assetId}")]
        [HttpGet]
        public ServiceResponse GetAssetActiveFileinfo(int assetId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = null;
                result.Response = marcomManager.DigitalAssetManager.GetAssetActiveFileinfo(assetId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetMimeType/")]
        [HttpGet]
        public ServiceResponse GetMimeType()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetMimeType();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/CropRescale")]
        [HttpPost]
        public ServiceResponse CropRescale([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray fileCropObj = (JArray)jobj["fileCropRescale"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CropRescale((int)fileCropObj[0]["AssetID"], (int)fileCropObj[0]["TopLeft"], (int)fileCropObj[0]["TopRight"], (int)fileCropObj[0]["BottomLeft"], (int)fileCropObj[0]["BottomRight"], (int)fileCropObj[0]["CropFormat"], (int)fileCropObj[0]["ScaleWidth"], (int)fileCropObj[0]["ScaleHeight"], (int)fileCropObj[0]["Dpi"], (int)fileCropObj[0]["profileid"], (string)fileCropObj[0]["fileformate"], (int)fileCropObj[0]["cropfrom"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetProfileFilesByUser/{UserId}")]
        [HttpGet]
        public ServiceResponse GetProfileFilesByUser(int UserId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetProfileFilesByUser(UserId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetBreadCrumFolderPath/{EntityID}/{CurrentFolderID}")]
        [HttpGet]
        public ServiceResponse GetBreadCrumFolderPath(int EntityID, int CurrentFolderID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetBreadCrumFolderPath(EntityID, CurrentFolderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetAllFolderStructure/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAllFolderStructure(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAllFolderStructure(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/AddNewsFeedinfo")]
        [HttpPost]
        public ServiceResponse AddNewsFeedinfo([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray newsFeedinfo = (JArray)jobj["newsFeedinfo"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.AddNewsFeedinfo((int)newsFeedinfo[0]["AssetID"], (string)newsFeedinfo[0]["action"], (string)newsFeedinfo[0]["TypeName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/SaveCropedImageForLightBox")]
        [HttpPost]
        public ServiceResponse SaveCropedImageForLightBox([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject newjobj = JObject.Parse(jobj["CropArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.SaveCropedImageForLightBox((string)newjobj["imgsourcepath"], (int)newjobj["imgwidth"], (int)newjobj["imgheight"], (int)newjobj["imgX"], (int)newjobj["imgY"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetMediaBankFilterAttributes")]
        [HttpGet]
        public ServiceResponse GetMediaBankFilterAttributes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetMediaBankFilterAttributes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/MoveAssets")]
        [HttpPost]
        public ServiceResponse MoveAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv["AssetId"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.MoveAssets(assetIdArr, (int)jobj["FolerId"], (int)jobj["EntityID"], (int)jobj["ActionCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/AttachAssets")]
        [HttpPost]
        public ServiceResponse AttachAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.AttachAssets(assetIdArr, (int)jobj["EntityID"], (int)jobj["FolerId"], true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetCustomFilterAttributes/{Type}")]
        [HttpGet]
        public ServiceResponse GetCustomFilterAttributes(int Type)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetCustomFilterAttributes(Type);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetSearchCriteriaAdminSettings/{LogoSettings}/{Key}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetSearchCriteriaAdminSettings(string LogoSettings, string Key, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetSearchCriteriaAdminSettings(LogoSettings, Key, typeid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/AttachAssetsforProofTask")]
        [HttpPost]
        public ServiceResponse AttachAssetsforProofTask([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.AttachAssetsforProofTask(assetIdArr, (int)jobj["EntityID"], (int)jobj["FolerId"], true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/DAM/CreateProofTaskWithAttachment")]
        [HttpPost]
        public ServiceResponse CreateProofTaskWithAttachment([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateProofTaskWithAttachment((int)jobj["TaskID"], assetIdArr, (int)jobj["Tasktypeid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/ChangeAssettype/{assetTypeID}/{assetID}")]
        [HttpGet]
        public ServiceResponse ChangeAssettype(int assetTypeID, int assetID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.ChangeAssettype(assetTypeID, assetID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/GetAssetAttributueswithTypeID/{assetTypeID}")]
        [HttpGet]
        public ServiceResponse GetAssetAttributueswithTypeID(int assetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetAttributueswithTypeID(assetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/DAM/SaveAssetTypeofAsset")]
        [HttpPost]
        public ServiceResponse SaveAssetTypeofAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray saveObj = (JArray)jobj["saveAsset"];
                JArray arrperiods = (JArray)saveObj[0]["PeriodData"];
                JArray arrobjAttibutevalues = (JArray)saveObj[0]["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                IList<IDamPeriod> listEntityperiods = new List<IDamPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        //if (entityPeriodObj.Count() > 0)
                        //{
                        //    foreach (var arrp in entityPeriodObj)
                        //    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                        {

                            IDamPeriod enti = marcomManager.DigitalAssetManager.Damperiodservice();
                            enti.Startdate = startDate;
                            enti.EndDate = endDate;
                            enti.Description = (string)arrp["comment"];
                            enti.SortOrder = (int)arrp["sortorder"];
                            listEntityperiods.Add(enti);
                        }
                        //    }
                        //}
                    }
                }

                var typeid = int.Parse(saveObj[0]["Typeid"].ToString());
                var Name = saveObj[0]["Name"].ToString();
                var AssetID = int.Parse(saveObj[0]["AssetID"].ToString());
                var oldAssetTypeId = int.Parse(saveObj[0]["oldAssetTypeId"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.SaveAssetTypeofAsset(typeid, Name, listattributevalues, AssetID, oldAssetTypeId, listEntityperiods);
            }
            catch
            {
            }
            return result;
        }

        [Route("api/DAM/SendMailWithMetaData")]
        [HttpPost]
        public ServiceResponse SendMailWithMetaData([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //JObject srcObj = (JObject)jobj["AssetArr"];
                bool IncludeMdata = (bool)jobj["metadata"];
                JArray assetArr = (JArray)jobj["AssetArr"];
                int[] assetIdArr = assetArr.Select(jv => (int)jv["AssetId"]).ToArray();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                if (assetArr[0]["Iscropped"] != null)
                {
                    int[] Iscropped = assetArr.Select(jv => (int)jv["Iscropped"]).ToArray();
                    for (int i = 0; i < assetIdArr.Length; i++)
                    {
                        list.Add(new KeyValuePair<string, int>(assetIdArr[i].ToString(), Iscropped[i]));
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.SendMailWithMetaData(IncludeMdata, assetIdArr, (string)jobj["Toaddress"], (string)jobj["subject"], list);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        //Newly Added by RaJa
        [Route("api/Dam/GetDamTreeNodeByEntityID/{AttributeID}/{ParentEntityID}")]
        [HttpGet]
        public ServiceResponse GetDamTreeNodeByEntityID(int AttributeID, int ParentEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDamTreeNode(AttributeID, false, ParentEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetDamTreeNode/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetDamTreeNode(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDamTreeNode(AttributeID, false);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetDropDownTreePricingObject/{AttributeID}/{IsInheritFromParent}/{Isfromparent}/{Entityid}/{ParentId}")]
        [HttpGet]
        public ServiceResponse GetDropDownTreePricingObject(int AttributeID, bool IsInheritFromParent, bool Isfromparent, int Entityid, int ParentId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDropDownTreePricingObjectDetail((int)AttributeID, (bool)IsInheritFromParent, (bool)Isfromparent, (int)Entityid, (int)ParentId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetDropDownTreePricingObjectFromParentDetail/{AttributeID}/{IsInheritFromParent}/{Isfromparent}/{Entityid}/{ParentId}")]
        [HttpGet]
        public ServiceResponse GetDropDownTreePricingObjectFromParentDetail(int AttributeID, bool IsInheritFromParent, bool Isfromparent, int Entityid, int ParentId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetDropDownTreePricingObjectFromParentDetail((int)AttributeID, (bool)IsInheritFromParent, (bool)Isfromparent, (int)Entityid, (int)ParentId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/UpdateDropDownTreePricing")]
        [HttpPost]
        public ServiceResponse UpdateDropDownTreePricing([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int EntityID = (int)jobj["EntityID"];
                int AttributeID = (int)jobj["AttributeID"];
                JArray jrnewValue = (JArray)jobj["NewValue"];
                IList<IDamTreeValue> iitree = new List<IDamTreeValue>();
                foreach (var obj in jrnewValue)
                {
                    IDamTreeValue itree = new DamTreeValue();
                    itree.Nodeid = (int)obj["NodeId"];
                    itree.Level = (int)obj["Level"];
                    itree.Value = (string)obj["value"] != "-1" ? (string)obj["value"] : "";
                    iitree.Add(itree);
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateDropDownTreePricing((int)EntityID, (int)jobj["AttributetypeID"], (int)AttributeID, iitree);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/DAM/InsertAssetPeriod")]
        [HttpPost]
        public ServiceResponse InsertAssetPeriod([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateAssetPeriod((int)jobj["EntityID"], (DateTime)jobj["StartDate"], (DateTime)jobj["EndDate"], (string)jobj["Description"], (int)jobj["SortOrder"], (int)jobj["attributeid"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/DAM/AssetPeriod")]
        [HttpPost]
        public ServiceResponse AssetPeriod([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetPeriod((int)jobj["ID"], (int)jobj["EntityID"], (string)jobj["StartDate"], (string)jobj["EndDate"], (int)jobj["SortOrder"], (string)jobj["Description"], (int)jobj["attributeid"]);
                //result.Response = marcomManager.PlanningManager.UpdateEntityPeriod((int)jobj["ID"], (int)planning.EntityID, (string)planning.StartDate, (string)planning.EndDate, (int)planning.SortOrder, (string)planning.Description);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Dam/DeleteAssetPeriod/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAssetPeriod(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteAssetPeriod(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Dam/GetAttributeTreeNode/{AttributeID}/{EntityID}/{Ischoosefromparent}")]
        [HttpGet]
        public ServiceResponse GetAttributeTreeNode(int AttributeID, int EntityID, bool Ischoosefromparent)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (!Ischoosefromparent)
                    result.Response = marcomManager.DigitalAssetManager.GetAttributeTreeNode(AttributeID, EntityID);
                else
                    result.Response = marcomManager.DigitalAssetManager.GetDetailAttributeTreeNodeFromParent(AttributeID, EntityID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/SaveDetailBlockForTreeLevels")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForTreeLevels([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int AssetID = (int)jobj["AssetID"];
                int AttributeID = (int)jobj["AttributeID"];
                JArray jrnewValue = (JArray)jobj["NewValue"];
                JArray jrnewtree = (JArray)jobj["newTree"];
                JArray jroldTree = (JArray)jobj["oldTree"];

                IList<IDamTreeValue> iitree = new List<IDamTreeValue>();

                foreach (var obj in jrnewValue)
                {
                    IDamTreeValue itree = new DamTreeValue();
                    itree.Nodeid = (int)obj["id"];
                    itree.Level = (int)obj["Level"];
                    iitree.Add(itree);
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.SaveDetailBlockForTreeLevels(AssetID, (int)jobj["AttributetypeID"], AttributeID, iitree, (JArray)jobj["oldTree"], (JArray)jobj["newTree"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/DAM/UpdateAssetThumpnail")]
        [HttpPost]
        public ServiceResponse UpdateAssetThumpnail([FromBody]JObject srcObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //JObject srcObj = (JObject)jobj["SourceObj"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetThumpnail((string)srcObj["imgsourcepath"], (int)srcObj["imgwidth"], (int)srcObj["imgheight"], (int)srcObj["imgX"], (int)srcObj["imgY"], (string)srcObj["Preview"], (string)srcObj["Fileguid"], (string)srcObj["AssetExt"], (int)srcObj["AssetId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/DAM/GetlistoflinkedAsset")]
        [HttpPost]
        public ServiceResponse GetlistoflinkedAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int entityID = (int)jobj["EntityID"];
                JArray idArr = (JArray)jobj["idArr"];
                JArray arrFilesObj = (JArray)jobj["FolderArr"];
                int[] folderIdList = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetlistoflinkedAsset(entityID, folderIdList);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Dam/IsActiveLinkEntity/{EntityID}")]
        [HttpGet]
        public ServiceResponse IsActiveLinkEntity(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.IsActiveLinkEntity(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/DAM/DownloadAssetWithMetadata")]
        [HttpPost]
        public ServiceResponse DownloadAssetWithMetadata([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["assetid"];
                bool entityID = (bool)(jobj["result"]);
                int[] folderIdList = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;

                //IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,(Guid)Session["ID"]);
                //result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DownloadAssetWithMetadata(folderIdList, entityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/CreateReformatProfile")]
        [HttpPost]
        public ServiceResponse CreateReformatProfile([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject saveObj = (JObject)jobj["SaveReformatProfile"];
                var ProfileID = int.Parse(saveObj["ProfileID"].ToString());
                var Name = saveObj["Name"].ToString();
                var Units = int.Parse(saveObj["Units"].ToString());
                var srcInputObj = (JObject)saveObj["ProfileSrcValue"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CreateReformatProfile(ProfileID, Name, Units, srcInputObj);
            }
            catch
            {
            }
            return result;
        }
        [Route("api/Dam/GetReformatProfile/{ProfileID}")]
        [HttpGet]
        public ServiceResponse GetReformatProfile(int ProfileID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetReformatProfile(ProfileID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/ReformatRescale")]
        [HttpPost]
        public ServiceResponse ReformatRescale([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject saveObj = (JObject)jobj["SaveReformatRescale"];
                var ProfileID = int.Parse(saveObj["ProfileID"].ToString());
                var AssetId = int.Parse(saveObj["AssetId"].ToString());
                var Units = int.Parse(saveObj["Units"].ToString());
                var Folderid = int.Parse(saveObj["Folderid"].ToString());
                var srcInputObj = (JObject)saveObj["srcInputObj"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.ReformatRescale(AssetId, ProfileID, Units, Folderid, srcInputObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/UnPublishAssets")]
        [HttpPost]
        public ServiceResponse UnPublishAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UnPublishAssets(assetIdArr, false, 0);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/ReformateCreateAsset")]
        [HttpPost]
        public ServiceResponse ReformateCreateAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject saveObj = (JObject)jobj["SaveReformatAsset"];
                var AssetId = int.Parse(saveObj["AssetId"].ToString());
                var EntityID = int.Parse(saveObj["EntityID"].ToString());
                var FolderID = int.Parse(saveObj["FolderID"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.ReformateCreateAsset(AssetId, EntityID, FolderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/GetAssetDetsforSelectedAssets")]
        [HttpPost]
        public ServiceResponse GetAssetDetsforSelectedAssets([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                int folderID = int.Parse(jobj["FolderID"].ToString());
                int entityID = int.Parse(jobj["EntityID"].ToString());
                int View = int.Parse(jobj["View"].ToString());
                int Orderby = int.Parse(jobj["Orderby"].ToString());
                int PageNo = int.Parse(jobj["PageNo"].ToString());
                //var Active = bool.Parse(saveObj[0]["Active"].ToString());
                bool ViewAll = bool.Parse(jobj["ViewAll"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetDetsforSelectedAssets(folderID, entityID, View, Orderby, PageNo, assetIdArr, ViewAll);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        //GetAssetsbasedonFolderandEntity


        //Get["/GetAssetsbasedonFolderandEntity/{entityID}/{FolderID}}/{View}/{Orderby}/{ViewAll}"] = dam =>
        //{

        //    result = new ServiceResponse();
        //    try
        //    {
        //        marcomManager =MarcomManagerFactory.GetMarcomManager(Request,(Guid)Session["ID"]);
        //        result.StatusCode = (int)HttpStatusCode.OK;
        //        result.Response = marcomManager.DigitalAssetManager.GetAssetsbasedonFolderandEntity((int)dam.entityID, (int)dam.FolderID, (int)dam.View, (int)dam.Orderby, (bool)dam.ViewAll);
        //    }
        //    catch
        //    {
        //        result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
        //        result.Response = null;
        //    }
        //    return result;
        //};

        //Get["/GetAllAssetsBasedOnFolderandEntity/{entityID}/{FolderID}}/{View}/{Orderby}/{ViewAll}"] = dam =>
        //    {
        //        result = new ServiceResponse();
        //        try
        //        {
        //            marcomManager =MarcomManagerFactory.GetMarcomManager(Request,(Guid)Session["ID"]);
        //            result.StatusCode = (int)HttpStatusCode.OK;
        //            result.Response = marcomManager.DigitalAssetManager.GetAssetsbasedonFolderandEntity((int)dam.entityID, (int)dam.FolderID, (int)dam.View, (int)dam.Orderby, (bool)dam.ViewAll);
        //        }
        //        catch
        //        {
        //            result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
        //            result.Response = null;
        //        }
        //        return result;
        //    };
        [Route("api/DAM/GetAllAssetsBasedOnFolderandEntity")]
        [HttpPost]
        public ServiceResponse GetAllAssetsBasedOnFolderandEntity([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //JArray arrFilesObj = (JArray)jobj["AssetArr"];
                //int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                int folderID = (int)jobj["FolderID"];
                int entityID = (int)jobj["EntityID"];
                int View = (int)jobj["View"];
                int Orderby = (int)jobj["Orderby"];
                //int PageNo = int.Parse(jobj["PageNo"].ToString());
                //var Active = bool.Parse(saveObj[0]["Active"].ToString());
                bool ViewAll = (bool)jobj["ViewAll"];
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.DigitalAssetManager.GetAssetDetsforSelectedAssets(folderID, entityID, View, Orderby, PageNo, assetIdArr, ViewAll);
                result.Response = marcomManager.DigitalAssetManager.GetAssetsbasedonFolderandEntity(entityID, folderID, View, Orderby, ViewAll);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetExternalAssetContent/{ProfileID}")]
        [HttpGet]
        public ServiceResponse GetExternalAssetContent(int ProfileID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetExternalAssetContent();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/InsertUpdateExternalAssetContent")]
        [HttpPost]
        public ServiceResponse InsertUpdateExternalAssetContent([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateExternalAssetContent((string)jobj["htmlcontent"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetAssetCategoryTreeCollection/")]
        [HttpGet]
        public ServiceResponse GetAssetCategoryTreeCollection()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetCategoryTreeCollection();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/InsertUpdateAssetCatergory")]
        [HttpPost]
        public ServiceResponse InsertUpdateAssetCatergory([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JObject jobj1 = (JObject)jobj["jobj"][0];
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateAssetCatergory(jobj1);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/DeleteAssetCategory/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAssetCategory(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteAssetCategory(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Dam/GetAssetTypeAssetCategoryRelation/")]
        [HttpGet]
        public ServiceResponse GetAssetTypeAssetCategoryRelation()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetTypeAssetCategoryRelation();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetSelectedCategoryTreeCollection/{CategoryID}")]
        [HttpGet]
        public ServiceResponse GetSelectedCategoryTreeCollection(int CategoryID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetSelectedCategoryTreeCollection(CategoryID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/InsertUpdateAssetCategoryRelation")]
        [HttpPost]
        public ServiceResponse InsertUpdateAssetCategoryRelation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //    JObject saveObj = (JObject)jobj["AssetCategoryRelation"];
                var AssetTypeId = int.Parse(jobj["AssetTypeID"].ToString());
                var categoryID = int.Parse(jobj["CategoryID"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertUpdateAssetCategoryRelation(AssetTypeId, categoryID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetAssetCategoryTree/{AsetTypeID}")]
        [HttpGet]
        public ServiceResponse GetAssetCategoryTree(int AsetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetCategoryTree(AsetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/GetAssetCategoryPathInAssetEdit/{AsetTypeID}")]
        [HttpGet]
        public ServiceResponse GetAssetCategoryPathInAssetEdit(int AsetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetCategoryPathInAssetEdit(AsetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/UpdateAssetImageName")]
        [HttpPost]
        public ServiceResponse UpdateAssetImageName([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.UpdateAssetImageName((int)jobj["EntityID"], (int)jobj["AttributeID"], (string)jobj["FileName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Dam/GetAssetTypeFileExtensionRelation/")]
        [HttpGet]
        public ServiceResponse GetAssetTypeFileExtensionRelation()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetTypeFileExtensionRelation();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/InsertAssetTypeFileExtension")]
        [HttpPost]
        public ServiceResponse InsertAssetTypeFileExtension([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                var ID = int.Parse(jobj["ID"].ToString());
                var AssetTypeId = int.Parse(jobj["AssetTypeID"].ToString());
                var category = jobj["Category"].ToString();
                var FileExt = jobj["FileExt"].ToString();

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.InsertAssetTypeFileExtension(ID, AssetTypeId, FileExt, category);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/DeleteAssetTypeFileExtension/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAssetTypeFileExtension(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.DeleteAssetTypeFileExtension(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/DAM/GetAssetSnippetTemplates")]
        [HttpPost]
        public ServiceResponse GetAssetSnippetTemplates([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject saveObj = (JObject)jobj["assetSnippetData"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.GetAssetSnippetTemplates(saveObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Dam/getassetSnippetData/")]
        [HttpGet]
        public ServiceResponse getassetSnippetData()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.getassetSnippetData();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/Get2ImagineTemplateUrl")]
        [HttpPost]
        public ServiceResponse Get2ImagineTemplateUrl([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.Get2ImagineTemplateUrl((int)jObj["entityID"], (int)jObj["folderID"], (int)jObj["userID"], (int)jObj["templateType"], (string)jObj["DocID"], (string)jObj["serverUrl"], (int)jObj["assetType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        //Download2ImagineAsset
        [Route("api/Dam/Download2ImagineAsset")]
        [HttpPost]
        public ServiceResponse Download2ImagineAsset([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.Download2ImagineAsset((int)jObj["entityID"], (int)jObj["folderID"], (int)jObj["userID"], (int)jObj["docVersionID"], (string)jObj["serverUrl"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Dam/saveTemplateEngine")]
        [HttpPost]
        public ServiceResponse saveTemplateEngine([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.saveTemplateEngine(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Dam/getAllTemplateEngines")]
        [HttpGet]
        public ServiceResponse getAllTemplateEngines()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.getAllTemplateEngines();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Dam/deleteTemplateEngine/{ID}")]
        [HttpDelete]
        public ServiceResponse deleteTemplateEngine(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.deleteTemplateEngine(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Dam/getServerUrls")]
        [HttpGet]
        public ServiceResponse getServerUrls()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.getServerUrls();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Dam/getCurrentTemplateAssetInfo/{DocID}")]
        [HttpGet]
        public ServiceResponse getCurrentTemplateAssetInfo(int DocID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.getCurrentTemplateAssetInfo(DocID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Dam/Get2ImagineResourceUrl")]
        [HttpPost]
        public ServiceResponse Get2ImagineResourceUrl([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.Get2ImagineResourceUrl((string)jObj["documentId"], (string)jObj["serverUrl"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }

            return result;
        }

        [Route("api/Dam/IsFileExist")]
        [HttpPost]
        public ServiceResponse IsFileExist([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.IsFileExist((string)jObj["filePath"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Dam/S3Settings")]
        [HttpGet]
        public ServiceResponse S3Settings()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.S3Settings();
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [HttpPost]
        [Route("api/Dam/SearchLucenedata")]
        public HttpResponseMessage ReinitiateRejectTask([FromBody]JObject jObj)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://searchlucene.marcomcloud.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());

                    response = client.PostAsJsonAsync("Home/SearchLucenedata", jObj).Result;
                }
                return response;
            }

            catch (Exception ex)
            {
                response = null;
            }
            return response;

        }


        [HttpPost]
        [Route("api/Dam/copydeleteTemplateAsset")]
        public ServiceResponse copydeleteTemplateAsset([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string tempGuid = (string)jObj["tempGuid"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.copydeleteTemplateAsset(tempGuid);
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }

        [Route("api/Dam/CheckAccessToPublish/{Userid}/{EntityId}")]
        [HttpGet]
        public ServiceResponse CheckAccessToPublish(int Userid,int EntityId )
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.CheckAccessToPublish(Userid, EntityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [HttpPost]
        [Route("api/Dam/writeUploadErrorlog")]
        public ServiceResponse writeUploadErrorlog([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager =MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.DigitalAssetManager.writeUploadErrorlog(jObj);
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
    }

}