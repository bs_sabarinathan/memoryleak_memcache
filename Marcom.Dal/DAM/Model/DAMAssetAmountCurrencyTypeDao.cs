﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class DAMAssetAmountCurrencyTypeDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _EntityID;
        protected int _Currencytypeid;
        protected decimal _Amount;
        protected int _Attributeid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

         #region Constructors
		public DAMAssetAmountCurrencyTypeDao() {}

        public DAMAssetAmountCurrencyTypeDao(int pId, int pEntityID, int pCurrencytypeid, decimal pAmount, int pAttributeid)
		{
            this._id = pId;
            this._EntityID = pEntityID;
            this._Currencytypeid = pCurrencytypeid;
            this._Amount = pAmount;
            this._Attributeid = pAttributeid;
		}
		
		#endregion
        #region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityID
        {
            get { return _EntityID; }
            set { _bIsChanged |= (_EntityID != value); _EntityID = value; }

        }

        public virtual int Currencytypeid
        {
            get { return _Currencytypeid; }
            set { _bIsChanged |= (_Currencytypeid != value); _Currencytypeid = value; }

        }

        public virtual decimal Amount
        {
            get { return _Amount; }
            set { _bIsChanged |= (_Amount != value); _Amount = value; }

        }
        public virtual int Attributeid
        {
            get { return _Attributeid; }
            set { _bIsChanged |= (_Attributeid != value); _Attributeid = value; }

        }
        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion 				

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string DAMAssetAmountCurrencyTypeDao = "DAMAssetAmountCurrencyTypeDao";
            public const string EntityID = "EntityID";
            public const string Currencytypeid = "Currencytypeid";
            public const string Amount = "Amount";
            public const string Attributeid = "Attributeid";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string DAMAssetAmountCurrencyTypeDao = "DAMAssetAmountCurrencyTypeDao";
            public const string EntityID = "EntityID";
            public const string Currencytypeid = "Currencytypeid";
            public const string Amount = "Amount";
            public const string Attributeid = "Attributeid";
        }

        #endregion

    }
}
