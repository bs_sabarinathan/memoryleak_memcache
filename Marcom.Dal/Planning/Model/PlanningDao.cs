﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class PlanningDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _cosrcentreid;
        protected DateTime _plandate;
        protected string _description;
        protected decimal _planAmount;
        protected int _currencytype;
        protected int? _status;
        protected int _userid;
        protected bool _isActive;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public PlanningDao() { }

        public PlanningDao(int pEntityid, int pCosrcentreId, DateTime pPlanDate, string pDescription, int pCurrencyType, int? pStatus, int pUserID, decimal pPlanAmount,bool pIsActive)
        {
            this._entityid = pEntityid;
            this._cosrcentreid = pCosrcentreId;
            this._plandate = pPlanDate;
            this._description = pDescription;
            this._currencytype = pCurrencyType;
            this._status = pStatus;
            this._userid = pUserID;
            this._planAmount = pPlanAmount;
            this._isActive = pIsActive;

        }

        public PlanningDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual int CostCentreID
        {
            get { return _cosrcentreid; }
            set { _bIsChanged |= (_cosrcentreid != value); _cosrcentreid = value; }

        }

        public virtual DateTime PlanDate
        {
            get { return _plandate; }
            set { _bIsChanged |= (_plandate != value); _plandate = value; }

        }

        public virtual decimal PlanAmount
        {
            get { return _planAmount; }
            set { _bIsChanged |= (_planAmount != value); _planAmount = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }


        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set { _bIsChanged |= (_currencytype != value); _currencytype = value; }

        }

    
        public virtual int? Status
        {
            get { return _status; }
            set { _bIsChanged |= (_status != value); _status = value; }

        }

        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }

        public virtual bool IsActive
        {
            get { return _isActive; }
            set { _bIsChanged |= (_isActive != value); _isActive = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string PlanningDao = "PlanningDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string CostCentreID = "CostCentreID";
            public const string PlanAmount = "PlanAmount";
            public const string CurrencyType = "CurrencyType";
            public const string Description = "Description";
            public const string PlanDate = "PlanDate";
            public const string Status = "Status";
            public const string UserID = "UserID";
            public const string IsActive = "IsActive";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PlanningDao = "PlanningDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string CostCentreID = "CostCentreID";
            public const string PlanAmount = "PlanAmount";
            public const string CurrencyType = "CurrencyType";
            public const string Description = "Description";
            public const string PlanDate = "PlanDate";
            public const string Status = "Status";
            public const string UserID = "UserID";
            public const string IsActive = "IsActive";
        }

        #endregion


    }

}
