using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFileDefaultInfo interface for table 'CM_FileDefaultInfo'.
    /// </summary>
    public interface IFileDefaultInfo
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Property
        {
            get;
            set;

        }

        string Value
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
