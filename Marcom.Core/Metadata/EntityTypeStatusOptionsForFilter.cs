/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// EntityTypeStatusOptionsForFilter object for table 'MM_EntityTypeStatus_Options'.
    /// </summary>

    internal class EntityTypeStatusOptionsForFilter : IEntityTypeStatusOptionsForFilter
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected string _statusoptions;
        protected bool _isdeleted;
        protected string _shortdesc;
        protected string _colorcode;
        #endregion

        #region Constructors
        public EntityTypeStatusOptionsForFilter() { }

        public EntityTypeStatusOptionsForFilter(int pID, int pEntityTypeID, string pStatusOptions,bool pIsDeleted,string pShortDesc,string pColorCode)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._statusoptions = pStatusOptions;
            this._isdeleted = pIsDeleted;
            this._shortdesc = pShortDesc;
            this._colorcode = pColorCode;
        }

        public EntityTypeStatusOptionsForFilter(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public string StatusOptions
        {
            get { return _statusoptions; }
            set
            {
                _statusoptions = value;
            }

        }
        public bool IsDeleted
        {
            get { return _isdeleted; }
            set
            {
                _isdeleted = value;
            }

        }
        public string ShortDesc
        {
            get { return _shortdesc; }
            set
            {
                _shortdesc = value;
            }

        }
        public string ColorCode
        {
            get { return _colorcode; }
            set
            {
                _colorcode = value;
            }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeStatusOptionsForFilter castObj = null;
            try
            {
                castObj = (EntityTypeStatusOptionsForFilter)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
