using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

	/// <summary>
	/// GlobalRole object for table 'AM_GlobalRole'.
	/// </summary>
	internal class GlobalRoleUser : IGlobalRoleUser
	{
		#region Member Variables

        protected int _globalroleid;
        protected int _userid;
        protected int _id;
		
		#endregion
		
		#region Constructors
		public GlobalRoleUser() {}

        public GlobalRoleUser(int pId, int pglobalroleid, int puserid)
		{
            this._id = pId;
            this._globalroleid = pglobalroleid;
            this._userid = puserid; 
		}

        //public GlobalRoleUser(int pId)
        //{
        //    this._id = pId; 
        //}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }
			
        }

        public int GlobalRoleId
		{
            get { return _globalroleid; }
            set { _globalroleid = value; }
			
		}

        public int UserId
		{
            get { return _userid; }
			set 
			{
			 
              _userid = value; 
			}
			
		}
		

		#endregion 
		
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            GlobalRoleUser castObj = null;
            try
            {
                castObj = (GlobalRoleUser)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
		
	}
	
}
