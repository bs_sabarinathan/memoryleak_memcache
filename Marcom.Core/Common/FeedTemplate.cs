using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// FeedTemplate object for table 'CM_Feed_Template'.
	/// </summary>
	
	internal class FeedTemplate : IFeedTemplate 
	{
		#region Member Variables

		protected int _id;
		protected int _moduleid;
		protected int _featureid;
		protected string _template;
        protected string _action;
		
		#endregion
		
		#region Constructors
		public FeedTemplate() {}
			
		public FeedTemplate(int pModuleid, int pFeatureid, string pTemplate,string pAction)
		{
			this._moduleid = pModuleid; 
			this._featureid = pFeatureid; 
			this._template = pTemplate;
            this._action = pAction; 
		}
				
		public FeedTemplate(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Moduleid
		{
			get { return _moduleid; }
			set { _moduleid = value; }
			
		}
		
		public int Featureid
		{
			get { return _featureid; }
			set { _featureid = value; }
			
		}
		
		public string Template
		{
			get { return _template; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Template", "Template value, cannot contain more than 1073741823 characters");
			  _template = value; 
			}
			
		}

        public string Action
        {
            get { return _action; }
            set
            {
                _action = value;
            }

        }
		
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FeedTemplate castObj = null;
			try
			{
				castObj = (FeedTemplate)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
