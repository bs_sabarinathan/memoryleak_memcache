using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

	/// <summary>
	/// EntityRoleUserDao object for table 'AM_Entity_Role_User'.
	/// </summary>
	public partial class EntityRoleUserDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _entityid;
		protected int _roleid;
		protected int _userid;
		protected bool _isinherited;
		protected int _inheritedfromentityid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    EntityRoleUserDao toCompare = obj as EntityRoleUserDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Entityid, toCompare.Entityid))
        //        return false;
        //    if (!Object.Equals(this.Roleid, toCompare.Roleid))
        //        return false;
        //    if (!Object.Equals(this.Userid, toCompare.Userid))
        //        return false;           

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Entityid.GetHashCode();
        //    hashCode = (hashCode * 7) + Roleid.GetHashCode();
        //    hashCode = (hashCode * 7) + Userid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public EntityRoleUserDao() {}
			
		public EntityRoleUserDao(int pId,int pEntityid, int pRoleid, int pUserid, bool pIsInherited, int pInheritedFromEntityid)
		{
            this._id = pId;
			this._entityid = pEntityid; 
			this._roleid = pRoleid; 
			this._userid = pUserid; 
			this._isinherited = pIsInherited; 
			this._inheritedfromentityid = pInheritedFromEntityid; 
		}
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual int Roleid
		{
			get { return _roleid; }
			set { _bIsChanged |= (_roleid != value); _roleid = value; }
			
		}
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		public virtual bool IsInherited
		{
			get { return _isinherited; }
			set { _bIsChanged |= (_isinherited != value); _isinherited = value; }
			
		}
		
		public virtual int InheritedFromEntityid
		{
			get { return _inheritedfromentityid; }
			set { _bIsChanged |= (_inheritedfromentityid != value); _inheritedfromentityid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";	
			public const string EntityRoleUserDao = "EntityRoleUserDao";			
			public const string Entityid = "Entityid";			
			public const string Roleid = "Roleid";			
			public const string Userid = "Userid";			
			public const string IsInherited = "IsInherited";			
			public const string InheritedFromEntityid = "InheritedFromEntityid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";	
			public const string EntityRoleUserDao = "EntityRoleUserDao";			
			public const string Entityid = "Entityid";			
			public const string Roleid = "Roleid";			
			public const string Userid = "Userid";			
			public const string IsInherited = "IsInherited";			
			public const string InheritedFromEntityid = "InheritedFromEntityid";			
		}

		#endregion
		
		
	}
	
}
