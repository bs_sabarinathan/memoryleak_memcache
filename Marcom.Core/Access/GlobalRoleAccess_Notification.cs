﻿using BrandSystems.Marcom.Core.Access.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access
{
    public class GlobalRoleAccess_Notification : IGlobalRoleAccess_Notification
    {
        #region Member Variables

        protected int _id;
        protected int _globalroleid;
        protected int _notificationtempid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public GlobalRoleAccess_Notification() { }

        public GlobalRoleAccess_Notification(int ID, int GlobalRoleID, int NotificationTempID)
        {
            this._id = ID;
            this._globalroleid = GlobalRoleID;
            this._notificationtempid = NotificationTempID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }

        }

        public virtual int NotificationTempID
        {
            get { return _notificationtempid; }
            set { _bIsChanged |= (_notificationtempid != value); _notificationtempid = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion


        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            GlobalAcl castObj = null;
            try
            {
                castObj = (GlobalAcl)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
