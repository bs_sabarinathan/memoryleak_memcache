//app.config(function (cfpLoadingBarProvider) {
//    cfpLoadingBarProvider.includeSpinner = true;
//    cfpLoadingBarProvider.latencyThreshold = 200;
//})
app.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            var attr = elem.attr('data-toggle');
            if (typeof attr !== 'undefined' && attr !== false) {
                elem.on('click', function (e) {
                    e.preventDefault();
                });
            }
        }
    };
});
$.ajaxSetup({
    beforeSend: function (xhr) {
        xhr.setRequestHeader('sessioncookie', $.cookie('Session'));
    }
});
app.config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;
}]);

//app.config(['$translateProvider', function ($translateProvider) {
//    $translateProvider.useStaticFilesLoader({
//        prefix: 'Tenants/BSI_Local/l10n/',
//        suffix: '.js'
//    });
//    $translateProvider.preferredLanguage('La_1');
//    $translateProvider.useLocalStorage();
//}]);

var GlobalIsWorkspaces = false;
var NewsFeedUniqueTimer = null;
var NewsFeedUniqueTimerForTask = null;
var AdditionalSettingsObj = [];
var RememberPlanFilterID = 0;
var RememberPlanFilterAttributes = [];
var RemeberPlanFilterName = "No filter applied";
var RememberCCFilterID = 0;
var RememberCCFilterAttributes = [];
var RemeberCCFilterName = "No filter applied";
var RememberObjFilterID = 0;
var RememberObjFilterAttributes = [];
var RemeberObjFilterName = "No filter applied";
var PreviewGeneratorTimer = null;
var EditPreviewGeneratorTimer = null;
var GlobalAssetEditID = 0;
var TenantFilePath = "";
var TenantID = 0;
var ReportCurrency = {
    CurrencyRate: 0,
    Name: ''
};
var appRecacheLastTime = Date.create().format('{12hr}:{mm}:{ss}');
var amazonURL = "";
var tempLightboxguid = [];



$.fn.editable.defaults.mode = 'Popup';

var data;
var IsGlobalAdmin = false;
var GlobalUserDateFormat = '';


$.ajax({
    type: "GET",
    url: "api/Access/CheckUserIsAdmin",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != false) {
            IsGlobalAdmin = true;
        }
    },
    error: function (data) { }
});
var Notifystatementsdisplaytime = '3';
$.ajax({
    type: "GET",
    url: "api/common/Getnotifytimesetbyuser",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != "false") {
            Notifystatementsdisplaytime = tempdata.Response;
        }
    },
    error: function (data) { }
});
var Notifycyclestatementsdisplaytime = 30000;
$.ajax({
    type: "GET",
    url: "api/common/Getnotifyrecycletimesetbyuser",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != false) {
            Notifycyclestatementsdisplaytime = parseInt(tempdata.Response);
        }
    },
    error: function (data) { }
});
var UserTime;

function NotifySuccess(msg) {
    $('.top-right').notify({
        message: {
            text: msg
        },
        type: 'success',
        fadeOut: {
            enabled: true,
            delay: parseInt(Notifystatementsdisplaytime) * 1000
        }
    }).show();
}

function NotifyError(msg) {
    $('.top-right').notify({
        message: {
            text: msg
        },
        type: 'danger',
        fadeOut: {
            enabled: true,
            delay: parseInt(Notifystatementsdisplaytime) * 1000
        }
    }).show();
}
var IsLock = false;
var SearchEntityID = 0;
var EntityTypeID = 0;
var SearchEntityLevel = "0";
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
Number.prototype.formatMoneyObj = function (c, d, t) {
    var n = this,
		c = isNaN(c) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = n = Math.abs(+n || 0).toFixed(c) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
String.prototype.contains = function (chr) {
    var str = this;
    if (str.indexOf(chr) == -1) return false;
    else return true;
};
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (needle) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}
Array.prototype.contains = function (elem) {
    for (var i in this) {
        if (this[i] == elem) return true;
    }
    return false;
}
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) === 0;
    };
}
var SystemDefiendAttributes = {
    FiscalYear: 1,
    Name: 68,
    Owner: 69,
    MilestoneStatus: 67,
    MilestoneEntityID: 66,
    ObjectiveType: 107,
    AssignedAmount: 59,
    Status: 70,
    EntityStatus: 71,
    ApproveTime: 73,
    ObjectiveStatus: 108,
    MyRoleGlobalAccess: 74,
    MyRoleEntityAccess: 75,
    EntityOnTimeStatus: 77,
    TakeDescription: 83,
    TaskNotes: 81,
    TaskDueDate: 84,
    ParentEntityName: -10,
    Description: 3,
    TagOption: 87,
    SubAssignedAmount: 88,
    AvailableAssignedAmount: 89,
    CreationDate: 90
}
var FinancialTransactionType = {
    Planned: 1,
    ApprovedAllocation: 2,
    Commited: 3,
    Spent: 4,
    ForeCasting: 5,
    TotalAssignedAmount: 6,
    SubAssignedAmount: 7,
    AvailableAssignAmount: 8,
    RequestedAmount: 9
};
var taskType = {
    Work_Task: 2,
    Approval_Task: 3,
    Reviewal_Task: 31,
    Asset_Approval_Task: 32,
    Proof_approval_task: 36,
    dalim_approval_task: 37
};
var clientFileStoragetype = {
    local: 0,
    Amazon: 1
};
var finAttributesEnum = (function () {
    var finAttributesEnum = {
        1: "Planned",
        2: "In requests",
        3: "Approved allocations",
        4: "Name"
    };
    return {
        get: function (value) {
            return finAttributesEnum[value];
        }
    }
})();
var finPurchaseOrderAttributesEnum = (function () {
    var finPurchaseOrderAttributesEnum = {
        5: {
            Name: "PONumber",
            Language: "Res_1769"
        },
        6: {
            Name: "strAmount",
            Language: "Res_1746"
        },
        7: {
            Name: "strCreatedDate",
            Language: "Res_31"
        },
        8: {
            Name: "SupplierName",
            Language: "Res_22"
        },
        9: {
            Name: "Description",
            Language: "Res_1736"
        },
        10: {
            Name: "RequesterName",
            Language: "Res_1770"
        },
        11: {
            Name: "strAppprovalDate",
            Language: "Res_1771"
        },
        12: {
            Name: "strSendDate",
            Language: "Res_1772"
        },
        13: {
            Name: "ApproverName",
            Language: "Res_1770"
        }
    };
    return {
        get: function (value) {
            return finPurchaseOrderAttributesEnum[value];
        }
    }
})();
var finSpentAttributesEnum = (function () {
    var finSpentAttributesEnum = {
        15: {
            Name: "InvoiceNumber",
            Language: "Res_1769"
        },
        16: {
            Name: "strCreatedDate",
            Language: "Res_1746"
        },
        17: {
            Name: "POID",
            Language: "Res_31"
        },
        18: {
            Name: "SupplierName",
            Language: "Res_22"
        },
        19: {
            Name: "Description",
            Language: "Res_1736"
        },
        20: {
            Name: "Amount",
            Language: "Res_1736"
        }
    };
    return {
        get: function (value) {
            return finSpentAttributesEnum[value];
        }
    }
})();
var assetAction = (function () {
    var assetAction = {
        1: "AddtoLightBox",
        2: "Task",
        3: "ApprovalTask",
        4: "PublishDAM",
        5: "DuplicateAsset",
        6: "Delete",
        7: "Download",
        8: "Cut",
        9: "Copy",
        10: "ViewLightBox",
        11: "DownloadAll",
        12: "UnpublishAssets",
        13: "Reformat"
    };
    return {
        get: function (value) {
            return assetAction[value];
        }
    }
})();
var taskTypes = (function () {
    var taskTypes = {
        2: "Work Task",
        3: "Approval Task",
        31: "Reviewal Task",
        32: "Asset Approval Task",
        36: "Proof approval task"
    };
    return {
        get: function (value) {
            return taskTypes[value];
        }
    }
})();
var TaskStatus = (function () {
    var TaskStatus = {
        0: "Unassigned",
        1: "In progress",
        2: "Completed",
        3: "Approved",
        4: "Unable to complete",
        5: "Rejected",
        6: "Withdrawn",
        7: "Not Applicable",
        8: "ForcefulComplete",
        9: "RevokeTask"
    };
    return {
        get: function (value) {
            return TaskStatus[value];
        }
    }
})();
var TaskStatusEnum = {
    "Unassigned": 0,
    "In progress": 1,
    "Completed": 2,
    "Approved": 3,
    "Unable to complete": 4,
    "Rejected": 5,
    "Withdrawn": 6,
    "Not Applicable": 7,
    "ForcefulComplete": 8,
    "RevokeTask": 9
};
var taskTypesEnum = {
    "Work Task": 2,
    "Approval Task": 3,
    "Reviewal Task": 31,
    "Asset Approval Task": 32,
    "Proof approval task": 36
};

function generateUniqueTracker() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};
var SystemDefiendFinancialMetadata = {
    FundingCostcenter: 3,
    PO: 1,
    Spent: 2
}
var SystemDefinedEntityTypes = {
    Milestone: 1,
    Objective: 10,
    CostCentre: 5,
    Plan: 6,
    AdditionalObjective: 11,
    Task: 30,
    FundingRequest: 7,
    Calendar: 35
}
var AttributeTypes = {
    Owner: 3,
    DateTime: 5,
    ObjectiveType: 4,
    Period: 10
}
var ShowAllRemember = {
    Activity: true,
    CostCentre: true,
    Objective: true
}
var ForecstDivisonIds = {
    Yearly: 1,
    Monthly: 2,
    Quaterly: 3,
    Half_yearly: 4
}
var EntityRoles = {
    Owner: 1,
    Editer: 2,
    Viewer: 3,
    ExternalParticipate: 5,
    BudgerApprover: 8,
    ProofInitiator: 9,
    Corporate: 10
}
var ForecstBasisIds = {
    Planned_Budget: 1,
    Approved_alloc: 2,
    Non_res_appr_alloc: 3
}
var Defaultdate = '';

function Mimer(Extenstion) {
    switch (Extenstion) {
        case "gif":
        case "png":
        case "jpeg":
        case "bmp":
        case "tiff":
        case "emf":
        case "eps":
        case "jpg":
        case "psd":
        case "tif":
            return "Image"
            break;
        case "pdf":
        case "doc":
        case "docx":
        case "ppt":
        case "pptx":
        case "xls":
        case "xlsx":
        case "txt":
        case "xml":
            return "Document"
            break;
        case "aac":
        case "mka":
        case "mp3":
        case "ogg":
        case "wav":
            return "Audio"
            break;
        case "mov":
        case "wmv":
        case "mp4":
        case "flv":
        case "webm":
        case "avi":
        case "mkv":
        case "mkv":
        case "m4v":
            return "Video"
            break;
        case "zip":
        case "rar":
        case "7z":
            return "Zip"
            break;
        default:
            return "Other";
    }
}
var _systemMimeTypes = {
    "ai": "application/postscript",
    "aif": "audio/x-aiff",
    "aifc": "audio/x-aiff",
    "aiff": "audio/x-aiff",
    "asc": "text/plain",
    "atom": "application/atom+xml",
    "au": "audio/basic",
    "avi": "video/x-msvideo",
    "bcpio": "application/x-bcpio",
    "bin": "application/octet-stream",
    "bmp": "image/bmp",
    "cdf": "application/x-netcdf",
    "cgm": "image/cgm",
    "class": "application/octet-stream",
    "cpio": "application/x-cpio",
    "cpt": "application/mac-compactpro",
    "csh": "application/x-csh",
    "css": "text/css",
    "dcr": "application/x-director",
    "dif": "video/x-dv",
    "dir": "application/x-director",
    "djv": "image/vnd.djvu",
    "djvu": "image/vnd.djvu",
    "dll": "application/octet-stream",
    "dmg": "application/octet-stream",
    "dms": "application/octet-stream",
    "doc": "application/msword",
    "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "dotx": "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
    "docm": "application/vnd.ms-word.document.macroEnabled.12",
    "dotm": "application/vnd.ms-word.template.macroEnabled.12",
    "dtd": "application/xml-dtd",
    "dv": "video/x-dv",
    "dvi": "application/x-dvi",
    "dxr": "application/x-director",
    "eps": "application/postscript",
    "etx": "text/x-setext",
    "exe": "application/octet-stream",
    "ez": "application/andrew-inset",
    "gif": "image/gif",
    "gram": "application/srgs",
    "grxml": "application/srgs+xml",
    "gtar": "application/x-gtar",
    "hdf": "application/x-hdf",
    "hqx": "application/mac-binhex40",
    "htm": "text/html",
    "html": "text/html",
    "ice": "x-conference/x-cooltalk",
    "ico": "image/x-icon",
    "ics": "text/calendar",
    "ief": "image/ief",
    "ifb": "text/calendar",
    "iges": "model/iges",
    "igs": "model/iges",
    "jnlp": "application/x-java-jnlp-file",
    "jp2": "image/jp2",
    "jpe": "image/jpeg",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "js": "application/x-javascript",
    "kar": "audio/midi",
    "latex": "application/x-latex",
    "lha": "application/octet-stream",
    "lzh": "application/octet-stream",
    "m3u": "audio/x-mpegurl",
    "m4a": "audio/mp4a-latm",
    "m4b": "audio/mp4a-latm",
    "m4p": "audio/mp4a-latm",
    "m4u": "video/vnd.mpegurl",
    "m4v": "video/x-m4v",
    "mac": "image/x-macpaint",
    "man": "application/x-troff-man",
    "mathml": "application/mathml+xml",
    "me": "application/x-troff-me",
    "mesh": "model/mesh",
    "mid": "audio/midi",
    "midi": "audio/midi",
    "mif": "application/vnd.mif",
    "mov": "video/quicktime",
    "movie": "video/x-sgi-movie",
    "mp2": "audio/mpeg",
    "mp3": "audio/mpeg",
    "mp4": "video/mp4",
    "mpe": "video/mpeg",
    "mpeg": "video/mpeg",
    "mpg": "video/mpeg",
    "mpga": "audio/mpeg",
    "ms": "application/x-troff-ms",
    "msh": "model/mesh",
    "mxu": "video/vnd.mpegurl",
    "nc": "application/x-netcdf",
    "oda": "application/oda",
    "ogg": "application/ogg",
    "pbm": "image/x-portable-bitmap",
    "pct": "image/pict",
    "pdb": "chemical/x-pdb",
    "pdf": "application/pdf",
    "pgm": "image/x-portable-graymap",
    "pgn": "application/x-chess-pgn",
    "pic": "image/pict",
    "pict": "image/pict",
    "png": "image/png",
    "pnm": "image/x-portable-anymap",
    "pnt": "image/x-macpaint",
    "pntg": "image/x-macpaint",
    "ppm": "image/x-portable-pixmap",
    "ppt": "application/vnd.ms-powerpoint",
    "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "potx": "application/vnd.openxmlformats-officedocument.presentationml.template",
    "ppsx": "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
    "ppam": "application/vnd.ms-powerpoint.addin.macroEnabled.12",
    "pptm": "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
    "potm": "application/vnd.ms-powerpoint.template.macroEnabled.12",
    "ppsm": "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
    "ps": "application/postscript",
    "qt": "video/quicktime",
    "qti": "image/x-quicktime",
    "qtif": "image/x-quicktime",
    "ra": "audio/x-pn-realaudio",
    "ram": "audio/x-pn-realaudio",
    "ras": "image/x-cmu-raster",
    "rdf": "application/rdf+xml",
    "rgb": "image/x-rgb",
    "rm": "application/vnd.rn-realmedia",
    "roff": "application/x-troff",
    "rtf": "text/rtf",
    "rtx": "text/richtext",
    "sgm": "text/sgml",
    "sgml": "text/sgml",
    "sh": "application/x-sh",
    "shar": "application/x-shar",
    "silo": "model/mesh",
    "sit": "application/x-stuffit",
    "skd": "application/x-koan",
    "skm": "application/x-koan",
    "skp": "application/x-koan",
    "skt": "application/x-koan",
    "smi": "application/smil",
    "smil": "application/smil",
    "snd": "audio/basic",
    "so": "application/octet-stream",
    "spl": "application/x-futuresplash",
    "src": "application/x-wais-source",
    "sv4cpio": "application/x-sv4cpio",
    "sv4crc": "application/x-sv4crc",
    "svg": "image/svg+xml",
    "swf": "application/x-shockwave-flash",
    "t": "application/x-troff",
    "tar": "application/x-tar",
    "tcl": "application/x-tcl",
    "tex": "application/x-tex",
    "texi": "application/x-texinfo",
    "texinfo": "application/x-texinfo",
    "tif": "image/tiff",
    "tiff": "image/tiff",
    "tr": "application/x-troff",
    "tsv": "text/tab-separated-values",
    "txt": "text/plain",
    "ustar": "application/x-ustar",
    "vcd": "application/x-cdlink",
    "vrml": "model/vrml",
    "vxml": "application/voicexml+xml",
    "wav": "audio/x-wav",
    "wbmp": "image/vnd.wap.wbmp",
    "wbmxl": "application/vnd.wap.wbxml",
    "wml": "text/vnd.wap.wml",
    "wmlc": "application/vnd.wap.wmlc",
    "wmls": "text/vnd.wap.wmlscript",
    "wmlsc": "application/vnd.wap.wmlscriptc",
    "wrl": "model/vrml",
    "xbm": "image/x-xbitmap",
    "xht": "application/xhtml+xml",
    "xhtml": "application/xhtml+xml",
    "xls": "application/vnd.ms-excel",
    "xml": "application/xml",
    "xpm": "image/x-xpixmap",
    "xsl": "application/xml",
    "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "xltx": "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
    "xlsm": "application/vnd.ms-excel.sheet.macroEnabled.12",
    "xltm": "application/vnd.ms-excel.template.macroEnabled.12",
    "xlam": "application/vnd.ms-excel.addin.macroEnabled.12",
    "xlsb": "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
    "xslt": "application/xslt+xml",
    "xul": "application/vnd.mozilla.xul+xml",
    "xwd": "image/x-xwindowdump",
    "xyz": "chemical/x-xyz",
    "zip": "application/zip"
}

function GetMIMEType(fileName) {
    if (fileName != null) {
        var fileext = fileName.split('.').pop();
        var extenstion = fileext.toLowerCase();
        if (extenstion.length > 0 && _systemMimeTypes[extenstion] != undefined) {
            return _systemMimeTypes[extenstion];
        }
        return "application/unknown";
    } else return "application/unknown";
}

function returnformatstring(format) {
    var replacematter = "",
        formatparts = [],
        formatpattern = "";
    if (format.contains('/')) formatparts = format.split('/');
    else formatparts = format.split('-');
    for (var p = 0; p <= formatparts.length - 1; p++) {
        formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
    }
    replacematter = formatparts.join("-");
    return replacematter;
}

function formatteddateFormat(date, format) {
    if (format.toString() == "MM-dd-yyyy" || format.toString() == "MM/dd/yyyy" || format.toString() == "M-d-yyyy" || format.toString() == "M/d/yyyy") {
        date = new Date.create(date);
    } else {
        format = format.toUpperCase();
        var dateSample = date;
        var replacematter = "",
            formatparts = [],
            formatpattern = "";
        if (format.contains('/')) {
            formatparts = format.split('/');
            formatpattern = "/";
        } else {
            formatparts = format.split('-');
            formatpattern = "-";
        }
        for (var p = 0; p <= formatparts.length - 1; p++) {
            formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
        }
        replacematter = formatparts.join(formatpattern);
        date = new Date.create(date, replacematter);
    }
    return date;
}

function dateFormat(date, format) {
    if (format == undefined) {
        format = GlobalUserDateFormat;
    }
    format = format.toUpperCase();
    var dateSample = date;
    var replacematter = "",
        formatparts = [];
    if (format.contains('/')) formatparts = format.split('/');
    else formatparts = format.split('-');
    for (var p = 0; p <= formatparts.length - 1; p++) {
        formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
    }
    replacematter = formatparts.join("-");
    date = new Date.create(date);
    if (date.getDate() == "1" && date.getMonth() == "0" && date.getFullYear() == '1970') {
        date = new Date(dateSample);
    }
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate());
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1));
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

function ConvertStringToDate(date) {
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    return date = new Date.create(Date.create((parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).toString()).utc(true))
}

function ConvertStringToDateObj(date) {
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    return date = new Date.create(Date.create((parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2]))))
}

function ConvertStringToDateByFormat(date, inputformat) {
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = inputformat.toUpperCase();
    if (defaultFormat.startsWith('D')) {
        date = new Date.create(Date.create(parseInt(dateVal[2]), parseInt(dateVal[1] - 1), parseInt(dateVal[0])).utc(true));
    } else if (defaultFormat.startsWith('M')) {
        date = new Date.create(Date.create(parseInt(dateVal[2]), parseInt(dateVal[0] - 1), parseInt(dateVal[1])).utc(true));
    } else {
        date = new Date.create(Date.create(parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).utc(true));
    }
    return date;
}

function ConvertDateToString(date) {
    if (typeof date == "string") date = new Date.create(date);
    return (date.getFullYear() + "-" + ((date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)) + "-" + ((date.getDate() < 10 ? '0' : '') + date.getDate()));
}

function ConvertDateFromStringToString(date) {
    var dateParts;
    if (date.contains('/')) dateParts = date.split('/');
    else dateParts = date.split('-');
    var dateObj = new Date.create(Date.create(parseInt(dateParts[0]), parseInt(dateParts[1] - 1), parseInt(dateParts[2])).utc(true));
    var format = GlobalUserDateFormat;
    format = format.replace("DD", (dateObj.getDate() < 10 ? '0' : '') + dateObj.getDate());
    format = format.replace("D", (dateObj.getDate() < 10 ? '' : '') + dateObj.getDate());
    format = format.replace("MM", (dateObj.getMonth() < 9 ? '0' : '') + (dateObj.getMonth() + 1));
    format = format.replace("M", (dateObj.getMonth() < 9 ? '' : '') + (dateObj.getMonth() + 1));
    format = format.replace("YYYY", dateObj.getFullYear());
    return format;
}

function ConvertDateFromStringToStringByFormat(date) {
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = GlobalUserDateFormat;
    if (defaultFormat.startsWith('D')) {
        dateSample = dateVal[2] + "-" + dateVal[1] + "-" + dateVal[0];
    } else if (defaultFormat.startsWith('M')) {
        dateSample = dateVal[2] + "-" + dateVal[0] + "-" + dateVal[1];
    } else {
        dateSample = dateVal[0] + "-" + dateVal[1] + "-" + dateVal[2];
    }
    return dateSample;
}

function FormatStandard(date, format) {
    format = format.toUpperCase();
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    date = new Date.create(Date.create(parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).utc(true))
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate());
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1));
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

function ReturnFormattedTodayDate() {
    var today = Date.create(new Date())
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = yyyy + "-" + mm + "-" + dd;
    return today;
}

function dstrToUTC(ds) {
    var dsarr = ds.split("-");
    var mm = parseInt(dsarr[1], 10);
    var dd = parseInt(dsarr[2], 10);
    var yy = parseInt(dsarr[0], 10);
    var dt = new Date('' + yy.toString() + '-' + mm.toString() + '-' + dd.toString() + '');
    return dt;
}

function datediff(ds1, ds2) {
    var date1 = new Date.create(ds1);
    var date2 = new Date.create(ds2);
    var timeDiff = Math.floor(date1.getTime() - date2.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
}

function GetCurrentCacheTime() {
    return Date.create().format('{12hr}:{mm}:{ss}');
}

function TrackTimeDifference(currentCacheTime, lastcachetime) {
    var srchms = currentCacheTime,
        trgthms = lastcachetime;
    var a = srchms.split(':'),
        b = lastcachetime.split(':');
    var srcseconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]),
        trgtseconds = (+b[0]) * 60 * 60 + (+b[1]) * 60 + (+b[2]);
    var mdiff = Math.floor((srcseconds - trgtseconds) / 60);
    return mdiff;
}

function CreateHisory(IDList) {
    var trackValue = IDList[0];
    for (var ti = 1; ti < IDList.length; ti++) {
        trackValue += ',' + IDList[ti];
    }
    $.cookie('ListofEntityID', IDList);
    var Guid = GenerateGUID();
    InsertGUID(Guid, trackValue);
    return Guid;
}

function GenerateGUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};

function InsertGUID(Guid, Value) {
    var ApplicationUrlTrack = {
        "TrackID": "" + Guid + "",
        "TrackValue": "" + Value + ""
    };
    var TrackData = JSON.stringify(ApplicationUrlTrack);
    $.ajax({
        type: "POST",
        url: "api/Common/InsertUpdateApplicationUrlTrack",
        contentType: "application/json",
        data: TrackData,
        async: true,
        success: function (tempdata) {
            if (tempdata.Response != false) { }
        },
        error: function (data) { }
    });
}

function GetGuidValue(trackid) {
    var guid = "";
    $.ajax({
        type: "GET",
        url: "api/Common/GetApplicationUrlTrackByID/" + trackid,
        contentType: "application/json",
        data: {
            TrackID: trackid
        },
        async: false,
        success: function (tempdata) {
            if (tempdata.Response != null) {
                guid = tempdata.Response;
            }
        },
        error: function (data) { }
    });
    return guid;
}
$(document).on("change", "#EntityMetadata select", function () {
    var ActualAttributeID = $(this).attr('id');
    if (ActualAttributeID != undefined) {
        var AttrID = $(this).attr('id').replace('ListSingleSelection_', '').replace('DropDownTree_', '');
        var OptionVal = $('option[value="' + $(this).val() + '"]', this).text().toString().trim();
    }
});


function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
$(document).on("mouseover", "#separator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#separator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                if (event.clientX > 599) {
                    $('#content').css('margin-left', 600 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 600 + 'px');
                } else if (event.clientX < 229) {
                    $('#content').css('margin-left', 230 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 230 + 'px');
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-left')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    } else {
                        $('#content').css('margin-left', 62 + 'px');
                        $('#separator').css('left', '0');
                        $('#left').css('width', 62 + 'px');
                    }
                } else {
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-right')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    }
                    $('#content').css('margin-left', (event.clientX + 1) + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', (event.clientX + 1) + 'px');
                }
            }
        });
    }
});
$(document).on("mouseover", "#attachmentseparator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#attachmentseparator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                var ClientX = event.clientX - ($('#left').width() + 38);
                if (ClientX > 599) {
                    $('#right_DamView').css('margin-left', 610 + 'px');
                    $('#left_folderTree').css('width', 600 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').show();
                    $('.collapsedLeftSection').hide();
                } else if (ClientX < 229) {
                    $('#right_DamView').css('margin-left', 30 + 'px');
                    $('#left_folderTree').css('width', 20 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').hide();
                    $('.collapsedLeftSection').show();
                } else {
                    $('#right_DamView').css('margin-left', (ClientX + 1 + 10) + 'px');
                    $('#left_folderTree').css('width', (ClientX + 1) + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').show();
                    $('.collapsedLeftSection').hide();
                }
            }
        });
    }
});
$(document).on("click", ".collapsedLeftSection", function () {
    $('.expandedLeftSection').show();
    $('.collapsedLeftSection').hide();
    $('#right_DamView').css('margin-left', 240 + 'px');
    $('#left_folderTree').css('width', 230 + 'px');
    $('#attachmentseparator').attr('style', 'right: -1px;');
});

function TreeOpenCloseClick(obj) {
    if ($(obj).find('i').hasClass('icon-chevron-left')) {
        $(obj).attr('data-width', $('#left').width());
        $('#left .ActionBtnArea').next().hide();
        $('#treeSettings').hide();
        $(obj).find('i').removeClass('icon-chevron-left').addClass('icon-chevron-right');
        $('#content').css('margin-left', 62 + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', 62 + 'px');
    } else {
        $('#left .ActionBtnArea').next().show();
        $('#treeSettings').show();
        $(obj).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
        $('#content').css('margin-left', $(obj).attr('data-width') + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', $(obj).attr('data-width') + 'px');
    }
}
$(document).on("click", "#TreeOpenClose", function () {
    TreeOpenCloseClick($(this));
});

function RecursiveUnbindAndRemove($jElement) {
    $jElement.children().each(function () {
        RecursiveUnbindAndRemove($(this));
    });
}

function dealoc(obj) {
    var jqCache = angular.element.cache;
    if (obj) {
        if (angular.isElement(obj)) {
            cleanup(angular.element(obj));
        } else if (!window.jQuery) {
            for (var key in jqCache) {
                var value = jqCache[key];
                if (value.data && value.data.$scope == obj) {
                    delete jqCache[key];
                }
            }
        }
    }

    function cleanup(element) {
        element.off().removeData();
        if (window.jQuery) {
            jQuery.cleanData([element]);
        }
        if (element != null && element != undefined) {
            if (element.length > 0) {
                var children = element[0].childNodes || [];
                for (var i = 0; i < children.length; i++) {
                    cleanup(angular.element(children[i]));
                }
            }
        }
    }
}
window.CreateDOCGeneratedAsset = "", window.CreateversionDOCGeneratedAsset = "";
$.ajax({
    type: "GET",
    url: "api/Common/GetOptimakerAddresspoints",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {
            var strArr = tempdata.Response.split(",");
            if (strArr.length > 0) {
                window.CreateDOCGeneratedAsset = strArr[0];
                window.CreateversionDOCGeneratedAsset = strArr[1];
            }
        }
    },
    error: function (data) { }
});
window.entityId = 0, window.folderid = 0, window.isword = false, window.createdbyid = 0, window.assetId = 0;

function setGlobalOptimakerSettings(eid, fid, isword, createdby) {
    window.entityId = eid, window.folderid = fid, window.isword = isword, window.createdbyid = createdby;
}

function setAssetIdfrGlobalOptimakerSettings(assetid, eid, createdBy) {
    window.assetId = assetid;
    window.entityId = eid;
    window.createdbyid = createdBy;
}
var jsonObject = {
    "Searchterm": "a1"
};
var cloudsetup = {};
$.ajax({
    type: "GET",
    url: "api/dam/S3Settings",
    contentType: "application/json",
    async: false,
    success: function (data) {
        if (data.Response != null && data.Response != "") {
            var res = JSON.parse(data.Response);
            cloudsetup.BucketName = res.BucketName;
            cloudsetup.AWSAccessKeyID = res.AWSAccessKeyID;
            cloudsetup.AWSSecretAccessKey = res.AWSSecretAccessKey;
            cloudsetup.RequestEndpoint = res.RequestEndpoint;
            cloudsetup.PolicyDocument = res.PolicyDocument;
            cloudsetup.PolicyDocumentSignature = res.PolicyDocumentSignature;
            cloudsetup.Uploaderurl = res.UploaderUrl;
            cloudsetup.storageType = res.FileStorageType;

            amazonURL = cloudsetup.Uploaderurl + "/";
        }
    },
    error: function (data) { }
});

angular.module('app').constant('JQ_CONFIG', {
    bootbox: ['assets/js/UtilityFramework/bootbox.js'],
    Plupload: ['assets/js/Plupload/browserplus-min.js', 'assets/js/Plupload/plupload.full.js'],
    videodev: ['assets/js/UtilityFramework/videodev.js', 'assets/css/video-js.css'],
    JqueryCustom: ['assets/js/Jquery/jquery-ui-1.10.3.custom.js'],
    Jqueryappear: ['assets/js/Scroll/jquery.appear.js'],
    AdjustWidth: ['assets/js/InHouse/jquery.AdjustWidth.js'],
    wizard: ['assets/js/Bootstrap/Fuelux/wizard.js'],
    checkbox: ['assets/js/Bootstrap/Fuelux/checkbox.js'],
    Canvas: ['assets/js/CanvasJS/canvasjs.min.js'],
    Html2Canvas: ['assets/js/html2canvas.js'],
    select2: ['assets/js/Select2/select2.js', 'assets/css/select2.css'],
    Pickcolor: ['assets/js/Pick-a-color/pick-a-color-1.1.4.js', 'assets/css/pick-a-color-1.1.4.css'],
    tinycolor: ['assets/js/Pick-a-color/tinycolor-0.9.14.min.js'],
    date: ['assets/js/UtilityFramework/date.js'],
    lodash: ['assets/js/UtilityFramework/lodash.js'],
    placeholders: ['assets/js/UtilityFramework/placeholders.min.js'],
    autoNumeric: ['assets/js/Jquery/Plugin/autoNumeric.js'],
    Redactor: ['assets/js/Redactor/redactor.js', 'assets/css/redactor.css'],
    jquerymask: ['assets/js/Jquery/Plugin/jquery.mask.min.js'],
    bootstrapeditable: ['assets/js/Bootstrap/Xeditable/bootstrap-editable.js', 'assets/css/bootstrap-editable.css'],
    datepicker: ['assets/js/Bootstrap/DatePicker/bootstrap-datepicker.js', 'assets/css/bootstrap-datepicker.css'],
    bootstrapnotify: ['assets/js/Bootstrap/bootstrap-notify.js', 'assets/css/bootstrap-notify.css'],
    scroll: ['assets/js/UtilityFramework/jquery-paged-scroll.js'],
    nod: ['assets/js/Nod/nod.js'],
    tablednd: ['assets/js/Jquery/Plugin/jquery.tablednd.js'],
    tipTip: ['assets/js/Jquery/Plugin/jquery.tipTip.minified.js', 'assets/css/tipTip.css'],
    jqueryQtip: ['assets/js/UtilityFramework/jquery.qtip.js'],
    Jcrop: ['assets/js/Jcrop/jquery.Jcrop.min.js', 'assets/css/jquery.Jcrop.css'],
    Gridster: ['assets/js/Gridster/jquery.gridster.js', 'assets/css/jquery.gridster.css'],
    multiselect: ['assets/js/UtilityFramework/bootstrap-multiselect.js', 'assets/css/bootstrap-multiselect.css', 'assets/css/multiselect.css'],
    multiselectpartial: ['assets/js/Bootstrap/bootstrap-multiselectpartial.js', 'assets/css/bootstrap-multiselect.css', 'assets/css/multiselect.css'],
    passwordstrength: ['assets/js/Jquery/Plugin/jquery.password-strength.js'],
    spectrum: ['assets/js/UtilityFramework/spectrum.js', 'assets/css/spectrum.css'],
    bootstrapbutton: ['assets/js/Bootstrap/bootstrap-button.js'],
    tpls: ['assets/js/Bootstrap/ui-bootstrap-tpls-0.11.0.js'],
    fontawesome: ['assets/css/font-awesome.css'],
    dashboard: ['assets/css/_dashboard.cs'],
    hack: ['assets/css/hack.css'],
    pace: ['assets/css/pace.css'],
    abn_tree: ['assets/css/abn_tree.css'],
    simple: ['assets/css/simple.css'],
    codemirror: ['assets/js/codemirror.js']
}).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: true,
        events: true,
        modules: [{
            name: 'ui.grid',
            files: ['appnew/bower_components/angular-ui-grid/ui-grid.min.js', 'appnew/bower_components/angular-ui-grid/ui-grid.min.css', 'appnew/bower_components/angular-ui-grid/ui-grid.bootstrap.css']
        }, {
            name: 'ui.select',
            files: ['appnew/bower_components/angular-ui-select/dist/select.min.js', 'appnew/bower_components/angular-ui-select/dist/select.min.css']
        }, {
            name: 'toaster',
            files: ['appnew/bower_components/angularjs-toaster/toaster.js', 'appnew/bower_components/angularjs-toaster/toaster.css']
        }, {
            name: 'smart-table',
            files: ['appnew/bower_components/angular-smart-table/dist/smart-table.min.js']
        }, {
            name: 'dndLists',
            files: ['assets/js/Angularjs/angular-drag-and-drop-lists.js']
        }, {
            name: 'ui.bootstrap',
            files: ['assets/js/Bootstrap/ui-bootstrap-tpls-0.11.0.js']
        }, {
            name: 'angucomplete-alt',
            files: ['assets/css/angucomplete-alt.css', 'assets/js/Angularjs/angucomplete-alt.js']
        }, {
            name: 'ngGrid',
            files: ['assets/js/ng-grid/ng-grid.js', 'assets/css/ng-grid.css']
        }, {
            name: 'angular-accordion',
            files: ['assets/js/Angularjs/angular-accordion.js']
        }, {
            name: 'adminalternate-directive',
            files: ['app/directives/adminalternate-directive.js']
        }]
    });
}]);
'use strict';
angular.module('app').run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}]);


$.ajax({
    type: "GET",
    url: "api/Common/GetNavigationandLanguageConfig",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {
            data = tempdata.Response.m_Item1;
            defaultlang = tempdata.Response.m_Item2;
            eval(defaultlang);
            eval(data);
        }
    },
    error: function (data) {
    }
});

$.ajax({
    type: "GET",
    url: "api/Common/GetAdditionalSettings",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {
            Defaultdate = tempdata.Response[0].SettingValue;
            AdditionalSettingsObj = tempdata.Response;
            app.value('$strapConfig', {
                datepicker: {
                    format: Defaultdate
                }
            });
            var objlen = tempdata.Response.length;
            TenantFilePath = tempdata.Response[objlen - 1].SettingValue;
            TenantID = tempdata.Response[objlen - 1].Id;
        }
    },
    error: function (data) { }
});


//    .config(['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', function ($stateProvider, $urlRouterProvider, JQ_CONFIG) {
//    $urlRouterProvider.otherwise('/mui/list');
//    $stateProvider
//         .state('mui', {
//             abstract: true,
//             url: '/mui',
//             templateUrl: 'newviews/app.html',
//             cache: true,
//             resolve: {
//                 deps: ['$ocLazyLoad',
//                 function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/muiJS.js', 'app/services/ServiceJs.js', 'app/directives/multiselect-directives.js']).then(function () {
//                     })
//                 }]
//             }
//         }).state('mui.metadatasettings', {
//             url: '/metadatasettings',
//             templateUrl: 'views/mui/MetadataSettings.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/mui/metadataSettings-controller.js', 'app/services/ServiceJs.js']);
//                 }]
//             }
//         })
//        .state('mui.MetadataConfiguration', {
//            url: '/MetadataConfiguration',
//            templateUrl: 'views/mui/MetadataConfiguration.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/metadataconfiguration-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.entitytypeattributerelation', {
//            url: '/entitytypeattributerelation',
//            templateUrl: 'views/mui/admin/entitytypeattributerelation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/entitytypeattributerelation-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.Damentitytype', {
//            url: '/Damentitytype',
//            templateUrl: 'views/mui/admin/Damentitytype.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/Damentitytype-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.Taskentitytype', {
//            url: '/Taskentitytype',
//            templateUrl: 'views/mui/admin/Taskentitytype.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/taskentitytype-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.cmspagetypeattributerelation', {
//            url: '/cmspagetypeattributerelation',
//            templateUrl: 'views/mui/admin/cmspagetypeattributerelation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/cmspagetypeattributerelation-controller.js', 'app/services/cms-service.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.attributegroup', {
//            url: '/attributegroup',
//            templateUrl: 'views/mui/admin/attributegroup.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/attributegroup-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.attribute', {
//            url: '/MetadataConfiguration/attribute',
//            templateUrl: 'views/mui/admin/attribute.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/attribute-controller.js']);
//                }]
//            }
//        }).state('mui.MetadataConfiguration.userdetails', {
//            url: '/userdetails',
//            templateUrl: 'views/mui/admin/userdetails.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/userdetails-controller.js', 'app/services/user-service.js']);
//                }]
//            }
//        }).state('mui.admin', {
//            url: '/admin',
//            templateUrl: 'views/mui/admin.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin-controller.js', 'app/services/admin-services.js']);
//                }]
//            }
//        })
//        .state('mui.admin.adminsetting', {
//            url: '/adminsetting',
//            templateUrl: 'views/mui/admin/titlelogo.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/titlelogo-controller.js']);
//                }]
//            }
//        })
//        .state('mui.admin.superadmin', {
//            url: '/superadmin',
//            templateUrl: 'views/mui/admin/superadmin.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/superadmin-controller.js']);
//                }]
//            }
//        })


//        .state('mui.admin.titlelogo', {
//            url: '/titlelogo',
//            templateUrl: 'views/mui/admin/titlelogo.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/titlelogo-controller.js']);
//                }]
//            }
//        }).state('mui.admin.navigation', {
//            url: '/navigation',
//            templateUrl: 'views/mui/admin/navigation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/navigation-controller.js']);
//                }]
//            }
//        }).state('mui.admin.additionalsettings', {
//            url: '/additionalsettings',
//            templateUrl: 'views/mui/admin/additionalsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/additionalsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.languagesettings', {
//            url: '/languagesettings',
//            templateUrl: 'views/mui/admin/languagesettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/languagesettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.notificationfiltersettings', {
//            url: '/NotificationFilterSettings',
//            templateUrl: 'views/mui/admin/NotificationFilterSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/notificationfiltersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.newsfeedfiltersettings', {
//            url: '/NewsFeedFilterSettings',
//            templateUrl: 'views/mui/admin/NewsFeedFilterSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/newsfeedfiltersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.searchengine', {
//            url: '/searchengine',
//            templateUrl: 'views/mui/admin/searchengine.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/searchengine-controller.js']);
//                }]
//            }
//        }).state('mui.admin.ganttviewheaderbar', {
//            url: '/ganttviewheaderbar',
//            templateUrl: 'views/mui/admin/ganttviewheaderbar.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewheaderbar-controller.js']);
//                }]
//            }
//        }).state('mui.admin.statistics', {
//            url: '/statistics',
//            templateUrl: 'views/mui/admin/statistics.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/statistics-controller.js']);
//                }]
//            }
//        }).state('mui.admin.broadcastmsg', {
//            url: '/broadcastmsg',
//            templateUrl: 'views/mui/admin/broadcastmsg.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/broadcastmsg-controller.js']);
//                }]
//            }
//        }).state('mui.admin.tabs', {
//            url: '/tabs',
//            templateUrl: 'views/mui/admin/tabs.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/tabs-controller.js']);
//                }]
//            }
//        }).state('mui.admin.updates', {
//            url: '/updates',
//            templateUrl: 'views/mui/admin/updates.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/updates-controller.js']);
//                }]
//            }
//        }).state('mui.admin.passwordpolicy', {
//            url: '/passwordpolicy',
//            templateUrl: 'views/mui/admin/passwordpolicy.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/passwordpolicy-controller.js']);
//                }]
//            }
//        }).state('mui.admin.searchcriteria', {
//            url: '/searchcriteria',
//            templateUrl: 'views/mui/admin/searchcriteria.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/searchcriteria-controller.js']);
//                }]
//            }
//        }).state('mui.admin.businessdays', {
//            url: '/businessdays',
//            templateUrl: 'views/mui/admin/businessdays.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/businessdays-controller.js']);
//                }]
//            }
//        }).state('mui.admin.cloudsettings', {
//            url: '/cloudsettings',
//            templateUrl: 'views/mui/admin/cloudsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/cloudsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.user', {
//            url: '/user',
//            templateUrl: 'views/mui/admin/user.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/user-controller.js']);
//                }]
//            }
//        }).state('mui.admin.apiusers', {
//            url: '/apiusers',
//            templateUrl: 'views/mui/admin/apiusers.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/apiusers-controller.js']);
//                }]
//            }
//        }).state('mui.admin.accountlock', {
//            url: '/accountlock',
//            templateUrl: 'views/mui/admin/accountlock.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/acclock-controller.js']);
//                }]
//            }
//        }).state('mui.admin.role', {
//            url: '/role',
//            templateUrl: 'views/mui/admin/role.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/role-controller.js']);
//                }]
//            }
//        }).state('mui.admin.pendinguser', {
//            url: '/PendingUser',
//            templateUrl: 'views/mui/admin/PendingUser.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/pendinguser-controller.js']);
//                }]
//            }
//        }).state('mui.admin.module', {
//            url: '/module',
//            templateUrl: 'views/mui/admin/module.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/module-controller.js']);
//                }]
//            }
//        }).state('mui.admin.SSO', {
//            url: '/SSO',
//            templateUrl: 'views/mui/admin/SSO.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/SSO-controller.js', 'assets/css/ng-tags-input.min.css', 'appnew/bower_components/bootstrap-tagsinput/src/bootstrap-tagsinput-angular.js', 'appnew/bower_components/bootstrap-tagsinput/src/bootstrap-tagsinput.js', 'appnew/bower_components/bootstrap-tagsinput/src/bootstrap-tagsinput.js']);
//                }]
//            }
//        }).state('mui.admin.assetaccess', {
//            url: '/assetaccess',
//            templateUrl: 'views/mui/admin/assetaccess.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/assetaccess-controller.js']);
//                }]
//            }
//        }).state('mui.admin.approvalrole', {
//            url: '/approvalrole',
//            templateUrl: 'views/mui/admin/approvalrole.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/approvalrole-controller.js']);
//                }]
//            }
//        }).state('mui.admin.dalimuser', {
//            url: '/dalimuser',
//            templateUrl: 'views/mui/admin/DalimUsers.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/DalimUser-controller.js']);
//                }]
//            }
//        }).state('mui.admin.listsettings', {
//            url: '/listsettings',
//            templateUrl: 'views/mui/admin/listsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/listsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.treesettings', {
//            url: '/treesettings',
//            templateUrl: 'views/mui/admin/treesettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/treesettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.ganttviewsettings', {
//            url: '/ganttviewsettings',
//            templateUrl: 'views/mui/admin/ganttviewsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.listviewsettings', {
//            url: '/listviewsettings',
//            templateUrl: 'views/mui/admin/listviewsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/listviewsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.rootlevelfiltersettings', {
//            url: '/rootlevelfiltersettings',
//            templateUrl: 'views/mui/admin/rootlevelfiltersettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/rootlevelfiltersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.detailfiltersettings', {
//            url: '/detailfiltersettings',
//            templateUrl: 'views/mui/admin/detailfiltersettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/detailfiltersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.myworkspacesettings', {
//            url: '/myworkspacesettings',
//            templateUrl: 'views/mui/admin/myworkspacesettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/myworkspacesettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.plantabsettings', {
//            url: '/plantabsettings',
//            templateUrl: 'views/mui/admin/plantabsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/plantabsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.designlayout', {
//            url: '/designlayout',
//            templateUrl: 'views/mui/admin/designlayout.html',
//            resolve: {
//                deps: ['$ocLazyLoad',

//                    function ($ocLazyLoad) {
//                        return $ocLazyLoad.load('dndLists').then(
//                        function () {
//                            return $ocLazyLoad.load(['app/controllers/mui/admin/designlayout-controller.js']);
//                        }
//                    );
//                    }]
//            },
//        }).state('mui.admin.financialforecastsettings', {
//            url: '/financialforecastsettings',
//            templateUrl: 'views/mui/admin/Financialforecastsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/Financialforecastsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.financialforecast', {
//            url: '/financialforecast',
//            templateUrl: 'views/mui/admin/financialforecast.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/financialforecast-controller.js']);
//                }]
//            }
//        }).state('mui.admin.POSettings', {
//            url: '/POSettings',
//            templateUrl: 'views/mui/admin/POSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/purchaseordersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.Currencyconvertersettings', {
//            url: '/Currencyconvertersettings',
//            templateUrl: 'views/mui/admin/Currencyconvertersettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/Currencyconvertersettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.financialmetadata', {
//            url: '/financialmetadata',
//            templateUrl: 'views/mui/admin/Financialmetadata.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js']);
//                }]
//            }
//        }).state('mui.admin.pometadata', {
//            url: '/pometadata',
//            templateUrl: 'views/mui/admin/pometadata.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js']);
//                }]
//            }
//        }).state('mui.admin.spenttransaction', {
//            url: '/spenttransaction',
//            templateUrl: 'views/mui/admin/spenttransaction.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js']);
//                }]
//            }
//        }).state('mui.admin.assetcreation', {
//            url: '/assetcreation',
//            templateUrl: 'views/mui/admin/assetcreationsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/assetcreationsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.damviewsettings', {
//            url: '/damviewsettings',
//            templateUrl: 'views/mui/admin/damviewsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/damviewsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.ReformatProfilesettings', {
//            url: '/ReformatProfilesettings',
//            templateUrl: 'views/mui/admin/ReformatProfilesettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/ReformatProfilesettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.optimaker', {
//            url: '/optimaker',
//            templateUrl: 'views/mui/admin/optimaker.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/optimaker-controller.js']);
//                }]
//            }
//        }).state('mui.admin.assetcategory', {
//            url: '/assetcategory',
//            templateUrl: 'views/mui/admin/assetcategory.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {

//                    return $ocLazyLoad.load(['app/controllers/mui/admin/assetcategory-controller.js']);
//                }]
//            }
//        }).state('mui.admin.assetTypeFileExtension', {
//            url: '/assetTypeFileExtension',
//            templateUrl: 'views/mui/admin/assetTypeFileExtensionRelation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/assetTypeFileExtensionRelation-controller.js']);
//                }]
//            }
//        }).state('mui.admin.assetsnippet', {
//            url: '/assetsnippet',
//            templateUrl: 'views/mui/admin/assetsnippet.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/assetsnippet-controller.js', 'app/services/dam-service.js']);
//                }]
//            }
//        }).state('mui.admin.TemplateEngines', {
//            url: '/TemplateEngines',
//            templateUrl: 'views/mui/admin/TemplateEngines.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/TemplateEngines-controller.js']);
//                }]
//            }
//        }).state('mui.admin.dashboardwidget', {
//            url: '/dashboardwidget',
//            templateUrl: 'views/mui/admin/dashboardwidget.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/dashboardwidget-controller.js']);
//                }]
//            }
//        }).state('mui.admin.dashboardtemplate', {
//            url: '/dashboardtemplate',
//            templateUrl: 'views/mui/admin/dashboardtemplate.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/dashboardtemplate-controller.js', 'app/controllers/mui/dashboard-controller.js']);
//                }]
//            }
//        }).state('mui.admin.Units', {
//            url: '/dalimcredential',
//            templateUrl: 'views/mui/admin/Units.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/Units-controller.js']);
//                }]
//            }
//        }).state('mui.admin.cmstemplate', {
//            url: '/cmstemplate',
//            templateUrl: 'views/mui/admin/cmstemplate.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/cmstemplate-controller.js']);
//                }]
//            }
//        }).state('mui.admin.cmscustomfontsettings', {
//            url: '/cmscustomfontsettings',
//            templateUrl: 'views/mui/admin/CmsCustomFontSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/CmsCustomFontSettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.cmscustomstylesettings', {
//            url: '/cmscustomstylesettings',
//            templateUrl: 'views/mui/admin/CmsCustomStyleSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/CmsCustomStyleSettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.cmstreestylesettings', {
//            url: '/cmstreestylesettings',
//            templateUrl: 'views/mui/admin/CmsTreeStyleSettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/CmsTreeStyleSettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.credentialmanagement', {
//            url: '/credentialmanagement',
//            templateUrl: 'views/mui/admin/credentialmanagement.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/credentialmanagement-controller.js']);
//                }]
//            }
//        }).state('mui.admin.reports', {
//            url: '/reports',
//            templateUrl: 'views/mui/admin/reports.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/reports-controller.js']);
//                }]
//            }
//        }).state('mui.admin.customview', {
//            url: '/customview',
//            templateUrl: 'views/mui/admin/customview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/customview-controller.js']);
//                }]
//            }
//        }).state('mui.admin.financialreportsettings', {
//            url: '/financialreportsettings',
//            templateUrl: 'views/mui/admin/financialreportsettings.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/financialreportsettings-controller.js']);
//                }]
//            }
//        }).state('mui.admin.ganttviewreport', {
//            url: '/ganttviewreport',
//            templateUrl: 'views/mui/admin/ganttviewreport.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewreport-controller.js']);
//                }]
//            }
//        }).state('mui.admin.customlist', {
//            url: '/customlist',
//            templateUrl: 'views/mui/admin/customlist.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/customlist-controller.js']);
//                }]
//            }
//        }).state('mui.admin.taskflag', {
//            url: '/taskflag',
//            templateUrl: 'views/mui/admin/taskflag.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/taskflag-controller.js']);
//                }]
//            }
//        }).state('mui.admin.approvaltemplate', {
//            url: '/approvalflowtemplate',
//            templateUrl: 'views/mui/admin/approvalflowtemplate.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/taskPredefinedApproveFlow.js']);
//                }]
//            }
//        }).state('mui.admin.tasklistlibrary', {
//            url: '/tasklistlibrary',
//            templateUrl: 'views/mui/admin/tasklistlibrary.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/tasklistlibrary-controller.js', 'app/services/admintask-service.js']);
//                }]
//            }
//        }).state('mui.admin.tasktemplate', {
//            url: '/tasktemplate',
//            templateUrl: 'views/mui/admin/tasktemplate.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/tasktemplate-controller.js', 'app/services/admintask-service.js']);
//                }]
//            }
//        }).state('mui.mypage', {
//            url: '/mypage',
//            templateUrl: 'views/mui/MyPage.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/Mypage_bundle.js', 'assets/css/jquery.Jcrop.css']);
//                }]
//            }
//        }).state('mui.task', {
//            url: '/task',
//            templateUrl: 'views/mui/mytask.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/Mytask_Js.js', 'assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.mynotification', {
//            url: '/mynotification',
//            templateUrl: 'views/mui/mynotification.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/mynotification-controller.js', 'app/services/cms-service.js', 'app/services/common-services.js']);
//                }]
//            }
//        }).state('mui.myfundingrequest', {
//            url: '/myfundingrequest',
//            templateUrl: 'views/mui/myfundingrequest.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/myfundingrequest-controller.js', 'app/services/task-service.js', 'app/services/common-services.js']);
//                }]
//            }
//        }).state('mui.myworkspace', {
//            url: '/myworkspace',
//            templateUrl: 'views/mui/planningtool/myworkspaces.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/myworkspaces-controller.js']);
//                }]
//            }
//        }).state('mui.myworkspace.list', {
//            url: '/list',
//            templateUrl: 'views/mui/planningtool/workspaces/list.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/workspaces/list-controller.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js']);
//                }]
//            }
//        })
//        .state('mui.dashboard', {
//            url: '/dashboard',
//            templateUrl: 'views/mui/dashboard.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/Dashboard_bundle.js', 'assets/css/jquery.gridster.css', 'assets/css/taskCSS.css']);
//                }]
//            }
//        }).state('mui.mediabank', {
//            url: '/mediabank',
//            templateUrl: 'views/mui/mediabank.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/mediabankJS.js', 'assets/css/damCSS.css']);
//                }]
//            }
//        })
//        .state('mui.marcommediabank', {
//            url: '/marcommediabank',
//            templateUrl: 'views/mui/mediabank.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/mediabankJS.js', 'assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.admin.proofhqcredential', {
//            url: '/proofhqcredential',
//            templateUrl: 'views/mui/admin/proofhqtaskcredential.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/proofhqcredential-controller.js']);
//                }]
//            }
//        }).state('mui.admin.dalimcredential', {
//            url: '/dalimcredential',
//            templateUrl: 'views/mui/admin/dalimtaskcredential.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/admin/dalimcredential-controller.js', 'app/services/task-service.js']);
//                }]
//            }
//        }).state('mui.planningtool', {
//            url: '/planningtool',
//            templateUrl: 'views/mui/planningtool.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool-controller.js']);
//                }]
//            }
//        }).state('mui.planningtool.default', {
//            url: '/default',
//            templateUrl: 'views/mui/planningtool/default.html',
//            resolve: {
//                deps: ['$ocLazyLoad',
//                  function ($ocLazyLoad) {
//                      return $ocLazyLoad.load(['app/controllers/mui/planningtool/default-controller.js']);
//                  }]
//            }
//        }).state('mui.list', {
//            url: '/list',
//            templateUrl: 'api/View/GetListView',
//            resolve: {
//                deps: ['$ocLazyLoad',
//                  function ($ocLazyLoad) {
//                      return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/planningJS.js', 'assets/css/planningCSS.css']);

//                  }]
//            }
//        }).state('mui.planningtool.default.detail', {
//            url: '/detail',
//            templateUrl: 'api/View/GetDetailView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/plandetailJS.js', 'assets/css/plandetailCSS.min.css']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.ganttview', {
//            url: '/ganttview/:TrackID',
//            templateUrl: 'views/mui/planningtool/default/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/planganttviewJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.listview', {
//            url: '/listview/:TrackID',
//            templateUrl: 'views/mui/planningtool/default/detail/listview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/planlistviewJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section', {
//            url: '/section/:ID',
//            templateUrl: 'api/View/GetSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/plansectionJS.js']);
//                }]
//            },
//            parent: 'mui.planningtool.default.detail'
//        }).state('mui.planningtool.default.detail.sectioncustom', {
//            url: '/section',
//            templateUrl: 'api/View/GetSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/plansectionJS.js']);
//                }]
//            },
//            parent: 'mui.planningtool.default.detail'
//        }).state('mui.planningtool.default.detail.section.overview', {
//            url: '/overview',
//            templateUrl: 'api/View/GetOverView',
//            resolve: {
//                deps: ['$ocLazyLoad',
//                  function ($ocLazyLoad) {
//                      return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planoverviewJS.js', 'assets/css/planoverviewCSS.css']);
//                  }]
//            }
//        }).state('mui.planningtool.default.detail.section.financial', {
//            url: '/financial',
//            templateUrl: 'api/View/GetFinancialView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planfinancialJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.workflow', {
//            url: '/workflow',
//            templateUrl: 'views/mui/planningtool/default/detail/section/workflow.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/workflow-controller.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.member', {
//            url: '/member',
//            templateUrl: 'views/mui/planningtool/default/detail/section/member.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/Plan_member_tabJs.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.objective', {
//            url: '/objective',
//            templateUrl: 'api/View/GetObjectiveView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planobjectiveJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.attachment', {
//            url: '/attachment',
//            templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.presentation', {
//            url: '/presentation',
//            templateUrl: 'views/mui/planningtool/default/detail/section/presentation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planpresentationJS.js', 'assets/css/planpresentationCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.default.detail.section.task', {
//            url: '/task',
//            templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('dndLists').then(
//                       function () {
//                           return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                       }
//                    );
//                }]
//            }
//        })
//        .state('mui.planningtool.default.detail.sectioncustom.customtab', {
//            url: '/customtab',
//            templateUrl: 'views/mui/planningtool/default/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.planningtool.default.detail.sectioncustom.customtabtID', {
//            url: '/customtab/:tabID/:ID',
//            templateUrl: 'views/mui/planningtool/default/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.planningtool.default.detail.sectioncustom.customtabID', {
//            url: '/customtab/:tabID',
//            templateUrl: 'views/mui/planningtool/default/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.costcentre', {
//            url: '/costcentre',
//            templateUrl: 'views/mui/planningtool/costcentre.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre-controller.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre', {
//            url: '/costcentre',
//            templateUrl: 'views/mui/planningtool/costcentre.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre-controller.js');
//                }]
//            }
//        }).state('mui.costcentre.list', {
//            url: '/list',
//            templateUrl: 'api/View/GetCostcentreListView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/cclistJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.subcostcentrecreation', {
//            url: '/subcostcentrecreation',
//            templateUrl: 'views/mui/planningtool/costcentre/SubCostCentreCreation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/subcostcentrecreationJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail', {
//            url: '/detail',
//            templateUrl: 'api/View/GetCostcentreDetailView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/ccdetailJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.ganttview', {
//            url: '/ganttview',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/ccganttviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.ganttviewtrack', {
//            url: '/ganttview/:TrackID',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/ccganttviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.ganttviewzoom', {
//            url: '/ganttview/:zoomlevel',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/ccganttviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.ganttviewhybrid', {
//            url: '/ganttview/:zoomlevel/:TrackID',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/ccganttviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.listview', {
//            url: '/listview',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/listview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/cclistviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.listviewTrack', {
//            url: '/listview/:TrackID',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/listview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/cclistviewJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectioncustom', {
//            url: '/section',
//            templateUrl: 'api/View/GetCostcentreSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccsectionJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section', {
//            url: '/section/:ID',
//            templateUrl: 'api/View/GetCostcentreSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccsectionJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity', {
//            url: '/sectionentity/:ID',
//            templateUrl: 'api/View/GetCostcentreSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccsectionJS.js');
//                }]
//            }
//        })
//        .state('mui.planningtool.costcentre.detail.sectionentity.overview', {
//            url: '/overview',
//            templateUrl: 'api/View/GetCostcentreOverView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccoverviewJS.js', 'assets/css/overviewCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.financial', {
//            url: '/financial',
//            templateUrl: 'api/View/GetCCFinancialView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccfinancialJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.member', {
//            url: '/member',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/member.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccmemberJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.task', {
//            url: '/task',
//            templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('assets/css/damCSS.css');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.taskbyID', {
//            url: '/task/:TaskID',
//            templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('assets/css/damCSS.css');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.workflow', {
//            url: '/workflow',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/workflow.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccworkflowJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.attachment', {
//            url: '/attachment',
//            templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.attachmentID', {
//            url: '/attachment/:AssetID',
//            templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectionentity.presentation', {
//            url: '/presentation',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/presentation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccpresentationJS.js', 'assets/css/redactor.css']);
//                }]
//            }
//        })
//        .state('mui.planningtool.costcentre.detail.section.overview', {
//            url: '/overview',
//            templateUrl: 'api/View/GetCostcentreOverView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccoverviewJS.js', 'assets/css/overviewCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.financial', {
//            url: '/financial',
//            templateUrl: 'api/View/GetCCFinancialView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccfinancialJS.js']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.member', {
//            url: '/member',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/member.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccmemberJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.task', {
//            url: '/task',
//            templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('assets/css/damCSS.css');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.taskbyID', {
//            url: '/task/:TaskID',
//            templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('assets/css/damCSS.css');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.workflow', {
//            url: '/workflow',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/workflow.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccworkflowJS.js');
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.attachment', {
//            url: '/attachment',
//            templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.section.presentation', {
//            url: '/presentation',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/presentation.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccpresentationJS.js', 'assets/css/redactor.css']);
//                }]
//            }
//        }).state('mui.planningtool.costcentre.detail.sectioncustom.customtab', {
//            url: '/customtab',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.planningtool.costcentre.detail.sectioncustom.customtabtID', {
//            url: '/customtab/:tabID/:ID',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.planningtool.costcentre.detail.sectioncustom.customtabID', {
//            url: '/customtab/:tabID',
//            templateUrl: 'views/mui/planningtool/costcentre/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.calender', {
//            url: '/calender',
//            templateUrl: 'views/mui/planningtool/calender.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender-controller.js']);
//                }]
//            }
//        }).state('mui.calender.list', {
//            url: '/list',
//            templateUrl: 'api/View/GetMuiListView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/Calenderlist_bundle.js']);
//                }]
//            }
//        }).state('mui.calender.detail', {
//            url: '/detail',
//            templateUrl: 'api/View/GetMuiDetailView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/Calenderdetail_bundle.js']);
//                }]
//            }
//        }).state('mui.calender.detail.ganttview', {
//            url: '/ganttview/:TrackID',
//            templateUrl: 'views/mui/planningtool/calender/detail/ganttview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/Calenderganttview_bundle.js']);
//                }]
//            }
//        }).state('mui.calender.detail.listview', {
//            url: '/listview/:TrackID',
//            templateUrl: 'views/mui/planningtool/calender/detail/listview.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/listview-controller.js']);
//                }]
//            }
//        }).state('mui.calender.detail.section', {
//            url: '/section/:ID',
//            templateUrl: 'api/View/GetMuiSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/Calendersection_bundel.js']);
//                }]
//            }
//        })
//        .state('mui.calender.detail.sectioncustom', {
//            url: '/section',
//            templateUrl: 'api/View/GetMuiSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/Calendersection_bundel.js']);
//                }]
//            }
//        }).state('mui.calender.detail.sectionentity', {
//            url: '/sectionentity/:ID',
//            templateUrl: 'api/View/GetMuiSectionView',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/Calendersection_bundel.js']);
//                }]
//            }
//        })
//         .state('mui.calender.detail.sectionentity.overview', {
//             url: '/overview',
//             templateUrl: 'views/mui/planningtool/calender/detail/section/overview.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calenderoverview_bundle.js']);
//                 }]
//             }
//         }).state('mui.calender.detail.sectionentity.member', {
//             url: '/member',
//             templateUrl: 'views/mui/planningtool/calender/detail/section/member.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calendermember_bundle.js']);
//                 }]
//             }
//         }).state('mui.calender.detail.sectionentity.attachment', {
//             url: '/attachment',
//             templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                 }]
//             }
//         }).state('mui.calender.detail.sectionentity.presentation', {
//             url: '/presentation',
//             templateUrl: 'views/mui/planningtool/calender/detail/section/presentation.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calenderpresentation_bundle.js']);
//                 }]
//             }
//         }).state('mui.calender.detail.sectionentity.task', {
//             url: '/task',
//             templateUrl: 'views/mui/planningtool/default/detail/section/task.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                 }]
//             }
//         }).state('mui.calender.detail.sectioncustom.customtab', {
//             url: '/customtab',
//             templateUrl: 'views/mui/planningtool/calender/detail/section/customtab.html',
//             resolve: {
//                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                 }]
//             }
//         })
//        .state('mui.calender.detail.sectioncustom.customtabtID', {
//            url: '/customtab/:tabID/:ID',
//            templateUrl: 'views/mui/planningtool/calender/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//            .state('mui.calender.detail.section.overview', {
//                url: '/overview',
//                templateUrl: 'views/mui/planningtool/calender/detail/section/overview.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calenderoverview_bundle.js']);
//                    }]
//                }
//            }).state('mui.calender.detail.section.member', {
//                url: '/member',
//                templateUrl: 'views/mui/planningtool/calender/detail/section/member.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calendermember_bundle.js']);
//                    }]
//                }
//            }).state('mui.calender.detail.section.attachment', {
//                url: '/attachment',
//                templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.calender.detail.section.presentation', {
//                url: '/presentation',
//                templateUrl: 'views/mui/planningtool/calender/detail/section/presentation.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/Calenderpresentation_bundle.js']);
//                    }]
//                }
//            }).state('mui.calender.detail.section.task', {
//                url: '/task',
//                templateUrl: 'views/mui/planningtool/default/detail/section/task.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.calender.detail.section.customtab', {
//                url: '/customtab',
//                templateUrl: 'views/mui/planningtool/calender/detail/section/customtab.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/customtab-controller.js']);
//                    }]
//                }
//            }).state('mui.objective', {
//                url: '/objective',
//                templateUrl: 'views/mui/planningtool/objective.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective-controller.js']);
//                    }]
//                }
//            }).state('mui.objective.list', {
//                url: '/list',
//                templateUrl: 'api/View/GetObjectiveListView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/Objectivelist_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail', {
//                url: '/detail',
//                templateUrl: 'api/View/GetObjectiveDetailView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/Objectivedetail_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.ganttview', {
//                url: '/ganttview/:TrackID',
//                templateUrl: 'views/mui/planningtool/objective/detail/ganttview.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/Objectiveganttview_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.listview', {
//                url: '/listview/:TrackID',
//                templateUrl: 'views/mui/planningtool/objective/detail/listview.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/listview-controller.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentitycustom', {
//                url: '/section',
//                templateUrl: 'api/View/GetObjectiveSectionView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/Objectivesection_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.section', {
//                url: '/section/:ID',
//                templateUrl: 'api/View/GetObjectiveSectionView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/Objectivesection_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.overview', {
//                url: '/overview',
//                templateUrl: 'api/View/GetObjectiveOverView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectiveoverview_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.member', {
//                url: '/member',
//                templateUrl: 'views/mui/planningtool/objective/detail/section/member.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectivemember_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.attachment', {
//                url: '/attachment',
//                templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.presentation', {
//                url: '/presentation',
//                templateUrl: 'views/mui/planningtool/objective/detail/section/presentation.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectivepresentation_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.task', {
//                url: '/task',
//                templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.objective.detail.section.customtab', {
//                url: '/customtab',
//                templateUrl: 'api/View/GetObjectiveDetailView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/Objectivedetail_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity', {
//                url: '/sectionentity/:ID',
//                templateUrl: 'api/View/GetObjectiveSectionView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/Objectivesection_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.overview', {
//                url: '/overview',
//                templateUrl: 'api/View/GetObjectiveOverView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectiveoverview_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.financial', {
//                url: '/financial',
//                templateUrl: 'api/View/GetCCFinancialView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/ccfinancialJS.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.workflow', {
//                url: '/workflow',
//                templateUrl: 'views/mui/planningtool/costcentre/detail/section/workflow.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre/detail/section/ccworkflowJS.js');
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.member', {
//                url: '/member',
//                templateUrl: 'views/mui/planningtool/objective/detail/section/member.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectivemember_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.objective', {
//                url: '/objective',
//                templateUrl: 'api/View/GetObjectiveView',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planobjectiveJS.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.attachment', {
//                url: '/attachment',
//                templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.attachmentid', {
//                url: '/attachment/:AssetID',
//                templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.presentation', {
//                url: '/presentation',
//                templateUrl: 'views/mui/planningtool/objective/detail/section/presentation.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/Objectivepresentation_bundle.js']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentity.task', {
//                url: '/task',
//                templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['assets/css/damCSS.css']);
//                    }]
//                }
//            }).state('mui.objective.detail.sectionentitycustom.customtab', {
//                url: '/customtab',
//                templateUrl: 'views/mui/planningtool/objective/detail/section/customtab.html',
//                resolve: {
//                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                        return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                    }]
//                }
//            })
//        .state('mui.objective.detail.sectionentitycustom.customtabtID', {
//            url: '/customtab/:tabID/:ID',
//            templateUrl: 'views/mui/planningtool/objective/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })
//        .state('mui.objective.detail.sectionentitycustom.customtabID', {
//            url: '/customtab/:tabID',
//            templateUrl: 'views/mui/planningtool/objective/detail/section/customtab.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/component/attributegroup-ListGanttViewInCustomTab-controller.js', 'app/controllers/mui/planningtool/component/attribute-group-controller.js', 'app/directives/mui-directives.js']);
//                }]
//            }
//        })




//                        /******  Brief  ******/

//        .state('mui.mybrief', {
//            url: '/mybrief/:ID',
//            templateUrl: 'views/mui/planningtool/iframe.html',
//            resolve: {
//                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load(['app/controllers/mui/planningtool/iframe-controller.js']);
//                }]
//            }
//        })

//                        /*********** Custom search   ***********/

//      .state('mui.customsearchresults', {
//          url: '/customsearchresults',
//          templateUrl: 'views/mui/customsearchresult.html',
//          resolve: {
//              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                  return $ocLazyLoad.load(['app/controllers/mui/customsearchresult-controller.js']);
//              }]
//          }
//      }).state('mui.customsearchresult', {
//          url: '/customsearchresult',
//          templateUrl: 'views/mui/customsearchresult.html',
//          resolve: {
//              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                  return $ocLazyLoad.load(['app/controllers/mui/customsearchresult-controller.js']);
//              }]
//          }
//      })
//}]);





(function (ng, app) {
    "use strict";

    function appCtrl($scope, $resource, $localStorage, $timeout, $interval, $location, $cookies, $translate, $http) {

        $scope.FinancialCurrentDivisionID = {
            ID: ForecstDivisonIds.Yearly
        }

        function GetDivisionValue() {
            GetCurrentDivisionId().then(function (res) {
                if (res.Response != 0) {
                    $scope.FinancialCurrentDivisionID.ID = res.Response;
                }
            });
        }
        getFinancialsettings();

        function getFinancialsettings() {
            GetFinancialForecastsettings().then(function (res) {
                if (res.StatusCode == 200) {
                    $scope.FinancialCurrentDivisionID.ID = res.Response.ForecastDivision;
                }
            });
        };

        function GetFinancialForecastsettings() { var request = $http({ method: "get", url: "api/Planning/GetFinancialForecastsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCurrentDivisionId() { var request = $http({ method: "get", url: "api/Planning/GetCurrentDivisionId/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }

        $scope.DefaultSettings = {
            DateFormat: 'yyyy-mm-dd',
            CurrencyFormat: {
                Id: 1,
                Name: "EUR",
                ShortName: "EUR",
                Symbol: "EUR"
            },
            LinkTypes: [{
                "Id": 1,
                "Type": "http://"
            }, {
                "Id": 2,
                "Type": "https://"
            }, {
                "Id": 3,
                "Type": "ftp://"
            }, {
                "Id": 4,
                "Type": "file://"
            }]
        }
        if (AdditionalSettingsObj != null && AdditionalSettingsObj.length > 0) {
            $scope.DefaultSettings.DateFormat = AdditionalSettingsObj[0].SettingValue;
            GlobalUserDateFormat = AdditionalSettingsObj[0].SettingValue.toUpperCase();
            Defaultdate = AdditionalSettingsObj[0].SettingValue;
            for (var i = 0; i < AdditionalSettingsObj[1].CurrencyFormatvalue.length; i++) {
                $scope.DefaultSettings.CurrencyFormat.Id = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Id;
                $scope.DefaultSettings.CurrencyFormat.Name = AdditionalSettingsObj[1].CurrencyFormatvalue[i].ShortName;
                $scope.DefaultSettings.CurrencyFormat.ShortName = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Name;
                $scope.DefaultSettings.CurrencyFormat.Symbol = AdditionalSettingsObj[1].CurrencyFormatvalue[i].Symbol;
            }
        }
        $scope.LoginStatus = function () {
            $scope.UserId = $cookies['UserId'];
        };
        $scope.GrabUserInfo = function () {
            $scope.Username = $cookies['Username'];
            $scope.UserImage = $cookies['UserImage'];
            $scope.UserEmail = $cookies['UserEmail'];
        };
        $scope.Logout = function () {
            Logout().then(function (logout1) {
                if (logout1.Response == null) {
                    $.removeCookie("UserId");
                    $.removeCookie("Username");
                    $.removeCookie("UserImage");
                    $.removeCookie("UserEmail");
                    $.removeCookie("planAttrs" + parseInt($cookies['UserId']));
                    localStorage.clear();
                    window.location.replace("login.html");
                    var date = new Date();
                    if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                        $('#loginpageTitle').attr('src', amazonURL + cloudsetup.BucketName + '/' + TenantFilePath + 'logo.png?ts=' + date.getTime());
                    } else {
                        $('#loginpageTitle').attr('src', 'assets/img/logo.png?ts=' + date.getTime());
                    }
                }
            });
        };


        function Logout() { var request = $http({ method: "get", url: "api/user/Logout/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }




        $scope.getInstanceTime = function () {
            var now = new Date();
            var timeString = now.toTimeString();
            var instanceTime = timeString.match(/\d+:\d+:\d+/i);
            return (instanceTime[0]);
        };
        $scope.openModalWindow = function (modalType) {
            bootbox.alert(arguments[1] || $translate.instant('LanguageContents.Res_1808.Caption'));
        };
        $scope.setWindowTitle = function (title) {
            $scope.windowTitle = title;
        };
        $scope.ContextMenuRecords = {
            ChildContextData: []
        };



        ChildEntityTypeHierarchy().then(function (res) {
            $scope.ContextMenuRecords.ChildContextData = res.Response;
        });

        function ChildEntityTypeHierarchy() { var request = $http({ method: "get", url: "api/Metadata/ChildEntityTypeHierarchy/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }


        $scope.DecodedTextName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };
        $scope.windowTitle = "Marcom Platform";
        $scope.LoginStatus();
        $scope.GrabUserInfo();

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
            }
            return undefined;
        }
        if (getCookie("SnippetLogin") != undefined) {
            $(".loading gear").hide();
            $(".gearSmall").hide();
            $(".gearMedium").hide();
            $(".gearLarge").hide();
        }
        $scope.lang = {
            isopen: false
        };

        $scope.TenantFileInfo = TenantFilePath;

        $scope.SystemSettings = { financialSettings: {}, finDefaultView: 0 };
        GetListFinancialView().then(function (res) {
            $scope.SystemSettings.financialSettings = res.Response;
            var tempObj = $.grep($scope.SystemSettings.financialSettings, function (rel) {
                return rel.IsDefault == true;
            })[0];
            if (tempObj != null)
                $scope.SystemSettings.finDefaultView = tempObj.Id;
        });
        function GetListFinancialView() { var request = $http({ method: "get", url: "api/Common/GetListFinancialView/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }


        function handleError(response) { if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); } return ($q.reject(response.data.message)); }
        function handleSuccess(response) { return (response.data); }


    }
    app.controller('appCtrl', ['$scope', '$resource', '$localStorage', '$timeout', '$interval', '$location', '$cookies', '$translate', '$http', appCtrl]);
})(angular, app);

angular.module('app').directive('uiModule', ['MODULE_CONFIG', 'uiLoad', '$compile', function (MODULE_CONFIG, uiLoad, $compile) {
    return {
        restrict: 'A',
        compile: function (el, attrs) {
            var contents = el.contents().clone();
            return function (scope, el, attrs) {
                el.contents().remove();
                uiLoad.load(MODULE_CONFIG[attrs.uiModule]).then(function () {
                    $compile(contents)(scope, function (clonedElement, scope) {
                        el.append(clonedElement);
                    });
                });
            }
        }
    };
}]);