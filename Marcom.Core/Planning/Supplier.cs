﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// Supplier object for table 'PM_Supplier'.
    /// </summary>

    internal class Supplier : ISupplier
    {
          #region Member Variables
        protected int _id;
        protected string _companyname;
        protected string _department;
        protected string _address1;
        protected string _address2;
        protected string _city;
        protected string _zipcode;
        protected string _state;
        protected string _country;
        protected string _vat;
        protected string _phone;
        protected string _email;
        protected string _fax;
        protected string _customernumber;
        #endregion

        #region Constructors
        public Supplier() { }
        public Supplier(string pCompanyName,string pDepartment,string pAddress1,string pAddress2,string pCity,string pZipcode,string pState,string pCountry,string pVat,string pPhone,string pEmail,string pFax,string pCustomerNamer)
        {
            this._companyname = pCompanyName;
            this._department = pDepartment;
            this._address1 = pAddress1;
            this._address2 = pAddress2;
            this._city = pCity;
            this._zipcode = pZipcode;
            this._state = pState;
            this._country = pCountry;
            this._vat = pVat;
            this._phone = pPhone;
            this._email = pEmail;
            this._fax = pFax;
            this._customernumber = pCustomerNamer;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public string CompanyName
        {
            get { return _companyname; }
            set {_companyname = value;}
        }

        public string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Vat
        {
            get { return _vat; }
            set { _vat = value; }
        }
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }

        }
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        public string CustomerNumber
        {
            get { return _customernumber; }
            set { _customernumber = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Supplier castObj = null;
            try
            {
                castObj = (Supplier)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }
}
