using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// NavigationAccessDao object for table 'CM_NavigationAccess'.
	/// </summary>
	
	public partial class NavigationAccessDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _navigationid;
		protected int _globalroleid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public NavigationAccessDao() {}
			
		public NavigationAccessDao(int pNavigationid, int pGlobalRoleid)
		{
			this._navigationid = pNavigationid; 
			this._globalroleid = pGlobalRoleid; 
		}
				
		public NavigationAccessDao(int pNavigationid)
		{
			this._navigationid = pNavigationid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Navigationid
		{
			get { return _navigationid; }
			set { _bIsChanged |= (_navigationid != value); _navigationid = value; }
			
		}
		
		public virtual int GlobalRoleid
		{
			get { return _globalroleid; }
			set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string NavigationAccessDao = "NavigationAccessDao";			
			public const string Navigationid = "Navigationid";			
			public const string GlobalRoleid = "GlobalRoleid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string NavigationAccessDao = "NavigationAccessDao";			
			public const string Navigationid = "Navigationid";			
			public const string GlobalRoleid = "GlobalRoleid";			
		}

		#endregion
		
		
	}
	
}
