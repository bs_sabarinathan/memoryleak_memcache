using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserNotificationDao object for table 'CM_User_Notification'.
	/// </summary>
	
	public partial class UserNotificationDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected int _entityid;
		protected int _actorid;
		protected DateTimeOffset _createdon;
		protected int _typeid;
		protected bool _isviewed;
		protected bool _issentinmail;
        protected string _typename;
        protected string _attributename;
        protected string _fromvalue;
        protected string _tovalue;
        protected int _associtedEntityID;
        protected int _phaseID;
        protected string _stepID;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public UserNotificationDao() {}

        public UserNotificationDao(int pUserid, int pEntityid, int pActorid, DateTimeOffset pCreatedOn, int pTypeid, bool pIsViewed, bool pIsSentInMail, string pTypeName, string pAttributeName, string pFromValue, string pToValue, int passocitedEntityID, int pphaseID, string pstepID)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._actorid = pActorid; 
			this._createdon = pCreatedOn; 
			this._typeid = pTypeid; 
			this._isviewed = pIsViewed; 
			this._issentinmail = pIsSentInMail;
            this._typename = pTypeName;
            this._attributename = pAttributeName;
            this._fromvalue = pFromValue;
            this._tovalue = pToValue;
            this._associtedEntityID = passocitedEntityID;
            this._phaseID=pphaseID;
            this._stepID = pstepID;    
		}
				
		public UserNotificationDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual int Actorid
		{
			get { return _actorid; }
			set { _bIsChanged |= (_actorid != value); _actorid = value; }
			
		}
		
		public virtual DateTimeOffset CreatedOn
		{
			get { return _createdon; }
			set { _bIsChanged |= (_createdon != value); _createdon = value; }
			
		}
		
		public virtual int Typeid
		{
			get { return _typeid; }
			set { _bIsChanged |= (_typeid != value); _typeid = value; }
			
		}
		
		public virtual bool IsViewed
		{
			get { return _isviewed; }
			set { _bIsChanged |= (_isviewed != value); _isviewed = value; }
			
		}
		
		public virtual bool IsSentInMail
		{
			get { return _issentinmail; }
			set { _bIsChanged |= (_issentinmail != value); _issentinmail = value; }
			
		}

        public virtual string TypeName
        {

            get { return _typename; }
            set { _bIsChanged |= (_typename != value); _typename = value; }

        }

        public virtual string AttributeName
        {

            get { return _attributename; }
            set { _bIsChanged |= (_attributename != value); _attributename = value; }

        }

        public virtual string FromValue
        {

            get { return _fromvalue; }
            set { _bIsChanged |= (_fromvalue != value); _fromvalue = value; }

        }

        public virtual string ToValue
        {

            get { return _tovalue; }
            set { _bIsChanged |= (_tovalue != value); _tovalue = value; }

        }

        public virtual int AssocitedEntityID
        {
            get { return _associtedEntityID; }
            set { _bIsChanged |= (_associtedEntityID != value); _associtedEntityID = value; }

        }

        public virtual int PhaseID
        {
            get { return _phaseID; }
            set { _bIsChanged |= (_phaseID != value); _phaseID = value; }

        }
        public virtual string StepID
        {
            get { return _stepID; }
            set { _bIsChanged |= (_stepID != value); _stepID = value; }

        }
       
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserNotificationDao = "UserNotificationDao";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string Actorid = "Actorid";			
			public const string CreatedOn = "CreatedOn";			
			public const string Typeid = "Typeid";			
			public const string IsViewed = "IsViewed";			
			public const string IsSentInMail = "IsSentInMail";
            public const string TypeName = "TypeName";
            public const string AttributeName = "AttributeName";
            public const string FromValue = "FromValue";
            public const string ToValue = "ToValue";
            public const string AssocitedEntityID = "AssocitedEntityID";	
            public const string PhaseID = "PhaseID";
            public const string StepID = "StepID";

		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserNotificationDao = "UserNotificationDao";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string Actorid = "Actorid";			
			public const string CreatedOn = "CreatedOn";			
			public const string Typeid = "Typeid";			
			public const string IsViewed = "IsViewed";			
			public const string IsSentInMail = "IsSentInMail";
            public const string TypeName = "TypeName";
            public const string AttributeName = "AttributeName";
            public const string FromValue = "FromValue";
            public const string ToValue = "ToValue";
            public const string AssocitedEntityID = "AssocitedEntityID";
            public const string PhaseID = "PhaseID";
            public const string StepID = "StepID";
		}

		#endregion
		
		
	}
	
}
