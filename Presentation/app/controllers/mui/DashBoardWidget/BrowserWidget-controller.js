﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetBrowserWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, WidgetbrowserService) {
        $scope.browserchartMetrics = {
            "Count": "1"
        };
        $scope.GlobalChartType = {
            1: "column",
            2: "pie"
        };
        $scope.userimg = parseInt($cookies['UserId']);
        $timeout(function () { drawdetail(); }, 100);
        $scope.BrowserWidgetContentID = $scope.WizardID;

        function drawdetail() {
            $scope.browserchartpoints = [];
            var visualtype = $scope.visualType;
            WidgetbrowserService.GetDynamicwidgetContentUserID(parseInt($cookies['UserId'], 10), $scope.WizardTypeID, $scope.WizardRealID, $scope.DimensionID).then(function (GetDynamicwidgetContentUserID) {
                var browserResult = GetDynamicwidgetContentUserID.Response;
                var data = [];
                for (var i = 0; i < browserResult.length; i++) {
                    if ($scope.GlobalChartType[$scope.visualType] == "column") data.push({
                        y: browserResult[i].VALUE,
                        label: browserResult[i].NAME,
                        name: browserResult[i].NAME
                    });
                    else data.push({
                        y: browserResult[i].VALUE,
                        legendText: browserResult[i].NAME + ": " + browserResult[i].VALUE,
                        indexLabel: "",
                        name: browserResult[i].NAME
                    });
                }
                $('#' + $scope.BrowserWidgetContentID + '').attr('data-visualType', $scope.GlobalChartType[$scope.visualType]);
                $('#' + $scope.BrowserWidgetContentID + '').attr('data-value', JSON.stringify(data));
                $('#' + $scope.BrowserWidgetContentID + '').attr('data-charttitle', 'Browser Summary');
                RenderBarChart($scope.WizardID);
                $('#' + $scope.BrowserWidgetContentID + '').parents('.box-content').find('.InProgress').remove();
            });
        };

        function RenderBarChart(WizardID) {
            $('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
            var charttitle = $('#' + WizardID + '').attr('data-charttitle');
            var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
            CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
            if ($('#' + WizardID + '').attr('data-visualType') == "column") {
                var chart = new CanvasJS.Chart(WizardID, {
                    title: {
                        text: charttitle,
                        verticalAlign: "top",
                        horizontalAlign: "center",
                        fontSize: 15,
                        fontFamily: "Calibri",
                        fontWeight: "normal",
                        fontColor: "black",
                        fontStyle: "normal",
                        borderThickness: 0,
                        borderColor: "black",
                        cornerRadius: 0,
                        backgroundColor: null,
                        margin: 0
                    },
                    colorSet: "finColorSet1",
                    height: $('#' + WizardID + '').parents('.box-content').height() - 10,
                    axisX: {
                        tickColor: "white",
                        tickLength: 5,
                        labelFontFamily: "sans-serif",
                        labelFontColor: "#7F7F7F",
                        labelFontSize: 10,
                        labelFontWeight: "normal"
                    },
                    axisY: {
                        tickLength: 0,
                        labelFontSize: 0,
                        lineThickness: 0,
                        gridThickness: 0,
                        includeZero: true,
                    },
                    data: [{
                        type: "column",
                        indexLabel: "{y}",
                        indexLabelFontSize: 12,
                        indexLabelFontWeight: "normal",
                        indexLabelPlacement: "outside",
                        indexLabelOrientation: "horizontal",
                        indexLabelFontFamily: "sans-serif",
                        indexLabelFontColor: "darkgrey",
                        legendMarkerColor: "gray",
                        showInLegend: false,
                        toolTipContent: "{name}:<strong>{y}</strong>",
                        dataPoints: data
                    }]
                });
            } else {
                var chart = new CanvasJS.Chart(WizardID, {
                    title: {
                        text: charttitle,
                        verticalAlign: "top",
                        horizontalAlign: "center",
                        fontSize: 15,
                        fontFamily: "Calibri",
                        fontWeight: "normal",
                        fontColor: "black",
                        fontStyle: "normal",
                        borderThickness: 0,
                        borderColor: "black",
                        cornerRadius: 0,
                        backgroundColor: null,
                        margin: 0
                    },
                    animationEnabled: true,
                    height: $('#' + WizardID + '').parents('.box-content').height() - 10,
                    legend: {
                        verticalAlign: "center",
                        horizontalAlign: "right",
                        fontSize: 12,
                        fontweight: "normal",
                        padding: 0,
                        fontColor: "darkgrey",
                        fontFamily: "sans-serif"
                    },
                    data: [{
                        indexLabelFontSize: 12,
                        indexLabelLineThickness: 0,
                        indexLabelFontStyle: "italic",
                        indexLabelFontWeight: "normal",
                        indexLabelFontFamily: "sans-serif",
                        indexLabelFontColor: "darkgrey",
                        indexLabelLineColor: "darkgrey",
                        indexLabelPlacement: "outside",
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{name}:<strong>{y}</strong>",
                        dataPoints: data
                    }]
                });
            }
            chart.render();
        }
        $(document).on("OnChartLoad", function (event, setChartID) {
            if (setChartID != undefined) {
                $('#' + setChartID + '').html('');
                RenderBarChart(setChartID);
            }
        });

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.BrowserWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.BrowserWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'WidgetbrowserService', muiDashBoardWidgetBrowserWidgetCtrl]);

    function WidgetbrowserService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetDynamicwidgetContentUserID: GetDynamicwidgetContentUserID
        });
        function GetDynamicwidgetContentUserID(UserID, WidgetTypeID, WidgetID, DimensionID) { var request = $http({ method: "get", url: "api/common/GetDynamicwidgetContentUserID/" + UserID + "/" + WidgetTypeID + "/" + WidgetID + "/" + DimensionID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("WidgetbrowserService", ['$http', '$q', WidgetbrowserService]);
})(angular, app);