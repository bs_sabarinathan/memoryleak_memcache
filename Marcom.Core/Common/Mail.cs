using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface; 

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
    /// Mail object for table 'CM_Mail'.
	/// </summary>
	
	internal class Mail : IMail
	{
		#region Member Variables

	
		protected int _id;
        protected string _tomail;
        protected string _subject;
        protected string _body;
        protected DateTimeOffset _actualtime;
        protected int _nooftrial;
        protected DateTimeOffset _senttime;
        protected DateTimeOffset _LastProccessedTime;
        protected string _status;
        protected int _mailtype;
        protected bool  _isrecapmail;
        protected DateTimeOffset _recaptimestamp;
        protected DateTimeOffset _recaptime;
        protected bool _isrecapsent;
		
		
		#endregion
		
		#region Constructors
		public Mail() {}

        public Mail(int pid, string pTomail, string pSubject, string pBody, DateTimeOffset pActualtime, int pNooftrial, DateTimeOffset pSenttime,
            int pmailtype, bool pisrecapmail, DateTimeOffset pLastProccessedTime, DateTimeOffset pRecapTimeStamp, DateTimeOffset pRecapTime, bool pIsRecapSent)
		{
            this._id = pid;
            this._tomail = pTomail;
            this._subject = pSubject;
            this._body = pBody;
            this._actualtime = pActualtime;
            this._nooftrial = pNooftrial;
            this._senttime = pSenttime;
            this._mailtype = pmailtype;
            this._isrecapmail = pisrecapmail;
            this._LastProccessedTime = pLastProccessedTime;
            this._recaptimestamp = pRecapTimeStamp;
            this._recaptime = pRecapTime;
            this._isrecapsent = pIsRecapSent;
		}

        public Mail(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public virtual string ToMail
        {
            get { return _tomail; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("ToMail", "ToMail value, cannot contain more than 100 characters");
               
                _tomail = value;
            }

        }

        public virtual string Subject
        {
            get { return _subject; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Subject", "Subject value, cannot contain more than 50 characters");
              
                _subject = value;
            }
        }

        public virtual string Body
        {
            get { return _body; }
            set
            {
                //if (value != null && value.Length > 1000)
                //    throw new ArgumentOutOfRangeException("Body", "Body value, cannot contain more than 1000 characters");

                _body = value;
            }

        }

        public virtual DateTimeOffset ActualTime
        {
            get { return _actualtime; }
            set { _actualtime = value; }

        }
        public virtual DateTimeOffset LastProccessedTime
        {
            get { return _LastProccessedTime; }
            set { _LastProccessedTime = value; }

        }
        public virtual int NoOfTrial
        {
            get { return _nooftrial; }
            set {_nooftrial = value; }
        }



        public virtual DateTimeOffset SentTime
        {
            get { return _senttime; }
            set { _senttime = value; }

        }
        public virtual string Status
        {
            get { return _status; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Length", "Length value, cannot contain more than 50 characters");
               
                _status = value;
            }

        }
        public virtual int mailtype
        {
            get { return _mailtype; }
            set { _mailtype = value; }

        }
        public virtual bool isrecapmail
        {
            get { return _isrecapmail; }
            set { _isrecapmail = value; }

        }
        public virtual DateTimeOffset RecapTimeStamp
        {
            get { return _recaptimestamp; }
            set { _recaptimestamp = value; }

        }
        public virtual DateTimeOffset RecapTime
        {
            get { return _recaptime; }
            set { _recaptime = value; }

        }
        public virtual bool IsRecapSent
        {
            get { return _isrecapsent; }
            set { _isrecapsent = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Mail castObj = null;
            try
            {
                castObj = (Mail)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
