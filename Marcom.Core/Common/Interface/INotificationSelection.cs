﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface INotificationSelection
    {
        int Typeid { get; set; }
        int UserId { get; set; }
        string UserName { get; set; }
        string UserEmail { get; set; }
        string UserImage { get; set; }
        string NotificationHappendTime { get; set; }
        string NotificationText { get; set; }
        bool Isviewed { get; set; }
        int Notificationid { get; set; }
        int TaskListID { get; set; }
        int TaskEntityID { get; set; }
    }
}
