using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttribute interface for table 'MM_EntityTypeStatus_Options'.
    /// </summary>
    public interface IEntityTypeStatusOptions
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityTypeID
        {
            get;
            set;

        }

        string StatusOptions
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        bool IsDeleted
        {
            get;
            set;
        }

        string ColorCode
        {
            get;
            set;
        }
        #endregion
    }
}
