﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class PopularTagWords : IPopularTagWords
    {
          #region Member Variables

		protected int _id;
		protected int _attributeid;
        protected string _usedon;
        protected int _totalhits;
		
		#endregion
		
		#region Constructors
		public PopularTagWords() {}

        public PopularTagWords(int pId, int pAttributeid, string pUsedon, int pTotalHits)
		{
			this._id = pId; 
			this._attributeid = pAttributeid;
            this._usedon = pUsedon;
            this._totalhits = pTotalHits;
		}

        public PopularTagWords(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public string UsedOn
        {
            get { return _usedon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("UsedOn", "UsedOn value, cannot contain more than 10 characters");
                _usedon = value;
            }

        }

        public int TotalHits
        {
            get { return _totalhits; }
            set { _totalhits = value; }
        }

        #endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            PopularTagWords castObj = null;
			try
			{
                castObj = (PopularTagWords)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
