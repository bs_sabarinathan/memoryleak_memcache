﻿(function(ng,app){"use strict";function muiplanningtoolcalenderdetaillistviewCtrl($scope,$timeout,$http,$resource,$compile,$window){$("#plandetailfilterdiv").css('display','block')
$scope.copyrightYear=(new Date()).getFullYear();$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detail.listviewCtrl']"));});var colStatus=false;ListViewLoad();function ListViewLoad(){$('#EntitiesTree').scrollTop(0);if($("#EntitiesTree li").hasClass('active'))
$("#EntitiesTree li.active").removeClass('active');if($scope.ListTemplate.length>0){$("#ListHolder").html($scope.ListTemplate);for(var i=0;i<$scope.ListViewDetails.length;i++){LoadListView($scope.ListViewDetails[i].data,$scope.ListViewDetails[i].PageIndex,$scope.ListViewDetails[i].UniqueID)}
$(window).AdjustHeightWidth();}}
$("#EntitiesTree").on('onListViewTemplateCreation',function(event,ListTemplate){});$scope.$on('onTreePageCreation',function(event,data){ListViewLoad();});function LoadListView(ListContentData,PageIndex,UniqueID){if($("#EntitiesTree li").hasClass('active'))
$("#EntitiesTree li.active").removeClass('active');var listColumnDefsdata=ListContentData.Response.GeneralColumnDefs;if(listColumnDefsdata!=null){var listContent=ListContentData.Response.Data;var contentnHtml="<tr>";var columnHtml="<tr >";for(var i=0;i<listContent.length;i++){if(i!=undefined){var UniqueId=UniqueKEY(listContent[i]["UniqueKey"]);var Iscalender=false;var GeneralUniquekey=UniqueKEY(listContent[i]["class"].toString());var ClassName=GenateClass(GeneralUniquekey);ClassName+=" mo"+GeneralUniquekey;if(listContent[i]["TypeID"]==35){Iscalender=true;}
contentnHtml+="<tr data-iscalender='"+Iscalender+"' data-EntityLevel='"+listContent[i]["Level"]+"' data-over='true' data-uniquekey='"+GeneralUniquekey+"' class='"+ClassName+"'>"
for(var j=0;j<listColumnDefsdata.m_Item1.length;j++){if(j!=undefined&&listColumnDefsdata.m_Item1[j]!="Name"){if(colStatus==false){columnHtml+="<th>";columnHtml+="    <span data-Column="+listColumnDefsdata.m_Item1[j]+">"+listColumnDefsdata.m_Item2[j]+"</span>";columnHtml+="</th>";}
if(AttributeTypes.Period==listColumnDefsdata.m_Item1[j].Type)
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i]["TempPeriod"]!=undefined?listContent[i]["TempPeriod"]:"-")+"</span></td>";else if(listColumnDefsdata.m_Item1[j]!=70&&listColumnDefsdata.m_Item1[j]!='IsExternal')
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i][listColumnDefsdata.m_Item1[j]]==null?"0":listContent[i][listColumnDefsdata.m_Item1[j]])+"</span></td>";else
contentnHtml+="<td><span class='ng-binding'>"+(listContent[i][listColumnDefsdata.m_Item1[j]]==null?"-":listContent[i][listColumnDefsdata.m_Item1[j]])+"</span></td>";}}
contentnHtml+="<td style='display:none;'><span><label class='checkbox checkbox-custom pull-right'><input type='checkbox'><i class='checkbox'></i></label></span></td>";contentnHtml+="</tr>";colStatus=true;}}
if($("#ListColumn > thead tr").length==0){columnHtml+="<th style='display:none;'><span><label class='checkbox checkbox-custom pull-right'><input type='checkbox'><i class='checkbox'></i></label></span></th>";columnHtml+="</tr>";$('#ListColumn > thead').html(columnHtml);}
if($("#ListColumn > thead tr").length>0){if(UniqueID!=""){$('#ListContainer > table tbody tr[data-uniquekey="'+UniqueID+'"]').after(contentnHtml);}
else{$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').html(contentnHtml);$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').removeClass('pending');$('#ListContainer > table tbody[data-page="'+PageIndex+'"]').removeAttr("style");}}}
$(window).AdjustHeightWidth();}
$('#ListContainer').scroll(function(){$("#EntitiesTree").scrollTop($(this).scrollTop());});function UniqueKEY(UniKey){var substr=UniKey.split('.');var id="";var row=substr.length;if(row>1){for(var i=0;i<row;i++){id+=substr[i];if(i!=row-1){id+="-";}}
return id;}
return UniKey;}
function GenateClass(UniKey){var ToLength=UniKey.lastIndexOf('-');if(ToLength==-1){return"";}
var id=UniKey.substring(0,ToLength);var afterSplit=id.split('-');var result="";for(var i=0;i<afterSplit.length;i++){result+=SplitClss(id,i+1);}
return result;}
function SplitClss(NewId,lengthofdata){var afterSplit=NewId.split('-');var finalresult=" p";for(var i=0;i<lengthofdata;i++){finalresult+=afterSplit[i];if(i!=lengthofdata-1){finalresult+="-"}}
return finalresult;}}
app.controller("mui.planningtool.calender.detail.listviewCtrl",['$scope','$timeout','$http','$resource','$compile','$window',muiplanningtoolcalenderdetaillistviewCtrl]);})(angular,app);