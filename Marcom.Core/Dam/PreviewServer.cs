﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using BrandSystems.Library.Threading;
using BrandSystems.Library.Threading.Internal;
using System.Reflection;
using System.Configuration;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Utility;
using System.Drawing;
using System.Net.Mail;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Dal.Common.Model;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Configuration;
using System.Net.Configuration;
using System.IO;
using System.Xml.Linq;

using BrandSystems.Marcom.Core.Common;
using System.Data.SqlClient;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.DAM.Model;
using BrandSystems.Marcom.Core.MediaHandlerService;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
//using BrandSystems.MediaHandler.Input;
//using BrandSystems.MediaHandler;
//using BrandSystems.MediaHandler.Output;
//using BrandSystems.MediaHandler.CustomType;

namespace Preview
{
    public class PreviewServer
    {
        // IMarcomManager marcomManager=null;
        //MarcomManager marcomManager = new MarcomManager();
        public PreviewServer()
        {



        }

        private static PreviewServer _instance = new PreviewServer();
        //private static string _header;
        //public static Image _logo;

        public static PreviewServer Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }
        //public static string Header
        //{
        //    get { return _header; }
        //    set { _header = value; }
        //}
        //public static Image Logo
        //{
        //    get { return _logo; }
        //    set { _logo = value; }
        //}


        string nuller = "";
        public bool Initialize(String smtpServer)
        {
            try
            {

                //PreviewServer.Instance.previewStatusUpdate += PreviewStatusUpdateToDb;
                //var client = new SmtpClient
                //{
                //    Credentials = new System.Net.NetworkCredential("marcom.noreply@brandsystems.in", "bsi1234$"),
                //    Host = "mail.brandsystems.in"
                //};

                return true;
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        //Create properties for Logo, Header 

        public Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();
        private Dictionary<Guid, IWorkItemsGroup> _workItemGroupList = new Dictionary<Guid, IWorkItemsGroup>();
        private List<Guid> _workItemGroupListIDs = new List<Guid>();

        private static BSThreadPool _bsThreadPool;
        //private static IWorkItemsGroup _workItemGroup;
        //public delegate void PreviewStatusUpdate(MailHolder mailHolder, PreviewStatus status);
        //public delegate void PreviewStatusUpdate(PreviewHolder previewHolder, PreviewStatus status);
        //public event PreviewStatusUpdate previewStatusUpdate;

        #region "Start and Stop Engine"

        /// <summary>
        /// For Starting the Engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StartEngine()
        {
            try
            {
                BSTPStartInfo bstpStartInfo = new BSTPStartInfo();
                bstpStartInfo.IdleTimeout = 30 * 1000; //30 Sec
                bstpStartInfo.MaxWorkerThreads = 30; //Max 30 thread we will create                
                bstpStartInfo.MinWorkerThreads = 0;
                bstpStartInfo.PerformanceCounterInstanceName = "Marcom Preview BSThreadPool";
                _bsThreadPool = new BSThreadPool(bstpStartInfo);

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        /// <summary>
        /// For Stoping the engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StopEngine()
        {


            try
            {
                _bsThreadPool.Shutdown();
                _bsThreadPool.Dispose();
                _bsThreadPool = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        #endregion

        /// <summary>
        /// Checking memory For Synchronous and ASynchronous tasks
        /// </summary>
        /// <param name="obj">MediaHandlerObject</param>
        /// <returns>True or false</returns>
        /// <remarks></remarks>
        public bool AddToQueque(PreviewHolder obj, string flag)
        {

            try
            {
                var pworkPriority = WorkItemPriority.Highest;
                var sworkPriority = WorkItemPriority.Normal;
                if (flag == "insert")
                {
                    //var workItemCallBackInsertToDb = new WorkItemCallback(HandleMail);
                    //_bsThreadPool.QueueWorkItem(workItemCallBackInsertToDb, obj);

                }
                //else if (flag == "RecapReport")
                //{
                //    var workPriority1 = WorkItemPriority.Lowest;

                //    if (obj.PrimaryTo != null)
                //    {
                //        workPriority1 = pworkPriority;
                //    }
                //    if (obj.SecondaryTo != null)
                //    {
                //        workPriority1 = sworkPriority;

                //    }
                //    obj.isrecap = true;
                //    var workItemCallBack1 = new WorkItemCallback(SendMailEvent);
                //    var postExeWorkItemCallBack1 = new PostExecuteWorkItemCallback(PostSendMailEvent);
                //    _bsThreadPool.QueueWorkItem(workItemCallBack1, obj, postExeWorkItemCallBack1, workPriority1);
                //}
                else
                {
                    var workPriority = WorkItemPriority.Lowest;
                    workPriority = pworkPriority;
                    //if (obj.PrimaryTo != null)
                    //{
                    //    workPriority = pworkPriority;
                    //}
                    //if (obj.SecondaryTo != null)
                    //{
                    //    workPriority = sworkPriority;

                    //}
                    var workItemCallBack = new WorkItemCallback(SendMailEvent);
                    var postExeWorkItemCallBack = new PostExecuteWorkItemCallback(PostSendMailEvent);
                    _bsThreadPool.QueueWorkItem(workItemCallBack, obj, postExeWorkItemCallBack, workPriority);

                }

                return true;
            }
            catch (Exception)
            {

                return true;
            }

            return true;

        }
        /// <summary>
        /// For executing single Job.
        /// </summary>
        /// <param name="state"></param>
        /// <returns>Media Handler Object</returns>
        /// <remarks></remarks>
        public object SendMailEvent(object state)
        {
            //Send mail here;

            BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandler MH = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandler();
            //BrandSystems.Marcom.Core.Utility.MediaHandler.ItemRequest obItemRequest = new BrandSystems.Marcom.Core.Utility.MediaHandler.ItemRequest();
            //obItemRequest.CropTop = 110; obItemRequest.CropLeft = 110; obItemRequest.CropWidth = 110; obItemRequest.CropHeight = 110;
            //obItemRequest.ResizeHeight = 85; obItemRequest.ResizeWidth = 85; obItemRequest.XResolutionDPI = 72; obItemRequest.YResolutionDPI = 72;
            //obItemRequest.Format = "jpg"; obItemRequest.Unit = BrandSystems.Marcom.Core.Utility.MediaHandler.ItemRequest.Units.Pixels;
            //obItemRequest.CornerColor = "white";
            //string[] itemInfo = new string[6];
            //string errorinfo = "";
            //string sessinon = "";
            // string  sourec="\\\\192.168.1.20\\e$\\test\\test1.jpg";
            //string sourec = "\\\\192.168.1.20\\e$\\test\\test2.png";
            //string descna = "\\\\192.168.1.20\\e$\\test1\\test10png.jpg";
            //MH.GereratePreview(sourec, descna, 140, 140);
            //test1.getFileInfo(sourec);
            // test1.Rescale(obItemRequest , "e:\\test\\test1.jpg", "e:\\test1\\testnew11.jpg", ref itemInfo, ref errorinfo, sessinon);
            //test1.GereratePreview("\\KARTHIKEYAN-NB\e$\test\test1.jpg", "\\KARTHIKEYA-NB\e$\test1\test2.jpg", 140, 140);
            var PreviewHolder = (PreviewHolder)state;
            // MH.GereratePreview(PrevieHolder.SourcePath, PrevieHolder.DestinationPath, PrevieHolder.MaxHight, PrevieHolder.MaxWidth);
            //var mailHolder = (MailHolder)state;
            try
            {
                object Preview = new object();

                // MH.ping();
                MH.GereratePreview(PreviewHolder.SourcePath, PreviewHolder.DestinationPath, PreviewHolder.MaxHight, PreviewHolder.MaxWidth);

                //{
                //var mail = new System.Net.Mail.MailMessage
                //{
                //    Subject = mailHolder.Subject,
                //    SubjectEncoding = System.Text.Encoding.UTF8,
                //    Body = mailHolder.Body,
                //    BodyEncoding = System.Text.Encoding.UTF8,
                //    IsBodyHtml = true,
                //    Priority = MailPriority.Normal
                //};

                //var client = new SmtpClient();
                ////Add the Creddentials- use your own email id and password
                //mail.From = new MailAddress("marcom.noreply@brandsystems.in", "Marcom");
                //client.Credentials = new System.Net.NetworkCredential("marcom.noreply@brandsystems.in", "bsi1234$");
                //client.Host = "webmail.brandsystems.in";

                //foreach (var mails in mailHolder.PrimaryTo)
                //{
                //    mail.To.Add(mails.ToString());
                //    try
                //    {
                //        client.SendAsync(mail, null);
                //    }
                //    catch (Exception ex)
                //    {
                //        Exception ex2 = ex;
                //        string errorMessage = string.Empty;
                //        while (ex2 != null)
                //        {
                //            errorMessage += ex2.ToString();
                //            ex2 = ex2.InnerException;
                //        }
                //        HttpContext.Current.Response.Write(errorMessage);
                //    } // end try 
                //}


                //previewStatusUpdate(PreviewHolder, PreviewStatus.Pending);

            }
            catch (Exception)
            {
                throw;
            }

            return PreviewHolder;
        }
        /// <summary>
        ///  For a single job if  response is nothing it will provide some basic information like error details ,AditionalInfo,GroupID,JobID
        /// </summary>
        /// <param name="result">Provides details like Exception ,In progress,Done.. ,</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public void PostSendMailEvent(IWorkItemResult result)
        {

            PreviewHolder previewHolder = (PreviewHolder)result.State;



            if (result.Exception != null)
            {

                //if (previewStatusUpdate != null)
                //{
                //    previewStatusUpdate(previewHolder, PreviewStatus.Error);
                //}
                //Fail
                //Read the mail id and update the database status to inprogress and increase the iteration


            }
            else
            {
                //if (previewStatusUpdate != null)
                //{
                //    previewStatusUpdate(previewHolder, PreviewStatus.Inprogress);
                //}

                //Success
                //Read tha mail and update the status
            }



        }
        //public MailHolder InsertMailIntoDb(MailHolder obj)
        //{
        //    PreviewServer.Instance.AddToQueque(obj, "insert");

        //    return obj;
        //}
        //public object HandleMail(object state)
        //{
        //    MailHolder mailHolder = (MailHolder)state;
        //    ClsDb clsDb = new ClsDb();
        //    DataSet dataSet = new DataSet();
        //    StringBuilder strquery = new StringBuilder();
        //    try
        //    {
        //        MailDao mailDao = new MailDao();

        //        dataSet = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where cmc.ID='" + mailHolder.MailType + "' and cmc.ContainerID !=0 and cmc.ContainerID NOT IN (SELECT id FROM CM_MailContent cmc2 WHERE cmc2.ContainerID=0) ;", CommandType.Text);

        //        if (mailHolder.MailType != 0)
        //        {

        //            mailDao.ActualTime = DateTimeOffset.UtcNow;
        //            StringBuilder mailBody = new StringBuilder();
        //            mailBody.Append(dataSet.Tables[0].Rows[0]["Body"].ToString());
        //            foreach (var key in mailHolder.MailTemplateDictionary.Keys)
        //            {
        //                if (mailBody.ToString().Contains(key))
        //                {
        //                    mailBody.Replace(key, mailHolder.MailTemplateDictionary[key]);
        //                }

        //                mailDao.Body = mailBody.ToString();

        //            }
        //            mailDao.Subject = dataSet.Tables[0].Rows[0]["Subject"].ToString();
        //        }
        //        else
        //        {
        //            mailDao.Subject = mailHolder.Subject;
        //            mailDao.Body = mailHolder.Body;
        //        }
        //        if (mailHolder.PrimaryTo != null)
        //        {
        //            foreach (var mail in mailHolder.PrimaryTo)
        //            {
        //                mailDao.ToMail = mail;
        //                strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status])VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + PreviewStatus.Pending + "','" + mailHolder.MailType + "',0); SELECT SCOPE_IDENTITY()");
        //                dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
        //                mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();
        //            }
        //        }
        //        else
        //        {
        //            foreach (var mail in mailHolder.SecondaryTo)
        //            {
        //                mailDao.ToMail = mail;
        //                strquery.AppendLine("declare @emailenable as bit");
        //                strquery.AppendLine("select @emailenable=isemailenable from  CM_UserMailSubscription where UserID in( select id from UM_User where Email='" + mailDao.ToMail + "')");
        //                strquery.AppendLine("if @emailenable =1");
        //                strquery.AppendLine("begin");
        //                strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status])VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + PreviewStatus.Pending + "','" + mailHolder.MailType + "',1); SELECT SCOPE_IDENTITY()");
        //                strquery.AppendLine("end");
        //                dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
        //                mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();
        //            }
        //        }
        //    }

        //    catch
        //    {

        //    }
        //    return mailHolder;
        //}
        //public object HandleUnScheduledMail(object state)   //insert immediately and  call fetch to send mail immediately
        //{
        //    MailHolder mailHolder = (MailHolder)state;
        //    ClsDb clsDb = new ClsDb();
        //    DataSet dataSet = new DataSet();
        //    StringBuilder mailBody = new StringBuilder();
        //    StringBuilder mailSubjectHeader = new StringBuilder();
        //    StringBuilder mailHeader = new StringBuilder();
        //    StringBuilder mailFormatted = new StringBuilder();
        //    StringBuilder strquery = new StringBuilder();

        //    try
        //    {
        //        MailDao mailDao = new MailDao();

        //        dataSet = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where cmc.ID='" + mailHolder.MailType + "' and cmc.ContainerID !=0 and cmc.ContainerID NOT IN (SELECT id FROM CM_MailContent cmc2 WHERE cmc2.ContainerID=0) ;", CommandType.Text);

        //        string applurl = "";
        //        string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], "AdminSettings.xml");
        //        XDocument adminXmlDoc = XDocument.Load(xmlpath);
        //        //The Key is root node current Settings
        //        string xelementName = "ApplicationURL";
        //        var xelementFilepath = XElement.Load(xmlpath);
        //        var xmlElement = xelementFilepath.Element(xelementName);
        //        if (mailHolder.MailType != 0)
        //        {

        //            mailDao.ActualTime = DateTimeOffset.UtcNow;
        //            mailDao.Subject = dataSet.Tables[0].Rows[0]["Subject"].ToString();
        //            mailBody.Append(dataSet.Tables[0].Rows[0]["Body"].ToString());
        //            foreach (var key in mailHolder.MailTemplateDictionary.Keys)
        //            {
        //                if (mailBody.ToString().Contains(key))
        //                {
        //                    mailBody.Replace(key, mailHolder.MailTemplateDictionary[key]);
        //                }

        //            }


        //            mailBody = mailBody.Replace("@ImagePath@", xmlElement.Value.ToString() + "/assets/img/logo.png"); // Need to change the Path of the logo here.

        //        }
        //        else
        //        {
        //            mailDao.Subject = mailHolder.Subject;
        //            mailDao.Body = mailHolder.Body;
        //            mailBody = mailBody.Replace("@ImagePath@", xmlElement.Value.ToString() + "/assets/img/logo.png"); // Need to change the Path of the logo here.

        //        }
        //        mailDao.Body = mailBody.ToString();
        //        if (mailHolder.PrimaryTo != null)
        //        {
        //            foreach (var mail in mailHolder.PrimaryTo)
        //            {
        //                mailDao.ToMail = mail;
        //                strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status],mailtype,isrecapmail)VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + PreviewStatus.Pending + "','" + mailHolder.MailType + "',0); SELECT SCOPE_IDENTITY()");
        //                dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
        //            }
        //        }
        //        if (mailHolder.SecondaryTo != null)
        //        {
        //            foreach (var mail in mailHolder.SecondaryTo)
        //            {
        //                mailDao.ToMail = mail;
        //                strquery.AppendLine("declare @emailenable as bit");
        //                strquery.AppendLine("select @emailenable=isemailenable from  CM_UserMailSubscription where UserID in( select id from UM_User where Email='" + mailDao.ToMail + "')");
        //                strquery.AppendLine("if @emailenable =1");
        //                strquery.AppendLine("begin");
        //                strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status],mailtype,isrecapmail)VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + PreviewStatus.Pending + "','" + mailHolder.MailType + "',1); SELECT SCOPE_IDENTITY()");
        //                strquery.AppendLine("end");
        //                dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
        //            }
        //        }
        //        mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();

        //        mailHolder.Subject = mailDao.Subject;
        //        mailHolder.PrimaryTo = new List<string>();
        //        mailHolder.PrimaryTo.Add(mailDao.ToMail);
        //        IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(MarcomManagerFactory._systemSessionId);
        //        string SSOUserQueryString = "";
        //        DataSet ds = new DataSet();
        //        StringBuilder formatmail = new StringBuilder();
        //        using (ITransaction tx = marcomManager.GetTransaction())
        //        {
        //            var userDetails = (from tt in tx.PersistenceManager.CommonRepository[proxy.MarcomManager.User.TenantID].Query<UserDao>() where tt.Email == mailDao.ToMail select tt).FirstOrDefault();
        //            bool isSsoUser = userDetails.IsSSOUser;
        //            if (isSsoUser)
        //            {
        //                SSOUserQueryString = "&IsSSO=true";
        //            }

        //            ds = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc", CommandType.Text);
        //            var subjidfetch = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(mailHolder.MailType) select cc).FirstOrDefault();
        //            var subjectcontenttemplate = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(subjidfetch.Field<int>("ContainerID")) select cc).FirstOrDefault();
        //            var headertemplate = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(subjectcontenttemplate.Field<int>("ContainerID")) select cont).FirstOrDefault();
        //            formatmail.Append(headertemplate.Field<string>("Body"));
        //            formatmail.Replace("@##@", subjectcontenttemplate.Field<string>("Body").ToString());
        //            formatmail.Replace("@##@", mailDao.Body.ToString());
        //            formatmail.Replace("@Subject@", mailDao.Subject.ToString());
        //            formatmail.Replace("@AccSettings@", xmlElement.Value + "/login.html?ReturnURL=" + System.Web.HttpUtility.UrlEncode("#/mui/mypage") + SSOUserQueryString);
        //            formatmail.Replace("@UserName@", userDetails.FirstName);
        //            formatmail.Replace("@ImagePath@", xmlElement.Value.ToString() + "/assets/img/logo.png");
        //            tx.Commit();
        //        }
        //        mailHolder.Body = formatmail.ToString();

        //        PreviewServer.Instance.AddToQueque(mailHolder, "fetch");  //as soon as inserted send mail 


        //    }

        //    catch
        //    {

        //    }
        //    return mailHolder;
        //}


        public void HandleSendPreview(IMarcomManager marcomManager, int TenantID) //send task Preview , Preview for actions that in MH .
        {

            try
            {

                BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandler MH = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandler();
                PreviewHolder obj1 = new PreviewHolder();

                ClsDb clsDb = new ClsDb(marcomManager.User.TenantHost);
                DataSet dataSet = new DataSet();
                string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], "MediaHandlerSettings.xml");
                XDocument MHXmlDoc = XDocument.Load(xmlpath);
                int SmallThumbMaxHight = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("SmallThumbMaxHight").ElementAt(0).Value);
                int SmallThumbMaxWidth = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("SmallThumbMaxWidth").ElementAt(0).Value);
                int BigThumbMaxHight = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("BigThumbMaxHight").ElementAt(0).Value);
                int BigThumbMaxWidth = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("BigThumbMaxWidth").ElementAt(0).Value);
                int VideoPerviewStartTime = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("VideoPerviewStartTime").ElementAt(0).Value);
                int VideoPerviewEndTime = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("VideoPerviewEndTime").ElementAt(0).Value);
                int VideoResolutionHeight = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("VideoResolutionHeight").ElementAt(0).Value);
                int VideoResolutionWidth = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("VideoResolutionWidth").ElementAt(0).Value);
                string OutputVideoCodec = Convert.ToString(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("OutputVideoCodec").ElementAt(0).Value);
                int NumberOfThumbnail = Convert.ToInt32(MHXmlDoc.Descendants("MHSetting").Descendants("PreviewSize").Descendants("NumberOfThumbnail").ElementAt(0).Value);
                clsDb.MailData("UPDATE DAM_file SET [Status] = 2 WHERE  ProcessType  NOT IN( 1,3) and  Status = 0 ", CommandType.Text);
                dataSet = clsDb.MailData("SELECT * FROM DAM_File where Status in(0,100) AND  ProcessType IN(1,3)", CommandType.Text);


                int tenantID = marcomManager.User.TenantID;
             
                string awsAccesskeyID = marcomManager.User.AwsStorage.AWSAccessKeyID;
                string awsSecretAccesskey = marcomManager.User.AwsStorage.AWSSecretAccessKey;
                string awsBusketName = marcomManager.User.AwsStorage.BucketName;
                string awsRegionEndPoint = marcomManager.User.AwsStorage.RegionEndpoint;          
           
                
                bool blnFileSystemsMode = false ;
                if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                    blnFileSystemsMode=true;


                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    if (MH.MHClientReady())
                    {
                        using (ITransaction tx = marcomManager.GetTransaction(marcomManager.User.TenantID))
                        {
                            DataSet ds = new DataSet();
                            StringBuilder formatmail = new StringBuilder();
                            ds = clsDb.MailData("SELECT ID,Extension,FileGuid as [filename],Status,ProcessType FROM DAM_File where Status in(0,100) AND ProcessType IN(1,3) ", CommandType.Text);
                            var previewtask = dataSet.Tables[0].AsEnumerable().ToList().GroupBy(s => s.Field<int>("ID"));

                            foreach (var previewfile in previewtask)
                            {
                                try
                                {
                                    var Filedetails = (from tt in tx.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<DAMFileDao>() where tt.ID == previewfile.Key select tt).FirstOrDefault();
                                    //string applicationPath = ConfigurationManager.AppSettings["OriginalXMLpath"];
                                    int ProcessorID = Filedetails.ProcessType;
                                    string applicationPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
                                    applicationPath = applicationPath.Replace(":", "$");
                                    string strIPAddress = ConfigurationManager.AppSettings["IPAddress"].ToString();
                                    string soureFolder = marcomManager.User.TenantPath + "DAMFiles\\Original\\";
                                    string DestinationFolder = marcomManager.User.TenantPath + "DAMFiles\\Preview\\";
                                    string strExtension = Filedetails.Extension;
                                    string fileinfo = "";
                                    string videofileinfo = "";
                                    int orgWidth = 0;
                                    int orghight = 0;
                                    strExtension = strExtension.Substring(1, strExtension.Length - 1).ToUpper();
                                    //var xmlElement1 = MHXmlDoc.Descendants("MHSetting").Descendants("FileFormat").Descendants("File").Where(a => a.Value == strExtension).FirstOrDefault();

                                    //if (xmlElement1 == null)
                                    //{
                                    //    clsDb.MailData("UPDATE DAM_file SET [Status] = 2  WHERE Id='" + Convert.ToInt32(Filedetails.ID) + "'", CommandType.Text);
                                    //    continue;
                                    //}
                                    //int ProcessorID = Convert.ToInt32(xmlElement1.Attribute("ProcessorID").Value);
                                    obj1.Id = Convert.ToInt32(Filedetails.ID);
                                    obj1.SourcePath = "\\\\" + strIPAddress + "\\" + applicationPath + soureFolder + Filedetails.FileGuid + Filedetails.Extension;
                                    obj1.DestinationPath = "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Small_" + Filedetails.FileGuid + ".jpg";
                                    string srcbucketfilepath = ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DamOriginal) + Filedetails.FileGuid + Filedetails.Extension;
                                   
                                    if (ProcessorID == 1)
                                    {


                                        string filePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Original\" + Filedetails.FileGuid + Filedetails.Extension);
                                        bool exists = System.IO.File.Exists(filePath);
                                        if (!blnFileSystemsMode)
                                        {
                                            if (!exists)
                                            {
                                                LogHandler.LogInfo("exception generated Preview sending files:File Not exits" + filePath, LogHandler.LogType.Notify);
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + Convert.ToInt32(Filedetails.ID) + "'", CommandType.Text);
                                                continue;
                                            }
                                        }else if(blnFileSystemsMode)
                                        {
                                            
                                             bool exists1 = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, srcbucketfilepath);
                                             if (!exists1)
                                             {
                                                 LogHandler.LogInfo("exception generated Preview sending files:File Not exits" + filePath, LogHandler.LogType.Notify);
                                                 clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + Convert.ToInt32(Filedetails.ID) + "'", CommandType.Text);
                                                 continue;
                                             }

                                        }

                                        // LogHandler.LogInfo("going to get getFileInfo", LogHandler.LogType.Notify);
                                        fileinfo = MH.getFileInfo(obj1.SourcePath, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", srcbucketfilepath);

                                        LogHandler.LogInfo("going to get getFileInfo" + fileinfo, LogHandler.LogType.Notify);
                                        if (fileinfo.Length > 0)
                                        {
                                            string[] Additionalinformation = fileinfo.Split(',');
                                            string Heightinfomartion = Additionalinformation[0].ToString();
                                            string Widthinfomartion = Additionalinformation[1].ToString();
                                            //LogHandler.LogInfo("Heightinfomartion and Widthinfomartion" + Heightinfomartion + "#" + Widthinfomartion, LogHandler.LogType.Notify);
                                            if (Heightinfomartion.Length > 0)
                                            {
                                                ////LogHandler.LogInfo("suspect Heightinfomartion place" + Heightinfomartion.IndexOf(':') + "#" + Heightinfomartion.Substring(Heightinfomartion.IndexOf(':') + 1), LogHandler.LogType.Notify);
                                                //LogHandler.LogInfo("height ----Info" + heightinfo, LogHandler.LogType.Notify);
                                                //LogHandler.LogInfo("TTT" + heightinfo.ToString(), LogHandler.LogType.Notify);
                                                //LogHandler.LogInfo("WWW" + orghight, LogHandler.LogType.Notify);
                                                //LogHandler.LogInfo("actual conversion" + double.Parse(heightinfo.ToString(), System.Globalization.CultureInfo.InvariantCulture), LogHandler.LogType.Notify);
                                                string heightinfo = (Heightinfomartion.IndexOf(':') != -1) ? Heightinfomartion.Substring(Heightinfomartion.IndexOf(':') + 1) : "";
                                                double dorghight;
                                                dorghight = (heightinfo.Length > 0) ? double.Parse(heightinfo.ToString(), System.Globalization.CultureInfo.InvariantCulture) : 0;
                                                Int32.TryParse(dorghight.ToString("F0"), out orghight);

                                            }
                                            if (Widthinfomartion.Length > 0 && Widthinfomartion.IndexOf(':') != -1)
                                            {
                                                string Widthinfo = (Widthinfomartion.IndexOf(':') != -1) ? Widthinfomartion.Substring(Widthinfomartion.IndexOf(':') + 1) : "";
                                                double dorgWidth;
                                                dorgWidth = (Widthinfo.Length > 0) ? double.Parse(Widthinfo.ToString(), System.Globalization.CultureInfo.InvariantCulture) : 0;
                                                Int32.TryParse(dorgWidth.ToString("F0"), out orgWidth);
                                            }
                                            if (orghight > 0 && orgWidth > 0)
                                            {

                                                if (orgWidth < SmallThumbMaxWidth)
                                                    SmallThumbMaxWidth = orgWidth;
                                                if (orghight < SmallThumbMaxHight)
                                                    SmallThumbMaxHight = orghight;
                                                if (orgWidth < BigThumbMaxWidth)
                                                    BigThumbMaxWidth = orgWidth;
                                                if (orghight < BigThumbMaxHight)
                                                    BigThumbMaxHight = orghight;
                                            }
                                        }

                                        //if (Filedetails.Extension == ".gif" || Filedetails.Extension == ".emf" || Filedetails.Extension == ".png" || Filedetails.Extension == ".jpg" || Filedetails.Extension == ".jpeg" || Filedetails.Extension == ".tif" || Filedetails.Extension == ".tiff" || Filedetails.Extension == ".bmp")
                                        //{
                                        //    System.Drawing.Image img = System.Drawing.Image.FromFile(filePath);
                                        //    if (img.Width < SmallThumbMaxWidth)
                                        //        SmallThumbMaxWidth = img.Width;
                                        //    if (img.Height < SmallThumbMaxHight)
                                        //        SmallThumbMaxHight = img.Height;
                                        //    if (img.Width < BigThumbMaxWidth)
                                        //        BigThumbMaxWidth = img.Width;
                                        //    if (img.Height < BigThumbMaxHight)
                                        //        BigThumbMaxHight = img.Height;
                                        //}

                                    }

                                    obj1.MaxHight = SmallThumbMaxHight;
                                    obj1.MaxWidth = SmallThumbMaxWidth;
                                    obj1.Extension = Filedetails.Extension;
                                    obj1.Filename = Filedetails.FileGuid + Filedetails.Extension;


                                    //PreviewServer.Instance.AddToQueque(obj1, "fetch");
                                    //PreviewServer.Instance.AddToQueque(obj1, "fetch");
                                    //ImageResponseObject  imgResObj = new ImageResponseObject();
                                    //imgResObj =(BrandSystems.MediaHandler.Output.ImageResponseObject) 
                                    if (Filedetails.Status == 0 && ProcessorID == 1)
                                        clsDb.MailData("UPDATE DAM_file SET [Status] = 1 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                    try
                                    {

                                        if (Convert.ToInt32(Filedetails.Status) == 0 && ProcessorID == 1)
                                        {
                                            if (MH.GereratePreview(obj1.SourcePath, obj1.DestinationPath, obj1.MaxHight, obj1.MaxWidth, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", srcbucketfilepath, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Small_" + Filedetails.FileGuid + ".jpg"))
                                            {
                                                if (MH.GereratePreview(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", BigThumbMaxHight, BigThumbMaxWidth, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", srcbucketfilepath, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Big_" + Filedetails.FileGuid + ".jpg"))
                                                {
                                                    if (!blnFileSystemsMode)
                                                    {
                                                        string bigfilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Big_" + Filedetails.FileGuid + ".jpg");
                                                        string EnlargefilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                        bool fileexists = System.IO.File.Exists(bigfilePath);
                                                        if (fileexists)
                                                            System.IO.File.Copy(bigfilePath, EnlargefilePath, true);
                                                    }
                                                    else if (blnFileSystemsMode)
                                                    {
                                                        bool fileexists = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Big_" + Filedetails.FileGuid + ".jpg");
                                                        if (fileexists)
                                                        {
                                                            AWSHelper.CopyS3Object(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Big_" + Filedetails.FileGuid + ".jpg", marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                        }
                                                    }
                                                    //string fileinfo = "";
                                                    //fileinfo = MH.getFileInfo(obj1.SourcePath);
                                                    clsDb.MailData("UPDATE DAM_file SET [Status] = 2,Additionalinfo='" + fileinfo.ToString() + "' WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                                    //clsDb.MailData("UPDATE DAM_file SET [Status] = 2 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                                }
                                                else
                                                    clsDb.MailData("UPDATE DAM_file SET [Status] = 100 WHERE Id='" + obj1.Id + "'", CommandType.Text);

                                            }
                                            else
                                            {
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                            }
                                        }
                                        else if (Convert.ToInt32(Filedetails.Status) == 100 && ProcessorID == 1)
                                        {
                                            if (MH.GereratePreview(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", BigThumbMaxHight, BigThumbMaxWidth, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg"))
                                            {
                                                //string fileinfo = "";
                                                //fileinfo = MH.getFileInfo(obj1.SourcePath);
                                                if (!blnFileSystemsMode)
                                                {
                                                    string bigfilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Big_" + Filedetails.FileGuid + ".jpg");
                                                    string EnlargefilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                    bool fileexists = System.IO.File.Exists(bigfilePath);
                                                    if (fileexists)
                                                        System.IO.File.Copy(bigfilePath, EnlargefilePath, true);
                                                }
                                                else if (blnFileSystemsMode)
                                                {
                                                    bool fileexists = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DamOriginal) + "Big_" + Filedetails.FileGuid + ".jpg");
                                                    if (fileexists)
                                                    {
                                                        AWSHelper.CopyS3Object(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DamOriginal) + "Big_" + Filedetails.FileGuid + ".jpg", marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DamOriginal) + "Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                    }
                                                }
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 2 ,Additionalinfo='" + fileinfo.ToString() + "' WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                            }
                                            else
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + obj1.Id + "'", CommandType.Text);

                                        }
                                        else if (ProcessorID == 3)
                                        {
                                            //LogHandler.LogInfo("beforecallingPreviewVideo " + obj1.SourcePath, LogHandler.LogType.Notify);
                                            clsDb.MailData("UPDATE DAM_file SET [Status] = 1 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                            if (MH.GereratePreviewVideo(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Small_" + Filedetails.FileGuid + ".jpg", "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + Filedetails.FileGuid + ".mp4", SmallThumbMaxWidth, SmallThumbMaxHight, VideoPerviewStartTime, VideoPerviewEndTime, VideoResolutionWidth, VideoResolutionHeight, OutputVideoCodec, NumberOfThumbnail, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", srcbucketfilepath, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Small_" + Filedetails.FileGuid + ".jpg"))
                                            {
                                                //LogHandler.LogInfo("aftercallingPreviewVideo sucess" + obj1.SourcePath, LogHandler.LogType.Notify);
                                                if (!blnFileSystemsMode)
                                                {
                                                    string bigfilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Big_" + Filedetails.FileGuid + ".jpg");
                                                    string EnlargefilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, @"DAMFiles\Preview\Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                    bool fileexists = System.IO.File.Exists(bigfilePath);
                                                    if (fileexists)
                                                        System.IO.File.Copy(bigfilePath, EnlargefilePath, true);
                                                }
                                                else if (blnFileSystemsMode)
                                                {
                                                    bool fileexists = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Big_" + Filedetails.FileGuid + ".jpg");
                                                    if (fileexists)
                                                    {
                                                        AWSHelper.CopyS3Object(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Big_" + Filedetails.FileGuid + ".jpg", marcomManager.User.AwsStorage.BucketName, ReadS3TenantBasePath(tenantID, (int)AWSFilePathIdentity.DAMPreview) + "Enlarge_" + Filedetails.FileGuid + ".jpg");
                                                    }
                                                }

                                                //LogHandler.LogInfo("beforecallingVideoFileInfo" + obj1.SourcePath, LogHandler.LogType.Notify);
                                                videofileinfo = MH.getVideoFileInfo(obj1.SourcePath, blnFileSystemsMode, awsAccesskeyID, awsSecretAccesskey, awsRegionEndPoint, awsBusketName, "", srcbucketfilepath);
                                                //LogHandler.LogInfo("VideoFileInfo:" + videofileinfo.ToString(), LogHandler.LogType.Notify);
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 2 ,Additionalinfo='" + videofileinfo.ToString() + "' WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                            }
                                            else
                                            {
                                                LogHandler.LogInfo("aftercallingPreviewVideo fail" + obj1.SourcePath, LogHandler.LogType.Notify);
                                                clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                            }
                                        }
                                        else
                                        {

                                            clsDb.MailData("UPDATE DAM_file SET [Status] = 2  WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        clsDb.MailData("UPDATE DAM_file SET [Status] = 3  WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                        LogHandler.LogInfo("exception generated Preview sending files" + ex.Message, LogHandler.LogType.Notify);
                                        LogHandler.LogInfo("exception generated Preview sending files" + ex.StackTrace, LogHandler.LogType.Notify);
                                        LogHandler.LogInfo("exception generated Preview sending files" + ex.InnerException.ToString(), LogHandler.LogType.Notify);
                                    }
                                    //else if (Convert.ToInt32(Filedetails.Status) == 2 && ProcessorID == 1)
                                    //{
                                    //    if (MH.GereratePreview(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", BigThumbMaxHight, BigThumbMaxWidth))
                                    //    {
                                    //        clsDb.MailData("UPDATE DAM_file SET [Status] = 3 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                    //    }
                                    //    else
                                    //        clsDb.MailData("UPDATE DAM_file SET [Status] = 4  WHERE Id='" + obj1.Id + "'", CommandType.Text);

                                    //}
                                    //else if (Convert.ToInt32(Filedetails.Status) == 0 && ProcessorID == 2)
                                    //{
                                    //    if (MH.GereratePreviewOffice(obj1.SourcePath, obj1.DestinationPath, obj1.MaxHight, obj1.MaxWidth))
                                    //    {
                                    //        if (MH.GereratePreviewOffice(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", BigThumbMaxHight, BigThumbMaxWidth))
                                    //        {
                                    //            clsDb.MailData("UPDATE DAM_file SET [Status] = 3 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                    //        }
                                    //        else
                                    //            clsDb.MailData("UPDATE DAM_file SET [Status] = 2 WHERE Id='" + obj1.Id + "'", CommandType.Text);

                                    //    }
                                    //    else
                                    //    {
                                    //        clsDb.MailData("UPDATE DAM_file SET [Status] = 4  WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                    //    }
                                    //}
                                    //else if (Convert.ToInt32(Filedetails.Status) == 2 && ProcessorID == 2)
                                    //{
                                    //    if (MH.GereratePreviewOffice(obj1.SourcePath, "\\\\" + strIPAddress + "\\" + applicationPath + DestinationFolder + "Big_" + Filedetails.FileGuid + ".jpg", BigThumbMaxHight, BigThumbMaxWidth))
                                    //    {
                                    //        clsDb.MailData("UPDATE DAM_file SET [Status] = 3 WHERE Id='" + obj1.Id + "'", CommandType.Text);
                                    //    }
                                    //    else
                                    //        clsDb.MailData("UPDATE DAM_file SET [Status] = 4  WHERE Id='" + obj1.Id + "'", CommandType.Text);

                                    //}


                                }
                                catch (Exception ex)
                                {
                                    //Debug.WriteLine(ex.Message);
                                    //Debug.WriteLine(ex.StackTrace);
                                    //Debug.WriteLine(ex.InnerException.ToString());                                    
                                    LogHandler.LogInfo("exception generated Preview sending files" + ex.Message, LogHandler.LogType.Notify);
                                    LogHandler.LogInfo("exception generated Preview sending files" + ex.StackTrace, LogHandler.LogType.Notify);
                                    LogHandler.LogInfo("exception generated Preview sending files" + ex.InnerException.ToString(), LogHandler.LogType.Notify);
                                }

                            }

                            tx.Commit();
                        }


                    }
                    else
                    {
                        LogHandler.LogInfo("MH not responding:", LogHandler.LogType.Notify);
                    }


                }

                //return obj1;
            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("exception generated Preview sending files" + ex.Message, LogHandler.LogType.Notify);
                // return null;
            }
        }

        public static string ReadS3TenantBasePath(int TenantID, int identity = 0)
        {
            TenantSelection tfp = new TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePath(TenantID);
            if (identity != 0)
            {
                if ((int)AWSFilePathIdentity.DamOriginal == identity)
                    return (TenantFilePath + @"DAMFiles\Original\");
                else if ((int)AWSFilePathIdentity.DAMPreview == identity)
                    return (TenantFilePath + @"DAMFiles\Preview\");
                else return TenantFilePath;
            }
            else return TenantFilePath;
        }

        public class PreviewHolder
        {
            public int MaxWidth { get; set; }
            public int MaxHight { get; set; }
            public string Extension { get; set; }
            public string SourcePath { get; set; }
            public string DestinationPath { get; set; }
            public string Filename { get; set; }
            public int Id { get; set; }

        }

      

    }


}
