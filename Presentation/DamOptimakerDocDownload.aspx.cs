﻿using BrandSystems.Marcom.Core.OptimakerServices;
using BrandSystems.Marcom.Dal.DAM.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;

namespace Presentation
{
    public partial class DamOptimakerDocDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPath = null;
            string tenanturl = HttpContext.Current.Request.Url.Host;
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePathByHostName(tenanturl);

            try
            {
                if (Request.QueryString["resol"] == "H")
                {
                    int TenantID = tfp.GetTenantIDByHostName(tenanturl);
                    downloadHighrespdf(TenantID, TenantFilePath);
                    strPath = Server.MapPath("~/" + TenantFilePath + "/DAMFiles/Templates/MediaGenerator/HighResPdf/" + Request.QueryString["FileName"]);
                }
                else if (Request.QueryString["resol"] == "L")
                {
                    strPath = Server.MapPath("~/" + TenantFilePath + "/DAMFiles/Templates/MediaGenerator/LowResPdf/" + Request.QueryString["FileName"]);

                }
                string fileName = Request.QueryString["FileName"];
                if ((fileName != null))
                {
                    if (fileName.Length > 0)
                    {
                        if (File.Exists(strPath))
                        {
                            DownloadFile(strPath, true);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs1 = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs1))
                {
                    sw.WriteLine("PDF source location ---- > " + ex);
                    sw.WriteLine("----------------");
                }

            }
        }

        private void HighResCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(" -- HighRes PDF Download completed from approval-- ");

            }
        }
        private void DownloadFile(string fname, bool forceDownload)
        {
            try
            {
                string fullpath = System.IO.Path.GetFullPath(fname);
                string strFriendlyName = Request.QueryString["FileName"];
                string downloadTokenValue = Request.QueryString["token"];

                string name = System.IO.Path.GetFileName(fullpath);
                name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
                string ext = System.IO.Path.GetExtension(fullpath);
                string type = "";

                switch (ext)
                {
                    case ".htm":
                    case ".html":
                        type = "text/HTML";
                        break;
                    case "-":
                        type = "text/HTML";
                        break;
                    case ".txt":
                        type = "text/plain";
                        break;
                    case ".doc":
                    case ".rtf":
                    case ".docx":
                        type = "Application/msword";
                        break;
                    case ".csv":
                    case ".xls":
                    case ".xlsx":
                        type = "Application/x-msexcel";
                        break;
                    case ".pdf":
                        type = "Application/pdf";
                        break;
                    case ".tiff":
                        type = "image/tiff";
                        break;
                    case ".bmp":
                        type = "image/bmp";
                        break;
                    case ".jpeg":
                        type = "image/jpeg";
                        break;
                    case ".jpg":
                        type = "image/jpeg";
                        break;
                    case ".png":
                        type = "image/png";
                        break;
                    case ".gif":
                        type = "image/gif";
                        break;
                    case ".zip":
                        type = "Application/zip";
                        break;
                    default:
                        type = "application/octet-stream";
                        break;
                }

                if ((forceDownload))
                {
                    using (FileStream fs = File.Open(fullpath, FileMode.Open, FileAccess.ReadWrite))
                    {

                        string clength = fs.Length.ToString(CultureInfo.InvariantCulture);
                        HttpResponse response = HttpContext.Current.Response;
                        response.ContentType = type;
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", downloadTokenValue)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                        response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                        if (fs.Length != -1) response.AddHeader("Content-Length", clength);
                        response.ClearContent();
                        fs.CopyTo(response.OutputStream);
                        response.OutputStream.Flush();
                        response.End();
                    }


                }
                if (!string.IsNullOrEmpty(type))
                {
                    using (FileStream fs = File.Open(fullpath, FileMode.Open))
                    {

                        string clength = fs.Length.ToString(CultureInfo.InvariantCulture);
                        HttpResponse response = HttpContext.Current.Response;
                        response.ContentType = type;
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", downloadTokenValue)); //downloadTokenValue will have been provided in the form submit via the hidden input field
                        response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                        if (fs.Length != -1) response.AddHeader("Content-Length", clength);
                        response.ClearContent();
                        fs.CopyTo(response.OutputStream);
                        response.OutputStream.Flush();
                        response.End();
                    }

                }

            }
            catch (Exception ex)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs1 = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs1))
                {
                    sw.WriteLine("DownloadFile ---- > " + ex);
                    sw.WriteLine("----------------");
                }

            }
        }

        public DamOptimakerDocDownload()
        {
            Load += Page_Load;
        }

        public void downloadHighrespdf(int tenantID, string TenantFilePath)
        {
            int docversionid = int.Parse(Request.QueryString["docversionid"]); 
            string DAMHighResPDFFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/" + TenantFilePath + "/DAMFiles/Templates/MediaGenerator/HighResPdf/"), Request.QueryString["FileName"]);
            GetDataFromOptimakerDB objOptiDB = new GetDataFromOptimakerDB();
            DataSet ds = objOptiDB.GetFilesMarcom(docversionid);
            string optimakerfilelink = ds.Tables[0].Rows[0]["pdf_highres"].ToString();
            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs1 = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs1))
            {
                sw.WriteLine("PDF source location ---- > " + optimakerfilelink);
                sw.WriteLine("----------------");
            }
            WebClient clientHighRes = new WebClient(); ;
            clientHighRes.DownloadFileCompleted += new AsyncCompletedEventHandler(HighResCompleted);
            clientHighRes.DownloadFile(new Uri(optimakerfilelink), DAMHighResPDFFilePath);
            
        }
    }


}