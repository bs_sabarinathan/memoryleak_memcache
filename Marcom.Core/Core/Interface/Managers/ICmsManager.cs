﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Cms.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface ICmsManager
    {
        int InsertCMSEntity(int Version, string Description, int level, string Name, int NavID, int ParentID, string PublishedDate, string PublishedTime, int TemplateID, string UniqueKey, string Tag);

        IList GetAllCmsEntitiesByNavID(int NavID, int StartpageNo, int MaxPageNo);

        bool DeleteCmsEntity(int ID);

        int InsertRevisedEntityContent(int EntityID, string Content);

        int UpdateRevisedEntityContent(int EntityID, string Content);

        bool DeleteRevisedEntityContentID(int ID);

        IRevisedEntityContent GetRevisedContentByFeature(int EntityID);

        ICmsEntity GetCmsEntityAttributeDetails(int CmsEntityID);

        int UpdateCmsEntityDetailsBlockValues(int CmsEntityID, string NewValue, string attrName);

        bool SaveUploaderImage(string sourcepath, string destinationfolder, int imgwidth, int imgheight, int imgX, int imgY);

        bool SaveCMSEntityColor(string Description, string Colorcode, int TenantID);

        IList GetCmsEntityPageAccess(int EntityID);

        bool UpdateCmsEntityPageAccess(int[] RoleIDs, int CmsEntityID);

        IList GetCmsEntityPublishVersion(int CmsEntityID);

        IList<IFeedSelection> GettingCmsFeedsByEntityID(string entityId, int pageNo, bool isForRealTimeUpdate, int entityIdForReference, int newsfeedid = 0, string newsfeedgroupid = "");

        bool IsActiveEntity(int EntityID);
        IList GetCMSBreadCrum(int CmsEntityID);

        ArrayList DuplicateEntity(int CmsEntityID, int ParentLevel, int DuplicateTimes, bool IsDuplicateChild, List<string> dupEntityName);

        IList<ICmsEntity> GetCmsEntitiesByID(int[] CmsEntityID);

        bool GetIsEditFeatureEnabled();


        IList<ICmsSnippetTemplate> GetAllCmsSnippetTemplates();

        bool DeleteCmsSnippetTemplate(int templateID);

        int InsertCmsSnippetTemplate(int id, string content, bool active, string thumbnailguid, string templateHTML);

        int UpdateSnippetTemplate(int id, string content, bool active, string thumbnailguid);

        IList<object> LinkpublishedfilestoCMS(int[] assetid, int EntityID, int FolderID, string AttachType);

        int DuplicateCmsSnippetTemplate(string content, string defaultFirstContent, string defaultLastContent, string thumbnailguid);
    }
}
