///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail/ganttview-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailganttviewCtrl($scope, $timeout, $http, $resource, $compile, $window, $translate, $stateParams, CcganttviewService) {
        $scope.IsPageLoad = false;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.copyrightYear = (new Date()).getFullYear();
        $scope.OnganttPeriodChange = function () {
            $scope.IsPageLoad = true;
            $scope.settings.Mode = $stateParams.zoomlevel;
            $scope.IsPageLoad = false;
            GetAllGanttHeaderBar();
            lazyLoadGantt();
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.ganttviewCtrl']"));
        });
        $scope.GanttDetail = {
            GanttStartDate: "",
            GanttEndDate: "",
            GanttDescription: ""
        }
        $scope.data = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var dataGantEdit = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var IsResized = false;
        $scope.GanttHeaderList = null;

        function GetAllGanttHeaderBar() {
            CcganttviewService.GetAllGanttHeaderBar().then(function (getGanttresult) {
                if (getGanttresult.Response != null) {
                    $scope.GanttHeaderList = getGanttresult.Response;
                }
                drawHeader($scope.dates);
                ScrollToCurrentDate();
            });
        }

        function getCaptionfromPerod(Id) {
            CcganttviewService.GetCaptionofPeriod(Id).then(function (CaptionResponse) {
                $scope.CaptionforPeriod = CaptionResponse.Response;
            });
        }
        $scope.GanttCurrentDateCollections = [{}];
        $scope.settings = {
            Mode: 'monthly',
            MonthlyDayWidth: 30,
            YearlyDayWidth: 2,
            QuarterlyDayWidth: 6,
            WeeklyDayWidth: 16,
            VerticallyConnected: null,
            HorizontallyConnected: null,
            DataColumnWidth: 100,
            StartDate: $scope.Gnttsettings.StartDate,
            EndDate: $scope.Gnttsettings.EndDate,
            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                onClick: function (data, BlockID) {
                    if (IsResized == false) {
                        $scope.GanttCurrentDateCollections = {};
                        CcganttviewService.GetEntityPeriod(data.id).then(function (ganttDateRes) {
                            if (ganttDateRes.Response != null) {
                                $scope.GanttCurrentDateCollections = [{
                                    Description: "",
                                    EndDate: "",
                                    Entityid: 0,
                                    Id: 0,
                                    SortOrder: 0,
                                    Startdate: ""
                                }]
                                $scope.GanttCurrentDateCollections.splice(0, 1);
                                for (var i = 0; i < ganttDateRes.Response.length; i++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = ganttDateRes.Response[i].Startdate.substr(6, (ganttDateRes.Response[i].Startdate.indexOf('+') - 6));
                                    datstartval = new Date(parseInt(datStartUTCval));
                                    datEndUTCval = ganttDateRes.Response[i].EndDate.substr(6, (ganttDateRes.Response[i].EndDate.indexOf('+') - 6));
                                    datendval = new Date(parseInt(datEndUTCval));
                                    $scope.GanttCurrentDateCollections.push({
                                        Description: ganttDateRes.Response[i].Description,
                                        EndDate: ConvertStringToDate(ConvertDateToString(datendval)),
                                        Entityid: ganttDateRes.Response[i].Entityid,
                                        Id: ganttDateRes.Response[i].Id,
                                        SortOrder: ganttDateRes.Response[i].SortOrder,
                                        Startdate: ConvertStringToDate(ConvertDateToString(datstartval))
                                    });
                                }
                            }
                        });
                        dataGantEdit.SortOid = data.SortOid
                        dataGantEdit.order = data.order
                        dataGantEdit.uniqueId = data.uniqueId
                        dataGantEdit.id = data.id
                        dataGantEdit.BlockID = BlockID
                        $("#moduleContextGantt").modal('show');
                    }
                    IsResized = false;
                },
                onDrag: function (data) {
                    EntityPeriodUpdate(data);
                },
                onResize: function (data) {
                    EntityPeriodUpdate(data);
                    IsResized = true;
                }
            },
        };
        $scope.AddNextDate = function (index, ID) {
            $scope.GanttCurrentDateCollections.splice((index + 1), 0, {
                Description: "",
                EndDate: "",
                Entityid: 0,
                Id: 0,
                SortOrder: 0,
                Startdate: ""
            });
        }
        $scope.RemoveDate = function (index, ID) {
            if ($scope.GanttCurrentDateCollections.length > 1) {
                if (ID > 0) {
                    $scope.deletePeriodDate(ID);
                }
                $scope.GanttCurrentDateCollections.splice(index, 1);
            } else {
                $scope.GanttCurrentDateCollections.splice(index, 1, {
                    Description: "",
                    EndDate: "",
                    Entityid: 0,
                    Id: 0,
                    SortOrder: 0,
                    Startdate: ""
                });
            }
        }
        $scope.deletePeriodDate = function (periodid, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_4090.Caption'), function (result) {
                if (result) {
                    CcganttviewService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                        if (deletePerById.StatusCode == 200) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            UpdateEntityPeriodTree(dataGantEdit.id);
                            $scope.GanttsaveDetail();
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                        }
                    });
                }
            });
        }

        function UpdateEntityPeriodTree(entityid) {
            CcganttviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (resultperiod) {
                if (resultperiod.StatusCode == 200) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                return false;
                            } else {
                                if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = resultperiod.Response;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
        }
        $scope.GanttRedrawDetail = function (data) {
            dataGantEdit.start = data.start;
            dataGantEdit.end = data.end;
            dataGantEdit.desc = data.desc;
            var block = $('.ganttview-data-block[data-dateblockuniqueid="' + data.order + '-' + data.uniqueId + '"]');
            block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + ConvertDateFromStringToString(data.start) + "</br><b>End Date:</b> " + ConvertDateFromStringToString(data.end) + " </br><b>Comment:</b> " + data.desc + "").qtip({
                content: {
                    text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                },
                style: {
                    classes: "qtip-dark qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
						event: 'unfocus click mouseleave mouseup mousedown'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(100);
                    },
                    event: 'unfocus click mouseleave mouseup mousedown'
                },
                position: {
                    my: 'bottom left',
                    at: 'top left',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom left',
                        tooltip: 'bottom left',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
        }
        $scope.GanttsaveDetail = function () {
            var data = dataGantEdit;
            if (DateCollectionValidate(data)) {
                var UpdateEntityPeriod = {};
                UpdateEntityPeriod.EntityID = data.id;
                UpdateEntityPeriod.GanttCurrentDateCollections = $scope.GanttCurrentDateCollections;
                CcganttviewService.InsertUpdateEntityPeriodLst(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                    if (EntityPeriodResult.Response.length > 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $("#moduleContextGantt").modal('hide');
                        var constructUniquekey = '';
                        var CurrentUniquekey = '';
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                CurrentUniquekey = $scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"];
                                if ($scope.ListViewDetails[i].data.Response.Data[j]["TypeID"] == 5) {
                                    constructUniquekey = CurrentUniquekey + "-";
                                    CurrentUniquekey = '';
                                }
                                var UniqueKey = constructUniquekey + UniqueKEY(CurrentUniquekey);
                                if (UniqueKey == data.uniqueId) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                    var ganttDrawHtml = drawSpecifiedBlock($scope.ListViewDetails[i].data.Response.Data[j]);
                                    $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + data.uniqueId + '"]').html(ganttDrawHtml);
                                    return false;
                                }
                            }
                        }
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
            }
        }

        function DateCollectionValidate() {
            var IsOverlap = false;
            if ($scope.GanttCurrentDateCollections.length > 1) {
                for (var i = 1; i < $scope.GanttCurrentDateCollections.length; i++) {
                    if (i != $scope.GanttCurrentDateCollections.length - 1) {
                        if ($scope.GanttCurrentDateCollections[i].Startdate < $scope.GanttCurrentDateCollections[i - 1].EndDate && $scope.GanttCurrentDateCollections[i].EndDate > $scope.GanttCurrentDateCollections[i + 1].Startdate) {
                            return IsOverlap
                        }
                    } else {
                        if ($scope.GanttCurrentDateCollections[i].Startdate < $scope.GanttCurrentDateCollections[i - 1].EndDate) {
                            return IsOverlap
                        }
                    }
                }
            }
            return true;
        }

        function drawSpecifiedBlock(data) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            var UniqueId = UniqueKEY(data["UniqueKey"]);
            var ClassName = GenateClass(UniqueId);
            ClassName += " mo" + UniqueId;
            blockDivPage += "<div data-over='true' data-EntityLevel='" + data["Level"] + "' data-uniquekey='" + UniqueId + "' class='" + ClassName + " ganttview-data-block-container'>";
            $($.parseXML(data['Period'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var endDt = $(this).attr('e');
                var duration = 0;
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                }
                startDt = ConvertDateFromStringToString(startDt);
                endDt = ConvertDateFromStringToString(endDt);
                if ($scope.GanttLock == true) {
                    if (data['Permission'] == 3 || data['Permission'] == 0) {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    } else {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    }
                } else {
                    blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                        id: parseInt(data['Id']),
                        order: parseInt($(this).attr("o")),
                        start: $(this).attr("s"),
                        end: $(this).attr("e"),
                        desc: $(this).attr("d"),
                        uniqueId: UniqueId,
                        SortOid: $(this).attr("sid")
                    }) + "'></div>";
                }
            });
            $($.parseXML(data['MileStone'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var MileStoneStatus = $(this).attr('ms');
                var MileStoneName = $(this).attr('n');
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                }
                if (startDt != undefined) {
                    var startDtFormat = ConvertDateFromStringToString(startDt);
                }
                var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
            });
            blockDivPage += '</div>';
            return blockDivPage;
        }

        function EntityPeriodUpdate(data) {
            var UpdateEntityPeriod = {};
            UpdateEntityPeriod.ID = data.order;
            UpdateEntityPeriod.StartDate = data.start;
            UpdateEntityPeriod.EndDate = data.end;
            UpdateEntityPeriod.Description = data.desc;
            CcganttviewService.UpdateEntityPeriod(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                if (EntityPeriodResult.Response.length > 0) {
                    var constructUniquekey = '';
                    var CurrentUniquekey = '';
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            CurrentUniquekey = $scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"];
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["TypeID"] == 5) {
                                constructUniquekey = CurrentUniquekey + "-";
                                CurrentUniquekey = '';
                            }
                            var UniqueKey = constructUniquekey + UniqueKEY(CurrentUniquekey);
                            if (UniqueKey == data.uniqueId) {
                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                $scope.GanttRedrawDetail(data);
                                return false;
                            }
                        }
                    }
                }
            });
        }
        $scope.dates = getDates($scope.settings.StartDate, $scope.settings.EndDate);
        var colStatus = false;
        GanttViewLoad();
        $scope.GanttViewHeaders = {};
        $("#EntitiesTree").on("onListViewTemplateCreation", function (event, GanttViewDataPageTemplate, GanttViewBlockPageTemplate) {
            GanttViewLoad()
            $(window).AdjustHeightWidth();
        });

        function GanttViewLoad() {
            CcganttviewService.CostCentreDetail($scope.NodeForward).then(function (NodeCnt) {
                $scope.ListViewDetails[0].data = NodeCnt;
                if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                if ($scope.ListTemplate.length > 0) {
                    CcganttviewService.ListSetting("GanttView", 5).then(function (ganttsetting) {
                        if (ganttsetting.Response.Attributes != null) {
                            $("#ganttChart #GanttDataContainer").html($scope.GanttViewDataPageTemplate);
                            $("#ganttChart #GanttBlockContainer").html($scope.GanttViewBlockPageTemplate);
                            GetAllGanttHeaderBar();
                            $('#ganttChart .ganttview-data').scroll(function () {
                                $("#ganttChart .ganttview-slide-container").scrollLeft($(this).scrollLeft());
                                $("#ganttChart .ganttview-list-container-data").scrollTop($(this).scrollTop());
                                $("#EntitiesTree").scrollTop($(this).scrollTop());
                            });
                            $scope.GanttViewHeaders = ganttsetting.Response.Attributes;
                            drawDataHeader(ganttsetting.Response.Attributes);
                            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                LoadGanttView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID);
                            }
                            ScrollToCurrentDate();
                        }
                    });
                }
                $(window).AdjustHeightWidth();
            });
        }

        function ScrollToCurrentDate() {
            var ToDay;
            var DaysToScroll;
            switch ($scope.settings.Mode) {
                case 'daily':
                    ToDay = new Date();
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.MonthlyDayWidth;
                    break;
                case 'quarterly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.QuarterlyDayWidth;
                    break;
                case 'weekly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.WeeklyDayWidth;
                    break;
                default:
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.YearlyDayWidth;
                    break;
            }
            $(".ganttview-data").scrollLeft(DaysToScroll);
        }

        function GetCalenderStartDate() {
            return new Date(parseInt((new Date).getFullYear()) - 1, 0, 1);
        }
        $scope.$on('onTreePageCreation', function (event, data) {
            LoadGanttView(data[0], data[1], data[2]);
        });

        function lazyLoadGantt() {
            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                LoadGanttView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID);
            }
        }

        function LoadGanttView(ListContentData, PageIndex, UniqueID) {
            var value = drawBlock(ListContentData.Response, UniqueID);
            if (UniqueID != "") {
                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').after(value.Block);
                $('#ganttChart tr[data-uniquekey="' + UniqueID + '"]').after(value.Data);
            } else {
                $('#ganttChart .ganttview-data-blocks-page[data-page="' + PageIndex + '"]').html(value.Block).removeAttr('style');
                $('#ganttChart tbody[data-page="' + PageIndex + '"]').html(value.Data).removeAttr('style');
            }
            $(window).AdjustHeightWidth();
            $(function () {
                $(".ganttview-data-block, .reached, .notreached ").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
							event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
            $timeout(function () {
                CallBehavior();
            }, 1000);
        }

        function CallBehavior() {
            apply($('#ganttChart'), $scope.settings);
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function daysBetween(start, end) {
            if (!start || !end) {
                return 0;
            }
            start = Date.parse(start);
            end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }
            return Math.round((end - start) / (1000 * 60 * 60 * 24));
        }

        function isWeekend(date) {
            return date.getDay() % 6 == 0;
        }

        function getWeek(d) {
            var target = new Date(d.valueOf());
            var dayNr = (d.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var jan4 = new Date(target.getFullYear(), 0, 4);
            var dayDiff = (target - jan4) / 86400000;
            var weekNr = 1 + Math.ceil(dayDiff / 7);
            return weekNr;
        }

        function getDates(start, end) {
            if (start == "NaN-NaN-NaN" || start == "") start = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            if (end == "NaN-NaN-NaN" || end == "") end = Date.parse('+1year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            start = Date.parse(start);
            end = Date.parse(end);
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start];
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) {
                    dates[next.getFullYear()] = [];
                }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }
            return dates;
        }

        function drawHeader(dates) {
            var totalW = 0;
            var mainHeaderDivContent = '';
            var subHeaderDivContent = '';
            var rowDivContent = '';
            var ganttHeaderBar = '';
            switch ($scope.settings.Mode) {
                case 'daily':
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.MonthlyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (isWeekend(dates[y][m][d])) {
                                                subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            } else {
                                                subHeaderDivContent += '<div class="ganttview-header-sub">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'monthly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.YearlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                }
                            }
                            mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">' + y + '</div>';
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'quarterly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            var Qno = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.QuarterlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                    if (Qno != Math.floor((parseInt(m) + 1) / 3)) {
                                        mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">Q' + (Qno + 1) + ' - ' + y + '</div>';
                                        yw = 0;
                                        Qno = Math.floor((parseInt(m) + 1) / 3);
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'weekly':
                    var possion = 1;
                    var weekno = getWeek(Date.parse($scope.settings.StartDate));
                    var weekLen = 0;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.WeeklyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (weekno != getWeek(dates[y][m][d])) {
                                                if ((possion % 2) == 1) {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                } else {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                }
                                                possion += 1;
                                                weekno = getWeek(dates[y][m][d]);
                                                weekLen = 0;
                                            }
                                            weekLen += $scope.settings.WeeklyDayWidth;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ((possion % 2) == 1) {
                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    } else {
                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                default:
            }
            $('#ganttChart .ganttview-header .ganttview-header-mains').html(mainHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-subs').html(subHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-block').html(ganttHeaderBar);
            $('#ganttChart .ganttview-header').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid-row').html(rowDivContent);
            $('#ganttChart #GanttBlockContainer').css("width", totalW + "px");
            $(function () {
                $(".qtip-header-block").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded"
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
							event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
        }

        function drawBlock(data, UniqueID) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            var Constructunique = "";
            if (UniqueID.length > 0) {
                Constructunique = UniqueID.substr(0, UniqueID.indexOf("-")) + ".";
            }
            var rootID = 0;
            if (data != undefined){
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = '';
                var IscostCenter = false;
                if (data.Data[i]["TypeID"] == 5) {
                    IscostCenter = true;
                }
                UniqueId = UniqueKEY(data.Data[i]["class"].toString());
                var GeneralUniquekey = UniqueKEY(data.Data[i]["class"].toString());
                var ClassName = GenateClass(GeneralUniquekey);
                ClassName += " mo" + UniqueId;
                blockDivPage += "<div data-iscostcentre='" + IscostCenter + "' data-over='true' data-EntityLevel='" + data.Data[i]["Level"] + "'  data-uniquekey='" + UniqueId + "' class='" + ClassName + " ganttview-data-block-container'>";
                $($.parseXML(data.Data[i]['Period'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var endDt = $(this).attr('e');
                    var duration = 0;
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                        case 'monthly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                            break;
                        case 'quarterly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                            break;
                        case 'weekly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                            break;
                        default:
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                    }
                    startDt = ConvertDateFromStringToString(startDt);
                    endDt = ConvertDateFromStringToString(endDt);
                    if ($scope.GanttLock == true) {
                        if (data.Data[i]['Permission'] == 3 || data.Data[i]['Permission'] == 0) {
                            blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: UniqueId,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        } else {
                            blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: UniqueId,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        }
                    } else {
                        blockDivPage += "<div data-name='" + data.Data[i]["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data.Data[i]['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                        updateganttlocksettings();
                    }
                });
                $($.parseXML(data.Data[i]['MileStone'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var MileStoneStatus = $(this).attr('ms');
                    var MileStoneName = $(this).attr('n');
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                        case 'monthly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                            break;
                        case 'quarterly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                            break;
                        case 'weekly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                            break;
                        default:
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                    }
                    if (startDt != undefined) {
                        var startDtFormat = ConvertDateFromStringToString(startDt);
                    }
                    var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                    blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
                });
                blockDivPage += '</div>';
                blockDivDataPage += "<tr data-iscostcentre='" + IscostCenter + "' data-EntityLevel='" + data.Data[i]["Level"] + "' data-over='true' data-uniquekey='" + UniqueId + "' class='" + ClassName + "' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivDataPage += "<td><span><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        if ($scope.GanttViewHeaders[k].Type != "5") blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                        else blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : dateFormat(data.Data[i][$scope.GanttViewHeaders[k].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                    }
                }
                blockDivDataPage += '</tr>';
            }
            }
            return {
                Block: blockDivPage,
                Data: blockDivDataPage
            };
        }

        function drawData(data, UniqueID) {
            var blockDivPage = '';
            var Constructunique = "";
            if (UniqueID.length > 0) {
                Constructunique = UniqueID.substr(0, UniqueID.indexOf("-")) + ".";
            }
            var rootID = 0;
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = '';
                var IscostCenter = false;
                if (data.Data[i]["TypeID"] == 5) {
                    IscostCenter = true;
                }
                UniqueId = UniqueKEY(data.Data[i]["class"].toString());
                var GeneralUniquekey = UniqueKEY(data.Data[i]["class"].toString());
                var ClassName = GenateClass(GeneralUniquekey);
                ClassName += " mo" + UniqueId;
                blockDivPage += "<tr data-iscostcentre='" + IscostCenter + "' data-EntityLevel='" + data.Data[i]["Level"] + "' data-over='true' data-uniquekey='" + UniqueId + "' class='" + ClassName + "' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivPage += "<td><span><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        if ($scope.GanttViewHeaders[k].Type != "5") blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                        else blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : dateFormat(data.Data[i][$scope.GanttViewHeaders[k].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                    }
                }
                blockDivPage += '</tr>';
            }
            return blockDivPage;
        }

        function drawDataHeader(ColumnDefs) {
            var HeaderHtml = '';
            var totalColumn = 0;
            for (var j = 0; j < ColumnDefs.length; j++) {
                if (ColumnDefs[j].Field != "68" && ColumnDefs[j].Type != "10") {
                    HeaderHtml += '<th><span>' + ColumnDefs[j].DisplayName + '</span></th>';
                    totalColumn += 1;
                }
            }
            $('#ganttChart #ganttviewDataHeader').html(HeaderHtml);
            $('.ganttview-left').width(totalColumn * 100);
            $('.ganttview-right').css('margin-left', (totalColumn * 100) + 'px');
        }

        function apply(parent, settings) {
            if (settings.behavior.clickable) {
                bindBlockClick(parent, settings.behavior.onClick);
            }
            if (settings.behavior.resizable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockResize(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'monthly':
                        bindBlockResize(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'quarterly':
                        bindBlockResize(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'weekly':
                        bindBlockResize(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    default:
                }
            }
            if (settings.behavior.draggable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockDrag(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'monthly':
                        bindBlockDrag(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'quarterly':
                        bindBlockDrag(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'weekly':
                        bindBlockDrag(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    default:
                }
            }
        }

        function bindBlockClick(parent, callback) {
            $("div.ganttview-data-block", parent).on("click", function () {
                var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                if (islock == 3 || islock == 0) {
                    $scope.IsLock = true;
                } else {
                    $scope.IsLock = false;
                }
                if ($scope.IsLock == true || $scope.GanttLock == true) {
                    block.css('position', 'absolute');
                    $("#moduleContextGantt").modal('hide');
                } else if (callback) {
                    if ($(this).attr('data-ResizeDrag') == 'true') {
                        $(this).removeAttr('data-ResizeDrag');
                    } else {
                        callback(JSON.parse($(this).attr("block-data")), $(this).attr('data-dateblockuniqueid'));
                    }
                }
            });
        }

        function bindBlockResize(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).resizable({
                grid: cellWidth,
                handles: "e,w",
                start: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    }
                },
                stop: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    } else {
                        var data = JSON.parse($(this).attr("block-data"));
                        updateDataAndPosition(parent, block, cellWidth, startDate);
                        if (DateValidate(JSON.parse($(this).attr("block-data")))) {
                            if (callback) {
                                callback(JSON.parse($(this).attr("block-data")));
                            }
                        } else {
                            bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                            var startDt = data.start;
                            var endDt = data.end;
                            var duration = 0;
                            var formLeft = 0;
                            switch (settings.Mode) {
                                case 'daily':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                                case 'monthly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                                    break;
                                case 'quarterly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                                    break;
                                case 'weekly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                                    break;
                                default:
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                            }
                            block.css('width', duration + 'px');
                            block.css('margin-left', formLeft + 'px');
                            block.attr('block-data', JSON.stringify(data));
                        }
                    }
                }
            });
        }

        function bindBlockDrag(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).draggable({
                axis: "x",
                grid: [cellWidth, cellWidth],
                start: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        block.removeAttr('position', 'relative');
                        return false;
                    } else {
                        block.css('position', 'relative');
                    }
                },
                stop: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1915.Caption'));
                        block.css('position', 'absolute');
                    } else {
                        ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings);
                    }
                }
            });
        }

        function ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings) {
            block.css('position', 'absolute');
            block.attr('data-ResizeDrag', 'true');
            var data = JSON.parse(block.attr("block-data"));
            updateDataAndPosition(parent, block, cellWidth, startDate);
            if (DateValidate(JSON.parse(block.attr("block-data")))) {
                data = JSON.parse(block.attr("block-data"));
                block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "").qtip({
                    content: {
                        text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                    },
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
							event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
                if (callback) {
                    callback(JSON.parse(block.attr("block-data")));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                var startDt = data.start;
                var endDt = data.end;
                var duration = 0;
                var formLeft = 0;
                switch (settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                }
                block.css('width', duration + 'px');
                block.css('margin-left', formLeft + 'px');
                block.attr('block-data', JSON.stringify(data));
            }
        }

        function updateDataAndPosition(parent, block, cellWidth, startDate) {
            startDate = Date.parse(startDate);
            var container = $("div.ganttview-slide-container", parent);
            var scroll = container.scrollLeft();
            var offset = block.offset().left - container.offset().left - 1 + scroll;
            var blockdata = JSON.parse(block.attr("block-data"));
            var daysFromStart = Math.round(offset / cellWidth);
            var newStart = startDate.clone().addDays(daysFromStart);
            blockdata.start = newStart.toString("yyyy-MM-dd");
            var width = block.outerWidth();
            var numberOfDays = Math.round(width / cellWidth) - 1;
            blockdata.end = newStart.clone().addDays(numberOfDays).toString("yyyy-MM-dd");
            block.attr('block-data', JSON.stringify(blockdata));
            block.css("top", "").css("left", "").css("position", "relative").css("margin-left", offset + "px");
        }

        function DateValidate(data) {
            var IsOverlap = false;
            var MainObject = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").parent();
            var currentHtml = MainObject.html();
            var childCount = MainObject.children(".ganttview-data-block").length;
            var NextjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").next().attr('block-data');
            var PrevjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").prev().attr('block-data');
            if (NextjsonData != undefined) {
                var NextObjectData = jQuery.parseJSON(NextjsonData);
            }
            if (PrevjsonData != undefined) {
                var PrevObjectData = jQuery.parseJSON(PrevjsonData);
            }
            if (childCount == 1) {
                IsOverlap = true;
            } else if (data.SortOid == 1) {
                if (data.end < NextObjectData.start) {
                    IsOverlap = true;
                }
            } else if (data.SortOid == childCount) {
                if (data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            } else if (childCount > 1 && data.SortOid < childCount) {
                if (data.end < NextObjectData.start && data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            }
            return IsOverlap
        }
        $scope.SetGanttLockCC = function () {
            var ganttlock = $("#CCGanttLock").hasClass("icon-lock");
            if (ganttlock == true) {
                $("#CCGanttLock").removeClass("icon-lock");
                $("#CCGanttLock").addClass("icon-unlock");
                $scope.GanttLock = false;
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
            } else {
                $("#CCGanttLock").removeClass("icon-unlock");
                $("#CCGanttLock").addClass("icon-lock");
                $scope.GanttLock = true;
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
            }
        }

        function updateganttlocksettings() {
            var ganttlock = $("#SetGanttLock").hasClass("icon-lock");
            if (ganttlock == false) {
                $timeout(function () {
                    CallBehavior();
                }, 1000);
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-draggable')
            } else {
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-draggable')
            }
        }
    }
    app.controller("mui.planningtool.costcentre.detail.ganttviewCtrl", ['$scope', '$timeout', '$http', '$resource', '$compile', '$window', '$translate', '$stateParams', 'CcganttviewService', muiplanningtoolcostcentredetailganttviewCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/default/detail/detailfilter-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetaildetailfilterCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $window, DetailfilterService, $translate) {
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
        };
        $scope.attributegroupTypeid = 0;
        $scope.treeNodeSelectedHolderValues = [];
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.deletefiltershow = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.EntityTypeID = 0;
        $scope.appliedfilter = "No filter applied";
        $scope.FilterFields = {};
        $scope.PeriodOptionValue = {};
        var d = new Date.create();
        d = "1990-01-01";
        var DateValidate = dateFormat(d, $scope.DefaultSettings.DateFormat);
        var elementNode = 'DetailFilter';
        var selectedfilterid = 0;
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {};
        $scope.TreePricing = [];
        $scope.items = [];
        $scope.FilterDataXML = [];
        if ($window.ActivityFilterName != '' && $window.ActivityFilterName != 'No filter applied') { } else {
            $window.ActivityFilterName = '';
        }
        var TypeID = 6;
        $scope.visible = false;
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.atributeGroupList = [];
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle($scope.FilterFields);
            $scope.FilterSettingsLoad();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#DetailFilterCreation").on("onDetailFilterCreation", function (event) {
            $scope.ClearScopeModle();
        });
        $("#plandetailfiltersettings").on("ClearScope", function (event, filterid) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.ClearScopeModle = function () {
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.ddlParententitytypeId = [];
            $scope.ddlEntitymember = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.attributegroupTypeid = 0;
            IsSave = 0;
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.ddlParententitytypeId = [];
            $scope.ddlEntitymember = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 13
            });
            if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                    if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                }
            }
            $scope.ShowPeriodOptions = false;
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var ApplyFilterobj = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $("#EntitiesTree").trigger("loadactivityfromfilter", [0, ApplyFilterobj, 1]);
            $("#plandetailfilter").trigger('ClearAndReApply');
        }

        function KeepAllStepsMarkedComplete() {
            $("#MyWizard ul.steps").find("li").addClass("complete");
            $("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $("#plandetailfiltersettings").on('loaddetailfiltersettings', function (event, TypeID) {
            $scope.FilterSettingsLoad();
        });
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            $scope.attributegroupTypeid = 0;
            $scope.EntityHierarchyTypesResult = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            if ($window.ActivityFilterName == '') {
                $window.ActivityFilterName = '';
            }
            $scope.dyn_Cont = '';
            $scope.Updatefilter = false;
            $scope.Applyfilter = true;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $("#dynamic_Controls").html('');
            $scope.atributesRelationList = [];
            $scope.atributesRelationListWithTree = [];
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.atributeGroupList = [];
            var OptionFrom = 0;
            var IsKeyword = "false";
            var IsEntityType = "false";
            var IsEntityMember = "false";
            var CurrentActiveVersion = 0;
            $scope.PeriodOptions = [{
                Id: 1,
                FilterPeriod: 'Between'
            }, {
                Id: 2,
                FilterPeriod: 'Within'
            }];
            var KeywordOptionsresponse;
            DetailfilterService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                KeywordOptionsresponse = KeywordOptionsResult.Response
                OptionFrom = KeywordOptionsresponse.split(',')[0];
                IsKeyword = KeywordOptionsresponse.split(',')[1];
                IsEntityType = KeywordOptionsresponse.split(',')[3];
                IsEntityMember = KeywordOptionsresponse.split(',')[4];
                CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                DetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    var IDList1 = new Array();
                    var filterattidsmeber = {};
                    filterattidsmeber.IDList = $window.ListofEntityID;;
                    filterattidsmeber.TypeID = TypeID;
                    filterattidsmeber.FilterType = 'DetailFilter';
                    filterattidsmeber.OptionFrom = OptionFrom;
                    filterattidsmeber.IsEntityMember = IsEntityMember
                    $scope.tagmemberOptions = [];
                    DetailfilterService.GettingFilterEntityMember(filterattidsmeber).then(function (entityyMemberRelation) {
                        $scope.entityMemberRelationdata = entityyMemberRelation.Response;
                        if (entityyMemberRelation.Response != null) {
                            $.each(entityyMemberRelation.Response, function (i, el) {
                                $scope.tagmemberOptions.push({
                                    "id": el.Id,
                                    "Name": el.FirstName + ' ' + el.LastName
                                });
                            });
                        }
                        var filterattids = {};
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'DetailFilter';
                        filterattids.OptionFrom = OptionFrom;
                        filterattids.IDList = $window.ListofEntityID;
                        filterattids.IsEntityMember = IsEntityMember
                        DetailfilterService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                            }
                            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    $scope.dyn_Cont += '</select></div></div>';
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    $scope.dyn_Cont += '</select></div></div>';
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                    var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                    });
                                    if (treecount12.length == 0) {
                                        var mm = $scope.atributesRelationList[i].AttributeId;
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                        $scope.FilterDataXML.push({
                                            "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                        });
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                    if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                        var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                        $scope.tagAllOptionsEntityStatus.data = [];
                                        if (EntityStatusOptions != null) {
                                            $.each(EntityStatusOptions, function (i, el) {
                                                $scope.tagAllOptionsEntityStatus.data.push({
                                                    "id": el.ID,
                                                    "text": el.StatusOptions,
                                                    "ShortDescription": el.ShortDesc,
                                                    "ColorCode": el.ColorCode
                                                });
                                            });
                                        }
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                        $scope.dyn_Cont += "</div></div>";
                                    } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" ";
                                        $scope.dyn_Cont += "data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \"";
                                        $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div></div>";
                                    } else {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += '<div class=\"controls\"> <select class=\"multiselect\" multiple="multiple" multiselect-dropdown ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].OptionValues \" data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model=\"FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '"\">';
                                        $scope.dyn_Cont += '</select></div></div>';
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span> " + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                    $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\"";
                                    $scope.dyn_Cont += "data-placeholder=\"Select " + $scope.atributesRelationList[i].DisplayName + "\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[" + i + "].OptionValues \"";
                                    $scope.dyn_Cont += "ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"";
                                    $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                    $scope.dyn_Cont += "</select>";
                                    $scope.dyn_Cont += "</div></div>";
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 10) {
                                    $scope.setoptions();
                                    $scope.items = [];
                                    $scope.items.push({
                                        startDate: '',
                                        endDate: ''
                                    });
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";
                                    $scope.dyn_Cont += "<div class=\"row-fluid\">";
                                    $scope.dyn_Cont += "<input class=\"sdate\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' id=\"items.startDate\"type=\"text\" value=\"\" name=\"startDate\" ng-click=\"Calanderopen($event," + $scope.FilterFields["DatePart_Calander_Open" + "items.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"FilterFields.DatePart_Calander_Open" + "items.startDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-model=\"items.startDate\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' value=\"\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" ng-click=\"Calanderopen($event," + $scope.FilterFields["DatePart_Calander_Open" + "items.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"FilterFields.DatePart_Calander_Open" + "items.endDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"  ng-change=\"OpenOptionsForPeriod()\"/>";
                                    $scope.dyn_Cont += "";
                                    $scope.dyn_Cont += "</div></div></div></div>";
                                    $scope.FilterFields["Period_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    $scope.setFieldKeys();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\"" + $scope.atributesRelationList[i].AttributeId + "\"><span>Period range : </span><div class=\"controls\">";
                                    $scope.dyn_Cont += "<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";
                                    $scope.ShowPeriodOptions = false;
                                    $scope.FilterFields["FilterFields.DatePart_Calander_Open" + "items.startDate"] = false;
                                    $scope.FilterFields["FilterFields.DatePart_Calander_Open" + "items.endDate"] = false;
                                }
                            }
                            if ($scope.atributesRelationList.length > 0) $scope.atributeGroupList = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeTypeId == 3 || e.AttributeTypeId == 4 || e.AttributeTypeId == 6 || e.AttributeTypeId == 7 || e.AttributeTypeId == 12
                            });
                            if (IsEntityType == "True") {
                                $scope.dyn_Cont += "<div class='control-group'>";
                                $scope.dyn_Cont += "<span>EntityType : </span><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input class='width2x' id='ddlChildren' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";
                                $scope.dyn_Cont += "</div></div>";
                            }
                            $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                return e.AttributeTypeId == 7
                            })
                            for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                    $scope.treePreviewObj = {};
                                    $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                    if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                        } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    } else {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    }
                                    $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                    $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                    $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                    $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                    $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                    $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                    $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.dyn_Cont += '</div></div></div>';
                                }
                            }
                            if (IsKeyword == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Keyword : </span><input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'></div>";
                            }
                            if (IsEntityMember == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Members </span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model='ddlEntitymember'  id= 'ddlChildrenmember'     ng-options=\"ndata.id as (ndata.Name)  for ndata in  tagmemberOptions \"></select></div>";
                            }
                            $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                            $scope.Applyfilter = true;
                            $scope.Updatefilter = false;
                            $scope.Deletefilter = false;
                            $scope.Savefilter = true;
                        });
                    });
                });
            });
        }
        $timeout(function () { $scope.FilterSettingsLoad() }, 100);
        $scope.OpenOptionsForPeriod = function () {
            $scope.PeriodOptionValue = {};
            $scope.PeriodOptionValue = 1;
            var startdate = $scope.items.startDate;
            var enddate = $scope.items.endDate;
            if (startdate != null && enddate != null) {
                if (startdate != undefined && enddate != undefined) {
                    $scope.ShowPeriodOptions = true;
                } else $scope.ShowPeriodOptions = false;
            } else $scope.ShowPeriodOptions = false;
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == '' || $scope.ngsaveasfilter == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                IsSave = 0;
                return false;
            }
            var ApplyFilterobj = [];
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            var filtertype = 1;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                FilterData.IsDetailFilter = 5;
                filtertype = 5;
            } else FilterData.IsDetailFilter = 1;
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            FilterData.WhereConditon = whereConditionData;
            DetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                $('#FilterSettingsModal').modal('hide');
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    $("#EntitiesTree").trigger("loadactivityfromfilter", [filterSettingsInsertresult.Response, ApplyFilterobj, filtertype]);
                    $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#plandetailfiltersettings").on('EditFilterSettingsByFilterID', function (event, filterId, typeId, filtertypeval) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId, filtertypeval);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId, filtertypeval) {
            $scope.ClearScopeModle();
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.filterSettingValues = [];
            selectedfilterid = filterId;
            DetailfilterService.GetFilterSettingValuesByFilertId(filterId).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if (filtertypeval != 5) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 && $scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) {
                                if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                    var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                        return e.AttributeId == parseInt(71);
                                    })[0].EntityStatusOptionValues;
                                    var seletedoptionvalue = [];
                                    for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
										return e.AttributeId == parseInt(71);
                                    }).length; sts++) {
                                        seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                            return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                        })[0]);
                                        if (seletedoptionvalue != null) {
                                            $.each(seletedoptionvalue, function (i, el) {
                                                $scope.tagAllOptionsEntityStatus.data.push({
                                                    "id": el.ID,
                                                    "text": el.StatusOptions,
                                                    "ShortDescription": el.ShortDesc,
                                                    "ColorCode": el.ColorCode
                                                });
                                            });
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                    }
                                } else {
                                    $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                                }
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 10) {
                            if ($scope.filterSettingValues.StartDate != null) {
                                $scope.ShowPeriodOptions = true;
                                $scope.PeriodOptionValue = $scope.filterSettingValues.FilterValues[i].Value;
                            }
                        }
                    }
                } else $scope.attributegroupTypeid = $scope.filterSettingValues.FilterValues[0].AttributeId;
                $scope.items.push({
                    startDate: $scope.filterSettingValues.StartDate,
                    endDate: $scope.filterSettingValues.EndDate
                });
                DetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                $scope.EntityHierarchyTypesResult = [];
                for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                    if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                        $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                        })[0]);
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult == undefined ? 0 : $scope.EntityHierarchyTypesResult[0] == undefined ? 0 : $scope.EntityHierarchyTypesResult;
                if (filterSettingsValues.Response.EntityMemberID != "" && filterSettingsValues.Response.EntityMemberID != null) {
                    for (var l = 0; l < filterSettingsValues.Response.EntityMemberID.split(',').length; l++) {
                        if (filterSettingsValues.Response.EntityMemberID.split(',')[l] != '0') {
                            if ($.grep($scope.tagmemberOptions, function (e) {
								return e.id == filterSettingsValues.Response.EntityMemberID.split(',')[l]
                            })[0] != undefined) {
                                var mval = filterSettingsValues.Response.EntityMemberID.split(',')[l];
                                if (mval != "" && mval != undefined) $scope.ddlEntitymember.push(parseInt(mval));
                            }
                        }
                    }
                }
                var startdateupdate = $scope.filterSettingValues.StartDate;
                var enddateupdate = $scope.filterSettingValues.EndDate;
                var editstartdate = startdateupdate.split('/');
                var editenddate = enddateupdate.split('/');
                startdateupdate = dateFormat(startdateupdate, $scope.DefaultSettings.DateFormat);
                enddateupdate = dateFormat(enddateupdate, $scope.DefaultSettings.DateFormat);
                if ($scope.filterSettingValues.StartDate == "" || $scope.filterSettingValues.EndDate == "") {
                    $scope.items.push({
                        startDate: "",
                        endDate: ""
                    });
                } else if ($scope.filterSettingValues.StartDate != DateValidate || $scope.filterSettingValues.EndDate != DateValidate) {
                    $scope.items.startDate = startdateupdate;
                    $scope.items.endDate = enddateupdate;
                } else {
                    $scope.items.push({
                        startDate: DateValidate,
                        endDate: DateValidate
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
            });
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            var ApplyFilterobj = [];
            var FilterData = {};
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = selectedfilterid;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            var filtertype = 1;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                filtertype = 5;
                FilterData.IsDetailFilter = 5;
            } else FilterData.IsDetailFilter = 1;
            if (whereConditionData.length == 0 && entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': '',
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            FilterData.WhereConditon = whereConditionData;
            if (selectedfilterid != 0) {
                ApplyFilterobj = whereConditionData;
            } else {
                ApplyFilterobj = [];
            }
            DetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response, filtertype]);
                    $("#EntitiesTree").trigger("loadactivityfromfilter", [filterSettingsInsertresult.Response, ApplyFilterobj, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.FilterID = 0;
        $("#plandetailfiltersettings").on('applyplandetailfilter', function (event, FilterID, FilterName, filtertypeval) {
            $scope.ApplyFilter(FilterID, FilterName, filtertypeval);
        });
        $scope.ApplyFilter = function (FilterID, FilterName, filtertypeval) {
            var filtertype = 1;
            if (FilterID != undefined && FilterID != 0) {
                $scope.FilterSettingsLoad();
                filtertype = filtertypeval;
            }
            $('#FilterSettingsModal').modal('hide');
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined) {
                selectedfilterid = FilterID;
                $scope.FilterID = FilterID;
            }
            $scope.appliedfilter = FilterName;
            $window.ActivityFilterName = FilterName;
            if (FilterID == 0) {
                $scope.appliedfilter = "No filter applied";
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        if ($scope.ngKeywordtext != '') whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext,
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                        else whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext,
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': '',
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                }
                            } else {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (entitytypeIdforFilter != '') {
                if ($scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext,
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                } else {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': '',
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                }
            }
            if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && ($scope.items.startDate != undefined || $scope.items.endDate != undefined)) {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': '',
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            var StartDate;
            var EndDate;
            var ApplyFilterobj = [];
            var optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate == undefined) optionperiod = 0;
            if ($scope.items.endDate == undefined) optionperiod = 0;
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate != undefined) StartDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
            if ($scope.items.endDate != undefined) EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
            if ($scope.items.startDate != undefined || $scope.items.endDate != undefined) whereConditionData.push({
                'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                'SelectedValue': optionperiod,
                'Level': 0,
                'AttributeTypeId': 10,
                'EntityTypeIDs': entitytypeIdforFilter,
                'Keyword': $scope.ngKeywordtext,
                'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                'EntityMemberIDs': entitymemberIdforFilter
            });
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 3 && res.AttributeId == SystemDefiendAttributes.Owner) {
                    for (var k = 0; k < res.Users.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.Users[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                ApplyFilterobj = whereConditionData;
                filtertype = 5;
            } else {
                if (FilterID != 0) {
                    ApplyFilterobj = whereConditionData;
                } else {
                    ApplyFilterobj = [];
                }
            }
            $("#EntitiesTree").trigger("loadactivityfromfilter", [FilterID, ApplyFilterobj, filtertype]);
            $(window).AdjustHeightWidth();
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            selectedfilterid = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var ApplyFilterobj = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_155.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = selectedfilterid;
                        DetailfilterService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $("#EntitiesTree").trigger("loadactivityfromfilter", [0, ApplyFilterobj, 1]);
                                $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, 'No filter applied', 0, 1]);
                            }
                        });
                    }, 100);
                }
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.detailfilterCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                branch.ischecked = false;
                if (branch.Children.length > 0) {
                    ClearRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function GetAllTreeObject(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                if (branch.Children.length > 0) {
                    FormRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
    }
    app.controller("mui.planningtool.default.detail.detailfilterCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$window', 'DetailfilterService', '$translate', muiplanningtooldefaultdetaildetailfilterCtrl]);
})(angular, app);
///#source 1 1 /app/services/ccganttview-service.js
(function(ng,app){function CcganttviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetCaptionofPeriod:GetCaptionofPeriod,GetEntityPeriod:GetEntityPeriod,DeleteEntityPeriod:DeleteEntityPeriod,GetEntitiPeriodByIdForGantt:GetEntitiPeriodByIdForGantt,InsertUpdateEntityPeriodLst:InsertUpdateEntityPeriodLst,UpdateEntityPeriod:UpdateEntityPeriod,CostCentreDetail:CostCentreDetail,ListSetting:ListSetting,GetAllGanttHeaderBar:GetAllGanttHeaderBar});function GetCaptionofPeriod(EntityTypeID){var request=$http({method:"get",url:"api/Planning/GetCaptionofPeriod/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityPeriod(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntityPeriod/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteEntityPeriod(ID){var request=$http({method:"delete",url:"api/Planning/DeleteEntityPeriod/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function GetEntitiPeriodByIdForGantt(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntitiPeriodByIdForGantt/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertUpdateEntityPeriodLst(formobj){var request=$http({method:"post",url:"api/Planning/InsertUpdateEntityPeriodLst/",params:{action:"add",},data:formobj});return(request.then(handleSuccess,handleError));}
function UpdateEntityPeriod(formobj){var request=$http({method:"put",url:"api/Planning/UpdateEntityPeriod/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function CostCentreDetail(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreDetail/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ListSetting(elementNode,entitytype){var request=$http({method:"get",url:"api/Metadata/ListSetting/"+elementNode+"/"+entitytype,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAllGanttHeaderBar(){var request=$http({method:"get",url:"api/common/GetAllGanttHeaderBar/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("CcganttviewService",['$http','$q',CcganttviewService]);})(angular,app);
///#source 1 1 /app/services/ccdetailfilter-service.js
(function(ng,app){"use strict";function CcdetailfilterService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetOptionsFromXML:GetOptionsFromXML,GettingEntityTypeHierarchyForAdminTree:GettingEntityTypeHierarchyForAdminTree,GettingFilterEntityMember:GettingFilterEntityMember,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings});function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingEntityTypeHierarchyForAdminTree(EntityTypeID,ModuleID){var request=$http({method:"get",url:"api/Metadata/GettingEntityTypeHierarchyForAdminTree/"+EntityTypeID+"/"+ModuleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterEntityMember(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterEntityMember/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){response.data.StatusCode=response.status;return(response.data);}}
app.service("CcdetailfilterService",['$http','$q',CcdetailfilterService]);})(angular,app);
