﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetRoleAccess
    {
        int ID { get; set; }
        int AssetID { get; set; }
        int RoleID { get; set; }
    }
}
