﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class AttributeGroupDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        protected string _description;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected bool _isPredefined;
        #endregion

        #region Constructors
        public AttributeGroupDao() { }

        public AttributeGroupDao(int pId, string pCaption, string pDescription, bool pIsPredefined)
        {
            this._id = pId;
            this._caption = pCaption;
            this._description = pDescription;
            this._isPredefined = pIsPredefined;
        }

        public AttributeGroupDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
                _bIsChanged |= (_caption != value);
                _caption = value;
            }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual bool IsPredefined
        {
            get { return _isPredefined; }
            set { _isPredefined = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttributeGroupDao = "AttributeGroupDao";
            public const string Id = "Id";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string IsPredefined = "IsPredefined";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttributeGroupDao = "AttributeGroupDao";
            public const string Id = "Id";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string IsPredefined = "IsPredefined";
        }

        #endregion

    }
}
