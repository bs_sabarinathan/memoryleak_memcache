using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// ObjectiveEntityValue object for table 'PM_ObjectiveEntityValue'.
	/// </summary>
	
	internal class ObjectiveEntityValue : IObjectiveEntityValue 
	{
		#region Member Variables

        protected int _id;
		protected int _objectiveid;
		protected int _entityid;
        protected decimal _plannedtarget;
        protected decimal _targetoutcome;
		protected int _ratingobjective;
		protected string _comments;
		protected int _status;
		protected int _fulfilment;
		
		
		#endregion
		
		#region Constructors
		public ObjectiveEntityValue() {}
			
		public ObjectiveEntityValue(int pId,int pObjectiveid, int pEntityid, int pPlannedTarget, int pTargetOutcome, int pRatingObjective, string pComments, int pStatus, int pFulfilment)
		{
            this._id = pId;
			this._objectiveid = pObjectiveid; 
			this._entityid = pEntityid; 
			this._plannedtarget = pPlannedTarget; 
			this._targetoutcome = pTargetOutcome; 
			this._ratingobjective = pRatingObjective; 
			this._comments = pComments; 
			this._status = pStatus; 
			this._fulfilment = pFulfilment; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
		public int Objectiveid
		{
			get { return _objectiveid; }
			set { _objectiveid = value; }
			
		}
		
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}

        public decimal PlannedTarget
		{
			get { return _plannedtarget; }
			set { _plannedtarget = value; }
			
		}

        public decimal TargetOutcome
		{
			get { return _targetoutcome; }
			set { _targetoutcome = value; }
			
		}
		
		public int RatingObjective
		{
			get { return _ratingobjective; }
			set { _ratingobjective = value; }
			
		}
		
		public string Comments
		{
			get { return _comments; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Comments", "Comments value, cannot contain more than 1073741823 characters");			  
			  _comments = value; 
			}
			
		}
		
		public int Status
		{
			get { return _status; }
			set { _status = value; }
			
		}
		
		public int Fulfilment
		{
			get { return _fulfilment; }
			set { _fulfilment = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			ObjectiveEntityValue castObj = null;
			try
			{
				castObj = (ObjectiveEntityValue)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _objectiveid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
