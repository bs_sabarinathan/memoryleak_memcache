﻿(function(ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailsectionCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, ObjectivesectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        setTimeout(function () { $scope.page_resize(); }, 100);
        $scope.IsCustomTabInObjective = true;
        $scope.TabCollectionsList = null;
        $scope.IsLock = true;
        $scope.EntityLockTask = false;
        ObjectivesectionService.GetLockStatus($stateParams.ID).then(function(entitylockstatus) {
            $scope.IsLock = entitylockstatus.Response.Item1;
        });
        $scope.set_bgcolor = function(clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            } 
            else {
                return '';
            }
        }
        $scope.set_color = function(clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            }
            else {
                return '';
            }
        }
        $scope.load = function(parameters) {
            $scope.LoadTab = function(tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                localStorage.setItem('lasttab', tab);
                $location.path('/mui/objective/detail/section/' + $stateParams.ID + '/' + tab + '');
            };
        };

        $scope.AttributeGroupById = 0;
        $scope.agroup = [];
        $scope.agroupListTabBlock = [];
        $scope.agroupkeyvaluepairTabBlock = [];

        function GetAttributeGroups() {
            $scope.LanguageContent = $scope.LanguageContents;
            $scope.AttrLock = $scope.IsLock;
            if ($scope.agroup.length > 0) {
                $scope.agroup.splice(0, $scope.agroup.length);
                $scope.agroupListTabBlock.splice(0, $scope.agroupListTabBlock.length);
                $scope.agroupkeyvaluepairTabBlock.splice(0, $scope.agroupkeyvaluepairTabBlock.length);
            }
            //         var GetAttributeGroupRelationByEnityTypeID = $resource('metadata/GetEntityTypeAttributeGroupRelation/:entitytypeId/:EntityID/:AttributeGroupId', { entitytypeId: 0, EntityID: parseInt($routeParams.ID), AttributeGroupId: $scope.AttributeGroupById }, { get: { method: 'GET' } });
            ObjectivesectionService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), $scope.AttributeGroupById).then(function (attributeGrpRelation) {
                //          var attributeGrpRelation = GetAttributeGroupRelationByEnityTypeID.get({ entitytypeId: 0, EntityID: parseInt($routeParams.ID) }, function () {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {
                        $scope.agroup = attributeGrpRelation.Response;
                        $scope.agroupListTabBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 3 && e.RepresentationType == true });
                        $scope.agroupkeyvaluepairTabBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 3 && e.RepresentationType == false });

                    }
                    if ($scope.agroupListTabBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("processlistganttintab" + ($scope.agroupListTabBlock[0].ID)) }, 200);
                    if ($scope.agroupkeyvaluepairTabBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("process" + ($scope.agroupkeyvaluepairTabBlock[0].ID)) }, 200);
                }
            });
        }

        $scope.CustomTabLoad = function (tab, tabUrl, IsSytemDefined, Name, AddEntityID, AttributeGroupId) {
            $scope.ShowCustomTabOnLoad = false;
            $scope.agroup.length = 0;
            $scope.agroupListTabBlock.length = 0;
            $scope.agroupkeyvaluepairTabBlock.length = 0;
            var customObj = { Name: Name, IsSysTab: IsSytemDefined, AttrGroupId: AttributeGroupId };
            localStorage.setItem('customTabName', JSON.stringify(customObj));
            if (IsSytemDefined == false) {
                $scope.AttributeGroupById = AttributeGroupId;
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                if (AttributeGroupId == 0 || AttributeGroupId == undefined) {
                    $scope.IsCustomTabInObjective = true;
                    ObjectivesectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function (gettabresult) {
                        if (gettabresult.Response != null) {
                            $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                            $timeout(function () {
                                $scope.ShowCustomTabOnLoad = true;
                            }, 750);
                        }
                    });
                }
                else {
                    $timeout(function () {
                        $scope.IsCustomTabInObjective = false;
                        GetAttributeGroups();
                        $scope.ShowCustomTabOnLoad = true;
                    }, 200);
                }
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $location.path('/mui/objective/detail/sectionCustom/customtab/' + tab + '/' + $stateParams.ID + '');
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }
        }
        localStorage.clear('customTabName');

        $scope.BreadCrumOverview = function(ID) {
            var actname = $('#breadcrumlink').text();
            actname = actname.substring(0, actname.length);
            $window.EntityTypeID = ID;
            $window.SubscriptionEntityTypeID = $('#breadcrumlink').attr('data-typeid');
            $window.GlobalUniquekey = $('#breadcrumlink').attr('data-uniquekey');
            $scope.RootLevelEntityName = actname;
            $scope.EntityShortText = $('#breadcrumlink').attr('data-shortdesc');
            $scope.EntityColorcode = $('#breadcrumlink').attr('data-colorcode');
            $location.path('/mui/objective/detail/section/' + ID);
            ObjectivesectionService.GetPath(ID).then(function(getbc) {
                $scope.array = getbc.Response;
            });
            $('#ObjectiveOverviewDiv').trigger('onObjOverViewDetail', ID);
        }
        $scope.loadBreadCrum = function(isOnload) {
            $("#plandetailfilterdiv").css('display', 'none')
            if ($("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").length == 0) {
                $scope.loadobjectivefromsearchByID($stateParams.ID, "100");
            }
            $("#EntitiesTree li.active").removeClass('active')
            $("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
            $scope.array = [];
            ObjectivesectionService.GetPath($stateParams.ID).then(function(getbc) {
                for (var i = 0; i < getbc.Response.length; i++) {
                    if (i == (getbc.Response.length - 1)) {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        var decodedEntityShortText = $('<div/>').html(getbc.Response[i].ShortDescription).text();
                        var decodedEntityBreadCrumTypeName = $('<div/>').html(getbc.Response[i].TypeName).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": getbc.Response[i].Name,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "999999"
                        });
                        $scope.EntityShortText = getbc.Response[i].ShortDescription;
                        $scope.EntityBreadCrumTypeName = getbc.Response[i].TypeName;
                        $scope.EntityColorcode = getbc.Response[i].ColorCode;
                        $scope.RootLevelEntityName = getbc.Response[i].Name;
                        $window.GlobalUniquekey = getbc.Response[i].UniqueKey;
                        $window.SubscriptionEntityTypeID = getbc.Response[i].TypeID;
                        $window.SubscriptionEntityID = getbc.Response[i].ID;
                        $scope.EntityTypeID = getbc.Response[i].TypeID;
                        if (getbc.Response[i].IsLock == "True") $scope.IsLock = true;
                        else $scope.IsLock = false;
                        $(window).trigger('LoadSubscription', $stateParams.ID);
                    } else {
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": getbc.Response[i].Name,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "0088CC"
                        });
                    }
                }
                GetAllTabCollections();
                if (isOnload == false) {
                    if ($scope.subview == "overview") {
                        $('#ObjectiveOverviewDiv').trigger('onObjOverViewDetail', $stateParams.ID);
                    } else if ($scope.subview == "member") {
                        $('#membersDivObj').trigger('LoadMembersDetailObj', $stateParams.ID);
                    } else if ($scope.subview == "attachment") {
                        $scope.$broadcast('LoadAttachment', $stateParams.ID);
                    } else if ($scope.subview == "presentation") {
                        $('#PresentationObj').trigger('LoadPresentationsObj', $stateParams.ID);
                    } else if ($scope.subview == "task") {
                        ResetTaskObjects();
                        $scope.$broadcast('LoadTask', $stateParams.ID);
                    }
                }
            });
        }
        $scope.DecodedTaskName = function(Name) {
            if (Name != null & Name != "") {
                return $('<div />').html(Name).text();
            } else {
                return "";
            }
        };
        $scope.loadBreadCrum(true);
        $scope.TaskExpandedDetails = [];
        $scope.SubLevelTaskExpandedDetails = [];
        $scope.SubLevelTaskLibraryListContainer = {
            SubLevelTaskLibraryList: []
        };
        $scope.RootLevelTaskLibraryListContainer = {
            RootLevelTaskLibraryList: []
        };
        $scope.groupbySubLevelTaskObj = [];
        $scope.ngExpandAll = true;
        $scope.ngExpandCollapseStatus = {
            ngExpandAll: true
        };
        $scope.TaskOrderListObj = {
            TaskSortingStatus: "Reorder task lists",
            TaskSortingStatusID: 1,
            TaskOrderHandle: false,
            SortOrderObj: [],
            UniqueTaskSort: "Reorder task",
            UniqueTaskSortingStatusID: 1,
            UniqueTaskOrderHandle: false,
            UniqueSortOrderObj: []
        };
        $scope.SubLevelEnableObject = {
            Enabled: false,
            SublevelText: "Show task(s) from sub-levels",
            SublevelTextStatus: 1
        };

        function GetAllTabCollections() {
            ObjectivesectionService.GetCustomEntityTabsByTypeID(10, $scope.EntityTypeID, $stateParams.ID, 0).then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID, currentObj[0].AttributeGroupID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function(item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID, currentObj[0].AttributeGroupID);
                    }
                    setTimeout(preselecttab, 200);
                }
            });
        }

        function preselecttab() {
            var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
            var curTabName = JSON.parse(localStorage.getItem('customTabName'));

            $("#SectionTabs li a").each(function () {
                try {
                    var src = $(this).attr("data-tabname");
                    var attrGroupId = $(this).attr("data-attrgroupid");
                    if (src.length > 0) {
                        if (curTabName != null && curTabName != undefined) {
                            if (!curTabName.IsSysTab) {
                                if (src == curTabName.Name) {
                                    if (attrGroupId == curTabName.AttrGroupId) {
                                        $("#SectionTabs li").removeClass('active');
                                        $(this).parents('li').removeClass('active');
                                        $(this).parents('li').addClass("active");
                                    }
                                }
                            }
                            else if (src == pgurl || src == '') {
                                $("#SectionTabs li").removeClass('active');
                                $(this).parents('li').removeClass('active');
                                $(this).parents('li').addClass("active");
                            }
                        }
                        else {
                            if (src == pgurl || src == '') {
                                $("#SectionTabs li").removeClass('active');
                                $(this).parents('li').removeClass('active');
                                $(this).parents('li').addClass("active");
                            }
                        }

                    }

                } catch (e) {

                }
            })
        }

        $scope.BindClass = function(index, tab, ID) {
            if (index == 0 && $scope.subview == null) {
                if ($scope.subview == null) {
                    var tabid = $scope.TabCollectionsList[index].ControleID;
                    $scope.subview = tabid;
                }
                return "active";
            } else if ($scope.subview == tab) {
                return "active";
            } else if (ID == $stateParams.tabID) {
                return "active";
            }
        }
        ResetTaskObjects();

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function(event, taskkist) {
            if (taskkist.length > 0) {
                $scope.MainTaskLiveUpdateIndex = 0;
                $scope.subTaskLiveUpdateIndex = 0;
                UpdateTaskData();
                UpdateSubLevelTaskData();
            }
        });

        $scope.$on('RefreshEntityTaskListCollection', function (event, taskobj) {
            $scope.LiveTaskListIDCollection.TasklistIDList = [{
                "TaskLiStID": taskobj.TaskListID,
                "EntityID": taskobj.EntityID
            }];
            InitiateTaskLiveupdateAction();
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.MainTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.TaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var taskListResObj = $.grep($scope.groups, function(e) {
                            return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                        });
                        if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                            if (tasklistList[0].TaskList.length > 0) {
                                taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                    if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateTaskData();
                }, 20);
            });
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }

        function LoadNextRenderContext() {
            // Get the render context local to this controller (and relevant params).
            $scope.thisContext = "";
            if ($scope.subview == null) {
                var a = window.location.hash.split('/');
                for (var i = 0; i < a.length; i++) {
                    if (a[i] == "section") {
                        $scope.thisContext = "DynamicObjective";
                    }

                }
            }
            if ($scope.thisContext == "" || $scope.thisContext == null) {
                if ($location.path().indexOf("sectionentity") != -1) {
                    $scope.thisContext = "EntityObjective";
                }
            }
        }

        $scope.$on("$destroy", function() {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.sectionCtrl']"));
        });
        $timeout(function() {
            $scope.load();
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
        $(window).on("UpdateObjectiveEntityName", function(event, optionarray) {
            $scope.RootLevelEntityName = optionarray.toString();
        });
    }
    app.controller("mui.planningtool.objective.detail.sectionCtrl", ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'ObjectivesectionService', muiplanningtoolobjectivedetailsectionCtrl]);
})(angular, app);