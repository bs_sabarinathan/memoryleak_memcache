using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Access.Model
{

    /// <summary>
    /// GlobalRoleDao object for table 'AM_GlobalRole'.
    /// </summary>
    public partial class GlobalRoleDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        protected string _description;
        protected bool _isassetaccess;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public GlobalRoleDao() { }

        public GlobalRoleDao(string pCaption, string pDescription, bool pIsAssetAccess)
        {
            this._caption = pCaption;
            this._description = pDescription;
            this._isassetaccess = pIsAssetAccess;
        }

        public GlobalRoleDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _bIsChanged |= (_caption != value); _caption = value; }

        }
        
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 4000)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 4000 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual bool IsAssetAccess
        {
            get { return _isassetaccess; }
            set { _isassetaccess |= (_isassetaccess != value); _isassetaccess = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string GlobalRoleDao = "GlobalRoleDao";
            public const string Id = "Id";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string IsAssetAccess = "IsAssetAccess";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string GlobalRoleDao = "GlobalRoleDao";
            public const string Id = "Id";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string IsAssetAccess = "IsAssetAccess";
        }

        #endregion


    }

}
