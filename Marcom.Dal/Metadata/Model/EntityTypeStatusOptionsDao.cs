using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// AttributeDao object for table 'MM_EntityTypeStatus_Options'.
    /// </summary>

    public partial class EntityTypeStatusOptionsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected string _statusoptions;
        protected int _sortorder;
        protected bool _isremoved;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected string _colorcode;
        #endregion

        #region Constructors
        public EntityTypeStatusOptionsDao() { }

        public EntityTypeStatusOptionsDao(int pID, int pEntityTypeID, string pStatusOptions, int pSortOrder, bool pIsRemoved, string pColorCode)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._statusoptions = pStatusOptions;
            this._sortorder = pSortOrder;
            this._isremoved = pIsRemoved;
            this._colorcode = pColorCode;
        }

        public EntityTypeStatusOptionsDao(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public virtual string StatusOptions
        {
            get { return _statusoptions; }
            set
            {
                _statusoptions = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set
            {
                _sortorder = value;
            }

        }
        public virtual bool IsRemoved
        {
            get { return _isremoved; }
            set { _isremoved = value;}
        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _colorcode = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityTypeStatusOptionsDao = "EntityTypeStatusOptionsDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string StatusOptions = "StatusOptions";
            public const string SortOrder = "SortOrder";
            public const string ColorCode = "ColorCode";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityTypeStatusOptionsDao = "EntityTypeStatusOptionsDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string StatusOptions = "StatusOptions";
            public const string SortOrder = "SortOrder";
            public const string ColorCode = "ColorCode";
        }

        #endregion


    }

}
