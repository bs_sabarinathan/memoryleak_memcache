﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;
//using Microsoft.SqlServer.Management.Common;
using System.Collections.Specialized;
using System.Configuration;
using NHibernate.Tool.hbm2ddl;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Interface;
using System.Xml;
using System.Web;
using log4net;
using System.Collections;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Web.UI;
using BrandSystems.Marcom.Dal.Base;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Utility;
namespace BrandSystems.Marcom.Utility
{
    public class DynamicTableLibrary
    {
        private static DynamicTableLibrary _instance = new DynamicTableLibrary();
        public static DynamicTableLibrary Instance
        {
            get { return _instance; }
        }

        private NHibernate.Cfg.Configuration cfg;




        public void Initialize()
        {
            cfg = new NHibernate.Cfg.Configuration();
            cfg.SetInterceptor(new QueryHintInterceptor());
            cfg.Configure();
            string conn = cfg.GetProperty(NHibernate.Cfg.Environment.ConnectionString);
            SqlConnection sqlConn = new SqlConnection(conn);
            //ServerConnection smoConn = new ServerConnection(sqlConn);
            //instanceServer = new Server(smoConn);
            //projectDatabase = instanceServer.Databases[sqlConn.Database];
        }


        public void UpdateTableUsingQuery(ITransaction tx, DynamicTable dynamicTable, string dbTableobjectID, int TenantID, bool IsAttributeGroup = false)
        {
            bool execute = false;
            if (IsAttributeGroup == true)
            {
                StringBuilder alterAttrTable = new StringBuilder();
                alterAttrTable.Append("ALTER TABLE " + dynamicTable.Name + " ADD ");
                string checkAttrColumnExistence = @"SELECT name
                                                    FROM syscolumns where ID = " + dbTableobjectID + " AND name = 'PredefinedId' ";
                var AttrcolumnExistence = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkAttrColumnExistence).Cast<Hashtable>().ToList();
                if (AttrcolumnExistence.Count == 0)
                {
                    alterAttrTable.Append(" PredefinedId INT ");
                    tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(alterAttrTable.ToString().Trim(','));
                }
            }
            StringBuilder alterTable = new StringBuilder();
            alterTable.Append("ALTER TABLE " + dynamicTable.Name + " ADD ");
            foreach (var newColumnList in dynamicTable.Columns)
            {
                string checkColumnExistence = @"SELECT name
                                                    FROM syscolumns where ID = " + dbTableobjectID + " AND name = '" + newColumnList.ColumnName + "'";
                var columnExistence = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkColumnExistence).Cast<Hashtable>().ToList();
                if (columnExistence.Count == 0)
                {
                    execute = true;
                    if (newColumnList.Type.ToLower() == "varchar" || newColumnList.Type.ToLower() == "nvarchar")
                        alterTable.Append(newColumnList.ColumnName + " nvarchar(4000) NULL,");
                    else if (newColumnList.Type.ToLower() == "datetimeoffset")
                        alterTable.Append(newColumnList.ColumnName + " datetime NULL,");
                    else if (newColumnList.Type.ToLower() == "money")
                        alterTable.Append(newColumnList.ColumnName + " money NULL,");
                    else if (newColumnList.Type.ToLower() == "bit")
                        alterTable.Append(newColumnList.ColumnName + " bit NULL,");
                    else
                        alterTable.Append(newColumnList.ColumnName + " " + newColumnList.Type + " NULL,");
                }
            }
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started altering table " + dynamicTable.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
            if (execute)
                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(alterTable.ToString().Trim(','));
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with alter of table " + dynamicTable.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
        }

        public void InsertUpdateAttributeSequenceUsingQuery(ITransaction tx, AttributeSequenceDao seq, int TenantID)
        {
            try
            {
                StringBuilder isbldr = new StringBuilder();

                isbldr.AppendLine(" DECLARE @startvalue AS BIGINT = " + seq.StartWithValue + " ");
                isbldr.AppendLine("DECLARE @seqName AS NVARCHAR(MAX) = 'AttributeSequence_" + seq.AttributeID + "' ");
                isbldr.AppendLine("DECLARE @existdata AS BIGINT ");
                isbldr.AppendLine("IF EXISTS (");
                isbldr.AppendLine("SELECT NAME");
                isbldr.AppendLine("FROM   sys.sequences");
                isbldr.AppendLine("WHERE  NAME = 'AttributeSequence_" + seq.AttributeID + "'");
                isbldr.AppendLine(")");
                isbldr.AppendLine("BEGIN");
                isbldr.AppendLine("SELECT @existdata = CONVERT(BIGINT, start_value)");
                isbldr.AppendLine("FROM   sys.sequences");
                isbldr.AppendLine("WHERE  NAME = 'AttributeSequence_" + seq.AttributeID + "'");
                isbldr.AppendLine("IF @existdata != @startvalue");
                isbldr.AppendLine("BEGIN");
                isbldr.AppendLine("DECLARE @sql NVARCHAR(MAX)");
                isbldr.AppendLine("SET @sql = 'ALTER SEQUENCE ' + @seqName + ' RESTART WITH ' + CONVERT(NVARCHAR, @startvalue)");
                isbldr.AppendLine("+ ' increment BY 1 NO MINVALUE  NO MAXVALUE NO CYCLE NO CACHE'");
                isbldr.AppendLine("EXEC (@sql)");
                isbldr.AppendLine("END");
                isbldr.AppendLine("END");
                isbldr.AppendLine("ELSE");
                isbldr.AppendLine("BEGIN");
                isbldr.AppendLine("IF EXISTS (");
                isbldr.AppendLine("SELECT NAME");
                isbldr.AppendLine("FROM   sys.sequences");
                isbldr.AppendLine(" WHERE  NAME = 'AttributeSequence_" + seq.AttributeID + "'");
                isbldr.AppendLine(")");
                isbldr.AppendLine(" DROP SEQUENCE AttributeSequence_" + seq.AttributeID + " ");
                isbldr.AppendLine("CREATE SEQUENCE AttributeSequence_" + seq.AttributeID + " AS BIGINT START ");
                isbldr.AppendLine("WITH 1 increment BY 1 minvalue 0 no cycle cache 50;");
                isbldr.AppendLine("END");

                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started sequence creation AttributeSequence_" + seq.AttributeID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(isbldr.ToString());
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with sequence creation AttributeSequence_" + seq.AttributeID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);


            }
            catch (Exception ex)
            {

            }
        }



        public XmlDocument CreateOrUpdateTableUsingQuery(ITransaction tx, DynamicTable dynamicTable, int TenantID, bool IsAttributeGroupTable = false, bool IsFinancialCostCentre = false, bool IsPredefinedAttr = false)
        {
            try
            {

                XmlDocument mappingdoc = new XmlDocument();
                string checkTableExistence = @"SELECT object_id
                                           FROM sys.Tables where name = '" + dynamicTable.Name + "'";
                var tableObjectID = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkTableExistence).Cast<Hashtable>().ToList();
                if (tableObjectID.Count() > 0)
                {
                    if (dynamicTable.Columns != null)
                        UpdateTableUsingQuery(tx, dynamicTable, tableObjectID[0]["object_id"].ToString(), TenantID, IsAttributeGroupTable);
                    if (IsFinancialCostCentre == false)
                        mappingdoc = Createmapingxmlfile(dynamicTable, IsAttributeGroupTable, IsPredefinedAttr);
                    if (IsFinancialCostCentre == true)
                        mappingdoc = CreatemapingxmlfileforFinancial(dynamicTable, IsAttributeGroupTable, IsPredefinedAttr);
                }
                else
                {
                    StringBuilder createTable = new StringBuilder();
                    createTable.Append("CREATE TABLE dbo.");
                    createTable.Append(dynamicTable.Name);

                    if (IsAttributeGroupTable == true)
                    {
                        createTable.Append("( ID int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,");
                        createTable.Append(" EntityId int NOT NULL,");
                        createTable.Append(" SortOrder int NOT NULL, ");
                        createTable.Append(" PredefinedId int NOT NULL ");
                    }
                    else if (IsPredefinedAttr == true)
                    {
                        createTable.Append("( ID int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED");
                    }
                    else
                        createTable.Append("( ID int NOT NULL PRIMARY KEY CLUSTERED");

                    if (dynamicTable.Columns != null)
                    {
                        createTable.Append(" , ");
                        foreach (var newColumnList in dynamicTable.Columns)
                        {

                            if (newColumnList.Type.ToLower() == "varchar" || newColumnList.Type.ToLower() == "nvarchar")
                                createTable.Append(" " + newColumnList.ColumnName + " nvarchar(4000) NULL,");
                            else if (newColumnList.Type.ToLower() == "datetimeoffset")
                                createTable.Append(" " + newColumnList.ColumnName + " datetime NULL,");
                            else if (newColumnList.Type.ToLower() == "money")
                                createTable.Append(" " + newColumnList.ColumnName + " money NULL,");
                            else if (newColumnList.Type.ToLower() == "bit")
                                createTable.Append(" " + newColumnList.ColumnName + " bit NULL,");
                            else
                                createTable.Append(" " + newColumnList.ColumnName + " " + newColumnList.Type + " NULL,");
                        }
                    }
                    createTable.Append(")");
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started creating new table " + dynamicTable.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(createTable.ToString());
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with creation of table " + dynamicTable.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    if (IsFinancialCostCentre == false)
                        mappingdoc = Createmapingxmlfile(dynamicTable, IsAttributeGroupTable, IsPredefinedAttr);
                    if (IsFinancialCostCentre == true)
                        mappingdoc = CreatemapingxmlfileforFinancial(dynamicTable, IsAttributeGroupTable, IsPredefinedAttr);

                }

                return mappingdoc;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public XmlDocument Createmapingxmlfile(DynamicTable dynamicxml, bool IsAttributeGroupTable = false, bool IsPredefinedAttr = false)
        {

            try
            {
                XmlDocument mappingdoc = new XmlDocument();
                XmlNode child = mappingdoc.AppendChild(mappingdoc.CreateElement("class"));
                XmlAttribute childAtt = child.Attributes.Append(mappingdoc.CreateAttribute("name"));

                if (IsAttributeGroupTable == true)
                    childAtt.InnerText = "BrandSystems.Marcom.Dal.Metadata.Model.AttrGroupDynamicAttributesDao,BrandSystems.Marcom.Dal";
                else if (IsPredefinedAttr == true)
                    childAtt.InnerText = "BrandSystems.Marcom.Dal.Metadata.Model.PreDefinedAttrGroupDynamicAttributesDao,BrandSystems.Marcom.Dal";
                else
                    childAtt.InnerText = "BrandSystems.Marcom.Dal.Planning.Model.DynamicAttributesDao,BrandSystems.Marcom.Dal";

                XmlAttribute childattr2 = child.Attributes.Append(mappingdoc.CreateAttribute("table"));
                childattr2.InnerText = dynamicxml.Name;
                XmlAttribute childattr3 = child.Attributes.Append(mappingdoc.CreateAttribute("entity-name"));
                childattr3.InnerText = dynamicxml.Name.Split('_')[1] + dynamicxml.Name.Split('_')[2] + "_V" + dynamicxml.entityVersionNumber;
                XmlAttribute childattr4 = child.Attributes.Append(mappingdoc.CreateAttribute("lazy"));
                childattr4.InnerText = "true";

                XmlNode child2 = child.AppendChild(mappingdoc.CreateElement("id"));
                XmlAttribute child2Att = child2.Attributes.Append(mappingdoc.CreateAttribute("name"));
                child2Att.InnerText = "Id";
                XmlAttribute child2attr2 = child2.Attributes.Append(mappingdoc.CreateAttribute("column"));
                child2attr2.InnerText = "Id";
                XmlNode child21 = child2.AppendChild(mappingdoc.CreateElement("generator"));
                XmlAttribute child3Att = child21.Attributes.Append(mappingdoc.CreateAttribute("class"));

                if (IsAttributeGroupTable == true)
                    child3Att.InnerText = "native";
                else if (IsPredefinedAttr == true)
                    child3Att.InnerText = "native";
                else
                    child3Att.InnerText = "assigned";

                if (IsAttributeGroupTable == true)
                {
                    XmlNode bagchild22 = child.AppendChild(mappingdoc.CreateElement("property"));
                    XmlAttribute bagchild2attr11 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("type"));
                    bagchild2attr11.InnerText = "int";

                    XmlAttribute bagchild2attr33 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                    bagchild2attr33.InnerText = "false";
                    XmlAttribute bagchild2attr44 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("name"));
                    bagchild2attr44.InnerText = "EntityId";
                    XmlAttribute bagchild2attr55 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("column"));
                    bagchild2attr55.InnerText = "EntityId";

                    XmlNode bagchildsort22 = child.AppendChild(mappingdoc.CreateElement("property"));
                    XmlAttribute bagchild2attrsort11 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("type"));
                    bagchild2attrsort11.InnerText = "int";
                    XmlAttribute bagchild2attrsort33 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                    bagchild2attrsort33.InnerText = "false";
                    XmlAttribute bagchild2attrsort44 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("name"));
                    bagchild2attrsort44.InnerText = "SortOrder";
                    XmlAttribute bagchild2attrsort55 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("column"));
                    bagchild2attrsort55.InnerText = "SortOrder";

                    XmlNode bagchildpredefined33 = child.AppendChild(mappingdoc.CreateElement("property"));
                    XmlAttribute bagchild2attrpredefined22 = bagchildpredefined33.Attributes.Append(mappingdoc.CreateAttribute("type"));
                    bagchild2attrpredefined22.InnerText = "int";
                    XmlAttribute bagchild2attrpredefined44 = bagchildpredefined33.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                    bagchild2attrpredefined44.InnerText = "false";
                    XmlAttribute bagchild2attrpredefined55 = bagchildpredefined33.Attributes.Append(mappingdoc.CreateAttribute("name"));
                    bagchild2attrpredefined55.InnerText = "PredefinedId";
                    XmlAttribute bagchild2attrpredefined66 = bagchildpredefined33.Attributes.Append(mappingdoc.CreateAttribute("column"));
                    bagchild2attrpredefined66.InnerText = "PredefinedId";
                }

                if (dynamicxml.Columns != null)
                {
                    XmlNode bagParent = child.AppendChild(mappingdoc.CreateElement("dynamic-component"));
                    XmlAttribute bagParentattr1 = bagParent.Attributes.Append(mappingdoc.CreateAttribute("name"));
                    bagParentattr1.InnerText = "Attributes";


                    foreach (var obj in dynamicxml.Columns)
                    {
                        XmlNode bagchild2 = bagParent.AppendChild(mappingdoc.CreateElement("property"));
                        XmlAttribute bagchild2attr1 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("type"));
                        if ((Convert.ToString(obj.Type)).Split('(').First().ToLower() == "varchar" || (Convert.ToString(obj.Type)).Split('(').First().ToLower() == "nvarchar")
                        {
                            bagchild2attr1.InnerText = "string";
                        }
                        if (Convert.ToString(obj.Type).ToLower() == "datetimeoffset")
                        {
                            bagchild2attr1.InnerText = "DateTime";
                        }
                        else if (Convert.ToString(obj.Type).ToLower() == "int")
                        {
                            bagchild2attr1.InnerText = "int";
                        }
                        else if (Convert.ToString(obj.Type).ToLower() == "boolean")
                        {
                            bagchild2attr1.InnerText = "Boolean";
                        }
                        else if (Convert.ToString(obj.Type).ToLower() == "money")
                        {
                            bagchild2attr1.InnerText = "Decimal";
                        }
                        else if (Convert.ToString(obj.Type).ToLower() == "bit")
                        {
                            bagchild2attr1.InnerText = "Boolean";
                        }
                        XmlAttribute bagchild2attr3 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                        bagchild2attr3.InnerText = "false";
                        XmlAttribute bagchild2attr4 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("name"));
                        bagchild2attr4.InnerText = Convert.ToString(obj.AttributeId);
                        XmlAttribute bagchild2attr5 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("column"));
                        bagchild2attr5.InnerText = Convert.ToString(obj.ColumnName);
                    }
                }

                return mappingdoc;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public XmlDocument CreatemapingxmlfileforFinancial(DynamicTable dynamicxml, bool IsAttributeGroupTable = false, bool IsPredefinedAttr = false)
        {
            XmlDocument mappingdoc = new XmlDocument();
            XmlNode child = mappingdoc.AppendChild(mappingdoc.CreateElement("class"));
            XmlAttribute childAtt = child.Attributes.Append(mappingdoc.CreateAttribute("name"));

            if (IsAttributeGroupTable == true)
                childAtt.InnerText = "BrandSystems.Marcom.Dal.Metadata.Model.AttrGroupDynamicAttributesDao,BrandSystems.Marcom.Dal";
            else if (IsPredefinedAttr == true)
                childAtt.InnerText = "BrandSystems.Marcom.Dal.Metadata.Model.PreDefinedAttrGroupDynamicAttributesDao,BrandSystems.Marcom.Dal";
            else
                childAtt.InnerText = "BrandSystems.Marcom.Dal.Planning.Model.DynamicAttributesDao,BrandSystems.Marcom.Dal";

            XmlAttribute childattr2 = child.Attributes.Append(mappingdoc.CreateAttribute("table"));
            childattr2.InnerText = dynamicxml.Name;
            XmlAttribute childattr3 = child.Attributes.Append(mappingdoc.CreateAttribute("entity-name"));
            childattr3.InnerText = dynamicxml.Name.Split('_')[1];
            XmlAttribute childattr4 = child.Attributes.Append(mappingdoc.CreateAttribute("lazy"));
            childattr4.InnerText = "true";

            XmlNode child2 = child.AppendChild(mappingdoc.CreateElement("id"));
            XmlAttribute child2Att = child2.Attributes.Append(mappingdoc.CreateAttribute("name"));
            child2Att.InnerText = "Id";
            XmlAttribute child2attr2 = child2.Attributes.Append(mappingdoc.CreateAttribute("column"));
            child2attr2.InnerText = "Id";
            XmlNode child21 = child2.AppendChild(mappingdoc.CreateElement("generator"));
            XmlAttribute child3Att = child21.Attributes.Append(mappingdoc.CreateAttribute("class"));

            if (IsAttributeGroupTable == true)
                child3Att.InnerText = "native";
            else if (IsPredefinedAttr == true)
                child3Att.InnerText = "native";
            else
                child3Att.InnerText = "assigned";

            if (IsAttributeGroupTable == true)
            {
                XmlNode bagchild22 = child.AppendChild(mappingdoc.CreateElement("property"));
                XmlAttribute bagchild2attr11 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("type"));
                bagchild2attr11.InnerText = "int";

                XmlAttribute bagchild2attr33 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                bagchild2attr33.InnerText = "false";
                XmlAttribute bagchild2attr44 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("name"));
                bagchild2attr44.InnerText = "EntityId";
                XmlAttribute bagchild2attr55 = bagchild22.Attributes.Append(mappingdoc.CreateAttribute("column"));
                bagchild2attr55.InnerText = "EntityId";

                XmlNode bagchildsort22 = child.AppendChild(mappingdoc.CreateElement("property"));
                XmlAttribute bagchild2attrsort11 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("type"));
                bagchild2attrsort11.InnerText = "int";
                XmlAttribute bagchild2attrsort33 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                bagchild2attrsort33.InnerText = "false";
                XmlAttribute bagchild2attrsort44 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("name"));
                bagchild2attrsort44.InnerText = "SortOrder";
                XmlAttribute bagchild2attrsort55 = bagchildsort22.Attributes.Append(mappingdoc.CreateAttribute("column"));
                bagchild2attrsort55.InnerText = "SortOrder";
            }

            if (dynamicxml.Columns != null)
            {
                XmlNode bagParent = child.AppendChild(mappingdoc.CreateElement("dynamic-component"));
                XmlAttribute bagParentattr1 = bagParent.Attributes.Append(mappingdoc.CreateAttribute("name"));
                bagParentattr1.InnerText = "Attributes";


                foreach (var obj in dynamicxml.Columns)
                {
                    XmlNode bagchild2 = bagParent.AppendChild(mappingdoc.CreateElement("property"));
                    XmlAttribute bagchild2attr1 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("type"));
                    if ((Convert.ToString(obj.Type)).Split('(').First().ToLower() == "varchar" || (Convert.ToString(obj.Type)).Split('(').First().ToLower() == "nvarchar")
                    {
                        bagchild2attr1.InnerText = "string";
                    }
                    if (Convert.ToString(obj.Type).ToLower() == "datetimeoffset")
                    {
                        bagchild2attr1.InnerText = "DateTime";
                    }
                    else if (Convert.ToString(obj.Type).ToLower() == "int")
                    {
                        bagchild2attr1.InnerText = "int";
                    }
                    else if (Convert.ToString(obj.Type).ToLower() == "boolean")
                    {
                        bagchild2attr1.InnerText = "Boolean";
                    }
                    else if (Convert.ToString(obj.Type).ToLower() == "money")
                    {
                        bagchild2attr1.InnerText = "Decimal";
                    }
                    else if (Convert.ToString(obj.Type).ToLower() == "bit")
                    {
                        bagchild2attr1.InnerText = "Boolean";
                    }
                    XmlAttribute bagchild2attr3 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("not-null"));
                    bagchild2attr3.InnerText = "false";
                    XmlAttribute bagchild2attr4 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("name"));
                    bagchild2attr4.InnerText = Convert.ToString(obj.AttributeId);
                    XmlAttribute bagchild2attr5 = bagchild2.Attributes.Append(mappingdoc.CreateAttribute("column"));
                    bagchild2attr5.InnerText = Convert.ToString(obj.ColumnName);
                }
            }

            return mappingdoc;
        }


        public bool CreateviewUsingQuery(ITransaction tx, Dynamicview dynamicview, string scourcetablename, string workingXMLPath, int TenantID)
        {


            try
            {

                StringBuilder createview = new StringBuilder();
                string checkviewExistence = @"select s.name from sysobjects as s where  s.type='v' AND s.name='" + dynamicview.Name + "'";
                var viewObjectID = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkviewExistence).Cast<Hashtable>().ToList();
                if (viewObjectID.Count > 0)
                {
                    createview.Append(" ALTER VIEW  ");
                }
                else if (viewObjectID.Count == 0)
                {
                    createview.Append(" CREATE VIEW  ");
                }



                createview.Append(dynamicview.Name);
                createview.Append(" AS ");
                createview.Append(" SELECT Att.ID, ent.Name, ");
                //createTable.Append("( ID int NOT NULL PRIMARY KEY,");
                if (dynamicview.Columns != null)
                {
                    foreach (var newColumnList in dynamicview.Columns)
                    {
                        if (newColumnList.AttributeTypeID == (int)AttributesList.TextSingleLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMultiLine || newColumnList.AttributeTypeID == (int)AttributesList.ListSingleSelection || newColumnList.AttributeTypeID == (int)AttributesList.DateTime || newColumnList.AttributeTypeID == (int)AttributesList.TextMoney)
                        {
                            createview.Append(newColumnList.ColumnName);
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        //else if (newColumnList.AttributeTypeID == 6)
                        //{
                        //    var TreeLeveldao = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetObject<TreeLevelDao>(workingXMLPath);
                        //    var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);
                        //    foreach (var treecount in Treelist)
                        //    {
                        //        createview.Append("(SELECT mtv.NodeID  ");
                        //        createview.Append("FROM  MM_TreeValue mtv ");
                        //        createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                        //        createview.Append("AND EntityID= Att.ID ");
                        //        createview.Append("AND mtv.[LEVEL]=" + treecount.Level + ") AS [" + treecount.LevelName + "], ");
                        //    }


                        //}

                        else if (newColumnList.AttributeTypeID == 10)
                        {
                            createview.Append("( SELECT MIN(Startdate) FROM PM_EntityPeriod WHERE EntityID= Att.ID ) as Startdate, ");
                            createview.Append("( SELECT MAX (EndDate) FROM PM_EntityPeriod WHERE EntityID= Att.ID ) as EndDate, ");
                        }

                    }
                    var index = createview.ToString().LastIndexOf(',');
                    if (index >= 0)
                        createview.Remove(index, 1);
                    createview.Append(" from ");
                    createview.Append(scourcetablename);
                    createview.Append(" AS Att INNER JOIN PM_Entity AS  ent ON ent.Id= Att.ID AND ent.[Active] = 1");

                }
                ////dropview.
                //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started drop view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                //tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(deleteview.ToString());
                //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with drop of view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                //createview.
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started creating view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(createview.ToString());
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with creation of view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return true;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("creating view  with follwing error  : " + dynamicview.Name, ex);
                return false;
            }


            return true;
        }

        public bool CreateviewTextUsingQuery(ITransaction tx, DynamicviewText dynamicviewText, string scourcetablename, string workingXMLPath, int TenantID)
        {


            try
            {
                XDocument XDoc = MarcomCache<XDocument>.GetXmlWorkingPath(TenantID);
                StringBuilder createview = new StringBuilder();
                string checkviewExistence = @"select s.name from sysobjects as s where  s.type='v' AND s.name='" + dynamicviewText.Name + "'";
                var viewObjectID = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkviewExistence).Cast<Hashtable>().ToList();
                if (viewObjectID.Count > 0)
                {
                    createview.Append(" ALTER VIEW  ");
                }
                else if (viewObjectID.Count == 0)
                {
                    createview.Append(" CREATE VIEW  ");
                }
                createview.Append(dynamicviewText.Name);
                createview.Append(" AS ");
                createview.Append(" SELECT Att.ID, ent.Name, ");

                createview.Append(" (SELECT (us.FirstName + ' ' + us.LastName)");
                createview.Append(" FROM   AM_Entity_Role_User aeru");
                createview.Append(" INNER JOIN UM_User us");
                createview.Append(" ON  us.ID = aeru.UserID INNER JOIN AM_EntityTypeRoleAcl aetra ON  aeru.RoleID = aetra.ID  AND aetra.EntityRoleID = 1 ");
                createview.Append(" WHERE  aeru.EntityID = ent.ID");
                createview.Append(" ) AS [Owner],");
                //createTable.Append("( ID int NOT NULL PRIMARY KEY,");
                if (dynamicviewText.Columns != null)
                {
                    foreach (var newColumnList in dynamicviewText.Columns)
                    {
                        if (newColumnList.AttributeTypeID == (int)AttributesList.TextSingleLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMultiLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMoney)
                        {
                            //if (newColumnList.AttributeTypeID == 5) { createview.Append("CONVERT(nvarchar(10), " + newColumnList.ColumnName + ", 105) "); }
                            //else
                            //{

                            //}
                            createview.Append("COALESCE(NULLIF(" + newColumnList.ColumnName + ",''), '-') ");
                            //createview.Append("isnull(" + newColumnList.ColumnName + ", '-') ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.DateTime)
                        {
                            createview.Append("REPLACE( CONVERT(varchar, ISNULL(" + newColumnList.ColumnName + ",''),121),'1900-01-01 00:00:00.000','-') ");   //AS [Date  time],
                            //createview.Append("isnull(" + newColumnList.ColumnName + ", '-') ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");

                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.ListSingleSelection)
                        {
                            createview.Append("ISNULL( (select top 1 Caption from  mm_option where ID IN(SELECT " + newColumnList.ColumnName + " FROM " + scourcetablename + "  where ID=Att.ID)),'-') AS  [" + newColumnList.Caption + "], ");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.ListMultiSelection)
                        {
                            createview.Append("ISNULL( (select distinct (SELECT ");
                            createview.Append("STUFF( ");
                            createview.Append("( ");
                            createview.Append("SELECT ',' +  mo.Caption ");
                            createview.Append("FROM   MM_MultiSelect mms2 ");
                            createview.Append("INNER JOIN MM_Option mo ");
                            createview.Append("ON  mms2.OptionID = mo.ID ");
                            createview.Append("WHERE  mms2.EntityID = mms.EntityID AND mms2.AttributeID=mms.AttributeID ");
                            createview.Append("FOR XML PATH('')  ");
                            createview.Append("),1,1,''  ");
                            createview.Append(") AS VALUE ");
                            createview.Append("FROM   MM_MultiSelect mms ");
                            createview.Append("WHERE  mms.EntityID=Att.ID and  mms.AttributeID = " + newColumnList.AttributeId + "  ");
                            createview.Append("GROUP BY  ");
                            createview.Append("mms.EntityID,mms.AttributeID) ),'-')   as [" + newColumnList.Caption + "],");

                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.DropDownTree)
                        {
                            var TreeLeveldao = tx.PersistenceManager.MetadataRepository[TenantID].GetObject<TreeLevelDao>(XDoc);
                            var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);
                            foreach (var treecount in Treelist)
                            {
                                createview.Append("ISNULL( (SELECT mtn.Caption ");
                                createview.Append("FROM   MM_TreeNode mtn ");
                                createview.Append("INNER JOIN MM_TreeValue mtv ");
                                createview.Append("ON  mtv.NodeID = mtn.ID ");
                                createview.Append("AND mtv.AttributeID = mtn.AttributeID ");
                                createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                createview.Append("AND EntityID= Att.ID ");
                                createview.Append("AND mtv.[LEVEL]=" + treecount.Level + "),'-')  AS [" + treecount.LevelName + "], ");
                            }


                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.TreeMultiSelection)
                        {
                            var TreeLeveldao = tx.PersistenceManager.MetadataRepository[TenantID].GetObject<TreeLevelDao>(XDoc);
                            var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);

                            foreach (var treecount in Treelist)
                            {
                                if (Treelist.Count() != treecount.Level)
                                {
                                    createview.Append("ISNULL( (SELECT mtn.Caption ");
                                    createview.Append("FROM   MM_TreeNode mtn ");
                                    createview.Append("INNER JOIN MM_TreeValue mtv ");
                                    createview.Append("ON  mtv.NodeID = mtn.ID ");
                                    createview.Append("AND mtv.AttributeID = mtn.AttributeID ");
                                    createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                    createview.Append("AND EntityID= Att.ID ");
                                    createview.Append("AND mtv.[LEVEL]=" + treecount.Level + "),'-')  AS [" + treecount.LevelName + "], ");
                                }
                                else
                                {
                                    createview.Append("ISNULL( (SELECT  ");
                                    createview.Append("STUFF( ");
                                    createview.Append("( ");
                                    createview.Append("SELECT ', ' +  mtn.Caption ");
                                    createview.Append("FROM   MM_TreeNode mtn ");
                                    createview.Append("INNER JOIN MM_TreeValue mtv ");
                                    createview.Append("ON  mtv.NodeID = mtn.ID and  mtv.AttributeID=" + newColumnList.AttributeId + " ");
                                    createview.Append("AND mtn.Level = " + treecount.Level + " WHERE mtv.EntityID = Att.ID AND mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                    createview.Append("FOR XML PATH('') ");
                                    createview.Append("), ");
                                    createview.Append("1, ");
                                    createview.Append("2, ");
                                    createview.Append("'' ");
                                    createview.Append(") ),'-') AS [" + treecount.LevelName + "], ");
                                }
                            }
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.CheckBoxSelection)
                        {
                            createview.Append("CASE when " + newColumnList.ColumnName + " = 1 THEN 'True' ");
                            createview.Append(" when " + newColumnList.ColumnName + " = 0 THEN 'False' ELSE '-' END ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.Period)
                        {
                            createview.Append("( SELECT REPLACE(( SELECT ISNULL((SELECT MIN(ISNULL(Startdate, '-'))");
                            createview.Append("FROM   PM_EntityPeriod ");
                            createview.Append(" WHERE  EntityID = Att.ID),'')),'1900-01-01','-') ) as Startdate, ");

                            createview.Append("( SELECT REPLACE(( SELECT ISNULL((SELECT MAX(ISNULL(EndDate, '-'))");
                            createview.Append("FROM   PM_EntityPeriod ");
                            createview.Append(" WHERE  EntityID = Att.ID),'')),'1900-01-01','-') ) as EndDate, ");
                            //createview.Append("( SELECT MIN(isnull(Startdate,'-')) FROM PM_EntityPeriod WHERE EntityID= Att.ID ) as Startdate, ");
                            //createview.Append("( SELECT MAX(isnull(EndDate,'-')) FROM PM_EntityPeriod WHERE EntityID= Att.ID ) as EndDate, ");
                        }

                    }
                    var index = createview.ToString().LastIndexOf(',');
                    if (index >= 0)
                        createview.Remove(index, 1);
                    createview.Append(" from ");
                    createview.Append(scourcetablename);
                    createview.Append(" AS Att INNER JOIN PM_Entity AS  ent ON ent.Id= Att.ID AND ent.[Active] = 1");

                }


                //createview.
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started creating view " + dynamicviewText.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(createview.ToString());
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with creation of view " + dynamicviewText.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return true;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("creating view  with follwing error  : " + dynamicviewText.Name, ex);
                return false;
            }

        }

        public bool CreateviewUsingQueryAttributeGroup(ITransaction tx, DynamicviewAttributeGroup dynamicview, string scourcetablename, string workingXMLPath, int TenantID)
        {


            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                StringBuilder createview = new StringBuilder();
                string checkviewExistence = @"select s.name from sysobjects as s where  s.type='v' AND s.name='" + dynamicview.Name + "'";
                var viewObjectID = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkviewExistence).Cast<Hashtable>().ToList();
                if (viewObjectID.Count > 0)
                {
                    createview.Append(" ALTER VIEW  ");
                }
                else if (viewObjectID.Count == 0)
                {
                    createview.Append(" CREATE VIEW  ");
                }



                createview.Append(dynamicview.Name);
                createview.Append(" AS ");
                createview.Append(" SELECT Att.ID,att.SortOrder, Att.EntityId, ent.Name AS [EntityName], ");
                //createTable.Append("( ID int NOT NULL PRIMARY KEY,");
                if (dynamicview.Columns != null)
                {
                    foreach (var newColumnList in dynamicview.Columns)
                    {
                        if (newColumnList.AttributeTypeID == (int)AttributesList.TextSingleLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMultiLine || newColumnList.AttributeTypeID == (int)AttributesList.ListSingleSelection || newColumnList.AttributeTypeID == (int)AttributesList.DateTime || newColumnList.AttributeTypeID == (int)AttributesList.TextMoney)
                        {
                            createview.Append(newColumnList.ColumnName);
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.Uploader)
                        {
                            string imageuploderpath;
                            XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                            //The Key is root node current Settings
                            string xelementName = "ApplicationURL";

                            imageuploderpath = xelementFilepath.Element(xelementName).Value;
                            imageuploderpath = imageuploderpath + "/UploadedImages//";
                            createview.Append("'" + imageuploderpath.Trim() + "'");
                            createview.Append("+");
                            createview.Append("COALESCE(NULLIF(" + newColumnList.ColumnName + ",''), 'noPreview.jpg') ");
                            //createview.Append(newColumnList.ColumnName);
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        //else if (newColumnList.AttributeTypeID == 6)
                        //{
                        //    var TreeLeveldao = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetObject<TreeLevelDao>(workingXMLPath);
                        //    var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);
                        //    foreach (var treecount in Treelist)
                        //    {
                        //        createview.Append("(SELECT mtv.NodeID  ");
                        //        createview.Append("FROM  MM_TreeValue mtv ");
                        //        createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                        //        createview.Append("AND EntityID= Att.ID ");
                        //        createview.Append("AND mtv.[LEVEL]=" + treecount.Level + ") AS [" + treecount.LevelName + "], ");
                        //    }


                        //}


                    }
                    var index = createview.ToString().LastIndexOf(',');
                    if (index >= 0)
                        createview.Remove(index, 1);
                    createview.Append(" from ");
                    createview.Append(scourcetablename);
                    createview.Append(" AS Att INNER JOIN PM_Entity AS  ent ON ent.Id= Att.EntityId AND ent.[Active] = 1");

                }
                ////dropview.
                //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started drop view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                //tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(deleteview.ToString());
                //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with drop of view " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                //createview.
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started creating view  for AttributeGroup: " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(createview.ToString());
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with creation of view for AttributeGroup: " + dynamicview.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return true;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("creating view for AttributeGroup with follwing error  : " + dynamicview.Name, ex);
                return false;
            }


            return true;
        }

        public bool CreateviewTextUsingQueryAttributeGroup(ITransaction tx, DynamicviewAttributeGroupText dynamicviewText, string scourcetablename, string workingXMLPath, int TenantID)
        {


            try
            {
                XDocument XDoc = MarcomCache<XDocument>.GetXmlWorkingPath(TenantID);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                StringBuilder createview = new StringBuilder();
                string checkviewExistence = @"select s.name from sysobjects as s where  s.type='v' AND s.name='" + dynamicviewText.Name + "'";
                var viewObjectID = tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuery(checkviewExistence).Cast<Hashtable>().ToList();
                if (viewObjectID.Count > 0)
                {
                    createview.Append(" ALTER VIEW  ");
                }
                else if (viewObjectID.Count == 0)
                {
                    createview.Append(" CREATE VIEW  ");
                }
                createview.Append(dynamicviewText.Name);
                createview.Append(" AS ");
                createview.Append(" SELECT Att.ID,att.SortOrder, Att.EntityId, ent.Name AS [EntityName], ");

                createview.Append(" (SELECT (us.FirstName + ' ' + us.LastName)");
                createview.Append(" FROM   AM_Entity_Role_User aeru");
                createview.Append(" INNER JOIN UM_User us");
                createview.Append(" ON  us.ID = aeru.UserID INNER JOIN AM_EntityTypeRoleAcl aetra ON  aeru.RoleID = aetra.ID  AND aetra.EntityRoleID = 1 ");
                createview.Append(" WHERE  aeru.EntityID = Att.EntityId");
                createview.Append(" ) AS [Owner],");
                //createTable.Append("( ID int NOT NULL PRIMARY KEY,");
                if (dynamicviewText.Columns != null)
                {
                    foreach (var newColumnList in dynamicviewText.Columns)
                    {
                        if (newColumnList.AttributeTypeID == (int)AttributesList.TextSingleLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMultiLine || newColumnList.AttributeTypeID == (int)AttributesList.TextMoney)
                        {
                            //if (newColumnList.AttributeTypeID == 5) { createview.Append("CONVERT(nvarchar(10), " + newColumnList.ColumnName + ", 105) "); }
                            //else
                            //{

                            //}

                            createview.Append("COALESCE(NULLIF(" + newColumnList.ColumnName + ",''), '-') ");
                            //createview.Append("isnull(" + newColumnList.ColumnName + ", '-') ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.Uploader)
                        {
                            string imageuploderpath;
                            XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);                           

                            //The Key is root node current Settings
                            string xelementName = "ApplicationURL";

                            imageuploderpath = xelementFilepath.Element(xelementName).Value;
                            imageuploderpath = imageuploderpath + "/UploadedImages//";
                            createview.Append("'" + imageuploderpath.Trim() + "'");
                            createview.Append("+");
                            createview.Append("COALESCE(NULLIF(" + newColumnList.ColumnName + ",''), 'noPreview.jpg') ");
                            //createview.Append("isnull(" + newColumnList.ColumnName + ", '-') ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.DateTime)
                        {
                            createview.Append("REPLACE( CONVERT(varchar, ISNULL(" + newColumnList.ColumnName + ",''),121),'1900-01-01 00:00:00.000','-') ");   //AS [Date  time],
                            //createview.Append("isnull(" + newColumnList.ColumnName + ", '-') ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");

                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.ListSingleSelection)
                        {
                            createview.Append("ISNULL( (select top 1 Caption from  mm_option where ID IN(SELECT " + newColumnList.ColumnName + " FROM " + scourcetablename + "  where EntityId=Att.EntityId and SortOrder = att.SortOrder )),'-') AS  [" + newColumnList.Caption + "], ");
                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.ListMultiSelection)
                        {
                            createview.Append("ISNULL( (select distinct (SELECT ");
                            createview.Append("STUFF( ");
                            createview.Append("( ");
                            createview.Append("SELECT ',' +  mo.Caption ");
                            createview.Append("FROM   MM_AttrGroupMultiSelect mms2 ");
                            createview.Append("INNER JOIN MM_Option mo ");
                            createview.Append("ON  mms2.OptionID = mo.ID ");
                            createview.Append("WHERE  mms2.GroupRecordID = mms.GroupRecordID AND mms2.AttributeID=mms.AttributeID ");
                            createview.Append("FOR XML PATH('')  ");
                            createview.Append("),1,1,''  ");
                            createview.Append(") AS VALUE ");
                            createview.Append("FROM   MM_AttrGroupMultiSelect mms ");
                            createview.Append("WHERE  mms.GroupRecordID=Att.ID and  mms.AttributeID = " + newColumnList.AttributeId + "  ");
                            createview.Append("GROUP BY  ");
                            createview.Append("mms.GroupRecordID,mms.AttributeID) ),'-')   as [" + newColumnList.Caption + "],");

                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.DropDownTree)
                        {
                            var TreeLeveldao = tx.PersistenceManager.MetadataRepository[TenantID].GetObject<TreeLevelDao>(XDoc);
                            var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);
                            foreach (var treecount in Treelist)
                            {
                                createview.Append("ISNULL( (SELECT mtn.Caption ");
                                createview.Append("FROM   MM_TreeNode mtn ");
                                createview.Append("INNER JOIN MM_AttrGroupTreeValue mtv ");
                                createview.Append("ON  mtv.NodeID = mtn.ID ");
                                createview.Append("AND mtv.AttributeID = mtn.AttributeID ");
                                createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                createview.Append("AND GroupRecordID= Att.ID ");
                                createview.Append("AND mtv.[LEVEL]=" + treecount.Level + "),'-')  AS [" + treecount.LevelName + "], ");
                            }


                        }
                        else if (newColumnList.AttributeTypeID == (int)AttributesList.TreeMultiSelection)
                        {
                            var TreeLeveldao = tx.PersistenceManager.MetadataRepository[TenantID].GetObject<TreeLevelDao>(XDoc);
                            var Treelist = TreeLeveldao.Where(a => a.AttributeID == newColumnList.AttributeId);

                            foreach (var treecount in Treelist)
                            {
                                if (Treelist.Count() != treecount.Level)
                                {
                                    createview.Append("ISNULL( (SELECT mtn.Caption ");
                                    createview.Append("FROM   MM_TreeNode mtn ");
                                    createview.Append("INNER JOIN MM_AttrGroupTreeValue mtv ");
                                    createview.Append("ON  mtv.NodeID = mtn.ID ");
                                    createview.Append("AND mtv.AttributeID = mtn.AttributeID ");
                                    createview.Append("WHERE  mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                    createview.Append("AND GroupRecordID= Att.ID ");
                                    createview.Append("AND mtv.[LEVEL]=" + treecount.Level + "),'-')  AS [" + treecount.LevelName + "], ");
                                }
                                else
                                {
                                    createview.Append("ISNULL( (SELECT  ");
                                    createview.Append("STUFF( ");
                                    createview.Append("( ");
                                    createview.Append("SELECT ', ' +  mtn.Caption ");
                                    createview.Append("FROM   MM_TreeNode mtn ");
                                    createview.Append("INNER JOIN MM_AttrGroupTreeValue mtv ");
                                    createview.Append("ON  mtv.NodeID = mtn.ID and  mtv.AttributeID=" + newColumnList.AttributeId + " ");
                                    createview.Append("AND mtn.Level = " + treecount.Level + " WHERE mtv.GroupRecordID = Att.ID AND mtv.AttributeID =" + newColumnList.AttributeId + " ");
                                    createview.Append("FOR XML PATH('') ");
                                    createview.Append("), ");
                                    createview.Append("1, ");
                                    createview.Append("2, ");
                                    createview.Append("'' ");
                                    createview.Append(") ),'-') AS [" + treecount.LevelName + "], ");
                                }
                            }
                        }

                        else if (newColumnList.AttributeTypeID == (int)AttributesList.CheckBoxSelection)
                        {
                            createview.Append("CASE when " + newColumnList.ColumnName + " = 1 THEN 'True' ");
                            createview.Append(" when " + newColumnList.ColumnName + " = 0 THEN 'False' ELSE '-' END ");
                            createview.Append(" AS  [");
                            createview.Append(newColumnList.Caption);
                            createview.Append("],");
                        }


                    }
                    var index = createview.ToString().LastIndexOf(',');
                    if (index >= 0)
                        createview.Remove(index, 1);
                    createview.Append(" from ");
                    createview.Append(scourcetablename);
                    createview.Append(" AS Att INNER JOIN PM_Entity AS  ent ON ent.Id= Att.EntityId AND ent.[Active] = 1");

                }


                //createview.
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started creating view for AttributeGroup " + dynamicviewText.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuery(createview.ToString());
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Done with creating view for AttributeGroup " + dynamicviewText.Name, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return true;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("creating view for AttributeGroup with follwing error  : " + dynamicviewText.Name, ex);
                return false;
            }
            //mappingdoc = Createmapingxmlfile(dynamicTable);


            return true;
        }

        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }
    }

    public class DynamicTable
    {
        public string Name { get; set; }
        public int entityVersionNumber { get; set; }
        public List<DynamicColumn> Columns { get; set; }

    }

    public class DynamicColumn
    {
        public int AttributeId { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public bool IsNullable { get; set; }
        public string ColumnName { get; set; }
    }

    public class Dynamicview
    {
        public string Name { get; set; }
        //public int entityVersionNumber { get; set; }
        public List<DynamicviewColumn> Columns { get; set; }

    }

    public class DynamicviewColumn
    {
        public int AttributeId { get; set; }
        public int AttributeTypeID { get; set; }
        public string ColumnName { get; set; }
        public string Caption { get; set; }
    }

    public class DynamicviewText
    {
        public string Name { get; set; }
        //public int entityVersionNumber { get; set; }
        public List<DynamicviewColumntext> Columns { get; set; }

    }

    public class DynamicviewColumntext
    {
        public int AttributeId { get; set; }
        public int AttributeTypeID { get; set; }
        public string ColumnName { get; set; }
        public string Caption { get; set; }
    }


    public class DynamicviewAttributeGroup
    {
        public string Name { get; set; }
        //public int entityVersionNumber { get; set; }
        public List<DynamicviewColumnAttributeGroup> Columns { get; set; }

    }

    public class DynamicviewColumnAttributeGroup
    {
        public int AttributeId { get; set; }
        public int AttributeTypeID { get; set; }
        public string ColumnName { get; set; }
        public string Caption { get; set; }
    }


    public class DynamicviewAttributeGroupText
    {
        public string Name { get; set; }
        //public int entityVersionNumber { get; set; }
        public List<DynamicviewColumnAttributeGrouptext> Columns { get; set; }

    }

    public class DynamicviewColumnAttributeGrouptext
    {
        public int AttributeId { get; set; }
        public int AttributeTypeID { get; set; }
        public string ColumnName { get; set; }
        public string Caption { get; set; }
    }


}
