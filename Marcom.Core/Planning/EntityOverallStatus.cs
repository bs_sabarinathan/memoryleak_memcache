﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class EntityOverallStatus : IEntityOverallStatus
    {
         #region Member Variables
        protected int _id;
        protected int _entitytypeid;
        protected int _overallstatus;
        protected int _attrid;
        protected int _days;
        protected int _templateID;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EntityOverallStatus() { }

        public EntityOverallStatus(int pID, int pentitytypeid, int poverallstatus, int pattrid, int pDays, int pTemnplateID)
        {
            this._id = pID;
            this._entitytypeid = pentitytypeid;
            this._overallstatus = poverallstatus;
            this._attrid = pattrid;
            this._days = pDays;
            this._templateID = pTemnplateID;
        }

        public EntityOverallStatus(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityTypeId
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public int OverallStatus
        {
            get { return _overallstatus; }
            set
            {
                _overallstatus = value;
            }
        }

        public int AttrID
        {
            get { return _attrid; }
            set
            {
                _attrid = value;
            }
        }
        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }
        public int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityOverallStatus castObj = null;
            try
            {
                castObj = (EntityOverallStatus)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
