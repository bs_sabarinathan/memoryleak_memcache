using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// FeedDao object for table 'cm_addtionalsettings'.
	/// </summary>

    public partial class AdditionalSettingsDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected string _settingname;
        protected string _settingvalue;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
		
		#endregion
		
		#region Constructors
		public AdditionalSettingsDao() {}

        public AdditionalSettingsDao( string pSettingName, string pSettingValue)
		{
		    this._settingname=pSettingName;
            this._settingvalue=pSettingValue;            
		}

        public AdditionalSettingsDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}



        public virtual string SettingName
		{
			get { return _settingname; }
			set 
			{  			
              _bIsChanged |= (_settingname != value);
              _settingname = value; 
			}
			
		}

        public virtual string SettingValue
		{
			get { return _settingvalue; }
			set 
			{

                _bIsChanged |= (_settingvalue != value);
                _settingvalue = value; 
			}
			
		}
		
	
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string AdditionalSettingsDao = "AdditionalSettingsDao";			
			public const string Id = "Id";
            public const string SettingName = "SettingName";
            public const string SettingValue = "SettingValue";			
			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string AdditionalSettingsDao = "AdditionalSettingsDao";
            public const string Id = "Id";
            public const string SettingName = "SettingName";
            public const string SettingValue = "SettingValue";					

		}

		#endregion
		
		
	}
	
}
