﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetWorkspaceWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, WorkspacewidgetService) {
		$scope.WorkspacedivID = "Wizarddiv_" + $scope.WizardID;
		$scope.Workspaces = [];
		$scope.TopxNoOfItem = $scope.WizardNoOfItem;
		$timeout(function () { LoadWorkspace(); }, 100);

		function LoadWorkspace() {
			WorkspacewidgetService.GetEntityHistoryByID(parseInt($cookies['UserId']), $scope.TopxNoOfItem).then(function(GetWorkspace) {
				$scope.Workspaces = GetWorkspace.Response;
				$('#' + $scope.WorkspacedivID + '').parents('.box-content').find('.InProgress').remove();
			});
		}
		
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.WorkspaceWidgetCtrl']"));
		});
		$scope.set_color = function(clr) {
			if (clr != null) return {
				'background-color': "#" + clr.toString().trim()
			};
			else return '';
		}
	}
	app.controller("mui.DashBoardWidget.WorkspaceWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'WorkspacewidgetService', muiDashBoardWidgetWorkspaceWidgetCtrl]);
	function WorkspacewidgetService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GetEntityHistoryByID: GetEntityHistoryByID
	    });
	    function GetEntityHistoryByID(UserID, Topx) { var request = $http({ method: "get", url: "api/Metadata/GetEntityHistoryByID/" + UserID + "/" + Topx, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("WorkspacewidgetService", ['$http', '$q', WorkspacewidgetService]);
})(angular, app);