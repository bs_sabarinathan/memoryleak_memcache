﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface ICategory
    {
        int id { get; set; }
        string Caption { get; set; }
        string ParentKey { get; set; }
        int Level { get; set; }
        int ParentID { get; set; }
        bool IsDeleted { get; set; }
        List<Category> Children { get;set;}

    }
}
