﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class TasktemplateConditionDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _TaskTempID;
        protected int _TypeID;
        protected int _AttributeID;
        protected int _AttributeTypeID;
        protected int _AttributeLevel;
        protected string _Value;
        protected int _ConditionType;
        protected int _Sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TasktemplateConditionDao() { }

        public TasktemplateConditionDao(int pTaskTempID, int pTypeID, int pAttributeID, int pAttributeTypeID, int pAttributeLevel, string pValue, int pConditionType, int pSortorder)
        {
            this._TaskTempID = pTaskTempID;
            this._TypeID = pTypeID;
            this._AttributeID = pAttributeID;
            this._AttributeTypeID = pTypeID;
            this._AttributeLevel = pAttributeLevel;
            this._Value = pValue;
            this._ConditionType = pConditionType;
            this._Sortorder = pSortorder;
        }

        public TasktemplateConditionDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual int TaskTempID
        {
            get { return _TaskTempID; }
            set { _bIsChanged |= (_TaskTempID != value); _TaskTempID = value; }

        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _bIsChanged |= (_TypeID != value); _TypeID = value; }

        }
        public virtual int ConditionType
        {
            get { return _ConditionType; }
            set { _bIsChanged |= (_ConditionType != value); _ConditionType = value; }

        }

        public virtual int AttributeID
        {
            get { return _AttributeID; }
            set { _bIsChanged |= (_AttributeID != value); _AttributeID = value; }

        }

        public virtual int AttributeTypeID
        {
            get { return _AttributeTypeID; }
            set { _bIsChanged |= (_AttributeTypeID != value); _AttributeTypeID = value; }

        }

        public virtual int AttributeLevel
        {
            get { return _AttributeLevel; }
            set { _bIsChanged |= (_AttributeLevel != value); _AttributeLevel = value; }

        }

        public virtual string Value
        {
            get { return _Value; }
            set { _bIsChanged |= (_Value != value); _Value = value; }

        }


        public virtual int Sortorder
        {
            get { return _Sortorder; }
            set { _bIsChanged |= (_Sortorder != value); _Sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TasktemplateConditionDao = "TasktemplateConditionDao";
            public const string ID = "ID";
            public const string TaskTempID = "TaskTempID";
            public const string TypeID = "TypeID";
            public const string AttributeID = "AttributeID";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string Value = "Value";
            public const string ConditionType = "ConditionType";
            public const string Sortorder = "Sortorder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TasktemplateConditionDao = "TasktemplateConditionDao";
            public const string ID = "ID";
            public const string TaskTempID = "TaskTempID";
            public const string TypeID = "TypeID";
            public const string AttributeID = "AttributeID";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string Value = "Value";
            public const string ConditionType = "ConditionType";
            public const string Sortorder = "Sortorder";
        }

        #endregion


    }

}
