﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class TempTaskListDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _TempID;
        protected int _TaskListID;
        protected int _Sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TempTaskListDao() { }

        public TempTaskListDao(int pTempID, int pTaskListID,int pSortorder)
        {
            this._TempID = pTempID;
            this._TaskListID = pTaskListID;
            this._Sortorder = pSortorder;
        }

        public TempTaskListDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual int TempID
        {
            get { return _TempID; }
            set { _bIsChanged |= (_TempID != value); _TempID = value; }

        }

        public virtual int TaskListID
        {
            get { return _TaskListID; }
            set { _bIsChanged |= (_TaskListID != value); _TaskListID = value; }

        }


        public virtual int Sortorder
        {
            get { return _Sortorder; }
            set { _bIsChanged |= (_Sortorder != value); _Sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TempTaskListDao = "TempTaskListDao";
            public const string ID = "ID";
            public const string TempID = "TempID";
            public const string TaskListID = "TaskListID";
            public const string Sortorder = "Sortorder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TempTaskListDao = "TempTaskListDao";
            public const string ID = "ID";
            public const string TempID = "TempID";
            public const string TaskListID = "TaskListID";
            public const string Sortorder = "Sortorder";
        }

        #endregion


    }

}
