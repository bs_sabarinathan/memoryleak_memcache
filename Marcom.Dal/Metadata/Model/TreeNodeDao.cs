using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// TreeNodeDao object for table 'MM_TreeNode'.
	/// </summary>
	
	public partial class TreeNodeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected int _nodeid;
		protected int _parentpodeid;
		protected int _level;
        protected string _key;
		protected int _attributeid;
		protected string _caption;
        protected int _sortOrder;
        protected string _colorcode;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public TreeNodeDao() {}

        public TreeNodeDao(int pId, int pNodeid, int pParentnodeid, int pLevel, string pKEY, int pAttributeid, string pCaption, int pSortOrder,string pColorCode)
		{
			this._id = pId; 
            this._nodeid = pNodeid;
			this._level = pLevel;
			this._key = pKEY;
            this._parentpodeid = pParentnodeid;
			this._attributeid = pAttributeid;
            this._caption = pCaption;
            this._sortOrder = pSortOrder;
            this._colorcode = pColorCode;
		}

        public TreeNodeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int NodeID
		{
            get { return _nodeid; }
			set {_bIsChanged |= (_nodeid != value);  _nodeid = value; }
			
		}
        public virtual int ParentNodeID
        {
            get { return _parentpodeid; }
            set { _bIsChanged |= (_parentpodeid != value); _parentpodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _bIsChanged |= (_level != value); _level = value; }

        }

        public virtual string KEY
		{
			get { return _key; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("key", "key value, cannot contain more than 250 characters");
              _bIsChanged |= (_key != value);
              _key = value; 
			}
			
		}

        public virtual int AttributeID
		{
			get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}




        public virtual string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
                _bIsChanged |= (_caption != value);
                _caption = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _bIsChanged |= (_sortOrder != value); _sortOrder = value; }

        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _bIsChanged |= (_colorcode != value); _colorcode = value; }

        }
      
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string TreeNodeDao = "TreeNodeDao";			
			public const string Id = "Id";
            public const string NodeID = "NodeID";
            public const string ParentNodeID = "ParentNodeID";
            public const string Level = "Level";
            public const string KEY = "KEY";
            public const string Attributeid = "AttributeID";
            public const string Caption = "Caption";
            public const string SortOrder = "SortOrder";
            public const string ColorCode = "ColorCode";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string TreeNodeDao = "TreeNodeDao";
            public const string Id = "Id";
            public const string NodeID = "NodeID";
            public const string ParentNodeID = "ParentNodeID";
            public const string Level = "Level";
            public const string KEY = "KEY";
            public const string AttributeID = "AttributeID";
            public const string Caption = "Caption";
            public const string SortOrder = "SortOrder";
            public const string ColorCode = "ColorCode";
		}

		#endregion
		
		
	}
	
}
