﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata.Interface;
using System.Xml;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Metadata.Interface;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.Planning;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMetadataManager
    {
        /// <summary>
        /// Gets the module.      
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of Imodule</returns>
        IList<IModule> GetModule();
        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>List of Imodule</returns>
        IList<IModule> GetModuleByID(int ID);
        /// <summary>
        /// Inserts the update module.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="isenable">if set to <c>true</c> [isenable].</param>
        /// <param name="id">The id.</param>
        /// <returns>IModule</returns>
        int InsertUpdateModule(string caption, string description, bool isenable, int id = 0);

        /// <summary>
        /// Deletes the module.
        /// </summary>
        /// <param name="moduleid">The moduleid.</param>
        /// <returns>bool</returns>
        bool DeleteModule(int moduleid);

        /// <summary>
        /// Gets the modulefeature.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of modulefeatrues </returns>
        IList<IModuleFeature> GetModulefeature(int version);

        /// <summary>
        /// Inserts the update modulefeature.
        /// </summary>
        /// <param name="moduleid">The moduleid.</param>
        /// <param name="featureid">The featureid.</param>
        /// <param name="isenable">if set to <c>true</c> [isenable].</param>
        /// <returns>int</returns>
        int InsertUpdateModulefeature(int moduleid, int featureid, bool isenable, int ID);

        /// <summary>
        /// Deletes the module feature.
        /// </summary>
        /// <param name="moduleid">The moduleid.</param>
        /// <param name="featureid">The featureid.</param>
        /// <returns>bool</returns>
        bool DeleteModuleFeature(int ID);

        /// <summary>
        /// Gets the type of the entity.
        /// </summary>
        ///  /// <param name="moduleid">The moduleid.</param>
        /// <returns>List of Entitytype</returns>
        IList<IEntityType> GetEntityType(int ModuleID);


        /// <summary>
        /// Gets the details of WorkflowType.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of IWorkFlowType</returns>
        IList<IWorkFlowType> GetWorkFlowDetails();

        /// <summary>
        /// Gets the type of the entity.
        /// </summary>
        /// <returns>List of Entitytype from DB</returns>
        IList<IEntityType> GetEntityTypeIsAssociate();


        // <summary>
        /// Gets the type of the entity.
        /// </summary>
        /// <returns>List of Entitytype from DB</returns>
        /// 
        IList<IEntityType> GetEntityTypefromDB();

        /// <summary>
        /// Getting list of Options for Fulfillmentfinical Entity Type Attributes
        /// </summary>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <returns>IList of IAttribute</returns>
        IList<IAttribute> GetFulfillmentFinicalAttribute(int entityTypeId);
        /// <summary>
        /// Gets the type of the entity by ID.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>List of Entitytype</returns>
        IList<IEntityType> GetEntityTypeByID(int ID);

        /// <summary>
        /// Inserts the type of the update entity.
        /// </summary>
        /// <param name="metadataManagerProxy">The metadata manager proxy.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="IsSystemDefined">if set to <c>true</c> [is system defined].</param>
        /// <param name="Category">The category.</param>
        /// <param name="shortDescription">The ShortDescription.</param>
        /// <param name="colorCode">The ColorCode.</param>
        /// <param name="Id">The ID as Optional Parameter</param>
        /// <returns>INT.</returns>
        int InsertUpdateEntityType(string caption, string description, int moduleId, int category, string shortDescription, string colorCode, bool isassociate, int workFlowID, bool IsRootLeve, int Id = 0);

        /// <summary>
        /// Deletes the type of the entity.
        /// </summary>
        /// <param name="Entitytypeid">The entitytypeid.</param>
        /// <returns>bool</returns>
        int DeleteEntityType(int Entitytypeid);

        /// <summary>
        /// Gets the entity typefeature.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IEntityTypeFeature.</returns>
        IList<IEntityTypeFeature> GetEntityTypefeature(int version);


        /// <summary>
        /// Gets the entity typefeature By ID.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="ID">The ID.</param>
        /// <returns>List </returns>
        //IList<IFeature> GetEntityTypefeatureByID(int TypeID);
        /// <summary>
        /// Gets the entity typefeature.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IEntityTypeFeature.</returns>
        IList<IEntityTypeFeature> GetEntityTypefeatureByID(int entitytypeId = 0);

        /// <summary>
        /// Inserts the entity typefeature.
        /// </summary>
        /// <param name="typeid">The typeid.</param>
        /// <param name="featureid">The featureid.</param>
        /// <returns>INT</returns>
        int InsertEntityTypefeature(int typeid, int featureid, int id = 0);

        /// <summary>
        /// Deletes the entity type feature.
        /// </summary>
        /// <param name="Entitytypeid">The entitytypeid.</param>
        /// <param name="featureID">The feature ID.</param>
        /// <returns>bool</returns>
        bool DeleteEntityTypeFeature(int id);

        /// <summary>
        /// Gets the entitytyperelation.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IEntityTypeAttributeRelation</returns>
        IList<IEntityTypeAttributeRelation> GetEntitytyperelation(int version);

        /// <summary>
        /// Gets the entitytyperelation.
        /// </summary>
        /// <returns>List of IEntityTypeAttributeRelation</returns>
        IList<IEntityTypeAttributeRelation> GetEntityTypeAttributeRelationByID(int id);

        /// <summary>
        /// Gets the entitytyperelationwithLevels.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of GetEntityTypeAttributeRelationWithLevels</returns>
        IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(int id, int ParentId = 0, int ImpersonateUID = 0);

        string GetJSONEntityTypeAttributeRelationWithLevelsByID(int id, int ParentId = 0);


        /// <summary>
        /// Creates the entitytyperelation.
        /// </summary>
        /// <param name="entitytypeId">The entitytype id.</param>
        /// <param name="attributeId">The attribute id.</param>
        /// <param name="validationId">The validation id.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>INT</returns>
        int InsertUpdateEntityTypeAttributeRelation(int entitytypeId, int attributeId, string validationId, int sortOrder, string DefaultValue, bool InheritFromParent, bool IsReadOnly, bool ChooseFromParentOnly, bool IsValidationNeeded, string Caption, bool IsSystemDefined, string PlaceHolderValue, int MinValue, int MaxValue, int ID = 0, bool IsHelptextEnabled = false, string HelptextDecsription = "");

        /// <summary>
        /// Deletes the entity typerelation.
        /// </summary>
        /// <param name="entitytypeid">The entitytypeid.</param>
        /// <param name="attributeid">The attributeid.</param>
        /// <returns>bool</returns>
        bool DeleteEntityAttributeRelation(int ID);

        /// <summary>
        /// Gets the feature.
        /// </summary>
        /// <returns>List of IFeature </returns>
        IList<IFeature> GetFeature();
        /// <summary>
        /// Gets the attributetype.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IAttributeType </returns>
        IList<IAttributeType> GetAttributetype();


        IList<IAttribute> GetAttributeTypeByEntityTypeID(int EnitityTypeID, bool IsAdmin);
        /// <summary>
        /// Inserts the upadate attributetype.
        /// </summary>
        /// <param name="Caption">The caption.</param>
        /// <param name="ClassName">Name of the class.</param>
        /// <param name="IsSelectable">if set to <c>true</c> [is selectable].</param>
        /// <param name="DataType">Type of the data.</param>
        /// <param name="SqlType">Type of the SQL.</param>
        /// <param name="Length">The length.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <param name="Id">The id.</param>
        /// <returns>INT</returns>
        int InsertUpadateAttributetype(string Caption, string ClassName, bool IsSelectable, string DataType, string SqlType, int Length, bool IsNullable, int Id);
        /// <summary>
        /// Gets the attribute by ID.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>List of IAttribute.</returns>
        IList<IAttribute> GetAttributeByID(int ID);
        /// <summary>
        /// Deletes the attributetype.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteAttributetype(int Id);

        IList<IAttribute> GetAttributefromDB();

        IList<IAttribute> GetAttributesforDetailFilter();
        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <returns>List of IAttribute.</returns>
        IList<IAttribute> GetAttribute();

        /// <summary>
        /// Gets the attribute with Tree level values.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// List of IAttribute.
        /// </returns>
        IList<IAttribute> GetAttributeWithLevels();
        /// <summary>
        /// Inserts the update attribute.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="attributetypeid">The attributetypeid.</param>
        /// <param name="issystemdefined">if set to <c>true</c> [issystemdefined].</param>
        /// <param name="isforeign">if set to <c>true</c> [isforeign].</param>
        /// <param name="ismultiselect">if set to <c>true</c> [ismultiselect].</param>
        /// <param name="foreigntablename">The foreigntablename.</param>
        /// <param name="foreignidcolumn">The foreignidcolumn.</param>
        /// <param name="foreignattributecolumn">The foreignattributecolumn.</param>
        /// <param name="foreignorderbytable">The foreignorderbytable.</param>
        /// <param name="foreignorderbyidcolumn">The foreignorderbyidcolumn.</param>
        /// <param name="foreignorderbycolumnname">The foreignorderbycolumnname.</param>  
        /// <param name="id">The id.</param>
        /// <returns>int</returns>   
        int InsertUpdateAttribute(string caption, string description, int attributetypeid, bool issystemdefined, bool isspecial, int id = 0);

        /// <summary>
        /// Deletes the attribute.
        /// </summary>
        /// <param name="attributeid">The attributeid.</param>
        /// <returns>bool</returns>
        int DeleteAttribute(int attributeid);

        /// <summary>
        /// Gets the option.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IOption </returns>
        IList<IOption> GetOption(int version);


        /// <summary>
        /// Gets the Options
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of IOption</returns>
        IList<IOption> GetOptionListByID(int id, bool isforadmin);

        IAttributeSequence GetAttributeSequenceByID(int id, bool isforadmin);

        /// <summary>
        /// Gets the Options based on attributeid
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="EntityID">The EntityID.</param>
        /// <returns>List of IOption</returns>
        IList<IOption> GetOptionDetailListByID(int id, int entityID);

        /// <summary>
        /// Inserts the update option.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="attributeid">The attributeid.</param>
        /// <param name="sortorder">The sortorder.</param>
        /// <param name="id">The id.</param>
        /// <returns>int</returns>
        int InsertUpdateOption(string caption, int attributeid, int sortorder, int id = 0);
        bool InsertAssetAccessOption(string caption, int sortorder, int id = 0);
        IList GetAssestAccessSaved();
        int[] GetAssignedAssetRoleIDs(int AssetId);
        IList GetAssignedAssetRoleName(int RoleId);

        /// <summary>
        /// Deletes the option.
        /// </summary>
        /// <param name="optionid">The optionid.</param>
        /// <returns>bool</returns>
        bool DeleteOption(int optionid);
        bool DeleteAssetAccessOption(int optionid);

        /// <summary>
        /// Gets the multi select.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IMultiSelect</returns>
        IList<IMultiSelect> GetMultiSelect(int version);

        /// <summary>
        /// Inserts the multi select.
        /// </summary>
        /// <param name="entityid">The entityid.</param>
        /// <param name="attributeid">The attributeid.</param>
        /// <param name="optionid">The optionid.</param>
        /// <returns>IMultiSelect Object.</returns>
        int InsertMultiSelect(int entityid, int attributeid, string optionid);

        /// <summary>
        /// Deletes the MultiSelect.
        /// </summary>
        /// <param name="EntityID">The EntityID.</param>
        /// <param name="AttributeID">The AttributeID.</param>
        /// <returns>bool</returns>
        bool DeleteMultiSelect(int ID);

        /// <summary>
        /// Gets the treelevel.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of ITreeLevel.</returns>
        IList<ITreeLevel> GetTreelevel(int version);

        /// <summary>
        /// Gets the treelevel by AttributeID.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="version">The version.</param>
        /// <param name="AttributeID">The AttributeID.</param>
        /// <param name="isAdminsettings">The IsAdminsettingsPresent.</param>
        /// <returns>List of ITreeLevel </returns>
        IList<ITreeLevel> GetTreelevelByAttributeID(int AttributeID, bool isAdminsettings);
        /// <summary>
        /// Inserts the update treelevel.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="Levelname">The levelname.</param>
        /// <param name="attributeid">The attributeid.</param>
        /// <param name="id">The id.</param>
        /// <returns>ITreeLevel Object.</returns>
        int InsertUpdateTreelevel(int level, string Levelname, int attributeid, bool ispercentage, int id = 0);

        /// <summary>
        /// Deletes the tree level.
        /// </summary>
        /// <param name="treelevelid">The treelevelid.</param>
        /// <returns>bool</returns>
        bool DeleteTreeLevel(int treelevelid);

        /// <summary>
        /// Gets the tree node.
        /// </summary>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="isAdminSettings">The IsAdminSettings.</param>
        /// <returns>string</returns>
        string GetTreeNode(int attributeID, bool isAdminSettings, int parententityID = 0);

        IList<DropDownTreePricing> GetDropDownTreePricingObject(int attributeID, bool isrootentity, bool isFetchParent = false, int entityid = 0, int parentid = 0);

        IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParent(int attributeID, bool isinheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0);

        IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParentDetail(int attributeID, bool isinheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0);

        IList<DropDownTreePricing> GetDropDownTreePricingObjectDetail(int attributeID, bool isinheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0);

        /// <summary>
        /// Inserts the tree node.
        /// </summary>
        /// <param name="NodeID">The node ID.</param>
        /// <param name="ParentNodeID">The parent node ID.</param>
        /// <param name="Level">The level.</param>
        /// <param name="KEY">The KEY.</param>
        /// <param name="AttributeID">The attribute ID.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="id">The id.</param>
        /// <returns>int.</returns>
        int InsertTreeNode(int NodeID, int ParentNodeID, int Level, string KEY, int AttributeID, string Caption, int SortOrder, string colorcode, int id = 0);

        int InsertAttributeSequencePattern(JObject jobj);

        /// <summary>
        /// To insert, update tree node in Bulk
        /// </summary>
        /// <param name="metadataManagerProxy"></param>
        /// <param name="jObject"></param>
        /// <param name="attributID"></param>
        /// <returns></returns>
        int InsertUpdateTree(JObject jObject, JArray treeLevelObj, int attributID);

        /// <summary>
        /// Deletes the tree node.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteTreeNode(int Id);

        /// <summary>
        /// Gets the tree value.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of ITreeValue</returns>
        IList<ITreeValue> GetTreeValue(int version);

        /// <summary>
        /// Inserts the update tree value.
        /// </summary>
        /// <param name="attributeid">The attributeid.</param>
        /// <param name="nodeid">The nodeid.</param>
        /// <param name="id">The id.</param>
        /// <returns>int.</returns>
        int InsertUpdateTreeValue(int attributeid, int entityid, int nodeid, int id = 0);

        /// <summary>
        /// Deletes the tree value.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteTreeValue(int id);

        /// <summary>
        /// Gets the validation.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of IValidation</returns>
        IList<IValidation> GetValidation(int version);

        /// <summary>
        /// Inserts the update validation.
        /// </summary>
        /// <param name="Caption">The caption.</param>
        /// <param name="optionid">The optionid.</param>
        /// <param name="id">The id.</param>
        /// <returns>IValidation Object.</returns>
        int InsertUpdateValidation(IList<IValidation> ValList, int AttributeId, int EntityTypeID, int AttributeTypeID, int ID = 0);


        IValidation CreateValidationInstace();

        IList<IValidation> GetAttributeValidationByEntityTypeId(int EntityTypeID, int AttributeId);

        List<List<string>> GetValidationDationByEntitytype(int EntityTypeID);


        /// <summary>
        /// Deletes the validation.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteValidation(int id);

        /// <summary>
        /// Syncs to db.
        /// </summary>
        /// <returns></returns>
        bool SyncToDb();

        /// <summary>
        /// List.
        /// </summary>
        /// <returns>IListofRecord</returns>
        IListofRecord ListSetting(string elementNode);

        /// <summary>
        /// Inserting EntityMultiSelectAttribute values
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="multiselectAttribute">The Selected Attribute Option values</param>
        /// <returns>bool</returns>
        bool MultiSelectAttributeInsertion(IList<IMultiSelect> multiselectAttribute);
        /// <summary>
        /// Getting EntityMultiSelectAttribute values
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="IList<IMultiSelect> multiselectAttributes">The Selected Attribute Option values</param>
        /// <returns>bool</returns>
        bool UpdateMultiSelectAttribute(IList<IMultiSelect> multiselectAttributes);
        /// <summary>
        /// Getting EntityMultiSelectAttribute values
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityId">The EntityID</param>
        /// <returns>IList<IMultiSelect></returns>
        IList<IMultiSelect> GetMultiSelectAttributes(int entityId);

        //  /// <summary>
        ///// AttributeFilter
        ///// </summary>
        ///// <param name="proxy">The proxy.</param>
        ///// <param name="ListOfRecordSetting">Record Settings</param>
        ///// <returns>IList<IEntityTypeAttributeRelationwithLevels></returns>
        //IList<IEntityTypeAttributeRelationwithLevels> AttributeFilter(ListSettings listSettings);

        string GetVersionsCountAndCurrentVersion(string key);
        bool UpdateActiveVersion(int version);
        bool UpdateWorkingVersion(int version);


        /// <summary>
        /// AttributeFilter
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="ListOfRecordSetting">Record Settings</param>
        /// <returns>IList<IEntityTypeAttributeRelationwithLevels></returns>
        IList<AttributeSettings> AttributeFilter(ListSettings listSettings);

        /// <summary>
        /// Gets the tree node.
        /// </summary>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="level">The Level.</param>
        /// <returns>List of ITreeNode</returns>
        IList<ITreeNode> GetTreeNodeByLevel(int attributeID, int level);

        /// <summary>
        /// Gettign Attributes from AdminSettings xml and based on AttributeId and Level getting Nodes.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>IList of IFiltersettingsAttributeData</returns>
        IList<IFiltersettingsAttributes> GettingFilterAttribute(int typeId, string FilterType, int OptionFrom, JArray Ids);
        IList<BrandSystems.Marcom.Core.User.Interface.IUser> GettingFilterEntityMember(int typeId, string FilterType, int OptionFrom, JArray Ids);

        IList<IEntityAttributeDetails> GetAttributesForDetailBlock(int EntityID);

        bool SaveDetailBlock(int ID, int EntityID, string NewValue);

        bool SaveDetailBlockForLevels(int EntityID, int AttributeTypeid, int OldValue, List<object> NewValue, int Level, int apiUser = 0);

        bool UpdateDropDownTreePricing(int EntityID, int AttributeTypeid, int AttributeId, IList<ITreeValue> NewValue);

        bool SaveDetailBlockForTreeLevels(int EntityID, int AttributeTypeid, int attributeid, IList<ITreeValue> NewValue, JArray jroldTree, JArray jrnewtree);

        /// <summary>
        /// Getting list of Entity Id's which are not Parent of this EntityTypeID
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityTypeId">The EntityTypeID.</param>
        /// <returns>IList of IEntityType</returns>
        IList<IEntityType> GettingChildEntityTypes(int entityTypeId);

        /// <summary>
        /// Adds the EntityType releation.
        /// </summary>
        /// <param name="parentactivityTypeId">The parentactivity type id.</param>
        /// <param name="childactivityTypeid">The childactivity typeid.</param>
        /// <param name="Id">The Id as option parameter.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>int</returns>
        int InsertEntityTypeHierarchy(int parentactivityTypeId, int childactivityTypeid, int sortOrder, int Id = 0);

        /// <summary>
        /// Getting list of EntityHeirarchy based on EntityTypeID
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityTypeId">The EntityTypeID.</param>
        /// <returns>IList of IEntityHeirarchy</returns>
        IList<IEntityTypeHierarchy> GettingEntityTypeHierarchy(int entityTypeId);

        IList<IEntityType> GettingEntityTypeHierarchyForRootLevel(int entityTypeID);

        bool GetOwnerForEntity(int EntityID);

        IList<IEntityType> GettingEntityTypeHierarchyForAdminTree(int entitytypeid, int ModuleID = -1);
        /// <summary>
        /// Deletes the entity type Hierarchy.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="Entitytypeid">The entitytypeid.</param>
        /// <returns>bool</returns>
        bool DeleteEntityTypeHierarchy(int id);



        string GetOptionsFromXML(string elementNode, int typeid, int TenantID);

        /// <summary>
        /// Getting list of Entity Id's which are having period
        /// </summary>
        /// <returns>IList of IEntityType</returns>
        IList<IEntityType> GetFulfillmentEntityTypes();
        IList<IEntityType> GetFulfillmentEntityTypesfrCal();

        /// <summary>
        /// Getting list of Options for Fulfillment Entity Type Attributes
        /// </summary>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <returns>IList of IAttribute</returns>
        IList<IAttribute> GetFulfillmentAttribute(int entityTypeId);

        /// <summary>
        /// Getting list of Options for Fulfillment Entity Type Attribute Options
        /// </summary>
        /// <param name="attributeId">The AttributeID</param>
        /// <param name="attributeLevel">The AttributeLevel</param>
        /// <returns>IList of IOptions</returns>
        IList<IOption> GetFulfillmentAttributeOptions(int attributeId, int attributeLevel = 0);

        List<int> GetAllEntityTypes();

        IListofRecord ListofRecords(int StartRowNo, int MaxNoofRow, int FilterID, IList<IFiltersettingsValues> filterSettingValues, int[] IdArr, string SortOrderColumn, bool IsDesc, ListSettings listSetting, bool IncludeChildren, int enumEntityTypeIds, int EntityID, bool IsSingleID, int UserID, int Level, bool IsobjectiveRootLevel, int ExpandingEntityID, bool IsWorkspaces = false, int actualtype = 6, bool IsCostCentreRootLevel = false, bool Iscustomentity = false, List<int> typeids = null);

        IListofRecord BriefListofRecords(int StartRowNo, int MaxNoofRow, int FilterID, IList<IFiltersettingsValues> filterSettingValues, int[] IdArr, string SortOrderColumn, bool IsDesc, ListSettings listSetting, bool IncludeChildren, int enumEntityTypeIds, int EntityID, bool IsSingleID, int UserID, int Level, bool IsobjectiveRootLevel, int ExpandingEntityID, bool IsWorkspaces = false, int actualtype = 6, bool IsCostCentreRootLevel = false, bool Iscustomentity = false, List<int> typeids = null);


        IList<int> ListofReportRecords(int FilterID, IList<IFiltersettingsValues> filterSettingValues, int[] IdArr, string SortOrderColumn, bool IsDesc, ListSettings listSetting, bool IncludeChildren, int enumEntityTypeIds, int EntityID, bool IsSingleID, int UserID, int Level, bool IsobjectiveRootLevel, int ExpandingEntityID);

        IList GetPath(int EntityID, bool IsWorkspace = false);

        bool DeleteOptionByAttributeID(int attributeId);

        bool DeleteSequencByeAttributeID(int attributeId);

        /// Inserts the EntityHistory.
        /// </summary>
        /// <param name="EntityID">The EntityID.</param>
        /// <param name="VisitedOn">The VisitedOn tieming .</param>
        /// <param name="UserID">particular  userId</param>
        int InsertEntityHistory(int EntityID, int UserID);

        /// <summary>
        /// Getting list of EntityHistory
        /// </summary>
        /// <param name="UserID">The UserID</param>
        ///  <param name="Topx">The top itme</param>
        /// <returns>IList of EntityHistory</returns>
        IList<IEntityHistory> EntityHistory_Select(int UserID, int Topx);
        /// <summary>
        /// Getting list of EntityHistory
        /// </summary>
        /// <param name="UserID">The EntityID</param>
        /// <param name="Topx">The top itme</param>
        /// <returns>IList of EntityHistory</returns>
        IList TopxActivity_Select(int UserID, int Topx);
        IList TopxMyTask_Select(int UserID, int Topx);

        int InsertWorkFlow(string name, string description, IList<IWorkFlowSteps> listWorkflowSteps, IList<IWorkFlowStepPredefinedTasks> listWorkflowStepstasks, int WorkFlowID = 0);

        IWorkFlowSteps CreateWorkFlowStepsInstace();
        IWorkFlowStepPredefinedTasks CreateWorkFlowStepPredefinedTasksInstace();


        /// <summary>
        /// Inserts the type of the update entity.
        /// </summary>
        /// <param name="metadataManagerProxy">The metadata manager proxy.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="IsSystemDefined">if set to <c>true</c> [is system defined].</param>
        /// <param name="Category">The category.</param>
        /// <param name="shortDescription">The ShortDescription.</param>
        /// <param name="colorCode">The ColorCode.</param>
        /// <param name="Id">The ID as Optional Parameter</param>
        /// <returns>INT.</returns>
        int InsertUpdatePredefinedWorkTask(string caption, string description, IList<IPredefinedWorflowFileAttachement> fileAttachments, int workflowtypeID, int Id = 0);

        IList<IPredefinedTasks> GetPredefinedWorkflowByID(int ID);

        IList<IWorkFlowType> GetWorkFlowDetailsByID(int ID);

        /// <summary>
        /// Gets the details of WorkflowStep by ID.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of IWorkFlowType</returns>
        IList<IWorkFlowSteps> GetWorkFlowTStepByID(int ID);

        /// <summary>
        /// Gets the details of WorkflowStep PredefinedTask by ID.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of IWorkFlowType</returns>
        IList<IWorkFlowStepPredefinedTasks> GetWorkFlowStepPredefinedTaskByID(string IDs);

        /// <summary>
        /// Deletes the Workflow and related tables.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteWorkflowByID(int id);

        /// <summary>
        /// Returns FileAttachment class.
        /// </summary>
        IPredefinedWorflowFileAttachement PredefinedWrkflwFileAttachmentService();

        ///GetPredefinedWorkflow file attached By ID
        /// <summary>
        /// get the Predefined Workflow file attached  by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>Ilist of IPredefinedTasks</returns>
        IList<IPredefinedWorflowFileAttachement> GetPredefinedWorkflowFilesAttchedByID(int ID);

        /// <summary>
        /// Deletes the predefined Workflow file.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeletePredWorkflowFileByID(string id);

        bool DeletePredefinedWorkflowByID(int id, string caption, string description, int workflowtypeID);

        IList<IFeature> GetModuleFeatures(int moudleID);

        IList<IFeature> GetModuleFeaturesForNavigation(int moudleID);

        int InsertUpdateFeature(string caption, string description, int moduleID, bool isenable, int id);

        /// <summary>
        /// Gets the treelevel by AttributeID.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="version">The version.</param>
        /// <param name="AttributeID">The AttributeID.</param>
        /// <returns>List of ITreeNode </returns>
        IList<ITreeNode> GetTreeNodeByAttributeID(int AttributeID);


        /// <summary>
        /// Gets the metadata settings.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="version">The version.</param>
        /// <returns>List of Imetadatasettings </returns>
        IList<IMetadataVersion> GetMetadataVersion();

        ///Insert MetadataVersion
        /// <summary>
        /// Insert new metadataversion.
        /// </summary>
        /// <param name="caption">The Name.</param>
        /// <param name="description">The description.</param>
        /// <returns>id</returns>
        int InsertMetadataVersion(int metadataID, string metdataName, string metadataDescription, int selectedMetadataVer);


        IList<IEntityType> GetTaskFulfillmentEntityTypes();

        ///GetXMlvaluesfromsysnctoDB
        /// <summary>
        /// Get if any value present or not in the sysnctodb
        /// </summary>
        /// <returns>ilist</returns>
        int GetXmlNodes_CheckIfValueExistsOrNot();

        IList<IEntityType> GettingEntityForRootLevel(bool IsRootLevel = true);

        /// <summary>
        /// 
        /// Getting list of EntityHeirarchy based on EntityTypeID
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityTypeId">The EntityTypeID.</param>
        /// <returns>IList of IEntityHeirarchy</returns>
        IList<IEntityTypeHierarchy> GettingEntityTypeHierarchyForChildActivityType(int entityTypeId);

        IList<IFeature> GetFeaturesForTopNavigation();
        IList<INavigation> GetUserEnabledNavigations();

        /// <summary>
        /// Gets the GetAttributeRelationByIDs.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of GetAttributeRelationByIDs</returns>
        IList<IAttribute> GetAttributeRelationByIDs(string ids);

        /// <summary>
        /// Creates the entitytyperelation.
        /// </summary>
        /// <param name="entitytypeId">The entitytype id.</param>
        /// <param name="attributeId">The attribute id.</param>
        /// <param name="validationId">The validation id.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>INT</returns>
        int InsertUpdateAttributeToAttributeRelations(int entitytypeId, int attributetypeID, int attributeId, int attributeOptionID, int attributeLevel, string attributeRelationID, int ID = 0);

        /// <summary>
        /// Deletes the entity attributetoattributerelation.
        /// </summary>
        /// <param name="entitytypeid">The entitytypeid.</param>
        /// <returns>bool</returns>
        bool DeleteAttributeToAttributeRelation(int entityID);

        /// <summary>
        /// Gets the Get Attribute To AttributeRelation.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of GetAttributeRelationByIDs</returns>
        IList<IAttributeToAttributeRelations> GetAttributeToAttributeRelationsByID(int entityId);

        IList<IAttributeToAttributeRelations> GetAttributeToAttributeRelationsByIDForEntity(int entityTypeID, int entityId = 0);

        IList<IOption> GetAttributeOptionsInAttrToAttrRelations(string[] attributeId);

        int InsertUpdateEntityTypeStatusOption(int EntityTypeID, string StatusOption, int SortOrder, int ID = 0, string ColorCode = "");
        int InsertUpdateDamTypeFileExtensionOption(int EntityTypeID, string ExtensionOption, int SortOrder, int ID = 0);

        IList<IEntityTypeStatusOptions> GetEntityStatusOptions(int EntityTypeID);
        IList<IDamTypeFileExtension> GetDamTypeFileExtensionOptions(int EntityTypeID);
        IList<IDamTypeFileExtension> GetAllDamTypeFileExtensionOptions();

        bool DeleteEntityTypeStatusOptions(int EntityTypeID);
        bool DeleteDamTypeFileExtensionOptions(int EntityTypeID);
        /// <summary>
        /// Get the entity type status
        /// </summary>
        /// <param name="entityTypeID"></param>
        /// <param name="isAdmin"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IList<IEntityTypeStatusOptions> GetEntityStatus(int entityTypeID, bool isAdmin, int entityId = 0);

        /// <summary>
        /// Get the context menu entity type
        /// </summary>
        /// <returns>IList<IEntityType></returns>
        IList<IEntityType> RootLevelEntityTypeHierarchy();

        /// <summary>
        /// Get the context menu child entity type
        /// </summary>
        /// <returns>IList<IEntityType></returns>
        IList<IEntityType> ChildEntityTypeHierarchy();

        /// <summary>
        /// Gets the Options based on attributeid
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="EntityID">The EntityID.</param>
        /// <returns>List of IOption</returns>
        IList<IOption> GetOptionDetailListByIDOptimised(int entityTypeID, int entityID);

        /// <summary>
        /// Force view creation and push the Schema
        /// </summary>
        /// <returns>Status</returns>
        int ReportViewCreationAndPushSchema();

        IList<IFeature> GetAllModuleFeatures(int moudleID);

        IAttributeGroupAttributeRelation CreateAttributeRelationInstace();

        int InsertUpdateAttributeGroupAndAttributeRelation(int attributegroupId, string attributegroupcaption, string attributegroupdescription, IList<IAttributeGroupAttributeRelation> ObjattributerelationList, bool IsPredefined = false);

        IList<IAttributeGroup> GetAttributeGroup();

        IList<IAttributeGroupAttributeRelation> GetAttributeGroupAttributeRelation(int attributegroupId);

        int DeleteAttributeGroup(int attributegroupid);

        int DeleteAttributeGroupAttributeRelation(int attributeRelationId);

        int InsertUpdateEntityTypeAttributeGroup(IList<IEntityTypeAttributeGroupRelation> ObjattributegroupList, IList<IAttributeGroupRoleAccess> attrgroupaccess, int entitytypeId, string globalaccessids);

        IEntityTypeAttributeGroupRelation CreateAttributeGroupRelationInstace();

        List<object> GetEntityTypeAttributeGroupRelation(int entitytypeId, int EntityID = 0, int AttributeGroupId = 0);

        int DeleteEntityTypeAttributeGroupRelation(int attributegroupId);

        IList<IEntityTypeAttributeGroupRelationwithLevels> GetAttributeGroupAttributeOptions(int GroupID, int EntityID, int GroupRecordID = 0);

        IList GetEntityAttributesGroupValues(int EntityID, int EntityTypeID, int GroupID, bool IsCmsContent, int pageNo = 0, int PageSize = 0);

        IList<IEntityType> DuplicateEntityType(int entitytypeId, string entitycaption, string entityshortdesc, string entitydescription, string entitycolor);

        IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails(int id, int ParentId = 0);
        IList<IEntityTypeAttributeGroupRelationwithLevels> GetUserDetailsAttributes(int TypeID, int UserID = 0);
        IList<IOption> GetOptionDetailListByIDForMyPage(int attributeID, int userID);
        bool SaveDetailBlockForLevelsFromMyPage(int UserID, int AttributeTypeid, int OldValue, List<object> NewValue, int Level);
        IList<IUserVisibleInfo> GetUserVisiblity();
        int InsertUpdateUserVisibleInfo(int AttributeId, int IsEnable);
        Tuple<int, int> InsertUpdateFinancialAttribute(string caption, string description, int attributetypeid, bool issystemdefined, bool isspecial, int finatttype, int id = 0);
        int InsertUpdateFinancialAttrOptions(string caption, int attributeid, int sortorder, int id = 0);

        IList<IFinancialAttribute> GetFinancialAttribute();
        int DeleteFinancialAttribute(int attributeid);
        bool DeleteFinancialOptionByAttributeID(int attributeId);
        bool UpdateFinancialMetadata(int ID, string Caption, string Description, int FinType, int AttribtueTypeid, bool IsSystemDefined, bool IsColumn, bool IsTooltip, bool IsCommitTooltip, int SortOrder);
        bool UpdateFinMetadataSortOrder(int ID, int SortOrder);
        IList<IFinancialOption> GetFinancialAttributeOptions(int ID);
        bool DeleteFinancialOption(int optionid);

        string GettingEntityTypeTreeStructure(int entityTypeId);

        List<string> GetSubEntityTypeAccessPermission(int entityId, string entityTypeId, int moduleID);

        int InsertUpdateEntityTypeRoleAccess(string Caption, int EntityTypeID, int EntityRoleID, int ModuleID, int SortOrder, int ID);

        Tuple<IList<IEntityTypeRoleAcl>, IList<IEntityTypeRoleAcl>> GetEntityTypeRoleAcl(int EntityTypeID);

        bool DeleteEntityTypeRoleAcl(int ID);

        /// <summary>
        /// GetRoleFeatures
        /// </summary>
        /// <param name="GlobalRoleID"></param>
        /// <returns>return Ilist of Rolefeatures</returns>
        IList<IRoleFeature> GetRoleFeatures(int GlobalRoleID);

        /// <summary>
        /// SaveUpdateRoleFeatures
        /// </summary>
        /// <param name="GlobalRoleID"></param>
        /// <param name="ModuleID"></param>
        /// <param name="FeatureID"></param>
        /// <param name="IsChecked"></param>
        /// <returns>return true if save is success</returns>
        bool SaveUpdateRoleFeatures(int GlobalRoleID, int ModuleID, int FeatureID, bool IsChecked, int GlobalAclId);

        bool IsRoleExist(int ID);

        /// <summary>
        /// Gets the tree node.
        /// </summary>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="isAdminSettings">The IsAdminSettings.</param>
        /// <returns>string</returns>
        string GetAttributeTreeNode(int attributeID, int entityID);


        /// <summary>
        /// Gets the tree node from parent.
        /// </summary>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="isAdminSettings">The IsAdminSettings.</param>
        /// <returns>string</returns>
        string GetAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent, bool isinheritfromParent = false);

        /// <summary>
        /// Gets the tree node from parent for detail block.
        /// </summary>
        /// <param name="attributeID">The AttributeID.</param>
        /// <param name="isAdminSettings">The IsAdminSettings.</param>
        /// <returns>string</returns>
        string GetDetailAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent);

        bool GetWorkspacePermission();
        IList<DropDownTreePricing> PricingValues(int filterid, int attributeid, int attributetypeid);

        IList<IEntityType> GetDAMEntityTypes();

        bool SaveDetailBlockForLink(int EntityID, int AttributeTypeid, int attributeid, string url, List<object> name, int linktype, int Module);

        bool DamDeleteEntityAttributeRelation(int ID, int attributeid, int entitytypeid);

        bool DeleteEntitytypeAttributeGrpAccessRole(int entityID);

        IAttributeGroupRoleAccess CreateAttributeGroupRoleAccessInstace();

        List<object> FetchEntityStatusTree(int EntityId);

        /// <summary>
        /// Gets the type of the task entity.
        /// </summary>
        /// <returns>
        /// List of Entitytype
        /// </returns>
        IList<IEntityType> GetTaskEntityType();

        IList<EntitytasktypeDao> GetEntityTaskType(int entitytypeid);

        int InsertUpdateEntityTaskType(int EntityTypeID, int taskType, int ID);

        /// <summary>
        /// Gets the type of the task entity.
        /// </summary>
        /// <returns>
        /// List of Entitytype
        /// </returns>
        IList<IEntityType> GetTaskTypes();

        string[] GetAttributeGroupImportedFileColumnName(string fileid);

        IList InsertImportedAttributeGroupData(List<object> LabelColumnValue, int EntityId, string attrgrpName, string filename, int AttrGrpID);

        IList GetEntityAttributesGroupLabelNames(int EntityID, int GroupID);

        IList<ITagOption> GetTagOptionList(bool isforadmin);

        int InsertUpdateTagOption(string caption, int attributeid, int sortorder, int id = 0, bool IsforAdmin = false, int PopulartagsToshow = 0);

        bool DeleteTagOption(int optionid);

        IList<IAssetAccess> GetAssetAccess();

        IListofRecord GetListOfEntity(int StartRowNo, int MaxNoofRow, int FilterID, IList<IFiltersettingsValues> filterSettingValues, int[] IdArr, string SortOrderColumn, bool IsDesc, ListSettings listSetting, bool IncludeChildren, int enumEntityTypeIds, int EntityID, bool IsSingleID, int UserID, int Level, bool IsobjectiveRootLevel, int ExpandingEntityID, bool IsWorkspaces = false);

        List<int> GetAllEntityTypesofAllLevels();

        IList GetEntitiesList(int UserID, int ParentID, int TypeID);

        string ExportMetadataSettingsXml(int MetadataVerId, string VersionName);

        string ImportMetadataSettingsXml(int MetadataVerId, Dictionary<string, bool> listofimporttypes, string ImportFileID);

        List<object> GetOwnerName(int EntityID);

        List<AttributeSettings> GetSpefictypeAttributefromDB(List<int> typeIds, List<int> atrrIds);

        IList GetUserInvolvedEntities(string UniqueKey, int Userid);

        IList CheckCustomHierarchy(int parentEntitytypeID, int[] subtype, int userId);

        int getTree(int ID);

        int getParentID(int entityID);

        ///<summary>
        /// Returns a sequence number as per the attribute selection.
        ///</summary>
        ///<param name="shortDesc">The Entity type short description.</param>
        ///<param name="Sd">The Entity type short description.</param>
        string SequenceNumber(int attributeId, int entitytypeid, string Sd);

        ///<summary>
        /// PerformAutoTaskOperation
        ///</summary>
        ///<param name="jobj">The JObject</param>
        int PerformAutoTaskOperation(AutoTaskCls jobj);

        IList<int> GetEntityGlobalAccessInDetail(int EntityId);

        bool InsertEntityGlobalAccessInDetail(int EntityId, int[] GlobalRoleID);
        IList GetTaskLinkedToEntity(int Entitytypeid);

        /// <summary>
        /// Get all the CMS custom fonts defined
        /// </summary>
        /// <param name="proxy"></param>
        /// <returns></returns>
        IList<object> GetListOfCmsCustomFont();

        bool InsertUpdateCmsCustomFont(string ListOfCustomFonts, string kitId, int ProviderId);

        bool DeleteCmsCustomFont(int ProviderId);

        bool InsertUpdateCmsCustomStyle(int Id, string ClassName, string CssCode);

        IList<ICmsCustomStyle> GetListOfCmsCustomStyle();

        bool DeleteCmsCustomStyle(int Id);

        bool InsertUpdateCmsTreeStyleSettings(string BackgroundClr, string ForegroundClr, string BorderClr, string LeftborderHlghtClr, string SelBackgroundClr, string MouseOvrBackgroundClr);

        string GetCmsTreeStyle();

        string SaveImageFromBaseFormat(string imageinbase);
        bool CheckBeforeRemovechecklist(int ID);

        bool InsertApprovalRole(int Id, string Caption, string Description);

        IList GetApprovalRoles();

        bool DeleteApprovalRole(int Id);

        Tuple<IList<IEntityType>, IList<object>> GetEntityTypeAttributes();

        bool updateentitytypehelptext(int EntityTypeID, int AttributeID, string HelptextDecsription, bool IsHelptextEnabled);
        bool UpdateFinancialHelptext(int ID, bool IsHelptextEnabled, string HelptextDecsription, bool isfromtable, string attrID);
        IList<object> GetAllFinancialAttribute();
        string ExportEntityHelpTextListtoExcel(int TenantID);

        string InsertEntityHelpTextImport(string FileImport);
        int InsertUpdateEntityObjectiveType(int EntityTypeID, int objectiveType, int ID, int objectiveBaseTypeId, int unitId);

        IList<object> GetObjectiveEntityType(bool IsAdmin = true);

        IList<IAttribute> GetObjectiveTypeAttributeByEntityType(int EnitityTypeID, bool IsAdmin);
        IList GetPreDefinedAttributeGroupAttributeOptions(int GroupID, int entityId, int EntityTypeID, IList<object> filterValues = null, bool isFromSave = false, int[] SelectedIDs = null, int pageNo = 0, int PageSize = 0, string sortColumnBy = "", bool IsSelectAll = false);

        IList<IFiltersettingsAttributes> GetAttrGroupSearchAttributes(int GroupID);

        JObject GetListOfAttributesGroupValuesToMiddleWare(int EntityTypeID, int AttrGroupId);

        IList<IAttributeData> GetEntityAttributesGroupHeaders(int entityId, int EntityTypeID, int GroupID);

        List<object> getEntityToSetOverallStauts(int moduleID);

        int getCurrentEntityStatus(int entityID);

        bool updateEntityOverviewStatus(int entityTypeID, int Entityid);

        Tuple<IList<object>, IList<object>> getPlanandObjTypes();

        IList InheritParentAttributeGroupRecords(int parentId, int EntityTypeID, int AttributeGroupId);

        IList<IAttribute> AttributeDetailsbyID(int AttrId);

        bool GetMetadataLockStatus();

        bool SetMetadataLockStatus(bool status);

        bool ClearCache();
    }
}
