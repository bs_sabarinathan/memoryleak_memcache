using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// INavigationAccess interface for table 'CM_NavigationAccess'.
    /// </summary>
    public interface INavigationAccess
    {
        #region Public Properties

        int Navigationid
        {
            get;
            set;

        }

        int GlobalRoleid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
