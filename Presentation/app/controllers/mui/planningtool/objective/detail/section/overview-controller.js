﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailoverviewCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $stateParams, $location, $window, $sce, $translate, ObjectiveoverviewService, PlanningService, CommonService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        var model;
        $scope.StartDate = false;
        $scope.EndDate = false;
        $scope.DateRangeFormat = $scope.format;
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            if (model == "StartDate") { $scope.StartDate = true; $scope.EndDate = false; }
            else { $scope.StartDate = false; $scope.EndDate = true; }
        };
        $scope.PerioddirectiveCalanderopen = function ($event, attrid, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") $scope.fields["PeriodStartDateopen_" + attrid] = true;
            else $scope.fields["PeriodEndDateopen_" + attrid] = true;
        };
        var cancelNewsTimerevent;
        var FeedInitiated = false;
        $scope.OwnerName = '';
        $('#ObjectiveOverviewDiv').on('onObjOverViewDetail', function (event, EntityTypeID) {
            PagenoforScroll = 0;
            $scope.processed.index = 0;
            if (NewsFeedUniqueTimer != undefined)
            { $timeout.cancel(NewsFeedUniqueTimer); }
            $scope.TaskListSummerydetal = [];
            $scope.GetEntityMembers();
            $scope.LoadNewsFeedBlockForObjective();
            $scope.DynamicTaskListDescriptionObj = "-";
            EnableDisableLoader();
            LoadObjRelatedData();
        });
        $scope.EntityUserListInObj = [];
        $scope.PersonalUserIdsInObj = [];
        $scope.ObjectiveMembers = {}
        $scope.ObjectiveSummaryDeatils = {};
        $scope.ObjectiveFulfillDeatils = {};
        $scope.FulfillmentEntityTypes = {};
        $scope.FulfillmentAttributes = [];
        $scope.FulfillmentAttributeOptions = [];
        $scope.creatObjectivefullfilmetadataHtml = '';
        $scope.ObjectiveEntityTypeHtml = '';
        $scope.ObjectiveFullfilOptionsHtml = '';
        $scope.AddMulticonditionHtml = '';
        $scope.FulfillmentConditionsList = [];
        $scope.CurrentObjectiveId = parseInt($stateParams.ID, 10);
        $scope.userimg = parseInt($cookies['UserId']);
        var getEntityNewsFeedResultnew = [];


        $scope.attrgrpImagefileName = '';
        $scope.userimgWidth = 0;
        $scope.userimgHeight = 0;
        $scope.userimgX = 0;
        $scope.userimgY = 0;
        //$scope.Feedgrouplistvalues = '-1';
        $scope.Dropdown = [];
        var cancelNewsTimerevent;
        var FeedInitiated = false;
        $scope.StopeUpdateStatusonPageLoad = false;
        $scope.EntityStatusResult = [];
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.items = [];
        $scope.treeSources = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.treelevels = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.MilestonePrevStatus = '';
        $scope.normaltreeSources = {};
        $scope.userimg = parseInt($cookies['UserId']);
        $scope.milestoneediting = '';
        $scope.EntityHistoryid = 0;
        $scope.IsStartDateEmpty = false;
        $scope.EntityID = $stateParams.ID;
        $scope.OverAllEntityStatus = '';
        $scope.OverAllEntityInTime = '';
        $scope.OverAllEntityStatusComment = '';
        $scope.processed = { index: 0 };
        $scope.agroup = [];
        $scope.DropDownTreePricing = {};
        $scope.PercentageVisibleSettings = {};
        $scope.agroupDetailBlock = [];
        $scope.agroupNotDetailBlock = [];
        var getEntityNewsFeedResultnew = [];
        var getEntityNewsFeedResult = "";
        $scope.listglobalrolesdata = [];
        $scope.entityGlobalRole = '';
        $scope.IsGlobalAccessToShow = true;

        $scope.agroupListDetailBlock = [];
        $scope.agroupListNotDetailBlock = [];
        var perioddates = [];
        $scope.IsLock = false;

        var Attributetypename = '';
        var node = [];

        $scope.$on('processnext', function (event, data) {
            var nextGroupToProcess = $scope.agroup.indexOf(($.grep($scope.agroup, function (e) { return e.ID == data }))[0]) + 1;
            if (nextGroupToProcess + 1 <= $scope.agroup.length)
                $scope.$broadcast("process" + $scope.agroup[nextGroupToProcess].ID);
            if (nextGroupToProcess == $scope.agroup.length)
                $scope.BindScrollOnNewsfeed();

        });
        function GetAttributeGroups() {
            $scope.LanguageContent = $scope.LanguageContents;
            $scope.AttrLock = $scope.IsLock;
            if ($scope.agroup.length > 0) {
                $scope.agroup.splice(0, $scope.agroup.length);
                $scope.agroupDetailBlock.splice(0, $scope.agroupDetailBlock.length);
                $scope.agroupDetailBlock.splice(0, $scope.agroupDetailBlock.length);
                $scope.agroupListDetailBlock.splice(0, $scope.agroupListDetailBlock.length);
                $scope.agroupListNotDetailBlock.splice(0, $scope.agroupListNotDetailBlock.length);
            }
            ObjectiveoverviewService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {
                        $scope.agroupDetailBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 1 && e.RepresentationType == false });
                        $scope.agroupNotDetailBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 2 && e.RepresentationType == false });

                        $scope.agroupListDetailBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 1 && e.RepresentationType == true });
                        $scope.agroupListNotDetailBlock = $.grep(attributeGrpRelation.Response, function (e) { return e.LocationType == 2 && e.RepresentationType == true });

                        $scope.agroup = attributeGrpRelation.Response;
                    }
                    else {
                        $scope.BindScrollOnNewsfeed();
                    }

                    if ($scope.agroupDetailBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("process" + ($scope.agroupDetailBlock[0].ID)) }, 200);
                    if ($scope.agroupNotDetailBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("process" + ($scope.agroupNotDetailBlock[0].ID)) }, 200);

                    if ($scope.agroupListDetailBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("process" + ($scope.agroupListDetailBlock[0].ID)) }, 200);
                    if ($scope.agroupListNotDetailBlock.length > 0)
                        $timeout(function () { $scope.$broadcast("process" + ($scope.agroupListNotDetailBlock[0].ID)) }, 200);
                }
                else {
                    $scope.BindScrollOnNewsfeed();
                }

            });
        }

        $scope.$on('updateObjectiveExpireactionEntityEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            $scope.fields["DateTime_" + AttributeID] = Attributevalue;
            LoadObjRelatedData();
            //$scope.LoadEntityRelatedDataSet1();

            $timeout(function () {
                $scope.TimerForLatestFeed();  //to give a time delay  to refresh feed.so that insertion operation happens and then feed load is called.
            }, 1000);
        });

        $scope.openExpireAction = function (event) {

            var attrevent = event.currentTarget;
            $scope.SourceAttributeID = attrevent.dataset.attributeid;
            $scope.SourceAttributename = attrevent.dataset.attrlable;
            $scope.dateExpireval = attrevent.dataset.expiredate;
            var sourcefrom = 4;


            $timeout(function () {
                $scope.$emit('CallExpireaction', $scope.dateExpireval, parseInt($stateParams.ID), parseInt($stateParams.ID), sourcefrom, $scope.EntityTypeID, parseInt($stateParams.ID), $scope.SourceAttributeID, $scope.SourceAttributename);
            }, 100);
        }

        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });

                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                //  var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        for (var i = 0; i < optionarray.length; i++) {
                            if (optionarray[i] != 0) {
                                var level = i + 1;
                                UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
                            }
                        }
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                    }
                });
                $scope.treeSelection = [];
            }

            else if (attributetypeid == 1 || attributetypeid == 2) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                if (attrID == 68) {
                    $(window).trigger("UpdateObjectiveEntityName", optionarray);
                    $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $stateParams.ID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
                                if ($stateParams.ID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $stateParams.ID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
                                    if (Splitarr.length == 0) {
                                        Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
                                }
                            } else {
                                break;
                            }

                        }
                    }
                }
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });

                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                //  var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                });

                //$scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 19) {
                $scope.newamount = "";
                $scope.newcurrencytypename = "";
                $scope.newamount = optionarray[0];
                $scope.newcurrencytypename = optionarray[2];
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ') + ' ' + optionarray[2]);
                    // $scope.fields["SingleLineTextValue_" + attrID] = $scope.newamount.toFixed($scope.DecimalSettings["FormatMoney"]["Financial_FormatMoney"]);
                    $scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
                    $scope.fields["NormalDropDown_" + attrID] = optionarray[1];

                });
                $scope.treeSelection = [];

            }
            else if (attributetypeid == 12) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });

                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });
                    //var updateentityattrib = new UpdateEntityAttributes();
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    //  var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                    ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405)
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                            $scope.fields['SingleLineText_' + attrID] = optionarray == "" ? 0 : optionarray;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        }
                    });
                    $scope.treeSelection = [];
                }
                else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            }
            else if (attributetypeid == 5) {


                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($stateParams.ID, 10), AttributeID: attrID, Level: 0 });
                var sdate = "";
                if ($scope.fields['DateTime_Dir_' + attrID].toString() != "" && $scope.fields['DateTime_Dir_' + attrID] != null && $scope.fields['DateTime_Dir_' + attrID].toString() != "-") {
                    //sdate = $scope.fields['DateTime_Dir_' + attrID].toString('yyyy-MM-dd');
                    sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    //sdate = new Date.create(optionarray.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    if (sdate == 'NaN-NaN-NaN') {
                        sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                    }
                }



                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                //  var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
                        if (sdate == "")
                            $scope.fields["DateTime_" + attrID] = "-";
                        else {
                            $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.DefaultSettings.DateFormat);

                            //$scope.fields["'DateTime_Dir" + attrID] = dateFormat(ConvertDateFromStringToString(ConvertDateToString(sdate)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);

                            //$scope.fields["DateTime_Dir_" + attrID] = formatteddateFormat(sdate, "dd/MM/yyyy");
                        }
                        //$scope.fields["DateTime_" + attrID] = sdate;
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 9) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = ($('#CheckBoxSelection_' + attrID)[0].checked) ? [1] : [0];
                updateentityattrib.AttributetypeID = attributetypeid;
                ObjectiveoverviewService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields['CheckBoxSelection_' + attrID] = $('#CheckBoxSelection_' + attrID)[0].checked;
                    }
                });
            }
            $scope.treeSelection = [];
            $timeout(function () {
                $scope.TimerForLatestFeed();  //to give a time delay  to refresh feed.so that insertion operation happens and then feed load is called.
            }, 5000);

        };

        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {

                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {

                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }

        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                //var InsertNewEntityPeriod = $resource('planning/InsertEntityPeriod/');
                //var newperiod = new InsertNewEntityPeriod();
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), $scope.format);
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), $scope.format);

                //----------> Start date and end date diff checking
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }

                var tempperioddates = [];

                for (var i = 0 ; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }

                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 1) {

                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }

                newperiod.EntityID = entityid;
                newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;

                //  var resultperiod = InsertNewEntityPeriod.save(newperiod, function () {
                PlanningService.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        var newid = resultperiod.Response;

                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }

                    perioddates.push({ ID: newid, value: edate });

                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate))

                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];


                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    }
                    else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("visibility", "hidden");
                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';

                    $scope.dyn_Cont += '<div class="inputHolder span11">';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5056.Caption') + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5057.Caption') + '</label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';

                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }

                    var divcompile = $compile($scope.dyn_Cont)($scope);
                    $("#detailsDiv div[data-addperiodid]").append(divcompile);


                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    //var EntityPeriodGantt = $resource('planning/GetEntitiPeriodByIdForGantt/:EntityID', { EntityID: entityid })
                    //var getGanttperiod = EntityPeriodGantt.get(function () {
                    PlanningService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                        if (getGanttperiod.StatusCode == 200) {

                            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                    if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                        return false;
                                    }
                                    else {
                                        if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                            $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod.Response;
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    });
                });
            }
            else {
                //var UpdateperiodByID = $resource('planning/EntityPeriod/');
                //var updateperiod = new UpdateperiodByID();
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {

                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[" + $translate.instant('LanguageContents.Res_5065.Caption') + "]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });

                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });


                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), $scope.format);
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), $scope.format))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), $scope.format);
                if (edate == 'NaN-NaN-NaN') {
                    edate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), $scope.format))
                }


                //----------> Start date and end date diff checking
                var diffval = ((parseInt((edate).getTime()) - parseInt(((sdate).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }

                $scope.fields["PeriodStartDate_Dir_" + periodid] = dateFormat(new Date.create(sdate.toString()), $scope.format);
                $scope.fields["PeriodEndDate_Dir_" + periodid] = dateFormat(new Date.create(edate.toString()), $scope.format);
                var maxPeriodEndDate = edate;
                var Convertsdate = sdate;
                var diffvalend = ((maxPeriodEndDate.getTime() - Convertsdate.getTime()));
                if (parseInt(diffvalend) < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }
                var diffvalstart = (parseInt(Convertsdate.getTime() - Convertsdate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));

                    return false;
                }


                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = dateFormat(sdate, $scope.format);
                updateperiod.EndDate = dateFormat(edate, $scope.format);
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                PlanningService.PostEntityPeriod1(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        $scope.fields["PeriodStartDate_" + periodid] = dateFormat(sdate, $scope.format);
                        $scope.fields["PeriodEndDate_" + periodid] = dateFormat(edate, $scope.format);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));

                        PlanningService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                            if (resultperiod.StatusCode == 200) {
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                            return false;
                                        } else {
                                            if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod.Response;
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
            $scope.TimerForLatestFeed();
        }

        $scope.deletePeriodDate = function (periodid) {
            //var DeletePeriodByID = $resource('planning/DeleteEntityPeriod/:ID', { ID: periodid }, { delete: { method: 'DELETE' } });
            //var deletePerById = DeletePeriodByID.delete({ ID: periodid }, function () {
            PlanningService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {

                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));

                    UpdateEntityPeriodTree($stateParams.ID);
                    $("#detailsDiv div[data-dynPeriodID = " + periodid + " ]").html('');

                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });

                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }

        function UpdateEntityPeriodTree(entityid) {

            PlanningService.GetEntitiPeriodByIdForGantt(entityid).then(function (resultperiod) {
                if (resultperiod.StatusCode == 200) {

                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                return false;
                            }
                            else {
                                if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = resultperiod.Response;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
        }

        $scope.ShowOrHideAttributeToAttributeRelationInOverview = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'NormalDropDown_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'NormalMultiDropDown_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'dropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'multiselectdropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }

                            if (relationobj[i].AttributeTypeID == 3) {
                                if ($scope.fields[Attributetypename] != "" && $scope.fields[Attributetypename] != "-" && ($scope[Attributetypename] == undefined || $scope[Attributetypename] == "")) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        var tempvar = ($.grep($scope.normaltreeSources[Attributetypename], function (e) { return e.Caption == $scope.fields[Attributetypename] }))[0];
                                        $scope[Attributetypename] = tempvar.Id;
                                    }
                                }

                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if (relationobj[i].AttributeOptionID == $scope[Attributetypename]) {
                                        return true;
                                    }
                            }
                            else if (relationobj[i].AttributeTypeID == 4) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        $scope['IsChanged_' + relationobj[i].AttributeID] = false;
                                        var captions = $scope.fields[Attributetypename].split(',')
                                        $scope[Attributetypename] = [];
                                        var tempvar = [];
                                        for (var x = 0; x <= captions.length; x++) {
                                            if (captions[x] != undefined) {
                                                tempvar.push(($.grep($scope.normaltreeSources[Attributetypename], function (e) { return e.Caption.trim() == captions[x].trim() }))[0]);
                                                $scope[Attributetypename].push(tempvar[x].Id);
                                            }
                                        }
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if ($.inArray(relationobj[i].AttributeOptionID, $scope[Attributetypename]) != -1) {
                                        return true;
                                    }
                            }
                            else if (relationobj[i].AttributeTypeID == 6) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == true || $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == undefined) {
                                    if ($scope.treeSources["dropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["dropdown_" + relationobj[i].AttributeID] != "") {

                                        if (relationobj[i].AttributeLevel == 1) {
                                            $scope[Attributetypename] = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim()
                                            }))[0];
                                            $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                        }
                                        else {
                                            //node.length = 0;
                                            var node = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel - 1)].toString().trim()
                                            }))[0];

                                            if (node != undefined) {
                                                $scope[Attributetypename] = ($.grep(node.Children, function (e) {
                                                    return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel)].toString().trim()
                                                }))[0];
                                                $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                            }
                                        }
                                    }

                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if ($scope[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                        return true;
                                    }

                            }
                            else if (relationobj[i].AttributeTypeID == 12) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != "") {
                                        $scope[Attributetypename] = ($.grep($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID].Children, function (e) { return e.Caption.toString() == $scope.treeTexts['multiselectdropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim() }))[0];
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if ($scope[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                        return true;
                                    }
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                        return true;
                                    }
                            }
                            else {
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "")
                                    if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                        return true;
                                    }
                            }
                        }
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        };

        EnableDisableLoader();

        function EnableDisableLoader() {
            $scope.detailsLoader = true;
            $scope.detailsData = false;
        }
        $scope.ObjresultHolder = [];

        function LoadObjRelatedData() {
            ObjectiveoverviewService.GetObjectiveRelatedDataOnLoad(parseInt($stateParams.ID, 10)).then(function (ObjRelatedDataResult) {
                if (ObjRelatedDataResult.StatusCode != 405) {
                    if (ObjRelatedDataResult.Response != null) {
                        $scope.ObjresultHolder = ObjRelatedDataResult.Response;
                        LoadTaskSummaryDetlTemp($scope.ObjresultHolder[0]);
                        LoadObjRelatedDataSet2();
                    }
                }
            });
        }

        LoadEntityRelatedData();

        function LoadEntityRelatedData() {
            $scope.EntityStatusResult = [];
            $scope.DynamicTaskListDescriptionObj = "-";
            PlanningService.GetEntityRelatedDataOnLoad(parseInt($stateParams.ID), parseInt($cookies['UserId']), 0, false, SystemDefinedEntityTypes.Milestone).then(function (EntityRelatedDataResult) {
                if (EntityRelatedDataResult.StatusCode != 405) {
                    if (EntityRelatedDataResult.Response != null) {
                        $scope.tempresultHolder = EntityRelatedDataResult.Response;
                        //$scope.EntityHistoryid = $scope.tempresultHolder[0];
                        $scope.funloadEntityStatus_Edit($scope.tempresultHolder[1], $scope.tempresultHolder[2]);
                        //LoadTaskSummaryDetl_Temp($scope.tempresultHolder[3]);
                        //$scope.loadEntityRelatedData_Set2();
                        //GetAttributeGroups();
                        //$timeout(function () { GetAttributeGroups() }, 5000);

                    }
                }
            });
            //$scope.GetGlobalAccess();
        }

        $scope.funloadEntityStatus_Edit = function (EntityStatusByID, EntityStatus) {
            $scope.OverAllEntityStatusVis = true;
            var EntityStatusByIDResult = EntityStatusByID;
            if (EntityStatus != null) {
                $scope.EntityStatusResult = EntityStatus;
                $timeout(function () {
                    if (EntityStatusByIDResult != null) {
                        $scope.OverAllEntityStatus = EntityStatusByIDResult.Status;
                        $scope.OverAllEntityInTime = EntityStatusByIDResult.TimeStatus;
                        $scope.DynamicTaskListDescriptionObj = (EntityStatusByIDResult.Comment != null ? EntityStatusByIDResult.Comment : '-');
                        $scope.Ontime = OnSelectionChange(EntityStatusByIDResult.TimeStatus);
                    }
                }, 100);
                $timeout(function () {
                    $scope.OverAllEntityStatusVis = false;
                }, 100);
            }
        }

        function OnSelectionChange(Selectionid) {
            if ($scope.OntimeStatusLists != undefined) {
                var Newvalue = $.grep($scope.OntimeStatusLists, function (n, i) {
                    return ($scope.OntimeStatusLists[i].Id == Selectionid);
                });
                $scope.DynamicClass = Newvalue[0].SelectedClass;
                return Newvalue[0].Name;
            }
            else
                return "";
        }

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }

            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };

        $scope.select2Config = {
            formatResult: $scope.overallstatusformat,
            formatSelection: $scope.overallstatusformat
        };

        $scope.UpdateEntityOverAllStatus = function () {
            var upd = {};
            upd.EntityID = parseInt($stateParams.ID);
            upd.EntityStatusID = $scope.OverAllEntityStatus;
            upd.OnTimeStatus = $scope.OverAllEntityInTime;
            upd.OnTimeComment = $scope.DynamicTaskListDescriptionObj;
            PlanningService.UpdateEntityStatus(upd).then(function (Result) {
                if (Result.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] != undefined) {
                                    $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityOnTimeStatus] = $("#OntimeStatus").text().trim();
                                }
                                else {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["EntityOnTimeStatus"] = $("#OntimeStatus").text().trim();
                                }
                                if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] != undefined) {
                                    var res = $.grep($scope.EntityStatusResult, function (e) { return e.ID == $scope.OverAllEntityStatus });
                                    if (res.length > 0) {
                                        $scope.ListViewDetails[i].data.Response.Data[j].StatusColorCode = res[0].ColorCode;
                                        $scope.ListViewDetails[i].data.Response.Data[j].Status = $('#OverAllEntityStats option:selected').text();
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.EntityStatus] = $('#OverAllEntityStats option:selected').text();//$scope.CurrentWorkflowStepName;
                                }
                                else
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $('#OverAllEntityStats option:selected').text();
                                $timeout(function () {
                                    $scope.TimerForLatestFeed();  //to give a time delay  to refresh feed.so that insertion operation happens and then feed load is called.
                                }, 2000);
                                return false;
                            }
                        }
                    }

                }

            });
            //}

        }

        $scope.ObjresultHolderSet2 = [];
        function LoadObjRelatedDataSet2() {
            ObjectiveoverviewService.GetObjectiveRelatedDataOnLoadSet2(parseInt($stateParams.ID, 10)).then(function (ObjRelatedDataSet2Result) {
                if (ObjRelatedDataSet2Result.StatusCode != 405) {
                    if (ObjRelatedDataSet2Result.Response != null) {
                        $scope.ObjresultHolderSet2 = ObjRelatedDataSet2Result.Response;
                        LoadDetailTemp($scope.ObjresultHolderSet2[0], $scope.ObjresultHolderSet2[1], $scope.ObjresultHolderSet2[2], $scope.ObjresultHolderSet2[3], $scope.ObjresultHolderSet2[4], parseInt($stateParams.ID, 10));
                        $scope.LoadSectionDetail_Temp($scope.ObjresultHolderSet2[5], $scope.ObjresultHolderSet2[6]);
                        GetAttributeGroups();
                    }
                }
            });
        }
        LoadObjRelatedData();

        GlistFeedgroup();
        function GlistFeedgroup() {
            ObjectiveoverviewService.GetFeedFilter().then(function (GetFeedglistvalue) {
                $scope.FeedgResult = GetFeedglistvalue.Response;
                $scope.Feedglist = [];
                if ($scope.FeedgResult.length > 0) {
                    for (var i = 0; i < $scope.FeedgResult.length; i++) {
                        $scope.Feedglist.push({
                            ID: $scope.FeedgResult[i].Id,
                            Name: $scope.FeedgResult[i].FeedGroup
                        });
                    }
                }
            });
        };

        $scope.FeedgroupFilterByOptionChange = function () {
            if ($scope.Newsfilter.FeedgroupMulitipleFilterStatus.length == 0 || $scope.Newsfilter.FeedgroupMulitipleFilterStatus == undefined) {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = [];
                $scope.Newsfilter.Feedgrouplistvalues = '-1';
                $scope.LoadNewsFeedBlockForObjective();
            } else {
                $scope.Newsfilter.FeedgroupMulitipleFilterStatus = $scope.Newsfilter.FeedgroupMulitipleFilterStatus;
                $scope.Newsfilter.Feedgrouplistvalues = '';
                $scope.Newsfilter.Feedgrouplistvalues = $scope.Newsfilter.FeedgroupMulitipleFilterStatus.join(',');
                $scope.LoadNewsFeedBlockForObjective();
            }
        }

        $scope.GetEntityMembers = function () {
            $scope.PersonalUserIdsInObj = [];
            $scope.EntityUserListInObj = [];
            ObjectiveoverviewService.GetMember($stateParams.ID).then(function (member) {
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserListInObj.length > 0) {
                                $.each($scope.EntityUserListInObj, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserListInObj.push(value);
                            } else $scope.EntityUserListInObj.push(value);
                        }
                    });
                }
            });
        }
        $scope.GetEntityMembers();

        $scope.GetNewsFeedOnPageScroll = function () {
            PagenoforScroll += 2;
            var ID = $stateParams.ID;
            //---------------------------> LOADING NEWS FEEDS STARTS HERE <-------------------------------
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            //var enityNewsFeed = $resource('common/GetEnityFeeds/:EntityID/:PageNo/:Feedsgroupid/');
            //var getEntityNewsFeedResultForScroll = enityNewsFeed.get({ EntityID: ID, PageNo: PagenoforScroll, Feedsgroupid: $scope.Newsfilter.Feedgrouplistvalues }, function () {
            CommonService.GetEnityFeeds(ID, PagenoforScroll, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResultForScroll) {
                getEntityNewsFeedResultnew.push(getEntityNewsFeedResultForScroll);
                var feeddivHtml = '';
                for (var i = 0 ; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                    feeddivHtml = '';
                    var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                    if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                        $scope.userimgsrc = $scope.NewUserImgSrc;
                    }
                    else {
                        $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                    }

                    feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                    feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >Comment</a></span>';
                    if (feedcomCount > 1) {
                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                    }
                    else {
                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></a></span></div></div></div>';
                    }
                    feeddivHtml = feeddivHtml + '<ul class="subComment">';
                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                        var j = 0;

                        if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                        }
                        else {
                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                        }

                        for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;

                            if (k == 0) {
                                feeddivHtml = feeddivHtml + '<li';
                            }
                            else {
                                feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                            }
                            feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                            feeddivHtml = feeddivHtml + '</div></div></li>';
                        }
                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></li>';
                    }

                    feeddivHtml = feeddivHtml + '</ul></li>';
                    $('#EntityFeedsdiv').append(feeddivHtml);
                    feeddivHtml = '';
                }

            });
        }

        $scope.AddNewsFeed = function () {
            if ($("#feedtextholder").text() != "") {
                return false;
            }
            var addnewsfeed = {};
            addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
            addnewsfeed.TemplateID = 2;
            addnewsfeed.EntityID = parseInt($stateParams.ID);
            addnewsfeed.TypeName = "";
            addnewsfeed.AttributeName = "";
            addnewsfeed.FromValue = "";
            addnewsfeed.ToValue = $('#feedcomment').text();
            addnewsfeed.PersonalUserIds = $scope.PersonalUserIdsInObj;
            ObjectiveoverviewService.PostFeed(addnewsfeed).then(function (savenewsfeed) { });
            $('#feedcomment').empty();
            if (document.getElementById('feedcomment').innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                document.getElementById('feedcomment').innerHTML = '<span id=\'feedtextholder\' class=\'placeholder\'>' + $translate.instant('LanguageContents.Res_1202.Caption') + '...</span>';
            }
            $scope.PersonalUserIdsInObj = [];
            $scope.TimerForLatestFeed();
        };

        var PagenoforScroll = 0;
        $scope.LoadNewsFeedBlockForObjective = function () {

            var ID = $stateParams.ID;
            //---------------------------> LOADING NEWS FEEDS STARTS HERE <-------------------------------
            $scope.userimgsrc = '';
            $scope.commentuserimgsrc = '';
            // var enityNewsFeed = $resource('common/GetEnityFeeds/:EntityID/:PageNo/:Feedsgroupid/');
            //--------------------------------------------------------------------------------------------------

            //---------------------------  Get the initial feed ------------------------------------
            try {
                $('#EntityFeedsdiv').html('');
                // var getEntityNewsFeedResult = enityNewsFeed.get({ EntityID: ID, PageNo: 0, Feedsgroupid: $scope.Newsfilter.Feedgrouplistvalues }, function () {
                CommonService.GetEnityFeeds(ID, 0, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                    var feeddivHtml = '';

                    $('#EntityFeedsdiv').html('');
                    if (getEntityNewsFeedResult.Response != null) {
                        getEntityNewsFeedResultnew.push(getEntityNewsFeedResult);
                        for (var i = 0 ; i < getEntityNewsFeedResult.Response.length; i++) {
                            feeddivHtml = '';
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            }
                            else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                            }
                            else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }

                            feeddivHtml = feeddivHtml + '<ul class="subComment">';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;

                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                }
                                else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }

                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    }
                                    else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }

                            feeddivHtml = feeddivHtml + '</ul></li>';
                            feedcomCount = 0;
                            $('#EntityFeedsdiv').append(feeddivHtml);
                            feeddivHtml = '';
                        }
                        feeddivHtml = feeddivHtml + '</ul></li>';
                        $('#EntityFeedsdiv').append(feeddivHtml);
                    }
                    $('#EntityFeedsdiv').scrollTop(1);
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);

                });
            }
            catch (e)
            { }

            $scope.TimerForLastestFeedCallBack = function () {
                if (FeedInitiated != true) {
                    $scope.TimerForLatestFeed();
                }
                if (NewsFeedUniqueTimer != undefined) {
                    NewsFeedUniqueTimer = $timeout(function () {
                        $scope.TimerForLastestFeedCallBack();
                    }, 30000);
                }
            }

            //---------------------------  Getting the latest feed ------------------------------------
            try {

                $scope.TimerForLatestFeed = function () {
                    FeedInitiated = true;
                    //var enityNewsFeed = $resource('common/GetLastEntityFeeds/:EntityID/:Feedsgroupid/');
                    var feeddivHtml = '';
                    //var getEntityNewsFeedResult = enityNewsFeed.get({ EntityID: ID, Feedsgroupid: $scope.Newsfilter.Feedgrouplistvalues }, function () {
                    CommonService.GetLastEntityFeeds(ID, $scope.Newsfilter.Feedgrouplistvalues).then(function (getEntityNewsFeedResult) {
                        getEntityNewsFeedResultnew.push(getEntityNewsFeedResult);
                        var feeddivHtml = '';
                        if (getEntityNewsFeedResult.Response != null) {
                            for (var i = 0 ; i < getEntityNewsFeedResult.Response.length; i++) {
                                feeddivHtml = '';
                                var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                                if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                    $scope.userimgsrc = $scope.NewUserImgSrc;
                                }
                                else {
                                    $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                                feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                                feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_5051.Caption') + '</a></span>';
                                if (feedcomCount > 1) {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" >' + $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedcomCount - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption') + '</a></span></div></div></div>';
                                }
                                else {
                                    feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                feeddivHtml = feeddivHtml + '<ul class="subComment"';

                                if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {

                                    var j = 0;

                                    if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    }
                                    else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }

                                    for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            feeddivHtml = feeddivHtml + '<li';
                                        }
                                        else {
                                            feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                        }
                                        feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                        feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        feeddivHtml = feeddivHtml + '</div></div></li>';
                                    }
                                }
                                feeddivHtml = feeddivHtml + '</ul></li>';
                                $('#EntityFeedsdiv [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getEntityNewsFeedResult.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });
                                $('#EntityFeedsdiv').prepend(feeddivHtml);
                                feedcomCount = 0;

                            }
                            FeedInitiated = false;
                        }
                    });
                    $('#EntityFeedsdiv').prepend(feeddivHtml);
                }
            }
            catch (e)
            { }

            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = [];
                for (var j = 0 ; j < getEntityNewsFeedResultnew.length ; j++) {
                    for (var k = 0 ; k < getEntityNewsFeedResultnew[j].Response.length ; k++) {
                        if (getEntityNewsFeedResultnew[j].Response[k].FeedId == parseInt(feedid)) {
                            feedfilter = $.grep(getEntityNewsFeedResultnew[j].Response, function (e) { return e.FeedId == parseInt(feedid); });
                        }
                    }
                }

                $("#Overviewfeed_" + feedid).next('div').hide();
                if (feedfilter != '') {
                    for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                        if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                            $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4896.Caption') + ' ' + (feedfilter[0].FeedComment.length - 1) + ' ' + $translate.instant('LanguageContents.Res_5054.Caption');
                            if (i != 0)
                                $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                            else
                                $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        }
                        else {
                            $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                            $(this)[0].innerHTML = $translate.instant('LanguageContents.Res_4960.Caption');
                        }
                    }
                }
            });

            $('#EntityFeedsdiv').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var feeddivHtml = '';
                feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
                feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>'; //should always be current userid
                feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="ButtonHTML" >' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0)
                    $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
                else
                    $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
                $timeout(function () { $('#feedcomment_' + commentuniqueid).html('').focus(); }, 10);
            });

            $('#EntityFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-entityid');
                var typeid = $(this).attr('data-typeid');
                var parentid = $(this).attr('data-parentid');
                // var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: $(this).attr('data-entityid') });
                var active = $(this).attr('data-fromvalue');
                var deactive = $(this).attr('data-tovalue');
                var EntityTypeName = $(this).attr('data-entitytypename');

                //var EntityTypeName = $(this).attr('data-Entitytypename)';


                //  var result = IsValidEntity.get(function () {
                CommonService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                    if (result.Response == true) {
                        //this is for predefined OBjective.
                        ///mui/planningtool/objective/detail/section/2294 
                        ///this name has been coming from commonmanager  with entitytype name  and entitytypeid   PredefinedObjective=10
                        if (typeid == 10) {
                            $("#EntitiesTree li.active").removeClass('active');
                            $("#EntitiesTree  li a[data-entityid=" + parentid + "]").parent('li').addClass('active');
                            $("#SectionTabs li.active").removeClass('active');
                            $("#SectionTabs li#Objective").addClass("active");
                            $location.path('/mui/planningtool/objective/detail/section/' + entityid);
                            $timeout(function () { $scope.loadBreadCrum_Edit(false) }, 100);
                        }
                            //Here checking that condition based on the status whether Actived or Deactived for the additionalobjective.
                        else if (typeid == 11) {
                            $("#EntitiesTree li.active").removeClass('active');
                            $("#EntitiesTree  li a[data-entityid=" + parentid + "]").parent('li').addClass('active');
                            $("#SectionTabs li.active").removeClass('active');
                            $("#SectionTabs li#Objective").addClass("active");
                            $location.path('/mui/planningtool/default/detail/section/' + parentid + '/' + 'objective' + '');
                            $timeout(function () { $scope.loadBreadCrum_Edit(false) }, 100);
                        }
                        else
                            $location.path('/mui/planningtool/default/detail/section/' + entityid + '/overview');
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });

            });
            $('#EntityFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
                var TargetControl = $(this);
                var mypage = TargetControl.attr('data-Name');
                var myname = TargetControl.attr('data-Name');
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {

                var entityid = $(this).attr('data-entityid');
                var taskid = $(this).attr('data-taskid');
                var typeid = $(this).attr('data-typeid');

                //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: taskid });

                //var result = IsValidEntity.get(function () {
                CommonService.IsActiveEntity(taskid).then(function (result) {
                    if (result.Response == true) {

                        $scope.$broadcast("pingTaskEdit", { TaskId: taskid, EntityId: entityid });

                        // $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, entityid, $stateParams.ID]);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });

            $(window).on("ontaskActionnotification", function (event) {
                $("#loadNotificationtask").modal("hide");

            });

            $('#EntityFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
                /* Respond to click here */

                var EntityID = $(this).attr('data-parentid');
                var funderqid = $(this).attr('data-entityid');
                //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: funderqid });

                //var result = IsValidEntity.get(function () {
                CommonService.IsActiveEntity(funderqid).then(function (result) {
                    if (result.Response == true) {
                        $("#feedFundingRequestModal").modal("show");
                        $('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [funderqid]);
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });

            $('#EntityFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-costcentreid');
                //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: $(this).attr('data-costcentreid') });
                //var result = IsValidEntity.get(function () {
                CommonService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                    if (result.Response == true) {
                        $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }

                });
            });
            $('#EntityFeedsdiv').on('click', 'a[data-id="objectivelink"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-objectiveid');
                //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: $(this).attr('data-objectiveid') });

                //var result = IsValidEntity.get(function () {
                CommonService.IsActiveEntity($(this).attr('data-objectiveid')).then(function (result) {
                    if (result.Response == true) {
                        $location.path('/mui/planningtool/objective/detail/section/' + entityid + '/overview');
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });
            });
            $('#EntityFeedsdiv').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    //var AddFeedComment = $resource('common/InsertFeedComment/');
                    //var addfeedcomment = new AddFeedComment();
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' +
                        (month < 10 ? '0' : '') + month + '-' +
                        (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);

                    // var saveusercomment = AddFeedComment.save(addfeedcomment, function () {
                    CommonService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var feeddivHtml = '';
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                        feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmnt">';
                        feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + $translate.instant('LanguageContents.Res_5055.Caption') + '</span></div>';
                        feeddivHtml = feeddivHtml + '</div></div></div></li>';
                        $("#EntityFeedsdiv").find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#OverviewComment_' + addfeedcomment.FeedID).show();
                    });
                }
            });

            $('#EntityFeedsdiv').on('click', 'a[data-id="Assetpath"]', function (event) {
                event.stopImmediatePropagation();
                var entityid = $(this).attr('data-entityid');
                var typeid = $(this).attr('data-typeid');
                var parentid = $(this).attr('data-parentid');
                var Assetid = $(this).attr('data-Assetid');
                //var IsValidEntity = $resource("common/IsAvailableAsset/:AssetID", { AssetID: $(this).attr('data-Assetid') });
                //var result = IsValidEntity.get(function () {
                CommonService.IsAvailableAsset($(this).attr('data-Assetid')).then(function (result) {
                    if (result.Response == true) {

                        $timeout(function () {

                            $scope.$emit('callBackAssetEditinMUI', Assetid, $scope.IsLock, true, 'ViewType');
                        }, 100);

                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });

            });
            //--------------------------->  NEWS FEEDS ENDS HERE <-------------------------------

        };

        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};

        $scope.BindScrollOnNewsfeed = function () {
            if ($('#EntityFeedsdiv').height() > 1260) {
                $scope.GetNewsFeedOnPageScroll();
            }

            $timeout(function () {
                $('#EntityFeedsdiv').unbind('scroll');
                $('#EntityFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        $scope.GetNewsFeedOnPageScroll();
                    }
                });
            }, 500)
        }

        //-----------> CALLING NEWS FEED ON PAGE LOAD <---------
        $scope.LoadNewsFeedBlockForObjective();

        function LoadDetailTemp(ObjSumBlockDet, ObjFulfillDet, ObjUnits, FulfillEntTypes, EntOwnerList, ID) {
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            if (NewsFeedUniqueTimer != undefined) $timeout.cancel(NewsFeedUniqueTimer);
            $scope.ObjectiveStatus = '';
            var GetObjectiveSummaryBlockObj = ObjSumBlockDet;
            var objeciveSummaryData = GetObjectiveSummaryBlockObj;
            LoadObjectiveSummaryBlock(objeciveSummaryData);

            function getMoney(A) {
                var a = new Number(A);
                var b = a.toFixed(2);
                a = parseInt(a);
                b = (b - a).toPrecision(2);
                b = parseFloat(b).toFixed(2);
                a = a.toLocaleString();
                a = a.replace(",", "");
                a = Number(a);
                a = a.formatMoney(0, ' ', ' ');
                if (a.lastIndexOf('.00') > -1) {
                    if (a.lastIndexOf('.00') == (a.length - 3)) {
                        a = a.substring(0, a.length - 3);
                    }
                }
                if (b.substring(0, 1) == '-') {
                    b = b.substring(2);
                } else b = b.substring(1);
                return a + b;
            }

            function LoadObjectiveSummaryBlock(objeciveSummaryData) {
                $scope.ObjectiveSummaryDeatils = objeciveSummaryData;
                var objectiveSummaryHtml = '';
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 1) {
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_709.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += ($scope.ObjectiveSummaryDeatils.GlobalBaseLine).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                    objectiveSummaryHtml += ' ' + $scope.ObjectiveSummaryDeatils.ObjectiveUnitCaption;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_710.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += ($scope.ObjectiveSummaryDeatils.GlobalTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                    objectiveSummaryHtml += ' ' + $scope.ObjectiveSummaryDeatils.ObjectiveUnitCaption;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_715.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += parseInt($scope.ObjectiveSummaryDeatils.PlannedAmount);
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_716.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += parseInt($scope.ObjectiveSummaryDeatils.TargetOutcome);
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5060.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == true) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5061.Caption');
                    }
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == false) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5062.Caption');
                    }
                    objectiveSummaryHtml += '</label></div></div>';
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 2) {
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4870.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.ObjectiveUnitCaption != null ? $scope.ObjectiveSummaryDeatils.ObjectiveUnitCaption : '-';
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_715.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += getMoney($scope.ObjectiveSummaryDeatils.PlannedAmount);
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_716.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += getMoney($scope.ObjectiveSummaryDeatils.TargetOutcome);
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5060.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == true) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5061.Caption');
                    }
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == false) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5062.Caption');
                    }
                    objectiveSummaryHtml += '</label></div></div>';
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 3) {
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4416.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.Fulfilled;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4525.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.NotFulfilled;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5063.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.LevelsInvolved;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5060.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == true) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5061.Caption');
                    }
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == false) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5062.Caption');
                    }
                    objectiveSummaryHtml += '</label></div></div>';
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 4) {
                    $scope.ratingItems = [];
                    for (var i = 0; i < $scope.ObjectiveSummaryDeatils.ObjectiveRating.length; i++) {
                        $scope.ratingItems.push({
                            RatingCaption: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatingCaption,
                            ObjectiveRatingID: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].ObjectiveRatingID,
                            IsRemoved: false,
                            RatingCount: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount == 0 ? false : $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount > 0 ? true : $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount
                        });

                        objectiveSummaryHtml += '<div class="control-group ng-scope">';
                        objectiveSummaryHtml += '<label for="label" class="control-label">';
                        objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatingCaption;
                        objectiveSummaryHtml += '</label>';
                        objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                        objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount;
                        objectiveSummaryHtml += '</label></div></div>';
                    }
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5063.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    objectiveSummaryHtml += $scope.ObjectiveSummaryDeatils.LevelsInvolved;
                    objectiveSummaryHtml += '</label></div></div>';
                    objectiveSummaryHtml += '<div class="control-group ng-scope">';
                    objectiveSummaryHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_5060.Caption') + '</label>';
                    objectiveSummaryHtml += '<div class="controls"><label for="label" class="control-label">';
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == true) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5061.Caption');
                    }
                    if ($scope.ObjectiveSummaryDeatils.EnableCommnets == false) {
                        objectiveSummaryHtml += $translate.instant('LanguageContents.Res_5062.Caption');
                    }
                    objectiveSummaryHtml += '</label></div></div>';
                }
                $('#objectiveAssignmentssummaryDiv').html(objectiveSummaryHtml);
            }
            $scope.objectiveRoleId = 0;
            $scope.ObjectiveOwnerName = '';
            $scope.ObjectiveOwnerId = 0;
            $scope.CurrentObjectiveOwnerId = 0;
            var GetObjectiveFulfillmentBlockObj = ObjFulfillDet;
            var ObjectiveFulfillDetails = GetObjectiveFulfillmentBlockObj;
            LoadFulfillMentBlock(ObjectiveFulfillDetails);

            function LoadFulfillMentBlock(ObjectiveFulfillDetails) {
                $scope.ObjectiveFulfillDeatils = ObjectiveFulfillDetails;
                $scope.objFulfillHtml = '';
                $scope.objDetailHtml = '';
                $scope.ObjectiveOwnerName = $scope.ObjectiveFulfillDeatils.OwnerName;
                $scope.CurrentObjectiveOwnerId = $scope.ObjectiveFulfillDeatils.OwnerId;
                $scope.objFulfillHtml += '<div class="control-group ng-scope">';
                $scope.objFulfillHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4765.Caption') + '</label>';
                $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
                $scope.objFulfillHtml += dateFormat($scope.ObjectiveFulfillDeatils.StartDate, $scope.DefaultSettings.DateFormat);
                $scope.objFulfillHtml += '</label></div></div>';
                $scope.objFulfillHtml += '<div class="control-group ng-scope">';
                $scope.objFulfillHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4237.Caption') + '</label>';
                $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
                $scope.objFulfillHtml += dateFormat($scope.ObjectiveFulfillDeatils.EndDate, $scope.DefaultSettings.DateFormat);
                $scope.objFulfillHtml += '</label></div></div>';
                $scope.objFulfillHtml += '<div class="control-group ng-scope">';
                $scope.objFulfillHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_815.Caption') + '</label>';
                $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
                $scope.objFulfillHtml += $scope.ObjectiveFulfillDeatils.DateRule;
                $scope.objFulfillHtml += '</label></div></div>';
                $scope.objFulfillHtml += '<div class="control-group ng-scope">';
                $scope.objFulfillHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_806.Caption') + '</label>';
                $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
                $scope.objFulfillHtml += $scope.ObjectiveFulfillDeatils.Mandatory;
                $scope.objFulfillHtml += '</label></div></div>';
                $scope.objFulfillHtml += '<div class="control-group ng-scope">';
                $scope.objFulfillHtml += '<label for="label" class="control-label">' + $translate.instant('LanguageContents.Res_4956.Caption') + '</label>';
                $scope.objFulfillHtml += '<div class="controls"><label for="label" class="control-label">';
                if ($scope.ObjectiveFulfillDeatils.FulfillCondition != '' && $scope.ObjectiveFulfillDeatils.FulfillCondition != undefined) $scope.objFulfillHtml += $scope.ObjectiveFulfillDeatils.FulfillCondition.replace(/\n|\r/g, "");
                else $scope.objFulfillHtml += $scope.ObjectiveFulfillDeatils.FulfillCondition;
                $scope.objFulfillHtml += '</label></div></div>';
                $('#fulfillmentDiv').html($compile($scope.objFulfillHtml)($scope));
            }
            $scope.ObjSummaryGlobalBaseLine = 0;
            $scope.ObjSummaryGlobalTarget = 0;
            $scope.Objective = {
                ngObjectiveNumericUnits: 0,
                ngObjectiveNumericGlobalbaseline: 0,
                ngObjectiveNumericGlobaltarget: 0,
                ngObjectiveNumericEnableCommentsCheck: false,
                ngObjectiveNumericDescriptionInstructions: '',
                ngObjectivenonNumericUnits: 0,
                ngObjectivenonNumericEnableCommentsCheck: false,
                ngObjectivenonNumericDescriptionInstructions: '',
                ngObjectiveQulaitativeDescriptionInstructions: '',
                ngObjectiveQulaitativeEnableCommentsCheck: false,
                ngObjectiveRatingDescriptionInstructions: '',
                ngObjectiveRatingEnableCommentsCheck: false,
            }
            $scope.ObjectiveSummaryEdit = function () {
                $('#EditObjectiveSummary').modal('show');
                var obej = $scope.ObjectiveSummaryDeatils.ObjectiveTypeId;
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 1) {
                    $scope.nonNumericQuantivedisplay = false;
                    $scope.qualitativedisplay = false;
                    $scope.ratingsdisplay = false;
                    $scope.numericqunatitativedisplay = true;
                    handleTextChangeEvents();
                    $scope.ObjSummaryGlobalBaseLine = ($scope.ObjectiveSummaryDeatils.GlobalBaseLine).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                    $scope.ObjSummaryGlobalTarget = ($scope.ObjectiveSummaryDeatils.GlobalTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                    $scope.Objective.ngObjectiveNumericUnits = $scope.ObjectiveSummaryDeatils.ObjectiveUnitId;
                    $scope.Objective.ngObjectiveNumericGlobalbaseline = $scope.ObjSummaryGlobalBaseLine;
                    $scope.Objective.ngObjectiveNumericGlobaltarget = $scope.ObjSummaryGlobalTarget;
                    $scope.Objective.ngObjectiveNumericEnableCommentsCheck = $scope.ObjectiveSummaryDeatils.EnableCommnets;
                    $scope.Objective.ngObjectiveNumericDescriptionInstructions = $('<div />').html($scope.ObjectiveSummaryDeatils.ObjectiveDescription).text();
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 2) {
                    $scope.numericqunatitativedisplay = false;
                    $scope.nonNumericQuantivedisplay = true;
                    $scope.qualitativedisplay = false;
                    $scope.ratingsdisplay = false;
                    $scope.Objective.ngObjectivenonNumericUnits = $scope.ObjectiveSummaryDeatils.ObjectiveUnitId;
                    $scope.Objective.ngObjectivenonNumericEnableCommentsCheck = $scope.ObjectiveSummaryDeatils.EnableCommnets;
                    $scope.Objective.ngObjectivenonNumericDescriptionInstructions = $('<div />').html($scope.ObjectiveSummaryDeatils.ObjectiveDescription).text();
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 3) {
                    $scope.numericqunatitativedisplay = false;
                    $scope.nonNumericQuantivedisplay = false;
                    $scope.ratingsdisplay = false;
                    $scope.qualitativedisplay = true;
                    $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck = $scope.ObjectiveSummaryDeatils.EnableCommnets;
                    $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions = $('<div />').html($scope.ObjectiveSummaryDeatils.ObjectiveDescription).text();
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 4) {
                    $scope.ratingItems = [];
                    for (var i = 0; i < $scope.ObjectiveSummaryDeatils.ObjectiveRating.length; i++) {
                        $scope.ratingItems.push({
                            RatingCaption: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatingCaption,
                            ObjectiveRatingID: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].ObjectiveRatingID,
                            IsRemoved: false,
                            RatingCount: $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount == 0 ? false : $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount > 0 ? true : $scope.ObjectiveSummaryDeatils.ObjectiveRating[i].RatinCount
                        });
                    }
                    $scope.numericqunatitativedisplay = false;
                    $scope.nonNumericQuantivedisplay = false;
                    $scope.qualitativedisplay = false
                    $scope.ratingsdisplay = true;
                    $scope.Objective.ngObjectiveRatingEnableCommentsCheck = $scope.ObjectiveSummaryDeatils.EnableCommnets;
                    $scope.Objective.ngObjectiveRatingDescriptionInstructions = $('<div />').html($scope.ObjectiveSummaryDeatils.ObjectiveDescription).text();
                }
                $timeout(function () {
                    $('#ObjectiveNumericGlobalbaseline').focus().select();
                }, 1000);

            }
            var ObjectiveUnitsResult = ObjUnits;
            $scope.ObjectiveUnitsData = ObjectiveUnitsResult;
            $scope.updateObjectiveSummary = function () {
                var objectiveSummary = {};
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 1) {
                    handleTextChangeEvents();
                    $scope.ObjSummaryGlobalBaseLine = 0;
                    $scope.ObjSummaryGlobalTarget = 0;
                    if ($scope.Objective.ngObjectiveNumericGlobalbaseline != 0) $scope.ObjSummaryGlobalBaseLine = $scope.Objective.ngObjectiveNumericGlobalbaseline.replace(/\s/g, '');
                    else $scope.ObjSummaryGlobalBaseLine = 0;
                    if ($scope.Objective.ngObjectiveNumericGlobaltarget != 0) $scope.ObjSummaryGlobalTarget = $scope.Objective.ngObjectiveNumericGlobaltarget.replace(/\s/g, '');
                    else $scope.ObjSummaryGlobalTarget = 0;
                    objectiveSummary.ObjectiveID = $scope.ObjectiveSummaryDeatils.Id;
                    objectiveSummary.ObjectiveUnits = $scope.Objective.ngObjectiveNumericUnits;
                    objectiveSummary.ObjectiveTypeId = $scope.ObjectiveSummaryDeatils.ObjectiveTypeId;
                    objectiveSummary.GlobalBaseLine = $scope.ObjSummaryGlobalBaseLine;
                    objectiveSummary.GlobalTarget = $scope.ObjSummaryGlobalTarget;
                    objectiveSummary.ObjectiveDescription = $scope.Objective.ngObjectiveNumericDescriptionInstructions;
                    objectiveSummary.EnableCommnets = $scope.Objective.ngObjectiveNumericEnableCommentsCheck;
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 2) {
                    objectiveSummary.ObjectiveID = $scope.ObjectiveSummaryDeatils.Id;
                    objectiveSummary.ObjectiveTypeId = $scope.ObjectiveSummaryDeatils.ObjectiveTypeId;
                    objectiveSummary.ObjectiveUnits = $scope.Objective.ngObjectivenonNumericUnits;
                    objectiveSummary.GlobalBaseLine = 0;
                    objectiveSummary.GlobalTarget = 0;
                    objectiveSummary.ObjectiveDescription = $scope.Objective.ngObjectivenonNumericDescriptionInstructions;
                    objectiveSummary.EnableCommnets = $scope.Objective.ngObjectivenonNumericEnableCommentsCheck;
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 3) {
                    objectiveSummary.ObjectiveID = $scope.ObjectiveSummaryDeatils.Id;
                    objectiveSummary.ObjectiveTypeId = $scope.ObjectiveSummaryDeatils.ObjectiveTypeId;
                    objectiveSummary.ObjectiveUnits = 0;
                    objectiveSummary.GlobalBaseLine = 0;
                    objectiveSummary.GlobalTarget = 0;
                    objectiveSummary.ObjectiveDescription = $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions;
                    objectiveSummary.EnableCommnets = $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck;
                }
                if ($scope.ObjectiveSummaryDeatils.ObjectiveTypeId == 4) {
                    if ($.grep($scope.ratingItems, function (e) { return e.RatingCaption == "" }).length > 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                        return false;
                    }
                    objectiveSummary.ObjectiveID = $scope.ObjectiveSummaryDeatils.Id;
                    objectiveSummary.ObjectiveTypeId = $scope.ObjectiveSummaryDeatils.ObjectiveTypeId;
                    objectiveSummary.ObjectiveUnits = 0;
                    objectiveSummary.GlobalBaseLine = 0;
                    objectiveSummary.GlobalTarget = 0;
                    objectiveSummary.ObjectiveDescription = $scope.Objective.ngObjectiveRatingDescriptionInstructions;
                    objectiveSummary.EnableCommnets = $scope.Objective.ngObjectiveRatingEnableCommentsCheck;
                    objectiveSummary.ObjectiveRatings = $scope.ratingItems;
                }
                ObjectiveoverviewService.UpdateObjectiveSummaryBlockData(objectiveSummary).then(function (objectiveSummaryResult) {
                    $('#EditObjectiveSummary').modal('hide');
                    if (objectiveSummaryResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_5653.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_5652.Caption'));
                        ObjectiveoverviewService.GettingObjectiveSummaryBlockDetails(parseInt($stateParams.ID, 10)).then(function (GetObjectiveSummaryBlockObj) {
                            var objeciveSummaryData = GetObjectiveSummaryBlockObj.Response;
                            LoadObjectiveSummaryBlock(objeciveSummaryData);
                        });
                    }
                });
                $timeout(function () {
                    $scope.TimerForLatestFeed();
                }, 2000);
            }
            $scope.objectiveItems = [];
            $scope.objectiveItems.push({
                ObjEntityType: [],
                ObjEntityAttributes: [],
                ObjAttributeOptionns: [],
                ObjCondition: 0
            });
            $scope.addMultiPleConditions = function () {
                if ($scope.objectiveItems[$scope.rowCount].ObjEntityType.length == 0 || $scope.objectiveItems[$scope.rowCount].ObjEntityAttributes.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1981.Caption'));
                    return false;
                } else {
                    $scope.rowCount = $scope.rowCount + 1;
                    $scope.objectiveItems.push({
                        ObjEntityType: [],
                        ObjEntityAttributes: [],
                        ObjAttributeOptionns: [],
                        ObjCondition: []
                    });
                    $scope.FulfillmentAttributes = [];
                    $scope.FulfillmentAttributeOptions = {};
                }
            };
            $scope.removeFulCondition = function (item) {
                if ($scope.rowCount > 0) {
                    $scope.rowCount = $scope.rowCount - 1;
                    $scope.objectiveItems.splice($.inArray(item, $scope.items), 1);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                    return false;
                }
            };
            $scope.ObjectiveFulfillEdit = function () {
                $('#EditObjectiveFulfillment').modal('show');
                ObjectiveoverviewService.GettingEditObjectiveFulfillmentDetails(parseInt($stateParams.ID, 10)).then(function (GetObjectiveFullConditionsListObj) {
                    var Result = GetObjectiveFullConditionsListObj.Response;                  
                    var datsval = "";
                    var datStartUTCval = "";
                    var datstartval = "";
                    var dateval = "";
                    var datEndUTCval = "";
                    var datendval = "";
                    datsval = Result[0].StartDate
                    datStartUTCval = datsval.substr(0, 10);
                    datstartval = new Date(datStartUTCval);
                    dateval = Result[0].EndDate
                    datEndUTCval = dateval.substr(0, 10);
                    datendval = new Date(datEndUTCval);                   
                    $scope.Objective.ngObjectiveStatDaterange = dateFormat(datstartval, $scope.DefaultSettings.DateFormat);
                    $scope.Objective.ngObjectiveEndDaterange = dateFormat(datendval, $scope.DefaultSettings.DateFormat);
                    $scope.Objective.ngObjectiveDaterule = Result[0].IsDateRule;
                    $scope.Objective.ngObjectiveMandatoryCheck = Result[0].IsMandatory;
                    if (Result != null) {
                        var html = '';
                        $.each(Result, function (val, item) {
                            var UniqueId = val;
                            html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                            html += "<ul class='repeter'>";
                            html += "<li class='form-inline' style='position: relative;'>";
                            if (val == 0) {
                                html += "<div style='display: inline-block; position: absolute; top: 0px; z-index: 100; bottom: 0px; width: 175px; background-color: #f9f9f9;'></div>";
                            } else {
                                html += "<div style='display: inline-block; position: absolute; top: 0px; z-index: 100; width: 175px;'></div>";
                            }
                            html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                            html += "<select ui-select2 id='ObjConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "' style='width:120px;'>";
                            html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + " -- </option>";
                            html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + "</option>";
                            html += "<option value='2'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                            html += "";
                            html += "</select>";
                            html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                            html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='dummymodule_ObjEntityType_" + UniqueId + "'  data-role='EntityType'    style='width: 120px;'>";
                            html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                            html += "";
                            html += "</select>";
                            html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                            html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'    data-role='Attributes' style='width: 120px;'>";
                            html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                            html += "";
                            html += "</select>";
                            html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                            html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "'  data-role='Equals' style='width: 120px;'>";
                            html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                            html += "";
                            html += "</select>";
                            html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                            html += "";
                            html += "</li>";
                            html += "</ul>";
                            html += "</div>";
                        });
                        $('#ObjectiveFulfillmentMetadatatbody').html($compile(html)($scope));
                        $.each(Result, function (val, item) {
                            var UniqueId = val;
                            $scope["dummymodule_ConditionType_" + UniqueId] = item.ConditionType;
                            FillEntityTypes("ObjEntityType" + UniqueId);
                            $timeout(function () {
                                $("#ObjEntityType" + UniqueId).select2("val", item.EntityTypeid);
                            }, 10);
                            LoadFillEntityTypeAttributes("ObjEntityAttributes" + UniqueId, item.EntityTypeid, item.Attributeid, item.AttributeLevel);
                            LoadFillEntityTypeOptions("ObjAttributeOptions" + UniqueId, item.Attributeid, item.AttributeLevel, item.ObjectiveOptionValue)
                        });
                    } else {
                        var html = '';
                        var UniqueId = 0;
                        html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                        html += "<ul class='repeter'>";
                        html += "<li class='form-inline' style='position: relative;'>";
                        html += "<div style='display: inline-block; position: absolute; top: 0px; z-index: 100; bottom: 0px; width: 175px; background-color: #f9f9f9;'></div>";
                        html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                        html += "<select ui-select2 id='ObjConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "' style='width:120px;'>";
                        html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + " -- </option>";
                        html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + "</option>";
                        html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                        html += "";
                        html += "</select>";
                        html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                        html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' data-role='EntityType' ng-model='dummymodule_ObjEntityType_" + UniqueId + "'  style='width: 120px;'>";
                        html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                        html += "";
                        html += "</select>";
                        html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                        html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes' style='width: 120px;'>";
                        html += "<option value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                        html += "";
                        html += "</select>";
                        html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                        html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "'  data-role='Equals' style='width: 120px;'>";
                        html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                        html += "";
                        html += "</select>";
                        html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                        html += "";
                        html += "</li>";
                        html += "</ul>";
                        html += "</div>";
                        $('#ObjectiveFulfillmentMetadatatbody').html($compile(html)($scope));
                        FillEntityTypes("ObjEntityType" + UniqueId);
                    }
                });

            }
            FilltFulfillmentEntityTypes();

            function FilltFulfillmentEntityTypes() {
                ObjectiveoverviewService.GetFulfillmentEntityTypes().then(function (fulFillmentEntityTypesObj) {
                    $scope.FulfillmentEntityTypes = fulFillmentEntityTypesObj.Response;
                });
            }

            function LoadFillEntityTypeAttributes(ControllerID, EntityTypeID, Attributeid, AttributeLevel) {
                if (ControllerID != undefined) {
                    ObjectiveoverviewService.GetFulfillmentAttribute(EntityTypeID).then(function (fulfillmentAttributeOptionsObj) {
                        if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                            if (ControllerID != undefined) {
                                $('#' + ControllerID).html("");
                                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                                if (fulfillmentAttributeOptionsObj.Response != null) {
                                    $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                        var currentId = item.Id.toString() + "_" + item.Level;
                                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                                    });
                                    $timeout(function () {
                                        $('#' + ControllerID).select2("val", Attributeid + "_" + AttributeLevel);
                                    }, 10);
                                }
                            }
                        }
                    });
                }
            }

            function LoadFillEntityTypeOptions(ControllerID, entityAttribtueId, entityAttributeLevel, selectedAttributeOption) {
                ObjectiveoverviewService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        if (ControllerID != undefined) {
                            $('#' + ControllerID).html("");
                            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>");
                            if (fulfillmentAttributeOptionsObj.Response != null) {
                                $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                    $('#' + ControllerID).append($("<option></option>").val(item.Id).html(item.Caption));
                                });
                                var Values = selectedAttributeOption.split(",");
                                $timeout(function () {
                                    $('#' + ControllerID).select2("val", Values);
                                }, 10);
                            }
                        }
                    }
                });
            }
            $("#ObjectiveFulfillmentMetadatatbody").click(function (event) {
                var TargetControl = $(event.target);
                var currentUniqueId = TargetControl.parents('div').attr('data-id');
                if (TargetControl.attr('data-role') == 'EntityType') {
                    if (TargetControl.val() == 0) {
                        ResetDropDown("ObjEntityAttributes" + currentUniqueId, currentUniqueId);
                        return false;
                    }
                    ObjectiveoverviewService.GetFulfillmentAttribute(TargetControl.val()).then(function (fulFillmentAttributesObj) {
                        if (fulFillmentAttributesObj.Response.length >= 0) {
                            FillEntityTypeAttributes("ObjEntityAttributes" + currentUniqueId, fulFillmentAttributesObj.Response);
                            ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                        }
                    });
                } else if (TargetControl.attr('data-role') == 'Attributes') {
                    if (TargetControl.val() == 0) {
                        ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                        return false;
                    }
                    var AttributeData = TargetControl.val().split("_");
                    var entityAttribtueId = parseInt(AttributeData[0], 10);
                    var entityAttributeLevel = parseInt(AttributeData[1], 10);
                    ObjectiveoverviewService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                        if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                            FillEntityTypeAttributes("ObjAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response);
                        }
                    });
                } else if (TargetControl.attr('data-role') == 'Equals') { } else if (TargetControl.attr('data-role') == 'Add') {
                    if (TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        return false;
                    }
                    if ($('#ObjEntityAttributes' + currentUniqueId).val() != "0" && $('#ObjEntityAttributes' + currentUniqueId).val() != null) {
                        if ($('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                            return false;
                        }
                    }
                    var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                    var html = '';
                    html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                    html += "<ul class='repeter' >";
                    html += "<li class='form-inline'>";
                    html += " <label class='control-label' for='ObjectiveCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                    html += "<select ui-select2 id='ObjConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "' style='width:120px;'>";
                    html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + "-- </option>";
                    html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + "</option>";
                    html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                    html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='dummymodule_ObjEntityType_" + UniqueId + "' data-role='EntityType'    style='width: 120px;'>";
                    html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                    html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes' style='width: 120px;'>";
                    html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                    html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "' data-role='Equals' style='width: 120px;'>";
                    html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                    html += "";
                    html += "</select>";
                    html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                    html += "";
                    html += "</li>";
                    html += "</ul>";
                    html += "</div>";
                    $("#container" + currentUniqueId).after($compile(html)($scope));
                    FillEntityTypes("ObjEntityType" + UniqueId);
                } else if (TargetControl.attr('data-role') == 'Remove') {
                    if (currentUniqueId == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                        return false;
                    }
                    $("#container" + currentUniqueId).remove();
                }
                $scope.TimerForLatestFeed();
            });

            function FillEntityTypes(ControllerID) {
                if (ControllerID != undefined) {
                    var objentities = $scope.FulfillmentEntityTypes;
                    $('#' + ControllerID).html("");
                    $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                    $.each(objentities, function (val, item) {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                    });
                }
            }

            function FillEntityTypeAttributes(ControllerID, Response) {
                if (ControllerID != undefined) {
                    $('#' + ControllerID).select2("val", "");
                    $('#' + ControllerID).html("");
                    $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                    $.each(Response, function (val, item) {
                        if (item.Level != undefined) {
                            var currentId = item.Id.toString() + "_" + item.Level;
                            $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                        } else {
                            $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                        }
                    });
                }
            }

            function ResetDropDown(ControllerID, currentUniqueId) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $('#' + ControllerID).select2("val", "");
                $('#ObjAttributeOptions' + currentUniqueId).html("");
                $('#ObjAttributeOptions' + currentUniqueId).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $('#ObjAttributeOptions' + currentUniqueId).select2("val", "");
            }
            $scope.SaveObjectiveDeatils = function (objectiveId, objectiveName, objectiveedittype, objectiveDescription, objectivenewValue) {
                var saveObjectiveNameObj = {};
                if (objectiveedittype == 111) {
                    saveObjectiveNameObj.ObjectiveID = $scope.CurrentObjectiveId;
                    saveObjectiveNameObj.ObjectiveName = objectivenewValue;
                    saveObjectiveNameObj.ObjectiveDescription = objectiveDescription;
                    saveObjectiveNameObj.Typeid = objectiveedittype;
                    $(window).trigger("UpdateObjectiveEntityName", objectivenewValue);
                    $('#breadcrumlink').text(objectivenewValue);
                    $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([objectivenewValue]);
                    $("ul li a[data-entityid='" + $stateParams.ID + "']").attr('data-entityname', objectivenewValue);
                } else {
                    saveObjectiveNameObj.ObjectiveID = $scope.CurrentObjectiveId;
                    saveObjectiveNameObj.ObjectiveName = objectiveName;
                    saveObjectiveNameObj.ObjectiveDescription = objectivenewValue;
                    saveObjectiveNameObj.Typeid = objectiveedittype;
                }
                ObjectiveoverviewService.UpdatingObjectiveOverDetails(saveObjectiveNameObj).then(function (saveObjectiveNameDataResult) {
                    if (saveObjectiveNameDataResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4368.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4540.Caption'));
                        saveObjectiveNameObj.ObjectiveID = '';
                        saveObjectiveNameObj.ObjectiveName = '';
                        saveObjectiveNameObj.ObjectiveDescription = '';
                    }
                });
                $scope.TimerForLatestFeed();
            }
            $scope.ConditionDataArr = [];

            function collectionObjectiveCondition() {
                var EntttyTypeArr = new Array();
                var attriArreArr = new Array();
                var optionArr = [];
                $scope.ConditionDataArr = [];
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    var uniquekey = $(this).attr('data-id')
                    if ($('#ObjEntityType' + uniquekey).val() != undefined) {
                        var collectionarr = {
                            'ObjEntityType': $('#ObjEntityType' + uniquekey).val(),
                            'ObjEntityAttributes': $('#ObjEntityAttributes' + uniquekey).val(),
                            'ObjAttributeOptionns': $('#ObjAttributeOptions' + uniquekey).val(),
                            'ConditionType': $('#ObjConditionType' + uniquekey).val()
                        };
                        $scope.ConditionDataArr.push(collectionarr);
                    }
                });
            }
            $scope.ObjDatavalue = {};
            var DateCompare = function () {
                var st_date = $('#ObjectiveStatDaterange').val();
                var end_date = $('#ObjectiveEndDaterange').val();
                if ($('#ObjectiveStatDaterange').val().trim() == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_4766.Caption'));
                    return false
                }
                if ($('#ObjectiveEndDaterange').val().trim() == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_4239.Caption'));
                    return false
                }
                if (Date.parse(end_date) < Date.parse(st_date)) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4238.Caption'));
                    return false
                } else return true
            }
            $scope.updateObjectiveFulFillment = function () {
                if (DateCompare() == false) return false;
                var tempval = false;
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    if ($('#ObjEntityType' + index).val() == "0" || $('#ObjEntityType' + index).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        tempval = true;
                        return false;
                    } else if ($('#ObjEntityAttributes' + index).val() != "0" && $('#ObjEntityAttributes' + index).val() != null) {
                        if ($('#ObjAttributeOptions' + index).val() == null) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                            tempval = true;
                            return false;
                        }
                    }
                });
                if (tempval == true) return false;
                collectionObjectiveCondition();
                $scope.ObjDatavalue = $scope.ConditionDataArr;
                var ObjectiveCondition = {};
                ObjectiveCondition.ObjectiveID = parseInt($stateParams.ID, 10);
                var datea = new Date($scope.Objective.ngObjectiveStatDaterange);
                var de = dateFormat(datea, $scope.DefaultSettings.DateFormat);
                ObjectiveCondition.ObjectiveStartDate =  new Date.create($scope.Objective.ngObjectiveStatDaterange.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                ObjectiveCondition.ObjectiveEndDate = new Date.create($scope.Objective.ngObjectiveEndDaterange.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                ObjectiveCondition.ObjectiveDateRule = $scope.Objective.ngObjectiveDaterule;
                ObjectiveCondition.IsMandatory = $scope.Objective.ngObjectiveMandatoryCheck;
                if ($scope.ObjectiveFulfillDeatils.FulfillCondition != '' && $scope.ObjectiveFulfillDeatils.FulfillCondition != undefined) ObjectiveCondition.ObjectiveFulfillDeatils = $scope.ObjectiveFulfillDeatils.FulfillCondition.replace(/\n|\r/g, "");
                else ObjectiveCondition.ObjectiveFulfillDeatils = $scope.ObjectiveFulfillDeatils.FulfillCondition;
                ObjectiveCondition.ObjectiveFulfillConditonValueData = [{
                    "ObjectiveFulfillValues": $scope.ObjDatavalue
                }];
                $('#EditObjectiveFulfillment').modal('hide');
                ObjectiveoverviewService.UpdateObjectiveFulfillmentCondition(ObjectiveCondition).then(function (objectiveFulfillConditionUpdateResult) {
                    if (objectiveFulfillConditionUpdateResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4367.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4537.Caption'));
                        ObjectiveoverviewService.GettingObjectiveFulfillmentBlockDetails(parseInt($stateParams.ID, 10)).then(function (GetObjectiveFulfillmentBlockObj) {
                            var ObjectiveFulfillDetails = GetObjectiveFulfillmentBlockObj.Response;
                            LoadFulfillMentBlock(ObjectiveFulfillDetails);
                            $scope.$emit("OnFullfillmentChange", ObjectiveCondition.ObjectiveID);
                        });
                    }
                });
                $scope.TimerForLatestFeed();
            }
            $scope.ObjectiveOwnerList = {};
            var ObjectiveOwners = EntOwnerList;
            var ObjectiveOwnersList = ObjectiveOwners
            $scope.ObjectiveOwnerList = ObjectiveOwnersList;
            $scope.ObjectivestatusList = [{
                'StatusId': 1,
                'StatusDescription': 'Active (0/1)'
            }, {
                'StatusId': 0,
                'StatusDescription': 'Deactivated (1/1)'
            }]
            $scope.UpdateObjectiveOwner = function (ObjectiveOwnerId, roleId, ObjectiveOwnerName) {
                var UpdateObjectiveOwnerObj = {};
                UpdateObjectiveOwnerObj.EntityID = parseInt($stateParams.ID, 10);
                UpdateObjectiveOwnerObj.UserId = ObjectiveOwnerId;
                UpdateObjectiveOwnerObj.RoleId = roleId;
                UpdateObjectiveOwnerObj.OldUserId = $scope.CurrentObjectiveOwnerId;
                UpdateObjectiveOwnerObj.ObjectiveOwnerName = ObjectiveOwnerName;
                UpdateObjectiveOwnerObj.EntityTypeID = 35;
                ObjectiveoverviewService.UpdateObjectiveOwner(UpdateObjectiveOwnerObj).then(function (UpdateObjectiveOwnerResult) { });
            };
            $scope.UpdateObjectivestatus = function (objSatusId) {
                var updateObjectiveStatusObj = {};
                updateObjectiveStatusObj.ObjeciveID = parseInt($stateParams.ID, 10);
                updateObjectiveStatusObj.ObjectiveStatus = objSatusId;
                ObjectiveoverviewService.UpdateObjectivestatus(updateObjectiveStatusObj).then(function (updateObjStatusDataResult) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = objSatusId == 1 ? 'Active' : 'Deactivated';
                                if ($scope.ListViewDetails[i].data.Response.Data[j]["Status"] != undefined) $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = objSatusId == 1 ? 'Active' : 'Deactivated';
                                if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.ObjectiveStatus] != undefined) $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.ObjectiveStatus] = objSatusId == 1 ? 'Active' : 'Deactivated';
                                $timeout(function () {
                                    $scope.TimerForLatestFeed();
                                }, 2000);
                                return false;
                            }
                        }
                    }
                });
            };
        }

        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];

            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }

        $scope.LoadSectionDetail_Temp = function (AttrtoAttrRel, EntAttrDet) {
            var ID = $stateParams.ID;

            //----------------------Financial Summary Block Started -----------------------//
            try {

                $scope.listAttriToAttriResult = [];
                //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
                try {
                    var entityAttrToAttrRelation = AttrtoAttrRel;
                    $scope.listAttriToAttriResult = [];
                    if (entityAttrToAttrRelation != null)
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation;

                }
                catch (ex) {
                }
                //-----------------------------------------------------------

                //----------------------> GET THE VALIDATION AND ISREADONLY DETAILS FOR THE ENTITY TYPE <----------------

                //var getentityattributesValidationDetails = EntAttrValDet;
                //$scope.listAttributeValidationResult = [];
                //$scope.listAttributeValidationResult = getentityattributesValidationDetails;

            }
            catch (e)
            { }

            try {
                $scope.fields = {
                    usersID: 0,
                };
                $scope.tree = {};
                $scope.fieldKeys = [];
                $scope.options = {};

                $scope.setFieldKeys = function () {
                    var keys = [];

                    angular.forEach($scope.fields, function (key) {
                        keys.push(key);
                        $scope.fieldKeys = keys;
                    });

                }

                $scope.ownername = $cookies['Username'];
                $scope.ownerid = $cookies['UserId'];
                $scope.ownerEmail = $cookies['UserEmail'];


                //----------------> LOADING DETAILS BLOCK STARTS HERE <---------------------
                $scope.StopeUpdateStatusonPageLoad = false;
                $scope.detailsLoader = false;
                $scope.detailsData = true;
                $scope.dyn_Cont = '';
                var getentityattributesdetails = EntAttrDet;
                $scope.dyn_Cont = "";
                $scope.attributedata = getentityattributesdetails;
                $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label">' + $translate.instant('LanguageContents.Res_69.Caption') + '</label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
                if ($scope.IsLock == false) {
                    $scope.ActivityName = $scope.RootLevelEntityName;
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_1.Caption') + '</label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="entityeditcontrolclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\"' + $translate.instant('LanguageContents.Res_1.Caption') + '\"  data-type=\"text\" data-original-title=\"Activity Name\">' + $scope.ActivityName + '</a></div></div>';
                }
                else if ($scope.IsLock == true) {
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $translate.instant('LanguageContents.Res_1.Caption') + '</label><div class=\"controls\"><label class="control-label">' + $scope.RootLevelEntityNameTemp + '</label></div></div>';
                }
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 6) {
                        $scope.dyn_Cont2 = '';
                        //construct inline scope objects

                        var CaptionObj = $scope.attributedata[i].Caption.split(",");

                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    //$scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                }
                                else {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    //$scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            }
                            else {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    //$scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                }
                                else {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    //$scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            }
                        }
                        $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        //$scope.settreelevels();
                        $scope.items = [];
                        //$scope.settreeSources();
                        //$scope.settreeTexts();
                        //$scope.settreelevels();
                        $scope.setFieldKeys();

                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group\"><label class=\"control-label AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + $scope.attributedata[i].ID + '\" >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 12) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption;
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Lable.length == 1) {
                                var k = j;
                                var treeTexts = [];
                                var fields = [];
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                if (k == CaptionObj.length) {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                                else {
                                    if (CaptionObj[k] != undefined) {
                                        for (k; k < CaptionObj.length; k++) {
                                            treeTexts.push(CaptionObj[k]);
                                            //$scope.settreeTexts();
                                            fields.push(CaptionObj[k]);
                                            $scope.setFieldKeys();
                                        }
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                    }
                                    else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }

                            }
                            else {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        //$scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    }
                                    else {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        //$scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                }
                                else {
                                    var k = j;
                                    if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                        else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    //$scope.settreeTexts();
                                                    fields.push(CaptionObj[k]);
                                                    $scope.setFieldKeys();
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            }
                                            else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    }
                                    else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            //$scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        }
                                        else {
                                            $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            //$scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    }
                                }
                            }
                        }
                        $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        //$scope.settreelevels();
                        $scope.items = [];
                        //$scope.settreeSources();
                        //$scope.settreeTexts();
                        //$scope.settreelevels();
                        $scope.setFieldKeys();

                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;

                            var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + '\" >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    }

                    else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";

                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    }

                    else if ($scope.attributedata[i].TypeID == 11) {  // inline uploader

                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                        $scope.setFieldKeys();
                        $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        $scope.setUploaderCaption();
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group ng-scope AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                        $scope.dyn_Cont += '<div class=\"controls\">';
                        //$scope.dyn_Cont += '<img src="UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        //$scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview_' + $scope.attributedata[i].ID + '">';
                        if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                            $scope.attributedata[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview_' + $scope.attributedata[i].ID + '">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '</div></div>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div class="modal-body hide" id="pickfilesobjdetailcontainer">';
                                $scope.dyn_Cont += '<button id="pickfilesobjdetail" style="visibility: hidden" type="submit"></button>';
                                $scope.dyn_Cont += '</div>';
                                $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefile(" + $scope.attributedata[i].ID + ")'  attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">' + $translate.instant('LanguageContents.Res_4729.Caption');
                                $scope.dyn_Cont += '</a></div></div>';
                            }
                            else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '</div></div>';
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 3) {

                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {

                            if ($scope.attributedata[i].Caption[0] != undefined) {

                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].options != null)
                                    if ($scope.attributedata[i].options.length > 0)
                                        $scope.normaltreeSources["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].options;
                                //$scope.setNormalDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                            else {

                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                //$scope.setNormalDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                    $scope.dyn_Cont += '</div></div>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            }

                        } else {

                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 0) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    //$scope.setNormalDropdownCaption();

                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                    else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        }
                                        else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                        }
                                    }
                                }
                            }
                            else {

                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                //$scope.setNormalDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 4) {

                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                //$scope.setNormalMultiDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        }
                        else {

                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            //$scope.setNormalMultiDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    }

                    else if ($scope.attributedata[i].TypeID == 10) {

                        var inlineEditabletitile = $scope.attributedata[i].Caption;

                        perioddates = [];

                        $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.IsStartDateEmpty = true;
                            $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                            $scope.dyn_Cont += '</div>';
                        }
                        else {

                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {

                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";

                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(0, 10);
                                datstartval = new Date.create(datStartUTCval);
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(0, 10);
                                datendval = new Date.create(datEndUTCval);

                                perioddates.push({ ID: $scope.attributedata[i].Value[j].Id, value: datendval });

                                $scope.fields["PeriodStartDateopen_" + $scope.attributedata[i].Value[j].Id] = false;

                                $scope.fields["PeriodEndDateopen_" + $scope.attributedata[i].Value[j].Id] = false;

                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.format);
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.format);

                                $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.format);
                                $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.format);

                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }

                                $('#fsedateid').css("visibility", "hidden");
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                $scope.dyn_Cont += '<div class="inputHolder span11">';

                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                $scope.dyn_Cont += '<div class="controls">';

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                    $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.MinValue = $scope.attributedata[i].MinValue;
                                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                        $scope.MinValue = $scope.attributedata[i].MinValue;
                                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
                                        }
                                        else {
                                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
                                        }
                                        else {
                                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
                                        }
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                        $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    }
                                }

                                $scope.dyn_Cont += '</div></div>';

                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $translate.instant('LanguageContents.Res_5051.Caption') + inlineEditabletitile + '</label>';
                                $scope.dyn_Cont += '<div class="controls">';

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    }
                                }

                                $scope.dyn_Cont += '</div></div></div>';

                                if (j != 0) {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                    }
                                }

                                $scope.dyn_Cont += '</div>';

                                if (j == ($scope.attributedata[i].Value.length - 1)) {

                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                                    $scope.dyn_Cont += '</div>';
                                }
                            }
                        }
                        $scope.dyn_Cont += ' </div>';

                        $scope.dyn_Cont += '<div class="control-group nomargin">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">' + $translate.instant('LanguageContents.Res_5056.Caption') + '</label>';
                            $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[' + $translate.instant('LanguageContents.Res_5065.Caption') + ' ]</a>';
                            //$scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                if ($scope.attributedata[i].Value == "-") {
                                    $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                }
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                $scope.dyn_Cont += '</div>';
                            }
                            else if ($scope.IsLock == true) {
                                //$scope.dyn_Cont += '<span class="controls"><label class="control-label">[Add ' + inlineEditabletitile + ' ]</label></span>';
                                $scope.dyn_Cont += '<span></span>';
                            }
                        }
                        $scope.dyn_Cont += '</div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {

                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;

                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {

                            datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                        }
                        else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    }
                                    else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    }
                                    else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                        else {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';

                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                        $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                        $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0 relative\">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        if ($scope.IsLock == false) {
                            $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                }
                                else
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }

                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';

                            $scope.dyn_Cont += ' </div>';
                        }
                        else {
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                }
                                else
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }

                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                        }
                        $scope.dyn_Cont += '</div></div>';

                    }
                    else if ($scope.attributedata[i].TypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 19) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                            if ($scope.attributedata[i].Value != null) {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
                                //$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html(($scope.attributedata[i].Value.Amount)).text();
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
                                var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
                                $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) { return e.Id == currtypeid; }));
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
                            } else {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
                            }
                            $scope.currencytypeslist = $scope.CurrencyFormatsList;
                            $scope.setFieldKeys();
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            //$scope.setNormalDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 13) {


                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                        for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {


                            if (price.selection.length > 0) {
                                var selectiontext = "";
                                var valueMatches = [];
                                if (price.selection.length > 0)
                                    valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                    });
                                if (valueMatches.length > 0) {
                                    selectiontext = "";
                                    for (var x = 0, val; val = valueMatches[x++];) {
                                        selectiontext += val.caption;
                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                        else selectiontext += "</br>";
                                    }
                                }
                                else
                                    selectiontext = "-";
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                            }
                            else {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                            }

                            if ($scope.attributedata[i].IsReadOnly == false && $scope.IsLock == false) {

                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                            }
                            else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                            }

                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 17) {

                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 0) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                //$scope.setNormalMultiDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                                else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        }
                        else {

                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            //$scope.setNormalMultiDropdownCaption();
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                            else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        //var datStartUTCval = "";
                        //var datstartval = "";
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date.create(dateExpireUTCval);
                            dateExpire = dateFormat((dateExpireUTCval), $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        }
                        else {
                            dateExpire = "-"
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            //$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">' + dateExpire + '</label></div></div>';
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + dateExpire + '</span></div></div>';
                        }
                        else {
                            if ($scope.IsLock == false) {
                                //$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                //$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">' + dateExpire + '</a></span></div></div>';
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">' + dateExpire + '</a></span></div></div>';
                            }
                            else if ($scope.IsLock == true) {
                                // $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"> ' + dateExpire + '</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        //var datStartUTCval = "";
                        //var datstartval = "";
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {

                            var dateExpireUTCval = "";
                            var dateExpireval = "";

                            // dateExpireUTCval = $scope.attributedata[i].Value.substr(6, ($scope.attributedata[i].Value.indexOf('+') - 6));
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date.create(dateExpireUTCval);
                            dateExpire = dateFormat(ConvertDateFromStringToString(ConvertDateToString(dateExpireval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);

                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        }

                    }

                    else if ($scope.attributedata[i].TypeID == 18) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";

                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';

                    }
                    else if ($scope.attributedata[i].TypeID == 9) {
                        //attrID, attributetypeid, entityTypeid, optionarray
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                        $scope.dyn_Cont += "<div ng-show= ShowHideAttributeOnRelation.Attribute_" + $scope.attributedata[i].ID + "  class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.attributedata[i].AttributeID + "\">" + $scope.attributedata[i].Caption + " : </label><div class=\"controls\"><input ng-click=\"saveDropdownTree(" + $scope.attributedata[i].ID + "," + $scope.attributedata[i].TypeID + "," + parseInt($stateParams.ID) + "," + $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] + "  )\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.attributedata[i].ID + "\" id=\"CheckBoxSelection_" + $scope.attributedata[i].ID + "\" placeholder=\"" + $scope.attributedata[i].Caption + "\"></div></div>";
                    }
                }


                $("#detailsDiv").empty();
                $("#detailsDiv").append($scope.dyn_Cont);
                $compile($("#detailsDiv").contents())($scope);

                $timeout(function () {
                    StartUploaderObjDetail();
                }, 100);


                $timeout(getentityattributesdetails, 100);

                //---------------->  DETAILS BLOCK ENDS HERE <---------------------

                $scope.CurrentWorkflowStepName = "";
                $scope.workflowstepOptions = [];
                $scope.workflowSummaryList = [];
                $scope.SelectedWorkflowStep = 0;
                $scope.WorkFlowStepID = 0;
                $scope.WorkflowSummaryControls = '';
                $scope.WorkFlowDiable = true;
                try {
                    $scope.UpdateEntityWorkFlowStep = function (stepID) {

                        //var TaskStatusserv = $resource('planning/UpdateEntityActiveStatus/:EntityID/:Status', { EntityID: parseInt($stateParams.ID, 10), Status: stepID }, { update: { method: 'PUT' } });

                        //var TaskStatusData = new TaskStatusserv();
                        var TaskStatusData = {};
                        TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
                        TaskStatusData.Status = stepID;
                        //  var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
                        PlanningService.UpdateEntityActiveStatus(TaskStatusData).then(function (TaskStatusResult) {
                            if (TaskStatusResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                            }
                            else {

                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                                        if (EntityId == parseInt($stateParams.ID, 10)) {
                                            if ($scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] != undefined)
                                                $scope.ListViewDetails[i].data.Response.Data[j][SystemDefiendAttributes.Status] = $scope.CurrentWorkflowStepName;
                                            else
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Status"] = $scope.CurrentWorkflowStepName;
                                            return false;
                                        }
                                    }
                                }
                                $scope.SelectedWorkflowStep = 0;
                            }
                        });
                    }

                }
                catch (exc) {

                }
            }
            catch (e)
            { }

        };


        $scope.objectiveeditcontrolclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select();
            }, 10);
        };

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0)
                    GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        $scope.TaskListSummerydetal = [];
        $scope.EnableDisableControlsHolder = {};

        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = [];
            $scope.DynamicTaskListDescriptionObj = "-";
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();
        }
        $scope.DynamicClass = 'btn-success';

        function FindmaxWidth() {
            var maxwidth = $scope.TasksInprgress;
            if (maxwidth < $scope.Unassigned) maxwidth = $scope.Unassigned;
            if (maxwidth < $scope.Overduetasks) maxwidth = $scope.Overduetasks;
            if (maxwidth < $scope.Unabletocomplete) maxwidth = $scope.Unabletocomplete;
            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }

        function LoadTaskSummaryDetlTemp(EntTaskList) {
            ClearSummaryDetl();
            $scope.TaskListSummerydetal = [];
            $scope.DynamicTaskListDescriptionObj = "-";
            var TaskSummaryresult = EntTaskList;
            if (TaskSummaryresult.length > 0) {
                $scope.ShowEmptyTaskSummary = false;
                $scope.ShowLoadedTaskSummary = true;
                $scope.TaskListSummerydetal = TaskSummaryresult;
                $scope.OverAllStatus = $scope.TaskListSummerydetal[0].ActiveEntityStateID;
                $scope.Ontimecomment = ($scope.TaskListSummerydetal[0].OnTimeComment != null ? $scope.TaskListSummerydetal[0].OnTimeComment : '-');
                $scope.TasksInprgress = $scope.TaskListSummerydetal[0].TaskInProgress;
                $scope.Unassigned = $scope.TaskListSummerydetal[0].UnAssignedTasks;
                $scope.Overduetasks = $scope.TaskListSummerydetal[0].OverDueTasks;
                $scope.Unabletocomplete = $scope.TaskListSummerydetal[0].UnableToComplete;
                FindmaxWidth();
            } else {
                $scope.ShowEmptyTaskSummary = true;
                $scope.ShowLoadedTaskSummary = false;
            }
        }

        $scope.ratingItems = [];
        $scope.checklistratingIndex = 0;
        $scope.AddRatings = function (index, Id) {
            if ($scope.ratingItems[index].RatingCaption == "" || $.grep($scope.ratingItems, function (e) { return e.RatingCaption == "" }).length > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                return false;
            }
            if ($scope.checklistratingIndex == -1) {
                $scope.ratingItems.push({
                    RatingCaption: '',
                    ObjectiveRatingID: 0,
                    IsRemoved: false,
                    RatingCount: false
                });
            }
            else {
                $scope.checklistratingIndex = parseInt(index) + 1;

                $scope.ratingItems.splice(parseInt($scope.checklistratingIndex), 0, {
                    RatingCaption: '',
                    ObjectiveRatingID: 0,
                    IsRemoved: false,
                    RatingCount: false
                });
            }
        }
        $scope.RemoveRatings = function (index, ID, item) {
            var res_count = $.grep($scope.ratingItems, function (e) { return e.IsRemoved == false }).length;
            if (res_count == 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_1917.Caption'));
                return false;
            }
            if (ID != 0) {
                var res = $.grep($scope.ratingItems, function (e) { return e.ObjectiveRatingID == ID });
                if (res.length > 0) {
                    res[0].IsRemoved = true;
                    return false;
                }
            }
            $scope.ratingItems.splice($.inArray(item.item, $scope.ratingItems), 1);
        }

        $scope.Clearxeditabletreedropdown = function Clearxeditabletreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel ; i <= levelcnt; i++) {
                $scope['dropdown_' + attrid + '_' + i] = "";
            }

        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        $scope.objectiveeditcontrolclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select();
            }, 10);
        };
        $scope.TaskListSummerydetal = [];

        function ClearSummaryDetl() {
            $scope.TaskListSummerydetal = [];
            $scope.DynamicTaskListDescriptionObj = "-";
            $scope.Ontimecomment = '-';
            $scope.TasksInprgress = 0;
            $scope.Unassigned = 0;
            $scope.Overduetasks = 0;
            $scope.Unabletocomplete = 0;
            $scope.TaskListSummerydetal = [];
            FindmaxWidth();
        }
        $scope.DynamicClass = 'btn-success';

        function FindmaxWidth() {
            var maxwidth = $scope.TasksInprgress;
            if (maxwidth < $scope.Unassigned) maxwidth = $scope.Unassigned;
            if (maxwidth < $scope.Overduetasks) maxwidth = $scope.Overduetasks;
            if (maxwidth < $scope.Unabletocomplete) maxwidth = $scope.Unabletocomplete;
            if (maxwidth == 0) {
                $scope.taskinprgwidth = 0;
                $scope.unassignwidth = 0;
                $scope.overduetaskwidth = 0;
                $scope.unabletocompletewidth = 0;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            } else {
                $scope.taskinprgwidth = ((($scope.TasksInprgress / maxwidth) * 100) / 100) * 80;
                $scope.unassignwidth = ((($scope.Unassigned / maxwidth) * 100) / 100) * 80;
                $scope.overduetaskwidth = ((($scope.Overduetasks / maxwidth) * 100) / 100) * 80;
                $scope.unabletocompletewidth = ((($scope.Unabletocomplete / maxwidth) * 100) / 100) * 80;
                if (document.getElementById('taskinprg') != undefined) {
                    document.getElementById('taskinprg').style.width = $scope.taskinprgwidth + '%';
                }
                if (document.getElementById('unassign') != undefined) {
                    document.getElementById('unassign').style.width = $scope.unassignwidth + '%';
                }
                if (document.getElementById('overduetask') != undefined) {
                    document.getElementById('overduetask').style.width = $scope.overduetaskwidth + '%';
                }
                if (document.getElementById('unabletocomplete') != undefined) {
                    document.getElementById('unabletocomplete').style.width = $scope.unabletocompletewidth + '%';
                }
            }
        }


        $scope.Clearxeditablemultiselecttreedropdown = function Clearxeditablemultiselecttreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel ; i <= levelcnt; i++) {
                $scope['multiselectdropdown_' + attrid + '_' + i] = "";
            }
        }

        $('#EntityFeedsdiv').on('click', 'a[data-id="taskpopupopen"]', function (event) {
            var entityid = $(this).attr('data-entityid');
            var taskid = $(this).attr('data-taskid');
            var typeid = $(this).attr('data-typeid');
            ObjectiveoverviewService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    $scope.$emit("pingMUITaskEdit", {
                        TaskId: taskid,
                        EntityId: entityid
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="feedpath"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-entityid');
            var typeid = $(this).attr('data-typeid');
            var parentid = $(this).attr('data-parentid');
            ObjectiveoverviewService.IsActiveEntity($(this).attr('data-entityid')).then(function (result) {
                if (result.Response == true) {
                    if (typeid == 11 || typeid == 10) $location.path('/mui/planningtool/default/detail/section/' + parentid + '/' + 'objective' + '');
                    else $location.path('/mui/planningtool/default/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-command="openlink"]', function (event) {
            var TargetControl = $(this);
            var mypage = TargetControl.attr('data-Name');
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        });
        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
            var EntityID = $(this).attr('data-parentid');
            var funderqid = $(this).attr('data-entityid');
            ObjectiveoverviewService.IsActiveEntity(funderqid).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', funderqid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('#EntityFeedsdiv').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-costcentreid');
            ObjectiveoverviewService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                if (result.Response == true) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });

        function SetCursorPosition(ctrID, cursorPosition) {
            var cnt = ctrID.val().length;
            if (cnt > 4) {
                cursorPosition = cursorPosition + 1;
                if (cnt > 5) cursorPosition = cursorPosition - 1;
                if (cnt > 8) cursorPosition = cursorPosition + 1;
                if (cnt > 9) cursorPosition = cursorPosition - 1;
            }
            setTimeout(function () {
                ctrID.caret(cursorPosition);
            }, 0);
        }

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function handleTextChangeEvents() {
            var allocatedamountList = $('#ObjectiveNumericGlobalbaseline ,#ObjectiveNumericGlobaltarget');
            for (var i = 0; i < allocatedamountList.length; i++) {
                $(allocatedamountList[i]).keydown(function (event) {
                    var caretPos = $(this).caret();
                    if ($(this).val().contains("-")) {
                        if (event.keyCode === 189 || event.key === "-") {
                            return false;
                            event.preventDefault();
                        }
                    }
                    if ($(this).val().contains(".")) {
                        if (event.keyCode === 190) {
                            return false;
                            event.preventDefault();
                        } else {
                            var dotindex = $(this).val().indexOf(".")
                            if (caretPos > (dotindex + 2)) {
                                return false;
                                event.preventDefault();
                            }
                        }
                    }
                    return FinancialValidation(event);
                }).change(function (event) {
                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                }).focusout(function (event) {
                    if ($(this).val().length == 0 || $(this).val() == 'NaN') {
                        $(this).val('0');
                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                        event.stopPropagation();
                        return false;
                    } else {
                        if (event.keyCode == 9) {
                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                            event.stopPropagation();
                            return false;
                        } else {
                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                        }
                    }
                }).keyup(function (event) {
                    if ($(this).val().length == 0 || $(this).val() == 'NaN') {
                        $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                        $(this).val('');
                        event.stopPropagation();
                        return false;
                    } else if ($(this).val().length == 0 || $(this).val() == '-') {
                        $(this).val('-');
                        event.stopPropagation();
                        return false;
                    } else {
                        if (event.keyCode != 9) {
                            var caretPos = $(this).caret();
                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                            SetCursorPosition($(this), caretPos);
                            if ($(this).val().length == 11) {
                                $(this).val($(this).val().slice(0, -1));
                                if (event.keyCode != 9) {
                                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                                } else {
                                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                }
                            } else if ($(this).val().length == 13) {
                                $(this).val($(this).val().slice(0, -2));
                                if (event.keyCode != 9) {
                                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, '')));
                                } else {
                                    $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                                }
                            }
                        } else {
                            $(event.target).val(CurrencyFormat($(event.target).val().replace(/ /g, ''))).select();
                            event.stopPropagation();
                            return false;
                        }
                    }
                });
            }
        }

        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesobjdetail").click();
        }
       

        function StartUploaderObjDetail () {
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesobjdetail',
                    container: 'pickfilesobjdetailcontainer',
                    max_file_size: '10mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#uploadprogress' + $scope.PopupModalGroupID + ' .bar').css("width", file.percent + "%");
                });
                uploader.bind('Error', function (up, err) {
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveFileDetails(file, response.response);

                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesobjdetail',
                    container: 'pickfilesobjdetailcontainer',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader.bind('Init', function (up, params) { });

                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) { });
                uploader.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response);

                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

        }

        function SaveFileDetails(file, response) {

            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.create('today');
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.EntityID = parseInt($stateParams.ID, 10);
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            ObjectiveoverviewService.copyuploadedImage(uplaodImageObject.FileName).then(function (ImgRes) {
                if (ImgRes.Response != null) {
                    uplaodImageObject.FileName = ImgRes.Response;
                    ObjectiveoverviewService.UpdateImageName(uplaodImageObject).then(function (uplaodImageObjectResult) {
                        $('#overviewUplaodImagediv').modal('hide');
                        if (uplaodImageObjectResult.Response != 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                                $('#' + PreviewID).attr('src', cloudpath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }
                            else {
                                $('#' + PreviewID).attr('src', TenantFilePath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }
                        }
                    });
                }
            })

        }

        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($stateParams.ID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                ObjectiveoverviewService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        ObjectiveoverviewService.GetAttributeTreeNodeByEntityID(attrID, parseInt($stateParams.ID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            Timeoutvar.latestfeedtimeout = $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.overviewCtrl']"));
        });
    }
    app.controller("mui.planningtool.objective.detail.overviewCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$stateParams', '$location', '$window', '$sce', '$translate', 'ObjectiveoverviewService', 'PlanningService', 'CommonService', muiplanningtoolobjectivedetailoverviewCtrl]);
})(angular, app);