///#source 1 1 /app/controllers/mui/planningtool/objective/list-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivelistCtrl($scope, $timeout, $compile, $resource, $location, $window, $cookies, $cookieStore, $translate, ObjectivelistService, $modal) {
        $scope.PageSize = 1;
        $scope.IsSelectAllChecked = false;
        $scope.LoadObjectiveControls = function () {
            $('#newObjectiveControls').modal('show');
            $('#newObjectiveControls').modal('show');
            $("#Div_Objlist").trigger('loadObjectiveUIControls');
            $timeout(function () {
                $('#ObjectiveName').focus().select()
            }, 1000);
        }
        $scope.ObjectiveCreationVar = {
            objEntityTypes: [],
            IsToShowRootObjEntity: false
        };
        $scope.appliedfilter = "No filter applied";
        $scope.SelectedFilterID = 0;
        $scope.AddEditFilter = "Add new filter";
        ObjectivelistService.GetFilterSettings(10).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () { $("#rootlevelfilter").trigger('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]); }, 200);
        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow");
                $scope.deletefiltershow = false;
                $("#rootlevelfilter").trigger('ClearScope', 10);
            } else {
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', $scope.SelectedFilterID);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow");
            }
        };
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            setTimeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $scope.ApplyFilter = function (filterid, filtername) {
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
            } else {
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
                $("#rootlevelfilter").trigger('ClearScope', 10);
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            $("#rootlevelfilter").trigger('ApplyRootLevelFilter', [filterid, filtername]);
        }
        $("#Div_Objlist").on('ClearAndReApply', function () {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        $("#Div_Objlist").on('ReloadFilterSettings', function (event, TypeID, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', $scope.SelectedFilterID);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = 'Edit filter';
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $scope.SelectedFilterID = 0;
            }
            ObjectivelistService.GetFilterSettings(TypeID).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.appliedfilter = filtername;
            });
        });
        $scope.Level = "0";
        var CurrentuserId = $scope.UserId;
        if (ShowAllRemember.Objective == true) {
            $scope.DisablePart = true;
            CurrentuserId = $scope.UserId;
            $("#btnParticipated").removeClass("active").addClass("active");
            $("#btnViewAll").removeClass("active");
        } else {
            $scope.DisablePart = false;
            CurrentuserId = 0;
            $("#btnViewAll").removeClass("active").addClass("active");
            $("#btnParticipated").removeClass("active");
        }
        $scope.tglHide = true;
        $scope.ShowAll = function (IsActive) {
            $scope.DisablePart = !$scope.DisablePart;
            $("#ListContainer table").html('');
            $("#treeHolder ul").html('');
            $('#RootLevelSelectAll').next('i').removeClass('checked');
            ClearCheckBoxes();
            if (ShowAllRemember.Objective == true) {
                CurrentuserId = $scope.UserId
            }
            if (IsActive) {
                $("#btnViewAll").removeClass("active").addClass("active");
                $("#btnParticipated").removeClass("active");
                CurrentuserId = 0;
                ShowAllRemember.Objective = false;
            } else {
                $("#btnParticipated").removeClass("active").addClass("active");
                $("#btnViewAll").removeClass("active");
                CurrentuserId = $scope.UserId;
                ShowAllRemember.Objective = true;
            }
            GetRootLevelActivityListCount();
            if ($("#ListColumn > thead tr th").length == 1) $('#ListColumn > thead').html("");
        };

        function ClearCheckBoxes() {
            $scope.IsSelectAllChecked = false;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = false;
                $(this).next('i').removeClass('checked');
            });
        }
        $scope.EntityTypeID = 10;
        var TypeID = 10;
        $scope.selectedFilterID = 0;
        $scope.FilterID = {
            selectedFilterID: 0
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };
        $scope.onTreeDataMouseEnter = function () {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-id="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeDataMouseLeave = function () {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-id="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.onTreeMouseEnter = function () {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-id="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeMouseLeave = function () {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-id="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.listColumnDefsdata = {};
        $scope.NameExcludeFilter = function (columndefs) {
            if (columndefs.Field != "Name") {
                return true;
            }
        };
        $scope.load = function (parameters) { };
        $scope.DeleteEntity = function () {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length != 0) {
                var object = {};
                object.ID = IDList;
                bootbox.confirm($translate.instant('LanguageContents.Res_16.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            ObjectivelistService.DeleteEntity(object).then(function (data) {
                                if (data.Response == true) {
                                    for (var i = 0; i < IDList.length; i++) {
                                        var entityId = IDList[i];
                                        $('#treeHolder a[data-id=' + entityId + ']').remove();
                                        $('#ListContainer tr[data-id=' + entityId + ']').remove()
                                    }
                                    $('#RootLevelSelectAll').next('i').removeClass('checked');
                                    ClearCheckBoxes()
                                }
                            });
                        }, 100);
                    }
                });
            }
        };

        function SelectEntityIDs() {
            var IDList = new Array();
            $('#listdataHolder input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        $scope.sortOrder = "null";
        $scope.IsDesc = false;
        var IsDesc = false;
        var sortOrder = "";
        $scope.LoadRootLevelObjective = function (PageIndex, StartRowNo, MaxNoofRow, filterid, filterattributes) {
            $(window).AdjustHeightWidth();
            var FilterID = filterid;
            if (RememberObjFilterID != 0 || RememberObjFilterAttributes.length != 0) {
                FilterID = RememberObjFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberObjFilterAttributes;
                if (RememberObjFilterAttributes.length > 0) {
                    setTimeout(function () {
                        $("#rootlevelfilter").trigger('RemeberApplyFilterReLoad', [10, RememberObjFilterAttributes])
                    }, 10);
                }
                if (RememberObjFilterID != 0) {
                    $scope.appliedfilter = RemeberObjFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = $scope.FilterID.selectedFilterID;
                }
            }
            var FilterObj = filterattributes;
            $window.ObjectiveFilterName = '';
            var getactivities = {};
            getactivities.StartRowNo = StartRowNo;
            getactivities.MaxNoofRow = MaxNoofRow;
            getactivities.FilterID = FilterID;
            getactivities.SortOrderColumn = $scope.sortOrder;
            getactivities.IsDesc = $scope.IsDesc;
            getactivities.IncludeChildren = false;
            getactivities.EntityID = '0';
            getactivities.UserID = CurrentuserId;
            getactivities.Level = $scope.Level;
            getactivities.IDArr = [];
            getactivities.FilterAttributes = FilterObj;
            ObjectivelistService.ObjectiveRootLevel(getactivities).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.ColumnDefs.length > 0) {
                    var listColumnDefsdata = getactivitylist.Response.ColumnDefs;
                    var listContent = getactivitylist.Response.Data;
                    var contentnHtml = "";
                    var columnHtml = "<tr >";
                    var treeHtml = "";
                    var colStatus = false;
                    for (var i = 0; i < listContent.length; i++) {
                        if (i != undefined) {
                            contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                            for (var j = 0; j < listColumnDefsdata.length; j++) {
                                if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                    if (colStatus == false) {
                                        columnHtml += "<th>";

                                        columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                        columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                        columnHtml += "</a>";
                                        columnHtml += "</th>";
                                    }
                                    if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                        contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                    else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                        contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null || "          " ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                    else
                                        contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                }
                            }
                            treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                            contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input type='checkbox' ><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                            contentnHtml += "</tr>";
                            colStatus = true;
                        }

                    }
                    if ($("#ListColumn > thead tr").length < 1) {
                        columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                        columnHtml += "</tr>";
                        $('#ListColumn > thead').html(columnHtml);
                    }
                    if ($("#ListColumn > thead tr").length > 0) {
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                        $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                        $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                        ListCheckBoxitemClick();
                    }
                }
                $(window).AdjustHeightWidth();
            });
        }
        GetRootLevelActivityListCount();

        function GetRootLevelActivityListCount() {
            var FilterID = 0;
            $scope.noData = false;
            var ObjAttrs = $cookieStore.get('ObjAttrs' + parseInt($cookies['UserId'], 10));
            var ObjFilterID = $cookieStore.get('ObjFilterID' + parseInt($cookies['UserId'], 10));
            var ObjFilterName = $cookieStore.get('ObjFilterName' + parseInt($cookies['UserId'], 10));
            RemeberObjFilterName = ObjFilterName;
            if (ObjAttrs != undefined) {
                RememberObjFilterAttributes = ObjAttrs;
            }
            if (ObjFilterID != undefined) {
                RememberObjFilterID = ObjFilterID;
            }
            if ($scope.FilterID.selectedFilterID != undefined) {
                FilterID = $scope.FilterID.selectedFilterID;
            } else {
                FilterID = 0;
            }
            if (RememberObjFilterID != 0 || RememberObjFilterAttributes.length != 0) {
                FilterID = RememberObjFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberObjFilterAttributes;
                if (RememberObjFilterAttributes.length > 0) {
                    setTimeout(function () {
                        $("#rootlevelfilter").trigger('RemeberApplyFilterReLoad', [10, RememberObjFilterAttributes])
                    }, 10);
                }
                if (RememberObjFilterID != 0) {
                    $scope.appliedfilter = RemeberObjFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = FilterID;
                }
            }
            $window.ObjectiveFilterName = '';
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = FilterID;
            Node.SortOrderColumn = $scope.sortOrder;
            Node.IsDesc = $scope.IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = CurrentuserId;
            Node.Level = $scope.Level;
            Node.IDArr = [];
            Node.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            ObjectivelistService.ObjectiveRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                    var PageIndex = 0;
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs;
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var colStatus = false;
                        for (var i = 0; i < listContent.length; i++) {
                            if (i != undefined) {
                                contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">";
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                        if (colStatus == false) {
                                            columnHtml += "<th>";

                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                        else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null || "          " ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }

                                treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                var status = "";
                                contentnHtml += "<td><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input type='checkbox' ><i class='checkbox " + status + "'></i></label></span></td>";
                                contentnHtml += "</tr>";
                                colStatus = true;
                            }

                        }
                        if ($("#ListColumn > thead tr").length < 3) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            $('#ListContainer').scrollTop(0);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                        if ($scope.PageSize > 1) {
                            $scope.LoadRootLevelObjective(1, 20, 20, $scope.FilterID.selectedFilterID, $scope.AppllyFilterObj.selectedattributes);
                        }
                    }
                } else {
                    $scope.noData = true;
                }
                $(window).AdjustHeightWidth();
            });
        }
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            var PageSize = 1;
            var itemsPerPage = 20;
            var height = 34 * ItemCnt;
            if (ItemCnt > itemsPerPage) {
                height = 34 * itemsPerPage;
                PageSize = Math.ceil(ItemCnt / itemsPerPage);
            }
            $scope.PageSize = PageSize;
            var TreeTemplate = '';
            var ListTemplate = '';
            for (var i = 0; i <= PageSize; i++) {
                if (i != undefined) {
                    TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list'></ul></li>"
                    ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"
                }
            }
            if (TreeTemplate.length > 0) {
                $("#ListContainer > table").html(ListTemplate);
                $("#treeHolder").html(TreeTemplate);
            }
        }
        $('#ListContainer').scroll(function () {
            $("#treeHolder").scrollTop($(this).scrollTop());
            var areaHeight = $('#ListContainer').height();
            var areaTop = $('#ListContainer').position().top;
            var top = $(this).position().top - areaTop;
            var height = $('#ListContainer table').height();
            $('tbody.pending', '#ListContainer').each(function () {
                var datapage = parseInt($(this).attr('data-page'));
                if ((($(this).height() * datapage) / 3) < $("#treeHolder").scrollTop()) {
                    //log("Page " + $(this).attr('data-page') + " is loaded");
                    $('#ListContainer > table tbody[data-page="' + $(this).attr('data-page') + '"]').removeClass('pending');
                    var StartRowNo = datapage * 20;
                    var MaxNoofRow = 20;
                    $(this).removeClass('pending');
                    $(this).removeAttr('style');
                    var filterattributes = [];
                    $('#treeHolder li[data-page="' + datapage + '"]').removeClass('pending');
                    $('#treeHolder li[data-page="' + datapage + '"]').removeAttr('style');
                    $scope.LoadRootLevelObjective(datapage, StartRowNo, MaxNoofRow, 0, filterattributes);
                }
            });
        });
        $("#rootLevelEntity").click(function (event) {
            $("#rootLevelEntity").trigger("onRootObjecitveCreation", [$scope.EntityTypeID])
        });
        $window.ListofEntityID = []
        $("#treeHolder").click(function (e) {
            var TargetControl = $(e.target);
            var id = $(e.target).attr("data-id");
            if (id != undefined) {
                ViewRootLevelEntity(id, e);
            }
            e.preventDefault();
        });
        $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function (e) {
            var status = this.checked;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $scope.IsSelectAllChecked = true;
                    $(this).next('i').addClass('checked');
                } else {
                    $scope.IsSelectAllChecked = false;
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $("#ViewSelect").click(function (event) {
            ViewRootLevelEntity(null, event)
        });

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        $("#ListContainer").click(function (e) {
            var IDList = new Array();
            IDList = GetRootLevelSelectedAll();
            if (IDList.length == $("#treeHolder").find(".nav-list").find("li").length) {
                $('#RootLevelSelectAll').next('i').addClass('checked');
            }
        });
        function ListCheckBoxitemClick() {
            $('#ListContainer > table > tbody input:checkbox').click(function () {
                $("#RootLevelSelectAll").next().removeClass('checked')
                $scope.IsSelectAllChecked = false;
            });
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            if ($scope.IsSelectAllChecked == false) {
                if (ID != null) {
                    $scope.selectedentitiesforcalender_mui.push(ID);
                    IDList.push(ID);
                    LoadDetailPart(event, IDList);
                } else {
                    IDList = GetRootLevelSelectedAll();
                    $scope.selectedentitiesforcalender_mui = IDList;
                    LoadDetailPart(event, IDList);
                }
            } else {
                $scope.IsSelectAllChecked = false;
                ObjectivelistService.GetAllEntityIds().then(function (Entityresult) {
                    if (Entityresult.Response != null) {
                        IDList = Entityresult.Response;
                        LoadSelectAllDetailPart(event, IDList);
                    }
                });
            }
        }

        function LoadDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                event.preventDefault();
                var localScope = $(event.target).scope();
                localScope.$apply(function () {
                    if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                        $location.path("mui/objective/detail/listview/" + TrackID);
                    } else {
                        $location.path("mui/objective/detail/ganttview/" + TrackID);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }
        ObjectivelistService.GetObjectiveEntityType(false).then(function (ResultPlansList) {
            if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
                $scope.ObjectiveCreationVar.IsToShowRootObjEntity = true;
                $scope.rootValues = ResultPlansList.Response;
            }
            else
                $scope.ObjectiveCreationVar.IsToShowRootObjEntity = false;

        });
        $scope.LoadAttributesForObjectiveCreation = function (rootID, rootCaption, objBaseType, objEntityTypeId, objUnitTypeId) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/objective/objectivecreation.html',
                controller: "mui.planningtool.objective.objectivecreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            rootID: rootID,
                            rootCaption: rootCaption,
                            objBaseType: objBaseType,
                            objEntityTypeId: objEntityTypeId,
                            objUnitTypeId: objUnitTypeId
                        };
                    }
                },
                scope: $scope,
                windowClass: 'popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        };
        function LoadSelectAllDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                if ($("#ViewSelect").attr('data-viewtype') == "ganttview") {
                    $location.path("mui/objective/detail/ganttview/" + TrackID);
                } else {
                    $location.path("mui/objective/detail/listview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.listCtrl']"));
        });
        setTimeout(function () { }, 0);
        $scope.visible = false;
    }
    app.controller("mui.planningtool.objective.listCtrl", ['$scope', '$timeout', '$compile', '$resource', '$location', '$window', '$cookies', '$cookieStore', '$translate', 'ObjectivelistService', '$modal', muiplanningtoolobjectivelistCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/filtersettings-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, PlanfiltersettingsService) {
        $scope.treeNodeSelectedHolderValues = [];
        $scope.appliedfilter = "No filter applied";
        var IsUpdate = 0;
        var IsSave = 0;
        $scope.deletefiltershow = false;
        $scope.FilterFields = {};
        var TypeID = $scope.EntityTypeID;
        $scope.visible = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.rememberAttrValue = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {}; 
        $scope.TreePricing = [];
        $scope.treeTexts = {};
        $scope.FilterDataXML = [];
        angular.element(document).ready(function () {
            $scope.$on('LoadRootLevelFilter', function (event, typeid, data) {
                if (data != undefined)
                    $scope.rememberAttrValue = data;
                $scope.FilterSettingsLoad();
            });

        });
        var rememberfilter = [];
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilterbutton = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsUpdate = 0;
            IsSave = 0;
        };
        $("#rootlevelfilter").on('ClearScope', function (event, TypeID) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
            }
        });
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearScopeModle = function () {
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationListWithTree, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            IsSave = 0;
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else if (TypeID == 5) {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            else if (TypeID == 10) {
                RememberObjFilterAttributes = [];
                RememberObjFilterID = 0;
                $cookieStore.remove('ObjAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('ObjFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('ObjFilterName' + parseInt($cookies['UserId'], 10));
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            $scope.deletefiltershow = false;
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
                var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 13
                });
                if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                    for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                        if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                    }
                }
            }
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattributes = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            if (TypeID == 6)
                $("#Div_list").trigger('ClearAndReApply');
            else if (TypeID == 5)
                $("#Div_listCC").trigger('ClearAndReApply');
            else if (TypeID == 10)
                $("#Div_Objlist").trigger('ClearAndReApply');

            if (TypeID == 6) {
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            } else if (TypeID == 5) {
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
            else if (TypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
        }
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            if ($scope.dyn_Cont.length == 0) {
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                $scope.EntityHierarchyTypesResult = [];
                $scope.dyn_Cont = '';
                $scope.Updatefilter = false;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $("#dynamic_Controls").html('');
                $scope.atributesRelationList = [];
                $scope.atributesRelationListWithTree = [];
                $scope.ngsaveasfilter = '';
                $scope.ngKeywordtext = '';
                var OptionFrom = 0;
                var IsKeyword = "false";
                var IsEntityType = "false";
                var CurrentActiveVersion = 0;
                var elementNode = 'Filter';
                var KeywordOptionsresponse;
                var CheckingPricicingAttr = [];
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    PlanfiltersettingsService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                        KeywordOptionsresponse = KeywordOptionsResult.Response;
                        OptionFrom = KeywordOptionsresponse.split(',')[0];
                        IsKeyword = KeywordOptionsresponse.split(',')[1];
                        IsEntityType = KeywordOptionsresponse.split(',')[3];
                        CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                        var IDList = new Array();
                        var filterattids = {};
                        filterattids.IDList = IDList;
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'Filter';
                        filterattids.OptionFrom = OptionFrom;
                        PlanfiltersettingsService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                    if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                        if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                            var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                            $scope.tagAllOptionsEntityStatus.data = [];
                                            if (EntityStatusOptions != null) {
                                                $.each(EntityStatusOptions, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.dyn_Cont += "<div class='control-group'>";
                                            $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                            $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Name  for ndata in  OntimeStatusLists \"></select></div>";
                                        } else {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption  for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                        if ($scope.atributesRelationList[i].AttributeId != 75 && $scope.atributesRelationList[i].AttributeId != 74) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"  id=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].LevelTreeNodes \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                        $scope.dyn_Cont += "<select multiple='multiple' ui-select2 data-placeholder='Select " + $scope.atributesRelationList[i].DisplayName + " options' ng-model='FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "'\ >";
                                        $scope.dyn_Cont += "<option ng-repeat='ndata in atributesRelationList[" + i + "].LevelTreeNodes' value='{{ndata.Id}}'>{{ndata.Caption}}</option>";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                        var k = $scope.TreePricing.length;
                                        var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                        });
                                        if (treecount12.length == 0) {
                                            var mm = $scope.atributesRelationList[i].AttributeId;
                                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                            $scope.FilterDataXML.push({
                                                "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                            });
                                        }
                                    }
                                }
                                if ($scope.EntityTypeID == 6 && IsEntityType == "True") {
                                    $scope.dyn_Cont += "<div class='control-group'>";
                                    $scope.dyn_Cont += "<span>EntityType : </span>";
                                    $scope.dyn_Cont += "<input id='ddlChildren' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' style='width: 200px;' />";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                    return e.AttributeTypeId == 7
                                })
                                for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                    if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                        $scope.treePreviewObj = {};
                                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        }
                                        $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                        $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_Cont += '</div></div>';
                                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                        $scope.dyn_Cont += '<div class="controls">';
                                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_Cont += '</div></div></div>';
                                    }
                                }
                                if (IsKeyword == "True") {
                                    $scope.dyn_Cont += "<div class=\"control-group\">";
                                    $scope.dyn_Cont += "<span>Keyword : </span>";
                                    $scope.dyn_Cont += "<input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'>";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                                $scope.Applyfilterbutton = false;
                                $scope.Updatefilter = false;
                                $scope.Deletefilter = false;
                                $scope.Savefilter = true;
                            }
                            RememberFilterOnReLoad($scope.rememberAttrValue);
                        });
                    });
                });
            }
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k].id,
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            if (orgLevel != undefined) {
                                for (var k = 0; k < orgLevel.length; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = [];
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    whereConditionData = [];
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    if (TypeID == 6)
                        $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    else if (TypeID == 5)
                        $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    else if (TypeID == 10)
                        $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);

                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = FilterData.FilterName;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = FilterData.FilterName;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                    else if (TypeID == 10) {
                        RememberObjFilterID = $scope.FilterID.selectedFilterID;
                        RememberObjFilterAttributes = $scope.filterattributes;
                        RemeberObjFilterName = FilterData.FilterName;
                        $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterId, typeId) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId) {
            $scope.Updatefilter = false;
            $scope.Applyfilterbutton = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.ClearScopeModle();
            $scope.FilterID.selectedFilterID = filterId;
            PlanfiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if ($scope.filterSettingValues.FilterValues.length > 0) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                    return e.AttributeId == parseInt(71);
                                })[0].EntityStatusOptionValues;
                                var seletedoptionvalue = [];
                                for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
									return e.AttributeId == parseInt(71);
                                }).length; sts++) {
                                    seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                        return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                    })[0]);
                                    if (seletedoptionvalue != null) {
                                        $.each(seletedoptionvalue, function (i, el) {
                                            $scope.tagAllOptionsEntityStatus.data.push({
                                                "id": el.ID,
                                                "text": el.StatusOptions,
                                                "ShortDescription": el.ShortDesc,
                                                "ColorCode": el.ColorCode
                                            });
                                        });
                                    }
                                    if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                }
                            } else {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        }
                    }
                }
                if ($scope.EntityTypeID == 6) {
                    PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                        if (filterSettingsValues.Response.EntityTypeID != "") {
                            for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                                if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                                    if ($.grep($scope.tagAllOptions.data, function (e) {
										return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                        return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0]);
                                }
                            }
                            $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                        }
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                    for (var k = 0; k < dataVal.length; k++) {
                        if ($scope.atributesRelationList[i].AttributeId == 71) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k].id,
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        } else {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            var FilterData = {};
            FilterData.FilterId = $scope.FilterID.selectedFilterID;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    if (TypeID == 6)
                        $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    else if (TypeID == 5)
                        $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    else if (TypeID == 10)
                        $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);

                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = $scope.appliedfilter;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = $scope.appliedfilter;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                    else if (TypeID == 10) {
                        RememberObjFilterID = $scope.FilterID.selectedFilterID;
                        RememberObjFilterAttributes = $scope.filterattributes;
                        RemeberObjFilterName = FilterData.FilterName;
                        $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $scope.ApplyFilter = function (FilterID, FilterName) {
            if (FilterID != undefined && FilterID != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                if ($scope.atributesRelationList.length > 0) {
                    if ($scope.atributesRelationList.length == 0) {
                        $scope.atributesRelationListtemp = $scope.atributesRelationList;
                    } else $scope.atributesRelationListtemp = $scope.atributesRelationList;
                }
            }
            if ($scope.atributesRelationList != undefined) if ($scope.atributesRelationList.length == 0) $scope.atributesRelationList = $scope.atributesRelationListtemp;
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var StartDate = '01/01/1990';
            var EndDate = '01/01/1990';
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId, e.ischecked == true;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            if ($scope.ngKeywordtext != '') whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': $scope.ngKeywordtext
                            });
                            else whereConditionData.push({
                                'AttributeID': nodeval.AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': ''
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    }
                }
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            } else {
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            $scope.FilterID.filterattributes = $scope.AppllyFilterObj.selectedattributes;
            if (TypeID == 6) {
                RememberPlanFilterID = FilterID;
                RememberPlanFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberPlanFilterName = $scope.appliedfilter;
                $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            } else if (TypeID == 5) {
                RememberCCFilterID = FilterID;
                RememberCCFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberCCFilterName = $scope.appliedfilter;
                $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
            else if (TypeID == 10) {
                RememberObjFilterID = FilterID;
                RememberObjFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberObjFilterName = $scope.appliedfilter;
                $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
            $scope.FilterID.filterattributes = [];
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            $scope.filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        PlanfiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4398.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                if (TypeID == 6)
                                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                else if (TypeID == 5)
                                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                else if (TypeID == 10)
                                    $("#Div_Objlist").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);

                                $scope.FilterID.selectedFilterID = 0;
                                $scope.FilterID = {
                                    selectedFilterID: 0
                                };
                                $scope.AppllyFilterObj = {};
                                $scope.ApplyFilter(0, 'No filter applied');
                                if (TypeID == 6) {
                                    $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 5) {
                                    $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 10) {
                                    $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 6 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            } else if ($scope.EntityTypeID == 5 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            }
            if (RememberPlanFilterAttributes.length != 0) {
                $scope.ddlParententitytypeId = [];
                $scope.filterSettingValues.FilterAttributes = RememberPlanFilterAttributes;
                for (var i = 0; i < $scope.filterSettingValues.FilterAttributes.length; i++) {
                    if ($scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 4) {
                        if ($scope.filterSettingValues.FilterAttributes[i].AttributeID == 71) {
                            var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeId == parseInt(71);
                            })[0].EntityStatusOptionValues;
                            var seletedoptionvalue = [];
                            for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterAttributes, function (e) {
								return e.AttributeID == parseInt(71);
                            }).length; sts++) {
                                seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                    return e.id == parseInt($scope.filterSettingValues.FilterAttributes[sts].SelectedValue);
                                })[0]);
                                if (seletedoptionvalue != null) {
                                    $.each(seletedoptionvalue, function (i, el) {
                                        $scope.tagAllOptionsEntityStatus.data.push({
                                            "id": el.ID,
                                            "text": el.StatusOptions,
                                            "ShortDescription": el.ShortDesc,
                                            "ColorCode": el.ColorCode
                                        });
                                    });
                                }
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] = seletedoptionvalue;
                            }
                        } else {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                        }
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                    }
                }
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.length == undefined) {
                    if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs != '0') {
                        if ($.grep($scope.tagAllOptions.data, function (e) {
							return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0]);
                    }
                } else {
                    for (var l = 0; l < $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',').length; l++) {
                        if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l] != '0') {
                            if ($.grep($scope.tagAllOptions.data, function (e) {
								return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0]);
                        }
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                $scope.ngKeywordtext = $scope.filterSettingValues.FilterAttributes[0].Keyword;
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.filtersettingsCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            if ($scope.treesrcdirec["Attr_" + attributeid].length != undefined) {
                for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                    branch.ischecked = false;
                    if (branch.Children.length > 0) {
                        ClearRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        };
    }
    app.controller("mui.planningtool.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'PlanfiltersettingsService', muiplanningtoolfiltersettingsCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/objective/objectivecreation-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectiveobjectivecreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, ObjectivecreationService, $modalInstance, params) {
        var model;
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }
        var ObjectivecreationTimout = {};
        $scope.StartDate = false;
        $scope.EndDate = false;
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.StartDate = false;
            $scope[model] = true;
            $scope.EndDate = true;
        };
        $scope.dynCalanderopen = function ($event, model1, isopen) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.StartDate = true;
            $scope.EndDate = false;
            model = model1;
        };
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        $scope.WizardPosition = 1;

        $scope.listAttriToAttriResult = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.ObjectiveCreationVar = {
            objEntityTypes: [],
            IsToShowRootObjEntity: false
        };

        $scope.Dropdown = {};
        $scope.treeSourcesObj = [];
        $scope.treeSources = {};
        $scope.ImageFileName = '';

        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.objectivecreationCtrl']"));
        });

        function ValidObjectiveTab1() {
            var detailTabValues = [
                ['#ObjectiveName', 'presence', 'Please Enter the Name'],
                ['#ObjectiveDescription', 'presence', 'Please Enter the Description']
            ];
            $("#ObjectiveTablMetadata").nod(detailTabValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp',
                'silentSubmit': 'true'
            });
        }

        function ValidObjectiveTab2() { }

        function ValidObjectiveTab3() {
            var DateCompare = function () {
                var st_date = $('#ObjectiveStatDaterange').val();
                var end_date = $('#ObjectiveEndDaterange').val();
                if (ConvertStringToDateByFormat(end_date, $scope.DefaultSettings.DateFormat) < ConvertStringToDateByFormat(st_date, $scope.DefaultSettings.DateFormat)) return false
                else return true
            }
            var fulfillTabValues = [
                ['#ObjectiveStatDaterange', 'presence', 'Please Enter the Start Date'],
                ['#ObjectiveEndDaterange', 'presence', 'Please Enter the End Date'],
                ['#ObjectiveEndDaterange', DateCompare, 'End Date Should be greater than Start Date'],
            ];
            $("#ObjectiveFulfillTab").nod(fulfillTabValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp3',
                'silentSubmit': 'true'
            });
        }

        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            var Attributetypename = '';
            var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
            });
            var ID = attrID.split("_");
            if (relationobj != undefined) {
                if (relationobj.length > 0) {
                    for (var i = 0; i <= relationobj.length - 1; i++) {
                        Attributetypename = '';
                        if (relationobj[i].AttributeTypeID == 3) {
                            Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 4) {
                            Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 6) {
                            Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        }
                        else if (relationobj[i].AttributeTypeID == 7) {
                            Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                        }
                        else if (relationobj[i].AttributeTypeID == 12) {
                            Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                        }

                        if ($scope.fields[Attributetypename] == undefined) {
                            if (attrTypeID == 1) {
                                $scope.fields["TextSingleLine_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 2) {
                                $scope.fields["TextMultiLine_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 3) {
                                $scope.fields["ListSingleSelection_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + ID[0]] = "";
                            }
                            else if (attrTypeID == 5) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            }
                            else if (attrTypeID == 6) {
                                $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                            }
                            else if (attrTypeID == 12) {
                                $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                            }
                            else if (attrTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                            }
                            continue;
                            //return false;
                        }
                        if (relationobj[i].AttributeTypeID == 4) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                            if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else if (relationobj[i].AttributeTypeID == 7) {
                            if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        else {
                            if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                try {
                                    $scope.DynamicAddValidation(parseInt(ID[0]));
                                }
                                catch (e) { };
                                return true;
                            }
                        }
                        //return false;
                    }
                    if (attrTypeID == 1) {
                        $scope.fields["TextSingleLine_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 2) {
                        $scope.fields["TextMultiLine_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 3) {
                        $scope.fields["ListSingleSelection_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + ID[0]] = "";
                    }
                    else if (attrTypeID == 5) {
                        $scope.fields["DatePart_" + ID[0]] = null;
                    }
                    else if (attrTypeID == 6) {
                        $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                    }
                    else if (attrTypeID == 12) {
                        $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                    }
                    else if (attrTypeID == 17) {
                        $scope.fields["ListTagwords_" + ID[0]] = [];
                    }
                    try {
                        $scope.DynamicRemoveValidation(parseInt(ID[0]));
                    }
                    catch (e) { };
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        };

        $scope.LoadObjectivePopuP = function () {
            $scope.Objective = {};
        }

        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttr").click();
        }

        $timeout(function () {
            StrartUpload_UploaderAttr();
        }, 100);

        function StrartUpload_UploaderAttr() {
            $('.moxie-shim').remove();
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }

                });

                uploader_Attr.bind('Init', function (up, params) {
                });

                uploader_Attr.init();


                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });

                uploader_Attr.bind('UploadProgress', function (up, file) {

                });

                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh(); // Reposition Flash/Silverlight
                });

                uploader_Attr.bind('FileUploaded', function (up, file, response) {

                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;

                    SaveFileDetails(file, response.response);

                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;

                });
            }
            else {
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }

        var uploaderAttrInfo = [];
        function SaveFileDetails(file, response) {
            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + resultArr[2];
            uploaderAttrInfo.push({ attrID: $scope.UploadAttributeId, attrFileName: ImageFileName });
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + ImageFileName);
        }
        //ObjectivecreationService.GetObjectiveEntityType(false).then(function (ResultPlansList) {
        //    if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
        //        $scope.ObjectiveCreationVar.IsToShowRootObjEntity = true;
        //        $scope.rootValues = ResultPlansList.Response;
        //    }
        //    else
        //        $scope.ObjectiveCreationVar.IsToShowRootObjEntity = false;

        //});

        //Validation part
        function GetValidationList() {
            ObjectivecreationService.GetValidationDationByEntitytype($scope.rootEntityID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }

                    //$("#ObjectiveMetadataModel").nod($scope.listValidationResult, {
                    //    'delay': 200,
                    //    'submitBtnSelector': '#btnTemp',
                    //    'silentSubmit': 'true'
                    //});
                }
            });
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetEntityTypeRoleAccess(rootID) {
            ObjectivecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        $scope.objectiveTypeChange = function () {
            if ($scope.Objective.ngObjectiveType == 1) {
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.numericqunatitativedisplay = true;
            }
            if ($scope.Objective.ngObjectiveType == 2) {
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = true;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
            }
            if ($scope.Objective.ngObjectiveType == 3) {
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.qualitativedisplay = true;

            }
            if ($scope.Objective.ngObjectiveType == 4) {
                $scope.ratingItems = [];
                $scope.ratingItems.push({ ObjRatingText: '' });
                $scope.RatingsHtml = "";
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = true;

                $scope.RatingsHtml += '<tr ng-repeat="item in ratingItems">';
                $scope.RatingsHtml += '<td><input class="nomargin" type="text" ng-model="item.ObjRatingText" maxlength="30" name="item.ObjRatingText" id="item.ObjRatingText"></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="AddRatings($index)" ng-model="AddRatings"><i class="icon-plus"></i></button></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="RemoveRatings(this)" ng-model="RemoveRatings"><i class="icon-remove"></i></button></td></tr>';
                $('#tblRatingRightbody').html($compile($scope.RatingsHtml)($scope));
                $scope.AddRatings1();

            }
            ValidObjectiveTab2($scope.Objective.ngObjectiveType);
        }

        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = 0;
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        var Childrens = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Childrens != undefined) {
                            var subleveloptions = [];
                            $.each(Childrens, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                    }
                    else if (attrType == 12) {

                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }

                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //    $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                        //        $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //    });
                        //}
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }

            }
            catch (e) { }
        }
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: null,
                    endDate: null,
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.LoadAttributesForObjectiveCreation = function (rootID, rootCaption, objBaseType, objEntityTypeId, objUnitTypeId) {
            //$("#ObjectiveMetadataModel").modal('show');
            $scope.Objective.objUnitTypeId = objUnitTypeId;
            $scope.Objective.objEntityTypeId = objEntityTypeId;
            $scope.Objective.ngObjectiveType = objBaseType;
            handleTextChangeEvents();
            $scope.treeCategory = [];

            //$scope.dyn_Cont = '';
            //$scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
            //$scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
            //$scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
            //$scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
            //$scope.dyn_Cont += '</div>';
            //$("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));

            $scope.UserimageNewTime = new Date.create().getTime().toString();
            $scope.fields = { usersID: '' };
            $scope.varfields = { CostcenterInfo: [] };
            $scope.dynamicEntityValuesHolder = {};
            $scope.fieldKeys = [];
            $scope.costcemtreObject = [];
            $scope.setFieldKeys = function () {
                var keys = [];

                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.OptionObj = {};
            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];
                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });
            }

            $scope.dropDownTreeListMultiSelectionObj = [];

            $scope.MemberLists = [];
            $scope.tempCount = 1;
            $scope.WizardPosition = 1;
            $("#btnWizardFinish").removeAttr('disabled');
            $('#ObjectiveWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#btnWizardFinish').hide();    //intially wizard Finish button is disabled
            $('#btnWizardNext').show();     //intially wizard Next button is enalbed
            $('#btnWizardPrev').hide();     //intially wizard Previous button is disabled

            $scope.status = true;
            //User autocomplete Start 
            var ownername = $cookies['Username'];
            var ownerid = $cookies['UserId'];
            $scope.OwnerName = ownername;
            $scope.OwnerID = ownerid;
            $scope.ownerEmail = $cookies['UserEmail'];
            $scope.AutoCompleteSelectedObj = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.contrls = '';
            var Version = 1;
            $scope.FinancialRequestObj = [];
            $scope.optionsLists = [];
            $scope.entityNameTyping = "";
            $scope.AttributeData = [];
            $scope.entityName = "";
            $scope.count = 1;
            $scope.treelevels = [];
            $scope.DropDownTreeOptionValues = {};
            $scope.tree = {};
            $scope.MultiSelecttree = {};
            $scope.Options = {};
            $scope.wizard = { newval: '' };
            $scope.costcemtreObject = [];
            $scope.dyn_Cont = '';
            $scope.rootEntityID = '';
            $scope.rootEntityCaption = rootCaption;
            $scope.rootEntityID = rootID;
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            $scope.items = [];
            $scope.Entityamountcurrencytypeitem = [];
            $scope.Dropdown = {};
            $scope.treeSourcesObj = [];
            $scope.treeSources = {};
            $scope.tagwordids = [];
            $scope.EntityStatusResult = [];
            $scope.treesrcdirec = {};


            //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
            ObjectivecreationService.GetAttributeToAttributeRelationsByIDForEntity($scope.rootEntityID).then(function (entityAttrToAttrRelation) {
                // var resourceAttributeToAttributeRelationByID = $resource('metadata/GetAttributeToAttributeRelationsByIDForEntity/:ID', { ID: $scope.rootEntityID }, { get: { method: 'GET' } });
                // var entityAttrToAttrRelation = resourceAttributeToAttributeRelationByID.get({ ID: $scope.rootEntityID }, function () {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });

            //-----------------------------------------------------------
            ObjectivecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
                //var GetOwner = $resource('user/GetUserById/:ID', { ID: parseInt($scope.OwnerID, 10) }, { get: { method: 'GET' } });
                //var Getowner = GetOwner.get(function () {
                ObjectivecreationService.GetEntityStatus(rootID, false, 0).then(function (getentitytypestatus) {
                    $scope.EntityStatusResult = getentitytypestatus.Response;
                    $scope.owner = Getowner.Response;
                    $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
                    $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
                    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
                    ObjectivecreationService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                        //var resourceEntityTypeAttributeRelationByID = $resource('metadata/GetEntityTypeAttributeRelationWithLevelsByID/:ID/:ParentID', { ID: rootID, ParentID: 0 }, { get: { method: 'GET' } });
                        //var entityAttributesRelation = resourceEntityTypeAttributeRelationByID.get({ ID: rootID }, function () {
                        $scope.atributesRelationList = entityAttributesRelation.Response;
                        for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                            if ($scope.atributesRelationList[i].IsHelptextEnabled == true) {
                                var attribteHelptext = $scope.atributesRelationList[i].HelptextDecsription == null ? "-" : $scope.atributesRelationList[i].HelptextDecsription.trim() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription;
                                var qtip = 'my-qtip2 qtip-content="' + attribteHelptext + '"';
                            }
                            else
                                var qtip = "";
                            if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                if ($scope.atributesRelationList[i].AttributeID != 70) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;

                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    } else {
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                    }
                                    $scope.setFieldKeys();
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].calenderAttributeID] = new Date.create();
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                // $scope.maxDate.setDate($scope.maxDate.getDate() + $scope.MaxValue);
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\"  ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.setFieldKeys();

                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].IsSpecial == true) {
                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                        $scope.setFieldKeys();
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                                else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                                }
                                else {
                                    $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                    $scope.setoptions();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                            }

                            else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };

                                    if (j == 0) {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;

                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                    else {
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"> ";
                                        $scope.dyn_Cont += "<option ng-repeat=\"item in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{item.id}}\">{{item.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                    }
                                    $scope.setFieldKeys();
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) { return e.Id == defaultmultiselectvalue[j]; })[0].Id);
                                    }
                                }
                                else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                                $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                                $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                                $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i ng-show=\"$first==true\" my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                                $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";

                                //$scope.item.startDate = null;
                                //$scope.item.endDate = null;

                                $scope.setFieldKeys();

                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                                $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                                $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                                $scope.setoptions();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                    treeTextVisbileflag = false;
                                    if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                    }
                                    else
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                }
                                else {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                                }
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                                $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                                $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                                $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                                $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                $scope.dyn_Cont += '</div><span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                $scope.dyn_Cont += '</div></div>';
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                                $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                var helptextdesc = $scope.atributesRelationList[i].HelptextDecsription.toString() == "" ? "-" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group \" drowdowntreepercentagemultiselection data-purpose='entity' data-helptextdescription=" + helptextdesc + " data-ishelptextenabled=" + $scope.atributesRelationList[i].IsHelptextEnabled.toString() + " data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";


                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                                for (var j = 0; j < totLevelCnt1; j++) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                        multiple: false,
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        }
                                    };
                                    if (totLevelCnt1 == 1) {

                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\"  ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        $scope.setFieldKeys();
                                    }
                                    else {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                        if (j == 0) {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                            $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                            $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                            $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                        } else {
                                            //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                            $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                            }
                                            else {
                                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";

                                            }
                                        }
                                    }
                                    $scope.setFieldKeys();
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                                $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.setoptions();
                                $scope.tempscope = [];
                                var helptext = $scope.atributesRelationList[i].HelptextDecsription == "" ? "" : $scope.atributesRelationList[i].HelptextDecsription.toString();
                                //$scope.dyn_Cont += "<div><directive-tagwords item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-caption=" + $scope.atributesRelationList[i].AttributeCaption + "></directive-tagwords></div></div>";
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                                $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                                $scope.dyn_Cont += "<div class=\"controls\">";
                                $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\" item-isenable=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" item-attributehelptext=\"" + helptext.toString() + "\"  ></directive-tagwords>";
                                $scope.dyn_Cont += "</div></div>";
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                }
                                else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                }
                                else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                var param1 = new Date.create();

                                var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.setFieldKeys();
                                $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.setoptions();
                                $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group"><label class="control-label"';
                                $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                                $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                                if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                                    $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                                }
                                $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                                $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl">';
                                $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                                $scope.dyn_Cont += '<span ng-show=\"' + $scope.atributesRelationList[i].IsHelptextEnabled + '\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"' + attribteHelptext + '\" class=\"icon-info-sign\"></i></span></div></div>';
                                $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                $scope.setFieldKeys();
                            }
                            else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $translate.instant('LanguageContents.Res_5613.Caption') + "</label><div class=\"controls bgDropdown\"><select ui-select2=\"select2Config\" id=\"OverAllEntityStats\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"OverAllEntityStatus\" data-placeholder=\"Entity Status\">";
                                $scope.dyn_Cont += "<option value=\"0\"> Select " + $translate.instant('LanguageContents.Res_5613.Caption') + "</option>";
                                $scope.dyn_Cont += "<option ng-repeat=\"Aitem in EntityStatusResult\" value=\"{{Aitem.ID}}\">{{Aitem.StatusOptions}}</option>";
                                $scope.dyn_Cont += "</select><span ng-show=\"" + $scope.atributesRelationList[i].IsHelptextEnabled + "\" class=\"signInfo margin-left20x\"><i my-qtip2 qtip-content=\"" + attribteHelptext + "\" class=\"icon-info-sign\"></i></span></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = 0;

                                $scope.select2Config = {
                                    formatResult: $scope.overallstatusformat,
                                    formatSelection: $scope.overallstatusformat
                                };
                            }
                            $scope.setFieldKeys();
                            if ($scope.atributesRelationList[i].IsReadOnly == true) {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        }
                        $scope.setFieldKeys();
                        $scope.dyn_Cont += '<input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
                        $("#ObjectiveTablMetadata").html(
                                     $compile($scope.dyn_Cont)($scope));

                        var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                        $scope.mindec = "";
                        if (mar.length == 1) {
                            $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "1" });
                        }
                        if (mar.length != 1) {
                            if (mar.length < 5) {
                                $scope.mindec = "0.";

                                for (i = 0; i < mar.length; i++) {
                                    $scope.mindec = $scope.mindec + "0";
                                }
                                $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: $scope.mindec });
                            }
                            else {
                                $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                            }
                        }

                        $("[id^='MTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                        $("[id^='MTextSingleLine_']").keydown(function (event) {
                            if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                                bootbox.alert($translate.instant('Please enter only Number'));
                                event.preventDefault(); //stop character from entering input
                            }
                        });
                        setTimeout(function () { $('[id^=TextSingleLine]:enabled:visible:first').focus().select() }, 1000);
                        $("#ObjectiveTablMetadata").scrollTop(0);
                        $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];

                        GetValidationList();
                        $("#ObjectiveTablMetadata").addClass('notvalidate');

                    });
                });
            });

            $scope.objectiveTypeChange();
            GetEntityTypeRoleAccess(rootID);

            //--Objective Units--
            ObjectivecreationService.GetObjectiveUnitsOptionValues(objUnitTypeId).then(function (ObjectiveUnitsResult) {
                $scope.ObjectiveUnitsData = ObjectiveUnitsResult.Response;
            });

            if (objUnitTypeId == 1)
                $scope.UnitLabel = 'Currency';
            else if (objUnitTypeId == 2)
                $scope.UnitLabel = 'Unit';

        }

        $scope.treePreviewObj = {};

        $("#rootLevelEntity").on("onRootObjecitveCreation", function (event, id) {
            $scope.WizardPosition = 1;
            $("#btnWizardFinish").removeAttr('disabled');
            $('#ObjectiveWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#btnWizardFinish').hide();
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $scope.Objective.ngObjectiveName = '';
            $scope.Objective.ngObjectiveDescription = '';
            $scope.Objective.ngObjectiveStatus = 1;
            $scope.Objective.ngObjectiveNumericUnits = 0;
            $scope.Objective.ngObjectiveNumericGlobalbaselineCheck = false;
            $scope.Objective.ngObjectiveNumericGlobalbaseline = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNumericGlobaltargetCheck = false;
            $scope.Objective.ngObjectiveNumericGlobaltarget = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNonNumericGlobalbaselineCheck = false;
            $scope.Objective.ngObjectiveNonNumericGlobalbaseline = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNonNumericGlobaltargetCheck = false;
            $scope.Objective.ngObjectiveNonNumericGlobaltarget = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.Objective.ngObjectiveNumericEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveNumericDescriptionInstructions = '';
            $scope.Objective.ngObjectivenonNumericUnits = 0;
            $scope.Objective.ngObjectivenonNumericEnableCommentsCheck = false;
            $scope.Objective.ngObjectivenonNumericDescriptionInstructions = '';
            $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions = '';
            $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveRatingDescriptionInstructions = '';
            $scope.Objective.ngObjectiveRatingEnableCommentsCheck = false;
            $scope.Objective.ngObjectiveStatDaterange = '';
            $scope.Objective.ngObjectiveEndDaterange = '';
            $scope.Objective.ngObjectiveDaterule = 1;
            $scope.Objective.ngObjectiveMandatoryCheck = false;
            $scope.Objective.ngObjectiveType = 1;
            $scope.numglobalchecked = false;
            $scope.numtargetchecked = false;
            $scope.nonglobalchecked = false;
            $scope.nontargetchecked = false;
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
        });
        var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
        $('#btnWizardFinish').hide();
        $('#btnWizardNext').show();
        $('#btnWizardPrev').hide();
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.UserimageNewTime = new Date.create().getTime().toString();
        $scope.changeTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            } else {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === 2) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
        }
        $scope.changeObjectiveTab2 = function (e) {
            $timeout(function () {
                $("#btnTemp").click();
            }, 100);
            $("#ObjectiveTablMetadata").removeClass('notvalidate');
            if ($("#ObjectiveTablMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 2) {
                if ($scope.Objective.ngObjectiveType == 1) {
                    $timeout(function () {
                        $("#btnnumeric").click();
                    }, 100);
                    if ($("#Numericform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 2) {
                    $timeout(function () {
                        $("#btnnonnumeric").click();
                    }, 100);
                    if ($("#NonNumericform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 3) {
                    $timeout(function () {
                        $("#btnqualitative").click();
                    }, 100);
                    if ($("#Qualitativeform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 4) {
                    $timeout(function () {
                        $("#btnrating").click();
                    }, 100);
                    if ($("#Ratingform .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 3) {
                if ($(e.target).attr("data-target") == "#step2") { } else {
                    $timeout(function () {
                        $("#btnTemp3").click();
                    }, 100);
                    if ($("#ObjectiveFulfillTab .error").length > 0) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        return false;
                    }
                }
            }
            if ($('#ObjectiveWizard').wizard('selectedItem').step == 3) {
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    if ($('#ObjEntityType' + index).val() == "0" || $('#ObjEntityType' + index).val() == null) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        return false;
                    }
                });
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#ObjectiveWizard ul.steps").find("li").addClass("complete");
            $("#ObjectiveWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#ObjectiveWizard').wizard('previous');
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            $scope.WizardPosition = currentWizardStep;
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            } else {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }
        $scope.status = true;
        $("#Div_Objlist").on('loadObjectiveUIControls', function (event) {
            ValidObjectiveTab1();
            ValidObjectiveTab2();
            ValidObjectiveTab3();
            handleTextChangeEvents();
        });
        $scope.cancelmodel = function () {
            $timeout(function () {
                if ($('#ObjectiveTablMetadata').nod() != undefined) $('#ObjectiveTablMetadata').nod().destroy();
                if ($('#NonNumericform').nod() != undefined) $('#NonNumericform').nod().destroy();
                if ($('#ObjectiveFulfillTab').nod() != undefined) $('#ObjectiveFulfillTab').nod().destroy();
            }, 100);
            $scope.closeObjectivecreationpopup();
        }
        $scope.changenexttab = function () {
            $("#btnTemp").click();
            $("#ObjectiveTablMetadata").removeClass('notvalidate');
            if ($("#ObjectiveTablMetadata .error").length > 0) {
                return false;
            }
            if ($scope.WizardPosition == 2) {
                if ($scope.Objective.ngObjectiveType == 1) {
                    setTimeout(function () { $("#btnnumeric").click(); }, 50);
                    if ($("#Numericform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 2) {
                    setTimeout(function () { $("#btnnonnumeric").click(); }, 50);
                    if ($("#NonNumericform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 3) {
                    setTimeout(function () { $("#btnqualitative").click(); }, 50);
                    if ($("#Qualitativeform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == 4) {
                    setTimeout(function () { $("#btnrating").click(); }, 50);
                    if ($scope.ratingItems[$scope.ratingItems.length - 1].ObjRatingText == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                        return false;
                    }
                    if ($("#Ratingform .error").length > 0) {
                        return false;
                    }
                }
                if ($scope.Objective.ngObjectiveType == "4") {
                    if ($scope.ratingItems.length > 0) {
                        for (var i = 0; i < 2; i++) {
                            if ($scope.ratingItems[i].ObjRatingText == "" || $scope.ratingItems[i].ObjRatingText == undefined) {
                                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                                return false;
                            }
                        }
                    }
                }
            }
            if ($scope.WizardPosition == 3) {
                setTimeout(function () { $("#btnTemp3").click(); }, 50);
                var st_date = $('#ObjectiveStatDaterange').val();
                var end_date = $('#ObjectiveEndDaterange').val();
                if (ConvertStringToDateByFormat(end_date, $scope.DefaultSettings.DateFormat) < ConvertStringToDateByFormat(st_date, $scope.DefaultSettings.DateFormat)) return false
                if ($("#ObjectiveFulfillTab .error").length > 0) {
                    return false;
                }
                $scope.ConditionDataArr = [];
                var EntttyTypeArr = new Array();
                var attriArreArr = new Array();
                var optionArr = [];
                var tempval = false;
                $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                    if (index > 0 && $('#ObjConditionType' + index).val() == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_4617.Caption'));
                        tempval = true;
                        return false;
                    }
                    if ($('#ObjEntityType' + index).val() == "0" || $('#ObjEntityType' + index).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                        tempval = true;
                        return false;
                    } else if ($('#ObjEntityAttributes' + index).val() != "0" && $('#ObjEntityAttributes' + index).val() != null) {
                        if ($('#ObjAttributeOptions' + index).val() == null) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                            tempval = true;
                            return false;
                        }
                    }
                });
            }
            if (tempval == true) return false;
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#ObjectiveWizard').wizard('next', '');
            var currentWizardStep = $('#ObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#ObjectiveWizard').wizard('totnumsteps').totstep;
            $scope.WizardPosition = currentWizardStep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            } else {
                $('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }
        $scope.ObjectiveUnitsData = [];
        $scope.FulfillmentEntityTypes = [];
        $scope.FulfillmentAttributes = [];
        $scope.FulfillmentAttributeOptions = [];
        $scope.fieldKeys = [];
        $scope.fields = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.creatObjectivefullfilmetadataHtml = '';
        $scope.ObjectiveEntityTypeHtml = '';
        $scope.ObjectiveFullfilAttributes = [];
        $scope.FulfillmentAttributeOptions = {};
        $scope.ObjectiveFullfilOptionsHtml = '';
        $scope.AddMulticonditionHtml = '';
        $scope.RatingsHtml = '';
        $scope.ObjectiveFulfillConditions = [];
        $scope.Objective = {
            ngObjectiveName: '',
            ngObjectiveDescription: '',
            ngObjectiveStatus: 1,
            ngObjectiveNumericUnits: 0,
            ngObjectiveNumericGlobalbaselineCheck: false,
            ngObjectiveNumericGlobalbaseline: 0,
            ngObjectiveNumericGlobaltargetCheck: false,
            ngObjectiveNumericGlobaltarget: 0,
            ngObjectiveNonNumericGlobalbaselineCheck: false,
            ngObjectiveNonNumericGlobalbaseline: 0,
            ngObjectiveNonNumericGlobaltargetCheck: false,
            ngObjectiveNonNumericGlobaltarget: 0,
            ngObjectiveNumericEnableCommentsCheck: false,
            ngObjectiveNumericDescriptionInstructions: '',
            ngObjectivenonNumericUnits: 0,
            ngObjectivenonNumericEnableCommentsCheck: false,
            ngObjectivenonNumericDescriptionInstructions: '',
            ngObjectiveQulaitativeDescriptionInstructions: '',
            ngObjectiveQulaitativeEnableCommentsCheck: false,
            ngObjectiveRatingDescriptionInstructions: '',
            ngObjectiveRatingEnableCommentsCheck: false,
            ngObjectiveStatDaterange: '',
            ngObjectiveEndDaterange: '',
            ngObjectiveDaterule: 1,
            ngObjectiveMandatoryCheck: false,
            ngObjectiveType: 1
        }
        $scope.rowCount = 0;
        $scope.ratingRowsCount = 0;
        $scope.numericObjectiveArr = [];
        var objOwnername = $cookies['Username'];
        var objOwnerid = $cookies['UserId'];
        $scope.OwnerName = objOwnername;
        $scope.OwnerID = objOwnerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.MemberLists = [];
        $scope.AutoCompleteSelectedObj = [];
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = [];
        $scope.OwnerList = [];
        $scope.owner = {};
        ObjectivecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({
                "Roleid": 1,
                "RoleName": "Owner",
                "UserEmail": $scope.ownerEmail,
                "DepartmentName": $scope.owner.Designation,
                "Title": $scope.owner.Title,
                "Userid": parseInt($scope.OwnerID, 10),
                "UserName": $scope.OwnerName,
                "IsInherited": '0',
                "InheritedFromEntityid": '0',
                "QuickInfo1": $scope.owner.QuickInfo1,
                "QuickInfo2": $scope.owner.QuickInfo2
            });
        })
        $scope.contrls = '';
        //ObjectivecreationService.GetEntityTypeRoleAccess(10).then(function (role) {
        //    $scope.Roles = role.Response;
        //});

        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10);
            });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.fields['userRoles'])
                })[0];
                if (membervalues != undefined) {
                    $scope.MemberLists.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedObj[0].Email,
                        "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation,
                        "Title": $scope.AutoCompleteSelectedObj[0].Title,
                        "Roleid": parseInt(membervalues.ID, 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10),
                        "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2
                    });
                    $scope.count = $scope.count + 1;
                }
                $scope.AutoCompleteSelectedObj = [];
                $scope.fields.usersID = '';
            } else bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
        };
        $scope.deleteOptions = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                $timeout(function () {
                    if (result) {
                        $scope.MemberLists.splice($.inArray(item, $scope.MemberLists), 1);
                    }
                }, 100);
            });
        };
        $scope.ratingItems = [];
        $scope.ratingItems.push({
            ObjRatingText: ''
        });
        $scope.checklistratingIndex = 0;
        $scope.AddRatings = function (index) {
            if ($scope.ratingItems[index].ObjRatingText == "" || $.grep($scope.ratingItems, function (e) { return e.ObjRatingText == "" }).length > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                return false;
            }
            if ($scope.checklistratingIndex == -1) {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.ratingItems.push({ ObjRatingText: '' });
            }
            else {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.checklistratingIndex = parseInt(index) + 1;
                $scope.ratingItems.splice(parseInt($scope.checklistratingIndex), 0, { ObjRatingText: '' });
            }
        }
        $scope.AddRatings1 = function () {
            $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
        };
        $scope.RemoveRatings = function (item) {
            if ($scope.ratingRowsCount < 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_1917.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount - 1;
                $scope.ratingItems.splice($.inArray(item.item, $scope.ratingItems), 1);
            }
        };
        $scope.numericqunatitativedisplay = true;
        $scope.Objective.ngObjectiveOwner = $scope.OwnerName;

        if ($('#ObjectiveNumericGlobalbaseline').is(':checked')) {
            $('#ObjectiveNumericGlobaltarget').attr("disabled", false);
        }
        $scope.numglobalchecked = false;
        $scope.numtargetchecked = false;
        $scope.nonglobalchecked = false;
        $scope.nontargetchecked = false;
        $scope.objectiveItems = [];
        $scope.objectiveItems.push({
            ObjEntityType: [],
            ObjEntityAttributes: [],
            ObjAttributeOptionns: [],
            ObjCondition: 0
        });
        $scope.addMultiPleConditions = function () {
            if ($scope.objectiveItems[$scope.rowCount].ObjEntityType.length == 0 || $scope.objectiveItems[$scope.rowCount].ObjEntityAttributes.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1981.Caption'));
                return false;
            } else {
                $scope.rowCount = $scope.rowCount + 1;
                $scope.objectiveItems.push({
                    ObjEntityType: [],
                    ObjEntityAttributes: [],
                    ObjAttributeOptionns: [],
                    ObjCondition: []
                });
                $scope.FulfillmentAttributes = [];
                $scope.FulfillmentAttributeOptions = {};
            }
        };
        $scope.objectiveItemsduplicate = [];
        $scope.objectiveItemsduplicate.push({
            ObjCondition: []
        });
        $scope.addMultiPleConditionsduplicate = function (item) {
            $scope.objectiveItemsduplicate.push({
                ObjCondition: []
            });
        };
        $scope.removeFulCondition = function (item) {
            if ($scope.rowCount > 0) {
                $scope.rowCount = $scope.rowCount - 1;
                $scope.objectiveItems.splice($.inArray(item, $scope.items), 1);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                return false;
            }
        };
        FilltFulfillmentEntityTypes("ObjEntityType0");

        function FilltFulfillmentEntityTypes(ControllerID) {
            ObjectivecreationService.GetFulfillmentEntityTypes().then(function (fulFillmentEntityTypesObj) {
                $scope.FulfillmentEntityTypes = fulFillmentEntityTypesObj.Response;
                var objentities = fulFillmentEntityTypesObj.Response;
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option></option>').val(item.Id).html(item.Caption))
                });
            });
        }
        $scope.LoadObjectiveFulfillmentMetadatatbody=function(event){
        //$("#ObjectiveFulfillmentMetadatatbody").click(function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') {
                ResetDropDown("ObjEntityAttributes" + currentUniqueId, currentUniqueId);
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                ObjectivecreationService.GetFulfillmentAttribute(TargetControl.val()).then(function (fulFillmentAttributesObj) {
                    if (fulFillmentAttributesObj.Response.length > 0) {
                        FillEntityTypeAttributes("ObjEntityAttributes" + currentUniqueId, fulFillmentAttributesObj.Response);
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Attributes') {
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                ObjectivecreationService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillEntityTypeAttributes("ObjAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response)
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Equals') { } else if (TargetControl.attr('data-role') == 'Add') {
                if (currentUniqueId > 0 && TargetControl.parents().find('#ObjConditionType' + currentUniqueId).val() == "") {
                    bootbox.alert($scope.LanguageContents_4617);
                    return false;
                }
                if (TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjEntityType' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                    return false;
                }
                if ($('#ObjEntityAttributes' + currentUniqueId).val() != "0" && $('#ObjEntityAttributes' + currentUniqueId).val() != null) {
                    if ($('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }
                var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='ObjectiveCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                html += "<select ui-select2 id='ObjConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "'>";
                html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + "-- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_813.Caption') + " </option>";
                html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_813.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='dummymodule_ObjEntityType_" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0' ng-selected='true'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes'>";
                html += "<option value='0'>--  " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "' data-role='Equals'>";
                html += "<option  value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("ObjEntityType" + UniqueId);
            } else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                    return false;
                }
                $("#container" + currentUniqueId).remove();
            }
        };

        function FillEntityTypes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentEntityTypes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                });
            }
        }

        function FillEntityTypeAttributes(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(ControllerID, currentUniqueId) {
            $('#' + ControllerID).html("");
            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#' + ControllerID).select2("val", 0);
            $('#ObjAttributeOptions' + currentUniqueId).html("");
            $('#ObjAttributeOptions' + currentUniqueId).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#ObjAttributeOptions' + currentUniqueId).select2("val", "");
        }
        $scope.ObjDatavalue = {};
        $scope.objectiveConditionsValues = {};
        $scope.createNewObjective = function (event) {
            // $("#btnWizardFinish").click(function (event) {
            $("#btnWizardFinish").attr('disabled', 'disabled');
            if (uploaderAttrInfo.length > 0) {

                ObjectivecreationService.copyuploadedImage(uploaderAttrInfo[0].attrFileName).then(function (newuplAttrName) {
                    var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfo[0].attrID });
                    var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                    $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                    saveobjectiveEntity();
                })
            }
            else {
                saveobjectiveEntity();
            }
        };
        function saveobjectiveEntity() {
            collectionObjectiveCondition();
            $scope.ObjDatavalue = $scope.ConditionDataArr;

            //-------------> DETAIL BLOCK DATA <----------
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                        else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                                else {
                                    $scope.AttributeData.push({

                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]],
                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1)
                                        valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                    else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                            var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }

                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({ amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    if ($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        var dte = new Date.create($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID]);
                        var tdte = ConvertDateToString(dte);

                        //  var dateval = ConvertDateFromStringToString($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID].toString())
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": tdte,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });

                }


                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {

                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null) {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                        }
                        else {
                            //$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID].toString('MM/dd/yyyy');
                            MyDateString = "";
                        }
                    }
                    else {
                        MyDateString = "";
                    }

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].attrFileID == undefined || $scope.atributesRelationList[i].attrFileID == null) {
                        $scope.atributesRelationList[i].attrFileID = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.atributesRelationList[i].attrFileID,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 33) {
                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
            }


            //-------------> OBJECTIVE TYPE DATA <--------------
            var ObjectiveEnttiy = {};
            ObjectiveEnttiy.ObjectiveTypeID = $scope.Objective.ngObjectiveType;
            ObjectiveEnttiy.ObjectiveName = $scope.entityName;
            ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveDescription;
            ObjectiveEnttiy.ObjectiveStatus = parseInt($scope.Objective.ngObjectiveStatus, 10);
            ObjectiveEnttiy.ObjectiveStartDate = $scope.Objective.ngObjectiveStatDaterange.toString('MM/dd/yyyy');
            ObjectiveEnttiy.ObjectiveEndDate = $scope.Objective.ngObjectiveEndDaterange.toString('MM/dd/yyyy');
            ObjectiveEnttiy.ObjectiveDateRule = $scope.Objective.ngObjectiveDaterule;
            $scope.CurrentGlobalBaseLine = 0;
            $scope.CurrentGlobalTarget = 0;
            if ($scope.Objective.ngObjectiveNumericGlobalbaseline != 0) $scope.CurrentGlobalBaseLine = $scope.Objective.ngObjectiveNumericGlobalbaseline.replace(/\s/g, '');
            else $scope.CurrentGlobalBaseLine = 0;
            if ($scope.Objective.ngObjectiveNumericGlobaltarget != 0) {
                $scope.CurrentGlobalTarget = $scope.Objective.ngObjectiveNumericGlobaltarget.replace(/\s/g, '');
            }
            if ($scope.Objective.ngObjectiveType == 1) {
                ObjectiveEnttiy.ObjectiveNumericData = [{
                    "UnitID": $scope.Objective.objUnitTypeId == '' ? 0 : parseInt($scope.Objective.objUnitTypeId, 10),
                    "GlobalBaseLine": $scope.CurrentGlobalBaseLine,
                    "GlobalTarget": $scope.CurrentGlobalTarget,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveNumericEnableCommentsCheck,
                    "Instruction": $scope.Objective.ngObjectiveNumericDescriptionInstructions,
                    "UnitValue": $scope.Objective.ngObjectiveNumericUnits == '' ? 0 : parseInt($scope.Objective.ngObjectiveNumericUnits, 10)
                }];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveNumericDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 2) {
                $scope.CurrentNonNumericGlobalBaseLine = 0;
                $scope.CurrentNonNumericGlobalTarget = 0;
                if ($scope.Objective.ngObjectiveNonNumericGlobalbaseline != 0) $scope.CurrentNonNumericGlobalBaseLine = $scope.Objective.ngObjectiveNonNumericGlobalbaseline.replace(/\s/g, '');
                else $scope.CurrentNonNumericGlobalBaseLine = 0;
                if ($scope.Objective.ngObjectiveNonNumericGlobaltarget != 0) {
                    $scope.CurrentNonNumericGlobalTarget = $scope.Objective.ngObjectiveNonNumericGlobaltarget.replace(/\s/g, '');
                }
                ObjectiveEnttiy.ObjectiveNonNumericData = [{
                    "UnitID": $scope.Objective.objUnitTypeId == '' ? 0 : parseInt($scope.Objective.objUnitTypeId, 10),
                    "GlobalBaseLine": $scope.CurrentNonNumericGlobalBaseLine,
                    "GlobalTarget": $scope.CurrentNonNumericGlobalTarget,
                    "IsEnableFeedback": $scope.Objective.ngObjectivenonNumericEnableCommentsCheck,
                    "Instruction": $scope.Objective.ngObjectivenonNumericDescriptionInstructions,
                    "UnitValue": $scope.Objective.ngObjectivenonNumericUnits == '' ? 0 : parseInt($scope.Objective.ngObjectivenonNumericUnits, 10)
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectivenonNumericDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 3) {
                ObjectiveEnttiy.ObjectiveQualitativeData = [{
                    "Instruction": $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveQulaitativeEnableCommentsCheck
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveRatingData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveQulaitativeDescriptionInstructions;
            }
            if ($scope.Objective.ngObjectiveType == 4) {
                var ratingCollectionObject = [];
                if ($scope.ratingItems.length > 0) {
                    for (var i = 0; i < $scope.ratingItems.length; i++) {
                        ratingCollectionObject.push($scope.ratingItems[i].ObjRatingText);
                    }
                }
                ObjectiveEnttiy.ObjectiveRatingData = [{
                    "Instruction": $scope.Objective.ngObjectiveRatingDescriptionInstructions,
                    "IsEnableFeedback": $scope.Objective.ngObjectiveRatingEnableCommentsCheck,
                    "Ratings": ratingCollectionObject
                }];
                ObjectiveEnttiy.ObjectiveNumericData = [];
                ObjectiveEnttiy.ObjectiveNonNumericData = [];
                ObjectiveEnttiy.ObjectiveQualitativeData = [];
                ObjectiveEnttiy.ObjectiveDescription = $scope.Objective.ngObjectiveRatingDescriptionInstructions;
            }

            //---------------> OBJECTIVE FULLFILLMENT DATA <------------            
            ObjectiveEnttiy.ObjectiveConditonData = [{
                "StartDate": $scope.Objective.ngObjectiveStatDaterange.toString('MM/dd/yyyy'),
                "EndDate": $scope.Objective.ngObjectiveEndDaterange.toString('MM/dd/yyyy'),
                "DateRule": parseInt($scope.Objective.ngObjectiveDaterule, 10),
                "IsMandatory": $scope.Objective.ngObjectiveMandatoryCheck,
            }]
            if ($scope.ObjDatavalue[0].ObjEntityType != "0") {
                ObjectiveEnttiy.ObjectiveFulfillConditonValueData = [{
                    "ObjectiveFulfillValues": $scope.ObjDatavalue
                }]
            } else if ($scope.ObjDatavalue[0].ObjEntityType == "0") {
                ObjectiveEnttiy.ObjectiveFulfillConditonValueData = []
            }
            ObjectiveEnttiy.ObjectiveMembers = $scope.MemberLists;
            ObjectiveEnttiy.ObjectiveOwner = $scope.OwnerList;
            ObjectiveEnttiy.Periods = [];
            $scope.StartEndDate = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.toString().length != 0 && $scope.items[m].endDate.toString().length != null) {
                        $scope.StartEndDate.push({
                            startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                            endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                            comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
            }
            ObjectiveEnttiy.Periods.push($scope.StartEndDate);
            ObjectiveEnttiy.AttributeData = $scope.AttributeData;
            ObjectiveEnttiy.objEntityTypeId = $scope.Objective.objEntityTypeId;
            ObjectivecreationService.CreateObjective(ObjectiveEnttiy).then(function (objetiveCreationResult) {
                if (objetiveCreationResult.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4538.Caption'));
                    var IDList = new Array();
                    IDList.push(objetiveCreationResult.Response);
                    $window.ListofEntityID = IDList;
                    var TrackID = CreateHisory(IDList);
                    if (!$scope.$$phase) {
                        var localScope = $(event.target).scope();
                        localScope.$apply(function () {
                            $location.path('/mui/objective/detail/section/' + objetiveCreationResult.Response + '/overview');
                        });
                    } else {
                        $location.path('/mui/objective/detail/section/' + objetiveCreationResult.Response + '/overview');
                    }
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4282.Caption'));
                }
            });
            $scope.closeObjectivecreationpopup();
        }
        $scope.ConditionDataArr = [];

        function collectionObjectiveCondition() {
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            var optionArr = [];
            $('#ObjectiveFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                if ($('#ObjConditionType' + index).val() == '') {
                    var collectionarr = {
                        'ObjEntityType': $('#ObjEntityType' + index).val(),
                        'ObjEntityAttributes': $('#ObjEntityAttributes' + index).val(),
                        'ObjAttributeOptionns': $('#ObjAttributeOptions' + index).val(),
                        'ConditionType': 0
                    };
                } else {
                    var collectionarr = {
                        'ObjEntityType': $('#ObjEntityType' + index).val(),
                        'ObjEntityAttributes': $('#ObjEntityAttributes' + index).val(),
                        'ObjAttributeOptionns': $('#ObjAttributeOptions' + index).val(),
                        'ConditionType': $('#ObjConditionType' + index).val()
                    };
                }
                $scope.ConditionDataArr.push(collectionarr);
            });
        }
        $scope.SaveObjectiveName = function (objValue) { }

        function SetCursorPosition(ctrID, cursorPosition) {
            var cnt = ctrID.val().length;
            if (cnt > 4) {
                cursorPosition = cursorPosition + 1;
                if (cnt > 5) cursorPosition = cursorPosition - 1;
                if (cnt > 8) cursorPosition = cursorPosition + 1;
                if (cnt > 9) cursorPosition = cursorPosition - 1;
            }
            setTimeout(function () {
                ctrID.caret(cursorPosition);
            }, 0);
        }

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function getMoney(A) {
            var a = new Number(A);
            var b = a.toFixed(2);
            a = parseInt(a);
            b = (b - a).toPrecision(2);
            b = parseFloat(b).toFixed(2);
            a = a.toLocaleString();
            a = a.replace(",", "");
            a = Number(a);
            a = a.formatMoney(0, ' ', ' ');
            if (a < 1 && a.lastIndexOf('.00') == (a.length - 3)) {
                a = a.substring(0, a.length - 3);
            }
            if (b.substring(0, 1) == '-') {
                b = b.substring(2);
            } else b = b.substring(1);
            return a + b;
        }

        function handleTextChangeEvents() {
            $('#ObjectiveNumericGlobalbaseline ,#ObjectiveNumericGlobaltarget ,#ObjectiveNonNumericGlobalbaseline ,#ObjectiveNonNumericGlobaltarget').autoNumeric('init', $scope.DecimalSettings['ObjectiveAutoNumeric']);
        }

        $scope.EntityStatusResult = [];

        $scope.settxtColor = function (hexcolor) {
            var r = parseInt(hexcolor.substr(0, 2), 16);
            var g = parseInt(hexcolor.substr(2, 2), 16);
            var b = parseInt(hexcolor.substr(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
            return (yiq >= 160) ? 'black' : 'white';
        }

        $scope.overallstatusformat = function (data) {
            var fontColor = 'blue';
            var txtcolor = 'white';
            $("#select2-drop").addClass("bgDropdown");
            if (data.id == "0" && data.text != "") {
                return "<div class='select2-chosen-label'style='background-color: #FEFEFE; color: #444444; font-size: 11px;'>" + data.text + "</div>";
            }
            else if (data.id != "" || data.text != "") {
                for (var i = 0; i < $scope.EntityStatusResult.length; i++) {
                    var res = $scope.EntityStatusResult[i];
                    if (res.ID == data.id) {
                        var stausoption = res.StatusOptions;
                        var colorcode = res.ColorCode;
                        if (colorcode == null || colorcode.trim() == "")
                            txtcolor = $scope.settxtColor("ffffff");
                        else
                            txtcolor = $scope.settxtColor(colorcode);
                        break;
                    }
                }
            }
            else {
                return "<div class='select2-chosen-label' style='background-color:#" + fontColor + "; color:" + txtcolor + "'>" + data.text + "</div>";
            }
            return "<div class='select2-chosen-label'style='background-color:#" + colorcode + ";color:" + txtcolor + "'>" + stausoption + "</div>";
        };
        setTimeout(function () {
            $scope.LoadAttributesForObjectiveCreation(params.rootID, params.rootCaption, params.objBaseType, params.objEntityTypeId, params.objUnitTypeId);
        }, 300);
        $scope.closeObjectivecreationpopup = function () {
            $modalInstance.dismiss('cancel');
        }

        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            if ($scope.fields["Tree_" + branch.AttributeId].length > 0) {
                if ($("#Tree_" + branch.AttributeId).parent().parent().parent().hasClass('error') == true) {
                    if (branch.AttributeId > 0) {
                        $("#Tree_" + branch.AttributeId).parent().parent().parent().removeClass("error");
                        $("#Tree_" + branch.AttributeId).next().hide();
                    }
                }
            }
            $scope.ShowOrHideAttributeToAttributeRelation(branch.AttributeId + "_0", 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};

        $scope.$on("$destroy", function () {
            $timeout.cancel(ObjectivecreationTimout);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.section.additionalobjectiveCtrl']"));
            var model, renderContext, detailTabValues, DateCompare, st_date, end_date, fulfillTabValues, totalWizardSteps, currentWizardStep, totalWizardSteps, EntttyTypeArr, attriArreArr, optionArr, tempval, keys, objOwnername, objOwnerid, result, membervalues, objentities, TargetControl, currentUniqueId, AttributeData, entityAttribtueId, entityAttributeLevel, UniqueId, html, currentId, ObjectiveEnttiy, ratingCollectionObject, i, x, j, a, b, IDList, TrackID, localScope, collectionarr, cnt, retCommaString, getstr, count, commaString = null, treeTextVisbileflag;
        });
    }
    app.controller("mui.planningtool.objective.objectivecreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'ObjectivecreationService', '$modalInstance', 'params', muiplanningtoolobjectiveobjectivecreationCtrl]);

})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/objective/detail/filtersettings-controller.js
(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, ObjectivefiltersettingsService) {
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.appliedfilter = "No filter applied";
        $scope.deletefiltershow = false;
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        var rememberfilter = [];
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.EntityTypeID = SystemDefinedEntityTypes.Objective;
        $scope.visible = false;
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.filterSettingValues = [];
        $scope.ObjectiveOwners = {};
        $scope.Savefilter = true;
        $scope.ObjectiveFilter = {
            ngObjectivefilterStatus: [],
            ngObjectivefilterOwner: [],
            ngObjectiveType: [],
            ngObjectivefilterSaveAs: '',
            ngObjectivefilterKeyword: ''
        }
        $scope.objectivefiltercreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#rootlevelfilter").on("ClearScope", function (event) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterid) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterid);
        });
        $scope.ClearScopeModle = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattr = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            RememberObjFilterID = 0;
            RememberObjFilterAttributes = [];
            $cookieStore.remove('ObjAttrs' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterID' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterName' + parseInt($cookies['UserId'], 10));
            RemeberObjFilterName = "No filter applied";
            $("#Div_Objlist").trigger('ClearAndReApply');
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattr)
            }
        }
        ObjectivefiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
            var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
            $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
        });
        var filterSettings;
        ObjectivefiltersettingsService.GetFilterSettings(parseInt($scope.EntityTypeID, 10)).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () {
            filterSettings;
        }, 100);
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ObjectiveFilter.ngObjectivefilterSaveAs == '' || $scope.ObjectiveFilter.ngObjectivefilterSaveAs == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $("#saveFilter").attr('disabled', 'disabled');
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.IsDetailFilter = 0;
            FilterData.WhereConditon = whereConditionData;
            ObjectivefiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4283.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4401.Caption'));
                    IsSave = 0;
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, filterSettingsInsertresult.Response]);
                    filterSettingsInsertresult.WhereConditon = [];
                    $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    RememberObjFilterID = $scope.FilterID.selectedFilterID;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = filterSettingsInsertresult.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), filterSettingsInsertresult.FilterName);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.LoadFilterSettingsByFilterID = function (event, filterId) {
            $scope.Updatefilter = false;
            $scope.Applyfilter = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.FilterID.selectedFilterID = filterId;
            ObjectivefiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                    if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3) $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveType) $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveStatus) $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].Value);
                }
                $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.Keyword;
                $scope.ObjectiveFilter.ngObjectivefilterSaveAs = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilter = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var filterattributes = [];
            var FilterData = {};
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            FilterData.FilterId = parseInt($scope.FilterID.selectedFilterID, 10);
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            ObjectivefiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsUpdateresult) {
                if (filterSettingsUpdateresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4399.Caption'));
                    IsUpdate = 0;
                    filterSettingsUpdateresult.WhereConditon = [];
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, filterSettingsUpdateresult.Response]);
                    $scope.appliedfilter = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
                    var StartRowNo = 0;
                    var MaxNoofRow = 20;
                    var PageIndex = 0;
                    RememberObjFilterID = filterSettingsUpdateresult.Response;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = filterSettingsUpdateresult.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), filterSettingsUpdateresult.Response);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, filterSettingsUpdateresult.Response, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.ApplyFilter = function (FilterID, FilterName) {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if (whereConditionData.length == 0 && $scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                });
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            RememberObjFilterID = FilterID;
            RememberObjFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
            RemeberObjFilterName = $scope.appliedfilter;
            $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
            $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), FilterID);
            $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }

        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattribtues = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        ObjectivefiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                $scope.ApplyFilter(0, 'No filter applied');
                                $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), 'No filter applied', 0]);
                                $scope.FilterID.selectedFilterID = 0;
                                $scope.AppllyFilterObj.selectedattributes = [];
                                if ($scope.EntityTypeID == 10) {
                                    $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattribtues)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };
        $("#rootlevelfilter").on('RemeberApplyFilterReLoad', function (event, TypeID, RememberPlanFilterAttributes) {
            ObjectivefiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
                //$scope.ObjectiveFilter.ngObjectivefilterOwner = [];
                //$scope.ObjectiveFilter.ngObjectiveType = [];
                //$scope.ObjectiveFilter.ngObjectivefilterKeyword = '';
                //$scope.ObjectiveFilter.ngObjectivefilterStatus = [];
                var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
                $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
                $timeout(function () {
                    RememberFilterOnReLoad(RememberPlanFilterAttributes)
                }, 10);
            });
        });

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 10) {
                if (RememberPlanFilterAttributes.length != 0) {
                    $('.FilterHolder').show()
                    $scope.ddlParententitytypeId = [];
                    $scope.filterSettingValues.FilterValues = RememberPlanFilterAttributes;
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3)
                        {
                            var res = $.grep($scope.ObjectiveFilter.ngObjectivefilterOwner, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res.length == 0) $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveType)
                        {
                            var res1 = $.grep($scope.ObjectiveFilter.ngObjectiveType, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res1.length == 0) $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveStatus) {
                            var res2 = $.grep($scope.ObjectiveFilter.ngObjectivefilterStatus, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res2.length == 0) $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                    }
                    $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.FilterValues[0].Keyword;
                }
            }
        }
        $scope.ObjFilterStatus = [{
            Id: 1,
            FilterStatus: $translate.instant('LanguageContents.Res_4038.Caption')
        }, {
            Id: 0,
            FilterStatus: $translate.instant('LanguageContents.Res_4202.Caption')
        }];
        $scope.ObjTypeList = [{
            Id: 1,
            objType: $translate.instant('LanguageContents.Res_4534.Caption')
        }, {
            Id: 2,
            objType: $translate.instant('LanguageContents.Res_4533.Caption')
        }, {
            Id: 3,
            objType: $translate.instant('LanguageContents.Res_706.Caption')
        }, {
            Id: 4,
            objType: $translate.instant('LanguageContents.Res_707.Caption')
        }];
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.filtersettingsCtrl']"));
        });
    }
    app.controller("mui.planningtool.objective.detail.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'ObjectivefiltersettingsService', muiplanningtoolobjectivedetailfiltersettingsCtrl]);
})(angular, app);
///#source 1 1 /app/services/objectivelist-service.js
(function(ng,app){"use strict";function ObjectivelistService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetFilterSettings:GetFilterSettings,DeleteEntity:DeleteEntity,ObjectiveRootLevel:ObjectiveRootLevel,GetAllEntityIds:GetAllEntityIds ,GetObjectiveEntityType: GetObjectiveEntityType});function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteEntity(formobj){var request=$http({method:"post",url:"api/Planning/DeleteEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function ObjectiveRootLevel(formobj){var request=$http({method:"post",url:"api/Metadata/ObjectiveRootLevel/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllEntityIds() { var request = $http({ method: "get", url: "api/Metadata/GetAllEntityIds/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function GetObjectiveEntityType(IsAdmin) { var request = $http({ method: "get", url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("ObjectivelistService",['$http','$q',ObjectivelistService]);})(angular,app);
///#source 1 1 /app/services/objectivecreation-service.js
(function (ng, app) {
    "use strict";

    function ObjectivecreationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetUserById: GetUserById,
            GettingObjectiveUnits: GettingObjectiveUnits,
            CreateObjective: CreateObjective,
            GetEntityTypeRoleAccess: GetEntityTypeRoleAccess,
            GetFulfillmentEntityTypes: GetFulfillmentEntityTypes,
            GetFulfillmentAttribute: GetFulfillmentAttribute,
            GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
            //GetObjectiveEntityType: GetObjectiveEntityType,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetObjectiveUnitsOptionValues: GetObjectiveUnitsOptionValues,
            GetEntityStatus: GetEntityStatus,
            copyuploadedImage: copyuploadedImage
        });

        function GetUserById(ID) {
            var request = $http({
                method: "get",
                url: "api/user/GetUserById/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingObjectiveUnits() {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingObjectiveUnits/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateObjective(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/CreateObjective/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAccess(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        //function GetObjectiveEntityType(IsAdmin) {

        //    var request = $http({
        //        method: "get",
        //        url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin,
        //        params: {
        //            action: "get"
        //        }
        //    });
        //    return (request.then(handleSuccess, handleError));
        //}

        function GetValidationDationByEntitytype(EntityTypeID) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetObjectiveUnitsOptionValues(unitTypeId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetObjectiveUnitsOptionValues/" + unitTypeId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function copyuploadedImage(filename) {
            var request = $http({
                method: "post",
                url: "api/Planning/copyuploadedImage/",
                params: {
                    action: "save"
                },
                data: {
                    filename: filename
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("ObjectivecreationService", ['$http', '$q', ObjectivecreationService]);
})(angular, app);
///#source 1 1 /app/services/objectivefiltersettings-service.js
(function(ng,app){"use strict";function ObjectivefiltersettingsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetAllObjectiveMembers:GetAllObjectiveMembers,GetFilterSettings:GetFilterSettings,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings});function GetAllObjectiveMembers(EntityTypeId){var request=$http({method:"get",url:"api/user/GetAllObjectiveMembers/"+EntityTypeId,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("ObjectivefiltersettingsService",['$http','$q',ObjectivefiltersettingsService]);})(angular,app);
///#source 1 1 /app/services/planfiltersettings-service.js
(function(ng,app){function PlanfiltersettingsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GettingEntityForRootLevel:GettingEntityForRootLevel,GetOptionsFromXML:GetOptionsFromXML,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings})
function GettingEntityForRootLevel(IsRootLevel){var request=$http({method:"get",url:"api/Metadata/GettingEntityForRootLevel/"+IsRootLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanfiltersettingsService",['$http','$q',PlanfiltersettingsService]);})(angular,app);
