﻿(function (ng, app) {
    "use strict";

    function ObjectivecreationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetUserById: GetUserById,
            GettingObjectiveUnits: GettingObjectiveUnits,
            CreateObjective: CreateObjective,
            GetEntityTypeRoleAccess: GetEntityTypeRoleAccess,
            GetFulfillmentEntityTypes: GetFulfillmentEntityTypes,
            GetFulfillmentAttribute: GetFulfillmentAttribute,
            GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
            //GetObjectiveEntityType: GetObjectiveEntityType,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetObjectiveUnitsOptionValues: GetObjectiveUnitsOptionValues,
            GetEntityStatus: GetEntityStatus,
            copyuploadedImage: copyuploadedImage
        });

        function GetUserById(ID) {
            var request = $http({
                method: "get",
                url: "api/user/GetUserById/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingObjectiveUnits() {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingObjectiveUnits/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateObjective(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/CreateObjective/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAccess(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        //function GetObjectiveEntityType(IsAdmin) {

        //    var request = $http({
        //        method: "get",
        //        url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin,
        //        params: {
        //            action: "get"
        //        }
        //    });
        //    return (request.then(handleSuccess, handleError));
        //}

        function GetValidationDationByEntitytype(EntityTypeID) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));

        }

        function GetObjectiveUnitsOptionValues(unitTypeId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetObjectiveUnitsOptionValues/" + unitTypeId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function copyuploadedImage(filename) {
            var request = $http({
                method: "post",
                url: "api/Planning/copyuploadedImage/",
                params: {
                    action: "save"
                },
                data: {
                    filename: filename
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("ObjectivecreationService", ['$http', '$q', ObjectivecreationService]);
})(angular, app);