﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Cms.Interface;

namespace BrandSystems.Marcom.Core.Cms
{
    internal class CmsEntity : ICmsEntity
    {
        #region Member variables

        protected int _id;
        protected int _parentid;
        protected bool _active;
        protected string _name;
        protected int _level;
        protected int _templateid;
        protected int _navid;
        protected string _publisheddate;
        protected string _publishedtime;
        protected string _description;
        protected int _contentVersionid;
        protected bool _publishedstatus;
        protected string _uniquekey;
        protected string _tag;

        #endregion

        #region Constructors

        public CmsEntity()
        {

        }

        public CmsEntity(int pid)
        {
            this._id = pid;
        }

        public CmsEntity(int pid, int pparentid, bool pactive, string pname, int plevel, int ptemplateid, int pnavid, string ppublisheddate, string pdescription, int pcontenversionid, bool ppublishedstatus, string puniquekey, string ppublishedtime)
        {
            this._id = pid;
            this._parentid = pparentid;
            this._active = pactive;
            this._contentVersionid = pcontenversionid;
            this._description = pdescription;
            this._level = plevel;
            this._name = pname;
            this._navid = pnavid;
            this._publisheddate = ppublisheddate;
            this._publishedstatus = ppublishedstatus;
            this._templateid = ptemplateid;
            this._uniquekey = puniquekey;
            this._publishedtime = ppublishedtime;
        }

        #endregion

        #region Public properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ParentID
        {
            get { return _parentid; }
            set { _parentid = value; }
        }


        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public string UniqueKey
        {
            get { return _uniquekey; }
            set { _uniquekey = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        public int TemplateID
        {
            get { return _templateid; }
            set { _templateid = value; }
        }

        public int NavID
        {
            get { return _navid; }
            set { _navid = value; }
        }


        public string PublishedDate
        {
            get { return _publisheddate; }
            set { _publisheddate = value; }
        }

        public string PublishedTime
        {
            get { return _publishedtime; }
            set { _publishedtime = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int Version
        {
            get { return _contentVersionid; }
            set { _contentVersionid = value; }
        }


        public bool PublishedStatus
        {
            get { return _publishedstatus; }
            set { _publishedstatus = value; }
        }

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public bool IsChildrenPresent
        {
            get;
            set;
        }

        #endregion



    }
}
