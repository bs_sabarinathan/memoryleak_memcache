﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public class ExternalTaskStepInfoDao : BaseDao, ICloneable
    {
        public virtual int ID { get; set; }
        public virtual int MarcomStepId { get; set; }
        public virtual int ExternalId { get; set; }
        public virtual string CallBackStatus { get; set; }

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
