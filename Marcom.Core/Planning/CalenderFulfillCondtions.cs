﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class CalenderFulfillCondtions : ICalenderFulfillCondtions
    {
        public int Id { get; set; }
        public int? EntityTypeid { get; set; }
        public int? Attributeid { get; set; }
        public int? Optionid { get; set; }
        public int ConditionType { get; set; }
        public IList<int> CalenderConditionValue { get; set; }
        public string EntityTypeCaption { get; set; }
        public string AttributeCaption { get; set; }
        public string OptionCaption { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DateRule { get; set; }
        public string Mandatory { get; set; }
        public string FulfillCondition { get; set; }
        public string CalenderName { get; set; }
        public string CalenderDescription { get; set; }
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }
        public bool IsMandatory { get; set; }
        public int IsDateRule { get; set; }
        public int ConditionId { get; set; }
        public int AttributeLevel { get; set; }
        public string CalenderOptionValue { get; set; }
    }
}
