﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IWidgetContainer
    {
        int ID { get; set; }
        int TypeID { get; set; }
        string VisualType { get; set; }
        bool IsDynamic { get; set; }
    }
}
