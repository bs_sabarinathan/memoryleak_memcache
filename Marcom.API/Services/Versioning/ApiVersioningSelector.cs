﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

//needed for .GetSubRoutes() extension method
using System.Web.Http.Routing;
 
namespace Marcom.API.Services.Versioning
{
    public class ApiVersioningSelector : DefaultHttpControllerSelector
    {
        private HttpConfiguration _HttpConfiguration;
        public ApiVersioningSelector(HttpConfiguration httpConfiguration)
            : base(httpConfiguration)
        {
            _HttpConfiguration = httpConfiguration;
        }


        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            HttpControllerDescriptor controllerDescriptor = null;

            // get list of all controllers provided by the default selector
            IDictionary<string, HttpControllerDescriptor> controllers = GetControllerMapping();

            IHttpRouteData routeData = request.GetRouteData();

            if (routeData == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            //check if this route is actually an attribute route
            IEnumerable<IHttpRouteData> attributeSubRoutes = routeData.GetSubRoutes();

            var apiVersion = GetVersionFromMediaType(request);
            
            if (attributeSubRoutes == null)
            {
                string controllerName = GetRouteVariable<string>(routeData, "controller");
                if (controllerName == null)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                string newControllerName = String.Concat(controllerName, "V", apiVersion);

                if (controllers.TryGetValue(newControllerName, out controllerDescriptor))
                {
                     return controllerDescriptor;
                }
                else
                {
                    //throw new HttpResponseException(HttpStatusCode.NotFound);
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent(string.Format("Requested version of API not found.")),
                        ReasonPhrase = "Requested version of API not found."
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                // we want to find all controller descriptors whose controller type names end with
                // the following suffix (example: PeopleV1)
                string newControllerNameSuffix = String.Concat("V", apiVersion);

                IEnumerable<IHttpRouteData> filteredSubRoutes = attributeSubRoutes
                    .Where(attrRouteData =>
                    {
                        HttpControllerDescriptor currentDescriptor = GetControllerDescriptor(attrRouteData);

                        bool match = currentDescriptor.ControllerName.EndsWith(newControllerNameSuffix);

                        if (match && (controllerDescriptor == null))
                        {
                            controllerDescriptor = currentDescriptor;
                        }

                        return match;
                    });

                routeData.Values["MS_SubRoutes"] = filteredSubRoutes.ToArray();
            }

            return controllerDescriptor;
        }

        private HttpControllerDescriptor GetControllerDescriptor(IHttpRouteData routeData)
        {
            return ((HttpActionDescriptor[])routeData.Route.DataTokens["actions"]).First().ControllerDescriptor;
        }

        // Get a value from the route data, if present.
        private static T GetRouteVariable<T>(IHttpRouteData routeData, string name)
        {
            object result = null;
            if (routeData.Values.TryGetValue(name, out result))
            {
                return (T)result;
            }
            return default(T);
        }


        //Accept: application/vnd.yournamespace.{yourresource}.v{version}+json
        private string GetVersionFromMediaType(HttpRequestMessage request)
        {
            if (request.Headers.Contains("X-API-Version"))
            {
                var stversion = request.Headers.GetValues("X-API-Version").First().ToString();
                var decVersion = decimal.Parse(stversion);
                return decVersion.ToString("#.#");
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.HttpVersionNotSupported)
                {
                    Content = new StringContent(string.Format("Version number is missing in request header.")),
                    ReasonPhrase = "X-API-Version missing in request header."
                };
                throw new HttpResponseException(resp);
            }
         }

          
    }
}