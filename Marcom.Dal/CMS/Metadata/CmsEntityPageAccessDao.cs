﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.CMS.Model
{
    public partial class CmsEntityPageAccessDao : BaseDao, ICloneable
    {
        #region Member variables

        protected int _id;
        protected int _entityid;
        protected int _roleid;

        #endregion

        #region Constructors

        public CmsEntityPageAccessDao()
        {

        }

        public CmsEntityPageAccessDao(int pid)
        {
            this._id = pid;
        }

        public CmsEntityPageAccessDao(int pid, int pentityid, int proleid)
        {
            this._id = pid;
            this._entityid = pentityid;
            this._roleid = proleid;
        }

        #endregion

        #region Public properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public virtual int RoleID
        {
            get { return _roleid; }
            set { _roleid = value; }
        }



        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CmsEntityPageAccessDao = "CmsEntityPageAccessDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string RoleID = "RoleID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CmsEntityPageAccessDao = "CmsEntityPageAccessDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string RoleID = "RoleID";
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
