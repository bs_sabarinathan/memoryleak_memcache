﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IEntityAmountCurrencyType
    {
       
            #region Public Properties


            int Id
            {
                get;
                set;

            }

            int EntityID
            {
                get;
                set;

            }

            int Currencytypeid
            {
                get;
                set;

            }

            decimal Amount
            {
                get;
                set;

            }
            int Attributeid
            {
                get;
                set;

            }

            #endregion
        }
    }

