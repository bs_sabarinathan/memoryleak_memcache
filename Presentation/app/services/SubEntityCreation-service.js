﻿(function (ng, app) {
    function SubentitycreationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetCostcentreforEntityCreation: GetCostcentreforEntityCreation,
            GetCostcentreTreeforPlanCreation: GetCostcentreTreeforPlanCreation,
            GetGlobalMembers: GetGlobalMembers,
            GettingPredefineObjectivesForEntityMetadata: GettingPredefineObjectivesForEntityMetadata,
            CreateEntity: CreateEntity,
            GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById,
            GetCurrencyListFFsettings: GetCurrencyListFFsettings,
            GetEntityTypeRoleAccess: GetEntityTypeRoleAccess,
            GetUserById: GetUserById,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetJSONEntityTypeAttributeRelationWithLevelsByID: GetJSONEntityTypeAttributeRelationWithLevelsByID,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            GetPlantabsettings: GetPlantabsettings,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetEntityStatus: GetEntityStatus,
            copyuploadedImage: copyuploadedImage,
            SaveAsset: SaveAsset,
            LinkFromEntityToAsset: LinkFromEntityToAsset,
            InheritParentAttributeGroupRecords: InheritParentAttributeGroupRecords
        });

        function GetCostcentreforEntityCreation(EntityTypeID, FiscalYear, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostcentreforEntityCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCostcentreTreeforPlanCreation(EntityTypeID, FiscalYear, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostcentreTreeforPlanCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetGlobalMembers(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetGlobalMembers/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingPredefineObjectivesForEntityMetadata(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/GettingPredefineObjectivesForEntityMetadata/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function CreateEntity(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/CreateEntity/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCurrencyListFFsettings() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCurrencyListFFsettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeRoleAccess(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetUserById(ID) {
            var request = $http({
                method: "get",
                url: "api/user/GetUserById/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeToAttributeRelationsByIDForEntity(ID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetJSONEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetJSONEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetValidationDationByEntitytype(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetPlantabsettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetPlantabsettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityStatus(entityTypeID, isAdmin, entityId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityStatus/" + entityTypeID + "/" + isAdmin + "/" + entityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InheritParentAttributeGroupRecords(parentId, EntityID, AttributeGroupId) {
            var request = $http({
                method: "get",
                url: "api/Metadata/InheritParentAttributeGroupRecords/" + parentId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function copyuploadedImage(filename) { var request = $http({ method: "post", url: "api/Planning/copyuploadedImage/", params: { action: "save" }, data: { filename: filename } }); return (request.then(handleSuccess, handleError)); }
        function SaveAsset(SaveAsset) {
            var request = $http({
                method: "post",
                url: "api/dam/CreateAsset/",
                params: {
                    action: "add"
                },
                data: {
                    saveAsset: [SaveAsset]
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function LinkFromEntityToAsset(SaveAsset) {
            var request = $http({
                method: "post",
                url: "api/dam/LinkFromEntityToAsset/",
                params: {
                    action: "add"
                },
                data:  SaveAsset
            });
            return (request.then(handleSuccess, handleError));
        } 
function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("SubentitycreationService", ['$http', '$q', SubentitycreationService]);
})(angular, app);