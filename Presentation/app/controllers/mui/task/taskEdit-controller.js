﻿(function (ng, app) {
    "use strict";

    function muitaskFlowEditCtrl($scope, $rootScope, $location, $compile, $timeout, $cookies, $stateParams, $translate, TaskEditService, $sce, $modalInstance, items, $modal) {

        //var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
        var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.Amazon) ? (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/") : TenantFilePath;
        $scope.cloudURL = imageBaseUrlcloud;
        $scope.taskcommonInfo = {};
        $scope.IsOwnerOfTask = true;
        var prevcall;
        $scope.Calanderopen = function ($event, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[prevcall] = false;
            $scope[Call_from] = true;
            prevcall = Call_from;
        }
        $scope.$emit('mui_resetuploaderstatus');
        var TASKEDIT_NS = {}, taskResponsedata = {}, taskModifieddata = {
            taskid: 0,
            CurrentActualTaskVersion: 1,
            timestamp: "",
            versions: [],
            overview: {},
            checklist: [],
            Assignees: [],
            phasestepstate: [],
            approvebtnArray: [],
            revokebtnarray: [],
            rejectbtnArray: [],
            completebtnArray: [],
            unabletocompletebtnArray: [],
            ShowPhaseStep: true,
            currentStep: {}
        }, arrayConstructor = [].constructor,
			objectConstructor = {}.constructor;
        $scope.PageFullData = {
            TaskName: "",
            CurrentActualTaskVersion: 1,
            taskid: 0,
            versions: [],
            timestamp: "",
            overview: {},
            checklist: [],
            Assignees: [],
            phasestepstate: [],
            approvebtnArray: [],
            revokebtnarray: [],
            rejectbtnArray: [],
            completebtnArray: [],
            unabletocompletebtnArray: [],
            ShowPhaseStep: true,
            currentStep: {},
            btnCreatenewVersion: false,
            approveDocumentAssetId: 0,
            taskVersioncreating: false
        };
        $scope.ActionHandler = {};
        $scope.AutoCompleteSelectedUserObj = {
            UserSelection: {},
            userrole: ''
        };
        $scope.IsApprovalReportEnables = false;
        $scope.TaskEntityLockTask = false;
        $scope.Steproles = [];
        $scope.UserRoles = [];
        $scope.UserApprovalRoles = [];
        $scope.taskID = 0;
        $scope.TaskEntityMemberLists = [];
        $scope.TaskGlobalMemberLists = [];
        $scope.CurrStepID = 0;
        $scope.PageFullData.overview.dueIn = 0;
        $scope.IsworkTask = false;
        $scope.showAddAssignees = true;
        $scope.isinitiatorexist = true;
        $scope.EntityMemberList = [];
        var TempStepID = 0;
        $scope.currentEntityID = 0;
        var TaskListRefresh;
        $scope.IsSelectAppDocExist = true;
        var taskeditTimer = "";
        var CurrentCheckListName = "";
        jQuery('#myTab a:first').tab('show');
        $scope.newdragoverCallback = function (event, index, external, type) {
            $scope.newlogListEvent('dragged over', event, index, external, type);
            return index >= 0;
        };
        $scope.contextCheckListID = 0;
        $scope.contextCheckListIndex = 0;
        $scope.SetcontextCheckListID = function (id, indexval) {
            $scope.contextCheckListID = id;
            $scope.contextCheckListIndex = indexval;
        };
        $scope.newdropCallback = function (event, index, item, external, type, allowedType) {
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }
            TASKEDIT_NS.calcLIheight();
            $scope.ReorderOverviewStructure = true;
            return item;
        };
        $scope.newlogEvent = function (event, index, item, external, type, allowedType) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + event + ' position ' + index;
            return false;
        };
        $scope.hello = function (event) {
            event = event.originalEvent || event;
            if (event.target.tagName === 'INPUT') {
                if (!(event.dataTransfer.types && event.dataTransfer.types.length)) {
                    event.preventDefault();
                }
                return;
            } else {
                event.preventDefault();
                return;
            }
        }
        $scope.newlogListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
            $scope.newlogEvent(message, event);
        };
        $scope.$watch('PageFullData.phasestepstate', function (model) {
            $scope.modelAsJson = angular.toJson(model, true);
        }, true);
        TASKEDIT_NS.initialize = function (taskid) {
            TaskEditService.GetTakUIData(taskid, 0).then(function (data) {
                if (data != null) {
                    taskResponsedata = JSON.parse(data.Response.m_Item1.replace(/\n/g, "\\n").replace(/\n/g, "<br/>").replace(/\<br\>/g, "\n"));
                    if (taskResponsedata[0] != null) {
                        taskModifieddata.CurrentActualTaskVersion = taskResponsedata[0].currentVersion;
                        $scope.PageFullData.CurrentActualTaskVersion = taskResponsedata[0].currentVersion;
                        $scope.PageFullData.TaskTypeID = taskResponsedata[0].TaskType;
                        $scope.PageFullData.taskParentId = taskResponsedata[0].EntityID;
                    }
                    TASKEDIT_NS.initializeObject(taskResponsedata[0]);
                    $("#taskImprovementPopup").modal('show');

                    //$timeout(function () {
                    //    $scope.$broadcast('LoadTaskMetadata', {
                    //        TaskId: $scope.PageFullData.taskid,
                    //        Response: [],
                    //        IsLock: !($scope.PageFullData.CurrentActualTaskVersion == $scope.PageFullData.overview.currentVersion),
                    //        visibleID: false,
                    //        visibleName: false
                    //    });
                    //}, 700);
                } else { }
            });
        }
        TASKEDIT_NS.custominitialize = function (taskid) {
            TaskEditService.GetTakUIData(taskid, 0).then(function (data) {
                if (data != null) {
                    taskResponsedata = JSON.parse(data.Response.m_Item1);
                    if (taskResponsedata[0] != null) {
                        taskModifieddata.CurrentActualTaskVersion = taskResponsedata[0].currentVersion;
                        $scope.PageFullData.CurrentActualTaskVersion = taskResponsedata[0].currentVersion;
                        $scope.PageFullData.TaskTypeID = taskResponsedata[0].TaskType;
                        $scope.PageFullData.taskParentId = taskResponsedata[0].EntityID;
                    }
                    TASKEDIT_NS.customInitializeObject(taskResponsedata[0]);
                } else { }
            });
        }
        TASKEDIT_NS.xmltoJSON = function (xml) {
            var obj = {};
            if (xml != null) {
                if (xml.nodeType == 1) {
                    if (xml.attributes.length > 0) {
                        obj["@attr"] = {};
                        for (var j = 0; j < xml.attributes.length; j++) {
                            var attribute = xml.attributes.item(j);
                            obj["@attr"][attribute.nodeName] = attribute.nodeValue;
                        }
                    }
                } else if (xml.nodeType == 3) {
                    obj = xml.nodeValue;
                }
                if (xml.hasChildNodes()) {
                    for (var i = 0; i < xml.childNodes.length; i++) {
                        var item = xml.childNodes.item(i);
                        var nodeName = item.nodeName;
                        if (typeof (obj[nodeName]) == "undefined") {
                            obj[nodeName] = TASKEDIT_NS.xmltoJSON(item);
                        } else {
                            if (typeof (obj[nodeName].push) == "undefined") {
                                var old = obj[nodeName];
                                obj[nodeName] = [];
                                obj[nodeName].push(old);
                            }
                            obj[nodeName].push(TASKEDIT_NS.xmltoJSON(item));
                        }
                    }
                }
                return obj;
            }
        }
        TASKEDIT_NS.initializeObject = function (obj) {
            try {
                TASKEDIT_NS._isApprovalReportEnables();
                TASKEDIT_NS._formVersionArray(obj.TaskVersionDetails);
                TASKEDIT_NS._markCurrentVersion(obj.currentVersion);
                TASKEDIT_NS._formOverview(obj);
                TASKEDIT_NS._formAssignees(obj.TotaTaskAssignees);
                TASKEDIT_NS._formPhases(obj.TaskPhases);
                TASKEDIT_NS._formPhaseSteps(obj.TaskSteps);
                TASKEDIT_NS._decideStepStatus(obj);
                TASKEDIT_NS._formPhasewithStepsandMembers(obj);
                TASKEDIT_NS._currentPhaseAssigneesStatus(obj);
                TASKEDIT_NS._formInactiveAssigneesStatus(obj);
                TASKEDIT_NS._formChecklists(obj);
                TASKEDIT_NS._decideTaskStatus(obj);
                TASKEDIT_NS._userTaskWorkPermit(obj);
                TASKEDIT_NS._decideTaskProgressName(obj);
                TASKEDIT_NS._duration(obj.currentVersion, obj.UpdatedOn);
            } catch (ex) {
                console.log(ex.message);
            }
        }
        TASKEDIT_NS.customInitializeObject = function (obj) {
            try {
                TASKEDIT_NS._isApprovalReportEnables();
                TASKEDIT_NS._formOverview(obj);
                TASKEDIT_NS._formAssignees(obj.TotaTaskAssignees);
                TASKEDIT_NS._formPhases(obj.TaskPhases);
                TASKEDIT_NS._formPhaseSteps(obj.TaskSteps);
                TASKEDIT_NS._decideStepStatus(obj);
                TASKEDIT_NS._formPhasewithStepsandMembers(obj);
                TASKEDIT_NS._currentPhaseAssigneesStatus(obj);
                TASKEDIT_NS._formInactiveAssigneesStatus(obj);
                TASKEDIT_NS._formChecklists(obj);
                TASKEDIT_NS._decideTaskStatus(obj);
                TASKEDIT_NS._userTaskWorkPermit(obj);
                TASKEDIT_NS._decideTaskProgressName(obj);
                TASKEDIT_NS._duration(obj.currentVersion, obj.UpdatedOn);
            } catch (ex) {
                console.log(ex.message);
            }
        }
        TASKEDIT_NS._formVersionArray = function (obj) {
            taskModifieddata.versions = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [];
            TASKEDIT_NS._createTaskVersionfromXML(dt);
            $scope.PageFullData.versions = taskModifieddata.versions;
        }
        TASKEDIT_NS._markCurrentVersion = function (versionNo) {
            var activeversion = $.grep($scope.PageFullData.versions, function (rel) {
                return rel.VersionNo == versionNo
            })[0],
				remainVersions = $.grep($scope.PageFullData.versions, function (rel) {
				    return rel.VersionNo != versionNo
				});
            if (activeversion != null) activeversion.active = true;
            for (var t = 0, tab; tab = remainVersions[t++];) {
                tab.active = false;
            }
        }
        TASKEDIT_NS._createTaskVersionfromXML = function (dt) {
            var tmp = [];
            if (dt.constructor === objectConstructor) {
                if (dt.path.p != null) {
                    if (dt.path.p.constructor === objectConstructor) {
                        tmp.push(dt.path.p);
                    } else if (dt.path.p.constructor === arrayConstructor) {
                        tmp = dt.path.p;
                    }
                }
            }
            if (tmp.length > 0) for (var j = 0, ver; ver = tmp[j++];) {
                taskModifieddata.versions.push({
                    "title": "V" + ver["@attr"]['VersionNo'],
                    "VersionNo": ver["@attr"]['VersionNo'],
                    "id": ver["@attr"]['ID'],
                    "CreatedDate": ver["@attr"]['CreatedDate'],
                    "InitiatorId": ver["@attr"]['InitiatorId'],
                    "TaskId": ver["@attr"]['TaskId'],
                    initiatorName: ver["@attr"]['initiatorName']
                });
            }
        }
        TASKEDIT_NS._formOverview = function (obj) {
            if (obj.TaskPhases == "<path/>") $scope.IsworkTask = true;
            else $scope.IsworkTask = false;
            TASKEDIT_NS._formOverviewCommonDetails(obj);
            TASKEDIT_NS._formVersionOverviewDetails(obj.CurrentTaskVersionDetails);
        }
        TASKEDIT_NS._formOverviewCommonDetails = function (obj) {
            $scope.PageFullData.taskVersioncreating = false;
            $scope.IsSelectAppDocExist = true;
            taskModifieddata.overview.name = $scope.ActionHandler.DecodedTaskName(obj.name);
            taskModifieddata.overview.Description = (obj.Description == "" || obj.Description == null || obj.Description == undefined) ? "-" : $scope.ActionHandler.DecodedTaskName(obj.Description);
            taskModifieddata.overview.DueDate = obj.DueDate = (obj.DueDate == "" || obj.DueDate == undefined || obj.DueDate == null) ? "-" : dateFormat(obj.DueDate, $scope.format);
            taskModifieddata.overview.EntityID = obj.EntityID;
            taskModifieddata.overview.TaskId = obj.Id;
            taskModifieddata.overview.Note = (obj.Note == "" || obj.Note == null || obj.Note == undefined) ? "-" : $scope.ActionHandler.DecodedTaskName(obj.Note);
            taskModifieddata.overview.TaskListID = obj.TaskListID;
            taskModifieddata.overview.TaskStatus = obj.TaskStatus;
            taskModifieddata.overview.TaskType = obj.TaskType;
            taskModifieddata.overview.checklists = obj.checklists;
            taskModifieddata.overview.completedchecklists = obj.completedchecklists;
            taskModifieddata.overview.currentVersion = obj.currentVersion;
            taskModifieddata.overview.AssetPreviewPath = obj.AssetPreviewPath;
            taskModifieddata.overview.AssetId = obj.AssetId;
            $scope.CurrTaskStatus = obj.TaskStatus;
            $scope.PageFullData.approveDocumentAssetId = obj.AssetId;
            if (taskModifieddata.overview.DueDate == "-") {
                taskModifieddata.overview.dueIn = "";
            } else {
                if (formatteddateFormat(taskModifieddata.overview.DueDate, $scope.format) > new Date.create()) {
                    taskModifieddata.overview.dueIn = dateDiffBetweenDates(formatteddateFormat(taskModifieddata.overview.DueDate, $scope.format)) - TASKEDIT_NS.HolidayCount(formatteddateFormat(taskModifieddata.overview.DueDate, $scope.format));
                } else {
                    taskModifieddata.overview.dueIn = dateDiffBetweenDates(formatteddateFormat(taskModifieddata.overview.DueDate, $scope.format)) + TASKEDIT_NS.HolidayCount(formatteddateFormat(taskModifieddata.overview.DueDate, $scope.format));
                }
            }
            if (taskModifieddata.overview.AssetPreviewPath != null) {
                if (taskeditTimer) $timeout.cancel(taskeditTimer);
                taskeditTimer = $timeout(function () {
                    if (taskModifieddata.overview.AssetPreviewPath != "-")
                        angular.element('#approvDoc').attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + taskModifieddata.overview.AssetPreviewPath.toString().toLowerCase() + '.jpg');
                }, 100);
            } else {
                if (taskeditTimer) $timeout.cancel(taskeditTimer);
                taskeditTimer = $timeout(function () {
                    angular.element('#approvDoc').attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/NoPreview.jpg');
                }, 100);
            }
            if (taskModifieddata.overview.TaskType == 2) {
                taskModifieddata.overview.TaskName = 'Work Task';
            } else if (taskModifieddata.overview.TaskType == 3) {
                taskModifieddata.overview.TaskName = 'Approval Task';
            } else if (taskModifieddata.overview.TaskType == 31) {
                taskModifieddata.overview.TaskName = 'Review Task';
            } else if (taskModifieddata.overview.TaskType == 32) {
                taskModifieddata.overview.TaskName = 'Asset Approval Task';
            } else if (taskModifieddata.overview.TaskType == 36) {
                taskModifieddata.overview.TaskName = 'Proof Approval Task';
            } else if (taskModifieddata.overview.TaskType == 37) {
                taskModifieddata.overview.TaskName = 'Dalim Task';
            }
            $scope.IsTaskLock = true;
            if ($scope.PageFullData.CurrentActualTaskVersion == taskModifieddata.overview.currentVersion) {
                $scope.IsTaskLock = false;
            }
            TASKEDIT_NS._formTaskPath(obj.TaskPath);
            $scope.PageFullData.overview = taskModifieddata.overview;
            if (obj.UpdatedOn != null && obj.UpdatedOn != undefined && obj.UpdatedOn != "") {
                $scope.PageFullData.versionUpdatedDate = dateFormat(obj.UpdatedOn.toString().substring(0, 10));
            } else {
                $scope.PageFullData.versionUpdatedDate = "No Updates";
            }
            $scope.IsPhaseEnable = false;
            if (($scope.PageFullData.overview.TaskType == 3 || $scope.PageFullData.overview.TaskType == 32 || $scope.PageFullData.overview.TaskType == 36 || $scope.PageFullData.overview.TaskType == 37) && ($scope.PageFullData.overview.TaskStatus != 3)) {
                $scope.IsPhaseEnable = true;
            }
        }
        TASKEDIT_NS._formTaskPath = function (obj) {
            taskModifieddata.overview.taskPath = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [];
            if (dt.constructor === objectConstructor) {
                if (dt.path.p != null) {
                    if (dt.path.p.constructor === objectConstructor) {
                        tmp.push(dt.path.p);
                    } else if (dt.path.p.constructor === arrayConstructor) {
                        tmp = dt.path.p;
                    }
                }
            }
            if (tmp.length > 0) for (var j = 0, ver; ver = tmp[j++];) {
                taskModifieddata.overview.taskPath.push({
                    "ID": ver["@attr"]['ID'],
                    "Name": ver["@attr"]['Name'],
                    "UniqueKey": ver["@attr"]['UniqueKey'],
                    "ColorCode": ver["@attr"]['ColorCode'],
                    "ShortDescription": ver["@attr"]['ShortDescription'],
                    "TypeID": ver["@attr"]['TypeID'],
                    "mystyle": "0088CC",
                    "EntityTypeID": ver["@attr"]['TypeID']
                });
            }
        }
        TASKEDIT_NS._formVersionOverviewDetails = function (obj) {
            taskModifieddata.overview.versionDetails = {};
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [];
            if (dt != null) {
                if (dt.constructor === objectConstructor) {
                    if (dt.path.p != null) {
                        if (dt.path.p.constructor === objectConstructor) {
                            tmp = dt.path.p;
                        } else if (dt.path.p.constructor === arrayConstructor) {
                            tmp = dt.path.p[0];
                        }
                        if (tmp["@attr"] == undefined) {
                            tmp["@attr"] = {};
                            tmp["@attr"]['initiatorName'] = tmp['initiatorName']["#text"];
                            tmp["@attr"]['InitiatorId'] = tmp['InitiatorId']["#text"];
                            tmp["@attr"]['VersionNo'] = tmp['VersionNo']["#text"];
                            tmp["@attr"]['CurrentVersionNo'] = tmp['VersionNo']["#text"];
                            tmp["@attr"]['ID'] = tmp['ID']["#text"];
                            tmp["@attr"]['TaskId'] = tmp['TaskId']["#text"];
                            tmp["@attr"]['CreatedDate'] = tmp['CreatedDate']["#text"];
                        }
                        $scope.showInitiator = true;
                        if (tmp["@attr"]['initiatorName'] == (null || undefined || "")) {
                            $scope.showInitiator = false;
                        }
                        $scope.isinitiatorexist = true;
                        taskModifieddata.overview.versionDetails.initiatorName = tmp["@attr"]['initiatorName'] == (null || undefined || "") ? "-" : tmp["@attr"]['initiatorName'];
                        taskModifieddata.overview.versionDetails.InitiatorId = tmp["@attr"]['InitiatorId'] == (null || undefined || "") ? 0 : tmp["@attr"]['InitiatorId'];
                        taskModifieddata.overview.versionDetails.VersionNo = tmp["@attr"]['VersionNo'];
                        taskModifieddata.overview.versionDetails.CurrentVersionNo = tmp["@attr"]['VersionNo'];
                        taskModifieddata.overview.versionDetails.ID = tmp["@attr"]['ID'];
                        taskModifieddata.overview.versionDetails.TaskId = tmp["@attr"]['TaskId'];
                        var datStartUTCval = tmp["@attr"]['CreatedDate'].substr(0, 10);
                        taskModifieddata.overview.versionDetails.TaskId = tmp["@attr"]['TaskId'];
                        var datStartUTCval = tmp["@attr"]['CreatedDate'].substr(0, 10);
                        var datstartval = new Date.create(datStartUTCval);
                        taskModifieddata.overview.versionDetails.CreatedDate = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                    } else {
                        taskModifieddata.overview.versionDetails.initiatorName = "-";
                        taskModifieddata.overview.versionDetails.InitiatorId = "-";
                        $scope.isinitiatorexist = false;
                        taskModifieddata.overview.versionDetails.VersionNo = 1;
                        taskModifieddata.overview.versionDetails.CurrentVersionNo = 1;
                        taskModifieddata.overview.versionDetails.ID = "-";
                    }
                }
                $scope.PageFullData.overview.versionDetails = taskModifieddata.overview.versionDetails;
            }
        }
        TASKEDIT_NS._formPhases = function (obj) {
            taskModifieddata.phases = [];
            $scope.PageFullData.phases = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [];
            if (dt != null) {
                if (dt.constructor === objectConstructor) {
                    if (dt.path.p != null) {
                        if (dt.path.p.constructor === objectConstructor) {
                            tmp.push(dt.path.p);
                        } else if (dt.path.p.constructor === arrayConstructor) {
                            tmp = dt.path.p;
                        }
                    }
                }
                if (tmp.length > 0) {
                    for (var j = 0, ver; ver = tmp[j++];) {
                        taskModifieddata.phases.push({
                            "ID": ver["@attr"]['ID'],
                            "Name": ver["@attr"]['Name'],
                            "Description": ver["@attr"]['Description'],
                            "ColorCode": ver["@attr"]['ColorCode'],
                            "EntityID": ver["@attr"]['EntityID'],
                            "CreatedDate": ver["@attr"]['CreatedDate'],
                            "SortOrder=": ver["@attr"]['SortOrder']
                        });
                    }
                    $scope.PageFullData.phases = taskModifieddata.phases;
                }
            }
        }
        TASKEDIT_NS._formPhaseSteps = function (obj) {
            taskModifieddata.phasesSteps = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [];
            if (dt != null) {
                if (dt.constructor === objectConstructor) {
                    if (dt.path.p != null) {
                        if (dt.path.p.constructor === objectConstructor) {
                            tmp.push(dt.path.p);
                        } else if (dt.path.p.constructor === arrayConstructor) {
                            tmp = dt.path.p;
                        }
                    }
                }
                if (tmp.length > 0) {
                    for (var j = 0, ver; ver = tmp[j++];) {
                        taskModifieddata.phasesSteps.push({
                            "ID": ver["@attr"]['ID'],
                            "Name": ver["@attr"]['Name'],
                            "Description": ver["@attr"]['Description'],
                            "PhaseId": ver["@attr"]['PhaseId'],
                            "Duration": ver["@attr"]['Duration'],
                            "MinApproval": ver["@attr"]['MinApproval'],
                            "Roles": ver["@attr"]['Roles'],
                            IsMandatory: ver["@attr"]['IsMandatory'],
                            CreatedDate: ver["@attr"]['CreatedDate'],
                            SortOrder: ver["@attr"]['SortOrder']
                        });
                    }
                    $scope.PageFullData.steps = taskModifieddata.phasesSteps;
                }
            }
            GetentityMembers($scope.PageFullData.overview.EntityID);
        }
        TASKEDIT_NS._formAssignees = function (obj) {
            taskModifieddata.Assignees = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj)),
				tmp = [],
				users = [];
            if (dt != null) {
                if (dt.constructor === objectConstructor) {
                    if (dt.path.p != null) {
                        if (dt.path.p.constructor === objectConstructor) {
                            tmp.push(dt.path.p);
                        } else if (dt.path.p.constructor === arrayConstructor) {
                            tmp = dt.path.p;
                        }
                    }
                }
                if (tmp.length > 0) {
                    for (var j = 0, ver; ver = tmp[j++];) {
                        users.push({
                            "Email": ver["@attr"]['E'],
                            "FirstName": ver["@attr"]['F'],
                            "RoleID": ver["@attr"]['RID'],
                            "Id": ver["@attr"]['id'],
                            "Userid": ver["@attr"]['UID'],
                            "TaskID": ver["@attr"]['TID'],
                            "Stepid": ver["@attr"]['Stepid'],
                            "PhaseId": ver["@attr"]['PhaseId'],
                            ApprovalStatus: ver["@attr"]['S'],
                            Name: ver["@attr"]['N'],
                            Flag: ver["@attr"]['F'],
                            "ApprovalRoleName": ver["@attr"]['steprolename']
                        });
                    }
                    $scope.PageFullData.owner = {};
                    taskModifieddata.owner = {};
                    $scope.PageFullData.owner = $.grep(users, function (rel) {
                        return rel.RoleID == "1";
                    })[0];
                    taskModifieddata.Assignees = $.grep(users, function (rel) {
                        return rel.RoleID != "1";
                    })
                    if (parseInt($.cookie('UserId'), 10) == parseInt($scope.PageFullData.owner.Userid)) {
                        $scope.IsOwnerOfTask = false;
                    }
                    $scope.PageFullData.Assignees = taskModifieddata.Assignees;
                    taskModifieddata.assigneesCount = taskModifieddata.Assignees.length;
                    $scope.PageFullData.assigneesCount = "No";
                    $scope.showAddAssignees = true;
                    if (taskModifieddata.Assignees.length > 0) {
                        $scope.PageFullData.assigneesCount = taskModifieddata.Assignees.length;
                        $scope.showAddAssignees = false;
                    }
                } else {
                    $scope.IsOwnerOfTask = false;
                    $scope.PageFullData.assigneesCount = "No";
                }
            }
        }
        TASKEDIT_NS._formChecklists = function (obj) {
            taskModifieddata.CheckList = [];
            var dt = TASKEDIT_NS.xmltoJSON($.parseXML(obj.CheckLists)),
				tmp = [];
            if (dt != null) {
                if (dt.constructor === objectConstructor) {
                    if (dt.path.p != null) {
                        if (dt.path.p.constructor === objectConstructor) {
                            tmp.push(dt.path.p);
                        } else if (dt.path.p.constructor === arrayConstructor) {
                            tmp = dt.path.p;
                        }
                    }
                }
                if (tmp.length > 0) {
                    for (var j = 0, ver; ver = tmp[j++];) {
                        taskModifieddata.CheckList.push({
                            "Id": ver["@attr"]['Id'],
                            "IsExisting": ver["@attr"]['IsExisting'] == 0 ? false : true,
                            "Name": ver["@attr"]['Name'] == ("" || null || undefined) ? "" : ver["@attr"]['Name'],
                            "SortOrder": ver["@attr"]['SortOrder'],
                            "OwnerName": ver["@attr"]['OwnerName'] == ("" || null || undefined) ? "" : ver["@attr"]['OwnerName'],
                            "UserName": ver["@attr"]['UserName'] == ("" || null || undefined) ? "" : ver["@attr"]['UserName'],
                            "Status": ver["@attr"]['Status'] == 0 ? false : true,
                            "CompletedOnValue": ver["@attr"]['CompletedOnValue'] == ("" || null || undefined) ? "" : ver["@attr"]['CompletedOnValue']
                        });
                    }
                    $scope.PageFullData.checklist = taskModifieddata.CheckList;
                }
            }
        }
        TASKEDIT_NS._formPhasewithStepsandMembers = function (obj) {
            taskModifieddata.phasestepstate = [];
            var tmpobj = [],
				memobj = [],
				phase = {}, step = {}, statusobj = {};
            if ($scope.PageFullData.phases != null && $scope.PageFullData.phases != undefined) {
                for (var i = 0, ph = {}; ph = $scope.PageFullData.phases[i++];) {
                    phase = {};
                    phase.Id = ph.ID;
                    phase.Name = ph.Name;
                    phase.Description = ph.Description;
                    phase.EntityID = ph.EntityID;
                    phase.CreatedDate = ph.CreatedDate;
                    phase.SortOrder = ph.SortOrder;
                    phase.actionPermit = false;
                    phase.steps = [];
                    phase.statusclass = "phs-approved";
                    if ($scope.PageFullData.steps != null && $scope.PageFullData.steps != undefined) {
                        tmpobj = $.grep($scope.PageFullData.steps, function (rel) {
                            return rel.PhaseId == ph.ID;
                        })
                    } else {
                        tmpobj = null
                    }
                    if (tmpobj != null) {
                        for (var j = 0, st = {}; st = tmpobj[j++];) {
                            step = {};
                            step.stepname = st.Name;
                            step.id = st.ID;
                            step.Description = st.Description;
                            step.PhaseId = st.PhaseId;
                            step.Duration = st.Duration;
                            step.EsitimatedDelivery = dateFormat(new Date(st.CreatedDate).addDays(st.Duration), $scope.DefaultSettings.DateFormat)
                            step.MinApproval = st.MinApproval;
                            step.IsMandatory = st.IsMandatory;
                            step.CreatedDate = st.CreatedDate;
                            step.SortOrder = st.SortOrder;
                            step.status = TASKEDIT_NS._giveBackStepStatus(st.ID);
                            step.Roles = st.Roles;
                            step.RoleName = '';
                            if ($scope.UserRoles != null && $scope.UserRoles != undefined) {
                                if ($scope.UserRoles.length > 0) {
                                    step.RoleName = "";
                                    var roleNames = $.grep($scope.UserRoles, function (e) {
                                        return e.ID == st.Roles
                                    });
                                    if (roleNames.length > 0) {
                                        step.RoleName = roleNames[0].Caption;
                                    }
                                }
                            }
                            step.members = [];
                            memobj = $.grep($scope.PageFullData.Assignees, function (rel) {
                                return rel.Stepid == st.ID;
                            })
                            for (var k = 0, mem = {}; mem = memobj[k++];) {
                                step.members.push({
                                    "ID": mem.Id,
                                    "StepID": mem.Stepid,
                                    "PhaseID": mem.PhaseId,
                                    "FirstName": mem.FirstName,
                                    "ApprovalStatus": mem.ApprovalStatus,
                                    "Email": mem.Email,
                                    "RoleID": mem.RoleID,
                                    "Userid": mem.Userid,
                                    "TaskID": mem.TaskID,
                                    "Name": mem.Name,
                                    "Flag": mem.Flag,
                                    "ApprovalRoleName": mem.ApprovalRoleName
                                });
                            }
                            phase.steps.push(step);
                        }
                        statusobj = TASKEDIT_NS._giveBackPhaseStatus(ph.ID);
                        phase.statusclass = statusobj.statusclass;
                        phase.statuscode = statusobj.status;
                    }
                    taskModifieddata.phasestepstate.push(phase);
                }
            }
            $scope.PageFullData.phasestepstate = taskModifieddata.phasestepstate;
            for (var i = 0; i < $scope.PageFullData.phasestepstate.length; i++) {
                var maxDuration = 0;
                for (var j = 0; j < $scope.PageFullData.phasestepstate[i].steps.length; j++) {
                    maxDuration = parseInt($scope.PageFullData.phasestepstate[i].steps[j].Duration) > maxDuration ? parseInt($scope.PageFullData.phasestepstate[i].steps[j].Duration) : maxDuration;
                }
                $scope.PageFullData.phasestepstate[i].maxduration = maxDuration;
            }
            TASKEDIT_NS._decidePhaseParallelAction(obj);
        }
        TASKEDIT_NS._currentPhaseAssigneesStatus = function (obj) {
            var phase = {}, inprogressPhases = [], memobj = {}, memcount = 0;
            if ($scope.PageFullData.phasestepstate.length > 0) {
                phase = $.grep($scope.PageFullData.phasestepstate, function (rel) {
                    return rel.statuscode == 0;
                })[0];
                if (phase != null) {
                    if (phase.statuscode == 0) {
                        for (var i = 0, st = {}; st = phase.steps[i++];) {
                            if (st.status == 0) {
                                for (var k = 0; k < st.members.length; k++) {
                                    if (st.members[k].ApprovalStatus == 0 || st.members[k].ApprovalStatus == null) {
                                        st.members[k].ApprovalStatus = 1;
                                        st.status = 1;
                                        phase.statuscode = 1;
                                        phase.statusclass = "phs-inprogress";
                                    }
                                }
                            }
                        }
                    }
                }
                //make all inprogress phase member sttaus is inprogress
                inprogressPhases = $.grep($scope.PageFullData.phasestepstate, function (rel) { return rel.statuscode == 1; });
                if (inprogressPhases.length > 0) {
                    for (var p = 0, phase; phase = inprogressPhases[p++];) {
                        if (phase.statuscode == 1) {
                            for (var i = 0, st = {}; st = phase.steps[i++];) {
                                if (st.status == 1) {
                                    for (var k = 0; k < st.members.length; k++) {
                                        if (st.members[k].ApprovalStatus == 0 || st.members[k].ApprovalStatus == null) {
                                            st.members[k].ApprovalStatus = 1;
                                            st.status = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                for (var m = 0, mem; mem = $scope.PageFullData.Assignees[m++];) {
                    if (mem.ApprovalStatus == 0) {
                        mem.ApprovalStatus = 1;
                    }
                }
            }
        }
        TASKEDIT_NS._formInactiveAssigneesStatus = function (obj) {
            var phase = {}, memobj = {}, memcount = 0;
            if ($scope.PageFullData.phasestepstate.length > 0) {
                for (var i = 0, phase = {}; phase = $scope.PageFullData.phasestepstate[i++];) {
                    if (phase != null) {
                        if (phase.statuscode == 5) {
                            for (var j = 0, st = {}; st = phase.steps[j++];) {
                                if (st.status == 5) {
                                    for (var k = 0; k < st.members.length; k++) {
                                        if (st.members[k].ApprovalStatus == 0 || st.members[k].ApprovalStatus == null) {
                                            st.members[k].ApprovalStatus = 10;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                for (var m = 0, mem; mem = $scope.PageFullData.Assignees[m++];) {
                    if (obj.TaskStatus == 5) {
                        if (mem.ApprovalStatus == 0 || mem.ApprovalStatus == 1) {
                            mem.ApprovalStatus = 10;
                        }
                    } else if (obj.TaskStatus == 8) {
                        if (mem.ApprovalStatus == 0 || mem.ApprovalStatus == 1) {
                            mem.ApprovalStatus = 10;
                        }
                    } else {
                        if (mem.ApprovalStatus == 0) {
                            mem.ApprovalStatus = 1;
                        }
                    }
                }
            }
        }
        TASKEDIT_NS._decideStepStatus = function (obj) {
            var memobj = [],
				memcount = 0,
				tempObj = [];
            if ($scope.PageFullData.steps != null && $scope.PageFullData.steps != undefined) {
                for (var i = 0, st = {}; st = $scope.PageFullData.steps[i++];) {
                    memobj = $.grep($scope.PageFullData.Assignees, function (rel) {
                        return rel.Stepid == st.ID;
                    });
                    memcount = memobj.length;
                    if (memcount > 0) {
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus === "5";
                        })
                        if (tempObj.length > 0) {
                            st.status = 5;
                            st.statusclass = "sts rejected";
                            continue;
                        }
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus === '3';
                        })
                        if (tempObj.length >= st.MinApproval) {
                            st.status = 3;
                            st.statusclass = "sts approved";
                            continue;
                        }
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus === '3';
                        })
                        if (tempObj.length == memcount) {
                            st.status = 3;
                            st.statusclass = "sts approved";
                            continue;
                        }
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus == 4;
                        })
                        if (tempObj.length == memcount) {
                            st.status = 4;
                            st.statusclass = "sts unabletocomplete";
                            continue;
                        }
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus === '0';
                        })
                        if (tempObj.length == memcount) {
                            st.status = 0;
                            st.statusclass = "sts unassigned";
                            continue;
                        }
                        tempObj = $.grep(memobj, function (rel) {
                            return rel.ApprovalStatus === '3';
                        })
                        if (tempObj.length == memcount) {
                            st.status = 3;
                            st.statusclass = "sts approved";
                            continue;
                        } else if (memobj.length > 0) {
                            st.status = 1;
                            st.statusclass = "sts inProgress";
                            continue;
                        }
                    } else {
                        st.status = 0;
                        st.unassigned = "sts unassigned";
                        continue;
                    }
                }
            }
        }
        TASKEDIT_NS._giveBackStepStatus = function (stepid) {
            var statusObj = $.grep($scope.PageFullData.steps, function (rel) {
                return rel.ID == stepid;
            })[0];
            return statusObj.status;
        }
        TASKEDIT_NS._giveBackPhaseStatus = function (phaseId) {
            var stepObj = [],
				stausobj = [];
            stepObj = $.grep($scope.PageFullData.steps, function (rel) {
                return rel.PhaseId == phaseId;
            });
            if (stepObj.length > 0) {
                stausobj = $.grep(stepObj, function (rel) {
                    return rel.status == 5;
                });
                if (stausobj.length > 0) {
                    return {
                        status: 5,
                        statusclass: "phs-rejected"
                    };
                }
                stausobj = $.grep(stepObj, function (rel) {
                    return rel.status == 3;
                });
                if (stausobj.length == stepObj.length) {
                    return {
                        status: 3,
                        statusclass: "phs-approved"
                    };
                }
                stausobj = $.grep(stepObj, function (rel) {
                    return rel.status == 4;
                });
                if (stausobj.length == stepObj.length) {
                    return {
                        status: 4,
                        statusclass: "phs-unabletocomplete"
                    };
                }
                stausobj = $.grep(stepObj, function (rel) {
                    return rel.status == 0;
                });
                if (stausobj.length == stepObj.length) {
                    return {
                        status: 0,
                        statusclass: "phs-notstarted"
                    };
                } else return {
                    status: 1,
                    statusclass: "phs-inprogress"
                };
            } else return {
                status: 0,
                statusclass: "phs-notstarted"
            };
        }
        TASKEDIT_NS._pushnewStep = function (obj) { }
        TASKEDIT_NS._pushnewMember = function (obj) { }
        TASKEDIT_NS._deletemember = function (obj) { }
        TASKEDIT_NS._decideUserStatus = function (obj) { }
        TASKEDIT_NS._decideTaskProgressName = function (obj) {
            if (taskModifieddata.Assignees.length > 0) {
                if (obj.EntityTaskListID == 0) {
                    var responsedMembers = $.grep($scope.PageFullData.Assignees, function (rel) {
                        return rel.ApprovalStatus != 0;
                    });
                    if (obj.TaskType != 2 && (obj.TaskStatus != 0 && obj.TaskStatus != 3 && obj.TaskStatus != 2)) {
                        if (obj.TaskStatus == 2 || obj.TaskStatus == 3 || obj.TaskStatus == 4) {
                            taskModifieddata.overview.ProgressCount = "";
                        } else {
                            taskModifieddata.overview.ProgressCount = "(" + responsedMembers.length.toString() + "/" + $scope.PageFullData.Assignees.length.toString() + ")";
                        }
                    } else {
                        if (taskModifieddata.CheckList.length > 0 && obj.TaskStatus == 1) {
                            var taskchecklistCount = taskModifieddata.CheckList.length;
                            var completedChecklists = $.grep(taskModifieddata.CheckList, function (rel) {
                                return rel.Status == true;
                            });
                            taskModifieddata.overview.ProgressCount = "(" + completedChecklists.length.toString() + "/" + taskchecklistCount.toString() + ")";
                        } else {
                            taskModifieddata.overview.ProgressCount = "";
                        }
                    }
                }
            }
            if (taskModifieddata.CheckList.length > 0 && obj.TaskStatus == 0 && obj.TaskType == 2) {
                var completedChecklists = $.grep(taskModifieddata.CheckList, function (rel) {
                    return rel.Status == true;
                });
                taskModifieddata.overview.ProgressCount = "(" + completedChecklists.length.toString() + "/" + taskModifieddata.CheckList.length.toString() + ")";
            }
            $scope.PageFullData.overview.ProgressCount = taskModifieddata.overview.ProgressCount;
        }
        TASKEDIT_NS._decideTaskStatus = function (obj) {
            if (obj.TaskStatus != 8) taskModifieddata.overview.StatusName = TASKEDIT_NS.taskStatus[obj.TaskStatus].replace("_", " ");
            else taskModifieddata.overview.StatusName = obj.TaskType == 3 ? "Approved" : "Completed";
            $scope.PageFullData.overview.StatusName = taskModifieddata.overview.StatusName;
        }
        TASKEDIT_NS._decidePhaseParallelAction = function (obj) {
            var fof = false,
				allowedArr = [0, 1, 5];
            if ($scope.PageFullData.phasestepstate != null) {
                for (var i = 0, ph; ph = $scope.PageFullData.phasestepstate[i++];) {
                    if (fof == false) {
                        if (allowedArr.indexOf(ph.statuscode) > -1) {
                            fof = true;
                            ph.actionPermit = true;
                            for (var j = 0, st; st = ph.steps[j++];) {
                                st.actionPermit = true;
                            }
                        }
                    } else {
                        ph.actionPermit = false;
                        for (var j = 0, st; st = ph.steps[j++];) {
                            st.actionPermit = false;
                        }
                    }
                }
            }
        }
        TASKEDIT_NS._decideStepSerialAction = function (obj) { }
        TASKEDIT_NS._userTaskAction = function (obj) { }
        TASKEDIT_NS._userTaskWorkPermit = function (obj) {
            var ispresent = [],
				isThisMemberPresent = [],
				approvebtnArray = [],
				revokebtnarray = [],
				rejectbtnArray = [],
				completebtnArray = [],
				unabletocompletebtnArray = [];
            ispresent = $.grep($scope.PageFullData.Assignees, function (rel) {
                return rel.Userid == $.cookie('UserId');
            });
            taskModifieddata.approvebtnArray = [];
            taskModifieddata.rejectbtnArray = [];
            taskModifieddata.unabletocompletebtnArray = [];
            taskModifieddata.revokebtnarray = [];
            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.ShowRevokeButton = false;
            $scope.viewbutton = false;
            if (obj.TaskType == 3 || obj.TaskType == 32 || obj.TaskType == 36) {
                if ($scope.PageFullData.phasestepstate != null && $scope.PageFullData.phasestepstate.length > 0) {
                    for (var i = 0, ph; ph = $scope.PageFullData.phasestepstate[i++];) {
                        if (ph.actionPermit == true) {
                            for (var j = 0, st; st = ph.steps[j++];) {
                                if (st.actionPermit == true) {
                                    ispresent = $.grep(st.members, function (rel) {
                                        return rel.Userid == $.cookie('UserId');
                                    });
                                    if (ispresent.length > 0) {
                                        if (obj.TaskStatus == 1 && ispresent.length >= 0) {
                                            if (st.status == 1) {
                                                if ($scope.EntityLockTask != true) {
                                                    if (ispresent.length == 1) {
                                                        if (ispresent[0].ApprovalStatus == 0) {
                                                            $scope.ShowApproveBtn = true;
                                                            $scope.ShowRejectedBtn = true;
                                                            $scope.ShowUnabletoCompleteBtn = true;
                                                            var isapprovepresent = [],
																isrejectpresent = [],
																isunabletocompletepresent = [];
                                                            isapprovepresent = $.grep(taskModifieddata.approvebtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.approvebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isrejectpresent = [];
                                                            isrejectpresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.rejectbtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isunabletocompletepresent = [];
                                                            isunabletocompletepresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.unabletocompletebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                        if (ispresent[0].ApprovalStatus == 1) {
                                                            $scope.ShowApproveBtn = true;
                                                            $scope.ShowRejectedBtn = true;
                                                            $scope.ShowUnabletoCompleteBtn = true;
                                                            var isapprovepresent = [],
																isrejectpresent = [],
																isunabletocompletepresent = [];
                                                            isapprovepresent = $.grep(taskModifieddata.approvebtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.approvebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isrejectpresent = [];
                                                            isrejectpresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.rejectbtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isunabletocompletepresent = [];
                                                            isunabletocompletepresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.unabletocompletebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (st.status == 0) {
                                                if ($scope.EntityLockTask != true) {
                                                    if (ispresent.length == 1) {
                                                        if (ispresent[0].ApprovalStatus == 0) {
                                                            $scope.ShowApproveBtn = true;
                                                            $scope.ShowRejectedBtn = true;
                                                            $scope.ShowUnabletoCompleteBtn = true;
                                                            var isapprovepresent = [],
																isrejectpresent = [],
																isunabletocompletepresent = [];
                                                            isapprovepresent = $.grep(taskModifieddata.approvebtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.approvebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isrejectpresent = [];
                                                            isrejectpresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.rejectbtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isunabletocompletepresent = [];
                                                            isunabletocompletepresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.unabletocompletebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (st.status == 1) {
                                                if ($scope.EntityLockTask != true) {
                                                    if (ispresent.length == 1) {
                                                        if (ispresent[0].ApprovalStatus == 0) {
                                                            $scope.ShowApproveBtn = true;
                                                            $scope.ShowRejectedBtn = true;
                                                            $scope.ShowUnabletoCompleteBtn = true;
                                                            var isapprovepresent = [],
																isrejectpresent = [],
																isunabletocompletepresent = [];
                                                            isapprovepresent = $.grep(taskModifieddata.approvebtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.approvebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isrejectpresent = [];
                                                            isrejectpresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.rejectbtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isunabletocompletepresent = [];
                                                            isunabletocompletepresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.unabletocompletebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (obj.TaskStatus == 0 && ispresent.length >= 0) {
                                            if (st.status == 0) {
                                                if ($scope.EntityLockTask != true) {
                                                    if (ispresent.length == 1) {
                                                        if (ispresent[0].ApprovalStatus == 0) {
                                                            $scope.ShowApproveBtn = true;
                                                            $scope.ShowRejectedBtn = true;
                                                            $scope.ShowUnabletoCompleteBtn = true;
                                                            var isapprovepresent = [],
																isrejectpresent = [],
																isunabletocompletepresent = [];
                                                            isapprovepresent = $.grep(taskModifieddata.approvebtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.approvebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isrejectpresent = [];
                                                            isrejectpresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.rejectbtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                            isunabletocompletepresent = [];
                                                            isunabletocompletepresent = $.grep(taskModifieddata.rejectbtnArray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isapprovepresent.length == 0) {
                                                                taskModifieddata.unabletocompletebtnArray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (obj.TaskStatus == 8) {
                                            $scope.ShowCompleteBtn = false;
                                            $scope.ShowApproveBtn = false;
                                            $scope.ShowRejectedBtn = false;
                                            $scope.ShowWithdrawBtn = false;
                                            $scope.ShowUnabletoCompleteBtn = false;
                                            $scope.ShowRevokeButton = false;
                                        }
                                        if (obj.TaskStatus == 5 && ispresent.length >= 1) {
                                            if (st.status == 5 && ispresent.length >= 1) {
                                                if ($scope.EntityLockTask != true) {
                                                    if (ispresent.length == 1) {
                                                        if (ispresent[0].ApprovalStatus == 5) {
                                                            $scope.ShowApproveBtn = false;
                                                            $scope.ShowRejectedBtn = false;
                                                            $scope.ShowUnabletoCompleteBtn = false;
                                                            $scope.ShowRevokeButton = true;
                                                            var isrevokepresent = [];
                                                            isrevokepresent = $.grep(taskModifieddata.revokebtnarray, function (rel) {
                                                                return rel.StepId == st.id;
                                                            });
                                                            if (isrevokepresent.length == 0) {
                                                                taskModifieddata.revokebtnarray.push({
                                                                    StepName: st.stepname,
                                                                    StepId: st.id
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (ispresent.length > 0) {
                        if (obj.TaskStatus == 1 && ispresent.length >= 0) {
                            if ($scope.EntityLockTask != true) {
                                if (ispresent.length == 1) {
                                    if (ispresent[0].ApprovalStatus == 0) {
                                        $scope.ShowApproveBtn = true;
                                        $scope.ShowRejectedBtn = true;
                                        $scope.ShowUnabletoCompleteBtn = true;
                                    }
                                    if (ispresent[0].ApprovalStatus == 1) {
                                        $scope.ShowApproveBtn = true;
                                        $scope.ShowRejectedBtn = true;
                                        $scope.ShowUnabletoCompleteBtn = true;
                                    }
                                }
                            }
                        }
                        if (obj.TaskStatus == 0 && ispresent.length >= 0) {
                            if ($scope.EntityLockTask != true) {
                                if (ispresent.length == 1) {
                                    if (ispresent[0].ApprovalStatus == 0) {
                                        $scope.ShowApproveBtn = true;
                                        $scope.ShowRejectedBtn = true;
                                        $scope.ShowUnabletoCompleteBtn = true;
                                    }
                                }
                            }
                        }
                        if (obj.TaskStatus == 8) {
                            $scope.ShowCompleteBtn = false;
                            $scope.ShowApproveBtn = false;
                            $scope.ShowRejectedBtn = false;
                            $scope.ShowWithdrawBtn = false;
                            $scope.ShowUnabletoCompleteBtn = false;
                            $scope.ShowRevokeButton = false;
                        }
                        if (obj.TaskStatus == 5 && ispresent.length >= 1) {
                            if ($scope.EntityLockTask != true) {
                                if (ispresent.length == 1) {
                                    if (ispresent[0].ApprovalStatus == 5) {
                                        $scope.ShowApproveBtn = false;
                                        $scope.ShowRejectedBtn = false;
                                        $scope.ShowUnabletoCompleteBtn = false;
                                        $scope.ShowRevokeButton = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (obj.TaskType == 2) {
                if (obj.TaskStatus == 1 && ispresent.length >= 1) {
                    if ($scope.EntityLockTask != true) {
                        if (ispresent.length == 1) {
                            if (ispresent[0].ApprovalStatus == 0) {
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowUnabletoCompleteBtn = true;
                            }
                            if (ispresent[0].ApprovalStatus == 1) {
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowUnabletoCompleteBtn = true;
                            }
                        }
                    }
                }
                if (obj.TaskStatus == 4 && ispresent.length >= 1) {
                    if ($scope.EntityLockTask != true) {
                        if (ispresent.length == 1) {
                            if (ispresent[0].ApprovalStatus == 0) {
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowUnabletoCompleteBtn = true;
                            }
                            if (ispresent[0].ApprovalStatus == 1) {
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowUnabletoCompleteBtn = true;
                            }
                        }
                    }
                }
                if (obj.TaskStatus == 2 || obj.TaskStatus == 8) {

                    if ($scope.EntityLockTask != true && $scope.PageFullData.owner.Userid == $.cookie('UserId')) {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                        $scope.ShowRevokeButton = true;
                    }
                    else if (ispresent.length == 1) {
                        if (ispresent[0].ApprovalStatus == 2 || ispresent[0].ApprovalStatus == 3) {
                            if ($scope.EntityLockTask != true) {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = true;
                            }
                        } else {
                            $scope.ShowCompleteBtn = false;
                            $scope.ShowApproveBtn = false;
                            $scope.ShowRejectedBtn = false;
                            $scope.ShowWithdrawBtn = false;
                            $scope.ShowUnabletoCompleteBtn = false;
                            $scope.ShowRevokeButton = false;
                        }
                    } else {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                        $scope.ShowRevokeButton = false;
                    }
                }
                if (obj.TaskStatus == 8) {
                    if (ispresent != null) if (ispresent.length == 1) {
                        if (ispresent[0].ApprovalStatus == 2 || ispresent[0].ApprovalStatus == 3) {
                            if ($scope.EntityLockTask != true) {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = true;
                            }
                        } else {
                            $scope.ShowCompleteBtn = false;
                            $scope.ShowApproveBtn = false;
                            $scope.ShowRejectedBtn = false;
                            $scope.ShowWithdrawBtn = false;
                            $scope.ShowUnabletoCompleteBtn = false;
                            $scope.ShowRevokeButton = false;
                        }
                    } else {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                        $scope.ShowRevokeButton = false;
                    }
                }
            } else if (obj.TaskType == 31) {
                if (obj.TaskStatus == 1 && ispresent.length >= 1) {
                    if ($scope.EntityLockTask != true) {
                        if (ispresent.length == 1) {
                            if (ispresent[0].ApprovalStatus == 0) {
                                $scope.ShowApproveBtn = false;
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowUnabletoCompleteBtn = true;
                                $scope.ShowRevokeButton = false;
                            }
                            if (ispresent[0].ApprovalStatus == 1) {
                                $scope.ShowApproveBtn = false;
                                $scope.ShowCompleteBtn = true;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowUnabletoCompleteBtn = true;
                                $scope.ShowRevokeButton = false;
                            }
                        }
                    }
                }
                if (obj.TaskStatus == 8) {
                    $scope.ShowCompleteBtn = false;
                    $scope.ShowApproveBtn = false;
                    $scope.ShowRejectedBtn = false;
                    $scope.ShowWithdrawBtn = false;
                    $scope.ShowUnabletoCompleteBtn = false;
                    $scope.ShowRevokeButton = false;
                }
            } else if (obj.TaskType == 37) {
                if ($scope.EntityLockTask != true) {
                    TaskEditService.GetDirectEntityMember(obj.EntityID).then(function (EntityMemberList) {
                        if (EntityMemberList.Response != null) {
                            var IsEntityUser = $.grep(EntityMemberList.Response, function (rel) {
                                return rel.Userid == $.cookie('UserId');
                            });
                            if (ispresent.length >= 1 || IsEntityUser.length >= 1) {
                                $scope.viewbutton = true;
                            }
                        }
                    });
                }
            }
            $scope.PageFullData.approvebtnArray = taskModifieddata.approvebtnArray;
            $scope.PageFullData.revokebtnarray = taskModifieddata.revokebtnarray;
            $scope.PageFullData.rejectbtnArray = taskModifieddata.rejectbtnArray;
            $scope.PageFullData.completebtnArray = taskModifieddata.completebtnArray;
            $scope.PageFullData.unabletocompletebtnArray = taskModifieddata.unabletocompletebtnArray;
        }
        TASKEDIT_NS._clearTempVersion = function () {
            if ($scope.PageFullData.btnCreatenewVersion == true) {
                var isPresence = $scope.PageFullData.versions[$scope.PageFullData.versions.length - 1];
                if (isPresence != null) {
                    $scope.PageFullData.versions.splice($.inArray(isPresence, $scope.PageFullData.versions), 1);
                    $scope.IsSelectAppDocExist = false;
                    angular.element('#approvDoc').attr('src', '');
                    $scope.PageFullData.approveDocumentAssetId = 0;
                }
                $scope.PageFullData.btnCreatenewVersion = false;
            }
        }
        TASKEDIT_NS._gobackVersionDetails = function (obj) {
            if ($scope.PageFullData.btnCreatenewVersion == true) {
                var isPresence = $scope.PageFullData.versions[$scope.PageFullData.versions.length - 1];
                if (isPresence != null) {
                    $scope.PageFullData.versions.splice($.inArray(isPresence, $scope.PageFullData.versions), 1);
                    $scope.IsSelectAppDocExist = false;
                    angular.element('#approvDoc').attr('src', '');
                    $scope.PageFullData.approveDocumentAssetId = 0;
                }
                $scope.PageFullData.btnCreatenewVersion = false;
            }
            var srcobj = {
                taskid: $scope.PageFullData.taskid,
                versionNo: obj.VersionNo
            };
            TaskEditService.GetTaskDetails(srcobj).then(function (data) {
                TASKEDIT_NS._markCurrentVersion(obj.VersionNo);
                taskModifieddata.overview.versionDetails.CurrentVersionNo = obj.VersionNo;
                $scope.PageFullData.overview.versionDetails.CurrentVersionNo = obj.VersionNo;
                if (data != null) {
                    taskResponsedata = JSON.parse(data.Response.m_Item1);
                    TASKEDIT_NS._formOverview(taskResponsedata[0]);
                    TASKEDIT_NS._formAssignees(taskResponsedata[0].TotaTaskAssignees);
                    TASKEDIT_NS._formPhases(taskResponsedata[0].TaskPhases);
                    TASKEDIT_NS._formPhaseSteps(taskResponsedata[0].TaskSteps);
                    TASKEDIT_NS._decideStepStatus(taskResponsedata[0]);
                    TASKEDIT_NS._formPhasewithStepsandMembers(taskResponsedata[0]);
                    TASKEDIT_NS._currentPhaseAssigneesStatus(taskResponsedata[0]);
                    TASKEDIT_NS._formInactiveAssigneesStatus(taskResponsedata[0]);
                    TASKEDIT_NS._formChecklists(taskResponsedata[0]);
                    TASKEDIT_NS._decideTaskStatus(taskResponsedata[0]);
                    TASKEDIT_NS._decideTaskProgressName(taskResponsedata[0]);
                    TASKEDIT_NS._formChecklists(taskResponsedata[0].TaskVersionDetails);
                    $timeout(function () {
                        $scope.$broadcast('LoadTaskMetadata', {
                            TaskId: $scope.PageFullData.taskid,
                            Response: (data.Response.m_Item2 != null && data.Response.m_Item2 != "") ? JSON.parse(data.Response.m_Item2) : [],
                            IsLock: !($scope.PageFullData.CurrentActualTaskVersion == obj.VersionNo && !$scope.IsLock),
                            visibleID: false,
                            visibleName: false
                        });
                    }, 1000);
                } else { }
            });
        }
        TASKEDIT_NS._createTemporaryVersion = function (obj) {
            $scope.IsSelectAppDocExist = false;
            $scope.PageFullData.btnCreatenewVersion = true;
            var datstartval = Date.create('today');
            $scope.PageFullData.versions.push({
                "title": "V" + parseInt($scope.PageFullData.versions.length + 1),
                "VersionNo": parseInt($scope.PageFullData.versions.length + 1),
                "id": TASKEDIT_NS.guid(),
                "CreatedDate": dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat),
                "InitiatorId": $cookies['UserId'],
                "TaskId": $scope.PageFullData.taskid,
                initiatorName: $cookies['Username']
            });
            TASKEDIT_NS._markCurrentVersion(parseInt($scope.PageFullData.versions.length));
            $scope.PageFullData.versions = taskModifieddata.versions;
        }
        TASKEDIT_NS._createNewVersion = function (obj) {
            $scope.PageFullData.taskVersioncreating = true;
            obj = {
                taskid: $scope.PageFullData.taskid,
                versionNo: 0,
                FileDetails: {
                    AssetID: $scope.PageFullData.approveDocumentAssetId
                }
            };
            TaskEditService.CreateNewVersion(obj).then(function (data) {
                if (data != null) {
                    TASKEDIT_NS.initialize($scope.PageFullData.taskid);
                    setTimeout(function () { loadTaskMetdata(); }, 1000);
                    $scope.PageFullData.versions = taskModifieddata.versions;
                    TASKEDIT_NS._markCurrentVersion($scope.PageFullData.versions[$scope.PageFullData.versions.length - 1].VersionNo);
                    $scope.PageFullData.versions = taskModifieddata.versions;
                    taskModifieddata.overview.versionDetails.CurrentVersionNo = $scope.PageFullData.versions[$scope.PageFullData.versions.length - 1].VersionNo;
                    $scope.PageFullData.overview.versionDetails.CurrentVersionNo = $scope.PageFullData.versions[$scope.PageFullData.versions.length - 1].VersionNo;
                    $scope.PageFullData.btnCreatenewVersion = false;
                } else {
                    $scope.PageFullData.taskVersioncreating = false;
                }
                TaskListRefresh = $timeout(function () {
                    $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                }, 50)
                TASKEDIT_NS.initialize($scope.PageFullData.overview.TaskId);
                setTimeout(function () { loadTaskMetdata(); }, 1000);
            });
        }
        TASKEDIT_NS._dispatchTaskStatus = function (obj) { }
        TASKEDIT_NS._changeTaskDueDate = function (obj) { }
        TASKEDIT_NS._changeTaskName = function (obj) { }
        TASKEDIT_NS._changeTaskDescription = function (obj) { }
        TASKEDIT_NS._reflectTaskOverView = function (obj) { }
        TASKEDIT_NS._dragdropTaskOverviewSteps = function (obj) { }
        TASKEDIT_NS._deleteSteps = function (obj) { }
        TASKEDIT_NS._deletePhase = function (obj) { }
        TASKEDIT_NS.ShowTaskAssigneeTooltipContent = function (memObj) {
            var html = "";
            var avlUsers = [];
            if ($scope.PageFullData.phasestepstate.length > 0) {
                html += '<div class="apprvlTaskMembersTooltip">';
                for (var i = 0, ph; ph = $scope.PageFullData.phasestepstate[i++];) {
                    if (TASKEDIT_NS.isMemberPresent(ph)) {
                        for (var j = 0, st; st = ph.steps[j++];) {
                            for (var k = 0, mem; mem = st.members[k++];) {
                                var classObj = TASKEDIT_NS.decidetooltipclass(mem.ApprovalStatus);
                                if (!avlUsers.contains(parseInt(mem.Userid))) {
                                    avlUsers.push(parseInt(mem.Userid));
                                    html += '<div><div class="groupAssigneeTooltipContent"><img position="right" src="Handlers/UserImage.ashx?id=' + parseInt(mem.Userid) + '"><span>' + mem.Name + '</span></div></div>';
                                }
                            }
                        }
                    }
                }
                html += '</div>';
            } else {
                if (memObj != null) {
                    for (var i = 0, mem = {}; mem = memObj[i++];) {
                        var classObj = TASKEDIT_NS.decidetooltipclass(mem.ApprovalStatus);
                        html += '<div class="memberBox"><div class="groupAssigneeTooltipContent"><img position="right" src="Handlers/UserImage.ashx?id=' + parseInt(mem.Userid) + '"><span class="usrNm">' + mem.Name + '</span></div></div>';
                    }
                }
            }
            return html;
        }
        TASKEDIT_NS.decidetooltipclass = function (statuscode) {
            if (statuscode == 2) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'completed'
                }
            }
            if (statuscode == 3) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'approved'
                }
            }
            if (statuscode == 4) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'unabletocomplete'
                }
            }
            if (statuscode == 5) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'rejected'
                }
            }
            if (statuscode == 0) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'unassigned'
                }
            }
            if (statuscode == 10) {
                return {
                    'buttonclass': 'icon-circle',
                    'classname': 'unassigned'
                }
            }
            if (statuscode == 1) {
                return {
                    'buttonclass': 'icon-arrow-right',
                    'classname': 'inprogress'
                }
            }
        }
        TASKEDIT_NS.isMemberPresent = function (obj) {
            var isStepMember = false,
				ispresent = [];
            for (var i = 0, step; step = obj.steps[i++];) {
                if (step.members.length > 0) {
                    isStepMember = true;
                    break;
                }
            }
            return isStepMember;
        }
        TASKEDIT_NS.calcLIheight = function () {
            var maxLIHeight = Math.max.apply(null, $(".phG-List > li > .phG-container > .phG-content > ul.phGsteps-List").map(function () {
                return $(this).height();
            }).get());
            $(".phG-List > li > .phG-container > .phG-content").css('height', ((maxLIHeight + 31) + 'px'));
        }
        TASKEDIT_NS.ViewExternalTask = function () {
            var param = {
                'TaskId': taskModifieddata.overview.TaskId,
                'UserEmail': $cookies['UserEmail']
            };
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            TASKEDIT_NS.OpenWindowWithPost("DocumentViewer.aspx", winprops, "NewFile", param);
        }
        TASKEDIT_NS.OpenWindowWithPost = function (url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("DocumentViewer.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        TASKEDIT_NS._ReorderOverviewStructure = function () {
            var phaseArray = [],
				stepArray = [];
            if ($scope.PageFullData.phasestepstate != null && $scope.PageFullData.phasestepstate != undefined) {
                for (var i = 0, ph = {}; ph = $scope.PageFullData.phasestepstate[i++];) {
                    phaseArray.push({
                        ID: ph.Id,
                        SortOrder: i
                    });
                    for (var j = 0, st = {}; st = ph.steps[j++];) {
                        stepArray.push({
                            ID: st.id,
                            SortOrder: j
                        });
                    }
                }
            }
            TaskEditService.ReorderOverviewStructure({
                phase: phaseArray,
                stepArray: stepArray,
                TaskId: $scope.PageFullData.overview.TaskId
            }).then(function (data) {
                if (data != null) {
                    TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                    $scope.ReorderOverviewStructure = false;
                } else { }
            });
        }
        $scope._PopulateStepInfo = function (stepinfo) {
            $scope.PageFullData.ShowPhaseStep = false;
            $scope.PageFullData.currentStep = {};
            $scope.PageFullData.currentStep = stepinfo;
        }
        TASKEDIT_NS.guid = function () {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "0" + p.substr(0, 4) + "0" + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        }
        TASKEDIT_NS.taskStatus = {
            0: 'Unassigned',
            1: 'In Progress',
            2: 'Completed',
            3: 'Approved',
            4: 'Unable to Complete',
            5: 'Rejected',
            6: 'Withdrawn',
            7: 'Not_Applicable',
            8: 'ForcefulComplete',
            9: 'RevokeTask'
        }
        TASKEDIT_NS.closePopup = function () {
            if (taskeditTimer) $timeout.cancel(taskeditTimer);
            var taskModifieddata = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            $scope.PageFullData = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.muitaskFlowEditCtrl']"));
            $scope.$emit('ClearAllTimerAttachment');
            var returnObj = { "TaskLiStID": $scope.PageFullData.overview.TaskListID, "EntityID": $scope.PageFullData.overview.EntityID };
            $scope.$emit('muiRefreshTasklistByID', returnObj);// Reload Tasklist
            $(".taskImprovementPopup ").hide();
            $modalInstance.dismiss('cancel');

        }
        $scope.HTMLDecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };
        TASKEDIT_NS.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            } else {
                return "";
            }
        };
        $scope.ActionHandler = TASKEDIT_NS;

        TASKEDIT_NS._isApprovalReportEnables = function () {
            TaskEditService.IsApprovalReportEnables().then(function (data) {
                if (data.Response != null) {
                    $scope.IsApprovalReportEnables = data.Response;
                }
            });
        }

        function formViewDetilsFromResponse(response) {
            var versions = [];
        }
        $scope.$on("$destroy", function () {
            if (taskeditTimer) $timeout.cancel(taskeditTimer);
            var taskModifieddata = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            $scope.PageFullData = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.muitaskFlowEditCtrl']"));
        });
        $('#taskeditblock').on('hidden.bs.modal', function () {
            var taskModifieddata = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            $scope.PageFullData = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.muitaskFlowEditCtrl']"));
        })
        $('#taskeditblock').on('hidden', function () {
            var taskModifieddata = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            $scope.PageFullData = {
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: []
            };
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.muitaskFlowEditCtrl']"));
        })

        function getScopeValues() {
            $scope.AssetFilesObj = {
                IsTaskLock: false,
                HideAddAsset: true,
                OrderBy: 0,
                PageNo: 1
            };
            $scope.attachmentAddIsLock = !$scope.AssetFilesObj.IsTaskLock;
            $scope.processingsrcobj = {
                processingid: $scope.PageFullData.taskid,
                processingplace: "task",
                processinglock: $scope.AssetFilesObj.IsTaskLock
            };
        }
        setTimeout(function () { loadpage(items); }, 300);
        $scope.ok = function () {
            $modalInstance.close($scope.selected.item);
        };
        $scope.cancel = function () {
            $(".taskImprovementPopup ").hide();
            $modalInstance.dismiss('cancel');
        };


        function loadpage(items) {
            var obj = items;
            $scope.IsEnableassetcreatepopup = false;
            var taskeditTimer = "";
            var CurrentCheckListName = "";
            $scope.TaskEntityLockTask = false;
            var taskModifieddata = {
                taskid: 0,
                CurrentActualTaskVersion: 1,
                timestamp: "",
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: [],
                approvebtnArray: [],
                revokebtnarray: [],
                rejectbtnArray: [],
                completebtnArray: [],
                unabletocompletebtnArray: [],
                ShowPhaseStep: true,
                currentStep: {},
                btnCreatenewVersion: false
            };
            $scope.PageFullData = {
                TaskName: "",
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: [],
                approvebtnArray: [],
                revokebtnarray: [],
                rejectbtnArray: [],
                completebtnArray: [],
                unabletocompletebtnArray: [],
                ShowPhaseStep: true,
                currentStep: {},
                approveDocumentAssetId: 0
            };
            taskModifieddata.taskid = obj.TaskId;
            $scope.PageFullData.taskid = obj.TaskId;
            taskModifieddata.timestamp = TASKEDIT_NS.guid();
            $scope.PageFullData.timestamp = TASKEDIT_NS.guid();
            //$("#taskImprovementPopup").modal('show');
            TASKEDIT_NS.initialize(taskModifieddata.taskid);
            setTimeout(function () { loadTaskMetdata(); }, 1000);
            getmembers();
            $scope.processingsrcobj = {
                processingid: $stateParams.ID,
                processingplace: "task",
                processinglock: $scope.IsLock
            };
            $scope.loadpopupTimeout = $timeout(function () {
                getScopeValues();
                $scope.$broadcast('ReloadAssetView', {
                    processingsrcobj: $scope.processingsrcobj,
                    AssetFilesObj: $scope.AssetFilesObj,
                    EntityID: $scope.PageFullData.taskParentId
                });
                $scope.$broadcast('loadNewsFeed', {
                    ID: obj.TaskId,
                    EntityID: obj.EntityId
                });
            }, 300);
        }
        function loadTaskMetdata() {
            $scope.$broadcast('LoadTaskMetadata', {
                TaskId: $scope.PageFullData.taskid,
                Response: [],
                IsLock: !(($scope.PageFullData.CurrentActualTaskVersion == $scope.PageFullData.overview.currentVersion) && !$scope.IsLock),
                visibleID: false,
                visibleName: false,
                TaskTypeId: $scope.PageFullData.TaskTypeID,
                ParentId: $scope.PageFullData.taskParentId

            });
        }
        $scope.$on("pingTaskEdit", function (event, obj) {
            $scope.IsEnableassetcreatepopup = false;
            var taskeditTimer = "";
            var CurrentCheckListName = "";
            $scope.TaskEntityLockTask = false;
            var taskModifieddata = {
                taskid: 0,
                CurrentActualTaskVersion: 1,
                timestamp: "",
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: [],
                approvebtnArray: [],
                revokebtnarray: [],
                rejectbtnArray: [],
                completebtnArray: [],
                unabletocompletebtnArray: [],
                ShowPhaseStep: true,
                currentStep: {},
                btnCreatenewVersion: false
            };
            $scope.PageFullData = {
                TaskName: "",
                taskid: 0,
                versions: [],
                overview: {},
                checklist: [],
                Assignees: [],
                phasestepstate: [],
                approvebtnArray: [],
                revokebtnarray: [],
                rejectbtnArray: [],
                completebtnArray: [],
                unabletocompletebtnArray: [],
                ShowPhaseStep: true,
                currentStep: {},
                approveDocumentAssetId: 0
            };
            taskModifieddata.taskid = obj.TaskId;
            $scope.PageFullData.taskid = obj.TaskId;
            taskModifieddata.timestamp = TASKEDIT_NS.guid();
            $scope.PageFullData.timestamp = TASKEDIT_NS.guid();
            $("#taskImprovementPopup").modal('show');
            TASKEDIT_NS.initialize(taskModifieddata.taskid);
            setTimeout(function () { loadTaskMetdata(); }, 1000);
            getmembers();
            $scope.loadpopupTimeout = $timeout(function () {
                getScopeValues();
                $scope.$broadcast('ReloadAssetView', {
                    processingsrcobj: $scope.processingsrcobj,
                    AssetFilesObj: $scope.AssetFilesObj
                });
                $scope.$broadcast('loadNewsFeed', {
                    ID: obj.TaskId,
                    EntityID: obj.EntityId
                });
            }, 300);
        });
        $scope.TaskTitleChange = function (taskname) {
            //$scope.name = $('<div />').html(taskname).text();
            $scope.taskcommonInfo.name = $('<div />').html(taskname).text();
            $timeout(function () {
                $("#inputTaskNameChange").focus().select()
            }, 10);
            $timeout(function () {
                $("#taskUnassignedName").focus().select()
            }, 10);
            $timeout(function () {
                $("#TaskApprovalTaskNameChange").focus().select()
            }, 10);
            $timeout(function () {
                $("#TaskReviewtask").focus().select()
            }, 10);
        };
        $scope.TaskDescChange2 = function (Description) {
            $scope.PageFullData.overview.DescriptionChange = $('<div />').html(Description.Description).text() == "-" ? "" : $('<div />').html(Description.Description).text();
            $timeout(function () {
                $("#TextDes").focus().select();
                $("#TextDes1").focus().select();
            }, 10);
        };
        $scope.TaskNoteChange2 = function (Note) {
            $scope.PageFullData.overview.NoteChange = $('<div />').html(Note.Note).text() == "-" ? "" : $('<div />').html(Note.Note).text();
            $timeout(function () {
                $("#TaskNote").focus().select()
            }, 10);
        };
        $scope.saveEntityTaskDetails = function (type) {
            $scope.EntityTaskChangesHolder = {
                "TaskName": "",
                "TaskDescription": "",
                "DueDate": ""
            };
            if (type == "nam") {
                if ($scope.taskcommonInfo.name != "") {
                    if ($scope.PageFullData.overview.name != $scope.taskcommonInfo.name) {
                        $scope.EntityTaskChangesHolder.TaskName = $scope.taskcommonInfo.name;
                        $scope.updatetaskEdit = true;
                    } else return false;
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1967.Caption'));
                    return false;
                }
            } else if (type == "des") {
                if ($scope.PageFullData.overview.Description != $scope.PageFullData.overview.DescriptionChange) {
                    $scope.EntityTaskChangesHolder.TaskDescription = $scope.PageFullData.overview.DescriptionChange;
                    $scope.updatetaskEdit = true;
                } else return false;
            } else if (type == "note") {
                if ($scope.PageFullData.overview.Note != $scope.PageFullData.overview.NoteChange) {
                    $scope.EntityTaskChangesHolder.TaskNote = $scope.PageFullData.overview.NoteChange;
                    $scope.updatetaskEdit = true;
                } else return false;
            } else if (type == "due") {
                if ($scope.PageFullData.overview.dueDateChange != "" && $scope.PageFullData.overview.dueDateChange != undefined && $scope.PageFullData.overview.dueDateChange != null) {
                    $scope.EntityTaskChangesHolder.DueDate = ConvertDateToString($scope.PageFullData.overview.dueDateChange);
                    var tempduedate1 = dateFormat(new Date.create($scope.oldDuedateVal.toString(), $scope.format), $scope.format);
                    if ((tempduedate1) == dateFormat($scope.PageFullData.overview.dueDateChange, $scope.format)) {
                        $scope.updatetaskEdit = false;
                    } else {
                        $scope.updatetaskEdit = true;
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1969.Caption'));
                    return false;
                }
            } else if (type == "unassignedue") {
                if ($scope.PageFullData.overview.dueDateChange != "" && $scope.PageFullData.overview.dueDateChange != undefined && $scope.PageFullData.overview.dueDateChange != null) {
                    $scope.EntityTaskChangesHolder.DueDate = ConvertDateToString($scope.PageFullData.overview.dueDateChange);
                    var tempduedate1 = formatteddateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.format);
                    if (dateFormat(tempduedate1) == dateFormat($scope.PageFullData.overview.dueDateChange, $scope.format)) {
                        $scope.updatetaskEdit = false;
                    } else {
                        $scope.updatetaskEdit = true;
                    }
                } else {
                    $scope.EntityTaskChangesHolder.DueDate = '';
                }
            }
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.PageFullData.overview.TaskId;
            TaskStatusData.TaskName = $scope.EntityTaskChangesHolder.TaskName;
            TaskStatusData.Description = $scope.EntityTaskChangesHolder.TaskDescription;
            TaskStatusData.Note = $scope.EntityTaskChangesHolder.TaskNote;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate;
            TaskStatusData.entityID = $scope.PageFullData.overview.EntityID;
            TaskStatusData.actionfor = type;
            TaskEditService.UpdatetaskEntityTaskDetailsInTaskTab(TaskStatusData).then(function (TaskStatusResult) {
                $scope.fired = false;
                if (TaskStatusResult.StatusCode == 405) {
                    if ($scope.updatetaskEdit != false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    $scope.fired = false;
                } else {
                    if (TaskStatusResult.Response == false) {
                        if ($scope.updatetaskEdit != false) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                        $scope.fired = false;
                    } else {
                        if ($scope.EntityTaskChangesHolder.DueDate != '') {
                            $scope.PageFullData.overview.dueIn = dateDiffBetweenDates($scope.PageFullData.overview.dueDateChange);
                            $scope.PageFullData.overview.dueDateChange = $scope.EntityTaskChangesHolder.DueDate;
                            $scope.fired = false;
                        } else {
                            $scope.PageFullData.overview.dueDateChange = '-';
                            $scope.PageFullData.overview.DueDate = '-';
                            $scope.PageFullData.overview.dueIn = 0;
                            $scope.fired = false;
                        }
                        $scope.EntityTaskChangesHolder = {
                            "TaskName": "",
                            "TaskDescription": "",
                            "DueDate": "",
                            "Note": ""
                        };
                        if ($scope.name != undefined && $scope.name != "" && $scope.name != null) {
                            $scope.PageFullData.overview.name = $scope.name;
                        }
                        if ($scope.PageFullData.overview.DescriptionChange != undefined && $scope.PageFullData.overview.DescriptionChange != "" && $scope.PageFullData.overview.DescriptionChange != null) {
                            $scope.PageFullData.overview.Description = $scope.PageFullData.overview.DescriptionChange;
                        }
                        if ($scope.PageFullData.overview.NoteChange != undefined && $scope.PageFullData.overview.NoteChange != "" && $scope.PageFullData.overview.NoteChange != null) {
                            $scope.PageFullData.overview.Note = $scope.PageFullData.overview.NoteChange;
                        }
                        if ($scope.updatetaskEdit != false) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        }
                        $scope.fired = false;
                        $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                    }
                }
                TaskListRefresh = $timeout(function () {
                    $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                }, 100)
                TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
            });
        }
        $scope.DatepickerShow = function (CurrentDueDate, CurrentId) {
            $scope.oldDuedateVal = CurrentDueDate
            $scope.showdatepicker = true;
            $scope.tempDate = true;
            if (CurrentDueDate != "-" && CurrentDueDate != null && CurrentDueDate != undefined) {
                $scope.PageFullData.overview.dueDateChange = CurrentDueDate;
                if (CurrentDueDate.length < 2) {
                    $('#' + CurrentId).val('');
                    $('#' + CurrentId).attr('AlredyDateSelected', 'true');
                } else {
                    $('#' + CurrentId).attr('AlredyDateSelected', 'false');
                }
                $timeout(function () {
                    $('[id=' + CurrentId + ']:enabled:visible:first').focus().select()
                }, 100);
                $timeout(function () { }, 100);
            }
        }
        $scope.saveEmptyDueDate = function (type) {
            $scope.EntityTaskChangesHolder.DueDate = '';
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.PageFullData.overview.TaskId;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
            TaskEditService.UpdatetaskEntityTaskDueDate(TaskStatusData).then(function (TaskStatusResult) {
                $scope.fired = false;
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $scope.fired = false;
                } else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $scope.fired = false;
                    } else {
                        if (type == "unassignedue") {
                            if ($scope.EntityTaskChangesHolder.DueDate != '') {
                                $scope.PageFullData.overview.dueIn = dateDiffBetweenDates($scope.PageFullData.overview.dueDateChange);
                                $scope.PageFullData.overview.DueDate = $scope.EntityTaskChangesHolder.DueDate;
                                $scope.fired = false;
                            } else {
                                $scope.PageFullData.overview.dueDateChange = '-';
                                $scope.EntityTaskChangesHolder.DueDate = '-';
                                $scope.PageFullData.overview.dueIn = 0;
                                $scope.fired = false;
                            }
                        } else {
                            $scope.PageFullData.overview.dueDateChange = '-';
                            $scope.EntityTaskChangesHolder.DueDate = '-';
                            $scope.PageFullData.overview.dueIn = 0;
                            $scope.fired = false;
                        }
                        $scope.PageFullData.overview.DueDate = $scope.PageFullData.overview.dueDateChange;
                        $scope.EntityTaskChangesHolder = {
                            "TaskName": "",
                            "TaskDescription": "",
                            "DueDate": "",
                            "Note": ""
                        };
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $scope.fired = false;
                    }
                }
                $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
            });
        }
        $scope.approvechangedate = function () {
            $scope.showdatepicker = false;
            if ($scope.PageFullData.overview.dueDateChange != null) {
                var test = isValidDate($scope.PageFullData.overview.dueDateChange.toString(), $scope.DefaultSettings.DateFormat.toString());
                if (test) {
                    if ($scope.PageFullData.overview.dueDateChange != null) {
                        $scope.PageFullData.overview.DueDate = dateFormat($scope.PageFullData.overview.dueDateChange, $scope.DefaultSettings.DateFormat.toString());
                        $scope.saveEntityTaskDetails('due');
                        $scope.editable = '';
                        $('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                        $('#ApproveTaskDatepicker').val('');
                    }
                } else {
                    $scope.PageFullData.overview.dueDateChange = new Date.create();
                    $scope.PageFullData.overview.dueDateChange = null;
                    $('#ApproveTaskDatepicker').val('');
                    $scope.fired = false;
                    $scope.saveEmptyDueDate('due');
                    $scope.editable = '';
                    $('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                    $('#ApproveTaskDatepicker').val('');
                    $scope.PageFullData.overview.dueDateChange = "-";
                    $scope.fired = false;
                }
            } else {
                $scope.PageFullData.overview.DueDate = $scope.PageFullData.overview.dueDateChange;
                $scope.saveEmptyDueDate('due');
                $scope.editable = '';
                $('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                $('#ApproveTaskDatepicker').val('');
                $scope.PageFullData.overview.dueDateChange = "-";
                $scope.fired = false;
            }
            $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
        }
        $scope.addAdditionalMember = function () {
            $('#ngTaskEditAddTaskMember').removeClass('disabled');
            $scope.AddUnAssign = false;
            $scope.globalEntityMemberLists = [];
            $scope.globalTempcount = 1;
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluserForEditTask = "";
            $scope.AutoCompleteSelectedObj = [];
            SeperateUsers();
            $("#AddTaskMemberModal").modal("show");

            function SeperateUsers() {
                var UniqueTaskMembers = [];
                var dupes = {};
                if ($scope.TaskMemberList != null) {
                    for (var k = 0, el; el = $scope.TaskMemberList[k++];) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            UniqueTaskMembers.push(el);
                        }
                    }
                }
                var UniqueEntityMembers = [];
                dupes = {};
                if ($scope.EntityMemberList != null) {
                    for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                        if (el.IsInherited != true) if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            UniqueEntityMembers.push(el);
                        }
                    }
                }
                $scope.RemainMembers = [];
                if (UniqueEntityMembers != null) {
                    for (var j = 0, value; value = UniqueEntityMembers[j++];) {
                        var MemberList = $.grep(UniqueTaskMembers, function (e) {
                            return e.Userid == value.Userid
                        });
                        if (MemberList.length == 0) {
                            $scope.RemainMembers.push(value);
                        }
                    }
                }
            }
            $scope.DeletableAssigneeName = "";
            $scope.SetDeletebleAssigneName = function (Name) {
                $scope.DeletableAssigneeName = Name;
            };
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };

        function dateDiffBetweenDates(dateTo) {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();
            var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
        }
        $scope.UpdateTaskStatus = function (StatusID, stepid) {
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.PageFullData.overview.TaskId;
            TaskStatusData.Status = StatusID;
            TaskStatusData.EntityID = $scope.PageFullData.overview.EntityID;
            TaskStatusData.Stepid = stepid;
            TaskStatusData.userId = parseInt($cookies['UserId']);
            TaskEditService.UpdateTaskStatusInTaskTab(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                } else {
                    TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    TaskListRefresh = $timeout(function () {
                        $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                    }, 100)
                }
            });
        }

        function getmembers() {
            $scope.globalEntityMemberLists = [];
            TaskEditService.GetApprovalRoles().then(function (UserRoles) {
                $scope.UserRoles = UserRoles.Response;
            });
        }
        $scope.PhaseDuration = [{
            ID: 1,
            Caption: "+1 days"
        }, {
            ID: 2,
            Caption: "+2 days"
        }, {
            ID: 3,
            Caption: "+3 days"
        }, {
            ID: 4,
            Caption: "+4 days"
        }, {
            ID: 5,
            Caption: "+5 days"
        }, {
            ID: 6,
            Caption: "+6 days"
        }, {
            ID: 7,
            Caption: "+1 week"
        }, {
            ID: 14,
            Caption: "+2 weeks"
        }, {
            ID: 21,
            Caption: "+3 weeks"
        }, {
            ID: 30,
            Caption: "+1 month"
        }];
        $scope.PhaseMinApproval = ["1", "2", "3", "4", "5", "6", "7", "10", "15", "20", "25", "30"];
        $scope.AddStep = function (phaseID) {
            $scope.stepPopup = "Add stage";
            $scope.stepSavebutton = "Save";
            $scope.IsAdd = true;
            $scope.showPhaseStepContext = false;
            $scope.showPhaseContext = false;
            $scope.TaskTemplateStepName = "";
            $scope.TaskTemplateStepDescription = "";
            $scope.TaskTemplateStepduration = "";
            $scope.TaskTemplateStepMinApproval = "";
            $scope.TaskTemplateSteproles = "";
            $scope.TaskTemplateIsMandatory = false;
            $scope.CurrentPhaseID = phaseID;
            $("#addPhaseStep").modal("show");
        }
        $scope.EditStep = function (obj) {
            $scope.stepSavebutton = "Update";
            $scope.stepPopup = "Edit stage";
            $scope.showPhaseStepContext = false;
            $scope.showPhaseContext = false;
            $scope.TaskTemplateStepName = obj.stepname;
            $scope.TaskTemplateStepDescription = obj.Description;
            $scope.TaskTemplateStepduration = obj.Duration;
            $scope.TaskTemplateStepMinApproval = obj.MinApproval;
            $scope.TaskTemplateSteproles = obj.Roles;
            $scope.TaskTemplateIsMandatory = false;
            $scope.CurrentPhaseID = obj.PhaseId;
            $("#addPhaseStep").modal("show");
        }
        $scope.addMemberPopup = function (CurrPhaseID, currStepID, taskID, StepName) {
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluserForEditTask = '';
            $("#taskGlobaluserForEditTask").val("");
            $scope.globalEntityMemberLists = [];
            $scope.CurrStepID = currStepID;
            $scope.CurrPhaseID = CurrPhaseID;
            $scope.CurrPhaseStepID = currStepID;
            $scope.currentEntityID = taskID;
            $scope.ApprovalTaskGlobalUsers.globalUser = [];
            $scope.ApprovalTaskGlobalUsers.EntityUser = [];
            $scope.TaskEntityMemberLists = [];
            $scope.TaskGlobalMemberLists = [];
            $scope.showPhaseStepContext = false;
            $scope.showPhaseContext = false;
            $scope.CurrPhaseStepName = StepName;
            $scope.addAdditionalMemberforTask(CurrPhaseID, currStepID, taskID, $scope.CurrPhaseStepName);
        }
        $scope.phaseContextmenu = function (type, Obj, taskID) {
            if (type == 'Phase') {
                $scope.showPhaseStepContext = false;
                $scope.showMemberContext = false;
                $scope.showPhaseContext = true;
                $scope.CurrPhaseID = Obj.Id;
                $scope.CurrPhaseStepID = 0;
            } else if (type == 'Step') {
                $scope.showMemberContext = false;
                $scope.showPhaseContext = false;
                $scope.showPhaseStepContext = true;
                $scope.CurrPhaseStepID = Obj.id;
                $scope.CurrPhaseStepName = Obj.stepname;
                $scope.CurrPhaseID = Obj.PhaseId;
                $scope.IsCurrStepMandatory = Obj.IsMandatory == 0 ? false : true;
                $scope.CurrStepRoles = Obj.Roles;
                $scope.CurrStepObj = Obj;
            } else if (type == 'Member') {
                $scope.showPhaseStepContext = false;
                $scope.showPhaseContext = false;
                $scope.showMemberContext = true;
                $scope.memberName = Obj.Name;
                $scope.memberID = Obj.ID != undefined ? Obj.ID : Obj.Id;
                $scope.CurrPhaseID = Obj.PhaseId == (null || undefined) ? 0 : Obj.PhaseId;
                $scope.CurrPhaseStepID = Obj.Stepid == (null || undefined) ? 0 : Obj.Stepid;
            }
            $scope.CurrEntityID = taskID;
        }
        $scope.AddPhase = function () {
            $scope.phaseAddText = "Add Phase";
            $scope.showPhaseStepContext = false;
            $scope.showPhaseContext = false;
            $scope.Phasename = "";
            $scope.Phasedescription = "";
            $("#AddNewPhasePopup").modal("show");
            $scope.CurrPhaseID = 0;
        }
        $scope.EditPhase = function (phaseID, taskID) {
            var PhaseInfo = $.grep($scope.PageFullData.phases, function (e) {
                return e.ID == phaseID
            });
            $scope.Phasename = PhaseInfo[0].Name;
            $scope.Phasedescription = PhaseInfo[0].Description;
            $scope.phaseAddText = "Update Phase";
            $("#AddNewPhasePopup").modal("show");
            $scope.CurrPhaseID = phaseID;
        }
        $scope.AddNewPhase = function (ID) {
            if ($scope.Phasename == "" || $scope.Phasename == undefined || $scope.Phasename == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_5772.Caption'));
            } else {
                var newPhase = {};
                newPhase.ID = ID;
                newPhase.Name = $scope.Phasename;
                newPhase.Description = $scope.Phasedescription;
                newPhase.EntityID = $scope.PageFullData.overview.TaskId;
                newPhase.sortorder = 1;
                if ($scope.PageFullData.phasestepstate != null) newPhase.sortorder = $scope.PageFullData.phasestepstate.length;
                newPhase.IsFirstPhase = true;
                newPhase.CurrUser = parseInt($.cookie('UserId'), 10);
                if ($scope.PageFullData.phases != null && $scope.PageFullData.phases != undefined) {
                    var PhasesOfCurrentTask = $.grep($scope.PageFullData.phases, function (e) {
                        return e.EntityID == newPhase.EntityID
                    })
                    if (PhasesOfCurrentTask.length > 0) {
                        newPhase.IsFirstPhase = false;
                    }
                }
                TaskEditService.AddNewPhase(newPhase).then(function (Result) {
                    if (Result.Response) {
                        TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                        $("#AddNewPhasePopup").modal("hide");
                        if (ID == 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_5813.Caption'));
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_5814.Caption'));
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_5815.Caption'));
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                    TaskListRefresh = $timeout(function () {
                        $scope.$emit('RefreshTaskByID', $scope.PageFullData.overview.TaskId);
                    }, 100)
                });
            }
        }
        $scope.AddNewPhaseStep = function (phaseID) {
            var alertmessage = "";
            if ($scope.TaskTemplateStepName == "" || $scope.TaskTemplateStepName == undefined || $scope.TaskTemplateStepName == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_5772.Caption'));
            } else {
                var addnewstep = {};
                addnewstep.PhaseID = phaseID;
                if ($scope.CurrPhaseStepID == 0) {
                    if ($scope.TaskTemplateStepduration == "" || $scope.TaskTemplateStepduration == undefined || $scope.TaskTemplateStepduration == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5773.Caption'));
                    } else if ($scope.PhaseMinApproval == "" || $scope.PhaseMinApproval == undefined || $scope.PhaseMinApproval == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5774.Caption'));
                    } else if ($scope.TaskTemplateSteproles == "" || $scope.TaskTemplateSteproles == undefined || $scope.TaskTemplateSteproles == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5775.Caption'));
                    } else {
                        addnewstep.Name = $scope.TaskTemplateStepName;
                        addnewstep.Description = $scope.TaskTemplateStepDescription;
                        addnewstep.Duration = $scope.TaskTemplateStepduration;
                        addnewstep.MinApproval = $scope.TaskTemplateStepMinApproval;
                        addnewstep.IsMandatory = $scope.TaskTemplateIsMandatory;
                        addnewstep.StepID = $scope.CurrPhaseStepID;
                        var phase = {};
                        phase = $.grep($scope.PageFullData.phasestepstate, function (rel) {
                            return rel.Id == 0;
                        })[0];
                        if (phase != null) {
                            addnewstep.SortOrder = phase.steps.length + 1;
                        } else addnewstep.SortOrder = 0;
                        addnewstep.Roles = "";
                        if ($scope.TaskTemplateSteproles != null) {
                            addnewstep.Roles = $scope.TaskTemplateSteproles;
                        }
                    }
                    TaskEditService.AddPhaseStep(addnewstep).then(function (AddStepResult) {
                        if (AddStepResult.Response != 0) {
                            TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                            $("#addPhaseStep").modal("hide");
                            $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                            NotifySuccess($translate.instant('LanguageContents.Res_5816.Caption'));
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_5830.Caption'));
                        }
                    });
                } else {
                    addnewstep.Name = $scope.TaskTemplateStepName;
                    addnewstep.Description = $scope.TaskTemplateStepDescription;
                    addnewstep.StepID = $scope.CurrPhaseStepID;
                    TaskEditService.AddPhaseStep(addnewstep).then(function (AddStepResult) {
                        if (AddStepResult.Response != 0) {
                            TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                            $("#addPhaseStep").modal("hide");
                            $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                            NotifySuccess($translate.instant('LanguageContents.Res_5817.Caption'));
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_5831.Caption'));
                        }
                    });
                }
            }
        }
        $scope.EditStep = function (stepObj) {
            $scope.stepPopup = "Edit stage";
            $scope.stepSavebutton = "Update";
            $scope.IsAdd = false;
            $scope.TaskTemplateStepName = stepObj.stepname;
            $scope.TaskTemplateStepDescription = stepObj.Description;
            $scope.CurrPhaseStepID = stepObj.id;
            $scope.CurrentPhaseID = stepObj.PhaseId;
            $("#addPhaseStep").modal("show");
        }
        $scope.RemovePhase = function (phaseID, EntityID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_5738.Caption')+"?", function (result) {
                if (result) {
                    $scope.showPhaseContext = !$scope.showPhaseContext;
                    TaskEditService.RemovePhase(phaseID, EntityID).then(function (result) {
                        if (result.Response == true) {
                            NotifySuccess($translate.instant('LanguageContents.Res_5803.Caption'));
                            TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_5804.Caption'));
                        }
                        $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                    })
                }
            })
        }
        $scope.RemovePhaseStep = function (stepID, phaseID, IsMandatory, TaskID) {
            var StepsofCurrentPhase = $.grep($scope.PageFullData.steps, function (e) {
                return e.PhaseId == phaseID;
            })
            if (StepsofCurrentPhase.length > 1) {
                if (IsMandatory == false) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5736.Caption')+"?", function (result) {
                        if (result) {
                            $scope.showPhaseStepContext = !$scope.showPhaseStepContext;
                            TaskEditService.DeletePhaseStep(stepID, TaskID).then(function (result) {
                                if (result.Response == true) {
                                    NotifySuccess($translate.instant('LanguageContents.Res_5818.Caption'));
                                    TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_5832.Caption'));
                                }
                                $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                            })
                        }
                    })
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_5776.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5737.Caption'));
            }
        }
        $scope.selectedMembers = [];
        $scope.selectMember = function (event, memberID) {
            var checkbox = event.target;
            memberID = parseInt(memberID);
            if (checkbox.checked) {
                var tempMemberID = $.grep($scope.selectedMembers, function (e) {
                    return e == memberID
                });
                if (tempMemberID.length < 1) {
                    $scope.selectedMembers.push(memberID);
                }
            } else {
                var index = $scope.selectedMembers.indexOf(memberID);
                if (index > -1) {
                    $scope.selectedMembers.splice(index, 1);
                }
            }
        }
        $scope.deleteGlobalTaskMembers = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
                    }, 100);
                }
            });
        };
        $scope.selectAllMember = function (event, stepID) {
            $scope.checkAll = event.target;
            $scope.selMember = $scope.checkAll.checked;
            $scope.selectedMembers = [];
            if ($scope.checkAll.checked) {
                TaskEditService.getAllMemberIDbyStepID(stepID).then(function (memberIDs) {
                    if (memberIDs.Response != null) {
                        $scope.selectedMembers = [];
                        $scope.selectedMembers = memberIDs.Response;
                    }
                })
            }
        }
        $scope.checkedStatus = function () {
            if ($scope.checkAll != undefined) {
                var selectAllChecked = "checkbox";
                if ($scope.checkAll.checked) {
                    selectAllChecked += " checked";
                }
            }
            return selectAllChecked;
        }
        $scope.RemindNofifiaction = function (memberID, stepID, TaskID) {
            var sendnotify = {};
            sendnotify.entityID = TaskID;
            sendnotify.stepID = stepID;
            sendnotify.Members = [];
            if (memberID == 0) {
                TaskEditService.getAllMemberIDbyStepID(stepID).then(function (memberIDs) {
                    if (memberIDs.Response != null) {
                        $scope.selectedMembers = [];
                        sendnotify.Members = memberIDs.Response;
                        bootbox.confirm($translate.instant('LanguageContents.Res_5792.Caption'), function (confirmation) {
                            if (confirmation) {
                                TaskEditService.sendReminderNotificationforTaskPhase(sendnotify).then(function (IsSent) {
                                    if (IsSent.Response) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_5819.Caption'));
                                        $scope.selectedMembers = [];
                                    } else {
                                        NotifyError($translate.instant('LanguageContents.Res_5833.Caption'));
                                    }
                                })
                            }
                        });
                    }
                })
            } else {
                sendnotify.Members.push(memberID);
                bootbox.confirm($translate.instant('LanguageContents.Res_5834.Caption') + $scope.memberName + "'", function (confirmation) {
                    if (confirmation) {
                        TaskEditService.sendReminderNotificationforTaskPhase(sendnotify).then(function (IsSent) {
                            if (IsSent.Response) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.selectedMembers = [];
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_5833.Caption'));
                            }
                        })
                    }
                });
            }
        }
        $scope.RemoveApprover = function (memberID, stepID, TaskID) {
            var removeApprover = {};
            removeApprover.entityID = TaskID;
            removeApprover.stepID = stepID;
            removeApprover.Members = [];
            TaskEditService.getAllMemberIDbyStepID(stepID).then(function (memberIDs) {
                if (memberIDs.Response != null) {
                    removeApprover.Members = memberIDs.Response;
                    bootbox.confirm($translate.instant('LanguageContents.Res_5793.Caption'), function (confirmation) {
                        if (confirmation) {
                            TaskEditService.RemovePhaseStepApprovar(removeApprover).then(function (IsRemoved) {
                                if (IsRemoved.Response) {
                                    TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                                    NotifySuccess($translate.instant('LanguageContents.Res_5820.Caption'));
                                    $scope.selectedMembers = [];
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_5835.Caption'));
                                }
                                $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                            })
                        }
                    });
                }
            });
        }
        $scope.RemoveSingleApprover = function (memberID, stepID, TaskID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_5794.Caption') + $scope.memberName + $translate.instant('LanguageContents.Res_5795.Caption'), function (confirmation) {
                if (confirmation) {
                    TaskEditService.DeleteTaskMemberById(memberID, $scope.PageFullData.overview.TaskId).then(function (MemResult) {
                        if (MemResult.Response) {
                            TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                            NotifySuccess($translate.instant('LanguageContents.Res_5821.Caption'));
                            TaskListRefresh = $timeout(function () {
                                $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                            }, 50)
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_5836.Caption'));
                        }
                    });
                }
            })
        }
        $scope.searchFilter = "";
        TASKEDIT_NS._duration = function CalculateDuration(version) {
            var allPhase = [];
            var phasemaximumDuration = [];
            var estimatedDelivery = new Date();
            var currentVersionDate = new Date();
            $scope.Duration = 0;
            $scope.Workload = 0;
            for (var i = 0; i < $scope.PageFullData.phasestepstate.length; i++) {
                var Duration = [];
                for (var j = 0; j < $scope.PageFullData.phasestepstate[i].steps.length; j++) {
                    Duration.push(parseInt($scope.PageFullData.phasestepstate[i].steps[j].Duration))
                    $scope.Workload += parseInt($scope.PageFullData.phasestepstate[i].steps[j].Duration);
                }
                allPhase.push(Duration);
            }
            for (var i = 0; i < allPhase.length; i++) {
                if (allPhase[i].length > 0) {
                    phasemaximumDuration.push(Math.max.apply(Math, allPhase[i]));
                }
            }
            for (var i = 0; i < phasemaximumDuration.length; i++) {
                $scope.Duration += phasemaximumDuration[i];
            }
            if ($scope.PageFullData.versions[version - 1] != null && $scope.PageFullData.versions[version - 1] != undefined) {
                //if ($scope.PageFullData.versions[version - 1].length > 0) {
                if ($scope.PageFullData.versions.length > 0) {
                    currentVersionDate = new Date($scope.PageFullData.versions[version - 1].CreatedDate)
                    estimatedDelivery = currentVersionDate.addDays($scope.Duration);
                    $scope.estimatedDelivery = dateFormat(estimatedDelivery, $scope.GetDefaultSettings.DateFormat);
                }
            }
        }
        $scope.addAdditionalMemberforTask = function (phaseid, stepid, taskID, StepName) {
            LoadMemberRoles();
            GetAllUserRoles();
            if (TempStepID == stepid) {
                TempStepID = stepid;
            } else {
                TempStepID = stepid;
                $scope.TaskEntityMemberLists = [];
                $scope.TaskGlobalMemberLists = [];
            }
            $scope.ApprovalTaskGlobalUsers.EntityUser = [];
            $scope.ApprovalTaskGlobalUsers.globalUser = [];
            $scope.AddUnAssign = false;
            $scope.globalTempcount = 1;
            $scope.AutoCompleteSelectedUserObj.userrole = '';
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluserForEditTask = '';
            $("#taskGlobaluserForEditTask").val("");
            $scope.AutoCompleteSelectedObj = [];
            $scope.AutoCompleteSelectedUserObj = {
                UserSelection: {}
            };
            $("#EditTaskMemberModalPopup").modal("show");
            if (stepid == 0) {
                $scope.addTaskMemberfromGlobal = 1;
                $scope.addTaskMemberfromStep = false;
                SeperateUsers("AddGlobalMember");
            } else {
                SeperateUsers("FromtStep");
                $scope.addTaskMemberfromGlobal = 1;
                $scope.addTaskMemberfromStep = true;
            }
            $scope.taskID = taskID == null ? 0 : taskID;
            $("#EditTaskMemberModalPopup").modal("show");
        }

        function SeperateUsers(type) {
            var UniqueTaskMembers = [];
            var dupes = {};
            if ($scope.TaskMemberList != null) {
                for (var k = 0, el; el = $scope.TaskMemberList[k++];) {
                    if (!dupes[el.Userid]) {
                        dupes[el.Userid] = true;
                        UniqueTaskMembers.push(el);
                    }
                }
            }
            var UniqueEntityMembers = [];
            dupes = {};
            if ($scope.EntityMemberList != null) {
                for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                    if (el.IsInherited != true) if (!dupes[el.Userid]) {
                        dupes[el.Userid] = true;
                        UniqueEntityMembers.push(el);
                    }
                }
            }
            $scope.RemainMembers = [];
            if (UniqueEntityMembers != null) {
                for (var j = 0, value; value = UniqueEntityMembers[j++];) {
                    var MemberList = $.grep(UniqueTaskMembers, function (e) {
                        return e.Userid == value.Userid
                    });
                    if (MemberList.length == 0) {
                        $scope.RemainMembers.push(value);
                    }
                    var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (e) {
                        return e.Userid == value.Userid
                    });
                    if (MemberList.length == 0) {
                        $scope.ApprovalTaskGlobalUsers.EntityUser.push({
                            Userid: value.Userid,
                            UserEmail: value.UserEmail,
                            UserName: value.UserName,
                            FirstName: value.FirstName,
                            LastName: value.LastName,
                            ApprovalRoleName: value.Role,
                            ApprovalId: 0,
                            ApprovalRoleforEntityUser: '',
                            EntityRole: value.Role,
                            Approvalroles: [],
                            ischecked: false
                        });
                    }
                }
            }
        }
        $scope.addUsers = function () {
            var result = $.grep($scope.globalEntityMemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10);
            });
            var entitymemberresult = $.grep($scope.EntityMemberList, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && (e.IsInherited == false);
            });
            if (result.length == 0 && entitymemberresult.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == $scope.AutoCompleteSelectedUserObj.userrole;
                })[0];
                if (membervalues != undefined) {
                    $scope.globalEntityMemberLists.push({
                        "TID": $scope.globalTempcount,
                        "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                        "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                        "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                        "Roleid": 4,
                        "StepRoleid": 0,
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                        "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "CostCentreID": 0
                    });
                    $scope.globalTempcount = $scope.globalTempcount + 1;
                    $scope.AutoCompleteSelectedUserObj.userrole = '';
                    $scope.taskGlobaluserForEditTask = '';
                    $("#taskGlobaluserForEditTask").val("");
                    $scope.AutoCompleteSelectedObj = [];
                    var isExist = [];
                    isExist = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                        return rel.Userid == $scope.AutoCompleteSelectedUserObj.UserSelection.Id;
                    });
                    if (isExist == null || isExist.length == 0) {
                        $scope.ApprovalTaskGlobalUsers.globalUser.push({
                            UserEmail: $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                            Userid: parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                            UserName: $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + " " + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                            FirstName: $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName,
                            LastName: $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                            ApprovalRoleName: membervalues.Caption,
                            ApprovalId: 0,
                            EntityRole: $scope.AutoCompleteSelectedUserObj.userrole,
                            Roleid: membervalues.ID,
                            Approvalroles: [],
                            ischecked: true
                        });
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
                    }
                }
                else {
                    bootbox.alert($translate.instant('LanguageContents.Res_5777.Caption'));
                }
            } else {
                $scope.AutoCompleteSelectedUserObj.userrole = '';
                $scope.taskGlobaluserForEditTask = '';
                $("#taskGlobaluserForEditTask").val("");
                $scope.AutoCompleteSelectedObj = [];
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };
        $scope.deleteGlobalTaskMembers = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
                    }, 100);
                }
            });
        };
        $scope.clearUserFilter = function () {
            userFilter.value = "";
        };
        $scope.ApprovalTaskGlobalUsers = {
            EntityUser: [],
            globalUser: []
        };

        function getEntityApprovalRoleUsers(steproles) {
            var approvalRoles = [];
            if (steproles != null) {
                if (typeof steproles === 'string') {
                    steproles = [steproles];
                }
                if (!isNaN(steproles)) {
                    steproles = [steproles];
                }
            }
            if (steproles != null && steproles.length > 0) {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            } else {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            }
            $scope.addTaskMemberfromGlobal = false;
            $scope.addTaskMemberfromStep = true;
            $scope.ApprovalTaskGlobalUsers.EntityUser = [];
            var jobj = {
                "approvalRoles": approvalRoles,
                EntityId: $scope.TaskGlobalEntityID
            };
            TaskEditService.getTaskApprovalRolesUsers(jobj).then(function (EntityMemberList) {
                $scope.ApprovalTaskGlobalUsers.EntityUser = [];
                if (EntityMemberList.Response != null) {
                    for (var i = 0, mem; mem = EntityMemberList.Response[i++];) {
                        var GroupResult = [],
							memberroles = [],
							roles = [],
							isExist = [];
                        isExist = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                            return rel.Userid == mem.ID;
                        });
                        if (isExist == null || isExist.length == 0) {
                            GroupResult = $.grep(EntityMemberList.Response, function (rel) {
                                return rel.ID == mem.ID;
                            });
                            if (GroupResult.length > 0) {
                                roles = [];
                                for (var j = 0, role; role = GroupResult[j++];) {
                                    roles.push({
                                        "Name": role.ApprovalRoles,
                                        "ID": role.ApprovalId
                                    })
                                }
                            }
                            var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (e) {
                                return e.Userid == mem.ID
                            });
                            if (MemberList.length == 0) {
                                $scope.ApprovalTaskGlobalUsers.EntityUser.push({
                                    UserEmail: mem.Email,
                                    Userid: mem.ID,
                                    UserName: mem.FirstName + " " + mem.LastName,
                                    FirstName: mem.FirstName,
                                    LastName: mem.LastName,
                                    ApprovalRoleName: "",
                                    ApprovalId: 0,
                                    ApprovalRoleforEntityUser: '',
                                    EntityRole: mem.Roles,
                                    Approvalroles: roles,
                                    ischecked: false
                                });
                            }
                        }
                    }
                }
            });
        }

        function getTaskApprovalRolesUsers(steproles) {
            var approvalRoles = [];
            if (steproles != null) {
                if (typeof steproles === 'string') {
                    steproles = [steproles];
                }
                if (!isNaN(steproles)) {
                    steproles = [steproles];
                }
            }
            if (steproles != null && steproles.length > 0) {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            } else {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            }
            $scope.ApprovalTaskGlobalUsers.globalUser = [];
            var jobj = {
                "approvalRoles": approvalRoles,
                EntityId: $scope.TaskGlobalEntityID
            };
            TaskEditService.getTaskApprovalRolesUsers(jobj).then(function (EntityMemberList) {
                $scope.ApprovalTaskGlobalUsers.globalUser = [];
                if (EntityMemberList.Response != null) {
                    for (var i = 0, mem; mem = EntityMemberList.Response[i++];) {
                        var GroupResult = [],
							memberroles = [],
							roles = [],
							isExist = [];
                        isExist = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (rel) {
                            return rel.Userid == mem.ID;
                        });
                        if (isExist == null || isExist.length == 0) {
                            GroupResult = $.grep(EntityMemberList.Response, function (rel) {
                                return rel.ID == mem.ID;
                            });
                            if (GroupResult.length > 0) {
                                roles = [];
                                for (var j = 0, role; role = GroupResult[j++];) {
                                    roles.push({
                                        "Name": role.ApprovalRoles,
                                        "ID": role.ApprovalId
                                    })
                                }
                            }
                            var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (e) {
                                return e.Userid == mem.ID
                            });
                            if (MemberList.length == 0) {
                                $scope.ApprovalTaskGlobalUsers.globalUser.push({
                                    UserEmail: mem.Email,
                                    Userid: mem.ID,
                                    UserName: mem.FirstName + " " + mem.LastName,
                                    FirstName: mem.FirstName,
                                    LastName: mem.LastName,
                                    ApprovalId: 0,
                                    ApprovalRoleName: "",
                                    Approvalroles: roles,
                                    ApprovalRoleforGlobalUser: '',
                                    EntityRoleforGlobalUser: '',
                                    EntityRole: 0,
                                    ischecked: false
                                });
                            }
                        }
                    }
                }
            });
        }
        $scope.HandlingMembers = function (PhaseID, StepID, Stepname) {
            var memberList = [];
            var GlobalMembers = [];
            if ($('#ngTaskEditAddTaskMember').hasClass('disabled')) {
                return;
            }
            $('#ngTaskEditAddTaskMember').addClass('disabled');
            if ($scope.ApprovalTaskGlobalUsers.EntityUser != null) memberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                return rel.ischecked == true;
            });
            if (memberList != null) {
                if (memberList.length > 0) {
                    for (var i = 0, mem; mem = memberList[i++];) {
                        var isPresence = $.grep($scope.TaskEntityMemberLists, function (rel) {
                            return rel.Userid == mem.Userid;
                        })[0];
                        if (isPresence == null || isPresence.length == 0) {
                            mem.StepRoleID = (mem.ApprovalRoleforEntityUser == '') ? 0 : mem.ApprovalRoleforEntityUser;
                            mem.StepId = StepID == undefined ? 0 : StepID;
                            mem.PhaseID = PhaseID == undefined ? 0 : PhaseID;
                            mem.VersionID = $scope.PageFullData.CurrentActualTaskVersion == undefined ? 1 : $scope.PageFullData.CurrentActualTaskVersion;
                            $scope.TaskEntityMemberLists.push(mem);
                        }
                    }
                }
            }
            if ($scope.ApprovalTaskGlobalUsers.globalUser != null) GlobalMembers = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (rel) {
                return rel.ischecked == true;
            });
            if (GlobalMembers.length > 0) {
                for (var i = 0, mem; mem = GlobalMembers[i++];) {
                    var isPresence = $.grep($scope.TaskEntityMemberLists, function (rel) {
                        return rel.Userid == mem.Userid;
                    })[0];
                    if (isPresence == null || isPresence.length == 0) {
                        mem.StepRoleID = (mem.ApprovalRoleforGlobalUser == "" || mem.ApprovalRoleforGlobalUser == null) ? 0 : mem.ApprovalRoleforGlobalUser;
                        mem.EntityRole = (mem.EntityRoleforGlobalUser == null) ? 0 : mem.EntityRoleforGlobalUser;
                        mem.StepId = StepID == undefined ? 0 : StepID;
                        mem.Roleid = mem.Roleid == undefined ? 0 : mem.Roleid;
                        mem.PhaseID = PhaseID == undefined ? 0 : PhaseID;
                        mem.VersionID = $scope.PageFullData.CurrentActualTaskVersion == undefined ? 1 : $scope.PageFullData.CurrentActualTaskVersion;
                        $scope.TaskGlobalMemberLists.push(mem);
                    }
                }
            }
            for (var i = 0, mem; mem = $scope.ApprovalTaskGlobalUsers.globalUser[i++];) {
                mem.ischecked = false;
            }
            for (var i = 0, mem; mem = $scope.ApprovalTaskGlobalUsers.EntityUser[i++];) {
                mem.ischecked = false;
            }
            var memberObj = {};
            memberObj.ParentId = $scope.TaskGlobalEntityID == null ? 0 : $scope.TaskGlobalEntityID;
            memberObj.TaskID = $scope.taskID == null ? 0 : $scope.taskID;
            memberObj.CurrentStepName = Stepname;
            if ($scope.TaskEntityMemberLists.length > 0 || $scope.TaskGlobalMemberLists.length > 0) {
                if ($scope.CurrStepObj != null) {
                    for (var i = 0, mem; mem = $scope.CurrStepObj.members[i++];) {
                        var isPresence = $.grep($scope.TaskEntityMemberLists, function (rel) {
                            return rel.Userid == mem.Userid;
                        })[0];
                        if (isPresence != null) {
                            $scope.TaskEntityMemberLists.splice(isPresence, 1);
                        }
                        var isGlobalPresence = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                            return rel.Userid == mem.Userid;
                        })[0];
                        if (isGlobalPresence != null) {
                            $scope.TaskGlobalMemberLists.splice(isGlobalPresence, 1);
                        }
                    }
                }
                if ($scope.TaskEntityMemberLists != null && $scope.TaskGlobalMemberLists != null) {
                    for (var i = 0, mem; mem = $scope.TaskEntityMemberLists[i++];) {
                        var isGlobalPresence = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                            return rel.Userid == mem.Userid;
                        })[0];
                        if (isGlobalPresence != null) {
                            $scope.TaskGlobalMemberLists.splice(isGlobalPresence, 1);
                        }
                    }
                }
                if (PhaseID == 0 || PhaseID == null || $scope.CurrPhaseStepID == 0 || $scope.CurrPhaseStepID == null) {
                    if ($scope.PageFullData.Assignees.length > 0) {
                        for (var i = 0, mem; mem = $scope.PageFullData.Assignees[i++];) {
                            var isPresence = $.grep($scope.TaskEntityMemberLists, function (rel) {
                                return rel.Userid == parseInt(mem.Userid);
                            })[0];
                            if (isPresence != null) {
                                $scope.TaskEntityMemberLists = $.grep($scope.TaskEntityMemberLists, function (rel) {
                                    return rel != isPresence;
                                });
                               // $scope.TaskEntityMemberLists.splice(isPresence, 1);
                            }
                            var isGlobalPresence = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                                return rel.Userid == parseInt(mem.Userid);
                            })[0];
                            if (isGlobalPresence != null) {
                                $scope.TaskGlobalMemberLists = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                                    return rel != isGlobalPresence;
                                });
                               // $scope.TaskGlobalMemberLists.splice(isGlobalPresence, 1);
                            }
                        }
                    }
                }
                else {
                    if ($scope.PageFullData.Assignees.length > 0) {
                        for (var i = 0, mem; mem = $scope.PageFullData.Assignees[i++];) {
                            var isPresence = $.grep($scope.TaskEntityMemberLists, function (rel) {
                                return rel.Userid == parseInt(mem.Userid) && rel.StepId == parseInt(mem.Stepid);
                            })[0];
                            if (isPresence != null) {
                                $scope.TaskEntityMemberLists = $.grep($scope.TaskEntityMemberLists, function (rel) {
                                    return rel != isPresence;
                                });
                               // $scope.TaskEntityMemberLists.splice(isPresence, 1);
                            }
                            var isGlobalPresence = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                                return rel.Userid == parseInt(mem.Userid) && rel.StepId == parseInt(mem.Stepid);
                            })[0];
                            if (isGlobalPresence != null) {
                                // $scope.TaskGlobalMemberLists.splice(isGlobalPresence, 1);
                                $scope.TaskGlobalMemberLists = $.grep($scope.TaskGlobalMemberLists, function (rel) {
                                    return rel != isGlobalPresence;
                                });
                            }
                        }
                    }
                }
                memberObj.TaskMembers = ($scope.TaskEntityMemberLists.length > 0 || $scope.TaskEntityMemberLists != null) ? $scope.TaskEntityMemberLists : [];
                memberObj.GlobalTaskMembers = ($scope.TaskGlobalMemberLists.length > 0 || $scope.TaskGlobalMemberLists != null) ? $scope.TaskGlobalMemberLists : [];
                TaskEditService.InsertTaskMembers(memberObj).then(function (memberResult) {
                    if (memberResult.Response.m_Item1) {
                        TASKEDIT_NS.custominitialize($scope.PageFullData.overview.TaskId);
                        $('#ngTaskEditAddTaskMember').removeClass('disabled');
                        $('#EditTaskMemberModalPopup').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4479.Caption'));
                        TaskListRefresh = $timeout(function () {
                            $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                        }, 100)
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_5837.Caption'));
                        $('#ngTaskEditAddTaskMember').removeClass('disabled');
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5777.Caption'));
                $('#ngTaskEditAddTaskMember').removeClass('disabled');
            }
        }

        function GetentityMembers(entityID) {
            $scope.EntityMemberList = [];
            TaskEditService.GetDirectEntityMember(entityID).then(function (EntityMemberList) {
                $scope.EntityMemberList = EntityMemberList.Response;
                $scope.EntityDistinctMembers = [];
                var dupes = {};
                if ($scope.EntityMemberList != null) {
                    for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                        if (el.IsInherited != true) {
                            if (!dupes[el.Userid]) {
                                dupes[el.Userid] = true;
                                $scope.EntityDistinctMembers.push(el);
                            }
                        }
                    }
                }
            });
        }

        function GetAllUserRoles() {
            TaskEditService.GetApprovalRoles().then(function (Result) {
                $scope.UserApprovalRoles = [];
                if (Result.Response.length != null) {
                    for (var i = 0, role; role = Result.Response[i++];) {
                        $scope.UserApprovalRoles.push({
                            "Id": role.ID,
                            "Caption": role.Caption,
                            "Description": role.Description
                        });
                    }
                }
            });
        }

        function LoadMemberRoles() {
            TaskEditService.GetEntityDetailsByID($scope.PageFullData.overview.EntityID).then(function (GetEntityresult) {
                if (GetEntityresult.Response != null) {
                    TaskEditService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function (role) {
                        $scope.Roles = role.Response;
                    });
                }
            });
        }
        $scope.SaveTaskCheckList = function (Index, CheckListID, ChklistName, ChkListStatus, CheCklistExisting) {
            var checkbox;
            if (ChkListStatus.target != undefined) {
                checkbox = ChkListStatus.target.checked;
            } else {
                checkbox = ChkListStatus;
            }
            if (ChklistName != '-' && ChklistName.length > 0) {
                var SaveTask = {};
                SaveTask.Id = CheckListID;
                SaveTask.taskId = $scope.PageFullData.overview.TaskId;
                SaveTask.ChkListStatus = checkbox;
                SaveTask.ISowner = (CheckListID > 0 ? false : true);
                SaveTask.SortOrder = (Index + 1);
                SaveTask.CheckListName = ChklistName;
                SaveTask.isnew = CheCklistExisting == undefined ? false : true;
                TaskEditService.InsertUpdateEntityTaskCheckList(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4263.Caption'));
                    } else {
                        if (checkbox== true)
                            NotifySuccess($translate.instant('LanguageContents.Res_4157.Caption'));
                        else
                            NotifySuccess('Checklist remove successfully');
                        var Object = $.grep($scope.PageFullData.checklist, function (n, i) {
                            if (i == Index) {
                                return $scope.PageFullData.checklist[i];
                            }
                        });
                        Object[0].Id = SaveTaskResult.Response;
                        Object[0].Status = checkbox;
                        Object[0].IsExisting = true;
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1872.Caption'));
                CheCklistExisting = true;
                TaskListRefresh = $timeout(function () {
                    $scope.$emit('muiRefreshTaskByID', $scope.PageFullData.overview.TaskId);
                }, 100)
            }
        }
        $scope.EditCheckList = function (Name, Index) {
            CurrentCheckListName = "";
            var Object = $.grep($scope.PageFullData.checklist, function (n, i) {
                if ((i != Index && n.IsExisting == false)) {
                    return $scope.PageFullData.checklist[i];
                }
            });
            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.PageFullData.checklist.splice($scope.PageFullData.checklist.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                } else {
                    Object[0].IsExisting = true;
                    Object[0].Name = CurrentCheckListName;
                }
            }
            CurrentCheckListName = Name;
        }
        $scope.TaskcheckEditable = function (tasktype, index) {
            $timeout(function () {
                $("#MyTaskworktxtChk_" + index + "").focus().select()
            }, 100);
        };
        $scope.CloseCheckListPopUp = function (Index, CheCklistExisting, ChklistName) {
            if ($scope.PageFullData.checklist[Index].Id == 0) {
                $scope.PageFullData.checklist.splice(Index, 1);
            } else {
                $scope.PageFullData.checklist[Index].Name = CurrentCheckListName;
                $scope.PageFullData.checklist[Index].IsExisting = true;
            }
            $("#NonEditableCL" + Index).show();
            $("#EditableCL" + Index).hide();
        }
        $scope.AddCheckListNext = function () {
            var Object = $.grep($scope.PageFullData.checklist, function (n, i) {
                if (n.IsExisting == false) {
                    return $scope.PageFullData.checklist[i];
                }
            });
            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.PageFullData.checklist.splice($scope.PageFullData.checklist.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                } else {
                    Object[0].Name = CurrentCheckListName;
                    Object[0].IsExisting = true;
                }
            }
            if ($scope.contextCheckListIndex == -1) {
                $scope.PageFullData.checklist.push({
                    Id: 0,
                    Name: '',
                    OwnerId: 0,
                    OwnerName: 0,
                    SortOrder: 1,
                    Status: false,
                    TaskId: $scope.PageFullData.overview.TaskId,
                    UserId: 0,
                    UserName: "",
                    CompletedOnValue: "",
                    CompletedOn: null,
                    IsExisting: false
                });
            } else {
                $scope.PageFullData.checklist.splice(parseInt($scope.contextCheckListIndex) + 1, 0, {
                    Id: 0,
                    Name: '',
                    OwnerId: 0,
                    OwnerName: 0,
                    SortOrder: 1,
                    Status: false,
                    TaskId: $scope.PageFullData.overview.TaskId,
                    UserId: 0,
                    UserName: "",
                    CompletedOnValue: "",
                    CompletedOn: null,
                    IsExisting: false
                });
            }
            $timeout(function () {
                $('[datafocus-id =0]').focus();
            }, 10);
        }
        $scope.CheckListSelection = function (status) {
            var baseClass = "checkbox";
            if (status) baseClass += " checked";
            return baseClass;
        }
        $scope.SetcontextCheckListID = function (id, indexval) {
            $scope.contextCheckListID = id;
            $scope.contextCheckListIndex = indexval;
        };
        $scope.DeleteEntityChecklists = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2027.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if ($scope.contextCheckListID != 0) {
                            TaskEditService.DeleteEntityCheckListByID($scope.contextCheckListID).then(function (result) {
                                if (result.StatusCode == 200) {
                                    var fileObj = $.grep($scope.PageFullData.checklist, function (e) {
                                        return e.Id == $scope.contextCheckListID;
                                    });
                                    $scope.PageFullData.checklist.splice($.inArray(fileObj[0], $scope.PageFullData.checklist), 1);
                                }
                            });
                        } else {
                            $scope.PageFullData.checklist.splice($scope.contextCheckListIndex, 1);
                        }
                    }, 100);
                }
            });
        }
        $scope.BreadCrumOverview = function (ID, TypeId) {
            if (TypeId == 5) {
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/planningtool/costcentre/detail/section/' + ID + '/overview');
            } else if (TypeId == 10) {
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/objective/detail/section/' + ID + '/overview');
            } else {
                $("#SectionTabs li.active").removeClass('active');
                $("#SectionTabs li#Overview").addClass("active");
                $location.path('/mui/planningtool/default/detail/section/' + ID + '/overview');
            }
            $(".taskImprovementPopup ").hide();
            $modalInstance.dismiss('cancel');
        }
        $scope.OpenAttachmentPopup = function () {
            var damtaskentityselectionParams = {};
            var fromPlace = "attachment";
            if ($scope.PageFullData.overview != null && $scope.PageFullData.overview != undefined) {
                damtaskentityselectionParams = $scope.PageFullData.overview;
                fromPlace = "task";
            }
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damtaskentityselection.html',
                controller: "mui.DAM.taskentitydamselectionCtrl",
                resolve: {
                    params: function () {
                        return {
                            fromPlace: fromPlace,
                            data: damtaskentityselectionParams,
                            TaskID: $scope.PageFullData.taskid,
                            EntityID: $scope.PageFullData.overview.EntityID
                        };
                    }
                },
                scope: $scope,
                windowClass: 'drpD-Link popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function () {
            }, function () { });
            //  $scope.$broadcast('callfromDalimTaskEditForAssetSelect', $scope.PageFullData.taskid, $scope.PageFullData.overview.EntityID);
        };

        function Editasset(assetID, isLock, isNotify, viewtype) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return { AssetID: assetID, IsLock: isLock, isNotify: isNotify, viewtype: viewtype };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup', backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem)
            { $scope.selected = selectedItem; }, function () {
                TASKEDIT_NS.closePopup();
            });
        }
        $scope.$on('callBackToDalimTaskEdit', function (event, AssetFilesDetails) {
            $scope.IsSelectAppDocExist = true;
            angular.element('#approvDoc').attr('src', imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + AssetFilesDetails[0].FileGuid.toString().toLowerCase() + '.jpg');
            $scope.PageFullData.approveDocumentAssetId = AssetFilesDetails[0].AssetID;
        });
        $scope.removeApprDoc = function () {
            $scope.IsSelectAppDocExist = false;
            angular.element('#approvDoc').attr('src', '');
            $scope.PageFullData.approveDocumentAssetId = 0;
        }
        $scope.EditassetPopUp = function () {
            if ($scope.PageFullData.btnCreatenewVersion == true) {
                Editasset($scope.PageFullData.approveDocumentAssetId, $scope.EntityLockTask, false, 'Thumbnail');
            } else {
                if ($scope.PageFullData.overview.versionDetails.CurrentVersionNo == $scope.PageFullData.CurrentActualTaskVersion) {
                    Editasset($scope.PageFullData.approveDocumentAssetId, $scope.EntityLockTask, false, 'Thumbnail');
                }
            }
        }

        TASKEDIT_NS.RedirectToExternalReport = function (url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("External/ExternalReport.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        TASKEDIT_NS.OpenExternalReport = function (reportType) {
            var param = {
                'TaskId': taskModifieddata.overview.TaskId,
                'UserEmail': $cookies['UserEmail'],
                'ReportType': reportType
            };
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            TASKEDIT_NS.RedirectToExternalReport("External/ExternalReport.aspx", winprops, "NewFile", param);
        }
        TASKEDIT_NS.HolidayCount = function (dueDate) {
            $scope.testduedate = dateFormat(dueDate, $scope.format);
            var nbcount = 0;
            var today = new Date.create();
            var holiday_exist = $.grep($scope.tempholidays, function (e) {
                return new Date.create(e) <= dueDate && new Date.create(e) >= today;
            });
            var holiday_exist_min = $.grep($scope.tempholidays, function (e) {
                return new Date.create(e) >= dueDate && new Date.create(e) <= today;
            });
            if (dueDate > today) {
                var tempdates = getDates(today, dueDate);
            } else {
                var tempdates = getDates(dueDate, today);
            }
            if (tempdates != null) {
                var holiday_exist_nonbusinessdays = $.grep(tempdates, function (e) {
                    return (($scope.nonbusinessdays.sunday === "true" && e.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && e.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && e.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && e.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && e.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && e.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && e.getDay() === 6));
                });
            }
            if (holiday_exist_nonbusinessdays != null) {
                nbcount = holiday_exist_nonbusinessdays.length;
                if (new Date.create($scope.testduedate) > new Date.create()) {
                    if (holiday_exist != null) {
                        var Temp_holiday_exist = $.grep(holiday_exist, function (e) {
                            return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
                        });
                        if (Temp_holiday_exist != null) {
                            if (Temp_holiday_exist.length > 0 || nbcount > 0) {
                                return (holiday_exist.length - Temp_holiday_exist.length) + nbcount;
                            } else {
                                return 0;
                            }
                        }
                    }
                } else {
                    if (holiday_exist_min != null) {
                        var Temp_holiday_exist = $.grep(holiday_exist_min, function (e) {
                            return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
                        });
                        if (Temp_holiday_exist != null) {
                            if (Temp_holiday_exist.length > 0 || nbcount > 0) {
                                return (holiday_exist_min.length - Temp_holiday_exist.length) + nbcount;
                            } else {
                                return 0;
                            }
                        }
                    }
                }
            }
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(new Date(currentDate))
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        }
        $scope.changeTab = function () {
            $("#overviewTab").removeClass('active');
            $("#memberTab").addClass('active');
        }
        $scope.itemscc = ['item1', 'item2', 'item3'];
        $scope.opencc = function () {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: "ModalInstanceCtrl",
                resolve: {
                    items: function () {
                        return $scope.itemscc;
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {

            });
        };
    }
    app.controller("mui.task.muitaskFlowEditCtrl", ['$scope', '$rootScope', '$location', '$compile', '$timeout', '$cookies', '$stateParams', '$translate', 'TaskEditService', '$sce', '$modalInstance', 'items', '$modal', muitaskFlowEditCtrl])
    app.directive('tabHighlight', [function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                var x, y, initial_background = '#c3d5e6';
                element.removeAttr('style').mousemove(function (e) {
                    if (!element.hasClass('active')) {
                        x = e.pageX - this.offsetLeft;
                        y = e.pageY - this.offsetTop;
                    }
                }).mouseout(function () {
                    element.removeAttr('style');
                });
            }
        };
    }]);
})(angular, app);