﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IWidgetTypeDimension interface for table 'CM_WidgetTypeDimension'.
    /// </summary>
    public interface IWidgetTypeDimension
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int WidgetTypeID
        {
            get;
            set;

        }

        string DimensionName
        {
            get;
            set;

        }



        #endregion
    }
}
