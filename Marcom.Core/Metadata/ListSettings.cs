﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;

namespace BrandSystems.Marcom.Core.Metadata
{
    public class ListSettings
    {
        #region Member Variables

        protected string _name;
        protected string _description;
        protected int _type;
        protected List<int> _entitytypes;
        protected List<AttributeSettings> _attributes;


        #endregion


        #region Constructors
        public ListSettings() { }

        public ListSettings(string mName, string mDescription, int mtype, List<int> mEntityTypes, List<AttributeSettings> mAttributes)
        {
            this._name = mName;
            this._description = mDescription;
            this._type = mtype;
            this._entitytypes = mEntityTypes;
            this._attributes = mAttributes;

        }

        #endregion


        #region Public Properties
        
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public List<int> EntityTypes
        {
            get { return _entitytypes; }
            set { _entitytypes = value; }
        }

        public List<AttributeSettings> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }

        }        

        #endregion

        
    }
}
