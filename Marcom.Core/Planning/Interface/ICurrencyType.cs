﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICurrencyType interface for table 'PM_CurrencyType'.
    /// </summary>
    public interface ICurrencyType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;
        }

        string ShortName
        {
            get;
            set;
        }

        string Symbol
        {
            get;
            set;
        }


     

        #endregion
    }
}
