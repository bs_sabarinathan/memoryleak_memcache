﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, SectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        setTimeout(function () { $scope.page_resize(); }, 100);
        $scope.TabCollectionsList = null;
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        var renderContext = "";
        $scope.CmsBreadCrumData = {};
        $scope.IsLock = false;
        $scope.EntityLockTask = false;
        $scope.cmspageColorcode = "";
        $scope.ShowCustomTabOnLoad = false;
        $scope.load = function (parameters) {
            if ($scope.subview == "" || $scope.subview == undefined || $scope.subview == null) {
                $scope.subview = parameters;
            }
            $scope.LoadTab = function (tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                var cmsroutename = $location.path().contains("fullcmspage") ? "fullcmspage" : "fullpagecms";
                //if ($location.path().indexOf("fullcmspage") != -1) $location.path('/mui/cms/fullcmspage/section/' + $stateParams.ID + '/' + tab + '');
                if ($location.path().indexOf(cmsroutename) != -1) $location.path('/mui/fullpagecms/section/' + $stateParams.ID + '/' + tab + '');
                else $location.path('/mui/cms/detail/section/' + $stateParams.ID + '/' + tab + '');
            };
            BreadCrumLoad($stateParams.ID);
            $("#CmsSectionTabs li.active").removeClass('active');
            TabActive();
        };

        function TabActive() {
            $("#CmsSectionTabs li.active").removeClass("active");
            if ($scope.subview == 'content') $("#CmsContent").addClass('active');
            else if ($scope.subview == 'settings') $("#CmsOverview").addClass('active');
            else if ($scope.subview == 'overview') $("#CmsOverview1").addClass('active');
            else if ($scope.subview == 'member') $("#CmsMember").addClass('active');
            else if ($scope.subview == 'task') $("#CmsTask").addClass('active');
        }
        $scope.BreadCrumCMSOverview = function (ID) {
            if ($location.path().indexOf("fullcmspage") != -1) $location.path('/mui/cms/fullcmspage/section/' + ID + '/content');
            else $location.path('/mui/cms/detail/section/' + ID + '/content');
            BreadCrumLoad(ID);
        };

        function BreadCrumLoad(ID) {
            SectionService.GetDataForBreadcrumLoadWithPath($stateParams.ID).then(function (BreadcrumData) {
                if (BreadcrumData.Response != null && BreadcrumData.Response.length > 0) {
                    var decodedName = $('<div/>').html(BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].Name).text();
                    $scope.CmsBreadCrumData = BreadcrumData.Response[1];
                    $scope.cmspageColorcode = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].ColorCode;
                    $scope.RootLevelEntityNameTemp = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].Name, $scope.RootLevelEntityName = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].Name;
                    $scope.EntityColorcode = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].ColorCode;
                    $scope.EntityShortText = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].ShortDescription;
                    $scope.RootLevelEntityNameTemp = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].Name
                    $scope.EntityShortText = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].ShortDescription;
                    $scope.EntityColorcode = $scope.cmspageColorcode;
                    $scope.EntityTypeID = BreadcrumData.Response[1][BreadcrumData.Response[1].length - 1].ID
                    $scope.RootLevelEntityName = decodedName;
                }
            });
        }

        function LoadTabOnEntityEdit() {
            $scope.$broadcast('LoadCmsContentTab', $stateParams.ID);
        }
        $scope.agroup = [];
        $scope.agroupListTabBlock = [];
        $scope.agroupkeyvaluepairTabBlock = [];
        $scope.AttributeGroupById = 0;

        function GetAttributeGroups() {
            $scope.LanguageContent = $scope.LanguageContents;
            $scope.AttrLock = $scope.IsLock;
            if ($scope.agroup.length > 0) {
                $scope.agroup.splice(0, $scope.agroup.length);
                $scope.agroupListTabBlock.splice(0, $scope.agroupListTabBlock.length);
                $scope.agroupkeyvaluepairTabBlock.splice(0, $scope.agroupkeyvaluepairTabBlock.length);
            }
            SectionService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), $scope.AttributeGroupById).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {
                        $scope.agroup = attributeGrpRelation.Response;
                        $scope.agroupListTabBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 3 && e.RepresentationType == true
                        });
                        $scope.agroupkeyvaluepairTabBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 3 && e.RepresentationType == false
                        });
                    }
                    if ($scope.agroupListTabBlock.length > 0) $timeout(function () {
                        $scope.$broadcast("processlistganttintab" + ($scope.agroupListTabBlock[0].ID))
                    }, 200);
                    if ($scope.agroupkeyvaluepairTabBlock.length > 0) $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupkeyvaluepairTabBlock[0].ID))
                    }, 200);
                }
            });
        }
        $scope.CustomTabLoad = function (tab, tabUrl, IsSytemDefined, Name, AddEntityID, AttributeGroupId) {
            $scope.ShowCustomTabOnLoad = false;
            $scope.agroup.length = 0;
            $scope.agroupListTabBlock.length = 0;
            $scope.agroupkeyvaluepairTabBlock.length = 0;
            if (IsSytemDefined == false) {
                $scope.AttributeGroupById = AttributeGroupId;
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                if (AttributeGroupId == 0) {
                    $scope.IsCustomTab = true;
                    SectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function (gettabresult) {
                        if (gettabresult.Response != null) {
                            $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                            $timeout(function () {
                                $scope.ShowCustomTabOnLoad = true;
                            }, 750);
                        }
                    });
                } else {
                    $timeout(function () {
                        $scope.IsCustomTab = false;
                        GetAttributeGroups();
                        $scope.ShowCustomTabOnLoad = true;;
                    }, 200);
                }
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl(tabUrl + (AddEntityID == true ? $stateParams.ID : ""));
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                if ($location.path().indexOf("fullcmspage") != -1) $location.path('/mui/cms/fullcmspage/fullpagecustomtab/' + tab + '/' + $stateParams.ID + '');
                else $location.path('/mui/cms/customtab/' + tab + '/' + $stateParams.ID + '');
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }
        }
        GetAllTabCollections();

        //TabActive();
        function GetAllTabCollections() {
            SectionService.GetCustomEntityTabsByTypeID(17, 0, $stateParams.ID, 0).then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function (item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID);
                    }
                }
            });
        }
        $scope.BindClass = function (index, tab, ID) {
            if (index == 0 && $scope.subview == null) {
                if ($scope.subview == null) {
                    var tabid = $scope.TabCollectionsList[index].ControleID;
                    $scope.subview = tabid;
                }
                return "active";
            } else if ($scope.subview == tab) {
                return "active";
            } else if (ID == $stateParams.tabID) {
                return "active";
            }
        }
        ResetTaskObjects();

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.groupbySubLevelTaskObj = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function (event, taskkist) {
            if (taskkist.length > 0) {
                $scope.MainTaskLiveUpdateIndex = 0;
                $scope.subTaskLiveUpdateIndex = 0;
                UpdateTaskData();
                UpdateSubLevelTaskData();
            }
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection[$scope.MainTaskLiveUpdateIndex];
            if (tlObj != undefined && tlObj.length > 0) {
                tasklistList = $.grep($scope.TaskExpandedDetails, function (e) {
                    return e.ID == tlObj.TaskLiStID;
                });
                if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                    var data = [];
                    data = result.Response;
                    if (data.length > 0) {
                        tasklistList[0].TaskList = [];
                        for (var j = 0, taskdata; taskdata = data[j++];) {
                            tasklistList[0].TaskList.push(taskdata);
                        }
                        try {
                            var taskListResObj = $.grep($scope.groups, function (e) {
                                return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                            });
                            if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                                if (tasklistList[0].TaskList.length > 0) {
                                    taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                    taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                                }
                            }
                        } catch (e) { }
                    }
                    $timeout(function () {
                        $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                        if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.length - 1) UpdateTaskData();
                    }, 20);
                });
            }
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function (e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function (e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function (e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) { }
                }
                $timeout(function () {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.sectionCtrl']"));
        });
        $timeout(function () {
            $scope.load('content');
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
    }
    app.controller('mui.cms.default.detail.sectionCtrl', ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'SectionService', muiplanningtooldefaultCtrl]);
    function SectionService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetCustomTabUrlTabsByTypeID: GetCustomTabUrlTabsByTypeID,
            GetDataForBreadcrumLoadWithPath: GetDataForBreadcrumLoadWithPath,
            GetCustomEntityTabsByTypeID: GetCustomEntityTabsByTypeID
        });
        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCustomTabUrlTabsByTypeID(tabID, entityID) { var request = $http({ method: "get", url: "api/common/GetCustomTabUrlTabsByTypeID/" + tabID + "/" + entityID, params: { action: "get", }, }); return (request.then(handleSuccess, handleError)); }
        function GetDataForBreadcrumLoadWithPath(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetDataForBreadcrumLoadWithPath/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCustomEntityTabsByTypeID(TypeID, EntityTypeID, EntityID, CalID) {
            if (arguments.length == 1) { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID, params: { action: "get", } }); } else { var request = $http({ method: "get", url: "api/common/GetCustomEntityTabsByTypeID/" + TypeID + "/" + CalID + "/" + EntityTypeID + "/" + EntityID, params: { action: "get", }, }); }
            return (request.then(handleSuccess, handleError));
        }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("SectionService", ['$http', '$q', SectionService]);

})(angular, app);