﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    /// <summary>
    /// ITaskFile interface for table 'TM_File'.
    /// </summary>
    public interface ITaskFile
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int VersionNo
        {
            get;
            set;

        }

        string MimeType
        {
            get;
            set;

        }

        string Extension
        {
            get;
            set;

        }


        long Size
        {
            get;
            set;

        }

        int Ownerid
        {
            get;
            set;

        }

        DateTimeOffset CreatedOn
        {
            get;
            set;

        }

        string Checksum
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        Guid Fileguid
        {
            get;
            set;

        }

        string strFileID { get; set; }

        string OwnerName
        {
            get;
            set;

        }

        string Description { get; set; }
        string StrCreatedDate { get; set; }
        string linkURL { get; set; }
        int LinkType { get; set; }
        int AssetTypeid { get; set; }
        int Category { get; set; }
        int ProcessType { get; set; }
        int Status { get; set; }
        int Assetid { get; set; }

        #endregion
    }
}
