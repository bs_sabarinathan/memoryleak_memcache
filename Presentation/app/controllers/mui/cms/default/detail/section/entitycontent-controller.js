﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope, $stateParams, EntitycontentService, $location, $modal) {
        $scope.EditFullPageCmsrevContent = function () {
            if ($scope.ISFullPages == true) $location.path('mui/fullpagecms/section/' + $stateParams.ID + '/content');
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.list.entitycontentCtrl']"));
        });
        $scope.ID = $stateParams.ID;
        GetCmsRevisedContent();

        function OpenAssetLibraryPopUp(AssetID) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: AssetID,
                            IsLock: true,
                            isNotify: true,
                            viewtype: 'ViewType'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            $scope.$apply();
        }

        function GetCmsRevisedContent() {
            EntitycontentService.GetRevisedContentByFeature($stateParams.ID).then(function (data) {
                if (data.Response != null) {
                    var temphtml = $("<div/>").html($.parseHTML(data.Response.Content, document, true));
                    if (temphtml.find("#cmsLinksContainer").find('a').length == 0) {
                        temphtml.find("#cmsLinksContainer").remove();
                    } else {
                        temphtml.find("#cmsLinksContainer div").first().find('a').each(function () {
                            if ($(this).attr("target") == undefined) {
                                $(this).attr('target', '_blank');
                            }
                        });
                        temphtml.find("#cmsLinksContainer").children().eq(1).find('a').each(function () {
                            if ($(this).attr("href") != "") {
                                $(this).attr("data-toggle", "modal");
                                $(this).attr("data-target", "#AssetEditpopup");
                                $(this).removeAttr("href", "");
                                var popassetid = $(this).attr("data-assetId");
                                $(this).click(function (event) {
                                    var targetCtrl = $(event.target);
                                    if (targetCtrl.attr("data-assetId") != undefined) {
                                        var modalInstance = $modal.open({
                                            templateUrl: 'views/mui/DAM/assetedit.html',
                                            controller: "mui.DAM.asseteditCtrl",
                                            resolve: {
                                                params: function () {
                                                    return {
                                                        AssetID: targetCtrl.attr("data-assetId"),
                                                        IsLock: true,
                                                        isNotify: true,
                                                        viewtype: 'ViewType'
                                                    };
                                                }
                                            },
                                            scope: $scope,
                                            windowClass: 'iv-Popup',
                                            backdrop: "static"
                                        });
                                        modalInstance.result.then(function (selectedItem) {
                                            $scope.selected = selectedItem;
                                        }, function () { });
                                        $scope.$apply();
                                    }
                                });
                            }
                        });
                    }
                    $('#htmlEntityContent').html(temphtml);
                }
            });
        }
    }
    app.controller('mui.cms.default.detail.list.entitycontentCtrl', ['$scope', '$stateParams', 'EntitycontentService', '$location', '$modal', muiplanningtooldefaultCtrl]);
    function EntitycontentService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetRevisedContentByFeature: GetRevisedContentByFeature
        });
        function GetRevisedContentByFeature(EntityID) { var request = $http({ method: "get", url: "api/cms/GetRevisedContentByFeature/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("EntitycontentService", ['$http', '$q', EntitycontentService]);
})(angular, app);