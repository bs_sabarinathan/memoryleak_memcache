﻿(function (ng, app) {
    function muicmscontentCtrl($scope, $location, $window, $timeout, $compile, $resource, $stateParams, $translate, ContentService) {
        var redSave = '';
        var redRestore = '';
        $scope.PageNoobj = {
            pageno: 1
        };
        $scope.TaskStatusObj = [{
            "Name": "Thumbnail",
            ID: 1
        }, {
            "Name": "Summary",
            ID: 2
        }, {
            "Name": "List",
            ID: 3
        }];
        $scope.SettingsDamAttributes = {};
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Creation date (Ascending)",
            ID: 3
        }, {
            "Name": "Creation date (Descending)",
            ID: 4
        }];
        $scope.FilterStatus = {
            filterstatus: 1
        };
        $scope.DamViewName = {
            viewName: "Thumbnail"
        };
        $scope.OrderbyName = {
            orderbyname: "Creation date (Descending)"
        };
        $scope.OrderBy = {
            order: 4
        };
        $scope.PageAsset = {
            CmsAssetSelectionFiles: []
        };
        $scope.processed = {
            index: 0
        };
        $scope.AttrLock = true;
        $scope.attrgrpImagefileName = '';
        $scope.agroup = [];
        $scope.customattrgroup = [];
        $scope.insertImageOptions = {
            header: {
                title: 'Add asset from computer',
                callback: function (obj) {
                    obj.insertHtml('Page 3');
                }
            },
            footer: {
                title: 'Add asset from page attachment',
                callback: function (obj) {
                    obj.insertHtml('Page 4');
                }
            }
        };
        $scope.ShowHideSave = false;
        $scope.tempContent = "";
        $scope.AttributeGroupList = [];
        $scope.snippettemplates = [];
        $.Redactor.prototype.advanced = function () {
            return {
                init: function () {
                    var dropdown = {};
                    dropdown.point1 = {
                        title: 'Add asset from computer',
                        func: this.advanced.addfrmComputer
                    };
                    dropdown.point2 = {
                        title: 'Add asset from page attachment',
                        func: this.advanced.AddassetsfromPageAttachments
                    };
                    dropdown.point3 = {
                        title: 'Add asset from asset library',
                        func: this.advanced.AddFilesFromAssetLib
                    };
                    var button = this.button.add('advanced', 'Asset to page');
                    this.button.setAwesome('advanced', 'icon-tasks');
                    this.button.addDropdown(button, dropdown);
                },
                addfrmComputer: function () {
                    this.selection.save();
                    addfrmComputer1(this);
                },
                AddassetsfromPageAttachments: function () {
                    this.selection.save();
                    AddassetsfromPageAttachments1(this);
                },
                AddFilesFromAssetLib: function () {
                    redactorCurPoint = this.caret.getOffset();
                    AddFilesFromAssetLib1(this);
                }
            };
        };
        FnUpload();

        function CallbackFromAsstLibrary(obj) {
            if ($scope.contentDatafrCMS.attachType != undefined) {
                switch ($scope.contentDatafrCMS.attachType) {
                    case 'link':
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            obj.insert.htmlWithoutClean('&nbsp;<a data-assetId=' + item.AssetID + ' href="DAMDownload.aspx?FileID=' + item.FileGuid + '&FileFriendlyName=' + item.AssetName + item.Ext + '&Ext=' + item.Ext + '&token=' + token + '">' + item.AssetName + '</a>');
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                        break;
                    case 'image':
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            var ste = item.Ext.split('.').join("");
                            if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                            } else {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'DAMFiles/StaticPreview/&amp;name=' + ste + '.png" alt="Image">');
                            }
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                        break;
                    default:
                        $scope.contentDatafrCMS.attachType = "";
                        if (!obj.focus.isFocused()) obj.focus.setStart();
                        if (redactorCurPoint > 0) obj.caret.setOffset(redactorCurPoint);
                        var token = new Date().getTime();
                        angular.forEach($scope.contentDatafrCMS.fileData, function (item, key) {
                            var ste = item.Ext.split('.').join("");
                            if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                obj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                            } else {
                                var videofilepath = TenantFilePath + "CMSFiles/Templates/Preview/" + item.FileGuid + ".mp4";
                                var vidImagePath = TenantFilePath + "CMSFiles/Templates/Preview/Big_" + item.FileGuid + ".jpg"
                                obj.insert.htmlWithoutClean('<div class="iv-p-videoPlayer embed-responsive embed-responsive-16by9 tmplt-video"><video class="video-js" controls="" preload="none" name="media" poster="' + vidImagePath + '"><source type="video/mp4" src="' + videofilepath + '"></video></div>');
                            }
                        });
                        $scope.contentDatafrCMS = {};
                        redactorCurPoint = 0;
                }
            }
        }
        $scope.$on('processnext', function (event, data) {
            var nextGroupToProcess = $scope.agroup.indexOf(($.grep($scope.agroup, function (e) {
                return e.ID == data
            }))[0]) + 1;
            if (nextGroupToProcess + 1 <= $scope.agroup.length) $scope.$broadcast("process" + $scope.agroup[nextGroupToProcess].ID);
        });

        function AttributeGroupInRedactor(attrGrpRes) {
            $.Redactor.prototype.plgattributegroup = function () {
                return {
                    init: function () {
                        var dropdown = {};
                        for (var i = 0; i < attrGrpRes.length; i++) {
                            dropdown[attrGrpRes[i].AttributeGroupID] = {
                                title: attrGrpRes[i].Caption,
                                func: function (obj) {
                                    var attrInfo = $.grep(attrGrpRes, function (e) {
                                        return e.AttributeGroupID == parseInt(obj)
                                    });
                                    var attrGroupBody = "";
                                    attrGroupBody += '<br/><br/><div><div class=\"row clearfix\" data-attributegroupId=\"' + attrInfo[0].AttributeGroupID + '\">';
                                    attrGroupBody += '<div class=\"column full display attGrp-editTemplate\">';
                                    attrGroupBody += '<div class=\"attGrp-header\"><h1>Attribute Group</h1></div>';
                                    attrGroupBody += '<p>' + attrInfo[0].Caption + '</p>'
                                    attrGroupBody += '</div></div></div><br/><br/>'
                                    this.insert.html(attrGroupBody, false);
                                }
                            };
                        }
                        var button = this.button.add('plgattributegroup', 'Attribute Group');
                        this.button.setAwesome('plgattributegroup', 'icon-list-alt');
                        this.button.addDropdown(button, dropdown);
                    }
                };
            };
        }

        function CustomFontsInRedactor(fontList) {
            $.Redactor.prototype.plgcustomfonts = function () {
                return {
                    init: function () {
                        var dropdown = {};
                        $.each(fontList, function (i, styl) {
                            dropdown[styl.Id + "=" + styl.CssCode] = {
                                title: styl.ClassName,
                                style: styl.CssCode,
                                func: function (styl) {
                                    this.inline.removeStyle();
                                    this.inline.format('span', 'style', styl.toString().split("=")[1]);
                                }
                            };
                        });
                        var button = this.button.add('plgcustomfonts', 'Custom Styles');
                        this.button.setAwesome('plgcustomfonts', 'icon-font');
                        this.button.addDropdown(button, dropdown);
                    }
                };
            };
        }
        $scope.BindScrollOnNewsfeed = function () {
            if ($('#CmsPageFeedsdiv').height() > 1260) {
                $scope.GetNewsFeedforPaging();
            }
            $timeout(function () {
                $('#CmsPageFeedsdiv').unbind('scroll');
                $('#CmsPageFeedsdiv').scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        $scope.GetNewsFeedforPaging();
                    }
                });
            }, 500)
        }

        function GetAttributeGroups() {
            ContentService.GetListOfCmsCustomStyle().then(function (ResCustomStyle) {
                if (ResCustomStyle.Response.length > 0) CustomFontsInRedactor(ResCustomStyle.Response);
            });
            $scope.AttributeGroupList.length = 0;
            $scope.LanguageContent = $scope.LanguageContents;
            ContentService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), 0).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null && attributeGrpRelation.Response != '') {
                    AttributeGroupInRedactor(attributeGrpRelation.Response);
                    for (var i = 0; i < attributeGrpRelation.Response.length; i++) {
                        $scope.AttributeGroupList.push({
                            AttributeGroupId: attributeGrpRelation.Response[i].AttributeGroupID,
                            Name: attributeGrpRelation.Response[i].Caption
                        });
                    }
                    getSnippettemplates();
                } else getSnippettemplates();
            });
        }

        function getSnippettemplates() {
            ContentService.GetAllCmsSnippetTemplates().then(function (result) {
                if (result.Response != null) {
                    $scope.snippettemplates.length = 0;
                    for (var i = 0; i < result.Response.length; i++) {
                        $scope.snippettemplates.push({
                            ID: result.Response[i].ID,
                            SnippetContent: result.Response[i].DefaultFirstContent + result.Response[i].SnippetContent + result.Response[i].DefaultLastContent
                        });
                    }
                    GetCmsRevisedContent();
                } else GetCmsRevisedContent();
            });
        };
        $scope.PopupModalGroupID = 0;

        $scope.$on("$destroy", function () {
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.section.contentCtrl']"));
        });
        GetAttributeGroups();

        function GetCmsRevisedContent() {
            if ($scope.contentDatafrCMS != undefined && $scope.contentDatafrCMS.attachType != undefined && $scope.contentDatafrCMS.attachType != '') {
                $scope.ShowHideContent = false;
                $('#onLoadSnippet').html($scope.contentDatafrCMS.cmsContentData);
                $('#cmsSnippetsContainer').html($('#onLoadSnippet #cmsSnippetsContainer').html());
                $('#cmsLinksContainer').html($('#onLoadSnippet #cmsLinksContainer').html());
                $('#onLoadSnippet').html('');
                $scope.EditCmsHtmlContent1();
            } else {
                ContentService.GetRevisedContentByFeature($stateParams.ID).then(function (data) {
                    if (data.Response != null) {
                        $('#CmsContentEditablePage #cmsSnippetsContainer #cmsLinksContainer').html('');
                        if ($('#CmsContentEditablePage').hasClass("redactor-editor")) $('#CmsContentEditablePage').redactor('core.destroy');
                        if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
                        $timeout(function () {
                            $('#onLoadSnippet').html(data.Response.Content);
                            $('#cmsSnippetsContainer').html($('#onLoadSnippet #cmsSnippetsContainer').html());
                            $('#cmsLinksContainer').html($('#onLoadSnippet #cmsLinksContainer').html());
                            $('#onLoadSnippet').html('');
                            $scope.ShowHideContent = true;
                            $("#cmsSnippetsContainer").contentbuilder({
                                snippetFile: TenantFilePath + 'CMSFiles/contentbuilder/snippets.html',
                                snippetPathReplace: ["CMSFiles", TenantFilePath + 'CMSFiles/'],
                                attributegroup: $scope.AttributeGroupList,
                                cmssnippetList: $scope.snippettemplates
                            });
                        }, 50);
                    }
                });
            }
        }
        $scope.SaveCmsrevContent = function () {
            if ($('#CmsContentEditablePage').html().trim().length == 0) {
                alert("Please enter content");
                return false;
            }
            var RevParams = {
                EntityID: $stateParams.ID,
                Content: $('#CmsContentEditablePage').html()
            };
            ContentService.InsertRevisedEntityContent(RevParams).then(function (data) {
                if (data.Response != null) {
                    NotifySuccess($translate.instant('LanguageContents.Res_1130.Caption'));
                }
            });
        }
        $scope.addimageatCursor = false;

        function FnUpload() {
            $('#contentpickfiles').unbind();
            $('.moxie-shim').remove();
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'contentpickfiles',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/ImageUploader.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png"
                }],
                resize: {
                    width: 320,
                    height: 240,
                    quality: 90
                }
            });
            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader.start();
            });
            uploader.bind('Error', function (up, err) {
                $scope.PrevShow = false;
                $scope.BtnSaveShow = false;
                $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.PrevImage = response.response.split(',')[0];
                if ($scope.addimageatCursor) {
                    redObj.insert.htmlWithoutClean('<img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + $scope.PrevImage + '" alt="Image">');
                    $scope.addimageatCursor = false;
                    NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                } else {
                    showImagePreview(file);
                }
            });
            uploader.bind('BeforeUpload', function (up, file) {
                $scope.BtnSaveShow = true;
                $scope.PrevShow = true;
            });

            function showImagePreview(file) {
                var preloader = new mOxie.Image();
                preloader.onload = function () {
                    var imgfile = $("#redactor-image-resizer").next();
                    preloader.downsize(300, 300);
                    if (imgfile.length > 0) {
                        $("#redactor-image-resizer").next().attr('src', preloader.getAsDataURL());
                        var redactorObj = $('#CmsContentEditablePage').redactor('core.getObject');
                        redactorObj.image.hideResize();
                    }
                };
                preloader.load(file.getSource());
            }
        }

        function addfrmComputer1(Obj) {
            redObj = Obj;
            $scope.addimageatCursor = true;
            $("#contentpickfiles").click();
        };

        function AddFilesFromAssetLib1(Obj) {
            redObj = Obj;
            $scope.MediaBankSettings.Redirectfromdam = true;
            $scope.MediaBankSettings.FolderId = 0;
            $scope.MediaBankSettings.EntityId = parseInt($stateParams.ID);
            $scope.MediaBankSettings.ViewType = 1;
            $scope.MediaBankSettings.directfromMediaBank = false;
            $scope.MediaBankSettings.ProductionTypeID = 17;
            $scope.MediaBankSettings.FolderName = '';
            $scope.MediaBankSettings.frmCMS = true;
            $scope.IsNavFromAssetToPage.IsFromAsset = true;
            $scope.contentDatafrCMS.cmsContentData = $("#CmsContentEditablePage").html();
            $scope.$apply($location.path("/mui/mediabank"));
        }
        $scope.AssetTaskentityselection = 'cms';
        $scope.$on('LoadCmsContentTab', function (event, data) {
            $timeout(function () {
                GetAttributeGroups();
            }, 100);
        });
        var sel, range, htmlcood, position;

        function AddassetsfromPageAttachments1(Obj) {
            $scope.PageAsset.CmsAssetSelectionFiles = [];
            redObj = Obj;
            position = redObj.caret.getOffset();
            $("#attachmentsection").modal("show");
            $scope.MediaBankSettings.ViewType = 1;
            $scope.MediaBankSettings.FolderId = 0;
            $scope.MediaBankSettings.EntityId = parseInt($stateParams.ID);
            $scope.MediaBankSettings.ProductionTypeID = 17;
            $scope.MediaBankSettings.FolderName = '';
            $scope.MediaBankSettings.frmCMS = true;
            $scope.$broadcast('callbackforEntitycreation', 0, parseInt($stateParams.ID), 4);
        };
        $scope.ShowHideContent = true;
        $scope.EditCmsHtmlContent = function () {
            $scope.ShowHideContent = false
            if ($('#cmsSnippetsContainer').data('contentbuilder')) {
                $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            }
            $('#CmsContentEditablePage').redactor({
                focus: true,
                paragraphize: false,
                replaceDivs: false,
                plugins: ['plgattributegroup', 'advanced', 'plgcustomfonts'],
            });
        }
        $scope.EditCmsHtmlContent1 = function () {
            $scope.ShowHideContent = false
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            $('#CmsContentEditablePage').redactor({
                focus: true,
                paragraphize: false,
                replaceDivs: false,
                plugins: ['plgattributegroup', 'advanced', 'plgcustomfonts'],
                initCallback: function () {
                    CallbackFromAsstLibrary(this);
                }
            });
        }
        $scope.DragModeContent = function () {
            if ($('#CmsContentEditablePage').hasClass("redactor-editor")) $('#CmsContentEditablePage').redactor('core.destroy');
            $scope.ShowHideContent = true;
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            $("#cmsSnippetsContainer").contentbuilder({
                snippetFile: TenantFilePath + 'CMSFiles/contentbuilder/snippets.html',
                snippetPathReplace: ["CMSFiles", TenantFilePath + 'CMSFiles/'],
                attributegroup: $scope.AttributeGroupList,
                cmssnippetList: $scope.snippettemplates
            });
        }
        $scope.attachFile = function (attachType) {
            var count = $scope.PageAsset.CmsAssetSelectionFiles.length;
            if ($scope.PageAsset.CmsAssetSelectionFiles.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            } else if ($scope.PageAsset.CmsAssetSelectionFiles.length > 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                return false;
            }
            ContentService.GetAssetActiveFileinfo($scope.PageAsset.CmsAssetSelectionFiles[0]).then(function (attrList) {
                var d = attrList.Response;
                var ste1 = attrList.Response[0].Extension.split('.').join("");
                var fileType = o.Mime.mimes[ste1.toLowerCase()].split('/')[0];
                if (attachType == 'video' && o.Mime.mimes[ste1.toLowerCase()].split('/')[0] != 'video') {
                    bootbox.alert($translate.instant('LanguageContents.Res_5724.Caption'));
                    return false;
                }
                var arr = [];
                for (var i = 0; i < $scope.PageAsset.CmsAssetSelectionFiles.length; i++) {
                    arr.push($scope.PageAsset.CmsAssetSelectionFiles[i]);
                }
                var assetdata = {};
                assetdata.AssetArr = arr;
                assetdata.EntityID = parseInt($stateParams.ID);
                assetdata.FolderID = 0;
                assetdata.AttachType = attachType;
                ContentService.LinkpublishedfilestoCMS(assetdata).then(function (result) {
                    if (result.Response != null) {
                        if (redObj != '' && redObj != undefined) {
                            redObj.selection.restore();
                            angular.forEach(result.Response, function (item, key) {
                                if (attachType == 'image') {
                                    var ste = item.Ext.split('.').join("");
                                    if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                        redObj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'CMSFiles/Templates/&amp;name=' + item.FileName + '" alt="Image">');
                                    } else {
                                        redObj.insert.htmlWithoutClean('</br><img src="Handlers/ImageUploadedPath.ashx?Path=' + TenantFilePath + 'DAMFiles/StaticPreview/&amp;name=' + ste + '.png" alt="Image">');
                                    }
                                } else if (attachType == 'link') {
                                    var token = new Date().getTime();
                                    redObj.insert.htmlWithoutClean('&nbsp;<a href="DAMDownload.aspx?FileID=' + item.FileGuid + '&FileFriendlyName=' + item.AssetName + item.Ext + '&Ext=' + item.Ext + '&token=' + token + '">' + item.AssetName + '</a>');
                                } else if (attachType == 'video') {
                                    var videofilepath = TenantFilePath + "CMSFiles/Templates/Preview/" + item.FileGuid + ".mp4";
                                    var vidImagePath = TenantFilePath + "CMSFiles/Templates/Preview/Big_" + item.FileGuid + ".jpg"
                                    redObj.insert.htmlWithoutClean('<div class="iv-p-videoPlayer embed-responsive embed-responsive-16by9 tmplt-video"><video  class="video-js" controls="" preload="none" name="media" poster="' + vidImagePath + '"><source type="video/mp4" src="' + videofilepath + '"></video></div>');
                                }
                            });
                            NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                        }
                    } else { }
                    $timeout(function () {
                        $("#attachmentsection").modal("hide");
                        $scope.PageAsset.CmsAssetSelectionFiles = [];
                    }, 500);
                    $scope.MediaBankSettings.ProductionTypeID = 0;
                    $scope.MediaBankSettings.FolderName = '';
                    $scope.MediaBankSettings.frmCMS = false;
                });
            });
        };
    }
    app.controller('mui.cms.default.detail.section.contentCtrl', ['$scope', '$location', '$window', '$timeout', '$compile', '$resource', '$stateParams', '$translate', 'ContentService',muicmscontentCtrl]);
    function ContentService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetListOfCmsCustomStyle: GetListOfCmsCustomStyle,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetAssetActiveFileinfo: GetAssetActiveFileinfo,
            GetAllCmsSnippetTemplates: GetAllCmsSnippetTemplates,
            GetRevisedContentByFeature: GetRevisedContentByFeature,
            InsertRevisedEntityContent: InsertRevisedEntityContent,
            LinkpublishedfilestoCMS: LinkpublishedfilestoCMS
        });
        function GetListOfCmsCustomStyle() { var request = $http({ method: 'get', url: 'api/metadata/GetListOfCmsCustomStyle', params: { action: 'get' } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAssetActiveFileinfo(assetId) { var request = $http({ method: "get", url: "api/dam/GetAssetActiveFileinfo/" + assetId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllCmsSnippetTemplates() { var request = $http({ method: "get", url: "api/cms/GetAllCmsSnippetTemplates/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetRevisedContentByFeature(EntityID) { var request = $http({ method: "get", url: "api/cms/GetRevisedContentByFeature/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertRevisedEntityContent(revisecmsentityobj) { var request = $http({ method: "post", url: "api/cms/InsertRevisedEntityContent/", params: { action: "add" }, data: revisecmsentityobj }); return (request.then(handleSuccess, handleError)); }
        function LinkpublishedfilestoCMS(jobj) { var request = $http({ method: "post", url: "api/cms/LinkpublishedfilestoCMS/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ContentService", ['$http', '$q', ContentService]);
})(angular, app);