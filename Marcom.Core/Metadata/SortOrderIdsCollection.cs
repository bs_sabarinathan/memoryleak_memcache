﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    public class SortOrderIdsCollection : ISortOrderIdsCollection
    {
        [XmlAttribute]
        public int Id { get; set; }
        [XmlAttribute]
        public int EntityIds { get; set; }
        [XmlAttribute]
        public int Level { get; set; }
        [XmlAttribute]
        public int ParentEntityID { get; set; }
        [XmlAttribute]
        public int InterMediateID { get; set; }
        [XmlAttribute]
        public string UniqueKey { get; set; }
        [XmlAttribute]
        public int OptionID { get; set; }
        [XmlAttribute]
        public string OptionCaption { get; set; }
    }
}
