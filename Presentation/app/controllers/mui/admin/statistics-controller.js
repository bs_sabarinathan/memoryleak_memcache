﻿(function(ng, app) {
	"use strict";

	function muiadminstatisticsCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, AdminService) {
		$scope.GlobalChartType = {
			1: "column",
			2: "pie"
		};
		$scope.statistics = {
			ddlMonth: 0,
			ddlyear: 0,
			ddltype: 0
		};
		var yeardata = [];
		for (var i = 2010; i <= 2030; i++) {
			yeardata.push({
				"ID": i,
				"Value": i
			});
		}
		$scope.years = yeardata;
		var Monthdata = [];
		Monthdata.push({
			"ID": 0,
			"Value": 'All'
		});
		Monthdata.push({
			"ID": 1,
			"Value": 'January'
		});
		Monthdata.push({
			"ID": 2,
			"Value": 'February'
		});
		Monthdata.push({
			"ID": 3,
			"Value": 'March'
		});
		Monthdata.push({
			"ID": 4,
			"Value": 'April'
		});
		Monthdata.push({
			"ID": 5,
			"Value": 'May'
		});
		Monthdata.push({
			"ID": 6,
			"Value": 'June'
		});
		Monthdata.push({
			"ID": 7,
			"Value": 'July'
		});
		Monthdata.push({
			"ID": 8,
			"Value": 'August'
		});
		Monthdata.push({
			"ID": 9,
			"Value": 'September'
		});
		Monthdata.push({
			"ID": 10,
			"Value": 'October'
		});
		Monthdata.push({
			"ID": 11,
			"Value": 'November'
		});
		Monthdata.push({
			"ID": 12,
			"Value": 'December'
		});
		$scope.Months = Monthdata;
		var Typedata = [];
		Typedata.push({
			"ID": 1,
			"Value": 'Bar Chart'
		});
		Typedata.push({
			"ID": 2,
			"Value": 'Pie Chart'
		});
		$scope.types = Typedata;
		$scope.statistics.ddlyear = (new Date()).getFullYear();
		$scope.statistics.ddlMonth = (new Date()).getMonth() + 1;
		$scope.statistics.ddltype = 1;
		$timeout(function () { drawdetailUniqueuser(); }, 10);
		$timeout(function () { drawdetailApplicationuser(); }, 10);
		$timeout(function () { drawdetailBrowser(); }, 10);
		$timeout(function () { drawdetailBrowserVersion(); }, 10);
		$timeout(function () { drawdetailUserTypes(); }, 10);
		$timeout(function () { drawdetailOS(); }, 10);
		$timeout(function () { drawProofInitiatorDets(); }, 10);
		$timeout(function () { drawdetailStartPage(); }, 10);
		$timeout(function () { drawdetailRoles(); }, 10);
		$timeout(function () { drawdetailEntityCount(); }, 10);
		$timeout(function () { drawdetailEntityCreation(); }, 10);
		$timeout(function () { drawdetailbandwidth(); }, 10);
		$scope.UniqueuserchartContentID = "Uniqueuserchart";
		$scope.ApplicationuserContentID = "Applicationuserchart";
		$scope.BrowserContentID = "Browsersyschart";
		$scope.BrowserVersionContentID = "BrowserVersionsyschart";
		$scope.UserTypesContentID = "UserTypeschart";
		$scope.OperatingSystemchartContentID = "OperatingSystemchart";
		$scope.ProofIntiatorchartContentID = "PIdets";
		$scope.StartPageContentID = "StartPagechart";
		$scope.RoleContentID = "Roleschart";
		$scope.EntityCountContentID = "EntityCountchart";
		$scope.EntityCreationContentID = "EntityCreationchart";
		$scope.bandwidthContentID = "bandwidthchart"
		$scope.addModule = function(charttype) {
			$scope.statistics.ddlyear = (new Date()).getFullYear();
			$scope.statistics.ddlMonth = (new Date()).getMonth() + 1;
			$scope.statistics.ddltype = 1;
			switch (charttype) {
			    case 'Uniqueuserchart':
			        $('#moduleModal').modal('show');
			        break;
			    case 'Applicationuserchart':
			        $('#ApplicationusermoduleModal').modal('show');
			        break;
			    case 'Browsersyschart':
			        $('#BrowsermoduleModal').modal('show');
			        break;
			    case 'BrowserVersionsyschart':
			        $('#BrowserVersionmoduleModal').modal('show');
			        break;
			    case 'UserTypeschart':
			        $('#UserTypesmoduleModal').modal('show');
			        break;
			    case 'OperatingSystemchart':
			        $('#OSmoduleModal').modal('show');
			        break;
			    case 'StartPagechart':
			        $('#StartPagemoduleModal').modal('show');
			        break;
			    case 'Roleschart':
			        $('#RolesmoduleModal').modal('show');
			        break;
			    case 'EntityCountchart':
			        $('#EntityCountmoduleModal').modal('show');
			        break;
			    case 'EntityCreationchart':
			        $('#EntityCreationmoduleModal').modal('show');
			        break;
			    case 'bandwidthchart':
			        $('#bandwidthmoduleModal').modal('show');
			        break;
			    case 'PIdets':
			        $('#PImoduleModal').modal('show');
			        break;
            
			}
		};
		$scope.SaveUniqueuser = function() {
		    $timeout(function () { drawdetailUniqueuser(); }, 10);
			$('#moduleModal').modal('hide');
		};
		$scope.SaveApplicationuser = function() {
		    $timeout(function () { drawdetailApplicationuser(); }, 10);
			$('#ApplicationusermoduleModal').modal('hide');
		};
		$scope.SaveBrowser = function() {
		    $timeout(function () { drawdetailBrowser(); }, 10);
			$('#BrowsermoduleModal').modal('hide');
		};
		$scope.SaveBrowserVersion = function() {
		    $timeout(function () { drawdetailBrowserVersion(); }, 10);
			$('#BrowserVersionmoduleModal').modal('hide');
		};
		$scope.SaveUserTypes = function() {
		    $timeout(function () { drawdetailUserTypes(); }, 10);
			$('#UserTypesmoduleModal').modal('hide');
		};
		$scope.SaveOS = function() {
		    $timeout(function () { drawdetailOS(); }, 10);
			$('#OSmoduleModal').modal('hide');
		};
		$scope.SaveStartPage = function() {
		    $timeout(function () { drawdetailStartPage(); }, 10);
			$('#StartPagemoduleModal').modal('hide');
		};
		$scope.SaveRolesFilter = function() {
		    $timeout(function () { drawdetailRoles(); }, 10);
			$('#RolesmoduleModal').modal('hide');
		};
		$scope.SaveEntityCountFilter = function() {
		    $timeout(function () { drawdetailEntityCount(); }, 10);
			$('#EntityCountmoduleModal').modal('hide');
		};
		$scope.SaveEntityCreationFilter = function() {
		    $timeout(function () { drawdetailEntityCreation(); }, 10);
			$('#EntityCreationmoduleModal').modal('hide');
		};
		$scope.SaveBandwidth = function() {
		    $timeout(function () { drawdetailbandwidth(); }, 10);
			$('#bandwidthmoduleModal').modal('hide');
		};
		$scope.SaveProofInitiatorFilter = function() {
		    $timeout(function () { drawProofInitiatorDets(); }, 10);
			$('#PImoduleModal').modal('hide');
		};

		function drawdetailUniqueuser() {
			$scope.Uniqueuserchartpoints = [];
			var Uniqueuservaluenamemonth;
			var Uniqueuservaluetitle;
			var Uniqueuservalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var Uniqueuservaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (Uniqueuservaluemonth > 0) {
				Uniqueuservaluenamemonth = Monthdata[Uniqueuservaluemonth].Value;
				Uniqueuservaluetitle = 'Users logged in for ' + Uniqueuservaluenamemonth + ' ' + Uniqueuservalueyear;
			} else {
				Uniqueuservaluetitle = 'Users logged in for ' + Uniqueuservalueyear;
			}
			var Uniqueuservisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetUniqueuserhit(Uniqueuservalueyear, Uniqueuservaluemonth).then(function(GetUniqueuserlistvalue) {
				var UniqueuserResult = GetUniqueuserlistvalue.Response;
				var data = [];
				for (var i = 0; i < UniqueuserResult.length; i++) {
					if ($scope.GlobalChartType[Uniqueuservisualtype] == "column") {
						if (Uniqueuservaluemonth == 0) data.push({
							y: UniqueuserResult[i].value,
							label: Monthdata[UniqueuserResult[i].Days].Value,
							name: Monthdata[UniqueuserResult[i].Days].Value
						});
						else data.push({
							y: UniqueuserResult[i].value,
							label: UniqueuserResult[i].Days,
							name: UniqueuserResult[i].Days
						});
					} else {
						if (Uniqueuservaluemonth == 0) data.push({
							y: UniqueuserResult[i].value,
							legendText: Monthdata[UniqueuserResult[i].Days].Value + ": " + UniqueuserResult[i].value,
							indexLabel: "",
							name: Monthdata[UniqueuserResult[i].Days].Value
						});
						else data.push({
							y: UniqueuserResult[i].value,
							legendText: UniqueuserResult[i].Days + ": " + UniqueuserResult[i].value,
							indexLabel: "",
							name: UniqueuserResult[i].Days
						});
					}
				}
				$('#' + $scope.UniqueuserchartContentID + '').attr('data-visualType', $scope.GlobalChartType[Uniqueuservisualtype]);
				$('#' + $scope.UniqueuserchartContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.UniqueuserchartContentID + '').attr('data-charttitle', Uniqueuservaluetitle);
				UniqueuserRenderBarChart($scope.UniqueuserchartContentID);
			});
		};

		function drawdetailApplicationuser() {
			$scope.Applicationuserchartpoints = [];
			var Applicationuservaluenamemonth;
			var Applicationuservaluetitle;
			var Applicationuservalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var Applicationuservaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (Applicationuservaluemonth > 0) {
				Applicationuservaluenamemonth = Monthdata[Applicationuservaluemonth].Value;
				Applicationuservaluetitle = 'Number of visits for ' + Applicationuservaluenamemonth + ' ' + Applicationuservalueyear;
			} else {
				Applicationuservaluetitle = 'Number of visits for ' + Applicationuservalueyear;
			}
			var Applicationuservisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			var visualtype = $scope.ApplicationuservisualType;
			AdminService.GetApplicationhit(Applicationuservalueyear, Applicationuservaluemonth).then(function(res) {
				var ApplicationuserResult = res.Response;
				var data = [];
				for (var i = 0; i < ApplicationuserResult.length; i++) {
					if ($scope.GlobalChartType[Applicationuservisualtype] == "column") {
						if (Applicationuservaluemonth == 0) data.push({
							y: ApplicationuserResult[i].value,
							label: Monthdata[ApplicationuserResult[i].Days].Value,
							name: Monthdata[ApplicationuserResult[i].Days].Value
						});
						else data.push({
							y: ApplicationuserResult[i].value,
							label: ApplicationuserResult[i].Days,
							name: ApplicationuserResult[i].Days
						});
					} else {
						if (Applicationuservaluemonth == 0) data.push({
							y: ApplicationuserResult[i].value,
							legendText: Monthdata[ApplicationuserResult[i].Days].Value + ": " + ApplicationuserResult[i].value,
							indexLabel: "",
							name: Monthdata[ApplicationuserResult[i].Days].Value
						});
						else data.push({
							y: ApplicationuserResult[i].value,
							legendText: ApplicationuserResult[i].Days + ": " + ApplicationuserResult[i].value,
							indexLabel: "",
							name: ApplicationuserResult[i].Days
						});
					}
				}
				$('#' + $scope.ApplicationuserContentID + '').attr('data-visualType', $scope.GlobalChartType[Applicationuservisualtype]);
				$('#' + $scope.ApplicationuserContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.ApplicationuserContentID + '').attr('data-charttitle', Applicationuservaluetitle);
				ApplicationuserRenderBarChart($scope.ApplicationuserContentID);
			});
		};

		function drawdetailBrowser() {
			$scope.Browserpoints = [];
			var Browservaluenamemonth;
			var Browservaluetitle;
			var Browservalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var Browservaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (Browservaluemonth > 0) {
				Browservaluenamemonth = Monthdata[Browservaluemonth].Value;
				Browservaluetitle = 'Browser Summary for ' + Browservaluenamemonth + ' ' + Browservalueyear
			} else {
				Browservaluetitle = 'Browser Summary  for ' + Browservalueyear
			}
			var Browservisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetBrowserStatistic(Browservalueyear, Browservaluemonth).then(function(res) {
				var BrowserResult = res.Response;
				var data = [];
				for (var i = 0; i < BrowserResult.length; i++) {
					if ($scope.GlobalChartType[Browservisualtype] == "column") data.push({
						y: BrowserResult[i].VALUE,
						label: BrowserResult[i].NAME,
						name: BrowserResult[i].NAME
					});
					else data.push({
						y: BrowserResult[i].VALUE,
						legendText: BrowserResult[i].NAME + ": " + BrowserResult[i].VALUE,
						indexLabel: "",
						name: BrowserResult[i].NAME
					});
				}
				$('#' + $scope.BrowserContentID + '').attr('data-visualType', $scope.GlobalChartType[Browservisualtype]);
				$('#' + $scope.BrowserContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.BrowserContentID + '').attr('data-charttitle', Browservaluetitle);
				BrowserRenderBarChart($scope.BrowserContentID);
			});
		};

		function drawdetailBrowserVersion() {
			$scope.BrowserVersionpoints = [];
			var BrowserVersionvaluenamemonth;
			var BrowserVersionvaluetitle;
			var BrowserVersionvalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var BrowserVersionvaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (BrowserVersionvaluemonth > 0) {
				BrowserVersionvaluenamemonth = Monthdata[BrowserVersionvaluemonth].Value;
				BrowserVersionvaluetitle = 'Browser Version Summary for ' + BrowserVersionvaluenamemonth + ' ' + BrowserVersionvalueyear;
			} else {
				BrowserVersionvaluetitle = 'Browser Version Summary for ' + BrowserVersionvalueyear;
			}
			var BrowserVersionvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetBrowserVersionStatistic(BrowserVersionvalueyear, BrowserVersionvaluemonth).then(function(res) {
				var BrowserVersionResult = res.Response;
				var data = [];
				for (var i = 0; i < BrowserVersionResult.length; i++) {
					if ($scope.GlobalChartType[BrowserVersionvisualtype] == "column") data.push({
						y: BrowserVersionResult[i].VALUE,
						label: BrowserVersionResult[i].NAME + BrowserVersionResult[i].VERSION,
						name: BrowserVersionResult[i].NAME + BrowserVersionResult[i].VERSION
					});
					else data.push({
						y: BrowserVersionResult[i].VALUE,
						legendText: BrowserVersionResult[i].NAME + BrowserVersionResult[i].VERSION + ": " + BrowserVersionResult[i].VALUE,
						indexLabel: "",
						name: BrowserVersionResult[i].NAME + BrowserVersionResult[i].VERSION
					});
				}
				$('#' + $scope.BrowserVersionContentID + '').attr('data-visualType', $scope.GlobalChartType[BrowserVersionvisualtype]);
				$('#' + $scope.BrowserVersionContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.BrowserVersionContentID + '').attr('data-charttitle', BrowserVersionvaluetitle);
				BrowserVersionRenderBarChart($scope.BrowserVersionContentID);
			});
		};

		function UniqueuserRenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			if (data.length > 0) {
				CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
				if ($('#' + WizardID + '').attr('data-visualType') == "column") {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						colorSet: "finColorSet1",
						height: $('#' + WizardID + '').parents('.box-content').height() - 10,
						axisX: {
							tickColor: "white",
							tickLength: 5,
							labelFontFamily: "sans-serif",
							labelFontColor: "#7F7F7F",
							labelFontSize: 10,
							labelFontWeight: "normal"
						},
						axisY: {
							tickLength: 0,
							labelFontSize: 0,
							lineThickness: 0,
							gridThickness: 0,
							includeZero: true,
						},
						data: [{
							type: "column",
							indexLabel: "{y}",
							indexLabelFontSize: 12,
							indexLabelFontWeight: "normal",
							indexLabelPlacement: "outside",
							indexLabelOrientation: "horizontal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							legendMarkerColor: "gray",
							showInLegend: false,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				} else {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						animationEnabled: true,
						legend: {
							verticalAlign: "center",
							horizontalAlign: "right",
							fontSize: 12,
							fontweight: "normal",
							padding: 0,
							fontColor: "darkgrey",
							fontFamily: "sans-serif"
						},
						data: [{
							indexLabelFontSize: 12,
							indexLabelLineThickness: 0,
							indexLabelFontStyle: "italic",
							indexLabelFontWeight: "normal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							indexLabelLineColor: "darkgrey",
							indexLabelPlacement: "outside",
							type: "pie",
							showInLegend: true,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				}
				chart.render();
			} else {
				$scope.dynWizard_Cont = '<span class="nodata">No data Available.</span>';
				$('#' + WizardID + '').html($compile($scope.dynWizard_Cont)($scope));
			}
		}

		function ApplicationuserRenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			if (data.length > 0) {
				CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
				if ($('#' + WizardID + '').attr('data-visualType') == "column") {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						colorSet: "finColorSet1",
						height: $('#' + WizardID + '').parents('.box-content').height() - 10,
						axisX: {
							tickColor: "white",
							tickLength: 5,
							labelFontFamily: "sans-serif",
							labelFontColor: "#7F7F7F",
							labelFontSize: 10,
							labelFontWeight: "normal"
						},
						axisY: {
							tickLength: 0,
							labelFontSize: 0,
							lineThickness: 0,
							gridThickness: 0,
							includeZero: true,
						},
						data: [{
							type: "column",
							indexLabel: "{y}",
							indexLabelFontSize: 12,
							indexLabelFontWeight: "normal",
							indexLabelPlacement: "outside",
							indexLabelOrientation: "horizontal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							legendMarkerColor: "gray",
							showInLegend: false,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				} else {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						animationEnabled: true,
						legend: {
							verticalAlign: "center",
							horizontalAlign: "right",
							fontSize: 12,
							fontweight: "normal",
							padding: 0,
							fontColor: "darkgrey",
							fontFamily: "sans-serif"
						},
						data: [{
							indexLabelFontSize: 12,
							indexLabelLineThickness: 0,
							indexLabelFontStyle: "italic",
							indexLabelFontWeight: "normal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							indexLabelLineColor: "darkgrey",
							indexLabelPlacement: "outside",
							type: "pie",
							showInLegend: true,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				}
				chart.render();
			} else {
				$scope.dynWizard_Cont = '<span class="nodata">No data Available.</span>';
				$('#' + WizardID + '').html($compile($scope.dynWizard_Cont)($scope));
			}
		}

		function BrowserRenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			if (data.length > 0) {
				CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
				if ($('#' + WizardID + '').attr('data-visualType') == "column") {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						colorSet: "finColorSet1",
						height: $('#' + WizardID + '').parents('.box-content').height() - 10,
						axisX: {
							tickColor: "white",
							tickLength: 5,
							labelFontFamily: "sans-serif",
							labelFontColor: "#7F7F7F",
							labelFontSize: 10,
							labelFontWeight: "normal"
						},
						axisY: {
							tickLength: 0,
							labelFontSize: 0,
							lineThickness: 0,
							gridThickness: 0,
							includeZero: true,
						},
						data: [{
							type: "column",
							indexLabel: "{y}",
							indexLabelFontSize: 12,
							indexLabelFontWeight: "normal",
							indexLabelPlacement: "outside",
							indexLabelOrientation: "horizontal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							legendMarkerColor: "gray",
							showInLegend: false,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				} else {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						animationEnabled: true,
						legend: {
							verticalAlign: "center",
							horizontalAlign: "right",
							fontSize: 12,
							fontweight: "normal",
							padding: 0,
							fontColor: "darkgrey",
							fontFamily: "sans-serif"
						},
						data: [{
							indexLabelFontSize: 12,
							indexLabelLineThickness: 0,
							indexLabelFontStyle: "italic",
							indexLabelFontWeight: "normal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							indexLabelLineColor: "darkgrey",
							indexLabelPlacement: "outside",
							type: "pie",
							showInLegend: true,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				}
				chart.render();
			} else {
				$scope.dynWizard_Cont = '<span class="nodata">No data Available.</span>';
				$('#' + WizardID + '').html($compile($scope.dynWizard_Cont)($scope));
			}
		}

		function BrowserVersionRenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			if (data.length > 0) {
				CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
				if ($('#' + WizardID + '').attr('data-visualType') == "column") {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						colorSet: "finColorSet1",
						height: $('#' + WizardID + '').parents('.box-content').height() - 10,
						axisX: {
							tickColor: "white",
							tickLength: 5,
							labelFontFamily: "sans-serif",
							labelFontColor: "#7F7F7F",
							labelFontSize: 10,
							labelFontWeight: "normal"
						},
						axisY: {
							tickLength: 0,
							labelFontSize: 0,
							lineThickness: 0,
							gridThickness: 0,
							includeZero: true,
						},
						data: [{
							type: "column",
							indexLabel: "{y}",
							indexLabelFontSize: 12,
							indexLabelFontWeight: "normal",
							indexLabelPlacement: "outside",
							indexLabelOrientation: "horizontal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							legendMarkerColor: "gray",
							showInLegend: false,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				} else {
					var chart = new CanvasJS.Chart(WizardID, {
						title: {
							text: charttitle,
							verticalAlign: "top",
							horizontalAlign: "center",
							fontSize: 15,
							fontFamily: "Calibri",
							fontWeight: "normal",
							fontColor: "black",
							fontStyle: "normal",
							borderThickness: 0,
							borderColor: "black",
							cornerRadius: 0,
							backgroundColor: null,
							margin: 0
						},
						animationEnabled: true,
						legend: {
							verticalAlign: "center",
							horizontalAlign: "right",
							fontSize: 12,
							fontweight: "normal",
							padding: 0,
							fontColor: "darkgrey",
							fontFamily: "sans-serif"
						},
						data: [{
							indexLabelFontSize: 12,
							indexLabelLineThickness: 0,
							indexLabelFontStyle: "italic",
							indexLabelFontWeight: "normal",
							indexLabelFontFamily: "sans-serif",
							indexLabelFontColor: "darkgrey",
							indexLabelLineColor: "darkgrey",
							indexLabelPlacement: "outside",
							type: "pie",
							showInLegend: true,
							toolTipContent: "{name}:<strong>{y}</strong>",
							dataPoints: data
						}]
					});
				}
				chart.render();
			} else {
				$scope.dynWizard_Cont = '<span class="nodata">No data Available.</span>';
				$('#' + WizardID + '').html($compile($scope.dynWizard_Cont)($scope));
			}
		}

		function drawdetailUserTypes() {
			$scope.UserTypeschartpoints = [];
			var UserTypesvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetUserStatistic().then(function(res) {
				var UserTypesResult = res.Response;
				var data = [];
				for (var i = 0; i < UserTypesResult.length; i++) {
					if ($scope.GlobalChartType[UserTypesvisualtype] == "column") data.push({
						y: UserTypesResult[i].value,
						label: UserTypesResult[i].Users,
						name: UserTypesResult[i].Users
					});
					else data.push({
						y: UserTypesResult[i].value,
						legendText: UserTypesResult[i].Users + ": " + UserTypesResult[i].value,
						indexLabel: "",
						name: UserTypesResult[i].Users
					});
				}
				$('#' + $scope.UserTypesContentID + '').attr('data-visualType', $scope.GlobalChartType[UserTypesvisualtype]);
				$('#' + $scope.UserTypesContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.UserTypesContentID + '').attr('data-charttitle', 'User Types Summary');
				UniqueuserRenderBarChart($scope.UserTypesContentID);
			});
		};

		function drawdetailOS() {
			$scope.OSchartpoints = [];
			var OSvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetOSStatistic().then(function(res) {
				var OSResult = res.Response;
				var data = [];
				for (var i = 0; i < OSResult.length; i++) {
					if ($scope.GlobalChartType[OSvisualtype] == "column") data.push({
						y: OSResult[i].VALUE,
						label: OSResult[i].NAME,
						name: OSResult[i].NAME
					});
					else data.push({
						y: OSResult[i].VALUE,
						legendText: OSResult[i].NAME + ": " + OSResult[i].VALUE,
						indexLabel: "",
						name: OSResult[i].NAME
					});
				}
				$('#' + $scope.OperatingSystemchartContentID + '').attr('data-visualType', $scope.GlobalChartType[OSvisualtype]);
				$('#' + $scope.OperatingSystemchartContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.OperatingSystemchartContentID + '').attr('data-charttitle', 'Operating System Summary');
				UniqueuserRenderBarChart($scope.OperatingSystemchartContentID);
			});
		};

		function drawdetailStartPage() {
			$scope.StartPagechartpoints = [];
			var StartPagevisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetstartpageStatistic().then(function(res) {
				var StartPageResult = res.Response;
				var data = [];
				for (var i = 0; i < StartPageResult.length; i++) {
					if ($scope.GlobalChartType[StartPagevisualtype] == "column") data.push({
						y: StartPageResult[i].value,
						label: StartPageResult[i].page,
						name: StartPageResult[i].page
					});
					else data.push({
						y: StartPageResult[i].value,
						legendText: StartPageResult[i].page + ": " + StartPageResult[i].value,
						indexLabel: "",
						name: StartPageResult[i].page
					});
				}
				$('#' + $scope.StartPageContentID + '').attr('data-visualType', $scope.GlobalChartType[StartPagevisualtype]);
				$('#' + $scope.StartPageContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.StartPageContentID + '').attr('data-charttitle', 'Start Page Summary');
				UniqueuserRenderBarChart($scope.StartPageContentID);
			});
		};

		function drawdetailRoles() {
			$scope.Rolechartpoints = [];
			var Rolevisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetUserRoleStatistic().then(function(res) {
				var RoleResult = res.Response;
				var data = [];
				for (var i = 0; i < RoleResult.length; i++) {
					if ($scope.GlobalChartType[Rolevisualtype] == "column") data.push({
						y: RoleResult[i].value,
						label: RoleResult[i].Role,
						name: RoleResult[i].Role
					});
					else data.push({
						y: RoleResult[i].value,
						legendText: RoleResult[i].Role + ": " + RoleResult[i].value,
						indexLabel: "",
						name: RoleResult[i].Role
					});
				}
				$('#' + $scope.RoleContentID + '').attr('data-visualType', $scope.GlobalChartType[Rolevisualtype]);
				$('#' + $scope.RoleContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.RoleContentID + '').attr('data-charttitle', 'User Roles Summary');
				UniqueuserRenderBarChart($scope.RoleContentID);
			});
		};

		function drawdetailEntityCount() {
			$scope.EntityCountchartpoints = [];
			var EntityCountvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetEnityStatistic().then(function(res) {
				var EntityCountResult = res.Response;
				var data = [];
				for (var i = 0; i < EntityCountResult.length; i++) {
					if ($scope.GlobalChartType[EntityCountvisualtype] == "column") data.push({
						y: EntityCountResult[i].value,
						label: EntityCountResult[i].EnitiesStatus,
						name: EntityCountResult[i].EnitiesStatus
					});
					else data.push({
						y: EntityCountResult[i].value,
						legendText: EntityCountResult[i].EnitiesStatus + ": " + EntityCountResult[i].value,
						indexLabel: "",
						name: EntityCountResult[i].EnitiesStatus
					});
				}
				$('#' + $scope.EntityCountContentID + '').attr('data-visualType', $scope.GlobalChartType[EntityCountvisualtype]);
				$('#' + $scope.EntityCountContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.EntityCountContentID + '').attr('data-charttitle', 'Entity Summary');
				UniqueuserRenderBarChart($scope.EntityCountContentID);
			});
		};

		function drawdetailEntityCreation() {
			$scope.EntityCreationchartpoints = [];
			var EntityCreationvaluenamemonth;
			var EntityCreationvaluetitle;
			var EntityCreationvalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var EntityCreationvaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (EntityCreationvaluemonth > 0) {
				EntityCreationvaluenamemonth = Monthdata[EntityCreationvaluemonth].Value;
				EntityCreationvaluetitle = 'Entity Creation Trend for  ' + EntityCreationvaluenamemonth + ' ' + EntityCreationvalueyear;
			} else {
				EntityCreationvaluetitle = 'Entity Creation Trend for  ' + EntityCreationvalueyear;
			}
			var EntityCreationvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetEnityCreateationStatistic(EntityCreationvalueyear, EntityCreationvaluemonth).then(function(res) {
				var EntityCreationResult = res.Response;
				var data = [];
				for (var i = 0; i < EntityCreationResult.length; i++) {
					if ($scope.GlobalChartType[EntityCreationvisualtype] == "column") {
						if (EntityCreationvaluemonth == 0) data.push({
							y: EntityCreationResult[i].value,
							label: Monthdata[EntityCreationResult[i].Days].Value,
							name: Monthdata[EntityCreationResult[i].Days].Value
						});
						else data.push({
							y: EntityCreationResult[i].value,
							label: EntityCreationResult[i].Days,
							name: EntityCreationResult[i].Days
						});
					} else {
						if (EntityCreationvaluemonth == 0) data.push({
							y: EntityCreationResult[i].value,
							legendText: Monthdata[EntityCreationResult[i].Days].Value + ": " + EntityCreationResult[i].value,
							indexLabel: "",
							name: Monthdata[EntityCreationResult[i].Days].Value
						});
						else data.push({
							y: EntityCreationResult[i].value,
							legendText: EntityCreationResult[i].Days + ": " + EntityCreationResult[i].value,
							indexLabel: "",
							name: EntityCreationResult[i].Days
						});
					}
				}
				$('#' + $scope.EntityCreationContentID + '').attr('data-visualType', $scope.GlobalChartType[EntityCreationvisualtype]);
				$('#' + $scope.EntityCreationContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.EntityCreationContentID + '').attr('data-charttitle', EntityCreationvaluetitle);
				UniqueuserRenderBarChart($scope.EntityCreationContentID);
			});
		};

		function drawdetailbandwidth() {
			$scope.bandwidthchartpoints = [];
			var bandwidthvaluenamemonth;
			var bandwidthvaluetitle;
			var bandwidthvalueyear = $scope.statistics.ddlyear == undefined ? (new Date()).getFullYear() : $scope.statistics.ddlyear;
			var bandwidthvaluemonth = $scope.statistics.ddlMonth == undefined ? (new Date()).getMonth() + 1 : $scope.statistics.ddlMonth;
			if (bandwidthvaluemonth > 0) {
				bandwidthvaluenamemonth = Monthdata[bandwidthvaluemonth].Value;
				bandwidthvaluetitle = 'Bandwidth usage (in MB) for  ' + bandwidthvaluenamemonth + ' ' + bandwidthvalueyear;
			} else {
				bandwidthvaluetitle = 'Bandwidth usage (in MB) for  ' + bandwidthvalueyear;
			}
			var bandwidthvisualtype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetbandwidthStatistic(bandwidthvalueyear, bandwidthvaluemonth).then(function(res) {
				var bandwidthResult = res.Response;
				var data = [];
				for (var i = 0; i < bandwidthResult.length; i++) {
					if ($scope.GlobalChartType[bandwidthvisualtype] == "column") {
						if (bandwidthvaluemonth == 0) data.push({
							y: bandwidthResult[i].Cvalue,
							label: Monthdata[bandwidthResult[i].Days].Value,
							name: Monthdata[bandwidthResult[i].Days].Value
						});
						else data.push({
							y: bandwidthResult[i].Cvalue,
							label: bandwidthResult[i].Days,
							name: bandwidthResult[i].Days
						});
					} else {
						if (bandwidthvaluemonth == 0) data.push({
							y: bandwidthResult[i].Cvalue,
							legendText: Monthdata[bandwidthResult[i].Days].Value + ": " + bandwidthResult[i].Cvalue,
							indexLabel: "",
							name: Monthdata[bandwidthResult[i].Days].Value
						});
						else data.push({
							y: bandwidthResult[i].Cvalue,
							legendText: bandwidthResult[i].Days + ": " + bandwidthResult[i].Cvalue,
							indexLabel: "",
							name: bandwidthResult[i].Days
						});
					}
				}
				$('#' + $scope.bandwidthContentID + '').attr('data-visualType', $scope.GlobalChartType[bandwidthvisualtype]);
				$('#' + $scope.bandwidthContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.bandwidthContentID + '').attr('data-charttitle', bandwidthvaluetitle);
				UniqueuserRenderBarChart($scope.bandwidthContentID);
			});
		};

		function drawProofInitiatorDets() {
			$scope.PIchartpoints = [];
			var PIchartpointstype = $scope.statistics.ddltype == undefined ? 1 : $scope.statistics.ddltype;
			AdminService.GetProofInitiatorStatistic().then(function(GetPIlistvalue) {
				var PIResult = GetPIlistvalue.Response;
				var data = [];
				for (var i = 0; i < PIResult.length; i++) {
					if ($scope.GlobalChartType[PIchartpointstype] == "column") data.push({
						y: PIResult[i].Count,
						label: PIResult[i].NAME,
						name: PIResult[i].NAME
					});
					else data.push({
						y: PIResult[i].Count,
						legendText: PIResult[i].NAME + ": " + PIResult[i].Count,
						indexLabel: "",
						name: PIResult[i].NAME
					});
				}
				$('#' + $scope.ProofIntiatorchartContentID + '').attr('data-visualType', $scope.GlobalChartType[PIchartpointstype]);
				$('#' + $scope.ProofIntiatorchartContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.ProofIntiatorchartContentID + '').attr('data-charttitle', 'User Roles Summary');
				UniqueuserRenderBarChart($scope.ProofIntiatorchartContentID);
			});
		};
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.admin.statisticsCtrl']"));
		});
	}
	app.controller("mui.admin.statisticsCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'AdminService', muiadminstatisticsCtrl]);
})(angular, app);