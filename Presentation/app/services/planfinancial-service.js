﻿(function (ng, app) {
    "use strict"; function PlanfinancialService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ GetEntityFinancialdForecastHeadings: GetEntityFinancialdForecastHeadings, GetFundingCostcenterMetadata: GetFundingCostcenterMetadata, GetEntityFinancialdDetail: GetEntityFinancialdDetail, GetfundingRequestsByEntityID: GetfundingRequestsByEntityID, GetFinancialForecastsettings: GetFinancialForecastsettings, GetlastUpdatedtime: GetlastUpdatedtime, GetFinancialforecastData: GetFinancialforecastData, AddCostCenterForFinancial: AddCostCenterForFinancial, FundRequestByID: FundRequestByID, GetAllPurchaseOrdersByEntityID: GetAllPurchaseOrdersByEntityID, GetAllInvoiceByEntityID: GetAllInvoiceByEntityID, GetAllSentPurchaseOrdersByEntityID: GetAllSentPurchaseOrdersByEntityID, EntityForecastAmountInsert: EntityForecastAmountInsert, InsertUpdateFinancialTrancation: InsertUpdateFinancialTrancation, EntityForecastAmountUpdate: EntityForecastAmountUpdate, GetCostcentreforFinancial: GetCostcentreforFinancial, ReleaseFund: ReleaseFund, AdjustApprovePlannedAmount: AdjustApprovePlannedAmount, EntityMoneyTransfer: EntityMoneyTransfer, EntityPlannedAmountInsert: EntityPlannedAmountInsert, ApprovePlannedAmountUpdate: ApprovePlannedAmountUpdate, DeleteCostcentreFinancial: DeleteCostcentreFinancial, CreateFundingRequest: CreateFundingRequest, GetAllSupplier: GetAllSupplier, CreateNewPurchaseOrder: CreateNewPurchaseOrder, UpdatePurchaseOrder: UpdatePurchaseOrder, CreateNewSupplier: CreateNewSupplier, ApprovePurchaseOrders: ApprovePurchaseOrders, SendPurchaseOrders: SendPurchaseOrders, RejectPurchaseOrders: RejectPurchaseOrders, GetPlanningTransactionsByEID: GetPlanningTransactionsByEID, DeletePlanTransactions: DeletePlanTransactions, CreateNewInvoice: CreateNewInvoice, GetNewsfeedFundRequestTaskDetails: GetNewsfeedFundRequestTaskDetails, SaveFinancialDynamicValues: SaveFinancialDynamicValues, GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById, GetAllCurrencyType: GetAllCurrencyType, GetDynamicfinancialattributes: GetDynamicfinancialattributes }); function GetEntityFinancialdForecastHeadings(EntityID, DivisionID, Iscc) { var request = $http({ method: "get", url: "api/Planning/GetEntityFinancialdForecastHeadings/" + EntityID + "/" + DivisionID + "/" + Iscc, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFundingCostcenterMetadata(Metadatatype) { var request = $http({ method: "get", url: "api/Planning/GetFundingCostcenterMetadata/" + Metadatatype, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityFinancialdDetail(EntityID, UserID, startRow, endRow, includedetails) { var request = $http({ method: "get", url: "api/Planning/GetEntityFinancialdDetail/" + EntityID + "/" + UserID + "/" + startRow + "/" + endRow + "/" + includedetails, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetfundingRequestsByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetfundingRequestsByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFinancialForecastsettings() { var request = $http({ method: "get", url: "api/Planning/GetFinancialForecastsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetlastUpdatedtime(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetlastUpdatedtime/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetFinancialforecastData() { var request = $http({ method: "get", url: "api/Planning/GetFinancialforecastData/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function AddCostCenterForFinancial(formobj) { var request = $http({ method: "post", url: "api/Planning/AddCostCenterForFinancial/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function FundRequestByID(ID, EntityID) { var request = $http({ method: "delete", url: "api/Planning/FundRequestByID/" + ID + "/" + EntityID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllPurchaseOrdersByEntityID(EntityID) { var request = $http({ method: "put", ignoreLoadingBar: true, url: "api/Planning/GetAllPurchaseOrdersByEntityID/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllInvoiceByEntityID(EntityID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Planning/GetAllInvoiceByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllSentPurchaseOrdersByEntityID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetAllSentPurchaseOrdersByEntityID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function EntityForecastAmountInsert(formobj) { var request = $http({ method: "post", url: "api/Planning/EntityForecastAmountInsert/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function InsertUpdateFinancialTrancation(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertUpdateFinancialTrancation/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityForecastAmountUpdate(EntityID) { var request = $http({ method: "post", url: "api/Planning/EntityForecastAmountUpdate/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCostcentreforFinancial(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentreforFinancial/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function ReleaseFund(formobj) { var request = $http({ method: "put", url: "api/Planning/ReleaseFund/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function AdjustApprovePlannedAmount(EntityID) { var request = $http({ method: "put", url: "api/Planning/AdjustApprovePlannedAmount/" + EntityID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function EntityMoneyTransfer(formobj) { var request = $http({ method: "put", url: "api/Planning/EntityMoneyTransfer/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function EntityPlannedAmountInsert(formobj) { var request = $http({ method: "post", url: "api/Planning/EntityPlannedAmountInsert/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function ApprovePlannedAmountUpdate(formobj) { var request = $http({ method: "put", url: "api/Planning/ApprovePlannedAmountUpdate/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeleteCostcentreFinancial(EntityID, CostCenterID) { var request = $http({ method: "delete", url: "api/Planning/DeleteCostcentreFinancial/" + EntityID + "/" + CostCenterID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function CreateFundingRequest(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateFundingRequest/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetAllSupplier() { var request = $http({ method: "get", url: "api/Planning/GetAllSupplier/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateNewPurchaseOrder(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateNewPurchaseOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdatePurchaseOrder(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdatePurchaseOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateNewSupplier(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateNewSupplier/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function ApprovePurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/ApprovePurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function SendPurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/SendPurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function RejectPurchaseOrders(formobj) { var request = $http({ method: "post", url: "api/Planning/RejectPurchaseOrders/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetPlanningTransactionsByEID(formobj) { var request = $http({ method: "post", url: "api/Planning/GetPlanningTransactionsByEID/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function DeletePlanTransactions(formobj) { var request = $http({ method: "post", url: "api/Planning/DeletePlanTransactions/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CreateNewInvoice(formobj) {
            var srcUrl = "";
            if (formobj.PONumber != 0)
                srcUrl = 'api/Planning/CreateNewInvoice/';
            //else if (formobj.PONumber == 0)
            //    srcUrl = 'api/Planning/CreateInvoiceAndPurchaseOrder/';
            else
                srcUrl = 'api/Planning/CreateNewInvoice/';
            var request = $http({ method: "post", url: srcUrl, params: { action: "add" }, data: formobj });
            return (request.then(handleSuccess, handleError));
        }
        function GetNewsfeedFundRequestTaskDetails(FundID) { var request = $http({ method: "get", url: "api/Planning/GetNewsfeedFundRequestTaskDetails/" + FundID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function SaveFinancialDynamicValues(formobj) { var request = $http({ method: "post", url: "api/Planning/SaveFinancialDynamicValues/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllCurrencyType() { var request = $http({ method: "get", url: "api/Planning/GetAllCurrencyType/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetDynamicfinancialattributes() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetDynamicfinancialattributes",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("PlanfinancialService", ['$http', '$q', PlanfinancialService]);
})(angular, app);