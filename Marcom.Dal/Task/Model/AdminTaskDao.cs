﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class AdminTaskDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _TaskListID;
        protected int _TaskType;
        protected string _Caption;
        protected string _description;
        protected int _Sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public AdminTaskDao() { }

        public AdminTaskDao(int pTaskListID, string pCaption,int pTaskType, string pDescription, int pSortorder)
        {
            this._TaskListID = pTaskListID;
            this._Caption = pCaption;
            this._description = pDescription;
            this._TaskType = pTaskType;
            this._Sortorder = pSortorder;
          
        }

        public AdminTaskDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }



        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1000000)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1000000 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string Caption
        {
            get { return _Caption; }
            set { _bIsChanged |= (_Caption != value); _Caption = value; }

        }

        public virtual int TaskListID
        {
            get { return _TaskListID; }
            set { _bIsChanged |= (_TaskListID != value); _TaskListID = value; }

        }

        public virtual int TaskType
        {
            get { return _TaskType; }
            set { _bIsChanged |= (_TaskType != value); _TaskType = value; }

        }

        public virtual int Sortorder
        {
            get { return _Sortorder; }
            set { _bIsChanged |= (_Sortorder != value); _Sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AdminTaskDao = "AdminTaskDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string TaskListID = "TaskListID";
            public const string TaskType = "TaskType";
            public const string Description = "Description";
            public const string Sortorder = "Sortorder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AdminTaskDao = "AdminTaskDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string TaskListID = "TaskListID";
            public const string TaskType = "TaskType";
            public const string Description = "Description";
            public const string Sortorder = "Sortorder";
        }

        #endregion


    }

}
