﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Metadata
{
    public class AttributeSettings
    {
        #region Member Variables

        protected int _id;
        protected int _type;
        protected int _level;
        protected bool _isselect;
        protected bool _isfilter;
        protected bool _isorderby;
        protected string _wherecondition;
        protected string _field;
        protected string _displayname;
        protected bool _isspecial;

        #endregion
        //AttributeIDs
        #region Constructors
		public AttributeSettings() {}

        public AttributeSettings(int pId, int pType, int pLevel, bool pIsSelect, bool pIsFilter, bool pIsOrderBy, string pWhereCondition, string pField, string pDisplayName, bool pIsSpecial)
		{
            this._id = pId;
            this._type = pType; 
			this._level = pLevel;
            this._isselect = pIsSelect;
            this._isfilter = pIsFilter;
            this._isorderby = pIsOrderBy;
            this._wherecondition = pWhereCondition;
            this._field = pField;
            this._displayname = pDisplayName;
            this._isspecial = pIsSpecial;
		}

		#endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }

        }
        public bool IsSelect
        {
            get { return _isselect; }
            set { _isselect = value; }

        }

        public bool IsFilter
        {
            get { return _isfilter; }
            set { _isfilter = value; }

        }

        public bool IsOrderBy
        {
            get { return _isorderby; }
            set { _isorderby = value; }

        }
        public string WhereCondition
        {
            get { return _wherecondition; }
            set { _wherecondition = value; }

        }

        public string Field
        {
            get { return _field; }
            set { _field = value; }

        }

        public bool IsSpecial
        {
            get { return _isspecial; }
            set { _isspecial = value; }

        }

        public string DisplayName
        {
            get { return _displayname; }
            set { _displayname = value; }

        }

        #endregion 

        
    }
}
