﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.User.Interface;

namespace BrandSystems.Marcom.Core.User
{
    public class ApprovalRole: IApprovalRole
    {
        #region Member Variables

		protected int _id;
		protected string _caption;
        protected string _description;
		#endregion
		
		#region Constructors
		public ApprovalRole() {}

        public ApprovalRole(int pId, string pCaption, string pDescription)
		{
            this._id = pId;
            this._caption = pCaption;
            this._description = pDescription; 
		}
				
		#endregion
		
		#region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }


        public string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
		
		#endregion 
    }
}
