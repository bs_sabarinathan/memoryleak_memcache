using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjective interface for table 'PM_Objective'.
    /// </summary>
    public interface IObjective : IBaseEntity
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }
        string Description
        {
            get;
            set;

        }
        string Instruction
        {
            get;
            set;

        }

        bool IsEnableFeedback
        {
            get;
            set;

        }

        DateTime StartDate
        {
            get;
            set;

        }

        DateTime EndDate
        {
            get;
            set;

        }

        int? DateRule
        {
            get;
            set;

        }

        bool? IsMandatory
        {
            get;
            set;

        }

        IObjectiveNumeric Numeric
        {
            get;
            set;

        }

        IList<IObjectiveRating> Ratings
        {
            get;
            set;

        }

        IList<IObjectiveCondition> Conditions
        {
            get;
            set;

        }

        IList<IObjectiveEntityValue> Value
        {
            get;
            set;

        }

        IList<IAttributeData> ObjectiveAttributes
        {
            get;
            set;
        }

        bool ObjectiveStatus
        {
            get;
            set;

        }
        

        #endregion
    }
}
