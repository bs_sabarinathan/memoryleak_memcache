﻿(function (ng, app) {
    "use strict";

    function muiadmintaskPredefinedflowCtrl($scope, $compile, $location, $resource, $timeout, $cookies, $translate, AdminService, $sce) {
        $scope.templateData = {
            "TemplateList": [],
            PhaseData: {
                AllPhaseList: [],
                PhaseList: [{
                    "ID": 0,
                    "Name": "",
                    "Description": "",
                    "Colorcode": ""
                }],
                StepsData: {
                    StepList: [],
                    "StepID": 0,
                    "Name": "",
                    "Description": "",
                    "Stepduration": "",
                    "StepMinApproval": '',
                    "Steproles": '',
                    "IsMandatory": false,
                    "PhaseId": 0
                }
            },
            templateSrcData: {
                "Name": "",
                "Description": "",
                "Templateid": 0,
                "ID": 0
            },
            UserRoles: [],
            phaseTempData: {},
            StepTempData: {}
        };
        $scope.IsUpdate = false;
        GetAllTemplates();
        GetAllUserRoles();

        function GetAllTemplates() {
            AdminService.GetAllApprovalFlowTemplates().then(function (Templatesdetails) {
                $scope.templateData.templateSrcData = Templatesdetails.Response;
            });
        }

        function getphasewithsteps(phaseid) {
            $scope.templateData.PhaseData.AllPhaseList = [];
            AdminService.GetAllApprovalFlowPhasesSteps(phaseid).then(function (data) {
                if (data != null) {
                    FormSourceData(data.Response);
                } else { }
            });
        }

        function xmlToJson(xml) {
            var obj = {};
            if (xml.nodeType == 1) {
                if (xml.attributes.length > 0) {
                    obj["@attributes"] = {};
                    for (var j = 0; j < xml.attributes.length; j++) {
                        var attribute = xml.attributes.item(j);
                        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                    }
                }
            } else if (xml.nodeType == 3) {
                obj = xml.nodeValue;
            }
            if (xml.hasChildNodes()) {
                for (var i = 0; i < xml.childNodes.length; i++) {
                    var item = xml.childNodes.item(i);
                    var nodeName = item.nodeName;
                    if (typeof (obj[nodeName]) == "undefined") {
                        obj[nodeName] = xmlToJson(item);
                    } else {
                        if (typeof (obj[nodeName].push) == "undefined") {
                            var old = obj[nodeName];
                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(xmlToJson(item));
                    }
                }
            }
            return obj;
        }

        function FormSourceData(data) {
            var PhaseData = [],
				steps = [];
            for (var i = 0, val; val = data[i++];) {
                steps = [];
                var stepjson = xmlToJson($.parseXML(val.steps));
                if (stepjson != null) {
                    if (stepjson.A.p != null) {
                        if (Object.prototype.toString.call(stepjson.A.p) !== '[object Array]') {
                            var rolearray = [];
                            if (stepjson.A.p['Roles'] != null) rolearray = (Object.keys(stepjson.A.p['Roles']).length > 0) ? stepjson.A.p['Roles']["#text"].split(",") : [];
                            for (var r = 0; r <= rolearray.length - 1; r++) {
                                rolearray[r] = parseInt(rolearray[r]);
                            }
                            steps.push({
                                "Name": stepjson.A.p['Name']["#text"],
                                "ID": stepjson.A.p['ID']["#text"],
                                "MinApproval": stepjson.A.p['MinApproval']["#text"],
                                "Duration": stepjson.A.p['Duration']["#text"],
                                "Description": stepjson.A.p['Description'] != undefined ? stepjson.A.p['Description']["#text"] : "",
                                "PhaseId": stepjson.A.p['PhaseId']["#text"],
                                "Roles": rolearray,
                                IsMandatory: Boolean(stepjson.A.p['IsMandatory']["#text"].toString().toLowerCase() == "1" ? true : false),
                                isopened: true
                            });
                        } else {
                            for (var j = 0, st; st = stepjson.A.p[j++];) {
                                if (st['Roles'] != null) {
                                    var rolearray = (Object.keys(st['Roles']).length > 0) ? st['Roles']["#text"].split(",") : [];
                                    for (var r = 0; r <= rolearray.length - 1; r++) {
                                        rolearray[r] = parseInt(rolearray[r]);
                                    }
                                } else {
                                    var rolearray = [];
                                }
                                steps.push({
                                    "Name": st['Name']["#text"],
                                    "ID": st['ID']["#text"],
                                    "MinApproval": st['MinApproval']["#text"],
                                    "Duration": st['Duration']["#text"],
                                    "Description": (st['Description']) == null ? "" : (st['Description']["#text"]),
                                    "PhaseId": st['PhaseId']["#text"],
                                    "Roles": rolearray,
                                    IsMandatory: (st['IsMandatory'] == null ? '' : st['IsMandatory']["#text"]),
                                    isopened: true
                                });
                            }
                        }
                    }
                }
                val.StepsData = steps;
                PhaseData.push(val);
            }
            $scope.templateData.PhaseData.AllPhaseList.push(PhaseData);
        }
        $scope.templateData.templateSrcData.Colorcode = 'ffffff';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { };
        $scope.PhaseDuration = [{
            ID: 1,
            Caption: "+1 days"
        }, {
            ID: 2,
            Caption: "+2 days"
        }, {
            ID: 3,
            Caption: "+3 days"
        }, {
            ID: 4,
            Caption: "+4 days"
        }, {
            ID: 5,
            Caption: "+5 days"
        }, {
            ID: 6,
            Caption: "+6 days"
        }, {
            ID: 7,
            Caption: "+1 week"
        }, {
            ID: 14,
            Caption: "+2 weeks"
        }, {
            ID: 21,
            Caption: "+3 weeks"
        }, {
            ID: 30,
            Caption: "+1 month"
        }];
        $scope.PhaseMinApproval = ["1", "2", "3", "4", "5", "6", "7", "10", "15", "20", "25", "30"];
        $scope.ClearPhaseSteps = function () {
            $scope.Phasename = "";
            $scope.Phasedescription = "";
            $scope.Phasecolorcode = "";
            $scope.TaskTemplateStepName = "";
            $scope.TaskTemplateStepDescription = "";
            $scope.TaskTemplateStepduration = "";
            $scope.TaskTemplateStepMinApproval = "";
            $scope.TaskTemplateSteproles = "";
            $scope.TaskTemplateIsMandatory = false;
        }
        $scope.EditApprovalTemplate = function (tempid) {
            var SelectedTempDets = $.grep($scope.templateData.templateSrcData, function (e) {
                return e.ID == tempid
            });
            $('#approvalflowTempPopup').modal('show');
            getphasewithsteps(tempid);
            $scope.TaskTemplateName = SelectedTempDets[0].Name;
            $scope.TaskTemplateDescription = SelectedTempDets[0].Description;
            $scope.templateData.PhaseData.PhaseList[0].Templateid = tempid;
        }
        $scope.TaskTemplatemodal = function () {
            $('#TaskTemplatemodal').modal('show');
            $scope.TaskTemplateName = "";
            $scope.TaskTemplateDescription = "";
            $scope.templateData.PhaseData.PhaseList[0].Templateid = 0;
        };
        $scope.AddNewStep = function (phaseid) {
            $scope.IsUpdate = false;
            $scope.ClearPhaseSteps();
            $scope.stagecreateandedit = "Add stage";
            $('#NewTaskTemplateStep').modal('show');
            $scope.templateData.PhaseData.StepsData.PhaseId = phaseid;
        };
        $scope.OpenCreateTaskTemplatePhasePopup = function () {
            $scope.ClearPhaseSteps();
            $('#NewTaskTemplatePhase').modal('show');
        }
        $scope.dragoverCallback = function (event, index, external, type) {
            $scope.logListEvent('dragged over', event, index, external, type);
            return index >= 0;
        };
        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            $scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }
            var Dest_Phase_Details = this.phase;
            if (allowedType == "itemType") {
                if (Dest_Phase_Details.ID == item.PhaseId) {
                    UpdateTemplateSortOrder(item.PhaseId, index, item.ID, true);
                } else {
                    UpdateTemplateSortOrder(Dest_Phase_Details.ID, index, item.ID, false);
                }
            } else {
                UpdateTemplatePhaseSortOrder(index, item.ID);
            }
            return item;
        };
        $scope.logEvent = function (message, event) { };
        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
            $scope.logEvent(message, event);
        };

        function GetAllUserRoles() {
            AdminService.GetApprovalRoles().then(function (Result) {
                $scope.templateData.UserRoles = [];
                if (Result.Response.length != null) {
                    for (var i = 0, role; role = Result.Response[i++];) {
                        $scope.templateData.UserRoles.push({
                            "Id": role.ID,
                            "Caption": role.Caption,
                            "Description": role.Description
                        });
                    }
                }
            });
        }
        $scope.InsertUpdateTaskTemplate = function () {
            if ($scope.TaskTemplateName != "" && $scope.TaskTemplateName != undefined) {
                $scope.templateData.PhaseData.StepsData.IsMandatory = ($scope.templateData.PhaseData.StepsData.IsMandatory) == undefined ? false : ($scope.templateData.PhaseData.StepsData.IsMandatory);
                $scope.templateData.templateSrcData.ID = ($scope.templateData.PhaseData.PhaseList[0].Templateid) == undefined ? 0 : ($scope.templateData.PhaseData.PhaseList[0].Templateid);
                $scope.templateData.templateSrcData.Name = $scope.TaskTemplateName;
                $scope.templateData.templateSrcData.Description = $scope.TaskTemplateDescription;
                AdminService.InsertUpdateTaskTemplate($scope.templateData.templateSrcData).then(function (res) {
                    if (res.Response != null) {
                        $scope.templateData.templateSrcData.Templateid = res.Response;
                        var allPhasedata = $scope.templateData.PhaseData.AllPhaseList[0];
                        if (allPhasedata != undefined) {
                            for (var i = 0; i < allPhasedata.length; i++) {
                                var stepsofcurrentphase = allPhasedata[i].StepsData;
                                for (var j = 0; j < stepsofcurrentphase.length; j++) {
                                    $scope.UpdateStep(stepsofcurrentphase[j]);
                                }
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        GetAllTemplates();
                        $("#approvalflowTempPopup").modal("hide");
                        $("#TaskTemplatemodal").modal("hide");
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    }
                });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4581.Caption'));
        };
        $scope.DeleteApprovalTemplate = function (tempid) {
            bootbox.confirm($translate.instant('LanguageContents.Res_5580.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteApprovalTemplate(tempid).then(function (isdeleted) {
                            if (isdeleted.Response != null && isdeleted.Response == true) {
                                NotifySuccess($translate.instant('LanguageContents.Res_5579.Caption'));
                                GetAllTemplates();
                            }
                        });
                    }, 50);
                }
            });
        }
        $scope.AddStep = function (buttontype) {
            if ($scope.TaskTemplateStepName != null && $scope.TaskTemplateStepName != "" && $scope.TaskTemplateStepduration != null && $scope.TaskTemplateStepduration != "" && $scope.TaskTemplateStepMinApproval != null && $scope.TaskTemplateStepMinApproval != "") {
                $scope.templateData.PhaseData.StepsData.IsUpdate = false;
                if (buttontype == 1) {
                    $scope.templateData.PhaseData.StepsData.IsUpdate = $scope.IsUpdate;
                } else {
                    $scope.templateData.PhaseData.StepsData.IsUpdate = $scope.IsUpdate;
                }
                $scope.templateData.PhaseData.StepsData.Name = $scope.TaskTemplateStepName;
                $scope.templateData.PhaseData.StepsData.Description = $scope.TaskTemplateStepDescription;
                $scope.templateData.PhaseData.StepsData.Stepduration = $scope.TaskTemplateStepduration;
                $scope.templateData.PhaseData.StepsData.StepMinApproval = $scope.TaskTemplateStepMinApproval;
                if (Object.prototype.toString.call($scope.TaskTemplateSteproles) === '[object Array]') {
                    $scope.templateData.PhaseData.StepsData.Steproles = $scope.TaskTemplateSteproles.join(",");
                }
                if (typeof $scope.TaskTemplateSteproles === 'string') {
                    $scope.templateData.PhaseData.StepsData.Steproles = $scope.TaskTemplateSteproles;
                }
                $scope.templateData.PhaseData.StepsData.IsMandatory = ($scope.TaskTemplateIsMandatory == undefined) ? false : $scope.TaskTemplateIsMandatory;
                AdminService.InsertUpdateTaskTemplateStep($scope.templateData.PhaseData.StepsData).then(function (stepid) {
                    if (stepid.Response != null && stepid.Response != 0) {
                        if ($scope.templateData.PhaseData.StepsData.StepID == stepid.Response) getphasewithsteps($scope.templateData.PhaseData.StepsData.TemplateID);
                        $('#NewTaskTemplateStep').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        var record = {
                            "PhaseId": $scope.templateData.PhaseData.StepsData.PhaseId,
                            "Name": $scope.TaskTemplateStepName,
                            "Description": $scope.TaskTemplateStepDescription,
                            "Duration": $scope.TaskTemplateStepduration,
                            "MinApproval": $scope.TaskTemplateStepMinApproval,
                            "Roles": $scope.TaskTemplateSteproles,
                            IsMandatory: $scope.TaskTemplateIsMandatory,
                            "ID": stepid.Response,
                            isopened: true
                        };
                        var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (rel) {
                            return rel.ID == $scope.templateData.PhaseData.StepsData.PhaseId;
                        })[0];
                        if (srcphase != null) srcphase.StepsData.push(record);
                        var phaseMaxduration = 0;
                        for (var i = 0; i < srcphase.StepsData.length; i++) {
                            phaseMaxduration = parseInt(srcphase.StepsData[i].Duration) > phaseMaxduration ? parseInt(srcphase.StepsData[i].Duration) : phaseMaxduration;
                        }
                        srcphase.maxduration = phaseMaxduration;
                        $scope.templateData.PhaseData.PhaseList.StepsData = {
                            StepList: [],
                            "StepID": 0,
                            "Name": "",
                            "Description": "",
                            "Stepduration": "",
                            "StepMinApproval": "",
                            "Steproles": "",
                            "IsMandatory": 0,
                            "PhaseId": 0
                        };
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5739.Caption'));
                return false;
            }
        }
        $scope.AddNewPhase = function () {
            if ($scope.Phasename != null && $scope.Phasename != "" && $scope.Phasename != undefined) {
                $scope.templateData.PhaseData.PhaseList[0].ID = 0;
                $scope.templateData.PhaseData.PhaseList[0].Name = $scope.Phasename;
                $scope.templateData.PhaseData.PhaseList[0].Description = $scope.Phasedescription;
                $scope.templateData.PhaseData.PhaseList[0].Colorcode = $scope.Phasecolorcode;
                AdminService.InsertUpdateTaskTemplatePhase($scope.templateData.PhaseData.PhaseList).then(function (phaseid) {
                    if (phaseid.Response != null && phaseid.Response != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        $('#NewTaskTemplatePhase').modal('hide');
                        if ($scope.templateData.PhaseData.AllPhaseList.length > 0) {
                            var record = {
                                "ColorCode": $scope.Phasecolorcode,
                                Description: $scope.Phasedescription,
                                ID: phaseid.Response[0],
                                Name: $scope.Phasename,
                                TemplateID: $scope.templateData.PhaseData.PhaseList[0].Templateid,
                                StepsData: [],
                                maxduration: 1
                            };
                            $scope.templateData.PhaseData.AllPhaseList[0].push(record);
                        } else {
                            var record = [{
                                "ColorCode": $scope.Phasecolorcode,
                                Description: $scope.Phasedescription,
                                ID: phaseid.Response[0],
                                Name: $scope.Phasename,
                                TemplateID: $scope.templateData.PhaseData.PhaseList[0].Templateid,
                                StepsData: [],
                                maxduration: 1
                            }];
                            $scope.templateData.PhaseData.AllPhaseList.push(record);
                        }
                        $scope.Phasename = "";
                        $scope.Phasedescription = "";
                        $scope.Phasecolorcode = "";
                        var record = {
                            "PhaseId": phaseid.Response[0],
                            "Name": "Stage 1",
                            "Description": "Stage 1",
                            "Duration": 1,
                            "MinApproval": 1,
                            "Roles": "",
                            IsMandatory: false,
                            "ID": phaseid.Response[1],
                            isopened: true
                        };
                        var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (rel) {
                            return rel.ID == phaseid.Response[0];
                        })[0];
                        if (srcphase != null) srcphase.StepsData.push(record);
                    }
                });
            } else {
                bootbox.alert(translate.instant('LanguageContents.Res_5740.Caption'));
                return false;
            }
        }

        function UpdateTemplateSortOrder(phaseid, sortorder, stepid, isphase) {
            var Sortorderdetails = {};
            Sortorderdetails.PhaseId = phaseid;
            Sortorderdetails.SortOrder = sortorder;
            Sortorderdetails.StepId = stepid;
            Sortorderdetails.IsPhase = isphase;
            AdminService.UpdatetaskTemplateSortOrder(Sortorderdetails).then(function (res) {
                if (res.Response) NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
            });
        }

        function UpdateTemplatePhaseSortOrder(sortorder, phaseid) {
            var Phasedetails = {};
            Phasedetails.SortOrder = sortorder;
            Phasedetails.PhaseId = phaseid;
            AdminService.UpdateTemplatePhaseSortOrder(Phasedetails).then(function (res) {
                if (res.Response) NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
            });
        }
        $scope.UpdateStep = function (stepsdata) {
            var stepsInfo = {};
            stepsInfo.IsUpdate = true;
            stepsInfo.StepID = stepsdata.ID;
            stepsInfo.PhaseId = stepsdata.PhaseId;
            stepsInfo.Name = stepsdata.Name;
            stepsInfo.Description = stepsdata.Description;
            stepsInfo.Stepduration = stepsdata.Duration;
            stepsInfo.StepMinApproval = stepsdata.MinApproval;
            stepsInfo.Steproles = stepsdata.Roles;
            stepsInfo.IsMandatory = true;
            AdminService.InsertUpdateTaskTemplateStep(stepsInfo).then(function (stepid) {
                if (stepid.Response != null && stepid.Response != 0) {
                    var maxDuraion = 0;
                    var currentPhase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (e) {
                        return e.ID == stepsdata.PhaseId
                    })[0];
                    var phaseIndex = $scope.templateData.PhaseData.AllPhaseList[0].indexOf(currentPhase);
                    for (var i = 0; i < $scope.templateData.PhaseData.AllPhaseList[0][phaseIndex].StepsData.length; i++) {
                        if (maxDuraion < parseInt($scope.templateData.PhaseData.AllPhaseList[0][phaseIndex].StepsData[i].Duration)) {
                            maxDuraion = parseInt($scope.templateData.PhaseData.AllPhaseList[0][phaseIndex].StepsData[i].Duration);
                        }
                    }
                    $scope.templateData.PhaseData.AllPhaseList[0][phaseIndex].maxduration = maxDuraion;
                    return true;
                } else {
                    return false;
                }
            });
        }
        $scope.DeleteStep = function (data) {
            $scope.templateData.StepTempData = data;
            var steplength = 0;
            var steps = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (e) {
                return e.ID == $scope.templateData.StepTempData.PhaseId
            });
            if (steps.length > 0) {
                steplength = steps[0].StepsData.length;
            }
            if (steplength > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_5736.Caption'+'?'), function (result) {
                    if (result) {
                        $scope.templateData.PhaseData.StepsData.TemplateID = $scope.templateData.PhaseData.PhaseList[0]["Templateid"];
                        AdminService.DeleteApprovalTaskStep($scope.templateData.StepTempData.ID).then(function (res) {
                            if (res.Response == true) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                getphasewithsteps($scope.templateData.PhaseData.StepsData.TemplateID);
                                $('#thumbnailActionMenu').modal('hide');
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_4316.Caption'));
                                $('#thumbnailActionMenu').modal('hide');
                            }
                        });
                    }
                })
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5737.Caption'));
            }
        };
        $scope.menuclick = function (data) {
            $scope.templateData.StepTempData = data;
        }
        $scope.Phasename = "";
        $scope.Phasedescription = "";
        $scope.Phasecolorcode = "";
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.taskPredefinedFlowCtrl']"));
            $scope.templateData = {};
        });
    }
    app.controller("mui.admin.taskPredefinedFlowCtrl", ['$scope', '$compile', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', '$sce', muiadmintaskPredefinedflowCtrl])
})(angular, app);