﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class PurchaseOrderDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected DateTime _createddate;
        protected string _ponumber;
        protected int _supplierid;
        protected string _description;
        protected DateTime? _expectespenddate;
        protected DateTime? _approvedate;
        protected int? _approverid;
        protected DateTime? _senddate;
        protected int _status;
        protected int _userid;
        protected bool _IsExternal;
      
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public PurchaseOrderDao() { }

        public PurchaseOrderDao(int pEntityid, DateTime pCreateddate, string pPonumber, int pSupplierid, string pDescription, DateTime? pExpectespenddate, DateTime? pApprovedate, int? pApproverId, int pStatus, int pUserID, DateTime? pSenddate, bool pIsExternal)
        {
            this._entityid = pEntityid;
            this._createddate = pCreateddate;
            this._ponumber = pPonumber;
            this._supplierid = pSupplierid;
            this._description = pDescription;
            this._expectespenddate = pExpectespenddate;
            this._approvedate = pApprovedate;
            this._senddate = pSenddate;
            this._approverid =pApproverId;
            this._status = pStatus;
            this._userid = pUserID;
            this._IsExternal = pIsExternal;

        }

        public PurchaseOrderDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual DateTime CreatedDate
        {
            get { return _createddate; }
            set { _bIsChanged |= (_createddate != value); _createddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string PONumber
        {
            get { return _ponumber; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("PONumber", "PONumber value, cannot contain more than 50 characters");
                _bIsChanged |= (_ponumber != value);
                _ponumber = value;
            }

        }


        public virtual int SupplierID
        {
            get { return _supplierid; }
            set { _bIsChanged |= (_supplierid != value); _supplierid = value; }

        }

        public virtual DateTime? ExpectedSpentDate
        {
            get { return _expectespenddate; }
            set { _bIsChanged |= (_expectespenddate != value); _expectespenddate = value; }

        }

        public virtual DateTime? ApprovedDate
        {
            get { return _approvedate; }
            set { _bIsChanged |= (_approvedate != value); _approvedate = value; }

        }

        public virtual int? ApproverID
        {
            get { return _approverid; }
            set { _bIsChanged |= (_approverid != value); _approverid = value; }

        }

        public virtual DateTime? SentDate
        {
            get { return _senddate; }
            set { _bIsChanged |= (_senddate != value); _senddate = value; }

        }


        public virtual int Status
        {
            get { return _status; }
            set { _bIsChanged |= (_status != value); _status = value; }

        }

        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }

        public virtual bool IsExternal
        {
            get { return _IsExternal; }
            set { _IsExternal = value; }
        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string PurchaseOrderDao = "PurchaseOrderDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string PONumber = "PoNumber";
            public const string SupplierID = "SupplierID";
            public const string Description = "Description";
            public const string ApproverID = "ApproverID";
            public const string Status = "Status";
            public const string UserID = "UserID";
            public const string CreatedDate = "CreatedDate";
            public const string ExpectedSpentDate = "ExpectedSpentDate";
            public const string ApprovedDate = "ApprovedDate";
            public const string SentDate = "SentDate";
            public const string IsExternal = "IsExternal";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PurchaseOrderDao = "PurchaseOrderDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string PONumber = "PoNumber";
            public const string SupplierID = "SupplierID";
            public const string Description = "Description";
            public const string ApproverID = "ApproverID";
            public const string Status = "Status";
            public const string UserID = "UserID";
            public const string CreatedDate = "CreatedDate";
            public const string ExpectedSpentDate = "ExpectedSpentDate";
            public const string ApprovedDate = "ApprovedDate";
            public const string SentDate = "SentDate";
            public const string IsExternal = "IsExternal";
        }

        #endregion


    }

}
