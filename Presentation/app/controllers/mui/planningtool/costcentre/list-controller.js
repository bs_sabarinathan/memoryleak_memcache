﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentrelistCtrl($scope, $timeout, $http, $compile, $resource, $location, $window, $cookies, $cookieStore, $translate, CclistService, $modal) {
        $scope.TreeClassList = [];
        $scope.CostCentreEntityID = 0;
        $scope.CostCentreEntityType = "";
        $scope.PageSize = 1;
        $scope.IsSelectAllChecked = false;
        $scope.createCostCentre = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/costcentre/costcentrecreation.html',
                controller: "mui.planningtool.costcentre.costcentrecreationCtrl",
                resolve: {
                    params: function () {
                        return 0;
                    }
                },
                scope: $scope,
                windowClass: 'entityMetadataModel popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            $timeout(function () {
                $("#Div_listCC").trigger('loadAttributeMetadata', [0, 0]);
            }, 100)
        }
        $scope.appliedfilter = "No filter applied";
        $scope.SelectedFilterID = 0;
        $scope.AddEditFilter = "Add new filter";
        CclistService.GetFilterSettings(5).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () {
            angular.element(document).ready(function () {
                $scope.$broadcast('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
            });
        }, 1000);

        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $scope.deletefiltershow = false;
                $("#rootlevelfilter").trigger('ClearScope', $scope.EntityTypeID);
            } else {
                $timeout(function () {
                    $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 5]);
                }, 20);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow")
            }
        };
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $scope.ApplyFilter = function (filterid, filtername) {
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
            } else {
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            $("#rootlevelfilter").trigger('ApplyRootLevelFilter', [filterid, filtername]);
        }

        function GetCostCenterEntityType() {
            CclistService.GetEntityTypeByID(5).then(function (res) {
                if (res.Response != null) {
                    $scope.CostCentreEntityType = res.Response;
                }
            });
        }
        GetCostCenterEntityType();
        $("#Div_listCC").on('ReloadFilterSettings', function (event, TypeID, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $scope.appliedfilter = filtername;
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [filterid, TypeID]);
                $scope.AddEditFilter = 'Edit filter';
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            CclistService.GetFilterSettings(TypeID).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.appliedfilter = filtername;
            });
        });
        $("#Div_listCC").on('ClearAndReApply', function () {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        $scope.Level = "0";
        $scope.FilterID = {
            selectedFilterID: 0,
            selectedfilterattribtues: []
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };
        $scope.EntityTypeID = 5;
        var TypeID = 5;
        var CurrentuserId = $scope.UserId;
        $scope.onTreeDataMouseEnter = function () {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-entityID="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeDataMouseLeave = function () {
            var id = this.item.Id;
            setTimeout('$(\'#treeHolder > li > a[data-entityID="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.onTreeMouseEnter = function () {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-entityID="' + id + '"]\').addClass(\'hover\');', 50);
        };
        $scope.onTreeMouseLeave = function () {
            var id = this.item.Id;
            setTimeout('$(\'#listdataHolder > tr[data-entityID="' + id + '"]\').removeClass(\'hover\');', 50);
        };
        $scope.listColumnDefsdata = {};
        $scope.NameExcludeFilter = function (columndefs) {
            if (columndefs.Field != "Name") {
                return true;
            }
        };
        $scope.load = function (parameters) { };
        if (ShowAllRemember.CostCentre == true) {
            $scope.DisablePart = true;
            CurrentuserId = $scope.UserId;
            $("#btnParticipated").removeClass("active").addClass("active");
            $("#btnViewAll").removeClass("active");
        } else {
            $scope.DisablePart = false;
            CurrentuserId = 0;
            $("#btnViewAll").removeClass("active").addClass("active");
            $("#btnParticipated").removeClass("active");
        }
        $scope.tglHide = true;
        $scope.ShowAll = function (IsActive) {
            $scope.DisablePart = !$scope.DisablePart;
            $("#ListContainer table").html('');
            $("#treeHolder ul").html('');
            $('#RootLevelSelectAll').next('i').removeClass('checked');
            ClearCheckBoxes();
            if (ShowAllRemember.CostCentre == true) {
                CurrentuserId = $scope.UserId
            }
            if (IsActive) {
                $("#btnViewAll").removeClass("active").addClass("active");
                $("#btnParticipated").removeClass("active");
                CurrentuserId = 0;
                ShowAllRemember.CostCentre = false;
            } else {
                $("#btnParticipated").removeClass("active").addClass("active");
                $("#btnViewAll").removeClass("active");
                CurrentuserId = $scope.UserId;
                ShowAllRemember.CostCentre = true;
            }
            GetRootLevelActivityListCount();
            if ($("#ListColumn > thead tr th").length == 1)
                $('#ListColumn > thead').html("");
        };

        function ClearCheckBoxes() {
            $scope.IsSelectAllChecked = false;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = false;
                $(this).next('i').removeClass('checked');
            });
        }
        $scope.DeleteEntity = function () {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length != 0) {
                var object = {};
                var deletesearch = {};
                deletesearch.ID = IDList;
                object.ID = IDList;
                CclistService.MemberAvailable(object).then(function (memberavailability) {
                    if (memberavailability.Response == false) {
                        bootbox.alert($translate.instant('LanguageContents.Res_2100.Caption'));
                        return true;
                    }
                    bootbox.confirm($translate.instant('LanguageContents.Res_16.Caption'), function (result) {
                        if (result) {
                            $timeout(function () {
                                CclistService.DeleteCostcentre(object).then(function (data) {
                                    if (data.Response == 1) {
                                        for (var i = 0; i < IDList.length; i++) {
                                            var entityId = IDList[i];
                                            $('#treeHolder a[data-entityID=' + entityId + ']').remove();
                                            $('#ListContainer tr[data-entityID=' + entityId + ']').remove();
                                        }
                                        $('#RootLevelSelectAll').next('i').removeClass('checked');
                                        ClearCheckBoxes();
                                    }
                                    if (data.Response == 2) {
                                        bootbox.alert($translate.instant('LanguageContents.Res_1912.Caption'));
                                    }
                                    if (data.Response == 3) {
                                        bootbox.alert($translate.instant('LanguageContents.Res_4850.Caption'));
                                    }
                                });
                            }, 100);
                        }
                    });
                });
            }
        };

        function SelectEntityIDs() {
            var IDList = new Array();
            $('#listdataHolder input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-entityID'));
            });
            return IDList
        }
        var IsDesc = false;
        var sortOrder = "";
        $scope.FilterID.selectedFilterID = 0;
        $scope.FilterID.selectedfilterattribtues = [];
        $scope.LoadRootLevelCostCenterActivity = function (PageIndex, StartRowNo, MaxNoofRow, filterid, filterattributes) {
            $(window).AdjustHeightWidth();
            var FilterID = filterid;
            var CCAttrs = $cookieStore.get('CCAttrs' + parseInt($cookies['UserId'], 10));
            var CCFilterID = $cookieStore.get('CCFilterID' + parseInt($cookies['UserId'], 10));
            var CCFilterName = $cookieStore.get('CCFilterName' + parseInt($cookies['UserId'], 10));
            RemeberCCFilterName = CCFilterName;
            if (CCAttrs != undefined) {
                RememberCCFilterAttributes = CCAttrs;
            }
            if (CCFilterID != undefined) {
                RememberCCFilterID = CCFilterID;
            }
            if (RememberCCFilterID != 0 || RememberCCFilterAttributes.length != 0) {
                FilterID = RememberCCFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberCCFilterAttributes;
                if (RememberCCFilterAttributes.length > 0) { }
                if (RememberCCFilterID != 0) {
                    $scope.appliedfilter = RemeberCCFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = FilterID;
                }
            }
            if (StartRowNo == 0)
                $timeout(function () {
                    //$("#rootlevelfilter").trigger('LoadRootLevelFilter', [5, RememberCCFilterAttributes]);
                    $scope.$broadcast('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
                }, 10);
            $scope.FilterID.selectedFilterID = filterid;
            $scope.FilterID.selectedfilterattribtues = filterattributes;
            $window.CostCentreFilterName = '';
            var getactivities = {};
            getactivities.StartRowNo = StartRowNo;
            getactivities.MaxNoofRow = MaxNoofRow;
            getactivities.FilterID = FilterID;
            getactivities.SortOrderColumn = $scope.sortOrder;
            getactivities.IsDesc = $scope.IsDesc;
            getactivities.IncludeChildren = false;
            getactivities.EntityID = '0';
            getactivities.UserID = CurrentuserId;
            getactivities.Level = $scope.Level;
            getactivities.IDArr = [];
            getactivities.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            CclistService.CostCentreRootLevel(getactivities).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var colStatus = false;
                        var TreeItem = "";
                        for (var i = 0; i < listContent.length; i++) {
                            if (i != undefined) {
                                var UniqueId = UniqueKEY(listContent[i]["UniqueKey"]);
                                var ClassName = GenateClass(UniqueId);
                                ClassName += " mo" + UniqueId;
                                contentnHtml += "<tr data-over='true' class='ng-scope" + ClassName + "' data-EntityLevel='" + listContent[i]["Level"] + "' data-entityID=" + listContent[i]["Id"] + " data-uniqueKey=" + UniqueId + ">"
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                        if (colStatus == false) {
                                            if (j != undefined && listColumnDefsdata[j].Field == SystemDefiendAttributes.AssignedAmount) {
                                                columnHtml += "<th>";
                                            } else {
                                                columnHtml += "<th>";
                                            }
                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (j != undefined && listColumnDefsdata[j].Field == SystemDefiendAttributes.AssignedAmount)
                                            contentnHtml += "<td><span class='currColumn ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field].formatMoney(0, ' ', ' ') : "-") + " " + listContent[i].CurrencyType.toUpperCase() + "</span></td>";
                                        else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }
                                TreeItem += "<li class='ng-scope" + ClassName + "' data-over='true' data-uniqueKey=" + UniqueId + "><a data-role='Activity' data-EntityLevel='" + listContent[i]["Level"] + "' data-EntityTypeID='" + listContent[i]["TypeID"] + "'  data-entityID=" + listContent[i]["Id"] + "  data-colorcode=" + listContent[i]["ColorCode"] + " data-shorttext=" + listContent[i]["ShortDescription"] + " data-Permission=" + listContent[i].Permission + " data-entityname=\"" + listContent[i]["Name"] + "\"> ";
                                TreeItem += Space(listContent[i]["UniqueKey"]);
                                if (listContent[i]["TotalChildrenCount"] <= 0 || $scope.IsSingleID == true) {
                                    TreeItem += "<i class='icon-'></i>";
                                    $scope.IsSingleID = false;
                                } else {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + listContent[i]["Id"] + " data-TotalChildrenCount=" + listContent[i]["TotalChildrenCount"] + " data-Permission=" + listContent[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                }
                                TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + listContent[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + listContent[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + listContent[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span data-name='text' data-EntityLevel='" + listContent[i]["Level"] + "' data-EntityTypeID='" + listContent[i]["TypeID"] + "'  data-entityID=" + listContent[i]["Id"] + "  data-colorcode=" + listContent[i]["ColorCode"] + " data-shorttext=" + listContent[i]["ShortDescription"] + " data-Permission=" + listContent[i].Permission + " data-entityname=\"" + listContent[i]["Name"] + "\">" + listContent[i]["Name"] + "</span>";
                                if (listContent[i].Permission == 1 || listContent[i].Permission == 2) {
                                    TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + listContent[i].Permission + " data-typeID=" + listContent[i]["TypeID"] + " data-entityID=" + listContent[i]["Id"] + "></i>";
                                } else if (listContent[i].Permission == 3) {
                                    var EntityTypesWithPermission = "";
                                    if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                                        EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                            return e.ParentTypeId == parseInt(listContent[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                        })
                                    }
                                    if (EntityTypesWithPermission.length > 0)
                                        TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + listContent[i].Permission + " data-typeID=" + listContent[i]["TypeID"] + " data-entityID=" + listContent[i]["Id"] + "></i>";
                                } else if (listContent[i].Permission == 0) { }
                                TreeItem += "</a> </li>";
                                var status = $scope.IsSelectAllChecked;
                                if (listContent[i]["Id"] != null) {
                                    contentnHtml += "<td id='mos" + listContent[i]["Id"] + "'><span class='pull-right'><label class='checkbox checkbox-custom pull-right' id=" + listContent[i]["Id"] + "><input type='checkbox' ><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>"
                                }
                                colStatus = true;
                                contentnHtml += "</tr>";
                                colStatus = true;
                            }
                        }
                        GetApprovedBugetIcon(listContent, $scope.IsSelectAllChecked);
                        if (PageIndex == 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                            $('#ListContainer').scrollTop(0);
                        }
                        if ($("#ListColumn > thead tr").length < 1) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(TreeItem);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));
                }
                $(window).AdjustHeightWidth();
            });
        }
        $scope.sortOrder = "null";
        $scope.IsDesc = false;
        GetRootLevelActivityListCount();

        function GetRootLevelActivityListCount() {
            var CCAttrs = $cookieStore.get('CCAttrs' + parseInt($cookies['UserId'], 10));
            var CCFilterID = $cookieStore.get('CCFilterID' + parseInt($cookies['UserId'], 10));
            var CCFilterName = $cookieStore.get('CCFilterName' + parseInt($cookies['UserId'], 10));
            RemeberCCFilterName = CCFilterName;
            if (CCAttrs != undefined) {
                RememberCCFilterAttributes = CCAttrs;
            }
            if (CCFilterID != undefined) {
                RememberCCFilterID = CCFilterID;
            }
            if (RememberCCFilterID != 0 || RememberCCFilterAttributes.length != 0) {
                $scope.FilterID.selectedFilterID = RememberCCFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberCCFilterAttributes;
                if (RememberCCFilterAttributes.length > 0) { }
                if (RememberCCFilterID != 0) {
                    $scope.appliedfilter = RemeberCCFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = $scope.FilterID.selectedFilterID;
                }
            }
            $scope.noData = false;
            $window.CostCentreFilterName = '';
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = $scope.FilterID.selectedFilterID;
            Node.SortOrderColumn = $scope.sortOrder;
            Node.IsDesc = $scope.IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = CurrentuserId;
            Node.Level = $scope.Level;
            Node.IDArr = [];
            Node.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            CclistService.CostCentreRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                    var PageIndex = 0;
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var TreeItem = "";
                        var colStatus = false;
                        for (var i = 0; i < listContent.length; i++) {
                            if (i != undefined) {
                                var UniqueId = UniqueKEY(listContent[i]["UniqueKey"]);
                                var ClassName = GenateClass(UniqueId);
                                ClassName += " mo" + UniqueId;
                                contentnHtml += "<tr data-over='true' class='ng-scope" + ClassName + "' data-EntityLevel='" + listContent[i]["Level"] + "' data-entityID=" + listContent[i]["Id"] + " data-uniqueKey=" + UniqueId + ">"
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != SystemDefiendAttributes.Name) {
                                        if (colStatus == false) {
                                            if (j != undefined && listColumnDefsdata[j].Field == SystemDefiendAttributes.AssignedAmount) {
                                                columnHtml += "<th>";
                                            } else {
                                                columnHtml += "<th>";
                                            }
                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (j != undefined && listColumnDefsdata[j].Field == SystemDefiendAttributes.AssignedAmount)
                                            contentnHtml += "<td><span class='currColumn ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field].formatMoney(0, ' ', ' ') : "-") + " " + listContent[i].CurrencyType.toUpperCase() + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }
                                TreeItem += "<li class='ng-scope" + ClassName + "' data-over='true' data-uniqueKey=" + UniqueId + "><a  data-role='Activity' data-EntityLevel='" + listContent[i]["Level"] + "' data-EntityTypeID='" + listContent[i]["TypeID"] + "'  data-entityID=" + listContent[i]["Id"] + "  data-colorcode=" + listContent[i]["ColorCode"] + " data-shorttext=" + listContent[i]["ShortDescription"] + " data-Permission=" + listContent[i].Permission + " data-entityname=\"" + listContent[i]["Name"] + "\"> ";
                                TreeItem += Space(listContent[i]["UniqueKey"]);
                                if (listContent[i]["TotalChildrenCount"] <= 0 || $scope.IsSingleID == true) {
                                    TreeItem += "<i class='icon-'></i>";
                                    $scope.IsSingleID = false;
                                } else {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + listContent[i]["Id"] + " data-TotalChildrenCount=" + listContent[i]["TotalChildrenCount"] + " data-Permission=" + listContent[i].Permission + "  data-UniqueId=\"" + UniqueId + "\"></i>";
                                }
                                TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + listContent[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + listContent[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + listContent[i]["ShortDescription"] + "</span>";
                                TreeItem += "<span data-name='text' data-EntityLevel='" + listContent[i]["Level"] + "' data-EntityTypeID='" + listContent[i]["TypeID"] + "'  data-entityID=" + listContent[i]["Id"] + "  data-colorcode=" + listContent[i]["ColorCode"] + " data-shorttext=" + listContent[i]["ShortDescription"] + " data-Permission=" + listContent[i].Permission + " data-entityname=\"" + listContent[i]["Name"] + "\">" + listContent[i]["Name"] + "</span>";
                                if (listContent[i].Permission == 1 || listContent[i].Permission == 2) {
                                    TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + listContent[i].Permission + " data-typeID=" + listContent[i]["TypeID"] + " data-entityID=" + listContent[i]["Id"] + "></i>";
                                } else if (listContent[i].Permission == 3) {
                                    var EntityTypesWithPermission = "";
                                    if ($scope.ContextMenuRecords.ChildContextData != null && $scope.ContextMenuRecords.ChildContextData != "") {
                                        EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                            return e.ParentTypeId == parseInt(listContent[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                        })
                                    }
                                    if (EntityTypesWithPermission.length > 0)
                                        TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + listContent[i].Permission + " data-typeID=" + listContent[i]["TypeID"] + " data-entityID=" + listContent[i]["Id"] + "></i>";
                                }
                                treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x'>" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName'>" + listContent[i]["Name"] + "</span></a></li>";
                                var status = "";
                                if (listContent[i]["Id"] != null) {
                                    contentnHtml += "<td id='mos" + listContent[i]["Id"] + "'><span class='pull-right'><label class='checkbox checkbox-custom pull-right' id=" + listContent[i]["Id"] + "><input type='checkbox' ><i class='checkbox " + status + "'></i></label></span></td>"
                                }
                                colStatus = true;
                            }
                        }
                        GetApprovedBugetIcon(listContent);
                        if ($("#ListColumn > thead tr").length < 1) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            $('#ListContainer').scrollTop(0);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(TreeItem);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                        if ($scope.PageSize > 1) {
                            $scope.LoadRootLevelCostCenterActivity(1, 20, 20, $scope.FilterID.selectedFilterID, $scope.AppllyFilterObj.selectedattributes);
                        }
                    }
                } else {
                    $scope.noData = true;
                }
                $(window).AdjustHeightWidth();
            });
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function Space(UniKey) {
            var substr = UniKey.split('.');
            var itag = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 1; i < row; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            return itag;
        }
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            var PageSize = 1;
            var itemsPerPage = 20;
            var height = 34 * ItemCnt;
            if (ItemCnt > itemsPerPage) {
                height = 34 * itemsPerPage;
                PageSize = Math.ceil(ItemCnt / itemsPerPage);
            }
            $scope.PageSize = PageSize;
            var TreeTemplate = '';
            var ListTemplate = '';
            for (var i = 0; i <= PageSize; i++) {
                if (i != undefined) {
                    TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list CCnav-list nav-list-menu'></ul></li>"
                    ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"
                }
            }
            if (TreeTemplate.length > 0) {
                $("#ListContainer > table").html(ListTemplate);
                $("#treeHolder").html(TreeTemplate);
            }
        }
        $('#ListContainer').scroll(function () {
            $("#treeHolder").scrollTop($(this).scrollTop());
            var areaHeight = $('#ListContainer').height();
            var areaTop = $('#ListContainer').position().top;
            var top = $(this).position().top - areaTop;
            var height = $('#ListContainer table').height();
            $('tbody.pending', '#ListContainer').each(function () {
                var datapage = parseInt($(this).attr('data-page'));
                if ((($(this).height() * datapage) / 3) < $("#treeHolder").scrollTop()) {
                    $('#ListContainer > table tbody[data-page="' + $(this).attr('data-page') + '"]').removeClass('pending');
                    var StartRowNo = datapage * 20;
                    var MaxNoofRow = 20;
                    $(this).removeClass('pending');
                    $(this).removeAttr('style');
                    var filterattributes = [];
                    $('#treeHolder li[data-page="' + datapage + '"]').removeClass('pending');
                    $('#treeHolder li[data-page="' + datapage + '"]').removeAttr('style');
                    $scope.LoadRootLevelCostCenterActivity(datapage, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.FilterID.selectedfilterattribtues);
                }
            });
        });
        $("#ListColumn").click(function (e) {
            var filterattributes = [];
            var TargetControl = $(e.target);
            var col = "";
            if (TargetControl[0].tagName == "SPAN") {
                $("#ListContainer table").html('');
                $("#treeHolder ul").html('');
                col = TargetControl.parent().attr("data-Column");
            } else if (TargetControl[0].tagName == "A") {
                $("#ListContainer table").html('');
                $("#treeHolder ul").html('');
                col = TargetControl.attr("data-Column");
            }
            if (col != "") {
                $scope.sortOrder = col;
                $('#RootLevelSelectAll').next('i').removeClass('checked');
                ClearCheckBoxes();
                if ($scope.sortOrder === col) {
                    $scope.IsDesc = !$scope.IsDesc;
                } else {
                    $scope.sortOrder = col;
                    $scope.IsDesc = false;
                }
                $scope.LoadRootLevelCostCenterActivity(0, 0, 20, 0, filterattributes)
            }
        });
        $("#rootLevelEntity").click(function (event) {
            $("#rootLevelEntity").trigger("onRootCostCentreCreation", [$scope.EntityTypeID])
        });
        $window.ListofEntityID = []
        $("#treeHolder").click(function (e) {
            $(window).AdjustHeightWidth();
            var TargetControl = $(e.target);
            $scope.EntityTypeID = parseInt(TargetControl.find("I[data-role='Overview']").attr('data-typeID'), 10);
            if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') {
                LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'))
            } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    $scope.CostCentreEntityID = TargetControl.attr('data-entityID');
                    $('#moduleContextCCRootmenu').css('top', $(TargetControl[0]).offset().top + 22 + 'px').show();
                    e.preventDefault();
                    return false;
                } else
                    e.preventDefault();
            } else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    e.preventDefault();
                    ViewRootLevelEntity(TargetControl.children().attr('data-TotalChildrenCount'), TargetControl.attr('data-entityID'), TargetControl.attr('data-entitylevel'), e);
                } else
                    e.preventDefault();
            } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') {
                if (TargetControl.attr('data-entityID') != undefined && TargetControl.attr('data-entityID') != "0") {
                    e.preventDefault();
                    ViewRootLevelEntity(TargetControl.children().attr('data-TotalChildrenCount'), TargetControl.attr('data-entityID'), TargetControl.attr('data-entitylevel'), e);
                } else
                    e.preventDefault();
            }
            $(window).AdjustHeightWidth();
        });
        $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function (e) {
            var status = this.checked;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $scope.IsSelectAllChecked = true;
                    $(this).next('i').addClass('checked');
                } else {
                    $scope.IsSelectAllChecked = false;
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $("#ViewSelect").click(function (event) {
            ViewRootLevelEntity(1, null, 0, event);
        });

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-entityID'));
            });
            return IDList
        }
        $("#ListContainer").click(function (e) {
            $timeout(function () {
                var IDList = new Array();
                IDList = GetRootLevelSelectedAll();
                if (IDList.length == $("#treeHolder").find(".nav-list").find("li").length) {
                    $('#RootLevelSelectAll').next('i').addClass('checked');
                }
                else
                    $('#RootLevelSelectAll').next('i').removeClass('checked');
            }, 50);
        });
        function ViewRootLevelEntity(TotalChildren, ID, Level, event) {
            var IDList = new Array();
            if ($scope.IsSelectAllChecked == false) {
                if (ID != null) {
                    IDList.push(ID);
                    LoadDetailPart(event, IDList, Level, TotalChildren);
                } else {
                    IDList = GetRootLevelSelectedAll();
                    if (IDList.length == 1) {
                        $('#ListContainer > table > tbody input:checked').each(function () {
                            var selectedUniqkey = ($(this).parents('tr').attr('data-uniquekey'));
                            TotalChildren = $('#treeHolder li[data-uniquekey="' + selectedUniqkey + '"] a i').attr('data-TotalChildrenCount');
                        });
                    }
                    LoadDetailPart(event, IDList, Level, TotalChildren);
                }
            } else {
                $scope.IsSelectAllChecked = false;
                CclistService.GetAllEntityIds().then(function (Entityresult) {
                    if (Entityresult.Response != null) {
                        IDList = Entityresult.Response;
                        $window.TreeLevel = "100";
                        LoadSelectAllDetailPart(event, IDList);
                    }
                });
            }
        }

        function LoadDetailPart(event, IDList, Level, TotalChildren) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.TreeLevel = "";
                $window.ListofEntityID = IDList;
                $window.TotalChildrenForSelecedCC = 0;
                if (IDList.length > 1)
                    $window.TreeLevel = "100";
                else {
                    $window.TreeLevel = Level;
                    $window.TotalChildrenForSelecedCC = TotalChildren == undefined ? 0 : TotalChildren;
                }
                var TrackID = CreateHisory(IDList);
                event.preventDefault();
                var localScope = $(event.target).scope();
                localScope.$apply(function () {
                    if ($("#ViewSelect").attr('data-viewtype') == "ganttview") {
                        $location.path("mui/planningtool/costcentre/detail/ganttview/" + TrackID);
                    } else {
                        $location.path("mui/planningtool/costcentre/detail/listview/" + TrackID);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function ListCheckBoxitemClick() {
            $('#ListContainer > table > tbody input:checkbox').click(function () {
                $("#RootLevelSelectAll").next().removeClass('checked')
                $scope.IsSelectAllChecked = false;
            });
        }

        function LoadSelectAllDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                if ($("#ViewSelect").attr('data-viewtype') == "ganttview") {
                    $location.path("mui/planningtool/costcentre/detail/ganttview/" + TrackID);
                } else {
                    $location.path("mui/planningtool/costcentre/detail/listview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }
        $scope.UpdateCostCentreApprovedBudget = function () {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length > 0) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2028.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if (IDList.length > 0) {
                                var CostCentreApprovedBudget = {};
                                CostCentreApprovedBudget.CCIDs = IDList;
                                CclistService.UpdateCostCentreApprovedBudget(CostCentreApprovedBudget).then(function (UpdateCostCentreApprovedBudget) {
                                    if (UpdateCostCentreApprovedBudget.StatusCode == 200) {
                                        GetApprovedBugetIconDtls(IDList);
                                        NotifySuccess($translate.instant('LanguageContents.Res_4086.Caption'));
                                        $('#RootLevelSelectAll').next('i').removeClass('checked');
                                    } else {
                                        NotifyError($translate.instant('LanguageContents.Res_4378.Caption'));
                                    }
                                });
                            } else {
                                bootbox.alert($translate.instant('LanguageContents.Res_2133.Caption'))
                            }
                        }, 100);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_2133.Caption'))
            }
        };

        function GetApprovedBugetIconDtls(IDList, IsChecked) {
            CclistService.GetApprovedBudgetDate(IDList.join(",")).then(function (ApprovedDates) {
                if (ApprovedDates.Response != null) {
                    for (var i = 0; i < ApprovedDates.Response.length; i++) {
                        $scope.ApprovedDateTime = [];
                        for (var i = 0; i < ApprovedDates.Response.length; i++) {
                            var BugetDts = ApprovedDates.Response[i].ApproveTime;
                            if (BugetDts != null) {
                                var status = IsChecked;
                                $('#mos' + ApprovedDates.Response[i].CostCentreID).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox"><i class="checkbox ' + (status ? "checked" : "") + '"></i></label></span><img id="ApprovedBudge' + ApprovedDates.Response[i].CostCentreID + '" src="assets/img/AppBudget.png"></td></tr>');
                                var colStatus = true;
                            }
                            var datStartUTCval = "";
                            var datstartval = "";
                            datStartUTCval = BugetDts.substr(6, (BugetDts.indexOf('+') - 6));
                            datstartval = new Date(parseInt(datStartUTCval));
                            $scope.fields = BugetDts;
                            var id = ApprovedDates.Response[i].CostCentreID;
                            $("#ApprovedBudge" + id).show();
                            $("#ApprovedBudge" + ApprovedDates.Response[i].CostCentreID).attr("title", "ApprovedBudgetDate: " + $scope.fields).qtip({
                                content: {
                                    text: $scope.fields,
                                },
                                style: {
                                    classes: "qtip-dark qtip-shadow qtip-rounded",
                                    title: {
                                        'display': 'none'
                                    }
                                },
                                show: {
                                    event: 'mouseenter click unfocus',
                                    solo: true
                                },
                                events: {
                                    hide: function (event, api) {
                                        event: 'unfocus click mouseleave mouseup mousedown'
                                    }
                                },
                                hide: {
                                    delay: 0,
                                    fixed: false,
                                    effect: function () {
                                        $(this).fadeOut(100);
                                    },
                                    event: 'unfocus click mouseleave mouseup mousedown'
                                },
                                position: {
                                    my: 'bottom left',
                                    at: 'top left',
                                    viewport: $(window),
                                    adjust: {
                                        method: 'shift',
                                        mouse: true
                                    },
                                    corner: {
                                        target: 'bottom left',
                                        tooltip: 'bottom left',
                                        mimic: 'top'
                                    },
                                    target: 'mouse'
                                }
                            });
                        }
                    }
                }
            });
        }

        function GetApprovedBugetIcon(IDList, IsChecked) {
            var ids = $.map(IDList, function (e) {
                return e.Id
            });
            CclistService.GetApprovedBudgetDate(ids.join(",")).then(function (ApprovedDates) {
                if (ApprovedDates.Response != null) {
                    for (var i = 0; i < ApprovedDates.Response.length; i++) {
                        $scope.ApprovedDateTime = [];
                        for (var i = 0; i < ApprovedDates.Response.length; i++) {
                            var BugetDts = ApprovedDates.Response[i].ApproveTime;
                            if (BugetDts != null) {
                                var status = IsChecked;
                                $('#mos' + ApprovedDates.Response[i].CostCentreID).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox"><i class="checkbox ' + (status ? "checked" : "") + '"></i></label></span><img id="ApprovedBudge' + IDList + '" src="assets/img/AppBudget.png"></td></tr>');
                                var colStatus = true;
                            }
                            var datStartUTCval = "";
                            var datstartval = "";
                            datStartUTCval = BugetDts.substr(6, (BugetDts.indexOf('+') - 6));
                            datstartval = new Date(parseInt(datStartUTCval));
                            $scope.fields = BugetDts;
                            $("#ApprovedBudge" + ApprovedDates.Response[i].CostCentreID).attr("title", "ApprovedBudgetDate: " + $scope.fields).qtip({
                                content: {
                                    text: $scope.fields,
                                },
                                style: {
                                    classes: "qtip-dark qtip-shadow qtip-rounded",
                                    title: {
                                        'display': 'none'
                                    }
                                },
                                show: {
                                    event: 'mouseenter click unfocus',
                                    solo: true
                                },
                                events: {
                                    hide: function (event, api) {
                                        event: 'unfocus click mouseleave mouseup mousedown'
                                    }
                                },
                                hide: {
                                    delay: 0,
                                    fixed: false,
                                    effect: function () {
                                        $(this).fadeOut(100);
                                    },
                                    event: 'unfocus click mouseleave mouseup mousedown'
                                },
                                position: {
                                    my: 'bottom left',
                                    at: 'top left',
                                    viewport: $(window),
                                    adjust: {
                                        method: 'shift',
                                        mouse: true
                                    },
                                    corner: {
                                        target: 'bottom left',
                                        tooltip: 'bottom left',
                                        mimic: 'top'
                                    },
                                    target: 'mouse'
                                }
                            });
                        }
                    }
                } else {
                    var status = "";
                    $('#mos' + IDList).html('<span class="pull-right"><label class="checkbox checkbox-custom pull-right"><input type="checkbox" ><i class="checkbox ' + status + '"></i></label></span></td></tr>');
                }
            });
        }
        $(document).click(function () {
            $('#moduleContextCCRootmenu').hide();
        })
        $("#moduleContextCCRootmenu").click(function () {
            CclistService.GetCostCentreAssignedAmount($scope.CostCentreEntityID).then(function (ResAssgnAmnt) {
                $timeout(function () {
                    $("#Div_listCC").trigger("loadAttributeMetadata", [$scope.CostCentreEntityID, ResAssgnAmnt.Response]);
                }, 100);
            });
        });

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="TreeCtrlCss">';
            $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }

        function LoadChildTreeNodes(data, ChildCount, UniqueId) {
            if (ChildCount > 0) {
                if ($('#treeHolder li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") {
                    $('#treeHolder li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right");
                    Collapse(UniqueId);
                    $('#treeHolder li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false");
                    $('#EntitiesTree').scroll();
                } else {
                    $('#treeHolder li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down");
                    Expand(UniqueId);
                    $('#treeHolder li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true");
                }
                if ($('#treeHolder li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") {
                    $scope.ExpandingEntityID = data;
                    TreeLoad(UniqueId, false);
                    $('#treeHolder li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false");
                }
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.listCtrl']"));
        });
        $timeout(function () {
            $scope.load();
        }, 0);
        $scope.visible = false;
        $scope.fields = {};
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.createcostcentre = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/costcentre/costcentrecreation.html',
                controller: "mui.planningtool.costcentre.costcentrecreationCtrl",
                resolve: {
                    params: function () {
                        return 0;
                    }
                },
                scope: $scope,
                windowClass: 'entityMetadataModel popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            $timeout(function () {
                $("#Div_listCC").trigger('loadAttributeMetadata', [0, 0]);
            }, 100)
        }
    }
    app.controller("mui.planningtool.costcentre.listCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$cookies', '$cookieStore', '$translate', 'CclistService', '$modal', muiplanningtoolcostcentrelistCtrl]);
})(angular, app);