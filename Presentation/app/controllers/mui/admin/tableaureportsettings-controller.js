﻿(function (ng, app) {
    "use strict";

    function muiadmintableaureportsettings($scope, $resource, $location, $timeout, $http, $translate, AdminService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        $scope.tableauReportSettings = [];
        $scope.FinancialFotrecastObj = {};
        $scope.reportname = "";
        $scope.tableauReportID = 0;
        $scope.ShowCreateReport = false;
        $scope.tableauFotrecastObj = {
            ID: 0,
            ReportAction: $translate.instant('LanguageContents.Res_4056.Caption'),
            description: "",
            imageurl: "NoPreview.jpg"
        };
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            $scope.reportPreviewImg = cloudsetup.Uploaderurl + '/' + cloudsetup.BucketName + "/Files/ReportFiles/Images/Preview/Temp/";
        }
        else {
            $scope.reportPreviewImg = "/Files/ReportFiles/Images/Preview/Temp/";
        }
        $timeout(function () {
            LoadtableauReportSettings();
            StrartUpload();
        }, 100);
        function LoadtableauReportSettings() {
            //0 - get all list of reports
            AdminService.GettableauReportSettings(0).then(function (res) {
                $scope.tableauReportSettings = [];
                $scope.tableauReportSettings = res.Response;
            });
        }
        $scope.DeleteReportById = function (id) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2005.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeletetableaureportByID(id).then(function (res) {
                            if (res.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4305.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.reportname = "";
                                LoadtableauReportSettings();
                            }
                        });
                    }, 100);
                }
            });
        }

        $scope.AddNewReport = function () {
            $scope.ClearScopevariables();
            $scope.ShowCreateReport = true;
            $scope.tableauFotrecastObj.ReportAction = $translate.instant('LanguageContents.Res_4056.Caption');
            $scope.tableauReportUploadImage = "NoPreview.jpg";
            $scope.tableauReportUrl = "";
            $timeout(function () {
                $('#reportCaption').focus().select();
            }, 100);
        }
        $scope.GettableauReportBlock = function (report) {
            $scope.ShowCreateReport = true;
            $scope.tableauFotrecastObj.ReportAction = $translate.instant('LanguageContents.Res_5023.Caption');
            $scope.tableauReportID = report.ID;
            $scope.tableauReportCaption = $scope.DecodedTextName(report.ReportSettingsName);
            $scope.tableauReportDescription = $scope.DecodedTextName(report.Description);
            $scope.tableauReportUploadImage = report.ReportImage;
            $scope.tableauReportUrl = report.Url;
        }
        $scope.updateReportBlock = function () {
            if ($scope.tableauReportCaption != "" && $scope.tableauReportCaption != undefined) {
                var inserttasktempparams = {};
                inserttasktempparams.ReportID = $scope.tableauReportID;
                inserttasktempparams.Name = $scope.tableauReportCaption;
                inserttasktempparams.ReportImage = $scope.tableauReportUploadImage;
                inserttasktempparams.ReportDescription = $scope.tableauReportDescription;
                inserttasktempparams.ReportUrl = $scope.tableauReportUrl;
                AdminService.insertupdatetableaureportsettings(inserttasktempparams).then(function (result) {
                    if (result.Response != null && result.Response != 0) {
                        if ($scope.tableauFotrecastObj.ReportAction == $translate.instant('LanguageContents.Res_4056.Caption')) { NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption')); }
                        else { NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption')); }
                        LoadtableauReportSettings();
                        $scope.ClearScopevariables();
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4689.Caption'));
            }
        }
        $scope.idFilter = function (location) {
            return (location.id > 1 || location.id < -1);
        };

        $scope.UploadReportImageID = 0;
        $scope.UploadImagefile = function () {
            $scope.UploadReportImageID = 1;
            $("#tableausettingspickfiles").click();
        };

        function StrartUpload() {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'tableausettingspickfiles',
                container: 'tableausettingsfilescontainer',
                max_file_size: '10mb',
                flash_swf_url: 'js/plupload.flash.swf',
                silverlight_xap_url: 'js/plupload.silverlight.xap',
                url: 'Handlers/ReportImagePicHandler.ashx?Type=Attachment',
                multi_selection: false,
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png"
                }]
            });
            uploader.bind('Init', function (up, params) { });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) { });
                up.refresh();
                uploader.start();
                $('#tableausettingspickfiles').hide();
                $('#tableausettingsuploadprogress').show();
                $('#tableausettingsuploadprogress .bar').css('width', '0');
                $('#tableausettingsuploadprogress #tableausettingsbarText').html('0 %');
            });
            uploader.bind('tableausettingsuploadprogress', function (up, file) {
                $('#tableausettingsuploadprogress .bar').css("width", file.percent + "%");
                $('#tableausettingsuploadprogress #tableausettingsbarText').html(file.percent + "%");
            });
            uploader.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.tempFile = file;
                $scope.tempResponse = response;
                var img = new Image();
                img.src = 'Files/ReportFiles/Images/Temp/' + response.response;
                $('#tableausettingspickfiles').show();
                $('#tableausettingsuploadprogress').hide();
                $('#tableausettingsReportImage').modal('show');
                $('#tableausettingsReportImage #tableausettingsCropReportImg').removeAttr('src').css('visibility', 'hidden');
                $('#tableausettingsReportImage #tableausettingsCropReportImg').attr('style', '');
                var w = parseInt(response.response.split(',')[1]);
                var h = parseInt(response.response.split(',')[2]);
                var path = response.response.split(',')[0];
                $timeout(function () {
                    AssigneNewImage(w, h, path);
                }, 200);
            });
        }
        var jcrop_api;

        function AssigneNewImage(w, h, path) {
            $('#tableausettingsReportImage #tableausettingsCropReportImg').attr('src', 'Files/ReportFiles/Images/Temp/' + path);
            $('#tableausettingsReportImage #tableausettingsCropReportImg').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#tableausettingsReportImage #tableausettingsCropReportImg').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#tableausettingsReportImage #tableausettingsCropReportImg').parent().css('margin-top', margine + 'px');
            } else {
                $('#tableausettingsReportImage #tableausettingsCropReportImg').css('height', '528px');
                $('#tableausettingsReportImage #tableausettingsCropReportImg').parent().css('margin-top', '0px');
            }
            $('#tableausettingsReportImage #tableausettingsCropReportImg').css('visibility', 'visible');
            initJcrop();
        }

        function initJcrop() {
            if ($('#tableausettingsReportImage .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#tableausettingsCropReportImg').Jcrop({
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: 4 / 5
                });
                jcrop_api.focus();
                var w = $('#tableausettingsReportImage #tableausettingsCropReportImg').width();
                var h = $('#tableausettingsReportImage #tableausettingsCropReportImg').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                var nw = 0;
                var nh = 0;
                if (w <= h) {
                    nw = w;
                    nh = Math.round((w / 4) * 5);
                } else {
                    nh = h;
                    nw = Math.round((h / 4) * 5);
                }
                jcrop_api.animateTo([0, 0, nw, nh]);
            });

            function showPreview(c) {
                var orgw = $('#tableausettingsReportImage #tableausettingsCropReportImg').attr('data-width');
                var orgh = $('#tableausettingsReportImage #tableausettingsCropReportImg').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $scope.UploadReportImage = function () {
            var res = {};
            res.imgsourcepath = $("#tableausettingsCropReportImg").attr('src');
            res.imgwidth = $scope.userimgWidth;
            res.imgheight = $scope.userimgHeight;
            res.imgX = $scope.userimgX;
            res.imgY = $scope.userimgY;
            res.Preview = $scope.tempResponse.response.split(',')[0];
            AdminService.UpdatetableauSettingsReportImage(res).then(function (result) {
                $scope.tableauReportUploadImage = $scope.tempResponse.response.split(',')[0];
                $scope.OrgimgWidth = 0;
                $scope.OrgimgHeight = 0;
            });
        }

        $scope.ClearScopevariables = function(){
            $scope.ShowCreateReport = false;
            $scope.tableauReportCaption = "";
            $scope.tableauReportUploadImage = "";
            $scope.tableauReportDescription = "";
            $scope.tableauReportUrl = "";
            $scope.tableauReportID = 0;
        }
    }
    app.controller("mui.admin.tableaureportsettings", ['$scope', '$resource', '$location', '$timeout', '$http', '$translate', 'AdminService', muiadmintableaureportsettings]);
})(angular, app);