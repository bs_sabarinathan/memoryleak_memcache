﻿(function (ng, app) {
    "use strict";

    function muiadmintasktemplateCtrl($scope, $location, $resource, $timeout, $compile, $sce, $translate, AdminTaskService) {
        $scope.TaskTemplatePopUpText = $translate.instant('LanguageContents.Res_5031.Caption');
        var html = $('#container0').html();
        $scope.FulfillmentEntityTypes = [];
        $scope.TaskConditontext = true;
        $scope.TaskConditionDataArr = [];
        $scope.TaskTempConditionResult = [];
        FilltFulfillmentEntityTypes();
        $scope.UnavilableTemplate = false;
        AdminTaskService.GetTaskTemplateDetails().then(function (res) {
            if (res.Response != null) {
                $scope.TaskTempConditionResult = res.Response;
                loadTasklistLibrarydata();
                if ($scope.TaskTempConditionResult.length == 0) {
                    $scope.UnavilableTemplate = true;
                }
            }
        });

        function collectionObjectiveCondition(templateId) {
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            $scope.TaskConditionDataArr = [];
            var optionArr = [];
            $scope.Criteriatext = '';
            $('#TaskFulfillmentMetadatatbody' + templateId.toString() + ' div[data-Holder="holder"]').each(function (index) {
                var UniqueId = $(this).attr('data-tableId') + '_' + $(this).attr('data-Id');
                if (UniqueId > $(this).attr('data-tableId') + '_' + '0') {
                    if ($('#ObjConditionType' + UniqueId).val() == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        $scope.TaskConditionDataArr = [];
                        return false;
                    }
                }
                if ($('#ObjEntityType' + UniqueId).val() == null || $('#ObjEntityType' + UniqueId).val() == "0") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1845.Caption'));
                    $scope.TaskConditionDataArr = [];
                    return false;
                }
                if ($('#ObjEntityAttributes' + UniqueId).val() != "0" && $('#ObjEntityAttributes' + UniqueId).val() != null) {
                    if ($('#ObjAttributeOptions' + UniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        $scope.TaskConditionDataArr = [];
                        return false;
                    }
                }
                var collectionarr = {
                    'ID': parseInt($(this).attr('data-Existingid')),
                    'ObjEntityType': $('#ObjEntityType' + UniqueId).val(),
                    'ObjEntityAttributes': ($('#ObjEntityAttributes' + UniqueId).val() != 0 ? $('#ObjEntityAttributes' + UniqueId).val() : '0_0'),
                    'ObjAttributeOptionns': $('#ObjAttributeOptions' + UniqueId).val(),
                    'ConditionType': $('#ObjConditionType' + UniqueId).val(),
                    "SortOrder": $(this).attr('data-Id')
                };
                if (collectionarr != null) {
                    $scope.TaskConditionDataArr.push(collectionarr);
                    var selectedvalues = '';
                    var Maxindex = $('#ObjAttributeOptions' + UniqueId + " option:selected").length - 1;
                    $('#ObjAttributeOptions' + UniqueId + " option:selected").each(function (index) {
                        selectedvalues += '"' + $(this).text() + '"';
                        if (Maxindex > index) selectedvalues += $translate.instant('LanguageContents.Res_5027.Caption')
                    });
                    if (index == 0) {
                        $scope.Criteriatext += $translate.instant('LanguageContents.Res_807.Caption') + ' "' + $('#ObjEntityType' + UniqueId + ' option:selected').text() + '" ' + ($('#ObjEntityAttributes' + UniqueId + ' option:selected').val() != '0' && $('#ObjEntityAttributes' + UniqueId + ' option:selected').val() != null ? '' + $translate.instant('LanguageContents.Res_808.Caption') + ' "' + $('#ObjEntityAttributes' + UniqueId + ' option:selected').text() + '" ' : '') + (selectedvalues.length > 0 ? ' ' + $translate.instant('LanguageContents.Res_809.Caption') + ' ' + selectedvalues : '') + '';
                    } else {
                        $scope.Criteriatext += ' <br/>' + $('#ObjConditionType' + UniqueId + ' option:selected').text() + '<br/> ' + $translate.instant('LanguageContents.Res_807.Caption') + ' "' + $('#ObjEntityType' + UniqueId + ' option:selected').text() + '" ' + ($('#ObjEntityAttributes' + UniqueId + ' option:selected').val() != '0' && $('#ObjEntityAttributes' + UniqueId + ' option:selected').val() != null ? ' ' + $translate.instant('LanguageContents.Res_808.Caption') + ' "' + $('#ObjEntityAttributes' + UniqueId + ' option:selected').text() + '" ' : '') + (selectedvalues.length > 0 ? ' ' + $translate.instant('LanguageContents.Res_809.Caption') + ' ' + selectedvalues : '') + '';
                    }
                }
            });
        }

        function AddTaskFilamentTypes(Result, Id) {
            var html = '';
            $.each(Result, function (val, item) {
                var UniqueId = Id + '_' + val;
                html += "<div data-control='main' data-Holder='holder' data-Existingid='" + item.Id + "' data-tableId='" + Id + "' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + val + "'>";
                html += "<ul class='repeter'>";
                html += "<li class='form-inline'>";
                if (val == 0) {
                    html += "<div class='coverFill'></div>";
                } else {
                    html += "<div class='cover'></div>";
                }
                html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>Condition " + (parseInt(val) + 1) + " :</label>";
                html += "<select ng-model='ObjConditionType" + UniqueId + "' ui-select2 id='ObjConditionType" + UniqueId + "'>";
                html += "<option value=''>-- " + $translate.instant('LanguageContents.Res_811.Caption') + "-- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + "</option>";
                html += "<option value='2'>" + $translate.instant('LanguageContents.Res_812.Caption') + " </option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                html += "<select ng-model='ObjEntityType" + UniqueId + "' ui-select2 id='ObjEntityType" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ng-model='ObjEntityAttributes" + UniqueId + "' ui-select2 id='ObjEntityAttributes" + UniqueId + "' data-role='Attributes'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ng-model='ObjAttributeOptions" + UniqueId + "' ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' data-role='Equals'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add'><i class='icon-plus'></i></button>&nbsp;<button class='btn' data-role='Remove' data-RemovalID='" + item.Id + "'><i class='icon-remove'></i></button>";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
            });
            $('#TaskFulfillmentMetadatatbody' + Id.toString()).html($compile(html)($scope));
            $.each(Result, function (val, item) {
                var UniqueId = Id + '_' + val;
                $timeout(function () {
                    $("#ObjConditionType" + UniqueId).select2('val', item.ConditionType);
                }, 100);
                FillEntityTypes("ObjEntityType" + UniqueId);
                $timeout(function () {
                    $("#ObjEntityType" + UniqueId).select2('val', item.TypeID);
                }, 100);
                $timeout(function () {
                    LoadFillEntityTypeAttributes("ObjEntityAttributes" + UniqueId, item.TypeID, item.AttributeID, item.AttributeLevel);
                }, 100);
                $timeout(function () {
                    LoadFillEntityTypeOptions("ObjAttributeOptions" + UniqueId, item.AttributeID, item.AttributeLevel, (item.Value.length > 0 ? item.Value : 0));
                }, 100);
            });
        }
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        function LoadFillEntityTypeAttributes(ControllerID, EntityTypeID, Attributeid, AttributeLevel) {
            if (ControllerID != undefined) {
                AdminTaskService.GetFulfillmentAttribute(EntityTypeID).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillEntityTypeAttributes(ControllerID, fulfillmentAttributeOptionsObj.Response)
                        if (ControllerID != undefined) {
                            $('#' + ControllerID).html("");
                            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                            if (fulfillmentAttributeOptionsObj.Response != null) {
                                $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                    var currentId = item.Id.toString() + "_" + item.Level;
                                    $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                                });
                                $('#' + ControllerID).select2("val", Attributeid + "_" + AttributeLevel);
                            }
                        }
                    }
                });
            }
        }

        function LoadFillEntityTypeOptions(ControllerID, entityAttribtueId, entityAttributeLevel, selectedAttributeOption) {
            AdminTaskService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (res) {
                if (res.Response.length > 0) {
                    if (ControllerID != undefined) {
                        $('#' + ControllerID).html("");
                        $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                        if (res.Response != null) {
                            $.each(res.Response, function (val, item) {
                                $('#' + ControllerID).append($("<option></option>").val(item.Id).html(item.Caption));
                            });
                            var Values = selectedAttributeOption.toString().split(",");
                            $('#' + ControllerID).select2("val", Values);
                        }
                    }
                }
            });
        }
        $scope.Backward = function (TemplateId) {
            $('#TaskConditontext' + TemplateId).show();
            $('#TaskConditonFulFilment' + TemplateId).hide();
        }
        $scope.SaveTaskFulfilment = function (templateId) {
            $("#moduleContextTaskCond").modal('show');
            collectionObjectiveCondition(templateId);
            if ($scope.TaskConditionDataArr.length > 0) {
                var inserttasktempCondparams = {};
                inserttasktempCondparams.TasktemplateID = templateId;
                inserttasktempCondparams.TaskTempCondition = $scope.TaskConditionDataArr;
                inserttasktempCondparams.TaskTempCriteria = $scope.Criteriatext;
                AdminTaskService.InsertUpdateTaskTemplateCondition(inserttasktempCondparams).then(function (tempCondResult) {
                    if (tempCondResult != null && tempCondResult != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        RefreshTemplateCondnDetail(templateId)
                        $('#TaskConditontext' + templateId).show();
                        $('#TaskConditonFulFilment' + templateId).hide();
                    }
                });
            }
        }

        function RefreshTemplateCondnDetail(templateId) {
            var newObject = $.grep($scope.TaskTempConditionResult, function (n, i) {
                return ($scope.TaskTempConditionResult[i].Id == templateId);
            });
            newObject[0].TemplateCriteria = $scope.Criteriatext;
            newObject[0].TempCondDetails = [];
            for (var i = 0; i < $scope.TaskConditionDataArr.length; i++) {
                var AttributeIdLevel = [];
                var attributeValue = 0;
                if ($scope.TaskConditionDataArr[i].ObjEntityAttributes == null) {
                    AttributeIdLevel[0] = 0;
                    AttributeIdLevel[0] = 0;
                    attributeValue = 0;
                } else {
                    AttributeIdLevel = $scope.TaskConditionDataArr[i].ObjEntityAttributes.split('_');
                    attributeValue = $scope.TaskConditionDataArr[i].ObjAttributeOptionns;
                }
                newObject[0].TempCondDetails.push({
                    AttributeID: AttributeIdLevel[0],
                    AttributeLevel: AttributeIdLevel[1],
                    AttributeTypeID: 0,
                    ConditionType: $scope.TaskConditionDataArr[i].ConditionType,
                    Id: $scope.TaskConditionDataArr[i].ID,
                    SortOder: $scope.TaskConditionDataArr[i].SortOrder,
                    TaskTempID: templateId,
                    TypeID: $scope.TaskConditionDataArr[i].ObjEntityType,
                    Value: attributeValue
                });
            }
        }
        $scope.DefaultCriteria = function (CriteriaText) {
            if (CriteriaText.length == 0) {
                return '"No criteria present. Click edit criteria for this template"';
            }
            return CriteriaText;
        }
        $scope.AddTaskCondition = function (TemplateId) {
            $scope.TaskConditionDataArr = [];
            $('#TaskConditontext' + TemplateId).hide();
            $('#TaskConditonFulFilment' + TemplateId).show();
            if ($scope.TaskTempConditionResult != null) {
                AdminTaskService.GetTaskTemplateConditionByTaskTempId(TemplateId).then(function (res) {
                    if (res.Response != null && res.Response.length > 0) {
                        AddTaskFilamentTypes(res.Response, TemplateId);
                    } else {
                        AddDefulttasdkFulFilment(TemplateId);
                    }
                });
            } else {
                AddDefulttasdkFulFilment(TemplateId);
            }
        }

        function AddDefulttasdkFulFilment(TemplateId) {
            var html = '';
            var UniqueId = TemplateId + '_0';
            html += "<div  data-control='main' data-Holder='holder' data-Existingid='0' data-tableId='" + TemplateId + "' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='0'>";
            html += "<ul class='repeter'>";
            html += "<li class='form-inline'>";
            html += "<div class='coverFill'></div>";
            html += " <label class='control-label' id='lblCondition" + UniqueId + "' for='ObjectiveCondition'>Condition 1 :</label>";
            html += "<select ng-model='ObjConditionType" + UniqueId + "' ui-select2 id='ObjConditionType" + UniqueId + "'>";
            html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + " -- </option>";
            html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + " </option>";
            html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + " </option>";
            html += "";
            html += "</select>";
            html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
            html += "<select ng-model='ObjEntityType" + UniqueId + "' ui-select2 id='ObjEntityType" + UniqueId + "' data-role='EntityType'>";
            html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
            html += "";
            html += "</select>";
            html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
            html += "<select ng-model='ObjEntityAttributes" + UniqueId + "' ui-select2 id='ObjEntityAttributes" + UniqueId + "' data-role='Attributes'>";
            html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
            html += "";
            html += "</select>";
            html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
            html += "<select ng-model='ObjAttributeOptions" + UniqueId + "' ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' data-role='Equals'>";
            html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
            html += "";
            html += "</select>";
            html += "<button class='btn' data-role='Add' ><i class='icon-plus'></i></button>&nbsp;<button class='btn' data-role='Remove' data-RemovalID='0' ><i class='icon-remove'></i></button>";
            html += "";
            html += "</li>";
            html += "</ul>";
            html += "</div>";
            $('#TaskFulfillmentMetadatatbody' + TemplateId.toString()).html($compile(html)($scope));
            FillEntityTypes("ObjEntityType" + UniqueId);
        }

        function FilltFulfillmentEntityTypes() {
            AdminTaskService.GetTaskFulfillmentEntityTypes().then(function (res) {
                $scope.FulfillmentEntityTypes = res.Response;
            });
        }
        $("#tasltempListholder").click(function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div[data-holder="holder"]').attr('data-tableId') + '_' + TargetControl.parents('div[data-holder="holder"]').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') {
                ResetDropDown("ObjEntityAttributes" + currentUniqueId, currentUniqueId);
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                AdminTaskService.GetFulfillmentAttribute(TargetControl.val()).then(function (res) {
                    if (res.Response.length > 0) {
                        FillEntityTypeAttributes("ObjEntityAttributes" + currentUniqueId, res.Response);
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Attributes') {
                ResetDropDown("ObjAttributeOptions" + currentUniqueId, currentUniqueId);
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                if (entityAttribtueId != 0) {
                    AdminTaskService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (res) {
                        if (res.Response.length > 0) {
                            FillEntityTypeAttributes("ObjAttributeOptions" + currentUniqueId, res.Response)
                        }
                    });
                }
            } else if (TargetControl.attr('data-role') == 'Equals') {
                if ($('#ObjAttributeOptions' + currentUniqueId).val() == '0') {
                    $('#ObjAttributeOptions' + currentUniqueId).val("");
                    $('#ObjAttributeOptions' + currentUniqueId).select2();
                }
            } else if (TargetControl.attr('data-role') == 'Add') {
                if (TargetControl.parent().find('#ObjEntityType' + currentUniqueId).val() == "0") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1845.Caption'));
                    return false;
                }
                if (TargetControl.parent().find('#ObjEntityAttributes' + currentUniqueId).val() != "0" && TargetControl.parent().find('#ObjEntityAttributes' + currentUniqueId).val() != null) {
                    if (TargetControl.parent().find('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }
                var idval = parseInt(TargetControl.parents('div[data-holder="holder"]').attr('data-id')) + 1;
                var UniqueId = TargetControl.parents('div[data-holder="holder"]').attr('data-tableId') + '_' + idval.toString();
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' data-Existingid='0' data-tableId='" + TargetControl.parents('div[data-holder="holder"]').attr('data-tableId') + "' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + idval + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='ObjectiveCondition'>Condition " + (idval + 1) + " :</label>";
                html += "<select ng-model='ObjConditionType" + UniqueId + "' ui-select2 id='ObjConditionType" + UniqueId + "'>";
                html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + " -- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + " </option>";
                html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + "</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='ObjEntityType" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='ObjEntityAttributes" + UniqueId + "' data-role='Attributes'>";
                html += "<option value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='ObjAttributeOptions" + UniqueId + "' data-role='Equals'>";
                html += "<option  value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus'></i></button>&nbsp;<button class='btn' data-role='Remove' data-RemovalID='0' ><i class='icon-remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("ObjEntityType" + UniqueId);
            } else if (TargetControl.parent('button').attr('data-role') == 'Add') {
                if (TargetControl.parent('button').parent().find('#ObjEntityType' + currentUniqueId).val() == "0") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1845.Caption'));
                    return false;
                }
                if (TargetControl.parent('button').parent().find('#ObjEntityAttributes' + currentUniqueId).val() != "0" && TargetControl.parent('button').parent().find('#ObjEntityAttributes' + currentUniqueId).val() != null) {
                    if (TargetControl.parent('button').parent().find('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }
                var idval = parseInt(TargetControl.parents('div[data-holder="holder"]').attr('data-id')) + 1;
                var UniqueId = TargetControl.parents('div[data-holder="holder"]').attr('data-tableId') + '_' + idval.toString();
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' data-Existingid='0' data-tableId='" + TargetControl.parents('div[data-holder="holder"]').attr('data-tableId') + "' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + idval + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='ObjectiveCondition'>Condition " + (idval + 1) + " :</label>";
                html += "<select ng-model='ObjConditionType" + UniqueId + "' ui-select2 id='ObjConditionType" + UniqueId + "'>";
                html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + " -- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + "</option>";
                html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + " </option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityType" + UniqueId + "' ng-model='ObjEntityType" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ui-select2 id='ObjEntityAttributes" + UniqueId + "' ng-model='ObjEntityAttributes" + UniqueId + "' data-role='Attributes'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ui-select2 multiple='multiple' id='ObjAttributeOptions" + UniqueId + "' ng-model='ObjAttributeOptions" + UniqueId + "' data-role='Equals'>";
                html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus'></i></button>&nbsp;<button class='btn' data-role='Remove' data-RemovalID='0' ><i class='icon-remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("ObjEntityType" + UniqueId);
            } else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId.contains('_0')) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1847.Caption'));
                    return false;
                }
                var tempCondId = TargetControl.attr('data-RemovalID')
                if (tempCondId == 0) {
                    $("#container" + currentUniqueId).remove();
                } else {
                    bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                        if (result) {
                            $timeout(function () {
                                AdminTaskService.DeleteTemplateConditionById(tempCondId).then(function (res) {
                                    if (res.StatusCode != 200) {
                                        NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                                    } else {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        $("#container" + currentUniqueId).remove();
                                    }
                                });
                            }, 100);
                        }
                    });
                }
            } else if (TargetControl.parent().attr('data-role') == 'Remove') {
                if (currentUniqueId.contains('_0')) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1847.Caption'));
                    return false;
                }
                var tempCondId = TargetControl.parent().attr('data-RemovalID')
                if (tempCondId == 0) {
                    $("#container" + currentUniqueId).remove();
                } else {
                    bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                        if (result) {
                            $timeout(function () {
                                AdminTaskService.DeleteTemplateConditionById(tempCondId).then(function (res) {
                                    if (res.StatusCode != 200) {
                                        NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                                    } else {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        $("#container" + currentUniqueId).remove();
                                    }
                                });
                            }, 100);
                        }
                    });
                }
            }
        });

        function FillEntityTypes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentEntityTypes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                });
            }
        }

        function FillEntityTypeAttributes(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(ControllerID, currentUniqueId) {
            $('#' + ControllerID).html("");
            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#' + ControllerID).select2("val", 0);
            $('#ObjAttributeOptions' + currentUniqueId).html("");
            $('#ObjAttributeOptions' + currentUniqueId).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#ObjAttributeOptions' + currentUniqueId).select2("val", "");
        }

        function loadTasklistLibrarydata() {
            AdminTaskService.GetTemplateAdminTaskList().then(function (res) {
                $scope.TaskLibraryList = res.Response;
                $timeout(function () {
                    applySortable();
                }, 10);
            });
        }
        $(document).on('click', '.checkbox-custom > input[id=TaskListSelectAll]', function (e) {
            var status = this.checked;
            $('#taskListtable tr  input:checkbox[data-ChildCheckbox="check"]').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $scope.templateId = 0;
        $scope.ShowTaskListmodal = function (templateId) {
            $("#TaskListShowModal").modal('show');
            $scope.templateId = templateId;
            $('#taskListtable tr  input:checkbox[data-ChildCheckbox="check"]').each(function () {
                $(this).next('i').removeClass('checked');
            });
            $('.checkbox-custom > input[id=TaskListSelectAll]').next().removeClass('checked');
        };
        $scope.SelectTaskLists = function () {
            if ($scope.templateId != 0) {
                $scope.newObjectlist = $.grep($scope.TaskTempConditionResult, function (n, i) {
                    return ($scope.TaskTempConditionResult[i].Id == $scope.templateId);
                });
                $('#taskListtable tr  i.checked').each(function () {
                    if ($(this).prev().attr('data-id') != undefined) {
                        var idval = parseInt($(this).prev().attr('data-id'));
                        var val = $.grep($scope.TaskLibraryList, function (n, m) {
                            return ($scope.TaskLibraryList[m].ID == idval);
                        });
                        if (val.length > 0) $scope.newObjectlist[0].TaskListDetails.push({
                            Caption: val[0].LibraryName,
                            Description: val[0].LibraryDescription,
                            Id: val[0].ID,
                            SortOrder: val[0].SortOrder,
                            TotalTasks: val[0].TaskList
                        });
                        $scope.params = {
                            Caption: val[0].LibraryName,
                            Description: val[0].LibraryDescription,
                            Id: val[0].ID,
                            SortOrder: val[0].SortOrder
                        };
                        var inserttasktempparams = {};
                        inserttasktempparams.Caption = val[0].LibraryName;
                        inserttasktempparams.templateId = $scope.templateId;
                        inserttasktempparams.taskListID = val[0].ID;
                        inserttasktempparams.SortOrder = (val[0].SortOrder.length == 0 ? 0 : val[0].SortOrder);
                        AdminTaskService.InsertTempTaskList(inserttasktempparams).then(function (res) {
                            if (res != null && res != 0) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                            }
                        });
                        $(this).removeClass('checked');
                        $('.checkbox-custom > input[id=TaskListSelectAll]').next().removeClass('checked');
                    }
                });
            }
            $('#TaskListShowModal').modal("hide");
        };
        $scope.ShowTemplatecreationmodel = function () {
            $scope.TaskTemplatePopUpText = $translate.instant('LanguageContents.Res_5031.Caption');
            $scope.TaskTempTabId = 0;
            $scope.TempCaption = "";
            $scope.TempDescription = "";
            $('#TasktemplateCreationModal').modal("show");
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        }
        $scope.SaveTaskTempList = function () {
            if ($scope.TempCaption == undefined || $scope.TempCaption.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1848.Caption'));
                return false;
            }
            var inserttasktempparams = {};
            inserttasktempparams.ID = $scope.TaskTempTabId;
            inserttasktempparams.caption = $scope.TempCaption;
            inserttasktempparams.description = $scope.TempDescription;
            AdminTaskService.InsertUpdateTemplate(inserttasktempparams).then(function (res) {
                if (res != null && res != 0) {
                    if ($scope.TaskTempTabId != 0) {
                        UpdateRefreshTemplteData($scope.TaskTempTabId);
                    } else {
                        $scope.TaskTempConditionResult.push({
                            Id: parseInt(res.Response),
                            Name: $scope.TempCaption,
                            Description: $scope.TempDescription,
                            TaskListDetails: [],
                            TempCondDetails: [],
                            TemplateCriteria: '"No criteria present. Click edit criteria for this template"'
                        });
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                    $scope.TaskTempTabId = 0;
                }
            });
            $('#TasktemplateCreationModal').modal("hide");
        }
        $scope.UpdateTemplate = function (templateId, Caption, Description) {
            $scope.TaskTemplatePopUpText = $translate.instant('LanguageContents.Res_5032.Caption');
            $('#TasktemplateCreationModal').modal("show");
            $scope.TempCaption = Caption;
            $scope.TempDescription = Description;
            $scope.TaskTempTabId = templateId;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        }
        $scope.DeleteTemplate = function (templateId) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2016.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminTaskService.DeleteTaskTemplateListById(templateId).then(function (res) {
                            if (res != null && res.Response != false) {
                                DeleteRefreshTemplteData(templateId);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }

        function DeleteRefreshTemplteData(templateId) {
            for (var i = 0; i < $scope.TaskTempConditionResult.length; i++) {
                if ($scope.TaskTempConditionResult[i].Id == templateId) {
                    $scope.TaskTempConditionResult.splice(i, 1);
                }
            }
        }
        $scope.UpdateTaskList = function () {
            if ($scope.TaskListCaption == undefined || $scope.TaskListCaption.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1848.Caption'));
                return false;
            }
            if ($scope.TaskListTabId != 0) {
                var addtaskObj = {};
                addtaskObj.ID = $scope.TaskListTabId;
                addtaskObj.Caption = $scope.TaskListCaption;
                addtaskObj.Description = $scope.TaskListDescription;
                addtaskObj.sortorder = 1;
                AdminTaskService.InsertUpdateTaskList(addtaskObj).then(function (SaveTask) {
                    if (SaveTask.StatusCode == 200) {
                        UpdateRefreshTaskData($scope.TaskListTabId);
                        NotifySuccess($translate.instant('LanguageContents.Res_4833.Caption'));
                        $scope.TaskListTabId = 0;
                        AdminTaskService.GetTaskTemplateDetails().then(function (Getresult) {
                            if (Getresult.Response != null) {
                                $scope.TaskTempConditionResult = Getresult.Response;
                            }
                        });
                    }
                });
            }
            $('#TaskListUpdationmodal').modal("hide");
        }
        $scope.DeleteTaskList = function (tasklistId, templateId) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2017.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminTaskService.DeleteAdminTemplateTaskRelationById(tasklistId, templateId).then(function (res) {
                            if (res != null && res.Response != false) {
                                DeleteRefreshTaskData(tasklistId, templateId);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }

        function UpdateRefreshTemplteData(templateId) {
            for (var i = 0; i < $scope.TaskTempConditionResult.length; i++) {
                if ($scope.TaskTempConditionResult[i].Id == templateId) {
                    $scope.TaskTempConditionResult[i].Name = $scope.TempCaption;
                    $scope.TaskTempConditionResult[i].Description = $scope.TempDescription;
                }
            }
        }

        function UpdateRefreshTaskData(TaskListId) {
            for (var i = 0; i < $scope.TaskTempConditionResult.length; i++) {
                for (var j = 0; j < $scope.TaskTempConditionResult[i].TaskListDetails.length; j++) {
                    if ($scope.TaskTempConditionResult[i].TaskListDetails[j].Id == TaskListId) {
                        $scope.TaskTempConditionResult[i].TaskListDetails[j].Name = $scope.TaskListCaption;
                        $scope.TaskTempConditionResult[i].TaskListDetails[j].Description = $scope.TaskListDescription;
                    }
                }
            }
        }

        function DeleteRefreshTaskData(TaskListId, templateId) {
            for (var i = 0; i < $scope.TaskTempConditionResult.length; i++) {
                if (templateId == $scope.TaskTempConditionResult[i].Id) {
                    for (var j = 0; j < $scope.TaskTempConditionResult[i].TaskListDetails.length; j++) {
                        if ($scope.TaskTempConditionResult[i].TaskListDetails[j].Id == TaskListId) {
                            $scope.TaskTempConditionResult[i].TaskListDetails.splice(j, 1);
                            return false;
                        }
                    }
                }
            }
        }
        $scope.ShowUpdateDialogTaskList = function (templateId, Caption, Description) {
            $scope.TaskListTabId = templateId;
            $('#TaskListUpdationmodal').modal("show");
            $scope.TaskListCaption = Caption;
            $scope.TaskListDescription = Description;
            $timeout(function () {
                $('#inputcaption').focus().select();
            }, 1000);
        }

        function applySortable() {
            $("#tasltempListholder > li > ul").sortable({
                axis: "y",
                scroll: true,
                cursor: "move",
                update: function (event, ui) {
                    var listItems = $("li", this);
                    var uiArray = new Array();
                    var templateid;
                    listItems.each(function (idx, li) {
                        templateid = $(li).attr("data-templateid");
                        uiArray.push($(li).attr("id"));
                    });
                    var taskListTemplateResObj = [];
                    taskListTemplateResObj = $.grep($scope.TaskTempConditionResult, function (e) {
                        return e.Id == parseInt(templateid);
                    });
                    for (var j = 0; j < taskListTemplateResObj.length; j++) {
                        for (var i = 0; i < uiArray.length; i++) {
                            var taskListResObj = [];
                            taskListResObj = $.grep(taskListTemplateResObj[j].TaskListDetails, function (e) {
                                return e.Id == parseInt(uiArray[i]);
                            });
                            if (taskListResObj.length > 0) {
                                taskListResObj[0].SortOder = uiArray.indexOf(taskListResObj[0].Id.toString()) + 1;
                                AdminTaskService.UpdateTemplateTaskListSortOrder(taskListTemplateResObj[j].Id, parseInt(taskListResObj[0].Id, 10), taskListResObj[0].SortOder).then(function () { });
                            }
                        }
                    }
                    $scope.$apply();
                }
            }).disableSelection();
        };
    }
    app.controller("mui.admin.tasktemplateCtrl", ['$scope', '$location', '$resource', '$timeout', '$compile', '$sce', '$translate', 'AdminTaskService', muiadmintasktemplateCtrl]);
})(angular, app);