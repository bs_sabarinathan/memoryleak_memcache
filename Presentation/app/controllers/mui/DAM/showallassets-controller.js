﻿(function (ng, app) {
    "use strict";

    function muiDAMshowallassetsCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, ShowAllAssetService, $compile, $modal) {
        var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");

        $scope.folderview = [];
        $scope.AllAssetFilesObj = {
            AssetFiles: [],
            AssetTaskObj: [],
            AllAssetDynamicData: [],
            AllAssetSelection: [],
            PageNo: 1,
            OrderBy: 4,
            selectedAssetId: 0
        };
        $scope.$on('LoadShowallassets', function (event, view, order) {
            GetDAMViewSettings();
            GetDAMToolTipSettings();
            LoadAllAssets(view, order);
        });
        if ($scope.DamViewName == "Thumbnail") LoadAllAssets(1, 4);
        else if ($scope.DamViewName == "Summary") LoadAllAssets(2, 4);
        else if ($scope.DamViewName == "List") LoadAllAssets(3, 4);

        $scope.EditassetfromShowall = function (AssetID) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: AssetID,
                            IsLock: $scope.IsLock,
                            isNotify: false,
                            viewtype: 'ViewType'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        function LoadAllAssets(view, order) {
            ShowAllAssetService.GetAllFolderStructure($stateParams.ID).then(function (data) {
                if (data.Response != null) {
                    $scope.folderview = [];
                    $scope.folderview = data.Response;
                    ShowAllAssetService.GetEntityAsset(0, $stateParams.ID, view, order, 1, true).then(function (data) {
                        var res = [];
                        res = data.Response;
                        if (view == 1) LoadThumbnailView(res);
                        else if (view == 2) LoadSummaryView(res);
                        else if (view == 3) LoadListView(res);
                        if ($scope.SelectAllAssetsObj.checkselectall == true) {
                            $scope.selectallassets(1);
                        }
                    });
                }
            });
        }

        function LoadThumbnailView(res) {
            if ($scope.AllAssetFilesObj.PageNo == 1) {
                $scope.AssetSelection = {};
                $scope.AssetSelectionClass = {};
                $scope.AllAssetFilesObj.AllAssetSelection = [];
                $scope.AllAssetFilesObj.AssetFiles = [];
                $scope.AssetFilesObj.AssetFiles = [];
                $scope.AllAssetFilesObj.AllAssetDynamicData = [];
            }
            for (var s = 0; s < $scope.folderview.length; s++) {
                var html = '';
                var folderid = $scope.folderview[s][$scope.folderview[s].length - 1].Id;
                if ($scope.AllAssetFilesObj.PageNo == 1) {
                    $("#thumbnailallassets_" + folderid).html($compile(html)($scope));
                }
                if (res != null) {
                    var assets = [];
                    assets = $.grep(res[0].AssetFiles, function (e) {
                        return e.FolderID == folderid || e.targetfolderid == folderid
                    });
                    if (assets.length != 0) {
                        for (var j = 0, asset; asset = assets[j++];) {
                            $scope.AllAssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                        }
                        var DynamicData = [];
                        DynamicData = res[0].AssetDynData;
                        for (var k = 0, dyn; dyn = assets[k++];) {
                            $scope.AllAssetFilesObj.AllAssetDynamicData.push($.grep(res[0].AssetDynData, function (e) {
                                return e.ID == dyn.AssetUniqueID
                            })[0]);
                        }
                        var isselectall = $scope.checkselectall;
                        for (var i = 0, asset; asset = assets[i++];) {
                            html += '<div class="th-Box" id="showalldivthboxid_' + asset.AssetUniqueID + '" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                            if (!$scope.checkselectall) {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                            } else {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                            }
                            var assetselection = "";
                            assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                            html += '<div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                            html += '<div class="th-chkBx">';
                            html += '<i class="icon-stop"></i>';
                            html += '<label class="checkbox checkbox-custom">';
                            html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                            html += '<i  id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                            html += '</label>';
                            html += '</div>';
                            html += '<div class="th-options">';
                            html += '<i class="icon-stop"></i>';
                            html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="assetActionMenu"></i>';
                            html += '</div>';
                            html += '<div class="th-Block" >';
                            html += '<div class="th-ImgBlock">';
                            html += '<div  class="th-ImgContainer"  ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\">';
                            if (asset.Category == 0) {
                                if (asset.Extension != null) {
                                    if (asset.ProcessType > 0) {
                                        if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                        else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                        else if (asset.Status == 3) {
                                            html += '<img src="' + imageBaseUrlcloud + '' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                        }
                                    } else {
                                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\" data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                    }
                                } else html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            } else if (asset.Category == 1) {
                                html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            } else if (asset.Category == 2) {
                                html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="th-DetailBlock">';
                            html += '<div class="th-ActionButtonContainer">';
                            html += '<span>';
                            html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                            if (asset.Category == 0) {
                                if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                                else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                                if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                                else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                            } else {
                                if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                                else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                                if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                                else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                            }
                            html += '</span>';
                            html += '</div>';
                            html += '<div class="th-detail-eIconContainer">';
                            html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                            html += '</div>';
                            html += '<div class="th-Details">';
                            try {
                                html += GenereteThumbnailDetailBlock(asset);
                            } catch (e) { }
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        $("#thumbnailallassets_" + folderid).append($compile(html)($scope));
                    } else {
                        if ($scope.AllAssetFilesObj.PageNo == 1) {
                            var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                            $("#thumbnailallassets_" + folderid).append($compile(emptyHtml)($scope));
                        }
                    }
                } else {
                    if ($scope.AllAssetFilesObj.PageNo == 1) {
                        var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                        $("#thumbnailallassets_" + folderid).append($compile(emptyHtml)($scope));
                    }
                }
            }
        }

        function GenereteThumbnailDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AllAssetFilesObj.AllAssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined)
                                if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }

        function LoadSummaryView(res) {
            if ($scope.AllAssetFilesObj.PageNo == 1) {
                $scope.AssetSelection = {};
                $scope.AssetSelectionClass = {};
                $scope.AllAssetFilesObj.AllAssetSelection = [];
                $scope.AllAssetFilesObj.AssetFiles = [];
                $scope.AssetFilesObj.AssetFiles = [];
                $scope.AllAssetFilesObj.AllAssetDynamicData = [];
            }
            for (var s = 0; s < $scope.folderview.length; s++) {
                var html = '';
                var folderid = $scope.folderview[s][$scope.folderview[s].length - 1].Id;
                if ($scope.AllAssetFilesObj.PageNo == 1) {
                    $("#thumbnailallassets_" + folderid).html($compile(html)($scope));
                }
                if (res != null) {
                    var assets = [];
                    assets = $.grep(res[0].AssetFiles, function (e) {
                        return e.FolderID == folderid || e.targetfolderid == folderid
                    });
                    if (assets.length != 0) {
                        for (var j = 0, asset; asset = assets[j++];) {
                            $scope.AllAssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                        }
                        var DynamicData = res[0].AssetDynData;
                        for (var k = 0, dyn; dyn = assets[k++];) {
                            var dynResult = $.grep(res[0].AssetDynData, function (e) {
                                return e.ID == dyn.AssetID
                            })[0];
                            if (dynResult != undefined || dynResult != null) $scope.AllAssetFilesObj.AllAssetDynamicData.push(dynResult);
                        }
                        var isselectall = $scope.checkselectall;
                        for (var i = 0, asset; asset = assets[i++];) {
                            if (!$scope.checkselectall) {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                            } else {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                            }
                            var assetselection = "";
                            assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                            html += '<div class="th-sum-Box" id="showalldivthsumboxid_' + asset.AssetUniqueID + '"> ';
                            html += '        <div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                            html += '            <div class="th-sum-chkBx"> ';
                            html += '                <i class="icon-stop"></i> ';
                            html += '                <label class="checkbox checkbox-custom"> ';
                            html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + asset.AssetUniqueID + '" type="checkbox" /> ';
                            html += '                    <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                            html += '                </label> ';
                            html += '            </div> ';
                            html += '            <div class="th-sum-options">';
                            html += '                <i class="icon-stop"></i>';
                            html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="assetActionMenu"></i>';
                            html += '            </div>';
                            html += '            <div class="th-sum-Block"> ';
                            html += '                <div class="th-sum-leftSection"> ';
                            html += '                    <div class="th-sum-ImgBlock"> ';
                            html += '                        <div class="th-sum-ImgContainer" ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\"> ';
                            if (asset.Category == 0) {
                                if (asset.Extension != null) {
                                    if (asset.ProcessType > 0) {
                                        if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                        else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                        else if (asset.Status == 3) {
                                            html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                        }
                                    } else {
                                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                    }
                                } else {
                                    html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else if (asset.Category == 1) {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            } else if (asset.Category == 2) {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                            html += '                        </div> ';
                            html += '                    </div> ';
                            html += '                    <div class="th-sum-baiscDetailBlock"> ';
                            html += '                        <div class="th-sum-ActionButtonContainer"> ';
                            html += '                            <span> ';
                            html += '                                <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                            html += '                                <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                            if (asset.Category == 0) {
                                if (asset.IsPublish == true) html += '                                   <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                                else html += '                                <i id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                                if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                                else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                            } else {
                                if (asset.IsPublish == true) html += '                                  <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                                else html += '                                <i id="Publishicon_' + asset.AssetUniqueID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                                if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                                else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                            }
                            html += '                            </span> ';
                            html += '                        </div> ';
                            html += '                        <div class="th-sum-detail-eIconContainer"> ';
                            html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                            html += '                        </div> ';
                            html += '                        <div class="th-sum-basicDetails"> ';
                            try {
                                html += GenereteSummaryDetailBlock(asset);
                            } catch (e) { }
                            html += '                        </div> ';
                            html += '                    </div> ';
                            html += '                </div> ';
                            html += '                <div class="th-sum-rightSection"> ';
                            html += '                    <div class="th-sum-expandedDetailBlock"> ';
                            html += '                        <div class="th-sum-expandedHeader"> ';
                            html += '                            <h4>' + asset.AssetName + '</h4> ';
                            html += '                        </div> ';
                            html += '                        <div class="th-sum-expandedDetails"> ';
                            html += '                            <form class="form-horizontal"> ';
                            try {
                                html += DrawSummaryBlock(asset);
                            } catch (e) { }
                            html += '                            </form> ';
                            html += '                        </div> ';
                            html += '                    </div> ';
                            html += '                </div> ';
                            html += '            </div> ';
                            html += '        </div> ';
                            html += '    </div> ';
                        }
                        $("#thumbnailallassets_" + folderid).html($compile(html)($scope));
                    } else {
                        if ($scope.AllAssetFilesObj.PageNo == 1) {
                            var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                            $("#thumbnailallassets_" + folderid).html(emptyHtml);
                        }
                    }
                } else {
                    if ($scope.AllAssetFilesObj.PageNo == 1) {
                        var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                        $("#thumbnailallassets_" + folderid).html(emptyHtml);
                    }
                }
            }
        }

        function GenereteSummaryDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AllAssetFilesObj.AllAssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined)
                                if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + ' ' + "" + ' control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += '                                    <label class="control-label">' + attr.Caption + '</label> ';
                    html += '                                    <div class="controls"> ';
                    if ($scope.AllAssetFilesObj.AllAssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                        else html += '<span class="control-label">-</span>';
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += '                                    </div> ';
                    html += '                                </div> ';
                }
            }
            return html;
        }

        function LoadListView(res) {
            if ($scope.AllAssetFilesObj.PageNo == 1) {
                $scope.AssetSelection = {};
                $scope.AssetSelectionClass = {};
                $scope.AllAssetFilesObj.AllAssetSelection = [];
                $scope.AllAssetFilesObj.AssetFiles = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                $scope.AllAssetFilesObj.AllAssetDynamicData = [];
            }
            for (var s = 0; s < $scope.folderview.length; s++) {
                var html = '';
                var folderid = $scope.folderview[s][$scope.folderview[s].length - 1].Id;
                if ($scope.AllAssetFilesObj.PageNo == 1) {
                    $("#thumbnailallassets_" + folderid).html($compile(html)($scope));
                }
                if (res != null) {
                    var assets = [];
                    assets = $.grep(res[0].AssetFiles, function (e) {
                        return e.FolderID == folderid || e.targetfolderid == folderid
                    });
                    if (assets.length != 0) {
                        for (var j = 0, asset; asset = assets[j++];) {
                            $scope.AllAssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                        }
                        var DynamicData = res[0].AssetDynData;
                        for (var k = 0, dyn; dyn = assets[k++];) {
                            var dynResult = $.grep(res[0].AssetDynData, function (e) {
                                return e.ID == dyn.AssetID
                            })[0];
                            if (dynResult != undefined || dynResult != null) $scope.AllAssetFilesObj.AllAssetDynamicData.push(dynResult);
                        }
                        if ($scope.AllAssetFilesObj.PageNo == 1) {
                            html += '<div>';
                            html += '    <table id="AssetListViewTable" class="table table-normal-both ListviewTable">';
                            html += '        <thead>';
                            try {
                                html += GenerateHeader();
                            } catch (e) { }
                            html += '        </thead>';
                            html += '        <tbody>';
                            try {
                                html += GenerateList(assets);
                            } catch (e) { }
                            html += '        </tbody>';
                            html += '    </table>';
                            html += '</div>';
                            $("#thumbnailallassets_" + folderid).html($compile(html)($scope));
                        } else {
                            html += GenerateList(assets);
                            $("#AssetListViewTable tbody").append($compile(html)($scope));
                        }
                    } else {
                        if ($scope.AllAssetFilesObj.PageNo == 1) {
                            var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                            $("#thumbnailallassets_" + folderid).html($compile(emptyHtml)($scope));
                        }
                    }
                } else {
                    if ($scope.AllAssetFilesObj.PageNo == 1) {
                        var emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                        $("#thumbnailallassets_" + folderid).html($compile(emptyHtml)($scope));
                    }
                }
            }
        }

        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input ng-click="checkAllAssets($event)" type="checkbox" />';
            html += '<input type="checkbox" ng-model="showallselectedAllunassignedtasks" ng-click=\"checkAllAssets($event)\" id="showalldamlistviewselectall" />';
            html += '<i id="showalllistselectheader" ng-class="showallListfullselection"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.SettingsAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.SettingsAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }
            html += '<th></th>';
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                if (!$scope.checkselectall) {
                    $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                } else {
                    $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                    $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                }
                var assetselection = "";
                assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                html += '<tr id="showalldivthlistboxid_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
                html += '   <td>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                html += '           <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <span class="thmbListviewImgSpan" ng-click="EditassetfromShowall(' + asset.AssetUniqueID + ')">';
                html += '           <div class="thmbListview-eIconContainer" > ';
                html += '               <span my-qtip2 qtip-content="' + asset.Caption + '" class="thmbListview-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.SettingsAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.SettingsAttributes["ListViewSettings"][i++];) {
                        html += '<td>';
                        if ($scope.AllAssetFilesObj.AllAssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                                return e.ID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                if (data.length > 0) {
                                    if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != " " && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                    else html += '<span class="thmbListviewDescSpan">-</span>';
                                } else html += '<span class="thmbListviewDescSpan">-</span>';
                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '   <td><span class="thmbListview-options"><i id="assetlistoptionsIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="assetActionMenu"></i></span></td>';
                html += '</tr>';
            }
            return html;
        }
        
        $scope.SetAssetActionId = function (assetid) {
            $('#chkBoxIcon_' + assetid + '').addClass('checked');
            var checkboxchecked = true;
            var assetselection = "";
            if ($scope.DamViewName == "Thumbnail") {
                assetselection = checkboxchecked == true ? "th-Selection selected" : "th-Selection";
                $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
                $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
                $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
            } else if ($scope.DamViewName == "Summary") {
                assetselection = checkboxchecked == true ? "th-sum-Selection selected" : "th-sum-Selection";
                $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
                $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
                $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
            } else if ($scope.DamViewName == "List") {
                assetselection = checkboxchecked == true ? "li-Selection selected" : "li-Selection";
                $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
                $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
                $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
            }
            $scope.AssetFilesObj.selectedAssetId = assetid;
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                return e == $scope.AssetFilesObj.selectedAssetId;
            });
            if (remainRecord.length == 0) {
                $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
            }
            var remdownloadrecord = [];
            remdownloadrecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                return e.AssetId == assetid;
            });
            if (remdownloadrecord.length == 0) {
                var res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid
                });
                if (res.length > 0) $scope.DownloadFileObj.DownloadFilesArr.push({
                    AssetId: assetid,
                    FileName: res[0].FileName,
                    Fileguid: res[0].FileGuid,
                    Extension: res[0].Extension,
                    Category: res[0].Category
                });
            }
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                        return e.Category == 1 && e.AssetUniqueID == assetid;
                    });
                    if (temparr.length > 0) {
                        $scope.IsPublish = false;
                        $scope.UnPublish = false;
                        $scope.showhidepackage = false;
                        $scope.IsBlankAsset = false;
                        return true;
                    } else {
                        $scope.showhidepackage = true;
                        $scope.IsBlankAsset = true;
                    }
                }
            }
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    if ($('#Publishicon_' + assetid)[0].className == "icon-arrow-up") {
                        $scope.IsPublish = false;
                        $scope.UnPublish = true;
                    } else {
                        $scope.IsPublish = true;
                        $scope.UnPublish = false;
                    }
                } else {
                    $scope.IsPublish = true;
                    $scope.UnPublish = true;
                }
            } else {
                $scope.IsBlankAsset = true;
                $scope.IsPublish = true;
                $scope.UnPublish = true;
                $scope.showhidepackage = true;
            }
        }
        $scope.CallBackAssetAction = function (assetid, istask, tasktype) {
            var actionName = assetAction.get(assetid);
            if (actionName != undefined) $scope.$emit('DamAssetAction', actionName, istask, tasktype);
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            var assetselection = "";
            if ($scope.DamViewName == "Thumbnail") {
                assetselection = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            } else if ($scope.DamViewName == "Summary") {
                assetselection = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            } else if ($scope.DamViewName == "List") {
                assetselection = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
            }
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkbox.checked == true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkbox.checked;
            var res = [];
            res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid, res[0].Category);
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.AssetFilesObj.AssetSelection.push(assetid);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
            if (!checkbox.checked) {
                $scope.$emit('checkselectall', false);
            }
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if (ischecked == true) {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                    return e.ID == assetid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
            }
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
             tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }

        function DownloadAssets(checkbox, assetid, name, ext, guid, category) {
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.push({
                        AssetId: assetid,
                        FileName: name,
                        Fileguid: guid,
                        Extension: ext,
                        Category: category
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.splice($.inArray(remainRecord[0], $scope.DownloadFileObj.DownloadFilesArr), 1);
                }
            }
        }

        function GetDAMViewSettings() {
            ShowAllAssetService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SettingsAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }

        function GetDAMToolTipSettings() {
            ShowAllAssetService.GetDAMToolTipSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsAttributes["ThumbnailToolTipSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsAttributes["ListViewToolTipSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }
        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                ShowAllAssetService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var html = '';
                    html += '<div class="th-AssetInfoTooltip">';
                    html += DrawThumbnailBlock(attrList);
                    html += '</div> ';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ThumbnailToolTipSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.SettingsAttributes["ThumbnailToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var data = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (data != undefined) $scope.attributedata.push(data);
                    }
                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</span></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption[0] + '</span></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span></div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datstartval = new Date($scope.attributedata[i].Value[j].Startdate);
                                datendval = new Date($scope.attributedata[i].Value[j].EndDate);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.attributedata[i].Caption + '</span></div>';
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.attributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div>';
                        }
                    }
                }
            }
            return html;
        }
        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                ShowAllAssetService.GetAttributeDetails(assetid).then(function (attrList) {
                    $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                        return e.ID == attrList.Response.ActiveFileID;
                    })[0];
                    var assets = [];
                    assets = $.grep($scope.AllAssetFilesObj.AssetFiles, function (e) {
                        return e.AssetUniqueID == assetid
                    });
                    var html = '';
                    html += '       <div class="lv-ImgDetailedPreview">';
                    html += '           <div class="idp-Box"> ';
                    html += '               <div class="idp-Block"> ';
                    html += '                   <div class="idp-leftSection"> ';
                    html += '                       <div class="idp-ImgBlock"> ';
                    html += '                           <div class="idp-ImgDiv"> ';
                    html += '                               <div class="idp-ImgContainer"> ';
                    if (attrList.Response.Category == 0) {
                        if ($scope.ActiveVersion.ProcessType > 0) {
                            if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"EditassetfromShowall(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            else if ($scope.ActiveVersion.Status == 3) {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (attrList.Response.Category == 1) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (attrList.Response.Category == 2) {
                        html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                               </div> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                       <div class="idp-baiscDetailBlock"> ';
                    html += '                           <div class="idp-ActionButtonContainer"> ';
                    html += '                               <span> ';
                    html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (attrList.Response.Category == 0) {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (assets[0].LinkUserDetail != '') html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
                        else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (assets[0].LinkUserDetail != '') html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                               </span> ';
                    html += '                           </div> ';
                    html += '                           <div class="idp-detail-eIconContainer"> ';
                    html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '                   <div class="idp-rightSection"> ';
                    html += '                       <div class="idp-expandedDetailBlock"> ';
                    html += '                           <div class="idp-expandedDetails"> ';
                    html += '                               <form class="form-horizontal"> ';
                    try {
                        html += DrawListBlock(attrList);
                    } catch (e) { }
                    html += '                               </form> ';
                    html += '                           </div> ';
                    html += '                       </div> ';
                    html += '                   </div> ';
                    html += '               </div> ';
                    html += '           </div> ';
                    html += '       </div>';
                    $('#' + qtip_id).html(html);
                });
            }
        };

        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsAttributes["ListViewToolTipSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.SettingsAttributes["ListViewToolTipSettings"], function (e) {
                        return e.assetType == attrList.Response.AssetTypeid
                    });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var isexist = $.grep(attrList.Response.AttributeData, function (e) {
                            return e.ID == tooltipattrs[l].ID
                        })[0];
                        if (isexist != null) $scope.attributedata.push(isexist);
                    }
                }
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.attributedata[i].Value + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2 && $scope.attributedata[i].ID != 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 2 && $scope.attributedata[i].ID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        } else {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html('-').text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.attributedata[i].Caption[0] + '</label></div></div>';
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.attributedata[i].Caption[0] + '</label></div></div>';
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datstartval = new Date($scope.attributedata[i].Value[j].Startdate);
                                datendval = new Date($scope.attributedata[i].Value[j].EndDate);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];
                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = {
                usersID: ''
            };
            $scope.UserId = $cookies['UserId'];
        }
        $scope.$on('ShowallUpdatePublishIcon', function (event, fileids) {
            for (var i = 0; i < fileids.length; i++) {
                $('#Publishicon_' + fileids[i]).removeClass('displayNone')
            }
        });
        $scope.$on('ShowallUpdateUnPublishIcon', function (event, fileids) {
            for (var i = 0; i < fileids.length; i++) {
                $('#Publishicon_' + fileids[i]).removeClass('displayNone');
                $('#Publishicon_' + fileids[i]).addClass('displayNone');
            }
        });
        $scope.$on('loadShowallAssetDetailSectionfr_th', function (event, assetList, view) {
            ShowAllAssetService.GetAssetDetsforSelectedAssets(0, $stateParams.ID, view, 4, 1, true, assetList).then(function (data) {
                var res = [];
                res = data.Response;
                if (view == 1) LoadViewDetails_th(res);
                else if (view == 2) LoadViewDetails_sum(res);
                else if (view == 3) LoadViewDetails_list(res);
            });
        });

        function LoadViewDetails_th(res) {
            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    var assetData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    var assettempData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    if (assetData != null) {
                        if (assetData.length == 0) {
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                        } else {
                            $scope.AssetFilesObj.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj.AssetFiles), 1);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            if (assettempData.length > 0) {
                                $scope.AssetFilesObj_Temp.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj_Temp.AssetFiles), 1);
                                $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                            }
                        }
                    } else {
                        $scope.AssetFilesObj.AssetFiles.push(asset);
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                var DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    var temp = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                        return e.ID == dyn.ID;
                    });
                    if (temp.length > 0) {
                        $scope.AllAssetFilesObj.AllAssetDynamicData.splice($.inArray(temp[0], $scope.AllAssetFilesObj.AllAssetDynamicData), 1);
                    }
                    $scope.AllAssetFilesObj.AllAssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    var html = '';
                    html += '<div class="th-Box"  id="divthboxid_' + asset.AssetUniqueID + '">';
                    if (!$scope.SelectAllAssetsObj.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                    html += '<i  id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="assetActionMenu"></i>';
                    html += '</div>';
                    html += '<div class="th-Block" >';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '  ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\" data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock" >';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" ng-click="IsActiveEntity($event)" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details" id="thid_' + asset.AssetUniqueID + '">';
                    html += GenereteThumbnailDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    $('#showalldivthboxid_' + asset.AssetUniqueID).html($compile('')($scope));
                    $('#showalldivthboxid_' + asset.AssetUniqueID).html($compile(html)($scope));
                }
            }
        }

        function LoadViewDetails_sum(res) {
            if (res != null) {
                var assets = [];
                var html = "";
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    var assetData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    var assettempData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    if (assetData != null) {
                        if (assetData.length == 0) {
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                        } else {
                            $scope.AssetFilesObj.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj.AssetFiles), 1);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            if (assettempData.length > 0) {
                                $scope.AssetFilesObj_Temp.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj_Temp.AssetFiles), 1);
                                $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                            }
                        }
                    } else {
                        $scope.AssetFilesObj.AssetFiles.push(asset);
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                var DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    var temp = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                        return e.ID == dyn.ID;
                    });
                    if (temp.length > 0) {
                        $scope.AllAssetFilesObj.AllAssetDynamicData.splice($.inArray(temp[0], $scope.AllAssetFilesObj.AllAssetDynamicData), 1);
                    }
                    $scope.AllAssetFilesObj.AllAssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    if (!$scope.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div class="th-sum-Box" id="showalldivthsumboxid_' + asset.AssetUniqueID + '"> ';
                    html += '        <div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + asset.AssetUniqueID + '" type="checkbox" /> ';
                    html += '                    <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-options">';
                    html += '                <i class="icon-stop"></i>';
                    html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="assetActionMenu"></i>';
                    html += '            </div>';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer" ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\"> ';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"EditassetfromShowall(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '                                   <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '                                   <i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i>';
                        else html += '                                <i id="Publishicon_' + asset.AssetUniqueID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    try {
                        html += GenereteSummaryDetailBlock(asset);
                    } catch (e) { }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    try {
                        html += DrawSummaryBlock(asset);
                    } catch (e) { }
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                    $('#showalldivthsumboxid_' + asset.AssetUniqueID).html($compile('')($scope));
                    $('#showalldivthsumboxid_' + asset.AssetUniqueID).html($compile(html)($scope));
                }
            }
        }

        function LoadViewDetails_list(res) {
            if (res != null) {
                var html = "";
                var assets = [];
                var asset = [];
                assets = res[0].AssetFiles;
                asset = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    var assetData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    var assettempData = $.grep($scope.AssetFilesObj.AssetFiles, function (rel) {
                        return rel.AssetUniqueID == asset.AssetUniqueID
                    });
                    if (assetData != null) {
                        if (assetData.length == 0) {
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                        } else {
                            $scope.AssetFilesObj.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj.AssetFiles), 1);
                            $scope.AssetFilesObj.AssetFiles.push(asset);
                            if (assettempData.length > 0) {
                                $scope.AssetFilesObj_Temp.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj_Temp.AssetFiles), 1);
                                $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                            }
                        }
                    } else {
                        $scope.AssetFilesObj.AssetFiles.push(asset);
                        $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                    }
                }
                var DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    var temp = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                        return e.ID == dyn.ID;
                    });
                    if (temp.length > 0) {
                        $scope.AllAssetFilesObj.AllAssetDynamicData.splice($.inArray(temp[0], $scope.AllAssetFilesObj.AllAssetDynamicData), 1);
                    }
                    $scope.AllAssetFilesObj.AllAssetDynamicData.push(dyn);
                }
                if (assetData.length != 0) {
                    try {
                        for (var i = 0, asset; asset = assets[i++];) {
                            if (!$scope.checkselectall) {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                            } else {
                                $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                                $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                            }
                            var assetselection = "";
                            assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                            html += '   <td>';
                            html += '       <label class="checkbox checkbox-custom">';
                            html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                            html += '           <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                            html += '       </label>';
                            html += '   </td>';
                            html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                            html += '       <span class="thmbListviewImgSpan" ng-click="EditassetfromShowall(' + asset.AssetUniqueID + ')">';
                            html += '           <div class="thmbListview-eIconContainer" > ';
                            html += '               <span my-qtip2 qtip-content="' + asset.Caption + '" class="thmbListview-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                            html += '           </div>';
                            html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                            html += '       </span>';
                            html += '   </td>';
                            if ($scope.SettingsAttributes["ListViewSettings"].length > 0) {
                                for (var i = 0, attr; attr = $scope.SettingsAttributes["ListViewSettings"][i++];) {
                                    html += '<td>';
                                    if ($scope.AllAssetFilesObj.AllAssetDynamicData.length > 0) {
                                        var data = [];
                                        data = $.grep($scope.AllAssetFilesObj.AllAssetDynamicData, function (e) {
                                            return e.ID == asset.AssetUniqueID;
                                        });
                                        if (data.length > 0) {
                                            if (data.length > 0) {
                                                if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != " " && data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
                                                else html += '<span class="thmbListviewDescSpan">-</span>';
                                            } else html += '<span class="thmbListviewDescSpan">-</span>';
                                        } else html += '<span class="thmbListviewDescSpan">-</span>';
                                    } else html += '<span class="thmbListviewDescSpan">-</span>';
                                    html += '</td>';
                                }
                            }
                            html += '   <td><span class="thmbListview-options"><i id="assetlistoptionsIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="assetActionMenu"></i></span></td>';
                            $('#showalldivthlistboxid_' + asset.AssetUniqueID).html($compile('')($scope));
                            $('#showalldivthlistboxid_' + asset.AssetUniqueID).html($compile(html)($scope));
                        }
                    } catch (e) { }
                }
            }
        }
        $scope.checkAllAssets = function ($event) {
            var checkbox = $event.target;
            if (checkbox.checked) {
                $scope.selectedAllunassignedtasks = true;
                $scope.SelectAllAssetsObj.checkselectall = true;
                $scope.$emit('clickcheckselectall', 1);
            } else {
                $scope.selectedAllunassignedtasks = false;
                $scope.SelectAllAssetsObj.checkselectall = false;
                $scope.$emit('clickDselectall');
            }
        }

        $scope.assetMenuActions = function (action) {
            var AssetObj = {};
            AssetObj.IsLock = $scope.AssetFilesObj.IsLock,
            AssetObj.isNotify = false,
            AssetObj.viewtype = $scope.DamViewName,
            $scope.$emit('callBacktoDam', action, AssetObj);
        }
    }
    app.controller("mui.DAM.showallassetsCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', 'ShowAllAssetService', '$compile', '$modal', muiDAMshowallassetsCtrl]);
})(angular, app);