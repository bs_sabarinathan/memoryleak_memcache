﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class MetadataVersion : IMetadataVersion
    {

        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected int _state;
        protected DateTime _startdate;
        protected DateTime _enddate;

        #endregion

        #region Constructors
        public MetadataVersion() { }

        public MetadataVersion(int pId, string pName, string pDescription, int pState, DateTime pStartDate, DateTime pEndDate)
        {
            this._id = pId; 
            this._name = pName;
            this._description = pDescription;
            this._state = pState;
            this._startdate = pStartDate;
            this._enddate = pEndDate;
        }
        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }



        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _name = value;
            }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }

        public int State
        {
            get { return _state; }
            set { _state = value; }
        }

        public DateTime StartDate
        {
            get { return _startdate; }
            set { _startdate = value; }

        }

        public DateTime EndDate
        {
            get { return _enddate; }
            set { _enddate = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            MetadataVersion castObj = null;
            try
            {
                castObj = (MetadataVersion)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
