using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
    {

    /// <summary>
    /// NavigationDao object for table 'CM_CustomTabDao'.
    /// </summary>

    public partial class CustomTabDao : BaseDao , ICloneable
        {
        #region Member Variables

        protected int _id;
        protected int _Typeid;
        protected string _name;
        protected string _externalurl;
        protected bool _issytemdefined;
        protected bool _addusername;
        protected bool _adduseremail;
        protected bool _adduserid;
        protected bool _addlanguagecode;
        protected bool _addentityid;
        protected int _featureid;
        protected int _sortorder;
        protected string _controleid;
        protected int _entityttypeid;
        protected int _attributegroupid;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public CustomTabDao() { }

        public CustomTabDao(int pId , int pTypeid , string pName , string pExternalUrl , bool pIsSystemDefined , bool pAddUserName , bool pAddUserEmail , bool pAddEntityID , bool pAddUserID , bool pAddLanguageCode , int pFeatureID , int pSortOrder , int pentitytypeid , int pattributegroupid)
            {
            this._id = pId;
            this._Typeid = pTypeid;
            this._name = pName;
            this._externalurl = pExternalUrl;
            this._issytemdefined = pIsSystemDefined;
            this._addusername = pAddUserName;
            this._adduseremail = pAddUserEmail;
            this._addentityid = pAddEntityID;
            this._adduserid = pAddUserID;
            this._addlanguagecode = pAddLanguageCode;
            this._featureid = pFeatureID;
            this._sortorder = pSortOrder;
            this._entityttypeid = pentitytypeid;
            this._attributegroupid = pattributegroupid;
            }

        #endregion

        #region Public Properties

        public virtual int ID
            {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

            }

        public virtual int Typeid
            {
            get { return _Typeid; }
            set { _bIsChanged |= (_Typeid != value); _Typeid = value; }

            }



        public virtual string Name
            {
            get { return _name; }
            set
                {
                if(value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Caption" , "Caption value, cannot contain more than 50 characters");
                _bIsChanged |= (_name != value);
                _name = value;
                }

            }


        public virtual string ExternalUrl
            {
            get { return _externalurl; }
            set
                {
                if(value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("ExternalUrl" , "ExternalUrl value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_externalurl != value);
                _externalurl = value;
                }

            }


        public virtual bool IsSytemDefined
            {
            get { return _issytemdefined; }
            set { _bIsChanged |= (_issytemdefined != value); _issytemdefined = value; }

            }


        public virtual bool AddUserName
            {
            get { return _addusername; }
            set { _bIsChanged |= (_addusername != value); _addusername = value; }

            }

        public virtual bool AddUserEmail
            {
            get { return _adduseremail; }
            set { _bIsChanged |= (_adduseremail != value); _adduseremail = value; }

            }

        public virtual bool AddUserID
            {
            get { return _adduserid; }
            set { _bIsChanged |= (_adduserid != value); _adduserid = value; }

            }

        public virtual bool AddLanguageCode
            {
            get { return _addlanguagecode; }
            set { _bIsChanged |= (_addlanguagecode != value); _addlanguagecode = value; }

            }

        public virtual bool AddEntityID
            {
            get { return _addentityid; }
            set { _bIsChanged |= (_addentityid != value); _addentityid = value; }

            }

        public virtual int FeatureID
            {
            get { return _featureid; }
            set { _bIsChanged |= (_featureid != value); _featureid = value; }
            }

        public virtual int SortOrder
            {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
            }

        public virtual string ControleID
            {
            get { return _controleid; }
            set { _bIsChanged |= (_controleid != value); _controleid = value; }
            }

        public virtual int EntityTypeID
            {
            get { return _entityttypeid; }
            set { _entityttypeid = value; }
            }
        public virtual int AttributeGroupID
            {
            get { return _attributegroupid; }
            set { _attributegroupid = value; }
            }
        public virtual bool IsDeleted
            {
            get
                {
                return _bIsDeleted;
                }
            set
                {
                _bIsDeleted = value;
                }
            }

        public virtual bool IsChanged
            {
            get
                {
                return _bIsChanged;
                }
            set
                {
                _bIsChanged = value;
                }
            }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
            {
            return this.MemberwiseClone();
            }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
            {
            public const string NavigationDao = "CustomTabDao";
            public const string ID = "ID";
            public const string Typeid = "Typeid";
            public const string Name = "Name";
            public const string ExternalUrl = "ExternalUrl";
            public const string IsSytemDefined = "IsSytemDefined";
            public const string AddUserName = "AddUserName";
            public const string AddUserEmail = "AddUserEmail";
            public const string AddUserID = "AddUserID";
            public const string AddLanguageCode = "AddLanguageCode";
            public const string AddEntityID = "AddEntityID";
            public const string FeatureID = "FeatureID";
            public const string SortOrder = "SortOrder";
            public const string ControleID = "ControleID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
            {
            public const string NavigationDao = "CustomTabDao";
            public const string ID = "ID";
            public const string Typeid = "Typeid";
            public const string Name = "Name";
            public const string ExternalUrl = "ExternalUrl";
            public const string IsSytemDefined = "IsSytemDefined";
            public const string AddUserName = "AddUserName";
            public const string AddUserEmail = "AddUserEmail";
            public const string AddUserID = "AddUserID";
            public const string AddLanguageCode = "AddLanguageCode";
            public const string AddEntityID = "AddEntityID";
            public const string FeatureID = "FeatureID";
            public const string SortOrder = "SortOrder";
            public const string ControleID = "ControleID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            }

        #endregion


        }

    }
