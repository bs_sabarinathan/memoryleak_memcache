﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IPurchaseOrder interface for table 'PM_PurchaseOrder'.
    /// </summary>
    public interface IPurchaseOrderDetail
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int CostCentreID
        {
            get;
            set;

        }

        decimal Amount
        {
            get;
            set;

        }

        int POID
        {
            get;
            set;

        }

        int CurrencyType
        {
            get;
            set;

        }


        #endregion
    }
}
