﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class AttributeToAttributeRelationsDao : BaseDao, ICloneable 
    {
        #region Member Variables
        protected int _id;
        protected int _entitytypeid;
        protected int _attributetypeid;
        protected int _attributeid;
        protected int _attributeoptionid;
        protected int _attibutelevel;
        protected string _attributerelationid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors

         public AttributeToAttributeRelationsDao() { }

         public AttributeToAttributeRelationsDao(int id, int entitytypeid, int attributetypeid, int attributeid, int attributeoptionid, int attibutelevel, string attributerelationid)
        {
            this._id = id;
            this._entitytypeid = entitytypeid;
            this._attributetypeid = attributetypeid;
            this._attributeid = attributeid;
            this._attributeoptionid = attributeoptionid;
            this._attibutelevel = attibutelevel;
            this._attributerelationid = attributerelationid;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }

        }

        public virtual int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _bIsChanged |= (_attributetypeid != value); _attributetypeid = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }

        public virtual int AttributeOptionID
        {
            get { return _attributeoptionid; }
            set { _bIsChanged |= (_attributeoptionid != value); _attributeoptionid = value; }

        }

        public virtual int AttributeLevel
        {
            get { return _attibutelevel; }
            set { _bIsChanged |= (_attibutelevel != value); _attibutelevel = value; }

        }

        public virtual string AttributeRelationID
        {
            get { return _attributerelationid; }
            set { _bIsChanged |= (_attributerelationid != value); _attributerelationid = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
