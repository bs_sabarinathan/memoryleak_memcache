using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.User.Interface
{
    /// <summary>
    /// IUser interface for table 'UM_ResetPasswordTrack'.
    /// </summary>
    public interface IResetPassword
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        Guid TrackingID
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        } 

        DateTime ResetPasswordDate
        {
            get;
            set;

        }
        #endregion
    }
}
