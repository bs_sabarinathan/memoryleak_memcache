﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.admin.entitystatusCtrl",
    function muiadminentitystatusCtrl($scope, $location, $resource, $timeout, $compile, $translate, AdminService) {

        $scope.EntityTempConditionResult = [];
        $scope.EntityConditontext = true;
        $scope.EntityTypes = [];
        $scope.EntityStatusOptions = [];
        $scope.EntityStatusOption = 0;

        var deleteConditionList = [];
        var CurrEntityType = 0;

        $scope.addDays = [{ Days: 0, Caption: "+0 day" }, { Days: 1, Caption: "+1 day" }, { Days: 2, Caption: "+2 days" }, { Days: 3, Caption: "+3 days" }, { Days: 4, Caption: "+4 days" }, { Days: 5, Caption: "+5 days" }, { Days: 6, Caption: "+6 days" },
                           { Days: 7, Caption: "+1 Week" }, { Days: 14, Caption: "+2 Weeks" }, { Days: 30, Caption: "+1 Month" }, { Days: 60, Caption: "+2 Months" }, { Days: 90, Caption: "+3 Months" }, { Days: 180, Caption: "+6 Months" }]


        /***************** Get All EntityTypes *****************/

        AdminService.getEntityToSetOverallStauts(3).then(function (entitytypes) {
            $scope.EntityTypes = entitytypes.Response;
        })

        //*************** Load All Templates with Condition ***************//

        AdminService.GetEntityTemplateDetails().then(function (GetEntityTemplateDetailsRes) {
            if (GetEntityTemplateDetailsRes.Response != null) {
                $scope.EntityTempConditionResult = GetEntityTemplateDetailsRes.Response;
            }
        });

        $scope.EntityTempTab = 0;
        $scope.EntityShowTemplatecreationmodel = function () {
            $scope.EntityTemplatePopUpText = 'Add Entitytask Template'
            $scope.EntityTempTab = 0;
            $scope.TempCaption = "";
            $scope.TempDescription = "";
            $('#EntitytemplateCreationModal').modal("show");
            $timeout(function () { $('#Caption').focus().select(); }, 1000);
        }

        /*************** Insert/Update Templates ***************/


        $scope.SaveEntityTempList = function () {
            if ($scope.TempCaption == undefined || $scope.TempCaption.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1848.Caption'));
                return false;
            }

            var insertentitytempparams = {};
            insertentitytempparams.ID = $scope.EntityTempTab;
            insertentitytempparams.caption = $scope.TempCaption;
            insertentitytempparams.description = $scope.TempDescription;

            AdminService.InsertUpdateEntityTemplate(insertentitytempparams).then(function (res) {
                if (res != null && res != 0) {
                    if ($scope.EntityTempTab != 0) {
                        UpdateRefreshTemplteData($scope.EntityTempTab);
                    }
                    else {
                        $scope.EntityTempConditionResult.push({ ID: parseInt(res.Response), Caption: $scope.TempCaption, Description: $scope.TempDescription, conditions: [] });
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                    $scope.TaskTempTabId = 0;
                }
            });
            $('#EntitytemplateCreationModal').modal("hide");
        }

        $scope.UpdateTemplate = function (templateId, Caption, Description) {
            $scope.EntityTemplatePopUpText = "Entity Overall Status template"
            $('#EntitytemplateCreationModal').modal("show");
            $scope.TempCaption = Caption;
            $scope.TempDescription = Description;
            $scope.EntityTempTab = templateId;
            $timeout(function () { $('#Caption').focus().select(); }, 1000);
        }

        function UpdateRefreshTemplteData(templateId) {
            for (var i = 0; i < $scope.EntityTempConditionResult.length; i++) {
                if ($scope.EntityTempConditionResult[i].ID == templateId) {
                    $scope.EntityTempConditionResult[i].Caption = $scope.TempCaption;
                    $scope.EntityTempConditionResult[i].Description = $scope.TempDescription;
                }
            }
        }


        //----   GET THE STATUS BASED ON ENTITYTYPE ID-----//
        $scope.getStatus = function (changeType, ConditionObj, Template) {

            if ($scope.EntityTempConditionResult.length > 0) {
                var templateWithCondition = $.grep($scope.EntityTempConditionResult, function (e) { return e.conditions.length > 0 });
                var filteredTemplate = $.grep(templateWithCondition, function (e) { return e.conditions[0].EntityTypeId == Template.conditions[0].EntityTypeId });
                var templateIndex = $scope.EntityTempConditionResult.indexOf(Template);;
                if (filteredTemplate.length > 1) {

                    $scope.EntityTempConditionResult[templateIndex].conditions[0].EntityTypeId = 0;
                    if ($scope.EntityTempConditionResult[templateIndex].conditions.length > 1) {
                        $scope.EntityTempConditionResult[templateIndex].conditions[0].EntityTypeId = Template.conditions[1].EntityTypeId;
                    }
                    bootbox.alert($translate.instant('LanguageContents.Res_5728.Caption'));
                    return false;
                }

            }

            var TemplateObj = $.grep($scope.EntityTempConditionResult, function (e) {
                return e.ID == Template.ID;
            })[0];

            //TemplateObj.avilableStatus = "";
            TemplateObj.availableAttributes = "";
            TemplateObj.avilableDays = $scope.addDays;
            TemplateObj.editableEntityType = TemplateObj.conditions[0].ID;

            if (changeType == 'EntityType') {

                for (var i = 0; i < TemplateObj.conditions.length; i++) {
                    TemplateObj.conditions[i].EntityTypeId = ConditionObj.EntityTypeId;
                    TemplateObj.conditions[i].AttrID = ConditionObj.AttrID;
                    if (ConditionObj.AttrID == 0 || ConditionObj.AttrID == null || ConditionObj.AttrID == undefined) {
                        TemplateObj.conditions[i].AttrID = 0
                    }

                }
                AdminService.GetEntityTypeStatus(ConditionObj.EntityTypeId).then(function (res) {
                    $scope.EntityStatusOptions = res.Response;

                    TemplateObj.avilableStatus = $scope.EntityStatusOptions;
                });

            }
            if (changeType == 'attribute') {
                for (var i = 0; i < TemplateObj.conditions.length; i++) {
                    TemplateObj.conditions[i].AttrID = ConditionObj.AttrID;
                }
            }
            var availableAttributes = $.grep($scope.EntityTypes, function (e) {
                return e.ID == ConditionObj.EntityTypeId;
            });


            if (availableAttributes != null && availableAttributes != undefined) {
                TemplateObj.availableAttributes = availableAttributes[0].AttrInfo;

            }
        }

        /********** Validation to Select Days *******/

        $scope.setDays = function (ConditionObj, Template) {

            var Index = Template.conditions.indexOf(ConditionObj);
            if (Index > 0) {
                var previousCondtionDays = parseInt(Template.conditions[Index - 1].Days);
            }
            if (Index < Template.conditions.length - 1) {
                var nextConditionDays = parseInt(Template.conditions[Index + 1].Days);
            }
            if (previousCondtionDays != undefined) {
                if (ConditionObj.Days <= previousCondtionDays) {
                    if (previousCondtionDays < 7) {
                        Template.conditions[Index].Days = previousCondtionDays + 1;
                    }
                    else if (previousCondtionDays < 14) {
                        Template.conditions[Index].Days = 14;
                    }
                    else if (previousCondtionDays < 30) {
                        Template.conditions[Index].Days = 30;
                    }
                    else if (previousCondtionDays < 60) {
                        Template.conditions[Index].Days = 60;
                    }
                    else if (previousCondtionDays < 90) {
                        Template.conditions[Index].Days = 90;
                    }
                    else if (previousCondtionDays < 180) {
                        Template.conditions[Index].Days = 180;
                    }

                    bootbox.alert($translate.instant('LanguageContents.Res_5729.Caption'));
                }
            }
            else if (nextConditionDays != undefined) {

                if (ConditionObj.Days >= nextConditionDays) {
                    if (nextConditionDays > 180) {
                        Template.conditions[Index].Days = 180;
                    }
                    else if (nextConditionDays > 90) {
                        Template.conditions[Index].Days = 90;
                    }
                    else if (nextConditionDays > 60) {
                        Template.conditions[Index].Days = 60;
                    }
                    else if (nextConditionDays > 30) {
                        Template.conditions[Index].Days = 30;
                    }
                    else if (nextConditionDays > 14) {
                        Template.conditions[Index].Days = 14;
                    }
                    else if (nextConditionDays < 7) {
                        Template.conditions[Index].Days = nextConditionDays - 1;
                    }
                    bootbox.alert($translate.instant('LanguageContents.Res_5730.Caption'));
                }
            }

        }

        ///-------------- DELETE THE TEMPLATE ------------------//

        $scope.DeleteTemplate = function (templateId) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2016.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteEntityTemplateListById(templateId).then(function (res) {
                            if (res != null && res.Response != false) {
                                DeleteRefreshTemplteData(templateId);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }
        function DeleteRefreshTemplteData(templateId) {
            for (var i = 0; i < $scope.EntityTempConditionResult.length; i++) {
                if ($scope.EntityTempConditionResult[i].ID == templateId) {
                    $scope.EntityTempConditionResult.splice(i, 1);
                }
            }
        }


        /************* Add New Condition ************/

        $scope.addCondition = function (templateObj) {

            var contionCount = templateObj.conditions.length;
            var newCondition = {};
            newCondition.ID = 0;
            newCondition.EntityTypeId = templateObj.conditions[0].EntityTypeId;
            newCondition.AttrID = templateObj.conditions[0].AttrID;
            newCondition.Days = parseInt(templateObj.conditions[contionCount - 1].Days) + 1;
            newCondition.OverallStatus = 0;
            newCondition.tempID = templateObj.conditions.length
            newCondition.TemplateID = templateObj.ID;
            newCondition.edit = false;

            var TemplateObj = $.grep($scope.EntityTempConditionResult, function (e) {
                return e.ID == templateObj.ID;
            })[0];

            var index = $scope.EntityTempConditionResult.indexOf(TemplateObj);

            $scope.EntityTempConditionResult[index].conditions.push(newCondition);
        }

        /************* Delete Existing Condition ************/

        $scope.removeCondition = function (conditionObj, templateID) {

            bootbox.confirm($translate.instant('LanguageContents.Res_5731.Caption'), function (result) {
                if (result) {

                    var srcCon = "";
                    var TemplateObj = $.grep($scope.EntityTempConditionResult, function (e) {
                        return e.ID == templateID;
                    })[0];

                    if (conditionObj.ID != 0) {
                        deleteConditionList.push(conditionObj.ID);
                        srcCon = $.grep(TemplateObj.conditions, function (e) {
                            return e.ID == conditionObj.ID;
                        })[0];
                    }
                    else {
                        srcCon = $.grep(TemplateObj.conditions, function (e) {
                            return e.tempID == conditionObj.tempID;
                        })[0];
                    }


                    var Index = TemplateObj.conditions.indexOf(srcCon);

                    //$timeout(
                    //    (TemplateObj.conditions.splice(Index, 1),
                    //    TemplateObj.editableEntityType = TemplateObj.conditions[0].ID),
                    //    20);

                    $timeout(function () {
                        (TemplateObj.conditions.splice(Index, 1),
                        TemplateObj.editableEntityType = TemplateObj.conditions[0].ID)
                    }, 20);


                }
            })
        }

        /*********** Save/Update Fulfilled Condition *********/

        $scope.SaveEntityFulfilment = function (templateID) {

            var TemplateObj = $.grep($scope.EntityTempConditionResult, function (e) {
                return e.ID == templateID;
            })[0];

            var emptyStatus = $.grep(TemplateObj.conditions, function (e) {
                return (e.OverallStatus == 0 || e.OverallStatus == null || e.OverallStatus == undefined)
            })

            if (TemplateObj.conditions[0].EntityTypeId == 0 || TemplateObj.conditions[0].EntityTypeId == null || TemplateObj.conditions[0].EntityTypeId == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
            }
            else if (TemplateObj.conditions[0].AttrID == 0 || TemplateObj.conditions[0].AttrID == null || TemplateObj.conditions[0].AttrID == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_5732.Caption'));
            }
            else if (emptyStatus.length > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_5733.Caption'));
            }
            else {

                var updateCondtion = {};
                updateCondtion.SaveorUpdate = TemplateObj.conditions;
                updateCondtion.delete = deleteConditionList;

                AdminService.UpdateEntityTypeOverallStatus(updateCondtion).then(function (result) {
                    if (result.Response) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4802.Caption'));
                    }
                    else {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    }
                })
            }
        }

        /******** Create New/First Conditon for the Template ********/

        $scope.createNewCondition = function (templateObj) {

            var newCondition = {};
            newCondition.ID = 0;
            newCondition.EntityTypeId = 0;
            newCondition.AttrID = 0;
            newCondition.Days = 0;
            newCondition.OverallStatus = 0;
            newCondition.tempID = templateObj.conditions.length;
            newCondition.TemplateID = templateObj.ID;

            var TemplateObj = $.grep($scope.EntityTempConditionResult, function (e) {
                return e.ID == templateObj.ID;
            })[0];

            TemplateObj.editableEntityType = 0;

            var index = $scope.EntityTempConditionResult.indexOf(TemplateObj);

            $scope.EntityTempConditionResult[index].conditions.push(newCondition);

        }


    }
    //);
    app.controller("mui.admin.entitystatusCtrl", ['$scope', '$location', '$resource', '$timeout', '$compile', '$translate', 'AdminService', muiadminentitystatusCtrl]);
})(angular, app);