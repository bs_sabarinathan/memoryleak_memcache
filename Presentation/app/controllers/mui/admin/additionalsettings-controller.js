﻿(function (ng, app) {
    "use strict";

    function muiadminadditionalsettingsCntrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        AdminService.GetEmailIds().then(function (getemailid) {
            $scope.adminemailid = getemailid.Response;
        });
        $scope.saveadminemail = function () {
            var adminsettings = {};
            adminsettings.emailids = $scope.adminemailid;
            AdminService.InsertAdminSettingXML(adminsettings).then(function (saveadminSettings) {
                if (saveadminSettings.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption')); 

                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        $scope.DateFormatsList = [{
            id: 1,
            name: "dd-mm-yyyy"
        }, {
            id: 2,
            name: "d-m-yyyy"
        }, {
            id: 3,
            name: "mm-dd-yyyy"
        }, {
            id: 4,
            name: "m-d-yyyy"
        }, {
            id: 5,
            name: "yyyy-mm-dd"
        }, {
            id: 6,
            name: "yyyy-m-d"
        }, {
            id: 7,
            name: "dd/mm/yyyy"
        }, {
            id: 8,
            name: "d/m/yyyy"
        }, {
            id: 9,
            name: "mm/dd/yyyy"
        }, {
            id: 10,
            name: "m/d/yyyy"
        }, {
            id: 11,
            name: "yyyy/mm/dd"
        }, {
            id: 12,
            name: "yyyy/m/d"
        }]
        $scope.Timelist = [{
            id: 1,
            name: "00:00"
        }, {
            id: 2,
            name: "00:30"
        }, {
            id: 3,
            name: "01:00"
        }, {
            id: 4,
            name: "01:30"
        }, {
            id: 5,
            name: "02:00"
        }, {
            id: 6,
            name: "02:30"
        }, {
            id: 7,
            name: "03:00"
        }, {
            id: 8,
            name: "03:30"
        }, {
            id: 9,
            name: "04:00"
        }, {
            id: 10,
            name: "04:30"
        }, {
            id: 11,
            name: "05:00"
        }, {
            id: 12,
            name: "05:30"
        }, {
            id: 13,
            name: "06:00"
        }, {
            id: 14,
            name: "06:30"
        }, {
            id: 15,
            name: "07:00"
        }, {
            id: 16,
            name: "07:30"
        }, {
            id: 17,
            name: "08:00"
        }, {
            id: 18,
            name: "08:30"
        }, {
            id: 19,
            name: "09:00"
        }, {
            id: 20,
            name: "09:30"
        }, {
            id: 21,
            name: "10:00"
        }, {
            id: 22,
            name: "10:30"
        }, {
            id: 23,
            name: "11:00"
        }, {
            id: 24,
            name: "11:30"
        }, {
            id: 25,
            name: "12:00"
        }, {
            id: 26,
            name: "12:30"
        }, {
            id: 27,
            name: "13:00"
        }, {
            id: 28,
            name: "13:30"
        }, {
            id: 29,
            name: "14:00"
        }, {
            id: 30,
            name: "14:30"
        }, {
            id: 31,
            name: "15:00"
        }, {
            id: 32,
            name: "15:30"
        }, {
            id: 33,
            name: "16:00"
        }, {
            id: 34,
            name: "16:30"
        }, {
            id: 35,
            name: "17:00"
        }, {
            id: 36,
            name: "17:30"
        }, {
            id: 37,
            name: "18:00"
        }, {
            id: 38,
            name: "18:30"
        }, {
            id: 39,
            name: "19:00"
        }, {
            id: 40,
            name: "19:30"
        }, {
            id: 41,
            name: "20:00"
        }, {
            id: 42,
            name: "20:30"
        }, {
            id: 43,
            name: "21:00"
        }, {
            id: 44,
            name: "21:30"
        }, {
            id: 45,
            name: "22:00"
        }, {
            id: 42,
            name: "22:30"
        }, {
            id: 43,
            name: "23:00"
        }, {
            id: 44,
            name: "23:30"
        }, ]
        $scope.Time = "00:00";
        $scope.CurrencyFormatsList = [];
        $scope.UserID = $cookies['UserId'];
        AdminService.GetAllCurrencyType().then(function (CurrencyListResult) {
            AdminService.GetAdditionalSettings().then(function (GetAdditionalresult) {
                if (GetAdditionalresult.StatusCode == 200) {
                    $scope.DateFormat = GetAdditionalresult.Response[0].SettingValue;
                } else {
                    $scope.DateFormat = "yyyy-mm-dd";
                }
            });
        });
        $scope.UpdateFormat = function (FormatType) {
            var Formatparms = {};
            Formatparms.Settingname = FormatType;
            Formatparms.ID = 1;
            if (FormatType == 'DateFormat')
                Formatparms.settingValue = $scope.DateFormat;
            else
                Formatparms.settingValue = $('#ddlcurrencyformat option:selected').val();
            AdminService.InsertUpdateAdditionalSettings(Formatparms).then(function (Result) {
                if (Result.StatusCode == 200) {
                    AdminService.GetAdditionalSettings().then(function (GetAdditionalresult) {
                        if (GetAdditionalresult.StatusCode == 200) {
                            $scope.DefaultSettings.DateFormat = GetAdditionalresult.Response[0].SettingValue;
                            for (var i = 0; i < GetAdditionalresult.Response[1].CurrencyFormatvalue.length; i++) {
                                $scope.DefaultSettings.CurrencyFormat.Id = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Id;
                                $scope.DefaultSettings.CurrencyFormat.Name = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].ShortName;
                                $scope.DefaultSettings.CurrencyFormat.ShortName = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Name;
                                $scope.DefaultSettings.CurrencyFormat.Symbol = GetAdditionalresult.Response[1].CurrencyFormatvalue[i].Symbol;
                            }
                        }
                    });
                    NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption') + FormatType);
                }
            });
        }
        $scope.UpdatePopularTagWords = function () {
            var updateindata = {};
            updateindata.TotalTagWords = $scope.PopularTagWords;
            AdminService.UpdatePopularTagWordsToShow(updateindata).then(function (updatedata) {
                if (updatedata.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        $scope.UpdateExpirytime = function () {
            var objexpirytime = {};
            objexpirytime.Time = $scope.Time;
            AdminService.UpdateExpirytime(objexpirytime).then(function (updateexpirytime) {
                if (updateexpirytime.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        getexpirytime();

        function getexpirytime() {
            AdminService.Getexpirytime().then(function (Expirytimeresult) {
                if (Expirytimeresult.StatusCode == 200) {
                    $scope.Time = Expirytimeresult.Response;
                }
            });
        }
        AdminService.GetTotalPopularTagWordsToShow().then(function (res) {
            if (res.StatusCode == 200) {
                if (res.Response != null && res.Response != 0)
                    $scope.PopularTagWords = res.Response;
            }
        });
        $('#notifytimeid').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if ((keycode >= 48 && keycode <= 57)) {
                return true;
            } else {
                return false;
            }
        });
        AdminService.GetTotalPopularTagWordsToShow().then(function (result) {
            if (result.StatusCode == 200) {
                if (result.Response != null && result.Response != 0)
                    $scope.PopularTagWords = result.Response;
            }
        });
        $scope.savenotifytimesetbyuser = function () {
            var adminsettings = {};
            adminsettings.notifydisplaytime = $scope.notifytimeid;
            AdminService.InsertNotifydisplaytime(adminsettings).then(function (saveadminSettings) {
                if (saveadminSettings.StatusCode == 200) {
                    Notifystatementsdisplaytime = $scope.notifytimeid;
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        getnotifytimesetbyuser();

        function getnotifytimesetbyuser() {
            AdminService.Getnotifytimesetbyuser().then(function (objGetnotifytimesetbyuser) {
                if (objGetnotifytimesetbyuser.Response != "false") {
                    $scope.notifytimeid = objGetnotifytimesetbyuser.Response;
                } else
                    $scope.notifytimeid = Notifystatementsdisplaytime;
            });
        }
        Getnotifyrecycletimesetbyuser();

        function Getnotifyrecycletimesetbyuser() {
            AdminService.Getnotifyrecycletimesetbyuser().then(function (objGetnotifytimesetbyuser) {
                if (objGetnotifytimesetbyuser.Response != "false") {
                    $scope.notifycycletimeid = objGetnotifytimesetbyuser.Response;
                } else
                    $scope.notifycycletimeid = Notifycyclestatementsdisplaytime;
            });
        }
        $scope.savenotifycycletimesetbyuser = function () {
            var adminsettings = {};
            adminsettings.notifydisplaytime = $scope.notifycycletimeid;
            AdminService.InsertNotifycycledisplaytime(adminsettings).then(function (saveadminSettings) {
                if (saveadminSettings.StatusCode == 200) {
                    Notifycyclestatementsdisplaytime = $scope.notifycycletimeid;
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        AdminService.GetDecimalSettingsValue().then(function (Decimalresult) {
            if (Decimalresult.Response != null && Decimalresult.Response.length > 0) {
                $scope.DecimalValueForFinancial = Decimalresult.Response[0];
                $scope.DecimalValueForObjective = Decimalresult.Response[2];
            } else {
                $scope.DecimalValueForFinancial = 0;
                $scope.DecimalValueForObjective = 0;
            }
        });
        $scope.SetDecimalPlaces = function (val) {
            var decimalvals = {};
            decimalvals.deciamlplaces = val == 'financial' ? $scope.DecimalValueForFinancial : $scope.DecimalValueForObjective;
            decimalvals.pagename = val == 'financial' ? 'financial' : 'objective';
            AdminService.InsertUpdateDecimalSettings(decimalvals).then(function (res) {
                if (res.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    AdminService.GetDecimalSettingsValue().then(function (Decimalresult) {
                        if (Decimalresult.Response != null && Decimalresult.Response.length > 0) {
                            $scope.DecimalSettings['FormatMoney'].Financial_FormatMoney = parseInt(Decimalresult.Response[0]);
                            $scope.DecimalSettings['FinancialAutoNumeric'].vMax = Decimalresult.Response[1];
                            $scope.DecimalSettings['FormatMoney'].Objective_FormatMoney = parseInt(Decimalresult.Response[2]);
                            $scope.DecimalSettings['ObjectiveAutoNumeric'].vMax = Decimalresult.Response[3];
                            if (parseInt(Decimalresult.Response[0]) > 0) {
                                if (parseInt(Decimalresult.Response[0]) == 1)
                                    $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0.0';
                                $scope.DecimalSettings['FinancialAutoNumeric'].aDec = ".";
                            } else
                                $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0';
                            if (parseInt(Decimalresult.Response[2]) > 0) {
                                if (parseInt(Decimalresult.Response[2]) == 1)
                                    $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0.0';
                                $scope.DecimalSettings['FinancialAutoNumeric'].aDec = ".";
                            } else
                                $scope.DecimalSettings['ObjectiveAutoNumeric'].vMin = '0';
                        }
                    });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        $scope.saveDefaultFolderInfo = function () {
            var defaultFolderObj = {};
            defaultFolderObj.IsEnable = $scope.IsdefaultfolderEnable;
            defaultFolderObj.FolderName = $scope.defaultFolderName;
            defaultFolderObj.FolderDescription = $scope.defaultFolderDesc;
            AdminService.updateDefaultFolderInfo(defaultFolderObj).then(function (result) {
                if (result.Response) {
                    NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            })
        }
        $scope.SetnewsFeedConfig = function (event, ID) {
            var NewsFeedConfig = {};
            NewsFeedConfig.ID = ID;
            var checkbox = event.target;
            if (checkbox.checked)
                NewsFeedConfig.IsSelected = 1;
            else
                NewsFeedConfig.IsSelected = 0;
            AdminService.UpdateNewFeedconfig(NewsFeedConfig).then(function (result) {
                if (result.Response == true)
                    NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                else
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
            });
        }
        $scope.checkEditornewsFeedConfig = false;
        $scope.checkownernewsFeedConfig = false;
        AdminService.GetNewsFeedConfigInfo().then(function (result) {
            if (result.Response != null) {
                var newsFeedConfigInfo = result.Response;
                if (newsFeedConfigInfo[0] == 1)
                    $scope.checkownernewsFeedConfig = true;
                else
                    $scope.checkownernewsFeedConfig = false;
                if (newsFeedConfigInfo[1] == 1)
                    $scope.checkEditornewsFeedConfig = true;
                else
                    $scope.checkEditornewsFeedConfig = false;
            }
        });
        AdminService.getDefaultFolderInfo().then(function (DefaultFolderInfo) {
            if (DefaultFolderInfo.Response != null) {
                $scope.IsdefaultfolderEnable = DefaultFolderInfo.Response[0];
                $scope.defaultFolderName = DefaultFolderInfo.Response[1];
                $scope.defaultFolderDesc = DefaultFolderInfo.Response[2];
            }
        })
        AdminService.GetNavigationType().then(function (getstartpage) {
            var optionhtml = '';
            for (var i = 0; i < getstartpage.Response.length; i++) {
                if (getstartpage.Response[i].Featureid == 16 && getstartpage.Response[i].Caption == "Administration")
                    optionhtml += '<option value="' + "Administration" + '">' + getstartpage.Response[i].Caption + ' </option>';
                else
                    optionhtml += '<option value="' + getstartpage.Response[i].Featureid + '">' + getstartpage.Response[i].Caption + ' </option>';
            }
            $("#addSingleSelection").html(optionhtml);

            AdminService.GetUserById($scope.UserID).then(function (mypageuserdetails) {
                $scope.startpageid = mypageuserdetails.Response.StartPage;
            });
        });

        $scope.changeStartPage = function () {
            var updatestartpage = {};
            updatestartpage.ID = $scope.startpageid.toNumber();
            if ($scope.startpageid == "Administration") {
                updatestartpage.ID = 16;
            }
            else {
                updatestartpage.ID = $scope.startpageid;
            }

            AdminService.Updateadditionalsettingsstartpage(updatestartpage).then(function (updatestartpageresult) {
                if (updatestartpageresult.Response == true)
                    NotifySuccess($translate.instant('LanguageContents.Res_4769.Caption'));
                else
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
            });
        };

        /**** Cache Related Settings ****/

        $scope.metadataApplyStatus = 1; //Set status as locked
        AdminService.getmetadataLockStatus().then(function (result) {
            if (!result.Response)
                $scope.metadataApplyStatus = 0; //set status as unlocked
        })

        $scope.setmetadataLockStatus = function () {
            AdminService.setmetadataLockStatus($scope.metadataApplyStatus == 1).then(function (result) {
                if (result.Response != null)
                    NotifySuccess("Successfully changed metadatalock status");
                else
                    NotifyError("Failed to change metadatalock status");
            })
        }

        $scope.clearCache = function clearCache() {
            bootbox.confirm('If you clear the cache all logged in user will get logout', function (result) {
                if (result) {
                    AdminService.clearCache().then(function (res) {
                        if (res.Response != null)
                            NotifySuccess("cleared Cache");
                        else
                            NotifyError("Failed to clear Cache");
                    })
                }
            })
        }


    }
    app.controller("mui.admin.additionalsettingsCntrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminadditionalsettingsCntrl]);
})(angular, app);