﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Tenants
{
    public class TenantSelection
    {
        public TenantSelection()
        {
            //do nothing
        }

        public string GetTenantFilePath(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@ID='" + TenantID + "']");
            return xn["FilePath"].InnerText;
        }

        public string GetTenantFilePathByHostName(string TenantHost)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            return xn["FilePath"].InnerText;
        }
    }
}
