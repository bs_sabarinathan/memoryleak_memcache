﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{
    internal class PublishAccess :IPublishAccess
    {
        #region Member Variables

        protected int _id;
        protected int _role;
        protected bool _accesspermission;
        #endregion

        #region Constructors

        public PublishAccess() { }


        public PublishAccess(int pID, int pRole)
		{
			this._id = pID;
            this._role = pRole; 
		}


        public PublishAccess(bool pAccesspermission)
		{
            this._accesspermission = pAccesspermission; 
		}

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Role
        {
            get { return _role; }
            set
            {
                if (value != null )
                    throw new ArgumentOutOfRangeException("Role", "Role value, cannot contain more than 50 characters");
                _role = value;
            }

        }

        public bool AccessPermission
        {
            get { return _accesspermission; }
            set
            {
                if (value != null )
                    throw new ArgumentOutOfRangeException("Accesspermission", "Accesspermission value, cannot contain more than 4000 characters");
                _accesspermission = value;
            }

        }


        #endregion 

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Role castObj = null;
            try
            {
                castObj = (Role)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
		
        
    }
}
