﻿using System;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Dal.Access.Model;

namespace BrandSystems.Marcom.Dal.DAM.Model
{

    /// <summary>
    /// object for table 'DAM_Asset'.
    /// </summary>

    public partial class AssetsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _folderid;
        protected string _name;
        protected int _activefileid;
        protected int _assettypeid;
        protected DateTime _createdon;
        protected int _createdby;
        protected int _status;
        protected bool _active;
        protected string _assetaccess;
        protected int _category;
        protected bool _ispublish;
        protected string _url;
        protected int _linkedassetid;
        protected int publisherID;
        protected DateTime? _publishedOn;
        protected DateTime _updatedon;
        protected DateTime? _expiryDate;
        protected bool _isTemplate;

        #endregion

        #region Constructors
        public AssetsDao() { }

        public AssetsDao(int pEntityid, string pName, int pActiveFileid, int pAssetTypeid, DateTime pCreatedon, int pFolderid, int pCreatedBy, int pStatus, bool pActive, string pAssetAccess, int pCategory, bool pIsPublish, string purl, int pLinkedAssetID, DateTime pUpdatedOn, int pPublisherID, DateTime? pPublishedOn, DateTime? pexpiryDate, bool pIsTemplate)
        {
            this._entityid = pEntityid;
            this._name = pName;
            this._activefileid = pActiveFileid;
            this._assettypeid = pAssetTypeid;
            this._createdon = pCreatedon;
            this._folderid = pFolderid;
            this._createdby = pCreatedBy;
            this._status = pStatus;
            this._active = pActive;
            this._assetaccess = pAssetAccess;
            this._category = pCategory;
            this._ispublish = pIsPublish;
            this._url = purl;
            this._linkedassetid = pLinkedAssetID;
            this._updatedon = pUpdatedOn;
            this._publishedOn = pPublishedOn;
            this.publisherID = pPublisherID;
            this._expiryDate = pexpiryDate;
            this._isTemplate = pIsTemplate;
        }

        public AssetsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set {  _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
            
                _name = value;
            }

        }

    

        public virtual int ActiveFileID
        {
            get { return _activefileid; }
            set {  _activefileid = value; }

        }

       
        public virtual int FolderID
        {
            get { return _folderid; }
            set { _folderid = value; }

        }


        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _createdon = value; }

        }
        public virtual int CreatedBy
        {
            get { return _createdby; }
            set { _createdby = value; }

        }
        public virtual int AssetTypeid
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public virtual int Status
        {
            get { return _status; }
            set { _status = value; }

        }


        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual string AssetAccess
        {
            get { return _assetaccess; }
            set { _assetaccess = value; }
        }

        public virtual int Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public virtual bool IsPublish
        {
            get { return _ispublish; }
            set { _ispublish = value; }
        }
        public virtual string Url
        {
            get { return _url; }
            set { _url = value; }

        }
        public virtual int LinkedAssetID
        {
            get { return _linkedassetid; }
            set { _linkedassetid = value; }
        }


        public virtual DateTime UpdatedOn
        {
            get { return _updatedon; }
            set { _updatedon = value; }

        }

        public virtual int PublisherID
        {
            get { return publisherID; }
            set { publisherID = value; }

        }


        public virtual DateTime? PublishedOn
        {
            get { return _publishedOn; }
            set { _publishedOn = value; }

        }

        public virtual DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }

        }

        public virtual bool IsTemplate
        {
            get { return _isTemplate; }
            set { _isTemplate = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetsDao = "AssetsDao";
            public const string ID = "ID";
            public const string FolderID = "FolderID";
            public const string EntityID = "EntityID";
            public const string Name = "Name";
            public const string ActiveFileID = "ActiveFileID";
            public const string AssetTypeid = "AssetTypeid";
            public const string Createdon = "Createdon";
            public const string CreatedBy = "CreatedBy";
            public const string Status = "Status";
            public const string Active = "Active";
            public const string AssetAccess = "AssetAccess";
            public const string Category = "Category";
            public const string IsPublish = "IsPublish";
            public const string Url = "Url";
            public const string LinkedAssetID = "LinkedAssetID";
            public const string UpdatedOn = "UpdatedOn";
            public const string PublisherID = "PublisherID";
            public const string PublishedOn = "PublishedOn";
            public const string ExpiryDate = "ExpiryDate";
            public const string IsTemplate = "IsTemplate";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetsDao = "AssetsDao";
            public const string ID = "ID";
            public const string FolderID = "FolderID";
            public const string EntityID = "EntityID";
            public const string Name = "Name";
            public const string ActiveFileID = "ActiveFileID";
            public const string AssetTypeid = "AssetTypeid";
            public const string Createdon = "Createdon";
            public const string CreatedBy = "CreatedBy";
            public const string Status = "Status";
            public const string Active = "Active";
            public const string AssetAccess = "AssetAccess";
            public const string Category = "Category";
            public const string IsPublish = "IsPublish";
            public const string Url = "Url";
            public const string LinkedAssetID = "LinkedAssetID";
            public const string UpdatedOn = "UpdatedOn";
            public const string PublisherID = "PublisherID";
            public const string PublishedOn = "PublishedOn";
            public const string ExpiryDate = "ExpiryDate";
            public const string IsTemplate = "IsTemplate";
        }

        #endregion


    }

}
