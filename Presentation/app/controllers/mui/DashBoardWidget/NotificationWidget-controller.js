﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetNotificationWidgetCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $window, NotificationwidgetService) {
        $scope.FilterFields = [];
        $scope.NotificationWidgetContentID = $scope.WizardID;
        $timeout(function () { LoadNotification(); }, 100);

        function LoadNotification() {
            $scope.Allnotifications = [];
            $scope.UnreadNotification = '';
            NotificationwidgetService.GetNotification(0).then(function (getnotification) {
                $scope.Allnotifications = getnotification.Response.m_Item1;
                $('#' + $scope.NotificationWidgetContentID + '').parents('.box-content').find('.InProgress').remove();
            });
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.NotificationWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.NotificationWidgetCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$window', 'NotificationwidgetService', muiDashBoardWidgetNotificationWidgetCtrl]);
    function NotificationwidgetService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetNotification: GetNotification
        });
        function GetNotification(flag) { var request = $http({ method: "get", url: "api/common/GetNotification/" + flag, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("NotificationwidgetService", ['$http', '$q', NotificationwidgetService]);
})(angular, app);