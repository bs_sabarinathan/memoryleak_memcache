﻿(function (ng, app) {
    "use strict"; function muiadmintreesettingsCtrl($scope, $resource, $location, $translate, AdminService) {
        var availableattrs = []; $scope.filterOptions = { filterText: '' }; var allEntityTypes = []; var allAttributes = []; $scope.EntityTypeData = []; $scope.entitydatavalues = []; $scope.attributedatavalues = []; var AllEntityTypes = []; $scope.EntityData; $scope.entitytype = 6; var AdminLogoSettings = 'Tree'; AdminService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (res) {
            $scope.entitytpesdata = res.Response; $scope.EntityData = res.Response; AdminService.GetAdminSettings(AdminLogoSettings, 6).then(function (getattribtuesResponse) {
                var res = JSON.parse(getattribtuesResponse.Response); if (res.Tree.EntityTypes != null) { $scope.attributedatavalues = res.Tree.EntityTypes.EntityType; var attribtueslength = res.Tree.EntityTypes.EntityType.length; }
                else {
                    $('#sortableAttribute2').html(''); var Attrhtml = ''; for (var i = 0; i < $scope.EntityData.length; i++) { Attrhtml += '<li data-Id="' + $scope.EntityData[i].Id + '" data-DisplayName="' + $scope.EntityData[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + $scope.EntityData[i].Caption + '</a></li>'; }
                    $('#sortableAttribute1').html(Attrhtml);
                }
                var Attrhtml2 = ''; if (attribtueslength != undefined) {
                    for (var b = 0; b < attribtueslength; b++) {
                        var EntitytypeValues = $.grep($scope.EntityData, function (e) { return e.Id == res.Tree.EntityTypes.EntityType[b]["@ID"]; }); if (EntitytypeValues.length != 0)
                            Attrhtml2 += '<li data-Id="' + EntitytypeValues[0].Id + '" data-DisplayName="' + EntitytypeValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + EntitytypeValues[0].Caption + '</a></li>';
                    }
                    $('#sortableAttribute2').html(Attrhtml2);
                }
                else {
                    var AttrValues = $.grep($scope.EntityData, function (e) { return e.Id == res.Tree.EntityTypes.EntityType["@ID"]; }); if (AttrValues.length != 0)
                        Attrhtml2 += '<li data-Id="' + AttrValues[0].Id + '" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; if ($scope.entitytype == 5) { Attrhtml2 += '<li data-Id="5" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; }
                        else if ($scope.entitytype == 10) { Attrhtml2 += '<li data-Id="10" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; }
                    $('#sortableAttribute2').html(Attrhtml2);
                }
                var Attrhtml = ''; for (var i = 0; i < $scope.EntityData.length; i++) {
                    var AttrValues; if (res.Tree.EntityTypes.EntityType.length == undefined) {
                        if (res.Tree.EntityTypes.EntityType["@ID"] == $scope.EntityData[i].Id) { AttrValues.length = 1; }
                        else { AttrValues.length = 0; }
                    }
                    else { AttrValues = $.grep(res.Tree.EntityTypes.EntityType, function (e) { return e["@ID"] == $scope.EntityData[i].Id; }); }
                    if (AttrValues.length == 0) { Attrhtml += '<li data-Id="' + $scope.EntityData[i].Id + '" data-DisplayName="' + $scope.EntityData[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + $scope.EntityData[i].Caption + '</a></li>'; availableattrs.push($scope.EntityData[i]); }
                }
                $('#sortableAttribute1').html(Attrhtml);
            });
        }); $("#sortableAttribute2").droppable({ drop: function (event, ui) { var dropeedid = ui.helper[0].attributes["data-id"].value; var attrDelObj = $.grep(availableattrs, function (e) { return e.Id == dropeedid; }); availableattrs.splice($.inArray(attrDelObj[0], availableattrs), 1); } }); $("#sortableAttribute1").droppable({ drop: function (event, ui) { var dropeedid = ui.helper[0].attributes["data-id"].value; var attrAddObj = $.grep($scope.EntityData, function (e) { return e.Id == dropeedid; })[0]; availableattrs.push(attrAddObj); } }); $scope.FilterAvailableAttrs = function () {
            var search = ''; var id = $("#sortableAttribute1 >li").attr("data-id"); var ids = []; if (availableattrs.length == 0) { $.each($("#sortableAttribute1 >li"), function (i, el) { ids.push($(el).attr("data-id")); availableattrs.push($.grep($scope.EntityData, function (e) { return e.Id == parseInt($(el).attr("data-id")); })[0]); }); }
            var searchresult = $.grep(availableattrs, function (e) { return e.Caption.toLowerCase().contains($scope.filterOptions.filterText.toLowerCase()); }); $('#sortableAttribute1').html(''); var Attrhtml = ''; for (var i = 0; i < searchresult.length; i++) {
                if (searchresult[i].AttributeTypeID == 6 || searchresult[i].AttributeTypeID == 12) { var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.DisplayName == searchresult[i].Caption; }); Attrhtml += '<li data-Id="' + searchresult[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + searchresult[i].Level + '"  data-Type="' + searchresult[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + searchresult[i].Id + '" data-DisplayName="' + searchresult[i].Caption + '"  data-IsSpecial="' + searchresult[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + searchresult[i].Caption + '</a></li>'; }
                else { var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == searchresult[i].Id; }); Attrhtml += '<li data-Id="' + searchresult[i].Id + '" data-IsOrderBy="false" data-IsSelect="true" data-IsFilter="true" data-Level="' + searchresult[i].Level + '" data-Type="' + searchresult[i].AttributeTypeID + '" data-WhereCondition="" data-Field="' + searchresult[i].Id + '" data-DisplayName="' + searchresult[i].Caption + '"  data-IsSpecial="' + searchresult[i].IsSpecial + '" class="active DragableItem"><a href="JavaScript: void(0);">' + searchresult[i].Caption + '</a></li>'; }
            }
            $('#sortableAttribute1').html(Attrhtml);
        }
        $scope.loadattributes = function () {
            availableattrs = []; $scope.filterOptions.filterText = ''; if ($scope.entitytype == 0) { $('#sortableAttribute2').html(''); $('#sortableAttribute1').html(''); return true; }
            AdminService.GettingEntityTypeHierarchyForAdminTree($scope.entitytype, 3).then(function (res) {
                $scope.entitytpesdata = res.Response; $scope.EntityData = res.Response; AdminService.GetAdminSettings(AdminLogoSettings, $scope.entitytype).then(function (getattribtues) {
                    var res = JSON.parse(getattribtues.Response); if (res.Tree.EntityTypes != null) { $scope.attributedatavalues = res.Tree.EntityTypes.EntityType; var attribtueslength = res.Tree.EntityTypes.EntityType.length; }
                    else {
                        $('#sortableAttribute2').html(''); var Attrhtml = ''; for (var i = 0; i < $scope.EntityData.length; i++) { Attrhtml += '<li data-Id="' + $scope.EntityData[i].Id + '" data-DisplayName="' + $scope.EntityData[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + $scope.EntityData[i].Caption + '</a></li>'; }
                        $('#sortableAttribute1').html(Attrhtml);
                    }
                    var Attrhtml2 = ''; if (attribtueslength != undefined) {
                        for (var b = 0; b < attribtueslength; b++) {
                            var EntitytypeValues = $.grep($scope.EntityData, function (e) { return e.Id == res.Tree.EntityTypes.EntityType[b]["@ID"]; }); if (EntitytypeValues.length != 0)
                                Attrhtml2 += '<li data-Id="' + EntitytypeValues[0].Id + '" data-DisplayName="' + EntitytypeValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + EntitytypeValues[0].Caption + '</a></li>';
                        }
                        $('#sortableAttribute2').html(Attrhtml2);
                    }
                    else {
                        var AttrValues = $.grep($scope.EntityData, function (e) { return e.Id == res.Tree.EntityTypes.EntityType["@ID"]; }); if (AttrValues.length != 0 && res.Tree.EntityTypes.EntityType["@ID"] != "5" && res.Tree.EntityTypes.EntityType["@ID"] != "10")
                            Attrhtml2 += '<li data-Id="' + AttrValues[0].Id + '" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; if ($scope.entitytype == 5) { Attrhtml2 += '<li data-Id="5" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; }
                            else if ($scope.entitytype == 10) { Attrhtml2 += '<li data-Id="10" data-DisplayName="' + AttrValues[0].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + AttrValues[0].Caption + '</a></li>'; }
                        $('#sortableAttribute2').html(Attrhtml2);
                    }
                    var Attrhtml = ''; for (var i = 0; i < $scope.EntityData.length; i++) {
                        var AttrValues; if (res.Tree.EntityTypes.EntityType.length == undefined) {
                            if (res.Tree.EntityTypes.EntityType["@ID"] == $scope.EntityData[i].Id) { AttrValues.length = 1; }
                            else { AttrValues.length = 0; }
                        }
                        else { AttrValues = $.grep(res.Tree.EntityTypes.EntityType, function (e) { return e["@ID"] == $scope.EntityData[i].Id; }); }
                        if (AttrValues.length == 0) { Attrhtml += '<li data-Id="' + $scope.EntityData[i].Id + '" data-DisplayName="' + $scope.EntityData[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + $scope.EntityData[i].Caption + '</a></li>'; availableattrs.push($scope.EntityData[i]); }
                    }
                    $('#sortableAttribute1').html(Attrhtml);
                });
            });
        }
        $(function () { $("#sortableAttribute1, #sortableAttribute2").sortable({ connectWith: ".connectedAttributeSortable", appendTo: 'body', containment: 'window', scroll: false, helper: 'clone' }).disableSelection(); }); $scope.save = function () {
            var entitytype = 6; if ($scope.entitytype != undefined) { entitytype = $scope.entitytype; }
            var Treesettings = 'Tree'; var rootlevelfilterSettings = {}; rootlevelfilterSettings.Key = Treesettings; rootlevelfilterSettings.TypeId = entitytype; rootlevelfilterSettings.Tree = { "EntityTypes": [], }; var EntityArray = { "EntityType": [] }; for (var i = 0; i < $("#sortableAttribute2").find('li').length; i++) { var EntityType = {}; EntityArray.EntityType.push(EntityType); EntityArray.EntityType[i]['@ID'] = $("#sortableAttribute2").find('li').eq(i).attr("data-Id"); }
            rootlevelfilterSettings.Tree.EntityTypes.push(EntityArray); rootlevelfilterSettings.View = { Tree: rootlevelfilterSettings.Tree }
            AdminService.AdminSettingsforRootLevelInsertUpdate(rootlevelfilterSettings).then(function () { NotifySuccess($translate.instant('LanguageContents.Res_4802.Caption')); });
        };
    }
    app.controller("mui.admin.treesettingsCtrl", ['$scope', '$resource', '$location', '$translate', 'AdminService', muiadmintreesettingsCtrl]);
})(angular, app);