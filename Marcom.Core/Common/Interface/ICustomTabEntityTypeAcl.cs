﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface ICustomTabEntityTypeAcl
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int CustomTabID
        {
            get;
            set;

        }

        int EntityTypeID
        {
            get;
            set;

        }

        int GlobalRoleID
        {
            get;
            set;

        }

        #endregion
    }
}
