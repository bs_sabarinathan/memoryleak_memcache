﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalReport.aspx.cs" Inherits="Presentation.External.HistoryReport" %>

<!DOCTYPE html>

<html>
<head>
    <title>Document Viewer</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script>
        function post_to_url(url, iframe, params) {
            //Create form to make post request for SSO authentication.
            var form = document.createElement('form');
            form.action = url;
            form.method = 'POST';
            form.target = iframe; //Set Iframe as the target

            //Add all the required parameter for the SSO authentication.
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            //Submit the form
            form.submit();
            //Look for iframe load event. Once the authentication is performed then we need to redirect for Document view page.
        }

        function iframeLoad(obj) {
            obj.width = window.innerWidth;
            obj.height = window.innerHeight - 4;

            if ($('#an_iframe').attr('src') == '<%= LogoutAction %>') {
                //Do nothing just ignore the logout operation to make sure that the previous session will get expire.
                $('#an_iframe').attr('src', '<%= LoginUrl %>');
                post_to_url('<%= LoginUrl %>', 'an_iframe',
              { 'DDMSAction': 'LoginAction', 'action': 'login', 'User/Name': '<%= UserName %>', 'User/Password': '<%= UserPassword %>', 'User/Role': '<%= UserRole %>' });
            }
            else if ($('#an_iframe').attr('src') != '<%= AfterLoginUrl %>') {
                $('#an_iframe').attr('src', '<%= AfterLoginUrl %>');
            } else {
                $('#ajaxloading').hide();
                $('#an_iframe').show();
            }
    }
    </script>
    <style>
        .loadIcon-large {
            font-family: sans-serif;
            text-align: center;
            font-size: 36px;
            color: #999;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, 0.6);
            z-index: 999;
            position: absolute;
            top: 0;
            left: 0;
        }

            .loadIcon-large > span {
                position: absolute;
                top: 50%;
                left: 0;
                width: 100%;
                text-align: center;
            }

                .loadIcon-large > span > img {
                    width: 50px;
                    margin-right: 10px;
                    vertical-align: middle;
                }
    </style>
</head>

<body style="margin: 0;">
    <form id="Form1" runat="server">
        <iframe id="an_iframe" name="an_iframe" style="border: none; display: none;"
            onload="iframeLoad(this);" src="<%= LogoutAction %>"></iframe>

        <div class="loadIcon-large" id="ajaxloading">
            <span>
                <img alt="Loading" src="../assets/img/loading.gif">
                Loading...
            </span>
        </div>

    </form>
</body>
</html>
