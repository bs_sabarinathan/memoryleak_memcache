﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Marcom.API.Services
{
    public class ExceptionHandlingAttribute: ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
                        
            if (context.Exception is NotImplementedException)
            {
                System.Web.HttpContext.Current.Response.Headers.Add("X-ErrorHeader", "Error Occured");
                System.Web.HttpContext.Current.Response.Headers.Add("X-ErrorMessage", "Not Implemented");
                System.Web.HttpContext.Current.Response.Headers.Add("X-ErrorDeveloperMessage", "This API is still not implemented. Please contact the administrator.");

                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = HttpStatusCode.NotImplemented;
                responseMessage.ReasonPhrase = "Not Implemented";
                responseMessage.Content = new StringContent("This API is still not implemented. Please contact the administrator.");
                throw new HttpResponseException(responseMessage);
            }

            if (context.Exception is UnauthorizedAccessException)
            {
                //log error logic
                HttpResponseMessage responseMessage = new HttpResponseMessage();
                responseMessage.StatusCode = HttpStatusCode.Unauthorized;
                responseMessage.ReasonPhrase = "Un-Authorized";
                responseMessage.Content = new StringContent("This request is denied due to Un-Authorized access.");
                throw new HttpResponseException(responseMessage);
            }

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                //log error logic
                Content = new StringContent("An error occurred, please try again or contact the administrator."),
                ReasonPhrase = "Critical Exception"
            });
           
        }
    }
}