﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ExpireHandler.Interface
{
   public interface IExpireActionSourcedatas
    {
       #region Public Properties

       int Id
       {
           get;
           set;
       }
       int ActionSourceID
       {
           get;
           set;
       }
       int AttributeID
       {
           get;
           set;
       }
       int AttributeTypeID
       {
           get;
           set;
       }
       string AttributeCaption
       {
           get;
           set;
       }
       string NodeID
       {
           get;
           set;
       }
       int Level
       {
           get;
           set;
       }
       int SplValue
       {
           get;
           set;
       }
       bool Ispublish
       {
           get;
           set;
       }
       string  Taskinputvalue
       {
           get;
           set;
       }
       #endregion
    }
}
