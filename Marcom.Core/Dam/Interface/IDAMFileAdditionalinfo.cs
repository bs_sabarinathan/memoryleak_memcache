﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.DAM.Interface
{
    public interface IDAMFileAdditionalinfo
    {
        #region Public Properties

        int Height
        {
            get;
            set;

        }
        int Width
        {
            get;
            set;

        }
        int Dpi
        {
            get;
            set;
        }
        #endregion
    }
}
