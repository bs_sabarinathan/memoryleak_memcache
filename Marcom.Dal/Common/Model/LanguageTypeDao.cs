using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// FeedCommentDao object for table 'CM_LanguageType'.
    /// </summary>
    public partial class LanguageTypeDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected int _inheritedfrom;
        protected string _addedon;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected string _langKey;
        protected string _langContent;
        protected int _translationPending;

        #endregion

        #region Constructors
        public LanguageTypeDao() { }

        public LanguageTypeDao(string pName, string pDescription, int pInheritedFrom, string pAddedOn, string pLangKey, string pLangContent, int pTranslationPending)
        {
            this._name = pName;
            this._description = pDescription;
            this._inheritedfrom = pInheritedFrom;
            this._addedon = pAddedOn;
            this._langKey = pLangKey;
            this._langKey = pLangContent;
            this._translationPending = pTranslationPending;
        }


        public LanguageTypeDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 1000 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }
        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1000 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }
        }

        public virtual int InheritedFrom
        {
            get { return _inheritedfrom; }
            set
            {
                _bIsChanged |= (_inheritedfrom != value); _inheritedfrom = value;
            }
        }

        public virtual string AddedOn
        {
            get { return _addedon; }
            set { _addedon = value; }
        }

        public virtual string LangKey
        {
            get { return _langKey; }
            set { _langKey = value; }
        }

        public virtual string LangContent
        {
            get { return _langContent; }
            set { _langContent = value; }
        }

        public virtual int TranslationPending
        {
            get { return _translationPending; }
            set { _translationPending = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string LanguageType = "LanguageType";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string InheritedFrom = "InheritedFrom";
            public const string AddedOn = "AddedOn";
            public const string LangKey = "LangKey";
            public const string LangContent = "LangContent";
            public const string TranslationPending = "TranslationPending";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string LanguageType = "LanguageType";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string InheritedFrom = "InheritedFrom";
            public const string AddedOn = "AddedOn";
            public const string LangKey = "LangKey";
            public const string LangContent = "LangContent";
            public const string TranslationPending = "TranslationPending";
        }

        #endregion


    }

}
