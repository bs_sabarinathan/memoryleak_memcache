﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IPurchaseOrder interface for table 'PM_PurchaseOrder'.
    /// </summary>
    public interface IApprovedAllocatedDetails
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        int CostCentreID
        {
            get;
            set;

        }

        DateTime PlanDate
        {
            get;
            set;

        }

        decimal PlanAmount
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }


        int CurrencyType
        {
            get;
            set;

        }


        int UserID
        {
            get;
            set;

        }

        string RequesterName { get; set; }
        string currencytypeName { get; set; }
        string strPlanDate { get; set; }
        string strAmount { get; set; }

        #endregion
    }
}
