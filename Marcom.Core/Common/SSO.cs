﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    public class SSO : ISSO
    {
        public string key { get; set; }
        public string IV { get; set; }
        public IList<string> Algorithmoption { get; set; }
        public string AlgorithmValue { get; set; }
        public IList<string> PaddingModeoption { get; set; }
        public IList<BrandSystems.Marcom.Core.Access.GlobalRole> UserGroupsoption { get; set; }
        public string UserGroupsvalue { get; set; }
        public string PaddingModeValue { get; set; }
        public string ClientIntranetUrl { get; set; }

        public IList<string> CipherModeoption { get; set; }
        public string CipherModeValue { get; set; }

        public string SSOTimeDifference { get; set; }
        public string SSODefaultGroups { get; set; }
        public IList<string> SSOTokenModeoption { get; set; }
        public string SSOTokenModeValue { get; set; }
        public IList SSOAccessRoles { get; set; }
        public string globalaccessRole { get; set; }
        public List<int> UserglobalAccess { get; set; }
        public List<string> SAMLroles { get; set; }


    }

}
