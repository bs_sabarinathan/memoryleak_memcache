﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class HolidayDetailsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _holidayname;
        protected DateTime _holidaydate;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public HolidayDetailsDao() { }

        public HolidayDetailsDao(int ID, string HolidayName, DateTime HolidayDate)
        {
            this._id = ID;
            this._holidayname = HolidayName;
            this._holidaydate = HolidayDate;


        }
        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string HolidayName
        {
            get { return _holidayname; }
            set { _bIsChanged |= (_holidayname != value); _holidayname = value; }

        }

        public virtual DateTime HolidayDate
        {
            get { return _holidaydate; }
            set { _bIsChanged |= (_holidaydate != value); _holidaydate = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }
        public virtual string sunday { get; set; }

        public virtual string monday { get; set; }

        public virtual string tuesday { get; set; }

        public virtual string wednesday { get; set; }

        public virtual string thursday { get; set; }

        public virtual string friday { get; set; }

        public virtual string saturday { get; set; }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string HolidayDetailsDao = "HolidayDetailsDao";
            public const string HolidayName = "HolidayName";
            public const string HolidayDate = "HolidayDate";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string HolidayDetailsDao = "HolidayDetailsDao";
            public const string HolidayName = "HolidayName";
            public const string HolidayDate = "HolidayDate";

        }
        #endregion
    }
   

}
