(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultlistCtrl($scope, $timeout, $http, $compile, $resource, $location, $window, $cookies, $cookieStore, $translate, PlanlistService, $modal) {
        $scope.Isfromcalender_mui = false;
        $scope.PageSize = 1;
        $scope.IsSelectAllChecked = false;
        $.cookie('ListofEntityID', new Array());
        $scope.DynamicHeight = $(window).height() - 100;
        $scope.IsDuplicate = false;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.SelectedFilterID = 0;
        $scope.AddEditFilter = "Add new filter";
        PlanlistService.GetFilterSettings(6).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        setTimeout(function () {
            $("#rootlevelfilter").trigger('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
        }, 200);
        $scope.DetailFilterCreation = function (event) {
            $scope.IsDuplicate = false;
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $scope.deletefiltershow = false;
                $("#rootlevelfilter").trigger('ClearScope', $scope.EntityTypeID);
            } else {
                setTimeout(function () {
                    $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 6]);
                }, 20);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow")
            }
        };
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $scope.ApplyFilter = function (filterid, filtername) {
            $scope.IsDuplicate = false;
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
            } else {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            $("#rootlevelfilter").trigger('ApplyRootLevelFilter', [filterid, filtername]);
        }
        $("#Div_list").on('ReloadFilterSettings', function (event, TypeID, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [filterid, 6]);
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            PlanlistService.GetFilterSettings(TypeID).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
            });
        });
        $("#Div_list").on('ClearAndReApply', function () {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = "Add new filter";
        });
        $scope.Level = "0";
        PlanlistService.GetEntityTypeByID(6).then(function (name) {
            if (name.Response != null) {
                $scope.activityname = name.Response[0].Caption;
                $scope.rootentitytypename = $scope.activityname + "s";
            }
        });
        $scope.FilterID = {
            selectedFilterID: 0,
            selectedfilterattribtues: []
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };
        var TypeID = 6;
        $scope.EntityTypeID = 6;
        $scope.sortOrder = "null";
        $scope.IsDesc = false;
        var CurrentuserId = $scope.UserId;
        $scope.listColumnDefsdata = {};
        $scope.NameExcludeFilter = function (columndefs) {
            if (columndefs.Field != "68") {
                return true;
            }
        };
        $scope.load = function (parameters) { };
        if (ShowAllRemember.Activity == true) {
            $scope.DisablePart = true;
            CurrentuserId = $scope.UserId;
            $("#btnParticipated").removeClass("active").addClass("active");
            $("#btnViewAll").removeClass("active");
        } else {
            $scope.DisablePart = false;
            CurrentuserId = 0;
            $("#btnViewAll").removeClass("active").addClass("active");
            $("#btnParticipated").removeClass("active");
        }
        $scope.tglHide = true;
        $scope.ShowAll = function (IsActive) {
            $scope.IsDuplicate = false;
            $scope.DisablePart = !$scope.DisablePart;
            $("#ListContainer table").html('');
            $("#treeHolder ul").html('');
            $('#RootLevelSelectAll').next('i').removeClass('checked');
            ClearCheckBoxes()
            if (ShowAllRemember.Activity == true) {
                CurrentuserId = $scope.UserId
            }
            if (IsActive) {
                $("#btnViewAll").removeClass("active").addClass("active");
                $("#btnParticipated").removeClass("active");
                CurrentuserId = 0;
                ShowAllRemember.Activity = false;
            } else {
                $("#btnParticipated").removeClass("active").addClass("active");
                $("#btnViewAll").removeClass("active");
                CurrentuserId = $scope.UserId;
                ShowAllRemember.Activity = true;
            }
            GetRootLevelActivityListCount();
            if ($("#ListColumn > thead tr th").length == 1)
                $('#ListColumn > thead').html("");
        };

        function ClearCheckBoxes() {
            $scope.IsSelectAllChecked = false;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = false;
                $(this).next('i').removeClass('checked');
            });
        }
        $scope.DeleteEntity = function () {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var data = {};
            data.Planlistids = IDList.join(",");
            PlanlistService.GetApprvdAlloc(data).then(function (result) {
                $scope.ApprovedAlloc = result.Response;
                if (parseInt($scope.ApprovedAlloc) > 0) {
                    bootbox.alert("You cannot able to delete entity due to amount has already approved for this entity.");
                } else {
                    $scope.IsDuplicate = false;
                    var IDList = new Array();
                    var ID = 0;
                    IDList = GetRootLevelSelectedAll();
                    if (IDList.length != 0) {
                        var object = {};
                        object.ID = IDList;
                        var lockavailablitiy = {};
                        lockavailablitiy.ID = IDList;
                        PlanlistService.IsLockAvailable(lockavailablitiy).then(function (lock) {
                            if (lock.Response == true) {
                                bootbox.alert($translate.instant('LanguageContents.Res_4915.Caption'));
                                return true;
                            }
                            PlanlistService.MemberAvailable(object).then(function (memberavailability) {
                                if (memberavailability.Response == false) {
                                    bootbox.alert($translate.instant('LanguageContents.Res_2100.Caption'));
                                    return true;
                                }
                                $scope.alert = $translate.instant('LanguageContents.Res_16.Caption');
                                bootbox.confirm($scope.alert, function (result) {
                                    if (result) {
                                        setTimeout(function () {
                                            var deletesearch = {};
                                            deletesearch.ID = IDList;
                                            PlanlistService.DeleteEntity(deletesearch).then(function (result) {
                                                if (result.Response == true) {
                                                    for (var i = 0; i < IDList.length; i++) {
                                                        var entityId = IDList[i];
                                                        $('#treeHolder a[data-id=' + entityId + ']').remove();
                                                        $('#ListContainer tr[data-id=' + entityId + ']').remove();
                                                    }
                                                    $('#RootLevelSelectAll').next('i').removeClass('checked');
                                                    ClearCheckBoxes();
                                                }
                                            });
                                        }, 100);
                                    }
                                });
                            });
                        });
                    }
                }
            });
        }
        $scope.DuplicateRootLevelEntities = function () {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length != 0) {
                var lockavailablitiy = {};
                lockavailablitiy.ID = IDList;
                PlanlistService.IsLockAvailable(lockavailablitiy).then(function (lock) {
                    if (lock.Response == true) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4916.Caption'));
                        return true;
                    }
                    $('#DuplicateModels').modal('show');
                    $scope.DuplicateEntity(IDList, 0, "LIST");
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1914.Caption'))
            }
        }
        $scope.$on('LISTEVENT', function (event) {
            $scope.IsDuplicate = true;
            GetRootLevelActivityListCount();
        });

        function SelectEntityIDs() {
            var IDList = new Array();
            $('#listdataHolder input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        var IsDesc = false;
        $scope.FilterID.selectedFilterID = 0;
        $scope.FilterID.selectedfilterattribtues = [];
        var sortOrder = "";
        $scope.LoadRootLevelActivity = function (PageIndex, StartRowNo, MaxNoofRow, filterid, filterattributes) {
            var FilterID = filterid;
            var planAttrs = $cookieStore.get('planAttrs' + parseInt($cookies['UserId'], 10));
            var planFilterID = $cookieStore.get('planFilterID' + parseInt($cookies['UserId'], 10));
            var planFilterName = $cookieStore.get('planFilterName' + parseInt($cookies['UserId'], 10));
            RemeberPlanFilterName = planFilterName;
            if (planAttrs != undefined) {
                RememberPlanFilterAttributes = planAttrs;
            }
            if (planFilterID != undefined) {
                RememberPlanFilterID = planFilterID;
            }
            if (RememberPlanFilterID != 0 || RememberPlanFilterAttributes.length != 0) {
                FilterID = RememberPlanFilterID;
                filterattributes = RememberPlanFilterAttributes;
                if (RememberPlanFilterAttributes.length > 0) { }
                if (RememberPlanFilterID != 0) {
                    $scope.appliedfilter = RemeberPlanFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = FilterID;
                }
            }
            if (StartRowNo == 0)
                setTimeout(function () {
                    $("#rootlevelfilter").trigger('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
                }, 100);
            $(window).AdjustHeightWidth();
            $scope.FilterID.selectedFilterID = FilterID;
            $scope.FilterID.selectedfilterattribtues = filterattributes;
            $window.ActivityFilterName = '';
            var TypeID = 6;
            var getactivities = {};
            getactivities.StartRowNo = StartRowNo;
            getactivities.MaxNoofRow = MaxNoofRow;
            getactivities.FilterID = FilterID;
            getactivities.SortOrderColumn = $scope.sortOrder;
            getactivities.IsDesc = $scope.IsDesc;
            getactivities.IncludeChildren = false;
            getactivities.EntityID = '0';
            getactivities.UserID = CurrentuserId;
            getactivities.Level = $scope.Level;
            getactivities.IsDuplicate = $scope.IsDuplicate;
            getactivities.IDArr = [];
            getactivities.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            PlanlistService.ActivityRootLevel(getactivities).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var colStatus = false;
                        for (var i = 0; i < listContent.length; i++) {
                            if (i != undefined) {
                                contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                        if (colStatus == false) {
                                            columnHtml += "<th>";
                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                        else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null || "          " ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }
                                treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                var status = "";
                                if (listContent[i].IsLock == true) {
                                    contentnHtml += "<td><span class='pull-right'><i class='icon-lock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                } else {
                                    contentnHtml += "<td><span class='pull-right'><i class='icon-unlock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                }
                                contentnHtml += "</tr>";
                                colStatus = true;
                            }
                        }
                        if (PageIndex == 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                            $('#ListContainer').scrollTop(0);
                        }
                        if ($("#ListColumn > thead tr").length < 1) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));
                }
            });
        }
        GetRootLevelActivityListCount();

        function GetRootLevelActivityListCount() {
            var planAttrs = $cookieStore.get('planAttrs' + parseInt($cookies['UserId'], 10));
            var planFilterID = $cookieStore.get('planFilterID' + parseInt($cookies['UserId'], 10));
            var planFilterName = $cookieStore.get('planFilterName' + parseInt($cookies['UserId'], 10));
            RemeberPlanFilterName = planFilterName;
            if (planAttrs != undefined) {
                RememberPlanFilterAttributes = planAttrs;
            }
            if (planFilterID != undefined) {
                RememberPlanFilterID = planFilterID;
            }
            if (RememberPlanFilterID != 0 || RememberPlanFilterAttributes.length != 0) {
                $scope.FilterID.selectedFilterID = RememberPlanFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberPlanFilterAttributes;
                if (RememberPlanFilterAttributes.length > 0) { }
                if (RememberPlanFilterID != 0) {
                    $scope.appliedfilter = RemeberPlanFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = $scope.FilterID.selectedFilterID;
                }
            }
            $scope.noData = false;
            $window.ActivityFilterName = '';
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = $scope.FilterID.selectedFilterID;
            Node.SortOrderColumn = $scope.sortOrder;
            Node.IsDesc = $scope.IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = CurrentuserId;
            Node.Level = $scope.Level;
            Node.IsDuplicate = $scope.IsDuplicate;
            Node.IDArr = [];
            Node.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            PlanlistService.ActivityRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response != null) {
                    if (getactivitylist.Response.Data != null) {
                        fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                        var PageIndex = 0;
                        if (getactivitylist.Response.ColumnDefs.length > 0) {
                            var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                            var listContent = getactivitylist.Response.Data;
                            var contentnHtml = "";
                            var columnHtml = "<tr >";
                            var treeHtml = "";
                            var colStatus = false;
                            for (var i = 0; i < listContent.length; i++) {
                                if (i != undefined) {
                                    contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                    for (var j = 0; j < listColumnDefsdata.length; j++) {
                                        if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                            if (colStatus == false) {
                                                columnHtml += "<th>";
                                                columnHtml += "<a data-Column=" + listColumnDefsdata[j].Field + " >";
                                                columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                                columnHtml += "</a>";
                                                columnHtml += "</th>";
                                            }
                                            if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                            else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type) {
                                                if (listContent[i][listColumnDefsdata[j].Field] == "-")
                                                    contentnHtml += '<td><span>' + "-" + '</span></td>';
                                                else
                                                    contentnHtml += '<td><span>' + (listContent[i][listColumnDefsdata[j].Field] == (null || "          ") ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                                            } else
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                        }
                                    }
                                    treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                    var status = "";
                                    if (listContent[i].IsLock == true) {
                                        contentnHtml += "<td><span class='pull-right'><i class='icon-lock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    } else {
                                        contentnHtml += "<td><span class='pull-right'><i class='icon-unlock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    }
                                    contentnHtml += "</tr>";
                                    colStatus = true;
                                }
                            }
                            if ($("#ListColumn > thead tr").length < 1) {
                                columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                                columnHtml += "</tr>";
                                $('#ListColumn > thead').html(columnHtml);
                            }
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            $('#ListContainer').scrollTop(0);
                            if ($("#ListColumn > thead tr").length > 0) {
                                $scope.noData = false;
                                $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                                $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                                $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                                $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                                ListCheckBoxitemClick();
                            }
                            if ($scope.PageSize > 1) {
                                $scope.LoadRootLevelActivity(1, 20, 20, $scope.FilterID.selectedFilterID, $scope.AppllyFilterObj.selectedattributes);
                            }
                        }
                        if (getactivitylist.Response != null) {
                            if (getactivitylist.Response.Data.length == 0) {
                                $scope.noData = true;
                            }
                        } else {
                            $scope.noData = true;
                        }
                    }
                } else {
                    $scope.noData = true;
                }
                $(window).AdjustHeightWidth();
            });
        }
        $("#ListContainer").click(function (e) {
            var targetcontrol = $(e.target);
            if (targetcontrol[0].tagName == "I") {
                var entityID = targetcontrol.attr('data-id');
                var IsMemberAvailable;
                if (entityID != undefined) {
                    PlanlistService.CheckForMemberAvailabilityForEntity(entityID).then(function (getperiodavail) {
                        if (getperiodavail.Response == true) {
                            IsMemberAvailable = true;
                        } else {
                            IsMemberAvailable = false;
                        }
                        if (IsMemberAvailable == false) {
                            bootbox.alert(LanguageContents.Res_1980.Caption);
                            return false;
                        }
                        var LockStatus = 0;
                        if (targetcontrol[0].className == 'icon-lock icon-large' || targetcontrol[0].className == 'icon-large icon-lock') {
                            LockStatus = 0;
                        } else if (targetcontrol[0].className == 'icon-unlock icon-large' || targetcontrol[0].className == 'icon-large icon-unlock') {
                            LockStatus = 1;
                        }
                        var IsLock = targetcontrol.attr('data-IsLock');
                        if (LockStatus == 1) {
                            PlanlistService.PendingFundRequest(entityID).then(function (pendingrequest) {
                                var ispending = pendingrequest.Response;
                                if (ispending == true) {
                                    bootbox.confirm($translate.instant('LanguageContents.Res_2041.Caption'), function (result) {
                                        if (result) {
                                            setTimeout(function () {
                                                PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) { });
                                                if (LockStatus == 1) {
                                                    targetcontrol.removeClass("icon-unlock");
                                                    targetcontrol.addClass("icon-lock");
                                                } else if (LockStatus == 0) {
                                                    targetcontrol.removeClass("icon-lock");
                                                    targetcontrol.addClass("icon-unlock");
                                                }
                                            }, 100);
                                        }
                                    });
                                } else {
                                    PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) {
                                        var lockstatus = updatelock.Response;
                                    });
                                    if (LockStatus == 1) {
                                        targetcontrol.removeClass("icon-unlock");
                                        targetcontrol.addClass("icon-lock");
                                    } else if (LockStatus == 0) {
                                        targetcontrol.removeClass("icon-lock");
                                        targetcontrol.addClass("icon-unlock");
                                    }
                                }
                            });
                        } else {
                            PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) {
                                var lockstatus = updatelock.Response;
                            });
                            if (LockStatus == 1) {
                                targetcontrol.removeClass("icon-unlock");
                                targetcontrol.addClass("icon-lock");
                            } else if (LockStatus == 0) {
                                targetcontrol.removeClass("icon-lock");
                                targetcontrol.addClass("icon-unlock");
                            }
                        }
                    });
                }
            }
        });
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            var PageSize = 1;
            var itemsPerPage = 20;
            var height = 34 * ItemCnt;
            if (ItemCnt > itemsPerPage) {
                height = 34 * itemsPerPage;
                PageSize = Math.ceil(ItemCnt / itemsPerPage);
            }
            $scope.PageSize = PageSize;
            var TreeTemplate = '';
            var ListTemplate = '';
            for (var i = 0; i <= PageSize; i++) {
                if (i != undefined) {
                    TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list'></ul></li>"
                    ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"
                }
            }
            if (TreeTemplate.length > 0) {
                $("#ListContainer > table").html(ListTemplate);
                $("#treeHolder").html(TreeTemplate);
                $(window).AdjustHeightWidth();
            }
        }
        $('#ListContainer').scroll(function () {
            $("#treeHolder").scrollTop($(this).scrollTop());
            var areaHeight = $('#ListContainer').height();
            var areaTop = $('#ListContainer').position().top;
            var top = $(this).position().top - areaTop;
            var height = $('#ListContainer table').height();
            $('tbody.pending', '#ListContainer').each(function () {
                var datapage = parseInt($(this).attr('data-page'));
                if ((($(this).height() * datapage) / 3) < $("#treeHolder").scrollTop()) {
                    $('#ListContainer > table tbody[data-page="' + $(this).attr('data-page') + '"]').removeClass('pending');
                    var StartRowNo = datapage * 20;
                    var MaxNoofRow = 20;
                    $(this).removeClass('pending');
                    $(this).removeAttr('style');
                    $scope.AppllyFilterObj.selectedattributes = [];
                    $scope.LoadRootLevelActivity(datapage, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.FilterID.selectedfilterattribtues);
                    $('#treeHolder li[data-page="' + datapage + '"]').removeClass('pending');
                    $('#treeHolder li[data-page="' + datapage + '"]').removeAttr('style');
                }
            });
        });
        $("#ListColumn").click(function (e) {
            var TargetControl = $(e.target);
            $scope.AppllyFilterObj.selectedattributes = [];
            var col = "";
            if (TargetControl[0].tagName == "SPAN") {
                col = TargetControl.parent().attr("data-Column");
            } else if (TargetControl[0].tagName == "A") {
                col = TargetControl.attr("data-Column");
            }
            if (col != "") {
                $scope.sortOrder = col;
                $('#RootLevelSelectAll').next('i').removeClass('checked');
                ClearCheckBoxes();
                if ($scope.sortOrder === col) {
                    $scope.IsDesc = !$scope.IsDesc;
                } else {
                    $scope.sortOrder = col;
                    $scope.IsDesc = false;
                }
                $scope.LoadRootLevelActivity(0, 0, 20, 0, $scope.AppllyFilterObj.selectedattributes)
            }
        });
        $("#rootLevelEntity").click(function (event) {
            $("#rootLevelEntity").trigger("onRootEntityCreation", [$scope.EntityTypeID])
        });
        $window.ListofEntityID = []
        $("#treeHolder").click(function (e) {
            var TargetControl = $(e.target);
            var id = $(e.target).attr("data-id");
            if (id != undefined) {
                $scope.Gnttsettings.StartDate = "";
                $scope.Gnttsettings.EndDate = "";
                $scope.LoadGantHeaderNew(id);
                setTimeout(function () {
                    ViewRootLevelEntity(id, e);
                }, 250);
            }
        });
        $scope.LoadGantHeaderNew = function (id) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            PlanlistService.GetPeriodByIdForGantt(id).then(function (GantResults) {
                if (GantResults.Response.length != null) {
                    for (var i = 0; i < GantResults.Response.length; i++) {
                        datStartUTCval = GantResults.Response[i].Startdate.substr(6, (GantResults.Response[i].Startdate.indexOf('+') - 6));
                        datstartval.push(new Date(parseInt(datStartUTCval)));
                        datEndUTCval = GantResults.Response[i].EndDate.substr(6, (GantResults.Response[i].EndDate.indexOf('+') - 6));
                        datendval.push(new Date(parseInt(datEndUTCval)));
                    }
                    maxstartDates = ConvertDateToString(new Date(Math.min.apply(null, datstartval)));
                    maxendDates = ConvertDateToString(new Date(Math.max.apply(null, datendval)));
                    if (maxstartDates != "")
                        start = maxstartDates;
                    if (maxendDates != "")
                        end = maxendDates;
                    if (end == "NaN-NaN-NaN") {
                        end = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                        end = end.substr(0, 4);
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    var currentYear = new Date().getFullYear();
                    var strtDate = maxstartDates.substring(0, 4);
                    var endingDate = maxendDates.substring(0, 4);
                    if (currentYear < endingDate) {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');;
                    } else {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (end == "NaN-NaN-NaN") {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (start == "NaN-NaN-NaN" || start == "")
                        start = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    if (end == "NaN-NaN-NaN" || end == "")
                        end = Date.parse('+1year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    start = Date.parse(start);
                    end = Date.parse(end);
                    var dates = [];
                    dates[start.getFullYear()] = [];
                    dates[start.getFullYear()][start.getMonth()] = [start];
                    var last = start;
                    while (last.compareTo(end) == -1) {
                        var next = last.clone().addDays(1);
                        if (!dates[next.getFullYear()]) {
                            dates[next.getFullYear()] = [];
                        }
                        if (!dates[next.getFullYear()][next.getMonth()]) {
                            dates[next.getFullYear()][next.getMonth()] = [];
                        }
                        dates[next.getFullYear()][next.getMonth()].push(next);
                        last = next;
                    }
                    $scope.dates = dates;
                    return dates;
                }
            });
        }
        $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function (e) {
            var status = this.checked;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $scope.IsSelectAllChecked = true;
                    $(this).next('i').addClass('checked');
                } else {
                    $scope.IsSelectAllChecked = false;
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $("#ViewSelect").click(function (event) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            $scope.LoadGantHeaderNews(IDList);
            setTimeout(function () {
                ViewRootLevelEntity(null, event)
            }, 250);
        });
        $scope.LoadGantHeaderNews = function (id) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            PlanlistService.GetPeriodByIdForGantt((IDList.length) > 0 ? IDList.join(",") : 0).then(function (GantResults) {
                if (GantResults.Response.length != null) {
                    for (var i = 0; i < GantResults.Response.length; i++) {
                        datStartUTCval = GantResults.Response[i].Startdate.substr(6, (GantResults.Response[i].Startdate.indexOf('+') - 6));
                        datstartval.push(new Date(parseInt(datStartUTCval)));
                        datEndUTCval = GantResults.Response[i].EndDate.substr(6, (GantResults.Response[i].EndDate.indexOf('+') - 6));
                        datendval.push(new Date(parseInt(datEndUTCval)));
                    }
                    maxstartDates = ConvertDateToString(new Date(Math.min.apply(null, datstartval)));
                    maxendDates = ConvertDateToString(new Date(Math.max.apply(null, datendval)));
                    if (maxstartDates != "")
                        start = maxstartDates;
                    if (maxendDates != "")
                        end = maxendDates;
                    if (end == "NaN-NaN-NaN") {
                        end = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                        end = end.substr(0, 4);
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    var currentYear = new Date().getFullYear();
                    var strtDate = maxstartDates.substring(0, 4);
                    var endingDate = maxendDates.substring(0, 4);
                    if (currentYear < endingDate) {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');;
                    } else {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (end == "NaN-NaN-NaN") {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (start == "NaN-NaN-NaN" || start == "")
                        start = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    if (end == "NaN-NaN-NaN" || end == "")
                        end = Date.parse('+1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    start = Date.parse(start);
                    end = Date.parse(end);
                    var dates = [];
                    dates[start.getFullYear()] = [];
                    dates[start.getFullYear()][start.getMonth()] = [start];
                    var last = start;
                    while (last.compareTo(end) == -1) {
                        var next = last.clone().addDays(1);
                        if (!dates[next.getFullYear()]) {
                            dates[next.getFullYear()] = [];
                        }
                        if (!dates[next.getFullYear()][next.getMonth()]) {
                            dates[next.getFullYear()][next.getMonth()] = [];
                        }
                        dates[next.getFullYear()][next.getMonth()].push(next);
                        last = next;
                    }
                    $scope.dates = dates;
                    return dates;
                }
            });
        }

        function LoadDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                event.preventDefault();
                var localScope = $(event.target).scope();
                localScope.$apply(function () {
                    if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                        $location.path("/mui/planningtool/default/detail/listview/" + TrackID);
                    } else {
                        $location.path("/mui/planningtool/default/detail/ganttview/" + TrackID);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function LoadSelectAllDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                    $location.path("/mui/planningtool/default/detail/listview/" + TrackID);
                } else {
                    $location.path("/mui/planningtool/default/detail/ganttview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }

        function ListCheckBoxitemClick() {
            $('#ListContainer > table > tbody input:checkbox').click(function () {
                $("#RootLevelSelectAll").next().removeClass('checked')
                $scope.IsSelectAllChecked = false;
            });
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            if ($scope.IsSelectAllChecked == false) {
                if (ID != null) {
                    IDList.push(ID);
                    LoadDetailPart(event, IDList);
                } else {
                    IDList = GetRootLevelSelectedAll();
                    LoadDetailPart(event, IDList);
                }
            } else {
                $scope.IsSelectAllChecked = false;
                PlanlistService.GetAllEntityIds().then(function (Entityresult) {
                    if (Entityresult.Response != null) {
                        IDList = Entityresult.Response;
                        LoadSelectAllDetailPart(event, IDList);
                    }
                });
            }
        }
        $scope.rootValues = [];
        PlanlistService.GettingEntityForRootLevel(true).then(function (ResultPlansList) {
            if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
                $scope.IsToShowRootEntity = true;
                $scope.rootValues = ResultPlansList.Response;
            } else
                $scope.IsToShowRootEntity = false;
        });
        $scope.openrootentitycreationpopup = function (Id, Caption) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/RootEntityTypeCreation.html',
                controller: "mui.planningtool.RootEntityTypeCreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            'ID': Id,
                            'Caption': Caption
                        };
                    }
                },
                scope: $scope,
                windowClass: 'rootEntityTypeCreationModel popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.listCtrl']"));
        });
        setTimeout(function () {
            $scope.load();
        }, 0);
    }
    app.controller('mui.planningtool.default.listCtrl', ['$scope', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$cookies', '$cookieStore', '$translate', 'PlanlistService', '$modal', muiplanningtooldefaultlistCtrl]);
})(angular, app);
(function (ng, app) {
    "use strict";

    function muiplanningtoolfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, PlanfiltersettingsService) {
        $scope.treeNodeSelectedHolderValues = [];
        $scope.appliedfilter = "No filter applied";
        var IsUpdate = 0;
        var IsSave = 0;
        $scope.deletefiltershow = false;
        $scope.FilterFields = {};
        var TypeID = $scope.EntityTypeID;
        $scope.visible = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.rememberAttrValue = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {};
        $scope.TreePricing = [];
        $scope.treeTexts = {};
        $scope.FilterDataXML = [];
        $("#rootlevelfilter").on('LoadRootLevelFilter', function (event, typeid, data) {
            $scope.rememberAttrValue = data;
            $scope.FilterSettingsLoad();
        });
        var rememberfilter = [];
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilterbutton = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsUpdate = 0;
            IsSave = 0;
        };
        $("#rootlevelfilter").on('ClearScope', function (event, TypeID) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
            }
        });
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearScopeModle = function () {
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationListWithTree, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            IsSave = 0;
            if (TypeID == 6) {
                RememberPlanFilterAttributes = [];
                RememberPlanFilterID = 0;
                $cookieStore.remove('planAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('planFilterName' + parseInt($cookies['UserId'], 10));
            } else {
                RememberCCFilterAttributes = [];
                RememberCCFilterID = 0;
                $cookieStore.remove('CCAttrs' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterID' + parseInt($cookies['UserId'], 10));
                $cookieStore.remove('CCFilterName' + parseInt($cookies['UserId'], 10));
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ddlParententitytypeId = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.ngsaveasfilter = "";
            $scope.ngKeywordtext = "";
            $scope.deletefiltershow = false;
            if ($scope.atributesRelationList != undefined) {
                var treecount = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 7
                });
                for (var i = 0; i < treecount.length; i++) {
                    ClearSavedTreeNode(treecount[i].AttributeId);
                }
                var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeTypeId == 13
                });
                if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                    for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                        if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                    }
                }
            }
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattributes = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $("#Div_list").trigger('ClearAndReApply');
            $("#Div_listCC").trigger('ClearAndReApply');
            if (TypeID == 6) {
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            } else if (TypeID == 5) {
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, filterattributes)
            }
        }
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            if ($scope.dyn_Cont.length == 0) {
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                $scope.EntityHierarchyTypesResult = [];
                $scope.dyn_Cont = '';
                $scope.Updatefilter = false;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $("#dynamic_Controls").html('');
                $scope.atributesRelationList = [];
                $scope.atributesRelationListWithTree = [];
                $scope.ngsaveasfilter = '';
                $scope.ngKeywordtext = '';
                var OptionFrom = 0;
                var IsKeyword = "false";
                var IsEntityType = "false";
                var CurrentActiveVersion = 0;
                var elementNode = 'Filter';
                var KeywordOptionsresponse;
                var CheckingPricicingAttr = [];
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    PlanfiltersettingsService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                        KeywordOptionsresponse = KeywordOptionsResult.Response;
                        OptionFrom = KeywordOptionsresponse.split(',')[0];
                        IsKeyword = KeywordOptionsresponse.split(',')[1];
                        IsEntityType = KeywordOptionsresponse.split(',')[3];
                        CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                        var IDList = new Array();
                        var filterattids = {};
                        filterattids.IDList = IDList;
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'Filter';
                        filterattids.OptionFrom = OptionFrom;
                        PlanfiltersettingsService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                    if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                        if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                            var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                            $scope.tagAllOptionsEntityStatus.data = [];
                                            if (EntityStatusOptions != null) {
                                                $.each(EntityStatusOptions, function (i, el) {
                                                    $scope.tagAllOptionsEntityStatus.data.push({
                                                        "id": el.ID,
                                                        "text": el.StatusOptions,
                                                        "ShortDescription": el.ShortDesc,
                                                        "ColorCode": el.ColorCode
                                                    });
                                                });
                                            }
                                            $scope.dyn_Cont += "<div class='control-group'>";
                                            $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                            $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                            $scope.dyn_Cont += "</div></div>";
                                        } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Name  for ndata in  OntimeStatusLists \"></select></div>";
                                        } else {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption  for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                        if ($scope.atributesRelationList[i].AttributeId != 75 && $scope.atributesRelationList[i].AttributeId != 74) {
                                            $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].OptionValues \"></select></div>";
                                            $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                        }
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"  id=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in  atributesRelationList[" + i + "].LevelTreeNodes \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                        $scope.dyn_Cont += "<select multiple='multiple' ui-select2 data-placeholder='Select " + $scope.atributesRelationList[i].DisplayName + " options' ng-model='FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel + "'\ >";
                                        $scope.dyn_Cont += "<option ng-repeat='ndata in atributesRelationList[" + i + "].LevelTreeNodes' value='{{ndata.Id}}'>{{ndata.Caption}}</option>";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                        var k = $scope.TreePricing.length;
                                        var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                        });
                                        if (treecount12.length == 0) {
                                            var mm = $scope.atributesRelationList[i].AttributeId;
                                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                            $scope.FilterDataXML.push({
                                                "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                            });
                                        }
                                    }
                                }
                                if ($scope.EntityTypeID == 6 && IsEntityType == "True") {
                                    $scope.dyn_Cont += "<div class='control-group'>";
                                    $scope.dyn_Cont += "<span>EntityType : </span>";
                                    $scope.dyn_Cont += "<input id='ddlChildren' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' style='width: 200px;' />";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                    return e.AttributeTypeId == 7
                                })
                                for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                    if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                        $scope.treePreviewObj = {};
                                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                        }
                                        $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                        $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_Cont += '</div></div>';
                                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                        $scope.dyn_Cont += '<div class="controls">';
                                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_Cont += '</div></div></div>';
                                    }
                                }
                                if (IsKeyword == "True") {
                                    $scope.dyn_Cont += "<div class=\"control-group\">";
                                    $scope.dyn_Cont += "<span>Keyword : </span>";
                                    $scope.dyn_Cont += "<input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'>";
                                    $scope.dyn_Cont += "</div>";
                                }
                                $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                                $scope.Applyfilterbutton = false;
                                $scope.Updatefilter = false;
                                $scope.Deletefilter = false;
                                $scope.Savefilter = true;
                            }
                            RememberFilterOnReLoad($scope.rememberAttrValue);
                        });
                    });
                });
            }
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k].id,
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': dataVal[k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            if (orgLevel != undefined) {
                                for (var k = 0; k < orgLevel.length; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = [];
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    whereConditionData = [];
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response]);
                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = FilterData.FilterName;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = FilterData.FilterName;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), FilterData.FilterName);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterId, typeId) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId) {
            $scope.Updatefilter = false;
            $scope.Applyfilterbutton = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.ClearScopeModle();
            $scope.FilterID.selectedFilterID = filterId;
            PlanfiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if ($scope.filterSettingValues.FilterValues.length > 0) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                    return e.AttributeId == parseInt(71);
                                })[0].EntityStatusOptionValues;
                                var seletedoptionvalue = [];
                                for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
									return e.AttributeId == parseInt(71);
                                }).length; sts++) {
                                    seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                        return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                    })[0]);
                                    if (seletedoptionvalue != null) {
                                        $.each(seletedoptionvalue, function (i, el) {
                                            $scope.tagAllOptionsEntityStatus.data.push({
                                                "id": el.ID,
                                                "text": el.StatusOptions,
                                                "ShortDescription": el.ShortDesc,
                                                "ColorCode": el.ColorCode
                                            });
                                        });
                                    }
                                    if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                }
                            } else {
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        }
                    }
                }
                if ($scope.EntityTypeID == 6) {
                    PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                        if (filterSettingsValues.Response.EntityTypeID != "") {
                            for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                                if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                                    if ($.grep($scope.tagAllOptions.data, function (e) {
										return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                        return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                                    })[0]);
                                }
                            }
                            $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                        }
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilterbutton = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            if ($scope.ngsaveasfilter == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            $scope.filterattributes = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                    for (var k = 0; k < dataVal.length; k++) {
                        if ($scope.atributesRelationList[i].AttributeId == 71) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k].id,
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        } else {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': dataVal[k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.EntityTypeID == 6) {
                if ($scope.ddlParententitytypeId != undefined) {
                    if ($scope.ddlParententitytypeId[0] != undefined) {
                        for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                            if (entitytypeIdforFilter == '') {
                                entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                            } else {
                                entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                            }
                        }
                    }
                }
            }
            var FilterData = {};
            FilterData.FilterId = $scope.FilterID.selectedFilterID;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanfiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    $("#Div_list").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, $scope.ngsaveasfilter, $scope.FilterID.selectedFilterID]);
                    if (filterSettingsInsertresult.Response != undefined) {
                        $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    }
                    $scope.FilterID.filterattributes = $scope.filterattributes;
                    if (TypeID == 6) {
                        RememberPlanFilterID = $scope.FilterID.selectedFilterID;
                        RememberPlanFilterAttributes = $scope.filterattributes;
                        RemeberPlanFilterName = $scope.appliedfilter;
                        $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    } else if (TypeID == 5) {
                        RememberCCFilterID = $scope.FilterID.selectedFilterID;
                        RememberCCFilterAttributes = $scope.filterattributes;
                        RemeberCCFilterName = $scope.appliedfilter;
                        $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.filterattributes);
                        $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                        $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                        $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $scope.ApplyFilter = function (FilterID, FilterName) {
            if (FilterID != undefined && FilterID != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                if ($scope.atributesRelationList.length > 0) {
                    if ($scope.atributesRelationList.length == 0) {
                        $scope.atributesRelationListtemp = $scope.atributesRelationList;
                    } else $scope.atributesRelationListtemp = $scope.atributesRelationList;
                }
            }
            if ($scope.atributesRelationList != undefined) if ($scope.atributesRelationList.length == 0) $scope.atributesRelationList = $scope.atributesRelationListtemp;
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var StartDate = '01/01/1990';
            var EndDate = '01/01/1990';
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            if ($scope.atributesRelationList != undefined) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        var dataVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        if (dataVal != undefined) {
                            for (var k = 0; k < dataVal.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k].id,
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': dataVal[k],
                                            'Level': $scope.atributesRelationList[i].TreeLevel,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': ''
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                        var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                        for (var ii = 0; ii < Array; ii++) {
                            if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                                var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                                for (var k = 0; k < j; k++) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                        'Level': ii + 1,
                                        'AttributeTypeId': 13,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            if ($scope.ngKeywordtext != '') whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': $scope.ngKeywordtext
                            });
                            else whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': parseInt(nodeval.id, 10),
                                'Level': parseInt(nodeval.Level, 10),
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'Keyword': ''
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0; k < orgLevel.length; k++) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                        'Level': $scope.atributesRelationList[i].TreeLevel,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': ''
                                    });
                                }
                            }
                        }
                    }
                }
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            } else {
                if (entitytypeIdforFilter != '') {
                    if ($scope.ngKeywordtext != '') {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': 0,
                            'SelectedValue': 0,
                            'Level': 0,
                            'AttributeTypeId': 0,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': ''
                        });
                    }
                }
                if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext
                    });
                }
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            $scope.FilterID.filterattributes = $scope.AppllyFilterObj.selectedattributes;
            if (TypeID == 6) {
                RememberPlanFilterID = FilterID;
                RememberPlanFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberPlanFilterName = $scope.appliedfilter;
                $cookieStore.put('planAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('planFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('planFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            } else if (TypeID == 5) {
                RememberCCFilterID = FilterID;
                RememberCCFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
                RemeberCCFilterName = $scope.appliedfilter;
                $cookieStore.put('CCAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
                $cookieStore.put('CCFilterID' + parseInt($cookies['UserId'], 10), FilterID);
                $cookieStore.put('CCFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
                $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
            $scope.FilterID.filterattributes = [];
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            $scope.filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        PlanfiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4398.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                $("#Div_list").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                $("#Div_listCC").trigger('ReloadFilterSettings', [TypeID, 'No filter applied', 0]);
                                $scope.FilterID.selectedFilterID = 0;
                                $scope.FilterID = {
                                    selectedFilterID: 0
                                };
                                $scope.AppllyFilterObj = {};
                                $scope.ApplyFilter(0, 'No filter applied');
                                if (TypeID == 6) {
                                    $scope.LoadRootLevelActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                } else if (TypeID == 5) {
                                    $scope.LoadRootLevelCostCenterActivity(PageIndex, StartRowNo, MaxNoofRow, 0, $scope.filterattributes)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 6 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            } else if ($scope.EntityTypeID == 5 && RememberPlanFilterAttributes.length != 0) {
                $('.FilterHolder').show()
            }
            if (RememberPlanFilterAttributes.length != 0) {
                $scope.ddlParententitytypeId = [];
                $scope.filterSettingValues.FilterAttributes = RememberPlanFilterAttributes;
                for (var i = 0; i < $scope.filterSettingValues.FilterAttributes.length; i++) {
                    if ($scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 4) {
                        if ($scope.filterSettingValues.FilterAttributes[i].AttributeID == 71) {
                            var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeId == parseInt(71);
                            })[0].EntityStatusOptionValues;
                            var seletedoptionvalue = [];
                            for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterAttributes, function (e) {
								return e.AttributeID == parseInt(71);
                            }).length; sts++) {
                                seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                    return e.id == parseInt($scope.filterSettingValues.FilterAttributes[sts].SelectedValue);
                                })[0]);
                                if (seletedoptionvalue != null) {
                                    $.each(seletedoptionvalue, function (i, el) {
                                        $scope.tagAllOptionsEntityStatus.data.push({
                                            "id": el.ID,
                                            "text": el.StatusOptions,
                                            "ShortDescription": el.ShortDesc,
                                            "ColorCode": el.ColorCode
                                        });
                                    });
                                }
                                if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] = seletedoptionvalue;
                            }
                        } else {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                        }
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterAttributes[i].Level != 0 && $scope.filterSettingValues.FilterAttributes[i].AttributeTypeId == 12) {
                        if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterAttributes[i].AttributeID + "_" + $scope.filterSettingValues.FilterAttributes[i].Level].push($scope.filterSettingValues.FilterAttributes[i].SelectedValue);
                    } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                    }
                }
                PlanfiltersettingsService.GettingEntityForRootLevel(false).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.length == undefined) {
                    if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs != '0') {
                        if ($.grep($scope.tagAllOptions.data, function (e) {
							return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs
                        })[0]);
                    }
                } else {
                    for (var l = 0; l < $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',').length; l++) {
                        if ($scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l] != '0') {
                            if ($.grep($scope.tagAllOptions.data, function (e) {
								return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0] != undefined) $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                                return e.id == $scope.filterSettingValues.FilterAttributes[0].EntityTypeIDs.split(',')[l]
                            })[0]);
                        }
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                $scope.ngKeywordtext = $scope.filterSettingValues.FilterAttributes[0].Keyword;
            }
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.filtersettingsCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            if ($scope.treesrcdirec["Attr_" + attributeid].length != undefined) {
                for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                    branch.ischecked = false;
                    if (branch.Children.length > 0) {
                        ClearRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        };
    }
    app.controller("mui.planningtool.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'PlanfiltersettingsService', muiplanningtoolfiltersettingsCtrl]);
})(angular, app);
(function(ng, app) {
	"use strict";

	function muiplanningtoolRootEntityTypeCreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $translate, $compile, RootentitytypecreationService, $modalInstance, params) {
		var timeoutvariableforentitycreation = {};
		$scope.listofValidations = [];
		$scope.GetNonBusinessDaysforDatePicker();
		$scope.Calanderopen = function($event, object, Call_from) {
			$event.preventDefault();
			$event.stopPropagation();
			if (Call_from == "DueDate") {
				object.StartDate = false;
				object.EndDate = false;
				object.DateAction = false;
				object.DueDate = true;
			} else if (Call_from == "StartDate") {
				object.EndDate = false;
				object.DueDate = false;
				object.DateAction = false;
				object.StartDate = true;
			} else if (Call_from == "EndDate") {
				object.StartDate = false;
				object.DueDate = false;
				object.DateAction = false;
				object.EndDate = true;
			} else {
				object.StartDate = false;
				object.EndDate = false;
				object.DueDate = false;
				object.DateAction = true;
			}
		};
		$scope.PeriodCalanderopen = function($event, item, place) {
			$event.preventDefault();
			$event.stopPropagation();
			if (place == "start") {
				item.calstartopen = true;
				item.calendopen = false;
			} else if (place == "end") {
				item.calstartopen = false;
				item.calendopen = true;
			}
		};
		$scope.treeCategory = [];
		var remValtimer = "";
		$scope.DynamicAddValidation = function(attrID) {
			var IsValExist = $.grep($scope.listValidationResult, function(e) {
				return parseInt(e[0].split('_')[1]) == attrID;
			});
			if (IsValExist == null || IsValExist.length == 0) {
				var getVal = $.grep($scope.listofValidations, function(e) {
					return parseInt(e[0].split('_')[1]) == attrID;
				});
				if (getVal != null && getVal.length > 0) {
					$scope.listValidationResult.push(getVal[0]);
					$("#EntityMetadata").nod().destroy();
					if (remValtimer) $timeout.cancel(remValtimer);
					remValtimer = $timeout(function() {
						$("#EntityMetadata").nod($scope.listValidationResult, {
							'delay': 200,
							'submitBtnSelector': '#btnTemp',
							'silentSubmit': 'true'
						});
					}, 10);
				}
			}
		}
		$scope.DynamicRemoveValidation = function(attrID) {
			if ($scope.listValidationResult != undefined) {
				var IsValExist = $.grep($scope.listValidationResult, function(e) {
					return parseInt(e[0].split('_')[1]) == attrID;
				});
				if (IsValExist != null && IsValExist.length > 0) {
					$scope.listValidationResult = $.grep($scope.listValidationResult, function(e) {
						return parseInt(e[0].split('_')[1]) != attrID;
					});
					$("#EntityMetadata").nod().destroy();
					if (remValtimer) $timeout.cancel(remValtimer);
					remValtimer = $timeout(function() {
						$("#EntityMetadata").nod($scope.listValidationResult, {
							'delay': 200,
							'submitBtnSelector': '#btnTemp',
							'silentSubmit': 'true'
						});
					}, 10);
				}
			}
		}
		$scope.ShowOrHideAttributeToAttributeRelation = function(attrID, attrTypeID) {
			var Attributetypename = '';
			var relationobj = $.grep($scope.listAttriToAttriResult, function(rel) {
				return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
			});
			var ID = attrID.split("_");
			if (relationobj != undefined) {
				if (relationobj.length > 0) {
					for (var i = 0; i <= relationobj.length - 1; i++) {
						Attributetypename = '';
						if (relationobj[i].AttributeTypeID == 3) {
							Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
						} else if (relationobj[i].AttributeTypeID == 4) {
							Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
						} else if (relationobj[i].AttributeTypeID == 6) {
							Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
						} else if (relationobj[i].AttributeTypeID == 7) {
							Attributetypename = 'Tree_' + relationobj[i].AttributeID;
						} else if (relationobj[i].AttributeTypeID == 12) {
							Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
						}
						if ($scope.fields[Attributetypename] == undefined) {
							if (attrTypeID == 1) {
								$scope.fields["TextSingleLine_" + ID[0]] = "";
							} else if (attrTypeID == 2) {
								$scope.fields["TextMultiLine_" + ID[0]] = "";
							} else if (attrTypeID == 3) {
								$scope.fields["ListSingleSelection_" + ID[0]] = "";
							} else if (attrTypeID == 4) {
								$scope.fields["ListMultiSelection_" + ID[0]] = "";
							} else if (attrTypeID == 5) {
								$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
							} else if (attrTypeID == 6) {
								$scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
							} else if (attrTypeID == 12) {
								$scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
							} else if (attrTypeID == 17) {
								$scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
							}
							continue;
						}
						if (relationobj[i].AttributeTypeID == 4) {
							if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
								try {
									$scope.DynamicAddValidation(parseInt(ID[0]));
								} catch (e) {};
								return true;
							}
						} else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
							if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
								try {
									$scope.DynamicAddValidation(parseInt(ID[0]));
								} catch (e) {};
								return true;
							}
						} else if (relationobj[i].AttributeTypeID == 7) {
							if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
								try {
									$scope.DynamicAddValidation(parseInt(ID[0]));
								} catch (e) {};
								return true;
							}
						} else {
							if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
								try {
									$scope.DynamicAddValidation(parseInt(ID[0]));
								} catch (e) {};
								return true;
							}
						}
					}
					if (attrTypeID == 1) {
						$scope.fields["TextSingleLine_" + ID[0]] = "";
					} else if (attrTypeID == 2) {
						$scope.fields["TextMultiLine_" + ID[0]] = "";
					} else if (attrTypeID == 3) {
						$scope.fields["ListSingleSelection_" + ID[0]] = "";
					} else if (attrTypeID == 4) {
						$scope.fields["ListMultiSelection_" + ID[0]] = "";
					} else if (attrTypeID == 5) {
						$scope.fields["DatePart_" + ID[0]] = null;
					} else if (attrTypeID == 6) {
						$scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
					} else if (attrTypeID == 12) {
						$scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
					} else if (attrTypeID == 17) {
						$scope.fields["ListTagwords_" + ID[0]] = [];
					}
					try {
						$scope.DynamicRemoveValidation(parseInt(ID[0]));
					} catch (e) {};
					return false;
				} else return true;
			} else return true;
		};
		$scope.tagwordids = [];
		$scope.Dropdown = {};
		$scope.treeSourcesObj = [];
		$scope.treeSources = {};
		$scope.IsToShowRootEntity = false;
		$scope.settreeSources = function() {
			var keys = [];
			angular.forEach($scope.treeSources, function(key) {
				keys.push(key);
				$scope.treeSourcesObj = keys;
			});
		}
		$scope.listAttriToAttriResult = [];
		$scope.ShowHideAttributeOnRelation = {};
		$scope.EnableDisableControlsHolder = {};
		$scope.fields = {
			usersID: ''
		};
		$scope.varfields = {
			CostcenterInfo: []
		}
		$scope.dynamicEntityValuesHolder = {};
		$scope.fieldKeys = [];
		$scope.costcemtreObject = [];
		$scope.setFieldKeys = function() {
			var keys = [];
			angular.forEach($scope.fields, function(key) {
				keys.push(key);
				$scope.fieldKeys = keys;
			});
		}
		$scope.OptionObj = {};
		$scope.rootEntityID = '';
		$scope.rootEntityCaption = '';
		$scope.ImageFileName = '';
		$scope.metadatano = 1;
		$scope.financialno = 2;
		$scope.memberno = 3;
		GetEntityTypeTabCollections();
		$scope.UploadImagefile = function(attributeId) {
			$scope.UploadAttributeId = attributeId;
			$("#pickfilesUploaderAttr").click();
		}

		function StrartUpload_UploaderAttr() {
			$('.moxie-shim').remove();
			var uploader_Attr = new plupload.Uploader({
				runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
				browse_button: 'pickfilesUploaderAttr',
				container: 'filescontaineroo',
				max_file_size: '10000mb',
				flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
				silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
				url: 'Handlers/UploadHandler.ashx?Type=Attachment',
				chunk_size: '64Kb',
				multi_selection: false,
				multipart_params: {}
			});
			uploader_Attr.bind('Init', function(up, params) {});
			uploader_Attr.init();
			uploader_Attr.bind('FilesAdded', function(up, files) {
				up.refresh();
				uploader_Attr.start();
			});
			uploader_Attr.bind('UploadProgress', function(up, file) {});
			uploader_Attr.bind('Error', function(up, err) {
				bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
				up.refresh();
			});
			uploader_Attr.bind('FileUploaded', function(up, file, response) {
				SaveFileDetails(file, response.response);
			});
			uploader_Attr.bind('BeforeUpload', function(up, file) {
				$.extend(up.settings.multipart_params, {
					id: file.id,
					size: file.size
				});
			});
		}

		function SaveFileDetails(file, response) {
			var extension = file.name.substring(file.name.lastIndexOf("."))
			var resultArr = response.split(",");
			$scope.ImageFileName = resultArr[0] + extension;
			var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
			$('#overviewUplaodImagediv').modal('hide');
			$('#' + PreviewID).attr('src', 'UploadedImages/' + $scope.ImageFileName);
		}

		function GetEntityTypeTabCollections() {
			RootentitytypecreationService.GetPlantabsettings().then(function(gettabresult) {
				if (gettabresult.Response != null) {
					$scope.ShowFinancial = gettabresult.Response.Financials;
					if ($scope.ShowFinancial == "false") {
						$scope.memberno = 2;
						$scope.ShowFinancial = false;
					}
				}
			});
		}
		$scope.fieldoptions = [];
		$scope.setoptions = function() {
			var keys = [];
			angular.forEach($scope.OptionObj, function(key) {
				keys.push(key);
				$scope.fieldoptions = keys;
			});
		}
		$scope.dyn_Cont = '';
		$scope.items = [];
		$scope.lastSubmit = [];
		$scope.addNew = function() {
			var ItemCnt = $scope.items.length;
			if (ItemCnt > 0) {
				if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
					return false;
				}
				$scope.items.push({
					startDate: null,
					endDate: null,
					comment: '',
					sortorder: 0
				});
			}
		};
		$scope.submitOne = function(item) {
			$scope.lastSubmit = angular.copy(item);
		};
		$scope.deleteOne = function(item) {
			$scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
			$scope.items.splice($.inArray(item, $scope.items), 1);
		};
		$scope.submitAll = function() {
			$scope.lastSubmit = angular.copy($scope.items);
		}
		$scope.set_color = function(clr) {
			if (clr != null) return {
				'background-color': "#" + clr.toString().trim()
			};
			else return '';
		}
		$("#rootLevelEntity").on("onRootEntityCreation", function(event, id) {
			$("#btnWizardFinish").removeAttr('disabled');
			$('#MyWizard').wizard('stepLoaded');
			window["tid_wizard_steps_all_complete_count"] = 0;
			window["tid_wizard_steps_all_complete"] = setInterval(function() {
				KeepAllStepsMarkedComplete();
			}, 25);
			$scope.ClearModelObject($scope.fields);
			$('#btnWizardNext').show();
			$('#btnWizardPrev').hide();
			$scope.OwnerList = [];
			$scope.treeNodeSelectedHolder = [];
			$scope.treesrcdirec = {};
			$scope.treePreviewObj = {};
			$scope.OwnerList.push({
				"Roleid": 1,
				"RoleName": "Owner",
				"UserEmail": $scope.ownerEmail,
				"DepartmentName": "-",
				"Title": "-",
				"Userid": parseInt($scope.OwnerID, 10),
				"UserName": $scope.OwnerName,
				"IsInherited": '0',
				"InheritedFromEntityid": '0'
			});
		});
		$scope.treePreviewObj = {};
		$scope.ClearModelObject = function(ModelObject) {
			for (var variable in ModelObject) {
				if (typeof ModelObject[variable] === "string") {
					if (variable !== "ListSingleSelection_69") {
						ModelObject[variable] = "";
					}
				} else if (typeof ModelObject[variable] === "number") {
					ModelObject[variable] = null;
				} else if (Array.isArray(ModelObject[variable])) {
					ModelObject[variable] = [];
				} else if (typeof ModelObject[variable] === "object") {
					ModelObject[variable] = {};
				}
			}
			$scope.costcemtreObject = [];
			$scope.MemberLists = [];
			$scope.$apply();
		}
		$scope.costCenterList = [];
		RootentitytypecreationService.GetCostcentreforEntityCreation(($scope.rootEntityID) == "" ? '0' : ($scope.rootEntityID), 0, 0).then(function(costCenterList) {
			$scope.varfields.CostcenterInfo = '';
			$scope.costCenterList = costCenterList.Response;
		});
		$scope.MemberLists = [];
		$scope.tempCount = 1;
		$scope.addcostCentre = function() {
			if ($scope.varfields.CostcenterInfo.length > 0 && $scope.varfields.CostcenterInfo != null) {
				$.each($scope.varfields.CostcenterInfo, function(val, item) {
					var result = $.grep($scope.costcemtreObject, function(e) {
						return e.CostcenterId == item;
					});
					if (result.length == 0) {
						var costCentrevalues = $.grep($scope.costCenterList, function(e) {
							return e.Id == parseInt(item)
						})[0];
						$scope.costcemtreObject.push({
							"CostcenterId": item,
							"costcentername": costCentrevalues.costcentername,
							"Sortorder": 1,
							"Isassociate": 1,
							"Isactive": 1,
							"OwnerName": costCentrevalues.username,
							"OwnerID": costCentrevalues.UserID
						});
						var memberObj = $.grep($scope.MemberLists, function(e) {
							return e.Roleid == costCentrevalues.RoleID && e.Userid == costCentrevalues.UserID;
						});
						if (memberObj.length == 0) {
							$scope.MemberLists.push({
								"TID": $scope.count,
								"UserEmail": costCentrevalues.usermail,
								"DepartmentName": costCentrevalues.Designation,
								"Title": costCentrevalues.Title,
								"Roleid": costCentrevalues.RoleID,
								"RoleName": 'BudgetApprover',
								"Userid": costCentrevalues.UserID,
								"UserName": costCentrevalues.username,
								"IsInherited": '0',
								"InheritedFromEntityid": '0',
								"FromGlobal": 1,
								"CostCentreID": item
							});
							$scope.count = $scope.count + 1;
						}
					} else {
						bootbox.alert($translate.instant('LanguageContents.Res_1919.Caption'));
					}
				});
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
			}
			$scope.varfields.CostcenterInfo.splice(0, $scope.varfields.CostcenterInfo.length);
			if ($scope.treeCategory.length > 0) $scope.RecursiveCostCentreTreeClearChecked($scope.treeCategory);
		}
		$scope.RecursiveCostCentreTreeClearChecked = function(Treeval) {
			$.each(Treeval, function(val, item) {
				item.ischecked = false;
				if (item.Children.length > 0) {
					$scope.RecursiveCostCentreTreeClearChecked(item.Children);
				}
			});
		}
		$scope.deleteCostCentre = function(item) {
			bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function(result) {
				if (result) {
					timeoutvariableforentitycreation.membersplice = $timeout(function() {
						$scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
						var memberToRempve = $.grep($scope.MemberLists, function(e) {
							return e.CostCentreID == item.CostcenterId
						});
						if (memberToRempve.length > 0) $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
					}, 100);
				}
			});
		};
		var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
		$('#btnWizardNext').show();
		$('#btnWizardPrev').hide();
		window["tid_wizard_steps_all_complete_count"] = 0;
		window["tid_wizard_steps_all_complete"] = setInterval(function() {
			KeepAllStepsMarkedComplete();
		}, 25);
		$scope.changeTab = function() {
			window["tid_wizard_steps_all_complete_count"] = 0;
			window["tid_wizard_steps_all_complete"] = setInterval(function() {
				KeepAllStepsMarkedComplete();
			}, 25);
			var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
			var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
			RefreshCostcentreSource();
			if (currentWizardStep === 1) {
				$('#btnWizardNext').show();
				$('#btnWizardPrev').hide();
			} else if (currentWizardStep === totalWizardSteps) {
				$('#btnWizardNext').hide();
				$('#btnWizardPrev').show();
			} else {
				$('#btnWizardNext').show();
				$('#btnWizardPrev').show();
			}
		}
		$scope.changeTab2 = function(e) {
			if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
				loadfinnacialtree();
			}
			timeoutvariableforentitycreation.buttonclicktimeout = $timeout(function() {
				$("#btnTemp").click();
			}, 100);
			$("#EntityMetadata").removeClass('notvalidate');
			if ($("#EntityMetadata .error").length > 0) {
				e.stopImmediatePropagation();
				e.stopPropagation();
				return false;
			}
		}

		function KeepAllStepsMarkedComplete() {
			$("#MyWizard ul.steps").find("li").addClass("complete");
			$("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
			window["tid_wizard_steps_all_complete_count"]++;
			if (window["tid_wizard_steps_all_complete_count"] >= 3) {
				clearInterval(window["tid_wizard_steps_all_complete"]);
			}
		}
		$scope.changePrevTab = function() {
			if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
				loadfinnacialtree();
			}
			window["tid_wizard_steps_all_complete_count"] = 0;
			window["tid_wizard_steps_all_complete"] = setInterval(function() {
				KeepAllStepsMarkedComplete();
			}, 25);
			var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
			var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
			RefreshCostcentreSource();
			if (($scope.ShowFinancial == "false" || $scope.ShowFinancial == false) && currentWizardStep == 3) {
				currentWizardStep = 1;
				$('#MyWizard').wizard('manualpreviousstep', currentWizardStep);
			} else {
				$('#MyWizard').wizard('previous', '');
				currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
			}
			if (currentWizardStep === 1) {
				$('#btnWizardPrev').hide();
			}
			if (currentWizardStep < totalWizardSteps) {
				$('#btnWizardNext').show();
			} else {
				$('#btnWizardNext').hide();
			}
		}
		$scope.status = true;
		$scope.changenexttab = function(event) {
			if ($scope.ShowFinancial == true || $scope.ShowFinancial == 'true') {
				loadfinnacialtree();
			}
			var currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
			var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
			var percentageflag = false;
			$('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function(index, value) {
				if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
					if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
						percentageflag = true;
					}
				}
			});
			if (percentageflag) {
				return false;
			}
			timeoutvariableforentitycreation.buttonclicktimeout2 = $timeout(function() {
				$("#btnTemp").click();
			}, 100);
			$("#EntityMetadata").removeClass('notvalidate');
			if ($("#EntityMetadata .error").length > 0) {
				return false;
			}
			window["tid_wizard_steps_all_complete_count"] = 0;
			window["tid_wizard_steps_all_complete"] = setInterval(function() {
				KeepAllStepsMarkedComplete();
			}, 25);
			if (($scope.ShowFinancial == "false" || $scope.ShowFinancial == false) && currentWizardStep == 1) {
				currentWizardStep = 3;
				$('#MyWizard').wizard('manualnextstep', currentWizardStep);
			} else {
				$('#MyWizard').wizard('next', '');
				currentWizardStep = $('#MyWizard').wizard('selectedItem').step;
				totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
			}
			RefreshCostcentreSource();
			if (currentWizardStep > 1) {
				$('#btnWizardPrev').show();
			}
			if (currentWizardStep === totalWizardSteps) {
				$('#btnWizardNext').hide();
			} else {
				$('#btnWizardNext').show();
			}
		}
		var ownername = $cookies['Username'];
		var ownerid = $cookies['UserId'];
		$scope.OwnerName = ownername;
		$scope.OwnerID = ownerid;
		$scope.ownerEmail = $cookies['UserEmail'];
		$scope.AutoCompleteSelectedObj = [];
		$scope.OwnerList = [];
		$scope.OwnerList.push({
			"Roleid": 1,
			"RoleName": "Owner",
			"UserEmail": $scope.ownerEmail,
			"DepartmentName": "-",
			"Title": "-",
			"Userid": parseInt($scope.OwnerID, 10),
			"UserName": $scope.OwnerName,
			"IsInherited": '0',
			"InheritedFromEntityid": '0'
		});
		$scope.contrls = '';
		var Version = 1;
		$scope.wizard = {
			newval: ''
		};
		$scope.entityObjectString = '{';
		$scope.FinancialRequestObj = [];
		$scope.optionsLists = [];
		$scope.entityNameTyping = "";
		$scope.AttributeData = [];
		$scope.entityName = "";
		$scope.count = 1;
		$scope.addUsers = function() {
			var result = $.grep($scope.MemberLists, function(e) {
				return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10);
			});
			if (result.length == 0) {
				var membervalues = $.grep($scope.Roles, function(e) {
					return e.ID == parseInt($scope.fields['userRoles'])
				})[0];
				if (membervalues != undefined) {
					$scope.MemberLists.push({
						"TID": $scope.count,
						"UserEmail": $scope.AutoCompleteSelectedObj[0].Email,
						"DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation,
						"Title": $scope.AutoCompleteSelectedObj[0].Title,
						"Roleid": parseInt($scope.fields['userRoles'], 10),
						"RoleName": membervalues.Caption,
						"Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10),
						"UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName,
						"IsInherited": '0',
						"InheritedFromEntityid": '0',
						"FromGlobal": 0,
						"CostCentreID": 0,
						"QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1,
						"QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2
					});
					$scope.fields.usersID = '';
					$scope.count = $scope.count + 1;
					$scope.AutoCompleteSelectedObj = [];
				}
			} else bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
		};
		$scope.deleteOptions = function(item) {
			bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function(result) {
				if (result) {
					timeoutvariableforentitycreation.memberspliceto = $timeout(function() {
						$scope.MemberLists.splice($.inArray(item, $scope.MemberLists), 1);
					}, 100);
				}
			});
		};
		$scope.saveRootEnt = function() {
			var percentageflag = false;
			$('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function(index, value) {
				if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
					if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
						percentageflag = true;
					}
				}
			});
			if (percentageflag) {
				return false;
			}
			timeoutvariableforentitycreation.buttonclicktimeout3 = $timeout(function() {
				$("#btnTemp").click();
			}, 100);
			$("#EntityMetadata").removeClass('notvalidate');
			if ($("#EntityMetadata .error").length > 0) {
				return false;
			}
			$("#btnWizardFinish").attr('disabled', 'disabled');
			var SaveEntity = {};
			if (parseInt($scope.OwnerList[0].Userid) == parseInt($cookies.UserId)) {
				$scope.IsLock = false;
			} else {
				$scope.IsLock = true;
			}
			for (var i = 0; i < $scope.atributesRelationList.length; i++) {
				if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
					for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
						if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
								"Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
								"Value": "-1"
							});
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
					for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
						var levelCount = $scope.atributesRelationList[i].Levels.length;
						if (levelCount == 1) {
							for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
								$scope.AttributeData.push({
									"AttributeID": $scope.atributesRelationList[i].AttributeID,
									"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
									"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
									"NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
									"Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
									"Value": "-1"
								});
							}
						} else {
							if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
								if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
									for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
										$scope.AttributeData.push({
											"AttributeID": $scope.atributesRelationList[i].AttributeID,
											"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
											"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
											"NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
											"Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
											"Value": "-1"
										});
									}
								} else {
									$scope.AttributeData.push({
										"AttributeID": $scope.atributesRelationList[i].AttributeID,
										"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
										"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
										"NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
										"Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
										"Value": "-1"
									});
								}
							}
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
					for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
						var attributeLevelOptions = [];
						attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function(e) {
							return e.level == (j + 1);
						}));
						if (attributeLevelOptions[0] != undefined) {
							if (attributeLevelOptions[0].selection != undefined) {
								for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
									var valueMatches = [];
									if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
										return relation.NodeId.toString() === opt;
									});
									$scope.AttributeData.push({
										"AttributeID": $scope.atributesRelationList[i].AttributeID,
										"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
										"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
										"NodeID": [opt],
										"Level": (j + 1),
										"Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
									});
								}
							}
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
					if ($scope.atributesRelationList[i].IsSpecial == true) {
						if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": parseInt($scope.OwnerList[0].Userid, 10),
								"Level": 0,
								"Value": "-1"
							});
						}
					} else if ($scope.atributesRelationList[i].IsSpecial == false) {
						if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
							var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
								"Level": 0,
								"Value": "-1"
							});
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
					if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
					else {
						$scope.AttributeData.push({
							"AttributeID": $scope.atributesRelationList[i].AttributeID,
							"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
							"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							"NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
							"Level": 0,
							"Value": "-1"
						});
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
						"Level": 0,
						"Value": "-1"
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
					if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
						if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
							var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
							for (var k = 0; k < multiselectiObject.length; k++) {
								$scope.AttributeData.push({
									"AttributeID": $scope.atributesRelationList[i].AttributeID,
									"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
									"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
									"NodeID": parseInt(multiselectiObject[k], 10),
									"Level": 0,
									"Value": "-1"
								});
							}
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
					var treenodes = [];
					treenodes = $.grep($scope.treeNodeSelectedHolder, function(e) {
						return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
					});
					for (var x = 0, nodeval; nodeval = treenodes[x++];) {
						$scope.AttributeData.push({
							"AttributeID": $scope.atributesRelationList[i].AttributeID,
							"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
							"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							"NodeID": [parseInt(nodeval.id, 10)],
							"Level": parseInt(nodeval.Level, 10),
							"Value": "-1"
						});
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
						"Level": 0,
						"Value": "-1"
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
					if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
						$scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
					}
					$scope.Entityamountcurrencytypeitem.push({
						amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
						currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
						Attributeid: $scope.atributesRelationList[i].AttributeID
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
					if ($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
						var dte = new Date.create($scope.fields['DatePart_' + $scope.atributesRelationList[i].AttributeID]);
						var tdte = ConvertDateToString(dte);
						$scope.AttributeData.push({
							"AttributeID": $scope.atributesRelationList[i].AttributeID,
							"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
							"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							"NodeID": tdte,
							"Level": 0,
							"Value": "-1"
						});
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
					if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
						for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
								"Level": 0,
								"Value": "-1"
							});
						}
					}
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": "",
						"Level": 0,
						"Value": "-1"
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
					var MyDate = new Date.create();
					var MyDateString;
					if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
						if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != null) {
							MyDateString = dateFormat($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID], $scope.format);
						} else {
							MyDateString = "";
						}
					} else {
						MyDateString = "";
					}
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": MyDateString,
						"Level": 0
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": $scope.ImageFileName,
						"Level": 0,
						"Value": "-1"
					});
				} else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
					$scope.AttributeData.push({
						"AttributeID": $scope.atributesRelationList[i].AttributeID,
						"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
						"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
						"NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
						"Level": 0,
						"Value": "-1"
					});
				}
			}
			SaveEntity.ParentId = 0;
			SaveEntity.Typeid = $scope.rootEntityID;
			SaveEntity.Active = true;
			SaveEntity.IsLock = false;
			SaveEntity.Name = $scope.entityName;
			SaveEntity.EntityMembers = [];
			SaveEntity.EntityCostRelations = $scope.costcemtreObject;
			SaveEntity.Periods = [];
			$scope.StartEndDate = [];
			for (var m = 0; m < $scope.items.length; m++) {
				if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
					if ($scope.items[m].startDate.toString().length != 0 && $scope.items[m].endDate.toString().length != null) {
						$scope.StartEndDate.push({
							startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
							endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
							comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
							sortorder: 0
						});
					}
				}
			}
			SaveEntity.Periods.push($scope.StartEndDate);
			SaveEntity.AttributeData = $scope.AttributeData;
			SaveEntity.EntityMembers = $scope.MemberLists;
			SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;
			SaveEntity.AssetArr = new Array();
			SaveEntity.IObjectiveEntityValue = [];
			SaveEntity.Entityamountcurrencytype = [];
			$scope.curram = [];
			for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
				$scope.curram.push({
					amount: $scope.Entityamountcurrencytypeitem[m].amount,
					currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
					Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
				});
			}
			SaveEntity.Entityamountcurrencytype.push($scope.curram);
			RootentitytypecreationService.CreateEntity(SaveEntity).then(function(SaveEntityResult) {
				if (SaveEntityResult.Response == 0) {
					NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
					$scope.Entityamountcurrencytypeitem = [];
				} else {
					$scope.Activity.IsActivitySectionLoad = false;
					$scope.MemberLists = [];
					$scope.Entityamountcurrencytypeitem = [];
					var IDList = new Array();
					IDList.push(SaveEntityResult.Response);
					$window.ListofEntityID = IDList;
					var TrackID = CreateHisory(IDList);
					$location.path('/mui/planningtool/default/detail/section/' + SaveEntityResult.Response + '/overview').replace();
				}
				timeoutvariableforentitycreation.cancelTimeout = $timeout(function() {
					$modalInstance.dismiss('cancel');
				}, 100)
			});
		};

		function RefreshCostcentreSource() {
			if ($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear] != undefined) {
				$scope.costCenterList = [];
				var year = (parseInt($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
				var FiscalYearValue = parseInt(($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
				RootentitytypecreationService.GetCostcentreforEntityCreation($scope.rootEntityID == "" ? 0 : $scope.rootEntityID, FiscalYearValue, 0).then(function(costCenterList) {
					$scope.costCenterList = costCenterList.Response;
				});
				if ($('#MyWizard').wizard('selectedItem').step == 2) {
					$scope.treeCategory = [];
					var FiscalYearValue = parseInt(($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
					RootentitytypecreationService.GetCostcentreTreeforPlanCreation($scope.rootEntityID == "" ? 0 : $scope.rootEntityID, FiscalYearValue, 0).then(function(costCenterList1) {
						if (costCenterList1.Response != null && costCenterList1.Response != false) {
							$scope.treeCategory = JSON.parse(costCenterList1.Response);
							$scope.dyn_Cont = '';
							$scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
							$scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
							$scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
							$scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
							$scope.dyn_Cont += '</div>';
							$("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));
						}
					});
				}
			}
		}
		$scope.changeduedate_changed = function(duedate, ID) {
			if (duedate != null) {
				var test = isValidDate(duedate.toString(), $scope.format.toString());
				if (test) {
					var a = $.grep($scope.tempholidays, function(e) {
						return e == dateFormat(duedate, $scope.format);
					});
					if (a != null) {
						if (a.length > 0) {
							bootbox.alert("Selected date should not be a non businessday");
							$scope.fields["DatePart_" + ID] = "";
						}
					}
				} else {
					$scope.fields["DatePart_" + ID] = "";
					bootbox.alert("Select valid duedate");
				}
			}
		}
		$scope.changeperioddate_changed = function(date, datetype) {
			if (date != null) {
				var test = isValidDate(date.toString(), $scope.format.toString());
				if (test) {
					var a = $.grep($scope.tempholidays, function(e) {
						return e == dateFormat(date, $scope.format);
					});
					if (a != null) {
						if (a.length > 0) {
							bootbox.alert("Selected date should not be a non businessday");
							if (datetype == "StartDate") $scope.items[0].startDate = "";
							else $scope.item[0].endDate = "";
						}
					}
				} else {
					if (datetype == "StartDate") $scope.items[0].startDate = "";
					else $scope.item[0].endDate = "";
					bootbox.alert("Select valid duedate");
				}
			}
		}

		function isValidDate(dateval, dateformat) {
			var formatlen;
			var defaultdateVal = [];
			defaultdateVal = dateval.length;
			formatlen = dateformat.length;
			if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
			else return false;
		};
		$scope.changeCostCenterSource = function() {
			$scope.costCenterList = [];
			$scope.costcemtreObject = [];
			var year = (parseInt($scope.fields['ListSingleSelection_' + SystemDefiendAttributes.FiscalYear]), 10);
			var FiscalYearValue = (year == " " ? 0 : year);
			RootentitytypecreationService.GetCostcentreforEntityCreation($scope.rootEntityID, FiscalYearValue, 0).then(function(costCenterList) {
				$scope.costCenterList = costCenterList.Response;
			});
		}

		function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
			var recursiveAttrID = '';
			var attributesToShow = [];
			if (attrLevel > 0) {
				attributesToShow.push($.grep(res, function(e) {
					return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
				}));
			} else {
				attributesToShow.push($.grep(res, function(e) {
					return (e.AttributeID == attrID);
				}));
			}
			if (attributesToShow[0] != undefined) {
				for (var i = 0; i < attributesToShow[0].length; i++) {
					var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
					if (attrRelIDs != undefined) {
						for (var j = 0; j < attrRelIDs.length; j++) {
							if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
								$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
								recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function(e) {
									return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
								}));
								if (recursiveAttrID != undefined) {
									for (var m = 0; m < recursiveAttrID.length; m++) {
										RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
									}
								}
							} else {
								$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
								$scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
								recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function(e) {
									return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
								}));
								if (recursiveAttrID != undefined) {
									for (var m = 0; m < recursiveAttrID.length; m++) {
										RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
									}
								}
							}
						}
					}
				}
			}
		}
		$scope.LoadSubLevels = function(attrID, attributeLevel, levelcnt, attrType) {
			try {
				if (levelcnt > 0) {
					var currntlevel = attributeLevel + 1;
					for (var j = currntlevel; j <= levelcnt; j++) {
						$scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
						if (attrType == 6) {
							$scope.fields["DropDown_" + attrID + "_" + j] = "";
						} else if (attrType == 12) {
							if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
							else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
						}
					}
					if (attrType == 6) {
						if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
							$.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
								$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
							});
						}
					} else if (attrType == 12) {
						if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
							$.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
								$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
							});
						}
					}
				}
				if (attrID == SystemDefiendAttributes.FiscalYear) {
					try {
						$scope.changeCostCenterSource();
					} catch (e) {}
				}
			} catch (e) {}
		}
		$scope.ShowHideAttributeToAttributeRelations = function(attrID, attributeLevel, levelcnt, attrType) {
			try {
				var recursiveAttrID = '';
				var optionValue = '';
				var attributesToShow = [];
				var hideAttributeOtherThanSelected = [];
				if (levelcnt > 0) {
					var currntlevel = attributeLevel + 1;
					for (var j = currntlevel; j <= levelcnt; j++) {
						$scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
						if (attrType == 6) {
							$scope.fields["DropDown_" + attrID + "_" + j] = "";
						} else if (attrType == 12) {
							if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
							else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
						}
					}
					if (attrType == 6) {
						if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
							$.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
								$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
							});
						}
					} else if (attrType == 12) {
						if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
							$.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
								$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
							});
						}
					}
				}
				if (attrID == SystemDefiendAttributes.FiscalYear) {
					try {
						$scope.changeCostCenterSource();
					} catch (e) {}
				}
				if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
					return false;
				}
				RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
				if (attrType == 3) {
					if ($scope.fields['ListSingleSelection_' + attrID] != null) {
						optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
					} else {
						optionValue = 0;
					}
					attributesToShow.push($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
					})[0]);
				} else if (attrType == 4) {
					if ($scope.fields['ListMultiSelection_' + attrID] != null) {
						optionValue = $scope.fields['ListMultiSelection_' + attrID];
					} else {
						optionValue = 0;
					}
					attributesToShow = ($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
					}));
				} else if (attrType == 6) {
					var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
					if (attrval != null) {
						attributesToShow = [];
						attributesToShow.push($.grep($scope.listAttriToAttriResult, function(e) {
							return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
						})[0]);
					}
				} else if (attrType == 7) {
					if ($scope.fields['Tree_' + attrID] != null) {
						optionValue = $scope.fields['Tree_' + attrID];
					} else {
						optionValue = 0;
					}
					attributesToShow = ($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
					}));
				} else if (attrType == 12) {
					var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
					if (attrval != null) {
						attributesToShow = [];
						attributesToShow.push($.grep($scope.listAttriToAttriResult, function(e) {
							return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
						})[0]);
					}
				}
				if (attributesToShow[0] != undefined) {
					for (var i = 0; i < attributesToShow.length; i++) {
						var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
						if (attrRelIDs != undefined) {
							for (var j = 0; j < attrRelIDs.length; j++) {
								if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
								}
							}
						}
					}
				}
			} catch (e) {}
		}

		function HideAttributeToAttributeRelationsOnPageLoad() {
			try {
				if ($scope.listAttriToAttriResult != undefined) {
					for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
						var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
						if (attrRelIDs != undefined) {
							for (var j = 0; j < attrRelIDs.length; j++) {
								if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
								}
							}
						}
					}
				}
			} catch (e) {}
		}
		$scope.addRootEntity = function() {
			$scope.EnableAdd = true;
			$scope.EnableUpdate = false;
			$scope.step = 0;
			$scope.EnableOptionUpdate = false;
			$scope.EnableOptionAdd = true;
		};

		function GetEntityTypeRoleAccess(rootID) {
			RootentitytypecreationService.GetEntityTypeRoleAccess(rootID).then(function(role) {
				$scope.Roles = role.Response;
			});
		}
		$scope.treelevels = [];
		$scope.tree = {};
		$scope.MultiSelecttree = {};
		$scope.Options = {};
		$scope.myTree = [];
		$scope.SelectionList = [];
		$scope.setSelected = function(c) {
			var checkopt = $.grep($scope.SelectionList, function(e) {
				return e == c.id;
			});
			if (c.checked1 == true && checkopt.length == 0) {
				$scope.SelectionList.push(c.id);
			} else if (c.checked1 == false && checkopt.length > 0) {
				$scope.SelectionList.splice($.inArray(c.id, $scope.SelectionList), 1);
			}
		}
		$scope.dropDownTreeListMultiSelectionObj = [];

		function loadfinnacialtree() {
			$scope.dyn_Cont = '';
			if ($scope.treeCategory.length == 0) {
				$scope.dyn_Cont += '<input type="text" class="read-only" placeholder="No Cost centre is available"/>';
			} else {
				$scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_RootEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_RootEntity"> ';
				$scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_RootEntity">';
				$scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
				$scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_RootEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreeSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
				$scope.dyn_Cont += '</div>';
			}
			$("#dynamicCostCentreTreeOnRootEntity").html($compile($scope.dyn_Cont)($scope));
		}
		$scope.LoadAttributesForEntityCreation = function(rootID, rootCaption) {
		    $scope.DateObject = {};
		    $scope.dyn_Cont = '';
			GetEntityTypeRoleAccess(rootID);
			$scope.UserimageNewTime = new Date.create().getTime().toString();
			$scope.fields = {
				usersID: ''
			};
			$scope.varfields = {
				CostcenterInfo: []
			};
			$scope.dynamicEntityValuesHolder = {};
			$scope.fieldKeys = [];
			$scope.costcemtreObject = [];
			$scope.setFieldKeys = function() {
				var keys = [];
				angular.forEach($scope.fields, function(key) {
					keys.push(key);
					$scope.fieldKeys = keys;
				});
			}
			$scope.OptionObj = {};
			$scope.fieldoptions = [];
			$scope.setoptions = function() {
				var keys = [];
				angular.forEach($scope.OptionObj, function(key) {
					keys.push(key);
					$scope.fieldoptions = keys;
				});
			}
			$scope.dropDownTreeListMultiSelectionObj = [];
			$scope.MemberLists = [];
			$scope.tempCount = 1;
			var totalWizardSteps = $('#MyWizard').wizard('totnumsteps').totstep;
			$('#MyWizard').wizard('stepLoaded');
			$('#btnWizardNext').show();
			$('#btnWizardPrev').hide();
			window["tid_wizard_steps_all_complete_count"] = 0;
			window["tid_wizard_steps_all_complete"] = setInterval(function() {
				KeepAllStepsMarkedComplete();
			}, 25);
			$scope.status = true;
			var ownername = $cookies['Username'];
			var ownerid = $cookies['UserId'];
			$scope.OwnerName = ownername;
			$scope.OwnerID = ownerid;
			$scope.ownerEmail = $cookies['UserEmail'];
			$scope.AutoCompleteSelectedObj = [];
			$scope.OwnerList = [];
			$scope.owner = {};
			$scope.contrls = '';
			var Version = 1;
			$scope.FinancialRequestObj = [];
			$scope.optionsLists = [];
			$scope.entityNameTyping = "";
			$scope.AttributeData = [];
			$scope.entityName = "";
			$scope.count = 1;
			$scope.treelevels = [];
			$scope.tree = {};
			$scope.MultiSelecttree = {};
			$scope.Options = {};
			$scope.wizard = {
				newval: ''
			};
			$scope.costcemtreObject = [];
			$scope.dyn_Cont = '';
			$scope.rootEntityID = '';
			$scope.rootEntityCaption = rootCaption;
			$scope.rootEntityID = rootID;
			$scope.PercentageVisibleSettings = {};
			$scope.DropDownTreePricing = {};
			$scope.items = [];
			$scope.Entityamountcurrencytypeitem = [];
			$scope.tagwordids = [];
			RootentitytypecreationService.GetAttributeToAttributeRelationsByIDForEntity($scope.rootEntityID).then(function(entityAttrToAttrRelation) {
				if (entityAttrToAttrRelation.Response != null) {
					if (entityAttrToAttrRelation.Response.length > 0) {
						$scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
					}
				}
			});
			RootentitytypecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function(Getowner) {
				$scope.owner = Getowner.Response;
				$scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
				$scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
				$scope.OwnerList.push({
					"Roleid": 1,
					"RoleName": "Owner",
					"UserEmail": $scope.ownerEmail,
					"DepartmentName": $scope.owner.Designation,
					"Title": $scope.owner.Title,
					"Userid": parseInt($scope.OwnerID, 10),
					"UserName": $scope.OwnerName,
					"IsInherited": '0',
					"InheritedFromEntityid": '0',
					"QuickInfo1": $scope.owner.QuickInfo1,
					"QuickInfo2": $scope.owner.QuickInfo2
				});
				RootentitytypecreationService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function(entityAttributesRelation) {
					$scope.atributesRelationList = entityAttributesRelation.Response;
					for (var i = 0; i < $scope.atributesRelationList.length; i++) {
						if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
							if ($scope.atributesRelationList[i].AttributeID != 70) {
								$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
								if ($scope.atributesRelationList[i].InheritFromParent) $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];
								if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
									$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
									$scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
								} else {
									$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
									$scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
								}
								$scope.setFieldKeys();
							}
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
							if ($scope.atributesRelationList[i].Caption == "Due Date") {
								$scope.IsOpend = "DueDate";
								$scope.DateObject.DueDate = false;
								var isopenedhtmlstr = "DateObject.DueDate";
							} else if ($scope.atributesRelationList[i].Caption == "StartDate") {
								$scope.IsOpend = "StartDate";
								var isopenedhtmlstr = "DateObject.StartDate";
								$scope.DateObject.StartDate = false;
							} else {
								$scope.IsOpend = "EndDate";
								$scope.DateObject.EndDate = false;
								var isopenedhtmlstr = "DateObject.EndDate";
							}
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].calenderAttributeID] = new Date.create();
							$scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
							$scope.MinValue = $scope.atributesRelationList[i].MinValue;
							$scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " : </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							if ($scope.atributesRelationList[i].IsSpecial == true) {
								if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
									$scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
									$scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
									$scope.setFieldKeys();
								}
							} else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
								$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
								$scope.setoptions();
								$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
								if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
								else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
								$scope.setFieldKeys();
							} else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {} else {
								$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
								$scope.setoptions();
								$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
								if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
								else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
								$scope.setFieldKeys();
							}
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
							var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
							for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
								$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
								$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
								$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
								$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
									return item.Caption
								};
								$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
									return item.Caption
								};
								if (j == 0) {
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
									$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
									$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
									$scope.settreeSources();
								} else {
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
									$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
									$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
								}
								$scope.setFieldKeys();
							}
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
							if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
							else $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
							$scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
							$scope.setoptions();
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
							if ($scope.atributesRelationList[i].InheritFromParent && $scope.atributesRelationList[i].ParentValue != null) {
								$scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
							} else {
								var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
								if ($scope.atributesRelationList[i].DefaultValue != "") {
									for (var j = 0; j < defaultmultiselectvalue.length; j++) {
										$scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function(e) {
											return e.Id == defaultmultiselectvalue[j];
										})[0].Id);
									}
								} else {
									$scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
								}
							}
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
							$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
							$scope.setoptions();
							$scope.items.push({
								startDate: null,
								endDate: null,
								comment: '',
								sortorder: 0,
								calstartopen: false,
								calendopen: false
							});
							$scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
							$scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
							$scope.MinValue = $scope.atributesRelationList[i].MinValue;
							$scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
							$scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
							$scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
							$scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
							$scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
							$scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
							$scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
							$scope.setoptions();
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div></div>";
							if ($scope.atributesRelationList[i].InheritFromParent) {
								$scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Amount;
								$scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Currencytypeid;
							} else {
								$scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
								$scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
							}
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
							$scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
							if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
								treeTextVisbileflag = false;
								if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
									$scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
								} else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
							} else {
								$scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
							}
							$scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
							$scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
							$scope.dyn_Cont += '<div class="controls treeNode-controls">';
							$scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
							$scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
							$scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
							$scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
							$scope.dyn_Cont += '</div></div></div>';
							$scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
							$scope.dyn_Cont += '<div class="controls">';
							$scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
							$scope.dyn_Cont += '</div></div>';
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
							$scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
							$scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
							$scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group \" drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
							var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
							for (var j = 0; j < totLevelCnt1; j++) {
								if (totLevelCnt1 == 1) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
									$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
									$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
									$scope.setFieldKeys();
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
										return item.Caption
									};
									if (j == 0) {
										$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
										$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
										$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
										$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
										$scope.settreeSources();
									} else {
										$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
										if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
											$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
											$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
											$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
										} else {
											$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
											$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
											$scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
										}
									}
								}
								$scope.setFieldKeys();
							}
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
							$scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
							$scope.setoptions();
							$scope.tempscope = [];
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
							$scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
							$scope.dyn_Cont += "<div class=\"controls\">";
							$scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
							$scope.dyn_Cont += "</div></div>";
							if ($scope.atributesRelationList[i].InheritFromParent) {} else {}
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.IsOpend = "DateAction";
							var isopenedhtmlstr = "DateObject.DateAction";
							$scope.DateObject = {
								"DateAction": false
							};
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							$scope.MinValue = $scope.atributesRelationList[i].MinValue;
							$scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
							$scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
							$scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"></div></div>";
							var param1 = new Date.create();
							var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
							$scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
							$scope.setFieldKeys();
							$scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
							StrartUpload_UploaderAttr();
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
							$scope.setoptions();
							$scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group"><label class="control-label"';
							$scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
							$scope.dyn_Cont += '<div id="Uploader" class="controls">';
							$scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
							$scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
							$scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
							$scope.dyn_Cont += '</div></div>';
							$scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
							$scope.setFieldKeys();
						} else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
							$scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
							$scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
							$scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
							$scope.setFieldKeys();
						}
						$scope.setFieldKeys();
						if ($scope.atributesRelationList[i].IsReadOnly == true) {
							$scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
						} else {
							$scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
						}
					}
					$scope.setFieldKeys();
					$scope.dyn_Cont += '<input style="display: none" type="submit" id="btnTemp" class="ng-scope" invisible>';
					$("#EntityMetadata").html($compile($scope.dyn_Cont)($scope));
					var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
					$scope.mindec = "";
					if (mar.length == 1) {
						$("[id^='dTextSingleLine_']").autoNumeric('init', {
							aSep: ' ',
							vMin: "0",
							mDec: "1"
						});
					}
					if (mar.length != 1) {
						if (mar.length < 5) {
							$scope.mindec = "0.";
							for (i = 0; i < mar.length; i++) {
								$scope.mindec = $scope.mindec + "0";
							}
							$("[id^='dTextSingleLine_']").autoNumeric('init', {
								aSep: ' ',
								vMin: $scope.mindec
							});
						} else {
							$("[id^='dTextSingleLine_']").autoNumeric('init', {
								aSep: ' ',
								vMin: "0",
								mDec: "0"
							});
						}
					}
					$("[id^='MTextSingleLine_']").autoNumeric('init', {
						aSep: ' ',
						vMin: "0",
						mDec: "0"
					});
					$("[id^='MTextSingleLine_']").keydown(function(event) {
						if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
							bootbox.alert($translate.instant('Please enter only Number'));
							event.preventDefault();
						}
					});
					setTimeout(function() {
						$('[id^=TextSingleLine]:enabled:visible:first').focus().select()
					}, 1000);
					$("#EntityMetadata").scrollTop(0);
					$scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];
					timeoutvariableforentitycreation.hideattr = $timeout(function() {
						HideAttributeToAttributeRelationsOnPageLoad();
					}, 200);
					GetValidationList();
					$("#EntityMetadata").addClass('notvalidate');
				});
			});
		}
		$scope.AddDefaultEndDate = function(objdateval) {
			$("#EntityMetadata").addClass('notvalidate');
			if (objdateval.startDate == null) {
				objdateval.endDate = null
			} else {
				objdateval.endDate = new Date.create(objdateval.startDate);
				objdateval.endDate = (7).daysAfter(objdateval.endDate);
			}
		};
		$scope.AddDefaultEndDate = function(enddate, startdate, currentindex) {
			var enddate1 = null;
			if (currentindex != 0) {
				enddate1 = $scope.items[currentindex - 1].endDate;
			}
			if (enddate1 != null && enddate1 >= startdate) {
				bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
				$scope.items[currentindex].startDate = null;
				$scope.items[currentindex].endDate = null;
			} else {
				$("#EntityMetadata").addClass('notvalidate');
				if (startdate == null) {
					$scope.items[currentindex].endDate = null;
				} else {
					var endDate = new Date.create(startdate);
					$scope.items[currentindex].endDate = (7).daysAfter(endDate);
				}
			}
		};
		$scope.CheckPreviousStartDate = function(enddate, startdate, currentindex) {
			var enddate1 = null;
			if (currentindex != 0 || currentindex == 0) {
				enddate1 = $scope.items[currentindex].endDate;
			}
			var edate = ConvertDateToString(enddate1);
			var sdate = ConvertDateToString(startdate);
			if (enddate1 != null) {
				if (edate < sdate) {
					bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
					$scope.items[currentindex].endDate = null;
				}
			}
		};
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.RootEntityTypeCreationCtrl']"));
		});

		function GetValidationList() {
			RootentitytypecreationService.GetValidationDationByEntitytype($scope.rootEntityID).then(function(GetValidationresult) {
				if (GetValidationresult.Response != null) {
					$scope.listValidationResult = GetValidationresult.Response;
					$scope.listofValidations = GetValidationresult.Response;
					if ($scope.listAttriToAttriResult != null) {
						for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
							var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
							if (attrRelIDs != undefined) {
								for (var j = 0; j < attrRelIDs.length; j++) {
									if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
										$scope.listValidationResult = $.grep($scope.listValidationResult, function(e) {
											return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
										});
									} else {
										$scope.listValidationResult = $.grep($scope.listValidationResult, function(e) {
											return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
										});
									}
								}
							}
						}
					}
					timeoutvariableforentitycreation.listvalidation = $timeout(function() {
						$("#EntityMetadata").nod($scope.listValidationResult, {
							'delay': 200,
							'submitBtnSelector': '#btnTemp',
							'silentSubmit': 'true'
						});
					}, 100);
				}
			});
		}
		var treeTextVisbileflag = false;

		function IsNotEmptyTree(treeObj) {
			for (var i = 0, node; node = treeObj[i++];) {
				if (node.ischecked == true) {
					treeTextVisbileflag = true;
					return treeTextVisbileflag;
				} else {
					IsNotEmptyTree(node.Children);
				}
			}
			return treeTextVisbileflag;
		}
		$scope.OnCostCentreTreeSelection = function(branch, parentArr) {
			if (branch.ischecked == true) {
				$scope.varfields.CostcenterInfo.push(branch.id);
			} else {
				$scope.varfields.CostcenterInfo.splice($scope.varfields.CostcenterInfo.indexOf(branch.id), 1);
			}
		};
		$scope.treeNodeSelectedHolder = [];
		var apple_selected, tree, treedata_avm, treedata_geography;
		$scope.my_tree_handler = function(branch, parentArr) {
			$scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
			if (branch.ischecked == true) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length == 0) {
					$scope.treeNodeSelectedHolder.push(branch);
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
				}
			}
			if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
				treeTextVisbileflag = false;
				if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
					$scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
				} else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			} else {
				$scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			}
			if ($scope.treeNodeSelectedHolder.length > 0) {
				for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
					$scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
				}
			}
			$scope.ShowOrHideAttributeToAttributeRelation(branch.AttributeId + "_0", 0, 7);
		};

		function RemoveRecursiveChildTreenode(children) {
			for (var j = 0, child; child = children[j++];) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == child.AttributeId && e.id == child.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
					if (child.Children.length > 0) {
						RemoveRecursiveChildTreenode(child.Children);
					}
				}
			}
		}
		$scope.treesrcdirec = {};
		$scope.my_tree = tree = {};
		$scope.origninalamount = 0;
		$scope.Getamountentered = function(atrid) {
			if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
			else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
		}
		$scope.GetCostCentreCurrencyRateById = function(atrid) {
			RootentitytypecreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function(resCurrencyRate) {
				if (resCurrencyRate.Response != null) {
					$scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
					if ($scope['origninalamountvalue_' + atrid] != 0) {
						$scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
					}
				}
			});
		}
		RootentitytypecreationService.GetCurrencyListFFsettings().then(function(CurrencyListResult) {
			if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
		});
		$scope.Closeentitycreationpopup = function() {
			$modalInstance.dismiss('cancel');
		}

		function openentitycreationpopup() {
			$scope.LoadAttributesForEntityCreation(params.ID, params.Caption);
		}
		openentitycreationpopup();
		$scope.$on("$destroy", function() {
			$timeout.cancel(timeoutvariableforentitycreation);
			RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.RootEntityTypeCreationCtrl']"));
			var remValtimer, IsValExist, getVal, IsValExist, Attributetypename, relationobj, ID, i, o, a, j, x, k, m, keys, extension, resultArr, PreviewID, variable, result, costCentrevalues, memberObj, memberToRempve, totalWizardSteps, currentWizardStep, totalWizardSteps, percentageflag, ownername, ownerid, Version, membervalues, SaveEntity, attributeLevelOptions, valueMatches, value, multiselectiObject, treenodes, dte, tdte, MyDate, MyDateString, IDList, TrackID, FiscalYearValue, year, test, formatlen, defaultdateVal, recursiveAttrID, attributesToShow, attrRelIDs, currntlevel, hideAttributeOtherThanSelected, optionValue, attrval, checkopt, Version, temp, totLevelCnt, defaultmultiselectvalue, totLevelCnt1, param1, param2, mar, enddate1, enddate, edate, sdate, renderContext, treeTextVisbileflag, apple_selected, tree, treedata_avm, treedata_geography, remainRecord = null;
		});
	}
	app.controller("mui.planningtool.RootEntityTypeCreationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$translate', '$compile', 'RootentitytypecreationService', '$modalInstance', 'params', muiplanningtoolRootEntityTypeCreationCtrl]);
	app.directive('abnTree', ['$timeout', function($timeout) {
		return {
			restrict: 'E',
			template: "<ul  class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">\n  <li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n   " + "<a  ng-hide=\"treeAccessable\" class=\"treesearchcls\"  ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"  class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "<a  ng-show=\"treeAccessable\"  class=\"treesearchcls\" ng-click=\"user_clicks_branch_Expand_Collapse(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"   class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "</div></li>\n</ul>",
			scope: {
				treeData: '=',
				treeFilter: '=',
				onSelect: '&',
				initialSelection: '@',
				accessable: '@',
				treeControl: '='
			},
			link: function(scope, element, attrs) {
				var error, expand_all_parents, expand_level, for_all_ancestors, for_each_branch, get_parent, n, on_treeData_change, select_branch, selected_branch, tree;
				error = function(s) {
					console.log('ERROR:' + s);
					return void 0;
				};
				if (attrs.iconExpand == null) {
					attrs.iconExpand = 'icon-caret-right';
				}
				if (attrs.iconCollapse == null) {
					attrs.iconCollapse = 'icon-caret-down';
				}
				if (attrs.iconLeaf == null) {
					attrs.iconLeaf = 'icon-fixed-width icon-blank';
				}
				if (attrs.expandLevel == null) {
					attrs.expandLevel = '100';
				}
				expand_level = parseInt(attrs.expandLevel, 10);
				if (!scope.treeData) {
					console.log('no treeData defined for the tree!');
					return;
				}
				if (scope.treeData.length == null) {
					if (treeData.Caption != null) {
						scope.treeData = [treeData];
					} else {
						console.log('treeData should be an array of root branches');
						return;
					}
				}
				for_each_branch = function(f) {
					var do_f, root_branch, _i, _len, _ref, _results;
					do_f = function(branch, level) {
						var child, _i, _len, _ref, _results;
						f(branch, level);
						if (branch.Children != null) {
							_ref = branch.Children;
							_results = [];
							for (_i = 0, _len = _ref.length; _i < _len; _i++) {
								child = _ref[_i];
								_results.push(do_f(child, level + 1));
							}
							return _results;
						}
					};
					_ref = scope.treeData;
					_results = [];
					if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
						root_branch = _ref[_i];
						_results.push(do_f(root_branch, 1));
					}
					return _results;
				};
				selected_branch = null;
				select_branch = function(branch, parentArr) {
					if (!branch) {
						if (selected_branch != null) {
							selected_branch.selected = false;
						}
						selected_branch = null;
						return;
					}
					if (branch !== undefined) {
						if (selected_branch != null) {
							selected_branch.selected = false;
						}
						branch.selected = true;
						selected_branch = branch;
						expand_all_parents(branch);
						if (branch.onSelect != null) {
							return $timeout(function() {
								var test = [];
								var parent;
								parent = get_parent(branch);
								if (parent != null) {
									test.push(parent);
									scope.parenttester = [];
									var ty = recursiveparent(parent);
									if (ty != undefined) {
										if (ty.length > 0) {
											for (var k = 0, obj; obj = scope.parenttester[k++];) {
												test.push(obj);
											}
										}
									}
								}
								return branch.onSelect(branch, test);
							});
						} else {
							if (scope.onSelect != null) {
								return $timeout(function() {
									var test = [];
									var parent;
									parent = get_parent(branch);
									if (parent != null) {
										test.push(parent);
										scope.parenttester = [];
										var ty = recursiveparent(parent);
										if (ty != undefined) {
											if (ty.length > 0) {
												for (var k = 0, obj; obj = scope.parenttester[k++];) {
													test.push(obj);
												}
											}
										}
									}
									return scope.onSelect({
										branch: branch,
										parent: test
									});
								});
							}
						}
					}
				};
				scope.parenttester = [];
				scope.user_clicks_branch = function(branch) {
					if (branch !== selected_branch) {
						var test = [];
						var parent;
						parent = get_parent(branch);
						if (parent != null) {
							test.push(parent);
							scope.parenttester = [];
							var ty = recursiveparent(parent);
							if (ty != undefined) {
								if (ty.length > 0) {
									for (var k = 0, obj; obj = scope.parenttester[k++];) {
										test.push(obj);
									}
								}
							}
						}
						return select_branch(branch, test);
					}
				};
				scope.filterItem = function(item) {
					if (!scope.treeFilter) {
						item.isShow = true;
						if (item.expanded != undefined) {
							item.expanded = true;
						}
						return true;
					}
					var found = item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase()) != -1;
					if (!found) {
						var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
						angular.forEach(itemColl, function(item) {
							var match = scope.filterItem(item);
							if (match) {
								found = true;
								item.isShow = true;
							}
						});
					}
					return found;
				};

				function collapseOnemptySearch(item) {
					var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
					angular.forEach(itemColl, function(item) {
						item.isShow = true;
					});
				}
				scope.user_clicks_branch_Expand_Collapse = function(branch, event) {
					var test = [];
					var parent;
					parent = get_parent(branch);
					var target = $(event.target);
					if (target.hasClass("tree-icon")) {
						for (var z = 0, childObj; childObj = branch.Children[z++];) {
							childObj.isShow = branch.expanded;
							Expandcollapse(childObj.Children, branch.expanded);
						}
						return false;
					}
				};
				scope.user_clicks_branch_1 = function(branch, event) {
					var test = [];
					var parent;
					parent = get_parent(branch);
					var target = $(event.target);
					if (target.hasClass("tree-icon")) {
						branch.ischecked = !branch.ischecked
						for (var z = 0, childObj; childObj = branch.Children[z++];) {
							childObj.isShow = branch.expanded;
							Expandcollapse(childObj.Children, branch.expanded);
						}
						return false;
					} else {
						branch.ischecked = branch.ischecked;
						return select_branch(branch, test);
					}
				};

				function Expandcollapse(Children, expanded) {
					for (var z = 0, childObj; childObj = Children[z++];) {
						childObj.isShow = expanded;
						if (childObj.Children.length > 0) {
							Expandcollapse(childObj.Children, expanded);
						}
					}
				}

				function recursiveparent(parent) {
					var parent1;
					parent1 = get_parent(parent);
					if (parent1 != null && parent1 != undefined) {
						scope.parenttester.push(parent1);
						recursiveparent(parent1);
					}
					return scope.parenttester;
				}
				get_parent = function(child) {
					var parent;
					parent = void 0;
					if (child.parent_uid) {
						for_each_branch(function(b) {
							if (b.uid === child.parent_uid) {
								return parent = b;
							}
						});
					}
					return parent;
				};
				for_all_ancestors = function(child, fn) {
					var parent;
					parent = get_parent(child);
					if (parent != null) {
						fn(parent);
						return for_all_ancestors(parent, fn);
					}
				};
				expand_all_parents = function(child) {
					return for_all_ancestors(child, function(b) {
						return b.expanded = true;
					});
				};
				scope.tree_rows = [];
				scope.treeAccessable = attrs.accessable != null ? (attrs.accessable == "false" ? false : true) : false;
				on_treeData_change = function() {
					var add_branch_to_list, root_branch, _i, _len, _ref, _results;
					for_each_branch(function(b, level) {
						if (!b.uid) {
							return b.uid = "" + Math.random();
						}
					});
					for_each_branch(function(b) {
						var child, _i, _len, _ref, _results;
						if (angular.isArray(b.Children)) {
							_ref = b.Children;
							_results = [];
							for (_i = 0, _len = _ref.length; _i < _len; _i++) {
								child = _ref[_i];
								_results.push(child.parent_uid = b.uid);
							}
							return _results;
						}
					});
					scope.tree_rows = [];
					for_each_branch(function(branch) {
						var child, f;
						if (branch.Children) {
							if (branch.Children.length > 0) {
								f = function(e) {
									if (typeof e === 'string') {
										return {
											Caption: e,
											Children: []
										};
									} else {
										return e;
									}
								};
								return branch.Children = (function() {
									var _i, _len, _ref, _results;
									_ref = branch.Children;
									_results = [];
									for (_i = 0, _len = _ref.length; _i < _len; _i++) {
										child = _ref[_i];
										_results.push(f(child));
									}
									return _results;
								})();
							}
						} else {
							return branch.Children = [];
						}
					});
					add_branch_to_list = function(level, branch, visible) {
						var child, child_visible, tree_icon, _i, _len, _ref, _results;
						if (branch.expanded == null) {
							branch.expanded = false;
						}
						if (!branch.Children || branch.Children.length === 0) {
							tree_icon = attrs.iconLeaf;
						} else {
							if (branch.expanded) {
								tree_icon = attrs.iconCollapse;
							} else {
								tree_icon = attrs.iconExpand;
							}
						}
						scope.tree_rows.push({
							level: level,
							branch: branch,
							Caption: branch.Caption,
							tree_icon: tree_icon,
							visible: visible,
							ischecked: branch.ischecked != undefined ? branch.ischecked : false,
							isShow: branch.isShow != undefined ? branch.isShow : true,
						});
						if (branch.Children != null) {
							_ref = branch.Children;
							_results = [];
							for (_i = 0, _len = _ref.length; _i < _len; _i++) {
								child = _ref[_i];
								child_visible = visible && branch.expanded;
								_results.push(add_branch_to_list(level + 1, child, child_visible));
							}
							return _results;
						}
					};
					_ref = scope.treeData;
					_results = [];
					if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
						root_branch = _ref[_i];
						_results.push(add_branch_to_list(1, root_branch, true));
					}
					return _results;
				};
				scope.$watch('treeData', on_treeData_change, true);
				if (attrs.initialSelection != null) {
					for_each_branch(function(b) {
						if (b.Caption === attrs.initialSelection) {
							var test = [];
							var parent;
							parent = get_parent(branch);
							if (parent != null) {
								test.push(parent);
								scope.parenttester = [];
								var ty = recursiveparent(parent);
								if (ty != undefined) {
									if (ty.length > 0) {
										for (var k = 0, obj; obj = scope.parenttester[k++];) {
											test.push(obj);
										}
									}
								}
							}
							return $timeout(function() {
								return select_branch(b, test);
							});
						}
					});
				}
				n = scope.treeData.length;
				for_each_branch(function(b, level) {
					b.level = level;
					return b.expanded = b.level < expand_level;
				});
				if (scope.treeControl != null) {
					if (angular.isObject(scope.treeControl)) {
						tree = scope.treeControl;
						tree.expand_all = function() {
							return for_each_branch(function(b, level) {
								return b.expanded = true;
							});
						};
						tree.collapse_all = function() {
							return for_each_branch(function(b, level) {
								return b.expanded = false;
							});
						};
						tree.get_first_branch = function() {
							n = scope.treeData.length;
							if (n > 0) {
								return scope.treeData[0];
							}
						};
						tree.select_first_branch = function() {
							var b;
							b = tree.get_first_branch();
							var test = [];
							var parent;
							parent = get_parent(branch);
							if (parent != null) {
								test.push(parent);
								scope.parenttester = [];
								var ty = recursiveparent(parent);
								if (ty != undefined) {
									if (ty.length > 0) {
										for (var k = 0, obj; obj = scope.parenttester[k++];) {
											test.push(obj);
										}
									}
								}
							}
							return tree.select_branch(b, test);
						};
						tree.get_selected_branch = function() {
							return selected_branch;
						};
						tree.get_parent_branch = function(b) {
							return get_parent(b);
						};
						tree.select_branch = function(b, parentArr) {
							var test = [];
							var parent;
							parent = get_parent(branch);
							if (parent != null) {
								test.push(parent);
								scope.parenttester = [];
								var ty = recursiveparent(parent);
								if (ty != undefined) {
									if (ty.length > 0) {
										for (var k = 0, obj; obj = scope.parenttester[k++];) {
											test.push(obj);
										}
									}
								}
							}
							select_branch(b, test);
							return b;
						};
						tree.get_children = function(b) {
							return b.Children;
						};
						tree.select_parent_branch = function(b) {
							var p;
							if (b == null) {
								b = tree.get_selected_branch();
							}
							if (b != null) {
								p = tree.get_parent_branch(b);
								var test = [];
								var parent;
								parent = get_parent(branch);
								if (parent != null) {
									test.push(parent);
									scope.parenttester = [];
									var ty = recursiveparent(parent);
									if (ty != undefined) {
										if (ty.length > 0) {
											for (var k = 0, obj; obj = scope.parenttester[k++];) {
												test.push(obj);
											}
										}
									}
								}
								if (p != null) {
									tree.select_branch(p, test);
									return p;
								}
							}
						};
						tree.add_branch = function(parent, new_branch) {
							if (parent != null) {
								parent.Children.push(new_branch);
								parent.expanded = true;
							} else {
								scope.treeData.push(new_branch);
							}
							return new_branch;
						};
						tree.add_root_branch = function(new_branch) {
							tree.add_branch(null, new_branch);
							return new_branch;
						};
						tree.expand_branch = function(b) {
							if (b == null) {
								b = tree.get_selected_branch();
							}
							if (b != null) {
								b.expanded = true;
								return b;
							}
						};
						tree.collapse_branch = function(b) {
							if (b == null) {
								b = selected_branch;
							}
							if (b != null) {
								b.expanded = false;
								return b;
							}
						};
					}
				}
			}
		};
	}]);
	app.directive('treecontext', [function() {
		return {
			restrict: 'A',
			compile: function compile(tElement, tAttrs, transclude, scope) {
				return {
					post: function postLink(scope, iElement, iAttrs, controller) {
						var ul = $('#' + iAttrs.treecontext),
							last = null;
						var id = $('#' + iAttrs.treecontext);
						ul.css({
							'display': 'none'
						});
						$(iElement).click(function(event) {
							event.stopPropagation();
							var top = event.clientY + 10;
							if ((ul.height() + top + 14) > $(window).height()) {
								top = top - (ul.height() + 50);
							} else {}
							ul.css({
								display: "block",
								zIndex: 999999
							});
							var target = $(event.target);
							last = event.timeStamp;
							if (event.stopPropagation) event.stopPropagation();
							if (event.preventDefault) event.preventDefault();
							event.cancelBubble = true;
							event.returnValue = false;
							jQuery('#' + iAttrs.treecontext).show().appendTo(jQuery(this).parent())
						});
						$(document).click(function(event) {
							var target = $(event.target);
							if (target.hasClass("treesearchcls")) {
								return;
							}
							if (!target.is(".popover") && !target.parents().is(".popover")) {
								if (last === event.timeStamp) return;
								ul.css({
									'display': 'none'
								});
								jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
							}
						});
						$(window).scroll(function(event) {
							var target = $(event.target);
							if (!target.is(".popover") && !target.parents().is(".popover")) {
								if (last === event.timeStamp) return;
								ul.css({
									'display': 'none'
								});
								jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
							}
						});
					}
				};
			}
		};
	}]);
	app.directive('euTree', ['$compile', function($compile) {
		return {
			restrict: 'E',
			link: function(scope, element, attrs) {
				scope.selectedNode = null;
				var treeformflag = false;
				var treenodetree = new Array();

				function CallBackTreeStructure(treeobj) {
					treenodetree = new Array();
					processrecords(treeobj);
					var dtstring = treenodetree.join("");
					return dtstring;
				}

				function processrecords(treeobj) {
					if (treeobj != undefined) {
						for (var i = 0, node; node = treeobj[i++];) {
							if (node.ischecked == true) {
								if (treenodetree.length > 0) if (treenodetree[treenodetree.length - 1] != "[") treenodetree.push(",");
								treenodetree.push(node.Caption);
								treeformflag = false;
								if (ischildSelected(node.Children)) {
									treenodetree.push("[");
									processrecords(node.Children);
									treenodetree.push("]");
								} else {
									processrecords(node.Children);
								}
							} else processrecords(node.Children);
						}
					}
				}

				function ischildSelected(children) {
					for (var j = 0, child; child = children[j++];) {
						if (child.ischecked == true) {
							treeformflag = true;
							return treeformflag
						}
					}
					return treeformflag;
				}
				scope.$watch(attrs.treeData, function(val) {
					var treestring = '';
					treenodetree = new Array();
					treestring = (attrs.fileid == undefined || attrs.fileid == null) ? CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.nodeAttributeid]) : CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.fileid + attrs.nodeAttributeid]);
					var template = '';
					var placeoftree = (attrs.treeplace != null || attrs.treeplace != undefined) ? attrs.treeplace : "inline";
					if (placeoftree === "detail") template = angular.element('<a href="javscript:void(0)" class="treeAttributeSelectionText">' + treestring + '</a>');
					else template = angular.element('<span class="treeAttributeSelectionText">' + treestring + '</span>');
					var linkFunction = $compile(template);
					linkFunction(scope);
					element.html(null).append(template);
				}, true);
			}
		};
	}]);
	app.directive('multidropdownpercentage', function() {
		return {
			restrict: 'A',
			transclude: 'element',
			priority: 1000,
			terminal: true,
			compile: function(tElement, tAttrs, transclude) {
				var repeatExpr, childExpr, rootExpr, childrenExpr, branchExpr;
				repeatExpr = tAttrs.multidropdownpercentage.match(/^(.*) in ((?:.*\.)?(.*)) at (.*)$/);
				childExpr = repeatExpr[1];
				rootExpr = repeatExpr[2];
				childrenExpr = repeatExpr[3];
				branchExpr = repeatExpr[4];
				return function link(scope, element, attrs) {
					var rootElement = element[0].parentNode,
						cache = [];

					function lookup(child) {
						var i = cache.length;
						while (i--) {
							if (cache[i].scope[childExpr] === child) {
								return cache.splice(i, 1)[0];
							}
						}
					}
					scope.$watch(rootExpr, function(root) {
						var currentCache = [];
						(function walk(children, parentNode, parentScope, depth) {
							var i = 0,
								n = children != undefined ? children.length : 0,
								last = n - 1,
								cursor, child, cached, childScope, grandchildren;
							for (; i < n; ++i) {
								cursor = parentNode.childNodes[i];
								child = children[i];
								cached = lookup(child);
								if (cached && cached.parentScope !== parentScope) {
									cache.push(cached);
									cached = null;
								}
								if (!cached) {
									transclude(parentScope.$new(), function(clone, childScope) {
										childScope[childExpr] = child;
										cached = {
											scope: childScope,
											parentScope: parentScope,
											element: clone[0],
											branch: clone.find(branchExpr)[0]
										};
										parentNode.insertBefore(cached.element, cursor);
									});
								} else if (cached.element !== cursor) {
									parentNode.insertBefore(cached.element, cursor);
								}
								childScope = cached.scope;
								childScope.$depth = depth;
								childScope.$index = i;
								childScope.$first = (i === 0);
								childScope.$last = (i === last);
								childScope.$middle = !(childScope.$first || childScope.$last);
								currentCache.push(cached);
								grandchildren = child[childrenExpr];
								if (grandchildren && grandchildren.length) {
									walk(grandchildren, cached.branch, childScope, depth + 1);
								}
							}
						})(root, rootElement, scope, 0);
						var i = cache.length;
						while (i--) {
							var cached = cache[i];
							if (cached.scope) {
								cached.scope.$destroy();
							}
							if (cached.element) {
								cached.element.parentNode.removeChild(cached.element);
							}
						}
						cache = currentCache;
					}, true);
				};
			}
		};
	});
	app.directive('numbersOnly', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function(inputValue) {
					if (inputValue == undefined) return ''
					if (inputValue == "00" || inputValue == "000") {
						inputValue == "0";
						modelCtrl.$setViewValue("0");
						modelCtrl.$render();
					};
					var transformedInput = inputValue.replace(/[^0-9]/g, '');
					if (transformedInput != inputValue) {
						modelCtrl.$setViewValue(transformedInput);
						modelCtrl.$render();
					}
					return transformedInput;
				});
			}
		};
	});
	app.directive('drowdowntreepercentagemultiselection', ['$compile', '$timeout', function($compile, $timeout) {
		return {
			priority: 100,
			terminal: true,
			compile: function compile(scope, tElement, tAttrs, transclude) {
				return {
					pre: function preLink(scope, iElement, iAttrs, controller) {
						var htmlText = "";
						var levels = [];
						var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
						var directiveObj = scope.DropDownTreePricing["AttributeId_Levels_" + parseInt(iAttrs.attributeid) + ""];
						if (directiveObj != null) {
							htmlText += '  <div id="DrpdwnPrice" ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + (iAttrs.attributeid) + '_0\')\" data-role="formpercentagetotalcontainer"> ';
							htmlText += '                        <ol  class="oltreelis"> ';
							htmlText += '                            <li  multidropdownpercentage="child in DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + ' at ol"> ';
							htmlText += '                                <div class="control-group"> ';
							htmlText += '                                    <label class="control-label">{{child.LevelName}}</label> ';
							htmlText += '                                    <div class="controls"> ';
							htmlText += '                                        <div> ';
							htmlText += '                                            <select ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
							htmlText += '                                                <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
							htmlText += '                                            </select> ';
							htmlText += '                                        </div> ';
							htmlText += '                                    </div> ';
							htmlText += '                                </div> ';
							htmlText += '                                <form class="form-horizontal">';
							htmlText += '                                   <div class="oltreelis-child"> ';
							htmlText += '                                       <div ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '"> ';
							htmlText += '                                           <div class="control-group" ng-repeat="val in  getPricingRecords(child.LevelOptions,child.selection) track by $index"> ';
							htmlText += '                                               <label class="control-label">{{val.caption}}:</label> ';
							htmlText += '                                               <div class="controls"> ';
							htmlText += '                                                   <input ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '"  type="text" ng-model="val.value" ng-blur="AutoAssignPercentage(child.LevelOptions,child.selection)" max="100" min="0" size="3" maxlength="3" name="txt" numbers-only="numbers-only"> ';
							htmlText += '                                                   <span class="perc">%</span> ';
							htmlText += '                                               </div> ';
							htmlText += '                                           </div> ';
							htmlText += '                                       </div> ';
							htmlText += '                                       <div data-role="percentagetotalcontainer" ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '" class="oltreelis-childTotal"> ';
							htmlText += '                                           <span data-selection="{{child.selection.length}}" data-isnotfilter="{{PercentageVisibleSettings.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '}}" data-ispercentage="{{child.isperc}}" data-role="percentageerror" ng-class="ErrorPercentageClass(child.LevelOptions)">{{ getPercentageTotal(child.LevelOptions) }}</span> ';
							htmlText += '                                           <span class="perc">%</span> ';
							htmlText += '                                       </div> ';
							htmlText += '                                   </div> ';
							htmlText += '                               </form> ';
							htmlText += '                               <ol></ol> ';
							htmlText += '                            </li> ';
							htmlText += '                        </ol> ';
							htmlText += '               </div> ';
							var tpl = htmlText;
							iElement.html(tpl);
							$compile(iElement.contents())(scope);
						}
						scope.OptionManipulation = function(attributeid, attributeLevel, totallevels, attributetypeid) {
							$timeout(function() {
								var attributeLevelOptions = [];
								attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
									return e.level == attributeLevel;
								}));
								if (attributeLevelOptions[0] != undefined) {
									if (attributeLevelOptions[0].selection != undefined) {
										if (attributeLevelOptions[0].selection.length > 0) {
											try {
												var valueMatches = [];
												if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
													return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
												});
												else valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
												scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
											} catch (e) {}
											for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
												if (attributeLevel <= totallevels) {
													if ((attributeLevel + 1) <= totallevels) {
														var attributenextLevelOptions = [];
														attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
															return e.level == (attributeLevel + 1);
														}));
														if (attributenextLevelOptions[0] != undefined) {
															if (attributenextLevelOptions[0].selection != undefined) {
																if (attributenextLevelOptions[0].selection.length > 0) {
																	var deletableIds = new Array();
																	for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
																		var childidObj = [];
																		childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function(e) {
																			return e.NodeId == parseInt(childid);
																		}));
																		if (childidObj.length > 0) {
																			var removableLevelOptions = [];
																			removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function(e) {
																				return e == childidObj[0].LevelParent.toString();
																			}));
																			if (removableLevelOptions.length == 0) {
																				childidObj[0].value = "";
																				deletableIds.push(childid.toString());
																			}
																		}
																	}
																	for (var l = 0, id; id = deletableIds[l++];) {
																		attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
																	}
																}
															}
														}
													}
												}
											}
										} else {
											try {
												var valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
											} catch (e) {}
											if (attributeLevel <= totallevels) {
												if ((attributeLevel + 1) <= totallevels) {
													var attributenextLevelOptions = [];
													attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
														return e.level == (attributeLevel + 1);
													}));
													if (attributenextLevelOptions[0] != undefined) {
														if (attributenextLevelOptions[0].selection != undefined) {
															if (attributenextLevelOptions[0].selection.length > 0) {
																attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
															}
														}
													}
												}
											}
										}
										if (attributeLevel <= totallevels) {
											if ((attributeLevel + 1) <= totallevels) {
												$timeout(function() {
													scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
												}, 100)
											}
										}
									}
								}
							}, 100);
						};
						scope.getPercentageTotal = function(leveloption) {
							var total = 0;
							for (var i = 0, obj; obj = leveloption[i++];) {
								total += parseInt(obj.value != "" ? obj.value : 0);
							}
							return total;
						}
						scope.ErrorPercentageClass = function(leveloptions) {
							var remainvalue = 0;
							remainvalue = scope.getPercentageTotal(leveloptions);
							if (remainvalue > 100) return "result lapse"
							else if (remainvalue < 100) return "result lapse"
							else return "";
						}
						scope.AutoAssignPercentage = function(leveloption, levelSelection) {
							if (levelSelection.length > 1) {
								var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
								emptyBoxes = ($.grep(leveloption, function(e) {
									return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
								}));
								if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
									fillObj = emptyBoxes[emptyBoxes.length - 1];
									if (fillObj != null) {
										remainvalue = 100 - scope.getPercentageTotal(leveloption);
										if (remainvalue > 0) fillObj.value = remainvalue.toString();
										if (remainvalue == 0) {
											var val = 0;
											fillObj.value = val.toString();
										}
									}
								}
							}
						};
						scope.getOptions = function(level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
							if (level == 1) return currentoptions;
							else {
								var matches = [];
								if (previousoptions != undefined) {
									if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
										return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
									});
								}
								return matches;
							}
						};
						scope.getPricingRecords = function(currentoptions, currentselection) {
							var matches = [];
							if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
								return currentselection.indexOf(relation.NodeId.toString()) != -1;
							});
							return matches;
						};
					},
					post: function postLink(scope, iElement, iAttrs, controller) {}
				}
			}
		}
	}]);
	app.directive('drowdowntreepercentagemultiselectionasset', ['$compile', '$timeout', function($compile, $timeout) {
		return {
			priority: 100,
			terminal: true,
			compile: function compile(scope, tElement, tAttrs, transclude) {
				return {
					pre: function preLink(scope, iElement, iAttrs, controller) {
						var htmlText = "";
						var levels = [];
						var fileid = iAttrs.fileid;
						var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
						var directiveObj = scope.DropDownTreePricing["AttributeId_Levels_" + fileid + parseInt(iAttrs.attributeid) + ""];
						if (directiveObj != null) {
							htmlText += '  <div data-role="formpercentagetotalcontainer"> ';
							htmlText += '                        <ol  class="oltreelis"> ';
							htmlText += '                            <li  multidropdownpercentage="child in DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + ' at ol"> ';
							htmlText += '                                <div class="control-group"> ';
							htmlText += '                                    <label class="control-label">{{child.LevelName}}</label> ';
							htmlText += '                                    <div class="controls"> ';
							htmlText += '                                        <div> ';
							htmlText += '                                            <select ng-disabled="EnableDisableControlsHolder.Selection_' + fileid + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
							htmlText += '                                                <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownTreePricing.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
							htmlText += '                                            </select> ';
							htmlText += '                                        </div> ';
							htmlText += '                                    </div> ';
							htmlText += '                                </div> ';
							htmlText += '                                <form class="form-horizontal">';
							htmlText += '                                   <div class="oltreelis-child"> ';
							htmlText += '                                       <div ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '"> ';
							htmlText += '                                           <div class="control-group" ng-repeat="val in  getPricingRecords(child.LevelOptions,child.selection) track by $index"> ';
							htmlText += '                                               <label class="control-label">{{val.caption}}:</label> ';
							htmlText += '                                               <div class="controls"> ';
							htmlText += '                                                   <input ng-disabled="EnableDisableControlsHolder.Selection_' + fileid + parseInt(iAttrs.attributeid) + '"  type="text" ng-model="val.value" ng-blur="AutoAssignPercentage(child.LevelOptions,child.selection)" max="100" min="0" size="3" maxlength="3" name="txt" numbers-only="numbers-only"> ';
							htmlText += '                                                   <span class="perc">%</span> ';
							htmlText += '                                               </div> ';
							htmlText += '                                           </div> ';
							htmlText += '                                       </div> ';
							htmlText += '                                       <div data-role="percentagetotalcontainer" ng-show="(child.selection.length > 1 && child.isperc) && PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '" class="oltreelis-childTotal"> ';
							htmlText += '                                           <span data-selection="{{child.selection.length}}" data-isnotfilter="{{PercentageVisibleSettings.AttributeId_Levels_' + fileid + parseInt(iAttrs.attributeid) + '}}" data-ispercentage="{{child.isperc}}" data-role="percentageerror" ng-class="ErrorPercentageClass(child.LevelOptions)">{{ getPercentageTotal(child.LevelOptions) }}</span> ';
							htmlText += '                                           <span class="perc">%</span> ';
							htmlText += '                                       </div> ';
							htmlText += '                                   </div> ';
							htmlText += '                               </form> ';
							htmlText += '                               <ol></ol> ';
							htmlText += '                            </li> ';
							htmlText += '                        </ol> ';
							htmlText += '               </div> ';
							var tpl = htmlText;
							iElement.html(tpl);
							$compile(iElement.contents())(scope);
						}
						scope.OptionManipulation = function(attributeid, attributeLevel, totallevels, attributetypeid) {
							$timeout(function() {
								var attributeLevelOptions = [];
								attributeLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function(e) {
									return e.level == attributeLevel;
								}));
								if (attributeLevelOptions[0] != undefined) {
									if (attributeLevelOptions[0].selection != undefined) {
										if (attributeLevelOptions[0].selection.length > 0) {
											try {
												var valueMatches = [];
												if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
													return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
												});
												else valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
												scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
											} catch (e) {}
											for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
												if (attributeLevel <= totallevels) {
													if ((attributeLevel + 1) <= totallevels) {
														var attributenextLevelOptions = [];
														attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function(e) {
															return e.level == (attributeLevel + 1);
														}));
														if (attributenextLevelOptions[0] != undefined) {
															if (attributenextLevelOptions[0].selection != undefined) {
																if (attributenextLevelOptions[0].selection.length > 0) {
																	var deletableIds = new Array();
																	for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
																		var childidObj = [];
																		childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function(e) {
																			return e.NodeId == parseInt(childid);
																		}));
																		if (childidObj.length > 0) {
																			var removableLevelOptions = [];
																			removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function(e) {
																				return e == childidObj[0].LevelParent.toString();
																			}));
																			if (removableLevelOptions.length == 0) {
																				childidObj[0].value = "";
																				deletableIds.push(childid.toString());
																			}
																		}
																	}
																	for (var l = 0, id; id = deletableIds[l++];) {
																		attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
																	}
																}
															}
														}
													}
												}
											}
										} else {
											try {
												var valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
											} catch (e) {}
											if (attributeLevel <= totallevels) {
												if ((attributeLevel + 1) <= totallevels) {
													var attributenextLevelOptions = [];
													attributenextLevelOptions = ($.grep(scope.DropDownTreePricing["AttributeId_Levels_" + fileid + attributeid + ""], function(e) {
														return e.level == (attributeLevel + 1);
													}));
													if (attributenextLevelOptions[0] != undefined) {
														if (attributenextLevelOptions[0].selection != undefined) {
															if (attributenextLevelOptions[0].selection.length > 0) {
																attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
															}
														}
													}
												}
											}
										}
										if (attributeLevel <= totallevels) {
											if ((attributeLevel + 1) <= totallevels) {
												$timeout(function() {
													scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
												}, 100)
											}
										}
									}
								}
							}, 100);
						};
						scope.getPercentageTotal = function(leveloption) {
							var total = 0;
							for (var i = 0, obj; obj = leveloption[i++];) {
								total += parseInt(obj.value != "" ? obj.value : 0);
							}
							return total;
						}
						scope.ErrorPercentageClass = function(leveloptions) {
							var remainvalue = 0;
							remainvalue = scope.getPercentageTotal(leveloptions);
							if (remainvalue > 100) return "result lapse"
							else if (remainvalue < 100) return "result lapse"
							else return "";
						}
						scope.AutoAssignPercentage = function(leveloption, levelSelection) {
							if (levelSelection.length > 1) {
								var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
								emptyBoxes = ($.grep(leveloption, function(e) {
									return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
								}));
								if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
									fillObj = emptyBoxes[emptyBoxes.length - 1];
									if (fillObj != null) {
										remainvalue = 100 - scope.getPercentageTotal(leveloption);
										if (remainvalue > 0) fillObj.value = remainvalue.toString();
										if (remainvalue == 0) {
											var val = 0;
											fillObj.value = val.toString();
										}
									}
								}
							}
						};
						scope.getOptions = function(level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
							if (level == 1) return currentoptions;
							else {
								var matches = [];
								if (previousoptions != undefined) {
									if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
										return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
									});
								}
								return matches;
							}
						};
						scope.getPricingRecords = function(currentoptions, currentselection) {
							var matches = [];
							if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
								return currentselection.indexOf(relation.NodeId.toString()) != -1;
							});
							return matches;
						};
					},
					post: function postLink(scope, iElement, iAttrs, controller) {}
				}
			}
		}
	}]);
	app.directive('drowdowntreepercentagemultiselectionfilter', ['$compile', '$timeout', function($compile, $timeout) {
		return {
			priority: 100,
			terminal: true,
			compile: function compile(scope, tElement, tAttrs, transclude) {
				return {
					pre: function preLink(scope, iElement, iAttrs, controller) {
						var htmlText = "";
						var levels = [];
						var isforfilter = iAttrs.purpose != null ? (iAttrs.purpose == "filter" ? false : true) : true;
						var directiveObj = scope.DropDownFilterTreePricing["AttributeId_Levels_" + parseInt(iAttrs.attributeid) + ""];
						if (directiveObj != null) {
							htmlText += '  <div data-role="formpercentagetotalcontainer" class=\"control-group\"> ';
							htmlText += '         <div   ng-repeat ="child in DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '"> ';
							htmlText += '               <label class="control-label">{{child.LevelName}}</label> ';
							htmlText += '                <div class="controls"> ';
							htmlText += '                      <select ng-disabled="EnableDisableControlsHolder.Selection_' + parseInt(iAttrs.attributeid) + '" ng-change="OptionManipulation(' + parseInt(iAttrs.attributeid) + ',child.level,child.TotalLevel,13)" ui-select2 multiple="multiple" data-placeholder="Select {{child.LevelName}}" ng-model="child.selection"> ';
							htmlText += '                              <option ng-repeat="item in getOptions(child.level,child.LevelOptions,child.selection,DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].selection,DropDownFilterTreePricing.AttributeId_Levels_' + parseInt(iAttrs.attributeid) + '[$index - 1].LevelOptions) track by item.NodeId" value="{{item.NodeId}}">{{item.caption}}</option> ';
							htmlText += '                      </select> ';
							htmlText += '                 </div> ';
							htmlText += '         </div> ';
							htmlText += '  </div> ';
							var tpl = htmlText;
							iElement.html(tpl);
							$compile(iElement.contents())(scope);
						}
						scope.OptionManipulation = function(attributeid, attributeLevel, totallevels, attributetypeid) {
							$timeout(function() {
								var attributeLevelOptions = [];
								attributeLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
									return e.level == attributeLevel;
								}));
								if (attributeLevelOptions[0] != undefined) {
									if (attributeLevelOptions[0].selection != undefined) {
										if (attributeLevelOptions[0].selection.length > 0) {
											try {
												var valueMatches = [];
												if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
													return attributeLevelOptions[0].selection.indexOf(relation.NodeId.toString()) == -1;
												});
												else valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
												scope.AutoAssignPercentage(attributeLevelOptions[0].LevelOptions, attributeLevelOptions[0].selection);
											} catch (e) {}
											for (var j = 0, parentid; parentid = attributeLevelOptions[0].selection[j++];) {
												if (attributeLevel <= totallevels) {
													if ((attributeLevel + 1) <= totallevels) {
														var attributenextLevelOptions = [];
														attributenextLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
															return e.level == (attributeLevel + 1);
														}));
														if (attributenextLevelOptions[0] != undefined) {
															if (attributenextLevelOptions[0].selection != undefined) {
																if (attributenextLevelOptions[0].selection.length > 0) {
																	var deletableIds = new Array();
																	for (var k = 0, childid; childid = attributenextLevelOptions[0].selection[k++];) {
																		var childidObj = [];
																		childidObj = ($.grep(attributenextLevelOptions[0].LevelOptions, function(e) {
																			return e.NodeId == parseInt(childid);
																		}));
																		if (childidObj.length > 0) {
																			var removableLevelOptions = [];
																			removableLevelOptions = ($.grep(attributeLevelOptions[0].selection, function(e) {
																				return e == childidObj[0].LevelParent.toString();
																			}));
																			if (removableLevelOptions.length == 0) {
																				childidObj[0].value = "";
																				deletableIds.push(childid.toString());
																			}
																		}
																	}
																	for (var l = 0, id; id = deletableIds[l++];) {
																		attributenextLevelOptions[0].selection.splice($.inArray(id, attributenextLevelOptions[0].selection), 1);
																	}
																}
															}
														}
													}
												}
											}
										} else {
											try {
												var valueMatches = attributeLevelOptions[0].LevelOptions;
												if (valueMatches != undefined) {
													if (valueMatches.length > 0) for (var m = 0, level; level = valueMatches[m++];) {
														level.value = "";
													}
												}
											} catch (e) {}
											if (attributeLevel <= totallevels) {
												if ((attributeLevel + 1) <= totallevels) {
													var attributenextLevelOptions = [];
													attributenextLevelOptions = ($.grep(scope.DropDownFilterTreePricing["AttributeId_Levels_" + attributeid + ""], function(e) {
														return e.level == (attributeLevel + 1);
													}));
													if (attributenextLevelOptions[0] != undefined) {
														if (attributenextLevelOptions[0].selection != undefined) {
															if (attributenextLevelOptions[0].selection.length > 0) {
																attributenextLevelOptions[0].selection.splice(0, attributenextLevelOptions[0].selection.length);
															}
														}
													}
												}
											}
										}
										if (attributeLevel <= totallevels) {
											if ((attributeLevel + 1) <= totallevels) {
												$timeout(function() {
													scope.OptionManipulation(attributeid, (attributeLevel + 1), totallevels, attributetypeid);
												}, 100)
											}
										}
									}
								}
							}, 100);
						};
						scope.getPercentageTotal = function(leveloption) {
							var total = 0;
							for (var i = 0, obj; obj = leveloption[i++];) {
								total += parseInt(obj.value != "" ? obj.value : 0);
							}
							return total;
						}
						scope.ErrorPercentageClass = function(leveloptions) {
							var remainvalue = 0;
							remainvalue = scope.getPercentageTotal(leveloptions);
							if (remainvalue > 100) return "result lapse"
							else if (remainvalue < 100) return "result lapse"
							else return "";
						}
						scope.AutoAssignPercentage = function(leveloption, levelSelection) {
							if (levelSelection.length > 1) {
								var emptyBoxes = [],
									fillObj = [],
									remainvalue = 0;
								emptyBoxes = ($.grep(leveloption, function(e) {
									return (levelSelection.indexOf(e.NodeId.toString()) != -1) && (e.value == null || e.value == undefined || e.value == "");
								}));
								if (emptyBoxes.length > 0 && emptyBoxes.length == 1) {
									fillObj = emptyBoxes[emptyBoxes.length - 1];
									if (fillObj != null) {
										remainvalue = 100 - scope.getPercentageTotal(leveloption);
										if (remainvalue > 0) fillObj.value = remainvalue.toString();
										if (remainvalue == 0) {
											var val = 0;
											fillObj.value = val.toString();
										}
									}
								}
							}
						};
						scope.getOptions = function(level, currentoptions, currentselection, previousoptions, previouslevelOptions) {
							if (level == 1) return currentoptions;
							else {
								var matches = [];
								if (previousoptions != null) if (previousoptions.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
									return previousoptions.indexOf(relation.LevelParent.toString()) != -1;
								});
								return matches;
							}
						};
						scope.getPricingRecords = function(currentoptions, currentselection) {
							var matches = [];
							if (currentselection != null) if (currentselection.length != 0) matches = jQuery.grep(currentoptions, function(relation) {
								return currentselection.indexOf(relation.NodeId.toString()) != -1;
							});
							return matches;
						};
					},
					post: function postLink(scope, iElement, iAttrs, controller) {}
				}
			}
		}
	}]);

})(angular, app);
(function(ng,app){function PlanlistService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetFilterSettings:GetFilterSettings,GetApprvdAlloc:GetApprvdAlloc,IsLockAvailable:IsLockAvailable,MemberAvailable:MemberAvailable,DeleteEntity:DeleteEntity,CheckForMemberAvailabilityForEntity:CheckForMemberAvailabilityForEntity,PendingFundRequest:PendingFundRequest,UpdateLock:UpdateLock,GetPeriodByIdForGantt:GetPeriodByIdForGantt,GetEntityTypeByID:GetEntityTypeByID,ActivityRootLevel:ActivityRootLevel,GetAllEntityIds:GetAllEntityIds,GettingEntityForRootLevel:GettingEntityForRootLevel})
function GetFilterSettings(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettings/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetApprvdAlloc(EntityId){var request=$http({method:"post",url:"api/Planning/GetApprvdAlloc/",params:{action:"add"},data:EntityId});return(request.then(handleSuccess,handleError));}
function IsLockAvailable(formobj){var request=$http({method:"post",url:"api/Planning/IsLockAvailable/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function MemberAvailable(formobj){var request=$http({method:"post",url:"api/Planning/MemberAvailable/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DeleteEntity(formobj){var request=$http({method:"post",url:"api/Planning/DeleteEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CheckForMemberAvailabilityForEntity(EntityID){var request=$http({method:"get",url:"api/Planning/CheckForMemberAvailabilityForEntity/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function PendingFundRequest(EntityID){var request=$http({method:"get",url:"api/Planning/PendingFundRequest/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function UpdateLock(EntityID,IsLock){var request=$http({method:"get",url:"api/Planning/UpdateLock/"+EntityID+"/"+IsLock,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPeriodByIdForGantt(ID){var request=$http({method:"get",url:"api/Planning/GetPeriodByIdForGantt/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeByID(ID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ActivityRootLevel(formobj){var request=$http({method:"post",url:"api/Metadata/ActivityRootLevel/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAllEntityIds(){var request=$http({method:"get",url:"api/Metadata/GetAllEntityIds/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingEntityForRootLevel(IsRootLevel){var request=$http({method:"get",url:"api/Metadata/GettingEntityForRootLevel/"+IsRootLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanlistService",['$http','$q',PlanlistService]);})(angular,app);
(function(ng,app){function PlanfiltersettingsService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GettingEntityForRootLevel:GettingEntityForRootLevel,GetOptionsFromXML:GetOptionsFromXML,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings})
function GettingEntityForRootLevel(IsRootLevel){var request=$http({method:"get",url:"api/Metadata/GettingEntityForRootLevel/"+IsRootLevel,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanfiltersettingsService",['$http','$q',PlanfiltersettingsService]);})(angular,app);
(function(ng,app){function RootentitytypecreationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetPlantabsettings:GetPlantabsettings,GetUserById:GetUserById,GetCostcentreforEntityCreation:GetCostcentreforEntityCreation,CreateEntity:CreateEntity,GetCostcentreTreeforPlanCreation:GetCostcentreTreeforPlanCreation,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,GetAttributeToAttributeRelationsByIDForEntity:GetAttributeToAttributeRelationsByIDForEntity,GetEntityTypeAttributeRelationWithLevelsByID:GetEntityTypeAttributeRelationWithLevelsByID,GetValidationDationByEntitytype:GetValidationDationByEntitytype})
function GetPlantabsettings(){var request=$http({method:"get",url:"api/common/GetPlantabsettings/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostcentreforEntityCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreforEntityCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CreateEntity(formobj){var request=$http({method:"post",url:"api/Planning/CreateEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCostcentreTreeforPlanCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreTreeforPlanCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeToAttributeRelationsByIDForEntity(ID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(ID,ParentID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/"+ID+"/"+ParentID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetValidationDationByEntitytype(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetValidationDationByEntitytype/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("RootentitytypecreationService",['$http','$q',RootentitytypecreationService]);})(angular,app);
(function (ng, app) {
    "use strict"; function PlanningToolService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ AutoCompleteMemberList: AutoCompleteMemberList, GetCurrencyListFFsettings: GetCurrencyListFFsettings, GetTreeNode: GetTreeNode, GetTreeNodeByEntityID: GetTreeNodeByEntityID, GetOptionDetailListByID: GetOptionDetailListByID, GetOptionDetailListByAssetID: GetOptionDetailListByAssetID, GetDamTreeNode: GetDamTreeNode, GetDamTreeNodeByEntityID: GetDamTreeNodeByEntityID }); function AutoCompleteMemberList(QueryStr, GroupID) { var request = $http({ method: "post", ignoreLoadingBar: true, url: "api/user/AutoCompleteMemberList", data: { QueryString: QueryStr }, params: { action: "add" } }); return (request.then(handleSuccess, handleError)); }
    function GetCurrencyListFFsettings() { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Planning/GetCurrencyListFFsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTreeNode(AttributeID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Metadata/GetTreeNode/" + AttributeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTreeNodeByEntityID(AttributeID, ParentEntityID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Metadata/GetTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetOptionDetailListByID(ID, EntityID) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/Metadata/GetOptionDetailListByID/" + ID + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetOptionDetailListByAssetID(ID, AssetID) { var request = $http({ method: "Get", ignoreLoadingBar: true, url: "api/dam/GetOptionDetailListByAssetID/" + ID + "/" + AssetID + "/", params: { action: "Get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDamTreeNode(AttributeID) { var request = $http({ method: "Get", ignoreLoadingBar: true, url: "api/dam/GetDamTreeNode/" + AttributeID, params: { action: "Get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDamTreeNodeByEntityID(AttributeID, ParentEntityID) { var request = $http({ method: "Get", ignoreLoadingBar: true, url: "api/dam/GetDamTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID, params: { action: "Get" } }); return (request.then(handleSuccess, handleError)); }
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PlanningToolService",['$http','$q',PlanningToolService]);})(angular,app);
