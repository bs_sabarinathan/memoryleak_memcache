using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// ObjectiveRating object for table 'PM_Objective_Rating'.
	/// </summary>
	
	internal class ObjectiveRating : IObjectiveRating 
	{
		#region Member Variables

		protected int _id;
		protected int _objectiveid;
		protected string _caption;
		protected int _sortorder;
		
		
		#endregion
		
		#region Constructors
		public ObjectiveRating() {}
			
		public ObjectiveRating(int pObjectiveid, string pCaption, int pSortOrder)
		{
			this._objectiveid = pObjectiveid; 
			this._caption = pCaption; 
			this._sortorder = pSortOrder; 
		}
				
		public ObjectiveRating(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Objectiveid
		{
			get { return _objectiveid; }
			set { _objectiveid = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public int SortOrder
		{
			get { return _sortorder; }
			set { _sortorder = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			ObjectiveRating castObj = null;
			try
			{
				castObj = (ObjectiveRating)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
	
}
