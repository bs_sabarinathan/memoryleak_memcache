﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultlistCtrl($scope, $timeout, $http, $compile, $resource, $location, $window, $cookies, $cookieStore, $translate, PlanlistService, $modal) {
        $scope.Isfromcalender_mui = false;
        $scope.PageSize = 1;
        $scope.IsSelectAllChecked = false;
        $.cookie('ListofEntityID', new Array());
        $scope.DynamicHeight = $(window).height() - 100;
        $scope.IsDuplicate = false;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.SelectedFilterID = 0;
        $scope.AddEditFilter = "Add new filter";
        PlanlistService.GetFilterSettings(6).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () {
            angular.element(document).ready(function () {
                $scope.$broadcast('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
            });
        }, 1000);
        $scope.DetailFilterCreation = function (event) {
            $scope.IsDuplicate = false;
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $scope.deletefiltershow = false;
                $("#rootlevelfilter").trigger('ClearScope', $scope.EntityTypeID);
            } else {
                setTimeout(function () {
                    $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 6]);
                }, 20);
                $scope.showSave = false;
                $scope.showUpdate = true;
                $('.FilterHolder').slideDown("slow")
            }
        };
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $scope.ApplyFilter = function (filterid, filtername) {
            $scope.IsDuplicate = false;
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $('.FilterHolder').slideUp("slow")
            } else {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            $("#rootlevelfilter").trigger('ApplyRootLevelFilter', [filterid, filtername]);
        }
        $("#Div_list").on('ReloadFilterSettings', function (event, TypeID, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
                $("#rootlevelfilter").trigger('EditFilterSettingsByFilterID', [filterid, 6]);
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
            }
            PlanlistService.GetFilterSettings(TypeID).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
            });
        });
        $("#Div_list").on('ClearAndReApply', function () {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = "Add new filter";
        });
        $scope.Level = "0";
        PlanlistService.GetEntityTypeByID(6).then(function (name) {
            if (name.Response != null) {
                $scope.activityname = name.Response[0].Caption;
                $scope.rootentitytypename = $scope.activityname + "s";
            }
        });
        $scope.FilterID = {
            selectedFilterID: 0,
            selectedfilterattribtues: []
        };
        $scope.AppllyFilterObj = {
            selectedattributes: []
        };
        var TypeID = 6;
        $scope.EntityTypeID = 6;
        $scope.sortOrder = "null";
        $scope.IsDesc = false;
        var CurrentuserId = $scope.UserId;
        $scope.listColumnDefsdata = {};
        $scope.NameExcludeFilter = function (columndefs) {
            if (columndefs.Field != "68") {
                return true;
            }
        };
        $scope.load = function (parameters) { };
        if (ShowAllRemember.Activity == true) {
            $scope.DisablePart = true;
            CurrentuserId = $scope.UserId;
            $("#btnParticipated").removeClass("active").addClass("active");
            $("#btnViewAll").removeClass("active");
        } else {
            $scope.DisablePart = false;
            CurrentuserId = 0;
            $("#btnViewAll").removeClass("active").addClass("active");
            $("#btnParticipated").removeClass("active");
        }
        $scope.tglHide = true;
        $scope.ShowAll = function (IsActive) {
            $scope.IsDuplicate = false;
            $scope.DisablePart = !$scope.DisablePart;
            $("#ListContainer table").html('');
            $("#treeHolder ul").html('');
            $('#RootLevelSelectAll').next('i').removeClass('checked');
            ClearCheckBoxes()
            if (ShowAllRemember.Activity == true) {
                CurrentuserId = $scope.UserId
            }
            if (IsActive) {
                $("#btnViewAll").removeClass("active").addClass("active");
                $("#btnParticipated").removeClass("active");
                CurrentuserId = 0;
                ShowAllRemember.Activity = false;
            } else {
                $("#btnParticipated").removeClass("active").addClass("active");
                $("#btnViewAll").removeClass("active");
                CurrentuserId = $scope.UserId;
                ShowAllRemember.Activity = true;
            }
            GetRootLevelActivityListCount();
            if ($("#ListColumn > thead tr th").length == 1)
                $('#ListColumn > thead').html("");
        };

        function ClearCheckBoxes() {
            $scope.IsSelectAllChecked = false;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = false;
                $(this).next('i').removeClass('checked');
            });
        }
        $scope.DeleteEntity = function () {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var data = {};
            data.Planlistids = IDList.join(",");
            PlanlistService.GetApprvdAlloc(data).then(function (result) {
                $scope.ApprovedAlloc = result.Response;
                if (parseInt($scope.ApprovedAlloc) > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5721.Caption'));
                } else {
                    $scope.IsDuplicate = false;
                    var IDList = new Array();
                    var ID = 0;
                    IDList = GetRootLevelSelectedAll();
                    if (IDList.length != 0) {
                        var object = {};
                        object.ID = IDList;
                        var lockavailablitiy = {};
                        lockavailablitiy.ID = IDList;
                        PlanlistService.IsLockAvailable(lockavailablitiy).then(function (lock) {
                            if (lock.Response == true) {
                                bootbox.alert($translate.instant('LanguageContents.Res_4915.Caption'));
                                return true;
                            }
                            PlanlistService.MemberAvailable(object).then(function (memberavailability) {
                                if (memberavailability.Response == false) {
                                    bootbox.alert($translate.instant('LanguageContents.Res_2100.Caption'));
                                    return true;
                                }
                                $scope.alert = $translate.instant('LanguageContents.Res_16.Caption');
                                bootbox.confirm($scope.alert, function (result) {
                                    if (result) {
                                        setTimeout(function () {
                                            var deletesearch = {};
                                            deletesearch.ID = IDList;
                                            PlanlistService.DeleteEntity(deletesearch).then(function (result) {
                                                if (result.Response == true) {
                                                    for (var i = 0; i < IDList.length; i++) {
                                                        var entityId = IDList[i];
                                                        $('#treeHolder a[data-id=' + entityId + ']').remove();
                                                        $('#ListContainer tr[data-id=' + entityId + ']').remove();
                                                    }
                                                    $('#RootLevelSelectAll').next('i').removeClass('checked');
                                                    ClearCheckBoxes();
                                                }
                                            });
                                        }, 100);
                                    }
                                });
                            });
                        });
                    }
                }
            });
        }
        $scope.DuplicateRootLevelEntities = function () {
            var IDList = new Array();
            var ID = 0;
            IDList = GetRootLevelSelectedAll();
            if (IDList.length != 0) {
                var lockavailablitiy = {};
                lockavailablitiy.ID = IDList;
                PlanlistService.IsLockAvailable(lockavailablitiy).then(function (lock) {
                    if (lock.Response == true) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4916.Caption'));
                        return true;
                    }
                    $('#DuplicateModels').modal('show');
                    $scope.DuplicateEntity(IDList, 0, "LIST");
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1914.Caption'))
            }
        }
        $scope.$on('LISTEVENT', function (event) {
            $scope.IsDuplicate = true;
            GetRootLevelActivityListCount();
        });

        function SelectEntityIDs() {
            var IDList = new Array();
            $('#listdataHolder input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }
        var IsDesc = false;
        $scope.FilterID.selectedFilterID = 0;
        $scope.FilterID.selectedfilterattribtues = [];
        var sortOrder = "";
        $scope.LoadRootLevelActivity = function (PageIndex, StartRowNo, MaxNoofRow, filterid, filterattributes) {
            var FilterID = filterid;
            var planAttrs = $cookieStore.get('planAttrs' + parseInt($cookies['UserId'], 10));
            var planFilterID = $cookieStore.get('planFilterID' + parseInt($cookies['UserId'], 10));
            var planFilterName = $cookieStore.get('planFilterName' + parseInt($cookies['UserId'], 10));
            RemeberPlanFilterName = planFilterName;
            if (planAttrs != undefined) {
                RememberPlanFilterAttributes = planAttrs;
            }
            if (planFilterID != undefined) {
                RememberPlanFilterID = planFilterID;
            }
            if (RememberPlanFilterID != 0 || RememberPlanFilterAttributes.length != 0) {
                FilterID = RememberPlanFilterID;
                filterattributes = RememberPlanFilterAttributes;
                if (RememberPlanFilterAttributes.length > 0) { }
                if (RememberPlanFilterID != 0) {
                    $scope.appliedfilter = RemeberPlanFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = FilterID;
                }
            }
            if (StartRowNo == 0)
                $timeout(function () {
                    $scope.$broadcast('LoadRootLevelFilter', [5, RememberPlanFilterAttributes]);
                }, 100);
            $(window).AdjustHeightWidth();
            $scope.FilterID.selectedFilterID = FilterID;
            $scope.FilterID.selectedfilterattribtues = filterattributes;
            $window.ActivityFilterName = '';
            var TypeID = 6;
            var getactivities = {};
            getactivities.StartRowNo = StartRowNo;
            getactivities.MaxNoofRow = MaxNoofRow;
            getactivities.FilterID = FilterID;
            getactivities.SortOrderColumn = $scope.sortOrder;
            getactivities.IsDesc = $scope.IsDesc;
            getactivities.IncludeChildren = false;
            getactivities.EntityID = '0';
            getactivities.UserID = CurrentuserId;
            getactivities.Level = $scope.Level;
            getactivities.IsDuplicate = $scope.IsDuplicate;
            getactivities.IDArr = [];
            getactivities.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            PlanlistService.ActivityRootLevel(getactivities).then(function (getactivitylist) {
                if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                    if (getactivitylist.Response.ColumnDefs.length > 0) {
                        var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                        var listContent = getactivitylist.Response.Data;
                        var contentnHtml = "";
                        var columnHtml = "<tr >";
                        var treeHtml = "";
                        var colStatus = false;
                        for (var i = 0; i < listContent.length; i++) {
                            if (i != undefined) {
                                contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                for (var j = 0; j < listColumnDefsdata.length; j++) {
                                    if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                        if (colStatus == false) {
                                            columnHtml += "<th>";
                                            columnHtml += "<a  data-Column=" + listColumnDefsdata[j].Field + " >";
                                            columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                            columnHtml += "</a>";
                                            columnHtml += "</th>";
                                        }
                                        if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                        else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type)
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] == null || "          " ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + "</span></td>";
                                        else
                                            contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                    }
                                }
                                treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                var status = "";
                                if (listContent[i].IsLock == true) {
                                    contentnHtml += "<td><span class='pull-right'><i class='icon-lock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                } else {
                                    contentnHtml += "<td><span class='pull-right'><i class='icon-unlock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox " + ($scope.IsSelectAllChecked ? 'checked' : '') + "'></i></label></span></td>";
                                }
                                contentnHtml += "</tr>";
                                colStatus = true;
                            }
                        }
                        if (PageIndex == 0) {
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                            $('#ListContainer').scrollTop(0);
                        }
                        if ($("#ListColumn > thead tr").length < 1) {
                            columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                            columnHtml += "</tr>";
                            $('#ListColumn > thead').html(columnHtml);
                        }
                        if ($("#ListColumn > thead tr").length > 0) {
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                            $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                            $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                            ListCheckBoxitemClick();
                        }
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));
                }
            });
        }
        GetRootLevelActivityListCount();

        function GetRootLevelActivityListCount() {
            var planAttrs = $cookieStore.get('planAttrs' + parseInt($cookies['UserId'], 10));
            var planFilterID = $cookieStore.get('planFilterID' + parseInt($cookies['UserId'], 10));
            var planFilterName = $cookieStore.get('planFilterName' + parseInt($cookies['UserId'], 10));
            RemeberPlanFilterName = planFilterName;
            if (planAttrs != undefined) {
                RememberPlanFilterAttributes = planAttrs;
            }
            if (planFilterID != undefined) {
                RememberPlanFilterID = planFilterID;
            }
            if (RememberPlanFilterID != 0 || RememberPlanFilterAttributes.length != 0) {
                $scope.FilterID.selectedFilterID = RememberPlanFilterID;
                $scope.FilterID.selectedfilterattribtues = RememberPlanFilterAttributes;
                if (RememberPlanFilterAttributes.length > 0) { }
                if (RememberPlanFilterID != 0) {
                    $scope.appliedfilter = RemeberPlanFilterName;
                    $scope.AddEditFilter = 'Edit filter';
                    $scope.SelectedFilterID = $scope.FilterID.selectedFilterID;
                }
            }
            $scope.noData = false;
            $window.ActivityFilterName = '';
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = $scope.FilterID.selectedFilterID;
            Node.SortOrderColumn = $scope.sortOrder;
            Node.IsDesc = $scope.IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = CurrentuserId;
            Node.Level = $scope.Level;
            Node.IsDuplicate = $scope.IsDuplicate;
            Node.IDArr = [];
            Node.FilterAttributes = $scope.FilterID.selectedfilterattribtues;
            PlanlistService.ActivityRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response != null) {
                    if (getactivitylist.Response.Data != null) {
                        fnPageTemplate(parseInt(getactivitylist.Response.DataCount));
                        var PageIndex = 0;
                        if (getactivitylist.Response.ColumnDefs.length > 0) {
                            var listColumnDefsdata = getactivitylist.Response.ColumnDefs
                            var listContent = getactivitylist.Response.Data;
                            var contentnHtml = "";
                            var columnHtml = "<tr >";
                            var treeHtml = "";
                            var colStatus = false;
                            for (var i = 0; i < listContent.length; i++) {
                                if (i != undefined) {
                                    contentnHtml += "<tr data-over='true' class='ng-scope mo" + listContent[i]["Id"] + "' data-id=" + listContent[i]["Id"] + ">"
                                    for (var j = 0; j < listColumnDefsdata.length; j++) {
                                        if (j != undefined && listColumnDefsdata[j].Field != "68") {
                                            if (colStatus == false) {
                                                columnHtml += "<th>";
                                                columnHtml += "<a data-Column=" + listColumnDefsdata[j].Field + " >";
                                                columnHtml += "    <span>" + listColumnDefsdata[j].DisplayName + "</span>";
                                                columnHtml += "</a>";
                                                columnHtml += "</th>";
                                            }
                                            if (AttributeTypes.Period == listColumnDefsdata[j].Type)
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                            else if (AttributeTypes.DateTime == listColumnDefsdata[j].Type) {
                                                if (listContent[i][listColumnDefsdata[j].Field] == "-")
                                                    contentnHtml += '<td><span>' + "-" + '</span></td>';
                                                else
                                                    contentnHtml += '<td><span>' + (listContent[i][listColumnDefsdata[j].Field] == (null || "          ") ? "-" : dateFormat(listContent[i][listColumnDefsdata[j].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                                            } else
                                                contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata[j].Field] != undefined ? listContent[i][listColumnDefsdata[j].Field] : "-") + "</span></td>";
                                        }
                                    }
                                    treeHtml += "<li><a data-over='true' class='mo" + listContent[i]["Id"] + "' href='javascript:void(0)' data-id=" + listContent[i]["Id"] + "><i class='icon-'></i><span style='background-color: #" + listContent[i]["ColorCode"] + "' class='eicon-s margin-right5x' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["ShortDescription"] + "</span><span class='treeItemName' data-id=" + listContent[i]["Id"] + ">" + listContent[i]["Name"] + "</span></a></li>";
                                    var status = "";
                                    if (listContent[i].IsLock == true) {
                                        contentnHtml += "<td><span class='pull-right'><i class='icon-lock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    } else {
                                        contentnHtml += "<td><span class='pull-right'><i class='icon-unlock icon-large' data-id=" + listContent[i].Id + " data-IsLock=" + listContent[i].IsLock + "></i><label class='checkbox checkbox-custom '><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                                    }
                                    contentnHtml += "</tr>";
                                    colStatus = true;
                                }
                            }
                            if ($("#ListColumn > thead tr").length < 1) {
                                columnHtml += "<th><span class='pull-right'><label class='checkbox checkbox-custom pull-right'><input id='RootLevelSelectAll'  type='checkbox' ><i class='checkbox'></i></label></span></th>";
                                columnHtml += "</tr>";
                                $('#ListColumn > thead').html(columnHtml);
                            }
                            $("#ListContainer > table tbody").html('');
                            $("#treeHolder ul").html('');
                            $('#ListContainer').scrollTop(0);
                            if ($("#ListColumn > thead tr").length > 0) {
                                $scope.noData = false;
                                $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                                $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                                $('#treeHolder  li[data-page="' + PageIndex + '"] > ul').html(treeHtml);
                                $('#treeHolder li[data-page="' + PageIndex + '"]').removeClass('pending').removeAttr('style');
                                ListCheckBoxitemClick();
                            }
                            if ($scope.PageSize > 1) {
                                $scope.LoadRootLevelActivity(1, 20, 20, $scope.FilterID.selectedFilterID, $scope.AppllyFilterObj.selectedattributes);
                            }
                        }
                        if (getactivitylist.Response != null) {
                            if (getactivitylist.Response.Data.length == 0) {
                                $scope.noData = true;
                            }
                        } else {
                            $scope.noData = true;
                        }
                    }
                } else {
                    $scope.noData = true;
                }
                $(window).AdjustHeightWidth();
            });
        }
        $("#ListContainer").click(function (e) {
            var targetcontrol = $(e.target);
            var IDList = new Array();
            IDList = GetRootLevelSelectedAll();
            if (IDList.length == $("#treeHolder").find(".nav-list").find("li").length)
            {
                $('#RootLevelSelectAll').next('i').addClass('checked');
            }
            if (targetcontrol[0].tagName == "I") {
                var entityID = targetcontrol.attr('data-id');
                var IsMemberAvailable;
                if (entityID != undefined) {
                    PlanlistService.CheckForMemberAvailabilityForEntity(entityID).then(function (getperiodavail) {
                        if (getperiodavail.Response == true) {
                            IsMemberAvailable = true;
                        } else {
                            IsMemberAvailable = false;
                        }
                        if (IsMemberAvailable == false) {
                            bootbox.alert(LanguageContents.Res_1980.Caption);
                            return false;
                        }
                        var LockStatus = 0;
                        if (targetcontrol[0].className == 'icon-lock icon-large' || targetcontrol[0].className == 'icon-large icon-lock') {
                            LockStatus = 0;
                        } else if (targetcontrol[0].className == 'icon-unlock icon-large' || targetcontrol[0].className == 'icon-large icon-unlock') {
                            LockStatus = 1;
                        }
                        var IsLock = targetcontrol.attr('data-IsLock');
                        if (LockStatus == 1) {
                            PlanlistService.PendingFundRequest(entityID).then(function (pendingrequest) {
                                var ispending = pendingrequest.Response;
                                if (ispending == true) {
                                    bootbox.confirm($translate.instant('LanguageContents.Res_2041.Caption'), function (result) {
                                        if (result) {
                                            setTimeout(function () {
                                                PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) { });
                                                if (LockStatus == 1) {
                                                    targetcontrol.removeClass("icon-unlock");
                                                    targetcontrol.addClass("icon-lock");
                                                } else if (LockStatus == 0) {
                                                    targetcontrol.removeClass("icon-lock");
                                                    targetcontrol.addClass("icon-unlock");
                                                }
                                            }, 100);
                                        }
                                    });
                                } else {
                                    PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) {
                                        var lockstatus = updatelock.Response;
                                    });
                                    if (LockStatus == 1) {
                                        targetcontrol.removeClass("icon-unlock");
                                        targetcontrol.addClass("icon-lock");
                                    } else if (LockStatus == 0) {
                                        targetcontrol.removeClass("icon-lock");
                                        targetcontrol.addClass("icon-unlock");
                                    }
                                }
                            });
                        } else {
                            PlanlistService.UpdateLock(entityID, LockStatus).then(function (updatelock) {
                                var lockstatus = updatelock.Response;
                            });
                            if (LockStatus == 1) {
                                targetcontrol.removeClass("icon-unlock");
                                targetcontrol.addClass("icon-lock");
                            } else if (LockStatus == 0) {
                                targetcontrol.removeClass("icon-lock");
                                targetcontrol.addClass("icon-unlock");
                            }
                        }
                    });
                }
            }
        });
        var LoadedPage = 0;

        function fnPageTemplate(ItemCnt) {
            var PageSize = 1;
            var itemsPerPage = 20;
            var height = 34 * ItemCnt;
            if (ItemCnt > itemsPerPage) {
                height = 34 * itemsPerPage;
                PageSize = Math.ceil(ItemCnt / itemsPerPage);
            }
            $scope.PageSize = PageSize;
            var TreeTemplate = '';
            var ListTemplate = '';
            for (var i = 0; i <= PageSize; i++) {
                if (i != undefined) {
                    TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list'></ul></li>"
                    ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"
                }
            }
            if (TreeTemplate.length > 0) {
                $("#ListContainer > table").html(ListTemplate);
                $("#treeHolder").html(TreeTemplate);
                $(window).AdjustHeightWidth();
            }
        }
        $('#ListContainer').scroll(function () {
            $("#treeHolder").scrollTop($(this).scrollTop());
            var areaHeight = $('#ListContainer').height();
            var areaTop = $('#ListContainer').position().top;
            var top = $(this).position().top - areaTop;
            var height = $('#ListContainer table').height();
            $('tbody.pending', '#ListContainer').each(function () {
                var datapage = parseInt($(this).attr('data-page'));
                if ((($(this).height() * datapage) / 3) < $("#treeHolder").scrollTop()) {
                    $('#ListContainer > table tbody[data-page="' + $(this).attr('data-page') + '"]').removeClass('pending');
                    var StartRowNo = datapage * 20;
                    var MaxNoofRow = 20;
                    $(this).removeClass('pending');
                    $(this).removeAttr('style');
                    $scope.AppllyFilterObj.selectedattributes = [];
                    $scope.LoadRootLevelActivity(datapage, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, $scope.FilterID.selectedfilterattribtues);
                    $('#treeHolder li[data-page="' + datapage + '"]').removeClass('pending');
                    $('#treeHolder li[data-page="' + datapage + '"]').removeAttr('style');
                }
            });
        });
        $("#ListColumn").click(function (e) {
            var TargetControl = $(e.target);
            $scope.AppllyFilterObj.selectedattributes = [];
            var col = "";
            if (TargetControl[0].tagName == "SPAN") {
                col = TargetControl.parent().attr("data-Column");
            } else if (TargetControl[0].tagName == "A") {
                col = TargetControl.attr("data-Column");
            }
            if (col != "") {
                $scope.sortOrder = col;
                $('#RootLevelSelectAll').next('i').removeClass('checked');
                ClearCheckBoxes();
                if ($scope.sortOrder === col) {
                    $scope.IsDesc = !$scope.IsDesc;
                } else {
                    $scope.sortOrder = col;
                    $scope.IsDesc = false;
                }
                $scope.LoadRootLevelActivity(0, 0, 20, 0, $scope.AppllyFilterObj.selectedattributes)
            }
        });
        $("#rootLevelEntity").click(function (event) {
            $("#rootLevelEntity").trigger("onRootEntityCreation", [$scope.EntityTypeID])
        });
        $window.ListofEntityID = []
        $("#treeHolder").click(function (e) {
            var TargetControl = $(e.target);
            var id = $(e.target).attr("data-id");
            if (id != undefined) {
                $scope.Gnttsettings.StartDate = "";
                $scope.Gnttsettings.EndDate = "";
                $scope.LoadGantHeaderNew(id);
                setTimeout(function () {
                    ViewRootLevelEntity(id, e);
                }, 250);
            }
        });
        $scope.LoadGantHeaderNew = function (id) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            PlanlistService.GetPeriodByIdForGantt(id).then(function (GantResults) {
                if (GantResults.Response.length != null) {
                    for (var i = 0; i < GantResults.Response.length; i++) {
                        datStartUTCval = GantResults.Response[i].Startdate.substr(6, (GantResults.Response[i].Startdate.indexOf('+') - 6));
                        datstartval.push(new Date(parseInt(datStartUTCval)));
                        datEndUTCval = GantResults.Response[i].EndDate.substr(6, (GantResults.Response[i].EndDate.indexOf('+') - 6));
                        datendval.push(new Date(parseInt(datEndUTCval)));
                    }
                    maxstartDates = ConvertDateToString(new Date(Math.min.apply(null, datstartval)));
                    maxendDates = ConvertDateToString(new Date(Math.max.apply(null, datendval)));
                    if (maxstartDates != "")
                        start = maxstartDates;
                    if (maxendDates != "")
                        end = maxendDates;
                    if (end == "NaN-NaN-NaN") {
                        end = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                        end = end.substr(0, 4);
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    var currentYear = new Date().getFullYear();
                    var strtDate = maxstartDates.substring(0, 4);
                    var endingDate = maxendDates.substring(0, 4);
                    if (currentYear < endingDate) {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');;
                    } else {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (end == "NaN-NaN-NaN") {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (start == "NaN-NaN-NaN" || start == "")
                        start = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    if (end == "NaN-NaN-NaN" || end == "")
                        end = Date.parse('+1year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    start = Date.parse(start);
                    end = Date.parse(end);
                    var dates = [];
                    dates[start.getFullYear()] = [];
                    dates[start.getFullYear()][start.getMonth()] = [start];
                    var last = start;
                    while (last.compareTo(end) == -1) {
                        var next = last.clone().addDays(1);
                        if (!dates[next.getFullYear()]) {
                            dates[next.getFullYear()] = [];
                        }
                        if (!dates[next.getFullYear()][next.getMonth()]) {
                            dates[next.getFullYear()][next.getMonth()] = [];
                        }
                        dates[next.getFullYear()][next.getMonth()].push(next);
                        last = next;
                    }
                    $scope.dates = dates;
                    return dates;
                }
            });
        }
        $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function (e) {
            var status = this.checked;
            $('#ListContainer > table > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $scope.IsSelectAllChecked = true;
                    $(this).next('i').addClass('checked');
                } else {
                    $scope.IsSelectAllChecked = false;
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $("#ViewSelect").click(function (event) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            $scope.LoadGantHeaderNews(IDList);
            setTimeout(function () {
                ViewRootLevelEntity(null, event)
            }, 250);
        });
        $scope.LoadGantHeaderNews = function (id) {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            var dates = [];
            var tempstartdates = [];
            var tempenddates = [];
            var start = "";
            var end = "";
            var datStartUTCval = "";
            var datstartval = [];
            var datEndUTCval = "";
            var datendval = [];
            var maxstartDates = "";
            var maxendDates = "";
            $scope.Gnttsettings.StartDate = "";
            $scope.Gnttsettings.EndDate = "";
            $scope.GanttCurrentDates = {};
            $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd');
            $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd');
            PlanlistService.GetPeriodByIdForGantt((IDList.length) > 0 ? IDList.join(",") : 0).then(function (GantResults) {
                if (GantResults.Response.length != null) {
                    for (var i = 0; i < GantResults.Response.length; i++) {
                        datStartUTCval = GantResults.Response[i].Startdate.substr(6, (GantResults.Response[i].Startdate.indexOf('+') - 6));
                        datstartval.push(new Date(parseInt(datStartUTCval)));
                        datEndUTCval = GantResults.Response[i].EndDate.substr(6, (GantResults.Response[i].EndDate.indexOf('+') - 6));
                        datendval.push(new Date(parseInt(datEndUTCval)));
                    }
                    maxstartDates = ConvertDateToString(new Date(Math.min.apply(null, datstartval)));
                    maxendDates = ConvertDateToString(new Date(Math.max.apply(null, datendval)));
                    if (maxstartDates != "")
                        start = maxstartDates;
                    if (maxendDates != "")
                        end = maxendDates;
                    if (end == "NaN-NaN-NaN") {
                        end = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                        end = end.substr(0, 4);
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    var currentYear = new Date().getFullYear();
                    var strtDate = maxstartDates.substring(0, 4);
                    var endingDate = maxendDates.substring(0, 4);
                    if (currentYear < endingDate) {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse(maxendDates).set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');;
                    } else {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (end == "NaN-NaN-NaN") {
                        $scope.Gnttsettings.StartDate = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                        $scope.Gnttsettings.EndDate = Date.parse('+year').set({
                            day: 31,
                            month: 11
                        }).toString('yyyy-MM-dd');
                    }
                    if (start == "NaN-NaN-NaN" || start == "")
                        start = Date.parse('-1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    if (end == "NaN-NaN-NaN" || end == "")
                        end = Date.parse('+1year').set({
                            day: 1,
                            month: 0
                        }).toString('yyyy-MM-dd');
                    start = Date.parse(start);
                    end = Date.parse(end);
                    var dates = [];
                    dates[start.getFullYear()] = [];
                    dates[start.getFullYear()][start.getMonth()] = [start];
                    var last = start;
                    while (last.compareTo(end) == -1) {
                        var next = last.clone().addDays(1);
                        if (!dates[next.getFullYear()]) {
                            dates[next.getFullYear()] = [];
                        }
                        if (!dates[next.getFullYear()][next.getMonth()]) {
                            dates[next.getFullYear()][next.getMonth()] = [];
                        }
                        dates[next.getFullYear()][next.getMonth()].push(next);
                        last = next;
                    }
                    $scope.dates = dates;
                    return dates;
                }
            });
        }

        function LoadDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                event.preventDefault();
                var localScope = $(event.target).scope();
                localScope.$apply(function () {
                    if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                        $location.path("/mui/planningtool/default/detail/listview/" + TrackID);
                    } else {
                        $location.path("/mui/planningtool/default/detail/ganttview/" + TrackID);
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function LoadSelectAllDetailPart(event, IDList) {
            if (IDList.length !== 0) {
                $window.ListofEntityID = [];
                $window.ListofEntityID = IDList;
                var TrackID = CreateHisory(IDList);
                if ($("#ViewSelect").attr('data-viewtype') == "listview") {
                    $location.path("/mui/planningtool/default/detail/listview/" + TrackID);
                } else {
                    $location.path("/mui/planningtool/default/detail/ganttview/" + TrackID);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1806.Caption'));
            }
        }

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#ListContainer > table > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr').attr('data-id'));
            });
            return IDList
        }

        function ListCheckBoxitemClick() {
            $('#ListContainer > table > tbody input:checkbox').click(function () {
                $("#RootLevelSelectAll").next().removeClass('checked')
                $scope.IsSelectAllChecked = false;
            });
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            if ($scope.IsSelectAllChecked == false) {
                if (ID != null) {
                    IDList.push(ID);
                    LoadDetailPart(event, IDList);
                } else {
                    IDList = GetRootLevelSelectedAll();
                    LoadDetailPart(event, IDList);
                }
            } else {
                $scope.IsSelectAllChecked = false;
                PlanlistService.GetAllEntityIds().then(function (Entityresult) {
                    if (Entityresult.Response != null) {
                        IDList = Entityresult.Response;
                        LoadSelectAllDetailPart(event, IDList);
                    }
                });
            }
        }
        $scope.rootValues = [];
        PlanlistService.GettingEntityForRootLevel(true).then(function (ResultPlansList) {
            if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
                $scope.IsToShowRootEntity = true;
                $scope.rootValues = ResultPlansList.Response;
            } else
                $scope.IsToShowRootEntity = false;
        });
        $scope.openrootentitycreationpopup = function (Id, Caption) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/RootEntityTypeCreation.html',
                controller: "mui.planningtool.RootEntityTypeCreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            'ID': Id,
                            'Caption': Caption
                        };
                    }
                },
                scope: $scope,
                windowClass: 'rootEntityTypeCreationModel popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.listCtrl']"));
        });
        setTimeout(function () {
            $scope.load();
        }, 0);
    }
    app.controller('mui.planningtool.default.listCtrl', ['$scope', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$cookies', '$cookieStore', '$translate', 'PlanlistService', '$modal', muiplanningtooldefaultlistCtrl]);
})(angular, app);