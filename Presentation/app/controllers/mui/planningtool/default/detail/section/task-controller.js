﻿(function(ng, app) {
	"use strict";

	function muiplanningtooldefaultdetailtaskCtrl($scope, $location, $resource, $timeout, $stateParams, $cookies, $window, $compile, $attrs, $sce, $translate, TaskNewService, $modal) {
	    var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/");
	    var model;
		var imagesrcpath = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
		$scope.GetNonBusinessDaysforDatePicker();
		$scope.Calanderopen = function($event, model1) {
			$event.preventDefault();
			$event.stopPropagation();
			$scope.calanderopened = true;
			model = model1;
			$scope.TaskDueDate1 = false;
		};
		$scope.dynCalanderopen = function($event, isopen, model) {
			$event.preventDefault();
			$event.stopPropagation();
			$scope.calanderopened = false;
			$scope.fields["DatePart_Calander_Open" + model] = true;
		};
		$scope.MinMaxValue = {};
		$scope.currentTaskListID = 0;
		$scope.TemplateTaskIds = [];

		function getAllTemplateTask() {
			TaskNewService.getAllTemplateTask().then(function(taskList) {
				$scope.TemplateTaskIds = taskList.Response;
			});
		}
		$scope.SubLevelTaskLibraryList = $scope.SubLevelTaskLibraryListContainer.SubLevelTaskLibraryList;
		if ($scope.SubLevelTaskLibraryList != undefined) {
			GroupSubLevelTaskList('EntityUniqueKey', 'All', 0);
		}
		$scope.Dropdown = {};
		$scope.DropDownTreeOptionValues = {};
		$scope.ShowHideAttributeOnRelation = {};
		$scope.EnableDisableControlsHolder = {};
		$scope.TaskGlobalEntityID = $stateParams.ID;
		$scope.ContextTaskEntityID = $stateParams.ID;
		$scope.temptaskDiv = "";
		$scope.tempTaskID = 0;
		$scope.IsShowTaskFlag = true;
		$scope.FilterValue = -1;
		$scope.divid = '';
		$scope.updatetaskEdit = true;
		$scope.returnObj = [];
		$scope.newTaskid = 0;
		$scope.AssetSelectionClass = [];
		$scope.addfromComputer = false;
		$('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
		$scope.addfromGenAttachments = false;
		$scope.proofUrl = $sce.trustAsResourceUrl("");
		$scope.taskTypeId = 0;
		$scope.FileIdForAssetCategory = "";
		$scope.AssetCategory = {};
		$scope.ImageFileName = '';
		var cancelevent;
		TaskNewService.gettaskflagstatus().then(function(flagstat) {
			if (flagstat.Response != null) {
				if (flagstat.Response) {
					$scope.IsShowTaskFlag = true;
				} else {
					$scope.IsShowTaskFlag = false;
				}
				$scope.LoadTaskList();
			}
		});
		$scope.DatepickerShow = function(CurrentDueDate, CurrentId) {
			if (CurrentDueDate.length < 2) {
				$('#' + CurrentId).val('');
				$('#' + CurrentId).attr('AlredyDateSelected', 'true');
			} else {
				$('#' + CurrentId).attr('AlredyDateSelected', 'false');
			}
			$timeout(function() {
				$('[id=' + CurrentId + ']:enabled:visible:first').focus().select()
			}, 100);
			$timeout(function() {
				$scope.fields["DatePart_Calander_Open" + model] = true;
			}, 100);
		}
		$scope.fired = false;
		$scope.changedate = function() {
			if ($scope.fired == false) {
				$scope.fired = true;
				$timeout(function() {
					if ($scope.dueDate != null) {
						var test = isValidDate($('#UnAssignedDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());
						if (test) {
							if (dateDiffBetweenDates($scope.dueDate) >= 0) {
								$scope.saveEntityTaskDetails('unassignedue');
								$scope.editable = '';
								$scope.fired = false;
								$('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');
								$('#UnAssignedDatepicker').val('');
							} else {
								bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
								$scope.dueDate = "-";
								$scope.fired = false;
							}
							$scope.fields["DatePart_Calander_Open" + model] = false;
						} else {
							$scope.dueDate = new Date.create();
							$scope.dueDate = null;
							$('#UnAssignedDatepicker').val('');
							$scope.fired = false;
							$scope.saveEmptyDueDate('unassignedue');
							$scope.editable = '';
							$('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');
							$('#UnAssignedDatepicker').val('');
							$scope.dueDate = "-";
							$scope.fired = false;
							$scope.fields["DatePart_Calander_Open" + model] = false;
						}
					} else {
						$scope.saveEmptyDueDate('unassignedue');
						$scope.editable = '';
						$('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');
						$('#UnAssignedDatepicker').val('');
						$scope.dueDate = "-";
						$scope.fired = false;
						$scope.fields["DatePart_Calander_Open" + model] = false;
					}
				}, 100);
			}
		}

		function isValidDate(dateval, dateformat) {
			var formatlen;
			var defaultdateVal = [];
			defaultdateVal = dateval.length;
			formatlen = dateformat.length;
			if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
			else return false;
		};
		$scope.saveEmptyDueDate = function(type) {
			$scope.EntityTaskChangesHolder.DueDate = '';
			var TaskStatusData = {};
			TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
			TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
			TaskNewService.UpdatetaskEntityTaskDueDate(TaskStatusData).then(function(TaskStatusResult) {
				$scope.fired = false;
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					$scope.fired = false;
				} else {
					if (TaskStatusResult.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						$scope.fired = false;
					} else {
						if (type == "unassignedue") {
							if ($scope.EntityTaskChangesHolder.DueDate != '') {
								$scope.TaskBriefDetails.dueIn = dateDiffBetweenDates($scope.dueDate);
								$scope.dueDate = formatteddateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
								$scope.fired = false;
							} else {
								$scope.dueDate = '-';
								$scope.TaskBriefDetails.dueDate = '-';
								$scope.TaskBriefDetails.dueIn = 0;
								$scope.fired = false;
							}
						} else {
							$scope.dueDate = '-';
							$scope.TaskBriefDetails.dueDate = '-';
							$scope.TaskBriefDetails.dueIn = 0;
							$scope.fired = false;
						}
						RefreshCurrentTask();
						$scope.EntityTaskChangesHolder = {
							"TaskName": "",
							"TaskDescription": "",
							"DueDate": "",
							"Note": ""
						};
						NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
						$timeout(function() {
							feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
						}, 2000);
						$scope.fired = false;
					}
				}
			});
		}
		$scope.showCalender = function() {
			alert('showCalender ');
		}
		$('#UnAssignedDatepicker').keydown(function(e) {
			if (e.keyCode == 13) {
				if ($('#UnAssignedDatepicker').val() != "") {
					try {
						$scope.dueDate = new Date.create($('#UnAssignedDatepicker').val());
						if ($scope.dueDate.toString() == "Invalid Date") {
							$scope.dueDate = null;
						}
					} catch (e) {
						$scope.dueDate = null;
					}
				}
				if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
					return false;
				} else {
					$scope.saveEntityTaskDetails('unassignedue');
					$scope.editable = '';
					$('#UnAssignedDatepicker').attr('AlredyDateSelected', 'false');
					$('#UnAssignedDatepicker').val('');
					$scope.fields["DatePart_Calander_Open" + model] = false;
				}
			}
		});
		$scope.reassignchangedate = function() {
			if ($scope.fired == false) {
				$scope.fired = true;
				$timeout(function() {
					$scope.fields["DatePart_Calander_Open" + model] = false;
					if ($scope.ListUnassignedTasksDueObjects.Duedate != null) {
						if (dateDiffBetweenDates($scope.ListUnassignedTasksDueObjects.Duedate) >= 0) {
							$scope.saveReassignedTaskDuedate('ReassignedAssignedDatepicker');
							$scope.editable = '';
							$('#ReassignedAssignedDatepicker').attr('AlredyDateSelected', 'false');
							$('#ReassignedAssignedDatepicker').val('');
						} else {
							bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
							$scope.fired = false;
						}
					}
				}, 100);
			}
		}
		$('#ReassignedAssignedDatepicker').keydown(function(e) {
			if (e.keyCode == 13) {
				if ($('#ReassignedAssignedDatepicker').val() != "") {
					try {
						$scope.ListUnassignedTasksDueObjects.Duedate = new Date.create($('#ReassignedAssignedDatepicker').val());
						if ($scope.ListUnassignedTasksDueObjects.Duedate.toString() == "Invalid Date") {
							$scope.ListUnassignedTasksDueObjects.Duedate = null;
						}
					} catch (e) {
						$scope.ListUnassignedTasksDueObjects.Duedate = null;
					}
				}
				if ($scope.ListUnassignedTasksDueObjects.Duedate != null && dateDiffBetweenDates($scope.ListUnassignedTasksDueObjects.Duedate) <= 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
					return false;
				} else {
					$scope.saveReassignedTaskDuedate('ReassignedAssignedDatepicker');
					$scope.editable = '';
					$('#ReassignedAssignedDatepicker').attr('AlredyDateSelected', 'false');
					$('#ReassignedAssignedDatepicker').val('');
					$scope.fields["DatePart_Calander_Open" + model] = false;
				}
			}
		});
		var perioddates = [];
		$scope.approvechangedate = function() {
			if ($scope.fired == false) {
				$scope.fired = true;
				$timeout(function() {
					$scope.fields["DatePart_Calander_Open" + model] = false;
					if ($scope.dueDate != null) {
						var test = isValidDate($('#ApproveTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());
						if (test) {
							if (dateDiffBetweenDates($scope.dueDate) >= 0) {
								$scope.saveEntityTaskDetails('due');
								$scope.editable = '';
								$('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
								$('#ApproveTaskDatepicker').val('');
							} else {
								bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
								$scope.fired = false;
							}
						} else {
							$scope.dueDate = new Date.create();
							$scope.dueDate = null;
							$('#ApproveTaskDatepicker').val('');
							$scope.fired = false;
							$scope.saveEmptyDueDate('due');
							$scope.editable = '';
							$('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
							$('#ApproveTaskDatepicker').val('');
							$scope.dueDate = "-";
							$scope.fired = false;
							$scope.fields["DatePart_Calander_Open" + model] = false;
						}
					} else {
						$scope.saveEmptyDueDate('due');
						$scope.editable = '';
						$('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
						$('#ApproveTaskDatepicker').val('');
						$scope.dueDate = "-";
						$scope.fired = false;
						$scope.fields["DatePart_Calander_Open" + model] = false;
					}
				}, 100);
			}
		}
		$('#ApproveTaskDatepicker').keydown(function(e) {
			if (e.keyCode == 13) {
				if ($('#ApproveTaskDatepicker').val() != "") {
					try {
						$scope.dueDate = new Date.create($('#ApproveTaskDatepicker').val());
						if ($scope.dueDate.toString() == "Invalid Date") {
							$scope.dueDate = null;
						}
					} catch (e) {
						$scope.dueDate = null;
					}
				}
				if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
					return false;
				} else {
					$scope.saveEntityTaskDetails('due');
					$scope.editable = '';
					$('#ApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
					$('#ApproveTaskDatepicker').val('');
					$scope.fields["DatePart_Calander_Open" + model] = false;
				}
			}
		});
		$scope.reviewchangedate = function() {
			if ($scope.fired == false) {
				$scope.fired = true;
				$timeout(function() {
					$scope.fields["DatePart_Calander_Open" + model] = false;
					if ($scope.dueDate != null) {
						var test = isValidDate($('#ReviewTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());
						if (test) {
							if (dateDiffBetweenDates($scope.dueDate) >= 0) {
								$scope.saveEntityTaskDetails('due');
								$scope.editable = '';
								$('#ReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
								$('#ReviewTaskDatepicker').val('');
							} else {
								bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
								$scope.fired = false;
							}
						} else {
							$scope.dueDate = new Date.create();
							$scope.dueDate = null;
							$('#ReviewTaskDatepicker').val('');
							$scope.fired = false;
							$scope.saveEmptyDueDate('due');
							$scope.editable = '';
							$('#ReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
							$('#ReviewTaskDatepicker').val('');
							$scope.dueDate = "-";
							$scope.fired = false;
							$scope.fields["DatePart_Calander_Open" + model] = false;
						}
					} else {
						$scope.saveEmptyDueDate('due');
						$scope.editable = '';
						$('#ReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
						$('#ReviewTaskDatepicker').val('');
						$scope.dueDate = "-";
						$scope.fired = false;
						$scope.fields["DatePart_Calander_Open" + model] = false;
					}
				}, 100);
			}
		}
		$('#ReviewTaskDatepicker').keydown(function(e) {
			if (e.keyCode == 13) {
				if ($('#ReviewTaskDatepicker').val() != "") {
					try {
						$scope.dueDate = new Date.create($('#ReviewTaskDatepicker').val());
						if ($scope.dueDate.toString() == "Invalid Date") {
							$scope.dueDate = null;
						}
					} catch (e) {
						$scope.dueDate = null;
					}
				}
				if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
					return false;
				} else {
					$scope.saveEntityTaskDetails('due');
					$scope.editable = '';
					$('#ReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
					$('#ReviewTaskDatepicker').val('');
					$scope.fields["DatePart_Calander_Open" + model] = false;
				}
			}
		});
		$scope.workchangedate = function() {
			if ($scope.fired == false) {
				$scope.fired = true;
				$timeout(function() {
					$scope.fields["DatePart_Calander_Open" + model] = false;
					if ($scope.dueDate != null) {
						var test = isValidDate($('#WorkTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());
						if (test) {
							if (dateDiffBetweenDates($scope.dueDate) >= 0) {
								$scope.saveEntityTaskDetails('due');
								$scope.editable = '';
								$('#WorkTaskDatepicker').attr('AlredyDateSelected', 'false');
								$('#WorkTaskDatepicker').val('');
							} else {
								bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
								$scope.fired = false;
							}
						} else {
							$scope.dueDate = new Date.create();
							$scope.dueDate = null;
							$('#WorkTaskDatepicker').val('');
							$scope.fired = false;
							$scope.saveEmptyDueDate('unassignedue');
							$scope.editable = '';
							$('#WorkTaskDatepicker').attr('AlredyDateSelected', 'false');
							$('#WorkTaskDatepicker').val('');
							$scope.dueDate = "-";
							$scope.fired = false;
							$scope.fields["DatePart_Calander_Open" + model] = false;
						}
					} else {
						$scope.saveEmptyDueDate('due');
						$scope.editable = '';
						$('#WorkTaskDatepicker').attr('AlredyDateSelected', 'false');
						$('#WorkTaskDatepicker').val('');
						$scope.dueDate = "-";
						$scope.fired = false;
						$scope.fields["DatePart_Calander_Open" + model] = false;
					}
				}, 100);
			}
		}
		$('#WorkTaskDatepicker').keydown(function(e) {
			if (e.keyCode == 13) {
				if ($('#WorkTaskDatepicker').val() != "") {
					try {
						$scope.dueDate = new Date.create($('#WorkTaskDatepicker').val());
						if ($scope.dueDate.toString() == "Invalid Date") {
							$scope.dueDate = null;
						}
					} catch (e) {
						$scope.dueDate = null;
					}
				}
				if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
					return false;
				} else {
					$scope.saveEntityTaskDetails('due');
					$scope.editable = '';
					$('#WorkTaskDatepicker').attr('AlredyDateSelected', 'false');
					$('#WorkTaskDatepicker').val('');
					$scope.fields["DatePart_Calander_Open" + model] = false;
				}
			}
		});
		$scope.OwnerName = $cookies['Username'];
		$scope.OwnerID = parseInt($cookies['UserId'], 10);
		$scope.ownerEmail = $cookies['UserEmail'];
		$scope.TaskActionFor = 1;
		$scope.OwnerList = [];
		$scope.OwnerList.push({
			"Roleid": 1,
			"RoleName": "Owner",
			"UserEmail": $scope.ownerEmail,
			"DepartmentName": "-",
			"Title": "-",
			"Userid": parseInt($scope.OwnerID, 10),
			"UserName": $scope.OwnerName,
			"IsInherited": '0',
			"InheritedFromEntityid": '0'
		});
		$scope.TaskBriefDetails = {
			taskID: 0,
			taskTypeId: 0,
			taskListUniqueID: 0,
			taskTypeName: "",
			EntityID: 0,
			ownerId: 0,
			taskName: "No Name",
			dueIn: 0,
			dueDate: "",
			strDueDate: "",
			status: "",
			statusID: 0,
			taskOwner: "",
			Description: "",
			Note: "",
			taskmembersList: [],
			taskAttachmentsList: [],
			taskProgressCount: "",
			totalTaskMembers: [],
			TaskMembersRoundTripGroup: [],
			taskCheckList: [],
			WorkTaskInprogressStatus: "",
			LatestMaxRound: 0,
			Totalassetcount: 0,
			Totalassetsize: ""
		};
		$scope.TaskAttachAsset = {
			SelectedAssetFiles: [],
			AssetFiles: [],
			AssetDynamicData: [],
			AssetSelectionFiles: [],
			html: "",
			attrRelation: []
		};
		$scope.NewTime = $scope.DefaultImageSettings.ImageSpan;
		$scope.$on('LoadTaskList', function() {
			$scope.LoadTaskList()
		});
		$scope.LoadTaskList = function() {
			TaskNewService.GetEntityTaskListWithoutTasks($scope.TaskGlobalEntityID).then(function(TaskListLibraryData) {
				$scope.TaskLibraryList = TaskListLibraryData.Response;
				$scope.groupBy('SortOrder', 'All', 0);
				$timeout(function() {
					$scope.ApplyTaskListCountDetails();
					$(".nestedsorting").each(applySortable);
				}, 10);
			});
		}
		$scope.$on('LoadTask', function(event, ID) {
			$scope.IsNotVersioning = true;
			$scope.TaskLibraryList = [];
			$scope.srcpageno = 1;
			$scope.groups.splice(0, $scope.groups.length);
			$scope.TaskExpandedDetails.splice(0, $scope.TaskExpandedDetails.length);
			$scope.SubLevelTaskLibraryList = [];
			$scope.groupbySubLevelTaskObj.splice(0, $scope.groupbySubLevelTaskObj.length);
			$scope.ngExpandCollapseStatus = {
				ngExpandAll: true
			};
			$scope.SubLevelTaskLibraryListContainer = {
				SubLevelTaskLibraryList: []
			};
			$scope.TaskGlobalEntityID = $stateParams.ID;
			$scope.ContextTaskEntityID = $stateParams.ID;
			$scope.TaskOrderListObj = {
				TaskSortingStatus: $translate.instant('LanguageContents.Res_1555.Caption'),
				TaskSortingStatusID: 1,
				TaskOrderHandle: false,
				SortOrderObj: [],
				UniqueTaskSort: $translate.instant('LanguageContents.Res_4986.Caption'),
				UniqueTaskSortingStatusID: 1,
				UniqueTaskOrderHandle: false,
				UniqueSortOrderObj: []
			};
			$scope.SubLevelEnableObject = {
				Enabled: false,
				SublevelText: $translate.instant('LanguageContents.Res_4959.Caption'),
				SublevelTextStatus: 1
			};
			CacheResult();
			$timeout(function() {
				LoadMemberRoles();
			}, 300);
		});

		function CacheResult() {
			$scope.TaskLibraryList = [];
			TaskNewService.GetEntityTaskListWithoutTasks($scope.TaskGlobalEntityID).then(function(TaskListLibraryData) {
				$scope.TaskLibraryList = TaskListLibraryData.Response;
				$scope.groupBy('SortOrder', 'All', 0);
				$timeout(function() {
					$scope.ApplyTaskListCountDetails();
					$(".nestedsorting").each(applySortable);
				}, 10);
			});
		}
		$scope.formatResult = function(item) {
			var markup = '<table class="user-result">';
			markup += '<tbody>';
			markup += '<tr>';
			markup += '<td class="user-image">';
			markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
			markup += '</td>';
			markup += '<td class="user-info">';
			markup += '<div class="user-title">' + item.text + '</div>';
			markup += '</td>';
			markup += '</tr>';
			markup += '</tbody>';
			markup += '</table>';
			return markup;
		};
		$scope.formatSelection = function(item) {
			var markup = '<table class="user-result">';
			markup += '<tbody>';
			markup += '<tr>';
			markup += '<td class="user-image">';
			markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
			markup += '</td>';
			markup += '<td class="user-info">';
			markup += '<div class="user-title">' + item.text + '</div>';
			markup += '</td>';
			markup += '</tr>';
			markup += '</tbody>';
			markup += '</table>';
			return markup;
		};
		$scope.TaskType = '';
		$scope.TaskTypeListdata = [];
		$scope.userid = parseInt($.cookie('UserId'), 10);
		$scope.userisProofInitiator = false;
		$scope.TaskFlagList = [];
		TaskNewService.GetTaskFlags().then(function(flagResult) {
			if (flagResult.Response != null) {
				$scope.TaskFlagList = flagResult.Response;
			}
		});
		$scope.entityeditcontrolclick = function() {
			$timeout(function() {
				$('[class=input-medium]').focus().select()
			}, 10);
		};
		$scope.TaskTitleChange = function(taskname) {
			$scope.name = $('<div />').html(taskname).text();
			$timeout(function() {
				$("#inputTaskNameChange").focus().select()
			}, 10);
			$timeout(function() {
				$("#taskUnassignedName").focus().select()
			}, 10);
			$timeout(function() {
				$("#TaskApprovalTaskNameChange").focus().select()
			}, 10);
			$timeout(function() {
				$("#TaskReviewtask").focus().select()
			}, 10);
		};
		$scope.TaskDescChange1 = function(originalDesc) {
			$scope.DescriptionChange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#txtDesc1").focus().select()
			}, 10);
		};
		$scope.TaskNoteChange1 = function(originalDesc) {
			$scope.Notechange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#notechangework").focus().select()
			}, 10);
		};
		$scope.TaskNoteChange2 = function(originalDesc) {
			$scope.Notechange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#noteapprovaltask").focus().select()
			}, 10);
		};
		$scope.TaskNoteChange3 = function(originalDesc) {
			$scope.Notechange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#noteReviewTask").focus().select()
			}, 10);
		};
		$scope.TaskNoteChange4 = function(originalDesc) {
			$scope.Notechange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#noteUnassignedtask").focus().select()
			}, 10);
		};
		$scope.TaskDescChange2 = function(originalDesc) {
			$scope.DescriptionChange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#txtDesc2").focus().select()
			}, 10);
		};
		$scope.TaskDescChange3 = function(originalDesc) {
			$scope.DescriptionChange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#txtDesc3").focus().select()
			}, 10);
		};
		$scope.TaskDescChange4 = function(originalDesc) {
			$scope.DescriptionChange = $('<div />').html(originalDesc).text();
			$timeout(function() {
				$("#txtDesc4").focus().select()
			}, 10);
		};
		$scope.TaskcheckEditable = function(type, index) {
			if (type == "WorkTask") $timeout(function() {
				$("#worktxtChk_" + index + "").focus().select()
			}, 10);
			else $timeout(function() {
				$("#unassignedtasktxtChk_" + index + "").focus().select()
			}, 10);
		};
		$scope.GetTaskListID = function(TaskListID) {
			$scope.currentTaskListID = TaskListID;
		}
		$scope.SelectedTaskLIstID = 0;
		$scope.SelectedTaskID = 0;
		$scope.TaskTypeFilter = function(item) {
			if (item.TaskTypeId == 32 || item.TaskTypeId == 36 || item.TaskTypeId == 37) return false;
			else return true;
		};
		$scope.addNewTask = function(TaskListID) {
			if (TaskListID != 0) {
				var opentaskpopupdata = {};
				opentaskpopupdata.TaskListID = TaskListID;
				opentaskpopupdata.IsLock = false;
				opentaskpopupdata.Fromplace = "Entity";
				opentaskpopupdata.Type = "addtask";
				opentaskpopupdata.TaskGlobalEntityID = $stateParams.ID;
				opentaskpopupdata.TaskBriefDetails = $scope.TaskBriefDetails;
				opentaskpopupdata.EntityLockTask = $scope.EntityLockTask;
				var modalInstance = $modal.open({
					templateUrl: 'views/mui/task/TaskCreation.html',
					controller: "mui.task.muitaskTaskCreationCtrl",
					resolve: {
						params: function() {
							return {
								data: opentaskpopupdata
							};
						}
					},
					scope: $scope,
					windowClass: 'addTaskPopup popup-widthL',
					backdrop: "static"
				});
				modalInstance.result.then(function(selectedItem) {
					$scope.selected = selectedItem;
				}, function() {});
				$scope.AssetTaskentityselection = "task";
				$scope.IsNotVersioning = true;
				$scope.TaskActionFor = 1;
				$scope.TaskGlobalEntityID = $stateParams.ID;
				$scope.SelectedTaskLIstID = TaskListID;
				$scope.TaskAttachAsset.html = '';
				$scope.taskTypeId = 0;
				$scope.currentTaskListID = 0;
				$scope.rytSideApprovalButtonPane = false;
			}
		}
		$scope.treePreviewObj = {};
		$scope.treeNodeSelectedHolder = [];
		$scope.treesrcdirec = {};
		$scope.treePreviewObj = {};
		$scope.staticTreesrcdirec = {};
		$scope.TreeEmptyAttributeObj = {};

		function RefreshAddNewTaskScopes() {
			$scope.ngShowUnassigned = false;
			$scope.ngShowTaskCreation = true;
			$scope.DisableIsAdminTask = false;
			$scope.TaskActionCaption = "Add task";
			$scope.TaskMembersList = "";
			$scope.NewTaskName = "";
			$scope.TaskDueDate = new Date.create();
			$scope.TaskDueDate = null;
			$scope.TaskDescription = "";
			$scope.TaskNote = "";
			$scope.SelectedTaskID = 0;
			$scope.dyn_Cont = '';
			$scope.dyn_ContAsset = '';
			$scope.AdminTaskCheckList = [{
				ID: 0,
				NAME: ""
			}];
			$("#DynamicTaskControlsAttributies").html("");
			$scope.treeNodeSelectedHolder = [];
			$scope.treesrcdirec = {};
			$scope.treePreviewObj = {};
			$('#assetlistActivityTask').empty();
			$("#taskentityattachassetdata").empty();
			$scope.addfromGenAttachments = false;
			$scope.addfromComputer = false;
			$('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
			refreshModel();
			$timeout(function() {
				$('#addtaskcaption').focus();
			}, 1000);
		}
		$scope.fields = {
			usersID: ''
		};
		$scope.assetfields = {
			usersID: ''
		};
		$scope.EnableDisableControlsHolder = {};
		$scope.atributesRelationList = {};
		$scope.optionsLists = [];
		$scope.EnableDisableControlsHolder = {};
		$scope.Inherritingtreelevels = {};
		$scope.InheritingLevelsitems = [];
		$scope.treeTexts = {};
		$scope.treeSources = {};		
		$scope.treeSourcesObj = [];
		$scope.UploadAttributeData = [];
		$scope.OptionObj = {};
		$scope.PercentageVisibleSettings = {};
		$scope.DropDownTreePricing = {};
		$scope.listAttriToAttriResult = [];
		$scope.items = [];
		$scope.treeSources = {};
		$scope.treelevels = {};
		$scope.NormalDropdownCaption = {};
		$scope.uploader = {};
		$scope.UploaderCaption = {};
		$scope.NormalMultiDropdownCaption = {};
		$scope.treeTexts = {};
		$scope.treeSelection = [];
		$scope.normaltreeSources = {};
		$scope.treeNodeSelectedHolder = new Array();
		var apple_selected, tree, treedata_avm, treedata_geography;
		$scope.my_tree_handler = function(branch, parentArr) {
			var _ref;
			$scope.output = "You selected: " + branch.Caption;
			if ((_ref = branch.data) != null ? _ref.description : void 0) {
				return $scope.output += '(' + branch.data.description + ')';
			}
			if (branch.ischecked == true) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length == 0) {
					$scope.treeNodeSelectedHolder.push(branch);
				}
				for (var i = 0, parent; parent = parentArr[i++];) {
					var remainRecord = [];
					remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
						return e.AttributeId == parent.AttributeId && e.id == parent.id;
					});
					if (remainRecord.length == 0) {
						$scope.treeNodeSelectedHolder.push(parent);
					}
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
					if (branch.Children.length > 0) {
						RemoveRecursiveChildTreenode(branch.Children);
					}
				}
			}
			if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
				treeTextVisbileflag = false;
				if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
					$scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
				} else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			} else {
				$scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			}
		};
		$scope.renderHtml = function(htmlCode) {
			return $sce.trustAsHtml(htmlCode);
		};
		var treeTextVisbileflag = false;

		function IsNotEmptyTree(treeObj) {
			for (var i = 0, node; node = treeObj[i++];) {
				if (node.ischecked == true) {
					treeTextVisbileflag = true;
					return treeTextVisbileflag;
				} else {
					IsNotEmptyTree(node.Children);
				}
			}
			return treeTextVisbileflag;
		}

		function RemoveRecursiveChildTreenode(children) {
			for (var j = 0, child; child = children[j++];) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == child.AttributeId && e.id == child.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
					if (child.Children.length > 0) {
						RemoveRecursiveChildTreenode(child.Children);
					}
				}
			}
		}
		$scope.treesrcdirec = {};
		$scope.my_tree = tree = {};
		$scope.ChecklistVisible = false;
		$scope.ClearModelObject = function(ModelObject) {
			for (var variable in ModelObject) {
				if (typeof ModelObject[variable] === "string") {
					if (variable !== "ListSingleSelection_69") {
						ModelObject[variable] = "";
					}
				} else if (typeof ModelObject[variable] === "number") {
					ModelObject[variable] = null;
				} else if (Array.isArray(ModelObject[variable])) {
					ModelObject[variable] = [];
				} else if (typeof ModelObject[variable] === "object") {
					ModelObject[variable] = {};
				}
			}
		}

		function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
			var recursiveAttrID = '';
			var attributesToShow = [];
			if (attrLevel > 0) {
				attributesToShow.push($.grep(res, function(e) {
					return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
				}));
			} else {
				attributesToShow.push($.grep(res, function(e) {
					return (e.AttributeID == attrID);
				}));
			}
			if (attributesToShow[0] != undefined) {
				for (var i = 0; i < attributesToShow[0].length; i++) {
					var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
					if (attrRelIDs != undefined) {
						for (var j = 0; j < attrRelIDs.length; j++) {
							if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
								$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
								recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function(e) {
									return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
								}));
								if (recursiveAttrID != undefined) {
									for (var m = 0; m < recursiveAttrID.length; m++) {
										RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
									}
								}
							} else {
								$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
								$scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
								recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function(e) {
									return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
								}));
								if (recursiveAttrID != undefined) {
									for (var m = 0; m < recursiveAttrID.length; m++) {
										RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
									}
								}
							}
						}
					}
				}
			}
		}
		

		function HideAttributeToAttributeRelationsOnPageLoad() {
			try {
				if ($scope.listAttriToAttriResult != undefined) {
					for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
						var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
						if (attrRelIDs != undefined) {
							for (var j = 0; j < attrRelIDs.length; j++) {
								if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
								}
							}
						}
					}
				}
			} catch (e) {}
		}
		$scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function(attrID, attributeLevel, attrVal, attrType) {
			try {
				var optionValue = attrVal;
				var attributesToShow = [];
				if (attrType == 3) {
					attributesToShow.push($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeOptionID == optionValue;
					})[0]);
				} else if (attrType == 4) {
					attributesToShow = ($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
					}));
				} else if (attrType == 7) {
					attributesToShow = ($.grep($scope.listAttriToAttriResult, function(e) {
						return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
					}));
				} else if (attrType == 6 || attrType == 12) {
					if (attrVal != null) {
						attributesToShow = [];
						attributesToShow.push($.grep($scope.listAttriToAttriResult, function(e) {
							return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
						})[0]);
					}
				}
				if (attributesToShow[0] != undefined) {
					for (var i = 0; i < attributesToShow.length; i++) {
						var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
						if (attrRelIDs != undefined) {
							for (var j = 0; j < attrRelIDs.length; j++) {
								if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
								}
							}
						}
					}
				}
			} catch (e) {}
		}

		function GetValidationList(rootID) {
			TaskNewService.GetValidationDationByEntitytype(rootID).then(function(GetValidationresult) {
				if (GetValidationresult.Response != null) {
					$scope.listValidationResult = GetValidationresult.Response;
					if ($scope.listAttriToAttriResult != null) {
						for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
							var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
							if (attrRelIDs != undefined) {
								for (var j = 0; j < attrRelIDs.length; j++) {
									if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
										$scope.listValidationResult = $.grep($scope.listValidationResult, function(e) {
											return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
										});
									} else {
										$scope.listValidationResult = $.grep($scope.listValidationResult, function(e) {
											return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
										});
									}
								}
							}
						}
						$("#TaskDynamicMetadata").nod($scope.listValidationResult, {
							'delay': 200,
							'submitBtnSelector': '#tskbtnTemp',
							'silentSubmit': 'true'
						});
					}
				}
			});
		}
		var treeTextVisbileflag = false;

		function IsNotEmptyTree(treeObj) {
			for (var i = 0, node; node = treeObj[i++];) {
				if (node.ischecked == true) {
					treeTextVisbileflag = true;
					return treeTextVisbileflag;
				} else {
					IsNotEmptyTree(node.Children);
				}
			}
			return treeTextVisbileflag;
		}

		function GetTreeCheckedNodes(treeobj, attrID) {
			for (var i = 0, node; node = treeobj[i++];) {
				if (node.ischecked == true) {
					$scope.fields["Tree_" + attrID].push(node.id);
				}
				if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
			}
		}
		$scope.treeNodeSelectedHolder = [];
		var apple_selected, tree, treedata_avm, treedata_geography;
		$scope.my_tree_handler = function(branch, parentArr) {
			$scope.fields = {};
			$scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
			if (branch.ischecked == true) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length == 0) {
					$scope.treeNodeSelectedHolder.push(branch);
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == branch.AttributeId && e.id == branch.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
				}
			}
			if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
				treeTextVisbileflag = false;
				if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
					$scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
				} else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			} else {
				$scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
			}
			if ($scope.treeNodeSelectedHolder.length > 0) {
				for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
					$scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
				}
			}
			$scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
		};

		function RemoveRecursiveChildTreenode(children) {
			for (var j = 0, child; child = children[j++];) {
				var remainRecord = [];
				remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
					return e.AttributeId == child.AttributeId && e.id == child.id;
				});
				if (remainRecord.length > 0) {
					$scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
					if (child.Children.length > 0) {
						RemoveRecursiveChildTreenode(child.Children);
					}
				}
			}
		}
		$scope.treesrcdirec = {};
		$scope.my_tree = tree = {};
		$scope.LoadDropDownChildLevels = function(attrID, attributeLevel, levelcnt, attrType) {
			if (levelcnt > 0) {
				var currntlevel = attributeLevel + 1;
				for (var j = currntlevel; j <= levelcnt; j++) {
					$scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
					if (attrType == 6) {
						$scope.fields["DropDown_" + attrID + "_" + j] = "";
					} else if (attrType == 12) {
						if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
						else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
					}
				}
				if (attrType == 6) {
					if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
						$.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
							$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
						});
					}
				} else if (attrType == 12) {
					if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
						$.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
							$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
						});
					}
				}
			}
		}
		var assignedforperiod = '';

		function LoadTaskMetadata(EntAttrDet, ID, tasktype, fromassignedplace) {
			assignedforperiod = fromassignedplace;
			$scope.StopeUpdateStatusonPageLoad = false;
			$scope.detailsLoader = false;
			$scope.detailsData = true;
			$scope.dyn_Cont = '';
			$scope.dyn_Cont = "";
			$scope.items = [];
			$scope.attributedata = EntAttrDet;
			if ($scope.attributedata != null) {
				for (var i = 0; i < $scope.attributedata.length; i++) {
					if ($scope.attributedata[i].TypeID == 6) {
						$scope.dyn_Cont2 = '';
						var CaptionObj = $scope.attributedata[i].Caption.split(",");
						for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
							if (j == 0) {
								if (CaptionObj[j] != undefined) {
									$scope.items.push({
										caption: $scope.attributedata[i].Lable[j].Label,
										level: j + 1
									});
									$scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
									$scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
								} else {
									$scope.items.push({
										caption: $scope.attributedata[i].Lable[j].Label,
										level: j + 1
									});
									$scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
									$scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
								}
							} else {
								if (CaptionObj[j] != undefined) {
									$scope.items.push({
										caption: $scope.attributedata[i].Lable[j].Label,
										level: j + 1
									});
									$scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
									$scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
								} else {
									$scope.items.push({
										caption: $scope.attributedata[i].Lable[j].Label,
										level: j + 1
									});
									$scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
									$scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
								}
							}
						}
						$scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
						$scope.items = [];
						for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
							var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
							if ($scope.attributedata[i].IsReadOnly == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
							} else {
								if ($scope.IsLock == false) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditableoptimizedtreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
								} else if ($scope.IsLock == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
								}
							}
						}
					} else if ($scope.attributedata[i].TypeID == 12) {
						$scope.dyn_Cont2 = '';
						var CaptionObj = $scope.attributedata[i].Caption;
						for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
							if ($scope.attributedata[i].Lable.length == 1) {
								var k = j;
								var treeTexts = [];
								var fields = [];
								$scope.items.push({
									caption: $scope.attributedata[i].Lable[j].Label,
									level: j + 1
								});
								if (k == CaptionObj.length) {
									$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
									$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
								} else {
									if (CaptionObj[k] != undefined) {
										for (k; k < CaptionObj.length; k++) {
											treeTexts.push(CaptionObj[k]);
											fields.push(CaptionObj[k]);
										}
										$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
										$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
									} else {
										$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
										$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
									}
								}
							} else {
								if (j == 0) {
									if (CaptionObj[j] != undefined) {
										$scope.items.push({
											caption: $scope.attributedata[i].Lable[j].Label,
											level: j + 1
										});
										$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
										$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
									} else {
										$scope.items.push({
											caption: $scope.attributedata[i].Lable[j].Label,
											level: j + 1
										});
										$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
										$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
									}
								} else {
									var k = j;
									if (j == ($scope.attributedata[i].Lable.length - 1)) {
										var treeTexts = [];
										var fields = [];
										$scope.items.push({
											caption: $scope.attributedata[i].Lable[j].Label,
											level: j + 1
										});
										if (k == CaptionObj.length) {
											$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
											$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
										} else {
											if (CaptionObj[k] != undefined) {
												for (k; k < CaptionObj.length; k++) {
													treeTexts.push(CaptionObj[k]);
													fields.push(CaptionObj[k]);
												}
												$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
												$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
											} else {
												$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
												$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
											}
										}
									} else {
										if (CaptionObj[j] != undefined) {
											$scope.items.push({
												caption: $scope.attributedata[i].Lable[j].Label,
												level: j + 1
											});
											$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
											$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
										} else {
											$scope.items.push({
												caption: $scope.attributedata[i].Lable[j].Label,
												level: j + 1
											});
											$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
											$scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
										}
									}
								}
							}
						}
						$scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
						$scope.items = [];
						for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
							var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
							if ($scope.attributedata[i].IsReadOnly == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
							} else {
								if ($scope.IsLock == false) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
								} else if ($scope.IsLock == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
								}
							}
						}
					} else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
						}
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 19) {
						$scope.isfromtask = 1;
						if ($scope.attributedata[i].Caption[0] != undefined) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
							if ($scope.attributedata[i].Value != null) {
								$scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
								$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
								var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
								$scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function(e) {
									return e.Id == currtypeid;
								}));
								$scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
							} else {
								$scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
								$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
								$scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
							}
							$scope.currencytypeslist = $scope.CurrencyFormatsList;
							$scope.setFieldKeys();
							$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
							if ($scope.attributedata[i].IsReadOnly == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							} else {
								if ($scope.IsLock == false) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
								} else if ($scope.IsLock == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								}
							}
						}
					} else if ($scope.attributedata[i].TypeID == 2) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
						}
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 11) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						$scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
						$scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
						$scope.setUploaderCaption();
						$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
						$scope.dyn_Cont += '<div class=\"controls\">';
						if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
						    $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
						}
						if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
						    $scope.attributedata[i].Value = "NoThumpnail.jpg";
						}
						$scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview UploaderImagePreview1_' + $scope.attributedata[i].ID + '" id="UploaderPreview1_' + $scope.attributedata[i].ID + '">';
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '</div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += "<a ng-click='UploadImagefileTask(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
								$scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
								$scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
								$scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
								$scope.dyn_Cont += '</a></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '</div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 3) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
							if ($scope.attributedata[i].Caption[0] != undefined) {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
									}
								}
							} else {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
									$scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
									$scope.dyn_Cont += '</div></div>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
										$scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
										$scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
										$scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
										$scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
										$scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
										$scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
										$scope.dyn_Cont += '</div></div>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
										$scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
										$scope.dyn_Cont += '</div></div>';
									}
								}
							}
						} else {
							if ($scope.attributedata[i].Caption[0] != undefined) {
								if ($scope.attributedata[i].Caption[0].length > 1) {
									$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
									$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
									if ($scope.attributedata[i].IsReadOnly == true) {
										$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
									} else {
										if ($scope.IsLock == false) {
											$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
										} else if ($scope.IsLock == true) {
											$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
										}
									}
								}
							} else {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
									}
								}
							}
						}
					} else if ($scope.attributedata[i].TypeID == 4) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						if ($scope.attributedata[i].Caption[0] != undefined) {
							if ($scope.attributedata[i].Caption.length > 1) {
								$scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
								$scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalMultiDropdownCaption();
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
									}
								}
							}
						} else {
							$scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
							$scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
							$scope.setNormalMultiDropdownCaption();
							if ($scope.attributedata[i].IsReadOnly == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							} else {
								if ($scope.IsLock == false) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
								} else if ($scope.IsLock == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								}
							}
						}
					} else if ($scope.attributedata[i].TypeID == 10) {
						var inlineEditabletitile = $scope.attributedata[i].Caption;
						$scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
						if ($scope.attributedata[i].Value == "-") {
							$scope.IsStartDateEmpty = true;
							$scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
							$scope.dyn_Cont += '</div>';
						} else {
							for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
								var datStartUTCval = "";
								var datstartval = "";
								var datEndUTCval = "";
								var datendval = "";
								$scope.MinValue = $scope.attributedata[i].MinValue;
								$scope.MaxValue = $scope.attributedata[i].MaxValue;
								$scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
								$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
								if ($scope.MinValue < 0) {
									$scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
								} else {
									$scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
								}
								if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
									$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
								} else {
									$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
								}
								var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id]);
								$scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MinDate);
								$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MaxDate);
								datstartval = new Date.create($scope.attributedata[i].Value[j].Startdate);
								datendval = new Date.create($scope.attributedata[i].Value[j].EndDate);
								perioddates.push({
									ID: $scope.attributedata[i].Value[j].Id,
									value: datendval
								});
								$scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval);
								$scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval);
								$scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datstartval, "dd/MM/yyyy")
								$scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datendval, "dd/MM/yyyy");
								if ($scope.attributedata[i].Value[j].Description == undefined) {
									$scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
									$scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
								} else {
									$scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
									$scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
								}
								$('#fsedateid').css("visibility", "hidden");
								$scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
								$scope.dyn_Cont += '<div class="inputHolder span11">';
								$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
								$scope.dyn_Cont += '<div class="controls">';
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
									$scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
										$scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
										$scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
									}
								}
								$scope.dyn_Cont += '</div></div>';
								$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
								$scope.dyn_Cont += '<div class="controls">';
								if ($scope.attributedata[i].IsReadOnly == true) {
									$scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
								} else {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
									} else if ($scope.IsLock == true) {
										$scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
									}
								}
								$scope.dyn_Cont += '</div></div></div>';
								if (j != 0) {
									if ($scope.IsLock == false) {
										$scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
									}
								}
								$scope.dyn_Cont += '</div>';
								if (j == ($scope.attributedata[i].Value.length - 1)) {
									$scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
									$scope.dyn_Cont += '</div>';
								}
							}
						}
						$scope.dyn_Cont += ' </div>';
						$scope.MinValue = $scope.attributedata[i].MinValue;
						$scope.MaxValue = $scope.attributedata[i].MaxValue;
						$scope.fields["DatePartMinDate_0"] = new Date.create();
						$scope.fields["DatePartMaxDate_0"] = new Date.create();
						if ($scope.MinValue < 0) {
							$scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue + 1));
						} else {
							$scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue));
						}
						if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
							$scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + ($scope.MaxValue - 1));
						} else {
							$scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + 100000);
						}
						$scope.dyn_Cont += '<div class="control-group nomargin">';
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">Start date / End date</label>';
							$scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
							$scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
						} else {
							if ($scope.IsLock == false) {
								if ($scope.attributedata[i].Value == "-") {
									$scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
								}
								$scope.dyn_Cont += '<div class="controls">';
								$scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
								$scope.dyn_Cont += '</div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<span class="controls">[Add ' + inlineEditabletitile + ' ]</span>';
							}
						}
						$scope.dyn_Cont += '</div>';
					} else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
						var datStartUTCval = "";
						var datstartval = "";
						var inlineEditabletitile = $scope.attributedata[i].Caption;
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						if ($scope.attributedata[i].Value != null) {
							datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
							datstartval = new Date.create(datStartUTCval);
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
						} else {
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
						}
						if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
							if ($scope.attributedata[i].IsReadOnly == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							} else {
								if ($scope.IsLock == false) {
									$scope.MinValue = $scope.attributedata[i].MinValue;
									$scope.MaxValue = $scope.attributedata[i].MaxValue;
									$scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
									$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
									if ($scope.MinValue < 0) {
										$scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
									} else {
										$scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
									}
									if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
										$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
									} else {
										$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
									}
									var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
									$scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
									$scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
									$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
									$scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
								} else if ($scope.IsLock == true) {
									$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
								}
							}
						} else {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
						}
					} else if ($scope.attributedata[i].TypeID == 7) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						$scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
						$scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
						GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
						$scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
						$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group">';
						$scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
						$scope.dyn_Cont += '<div class="controls">';
						if ($scope.IsLock == false) {
							$scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
							if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
								treeTextVisbileflag = false;
								if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
									$scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
								} else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
							} else {
								$scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
							}
							$scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
							$scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
							$scope.dyn_Cont += ' </div>';
						} else {
							if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
								treeTextVisbileflag = false;
								if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
									$scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
								} else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
							} else {
								$scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
							}
							$scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
							$scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
						}
						$scope.dyn_Cont += '</div></div>';
					} else if ($scope.attributedata[i].TypeID == 8) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html(($scope.attributedata[i].Caption).formatMoney(0, ' ', ' ')).text();
						}
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 13) {
						$scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
						$scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
						for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {
							if (price.selection.length > 0) {
								var selectiontext = "";
								var valueMatches = [];
								if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function(relation) {
									return price.selection.indexOf(relation.NodeId.toString()) != -1;
								});
								if (valueMatches.length > 0) {
									selectiontext = "";
									for (var x = 0, val; val = valueMatches[x++];) {
										selectiontext += val.caption;
										if (val.value != "") selectiontext += " - " + val.value + "% </br>";
										else selectiontext += "</br>";
									}
								} else selectiontext = "-";
								$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
							} else {
								$scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
							}
							if ($scope.IsLock == false) {
								$scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
								$scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
							} else {
								$scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
								$scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 17) {
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						if ($scope.attributedata[i].Caption[0] != undefined) {
							if ($scope.attributedata[i].Caption.length > 0) {
								$scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
								$scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
							}
						} else {
							$scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
							$scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
						}
						$scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '_taskedit"   data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-original-title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\"  >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 16) {
						var dateExpireUTCval = "";
						var dateExpireval = "";
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						var inlineEditabletitile = $scope.attributedata[i].Caption;
						var dateExpire;
						if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
							var dateExpireUTCval = "";
							var dateExpireval = "";
							dateExpireval = new Date.create($scope.attributedata[i].Value);
							dateExpire = dateFormat(dateExpireval);
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
						} else {
							dateExpire = "-"
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
						}
						$scope.fields["ExpireDateTime_" + $scope.attributedata[i].ID] = dateExpire;
						if ($scope.attributedata[i].IsReadOnly == true) {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span>{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
						} else {
							if ($scope.IsLock == false) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span><a data-AttributeID="' + $scope.attributedata[i].ID + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</a></span></div></div>';
							} else if ($scope.IsLock == true) {
								$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span>{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 18) {
						$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
					}
				}
				if (fromassignedplace) {
					if (tasktype == 2) $("#workAssetdynamicdetail").html($compile($scope.dyn_Cont)($scope));
					if (tasktype == 3) $("#approvalworktaskdynamicHolder").html($compile($scope.dyn_Cont)($scope));
					if (tasktype == 31) $("#reviewtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
					if (tasktype == 36) $("#proofHQtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
					if (tasktype == 37) $("#dalimtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
				} else $("#unassignedtaskdynamicdataholder").html($compile($scope.dyn_Cont)($scope));
				HideAttributeToAttributeRelationsOnPageLoad();
				for (i = 0; i < $scope.attributedata.length; i++) {
					if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
						if ($scope.attributedata[i].TypeID == 12) {
							for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
								if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
									if (($scope.attributedata[i].Lable.length - 1) == j) {
										var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
										for (k; k < $scope.attributedata[i].Value.length; k++) {
											$scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
										}
									} else {
										$scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
									}
								}
							}
						} else if ($scope.attributedata[i].TypeID == 6) {
							for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
								if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
							}
						} else if ($scope.attributedata[i].TypeID == 4) {
							$scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
						} else if ($scope.attributedata[i].TypeID == 7) {
							$scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
						} else {
							$scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, $scope.attributedata[i].TypeID);
						}
					}
				}
			}
		}
		$scope.setNormalMultiDropdownCaption = function() {
			var keys1 = [];
			angular.forEach($scope.NormalMultiDropdownCaption, function(key) {
				keys1.push(key);
				$scope.NormalMultiDropdownCaptionObj = keys1;
			});
		}
		$scope.UploaderCaptionObj = [];
		$scope.setUploaderCaption = function() {
			var keys1 = [];
			angular.forEach($scope.UploaderCaption, function(key) {
				keys1.push(key);
				$scope.UploaderCaptionObj = keys1;
			});
		}
		//$scope.openExpireAction = function(event) {
		//	var attrevent = event.currentTarget;
		//	$scope.SourceAttributeID = attrevent.dataset.attributeid;
		//	$scope.SourceAttributename = attrevent.dataset.attrlable;
		//	$scope.dateExpireval = $scope.fields["ExpireDateTime_" + $scope.SourceAttributeID];
		//	var sourcefrom = 3;
		//	var modalInstance = $modal.open({
		//		templateUrl: 'views/mui/expirehandleractions.html',
		//		controller: "mui.muiexpireaddActionsCtrl",
		//		resolve: {
		//			params: function() {
		//				return {
		//					dateExpireval: $scope.dateExpireval,
		//					AssetEditID: $scope.TaskBriefDetails.taskID,
		//					assetEntityId: $scope.TaskGlobalEntityID,
		//					sourcefrom: sourcefrom,
		//					assettypeidfrbtn: $scope.TaskBriefDetails.taskTypeId,
		//					ProductionEntityID: $scope.TaskGlobalEntityID,
		//					SourceAttributeID: $scope.SourceAttributeID,
		//					SourceAttributename: $scope.SourceAttributename
		//				};
		//			}
		//		},
		//		scope: $scope,
		//		windowClass: 'expirehandlerAddActionsPopup popup-widthL',
		//		backdrop: "static"
		//	});
		//	modalInstance.result.then(function(selectedItem) {
		//		$scope.selected = selectedItem;
		//	}, function() {});
		//}
		//$scope.$on('updateExpireactiontaskEditvalues', function(event, SourceID, EntityID, AttributeID, Attributevalue) {
		//	$scope.fields["ExpireDateTime_" + AttributeID] = Attributevalue;
		//});

		$scope.drpdirectiveSource = {};

		$scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
		    if (levelcnt > 0) {
		        var dropdown_text = '', subid = 0;
		        var currntlevel = attributeLevel + 1;
		        if (attributeLevel == 1) {
		            var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
		            var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
		            if (idtomatch != 0)
		                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
		        }
		        for (var j = currntlevel; j <= levelcnt; j++) {
		            dropdown_text = 'dropdown_text_' + attrID + '_' + j;
		            subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
		            $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
		            if (subid != 0 && subid > 0) {
		                if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
		                    if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
		                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
		                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
		                }
		                else {
		                    if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
		                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
		                }
		            }
		            else {
		                if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
		                    if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
		                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
		                    $scope['dropdown_' + attrID + '_' + j] = 0;
		                }
		                else {
		                    if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
		                        var res = [];
		                        if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
		                            $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
		                        else
		                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
		                    }
		                }
		            }
		        }
		    }
		}

		$scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

		    if (levelcnt > 0) {
		        var currntlevel = attributeLevel + 1;
		        for (var j = currntlevel; j <= levelcnt; j++) {
		            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
		            if (attrType == 6) {
		                $scope["dropdown_" + attrID + "_" + j] = 0;
		                $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
		            } else if (attrType == 12) {
		                if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
		                else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
		            }
		        }
		        if (attrType == 6) {
		            if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
		                var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

		                if (children != undefined) {
		                    var subleveloptions = [];
		                    $.each(children, function (i, el) {
		                        subleveloptions.push(el);
		                    });
		                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
		                }
		                else
		                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
		            }
		        } else if (attrType == 12) {
		            if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

		                var sublevel_res = [];
		                if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
		                    sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
		                if (sublevel_res != undefined) {
		                    var subleveloptions = [];
		                    $.each(sublevel_res, function (i, el) {
		                        subleveloptions.push(el);
		                    });
		                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
		                }
		                else
		                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
		            }
		        }
		    }
		}

		var treeTextVisbileflag = false;

		function IsNotEmptyTree(treeObj) {
			for (var i = 0, node; node = treeObj[i++];) {
				if (node.ischecked == true) {
					treeTextVisbileflag = true;
					return treeTextVisbileflag;
				} else {
					IsNotEmptyTree(node.Children);
				}
			}
			return treeTextVisbileflag;
		}
		$scope.deletePeriodDate = function(periodid) {
			TaskNewService.DeleteEntityPeriod(periodid).then(function(deletePerById) {
				if (deletePerById.StatusCode == 200) {
					NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
					UpdateEntityPeriodTree($stateParams.ID);
					$("#dynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');
					perioddates = $.grep(perioddates, function(val) {
						return val.ID != periodid;
					});
				} else {
					NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
				}
			});
		}
		$scope.saveDropdownTree = function(attrID, attributetypeid, entityTypeid, optionarray) {
			if (attributetypeid == 6) {
				var updateentityattrib = {};
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				updateentityattrib.NewValue = optionarray;
				updateentityattrib.AttributetypeID = attributetypeid;
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					} else {
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						for (var i = 0; i < optionarray.length; i++) {
							if (optionarray[i] != 0) {
								var level = i + 1;
								UpdateTreeScope(attrID + "_" + level, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"][data-ng-model="dropdown_text_' + attrID + "_" + level + '"]').text());
							}
						}
						for (var j = 0; j < optionarray.length; j++) {
							if (optionarray[j] != 0) {
								$scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
							}
						}
					}
				});
				$scope.treeSelection = [];
			} else if (attributetypeid == 1 || attributetypeid == 2) {
				var updateentityattrib = {};
				updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
				updateentityattrib.AttributetypeID = attributetypeid;
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				if (attrID == 68) {
					$(window).trigger("UpdateEntityName", optionarray);
					$("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "'] span[data-name='text']").text([optionarray]);
					$("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "']").attr('data-entityname', optionarray);
					var Splitarr = [];
					for (var i = 0; i < $scope.ListViewDetails.length; i++) {
						for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
							if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
								if ($scope.TaskBriefDetails.taskID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $scope.TaskBriefDetails.taskID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
									if (Splitarr.length == 0) {
										Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
									}
									$scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
								}
							} else {
								break;
							}
						}
					}
				}
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else {
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
						$scope.fields['SingleLineTextValue_' + attrID] = optionarray;
						if (attrID == 68) {
							$('#breadcrumlink').text(optionarray);
						}
					}
				});
				$scope.treeSelection = [];
			} else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
				var updateentityattrib = {};
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				updateentityattrib.NewValue = optionarray == "" ? ["0"] : optionarray;
				updateentityattrib.AttributetypeID = attributetypeid;
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
					UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
				});
				$scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
				$scope.treeSelection = [];
			} else if (attributetypeid == 12) {
				var updateentityattrib = {};
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				updateentityattrib.NewValue = optionarray;
				updateentityattrib.AttributetypeID = attributetypeid;
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else {
						for (var j = 0; j < optionarray.length; j++) {
							if (optionarray[j] != 0) {
								$scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
							}
						}
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
					}
				});
				$scope.treeSelection = [];
			} else if (attributetypeid == 19) {
				$scope.newamount = optionarray[0];
				$scope.newcurrencytypename = optionarray[2];
				var updateentityattrib = {};
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				updateentityattrib.NewValue = optionarray;
				updateentityattrib.AttributetypeID = attributetypeid;
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
					$scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
					$scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
					$scope.fields["NormalDropDown_" + attrID] = optionarray[1];
				});
				$scope.treeSelection = [];
			} else if (attributetypeid == 8) {
				if (!isNaN(optionarray.toString())) {
					var updateentityattrib = {};
					updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
					updateentityattrib.AttributeID = attrID;
					updateentityattrib.Level = 0;
					updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
					updateentityattrib.AttributetypeID = attributetypeid;
					TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
						if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						else {
							$scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : parseInt(optionarray).formatMoney(0, ' ', ' ');
							NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
							UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
						}
					});
					$scope.treeSelection = [];
				} else {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
				}
			} else if (attributetypeid == 5) {
				var sdate = "";
				if ($scope.fields['DateTime_Dir_' + attrID].toString() != "" && $scope.fields['DateTime_Dir_' + attrID] != null && $scope.fields['DateTime_Dir_' + attrID].toString() != "-") {
					sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
					if (sdate == 'NaN-NaN-NaN') {
						sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
					}
				}
				var updateentityattrib = {};
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				updateentityattrib.Level = 0;
				updateentityattrib.NewValue = [sdate];
				updateentityattrib.AttributetypeID = attributetypeid;
				TaskNewService.SaveDetailBlockForLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else {
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());
						$scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.DefaultSettings.DateFormat);
					}
				});
				$scope.treeSelection = [];
			}
			$scope.treeSelection = [];
		};
		$scope.savePeriodVal = function(attrID, attrTypeID, entityid, periodid) {
			var one_day = 1000 * 60 * 60 * 24;
			if (periodid == 0) {
				var newperiod = {};
				var tempsdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid]);
				var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
				if (sdate == 'NaN-NaN-NaN') {
					sdate = dateFormat(formatteddateFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
				}
				var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
				if (edate == 'NaN-NaN-NaN') {
					edate = dateFormat(formatteddateFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
				}
				var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
				var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
				if (diffval < 0) {
					$scope.fields["PeriodStartDate_Dir_0"] = "";
					$scope.fields["PeriodEndDate_Dir_0"] = "";
					$scope.fields["PeriodDateDesc_Dir_0"] = "";
					bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
					return false;
				}
				var tempperioddates = [];
				for (var i = 0; i < perioddates.length; i++) {
					tempperioddates.push(perioddates[i].value)
				}
				var one_day = 1000 * 60 * 60 * 24;
				var maxPeriodSDate = new Date.create(Math.max.apply(null, tempsdate));
				var Convertsdate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
				var diffval = (parseInt(Convertsdate.getTime() - tempsdate.getTime()));
				if (diffval < 1) {
					$scope.fields["PeriodStartDate_Dir_0"] = "";
					$scope.fields["PeriodEndDate_Dir_0"] = "";
					$scope.fields["PeriodDateDesc_Dir_0"] = "";
					bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
					return false;
				}
				newperiod.EntityID = entityid;
				newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
				newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
				newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
				newperiod.SortOrder = 0;
				TaskNewService.InsertEntityPeriod(newperiod).then(function(resultperiod) {
					if (resultperiod.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					else var newid = resultperiod.Response;
					if ($scope.IsStartDateEmpty == true) {
						$scope.IsStartDateEmpty = false;
						$("[data-tempid=startendID]").remove();
					}
					perioddates.push({
						ID: newid,
						value: edate
					});
					$scope.dyn_Cont = '';
					$scope.fields["PeriodStartDate_" + newid] = dateFormat(sdate);
					$scope.fields["PeriodEndDate_" + newid] = dateFormat(edate);
					$scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
					$scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
					$scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
					if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
						$scope.fields["PeriodDateDesc_" + newid] = "-";
					} else {
						$scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
					}
					$('#fsedateid').css("visibility", "hidden");
					$scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';
					$scope.dyn_Cont += '<div class="inputHolder span11">';
					$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Period </label>';
					$scope.dyn_Cont += '<div class="controls">';
					$scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
					$scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
					$scope.dyn_Cont += '</div></div>';
					$scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment Period </label>';
					$scope.dyn_Cont += '<div class="controls">';
					$scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
					$scope.dyn_Cont += '</div></div></div>';
					if (perioddates.length != 1) {
						$scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
					}
					var divcompile = $compile($scope.dyn_Cont)($scope);
					if ($scope.fromAssignedpopup) {
						if ($scope.TaskBriefDetails.taskTypeId == 2) $("#workAssetdynamicdetail div[data-addperiodid]").append(divcompile);
						if ($scope.TaskBriefDetails.taskTypeId == 3) $("#approvalworktaskdynamicHolder div[data-addperiodid]").append(divcompile);
						if ($scope.TaskBriefDetails.taskTypeId == 31) $("#reviewtaskdynamicholder div[data-addperiodid]").append(divcompile);
						if ($scope.TaskBriefDetails.taskTypeId == 36) $("#proofHQtaskdynamicholder div[data-addperiodid]").append(divcompile);
						if ($scope.TaskBriefDetails.taskTypeId == 37) $("#dalimtaskdynamicholder div[data-addperiodid]").append(divcompile);
					} else $("#unassignedtaskdynamicdataholder div[data-addperiodid]").append(divcompile);
					$scope.fields["PeriodStartDate_Dir_0"] = "";
					$scope.fields["PeriodEndDate_Dir_0"] = "";
					$scope.fields["PeriodDateDesc_Dir_0"] = "";
					NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
				});
			} else {
				var updateperiod = {};
				var temparryStartDate = [];
				$('[data-periodstartdate_id^=PeriodStartDate_]').each(function() {
					if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
						var sdate;
						if (this.text != "[Add Start / End Date ]") {
							sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
							temparryStartDate.push(sdate);
						}
					}
				});
				var temparryEndate = [];
				$('[data-periodenddate_id^=PeriodEndDate_]').each(function() {
					if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
						var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
						temparryEndate.push(edate);
					}
				});
				var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
				if (sdate == 'NaN-NaN-NaN') {
					sdate = dateFormat(formatteddateFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
				}
				var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
				if (edate == 'NaN-NaN-NaN') {
					edate = dateFormat(formatteddateFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
				}
				var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
				if (diffval < 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
					$scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
					$scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
					return false;
				}
				var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
				var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
				var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
				if (parseInt(diffvalend) < 1) {
					$scope.fields["PeriodStartDate_Dir_0"] = "";
					$scope.fields["PeriodEndDate_Dir_0"] = "";
					$scope.fields["PeriodDateDesc_Dir_0"] = "";
					$scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
					$scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
					bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
					return false;
				}
				var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
				var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
				var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
				if (parseInt(diffvalstart) > 1) {
					$scope.fields["PeriodStartDate_Dir_0"] = "";
					$scope.fields["PeriodEndDate_Dir_0"] = "";
					$scope.fields["PeriodDateDesc_Dir_0"] = "";
					$scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
					$scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
					bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));
					return false;
				}
				updateperiod.ID = periodid;
				updateperiod.EntityID = entityid;
				updateperiod.StartDate = sdate;
				updateperiod.EndDate = edate;
				updateperiod.SortOrder = 0;
				updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
				TaskNewService.PostEntityPeriod1(updateperiod).then(function(resultperiod) {
					if (resultperiod.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
					} else {
						$scope.fields["PeriodStartDate_" + periodid] = dateFormat(sdate);
						$scope.fields["PeriodEndDate_" + periodid] = dateFormat(edate);
						$scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
					}
				});
			}
		}
		$scope.savetreeDetail = function(attrID, attributetypeid, entityTypeid) {
			if (attributetypeid == 7) {
				$scope.treeNodeSelectedHolder = [];
				GetTreeObjecttoSave(attrID);
				var updateentityattrib = {};
				updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
				updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
				updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
				updateentityattrib.AttributetypeID = attributetypeid;
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				TaskNewService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					} else {
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						TaskNewService.GetAttributeTreeNodeByEntityID(attrID, parseInt($scope.TaskBriefDetails.taskID, 10)).then(function(GetTree) {
							$scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
							if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
							else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
						});
						$scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
						GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
						$scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
					}
				});
			}
		}
		$scope.saveDropDownTreePricing = function(attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
			if (attributetypeid == 13) {
				var NewValue = [];
				NewValue = ReturnSelectedTreeNodes(attrID);
				var updateentityattrib = {};
				updateentityattrib.NewValue = NewValue;
				updateentityattrib.AttributetypeID = attributetypeid;
				updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
				updateentityattrib.AttributeID = attrID;
				TaskNewService.UpdateDropDownTreePricing(updateentityattrib).then(function(updateentityattribresult) {
					if (updateentityattribresult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					} else {
						NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
						var GetTreeRes;
						if (choosefromParent) {
							TaskNewService.GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId).then(function(GetTree) {
								if (GetTree.Response != null) {
									var result = GetTree.Response;
									for (var p = 0, price; price = result[p++];) {
										var attributeLevelOptions = [];
										attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function(e) {
											return e.level == p;
										}));
										if (attributeLevelOptions[0] != undefined) {
											attributeLevelOptions[0].selection = price.selection;
											attributeLevelOptions[0].LevelOptions = price.LevelOptions;
											if (price.selection.length > 0) {
												var selectiontext = "";
												var valueMatches = [];
												if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function(relation) {
													return price.selection.indexOf(relation.NodeId.toString()) != -1;
												});
												if (valueMatches.length > 0) {
													selectiontext = "";
													for (var x = 0, val; val = valueMatches[x++];) {
														selectiontext += val.caption;
														if (val.value != "") selectiontext += " - " + val.value + "% </br>";
														else selectiontext += "</br>";
													}
												} else selectiontext = "-";
												$scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
											} else {
												$scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
											}
										}
									}
								}
							});
						} else {
							TaskNewService.GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId).then(function(GetTree) {
								if (GetTree.Response != null) {
									var result = GetTree.Response;
									for (var p = 0, price; price = result[p++];) {
										var attributeLevelOptions = [];
										attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function(e) {
											return e.level == p;
										}));
										if (attributeLevelOptions[0] != undefined) {
											attributeLevelOptions[0].selection = price.selection;
											attributeLevelOptions[0].LevelOptions = price.LevelOptions;
											if (price.selection.length > 0) {
												var selectiontext = "";
												var valueMatches = [];
												if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function(relation) {
													return price.selection.indexOf(relation.NodeId.toString()) != -1;
												});
												if (valueMatches.length > 0) {
													selectiontext = "";
													for (var x = 0, val; val = valueMatches[x++];) {
														selectiontext += val.caption;
														if (val.value != "") selectiontext += " - " + val.value + "% </br>";
														else selectiontext += "</br>";
													}
												} else selectiontext = "-";
												$scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
											} else {
												$scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
											}
										}
									}
								}
							});
						}
					}
				});
			}
		}

		function ReturnSelectedTreeNodes(attrID) {
			var selection = [];
			for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {
				if (price.selection.length > 0) {
					var matches = [];
					matches = jQuery.grep(price.LevelOptions, function(relation) {
						return price.selection.indexOf(relation.NodeId.toString()) != -1;
					});
					if (matches.length > 0) for (var z = 0, node; node = matches[z++];) {
						selection.push({
							"NodeId": node.NodeId,
							"Level": price.level,
							"value": node.value != "" ? node.value : "-1"
						})
					}
				}
			}
			return selection;
		}

		function UpdateTreeScope(ColumnName, Value) {
			for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
				if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
					for (var i = 0; i < $scope.ListViewDetails.length; i++) {
						for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
							var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
							if (EntityId == parseInt($scope.TaskBriefDetails.taskID, 10)) {
								$scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
								return false;
							}
						}
					}
				}
			}
		}

		function GetTreeObjecttoSave(attributeid) {
			GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
		}
		var treeformflag = false;

		function GenerateTreeStructure(treeobj) {
			for (var i = 0, node; node = treeobj[i++];) {
				if (node.ischecked == true) {
					var remainRecord = [];
					remainRecord = $.grep($scope.treeNodeSelectedHolder, function(e) {
						return e.AttributeId == node.AttributeId && e.id == node.id;
					});
					if (remainRecord.length == 0) {
						$scope.treeNodeSelectedHolder.push(node);
					}
					treeformflag = false;
					if (ischildSelected(node.Children)) {
						GenerateTreeStructure(node.Children);
					} else {
						GenerateTreeStructure(node.Children);
					}
				} else GenerateTreeStructure(node.Children);
			}
		}

		function ischildSelected(children) {
			for (var j = 0, child; child = children[j++];) {
				if (child.ischecked == true) {
					treeformflag = true;
					return treeformflag
				}
			}
			return treeformflag;
		}
		$scope.AttributeData = [];
		$scope.Entityamountcurrencytypeitem = [];
		$scope.AttributeData = [];
		$scope.CreateUnassignedNewTask = function() {
			$scope.addtaskvallid = 0;
			if ($('#ngShowUnassigned').hasClass('disabled')) {
				return;
			}
			$('#ngShowUnassigned').addClass('disabled');
			if ($("#assetlistActivityTask  > div").length > 0) {
				$scope.uploadingInProgress = true;
			}
			var percentageflag = false;
			$('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function(index, value) {
				if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
					if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
						percentageflag = true;
					}
				}
			});
			if (percentageflag) {
				$scope.uploadingInProgress = false;
				return false;
			}
			$("#tskbtnTemp").click();
			$("#TaskDynamicMetadata").removeClass('notvalidate');
			if ($("#TaskDynamicMetadata .error").length > 0) {
				$scope.uploadingInProgress = false;
				return false;
			}
			var dateval = new Date.create();
			dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
			var alertText = "";
			$scope.AttributeData = [];
			var taskMembersAvailable = [];
			if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "") taskMembersAvailable = [];
			else taskMembersAvailable = $scope.TaskMembersList;
			if ($scope.TaskType == "" || $scope.TaskType == undefined || $scope.TaskType == 0) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
			var Tasktype_value = $.grep($scope.TaskTypeList, function(rel) {
				return rel.Id == ($scope.TaskType.id == undefined ? $scope.TaskType : $scope.TaskType.id);
			})[0];
			if (Tasktype_value != undefined) if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
				if (!alertText.contains($translate.instant('LanguageContents.Res_4637.Caption'))) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
			}
			if ($scope.NewTaskName == "") alertText += $translate.instant('LanguageContents.Res_4587.Caption');
			if ($scope.TaskDueDate != undefined && new Date.create(dateFormat($scope.TaskDueDate, $scope.DefaultSettings.DateFormat)) < dateval) alertText += ($translate.instant('LanguageContents.Res_1909.Caption'));
			if (taskMembersAvailable.length > 0) {
				if ($scope.TaskDueDate == "" || $scope.TaskDueDate == undefined || $scope.TaskDueDate == null) alertText += $translate.instant('LanguageContents.Res_4223.Caption');
			}
			if (alertText == "") {
				$scope.addtaskvallid = 1;
				$scope.AttributeData = [];
				var Tasktype_value = $.grep($scope.TaskTypeList, function(rel) {
					return rel.Id == ($scope.TaskType.id == undefined ? $scope.TaskType : $scope.TaskType.id);
				})[0];
				var SaveTask = {};
				SaveTask.TaskType = Tasktype_value.TaskTypeId;
				SaveTask.Typeid = $scope.TaskType;
				SaveTask.Name = $scope.NewTaskName;
				SaveTask.TaskListID = $scope.SelectedTaskLIstID;
				SaveTask.TaskID = $scope.SelectedUnassignedTaskID;
				SaveTask.Description = $scope.TaskDescription;
				SaveTask.Note = $scope.TaskNote;
				SaveTask.TaskAttachments = [];
				SaveTask.TaskAttachments = $scope.AttachmentFilename;
				SaveTask.TaskFiles = $scope.FileList;
				SaveTask.ParentEntityID = $scope.TaskGlobalEntityID;
				SaveTask.DueDate = $scope.TaskDueDate != undefined ? $scope.TaskDueDate : "";
				SaveTask.TaskMembers = [];
				var taskOwnerObj = [];
				taskOwnerObj = $scope.OwnerList[0];
				SaveTask.TaskMembers = $scope.TaskMembersList != undefined ? $scope.TaskMembersList : [];
				SaveTask.Entityamountcurrencytype = [];
				$scope.curram = [];
				for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
					$scope.curram.push({
						amount: $scope.Entityamountcurrencytypeitem[m].amount,
						currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
						Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
					});
				}
				SaveTask.Entityamountcurrencytype.push($scope.curram);
				for (var i = 0; i < $scope.atributesRelationList.length; i++) {
					if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
						for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
							if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
								$scope.AttributeData.push({
									"AttributeID": $scope.atributesRelationList[i].AttributeID,
									"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
									"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
									"NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
									"Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
									"Value": "-1"
								});
							}
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
						for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
							var levelCount = $scope.atributesRelationList[i].Levels.length;
							if (levelCount == 1) {
							    if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
							        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
							            $scope.AttributeData.push({
							                "AttributeID": $scope.atributesRelationList[i].AttributeID,
							                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
							                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							                "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
							                "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
							                "Value": "-1"
							            });
							        }
							    }
							} else {
								if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
									if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
										for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
											$scope.AttributeData.push({
												"AttributeID": $scope.atributesRelationList[i].AttributeID,
												"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
												"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
												"NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]],
												"Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
												"Value": "-1"
											});
										}
									} else {
										$scope.AttributeData.push({
											"AttributeID": $scope.atributesRelationList[i].AttributeID,
											"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
											"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
											"NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]],
											"Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
											"Value": "-1"
										});
									}
								}
							}
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
						for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
							var attributeLevelOptions = [];
							attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function(e) {
								return e.level == (j + 1);
							}));
							if (attributeLevelOptions[0] != undefined) {
								if (attributeLevelOptions[0].selection != undefined) {
									for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
										var valueMatches = [];
										if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
											return relation.NodeId.toString() === opt;
										});
										$scope.AttributeData.push({
											"AttributeID": $scope.atributesRelationList[i].AttributeID,
											"AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
											"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
											"NodeID": [opt],
											"Level": (j + 1),
											"Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
										});
									}
								}
							}
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
						if ($scope.atributesRelationList[i].IsSpecial == true) {
							if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
								$scope.AttributeData.push({
									"AttributeID": $scope.atributesRelationList[i].AttributeID,
									"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
									"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
									"NodeID": parseInt($scope.OwnerList[0].Userid, 10),
									"Level": 0,
									"Value": "-1"
								});
							}
						} else if ($scope.atributesRelationList[i].IsSpecial == false) {
							if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
								var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
								$scope.AttributeData.push({
									"AttributeID": $scope.atributesRelationList[i].AttributeID,
									"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
									"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
									"NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
									"Level": 0,
									"Value": "-1"
								});
							}
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
						if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
						else {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
								"Level": 0,
								"Value": "-1"
							});
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
						if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
								"Level": 0,
								"Value": "-1"
							});
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
						if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
							if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
								var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
								for (var k = 0; k < multiselectiObject.length; k++) {
									$scope.AttributeData.push({
										"AttributeID": $scope.atributesRelationList[i].AttributeID,
										"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
										"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
										"NodeID": parseInt(multiselectiObject[k], 10),
										"Level": 0,
										"Value": "-1"
									});
								}
							}
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
						var MyDate = new Date.create();
						var MyDateString;
						if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
							if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]) MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
							else MyDateString = "";
						} else {
							MyDateString = "";
						}
						if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": MyDateString,
								"Level": 0,
								"Value": "-1"
							});
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
						var treenodes = [];
						treenodes = $.grep($scope.treeNodeSelectedHolder, function(e) {
							return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
						});
						for (var x = 0, nodeval; nodeval = treenodes[x++];) {
							$scope.AttributeData.push({
								"AttributeID": $scope.atributesRelationList[i].AttributeID,
								"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
								"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
								"NodeID": [parseInt(nodeval.id, 10)],
								"Level": parseInt(nodeval.Level, 10),
								"Value": "-1"
							});
						}
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
						$scope.AttributeData.push({
							"AttributeID": $scope.atributesRelationList[i].AttributeID,
							"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
							"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							"NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
							"Level": 0,
							"Value": "-1"
						});
					} else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
						$scope.AttributeData.push({
							"AttributeID": $scope.atributesRelationList[i].AttributeID,
							"AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
							"AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
							"NodeID": "",
							"Level": 0,
							"Value": "-1"
						});
					}
				}
				SaveTask.AttributeData = $scope.AttributeData;
				var taskResObj = $.grep($scope.AdminTaskCheckList, function(e) {
					return e.Name != null;
				});
				SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
				TaskNewService.InsertUnassignedEntityTaskWithAttachments(SaveTask).then(function(SaveTaskResult) {
					if (SaveTaskResult.StatusCode == 405) {
						$('#ngShowUnassigned').removeClass('disabled');
						NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
						$scope.uploadingInProgress = false;
					} else {
						if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
							var returnObj = SaveTaskResult.Response.m_Item2;
							$scope.newTaskid = SaveTaskResult.Response.m_Item1;
							$scope.returnObj = returnObj;
							if ($("#assetlistActivityTask > div").length == 0 && $("#taskentityattachassetdata > div").length == 0) {
								$('#ngShowUnassigned').removeClass('disabled');
								NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
								if ($scope.TaskActionFor == 1) RereshTaskObj(returnObj);
								else RereshTaskObj(returnObj);
								$scope.FileList = [];
								$scope.SelectedTaskLIstID = 0;
								$scope.AttachmentFilename = [];
								refreshModel();
								$scope.newTaskid = 0;
								$scope.returnObj = [];
								$scope.addfromGenAttachments = false;
							} else if ($("#taskentityattachassetdata > div").length > 0) {
								$timeout(function() {
									TaskNewService.AttachAssets($scope.TaskAttachAsset.SelectedAssetFiles, $scope.newTaskid, 0).then(function(result) {
										if (result.Response == 0) {
											$('#ngShowUnassigned').removeClass('disabled');
											NotifyError($translate.instant('LanguageContents.Res_4327.Caption'));
										} else {
											$scope.TaskAttachAsset.SelectedAssetFiles = [];
											$scope.TaskAttachAsset.html = '';
											if ($("#assetlistActivityTask > div").length == 0) {
												$('#ngShowUnassigned').removeClass('disabled');
												NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
											}
										}
									});
								}, 50);
								if ($("#assetlistActivityTask > div").length == 0) {
									$('#ngShowUnassigned').removeClass('disabled');
									NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
									if ($scope.TaskActionFor == 1) RereshTaskObj(returnObj);
									else RereshTaskObj(returnObj);
									$scope.FileList = [];
									$scope.SelectedTaskLIstID = 0;
									$scope.AttachmentFilename = [];
									refreshModel();
									$scope.newTaskid = 0;
									$scope.returnObj = [];
									$scope.addfromGenAttachments = false;
									$scope.TaskAttachAsset.html = '';
								} else if ($("#assetlistActivityTask  > div").length > 0) {
									$scope.uploadingInProgress = true;
									$("#tempTaskCreation").click();
								}
							} else if ($("#assetlistActivityTask  > div").length > 0) {
								$scope.uploadingInProgress = true;
								$("#tempTaskCreation").click();
							}
						} else {
							$('#ngShowUnassigned').removeClass('disabled');
							NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
							$scope.uploadingInProgress = false;
						}
					}
				});
			} else {
				$('#ngShowUnassigned').removeClass('disabled');
				bootbox.alert(alertText);
				return false;
			}
		}
		$scope.SetToCompleteUnassignedTask = function() {
			$scope.AttributeData = [];
			var dateval = new Date.create();
			var taskListResObj = [];
			var EntityTaskObj = [];
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				EntityTaskObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				if (EntityTaskObj.length > 0) {
					taskListResObj = $.grep(EntityTaskObj[0].TaskList, function(e) {
						return e.Id == $scope.contextTaskID;
					});
				}
			} else {
				EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.ContextTaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskObjHolder.length > 0) taskListResObj = taskObjHolder;
					}
				}
			}
			if (taskListResObj[0].TaskType == 3 || taskListResObj[0].TaskType == 31 || taskListResObj[0].TaskType == 32 || taskListResObj[0].TaskType == 36 || taskListResObj[0].TaskType == 37) {
				if ((taskListResObj[0].TaskStatus == 0 || taskListResObj[0].TaskStatus == 1)) {
					var SaveTask = {};
					SaveTask.TaskID = taskListResObj[0].Id;
					TaskNewService.CompleteUnnassignedEntityTask(SaveTask).then(function(SaveTaskResult) {
						if (SaveTaskResult.StatusCode == 405) {
							NotifyError($translate.instant('LanguageContents.Res_4276.Caption'));
						} else {
							if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
								var returnObj = SaveTaskResult.Response.m_Item2;
								NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
								RefreshUnassignedTaskInObject(returnObj);
							} else {
								NotifyError($translate.instant('LanguageContents.Res_4276.Caption'));
							}
						}
					});
				} else {}
			} else {
				if (taskListResObj[0].TaskType == 2) {
					if (taskListResObj[0].TaskStatus != 2 && taskListResObj[0].TaskStatus != 8) {
						var SaveTask = {};
						SaveTask.TaskID = taskListResObj[0].Id;
						TaskNewService.CompleteUnnassignedEntityTask(SaveTask).then(function(SaveTaskResult) {
							if (SaveTaskResult.StatusCode == 405) {
								NotifyError($translate.instant('LanguageContents.Res_4276.Caption'));
							} else {
								if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
									var returnObj = SaveTaskResult.Response.m_Item2;
									NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
									RefreshUnassignedTaskInObject(returnObj);
								} else {
									NotifyError($translate.instant('LanguageContents.Res_4276.Caption'));
								}
							}
						});
					} else {
						bootbox.alert($translate.instant('LanguageContents.Res_1959.Caption'));
					}
				}
			}
		}

		function RefreshUnassignedTaskInObject(taskObj) {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				if (taskListResObj.length > 0) {
					if (taskListResObj[0].TaskList != null) {
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskResObj.length > 0) {
							UpdateTaskObject(taskResObj, taskObj)
						} else {
							taskListResObj[0].TaskList.push(taskObj);
						}
					} else {
						taskListResObj[0].TaskList.push(taskObj);
					}
				}
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == taskObj.EntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == taskObj.TaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskObjHolder.length > 0) {
							UpdateTaskObject(taskObjHolder, taskObj)
						}
					}
				}
			}
		}

		function UpdateTaskObject(taskResObj, taskObj) {
			taskResObj[0].Name = taskObj.Name;
			taskResObj[0].Description = taskObj.Description;
			taskResObj[0].Id = taskObj.Id;
			taskResObj[0].TaskTypeName = taskObj.TaskTypeName
			taskResObj[0].TaskType = taskObj.TaskType;
			taskResObj[0].TaskListID = taskObj.TaskListID;
			taskResObj[0].TaskStatus = taskObj.TaskStatus;
			taskResObj[0].EntityID = taskObj.EntityID;
			taskResObj[0].EntityTaskListID = taskObj.EntityTaskListID;
			taskResObj[0].StatusName = taskObj.StatusName;
			taskResObj[0].SortOrder = taskObj.SortOrder;
			taskResObj[0].strDate = taskObj.strDate;
			taskResObj[0].TaskCheckList = null;
			taskResObj[0].Note = taskObj.Note;
			taskResObj[0].DueDate = taskObj.DueDate;
			if (new Date.create(taskObj.DueDate) > new Date.create()) {
				taskObj.totalDueDays = taskObj.totalDueDays - HolidayCount(new Date.create(taskObj.DueDate));
			} else {
				taskObj.totalDueDays = taskObj.totalDueDays + HolidayCount(new Date.create(taskObj.DueDate));
			}
			taskResObj[0].totalDueDays = taskObj.totalDueDays;
			taskResObj[0].taskMembers = taskObj.taskMembers;
			taskResObj[0].TotaltaskAssigness = taskObj.TotaltaskAssigness;
			taskResObj[0].taskAssigness = taskObj.taskAssigness;
			taskResObj[0].ProgressCount = taskObj.ProgressCount;
			taskResObj[0].AssigneeName = taskObj.AssigneeName;
			taskResObj[0].AssigneeID = taskObj.AssigneeID;
			taskResObj[0].AttributeData = taskObj.AttributeData;
			taskResObj[0].MemberFlagColorCode = taskObj.MemberFlagColorCode;
			taskResObj[0].TotalTaskAttachment = taskObj.TotalTaskAttachment;
			taskResObj[0].taskAttachment = taskObj.taskAttachment;
		}
		$scope.FileWizardHeader = "Change friendly name";
		$scope.EditLink = false;
		$scope.SaveLink = true;
		$scope.HidecfnTitle = "Add link";
		$scope.fileSaveObject = {
			"FileFriendlyName": "",
			"FileDescription": ""
		};
		$scope.linkSaveObject = {
			"LinkFriendlyName": "",
			"linkDescription": ""
		};
		$scope.ChangeFileFriendlyName = function() {
			$('#TaskFileControls tbody tr').remove();
			var taskListResObj = $.grep($scope.groups, function(e) {
				return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
			});
			var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
				return e.Id == $scope.TaskBriefDetails.taskID;
			});
			if (taskResObj.length > 0) {
				if (taskResObj[0].taskAttachment.length > 0) {
					var taskAttachmentObj = $.grep(taskResObj[0].taskAttachment, function(e) {
						return (e.Id == parseInt($scope.contextFileID));
					});
					if (taskAttachmentObj.length > 0) {
						if (taskAttachmentObj[0].Extension != "Link") {
							$scope.FileWizardHeader = "Change friendly name";
							var htmlStr = '';
							htmlStr += '<tr> ';
							htmlStr += ' <td>Name:</td> ';
							htmlStr += ' <td> ';
							htmlStr += '    <input type="text" id="friendlyName" ng-model="fileSaveObject.FileFriendlyName"  placeholder="Friendly Name">';
							htmlStr += ' </td> ';
							htmlStr += ' <td>Description:</td> ';
							htmlStr += ' <td> ';
							htmlStr += '    <textarea id="FileDescriptionVal" ng-model="fileSaveObject.FileDescription" placeholder="File Description" rows="3"></textarea>';
							htmlStr += ' </td> ';
							htmlStr += '</tr> ';
							var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
							$("#TaskFileUpdateModal").modal("show");
							$timeout(function() {
								$('#friendlyName').focus().select();
							}, 1000);
							$scope.fileSaveObject.FileFriendlyName = trimFileName;
							$scope.fileSaveObject.FileDescription = taskAttachmentObj[0].Description;
							$scope.FileChange = true;
							$scope.LinkChange = false;
							$("#TaskFileControls tbody").append($compile(htmlStr)($scope));
						} else {
							$scope.HidecfnTitle = "Edit link";
							$scope.TasktxtLinkName = taskAttachmentObj[0].Name;
							$scope.TasktxtLinkDesc = taskAttachmentObj[0].Description;
							$scope.TasktxtLinkURL = taskAttachmentObj[0].LinkURL.replace('http://', '').replace('https://', '').replace("ftp://", "").replace("file://", "");
							$scope.linkType = taskAttachmentObj[0].LinkType;
							$("#TaskaddLinkPopup").modal("show");
							$timeout(function() {
								$('#TasktxtLinkName').focus().select();
							}, 1000);
							$scope.EditLink = true;
							$scope.SaveLink = false;
						}
					}
				}
			}
		}
		$scope.FileID = 0;
		$scope.ChangeVersionNo = function() {
			$scope.FileID = $scope.contextFileID;
			$scope.IsNotVersioning = false;
			$scope.fnTimeOut();
		}
		$scope.versionFileId = 0;
		$scope.TaskVersionFileList = [];
		$scope.ViewAllFiles = function() {
			$scope.FileID = $scope.contextFileID;
			var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function(n, i) {
				return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
			});
			if (objfile != null && objfile.length > 0) {
				$scope.versionFileId = objfile[0].VersioningFileId;
				var taskid = $scope.TaskBriefDetails.taskID;
				TaskNewService.ViewAllFilesByEntityID($scope.TaskBriefDetails.taskID, objfile[0].VersioningFileId).then(function(FileResult) {
					if (FileResult.Response != null) {
						$scope.TaskVersionFileList = FileResult.Response;
						$scope.SelectedVersionID = $scope.TaskVersionFileList[0].VersionNo;
					}
				});
			}
		}
		$scope.ChangeFileDescription = function() {
			$('#TaskFileControls tbody tr').remove();
			var taskListResObj = $.grep($scope.groups, function(e) {
				return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
			});
			var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
				return e.Id == $scope.TaskBriefDetails.taskID;
			});
			if (taskResObj.length > 0) {
				if (taskResObj[0].taskAttachment.length > 0) {
					var taskAttachmentObj = $.grep(taskResObj[0].taskAttachment, function(e) {
						return (e.Id == parseInt($scope.contextFileID));
					});
					if (taskAttachmentObj.length > 0) {
						if (taskAttachmentObj[0].Extension != "Link") {
							$scope.FileWizardHeader = "Change file description";
							var htmlStr = '';
							htmlStr += '<tr> ';
							htmlStr += ' <td>Description:</td> ';
							htmlStr += ' <td> ';
							htmlStr += '    <textarea class="large-textarea" id="FileDescriptionVal" ng-model="fileSaveObject.FileDescription" placeholder="File Description" rows="3"></textarea>';
							htmlStr += ' </td> ';
							htmlStr += '</tr> ';
							var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
							$scope.fileSaveObject.FileFriendlyName = trimFileName;
							$scope.fileSaveObject.FileDescription = taskAttachmentObj[0].Description;
							$("#TaskFileControls tbody").append($compile(htmlStr)($scope));
							$("#TaskFileUpdateModal").modal("show");
							$scope.FileChange = true;
							$scope.LinkChange = false;
						} else {
							$scope.FileWizardHeader = "Change link description";
							var htmlStr = '';
							htmlStr += '<tr> ';
							htmlStr += ' <td>Description:</td> ';
							htmlStr += ' <td> ';
							htmlStr += '    <textarea class="large-textarea" type="text" id="linkfriendlyName" ng-model="linkSaveObject.linkDescription"  placeholder="Link Description" rows="1"></textarea>';
							htmlStr += ' </td> ';
							htmlStr += '</tr> ';
							var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
							$scope.linkSaveObject.linkFriendlyName = trimFileName;
							$scope.linkSaveObject.linkDescription = taskAttachmentObj[0].Description;
							$("#TaskFileControls tbody").append($compile(htmlStr)($scope));
							$("#TaskFileUpdateModal").modal("show");
							$scope.FileChange = false;
							$scope.LinkChange = true;
						}
					}
				}
			}
		}
		$scope.SaveLinkObject = function() {
			var TaskStatusData = {};
			TaskStatusData.FileID = $scope.contextFileID;
			TaskStatusData.FileName = $scope.linkSaveObject.linkFriendlyName;
			TaskStatusData.Description = $scope.linkSaveObject.linkDescription;
			TaskStatusData.URL = $scope.TasktxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
			TaskStatusData.LinkType = $scope.linkType;
			TaskNewService.UpdatetaskLinkDescription(TaskStatusData).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
				} else {
					if (TaskStatusResult.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						$("#TaskFileUpdateModal").modal("hide");
					} else {
						var taskListResObj = $.grep($scope.groups, function(e) {
							return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
						});
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == $scope.TaskBriefDetails.taskID;
						});
						if (taskResObj.length > 0) {
							if (taskResObj[0].taskAttachment.length > 0) {
								var taskAttachmentObj = $.grep(taskResObj[0].taskAttachment, function(e) {
									return (e.Id == parseInt($scope.contextFileID));
								});
								if (taskAttachmentObj.length > 0) {
									taskAttachmentObj[0].Name = $scope.linkSaveObject.linkFriendlyName;
									taskAttachmentObj[0].Description = $scope.linkSaveObject.linkDescription;
									$("#TaskFileUpdateModal").modal("hide");
								}
							}
						}
						NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					}
					$timeout(function() {
						feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
					}, 2000);
				}
			});
		}
		$scope.AddNewLinktoTask = function() {
			$scope.EditLink = false;
			$scope.SaveLink = true;
			$scope.HidecfnTitle = "Add link";
			$scope.TasktxtLinkName = "";
			$scope.TasktxtLinkDesc = "";
			$scope.TasktxtLinkURL = "";
			$scope.linkType = 1;
			$("#TaskaddLinkPopup").modal("show");
			$timeout(function() {
				$('#TasktxtLinkName').focus();
			}, 1000);
		}
		$scope.EditAttachLink = function() {
			if ($scope.TasktxtLinkName != "" && $scope.TasktxtLinkURL != "") {
				var TaskStatusData = {};
				TaskStatusData.FileName = $scope.TasktxtLinkName;
				TaskStatusData.Description = "";
				TaskStatusData.URL = $scope.TasktxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
				TaskStatusData.LinkType = $scope.linkType;
				TaskNewService.UpdatetaskLinkDescription(TaskStatusData).then(function(TaskStatusResult) {
					if (TaskStatusResult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					} else {
						if (TaskStatusResult.Response == false) {
							NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
							$("#TaskaddLinkPopup").modal("hide");
						} else {
							var taskListResObj = $.grep($scope.groups, function(e) {
								return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
							});
							var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
								return e.Id == $scope.TaskBriefDetails.taskID;
							});
							if (taskResObj.length > 0) {
								if (taskResObj[0].taskAttachment.length > 0) {
									var taskAttachmentObj = $.grep(taskResObj[0].taskAttachment, function(e) {
										return (e.Id == parseInt($scope.contextFileID));
									});
									if (taskAttachmentObj.length > 0) {
										taskAttachmentObj[0].Name = $scope.TasktxtLinkName;
										taskAttachmentObj[0].Description = $scope.TasktxtLinkDesc;
										taskAttachmentObj[0].LinkURL = $scope.TasktxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
										taskAttachmentObj[0].LinkType = $scope.linkType;
										$scope.EditLink = false;
										$scope.SaveLink = true;
										$scope.HidecfnTitle = "Add link";
										$scope.TasktxtLinkName = "";
										$scope.TasktxtLinkDesc = "";
										$scope.TasktxtLinkURL = "";
										$scope.linkType = 0;
										$('#TasktxtLink').val('');
										$("#TaskaddLinkPopup").modal("hide");
									}
								}
							}
							NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
							$timeout(function() {
								feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
							}, 2000);
						}
					}
				});
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
			}
		}
		$scope.SaveFileObject = function() {
			var TaskStatusData = {};
			TaskStatusData.FileID = $scope.contextFileID;
			TaskStatusData.FileName = $scope.fileSaveObject.FileFriendlyName;
			TaskStatusData.Description = $scope.fileSaveObject.FileDescription;
			TaskNewService.UpdatetaskAttachmentDescription(TaskStatusData).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
				} else {
					if (TaskStatusResult.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						$("#TaskFileUpdateModal").modal("hide");
					} else {
						var taskListResObj = $.grep($scope.groups, function(e) {
							return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
						});
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == $scope.TaskBriefDetails.taskID;
						});
						if (taskResObj.length > 0) {
							if (taskResObj[0].taskAttachment.length > 0) {
								var taskAttachmentObj = $.grep(taskResObj[0].taskAttachment, function(e) {
									return (e.Id == parseInt($scope.contextFileID));
								});
								if (taskAttachmentObj.length > 0) {
									taskAttachmentObj[0].Name = $scope.fileSaveObject.FileFriendlyName;
									taskAttachmentObj[0].Description = $scope.fileSaveObject.FileDescription;
									$("#TaskFileUpdateModal").modal("hide");
								}
							}
						}
						$timeout(function() {
							feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
						}, 2000);
						NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					}
				}
			});
		}
		$scope.DeleteFile = function(e) {
			if ($scope.ContextFileExtention == "Link") {
				bootbox.confirm($translate.instant('LanguageContents.Res_2022.Caption'), function(result) {
					if (result) {
						$timeout(function() {
							TaskNewService.DeleteTaskLinkByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function(deleteattach) {
								if (deleteattach.Response == true) {
									var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function(e) {
										return e.Id == $scope.contextFileID;
									});
									$scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
									$timeout(function() {
										feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
									}, 2000);
									NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
								} else {
									NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
								}
							});
						}, 100);
					}
				});
			} else {
				bootbox.confirm($translate.instant('LanguageContents.Res_2023.Caption'), function(result) {
					if (result) {
						$timeout(function() {
							TaskNewService.DeleteTaskLinkByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function(deleteattach) {
								if (deleteattach.Response == true) {
									var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function(e) {
										return e.Id == $scope.contextFileID;
									});
									$scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
									$timeout(function() {
										feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
									}, 2000);
									NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
								} else {
									NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
								}
							});
						}, 100);
					}
				});
			}
		};
		$scope.deleteAttachedFiles = function(fileObject) {
			bootbox.confirm($translate.instant('LanguageContents.Res_2027.Caption'), function(result) {
				if (result) {
					$timeout(function() {
						if (fileObject.Extension != "link") {
							var attachmentDelete = $.grep($scope.AttachmentFilename, function(e) {
								return e.FileGuid == fileObject.FileGuid
							});
							$scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
							var fileToDelete = $.grep($scope.FileList, function(e) {
								return e.FileGuid == fileObject.FileGuid
							});
							$scope.FileList.splice($.inArray(fileToDelete[0], $scope.FileList), 1);
							if (attachmentDelete[0].ID > 0) {
								TaskNewService.DeleteAttachments(attachmentDelete[0].ID).then(function(data) {
									if (data != null && data.Response != false) {
										NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
										ReloadTaskAttachments($scope.SelectedTaskID);
									}
								});
							}
						} else {
							if (fileObject.ID == 0) {
								var attachmentDelete = $.grep($scope.AttachmentFilename, function(e) {
									return e.LinkID == fileObject.LinkID
								});
								$scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
							} else if (fileObject.ID > 0) {
								var attachmentDelete = $.grep($scope.AttachmentFilename, function(e) {
									return e.ID == fileObject.ID
								});
								$scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
								TaskNewService.DeleteLinkByID(attachmentDelete[0].ID).then(function(data) {
									if (data != null && data.Response != false) {
										NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
										ReloadTaskAttachments($scope.SelectedTaskID);
									}
								});
							}
						}
					}, 100);
				}
			});
		}

		function RereshTaskObj(taskObj) {
			if ($scope.TaskActionFor == 1) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				if (taskListResObj.length > 0) {
					if (taskListResObj[0].TaskList != null) {
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskResObj.length > 0) {
							UpdateTaskObject(taskResObj, taskObj)
						} else {
							taskListResObj[0].TaskList.push(taskObj);
						}
					} else {
						taskListResObj[0].TaskList.push(taskObj);
					}
					$scope.ApplyTaskListCountDetails();
				}
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == taskObj.EntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == taskObj.TaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskObjHolder.length > 0) {
							UpdateTaskObject(taskObjHolder, taskObj)
						} else {
							taskListGroup[0].TaskList.push(taskObj);
						}
					}
					$scope.ApplySubTaskListCountDetails();
				}
			}
		}

		function refreshModel() {
			$scope.TaskBriefDetails = {
				taskID: 0,
				taskListUniqueID: 0,
				taskTypeId: 0,
				taskTypeName: "",
				EntityID: 0,
				ownerId: 0,
				taskName: "",
				dueIn: 0,
				dueDate: "",
				status: "",
				statusID: 0,
				taskOwner: "",
				Description: "",
				Note: "",
				taskmembersList: [],
				taskAttachmentsList: [],
				taskProgressCount: "",
				totalTaskMembers: [],
				TaskMembersRoundTripGroup: [],
				taskCheckList: [],
				WorkTaskInprogressStatus: ""
			};
			$scope.FileList = [];
			$scope.AttachmentFilename = [];
			$scope.TaskType = '';
			$scope.FileIdForAssetCategory = "";
			$scope.AssetCategory = {};
		}
		$scope.ContextTaskListID = 0;
		$scope.contextTaskID = 0;
		$scope.ContextMemberID = 0;
		$scope.DuplicateEntityTaskListWithTasks = function() {
			var DuplicateTaskListData = {};
			DuplicateTaskListData.TaskListID = parseInt($scope.ContextTaskListID);
			DuplicateTaskListData.EntityID = $scope.ContextTaskEntityID;
			TaskNewService.DuplicateEntityTaskList(DuplicateTaskListData).then(function(DuplicateTaskListDataResult) {
				if (DuplicateTaskListDataResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4323.Caption'));
				} else {
					if (DuplicateTaskListDataResult.Response != null) {
						AppendDuplicatedTaskList(DuplicateTaskListDataResult.Response, parseInt($scope.ContextTaskListID), $scope.ContextTaskEntityID);
						NotifySuccess($translate.instant('LanguageContents.Res_4835.Caption'));
					} else {
						NotifyError($translate.instant('LanguageContents.Res_4322.Caption'));
					}
				}
			});
		};

		function AppendDuplicatedTaskList(taskListObj, taskListId, EntityId) {
			var returnObj = taskListObj;
			if (EntityId == $stateParams.ID) {
				var group = {
					EntityParentID: EntityId,
					LibraryName: returnObj.LibraryName,
					LibraryDescription: returnObj.LibraryDescription,
					ID: returnObj.ID,
					SortOrder: returnObj.SortOrder,
					IsGetTasks: false,
					IsExpanded: false,
					TaskList: returnObj.TaskList
				};
				$scope.groups.push(group);
				$timeout(function() {
					$scope.GetParentTaskCount(returnObj.ID, EntityId);
				}, 100);
			} else {
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == EntityId;
				});
				if (taskListResObj.length > 0) {
					taskListResObj[0].TaskListGroup.push(taskListObj);
					$timeout(function() {
						$scope.GetSubTaskCount(returnObj.ID, EntityId);
					}, 100);
				}
			}
		}
		$scope.DuplicateEntityTask = function() {
			var DuplicateTaskData = {};
			DuplicateTaskData.TaskID = parseInt($scope.contextTaskID);
			DuplicateTaskData.EntityID = parseInt($stateParams.ID, 10);
			TaskNewService.DuplicateEntityTask(DuplicateTaskData).then(function(DuplicateTaskDataResult) {
				if (DuplicateTaskDataResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
				} else {
					if (DuplicateTaskDataResult.Response != null && DuplicateTaskDataResult.Response.m_Item1 != 0) {
						var returnObj = DuplicateTaskDataResult.Response.m_Item2;
						NotifySuccess($translate.instant('LanguageContents.Res_4822.Caption'));
						AppendDuplicateTask(returnObj);
					} else {
						NotifyError($translate.instant('LanguageContents.Res_4322.Caption'));
					}
				}
			});
		};

		function AppendDuplicateTask(taskObj) {
			if (taskObj.EntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				taskListResObj[0].TaskList.push(taskObj);
				$timeout(function() {
					$scope.GetParentTaskCount(taskObj.TaskListID, taskObj.EntityID);
				}, 100);
			} else {
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == taskObj.EntityID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				if (taskResObj.length > 0) {
					taskResObj[0].TaskList.push(taskObj);
					$timeout(function() {
						$scope.GetSubTaskCount(taskObj.TaskListID, taskObj.EntityID);
					}, 100);
				}
			}
		}
		$scope.DeleteEntityTaskList = function() {
			var tasklistname = "";
			tasklistname = ReturnDeleteTasklistName();
			bootbox.confirm($translate.instant('LanguageContents.Res_2036.Caption') + " " + tasklistname + " ?", function(result) {
				if (result) {
					$timeout(function() {
						TaskNewService.DeleteTaskEntityListID($scope.ContextTaskListID, parseInt($stateParams.ID, 10)).then(function(delresult) {
							if (delresult.StatusCode == 405) {
								NotifyError($translate.instant('LanguageContents.Res_4310.Caption'));
							} else {
								if (delresult.Response == false) {
									bootbox.alert($translate.instant('LanguageContents.Res_1960.Caption'));
								} else {
									NotifySuccess($translate.instant('LanguageContents.Res_4834.Caption'));
									refreshTaskListObjectAfterDelete();
								}
							}
						});
					}, 100);
				}
			});
		}

		function ReturnDeleteTasklistName() {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				return taskListResObj[0].LibraryName.toString();
			} else {
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				if (taskResObj.length > 0) {
					return taskResObj[0].LibraryName.toString();
				}
			}
		}

		function refreshTaskListObjectAfterDelete() {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				$scope.groups.splice($.inArray(taskListResObj[0], $scope.groups), 1);
			} else {
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				if (taskResObj.length > 0) {
					taskListResObj[0].TaskListGroup.splice($.inArray(taskResObj[0], taskListResObj[0].TaskListGroup), 1);
				}
			}
		}
		$scope.DeleteEntityTask = function() {
			var taskname = "";
			taskname = ReturnDeletableEntitytaskName();
			bootbox.confirm($translate.instant('LanguageContents.Res_2037.Caption') + " " + taskname + " ?", function(result) {
				if (result) {
					$timeout(function() {
						TaskNewService.DeleteEntityTask($scope.contextTaskID, parseInt($stateParams.ID, 10)).then(function(delresult) {
							if (delresult.StatusCode == 405) {
								NotifyError($translate.instant('LanguageContents.Res_4309.Caption'));
							} else {
								if (delresult.Response == -1) {
									bootbox.alert($translate.instant('LanguageContents.Res_4522.Caption'));
								} else if (delresult.Response == 0) {
									bootbox.alert($translate.instant('LanguageContents.Res_1961.Caption'));
								} else {
									RefreshDeletableEntitytask();
									NotifySuccess($translate.instant('LanguageContents.Res_4821.Caption'));
								}
							}
						});
					}, 100);
				}
			});
		}

		function ReturnDeletableEntitytaskName() {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				if (taskListResObj.length > 0) {
					if (taskListResObj[0].TaskList != null) {
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskResObj.length > 0) {
							return taskResObj[0].Name;
						}
					}
				}
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.ContextTaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskObjHolder.length > 0) {
							return taskObjHolder[0].Name;
						}
					}
				}
			}
		}

		function RefreshDeletableEntitytask() {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				if (taskListResObj.length > 0) {
					if (taskListResObj[0].TaskList != null) {
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskResObj.length > 0) {
							taskListResObj[0].TaskList.splice($.inArray(taskResObj[0], taskListResObj[0].TaskList), 1);
						}
					}
				}
				$scope.GetParentTaskCount($scope.ContextTaskListID, $scope.ContextTaskEntityID);
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.ContextTaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskObjHolder.length > 0) {
							taskListGroup[0].TaskList.splice($.inArray(taskObjHolder[0], taskListGroup[0].TaskList), 1);
						}
					}
				}
				$scope.GetSubTaskCount($scope.ContextTaskListID, $scope.ContextTaskEntityID);
			}
		}
		$scope.TaskStatusFilterObj = {
			TaskStatusObj: [{
				ID: 0,
				Name: 'Unassigned'
			}, {
				ID: 1,
				Name: 'In progress'
			}, {
				ID: 2,
				Name: 'Completed'
			}, {
				ID: 4,
				Name: 'Unable to complete'
			}, {
				ID: 3,
				Name: 'Approved'
			}, {
				ID: 5,
				Name: 'Rejected'
			}, {
				ID: 7,
				Name: 'Not applicable'
			}]
		};
		$scope.CurrentFilterStatus = "All";
		$scope.EnableDisableTask = function(TaskStatus, tasklistID, TaskType) {
			if ($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.length == 0) {
				return true;
			} else {
				if (TaskStatus == 8) {
					if (($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.indexOf(2) != -1) && (TaskType == 2 || TaskType == 31)) {
						return true;
					} else if (($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.indexOf(3) != -1) && (TaskType == 3)) {
						return true;
					} else {
						return false;
					}
				} else {
					if ($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.indexOf(TaskStatus) != -1) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
		$scope.FilterByOptionChange = function() {
			if ($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.length == 0 || $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus == undefined) $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus = [];
			else $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			$scope.ApplyTaskListCountDetails();
			$scope.ApplySubTaskListCountDetails();
		}

		function AutoFilterFeature() {
			if ($scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus.length == 0 || $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus == undefined) $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus = [];
			else $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			$scope.ApplyTaskListCountDetails();
			$scope.ApplySubTaskListCountDetails();
		}
		$timeout(function () { AutoFilterFeature(); }, 0);
		$scope.FilterByStatus = function(status) {
			$scope.FilterValue = status;
			$scope.FilterValue = status;
			var Currentitem = $.grep($scope.TaskStatusFilterObj.TaskStatusObj, function(n, i) {
				return $scope.TaskStatusFilterObj.TaskStatusObj[i].ID == status;
			});
			$scope.CurrentFilterStatus = Currentitem[0].Name;
			$scope.ApplyTaskListCountDetails();
			$scope.ApplySubTaskListCountDetails();
		}

		function sortOn(collection, name) {
			collection.sort(function(a, b) {
				if (a[name] <= b[name]) {
					return (-1);
				}
				return (1);
			});
		}
		$scope.groupBy = function(attribute, ISglobal, taskStatus) {
			$scope.groups.splice(0, $scope.groups.length);
			sortOn($scope.TaskLibraryList, attribute);
			for (var i = 0, friend; friend = $scope.TaskLibraryList[i++];) {
				var taskList = friend.TaskList;
				sortOn(taskList, "SortOrder");
				if (ISglobal != "All") {
					var group = {
						EntityParentID: friend.EntityParentID,
						LibraryName: friend.LibraryName,
						LibraryDescription: friend.LibraryDescription,
						ID: friend.ID,
						SortOrder: friend.SortOrder,
						TaskCount: friend.TaskCount,
						IsGetTasks: false,
						IsExpanded: false,
						TaskList: []
					}
					for (var j = 0; j < taskList.length; j++) {
						if (taskList[j]["TaskStatus"] === taskStatus) {
							group.TaskList.push(taskList[j]);
						}
					}
					var taskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
						return e.ID == friend.ID;
					});
					if (taskListResObj.length > 0) {
						group.TaskList = taskListResObj[0].TaskList;
						group.IsGetTasks = true;
						group.IsExpanded = true;
					}
					$scope.groups.push(group);
				} else if (ISglobal == "All") {
					var group = {
						EntityParentID: friend.EntityParentID,
						LibraryName: friend.LibraryName,
						LibraryDescription: friend.LibraryDescription,
						ID: friend.ID,
						SortOrder: friend.SortOrder,
						TaskCount: friend.TaskCount,
						IsGetTasks: false,
						IsExpanded: false,
						TaskList: []
					};
					group.TaskList = taskList;
					var taskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
						return e.ID == friend.ID;
					});
					if (taskListResObj.length > 0) {
						group.TaskList = taskListResObj[0].TaskList;
						group.IsGetTasks = true;
						group.IsExpanded = true;
					}
					$scope.groups.push(group);
				}
			}
			$timeout(function() {
				$(".nestedsorting").each(applySortable);
			});
		};
		$scope.groups.splice(0, $scope.groups.length);
		$scope.DisableIsAdminTask = false;
		$scope.ngShowUnassigned = false;
		$scope.ngShowTaskCreation = true;
		$scope.SelectedUnassignedTaskID = 0;
		$scope.AdminTaskCheckList = [];
		$scope.MainSelectedTaskListId = 0;
		$scope.AssetId = '';
		$scope.OpenPopUpAction = function(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, assetid) {
			getAllTemplateTask();
			$("#workAssetdynamicdetail").html('');
			$("#approvalworktaskdynamicHolder").html('');
			$("#reviewtaskdynamicholder").html('');
			$("#proofHQtaskdynamicholder").html('');
			$("#dalimtaskdynamicholder").html('');
			$("#unassignedtaskdynamicdataholder").html('');
			$scope.AssetId = assetid;
			$scope.MainSelectedTaskListId = TaskObject.TaskListID;
			$scope.TaskGlobalEntityID = parseInt($stateParams.ID);
			$scope.TaskActionFor = 1;
			$scope.DisableIsAdminTask = true;
			$scope.showdatepicker = false;
			PopulateBasicTaskDetails(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, $scope.TaskGlobalEntityID);
			GetEntityLocationPath(taskID);
			GetentityMembers($scope.TaskGlobalEntityID);
			$scope.MinMaxValue = GetTaskMinMaxValue(taskTypeId);
		}
		$scope.populateTaskEditPopup = function(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, assetid) {
		    $scope.ReorderOverviewStructure = false;
		    var globalEntityID = parseInt($scope.TaskGlobalEntityID);
			var modalInstance = $modal.open({
				templateUrl: 'views/mui/task/TaskEdit.html',
				controller: "mui.task.muitaskFlowEditCtrl",
				resolve: {
					items: function() {
						return {
							TaskId: taskID,
							EntityId: globalEntityID,
							taskStatus: taskStatus
						};
					}
				},
				scope: $scope,
				windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
				backdrop: "static"
			});
			modalInstance.result.then(function(selectedItem) {
				$scope.selected = selectedItem;
			}, function() {
				//console.log('Modal dismissed at: ' + new Date());
			});
		}
		if ($stateParams.TaskID !== undefined) {
			$timeout(function() {
				var urlTaskID = $stateParams.TaskID;
				TaskNewService.GetEntityTaskDetails(urlTaskID).then(function(EntityTaskDetails) {
					var taskDetail = EntityTaskDetails.Response;
					$scope.OpenPopUpAction(urlTaskID, taskDetail.EntityTaskListID, taskDetail.TaskType, taskDetail, taskDetail.TaskStatus)
					$(window).AdjustHeightWidth();
				});
			}, 10);
		}
		var cssToRemove = "";
		$scope.fromAssignedpopup = true;

		function BindFundRequestTaskDetails(taskTypeId, TaskObject) {
			$scope.ShowCompleteBtn = false;
			$scope.ShowApproveBtn = false;
			$scope.ShowRejectedBtn = false;
			$scope.ShowWithdrawBtn = false;
			$scope.ShowUnabletoCompleteBtn = false;
			$scope.ShowRevokeButton = false;
			try {
				$("#ReviewTask,#ApprovalTask,#WorkTask,#UnassignedTask,#ProofHqTask,#dalimTask").removeClass(cssToRemove);
				$("#ReviewTask,#ApprovalTask,#WorkTask,#UnassignedTask,#ProofHqTask,#dalimTask").addClass(TaskObject.StatusName.replace(/\s+/g, ""));
				cssToRemove = TaskObject.StatusName.replace(/\s+/g, "");
				$scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
				var taskOwnerObj = $.grep(TaskObject.taskMembers, function(e) {
					return (e.RoleID == 1);
				})[0];
				if (TaskObject.taskAssigness != null) {
					var isThisMemberPresent = $.grep(TaskObject.taskAssigness, function(e) {
						return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1);
					});
					if (taskTypeId == 2) {
						if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
								$scope.ShowWithdrawBtn = false;
							}
						}
						if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
							if ($scope.EntityLockTask != true) {
								if (isThisMemberPresent.length == 1) {
									if (isThisMemberPresent[0].ApprovalStatus == 0) {
										$scope.ShowCompleteBtn = true;
										$scope.ShowUnabletoCompleteBtn = true;
									}
								}
							}
						}
						if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {
							if ($scope.EntityLockTask != true) {
								if (isThisMemberPresent.length == 1) {
									if (isThisMemberPresent[0].ApprovalStatus == 0) {
										$scope.ShowCompleteBtn = true;
										$scope.ShowUnabletoCompleteBtn = true;
									}
								}
							}
						}
						if (TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 8) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
									if ($scope.EntityLockTask != true) {
										$scope.ShowCompleteBtn = false;
										$scope.ShowApproveBtn = false;
										$scope.ShowRejectedBtn = false;
										$scope.ShowWithdrawBtn = false;
										$scope.ShowUnabletoCompleteBtn = false;
										$scope.ShowRevokeButton = true;
									}
								} else {
									$scope.ShowCompleteBtn = false;
									$scope.ShowApproveBtn = false;
									$scope.ShowRejectedBtn = false;
									$scope.ShowWithdrawBtn = false;
									$scope.ShowUnabletoCompleteBtn = false;
									$scope.ShowRevokeButton = false;
								}
							} else {
								$scope.ShowCompleteBtn = false;
								$scope.ShowApproveBtn = false;
								$scope.ShowRejectedBtn = false;
								$scope.ShowWithdrawBtn = false;
								$scope.ShowUnabletoCompleteBtn = false;
								$scope.ShowRevokeButton = false;
							}
						}
						if (TaskObject.TaskStatus == 8) {
							if (isThisMemberPresent != null) if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
									if ($scope.EntityLockTask != true) {
										$scope.ShowCompleteBtn = false;
										$scope.ShowApproveBtn = false;
										$scope.ShowRejectedBtn = false;
										$scope.ShowWithdrawBtn = false;
										$scope.ShowUnabletoCompleteBtn = false;
										$scope.ShowRevokeButton = true;
									}
								} else {
									$scope.ShowCompleteBtn = false;
									$scope.ShowApproveBtn = false;
									$scope.ShowRejectedBtn = false;
									$scope.ShowWithdrawBtn = false;
									$scope.ShowUnabletoCompleteBtn = false;
									$scope.ShowRevokeButton = false;
								}
							} else {
								$scope.ShowCompleteBtn = false;
								$scope.ShowApproveBtn = false;
								$scope.ShowRejectedBtn = false;
								$scope.ShowWithdrawBtn = false;
								$scope.ShowUnabletoCompleteBtn = false;
								$scope.ShowRevokeButton = false;
							}
						}
					} else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36 || taskTypeId == 37) {
						if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
								$scope.ShowWithdrawBtn = false;
							}
						}
						if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
							if ($scope.EntityLockTask != true) {
								if (isThisMemberPresent.length == 1) {
									if (isThisMemberPresent[0].ApprovalStatus == 0) {
										$scope.ShowApproveBtn = true;
										$scope.ShowRejectedBtn = true;
										$scope.ShowUnabletoCompleteBtn = true;
									}
								}
							}
						}
						if (TaskObject.TaskStatus == 8) {
							$scope.ShowCompleteBtn = false;
							$scope.ShowApproveBtn = false;
							$scope.ShowRejectedBtn = false;
							$scope.ShowWithdrawBtn = false;
							$scope.ShowUnabletoCompleteBtn = false;
							$scope.ShowRevokeButton = false;
						}
						if (TaskObject.TaskStatus == 5 && isThisMemberPresent.length >= 1) {
							if ($scope.EntityLockTask != true) {
								if (isThisMemberPresent.length == 1) {
									if (isThisMemberPresent[0].ApprovalStatus == 5) {
										$scope.ShowApproveBtn = false;
										$scope.ShowRejectedBtn = false;
										$scope.ShowUnabletoCompleteBtn = false;
										$scope.ShowRevokeButton = true;
									}
								}
							}
						}
					} else if (taskTypeId == 31) {
						if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
								$scope.ShowWithdrawBtn = false;
							}
						}
						if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
							if ($scope.EntityLockTask != true) {
								if (isThisMemberPresent.length == 1) {
									if (isThisMemberPresent[0].ApprovalStatus == 0) {
										$scope.ShowApproveBtn = false;
										$scope.ShowCompleteBtn = true;
										$scope.ShowRejectedBtn = false;
										$scope.ShowUnabletoCompleteBtn = true;
										$scope.ShowRevokeButton = false;
									}
								}
							}
						}
						if (TaskObject.TaskStatus == 8) {
							$scope.ShowCompleteBtn = false;
							$scope.ShowApproveBtn = false;
							$scope.ShowRejectedBtn = false;
							$scope.ShowWithdrawBtn = false;
							$scope.ShowUnabletoCompleteBtn = false;
							$scope.ShowRevokeButton = false;
						}
					}
				}
				$scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
				$scope.TaskBriefDetails.taskName = TaskObject.Name;
				$scope.TaskBriefDetails.taskTypeId = taskTypeId;
				$scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
				$scope.TaskBriefDetails.dueDate = '-';
				if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
					$scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat);
				}
				$scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
				$scope.TaskBriefDetails.taskOwner = taskOwnerObj.UserName;
				$scope.TaskBriefDetails.status = TaskObject.StatusName;
				$scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
				$scope.TaskBriefDetails.Description = '-';
				$scope.TaskBriefDetails.Note = '-';
				if (TaskObject.Description.length > 0) {
					$scope.TaskBriefDetails.Description = TaskObject.Description;
				}
				if (TaskObject.Note != null) if (TaskObject.Note.length > 0) {
					$scope.TaskBriefDetails.Note = TaskObject.Note;
				}
				$scope.TaskBriefDetails.taskID = TaskObject.Id;
				$scope.TaskBriefDetails.ownerId = taskOwnerObj.UserID;
				$scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
				$scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
				$scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
				$scope.TaskBriefDetails.taskProgressCount = "";
				if (TaskObject.TaskType == 2 && TaskObject.TaskStatus == 0) {
					$scope.TaskBriefDetails.taskProgressCount = TaskObject.ProgressCount;
				}
				if (TaskObject.taskAssigness != null) {
					var unresponsedMembers = $.grep(TaskObject.taskAssigness, function(e) {
						return (e.ApprovalStatus != 0);
					});
					if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.TaskType != 2) {
						$scope.TaskBriefDetails.taskProgressCount = "";
					} else {
						if (unresponsedMembers != null) $scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";
					}
				}
				$scope.TaskMemberList = TaskObject.taskAssigness;
				$scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
				$scope.name = $scope.TaskBriefDetails.taskName;
				$scope.DescriptionChange = $scope.TaskBriefDetails.Description;
				if (TaskObject.strDate != "") {
					$scope.dueDate = TaskObject.strDate;
					$scope.TaskBriefDetails.strDueDate = (TaskObject.strDate);
				} else {
					$scope.dueDate = "-";
					$scope.TaskBriefDetails.strDueDate = "-";
				}
				if (TaskObject.TaskStatus != 8) if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36 || taskTypeId == 37) $scope.groupByTaskRoundTrip('ApprovalRount');
				SeperateUsers();
				$scope.fromAssignedpopup = true;
				GetEntityAttributesDetails($scope.TaskBriefDetails.taskID);
				GetTaskCheckLists();
				ReloadTaskAttachments(TaskObject.Id);
				$scope.processingsrcobj.processingid = $scope.TaskBriefDetails.taskID;
				$scope.$broadcast('ReloadAssetView');
			} catch (e) {}
		}
		$scope.PopupClassApply = function() {
			return $scope.TaskBriefDetails.taskpoupClass;
		}
		$scope.SaveUnassignedTaskChanges = function() {
			$scope.UpdateTaskStatus($scope.TaskBriefDetails.taskID, 1);
		}
		$scope.SendReminderNotification = function() {
			TaskNewService.SendReminderNotification($scope.ContextMemberID, $scope.TaskBriefDetails.taskID).then(function(getstatus) {
				var sentstatus = getstatus.Response;
			});
		}
		$scope.DeletableAssigneeName = "";
		$scope.SetDeletebleAssigneName = function(Name) {
			$scope.DeletableAssigneeName = Name;
		};
		$scope.DeleteTaskMembers = function() {
			if ($scope.TaskBriefDetails.status == "Completed") {
			    NotifyError($translate.instant('LanguageContents.Res_5828.Caption'));
			} else {
				bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + " " + $scope.DeletableAssigneeName + " ?", function(result) {
					if (result) {
						$timeout(function() {
							TaskNewService.DeleteTaskMemberById($scope.ContextMemberID, $scope.TaskBriefDetails.taskID).then(function(delresult) {
								if (delresult.StatusCode == 405) {
									NotifyError($translate.instant('LanguageContents.Res_4308.Caption'));
								} else {
									if (delresult.Response == false) {
										bootbox.alert($translate.instant('LanguageContents.Res_1962.Caption'));
									} else {
										$timeout(function() {
											feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
										}, 2000);
										NotifySuccess($translate.instant('LanguageContents.Res_4480.Caption'));
										$scope.DeletableAssigneeName = "";
										RefreshCurrentTask();
									}
								}
							});
						}, 100);
					}
				});
			}
		}
		$scope.FlagColorOption = function (taskObj) {
		    if (taskObj != undefined) {
		        if (taskObj.taskAssigness != undefined || taskObj.taskAssigness != null) {
		            if (taskObj.taskMembers.length > 0) {
		                var ColorObj = $.grep(taskObj.taskMembers, function (e) {
		                    return e.UserID == parseInt($cookies['UserId'], 10);
		                });
		                if (ColorObj.length > 0) return ColorObj[0].FlagColorCode;
		                else return 'F2F8FB';
		            }
		        } else return 'F2F8FB';
		    }
		}
		$scope.TaskBriefDetailDateFormat = function(dateObj) {
			return dateFormat(dateObj, $scope.DefaultSettings.DateFormat);
		}
		$scope.StatusDivClasses = function(statusCode, overdue, IsAdminTask, taskType) {
			var baseClass = 'ListItem ';
			if (statusCode == 0 && overdue >= 0 && IsAdminTask == 0) return baseClass + 'Unassigned';
			if (statusCode == 0 && overdue >= 0 && IsAdminTask >= 0) return baseClass + 'Unassigned';
			if (statusCode == 0 && overdue < 0 && IsAdminTask > 0) return baseClass + 'Unassigned OverDue';
			if (statusCode == 0 && overdue == 0 && IsAdminTask > 0) return baseClass + 'Unassigned';
			if (statusCode == 0 && overdue < 0 && IsAdminTask >= 0) return baseClass + 'Unassigned OverDue';
			if (statusCode == 1 && overdue < 0 && IsAdminTask >= 0) return baseClass + 'InProgress OverDue';
			if (statusCode == 1) return baseClass + 'InProgress';
			if (statusCode == 2) return baseClass + 'Completed';
			if (statusCode == 3) return baseClass + 'Approved';
			if (statusCode == 4) return baseClass + 'UnableToComplete';
			if (statusCode == 5) return baseClass + 'Rejected';
			if (statusCode == 6) return baseClass + 'Rejected';
			if (statusCode == 7) return baseClass + 'NotApplicable';
			if (statusCode == 8) {
				var classname = (taskType == 3 ? "Approved" : 'Completed');
				return baseClass + classname.toString();
			}
		}
		$scope.DueDateFormat = function(duedate) {
			if (duedate == "" || duedate == null) return '';
			else return dateFormat(duedate, $scope.DefaultSettings.DateFormat);
		}
		$scope.flag = function(newColorCode) {
			var taskListResObj = $.grep($scope.groups, function(e) {
				return e.ID == $scope.ContextTaskListID;
			});
			var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
				return e.Id == $scope.contextTaskID;
			});
			if (taskResObj[0].taskMembers != null) {
				if (taskResObj[0].taskAssigness.length > 0) {
					var taskAssigneesResObj = $.grep(taskResObj[0].taskMembers, function(e) {
						return e.UserID == parseInt($cookies['UserId'], 10);
					});
					if (taskAssigneesResObj.length > 0) {
						var ColorCode = newColorCode;
						var values = {};
						values.TaskID = $scope.contextTaskID;
						values.ColorCode = ColorCode;
						TaskNewService.updateTaskMemberFlag(values).then(function(TaskFlagStatusResult) {
							if (TaskFlagStatusResult.StatusCode == 405) {
								NotifyError($translate.instant('LanguageContents.Res_4739.Caption'));
							} else {
								taskAssigneesResObj[0].FlagColorCode = newColorCode;
								NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
							}
						});
					} else {
						bootbox.alert($translate.instant('LanguageContents.Res_1963.Caption'));
					}
				}
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_1964.Caption'));
			}
		};
		$scope.addAditionalLinks = function() {
			$scope.IsNotVersioning = true;
			$scope.globalAttachment = false;
			$scope.fnTimeOut();
		}
		$scope.IsNotVersioning = true;
		$scope.AttachmentFilename = [];
		$scope.globalAttachment = true;
		$scope.listPredfWorkflowFilesAttch = [];
		$scope.addAditionalattachments = function() {
			$scope.IsNotVersioning = true;
			$scope.globalAttachment = false;
			$scope.fnTimeOut();
		}
		$scope.AddAttachments = function() {
			if ($scope.SelectedTaskID > 0) $scope.globalAttachment = false;
			else $scope.globalAttachment = true;
			$scope.fnTimeOut();
		}
		$scope.fnTimeOut = function() {
			$('#TaskfilelistWorkFlow').empty();
			$('#TaskdragfilesAttachment').show();
			$("#totalProgress").empty();
			$("#totalProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>');
			$scope.StrartUpload();
		}
		$scope.StrartUpload = function() {
			var uploader = new plupload.Uploader({
				runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
				browse_button: 'pickfilesAlternate',
				drop_element: 'TaskdragfilesAttachment',
				container: 'filescontainer',
				max_file_size: '10000mb',
				url: 'Handlers/UploadHandler.ashx?Type=Attachment',
				flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
				silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
				chunk_size: '64Kb',
				multipart_params: {}
			});
			uploader.bind('Init', function(up, params) {
				uploader.splice();
			});
			$('#uploadfiles').click(function(e) {
				uploader.start();
				e.preventDefault();
			});
			$('#clearUploader').click(function(e) {
				uploader.destroy();
				$('.moxie-shim').remove();
			});
			uploader.init();
			$('#pickfiles').each(function() {
				var input = new mOxie.FileInput({
					browse_button: this,
					multiple: $scope.IsNotVersioning
				});
				input.onchange = function(event) {
					uploader.addFile(input.files);
				};
				input.init();
			});
			uploader.bind('FilesAdded', function(up, files) {
				if (!$scope.IsNotVersioning) {
					if (files.length > 1) {
						uploader.splice(1, files.length - 1);
						bootbox.alert($translate.instant('LanguageContents.Res_4146.Caption'));
						return false;
					}
				}
				$.each(files, function(i, file) {
					var ste = file.name.split('.')[file.name.split('.').length - 1];
					var stes = [];
					stes = [{
						ID: file.id,
						Extension: ste
					}];
					if ($scope.listPredfWorkflowFilesAttch == undefined) $scope.listPredfWorkflowFilesAttch = [];
					$scope.listPredfWorkflowFilesAttch.push({
						ID: file.id,
						Name: file.name,
						Createdon: new Date.create()
					});
					$('#TaskfilelistWorkFlow').append('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="info">' + '<span class="name" >' + '<i class="icon-file-alt"></i>' + file.name + '</span>' + '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' + '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + '</span>' + '</div>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '</div>' + '</div>' + '<div class="row-fluid">' + '<div class="span12">' + '<form id="Form_' + file.id + '" class="form-inline">' + '<select>' + '<option>' + Mimer(ste) + '</option>' + '<option>Video</option>' + '<option>Document</option>' + '<option>Zip</option>' + '</select>' + '<input type="text" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Description">' + '</form>' + '</div>' + '</div>' + '</div>');
				});
				$('#TaskdragfilesAttachment').hide();
				up.refresh();
				TotalUploadProgress();
			});
			$('#TaskfilelistWorkFlow').on('click', '.removefile', function() {
				var fileToRemove = $(this).attr('data-fileid');
				$.each(uploader.files, function(i, file) {
					if (file.id == fileToRemove) {
						uploader.removeFile(file);
						$('#' + file.id).remove();
					}
				});
				if ($('#TaskfilelistWorkFlow .attachmentBox').length == 0) {
					$('#TaskdragfilesAttachment').show();
				}
				TotalUploadProgress();
			});
			uploader.bind('UploadProgress', function(up, file) {
				$('#' + file.id + " .bar").css("width", file.percent + "%");
				$('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
				$('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
				TotalUploadProgress();
			});
			uploader.bind('Error', function(up, err) {
				$('#TaskfilelistWorkFlow').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
				up.refresh();
			});
			uploader.bind('FileUploaded', function(up, file, response) {
				$('#' + file.id).attr('data-status', 'true');
				var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
				SaveFileDetails(file, response.response, fileDescription);
				$scope.globalAttachment = true;
				TotalUploadProgress();
			});
			uploader.bind('BeforeUpload', function(up, file) {
				$.extend(up.settings.multipart_params, {
					id: file.id,
					size: file.size
				});
			});
			uploader.bind('FileUploaded', function(up, file, response) {
				var obj = response;
				$('#' + file.id).attr('data-fileId', response.response);
			});

			function TotalUploadProgress() {
				var TotalSize = 0;
				var TotalCount = $('#TaskfilelistWorkFlow .attachmentBox').length;
				var UploadedSize = 0;
				var UploadedCount = 0;
				$('#TaskfilelistWorkFlow .attachmentBox').each(function() {
					TotalSize += parseInt($(this).attr('data-size'));
					UploadedSize += parseInt($(this).attr('data-size-uploaded'));
					if ($(this).attr('data-status') == 'true') {
						UploadedCount += 1;
					}
				});
				$('#totalProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded');
				$('#totalProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
				$('#totalProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
			}
		}
		$scope.FileList = [];
		$scope.UniqueLinkID = 0;
		$scope.InsertableFileList = [];
		$scope.InsertableFileListforVersion = [];
		$scope.InsertableAttachmentFilename = [];
		$scope.InsertableAttachmentFilenameforVersion = [];

		function ReloadTaskVersionDetails(TaskID) {
			if ($scope.TaskVersionFileList.length > 0) {
				$scope.TaskVersionFileList = [];
				$scope.ViewAllFiles();
			}
		}
		$scope.SaveActiveVersion = function() {
			if ($scope.versionFileId != 0) {
				var savetask = {};
				savetask.TaskID = $scope.SelectedTaskID;
				savetask.SelectedVersion = $scope.SelectedVersionID;
				savetask.VersioningFileId = $scope.versionFileId;
				TaskNewService.UpdateAttachmentVersionNo(savetask).then(function(SaveResult) {
					ReloadTaskAttachments($scope.SelectedTaskID);
					$("#AttachmentVersion").modal('hide');
				});
			}
		}

		function ReloadTaskAttachments(taskID) {
			TaskNewService.GetEntityTaskAttachmentFile(taskID).then(function(TaskDetailList) {
				$scope.attachedFiles = TaskDetailList.Response;
				$scope.TaskBriefDetails.taskAttachmentsList = TaskDetailList.Response;
				if ($scope.TaskActionFor == 1) {
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
					});
					var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
						return e.Id == $scope.TaskBriefDetails.taskID;
					});
					if (taskResObj.length > 0) {
						taskResObj[0].taskAttachment = TaskDetailList.Response;
					}
				} else {
					var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == $scope.TaskBriefDetails.EntityID;
					});
					var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
					});
					if (taskResObj.length > 0) {
						var taskObj = $.grep(taskResObj[0].TaskList, function(e) {
							return e.Id == $scope.TaskBriefDetails.taskID;
						});
						if (taskObj.length > 0) taskObj[0].taskAttachment = TaskDetailList.Response;
					}
				}
			});
		}
		$scope.OpenAttachLink = function() {
			$scope.TasktxtLinkName = "";
			$scope.TasktxtLinkURL = "";
			$scope.linkType = 1;
			$("#TaskaddLinkPopup").modal("show");
		}

		function validateURL(textval) {
			var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
			return urlregex.test(textval);
		}
		$scope.AttachLink = function() {
			if ($scope.TasktxtLinkName != "" && $scope.TasktxtLinkURL != "") {
				if ($scope.SelectedTaskID > 0) {
					var AttachLink = {};
					AttachLink.EntityID = $scope.TaskBriefDetails.taskID;
					AttachLink.Name = $scope.TasktxtLinkName;
					AttachLink.URL = $scope.TasktxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
					AttachLink.LinkType = $scope.linkType;
					AttachLink.Description = "";
					AttachLink.ActiveVersionNo = 1;
					AttachLink.TypeID = 9;
					AttachLink.CreatedOn = new Date.create();
					AttachLink.OwnerID = $cookies['UserId'];
					AttachLink.OwnerName = $cookies['Username'];
					AttachLink.ModuleID = 1;
					TaskNewService.InsertLink(AttachLink).then(function(AttachResponse) {
						if (AttachResponse.Response != 0) {
							NotifySuccess($translate.instant('LanguageContents.Res_4788.Caption'));
							$scope.TasktxtLinkName = "";
							$scope.TasktxtLinkURL = "";
							$scope.TasktxtLinkDesc = "";
							$scope.linkType = 1;
							$scope.EditLink = false;
							$scope.SaveLink = true;
							$scope.HidecfnTitle = "Add link";
							$('#TasktxtLink').val('');
							$("#TaskaddLinkPopup").modal("hide");
							ReloadTaskAttachments($scope.SelectedTaskID);
						}
					});
				} else {
					var linkName = ($scope.TasktxtLinkName != undefined || $scope.TasktxtLinkName != null) ? $scope.TasktxtLinkName : "";
					var url = ($scope.TasktxtLinkURL != undefined || $scope.TasktxtLinkURL != null) ? $scope.TasktxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '') : "";
					var linkdesc = ($scope.TasktxtLinkDesc != undefined || $scope.TasktxtLinkDesc != null) ? $scope.TasktxtLinkDesc : "";
					$scope.AttachmentFilename.push({
						"FileName": $scope.TasktxtLinkName,
						"Extension": "Link",
						"Size": 0,
						"FileGuid": 0,
						"FileDescription": "",
						"LinkType": $scope.linkType,
						"URL": url,
						"ID": 0,
						"LinkID": ($scope.UniqueLinkID + 1)
					});
					$scope.TasktxtLinkName = "";
					$scope.TasktxtLinkURL = "";
					$scope.TasktxtLinkDesc = "";
					$scope.linkType = 1;
					$scope.EditLink = false;
					$scope.SaveLink = true;
					$scope.HidecfnTitle = "Add link";
					$('#TasktxtLink').val('');
					$("#TaskaddLinkPopup").modal("hide");
				}
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
			}
		}

		function ReloadUnassignedTaskAttachments(taskID) {
			$scope.AttachmentFilename = [];
			TaskNewService.GetEntityTaskAttachmentFile(taskID).then(function(TaskDetailList) {
				$scope.attachedFiles = TaskDetailList.Response;
				for (var i = 0; i <= $scope.attachedFiles.length - 1; i++) {
					$scope.AttachmentFilename.push({
						"FileName": $scope.attachedFiles[i].Name,
						"Extension": $scope.attachedFiles[i].Extension,
						"Size": parseSize($scope.attachedFiles[i].Size),
						"FileGuid": $scope.attachedFiles[i].Fileguid,
						"FileDescription": $scope.attachedFiles[i].Description,
						"URL": "",
						"LinkType": "",
						"ID": $scope.attachedFiles[i].Id,
						"LinkID": 0
					});
				}
			});
		}

		function parseSize(size) {
			var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
			while (size >= 1024) {
				size = size / 1024;
				tier++;
			}
			return Math.round(size * 10) / 10 + " " + suffix[tier];
		}
		$scope.EntityMemberList = [];
		$scope.EntityDistinctMembers = [];
		TaskNewService.GetMember($scope.TaskGlobalEntityID).then(function(EntityMemberList) {
			$scope.EntityMemberList = EntityMemberList.Response;
			$scope.EntityMemberObj = {
				EntityMemberList: $scope.EntityMemberList
			};
			$scope.EntityDistinctMembers = [];
			var dupes = {};
			if ($scope.EntityMemberList != null) {
				for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
					if (el.IsInherited != true) {
						if (!dupes[el.Userid]) {
							dupes[el.Userid] = true;
							$scope.EntityDistinctMembers.push(el);
							$scope.tagAllOptions.data.push({
								"id": el.Userid,
								"text": el.UserName,
								"UserName": el.UserName,
								"UserEmail": el.UserEmail,
							});
						}
					}
				}
			}
		});
		$scope.SetContextTaskListID = function(id) {
			$scope.ContextTaskListID = id;
		};
		$scope.SetContextTaskListEntityID = function(id) {
			if (id != 0) $scope.ContextTaskEntityID = id;
			else $scope.ContextTaskEntityID = $stateParams.ID;
		};
		$scope.SetContextMemberID = function(id) {
			$scope.ContextMemberID = id;
		};
		$scope.SetContextTaskID = function(id) {
			$scope.contextTaskID = id;
		};
		$scope.contextFileID = 0;
		$scope.SetContextFileID = function(fileid) {
			$scope.contextFileID = fileid;
		};
		$scope.GetTaskByID = function GetTaskByID() {
			var taskObj = [];
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				taskObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				})[0];
			} else {
				var EntityTaskObj = [];
				EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.ContextTaskListID;
					});
					if (taskObjHolder.length > 0) {
						taskObj = taskObjHolder[0];
					}
				}
			}
			$scope.TaskListID = $scope.ContextTaskListID;
			$scope.Caption = taskObj.LibraryName;
			$scope.Description = taskObj.LibraryDescription;
			$scope.EnableTaskListUpdate = true;
			$scope.EnableTaskListAdd = false;
			$("#TaskCreationModal").modal("show");
		};
		$scope.AllowTaskListSorting = function() {
			if ($scope.TaskOrderListObj.TaskSortingStatusID == 1) {
				$scope.TaskOrderListObj.TaskSortingStatus = $translate.instant('LanguageContents.Res_4975.Caption');
				$scope.TaskOrderListObj.TaskSortingStatusID = 2;
				$scope.TaskOrderListObj.TaskOrderHandle = true;
			} else {
				var TaskListSortOrderData = {};
				TaskListSortOrderData.TaskListObj = $scope.TaskOrderListObj.SortOrderObj;
				TaskNewService.UpdateEntityTaskListSortOrder(TaskListSortOrderData).then(function(TaskListSortOrderResult) {
					if (TaskListSortOrderResult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4374.Caption'));
					} else {
						$scope.TaskOrderListObj.TaskSortingStatus = $translate.instant('LanguageContents.Res_1555.Caption');
						$scope.TaskOrderListObj.TaskSortingStatusID = 1;
						$scope.TaskOrderListObj.TaskOrderHandle = false;
						NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					}
				});
			}
		}
		$scope.SourceTaskId = 0;
		$scope.TargetTaskListID = 0;
		$scope.TargetEntityID = 0;
		$scope.SourceList = {};
		$scope.targetList = {};

		function applySortable() {
			$(this).sortable({
				handle: "[data-handle='true']",
				cursor: "move",
				axis: 'y',
				opacity: 0.7,
				helper: 'clone',
				tolerance: 'pointer',
				connectWith: '.nestedsorting',
				dropOnEmpty: true,
				update: function(event, ui) {
					if (this === ui.item.parent()[0]) {
						if ($(this).attr('id') == "ListHolderID") {
							var uiArray = $(this).sortable('toArray');
							$scope.TaskOrderListObj.SortOrderObj = [];
							for (var i = 0; i < $scope.groups.length; i++) {
								$scope.groups[i].SortOrder = uiArray.indexOf($scope.groups[i].ID.toString()) + 1;
								$scope.TaskOrderListObj.SortOrderObj.push({
									TaskListID: parseInt($scope.groups[i].ID, 10),
									SortOrderID: $scope.groups[i].SortOrder
								});
							}
						} else if ($(this).attr('data-role') == "TaskListHolder") {
							var uiArray = $(this).sortable('toArray');
							$scope.TaskOrderListObj.UniqueSortOrderObj = [];
							for (var i = 0; i < $scope.groups.length; i++) {
								for (var j = 0; j < $scope.groups[i].TaskList.length; j++) {
									var sortorderval = uiArray.indexOf($scope.groups[i].TaskList[j].Id.toString()) + 1;
									$scope.TaskOrderListObj.UniqueSortOrderObj.push({
										TaskListID: parseInt($scope.groups[i].ID, 10),
										TaskID: $scope.groups[i].TaskList[j].Id,
										SortOrderID: sortorderval
									});
								}
							}
							CallReorderTaskNewService(ui.item.attr("data-order"), ui.item.attr("data-tasklistid"), ui.item.attr("data-entityid"), ui.item.attr("data-taskid"), uiArray.indexOf(ui.item.attr("data-taskid")));
						} else if ($(this).parent('ul').attr('data-role') == "SubTaskListHolder") {}
					}
				},
				receive: function(event, ui) {
					$scope.SourceList = {
						taskid: ui.item.attr("data-taskid"),
						tasklistid: ui.item.attr("data-tasklistid"),
						tasktype: ui.item.attr("data-tasktype"),
						entityid: ui.item.attr("data-entityid"),
						Isunassigned: ui.item.attr("data-Isunassigned"),
						IsParentEntity: ui.item.attr("data-IsParentEntity")
					};
					$scope.targetList = {
						taskid: ui.item.attr("data-taskid"),
						tasklistid: $(this).parents("li.TaskEntityList").attr("data-tasklistid"),
						tasktype: ui.item.attr("data-tasktype"),
						entityid: $(this).parents("li.TaskEntityList").attr("data-entityid"),
						Isunassigned: ui.item.attr("data-Isunassigned"),
						IsParentEntity: $(this).parents("li.TaskEntityList").attr("data-IsParentEntity"),
					};
					ui.item.attr("data-IsAdded", true);
					if ($(this).parents("li.TaskEntityList").length == 0) {
						$('.nestedsorting').sortable('cancel');
					} else if (ui.item.attr("data-entityid") != $(this).parents("li.TaskEntityList").attr("data-entityid")) {
						$scope.SourceTaskId = 0;
						$scope.TargetTaskListID = 0;
						$scope.TargetEntityID = 0;
						$scope.SourceTaskId = $scope.SourceList.taskid;
						$scope.TargetTaskListID = $scope.targetList.tasklistid;
						$scope.TargetEntityID = $scope.targetList.entityid;
						if ($scope.SourceList.Isunassigned > 0) {
							DragAndDropPopUpShow($scope.SourceList, $scope.targetList);
							GetDestinationEntityRoles($scope.targetList.entityid);
						} else {
							var collection = {
								Id: $scope.SourceList.taskid,
								tasklistid: $scope.targetList.tasklistid,
								entityid: $scope.targetList.entityid
							}
							SourceToDestinationUpDateUnassignestask(collection);
						}
					} else if (ui.sender.attr("data-tasklistid") != $(this).parents("li.TaskEntityList").attr("data-tasklistid")) {
						var updatetaskParam = {};
						updatetaskParam.TaskID = ui.item.attr("data-taskid");
						updatetaskParam.SrcTaskListID = ui.item.attr("data-tasklistid");
						updatetaskParam.TargetTaskListID = $(this).parents("li.TaskEntityList").attr("data-tasklistid");
						TaskNewService.UpdateDragEntityTaskListByTask(updatetaskParam).then(function(updResult) {
							if (updResult.Response != false) {
								$("li[data-IsAdded=true]").remove();
								$scope.RefreshEntityTaskSortorder();
								//NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
								$("#PopUpMembersToMoveTask").modal("hide");
							}
						});
					}
				}
			});
			$(this).find(".nestedsorting").each(applySortable);
		}

		function SourceToDestinationUpDateUnassignestask(collection) {
			var updatetaskParam = {};
			updatetaskParam.Parameter = collection;
			TaskNewService.UpdateEntityTask(updatetaskParam).then(function(updResult) {
				if (updResult.Response != false) {
					$("li[data-IsAdded=true]").remove();
					$scope.RefreshEntityTaskSortorder();
					NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					$("#PopUpMembersToMoveTask").modal("hide");
				}
			});
		}
		$scope.SourceCommonMembers = [{}];
		$scope.SourceExtraMembers = [{}];

		function DragAndDropPopUpShow(SourceList, targetList) {
			TaskNewService.GetSourceToDestinationmembers(SourceList.taskid, SourceList.entityid, targetList.entityid).then(function(EntityMemberList) {
				if (EntityMemberList.Response != null) {
					if (EntityMemberList.Response.m_Item3 == false) {
						$("#PopUpMembersToMoveTask").modal("show");
						$scope.SourceCommonMembers = EntityMemberList.Response.m_Item1;
						$scope.SourceExtraMembers = EntityMemberList.Response.m_Item2;
					} else {
						$('.nestedsorting').sortable('cancel');
						bootbox.alert($translate.instant('LanguageContents.Res_4901.Caption'));
					}
				}
			});
		}
		$scope.CancelDragDropPopup = function() {
			$('.nestedsorting').sortable('cancel');
		}
		$scope.DestinationMemberRoles = {};

		function GetDestinationEntityRoles(entityid) {
			TaskNewService.GetDestinationEntityIdRoleAccess(entityid).then(function(EntityMemberList) {
				if (EntityMemberList.Response != null) {
					$scope.DestinationMemberRoles = EntityMemberList.Response;
				}
			});
		}
		$scope.SaveDragandDropMember = function() {
			$("#SourceMemberTable i.checkbox").each(function() {
				if ($(this).hasClass("checked")) {
					$scope.SourceCommonMembers[$(this).attr("data-index")].Status = true;
				} else {
					$scope.SourceCommonMembers[$(this).attr("data-index")].Status = false;
				}
			});
			$("#TargetMemberTable i.checkbox").each(function() {
				if ($(this).hasClass("checked")) {
					$scope.SourceExtraMembers[$(this).attr("data-index")].Status = true;
				} else {
					$scope.SourceExtraMembers[$(this).attr("data-index")].Status = false;
				}
			});
			var DragMemberparams = {};
			DragMemberparams.Matchedmembers = $scope.SourceCommonMembers;
			DragMemberparams.UnMatchedmembers = $scope.SourceExtraMembers;
			DragMemberparams.SourceTaskId = $scope.SourceTaskId;
			DragMemberparams.TargetTaskListID = $scope.TargetTaskListID;
			DragMemberparams.TargetEntityID = $scope.TargetEntityID;
			TaskNewService.InsertUpdateDragTaskMembers(DragMemberparams).then(function(DragTaskMemberRst) {
				if (DragTaskMemberRst.Response != false) {
					$("li[data-IsAdded=true]").remove();
					$scope.RefreshEntityTaskSortorder();
					NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					$("#PopUpMembersToMoveTask").modal("hide");
				}
			});
		}

		function SortableInsertUpdatetParentTask(taskListID, entityID, taskId) {
			if ($scope.TaskExpandedDetails != null) {
				var taskObjListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
					return e.ID == taskListID && e.EntityID == entityID;
				});
				if (taskObjListResObj.length > 0) {
					for (var j = 0; j < taskObjListResObj[0].TaskList.length; j++) {
						if (taskObjListResObj[0].TaskList[j] != undefined && taskObjListResObj[0].TaskList[j].Id == taskId) {
							taskObjListResObj[0].TaskList.splice(j, 1);
							break;
						}
					}
				}
			}
		}

		function SortableSubTaskInsertUpdatetParentTask(taskListID, entityID, taskId) {
			if ($scope.TaskExpandedDetails != null) {
				var memorytaskListResObj = [];
				memorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
					return e.ID == taskListID && e.EntityID == entityID;
				});
				if (memorytaskListResObj.length > 0) {
					for (var j = 0; j < memorytaskListResObj[0].TaskList.length; j++) {
						if (memorytaskListResObj[0].TaskList[j] != undefined && memorytaskListResObj[0].TaskList[j].Id.toString() == taskId) {
							memorytaskListResObj[0].TaskList.splice(j, 1);
							break;
						}
					}
				}
			}
		}
		$scope.RefreshEntityTaskSortorder = function() {
			if ($scope.SourceList.IsParentEntity != "false") {
				for (var i = 0; i < $scope.groups.length; i++) {
					if ($scope.groups[i].ID.toString() == $scope.SourceList.tasklistid) {
						for (var j = 0; j < $scope.groups[i].TaskList.length; j++) {
							if ($scope.groups[i].TaskList != undefined && $scope.groups[i].TaskList[j] != undefined && $scope.groups[i].TaskList[j].Id.toString() == $scope.SourceList.taskid) {
								var SourceObj = $scope.groups[i].TaskList[j];
								SortableInsertUpdatetParentTask($scope.SourceList.tasklistid, $scope.SourceList.entityid, $scope.SourceList.taskid);
								SourceObj.EntityID = $scope.targetList.entityid;
								SourceObj.TaskListID = $scope.targetList.tasklistid;
								DestinationArrayrefresh(SourceObj);
								$scope.groups[i].TaskCount -= 1;
								break;
							}
						}
					}
				}
			} else {
				for (var i = 0; i < $scope.groupbySubLevelTaskObj.length; i++) {
					if ($scope.groupbySubLevelTaskObj[i].EntityID.toString() == $scope.SourceList.entityid) {
						for (var j = 0; j < $scope.groupbySubLevelTaskObj[i].TaskListGroup.length; j++) {
							if ($scope.groupbySubLevelTaskObj[i].TaskListGroup[j].ID == $scope.SourceList.tasklistid) {
								for (var k = 0; k < $scope.groupbySubLevelTaskObj[i].TaskListGroup[j].TaskList.length; k++) {
									if ($scope.groupbySubLevelTaskObj[i].TaskListGroup[j].TaskList[k].Id.toString() == $scope.SourceList.taskid) {
										var SourceObj = $scope.groupbySubLevelTaskObj[i].TaskListGroup[j].TaskList[k];
										SortableSubTaskInsertUpdatetParentTask($scope.SourceList.tasklistid, $scope.SourceList.entityid, $scope.SourceList.taskid);
										SourceObj.EntityID = $scope.targetList.entityid;
										SourceObj.TaskListID = $scope.targetList.tasklistid;
										DestinationArrayrefresh(SourceObj);
										$scope.groupbySubLevelTaskObj[i].TaskListGroup[j].TaskCount -= 1;
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		function DestinationArrayrefresh(SourceObj) {
			if ($scope.targetList.IsParentEntity != "false") {
				for (var i = 0; i < $scope.groups.length; i++) {
					if ($scope.groups[i].ID == $scope.targetList.tasklistid) {
						if ($scope.TaskExpandedDetails != null) {
							var taskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
								return e.ID == $scope.targetList.tasklistid && e.EntityID == $scope.targetList.entityid;
							});
							if (taskListResObj.length > 0) {
								taskListResObj[0].TaskList.push(SourceObj);
								if (taskListResObj[0].TaskCount == undefined) {
									taskListResObj[0].TaskCount = 0;
								}
								refereshTask();
								taskListResObj[0].TaskCount += 1;
								$scope.groups[i].TaskCount += 1;
							}
						}
						break;
					}
				}
			} else {
				for (var i = 0; i < $scope.groupbySubLevelTaskObj.length; i++) {
					if ($scope.groupbySubLevelTaskObj[i].EntityID.toString() == $scope.targetList.entityid) {
						for (var j = 0; j < $scope.groupbySubLevelTaskObj[i].TaskListGroup.length; j++) {
							if ($scope.groupbySubLevelTaskObj[i].TaskListGroup[j].ID.toString() == $scope.targetList.tasklistid) {
								var memorytaskListResObj = [];
								memorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
									return e.ID == $scope.targetList.tasklistid && e.EntityID == $scope.targetList.entityid;
								});
								if (memorytaskListResObj.length > 0) {
									memorytaskListResObj[0].TaskList.push(SourceObj);
									if (memorytaskListResObj[0].TaskCount == undefined) {
										memorytaskListResObj[0].TaskCount = 0;
									}
									refereshTask();
									memorytaskListResObj[0].TaskCount += 1;
									$scope.groupbySubLevelTaskObj[i].TaskListGroup[j].TaskCount += 1;
								}
								break;
							}
						}
					}
				}
			}
		}

		function refereshTask() {
			$timeout(function() {
				$scope.ExpandManually();
				if ($scope.SubLevelTaskLibraryList != undefined) {
					GroupSubLevelTaskList('EntityUniqueKey', 'All', 0);
				}
			}, 5);
		}

		function CallReorderTaskNewService(order, tasklistid, entityid, taskId, CurrentIndex) {
			var TaskListSortOrderDataip = {};
			TaskListSortOrderDataip.TaskListObj = $scope.TaskOrderListObj.UniqueSortOrderObj;
			TaskNewService.UpdateEntityTaskSortOrder(TaskListSortOrderDataip).then(function(TaskListSortOrderResult) {
				if (TaskListSortOrderResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4374.Caption'));
				} else {
					InternaltaskSortOrder(order, tasklistid, entityid, taskId, CurrentIndex);
					$scope.TaskOrderListObj.UniqueTaskSort = $translate.instant('LanguageContents.Res_4986.Caption');
					$scope.TaskOrderListObj.UniqueTaskSortingStatusID = 1;
					$scope.TaskOrderListObj.UniqueTaskOrderHandle = false;
					NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
				}
			});
		}

		function InternaltaskSortOrder(order, tasklistid, entityid, taskId, CurrentIndex) {
			var taskObjListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
				return e.ID == tasklistid && e.EntityID == entityid;
			});
			if (taskObjListResObj.length > 0) {
				var sourceobj = taskObjListResObj[0].TaskList[order];
				taskObjListResObj[0].TaskList.splice(order, 1);
				taskObjListResObj[0].TaskList.splice(CurrentIndex, 0, sourceobj);
				return false;
			}
		}
		$scope.ExpandCollapseTaskList = function() {
			$(".TL_" + $scope.ContextTaskListID).slideToggle();
		}
		$scope.addTaskList = function() {
			$scope.ContextTaskListID = 0;
			$scope.Caption = '';
			$scope.Description = '';
			$scope.EnableTaskListAdd = true;
			$scope.EnableTaskListUpdate = false;
			$("#TaskCreationModal").modal("show");
			$timeout(function() {
				$('#Caption').focus();
			}, 1000);
		};
		$scope.SaveTaskList = function() {
			if ($scope.Caption != "" && $scope.Caption != undefined) {
				if ($('#AddTaskLibrary').hasClass('disabled')) {
					return;
				}
				$('#AddTaskLibrary').addClass('disabled');
				if ($('#UpdateTaskLibrary').hasClass('disabled')) {
					return;
				}
				$('#UpdateTaskLibrary').addClass('disabled');
				var addtaskObj = {};
				addtaskObj.ID = $scope.ContextTaskListID;
				addtaskObj.Caption = $scope.Caption;
				addtaskObj.Description = $scope.Description != undefined ? $scope.Description : "";
				addtaskObj.EntityID = $scope.ContextTaskEntityID;
				addtaskObj.sortorder = 0;
				TaskNewService.InsertUpdateEntityTaskList(addtaskObj).then(function(SaveTask) {
					if (SaveTask.StatusCode == 405) {
						$('#AddTaskLibrary').removeClass('disabled');
						$('#UpdateTaskLibrary').removeClass('disabled');
						NotifyError($translate.instant('LanguageContents.Res_4271.Caption'));
					} else {
						$('#AddTaskLibrary').removeClass('disabled');
						$('#UpdateTaskLibrary').removeClass('disabled');
						$("#TaskCreationModal").modal('hide');
						var returnObj = SaveTask.Response.m_Item2;
						NotifySuccess($translate.instant('LanguageContents.Res_4833.Caption'));
						RefreshTaskList(returnObj);
					}
				});
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_4595.Caption'));
			}
		};

		function RefreshTaskList(TaskListObj) {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskObj = $.grep($scope.groups, function(e) {
					return e.ID == TaskListObj.Id;
				});
				if (taskObj.length > 0) {
					taskObj[0].LibraryName = TaskListObj.Name;
					taskObj[0].LibraryDescription = TaskListObj.Description;
				} else {
					$scope.groups.push({
						"ID": TaskListObj.Id,
						"LibraryName": TaskListObj.Name,
						"LibraryDescription": TaskListObj.Description,
						"SortOrder": TaskListObj.SortOder,
						'TaskCount': 0,
						"EntityParentID": TaskListObj.EntityParentID,
						"TaskList": [],
						"IsGetTasks": false,
						"IsExpanded": false
					});
				}
			} else {
				var EntityTaskObj = [];
				EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == TaskListObj.Id;
					});
					if (taskObjHolder.length > 0) {
						taskObjHolder[0].LibraryName = TaskListObj.Name;
						taskObjHolder[0].LibraryDescription = TaskListObj.Description;
						$timeout(function() {
							$scope.ApplySubTaskListCountDetails();
						}, 100);
					}
				} else {}
			}
		}
		$scope.UpdateTaskStatus = function(taskID, StatusID) {
			if ($scope.TemplateTaskIds.contains(taskID)) {
				$('#TemplateTaskApprovePageModel').modal("show");
			}
			var TaskStatusData = {};
			TaskStatusData.TaskID = taskID;
			TaskStatusData.Status = StatusID;
			TaskStatusData.EntityID = parseInt($stateParams.ID, 10);
			TaskNewService.UpdateTaskStatusInTaskTab(TaskStatusData).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
				} else {
					if (TaskStatusResult.Response.m_Item1 == false) {
						bootbox.alert($translate.instant('LanguageContents.Res_1965.Caption'));
						$('#WorkTask').modal('hide');
						$('#ApprovalTask').modal('hide');
						$('#ReviewTask').modal('hide');
					} else {
						RefreshCurrentTask();
						$('#WorkTask').modal('hide');
						$('#ApprovalTask').modal('hide');
						$('#ReviewTask').modal('hide');
						$('#TemplateTaskApprovePageModel').modal("hide");
						NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					}
				}
			});
		}

		function RefreshTaskAssigneeRound(taskStatusID, statusName) {
			if ($scope.TaskActionFor == 1) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
					return e.Id == $scope.TaskBriefDetails.taskID;
				});
				if (taskResObj.length > 0) {
					taskResObj[0].TaskStatus = taskStatusID;
					taskResObj[0].StatusName = statusName;
					$scope.TaskBriefDetails.status = statusName;
					$scope.TaskBriefDetails.statusID = taskStatusID;
					if (taskResObj[0].TaskType == 3 || taskResObj[0].TaskType == 31 || taskResObj[0].TaskType == 36 || taskResObj[0].TaskType == 37 || taskResObj[0].TaskType == 2) if (taskResObj[0].taskAssigness.length > 0) {
						var taskResAssigneeObj = $.grep(taskResObj[0].taskAssigness, function(e) {
							return (e.UserID == parseInt($cookies['UserId']));
						});
						if (taskResAssigneeObj.length > 0) taskResAssigneeObj[0].ApprovalStatus = taskStatusID;
					}
				}
			} else {
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.TaskBriefDetails.EntityID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
					return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
				});
				if (taskResObj.length > 0) {
					var taskObj = $.grep(taskResObj[0].TaskList, function(e) {
						return e.Id == $scope.TaskBriefDetails.taskID;
					});
					if (taskObj.length > 0) {
						taskObj[0].TaskStatus = taskStatusID;
						taskObj[0].StatusName = statusName;
						$scope.TaskBriefDetails.status = statusName;
						$scope.TaskBriefDetails.statusID = taskStatusID;
						if (taskObj[0].TaskType == 3 || taskObj[0].TaskType == 31 || taskResObj[0].TaskType == 36 || taskResObj[0].TaskType == 37 || taskObj[0].TaskType == 2) if (taskObj[0].taskAssigness.length > 0) {
							var taskResAssigneeObj = $.grep(taskObj[0].taskAssigness, function(e) {
								return (e.UserID == parseInt($cookies['UserId']));
							});
							if (taskResAssigneeObj.length > 0) taskResAssigneeObj[0].ApprovalStatus = taskStatusID;
						}
					}
				}
			}
		}
		$scope.UpdateTaskStatusNotApplicableUnassigned = function() {
			TaskNewService.UpdatetasktoNotApplicableandUnassigned($scope.contextTaskID).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
				} else {
					RefreshUnApplicapleStatusObject(TaskStatusResult.Response.m_Item2, TaskStatusResult.Response.m_Item3);
					NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
				}
			});
		}

		function RefreshUnApplicapleStatusObject(statusId, StatusName) {
			if ($scope.ContextTaskEntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.ContextTaskListID;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
					return e.Id == $scope.contextTaskID;
				});
				if (taskResObj.length > 0) {
					taskResObj[0].TaskStatus = statusId;
					taskResObj[0].StatusName = StatusName;
				}
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.ContextTaskEntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.ContextTaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == $scope.contextTaskID;
						});
						if (taskObjHolder.length > 0) {
							taskObjHolder[0].TaskStatus = statusId;
							taskObjHolder[0].StatusName = StatusName;
						}
					}
				}
			}
		}
		$scope.taskMenuOptionText = 'Set to "Not Applicable"';
		$scope.OrganizeTaskMenuOption = function(taskStatus) {
			if (taskStatus == 0) {
				$scope.taskMenuOptionText = 'Set to "Not Applicable"';
				var phase = this.$root.$$phase;
				if (phase == '$apply' || phase == '$digest') {} else {
					this.$apply();
				}
				$('#context2 li#TM3').show();
			} else if (taskStatus == 7) {
				$scope.taskMenuOptionText = 'Set to "Unassigned"';
				var phase = this.$root.$$phase;
				if (phase == '$apply' || phase == '$digest') {} else {
					this.$apply();
				}
				$('#context2 li#TM3').show();
			} else {
				$('#context2 li#TM3').hide();
			}
		}
		$scope.Fund_newsfeed_TEMP = [];
		var divtypeid = '';
		var fundrequestID = 0;
		$scope.userimgsrc = '';
		$scope.commentuserimgsrc = '';

		function feedforApprovalTask(entityid, divid, isLatestnewsfeed) {
			var tempPageno = 0;
			if (isLatestnewsfeed == false) {
				$('#' + divid).html('');
				PagenoforScroll = 0;
				$scope.temptaskDiv = divid;
				$scope.TaskFeedReloadOnScroll();
				tempPageno = 0;
			} else {
				tempPageno = -1;
			}
			fundrequestID = entityid;
			divtypeid = divid;
			try {
				TaskNewService.GetEnityFeedsForFundingReq(entityid, tempPageno, isLatestnewsfeed).then(function(getFundingreqNewsFeedResult) {
					var fundingreqfundingreqfeeddivHtml = '';
					if (getFundingreqNewsFeedResult.Response.length > 0) {
						if (!isLatestnewsfeed) {
							$scope.Fund_newsfeed_TEMP = [];
						}
						$scope.Fund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
					}
					for (var i = 0; i < getFundingreqNewsFeedResult.Response.length; i++) {
						var feedcomCount = getFundingreqNewsFeedResult.Response[i].FeedComment != null ? getFundingreqNewsFeedResult.Response[i].FeedComment.length : 0;
						var fundingreqfeeddivHtml = '';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></h5></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedText + '</p></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + divid + '"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" id="Comment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
						if (feedcomCount > 0) {
							fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="taskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Taskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
						} else {
							fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="taskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Taskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
						}
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
						if (getFundingreqNewsFeedResult.Response[i].FeedComment != null && getFundingreqNewsFeedResult.Response[i].FeedComment != '') {
							var j = 0;
							if (getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
								$scope.commentuserimgsrc = $scope.NewUserImgSrc;
							} else {
								$scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
							}
							for (var k = 0; k < getFundingreqNewsFeedResult.Response[i].FeedComment.length; k++) {
								$scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
								if (k == 0) {
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
								} else {
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
								}
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="mytaskfeedcomment_' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
								fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
							}
						}
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul/></li>';
						$('#' + divid + ' [data-id]').each(function() {
							if (parseInt($(this).attr('data-id')) == getFundingreqNewsFeedResult.Response[i].FeedId) {
								$(this).remove();
							}
						});
						if (isLatestnewsfeed == false) {
							$('#' + divid).append(fundingreqfeeddivHtml);
							$('#' + divid).scrollTop(0);
						} else {
							$('#' + divid).prepend(fundingreqfeeddivHtml);
						}
						fundingreqfeeddivHtml = '';
						feedcomCount = 0;
					}
					commentclick(divid);
					$('#' + divid).on('click', 'a[data-command="openlink"]', function(event) {
						var TargetControl = $(this);
						var mypage = TargetControl.attr('data-Name');
						var myname = TargetControl.attr('data-Name');
						var w = 1200;
						var h = 800
						var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
						var win = window.open(mypage, myname, winprops)
					});
				});
			} catch (e) {}
		}
		$scope.Timerforcallback = function(entityid, divid, stat) {
			feedforApprovalTask(parseInt(entityid), divid, true);
			NewsFeedUniqueTimerForTask = $timeout(function() {
				if ($('#' + divid).css('display') != 'none') {
					$scope.Timerforcallback(parseInt(entityid), divid, true);
				}
			}, 30000);
		}
		var PagenoforScroll = 0;
		$scope.TaskFeedReloadOnScroll = function() {
			try {
				$('#' + $scope.temptaskDiv).scroll(function() {
					if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
						PagenoforScroll += 2;
						TaskNewService.GetEnityFeedsForFundingReq($scope.tempTaskID, PagenoforScroll, false).then(function(latestNewsFeedResult) {
							var fundingreqfundingreqfeeddivHtml = '';
							if (latestNewsFeedResult.Response.length > 0) {
								for (var i = 0; i < latestNewsFeedResult.Response.length; i++) {
									var feedcomCount = latestNewsFeedResult.Response[i].FeedComment != null ? latestNewsFeedResult.Response[i].FeedComment.length : 0;
									var fundingreqfeeddivHtml = '';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + latestNewsFeedResult.Response[i].FeedId + '>';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + latestNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].UserEmail + '>' + latestNewsFeedResult.Response[i].UserName + '</a></h5></div>';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedText + '</p></div>';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedHappendTime + '</span>';
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + $scope.temptaskDiv + '"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedId + '" id="Comment_' + latestNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
									if (feedcomCount > 0) {
										fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="taskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Taskfeed_' + latestNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
									} else {
										fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="taskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Taskfeed_' + latestNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
									}
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
									if (latestNewsFeedResult.Response[i].FeedComment != null && latestNewsFeedResult.Response[i].FeedComment != '') {
										var j = 0;
										if (latestNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
											$scope.commentuserimgsrc = $scope.NewUserImgSrc;
										} else {
											$scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
										}
										for (var k = 0; k < latestNewsFeedResult.Response[i].FeedComment.length; k++) {
											$scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
											if (k == 0) {
												fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
											} else {
												fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
											}
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="mytaskfeedcomment_' + latestNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + latestNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
											fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
										}
									}
									fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
									$('#' + $scope.temptaskDiv).append(fundingreqfeeddivHtml);
									fundingreqfeeddivHtml = '';
									var duplicateChk = {};
									$('#' + $scope.temptaskDiv + '[data-id]').each(function() {
										if (duplicateChk.hasOwnProperty(this.id)) {
											$(this).remove();
										} else {
											duplicateChk[this.id] = 'true';
										}
									});
								}
							} else PagenoforScroll -= 2;
							commentclick($scope.temptaskDiv);
						});
					}
				});
			} catch (e) {}
		}
		$('#Taskfeeddivforapprovaltask').on('click', 'a[data-DynHTML="taskCommentshowHTML"]', function(event) {
			var feedid1 = event.target.attributes["id"].nodeValue;
			var feedid = feedid1.substring(9);
			var feedfilter = $.grep($scope.Fund_newsfeed_TEMP[0], function(e) {
				return e.FeedId == parseInt(feedid);
			});
			$("#Taskfeed_" + feedid).next('div').hide();
			if (feedfilter != '') {
				for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
					if ($('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
						$(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
						if (i != 0) $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function() {});
						else $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
					} else {
						$('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
						$(this)[0].innerHTML = "Hide comment(s)";
					}
				}
			}
		});
		$('#feeddivforreviewTask').on('click', 'a[data-DynHTML="taskCommentshowHTML"]', function(event) {
			var feedid1 = event.target.attributes["id"].nodeValue;
			var feedid = feedid1.substring(9);
			var feedfilter = $.grep($scope.Fund_newsfeed_TEMP[0], function(e) {
				return e.FeedId == parseInt(feedid);
			});
			$("#Taskfeed_" + feedid).next('div').hide();
			if (feedfilter != '') {
				for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
					if ($('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
						$(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
						if (i != 0) $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function() {});
						else $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
					} else {
						$('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
						$(this)[0].innerHTML = "Hide comment(s)";
					}
				}
			}
		});
		$('#Taskfeeddivforworktask').on('click', 'a[data-DynHTML="taskCommentshowHTML"]', function(event) {
			var feedid1 = event.target.attributes["id"].nodeValue;
			var feedid = feedid1.substring(9);
			var feedfilter = $.grep($scope.Fund_newsfeed_TEMP[0], function(e) {
				return e.FeedId == parseInt(feedid);
			});
			$("#Taskfeed_" + feedid).next('div').hide();
			if (feedfilter != '') {
				for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
					if ($('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
						$(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
						if (i != 0) $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function() {});
						else $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
					} else {
						$('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
						$(this)[0].innerHTML = "Hide comment(s)";
					}
				}
			}
		});
		$('#feeddivforunassignedassigned').on('click', 'a[data-DynHTML="taskCommentshowHTML"]', function(event) {
			var feedid1 = event.target.attributes["id"].nodeValue;
			var feedid = feedid1.substring(9);
			var feedfilter = $.grep($scope.Fund_newsfeed_TEMP[0], function(e) {
				return e.FeedId == parseInt(feedid);
			});
			$("#Taskfeed_" + feedid).next('div').hide();
			if (feedfilter != '') {
				for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
					if ($('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
						$(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
						if (i != 0) $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function() {});
						else $('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
					} else {
						$('#mytaskfeedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function() {});
						$(this)[0].innerHTML = "Hide comment(s)";
					}
				}
			}
		});
		$scope.AddApprovalTaskNewsFeed = function(divid, commentbuttinid) {
			var temp = '';
			if (fundrequestID != 0) {
				if (commentbuttinid == 'Taskcommentwork') temp = 'taskfeedtextholderwork';
				else if (commentbuttinid == 'ApprovalTaskcomment') temp = 'ApprovalTaskfeedtextholder';
				else if (commentbuttinid == 'reviewTaskcomment') temp = 'reviewtextholder';
				else if (commentbuttinid == 'ProofHQApprovalTaskcomment') temp = 'ProofHQApprovalTaskfeedtextholder';
				else if (commentbuttinid == 'dalimApprovalTaskcomment') temp = 'dalimapprovaltaskfeedtextholder';
				else temp = 'feedtextholderunassigned';
				if ($('#' + temp).text() != "") {
					return false;
				}
				var addnewsfeed = {};
				addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
				addnewsfeed.TemplateID = 40;
				addnewsfeed.EntityID = parseInt(fundrequestID);
				addnewsfeed.TypeName = "Tasks";
				addnewsfeed.AttributeName = "";
				addnewsfeed.FromValue = "";
				addnewsfeed.ToValue = $('#' + commentbuttinid).text();
				TaskNewService.PostFeed(addnewsfeed).then(function(savenewsfeed) {});
				$('#' + commentbuttinid).empty();
				if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/g, '').replace(/ /g, '').length == 0) {
					document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
				}
			}
			$timeout(function() {
				feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
			}, 2000);
		};

		function commentclick(divtypeid) {
			$('#' + divtypeid).on('click', 'a[data-DynHTML="CommentHTML1_' + divtypeid + '"]', function(event) {
				var commentuniqueid = this.id;
				event.stopImmediatePropagation();
				$(this).hide();
				var fundingreqfeeddivHtml = '';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li class="writeNewComment">';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="mytaskfeedcomment_' + commentuniqueid + '"></div></div>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary"  data-dyncommentbtn="ButtonHTML_' + divtypeid + '" >Comment</button></div></li>';
				var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
				if ($(this).parents('li[data-parent="NewsParent"]').find('ul li').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(fundingreqfeeddivHtml);
				else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(fundingreqfeeddivHtml);
				$timeout(function() {
					$('#mytaskfeedcomment_' + commentuniqueid).html('').focus();
				}, 10);
				buttonclick(divtypeid);
			});
		}

		function buttonclick(divtypeid) {
			$('#' + divtypeid).on('click', 'button[data-dyncommentbtn="ButtonHTML_' + divtypeid + '"]', function(event) {
				event.stopImmediatePropagation();
				if ($(this).prev().eq(0).text().toString().trim().length > 0) {
					var addfeedcomment = {};
					var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
					var myDate = new Date.create();
					addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
					addfeedcomment.Actor = parseInt($cookies['UserId']);
					addfeedcomment.Comment = $(this).prev().eq(0).text();
					var d = new Date.create();
					var month = d.getMonth() + 1;
					var day = d.getDate();
					var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
					var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
					var _this = $(this);
					TaskNewService.InsertFeedComment(addfeedcomment).then(function(saveusercomment) {
						var fundingreqfeeddivHtml = '';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class=\"newsFeed\">';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
						fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></div></li>';
						$('#' + divtypeid).find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(fundingreqfeeddivHtml)
						$(".writeNewComment").remove();;
						$('#Comment_' + addfeedcomment.FeedID).show();
					});
				}
			});
		}
		$scope.GetAttachmentResponse = [];
		$scope.openattachment = function() {
			TaskNewService.GetFilesandLinksByEntityID($scope.TaskGlobalEntityID).then(function(getattachments) {
				$scope.getattachmentslist = getattachments.Response;
				if ($scope.getattachmentslist.length != 0) {
					$scope.GetAttachmentResponse = $scope.getattachmentslist;
					$scope.noattachments = '';
				} else {
					$scope.noattachments = 'No attachment present';
				}
			});
		}
		$scope.parseSize = function(size) {
			var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
			while (size >= 1024) {
				size = size / 1024;
				tier++;
			}
			return Math.round(size * 10) / 10 + " " + suffix[tier];
		}
		$scope.LinkSelected = [];
		$scope.FileSelected = [];
		$scope.attachmentid = function(e) {
			var check = e.target.checked;
			var extn = e.target.attributes[1].nodeValue;
			var checkedid = e.target.attributes[2].nodeValue;
			if (check == true) {
				if (extn == "Link") {
					$scope.LinkSelected.push(checkedid);
				} else {
					$scope.FileSelected.push(checkedid);
				}
			} else {
				if (extn == "Link") {
					$scope.LinkSelected = jQuery.grep($scope.LinkSelected, function(value) {
						return value != checkedid;
					});
				} else {
					$scope.FileSelected = jQuery.grep($scope.FileSelected, function(value) {
						return value != checkedid;
					});
				}
			}
		}
		$scope.SaveAttachfile = function() {
			if ($scope.LinkSelected.length == 0 && $scope.FileSelected.length == 0) {
				bootbox.alert($translate.instant('LanguageContents.Res_1966.Caption'));
				return false;
			}
			var items = GetRootLevelSelectedAll();
			var IDList = new Array();
			IDList = GetRootLevelSelectedAll();
			var TaskStatusData = {};
			TaskStatusData.EntityID = $scope.TaskBriefDetails.taskID;
			TaskStatusData.linkIDs = $scope.LinkSelected;
			TaskStatusData.ActivFileIDs = $scope.FileSelected;
			TaskNewService.CopyAttachmentsfromtask(TaskStatusData).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					$('#generalattachmentsPopup').modal('hide');
				} else {
					if (TaskStatusResult.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						$("#FileUpdateModal").modal("hide");
					} else {
						if (TaskStatusResult.StatusCode == 200) {
							if (TaskStatusResult.Extension == "Link") {
								ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
								NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
								$('#generalattachmentsPopup').modal('hide');
							} else {
								ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
								NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
								$('#generalattachmentsPopup').modal('hide');
							}
						}
					}
				}
				$scope.LinkSelected = [];
				$scope.FileSelected = [];
			});
		};
		$scope.ContextFileExtention = "";
		$scope.loadcontextmenu = function(e) {
			var linkextn = $(e.target).attr('data-extension');
			$scope.ContextFileExtention = linkextn;
			if (linkextn == "Link") {
				$scope.showmanageverson = false;
				$scope.shownewversion = false;
			} else {
				$scope.showmanageverson = true;
				$scope.shownewversion = true;
			}
		}
		$scope.CopytoGeneralAttachments = function(event) {
			var TaskStatusData = {};
			$scope.filelist = 0;
			$scope.linklist = 0;
			var fileextn = $("#context5copy_" + $scope.contextFileID + "").attr("data-extension");
			if (fileextn == "Link") {
				$scope.linklist = $scope.contextFileID;
			} else {
				$scope.filelist = $scope.contextFileID;
			}
			TaskStatusData.EntityID = $scope.TaskGlobalEntityID;
			TaskStatusData.ActivFileIDs = $scope.filelist;
			TaskStatusData.linkIDs = $scope.linklist;
			TaskNewService.CopytoGeneralAttachments(TaskStatusData).then(function(TaskStatusData) {
				if (TaskStatusData.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
				} else {
					if (TaskStatusData.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					} else {
						ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
						NotifySuccess($translate.instant('LanguageContents.Res_4170.Caption'));
					}
				}
			});
		}

		function GetRootLevelSelectedAll() {
			var IDList = new Array();
			$('#attchmentListContainer > tbody input:checked').each(function() {
				IDList.push($(this).attr('data-id'));
			});
			return IDList
		}
		$('#finicialfundingrequestfeeddiv1').on('click', 'button[data-DynHTML="CommentHTML1"]', function(event) {
			event.stopImmediatePropagation();
			$(this).hide();
			var fundingreqfeeddivHtml = '';
			fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="feedcontent"><li class="postComment">';
			fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="postbox"><div class="avatar"><span class="user"><img data-role="user-avatar" src=Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.NewTime + '></span></div>';
			fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment1"><br></div></div>';
			fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary" data-dyncommentbtn="ButtonHTML1" >Comment</button></div></li></ul>';
			$(this).parents('div.block').after(fundingreqfeeddivHtml);
		});
		$('#finicialfundingrequestfeeddiv1').on('click', 'button[data-dyncommentbtn="ButtonHTML1"]', function(event) {
			event.stopImmediatePropagation();
			var addfeedcomment = {};
			var myDate = new Date.create();
			addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
			addfeedcomment.Actor = parseInt($cookies['UserId']);
			addfeedcomment.Comment = $(this).prev().eq(0).text();
			var d = new Date.create();
			var month = d.getMonth() + 1;
			var day = d.getDate();
			var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
			var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
			var _this = $(this);
			TaskNewService.InsertFeedComment(addfeedcomment).then(function(saveusercomment) {
				var fundingreqfeeddivHtml = '';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="feedcontent"><li>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"><img src= Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.NewTime + ' alt="Avatar"></span>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block"><div class="arrow-left"></div>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"> <a href="undefined">' + $cookies['Username'] + '</a>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</span>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">' + saveusercomment.Response + '</span>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">Few seconds ago</span></span>';
				fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></li></ul>';
				$(_this).parents("li:eq(1)").append(fundingreqfeeddivHtml);
				$('#Comment_' + addfeedcomment.FeedID).show();
				$(_this).closest('ul').empty();
			});
		});

		function sortOnMemberRound(collection, name) {
			collection.sort(function(a, b) {
				if (a[name] >= b[name]) {
					return (-1);
				}
				return (1);
			});
		}
		$scope.groupByTaskRoundTrip = function(attribute) {
			$scope.TaskBriefDetails.TaskMembersRoundTripGroup = [];
			sortOnMemberRound($scope.TaskBriefDetails.totalTaskMembers, attribute);
			$scope.TaskBriefDetails.LatestMaxRound = "Round " + ($scope.TaskBriefDetails.totalTaskMembers[0].ApprovalRount + 1).toString();
			var groupValue = "_INVALID_GROUP_VALUE_";
			for (var i = 0, friend; friend = $scope.TaskBriefDetails.totalTaskMembers[i++];) {
				if (friend[attribute] !== groupValue) {
					var group = {
						label: friend[attribute],
						friends: [],
						rolename: "Round " + friend.ApprovalRount.toString()
					};
					groupValue = group.label;
					$scope.TaskBriefDetails.TaskMembersRoundTripGroup.push(group);
				}
				group.friends.push(friend);
			}
		};
		$scope.name = "";
		$scope.DescriptionChange = "";
		$scope.dueDate = new Date.create();
		$scope.dueDate = null;
		$scope.EntityTaskChangesHolder = {
			"TaskName": "",
			"TaskDescription": "",
			"DueDate": "",
			"Note": ""
		};
		$scope.saveEntityTaskDetails = function(type) {
			if (type == "nam") {
				if ($scope.name != "") {
					if ($scope.TaskBriefDetails.taskName != $scope.name) {
						$scope.EntityTaskChangesHolder.TaskName = $scope.name;
						$scope.updatetaskEdit = true;
					} else return false;
				} else {
					bootbox.alert($translate.instant('LanguageContents.Res_1967.Caption'));
					return false;
				}
			} else if (type == "note") {
				if ($scope.TaskBriefDetails.Note != $scope.Notechange) {
					$scope.EntityTaskChangesHolder.Note = $scope.Notechange;
					$scope.updatetaskEdit = true;
				} else return false;
			} else if (type == "des") {
				if ($scope.TaskBriefDetails.Description != $scope.DescriptionChange) {
					$scope.EntityTaskChangesHolder.TaskDescription = $scope.DescriptionChange;
					$scope.updatetaskEdit = true;
				} else return false;
			} else if (type == "due") {
				if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
					$scope.EntityTaskChangesHolder.DueDate = ConvertDateToString($scope.dueDate);
					var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
					if (dateFormat(tempduedate1) == dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
						$scope.updatetaskEdit = false;
					} else {
						$scope.updatetaskEdit = true;
					}
				} else {
					bootbox.alert($translate.instant('LanguageContents.Res_1969.Caption'));
					return false;
				}
			} else if (type == "unassignedue") {
				if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
					$scope.EntityTaskChangesHolder.DueDate = ConvertDateToString($scope.dueDate);
					var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
					if (dateFormat(tempduedate1) == dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
						$scope.updatetaskEdit = false;
					} else {
						$scope.updatetaskEdit = true;
					}
				} else {
					$scope.EntityTaskChangesHolder.DueDate = '';
				}
			}
			var TaskStatusData = {};
			TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
			TaskStatusData.TaskName = $scope.EntityTaskChangesHolder.TaskName;
			TaskStatusData.Description = $scope.EntityTaskChangesHolder.TaskDescription;
			TaskStatusData.Note = $scope.EntityTaskChangesHolder.Note;
			TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate;
			TaskStatusData.entityID = parseInt($stateParams.ID, 10);
			TaskStatusData.actionfor = type;
			TaskNewService.UpdatetaskEntityTaskDetailsInTaskTab(TaskStatusData).then(function(TaskStatusResult) {
				$scope.fired = false;
				if (TaskStatusResult.StatusCode == 405) {
					if ($scope.updatetaskEdit != false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
					}
					$scope.fired = false;
				} else {
					if (TaskStatusResult.Response == false) {
						if ($scope.updatetaskEdit != false) {
							NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						}
						$scope.fired = false;
					} else {
						if (type == "unassignedue") {
							if ($scope.EntityTaskChangesHolder.DueDate != '') {
								$scope.TaskBriefDetails.dueIn = dateDiffBetweenDates($scope.dueDate);
								$scope.dueDate = $scope.EntityTaskChangesHolder.DueDate;
								$scope.fired = false;
							} else {
								$scope.dueDate = '-';
								$scope.TaskBriefDetails.dueDate = '-';
								$scope.TaskBriefDetails.dueIn = 0;
								$scope.fired = false;
							}
						}
						if ($scope.targetList.IsParentEntity != "false") {
							RefreshCurrentTask();
						} else {
							RefreshSubEntityCurrentTask();
						}
						$scope.EntityTaskChangesHolder = {
							"TaskName": "",
							"TaskDescription": "",
							"DueDate": "",
							"Note": ""
						};
						$scope.TaskBriefDetails.taskName = $scope.name;
						$scope.TaskBriefDetails.Description = $scope.DescriptionChange;
						$scope.TaskBriefDetails.Note = $scope.Notechange;
						if ($scope.updatetaskEdit != false) {
							NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
						}
						$timeout(function() {
							feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
						}, 2000);
						$scope.fired = false;
					}
				}
			});
		}
		$scope.saveReassignedTaskDuedate = function(type) {
			if (type == "ReassignedAssignedDatepicker") {
				if ($scope.ListUnassignedTasksDueObjects.Duedate != '-' && $scope.ListUnassignedTasksDueObjects.Duedate != undefined && $scope.ListUnassignedTasksDueObjects.Duedate != null) {
				    $scope.ListUnassignedTasksDueObjects.AssignableDueDate = dateFormat($scope.ListUnassignedTasksDueObjects.Duedate, $scope.DefaultSettings.DateFormat);
				    if (new Date.create($scope.ListUnassignedTasksDueObjects.Duedate) > new Date.create()) {
				        $scope.ListUnassignedTasksDueObjects.dueIn = dateDiffBetweenDates($scope.ListUnassignedTasksDueObjects.Duedate) - HolidayCount(new Date.create($scope.ListUnassignedTasksDueObjects.Duedate));
				    } else {
				        $scope.ListUnassignedTasksDueObjects.dueIn = dateDiffBetweenDates($scope.ListUnassignedTasksDueObjects.Duedate) + HolidayCount(new Date.create($scope.ListUnassignedTasksDueObjects.Duedate));
				    }
					//$scope.ListUnassignedTasksDueObjects.dueIn = dateDiffBetweenDates($scope.ListUnassignedTasksDueObjects.Duedate);
					$scope.ListUnassignedTasksDueObjects.TaskDueDate = $scope.ListUnassignedTasksDueObjects.AssignableDueDate;
					$scope.fired = false;
				} else {
					$scope.ListUnassignedTasksDueObjects.TaskDueDate = '-';
					$scope.ListUnassignedTasksDueObjects.dueIn = 0;
					$scope.ListUnassignedTasksDueObjects.Duedate = '-';
					$scope.fired = false;
				}
			}
		}

		function dateDiffBetweenDates(dateTo) {
			var _MS_PER_DAY = 1000 * 60 * 60 * 24;
			var dateToday = new Date.create();
			var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
			var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
			return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
		}

		function GetTaskCheckLists() {
			TaskNewService.getTaskchecklist($scope.TaskBriefDetails.taskID).then(function(TaskDetailList) {
				$scope.TaskBriefDetails.taskCheckList = [];
				if (TaskDetailList.Response.length > 0) {
					$scope.TaskBriefDetails.taskCheckList = TaskDetailList.Response;
				}
				if ($scope.TaskBriefDetails.taskTypeId == 2 && $scope.TaskBriefDetails.statusID == 0 && $scope.TaskBriefDetails.taskCheckList.length > 0) {
					var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function(e) {
						return e.Status == true;
					});
					$scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
				}
				if ($scope.TaskBriefDetails.statusID == 2 || $scope.TaskBriefDetails.statusID == 3 || $scope.TaskBriefDetails.statusID == 8) {
					$scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
				} else {
					if ($scope.TaskBriefDetails.taskCheckList.length > 0) {
						var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function(e) {
							return e.Status == true;
						});
						$scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
					} else {
						$scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
					}
				}
				if ($scope.TaskActionFor == 1) {
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
					});
					var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
						return e.Id == $scope.TaskBriefDetails.taskID;
					});
					if (taskResObj.length > 0) {
						taskResObj[0].TaskCheckList = TaskDetailList.Response;
					}
				} else {
					var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == $scope.TaskBriefDetails.EntityID;
					});
					var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
					});
					if (taskResObj.length > 0) {
						var taskObj = $.grep(taskResObj[0].TaskList, function(e) {
							return e.Id == $scope.TaskBriefDetails.taskID;
						});
						if (taskObj.length > 0) taskObj[0].TaskCheckList = TaskDetailList.Response;
					}
				}
			});
		}

		function GetadminTaskCheckLists(taskID) {
			$scope.AdminTaskCheckList = [];
			TaskNewService.getTaskchecklist(taskID).then(function(TaskDetailList) {
				$scope.AdminTaskCheckList = TaskDetailList.Response;
				if ($scope.AdminTaskCheckList.length == 0) $scope.AdminTaskCheckList.push({
					ID: 0,
					NAME: ""
				});
			});
		}

		function GetEntityAttributesDetails(taskID) {
			TaskNewService.GetEntityAttributesDetails(taskID).then(function(TaskDetailList) {
				LoadTaskMetadata(TaskDetailList.Response, $scope.TaskBriefDetails.taskID, $scope.TaskBriefDetails.taskTypeId, $scope.fromAssignedpopup);
			});
		}
		$scope.OpenChecklist = function() {
			$scope.txtCheckListName = "";
			$("#TaskCheckListPopup").modal("show");
		}
		$scope.SaveTaskCheckList = function(Index, CheckListID, ChklistName, ChkListStatus, CheCklistExisting) {
			var checkbox;
			if (ChkListStatus.target != undefined) {
				checkbox = ChkListStatus.target.checked;
			} else {
				checkbox = ChkListStatus;
			}
			if (ChklistName != '-' && ChklistName.length > 0) {
				var SaveTask = {};
				SaveTask.Id = CheckListID;
				SaveTask.taskId = $scope.TaskBriefDetails.taskID;
				SaveTask.ChkListStatus = checkbox;
				SaveTask.ISowner = (CheckListID > 0 ? false : true);
				SaveTask.SortOrder = (Index + 1);
				SaveTask.CheckListName = ChklistName;
				SaveTask.isnew = CheCklistExisting == undefined ? false : true;
				SaveTask.EntityID = parseInt($stateParams.ID, 10);
				TaskNewService.InsertUpdateEntityTaskCheckListInTask(SaveTask).then(function(SaveTaskResult) {
					if (SaveTaskResult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4263.Caption'));
					} else {
						NotifySuccess($translate.instant('LanguageContents.Res_4157.Caption'));
						var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function(n, i) {
							if (i == Index) {
								return $scope.TaskBriefDetails.taskCheckList[i];
							}
						});
						Object[0].Id = SaveTaskResult.Response;
						Object[0].Status = checkbox;
						Object[0].IsExisting = true;
						RefreshCurrentTask();
					}
				});
			} else {
				bootbox.alert($translate.instant('LanguageContents.Res_1970.Caption'));
				CheCklistExisting = true;
			}
			$timeout(function() {
				feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
			}, 2000);
		}
		var CurrentCheckListName = "";
		$scope.EditCheckList = function(Name, Index) {
			var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function(n, i) {
				if ((i != Index && n.IsExisting == false)) {
					return $scope.TaskBriefDetails.taskCheckList[i];
				}
			});
			if (Object.length > 0) {
				if (Object[0].Name.length == 0) {
					if (Object[0].Id == 0) {
						$scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
					} else {
						Object[0].IsExisting = true;
						Object[0].Name = CurrentCheckListName;
					}
				} else {
					Object[0].IsExisting = true;
					Object[0].Name = CurrentCheckListName;
				}
			}
			CurrentCheckListName = Name;
		}
		$scope.CloseCheckListPopUp = function(Index, CheCklistExisting, ChklistName) {
			if ($scope.TaskBriefDetails.taskCheckList[Index].Id == 0) {
				$scope.TaskBriefDetails.taskCheckList.splice(Index, 1);
			} else {
				$scope.TaskBriefDetails.taskCheckList[Index].Name = CurrentCheckListName;
				$scope.TaskBriefDetails.taskCheckList[Index].IsExisting = true;
			}
			$("#NonEditableCL" + Index).show();
			$("#EditableCL" + Index).hide();
		}
		$scope.ChecklistisSelected = function($event, id) {
			var checkbox = $event.target;
			TaskNewService.ChecksTaskCheckList(id, checkbox.checked).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
				} else {
					NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
					GetTaskCheckLists();
				}
			});
		};
		$scope.AddCheckList = function(Index) {
			if ($scope.AdminTaskCheckList[Index].Name == undefined || $scope.AdminTaskCheckList[Index].Name.length == 0) {
				bootbox.alert($translate.instant('LanguageContents.Res_4589.Caption'));
				return false;
			}
			if ($scope.SelectedTaskID != 0) $scope.AdminTaskCheckList.push({
				Id: 0,
				Name: "",
				SortOrder: 0,
				Status: false,
				UserName: "",
				UserId: 0,
				CompletedOnValue: "",
				OwnerId: 0,
				OwnerName: "",
				CompletedOn: ""
			});
			else $scope.AdminTaskCheckList.push({
				ID: 0,
				NAME: ""
			});
		}
		$scope.RemoveCheckList = function(Index, ID) {
			if ($scope.AdminTaskCheckList.length > 1) {
				bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function(result) {
					if (result) {
						$timeout(function() {
							if (ID > 0) {
								TaskNewService.DeleteEntityCheckListByID(ID).then(function(result) {
									if (result.StatusCode == 200) {
										$scope.AdminTaskCheckList.splice(Index, 1);
									}
								});
							} else {
								$scope.AdminTaskCheckList.splice(Index, 1);
							}
						}, 100);
					}
				});
			} else {
				bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function(result) {
					if (result) {
						$timeout(function() {
							if (ID > 0) {
								TaskNewService.DeleteEntityCheckListByID(ID).then(function(result) {
									if (result.StatusCode == 200) {
										$scope.AdminTaskCheckList[Index].NAME = "";
									}
								});
							} else {
								$scope.AdminTaskCheckList[Index].NAME = "";
							}
						}, 100);
					}
				});
			}
			$timeout(function() {
				feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
			}, 5000);
		}
		$scope.CheckListSelection = function(status) {
			var baseClass = "checkbox";
			if (status) baseClass += " checked";
			return baseClass;
		}
		$scope.contextCheckListID = 0;
		$scope.contextCheckListIndex = 0;
		$scope.SetcontextCheckListID = function(id, indexval) {
			$scope.contextCheckListID = id;
			$scope.contextCheckListIndex = indexval;
		};
		$scope.DeleteEntityChecklists = function() {
			bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function(result) {
				if (result) {
					$timeout(function() {
						if ($scope.contextCheckListID != 0) {
							TaskNewService.DeleteEntityCheckListByID($scope.contextCheckListID).then(function(data) {
								if (data.StatusCode == 200) {
									var fileObj = $.grep($scope.TaskBriefDetails.taskCheckList, function(e) {
										return e.Id == $scope.contextCheckListID;
									});
									$scope.TaskBriefDetails.taskCheckList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskCheckList), 1);
									RefreshCurrentTask();
									feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
								}
							});
						} else {
							$scope.TaskBriefDetails.taskCheckList.splice($scope.contextCheckListIndex, 1);
						}
					}, 100);
				}
			});
		}
		$scope.AddCheckListNext = function() {
			var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function(n, i) {
				if (n.IsExisting == false) {
					return $scope.TaskBriefDetails.taskCheckList[i];
				}
			});
			if (Object.length > 0) {
				if (Object[0].Name.length == 0) {
					if (Object[0].Id == 0) {
						$scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
					} else {
						Object[0].IsExisting = true;
						Object[0].Name = CurrentCheckListName;
					}
				} else {
					Object[0].Name = CurrentCheckListName;
					Object[0].IsExisting = true;
				}
			}
			if ($scope.contextCheckListIndex == -1) {
				$scope.TaskBriefDetails.taskCheckList.push({
					Id: 0,
					Name: '',
					OwnerId: 0,
					OwnerName: 0,
					SortOrder: 1,
					Status: false,
					TaskId: $scope.TaskBriefDetails.taskID,
					UserId: 0,
					UserName: "",
					CompletedOnValue: "",
					CompletedOn: null,
					IsExisting: false
				});
			} else {
				$scope.TaskBriefDetails.taskCheckList.splice(parseInt($scope.contextCheckListIndex) + 1, 0, {
					Id: 0,
					Name: '',
					OwnerId: 0,
					OwnerName: 0,
					SortOrder: 1,
					Status: false,
					TaskId: $scope.TaskBriefDetails.taskID,
					UserId: 0,
					UserName: "",
					CompletedOnValue: "",
					CompletedOn: null,
					IsExisting: false
				});
			}
			$timeout(function() {
				$('[datafocus-id =0]').focus();
			}, 10);
		}

		function RefreshCurrentTask() {
			$scope.TaskDetails = [];
			TaskNewService.GetEntityTaskDetails($scope.TaskBriefDetails.taskID).then(function(EntityTaskDetails) {
				$scope.TaskDetails = EntityTaskDetails.Response;
				RefreshUnassignedTaskInObject($scope.TaskDetails);
				if ($scope.TaskDetails != null) if ($scope.TaskDetails.TaskStatus != 0) BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				else {
					LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				}
			});
		}

		function RefreshCurrentTasksCollectionAfterAttachFile(taskidCollection) {
			if (taskidCollection.length > 0) {
				for (var i = 0; i <= taskidCollection.length - 1; i++) {
					$scope.TaskDetails = [];
					TaskNewService.GetEntityTaskDetails(taskidCollection[i].TaskID).then(function(EntityTaskDetails) {
						$scope.TaskDetails = EntityTaskDetails.Response;
						RefreshExistsingTaskAfterAttachFile($scope.TaskDetails);
					});
				}
			}
		}

		function RefreshExistsingTaskAfterAttachFile(taskObj) {
			if (taskObj.EntityID == $stateParams.ID) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				if (taskListResObj.length > 0) {
					if (taskListResObj[0].TaskList != null) {
						var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskResObj.length > 0) {
							UpdateTaskObject(taskResObj, taskObj)
						} else {
							taskListResObj[0].TaskList.push(taskObj);
						}
					} else {
						taskListResObj[0].TaskList.push(taskObj);
					}
				}
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == taskObj.EntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == taskObj.TaskListID;
					});
					if (taskListGroup.length > 0) {
						var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
							return e.Id == taskObj.Id;
						});
						if (taskObjHolder.length > 0) {
							UpdateTaskObject(taskObjHolder, taskObj)
						}
					}
				}
			}
		}
		$timeout(function() {
			$("#ListHolderID").click(function(event) {
				var targetCtrl = $(event.target);
				if (targetCtrl.attr("data-ExpandCollapse") == "ExpandCollapse") {
					if (targetCtrl.parent().attr("data-isexpandParent") == "false") {
						if (targetCtrl.parent().attr("data-ischildupdated") == "false") {
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
							var appendIcon = "<i class=\'icon-spinner icon-spin icon-large\'></i>";
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").append(appendIcon);
							$scope.srcpageno = 1;
							$scope.RetriveTaskCollection(targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
						} else {
							var group = $.grep($scope.groups, function(e) {
								return e.ID == targetCtrl.attr("data-tasklistid")
							});
							if (group[0].TaskCount != group[0].TaskList.length) {
								$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
								var appendIcon = "<i class=\'icon-spinner icon-spin icon-large\'></i>";
								$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").append(appendIcon);
								$scope.srcpageno = 1;
								$scope.RetriveTaskCollection(targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
							} else {
								$timeout(function() {
									var taskListResObj = $.grep($scope.groups, function(e) {
										return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
									});
									if (taskListResObj.length > 0) {
										taskListResObj[0].IsExpanded = true;
									}
									$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
									$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
									ExpandCollapse(targetCtrl.attr("data-tlid"), true);
								}, 20);
							}
						}
					} else {
						$timeout(function() {
							var taskListResObj = $.grep($scope.groups, function(e) {
								return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
							});
							if (taskListResObj.length > 0) {
								taskListResObj[0].IsExpanded = false;
							}
							var memorytaskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
								return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
							});
							$scope.TaskExpandedDetails.splice($.inArray(memorytaskListResObj[0], $scope.TaskExpandedDetails), 1);
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
							ExpandCollapse(targetCtrl.attr("data-tlid"), false);
						}, 20);
					}
				}
			});
			$("#SubListHolderID").click(function(event) {
				var targetCtrl = $(event.target);
				if (targetCtrl.attr("data-ExpandCollapse") == "ExpandCollapse") {
					if (targetCtrl.parent().attr("data-isexpandParent") == "false") {
						if (targetCtrl.parent().attr("data-ischildupdated") == "false") {
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
							var appendIcon = "<i class=\'icon-spinner icon-spin icon-large\'></i>";
							$("#ListIcon_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").append(appendIcon);
							$scope.srcpageno = 1;
							$scope.RetriveTaskCollection(targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
						} else {
							$timeout(function() {
								var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
									return e.EntityID == parseInt(targetCtrl.attr("data-entityid"));
								});
								if (EntityTaskObj.length > 0) {
									var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
										return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
									});
									if (taskListGroup.length > 0) {
										taskListGroup[0].IsExpanded = true;
									}
								}
								$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
								$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
								ExpandCollapse(targetCtrl.attr("data-tlid"), true);
							}, 20);
						}
					} else {
						$timeout(function() {
							var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
								return e.EntityID == parseInt(targetCtrl.attr("data-entityid"));
							});
							if (EntityTaskObj.length > 0) {
								var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
									return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
								});
								if (taskListGroup.length > 0) {
									taskListGroup[0].IsExpanded = false;
								}
							}
							var memorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
								return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
							});
							$scope.SubLevelTaskExpandedDetails.splice($.inArray(memorytaskListResObj[0], $scope.SubLevelTaskExpandedDetails), 1);
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").html("");
							$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
							ExpandCollapse(targetCtrl.attr("data-tlid"), false);
						}, 20);
					}
				}
			});
		}, 10);
		$scope.srcpageno = 1;

		function OrganizeTaskCollection(ResultTaskList) {
			var taskCollection = [],
				task = {}, tasktypeobj = {}, taskstatusobj = {}, taskmembers = [],
				entitytypedetails = {};
			for (var t = 0, val; val = ResultTaskList[t++];) {
				task = {};
				tasktypeobj = taskTypes.get('' + val.TaskType + '');
				taskstatusobj = TaskStatus.get('' + val.TaskStatus + '');
				task.Name = $('<textarea/>').html(val.name).text(), task.Description = $('<textarea/>').html(val.Description).text();
				task.Id = val.Id, task.TaskTypeID = val.TaskType, task.TaskTypeName = tasktypeobj, task.TaskType = val.TaskType;
				task.TaskListID = val.TaskListID, task.colorcode = "", task.shortdesc = "";
				task.EntityID = val.EntityID, task.TaskStatus = val.TaskStatus;
				task.Note = val.Note != null ? $('<textarea/>').html(val.Note).text() : "-";
				task.EntityTaskListID = val.EntityTaskListID, task.AssetId = val.AssetId;
				if (val.TaskStatus != 8) task.StatusName = taskstatusobj;
				else task.StatusName = val.TaskType == 3 ? "Approved" : "Completed";
				task.SortOrder = val.Sortorder, task.strDate = "", task.TaskCheckList = null, task.DueDate = null, task.totalDueDays = 0;
				if (val.DueDate != null) {
					task.DueDate = val.DueDate;
					if (task.DueDate == ReturnFormattedTodayDate()) task.totalDueDays = 0;
					else {
						if (new Date.create(task.DueDate) > new Date.create()) {
							task.totalDueDays = datediff(task.DueDate, ReturnFormattedTodayDate()) - HolidayCount(new Date.create(task.DueDate));
						} else {
							task.totalDueDays = datediff(task.DueDate, ReturnFormattedTodayDate()) + HolidayCount(new Date.create(task.DueDate));
						}
					}
					task.strDate = val.DueDate;
				}
				taskmembers = [];
				$($.parseXML(val.TypeDetails)).find('p').each(function() {
					task.colorcode = $(this).attr('c'), task.shortdesc = $(this).attr('s');
				});
				$($.parseXML(val.TotaTaskAssignees)).find('p').each(function() {
					taskmembers.push({
						"Id": $(this).attr('id'),
						"TaskID": $(this).attr('TID'),
						"RoleID": $(this).attr('RID'),
						"UserID": $(this).attr('UID'),
						"FlagColorCode": $(this).attr('F'),
						"ApprovalRount": $(this).attr('R'),
						"ApprovalStatus": $(this).attr('S'),
						"UserName": $(this).attr('N'),
						"Role": $(this).attr('RID') == 1 ? "Owner" : "Assignee",
						"UserEmail": $(this).attr('E')
					});
				});
				var taskAssigneesList = $.grep(taskmembers, function(rel) {
					return rel.RoleID != 1;
				});
				if (taskmembers.length > 0) {
					task.taskMembers = taskmembers;
					if (taskAssigneesList.length > 0) {
						var totalTaskmembers = $.grep(taskmembers, function(a) {
							return a.TaskID == val.Id && a.RoleID != 1;
						});
						var currentTaskRount = totalTaskmembers[0].ApprovalRount;
						if (task.TaskType != taskTypesEnum["Work Task"]) {
							task.TotaltaskAssigness = $.grep(taskmembers, function(a) {
								return a.RoleID != 1;
							});
						}
						task.taskAssigness = $.grep(taskAssigneesList, function(a) {
							return a.ApprovalRount == currentTaskRount;
						});
						task.ProgressCount = "";
						if (val.EntityTaskListID == 0) {
							task.taskAssigness = $.grep(taskAssigneesList, function(a) {
								return a.ApprovalRount == currentTaskRount;
							});
							var responsedMembers = $.grep(taskAssigneesList, function(a) {
								return a.ApprovalRount == currentTaskRount && (a.ApprovalStatus != 0);
							}).length;
							if (task.TaskType != taskTypesEnum["Work Task"] && (task.TaskStatus != TaskStatusEnum["Unassigned"] && task.TaskStatus != TaskStatusEnum["Approved"] && task.TaskStatus != TaskStatusEnum["Completed"])) {
								if (val.TaskStatus == 2 || val.TaskStatus == 3 || val.TaskStatus == 4) {
									task.ProgressCount = "";
								} else {
									var resCount = $.grep(taskAssigneesList, function(a) {
										return a.ApprovalRount == currentTaskRount;
									}).length;
									task.ProgressCount = "(" + responsedMembers + "/" + resCount + ")";
								}
							} else {
								if (val.checklists > 0 && task.TaskStatus == TaskStatusEnum["In progress"]) {
									var completedChecklists = val.completedchecklists;
									task.ProgressCount = "(" + completedChecklists + "/" + val.checklists + ")";
								} else {
									task.ProgressCount = "";
								}
							}
							if (taskAssigneesList.length > 1) {
								var result = $.map(taskAssigneesList, function(n, i) {
									return n.UserName;
								}).join(', ');
								task.AssigneeName = result;
								task.AssigneeID = 0;
							} else if (taskAssigneesList.length == 1) {
								task.AssigneeName = taskAssigneesList[0].UserName;
								task.AssigneeID = taskAssigneesList[0].UserID;
							}
						} else {
							task.taskAssigness = null;
							task.AssigneeName = "";
						}
					}
				}
				if (val.checklists > 0 && val.TaskStatus == TaskStatusEnum["Unassigned"] && val.TaskType == taskTypesEnum["Work Task"]) {
					var completedChecklists = val.completedchecklists;
					task.ProgressCount = "(" + completedChecklists + "/" + val.checklists + ")";
				}
				task.AttributeData = null;
				taskCollection.push(task);
			}
			return taskCollection;
		}

		function pagingOnAutoScroll(TaskListID, EntityId) {
			$scope.srcpageno = $scope.srcpageno + 1;
			if (EntityId == $stateParams.ID) {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == TaskListID;
					});
					if (taskListResObj.length > 0) {
						if (taskCollection != undefined) {
							for (var s = 0, task; task = taskCollection[s++];) {
								taskListResObj[0].TaskList.push(task)
							}
							if (taskCollection.length > 0) pagingOnAutoScroll(TaskListID, EntityId);
						} else $scope.srcpageno = 1;
					}
				});
			} else {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == EntityId;
					});
					if (EntityTaskObj.length > 0) {
						var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
							return e.ID == TaskListID;
						});
						if (taskListGroup.length > 0) {
							if (taskCollection != undefined) {
								for (var s = 0, task; task = taskCollection[s++];) {
									taskListGroup[0].TaskList.push(task)
								}
								if (taskCollection.length > 0) pagingOnAutoScroll(TaskListID, EntityId);
							} else $scope.srcpageno = 1;
						}
					}
				});
			}
		}

		function ShowLocalDate() {
			var dNow = new Date.create();
			var localdate = (dNow.getMonth() + 1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
			return localdate;
		}
		$scope.RetriveTaskCollection = function(EntityId, TaskListID) {
			if (EntityId == $stateParams.ID) {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == TaskListID;
					});
					if (taskListResObj.length > 0) {
						taskListResObj[0].TaskList = taskCollection;
						taskListResObj[0].IsGetTasks = true;
						taskListResObj[0].IsExpanded = true;
						if (ResultTaskList.length == 0) $("#ListName_" + TaskListID + "").html("");
						$("#ListName_" + TaskListID + "").empty();
						ExpandCollapse("TL_" + TaskListID, true);
					} else {
						$("#ListName_" + TaskListID + "").html("");
						$("#ListName_" + TaskListID + "").empty();
					}
					var MemorytaskListResObj = [];
					MemorytaskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
						return e.ID == TaskListID;
					});
					if (MemorytaskListResObj.length == 0) {
						var memoryTasklist = {
							ID: TaskListID,
							EntityID: EntityId,
							TaskList: taskCollection
						}
						$scope.TaskExpandedDetails.push(memoryTasklist);
					}
					$timeout(function() {
						if (ResultTaskList.length == 0) $("#ListName_" + TaskListID + "").html("");
						$("#ListName_" + TaskListID + "").empty();
						ExpandCollapse("TL_" + TaskListID, true);
						$(".nestedsorting").each(applySortable);
						$timeout(function() {
							pagingOnAutoScroll(TaskListID, EntityId);
						}, 100);
					}, 10);
				});
			} else {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == EntityId;
					});
					if (EntityTaskObj.length > 0) {
						var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
							return e.ID == TaskListID;
						});
						if (taskListGroup.length > 0) {
							taskListGroup[0].TaskList = taskCollection;
							taskListGroup[0].IsGetTasks = true;
							taskListGroup[0].IsExpanded = true;
							if (ResultTaskList.length == 0) $("#ListName_" + TaskListID + "").html("");
							$("#ListName_" + TaskListID + "").empty();
							$("#ListIcon_" + TaskListID + "").empty();
							ExpandCollapse("TL_" + TaskListID, true);
						}
					} else {
						$("#ListName_" + TaskListID + "").html("");
						$("#ListName_" + TaskListID + "").empty();
					}
					var MemorytaskListResObj = [];
					MemorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
						return e.ID == TaskListID;
					});
					if (MemorytaskListResObj.length == 0) {
						var memoryTasklist = {
							ID: TaskListID,
							EntityID: EntityId,
							TaskList: taskCollection
						}
						$scope.SubLevelTaskExpandedDetails.push(memoryTasklist);
					}
					$timeout(function() {
						if (ResultTaskList.length == 0) $("#ListName_" + TaskListID + "").html("");
						$("#ListName_" + TaskListID + "").empty();
						$("#ListIcon_" + TaskListID + "").empty();
						ExpandCollapse("TL_" + TaskListID, true);
						$(".nestedsorting").each(applySortable);
						$timeout(function() {
							pagingOnAutoScroll(TaskListID, EntityId);
						}, 100);
					}, 10);
				});
			}
		}

		function refreshTaskCountAsperVisibility(eid, tlid) {
			$scope.GetSubTaskCount(tlid, eid);
		}

		function ExpandCollapse(id, isdisplay) {
			if (isdisplay) $("." + id).css("display", "block");
			else $("." + id).css("display", "none");
		}

		function ExpandCollapseSubLevels(id, isdisplay) {
			if (isdisplay) $("." + id).css("display", "block");
			else $("." + id).css("display", "none");
		}
		$scope.ShowHideSettoComplete = function(taskTypeId, taskStatus) {
			if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36 || taskTypeId == 37) {
				if (taskStatus == 0) $("#TM1").show();
				else $("#TM1").hide();
			} else {
				$("#TM1").show();
			}
		}
		$scope.GetTaskFromSubLevel = function() {
			$scope.srcpageno = 1;
			$scope.srcCurrentexpandallpageno = 1;
			$scope.srcSubexpandallpageno = 1;
			if ($scope.SubLevelEnableObject.SublevelTextStatus == 1) {
				$scope.SubLevelEnableObject.Enabled = true;
				$scope.SubLevelEnableObject.SublevelText = $translate.instant('LanguageContents.Res_4958.Caption');
				$scope.SubLevelEnableObject.SublevelTextStatus = 0;
				$scope.SubLevelTaskLibraryList = [];
				$scope.groupbySubLevelTaskObj.splice(0, $scope.groupbySubLevelTaskObj.length);
				CallSublevelTaskList();
			} else {
				$scope.SubLevelTaskLibraryListContainer.SubLevelTaskLibraryList = [];
				$scope.groupbySubLevelTaskObj.splice(0, $scope.groupbySubLevelTaskObj.length);
				$scope.SubLevelTaskLibraryList = [];
				$scope.SubLevelEnableObject.Enabled = false;
				$scope.SubLevelTaskExpandedDetails.splice(0, $scope.SubLevelTaskExpandedDetails.length);
				$scope.SubLevelEnableObject.SublevelText = $translate.instant('LanguageContents.Res_4959.Caption');
				$scope.SubLevelEnableObject.SublevelTextStatus = 1;
			}
		}

		function CallSublevelTaskList() {
			TaskNewService.GetSublevelTaskList($stateParams.ID).then(function(TaskListLibraryData) {
				$scope.SubLevelTaskLibraryList = TaskListLibraryData.Response;
				$scope.SubLevelTaskLibraryListContainer.SubLevelTaskLibraryList = TaskListLibraryData.Response;
				$scope.groupbySubLevelTask('EntityUniqueKey', 'All', 0);
				$timeout(function() {
					$scope.ApplySubTaskListCountDetails();
					$(".nestedsorting").each(applySortable);
				}, 10);
			});
		}

		function sortOnSubLevel(collection, name, SortOrder) {
			collection.sort(function(a, b) {
				var prevLen = a[name].split('.').length;
				var CurreLen = b[name].split('.').length;
				if (a[name] <= b[name] && a[SortOrder] <= b[SortOrder]) {
					return (-1);
				}
				return (1);
			});
		}
		$scope.groupbySubLevelTask = function(attribute, ISglobal, taskStatus) {
			$scope.groupbySubLevelTaskObj.splice(0, $scope.groupbySubLevelTaskObj.length);
			for (var i = 0, friend; friend = $scope.SubLevelTaskLibraryList[i++];) {
				var taskListGroup = friend.TaskListGroup;
				if (ISglobal != "All") {
					var Entitygroup = {
						EntityID: friend.EntityID,
						EntityName: friend.EntityName,
						EntityTypeColorCode: friend.EntityTypeColorCode,
						EntityTypeID: friend.EntityTypeID,
						EntityTypeShortDescription: friend.EntityTypeShortDescription,
						EntityUniqueKey: friend.EntityUniqueKey,
						SortOrder: friend.SortOrder,
						TaskListGroup: []
					}
					for (var j = 0; j < taskListGroup.length; j++) {
						var taskContainer = taskListGroup[j].TaskList;
						var group = {
							LibraryName: taskListGroup[j].LibraryName,
							LibraryDescription: taskListGroup[j].LibraryDescription,
							ID: taskListGroup[j].ID,
							SortOrder: taskListGroup[j].SortOrder,
							TaskCount: taskListGroup[j].TaskCount,
							IsGetTasks: false,
							IsExpanded: false,
							TaskList: []
						}
						for (var k = 0; k < taskContainer.length; k++) {
							if (taskContainer[k]["TaskStatus"] === taskStatus) {
								group.TaskList.push(taskContainer[k]);
							}
						}
						Entitygroup.TaskListGroup.push(group);
					}
					$scope.groupbySubLevelTaskObj.push(Entitygroup);
				} else if (ISglobal == "All") {
					var Entitygroup = {
						EntityID: friend.EntityID,
						EntityName: friend.EntityName,
						EntityTypeColorCode: friend.EntityTypeColorCode,
						EntityTypeID: friend.EntityTypeID,
						EntityTypeShortDescription: friend.EntityTypeShortDescription,
						EntityUniqueKey: friend.EntityUniqueKey,
						SortOrder: friend.SortOrder,
						TaskListGroup: []
					}
					Entitygroup.TaskListGroup = taskListGroup;
					$scope.groupbySubLevelTaskObj.push(Entitygroup);
				}
			}
		}

		function GroupSubLevelTaskList(attribute, ISglobal, taskStatus) {
			$scope.groupbySubLevelTaskObj.splice(0, $scope.groupbySubLevelTaskObj.length);
			for (var i = 0, friend; friend = $scope.SubLevelTaskLibraryList[i++];) {
				var taskListGroup = friend.TaskListGroup;
				if (ISglobal != "All") {
					var Entitygroup = {
						EntityID: friend.EntityID,
						EntityName: friend.EntityName,
						EntityTypeColorCode: friend.EntityTypeColorCode,
						EntityTypeID: friend.EntityTypeID,
						EntityTypeShortDescription: friend.EntityTypeShortDescription,
						EntityUniqueKey: friend.EntityUniqueKey,
						SortOrder: friend.SortOrder,
						TaskListGroup: []
					}
					for (var j = 0; j < taskListGroup.length; j++) {
						var taskContainer = taskListGroup[j].TaskList;
						var group = {
							LibraryName: taskListGroup[j].LibraryName,
							LibraryDescription: taskListGroup[j].LibraryDescription,
							ID: taskListGroup[j].ID,
							SortOrder: taskListGroup[j].SortOrder,
							TaskCount: taskListGroup[j].TaskCount,
							IsGetTasks: false,
							IsExpanded: false,
							TaskList: []
						}
						for (var k = 0; k < taskContainer.length; k++) {
							if (taskContainer[k]["TaskStatus"] === taskStatus) {
								group.TaskList.push(taskContainer[k]);
							}
						}
						Entitygroup.TaskListGroup.push(group);
					}
					$scope.groupbySubLevelTaskObj.push(Entitygroup);
				} else if (ISglobal == "All") {
					var Entitygroup = {
						EntityID: friend.EntityID,
						EntityName: friend.EntityName,
						EntityTypeColorCode: friend.EntityTypeColorCode,
						EntityTypeID: friend.EntityTypeID,
						EntityTypeShortDescription: friend.EntityTypeShortDescription,
						EntityUniqueKey: friend.EntityUniqueKey,
						SortOrder: friend.SortOrder,
						TaskListGroup: []
					}
					Entitygroup.TaskListGroup = taskListGroup;
					$scope.groupbySubLevelTaskObj.push(Entitygroup);
				}
			}
			$timeout(function() {
				$scope.SubLevelExpandManually();
			}, 10);
			$timeout(function() {
				$scope.ApplySubTaskListCountDetails();
				$(".nestedsorting").each(applySortable);
			}, 10);
		}
		$scope.addNewTaskfromSublevel = function(TaskListID, EntityID) {
			$scope.TaskActionFor = 2;
			$scope.SelectedTaskLIstID = TaskListID;
			$scope.TaskGlobalEntityID = EntityID;
			RefreshAddNewTaskScopes();
		};
		$scope.OpenSublevelTaskPopUpAction = function(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, EntityID) {
			$scope.TaskGlobalEntityID = EntityID;
			$scope.MainSelectedTaskListId = TaskObject.TaskListID;
			$scope.TaskActionFor = 2;
			PopulateBasicTaskDetails(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, $scope.TaskGlobalEntityID);
			GetEntityLocationPath(taskID);
			GetentityMembers($scope.TaskGlobalEntityID);
			GetTaskMinMaxValue(taskTypeId);
		}

		function GetEntityLocationPath(EntityIDVal) {
			TaskNewService.GetPath(EntityIDVal).then(function(getbc) {
				$scope.array = [];
				if (getbc != null && getbc != undefined) {
					for (var i = 0; i < getbc.Response.length - 1; i++) {
						$scope.array.push({
							"ID": getbc.Response[i].ID,
							"Name": getbc.Response[i].Name,
							"UniqueKey": getbc.Response[i].UniqueKey,
							"ColorCode": getbc.Response[i].ColorCode,
							"ShortDescription": getbc.Response[i].ShortDescription,
							"TypeID": getbc.Response[i].TypeID,
							"mystyle": "0088CC",
							"EntityTypeID": getbc.Response[i].EntityTypeID
						});
					}
				}
			});
		}
		$scope.TaskMinMaxList = [];

		function GetTaskMinMaxValue(tasktypeid) {
			TaskNewService.GetTaskMinMaxValue(tasktypeid).then(function(TaskMinMaxListres) {
				$scope.TaskMinMaxList = TaskMinMaxListres.Response;
			});
		}
		$scope.showattachbtninassetapproval = true;

		function PopulateBasicTaskDetails(taskID, IsAdminTask, taskTypeId, TaskObject, taskStatus, EntityID) {
			$scope.showattachbtninassetapproval = true;
			$scope.approvalflowTempPopup = true;
			GetEntityApprovalTaskTemplatedetails(TaskObject.EntityTaskListID);
			$scope.tempTaskID = parseInt(taskID);
			$scope.editable = '';
			GetTaskMinMaxValue(taskTypeId);
			$scope.proofUrl = $sce.trustAsResourceUrl("");
			$scope.dalimUrl = $sce.trustAsResourceUrl("");
			$scope.DisableIsAdminTask = true;
			if (NewsFeedUniqueTimerForTask != undefined) $timeout.cancel(NewsFeedUniqueTimerForTask);
			$('#task').on('hide.bs.modal', function() {
				if (NewsFeedUniqueTimerForTask != undefined) $timeout.cancel(NewsFeedUniqueTimerForTask);
			});
			if (IsAdminTask == 0 && ((taskStatus > 0 && taskStatus < 7) || (taskStatus == 8))) {
				var newsFeedDiv;
				if (taskTypeId == 2) {
					MaxMinPopulate(taskTypeId);
					refreshModel();
					BindFundRequestTaskDetails(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "Taskfeeddivforworktask";
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$("#WorkTask").modal("show");
					var commentbuttinid = "Taskcommentwork";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'taskfeedtextholderwork';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.tempTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "worktask";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				} else if (taskTypeId == 3 || taskTypeId == 32) {
					refreshModel();
					MaxMinPopulate(taskTypeId);
					BindFundRequestTaskDetails(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "Taskfeeddivforapprovaltask";
					$scope.showattachbtninassetapproval = (taskTypeId == 32) ? false : true;
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$("#ApprovalTask").modal("show");
					var commentbuttinid = "ApprovalTaskcomment";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'ApprovalTaskfeedtextholder';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.tempTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "approvaltask";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				} else if (taskTypeId == 36) {
					refreshModel();
					BindFundRequestTaskDetails(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "TaskfeeddivforProofHQapprovaltask";
					$scope.showattachbtninassetapproval = (taskTypeId == 32) ? false : true;
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$('#proofHQTabs li a:first').tab('show');
					var urlstring = "proof.aspx?tid=" + $scope.TaskBriefDetails.taskID + "&uid=" + $scope.OwnerID + "&oid=" + $scope.TaskBriefDetails.ownerId + "&uname=" + $scope.ownerEmail + "";
					$scope.proofUrl = $sce.trustAsResourceUrl(urlstring);
					$('#task-proofHQ-iframe').load(function() {
						$('#loadproofIframeImg').css('display', 'none');
						$('#task-proofHQ-iframe').css('visibility', 'visible');
					});
					$timeout(function() {
						$("#ProofHqTask").modal("show");
					}, 10);
					var commentbuttinid = "ProofHQApprovalTaskcomment";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'ProofHQApprovalTaskfeedtextholder';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.tempTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "ProofHQapprovaltask";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				} else if (taskTypeId == 37) {
					refreshModel();
					BindFundRequestTaskDetails(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "Taskfeeddivfordalimapprovaltask";
					$scope.showattachbtninassetapproval = (taskTypeId == 32) ? false : true;
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$('#dalimTabs li a:first').tab('show');
					var urlstring = "dalim.aspx?tid=" + $scope.TaskBriefDetails.taskID + "&uid=" + $scope.OwnerID + "&oid=" + $scope.TaskBriefDetails.ownerId + "&uname=" + $scope.ownerEmail + "";
					$scope.dalimUrl = $sce.trustAsResourceUrl(urlstring);
					$('#task-dalim-iframe').load(function() {
						$('#loaddalimproofIframeImg').css('display', 'none');
						$('#task-dalim-iframe').css('visibility', 'visible');
					});
					$timeout(function() {
						$("#dalimTask").modal("show");
					}, 10);
					var commentbuttinid = "dalimApprovalTaskcomment";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'dalimapprovaltaskfeedtextholder';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.tempTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "dalimapprovaltask";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				} else if (taskTypeId == 31) {
					refreshModel();
					BindFundRequestTaskDetails(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "feeddivforreviewTask";
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$("#ReviewTask").modal("show");
					var commentbuttinid = "reviewTaskcomment";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'reviewtextholder';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.tempTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "reveiewtask";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				}
			} else if (taskStatus == 0) {
				if (TaskObject.taskAssigness != null) {
					if (TaskObject.taskAssigness.length > 0) {
						if (taskTypeId == 2) {
							refreshModel();
							BindFundRequestTaskDetails(taskTypeId, TaskObject);
							$scope.SelectedTaskID = taskID;
							$scope.divid = "Taskfeeddivforworktask";
							feedforApprovalTask(parseInt(taskID), $scope.divid, false);
							$("#WorkTask").modal("show");
							var commentbuttinid = "Taskcommentwork";
							$('#' + commentbuttinid).empty();
							var temp = '';
							if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
								temp = 'taskfeedtextholderwork';
								document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
							}
							$scope.processingsrcobj.processingid = $scope.SelectedTaskID;
							$scope.processingsrcobj.processingplace = "task";
							$timeout(function() {
								$scope.AssetfileTemplateLoading = "worktask";
								$scope.$broadcast('ReloadAssetView');
							}, 100);
						} else if (taskTypeId == 3 || taskTypeId == 32) {
							refreshModel();
							BindFundRequestTaskDetails(taskTypeId, TaskObject);
							$scope.SelectedTaskID = taskID;
							$scope.divid = "Taskfeeddivforapprovaltask";
							if (taskTypeId == 32) {}
							feedforApprovalTask(parseInt(taskID), $scope.divid, false);
							$("#ApprovalTask").modal("show");
							var commentbuttinid = "ApprovalTaskcomment";
							$('#' + commentbuttinid).empty();
							var temp = '';
							if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
								temp = 'ApprovalTaskfeedtextholder';
								document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
							}
							$scope.processingsrcobj.processingid = $scope.SelectedTaskID;
							$scope.processingsrcobj.processingplace = "task";
							$timeout(function() {
								$scope.AssetfileTemplateLoading = "approvaltask";
								$scope.$broadcast('ReloadAssetView');
							}, 100);
						} else if (taskTypeId == 36) {
							refreshModel();
							BindFundRequestTaskDetails(taskTypeId, TaskObject);
							$scope.SelectedTaskID = taskID;
							$scope.divid = "TaskfeeddivforProofHQapprovaltask";
							$scope.showattachbtninassetapproval = (taskTypeId == 32) ? false : true;
							feedforApprovalTask(parseInt(taskID), $scope.divid, false);
							$("#ProofHqTask").modal("show");
							var commentbuttinid = "ProofHQApprovalTaskcomment";
							$('#' + commentbuttinid).empty();
							var temp = '';
							if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
								temp = 'ProofHQApprovalTaskfeedtextholder';
								document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
							}
							$scope.processingsrcobj.processingid = $scope.tempTaskID;
							$scope.processingsrcobj.processingplace = "task";
							$timeout(function() {
								$scope.AssetfileTemplateLoading = "ProofHQapprovaltask";
								$scope.$broadcast('ReloadAssetView');
							}, 100);
						} else if (taskTypeId == 37) {
							refreshModel();
							BindFundRequestTaskDetails(taskTypeId, TaskObject);
							$scope.SelectedTaskID = taskID;
							$scope.divid = "Taskfeeddivfordalimapprovaltask";
							$scope.showattachbtninassetapproval = (taskTypeId == 32) ? false : true;
							feedforApprovalTask(parseInt(taskID), $scope.divid, false);
							$("#dalimTask").modal("show");
							var commentbuttinid = "dalimApprovalTaskcomment";
							$('#' + commentbuttinid).empty();
							var temp = '';
							if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
								temp = 'dalimapprovaltaskfeedtextholder';
								document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
							}
							$scope.processingsrcobj.processingid = $scope.tempTaskID;
							$scope.processingsrcobj.processingplace = "task";
							$timeout(function() {
								$scope.AssetfileTemplateLoading = "dalimApprovalTask";
								$scope.$broadcast('ReloadAssetView');
							}, 100);
						} else if (taskTypeId == 31) {
							refreshModel();
							BindFundRequestTaskDetails(taskTypeId, TaskObject);
							$scope.SelectedTaskID = taskID;
							$scope.divid = "feeddivforreviewTask";
							feedforApprovalTask(parseInt(taskID), $scope.divid, false);
							$("#ReviewTask").modal("show");
							var commentbuttinid = "reviewTaskcomment";
							$('#' + commentbuttinid).empty();
							var temp = '';
							if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
								temp = 'reviewtextholder';
								document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
							}
							$scope.processingsrcobj.processingid = $scope.SelectedTaskID;
							$scope.processingsrcobj.processingplace = "task";
							$timeout(function() {
								$scope.AssetfileTemplateLoading = "reveiewtask";
								$scope.$broadcast('ReloadAssetView');
							}, 100);
						}
					}
				} else {
					refreshModel();
					LoadUnassignedTaskDetl(taskTypeId, TaskObject);
					$scope.SelectedTaskID = taskID;
					$scope.divid = "feeddivforunassignedassigned";
					feedforApprovalTask(parseInt(taskID), $scope.divid, false);
					$("#UnassignedTask").modal("show");
					var commentbuttinid = "feedcommentunassigned";
					$('#' + commentbuttinid).empty();
					var temp = '';
					if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
						temp = 'feedtextholderunassigned';
						document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
					}
					$scope.processingsrcobj.processingid = $scope.SelectedTaskID;
					$scope.processingsrcobj.processingplace = "task";
					$timeout(function() {
						$scope.AssetfileTemplateLoading = "unassigned";
						$scope.$broadcast('ReloadAssetView');
					}, 100);
				}
			} else if (taskStatus == 7) {
				bootbox.alert($translate.instant('LanguageContents.Res_1971.Caption'));
			}
			$timeout(function() {
				$scope.Timerforcallback(parseInt($scope.SelectedTaskID), $scope.divid, true);
			}, 30000);
		}
		$scope.taskUnassignTypeID = 0;

		function LoadUnassignedTaskDetl(taskTypeId, TaskObject) {
			MaxMinPopulate(taskTypeId);
			$scope.taskUnassignTypeID = taskTypeId;
			$scope.TaskTypeName = "";
			if ($scope.TaskTypeList != undefined) {
				var TaskTypeObj = $.grep($scope.TaskTypeList, function(n, i) {
					return $scope.TaskTypeList[i].Id == TaskObject.TaskTypeID;
				});
				if (TaskTypeObj != null && TaskTypeObj.length > 0) {
					$scope.TaskTypeName = TaskTypeObj[0].Caption;
				}
			}
			$scope.ShowCompleteBtn = false;
			$scope.ShowApproveBtn = false;
			$scope.ShowRejectedBtn = false;
			$scope.ShowWithdrawBtn = false;
			$scope.ShowUnabletoCompleteBtn = false;
			$scope.ShowRevokeButton = false;
			$("#ReviewTask,#ApprovalTask,#WorkTask,#UnassignedTask,#ProofHqTask").removeClass(cssToRemove);
			if (TaskObject != null) {
				$("#ReviewTask,#ApprovalTask,#WorkTask,#UnassignedTask,#ProofHqTask").addClass(TaskObject.StatusName.replace(/\s+/g, ""));
				cssToRemove = TaskObject.StatusName.replace(/\s+/g, "");
			}
			$scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
			var isThisMemberPresent = 0;
			if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
				isThisMemberPresent = $.grep(TaskObject.taskAssigness, function(e) {
					return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1);
				});
			}
			var taskOwnerObj = null;
			if (TaskObject.taskMembers != null && TaskObject.taskMembers.length > 0) {
				taskOwnerObj = $.grep(TaskObject.taskMembers, function(e) {
					return (e.RoleID == 1);
				})[0];
			}
			if (taskOwnerObj != null && taskOwnerObj.length > 0) {
				if (taskTypeId == 2) {
					if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
						if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							$scope.ShowWithdrawBtn = false;
						}
					}
					if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
						if ($scope.EntityLockTask != true) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 0) {
									$scope.ShowCompleteBtn = true;
									$scope.ShowUnabletoCompleteBtn = true;
								}
							}
						}
					}
					if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {
						if ($scope.EntityLockTask != true) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 0) {
									$scope.ShowCompleteBtn = true;
									$scope.ShowUnabletoCompleteBtn = true;
								}
							}
						}
					}
					if (TaskObject.TaskStatus == 2 && isThisMemberPresent.length >= 1) {
						if ($scope.EntityLockTask != true) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 0) {
									$scope.ShowCompleteBtn = true;
									$scope.ShowUnabletoCompleteBtn = true;
								}
							}
						}
					}
				} else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36 || taskTypeId == 37) {
					if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
						if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							$scope.ShowWithdrawBtn = false;
						}
					}
					if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
						if ($scope.EntityLockTask != true) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 0) {
									$scope.ShowApproveBtn = true;
									$scope.ShowRejectedBtn = true;
									$scope.ShowUnabletoCompleteBtn = true;
								}
							}
						}
					}
				} else if (taskTypeId == 31) {
					if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
						if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
							$scope.ShowWithdrawBtn = false;
						}
					}
					if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
						if ($scope.EntityLockTask != true) {
							if (isThisMemberPresent.length == 1) {
								if (isThisMemberPresent[0].ApprovalStatus == 0) {
									$scope.ShowApproveBtn = false;
									$scope.ShowCompleteBtn = true;
									$scope.ShowRejectedBtn = false;
									$scope.ShowUnabletoCompleteBtn = true;
								}
							}
						}
					}
				}
			}
			$scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
			$scope.TaskBriefDetails.taskName = TaskObject.Name;
			$scope.TaskBriefDetails.taskTypeId = taskTypeId;
			$scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
			$scope.TaskBriefDetails.dueDate = '-';
			if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
				$scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat);
			}
			$scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
			$scope.TaskBriefDetails.status = TaskObject.StatusName;
			$scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
			$scope.TaskBriefDetails.Description = '-';
			$scope.TaskBriefDetails.Note = '-';
			if (TaskObject.Description.length > 0) {
				$scope.TaskBriefDetails.Description = TaskObject.Description;
			}
			if (TaskObject.Note != null) if (TaskObject.Note.length > 0) {
				$scope.TaskBriefDetails.Note = TaskObject.Note;
			}
			$scope.TaskBriefDetails.taskID = TaskObject.Id;
			$scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
			$scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
			$scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
			var unresponsedMembers = null;
			if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
				unresponsedMembers = $.grep(TaskObject.taskAssigness, function(e) {
					return (e.ApprovalStatus != 0);
				});
			}
			if (TaskObject.TaskType == 2 && TaskObject.TaskStatus == 0) {
				$scope.TaskBriefDetails.taskProgressCount = TaskObject.ProgressCount;
			}
			if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.taskTypeId != 2) {
				$scope.TaskBriefDetails.taskProgressCount = "";
			} else {
				if (unresponsedMembers != null && unresponsedMembers.length > 0) {
					$scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";
				}
			}
			$scope.TaskMemberList = TaskObject.taskAssigness;
			$scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
			$scope.name = $scope.TaskBriefDetails.taskName;
			$scope.DescriptionChange = $scope.TaskBriefDetails.Description;
			$scope.dueDate = "-";
			if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
				$scope.dueDate = TaskObject.strDate;
				$scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
			}
			if ($scope.taskUnassignTypeID == 2) {
				GetTaskCheckLists();
			}
			$scope.fromAssignedpopup = false;
			GetEntityAttributesDetails($scope.TaskBriefDetails.taskID);
			ReloadTaskAttachments(TaskObject.Id);
		}
		$scope.addUnAssignAdditionalMember = function() {}
		$scope.AddUnAssignedTaskAdditionalMembers = function() {
			$scope.fromunassignedtask = true;
			var ownerlist = null;
			if ($('#ngAddUnassignTaskMember').hasClass('disabled')) {
				return;
			}
			$('#ngAddUnassignTaskMember').addClass('disabled');
			if ($scope.TaskBriefDetails.ownerId == 0) {
				ownerlist = $scope.OwnerList;
			}
			var memberList = [];
			memberList = GetUserSelectedAll();
			if (memberList.length > 0 || $scope.globalEntityMemberLists.length > 0) {
				var insertMemberTask = {};
				insertMemberTask.ParentId = $scope.TaskGlobalEntityID;
				insertMemberTask.TaskID = $scope.TaskBriefDetails.taskID;
				insertMemberTask.TaskMembers = memberList;
				insertMemberTask.ownerlist = ownerlist;
				insertMemberTask.GlobalTaskMembers = $scope.globalEntityMemberLists;
				TaskNewService.InsertUnAssignedTaskMembers(insertMemberTask).then(function(SaveTaskMemberResult) {
					if (SaveTaskMemberResult.StatusCode == 405) {
						NotifyError($translate.instant('LanguageContents.Res_4267.Caption'));
						$('#ngAddUnassignTaskMember').removeClass('disabled');
					} else {
						$scope.globalEntityMemberLists = [];
						$('#ngAddUnassignTaskMember').removeClass('disabled');
						var taskListResObj = {};
						var taskResObj = {};
						if ($scope.targetList.IsParentEntity != "false") {
							taskListResObj = $.grep($scope.groups, function(e) {
								return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
							});
							taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
								return e.Id == $scope.TaskBriefDetails.taskID;
							});
							if (taskResObj.length > 0) {
								var taskMemberObj = SaveTaskMemberResult.Response.m_Item2;
								taskResObj[0].taskMembers = taskMemberObj;
								var currentTaskRound = taskMemberObj[0].ApprovalRount;
								var taskAssignessObj = $.grep(taskMemberObj, function(e) {
									return e.RoleID > 1 && e.ApprovalRount == currentTaskRound;
								});
								taskResObj[0].taskAssigness = taskAssignessObj;
								$scope.TaskBriefDetails.taskmembersList = taskAssignessObj;
								if (taskMemberObj.length > 0) {
									$scope.TaskBriefDetails.totalTaskMembers = taskMemberObj;
								}
								$scope.TaskMemberList = taskAssignessObj;
								RefreshCurrentTask();
							}
						} else {
							var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
								return e.EntityID == $scope.targetList.entityid;
							});
							taskListResObj = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
								return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
							});
							taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
								return e.Id == $scope.TaskBriefDetails.taskID;
							});
							if (taskResObj.length > 0) {
								var taskMemberObj = SaveTaskMemberResult.Response.m_Item2;
								taskResObj[0].taskMembers = taskMemberObj;
								var currentTaskRound = taskMemberObj[0].ApprovalRount;
								var taskAssignessObj = $.grep(taskMemberObj, function(e) {
									return e.RoleID > 1 && e.ApprovalRount == currentTaskRound;
								});
								taskResObj[0].taskAssigness = taskAssignessObj;
								$scope.TaskBriefDetails.taskmembersList = taskAssignessObj;
								if (taskMemberObj.length > 0) {
									$scope.TaskBriefDetails.totalTaskMembers = taskMemberObj;
								}
								$scope.TaskMemberList = taskAssignessObj;
								RefreshSubEntityCurrentTask();
							}
						}
						if ($scope.TaskBriefDetails.ownerId == 0) {
							$scope.TaskBriefDetails.taskOwner = $scope.OwnerList[0].UserName;
							$scope.TaskBriefDetails.ownerId = $scope.OwnerList[0].Userid;
						}
						GetentityMembers($scope.TaskGlobalEntityID);
						SeperateUsers();
						$timeout(function() {
							feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
						}, 2000);
						$('#AdditionalTaskMembers > tbody input:checked').each(function() {
							$(this).next('i').removeClass('checked');
						});
						NotifySuccess($translate.instant('LanguageContents.Res_4108.Caption'));
						$('#AddTaskMemberModal').modal('hide');
					}
				});
			} else {
				$('#ngAddUnassignTaskMember').removeClass('disabled');
			}
		}

		function RefreshSubEntityCurrentTask() {
			$scope.TaskDetails = [];
			TaskNewService.GetEntityTaskDetails($scope.TaskBriefDetails.taskID).then(function(EntityTaskDetails) {
				$scope.TaskDetails = EntityTaskDetails.Response;
				RefreshUnassignedSubEntityTaskInObject($scope.TaskDetails);
				if ($scope.TaskDetails != null) if ($scope.TaskDetails.TaskStatus != 0) BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				else {
					LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				}
			});
		}

		function MaxMinPopulate(typeid) {
			$scope.MinValue = parseInt(($scope.TaskMinMaxList[0] == undefined ? 0 : $scope.TaskMinMaxList[0].MinValue));
			$scope.MaxValue = parseInt(($scope.TaskMinMaxList[0] == undefined ? 0 : $scope.TaskMinMaxList[0].MaxValue));
			$scope.TaskDueDateMinValue = new Date.create();
			$scope.TaskDueDateMaxValue = new Date.create();
			if ($scope.MinValue < 0) {
				$scope.TaskDueDateMinValue.setDate($scope.TaskDueDateMinValue.getDate() + ($scope.MinValue + 1));
			} else {
				$scope.TaskDueDateMinValue.setDate($scope.TaskDueDateMinValue.getDate() + ($scope.MinValue));
			}
			if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
				$scope.TaskDueDateMaxValue.setDate($scope.TaskDueDateMaxValue.getDate() + ($scope.MaxValue - 1));
			} else {
				$scope.TaskDueDateMaxValue.setDate($scope.TaskDueDateMaxValue.getDate() + 100000);
			}
		}

		function RefreshUnassignedSubEntityTaskInObject(taskObj) {
			var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
				return e.EntityID == taskObj.EntityID;
			});
			if (EntityTaskObj.length > 0) {
				var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
					return e.ID == taskObj.TaskListID;
				});
				if (taskListGroup.length > 0) {
					var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
						return e.Id == taskObj.Id;
					});
					if (taskObjHolder.length > 0) {
						UpdateTaskObject(taskObjHolder, taskObj)
					}
				}
			}
		}

		function LoadUnassigneeMembers() {
			$scope.TaskMemberList = [];
			var UniqueTaskMembers = [];
			var dupes = {};
			if ($scope.TaskMemberList != null && $scope.TaskMemberList.length > 0) {
				$.each($scope.TaskMemberList, function(i, el) {
					if (!dupes[el.UserID]) {
						dupes[el.UserID] = true;
						UniqueTaskMembers.push(el);
					}
				});
			}
			var UniqueEntityMembers = [];
			dupes = {};
			if ($scope.EntityMemberList != null && $scope.EntityMemberList.length > 0) {
				$.each($scope.EntityMemberList, function(i, el) {
					if (el.IsInherited != true) {
						if (!dupes[el.Userid]) {
							dupes[el.Userid] = true;
							UniqueEntityMembers.push(el);
						}
					}
				});
			}
			$scope.RemainMembers = [];
			if (UniqueEntityMembers != null && UniqueEntityMembers.length > 0) {
				$.each(UniqueEntityMembers, function(key, value) {
					var MemberList = $.grep(UniqueTaskMembers, function(e) {
						return e.UserID == value.Userid;
					});
					if (MemberList.length == 0) {
						$scope.RemainMembers.push(value);
					}
				});
			}
		}
		$scope.addNewTaskfromEntity = function(MainSelectedTaskListId, copyObj) {
			$scope.SelectedTaskLIstID = MainSelectedTaskListId;
			$('#WorkTask').modal('hide');
			$('#ApprovalTask').modal('hide');
			$('#ReviewTask').modal('hide');
			$('#UnassignedTask').modal('hide');
			RefreshAddNewTaskScopes();
			var file = $.grep(copyObj, function(e) {
				return e.Id == $scope.contextFileID;
			});
			for (var i = 0; i < file.length; i++) {
				if (file[i].Extension != "Link") $scope.FileList.push({
					"Name": file[i].Name,
					"VersionNo": 1,
					"MimeType": file[i].MimeType,
					"Extension": file[i].Extension,
					"OwnerID": $cookies['UserId'],
					"CreatedOn": new Date.create(),
					"Checksum": "",
					"ModuleID": 1,
					"EntityID": $scope.TaskGlobalEntityID,
					"Size": file[i].Size,
					"FileGuid": file[i].Fileguid,
					"FileDescription": file[i].Description,
					"IsExists": true
				});
				$scope.AttachmentFilename.push({
					"FileName": file[i].Name,
					"Extension": file[i].Extension,
					"Size": parseSize(file[i].Size),
					"FileGuid": file[i].Fileguid,
					"FileDescription": file[i].Description,
					"URL": file[i].LinkURL.replace("http://", "").replace("https://", "").replace("ftp://", "").replace("file://", ""),
					"LinkType": file[i].LinkType,
					"ID": 0,
					"LinkID": 0
				});
			}
		};
		$scope.ListofTasks = [];
		$scope.FillListoftasks = function() {
			$scope.ListofTasks = [];
			if ($scope.TaskBriefDetails.EntityID == $stateParams.ID) {
				var taskListResObj = $scope.groups;
				var Entitygroup = {
					EntityID: $stateParams.ID,
					EntityName: $scope.RootLevelEntityName,
					EntityTypeColorCode: $scope.EntityColorcode,
					EntityTypeShortDescription: $scope.EntityShortText,
					TaskListGroup: []
				}
				TaskNewService.GetExistingEntityTasksByEntityID($scope.TaskBriefDetails.EntityID).then(function(TaskListLibraryData) {
					var TaskLibraryList = TaskListLibraryData.Response;
					for (var k = 0; k < TaskLibraryList.length; k++) {
						var group = {
							LibraryName: taskListResObj[k].LibraryName,
							LibraryDescription: taskListResObj[k].LibraryDescription,
							ID: taskListResObj[k].ID,
							SortOrder: taskListResObj[k].SortOrder,
							TaskList: []
						}
						var taskResObj = TaskLibraryList[k].TaskList;
						for (var m = 0; m < taskResObj.length; m++) {
							if (taskResObj[m].Id != $scope.TaskBriefDetails.taskID) {
								group.TaskList.push({
									"TaskListName": taskListResObj[k].LibraryName,
									"TaskListID": taskListResObj[k].ID,
									"TaskName": taskResObj[m].Name,
									"TaskID": taskResObj[m].Id,
									"EntityID": taskResObj[m].EntityID,
									"taskSortOrder": taskResObj[m].SortOrder,
									"tasklistSortOrder": taskListResObj[k].SortOrder
								});
							}
						}
						Entitygroup.TaskListGroup.push(group);
					}
					$scope.ListofTasks.push(Entitygroup);
					$("#ExistingTaskPopupHolder").scrollTop(0);
					$("#AddtoExistingTaskPopup").modal("show");
				});
			} else {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.TaskBriefDetails.EntityID;
				});
				if (EntityTaskObj.length > 0) {
					var Entitygroup = {
						EntityID: EntityTaskObj[0].EntityID,
						EntityName: EntityTaskObj[0].EntityName,
						EntityTypeColorCode: EntityTaskObj[0].EntityTypeColorCode,
						EntityTypeShortDescription: EntityTaskObj[0].EntityTypeShortDescription,
						TaskListGroup: []
					}
					TaskNewService.GetExistingEntityTasksByEntityID($scope.TaskBriefDetails.EntityID).then(function(TaskListLibraryData) {
						var TaskLibraryList = TaskListLibraryData.Response;
						for (var k = 0; k < TaskLibraryList.length; k++) {
							var group = {
								LibraryName: taskListResObj[k].LibraryName,
								LibraryDescription: taskListResObj[k].LibraryDescription,
								ID: taskListResObj[k].ID,
								SortOrder: taskListResObj[k].SortOrder,
								TaskList: []
							}
							var taskResObj = TaskLibraryList[k].TaskList;
							for (var m = 0; m < taskResObj.length; m++) {
								if (taskResObj[m].Id != $scope.TaskBriefDetails.taskID) {
									group.TaskList.push({
										"TaskListName": taskListResObj[k].LibraryName,
										"TaskListID": taskListResObj[k].ID,
										"TaskName": taskResObj[m].Name,
										"TaskID": taskResObj[m].Id,
										"EntityID": taskResObj[m].EntityID,
										"taskSortOrder": taskResObj[m].SortOrder,
										"tasklistSortOrder": taskListResObj[k].SortOrder
									});
								}
							}
							Entitygroup.TaskListGroup.push(group);
						}
						$scope.ListofTasks.push(Entitygroup);
						$("#ExistingTaskPopupHolder").scrollTop(0);
						$("#AddtoExistingTaskPopup").modal("show");
					});
				}
			}
		};
		$scope.SelectedListofTasks = [];
		$scope.GetSelectedTasks = function(Attachment, $event) {
			var checkbox = $event.target;
			if (checkbox.checked) {
				$scope.SelectedListofTasks.push({
					"EntityID": Attachment.EntityID,
					"TaskID": Attachment.TaskID,
					"TaskListID": Attachment.TaskListID
				});
			} else {
				var fileObj = $.grep($scope.SelectedListofTasks, function(e) {
					return e.TaskID == Attachment.TaskID;
				});
				$scope.SelectedListofTasks.splice($.inArray(fileObj[0], $scope.SelectedListofTasks), 1);
			}
		};

		function GroupingExisitngTasks(TaskCollection, attribute) {
			$scope.ListofTasks = [];
			sortOn(TaskCollection, "tasklistSortOrder");
			var groupValue = "_INVALID_GROUP_VALUE_";
			for (var i = 0; i < TaskCollection.length; i++) {
				var friend = TaskCollection[i];
				if (friend[attribute] !== groupValue) {
					var group = {
						label: friend[attribute],
						friends: [],
						rolename: friend.TaskName
					};
					groupValue = group.label;
					$scope.ListofTasks.push(group);
				}
				group.friends.push(friend);
			}
		}
		$scope.SaveAttachfiletoSelectedTasks = function() {
			var IDList = new Array();
			IDList = $scope.SelectedListofTasks;
			var FileID = $scope.contextFileID;
			var FiletoTaskStatusData = {};
			FiletoTaskStatusData.FileID = FileID;
			FiletoTaskStatusData.TaskIdsArr = IDList;
			FiletoTaskStatusData.FileType = $scope.ContextFileExtention;
			TaskNewService.CopyAttachmentsfromtaskToExistingTasks(FiletoTaskStatusData).then(function(TaskStatusResult) {
				if (TaskStatusResult.StatusCode == 405) {
					NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
				} else {
					if (TaskStatusResult.Response == false) {
						NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						$("#AddtoExistingTaskPopup").modal("hide");
					} else {
						$scope.ContextFileExtention = "";
						NotifySuccess($translate.instant('LanguageContents.Res_4065.Caption'));
						$scope.SelectedListofTasks = [];
						$("#AddtoExistingTaskPopup").modal("hide");
						RefreshCurrentTasksCollectionAfterAttachFile(IDList);
					}
				}
			});
		};
		$scope.ListofTasks = {
			Enabled: false,
			SublevelText: $translate.instant('LanguageContents.Res_4939.Caption'),
			SublevelTextStatus: 1
		};
		$scope.ExpandCollapseStatus = {
			isExpand: false
		};
		$("#TaskExpandAll").click(function(e) {
			$scope.ngExpandCollapseStatus.ngExpandAll = false;
		});
		$("#TaskCollapseAll").click(function(e) {
			$scope.ngExpandCollapseStatus.ngExpandAll = true;
		});
		$scope.srcCurrentexpandallpageno = 1;
		$scope.srcSubexpandallpageno = 1;
		$scope.ExpandAllTasks = function() {
			$scope.srcpageno = 1;
			$scope.srcCurrentexpandallpageno = 1;
			$scope.srcSubexpandallpageno = 1;
			$scope.ngExpandCollapseStatus.ngExpandAll = false;
			expandParentList();
			expandSubList();
		};

		function expandParentList() {
			var inputs = $(".maintask li[data-ExpandStatus='false']").eq(0);
			inputs.each(function() {
				var targetCtrl = $(this);
				$scope.srcCurrentexpandallpageno = 1;
				ExpandAllFunctinality(targetCtrl, targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
			});
		}

		function expandSubList() {
			var sublevelinputs = $(".subtask li[data-ExpandStatus='false']").eq(0);
			sublevelinputs.each(function() {
				var targetCtrl = $(this);
				$scope.srcSubexpandallpageno = 1;
				ExpandSublevelTaskAllFunctinality(targetCtrl, targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
			});
		}
		$scope.CollapseAllTasks = function() {
			$scope.ngExpandCollapseStatus.ngExpandAll = true;
			$scope.srcpageno = 1;
			$scope.srcCurrentexpandallpageno = 1;
			$scope.srcSubexpandallpageno = 1;
			var inputs = $(".maintask li[data-ExpandStatus='true']").eq(0);
			inputs.each(function() {
				var targetCtrl = $(this);
				collapseAllFunctionality(targetCtrl, targetCtrl.attr("data-entityid"), targetCtrl.attr("data-tasklistid"));
			});
			var sublevelinputs = $(".subtask li[data-ExpandStatus='true']").eq(0);
			sublevelinputs.each(function() {
				var subtargetCtrl = $(this);
				collapseAllSublevelFunctionality(subtargetCtrl, subtargetCtrl.attr("data-entityid"), subtargetCtrl.attr("data-tasklistid"));
			});
		}

		function collapseAllFunctionality(targetCtrl, EntityId, TaskListID) {
			$timeout(function() {
				$("#ListName_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
				$(".nestedsorting").each(applySortable);
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
				});
				if (taskListResObj.length > 0) {
					taskListResObj[0].IsExpanded = false;
				}
				var memorytaskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
					return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
				});
				$scope.TaskExpandedDetails.splice($.inArray(memorytaskListResObj[0], $scope.TaskExpandedDetails), 1);
				$scope.CollapseAllTasks();
			}, 0);
		}

		function collapseAllSublevelFunctionality(targetCtrl, EntityId, TaskListID) {
			$timeout(function() {
				$("#ListIcon_" + parseInt(targetCtrl.attr("data-tasklistid")) + "").empty();
				$("." + targetCtrl.attr("data-tlid")).css("display", "none");
				$(".nestedsorting").each(applySortable);
				var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == EntityId;
				});
				var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function(e) {
					return e.ID == TaskListID;
				});
				if (taskResObj.length > 0) {
					taskResObj[0].IsExpanded = false;
				}
				var memorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
					return e.ID == parseInt(targetCtrl.attr("data-tasklistid"));
				});
				$scope.SubLevelTaskExpandedDetails.splice($.inArray(memorytaskListResObj[0], $scope.SubLevelTaskExpandedDetails), 1);
				$scope.CollapseAllTasks();
			}, 0);
		}
		$scope.ExpandManually = function() {
			for (var i = 0; i <= $scope.TaskExpandedDetails.length - 1; i++) {
				var taskListResObj = $.grep($scope.groups, function(e) {
					return e.ID == $scope.TaskExpandedDetails[i].ID;
				});
				if (taskListResObj.length > 0) {
					taskListResObj[0].TaskList = $scope.TaskExpandedDetails[i].TaskList;
					taskListResObj[0].IsGetTasks = true;
					taskListResObj[0].IsExpanded = true;
					$("#TaskListHolderID_" + parseInt($scope.TaskExpandedDetails[i].ID)).css("display", "block");
					$(".nestedsorting").each(applySortable);
				}
			}
		}
		$scope.SubLevelExpandManually = function() {
			for (var i = 0; i <= $scope.SubLevelTaskExpandedDetails.length - 1; i++) {
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == $scope.SubLevelTaskExpandedDetails[i].EntityID;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == $scope.SubLevelTaskExpandedDetails[i].ID;
					});
					if (taskListGroup.length > 0) {
						taskListGroup[0].TaskList = $scope.SubLevelTaskExpandedDetails[i].TaskList;
						taskListGroup[0].IsGetTasks = true;
						taskListGroup[0].IsExpanded = true;
						$("#SubTaskListHolderID_" + parseInt($scope.SubLevelTaskExpandedDetails[i].ID)).css("display", "block");
						$(".nestedsorting").each(applySortable);
					}
				}
			}
		}

		function ExpandAllFunctinality(targetCtrl, EntityId, TaskListID) {
			if (EntityId == $stateParams.ID) {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == TaskListID;
					});
					if (taskListResObj.length > 0) {
						taskListResObj[0].TaskList = taskCollection;
						taskListResObj[0].IsGetTasks = true;
						taskListResObj[0].IsExpanded = true;
					}
					var MemorytaskListResObj = [];
					MemorytaskListResObj = $.grep($scope.TaskExpandedDetails, function(e) {
						return e.ID == TaskListID;
					});
					if (MemorytaskListResObj.length == 0) {
						var memoryTasklist = {
							ID: TaskListID,
							EntityID: EntityId,
							TaskList: taskCollection
						}
						$scope.TaskExpandedDetails.push(memoryTasklist);
					}
					$timeout(function() {
						$(".nestedsorting").each(applySortable);
						CurrentLevelexpandallpagingOnAutoScroll(EntityId, TaskListID);
					}, 10);
				});
			}
		}

		function CurrentLevelexpandallpagingOnAutoScroll(EntityId, TaskListID) {
			$scope.srcCurrentexpandallpageno = $scope.srcCurrentexpandallpageno + 1;
			if (EntityId == $stateParams.ID) {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcCurrentexpandallpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == TaskListID;
					});
					if (taskListResObj.length > 0) {
						if (taskCollection != undefined) {
							taskListResObj[0].IsGetTasks = true;
							taskListResObj[0].IsExpanded = true;
							for (var s = 0, task; task = taskCollection[s++];) {
								taskListResObj[0].TaskList.push(task)
							}
							if (taskCollection.length > 0) CurrentLevelexpandallpagingOnAutoScroll(EntityId, TaskListID);
							else {
								$scope.srcCurrentexpandallpageno = 1;
								$timeout(function() {
									$(".nestedsorting").each(applySortable);
									expandParentList();
								}, 10);
							}
						} else {
							$scope.srcCurrentexpandallpageno = 1;
							$timeout(function() {
								$(".nestedsorting").each(applySortable);
								expandParentList();
							}, 10);
						}
					}
				});
			}
		}

		function SubLevelexpandallpagingOnAutoScroll(EntityId, TaskListID) {
			$scope.srcSubexpandallpageno = $scope.srcSubexpandallpageno + 1;
			if (EntityId != $stateParams.ID) {
				TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcSubexpandallpageno).then(function(TaskListLibraryData) {
					var ResultTaskList = [];
					ResultTaskList = TaskListLibraryData.Response;
					var taskCollection = OrganizeTaskCollection(ResultTaskList);
					var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == EntityId;
					});
					if (EntityTaskObj.length > 0) {
						var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
							return e.ID == TaskListID;
						});
						if (taskListGroup.length > 0) {
							if (taskCollection != undefined) {
								for (var s = 0, task; task = taskCollection[s++];) {
									taskListGroup[0].TaskList.push(task)
								}
								if (taskCollection.length > 0) CurrentLevelexpandallpagingOnAutoScroll(EntityId, TaskListID);
								else {
									$scope.srcSubexpandallpageno = 1;
									$timeout(function() {
										$(".nestedsorting").each(applySortable);
										expandSubList();
									}, 10);
								}
							} else {
								$scope.srcSubexpandallpageno = 1;
								$timeout(function() {
									$(".nestedsorting").each(applySortable);
									expandSubList();
								}, 10);
							}
						}
					}
				});
			}
		}

		function ExpandSublevelTaskAllFunctinality(targetCtrl, EntityId, TaskListID) {
			TaskNewService.GetEntityTaskCollection(EntityId, TaskListID, $scope.srcpageno).then(function(TaskListLibraryData) {
				var ResultTaskList = [];
				ResultTaskList = TaskListLibraryData.Response;
				var taskCollection = OrganizeTaskCollection(ResultTaskList);
				var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
					return e.EntityID == EntityId;
				});
				if (EntityTaskObj.length > 0) {
					var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
						return e.ID == TaskListID;
					});
					if (taskListGroup.length > 0) {
						taskListGroup[0].TaskList = taskCollection;
						taskListGroup[0].IsGetTasks = true;
						taskListGroup[0].IsExpanded = true;
					}
				}
				var MemorytaskListResObj = [];
				MemorytaskListResObj = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
					return e.ID == TaskListID;
				});
				if (MemorytaskListResObj.length == 0) {
					var memoryTasklist = {
						ID: TaskListID,
						EntityID: EntityId,
						TaskList: taskCollection
					}
					$scope.SubLevelTaskExpandedDetails.push(memoryTasklist);
				}
				$timeout(function() {
					$("." + targetCtrl.attr("data-tlid")).css("display", "block");
					$(".nestedsorting").each(applySortable);
					SubLevelexpandallpagingOnAutoScroll(EntityId, TaskListID);
				}, 10);
			});
		}
		$scope.setfocusontext = function(checklistID) {
			$timeout(function() {
				$('[datafocus-id ="' + checklistID + '" ]').focus().select()
			}, 10);
		}
		$scope.popMe = function(link) {
			var linktypeObj = $.grep($scope.DefaultSettings.LinkTypes, function(val) {
				return val.Id == parseInt(link.LinkType)
			})[0];
			if (linktypeObj != undefined) {
				var mypage = linktypeObj.Type + link.LinkURL;
				var myname = mypage;
				var w = 1200;
				var h = 800
				var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
				var win = window.open(mypage, myname, winprops)
			}
		};
		$scope.DeleteAttachmentVersionByID = function(ID, Index, ActiveFileVersionID) {
			bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function(result) {
				if (result) {
					$timeout(function() {
						if (ActiveFileVersionID != 0) {
							bootbox.alert($translate.instant('LanguageContents.Res_1973.Caption'));
							return false;
						}
						TaskNewService.DeleteFileByID(ID).then(function(deleteattach) {
							if (deleteattach.Response == true) {
								$scope.TaskVersionFileList.splice(Index, 1);
								NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
							} else {
								NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
							}
						});
					}, 100);
				}
			});
		}
		$scope.callcontent = function(file) {
			var msg = "";
			if (file.Extension != "Link") {
				msg += "<b>Description:</b> " + (file.Description != null ? file.Description.toString() : "-") + "<br/><br/>";
				msg += "<b>Uploaded by:</b> " + (file.OwnerName != null ? file.OwnerName.toString() : "-") + "<br/><br/>";
				msg += "<b>File size: </b> " + (file.Size != null ? parseSize(file.Size) : "-") + "<br/><br/>";
				msg += "<b>Type:</b> " + (file.Extension != null ? file.Extension.toString() : "-") + "<br/><br/>";
				return msg;
			} else {
				msg += "<b>Url:</b> " + (file.LinkURL != null ? file.LinkURL.toString() : "-") + "<br/><br/>";
				return msg;
			}
		};
		$scope.TooltipTaskName = function(description) {
			if (description != null & description != "") {
				return "<span class='preWrap-ws'>" + description + "</span>";
			} else {
				return "<label class=\"align-center\">No description</label>";
			}
		};
		$scope.DecodedTaskName = function(TaskName) {
			if (TaskName != null & TaskName != "") {
				return $('<div />').html(TaskName).text();
			} else {
				return "";
			}
		};
		$scope.ShowTaskAssigneeTooltipContent = function(memObj) {
			var msg = "";
			if (memObj != null) {
				for (var i = 0; i <= memObj.length - 1; i++) {
					msg += '<div class="groupAssigneeTooltipContent"><img position="right" src="Handlers/UserImage.ashx?id=' + memObj[i].UserID + '">' + memObj[i].UserName + '</div>';
				}
			}
			return msg;
		}
		$scope.FileChange = false;
		$scope.LinkChange = false;
		$scope.GetParentTaskCount = function(taskListID, EntityID) {
			var TaskStatusData = {};
			TaskStatusData.EntityID = EntityID;
			TaskStatusData.TaskListID = taskListID;
			TaskStatusData.Status = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			TaskNewService.GettaskCountByStatus(TaskStatusData).then(function(TaskStatusResult) {
				var count = TaskStatusResult.Response;
				try {
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == taskListID && e.EntityParentID == EntityID;
					});
					if (taskListResObj.length > 0) {
						taskListResObj[0].TaskCount = count.toString();
					}
				} catch (e) {}
			});
		}
		$scope.GetSubTaskCount = function(taskListID, EntityID) {
			var TaskStatusData = {};
			TaskStatusData.EntityID = EntityID;
			TaskStatusData.TaskListID = taskListID;
			TaskStatusData.Status = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			TaskNewService.GettaskCountByStatus(TaskStatusData).then(function(TaskStatusResult) {
				var count = TaskStatusResult.Response;
				try {
					var EntityTaskObj = [];
					EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == EntityID;
					});
					if (EntityTaskObj.length > 0) {
						var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
							return e.ID == taskListID;
						});
						if (taskObjHolder.length > 0) {
							taskObjHolder[0].TaskCount = count.toString();
						}
					}
				} catch (e) {}
			});
		}
		$scope.ApplyTaskListCountDetails = function() {
			var parentObj = [];
			for (var i = 0, grp; grp = $scope.groups[i++];) {
				parentObj.push({
					"TaskListID": grp.ID,
					"EntityID": grp.EntityParentID
				});
			}
			var TaskStatusData = {};
			TaskStatusData.Status = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			TaskStatusData.TaskObject = parentObj;
			TaskNewService.GetFiltertaskCountByStatus(TaskStatusData).then(function(TaskStatusResult) {
				var count = [];
				count = TaskStatusResult.Response;
				if (TaskStatusResult.Response != null) {
					for (var i = 0, obj; obj = count[i++];) {
						try {
							var taskListResObj = $.grep($scope.groups, function(e) {
								return e.ID == obj.ID && e.EntityParentID == obj.EntityParentID;
							});
							if (taskListResObj.length > 0) {
								taskListResObj[0].TaskCount = obj.TaskCount != null ? obj.TaskCount : 0;
							}
						} catch (e) {}
					}
				}
			});
		}
		$scope.ApplySubTaskListCountDetails = function() {
			var subObj = [];
			for (var k = 0, sgrp; sgrp = $scope.groupbySubLevelTaskObj[k++];) {
				var taskgoup = sgrp.TaskListGroup;
				for (var j = 0, subgrp; subgrp = taskgoup[j++];) {
					subObj.push({
						"TaskListID": subgrp.ID,
						"EntityID": sgrp.EntityID
					});
				}
			}
			var TaskStatusData = {};
			TaskStatusData.Status = $scope.GlobalTaskFilterStatusObj.MulitipleFilterStatus;
			TaskStatusData.TaskObject = subObj;
			TaskNewService.GetFiltertaskCountByStatus(TaskStatusData).then(function(TaskStatusResult) {
				var count = [];
				count = TaskStatusResult.Response;
				if (TaskStatusResult.Response != null) {
					if (TaskStatusResult.Response.length > 0) {
						for (var i = 0, obj; obj = count[i++];) {
							try {
								var EntityTaskObj = [];
								EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
									return e.EntityID == obj.EntityParentID;
								});
								if (EntityTaskObj.length > 0) {
									var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
										return e.ID == obj.ID;
									});
									if (taskObjHolder.length > 0) {
										taskObjHolder[0].TaskCount = obj.TaskCount != null ? obj.TaskCount : 0;
									}
								}
							} catch (e) {}
						}
					}
				}
			});
		}
		$scope.ListUnassignedTasks = [];
		$scope.UnassignedTaskMembersList = [];
		$scope.ListUnassignedTasksDueObjects = {
			Duedate: null,
			EntityID: 0,
			TaskListID: 0,
			dueIn: 0,
			TaskDueDate: '-',
			AssignableDueDate: ''
		};
		$scope.FetchUnassignedTasktoAssign = function() {
			$scope.editable = '';
			$scope.selectedAllunassignedtasks = false;
			$scope.UnassignedTaskMembers = [];
			$scope.UnassignedTaskMembersList = [];
			$scope.ListUnassignedTasksDueObjects = {
				Duedate: '-',
				EntityID: $scope.ContextTaskEntityID,
				TaskListID: $scope.ContextTaskListID,
				dueIn: 0,
				TaskDueDate: '-',
				AssignableDueDate: ''
			};
			$scope.ListUnassignedTasks = [];
			TaskNewService.FetchUnassignedTaskforReassign($scope.ContextTaskEntityID, $scope.ContextTaskListID).then(function(TaskListLibraryData) {
				var TaskLibraryList = TaskListLibraryData.Response;
				for (var k = 0; k < TaskLibraryList.length; k++) {
					if (k == 0) {
						GetEntityLocationPath(TaskLibraryList[k].ID);
					}
					$scope.ListUnassignedTasks.push({
						"TaskName": TaskLibraryList[k].Name,
						"TaskID": TaskLibraryList[k].ID,
						"EntityID": TaskLibraryList[k].EntityID
					});
				}
				if ($scope.ListUnassignedTasks.length > 0) {
					$("#multiassignTask").scrollTop(0);
					$("#multiassignTask").modal("show");
				} else bootbox.alert($translate.instant('LanguageContents.Res_1974.Caption'));
			});
		}
		$scope.checkAllunassignedtasks = function($event) {
			var checkbox = $event.target;
			if (checkbox.checked) {
				$scope.selectedAllunassignedtasks = true;
			} else {
				$scope.selectedAllunassignedtasks = false;
			}
			angular.forEach($scope.ListUnassignedTasks, function(item) {
				item.Selected = $scope.selectedAllunassignedtasks;
			});
		};
		$scope.RemoveCheckallUnassignedTaskSelection = function(list, $event) {
			var checkbox = $event.target;
			list.Selected = checkbox.checked;
			var taskCollection = $.grep($scope.ListUnassignedTasks, function(e) {
				return e.Selected == true;
			}).length;
			if (taskCollection != $scope.ListUnassignedTasks.length) $scope.selectedAllunassignedtasks = false;
			else $scope.selectedAllunassignedtasks = true;
		}
		$scope.UnassignedTaskMembers = [];
		$scope.AssignUnassignTasktoMembers = function() {
			if ($('#btnmultiassigntasks').hasClass('disabled')) {
				return;
			}
			$('#btnmultiassigntasks').addClass('disabled');
			var taskCollection = $.grep($scope.ListUnassignedTasks, function(e) {
				return e.Selected == true;
			});
			if (taskCollection.length == 0) {
				bootbox.alert($translate.instant('LanguageContents.Res_1975.Caption'));
				$('#btnmultiassigntasks').removeClass('disabled');
				return false;
			} else {
				var alertmsg = '';
				if ($scope.UnassignedTaskMembersList.length == 0) alertmsg += $translate.instant('LanguageContents.Res_4564.Caption');
				if (alertmsg.trim().length == 0) {
					var TaskStatusData = {};
					TaskStatusData.TaskListID = $scope.ContextTaskListID;
					TaskStatusData.EntityID = $scope.ContextTaskEntityID;
					if ($scope.ListUnassignedTasksDueObjects.AssignableDueDate != "") TaskStatusData.DueDate = ConvertDateToString($scope.ListUnassignedTasksDueObjects.Duedate);
					else TaskStatusData.DueDate = "";
					TaskStatusData.TaskMembers = $scope.UnassignedTaskMembersList;
					TaskStatusData.Tasks = taskCollection;
					TaskNewService.AssignUnassignTasktoMembers(TaskStatusData).then(function(TaskStatusResult) {
						if (TaskStatusResult.StatusCode == 405) {
							NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
							$('#btnmultiassigntasks').removeClass('disabled');
						} else {
							if (TaskStatusResult.Response == null) {
								$('#btnmultiassigntasks').removeClass('disabled');
								NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
							} else {
								$('#btnmultiassigntasks').removeClass('disabled');
								$scope.ListUnassignedTasksDueObjects = {
									Duedate: '-',
									EntityID: 0,
									TaskListID: 0,
									dueIn: 0,
									TaskDueDate: '-',
									AssignableDueDate: ''
								};
								NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
								$('#multiassignTask').modal('hide');
								$scope.UnassignedTaskMembersList = [];
								$scope.ListUnassignedTasks = [];
								RefreshReassignedUnassignedTaskInObject(TaskStatusResult.Response);
							}
						}
					});
				} else {
					$('#btnmultiassigntasks').removeClass('disabled');
					bootbox.alert(alertmsg);
				}
			}
		}

		function RefreshReassignedUnassignedTaskInObject(returnObj) {
			for (var i = 0, taskObj; taskObj = returnObj[i++];) {
				if ($scope.ContextTaskEntityID == $stateParams.ID) {
					var taskListResObj = $.grep($scope.groups, function(e) {
						return e.ID == taskObj.TaskListID;
					});
					if (taskListResObj.length > 0) {
						if (taskListResObj[0].TaskList != null) {
							var taskResObj = $.grep(taskListResObj[0].TaskList, function(e) {
								return e.Id == taskObj.Id;
							});
							if (taskResObj.length > 0) {
								UpdateTaskObject(taskResObj, taskObj);
							}
						}
					}
				} else {
					var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
						return e.EntityID == taskObj.EntityID;
					});
					if (EntityTaskObj.length > 0) {
						var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
							return e.ID == taskObj.TaskListID;
						});
						if (taskListGroup.length > 0) {
							var taskObjHolder = $.grep(taskListGroup[0].TaskList, function(e) {
								return e.Id == taskObj.Id;
							});
							if (taskObjHolder.length > 0) {
								UpdateTaskObject(taskObjHolder, taskObj);
							}
						}
					}
				}
			}
		}
		$scope.ReassignMembersData = [];
		$scope.formatResult = function(item) {
			var markup = '<table class="user-result">';
			markup += '<tbody>';
			markup += '<tr>';
			markup += '<td class="user-image">';
			markup += '<img src="Handlers/UserImage.ashx?id=' + item.id + '">';
			markup += '</td>';
			markup += '<td class="user-info">';
			markup += '<div class="user-title">' + item.UserName + '</div>';
			markup += '</td>';
			markup += '</tr>';
			markup += '</tbody>';
			markup += '</table>';
			return markup;
		};
		$scope.formatSelection = function(item) {
			return item.UserName;
		};
		$scope.tagAllOptions = {
			multiple: true,
			allowClear: true,
			data: $scope.ReassignMembersData,
			formatResult: $scope.formatResult,
			formatSelection: $scope.formatSelection,
			dropdownCssClass: "bigdrop",
			escapeMarkup: function(m) {
				return m;
			}
		};
		$scope.ExportTasklisttoExcel = function(EntireListoption) {
			$('#ExportTasklistReportPageModel').modal("show");
			TaskNewService.GetTaskListReport($scope.ContextTaskEntityID, $scope.ContextTaskListID, (EntireListoption == 'true') ? true : false, ($scope.SubLevelEnableObject.SublevelTextStatus == 0 ? true : false)).then(function(RptResult) {
				if (RptResult.StatusCode == 200 && RptResult.Response != null) {
					var a = document.createElement('a'),
						fileid = RptResult.Response,
						extn = '.xlsx';
					var filename = 'TaskListReport.xlsx';
					a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
					a.download = fileid + extn;
					document.body.appendChild(a);
					a.click();
					$('#ExportTasklistReportPageModel').modal("hide");
				}
			});
		}
		$scope.Roles = [];
		$scope.globalEntityMemberLists = [];
		$scope.globalTempcount = 1;
		$timeout(function() {
			LoadMemberRoles();
		}, 300);

		function LoadMemberRoles() {
			TaskNewService.GetEntityDetailsByID($stateParams.ID).then(function(GetEntityresult) {
				if (GetEntityresult.Response != null) {
					TaskNewService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function(role) {
						$scope.Roles = role.Response;
					});
				}
			});
		}
		$scope.deleteGlobalTaskMembers = function(item) {
			bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function(result) {
				if (result) {
					$timeout(function() {
						$scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
					}, 100);
				}
			});
		};
		$scope.OpenWorkTask = function(tasktype, tasklist) {
			$scope.AttachmentFilename = [];
			$scope.FileList = [];
			$scope.$broadcast('Cleasetaskpopup', "Task", tasktype, tasklist, 0);
		}
		$scope.LoadTreeData = function() {
			$scope.$broadcast('OpenTree', "Task", 2);
		}
		$scope.AssetId = 0;
		$scope.$on("HandOverDamFiles", function(event, files, assetid) {
			$scope.AssetId = 0;
			$scope.AssetId = assetid;
			if (files != null) if (files.length > 0) for (var i = 0, response; response = files[i++];) {
				$scope.AttachmentFilename.push({
					"FileName": response.Name,
					"Extension": response.Extension,
					"Size": parseSize(response.Size),
					"FileGuid": response.Fileguid,
					"FileDescription": response.Description,
					"URL": '',
					"LinkType": "",
					"ID": 0,
					"LinkID": 0
				});
				$scope.FileList.push({
					"Name": response.Name,
					"VersionNo": response.VersionNo,
					"MimeType": response.MimeType,
					"Extension": response.Extension,
					"OwnerID": $cookies['UserId'],
					"CreatedOn": new Date.create(),
					"Checksum": "",
					"ModuleID": 1,
					"EntityID": $stateParams.ID,
					"Size": response.Size,
					"FileGuid": response.Fileguid,
					"FileDescription": response.Description,
					"IsExists": true
				});
			}
		});
		$timeout(function() {
			$("#totalAssetActivityAttachProgress_Version").empty();
			$("#totalAssetActivityAttachProgress_Version").append('<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>');
			StrartUpload_Version();
		}, 50);

		function StrartUpload_Version() {
			var uploader_V = new plupload.Uploader({
				runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
				browse_button: 'pickfiles_V',
				container: 'filescontainer_V',
				max_file_size: '10mb',
				flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
				silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
				url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
				multi_selection: false,
				multipart_params: {}
			});
			uploader_V.bind('Init', function(up, params) {});
			uploader_V.init();
			$('#pickfiles_V').each(function() {
				var input = new mOxie.FileInput({
					browse_button: this,
					multiple: false
				});
				input.onchange = function(event) {
					uploader_V.addFile(input.files);
				};
				input.init();
			});
			uploader_V.bind('FilesAdded', function(up, files) {
				TotalUploadProgress_V(files[0]);
				up.refresh();
				uploader_V.start();
				$('#totalAssetActivityAttachProgress_Version').show();
				$('#totalAssetActivityAttachProgress_Version .bar').css('width', '0');
			});
			uploader_V.bind('UploadProgress', function(up, file) {
				$('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
				TotalUploadProgress_V(file);
			});
			uploader_V.bind('Error', function(up, err) {
				bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
				up.refresh();
			});
			uploader_V.bind('FileUploaded', function(up, file, response) {
				SaveFileDetails_V(file, response.response);
				TotalUploadProgress_V(file);
			});
			uploader_V.bind('BeforeUpload', function(up, file) {
				$.extend(up.settings.multipart_params, {
					id: file.id,
					size: file.size
				});
			});
			uploader_V.bind('FileUploaded', function(up, file, response) {
				var obj = response;
			});

			function TotalUploadProgress_V(file) {
				$('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
			};
		}

		function SaveFileDetails_V(file, response) {
			$scope.SaveFiletoAsset = [];
			var extension = file.name.substring(file.name.lastIndexOf("."));
			var resultArr = response.split(",");
			$scope.SaveFiletoAsset.push({
				"AssetID": $scope.AssetId,
				"Status": 0,
				"MimeType": resultArr[1],
				"Size": file.size,
				"FileGuid": resultArr[0],
				"CreatedOn": new Date.create(),
				"Extension": extension,
				"Name": file.name,
				"VersionNo": 0,
				"Description": file.name,
				"OwnerID": $cookies['UserId']
			});
			TaskNewService.SaveFiletoAsset($scope.SaveFiletoAsset).then(function(result) {
				if (result.Response != null && result.Response != undefined) {
					$('#totalAssetActivityAttachProgress_Version').hide();
					NotifySuccess($translate.instant('LanguageContents.Res_4786.Caption'));
					ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
				} else {
					NotifyError($translate.instant('LanguageContents.Res_4266.Caption'));
				}
			});
		}
		$scope.CloseDampopup = function() {
			if ($scope.AssetfileTemplateLoading != null) {
				$scope.SetCurrentTabid("task-MetaData-" + $scope.AssetfileTemplateLoading, "task-Attachments-" + $scope.AssetfileTemplateLoading)
				$("#task-Attachments-" + $scope.AssetfileTemplateLoading + "dam").empty();
				$("#task-Attachments-" + $scope.AssetfileTemplateLoading + "dam").unbind();
				$scope.AssetfileTemplateLoading = "Null";
			}
			$scope.fromunassignedtask = false;
		};
		$scope.DAMentitytypes = [];

		function TooltipattachmentName(description) {
			if (description != null & description != "") {
				return description;
			} else {
				return "<label class=\"align-center\">No description</label>";
			}
		};
		$scope.popMe = function(e) {
			var TargetControl = $(e.target);
			var mypage = TargetControl.attr('data-Name');
			if (!(mypage.indexOf("http") == 0)) {
				mypage = "http://" + mypage;
			}
			var myname = TargetControl.attr('data-Name');
			var w = 1200;
			var h = 800
			var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
			var win = window.open(mypage, myname, winprops)
		};
		$scope.set_bgcolor = function(clr) {
			if (clr != null) return {
				'background-color': "#" + clr.toString().trim()
			};
			else return '';
		}
		$scope.set_color = function(clr) {
			if (clr != null) return {
				'color': "#" + clr.toString().trim()
			};
			else return '';
		}
		$scope.dyn_ContAsset = '';
		$scope.DAMtypeswithExt = [];
		$scope.DamExt = [];
		$scope.Fileids = [];

		function GetAllExtensionTypesforDAM() {
			TaskNewService.GetAllExtensionTypesforDAM().then(function(data) {
				$scope.DAMtypeswithExt = data.Response;
				$scope.AssetTasktypeswithExt = data.Response;
				$scope.damExtensionObj = {
					DAMtypeswithExt: $scope.DAMtypeswithExt,
					AssetTasktypeswithExt: $scope.AssetTasktypeswithExt
				}
			});
		}
		var filecount = 0;
		var totalfilecount = 0;

		function applyRemoteData(newFriends) {
			var friends = newFriends;
		}
		var TotalCount = 0;
		var saveBlankAssetsCalled = false;
		$scope.fnTimeOutfrAssetCreationTask = function() {
			if ($scope.processingsrcobj.processingplace == "task") {
				GetAllExtensionTypesforDAM();
				$('#pickassetsAlternatetask').removeAttr('disabled', 'disabled');
				$('#assetlistActivityTask').empty();
				$("#totalAssetActivityAttachProgresstask").empty();
				$('#taskuploaded').empty();
				$("#totalAssetActivityAttachProgresstask").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="taskuploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
			} else {}
		};
		$timeout(function() {
			$scope.fnTimeOutfrAssetCreationTask();
		}, 250);
		$scope.onDamtypechange = function(fileID) {
			if ($scope.assetfields['assettype_' + fileID] != null) {
				var oldAssettype = $scope.oldfiledetails["AssetType_" + fileID];
				var newAssettype = parseInt($scope.assetfields['assettype_' + fileID].id);
				TaskNewService.GetAssetCategoryTree(newAssettype).then(function(data) {
					if (data.Response != null) {
						$scope.AssetCategory["AssetCategoryTree_" + fileID] = JSON.parse(data.Response);
						$scope.AssetCategory["filterval_" + fileID] = '';
						var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
						$("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
					}
				});
				TaskNewService.GetAssetCategoryPathInAssetEdit(newAssettype).then(function(datapath) {
					if (datapath.Response != null) {
						if (datapath.Response[1].AssetTypeIDs.length > 0) {
							$scope.AssetCategory["treestructure_" + fileID] = datapath.Response[0].CategoryPath;
						} else $scope.assetfields["assettype_" + fileID] = '';
					}
				});
				$scope.AssetCategory['showcategroytext_' + fileID] = true;
				$('#div_' + fileID).html($compile("")($scope));
				$(".testdrop").select2("destroy").select2({});
				$('#div_' + fileID).html('');
				if (oldAssettype != '') {
					TaskNewService.GetDamAttributeRelation(oldAssettype).then(function(attrList) {
						var attrreldata = attrList.Response;
						var ind = FileAttributes.FileID.indexOf(fileID);
						FileAttributes.FileID.splice(ind, 1);
						FileAttributes.AttributeDataList.splice(ind, 1);
						FileAttributes.AssetType.splice(ind, 1);
						for (var i = 0; i < attrreldata.length; i++) {
							if (attrreldata[i].TypeID == 1) {
								if (attrreldata[i].ID != 70) {
									if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
										delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
									} else {
										delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
									}
								}
							} else if (attrreldata[i].TypeID == 3) {
								if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
									delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
								} else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
									delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
									delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
								} else {
									delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
									delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
								}
							} else if (attrreldata[i].TypeID == 2) {
								delete $scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID];
							} else if (attrreldata[i].TypeID == 4) {
								delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
								delete $scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID];
							} else if (attrreldata[i].AttributeTypeID == 12) {
								$scope.Dropdown = {};
							} else if (attrreldata[i].AttributeTypeID == 13) {
								$scope.PercentageVisibleSettings = {};
								$scope.DropDownTreePricing = {};
							} else if (attrreldata[i].AttributeTypeID == 10) {
								$scope.items = [];
							} else if (attrreldata[i].AttributeTypeID == 7) {
								$scope.treesrcdirec = {};
								$scope.treePreviewObj = {};
								$scope.treeNodeSelectedHolder = [];
							}
						}
					});
				}
				TaskNewService.GetDamAttributeRelation(newAssettype).then(function(attrList) {
					var attrreldata = attrList.Response;
					var newHTML = '';
					FileAttributes.FileID.push(fileID);
					FileAttributes.AttributeDataList.push(attrreldata);
					FileAttributes.AssetType.push(newAssettype);
					for (var i = 0; i < attrreldata.length; i++) {
						if (attrreldata[i].AttributeTypeID == 1) {
							if (attrreldata[i].ID != 70) {
								if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
									newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
									$scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
								} else {
									newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
									$scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
								}
							}
						} else if (attrreldata[i].AttributeTypeID == 3) {
							if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
								newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\" dirownernameautopopulate placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
								$scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = $scope.OwnerList[0].UserName;
							} else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
								$scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
								newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
								$scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
							} else {
								$scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
								newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
								$scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
							}
						} else if (attrreldata[i].AttributeTypeID == 2) {
							if (attrreldata[i].ID != 3) newHTML += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" name=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" id=\"TextMultiLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\" rows=\"3\"></textarea></div></div>";
							$scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
						} else if (attrreldata[i].AttributeTypeID == 4) {
							$scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
							newHTML += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListMultiSelection_" + fileID + attrreldata[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + fileID + attrreldata[i].ID + "\"  > </select></div></div>";
							$scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
						} else if (attrreldata[i].AttributeTypeID == 5 && attrreldata[i].AttributeID != SystemDefiendAttributes.ApproveTime && attrreldata[i].AttributeID != SystemDefiendAttributes.CreationDate) {
							$scope.calanderopened = false;
							$scope.MinValue = attrreldata[i].MinValue;
							$scope.MaxValue = attrreldata[i].MaxValue;
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = new Date.create();
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID]);
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = (temp.MinDate);
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = (temp.MaxDate);
							newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + fileID + attrreldata[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + attrreldata[i].ID + "\" id=\"DatePart_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\" ng-click=\"Calanderopen($event," + $scope.assetfields["DatePart_Calander_Open" + fileID + attrreldata[i].ID] + ")\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].ID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].ID + "\" datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + fileID + attrreldata[i].ID + "," + fileID + attrreldata[i].ID + "),3000)\" ng-model=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\"></div></div>";
							$scope.assetfields["DatePart_" + fileID + attrreldata[i].ID] = "";
							$scope.assetfields["assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID] = false;
						} else if (attrreldata[i].AttributeTypeID == 16) {
							$scope.TaskDueDate1 = false;
							$scope.MinValue = attrreldata[i].MinValue;
							$scope.MaxValue = attrreldata[i].MaxValue;
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = new Date.create();
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID]);
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = (temp.MinDate);
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = (temp.MaxDate);
							newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + fileID + attrreldata[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + attrreldata[i].ID + "\" id=\"DatePart_" + fileID + attrreldata[i].ID + "\" ng-click=\"dynCalanderopen($event," + $scope.assetfields["DatePart_Calander_Open" + fileID + attrreldata[i].ID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].ID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + attrreldata[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + fileID + attrreldata[i].ID + "," + fileID + attrreldata[i].ID + "),3000)\" ng-model=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\"></div></div>";
							$scope.assetfields["assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID] = false;
						} else if (attrreldata[i].AttributeTypeID == 12) {
							newHTML += "<div class\"control-group pull-left\">";
							var totLevelCnt1 = attrreldata[i].Levels.length;
							for (var j = 0; j < totLevelCnt1; j++) {
								if (totLevelCnt1 == 1) {
									$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = {};
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrreldata[i].tree).Children;
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = true;
									newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
									newHTML += "<input class=\"testdrop\" data-ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"   type=\"hidden\" /></div></div>";
									$scope.setFieldKeys();
								} else {
									$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = {};
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
										return item.Caption
									};
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
										return item.Caption
									};
									if (j == 0) {
										$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrreldata[i].tree).Children;
										$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = false;
										newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\">";
										newHTML += "<input class=\"testdrop\" data-ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"   type=\"hidden\" /></div></div>";
									} else {
										if (j == (attrreldata[i].Levels.length - 1)) {
											$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = [];
											$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = true;
											newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
											newHTML += "<input class=\"testdrop\" data-ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"   type=\"hidden\" /></div></div>";
										} else {
											$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = false;
											$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = [];
											newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
											newHTML += "<input class=\"testdrop\" ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"   type=\"hidden\" /></div></div>";
										}
									}
								}
							}
							newHTML += "</div>";
						} else if (attrreldata[i].AttributeTypeID == 10) {
							$scope.items = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
							$scope.OptionObj["option_" + fileID + attrreldata[i].AttributeID] = attrreldata[i].Options;
							$scope.setoptions();
							$scope.items.push({
								startDate: [],
								endDate: [],
								comment: '',
								sortorder: 0,
								fileid: fileID
							});
							$scope.assetfields["Period_" + fileID] = $scope.items;
							$scope.MinValue = attrreldata[i].MinValue;
							$scope.MaxValue = attrreldata[i].MaxValue;
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID] = new Date.create();
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID] = new Date.create();
							if ($scope.MinValue < 0) {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MinValue + 1));
							} else {
								$scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MinValue));
							}
							if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MaxValue - 1));
							} else {
								$scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].getDate() + 100000);
							}
							var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID]);
							$scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID] = (temp.MinDate);
							$scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID] = (temp.MaxDate);
							newHTML += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].AttributeID + "\" class=\"control-label\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + fileID + "\" ng-form=\"subForm\">";
							newHTML += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
							newHTML += "<input class=\"sdate margin-bottom0x Period_" + fileID + attrreldata[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + "item.startDate" + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + fileID + attrreldata[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + "item.endDate" + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + attrreldata[i].Caption + " Comment --\" />";
							newHTML += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
							newHTML += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + fileID + "')\">[Add " + attrreldata[i].Caption + "]</a></div></div>";
							$scope.assetfields["Period_" + fileID + attrreldata[i].AttributeID] = "";
							$scope.fields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
							$scope.fields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
						} else if (attrreldata[i].AttributeTypeID == 13) {
							$scope.PercentageVisibleSettings["AttributeId_Levels_" + fileID + attrreldata[i].AttributeID.toString() + ""] = true;
							$scope.DropDownTreePricing["AttributeId_Levels_" + fileID + attrreldata[i].AttributeID.toString() + ""] = attrreldata[i].DropDownPricing;
							newHTML += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity'  data-fileid=" + fileID.toString() + "  data-attributeid=" + attrreldata[i].AttributeID.toString() + "></div>";
						} else if (attrreldata[i].AttributeTypeID == 8) {
							$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
							newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].AttributeID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].AttributeID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].AttributeID + "\" placeholder=\"" + attrreldata[i].PlaceHolderValue + "\"></div></div>";
							$scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].AttributeID] = attrreldata[i].DefaultValue;
						} else if (attrreldata[i].AttributeTypeID == 17) {
							$scope.assetfields["ListTagwords_" + fileID + attrreldata[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
							$scope.OptionObj["tagoption_" + fileID + attrreldata[i].AttributeID] = [];
							$scope.setoptions();
							$scope.tempscope = [];
							newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "\" class=\"control-group\">";
							newHTML += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + fileID + attrreldata[i].AttributeID + "\">" + attrreldata[i].Caption + " </label>";
							newHTML += "<div class=\"controls\">";
							newHTML += "<directive-tagwords item-attrid = \"" + fileID + attrreldata[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + fileID + attrreldata[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
							newHTML += "</div></div>";
							if (attrreldata[i].InheritFromParent) {} else {}
						} else if (attrreldata[i].AttributeTypeID == 6) {
							var totLevelCnt = attrreldata[i].Levels.length;
							for (var j = 0; j < attrreldata[i].Levels.length; j++) {
								$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;
								$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = {};
								$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = false;
								$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatResult = function(item) {
									return item.Caption
								};
								$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].formatSelection = function(item) {
									return item.Caption
								};
								if (j == 0) {
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrreldata[i].tree).Children;
									newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\">";
									newHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + fileID + "' )\"  ng-model=\"assetfields.DropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
								} else {
									$scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].data = [];
									newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
									newHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + fileID + "' )\"  ng-model=\"assetfields.DropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
								}
							}
						} else if (attrreldata[i].AttributeTypeID == 7) {
							$scope.assetfields["Tree_" + fileID + attrreldata[i].AttributeID] = [];
							$scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
							$scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID] = JSON.parse(attrreldata[i].tree).Children;
							if ($scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID].length > 0) {
								treeTextVisbileflag = false;
								if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID])) {
									$scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = true;
								} else $scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = false;
							} else {
								$scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = false;
							}
							newHTML += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + fileID + attrreldata[i].AttributeID + '\" class="control-group treeNode-control-group">';
							newHTML += '    <label class="control-label">' + attrreldata[i].Caption + '</label>';
							newHTML += '    <div class="controls treeNode-controls">';
							newHTML += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + fileID + attrreldata[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + fileID + attrreldata[i].AttributeID + '"></div>';
							newHTML += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + fileID + attrreldata[i].AttributeID + '">';
							newHTML += '            <span ng-if="doing_async">...loading...</span>';
							newHTML += '            <abn-tree tree-filter="filterValue_' + fileID + attrreldata[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + fileID + attrreldata[i].AttributeID + '\" accessable="' + attrreldata[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
							newHTML += '        </div>';
							newHTML += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + fileID + attrreldata[i].AttributeID + '\">';
							newHTML += '            <div class="controls">';
							newHTML += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + fileID + attrreldata[i].AttributeID + '\" node-attributeid="' + attrreldata[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
							newHTML += '        </div></div>';
							newHTML += '</div></div>';
						}
					}
					$scope.oldfiledetails["AssetType_" + fileID] = newAssettype;
					$('#div_' + fileID).append($compile(newHTML)($scope));
				});
				$scope.addfromComputer = true;
			}
		}
		$scope.dyn_ContAsset = '';
		$scope.fieldoptions = [];
		$scope.setFieldKeys = function() {
			var keys = [];
			angular.forEach($scope.assetfields, function(key) {
				keys.push(key);
				$scope.fieldKeys = keys;
			});
		}
		$scope.setoptions = function() {
			var keys = [];
			angular.forEach($scope.OptionObj, function(key) {
				keys.push(key);
				$scope.fieldoptions = keys;
			});
		}
		$scope.DAMattributesRelationList = [];
		$scope.AttributeData = [];
		var FileAttributes = {
			"FileID": [],
			"AttributeDataList": [],
			"AssetType": []
		};
		$scope.SaveAsset = [];
		$scope.oldfiledetails = {};
		$scope.SaveAssetFileDetails = function(file, response, fileAttributes) {
			if ($scope.assetfields['assettype_' + file.id] == undefined || $scope.assetfields['assettype_' + file.id] == null || $scope.assetfields['assettype_' + file.id] == '') {
				bootbox.alert($translate.instant('LanguageContents.Res_2511.Caption'));
				return false;
			}
			var AssetTypeID = $scope.assetfields['assettype_' + file.id].id;
			var AssetName = $scope.assetfields['assetname_' + file.id];
			var extension = file.name.substring(file.name.lastIndexOf("."));
			var resultArr = response.split(",");
			var AssetFileGuid = file.id;
			var AssetAttribute = fileAttributes;
			$scope.AttributeData = [];
			$scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] = ($scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] == undefined) ? '' : $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3];
			$scope.AttributeData.push({
				"AttributeID": "3",
				"AttributeCaption": "Description",
				"AttributeTypeID": "2",
				"NodeID": $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3],
				"Level": 0
			});
			for (var i = 0; i < AssetAttribute.length; i++) {
				if (AssetAttribute[i].AttributeTypeID == 3) {
					if (AssetAttribute[i].ID == SystemDefiendAttributes.Owner) {
						$scope.AttributeData.push({
							"AttributeID": AssetAttribute[i].ID,
							"AttributeCaption": AssetAttribute[i].Caption,
							"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
							"NodeID": parseInt($scope.OwnerList[0].Userid, 10),
							"Level": 0
						});
					} else if ($scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined && $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "") {
						var value = $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID];
						$scope.AttributeData.push({
							"AttributeID": AssetAttribute[i].ID,
							"AttributeCaption": AssetAttribute[i].Caption,
							"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
							"NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
							"Level": 0
						});
					}
				} else if (AssetAttribute[i].AttributeTypeID == 1) {
					if (AssetAttribute[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID];
					else {
						$scope.AttributeData.push({
							"AttributeID": AssetAttribute[i].ID,
							"AttributeCaption": AssetAttribute[i].Caption,
							"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
							"NodeID": ($scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
							"Level": 0
						});
					}
				} else if (AssetAttribute[i].AttributeTypeID == 2) {
					$scope.AttributeData.push({
						"AttributeID": AssetAttribute[i].ID,
						"AttributeCaption": AssetAttribute[i].Caption,
						"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
						"NodeID": ($scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
						"Level": 0
					});
				} else if (AssetAttribute[i].AttributeTypeID == 4) {
					if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "" && $scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
						if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID].length > 0) {
							var multiselectiObject = $scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID];
							for (var k = 0; k < multiselectiObject.length; k++) {
								$scope.AttributeData.push({
									"AttributeID": AssetAttribute[i].ID,
									"AttributeCaption": AssetAttribute[i].Caption,
									"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
									"NodeID": parseInt(multiselectiObject[k], 10),
									"Level": 0
								});
							}
						}
					}
				} else if (AssetAttribute[i].AttributeTypeID == 6) {
					for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
						if ($scope.assetfields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != "" && $scope.assetfields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != undefined) {
							$scope.AttributeData.push({
								"AttributeID": AssetAttribute[i].ID,
								"AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
								"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
								"NodeID": [$scope.assetfields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)].id],
								"Level": $scope.assetfields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)].Level,
								"Value": "-1"
							});
						}
					}
				} else if (AssetAttribute[i].AttributeTypeID == 5 && AssetAttribute[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
					var MyDate = new Date.create();
					var MyDateString;
					if (AssetAttribute[i].AttributeID != SystemDefiendAttributes.CreationDate) {
						if ($scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID] != undefined) MyDateString = $scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID].toString('MM/dd/yyyy');
						else MyDateString = "";
					} else {
						MyDateString = "";
					}
					$scope.AttributeData.push({
						"AttributeID": AssetAttribute[i].ID,
						"AttributeCaption": AssetAttribute[i].Caption,
						"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
						"NodeID": MyDateString,
						"Level": 0
					});
				} else if (AssetAttribute[i].AttributeTypeID == 16) {
					if ($scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
						$scope.AttributeData.push({
							"AttributeID": AssetAttribute[i].ID,
							"AttributeCaption": AssetAttribute[i].Caption,
							"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
							"NodeID": $scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID],
							"Level": 0
						});
					}
				} else if (AssetAttribute[i].AttributeTypeID == 7) {
					var treenodes = [];
					treenodes = $.grep($scope.treeNodeSelectedHolder, function(e) {
						return e.AttributeId == AssetAttribute[i].ID;
					});
					for (var x = 0, nodeval; nodeval = treenodes[x++];) {
						$scope.AttributeData.push({
							"AttributeID": AssetAttribute[i].ID,
							"AttributeCaption": AssetAttribute[i].Caption,
							"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
							"NodeID": [parseInt(nodeval.id, 10)],
							"Level": parseInt(nodeval.Level, 10),
							"Value": "-1"
						});
					}
				} else if (AssetAttribute[i].AttributeTypeID == 12) {
					for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
						var levelCount = AssetAttribute[i].Levels.length;
						if (levelCount == 1) {
							for (var k = 0; k < $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
								$scope.AttributeData.push({
									"AttributeID": AssetAttribute[i].AttributeID,
									"AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
									"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
									"NodeID": [$scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
									"Level": $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
									"Value": "-1"
								});
							}
						} else {
							if ($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != "" && $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != undefined) {
								if (j == (AssetAttribute[i].Levels.length - 1)) {
									for (var k = 0; k < $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
										$scope.AttributeData.push({
											"AttributeID": AssetAttribute[i].AttributeID,
											"AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
											"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
											"NodeID": [$scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
											"Level": $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
											"Value": "-1"
										});
									}
								} else {
									$scope.AttributeData.push({
										"AttributeID": AssetAttribute[i].AttributeID,
										"AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
										"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
										"NodeID": [$scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].id],
										"Level": $scope.assetfields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].Level,
										"Value": "-1"
									});
								}
							}
						}
					}
				} else if (AssetAttribute[i].AttributeTypeID == 13) {
					for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
						var attributeLevelOptions = [];
						attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + AssetFileGuid + AssetAttribute[i].AttributeID + ""], function(e) {
							return e.level == (j + 1);
						}));
						if (attributeLevelOptions[0] != undefined) {
							if (attributeLevelOptions[0].selection != undefined) {
								for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
									var valueMatches = [];
									if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function(relation) {
										return relation.NodeId.toString() === opt;
									});
									$scope.AttributeData.push({
										"AttributeID": AssetAttribute[i].AttributeID,
										"AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
										"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
										"NodeID": [opt],
										"Level": (j + 1),
										"Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
									});
								}
							}
						}
					}
				} else if (AssetAttribute[i].AttributeTypeID == 8) {
					$scope.AttributeData.push({
						"AttributeID": AssetAttribute[i].AttributeID,
						"AttributeCaption": AssetAttribute[i].AttributeCaption,
						"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
						"NodeID": $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].AttributeID].toString(),
						"Level": 0,
						"Value": "-1"
					});
				} else if (AssetAttribute[i].AttributeTypeID == 17) {
					if ($scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != "" && $scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != undefined) {
						for (var j = 0; j < $scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID].length; j++) {
							$scope.AttributeData.push({
								"AttributeID": AssetAttribute[i].AttributeID,
								"AttributeCaption": AssetAttribute[i].AttributeCaption,
								"AttributeTypeID": AssetAttribute[i].AttributeTypeID,
								"NodeID": parseInt($scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID][j], 10),
								"Level": 0,
								"Value": "-1"
							});
						}
					}
				}
			}
			$scope.StartEndDate = [];
			if ($scope.assetfields["Period_" + AssetFileGuid] != undefined) {
				$scope.perItems = [];
				$scope.perItems = $scope.assetfields["Period_" + AssetFileGuid];
				for (var m = 0; m < $scope.perItems.length; m++) {
					$scope.StartEndDate.push({
						startDate: ($scope.perItems[m].startDate.length != 0 ? dateFormat($scope.perItems[m].startDate) : ''),
						endDate: ($scope.perItems[m].endDate.length != 0 ? dateFormat($scope.perItems[m].endDate) : ''),
						comment: ($scope.perItems[m].comment.trim().length != 0 ? $scope.perItems[m].comment : ''),
						sortorder: 0
					});
				}
			}
			$scope.SaveAsset = [];
			$scope.SaveAsset.push({
				"Typeid": AssetTypeID,
				"Active": true,
				"FolderID": 0,
				"AttributeData": $scope.AttributeData,
				"Status": 1,
				"MimeType": resultArr[1],
				"Size": file.size,
				"FileGuid": resultArr[0],
				"Extension": extension,
				"Name": AssetName,
				"VersionNo": 1,
				"FileName": file.name,
				"Description": file.name,
				"EntityID": $scope.newTaskid,
				"Periods": $scope.StartEndDate
			});
			TaskNewService.SaveAsset($scope.SaveAsset).then(function(result) {
				document.getElementById('icon_ok' + file.id).style.display = 'block';
				filecount = filecount + 1;
				if (result.Response == 0) {
					NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
					$('#CreateNewTask').css('display', 'none');
					$('#pickassetsAlternatetask').css('display', 'none');
					$('#createblankAsset').css('display', 'none');
					$('#CreateNewTaskBtn').removeClass('disabled');
					totalfilecount = 0;
					filecount = 0;
					$scope.UniqueidsfrBlankAsset = [];
					saveBlankAssetsCalled = false;
					$scope.uploadingInProgress = false;
				} else {
					$scope.SaveAsset = [];
					TotalCount = TotalCount + 1;
					if (totalfilecount == filecount) {
						$('#CreateNewTaskBtn').removeClass('disabled');
						$('#CreateNewTask').css('display', 'none');
						$('#pickassetsAlternatetask').css('display', 'none');
						$('#createblankAsset').css('display', 'none');
						$('#pickassetsAlternatetask').removeAttr('disabled', 'disabled');
						$('#pickassetsAlternatetask').css('pointer-events', 'auto');
						$('#pickassetsAlternatetask').css('display', 'block');
						$('#CreateNewTaskBtn').removeAttr('disabled', 'disabled');
						$('#CreateNewTaskBtn').css('pointer-events', 'auto');
						totalfilecount = 0;
						filecount = 0;
						$scope.UniqueidsfrBlankAsset = [];
						$scope.Fileids = [];
						var FileAttributes = {
							"FileID": [],
							"AttributeDataList": [],
							"AssetType": []
						};
						NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
						$scope.uploadingInProgress = false;
						if ($scope.TaskActionFor == 1) RereshTaskObj($scope.returnObj);
						else RereshTaskObj($scope.returnObj);
						$scope.FileList = [];
						$scope.SelectedTaskLIstID = 0;
						$scope.AttachmentFilename = [];
						refreshModel();
						$timeout(function() {
							$scope.ApplySubTaskListCountDetails();
							$(".nestedsorting").each(applySortable);
						}, 10);
						$scope.returnObj = [];
						$scope.newTaskid = 0;
						$scope.addfromGenAttachments = false;
						$scope.addfromComputer = false;
						$('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
						$scope.TaskAttachAsset.html = '';
						$('#assetlistActivityTask').empty();
						$("#taskentityattachassetdata").empty();
						$('#addTask').modal('hide');
						var str = $('#taskuploaded').text().replace('Upload Successful.', '');
						$('#taskuploaded').text(str);
						$scope.uploadingInProgress = false;
					}
				}
			});
		}
		$scope.fieldsfrDet = {
			usersID: ''
		};
		$scope.setFieldKeys = function() {
			var keys = [];
			angular.forEach($scope.fieldsfrDet, function(key) {
				keys.push(key);
				$scope.fieldKeys = keys;
			});
		}

		function generateUUID() {
			var d = new Date.create().getTime();
			var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = (d + Math.random() * 16) % 16 | 0;
				d = Math.floor(d / 16);
				return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
			});
			return uuid;
		};
		$scope.tree = {};
		$scope.fieldKeys = [];
		$scope.options = {};
		$scope.fileguid = '';
		$scope.UniqueidsfrBlankAsset = [];
		$scope.IsLock
		$scope.processingsrcobj = {
			processingid: $scope.TaskBriefDetails.taskID,
			processingplace: "task",
			processinglock: $scope.IsLock
		};
		$scope.AssetfileTemplateLoading = "Null";
		$scope.AssetTaskentityselection = "Null";
		$scope.SetCurrentTabid = function(activecalss, deactiveclass) {
			$("#li" + activecalss + "").removeClass('active');
			$("#" + activecalss + "").removeClass('tab-pane active');
			$("#" + activecalss + "").removeClass('tab-pane');
			$("#li" + activecalss + "").addClass('active');
			$("#" + activecalss + "").addClass('tab-pane active');
			$("#li" + deactiveclass + "").removeClass('active');
			$("#" + deactiveclass + "").removeClass('tab-pane active');
			$("#" + deactiveclass + "").removeClass('tab-pane');
			$("#" + deactiveclass + "").addClass('tab-pane');
		}

		function refreshAssetobjects() {
			$scope.PageNoobj = {
				pageno: 1
			};
			$scope.TaskStatusObj = [{
				"Name": $translate.instant('LanguageContents.Res_4857.Caption'),
				ID: 1
			}, {
				"Name": $translate.instant('LanguageContents.Res_4811.Caption'),
				ID: 2
			}, {
				"Name": $translate.instant('LanguageContents.Res_663.Caption'),
				ID: 3
			}];
			$scope.SettingsDamAttributes = {};
			$scope.OrderbyObj = [{
				"Name": "Name (Ascending)",
				ID: 1
			}, {
				"Name": "Name (Descending)",
				ID: 2
			}, {
				"Name": "Creation date (Ascending)",
				ID: 3
			}, {
				"Name": "Creation date (Descending)",
				ID: 4
			}];
			$scope.FilterStatus = {
				filterstatus: 1
			};
			$scope.DamViewName = {
				viewName: "Thumbnail"
			};
			$scope.OrderbyName = {
				orderbyname: "Creation date (Descending)"
			};
			$scope.OrderBy = {
				order: 4
			};
			$scope.AssetSelectionFiles = [];
		}
		refreshAssetobjects();
		$scope.AttachAsset = function() {
			if ($scope.processingsrcobj.processingplace == "task") {
				$scope.$broadcast('CreateAssetAttach', $stateParams.ID);
			}
		}
		$scope.$on('CallBackAttachtak', function(event, ID) {
			refreshAssetobjects();
			$timeout(function() {
				$scope.processingsrcobj.processingid = ID;
				$scope.processingsrcobj.processingplace = "task";
				$scope.$broadcast('ReloadAssetView');
			}, 100);
		});
		$scope.$on('CallBackAttachtakdraw', function(event, ID) {
			$timeout(function() {
				var html = '';
				$("#taskentityattachassetdata").html(html);
				$scope.TaskAttachAsset.AssetSelectionFiles = ID;
				$scope.$broadcast('CallBackAttachtakdrawFromDamAsset', $scope.TaskAttachAsset);
			}, 100);
		});
		$scope.removeasset = function(assetid) {
			$("#taskentityattachassetdata").children("#Asset_" + assetid + "").remove();
			if ($("#taskentityattachassetdata > div").length == 0) {
				$scope.addfromGenAttachments = false;
				$scope.TaskAttachAsset.html = '';
			} else {
				$scope.TaskAttachAsset.html = '';
				$scope.TaskAttachAsset.html = $("#taskentityattachassetdata").html();
			}
			$scope.TaskAttachAsset.SelectedAssetFiles.splice($.inArray(assetid, $scope.TaskAttachAsset.SelectedAssetFiles), 1);
			$scope.TaskAttachAsset.AssetSelectionFiles.splice($.inArray(assetid, $scope.TaskAttachAsset.AssetSelectionFiles), 1);
			var remainRecord = [];
			var remainRecorddata = [];
			remainRecord = $.grep($scope.TaskAttachAsset.AssetFiles, function(e) {
				return e.AssetUniqueID == assetid;
			});
			if (remainRecord.length > 0) {
				$scope.TaskAttachAsset.AssetFiles.splice($.inArray(remainRecord[0], $scope.TaskAttachAsset.AssetFiles), 1);
			}
			remainRecorddata = $.grep($scope.TaskAttachAsset.AssetDynamicData, function(e) {
				return e.ID == assetid;
			});
			if (remainRecorddata.length > 0) {
				$scope.TaskAttachAsset.AssetDynamicData.splice($.inArray(remainRecorddata[0], $scope.TaskAttachAsset.AssetDynamicData), 1);
			}
		}
		$scope.returnUrl = function() {
			var str = "proof.aspx?tid=" + $scope.TaskBriefDetails.taskID + "&uid= " + $scope.OwnerID + "&oid=" + $scope.TaskBriefDetails.ownerId + "";
			return str;
		}
		$scope.AddDefaultEndDate = function(enddate, startdate, currentindex) {
			if (currentindex != undefined) {
				var enddate1 = null;
				if (currentindex != 0) {
					enddate1 = $scope.items[currentindex - 1].endDate;
				}
				if (enddate1 != null && enddate1 >= startdate) {
					bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
					$scope.items[currentindex].startDate = null;
					$scope.items[currentindex].endDate = null;
				} else {
					if (assignedforperiod) {
						if (tasktype == 2) $("#workAssetdynamicdetail").addClass('notvalidate');
						if (tasktype == 3) $("#approvalworktaskdynamicHolder").addClass('notvalidate');
						if (tasktype == 31) $("#reviewtaskdynamicholder").addClass('notvalidate');
						if (tasktype == 36) $("#proofHQtaskdynamicholder").addClass('notvalidate');
						if (tasktype == 37) $("#dalimtaskdynamicholder").addClass('notvalidate');
					} else $("#unassignedtaskdynamicdataholder").addClass('notvalidate');
					if (startdate == null) {
						$scope.items[currentindex].endDate = null;
					} else {
						$scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
					}
				}
			} else {
				if (enddate.toUpperCase().contains("STARTDATE")) {
					var temp = startdate;
					startdate = enddate;
					enddate = temp;
				}
				var objsetenddate = new Date.create($scope.fields[startdate]);
				$scope.fields[enddate] = objsetenddate.addDays(7);
			}
		};
		$scope.CheckPreviousEndDate = function(enddate, startdate, currentindex) {
			var enddate1 = null;
			if (currentindex != 0) {
				enddate1 = $scope.items[currentindex - 1].endDate;
			}
			if (enddate1 != null && enddate1 >= startdate) {
				bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
				$scope.items[currentindex].startDate = null;
				$scope.items[currentindex].endDate = null;
			} else {
				if (assignedforperiod) {
					if (tasktype == 2) $("#workAssetdynamicdetail").addClass('notvalidate');
					if (tasktype == 3) $("#approvalworktaskdynamicHolder").addClass('notvalidate');
					if (tasktype == 31) $("#reviewtaskdynamicholder").addClass('notvalidate');
					if (tasktype == 36) $("#proofHQtaskdynamicholder").addClass('notvalidate');
					if (tasktype == 37) $("#dalimtaskdynamicholder").addClass('notvalidate');
				} else $("#unassignedtaskdynamicdataholder").addClass('notvalidate');
				if (startdate == null) {
					$scope.items[currentindex].endDate = null
				} else {
					$scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
				}
			}
		};
		$scope.items = [];
		$scope.lastSubmit = [];
        $scope.addNew = function() {
			var ItemCnt = $scope.items.length;
			if (ItemCnt > 0) {
				if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
					return false;
				}
				$scope.items.push({
					startDate: [],
					endDate: [],
					comment: '',
					sortorder: 0
				});
			}
		};
		$scope.submitOne = function(item) {
			$scope.lastSubmit = angular.copy(item);
		};
		$scope.deleteOne = function(item) {
			$scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
			$scope.items.splice($.inArray(item, $scope.items), 1);
		};
		$scope.submitAll = function() {
			$scope.lastSubmit = angular.copy($scope.items);
		}
		$scope.CheckPreviousStartDate = function(enddate, startdate, currentindex) {
			var enddate1 = null;
			if (currentindex != 0 || currentindex == 0) {
				enddate1 = $scope.items[currentindex].endDate;
			}
			if (enddate1 != null && startdate >= enddate1) {
				bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
				$scope.items[currentindex].endDate = null;
			}
		};
		$scope.addNew = function(fileid) {
			var ItemCnt = $scope.assetfields["Period_" + fileid].length;
			if (ItemCnt > 0) {
				if ($scope.assetfields["Period_" + fileid][ItemCnt - 1].startDate == null || $scope.assetfields["Period_" + fileid][ItemCnt - 1].startDate.length == 0 || $scope.assetfields["Period_" + fileid][ItemCnt - 1].endDate.length == 0) {
					bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
					return false;
				}
				$scope.assetfields["Period_" + fileid].push({
					startDate: [],
					endDate: [],
					comment: '',
					sortorder: 0,
					fileid: fileid
				});
			}
		};
		$scope.AddDefaultEndDateAsset = function(enddate, startdate, currentindex, fileid) {
			var enddate1 = null;
			if (currentindex != 0) {
				enddate1 = $scope.fields["Period_" + fileid][currentindex - 1].endDate;
			}
			if (enddate1 != null && enddate1 >= startdate) {
				bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
				$scope.assetfields["Period_" + fileid][currentindex].startDate = null;
				$scope.assetfields["Period_" + fileid][currentindex].endDate = null;
			} else {
				$("#EntityMetadata").addClass('notvalidate');
				if (startdate == null) {
					$scope.assetfields["Period_" + fileid][currentindex].endDate = null;
				} else {
					$scope.assetfields["Period_" + fileid][currentindex].endDate = (new Date.create(startdate)).addDays(7);
				}
			}
		};
		$scope.$on('Taskassetinfoupdate', function(event) {
			$timeout(function() {}, 100);
		});
		$("input[id*='Datepicker']").keypress(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				return false;
			}
		});
		$("input[id*='TaskDueDate']").keypress(function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				return false;
			}
		});
		$scope.UploadAttributeId = 0;
		$scope.UploadImagefileTask = function(attributeId) {
			$scope.UploadAttributeId = attributeId;
			$("#pickfilesUploaderAttrTask").click();
		}
		$scope.ReassignMembersData = [];
		$scope.formatResult = function(item) {
			var markup = '<table class="user-result">';
			markup += '<tbody>';
			markup += '<tr>';
			markup += '<td class="user-image">';
			markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
			markup += '</td>';
			markup += '<td class="user-info">';
			markup += '<div class="user-title">' + item.text + '</div>';
			markup += '</td>';
			markup += '</tr>';
			markup += '</tbody>';
			markup += '</table>';
			return markup;
		};
		$scope.formatSelection = function(item) {
			var markup = '<table class="user-result">';
			markup += '<tbody>';
			markup += '<tr>';
			markup += '<td class="user-image">';
			markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
			markup += '</td>';
			markup += '<td class="user-info">';
			markup += '<div class="user-title">' + item.text + '</div>';
			markup += '</td>';
			markup += '</tr>';
			markup += '</tbody>';
			markup += '</table>';
			return markup;
		};
		$scope.CloseAddTask = function() {
			$scope.items = [];
			$scope.ngShowUnassigned = false;
			$scope.ngShowTaskCreation = true;
			$scope.DisableIsAdminTask = false;
			$scope.TaskActionCaption = "Add task";
			$scope.TaskMembersList = "";
			$scope.NewTaskName = "";
			$scope.TaskDueDate = new Date.create();
			$scope.TaskDueDate = null;
			$scope.TaskDescription = "";
			$scope.TaskNote = "";
			$scope.SelectedTaskID = 0;
			$scope.dyn_Cont = '';
			$scope.dyn_ContAsset = '';
			$scope.AdminTaskCheckList = [{
				ID: 0,
				NAME: ""
			}];
			$('#assetlistActivityTask').empty();
			$("#taskentityattachassetdata").empty();
			$scope.addfromGenAttachments = false;
			$scope.addfromComputer = false;
			$('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
			$("#DynamicTaskControlsAttributies").html("");
			$("#DynamicTaskControlsAttributies").append($compile("")($scope));
			$scope.treeNodeSelectedHolder = [];
			$scope.treesrcdirec = {};
			$scope.treePreviewObj = {};
			$scope.PercentageVisibleSettings = {};
			$scope.DropDownTreePricing = {};
			$scope.ClearModelObject($scope.fields);
			$scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
			$scope.dyn_Cont = '';
			refreshModel();
		};
		TaskNewService.GetCurrencyListFFsettings().then(function(CurrencyListResult) {
			if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
		});
		$scope.Getamountentered = function(atrid) {
			if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
			else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
		}
		$scope.GetCostCentreCurrencyRateById = function(atrid) {
			TaskNewService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function(resCurrencyRate) {
				if (resCurrencyRate.Response != null) {
					$scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
					if ($scope['origninalamountvalue_' + atrid] != 0) {
						$scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
					}
				}
			});
		}
		$scope.Getamountenteredforedit = function(atrid) {
			if (1 == $scope['NormalDropDown_' + atrid]) $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '');
			else $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '') / 1 / $scope.currRate;
		}
		$scope.GetCostCentreCurrencyRateByIdforedit = function(atrid) {
			TaskNewService.GetCostCentreCurrencyRateById(0, $scope['NormalDropDown_' + atrid], true).then(function(resCurrencyRate) {
				if (resCurrencyRate.Response != null) {
					$scope.currRate = parseFloat(resCurrencyRate.Response[1]);
					if ($scope['origninalamountvalue_' + atrid] != 0) {
						$scope['SingleTextValue_' + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope.currRate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
					}
				}
			});
		}
		$scope.$on('RefreshTaskByID', function($event, taskID) {
			$scope.TaskDetails = [];
			TaskNewService.GetEntityTaskDetails(taskID).then(function(EntityTaskDetails) {
				$scope.TaskDetails = EntityTaskDetails.Response;
				RefreshUnassignedTaskInObject($scope.TaskDetails);
				if ($scope.TaskDetails != null) if ($scope.TaskDetails.TaskStatus != 0) BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				else {
					LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
				}
			});
		});

		function HolidayCount(dueDate) {
			$scope.testduedate = dateFormat(dueDate, $scope.format);
			var nbcount = 0;
			var today = new Date.create();
			var holiday_exist = $.grep($scope.tempholidays, function(e) {
				return new Date.create(e) <= dueDate && new Date.create(e) >= today;
			});
			var holiday_exist_min = $.grep($scope.tempholidays, function(e) {
				return new Date.create(e) >= dueDate && new Date.create(e) <= today;
			});
			if (dueDate > today) {
				var tempdates = getDates(today, dueDate);
			} else {
				var tempdates = getDates(dueDate, today);
			}
			if (tempdates != null) {
				var holiday_exist_nonbusinessdays = $.grep(tempdates, function(e) {
					return (($scope.nonbusinessdays.sunday === "true" && e.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && e.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && e.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && e.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && e.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && e.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && e.getDay() === 6));
				});
			}
			if (holiday_exist_nonbusinessdays != null) {
				nbcount = holiday_exist_nonbusinessdays.length;
				if (new Date.create($scope.testduedate) > new Date.create()) {
					if (holiday_exist != null) {
						var Temp_holiday_exist = $.grep(holiday_exist, function(e) {
							return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
						});
						if (Temp_holiday_exist != null) {
							if (Temp_holiday_exist.length > 0 || nbcount > 0) {
								return (holiday_exist.length - Temp_holiday_exist.length) + nbcount;
							} else {
								return 0;
							}
						}
					}
				} else {
					if (holiday_exist_min != null) {
						var Temp_holiday_exist = $.grep(holiday_exist_min, function(e) {
							return (($scope.nonbusinessdays.sunday === "true" && new Date.create(e).getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && new Date.create(e).getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && new Date.create(e).getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && new Date.create(e).getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && new Date.create(e).getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && new Date.create(e).getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && new Date.create(e).getDay() === 6));
						});
						if (Temp_holiday_exist != null) {
							if (Temp_holiday_exist.length > 0 || nbcount > 0) {
								return (holiday_exist_min.length - Temp_holiday_exist.length) + nbcount;
							} else {
								return 0;
							}
						}
					}
				}
			}
		}

		function getDates(startDate, stopDate) {
			var dateArray = new Array();
			var currentDate = startDate;
			while (currentDate <= stopDate) {
				dateArray.push(new Date(currentDate))
				currentDate = currentDate.addDays(1);
			}
			return dateArray;
		}
		$scope.changeduedate_changed = function(duedate, ID) {
			if (duedate != null) {
				var test = isValidDate(duedate.toString(), $scope.format.toString());
				if (test) {
					var a = $.grep($scope.tempholidays, function(e) {
						return e == dateFormat(duedate, $scope.format);
					});
					if (a != null) {
						if (a.length > 0) {
						    bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
							$scope.fields["DatePart_" + ID] = "";
						}
					}
				} else {
					$scope.fields["DatePart_" + ID] = "";
					bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
				}
			}
		}
		$scope.changeperioddate_changed = function(date, datetype) {
			if (date != null) {
				var test = isValidDate(date.toString(), $scope.format.toString());
				if (test) {
					var a = $.grep($scope.tempholidays, function(e) {
						return e == dateFormat(date, $scope.format);
					});
					if (a != null) {
						if (a.length > 0) {
						    bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
							if (datetype == "StartDate") $scope.items[0].startDate = "";
							else $scope.item[0].endDate = "";
						}
					}
				} else {
					if (datetype == "StartDate") $scope.items[0].startDate = "";
					else $scope.item[0].endDate = "";
					bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
				}
			}
		}

		function isValidDate(dateval, dateformat) {
			var formatlen;
			var defaultdateVal = [];
			defaultdateVal = dateval.length;
			formatlen = dateformat.length;
			if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
			else return false;
		};
		$scope.$on("$destroy", function() {
			$timeout.cancel(NewsFeedUniqueTimerForTask);
			RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.taskCtrl']"));
		});
	}
	app.controller("mui.planningtool.default.detail.taskCtrl", ['$scope', '$location', '$resource', '$timeout', '$stateParams', '$cookies', '$window', '$compile', '$attrs', '$sce', '$translate', 'TaskNewService', '$modal', muiplanningtooldefaultdetailtaskCtrl]);
})(angular, app);