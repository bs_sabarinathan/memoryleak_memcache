﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BrandSystems.Cryptography;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Core.User;
using System.Net.Mail;
using System.Web;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Access;
using System.Data.Common;
using System.Collections;
using System.Text.RegularExpressions;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Core.Managers;
using System.Xml;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Utility;

namespace BrandSystems.Marcom.Utility
{
    public class ClsDb
    {

        public readonly string strtenantconn;
        public readonly string reportstrcon;
        public string Tenanthost = "";
        public ClsDb(string TenantHost)
        {
            Tenanthost = TenantHost;
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
            strtenantconn = xn["conn"].InnerText;
            reportstrcon = xn["ReportServerconn"].InnerText;
        }

        public int CheckValidUserID(string query, CommandType type, string pwd)
        {
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************CheckValidUserID using clsDb  Started " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
            DataTable dt = new DataTable();
            dt.Columns.Add("password");
            dt.Columns.Add("saltpassword");
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************clsDb Connection open //////" + query + "//////// " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        if ((BCrypt.CheckBytePassword(pwd, dr["PasswordSalt"].ToString(), (byte[])dr["Password"]) == true) || pwd.Length == 0)
                        {
                            return (int)dr["Id"];
                        }
                        else
                        {
                            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************clsDB Salt password is wrong " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                        }
                    }
                    else
                    {
                        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************ clsDb  unable to get user data " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    }

                }
                catch (Exception ex)
                {
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************CheckValidUserID using clsDb failed due to " + ex.Message + " " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                }
            }
            return 0;
        }

        public IUser GetUserByID(string query, CommandType type)
        {


            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        dr.Read();

                        IUser user = new BrandSystems.Marcom.Core.User.User();
                        user.Email = dr["Email"].ToString();
                        user.FirstName = System.Uri.UnescapeDataString(dr["FirstName"].ToString());
                        user.Id = (int)dr["Id"];
                        user.Image = (dr["Image"] == null ? "" : dr["Image"].ToString());
                        user.Language = dr["Language"].ToString();
                        user.LastName = System.Uri.UnescapeDataString(dr["LastName"].ToString());
                        user.Password = (byte[])dr["Password"];
                        user.SaltPassword = dr["PasswordSalt"].ToString();
                        user.StartPage = (int?)dr["StartPage"];
                        user.TimeZone = dr["TimeZone"].ToString();
                        user.UserName = dr["UserName"].ToString();
                        user.Feedselection = dr["FeedSelection"].ToString();
                        user.ListOfUserGlobalRoles = new List<GlobalAcl>();
                        user.ListOfUserGlobalRoles = getuserglobalaccess((int)dr["Id"]);

                        return user;
                    }

                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }

        public IUser GetEmailByID(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        IUser user = new BrandSystems.Marcom.Core.User.User();
                        user.Email = dr["Email"].ToString();
                        return user;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }

        public IList<dynamic> GetAdminsEmail(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        IList<dynamic> user = new List<dynamic>();
                        while (dr.Read())
                        {
                            user.Add(dr["Email"].ToString());
                        }
                        return user;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }

        public int GetPasswordCountByPwd(string query, CommandType type, string pwd)
        {
            int count = 0;
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if ((BCrypt.CheckBytePassword(pwd, dr["PasswordSalt"].ToString(), (byte[])dr["Password"]) == true) || pwd.Length == 0)
                            {
                                count++;
                            }
                            else
                            {
                                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************clsDB Salt password is wrong " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                            }
                        }
                    }
                    else
                    {
                        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************ clsDb  unable to get user data " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            return count;
        }

        public ILoginCheck GetAttemptByEmail(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        dr.Read();

                        ILoginCheck user = new BrandSystems.Marcom.Core.User.LoginCheck();
                        user.Id = (int)dr["attempt"];
                        return user;
                    }

                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }

        public PasswordSetting GetMinuteByEmail(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        dr.Read();

                        PasswordSetting user = new PasswordSetting();
                        user.Duration = (int)dr["LoginTime"];
                        return user;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }


        public IUser UserData(string query, CommandType type, string pwd)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("password");
            dt.Columns.Add("saltpassword");
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();

                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        dr.Read();
                        if ((BCrypt.CheckBytePassword(pwd, dr["PasswordSalt"].ToString(), (byte[])dr["Password"]) == true) || pwd.Length == 0)
                        {
                            IUser user = new BrandSystems.Marcom.Core.User.User();
                            user.Email = dr["Email"].ToString();
                            user.FirstName = System.Uri.UnescapeDataString(dr["FirstName"].ToString());
                            user.Id = (int)dr["Id"];
                            user.Image = (dr["Image"] == null ? "" : dr["Image"].ToString());
                            user.Language = dr["Language"].ToString();
                            user.LastName = System.Uri.UnescapeDataString(dr["LastName"].ToString());
                            user.Password = (byte[])dr["Password"];
                            user.SaltPassword = dr["PasswordSalt"].ToString();
                            user.StartPage = (int?)dr["StartPage"];
                            user.TimeZone = dr["TimeZone"].ToString();
                            user.UserName = dr["UserName"].ToString();
                            user.Feedselection = dr["FeedSelection"].ToString();
                            user.ListOfUserGlobalRoles = new List<GlobalAcl>();
                            user.ListOfUserGlobalRoles = getuserglobalaccess((int)dr["Id"]);
                            return user;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return null;
        }

        public IList<GlobalAcl> getuserglobalaccess(int userid)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd1 = new SqlCommand();
                sqlcmd1.Connection = sqlcon;
                sqlcmd1.CommandType = CommandType.Text;
                sqlcmd1.CommandText = "SELECT * FROM  GetUserGlobalAccess(" + userid + ")";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlcmd1;
                da.Fill(ds);
                IList<GlobalAcl> listofglobalaccess = new List<GlobalAcl>();
                foreach (DataRow prod in ds.Tables[0].Rows)
                {
                    GlobalAcl gacl = new GlobalAcl();
                    gacl.AccessPermission = Convert.ToBoolean(prod.ItemArray[0]);
                    gacl.EntityTypeid = Convert.ToInt32(prod.ItemArray[1]);
                    gacl.Featureid = Convert.ToInt32(prod.ItemArray[2]);
                    gacl.GlobalRoleid = Convert.ToInt32(prod.ItemArray[3]);
                    gacl.Moduleid = Convert.ToInt32(prod.ItemArray[4]);
                    gacl.Id = Convert.ToInt32(prod.ItemArray[5]);
                    listofglobalaccess.Add(gacl);
                }
                return listofglobalaccess;
            }
        }

        public IList<Assets> getAssetSearchFiles()
        {
            IList<Assets> listofglobalaccess = new List<Assets>();
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd1 = new SqlCommand();
                    sqlcmd1.Connection = sqlcon;
                    sqlcmd1.CommandType = CommandType.Text;
                    sqlcmd1.CommandText = "SELECT ID ,activefileid, CASE WHEN ispublish = 1 THEN 'Assetlibrary' WHEN ispublish = 0 THEN 'Attachments' ELSE 'Attachments'  END AS 'type'  FROM dam_asset WHERE isIndexed=0 or  isIndexed is null";
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlcmd1;
                    da.Fill(ds);

                    foreach (DataRow prod in ds.Tables[0].Rows)
                    {
                        Assets gacl = new Assets();
                        gacl.ID = Convert.ToInt32(prod.ItemArray[0]);
                        gacl.ActiveFileID = Convert.ToInt32(prod.ItemArray[1]);
                        gacl.searchtype = Convert.ToString(prod.ItemArray[2]);
                        listofglobalaccess.Add(gacl);
                    }
                    return listofglobalaccess;
                }
            }
            catch (Exception ex)
            {
                return listofglobalaccess;
            }
        }


        public int InsertUpdateAssetIndexData(string query, CommandType type)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                int x = sqlcmd.ExecuteNonQuery();
                return x;
            }
        }

        public bool Resetpwdlink(string query, CommandType type, string Guid)
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                SqlDataReader dr;
                dr = sqlcmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    return ((int)dr["count"] == 0 ? false : true);

                }
            }
            return false;
        }

        public bool DataExist(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                SqlDataReader dr;
                dr = sqlcmd.ExecuteReader();
                if (dr.HasRows)
                {
                    return true;
                }
                //dr.Close();
            }
            return false;
        }

        public int ForgotPwd(string strqry, CommandType type, string emailId, bool isResetpwd, int TenantID, Guid TrackingID, bool islocked = false, int fileguid = 0000)
        {
            BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
            S3Obj = getcloudsettings(TenantID);
            Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = strqry;
                SqlDataReader dr = sqlcmd.ExecuteReader();
                int i = 2;
                if (dr.HasRows)
                {
                    dr.Read();
                    string query = "";
                    int id = (int)dr["Id"];
                    bool issso = (bool)dr["IsSSOUser"];
                    if (issso)
                    {
                        SqlCommand cmd1 = new SqlCommand();
                        cmd1.CommandType = type;
                        cmd1.Connection = sqlcon;
                        cmd1.CommandText = "select count(am.GlobalRoleId) as count from AM_GlobalRole_User am inner join AM_GlobalAcl gacl on am.GlobalRoleId=gacl.GlobalRoleID where am.UserId=" + id + " and FeatureID=29";
                        dr.Close();
                        i = (int)cmd1.ExecuteScalar();
                        if (i == 0)
                        {
                            return i;
                        }
                    }

                    string ToMail = emailId;
                    string ResetPasswordDate;
                    ResetPasswordDate = (DateTime.Now).ToString("yyyy-MM-dd hh:mm");

                    sqlcon.Close();
                    sqlcon.Open();
                    query = "insert into UM_ResetPasswordTrack(TrackingID,UserID,ResetPasswordDate) values('" + TrackingID + "','" + id + "','" + ResetPasswordDate + "') ";
                    sqlcmd.CommandText = query;
                    sqlcmd.Connection = sqlcon;
                    var s = sqlcmd.ExecuteScalar();

                    string Resetpwd = "Reset password";
                    string Forgetpwd = "Forget password";

                    SmtpClient objsmtp = new SmtpClient();
                    MailMessage _email = new MailMessage();
                    StringBuilder body = new StringBuilder();

                    _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                    _email.IsBodyHtml = true;
                    _email.To.Add(ToMail);
                    if (isResetpwd == true) { _email.Subject = Resetpwd; }
                    else { _email.Subject = Forgetpwd; }
                    string xelementName = "ApplicationURL";
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                    var xmlElement = xelementFilepath.Element(xelementName);
                    string str = xmlElement.Value;

                    //  string str = "http" + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                    if (S3Obj.storageType == (int)StorageArea.Amazon)
                    {
                        string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                        body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + cloudUrl + "logo.png'/></p><hr>");
                    }
                    else
                    {
                        body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + str + GetTenantFilePath(TenantID) + "/logo.png'/></p><hr>");
                    }
                    body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");

                    if (isResetpwd)
                    {
                        if (islocked)
                        {
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'><b>Your Verification Code is :" + fileguid + " </b> .</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'>Please click the link below and Enter the Verification Code to reset your password. That will take you to a web page where you can create a new password.</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Please note that the link will expire in 24hours after this email was sent.</p>");
                            body.Append(@"<a style='font-size: 10pt;color: #00B0F0;padding-left: 20px;' href='" + str + "/LockPasswordReset.html?TrackID=" + TrackingID + "'>Click here to set a new password</a>");
                        }
                        else
                        {
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'>Please click the link below to reset your password. That will take you to a web page where you can create a new password.</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Please note that the link will expire in 24hours after this email was sent.</p>");
                            body.Append(@"<a style='font-size: 10pt;color: #00B0F0;padding-left: 20px;' href='" + str + "/ResetPassword.html?TrackID=" + TrackingID + "'>Click here to set a new password</a>");
                        }
                    }
                    else
                    {
                        body.Append(@"<a style='font-size: 10pt;color: #00B0F0;padding-left: 20px;' href='" + str + "/ForgetPassword.html?TrackID=" + TrackingID + "'>Click here to set a new password</a>");
                    }
                    body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from marcom manager platform.You cannot respond to this mail in any form. If you encounter any problem with Marcom Manager, please contact the helpdesk administrator.</span><br><br>");
                    body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                    body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged and it is intended for the addressee(s) only.Access to this e-mail by anyone else is unauthorised. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorised and may be unlawful.  If you are not an addressee, please contact the helpdesk administrator. </span></div>");
                    _email.Body = body.ToString();

                    try
                    {
                        objsmtp.Send(_email);

                        return i;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return -1;
        }

        public IList<string> GetLoginLogoDetails(int TenantID)
        {
            try
            {
                BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
                S3Obj = getcloudsettings(TenantID);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    string title = ""; IList<string> logoDetails = new List<string>();
                    string xelementName = "LogoSettings";
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                    var xmlElement = xelementFilepath.Element(xelementName);
                    foreach (var c in xmlElement.Descendants())
                    {
                        if (c.Name == "Title")
                        {
                            title = c.Value;
                            logoDetails.Add(title);
                        }
                    }
                    //add tenant specific logo path
                    if (S3Obj.storageType == (int)StorageArea.Amazon)
                    {
                        logoDetails.Add((S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID) + "logo.png").Replace("\\", "/"));
                    }
                    else
                    {
                        logoDetails.Add(GetTenantFilePath(TenantID) + "logo.png");
                    }
                    return logoDetails;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public string GetClientIntranetUrl(string TenantHost)
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    int TenantID = GetTenantIDbyHost(TenantHost);
                    Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);

                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                    //The Key is root node current Settings
                    string xelementName = "IntranetLoginUrl";
                    var xmlElement = xelementFilepath.Element(xelementName);
                    string path = "";
                    if (xmlElement != null)
                        if (xmlElement.Value.Length > 0)
                            path = xmlElement.Value;

                    return path;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public int RegisterData(JObject jobj, string TenantHost)
        {
            int userpendingId = 0;
            try
            {
                BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
                int TenantID = GetTenantIDbyHost(TenantHost);
                S3Obj = getcloudsettings(TenantID);
                PasswordSetting passwordSetting = Helper.GetPasswordSetting(TenantHost);
                StringBuilder sbPasswordRegx = new StringBuilder(string.Empty);

                //min and max
                sbPasswordRegx.Append(@"(?=^.{" + passwordSetting.MinLength + "," + passwordSetting.MaxLength + "}$)");

                //numbers length
                sbPasswordRegx.Append(@"(?=(?:.*?\d){" + passwordSetting.NumsLength + "})");

                //a-z characters
                sbPasswordRegx.Append(@"(?=.*[a-z])");

                //A-Z length
                sbPasswordRegx.Append(@"(?=(?:.*?[A-Z]){" + passwordSetting.UpperLength + "})");

                //special characters length
                sbPasswordRegx.Append(@"(?=(?:.*?[" + passwordSetting.SpecialChars + "]){" + passwordSetting.SpecialLength + "})");

                //(?!.*\s) - no spaces
                //[0-9a-zA-Z!@#$%*()_+^&] -- valid characters
                sbPasswordRegx.Append(@"(?!.*\s)[0-9a-zA-Z" + passwordSetting.SpecialChars + "]*$");

                if (Regex.IsMatch((string)jobj["Password"], sbPasswordRegx.ToString()))
                {
                    string saltpwd = "";
                    string password = "";
                    string query = "";
                    bool userexists = false;
                    bool userexists1 = false;
                    SqlCommand sqlcmd1 = new SqlCommand();
                    string query1 = "Select id from UM_User where Email='" + (string)jobj["Email"] + "'";
                    userexists = DataExist(query1, CommandType.Text);
                    string query2 = "Select id from UM_Pending_User where Email='" + (string)jobj["Email"] + "'";
                    userexists1 = DataExist(query2, CommandType.Text);
                    using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                    {
                        sqlcon.Open();
                        if (userexists1 == true)
                        {
                            return userpendingId;
                        }
                        if (userexists == false)
                        {
                            password = (string)jobj["Password"];
                            sqlcmd1.Connection = sqlcon;
                            sqlcmd1.CommandType = CommandType.Text;
                            saltpwd = BCrypt.GenerateSalt();
                            sqlcmd1.Parameters.Add("@saltpwd", SqlDbType.VarChar).Value = saltpwd;
                            sqlcmd1.Parameters.Add("@pwd", SqlDbType.VarBinary).Value = BCrypt.HashByteArrayPassword(password, saltpwd);
                            query = "insert into um_pending_user(firstname,lastname,username,password,PasswordSalt,email,title,department) values('" + (string)jobj["FirstName"] + "','" + (string)jobj["LastName"] + "','" + (string)jobj["UserName"] + "', @pwd ,@saltpwd ,'" + (string)jobj["Email"] + "','" + (string)jobj["Title"] + "','" + (string)jobj["Department"] + "');Select Scope_Identity()";
                            sqlcmd1.CommandText = query;
                            userpendingId = Convert.ToInt32(sqlcmd1.ExecuteScalar());
                            XDocument adminXmlDoc1 = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                            XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                            XElement xelementFilepath1 = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                            //The Key is root node current Settings
                            string xelementName = "MailConfig";

                            var xmlElement = xelementFilepath.Element(xelementName);
                            string[] arr = null;
                            foreach (var ele in xmlElement.Descendants())
                            {
                                if (ele.Name.ToString() == "AdminEmailID")
                                {
                                    arr = ele.Value.Trim().Split(';');
                                }
                            }

                            //   string[] arr = xmlElement.Value.Trim().Split(';');

                            string Subject = "New User Request";
                            SmtpClient objsmtp = new SmtpClient();
                            MailMessage _email = new MailMessage();
                            StringBuilder body = new StringBuilder();

                            _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                            _email.IsBodyHtml = true;
                            _email.Subject = Subject;

                            //   string str = "http" + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;


                            //The Key is root node current Settings
                            string xelementName1 = "ApplicationURL";
                            var xmlElement1 = xelementFilepath1.Element(xelementName1);
                            string str = xmlElement1.Value;
                            if (S3Obj.storageType == (int)StorageArea.Amazon)
                            {
                                string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                                body.Append("<div style='width: 600px;'><p><img src='" + cloudUrl + "logo.png'/></p><hr>");
                            }
                            else
                            {
                                body.Append("<div style='width: 600px;'><p><img src='" + str + GetTenantFilePathByHost(TenantHost) + "/logo.png'/></p><hr>");
                            }
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> You have new user request for approval by " + (string)jobj["FirstName"] + " " + (string)jobj["LastName"] + ".</p>");
                            body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from Brandsystems Planning tool. You can´t respond to this mail in any form. If you encounter any problem with Brandsystems Planning tool, please contact the Helpdesk, at email:marcom.noreply@brandsystems.in</span><br><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged.It is intended for the addressee(s) only. Access to this e-mail by anyone else is unauthorized. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorized and may be unlawful. If you are not an addressee,please inform the sender immediately.</span></div>");
                            _email.Body = body.ToString();
                            foreach (string ToMail in arr)
                            {
                                _email.To.Add(ToMail);
                            }
                            //objsmtp.Send(_email);
                            System.Threading.Tasks.Task sendmail = new System.Threading.Tasks.Task(() => objsmtp.Send(_email));
                            sendmail.Start();
                            return userpendingId;
                        }
                        else
                        {
                            return userpendingId;
                        }

                    }
                }
                else
                {
                    return -100;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public string SSORegisterData(JObject jobj, string TenantHost)
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    string saltpwd = "";
                    string query = "";
                    bool userexists = false;
                    SqlCommand sqlcmd1 = new SqlCommand();

                    //Decrypt token string and read the data
                    string token = (string)jobj["token"];
                    List<byte> Key = new List<byte>();

                    int TenantID = GetTenantIDbyHost(TenantHost);
                    XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                    //The Key is root node current Settings
                    string xelementName = "SSO";
                    var xmlElement = xelementFilepath.Element(xelementName);

                    BrandSystems.Marcom.Core.Common.SSO ssodata = new BrandSystems.Marcom.Core.Common.SSO();
                    foreach (var des in xmlElement.Descendants())
                    {
                        switch (des.Name.ToString())
                        {
                            case "Key":
                                ssodata.key = des.Value;
                                break;
                            case "IV":
                                ssodata.IV = des.Value;
                                break;
                            case "Algorithm":
                                ssodata.AlgorithmValue = des.Value;
                                break;
                            case "PaddingMode":
                                if (!(des.Value == "--Select--")) //Prabhu: we need to do proper validation.
                                    ssodata.PaddingModeValue = des.Value;
                                break;
                            case "CipherMode":
                                if (!(des.Value == "--Select--"))
                                    ssodata.CipherModeValue = des.Value;
                                break;
                            case "SSOTokenMode":
                                ssodata.SSOTokenModeValue = des.Value;
                                break;
                            case "SSOTimeDifference":
                                ssodata.SSOTimeDifference = des.Value;
                                break;
                            case "SSODefaultGroups":
                                ssodata.UserGroupsvalue = des.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (string id in ssodata.key.ToString().Split(','))
                    {
                        Key.Add(byte.Parse(id));
                    }

                    BrandSystems.Cryptography.Security.Key = Key.ToArray();

                    List<byte> IV = new List<byte>();

                    foreach (string id in ssodata.IV.ToString().Split(','))
                    {
                        IV.Add(byte.Parse(id));
                    }
                    BrandSystems.Cryptography.Security.IV = IV.ToArray();

                    BrandSystems.Cryptography.Security.Algo = (BrandSystems.Cryptography.Security.AlgoType)Enum.Parse(typeof(BrandSystems.Cryptography.Security.AlgoType), ssodata.AlgorithmValue.ToString()); //Security.AlgoType.TripleDES; //(BrandSystems.Cryptography.Security.AlgoType)Enum.Parse(typeof(BrandSystems.Cryptography.Security.AlgoType), Application.Item("AlgoType").ToString());

                    if (ssodata.PaddingModeValue != null)
                    {
                        BrandSystems.Cryptography.Security.PaddingMode = (System.Security.Cryptography.PaddingMode)Enum.Parse(typeof(System.Security.Cryptography.PaddingMode), ssodata.PaddingModeValue.ToString());
                    }

                    if (ssodata.CipherModeValue != null)
                    {
                        BrandSystems.Cryptography.Security.CipherMode = (System.Security.Cryptography.CipherMode)Enum.Parse(typeof(System.Security.Cryptography.CipherMode), ssodata.CipherModeValue.ToString());
                    }
                    string PlainText = BrandSystems.Cryptography.Security.DecryptString(token).Replace("\0", "");

                    //Validate the token date and time
                    PlainText = HttpContext.Current.Server.UrlDecode(PlainText);

                    List<string> TokenInfo = new List<string>();
                    List<KeyValuePair<string, string>> TokenInfoPair = new List<KeyValuePair<string, string>>();
                    TokenInfo.Add(PlainText.Split(')')[0] + ")");
                    if (PlainText.Split(')')[1].Split('&').Length > 1)
                    {
                        TokenInfo.Add(PlainText.Split(')')[1].Split('&')[1]);
                    }
                    if (PlainText.Split(')')[1].Split('&').Length > 2)
                    {
                        TokenInfo.Add(PlainText.Split(')')[1].Split('&')[2]);
                    }

                    foreach (var Item in TokenInfo)
                    {

                        if (Item.Split('=')[0].ToUpper() == "USERCREDENTIAL")
                        {
                            TokenInfoPair.Add(new KeyValuePair<string, string>(Item.Split('=')[0], Item.Split('(')[1].Replace(")", "")));
                        }
                        else
                        {
                            TokenInfoPair.Add(new KeyValuePair<string, string>(Item.Split('=')[0], Item.Split('=')[1]));
                        }
                    }

                    TimeSpan TimeDiff = default(TimeSpan);
                    foreach (var Item in TokenInfoPair)
                    {
                        if (Item.Key.ToUpper() == "TIMESTAMP")
                        {
                            DateTime StartTime = DateTime.UtcNow;
                            DateTime GeneratedTime = ConvertTimestamp(double.Parse(Item.Value));

                            TimeDiff = StartTime.Subtract(GeneratedTime);
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }

                    if (TimeDiff.TotalMinutes <= Int32.Parse(ssodata.SSOTimeDifference.ToString()))
                    {
                        string UserCredencial = null;
                        foreach (var Item in TokenInfoPair)
                        {
                            if (Item.Key.ToUpper() == "USERCREDENTIAL")
                            {
                                UserCredencial = Item.Value;
                                break;
                            }
                        }

                        //Check we have Text or XML and proceed accrodingly
                        string userid = null;
                        string firstname = null;
                        string lastname = null;
                        string email = null;
                        string passwordtext = null;
                        string usergroups = "";//Application.Item("SSODefaultGroups"); we need to check
                        if (usergroups == null)
                        {
                            usergroups = "";
                        }

                        //var Mode = "TEXT"; //Application.Item("SSOTokenMode").ToString();
                        var Mode = ssodata.SSOTokenModeValue.ToString();
                        if (Mode == "TEXT")
                        {
                            List<KeyValuePair<string, string>> UserInfo = new List<KeyValuePair<string, string>>();

                            foreach (var Item in UserCredencial.Split('&'))
                            {
                                UserInfo.Add(new KeyValuePair<string, string>(Item.Split('=')[0], Item.Split('=')[1]));
                            }

                            foreach (var item in UserInfo)
                            {
                                switch (item.Key.ToLower())
                                {
                                    case "uid":
                                        userid = item.Value;
                                        break;
                                    case "e":
                                        email = item.Value;
                                        break;
                                    case "fn":
                                        firstname = item.Value;
                                        break;
                                    case "ln":
                                        lastname = item.Value;
                                        break;
                                    case "pw":
                                        passwordtext = item.Value;
                                        break;
                                }
                            }
                        }
                        else if (Mode == "XML")
                        {
                            UserCredencial = UserCredencial.Replace(" BS_auth.dtd", "");

                            XDocument Doc = XDocument.Parse(UserCredencial);
                        }

                        if (email == null)
                        {
                            email = userid;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(email))
                            {
                                email = userid;
                            }
                        }

                        if (firstname == null)
                        {
                            firstname = "";
                        }

                        if (lastname == null)
                        {
                            lastname = userid.Split('@')[0];
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(lastname))
                            {
                                lastname = userid.Split('@')[0];
                            }
                        }

                        if (passwordtext == null)
                        {
                            passwordtext = "";
                        }

                        if (IsValidEmail(email))
                        {
                            string query1 = "Select id from UM_User where Email='" + (string)email + "'";
                            userexists = DataExist(query1, CommandType.Text);


                            if (userexists)
                            {
                                //retrive user information
                                return (string)email;
                            }
                            else
                            {
                                //create user and retrive user information
                                sqlcmd1.Connection = sqlcon;
                                sqlcmd1.CommandType = CommandType.Text;
                                saltpwd = BCrypt.GenerateSalt();
                                sqlcmd1.Parameters.Add("@saltpwd", SqlDbType.VarChar).Value = saltpwd;
                                sqlcmd1.Parameters.Add("@pwd", SqlDbType.VarBinary).Value = BCrypt.HashByteArrayPassword(passwordtext, saltpwd);
                                query =
                                    "DECLARE @feedSelection VARCHAR(MAX)" +
                                    "SELECT @feedSelection = COALESCE(@feedSelection+',' ,'') + cast(id as nvarchar(50))" +
                                    "FROM MM_EntityType  where IsAssociate=0" +
                                    "; INSERT INTO [UM_User]([FirstName],[LastName],[UserName],[Password],[PasswordSalt],[Email],[Language] ,[TimeZone],[StartPage],[DashboardTemplateID],[IsDashboardCustomized],[FeedSelection],[IsSSOUser])VALUES('" +
                                    firstname + "','" + lastname + "','" + userid + "',@pwd ,@saltpwd,'" + email +
                                    "','English','+01:00',0,0,0,@feedSelection,1) ;  select @@identity;";
                                sqlcmd1.CommandText = query;
                                SqlDataAdapter da = new SqlDataAdapter();
                                da.SelectCommand = sqlcmd1;
                                DataSet ds = new DataSet();
                                da.Fill(ds);
                                if (ds.Tables[0] != null)
                                {
                                    SqlCommand insertrole = new SqlCommand();
                                    insertrole.Connection = sqlcon;
                                    foreach (var c in ssodata.UserGroupsvalue.Split(','))
                                    {
                                        insertrole.CommandText = "insert into AM_GlobalRole_User(GlobalRoleId,UserId)  values (" + c + "," + ds.Tables[0].Rows[0][0] + ")";
                                        insertrole.CommandType = CommandType.Text;
                                        insertrole.ExecuteNonQuery();
                                    }
                                    SqlCommand insertDefaultAppNotification = new SqlCommand();
                                    insertDefaultAppNotification.Connection = sqlcon;
                                    string defaultsubscriptionquery = "DECLARE @subscrlist VARCHAR(MAX)" +
                                            "DECLARE @mailsubscrlist VARCHAR(MAX)" +
                                            "SELECT @subscrlist = COALESCE(@subscrlist+',' ,'') + cast(id as nvarchar(50))" +
                                            "FROM CM_SubscriptionType  where isAppDefault=1" +
                                            "SELECT @mailsubscrlist = COALESCE(@mailsubscrlist+',' ,'') + cast(id as nvarchar(50))" +
                                            "FROM CM_SubscriptionType  where isMailDefault=1" +
                                            "insert into CM_UserDefaultSubscription values('" + ds.Tables[0].Rows[0][0] + "',@subscrlist,@mailsubscrlist)";
                                    insertDefaultAppNotification.CommandText = defaultsubscriptionquery;
                                    insertDefaultAppNotification.CommandType = CommandType.Text;
                                    insertDefaultAppNotification.ExecuteNonQuery();
                                }
                                return (string)email;
                            }
                        }
                        else
                        {
                            //return null;
                        }
                    }
                    else
                    {
                        //Log("SSO", "Timetaamp is invalid");
                        //return null;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public DataSet MailData(string query, CommandType type)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = sqlcmd;
                dataAdapter.Fill(dataSet);
            }
            return dataSet;
        }

        public int InsertUpdateMailData(string query, CommandType type)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                int x = sqlcmd.ExecuteNonQuery();
                return x;
            }
        }

        public DataSet reportserverData(string query, CommandType type)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection reportsqlcon = new SqlConnection(reportstrcon))
            {
                try
                {
                    reportsqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = reportsqlcon;
                    sqlcmd.CommandText = query;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = sqlcmd;
                    dataAdapter.Fill(dataSet);

                    return dataSet;
                }
                catch (Exception ex)
                {
                }
            }
            return dataSet;
        }

        public bool reportserverupdateData(string query, CommandType type)
        {
            DataTable dt = new DataTable();
            using (SqlConnection reportsqlcon = new SqlConnection(reportstrcon))
            {
                try
                {
                    reportsqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = reportsqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    var s = sqlcmd.ExecuteScalar();
                    return true;
                }
                catch (Exception ex)
                {
                }
                return false;
            }
        }

        public IList<IDataView> Dataview_select(string query, CommandType type)
        {
            IList<IDataView> listData = new List<IDataView>();
            IDataView Dview;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection reportsqlcon = new SqlConnection(reportstrcon))
            {
                try
                {
                    reportsqlcon.Open();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = reportsqlcon;
                    sqlcmd.CommandText = query;
                    DataTable dt = new DataTable();
                    DataSet dataSet = new DataSet();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = sqlcmd;
                    dataAdapter.Fill(dataSet);

                    foreach (DataRow dr in dataSet.Tables[0].Rows)
                    {
                        Dview = new BrandSystems.Marcom.Core.Report.DataView();
                        Dview.DataviewID = Convert.ToInt32(dr.ItemArray[0]);
                        Dview.Dataviewname = Convert.ToString(dr.ItemArray[1]);
                        listData.Add(Dview);
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return listData;
        }

        public DateTime ConvertTimestamp(double timestamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(timestamp);
        }

        public bool IsValidEmail(string emailAddress)
        {
            string pattern = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Match emailAddressMatch = Regex.Match(emailAddress, pattern);
            if (emailAddressMatch.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool[] pwdreset(string strqry, CommandType type, string TrackingID, string Pwd, string oldPassword, bool flag, bool fwdpwd, int TenantID)
        {
            BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
            S3Obj = getcloudsettings(TenantID);
            Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
            try
            {
                bool[] retunBool = new bool[2];
                bool flagcheck = flag;
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = "SELECT um.UserID as UserID ,us.Email as Email,us.Password as Password,us.PasswordSalt as PasswordSalt  FROM UM_ResetPasswordTrack um inner join UM_User us on um.UserID=us.ID  where  um.TrackingID='" + TrackingID + "'";
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    int userid = 0;
                    string emailID = "";
                    {
                        dr.Read();
                        userid = (int)dr["UserID"];
                        emailID = (string)dr["Email"];
                    }

                    if (userid > 0 && (fwdpwd == false ? (BCrypt.CheckBytePassword(oldPassword, dr["PasswordSalt"].ToString(), (byte[])dr["Password"]) == true) : fwdpwd))
                    {
                        sqlcon.Close();
                        sqlcon.Open();
                        sqlcmd.CommandType = type;
                        sqlcmd.Connection = sqlcon;
                        sqlcmd.CommandText = strqry;
                        string saltPwd = BCrypt.GenerateSalt();
                        sqlcmd.Parameters.AddWithValue("@saltpassword", saltPwd);
                        sqlcmd.Parameters.AddWithValue("@password", BCrypt.HashByteArrayPassword(Pwd, saltPwd));
                        sqlcmd.Parameters.AddWithValue("@userID", userid);
                        bool result = (sqlcmd.ExecuteNonQuery() == 1 ? true : false);
                        flagcheck = true;
                        retunBool[0] = flagcheck;
                        retunBool[1] = result;
                        if (result == true)
                        {
                            string Subject = "Password has been reset";
                            SmtpClient objsmtp = new SmtpClient();
                            MailMessage _email = new MailMessage();
                            StringBuilder body = new StringBuilder();
                            string ToMail = emailID;

                            _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                            _email.IsBodyHtml = true;
                            _email.To.Add(ToMail);
                            _email.Subject = Subject;

                            string xelementName1 = "ApplicationURL";
                            XElement xelementFilepath1 = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                            var xmlElement1 = xelementFilepath1.Element(xelementName1);
                            string str = xmlElement1.Value;

                            if (S3Obj.storageType == (int)StorageArea.Amazon)
                            {
                                string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                                body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + cloudUrl + "logo.png'/></p><hr>");
                            }
                            else
                            {
                                body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + str + GetTenantFilePath(TenantID) + "/logo.png'/></p><hr>");
                            }
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> The password for your username <FONT color=\"#00B0F0\"> " + emailID + " </FONT> has been successfully reset.</p>");
                            body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from marcom manager platform.You cannot respond to this mail in any form. If you encounter any problem with Marcom Manager, please contact the helpdesk administrator.</span><br><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged and it is intended for the addressee(s) only.Access to this e-mail by anyone else is unauthorised. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorised and may be unlawful.  If you are not an addressee, please contact the helpdesk administrator. </span></div>");

                            _email.Body = body.ToString();
                            objsmtp.Send(_email);
                        }
                    }
                }
                return retunBool;
            }
            catch
            {
                throw;
            }
        }

        public DataSet GetUserRegistrationAttributes(string query, CommandType type)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = sqlcmd;
                dataAdapter.Fill(dataSet);
            }
            return dataSet;
        }

        public bool InsertUserRegisterDynamicData(JArray jobj)
        {
            int pendinguserid = (int)jobj[0]["PendingID"];
            Dictionary<int, object> dynamicval = new Dictionary<int, object>();
            string colmn = "", val = "", query = "";
            colmn += "ID" + ",";
            val += pendinguserid + ",";

            foreach (var obj in jobj)
            {
                if ((int)obj["AttributeTypeID"] == 1)
                {
                    colmn += "Attr_" + (int)obj["AttributeID"] + ",";
                    val += Convert.ToString((object)obj["NodeID"].ToString()) == "" ? "''" + "," : "'" + (object)obj["NodeID"].ToString() + "'" + ",";
                }
                else if ((int)obj["AttributeTypeID"] == 2)
                {
                    colmn += "Attr_" + (int)obj["AttributeID"] + ",";
                    val += Convert.ToString((object)obj["NodeID"].ToString()) == "" ? "''" + "," : "'" + (object)obj["NodeID"].ToString() + "'" + ",";
                }
                else if ((int)obj["AttributeTypeID"] == 3)
                {
                    colmn += "Attr_" + (int)obj["AttributeID"] + ",";
                    val += Convert.ToString((object)obj["NodeID"].ToString()) == "" ? 0 + "," : (object)obj["NodeID"].ToString() + ",";
                }
                else if ((int)obj["AttributeTypeID"] == 5)
                {
                    colmn += "Attr_" + (int)obj["AttributeID"] + ",";
                    val += Convert.ToString((object)obj["NodeID"].ToString()) == "" ? "''" + "," : "'" + (object)obj["NodeID"].ToString() + "'" + ",";
                }
                else if ((int)obj["AttributeTypeID"] == 11)
                {
                    colmn += "Attr_" + (int)obj["AttributeID"] + ",";
                    val += Convert.ToString((object)obj["NodeID"].ToString()) == "" ? "''" + "," : "'" + (object)obj["NodeID"].ToString() + "'" + ",";
                }
            }
            if (colmn != "")
            {
                colmn = colmn.TrimEnd(',');
                val = val.TrimEnd(',');

                query = "INSERT INTO MM_AttributeRecord_12PendingUser(" + colmn + ") Values(" + val + ")";

                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    try
                    {
                        sqlcon.Open();
                        SqlCommand sqlcmd1 = new SqlCommand();
                        sqlcmd1.Connection = sqlcon;
                        sqlcmd1.CommandType = CommandType.Text;

                        sqlcmd1.CommandText = query;
                        sqlcmd1.ExecuteNonQuery();

                        foreach (var obj in jobj)
                        {
                            if ((int)obj["AttributeTypeID"] == 4)
                            {
                                if ((object)obj["NodeID"].ToString() != "")
                                {
                                    query = "DELETE FROM MM_UserRegistrationMultiSelect WHERE UserID =" + pendinguserid + " AND AttributeID = " + (int)obj["AttributeID"];
                                    SqlCommand sqlcmd = new SqlCommand();
                                    sqlcmd.Connection = sqlcon;
                                    sqlcmd.CommandType = CommandType.Text;

                                    sqlcmd.CommandText = query;
                                    sqlcmd.ExecuteNonQuery();

                                    string stmt = "INSERT INTO MM_UserRegistrationMultiSelect(UserID,AttributeID,OptionID) Values(@UserID, @AttributeID,@OptionID)";

                                    SqlCommand cmd = new SqlCommand(stmt, sqlcon);
                                    cmd.Parameters.Add("@UserID", SqlDbType.Int);
                                    cmd.Parameters.Add("@AttributeID", SqlDbType.Int);
                                    cmd.Parameters.Add("@OptionID", SqlDbType.Int);

                                    for (int j = 0; j < Convert.ToString((object)obj["NodeID"]).ToString().Split(',').Length; j++)
                                    {
                                        cmd.Parameters["@UserID"].Value = pendinguserid;
                                        cmd.Parameters["@AttributeID"].Value = (int)obj["AttributeID"];
                                        cmd.Parameters["@OptionID"].Value = Convert.ToString((object)obj["NodeID"]).ToString().Split(',')[j];

                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    { return false; }
                }
            }

            return true;
        }

        public int SetUpToolUserRegister(string TenantHost)
        {
            BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();

            int TenantID = GetTenantIDbyHost(TenantHost);
            S3Obj = getcloudsettings(TenantID);

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = "SELECT ums.TrackingID, umu.Email FROM [UM_SetUpToolUsersRegister] ums INNER JOIN [dbo].[UM_User] umu ON ums.UserID = umu.ID WHERE ums.SetPassword = 0";
                SqlDataReader dr = sqlcmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string ToMail = dr["Email"].ToString();
                        SmtpClient objsmtp = new SmtpClient();
                        MailMessage _email = new MailMessage();
                        StringBuilder body = new StringBuilder();

                        _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                        _email.IsBodyHtml = true;
                        _email.To.Add(ToMail);
                        _email.Subject = "Set password";

                        XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                        XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                        //The Key is root node current Settings
                        string xelementName = "ApplicationURL";

                        var xmlElement = xelementFilepath.Element(xelementName);
                        string str = xmlElement.Value;

                        if (S3Obj.storageType == (int)StorageArea.Amazon)
                        {
                            string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                            body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + cloudUrl + "logo.png'/></p><hr>");
                        }
                        else
                        {
                            body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + str + GetTenantFilePathByHost(TenantHost) + "/logo.png'/></p><hr>");
                        }
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Please click the link below to set your password. That will take you to a web page where you can create a new password.</p>");
                        body.Append(@"<a style='font-size: 10pt;color: #00B0F0;padding-left: 20px;' href='" + str + "/SetPassword.html?TrackID=" + dr["TrackingID"].ToString() + "'>Click here to set a new password</a>");

                        body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from marcom manager platform.You cannot respond to this mail in any form. If you encounter any problem with Marcom Manager, please contact the helpdesk administrator.</span><br><br>");
                        body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                        body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged and it is intended for the addressee(s) only.Access to this e-mail by anyone else is unauthorised. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorised and may be unlawful.  If you are not an addressee, please contact the helpdesk administrator. </span></div>");
                        _email.Body = body.ToString();

                        try
                        {
                            objsmtp.Send(_email);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                }
            }

            return 1;
        }

        public bool UpdateSetpassword(string strqry, CommandType type, string TrackingID, string Pwd, string oldPassword, bool setpwd)
        {
            try
            {

                string query1 = "select count(*) as count from UM_SetUpToolUsersRegister pt WHERE TrackingID='" + TrackingID + "' and setpassword=1";
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd1 = new SqlCommand();
                    sqlcmd1.CommandType = type;
                    sqlcmd1.Connection = sqlcon;
                    sqlcmd1.CommandText = query1;
                    int count = (int)sqlcmd1.ExecuteScalar();
                    if (count == 1)
                    {
                        return false;
                    }
                    else
                    {
                        sqlcon.Close();
                        sqlcon.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        sqlcmd.CommandType = type;
                        sqlcmd.Connection = sqlcon;
                        sqlcmd.CommandText = "SELECT um.UserID as UserID ,us.Email as Email,us.Password as Password,us.PasswordSalt as PasswordSalt  FROM UM_SetUpToolUsersRegister um inner join UM_User us on um.UserID=us.ID  where  um.TrackingID='" + TrackingID + "'";
                        SqlDataReader dr;
                        dr = sqlcmd.ExecuteReader();
                        int userid = 0;
                        string emailID = "";
                        if (dr.HasRows)
                        {
                            dr.Read();
                            userid = (int)dr["UserID"];
                            emailID = (string)dr["Email"];
                        }
                        if (userid > 0 && (setpwd == false ? (BCrypt.CheckBytePassword(oldPassword, dr["PasswordSalt"].ToString(),
                            (byte[])dr["Password"]) == true) : setpwd))
                        {
                            sqlcon.Close();
                            sqlcon.Open();
                            sqlcmd.CommandType = type;
                            sqlcmd.Connection = sqlcon;
                            sqlcmd.CommandText = strqry;
                            string saltPwd = BCrypt.GenerateSalt();
                            sqlcmd.Parameters.AddWithValue("@saltpassword", saltPwd);
                            sqlcmd.Parameters.AddWithValue("@password", BCrypt.HashByteArrayPassword(Pwd, saltPwd));
                            sqlcmd.Parameters.AddWithValue("@userID", userid);
                            bool result = (sqlcmd.ExecuteNonQuery() > 0 ? true : false);
                        }
                    }
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool IsUserExistsForSetpassword(string query, CommandType type)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                sqlcon.Open();
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = type;
                sqlcmd.Connection = sqlcon;
                sqlcmd.CommandText = query;

                int count = (int)sqlcmd.ExecuteScalar();
                sqlcon.Close();
                if (count == 1)
                {
                    return false;
                }
                return true;
            }
        }

        public string sqlExecuteNonQuery(string query)
        {
            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand sqlcmd1 = new SqlCommand();
                    sqlcmd1.Connection = sqlcon;
                    sqlcmd1.CommandType = CommandType.Text;

                    sqlcmd1.CommandText = query;
                    sqlcmd1.ExecuteNonQuery();
                    return "Valid";
                }
                catch (SqlException ex)
                {
                    string str;

                    //str = "Source:" + ex.Source;
                    str = "Msg " + ex.Number.ToString() + ",";
                    str += "Level " + ex.Class.ToString() + ",";
                    str += "State " + ex.State.ToString() + ",";
                    str += "Line " + ex.LineNumber.ToString();
                    str += "\n" + "<br/> " + ex.Message;
                    return str;
                }
            }
        }


        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }
        public string GetTenantFilePathByHost(string TenantHost)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantFilePathByHostName(TenantHost);
        }
        public int GetTenantIDbyHost(string TenantHost)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantIDByHostName(TenantHost);
        }

        public IList<string> GetLoginTitleLogoBGImage(int TenantID)
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    IList<string> BGImagePath = new List<string>();
                    string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID) + "TitleLogo" + @"\Title.css");
                    BGImagePath.Add(GetTenantFilePath(TenantID) + "TitleLogo" + @"\Title.css");
                    return BGImagePath;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool[] lockpwdreset(string strqry, CommandType type, string TrackingID, string Pwd, string captcha, bool flag, bool fwdpwd, int TenantID)
        {
            try
            {
                bool[] retunBool = new bool[2];
                bool flagcheck = flag;

                BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
                S3Obj = getcloudsettings(TenantID);
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();

                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = "SELECT us.ID as UserID,um.Email as Email,us.Password as Password,us.PasswordSalt as PasswordSalt FROM UM_LoginMails um inner join UM_User us on us.Email=um.Email  where  um.Captcha='" + captcha + "'";
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    int userid = 0;
                    string emailID = "";
                    {
                        dr.Read();
                        userid = (int)dr["UserID"];
                        emailID = (string)dr["Email"];
                        fwdpwd = true;
                    }

                    if (userid > 0 && (fwdpwd == true))
                    {
                        sqlcon.Close();
                        sqlcon.Open();
                        sqlcmd.CommandType = type;
                        sqlcmd.Connection = sqlcon;
                        sqlcmd.CommandText = strqry;
                        string saltPwd = BCrypt.GenerateSalt();
                        sqlcmd.Parameters.AddWithValue("@saltpassword", saltPwd);
                        sqlcmd.Parameters.AddWithValue("@password", BCrypt.HashByteArrayPassword(Pwd, saltPwd));
                        sqlcmd.Parameters.AddWithValue("@userID", userid);
                        bool result = (sqlcmd.ExecuteNonQuery() == 1 ? true : false);
                        flagcheck = true;
                        retunBool[0] = flagcheck;
                        retunBool[1] = result;

                        if (result == true)
                        {
                            Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                            var Req = System.Web.HttpContext.Current.Request.Browser;
                            string ipAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                            string IPNumber = UserManager.Instance.GET_IPnumber(ipAddress).ToString();
                            string PasswordSalt = BCrypt.GenerateSalt();
                            sqlcon.Close();
                            sqlcon.Open();
                            SqlCommand sqlcmd1 = new SqlCommand();
                            sqlcmd1.CommandType = type;
                            sqlcmd1.Connection = sqlcon;
                            sqlcmd1.Parameters.Add("@saltpwd", SqlDbType.VarChar).Value = PasswordSalt;
                            sqlcmd1.Parameters.Add("@pwd", SqlDbType.VarBinary).Value = BCrypt.HashByteArrayPassword(Pwd, PasswordSalt);
                            sqlcmd1.CommandText = "INSERT INTO [dbo].[UM_InsertUserDetails]([UserID],[Email],[Password],[PasswordSalt],[CreationTime],[IPAddress],[IPNumber],[OS])VALUES('" + userid + "','" + emailID + "', @pwd,@saltpwd,'" + DateTime.Now + "','" + ipAddress + "','" + IPNumber + "','" + Req.Platform + "')";
                            SqlDataReader dr1;
                            dr1 = sqlcmd1.ExecuteReader();
                            sqlcon.Close();
                            IUser userEmail = GetEmailByID("SELECT Email FROM UM_LoginMails WHERE Captcha='" + captcha + "'", CommandType.Text);
                            var Email = (userEmail == null) ? "" : (string)userEmail.Email;
                            bool ValidLoginAfterInvalid = marcommanager.UserManager.ValidLoginAfterInvalid(Email);
                            string Subject = "Password has been reset";
                            SmtpClient objsmtp = new SmtpClient();
                            MailMessage _email = new MailMessage();
                            StringBuilder body = new StringBuilder();
                            string ToMail = emailID;

                            _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                            _email.IsBodyHtml = true;
                            _email.To.Add(ToMail);
                            _email.Subject = Subject;
                            string xelementName1 = "ApplicationURL";
                            XElement xelementFilepath1 = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                            var xmlElement1 = xelementFilepath1.Element(xelementName1);
                            string str = xmlElement1.Value;

                            if (S3Obj.storageType == (int)StorageArea.Amazon)
                            {
                                string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                                body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + cloudUrl + "logo.png'/></p><hr>");
                            }
                            else
                            {
                                body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + str + GetTenantFilePath(TenantID) + "/logo.png'/></p><hr>");
                            }
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");
                            body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> The password for your username <FONT color=\"#00B0F0\"> " + emailID + " </FONT> has been successfully reset.</p>");
                            body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from marcom manager platform.You cannot respond to this mail in any form. If you encounter any problem with Marcom Manager, please contact the helpdesk administrator.</span><br><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                            body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged and it is intended for the addressee(s) only.Access to this e-mail by anyone else is unauthorised. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorised and may be unlawful.  If you are not an addressee, please contact the helpdesk administrator. </span></div>");

                            _email.Body = body.ToString();
                            objsmtp.Send(_email);
                        }

                    }
                }
                return retunBool;
            }
            catch
            {

                throw;
            }
        }

        public bool UserNotificationMail(string Email, int TenantID, string osPlatform, string ipAddress, bool isadminnotify = false, string usermail = "")
        {
            Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
            BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
            S3Obj = getcloudsettings(TenantID);
            try
            {
                IUser a = GetUserByID("SELECT * FROM UM_User WHERE Email='" + Email + "'", CommandType.Text);
                if (a.Email.Equals(Email))
                {
                    string xelementName = "MailConfig";
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                    XDocument adminXmlDoc1 = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                    XElement xelementFilepath1 = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                    var xmlElement = xelementFilepath.Element(xelementName);
                    string[] arr = null;
                    foreach (var ele in xmlElement.Descendants())
                    {
                        if (ele.Name.ToString() == "AdminEmailID")
                        {
                            arr = ele.Value.Trim().Split(';');
                        }
                    }
                    string Subject = "Unsuccessful login attempt Notification";
                    SmtpClient objsmtp = new SmtpClient();
                    MailMessage _email = new MailMessage();
                    StringBuilder body = new StringBuilder();
                    string ToMail = Email;
                    _email.Subject = Subject;

                    //The Key is root node current Settings
                    string xelementName1 = "ApplicationURL";
                    var xmlElement1 = xelementFilepath1.Element(xelementName1);
                    string str = xmlElement1.Value;

                    _email.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"]);
                    _email.IsBodyHtml = true;
                    _email.To.Add(ToMail);
                    _email.Subject = Subject;


                    if (S3Obj.storageType == (int)StorageArea.Amazon)
                    {
                        string cloudUrl = (S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                        body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + cloudUrl + "logo.png'/></p><hr>");
                    }
                    else
                    {
                        body.Append("<div style='width: 600px;'><p><img style='height:45px;' src='" + str + GetTenantFilePath(TenantID) + "/logo.png'/></p><hr>");
                    }
                    if (isadminnotify)
                    {
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Administrator,</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> User account <FONT color=\"#00B0F0\"> <u>" + usermail + "</u></FONT> is recently locked due to unsuccessful login attempts.</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'><b>Login attempt details</b><br/>Date & Time : " + DateTime.Now.ToString("F") + " <br/>IP Address : " + ipAddress + " <br/>Operating System : " + osPlatform + "</p>");
                    }
                    else
                    {
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Dear Member,</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> Your user account <FONT color=\"#00B0F0\"> <u>" + Email + "</u> </FONT> is recently locked due to unsuccessful login attempts.</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'><b>Login attempt details</b><br/>Date & Time : " + DateTime.Now.ToString("F") + " <br/>IP Address : " + ipAddress + " <br/>Operating System : " + osPlatform + "</p>");
                        body.Append("<p style='font-size: 13px;color: #4A4A4A;padding-left: 20px;'> If you have not tried this activity,Please contact Administrator or change your password as soon as possible</p>");
                        body.Append("<hr><span style='font-size: 12px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'>This is a autogenerated mail from marcom manager platform.You cannot respond to this mail in any form. If you encounter any problem with Marcom Manager, please contact the helpdesk administrator.</span><br><br>");
                        body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Legal Notice:</span><br>");
                        body.Append("<span style='font-size: 11px;color: #999999;font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;'><span>Unless expressly stated otherwise, this message is confidential and may be privileged and it is intended for the addressee(s) only.Access to this e-mail by anyone else is unauthorised. If you are not an addressee, any disclosure or copying of the contents of this e-mail or any action taken (or not taken) in reliance on it is unauthorised and may be unlawful.  If you are not an addressee, please contact the helpdesk administrator. </span></div>");
                    }
                    _email.Body = body.ToString();
                    objsmtp.Send(_email);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public string SAMLUserRegistration(string TenantHost, string email, string firstname = "", string lastname = "", string passwordtext = "", List<string> SAMLRoles = null)
        {
            try
            {
                int TenantID = GetTenantIDbyHost(TenantHost);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                //The Key is root node current Settings
                string xelementName = "SSO";
                var xmlElement = xelementFilepath.Element(xelementName);
                BrandSystems.Marcom.Core.Common.SSO ssodata = new BrandSystems.Marcom.Core.Common.SSO();
                ssodata.UserglobalAccess = new List<int>();
                foreach (var des in xmlElement.Descendants())
                {
                    switch (des.Name.ToString())
                    {

                        case "SSODefaultGroups":
                            ssodata.UserGroupsvalue = des.Value.ToString();
                            break;
                        case "SSOGlobalAccess":
                            if (des.Value != null && des.Value != "")
                            {
                                ssodata.UserglobalAccess.Add(Convert.ToInt32(des.Value));
                            }
                            break;
                        default:
                            break;
                    }
                }

                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();


                    foreach (var role in SAMLRoles)
                    {
                        SqlCommand SAMLRolesCmd = new SqlCommand();
                        SAMLRolesCmd.CommandType = CommandType.Text;
                        SAMLRolesCmd.Connection = sqlcon;
                        SAMLRolesCmd.CommandText = "SELECT TOP 1 * FROM AM_GlobalRole WHERE Caption='" + role.ToLower().ToString() + "'";
                        SqlDataReader readSAMLRoles;
                        using (readSAMLRoles = SAMLRolesCmd.ExecuteReader())
                        {
                            while (readSAMLRoles.Read())
                            {
                                if (readSAMLRoles.HasRows)
                                {
                                    ssodata.UserglobalAccess.Add(Convert.ToInt32(readSAMLRoles["ID"].ToString()));
                                }
                            }
                        }

                    }

                    string saltpwd = "";

                    string query = "";
                    bool userexists = false;
                    SqlCommand sqlcmd1 = new SqlCommand();

                    if (IsValidEmail(email))
                    {
                        string query1 = "Select id from UM_User where Email='" + (string)email + "'";
                        userexists = DataExist(query1, CommandType.Text);
                        if (userexists)
                        {
                            return (string)email;
                        }
                        else
                        {
                            //create user and retrive user information
                            sqlcmd1.Connection = sqlcon;
                            sqlcmd1.CommandType = CommandType.Text;
                            saltpwd = BCrypt.GenerateSalt();
                            sqlcmd1.Parameters.Add("@saltpwd", SqlDbType.VarChar).Value = saltpwd;
                            sqlcmd1.Parameters.Add("@pwd", SqlDbType.VarBinary).Value = BCrypt.HashByteArrayPassword(passwordtext, saltpwd);
                            query =
                                "DECLARE @feedSelection VARCHAR(MAX)" +
                                "SELECT @feedSelection = COALESCE(@feedSelection+',' ,'') + cast(id as nvarchar(50))" +
                                "FROM MM_EntityType  where IsAssociate=0" +
                                "; INSERT INTO [UM_User]([FirstName],[LastName],[UserName],[Password],[PasswordSalt],[Email],[Language] ,[TimeZone],[StartPage],[DashboardTemplateID],[IsDashboardCustomized],[FeedSelection],[IsSSOUser])VALUES('" +
                                firstname + "','" + lastname + "','" + firstname + " " + lastname + "',@pwd ,@saltpwd,'" + email +
                                "','English','+01:00',0,0,0,@feedSelection,1) ;  select @@identity;";
                            sqlcmd1.CommandText = query;
                            SqlDataAdapter da = new SqlDataAdapter();

                            da.SelectCommand = sqlcmd1;
                            DataSet ds = new DataSet();
                            da.Fill(ds);
                            if (ds.Tables[0] != null)
                            {
                                SqlCommand insertrole = new SqlCommand();
                                insertrole.Connection = sqlcon;
                                if (ssodata.UserGroupsvalue.Length > 0)
                                {
                                    foreach (var c in ssodata.UserGroupsvalue.Split(','))
                                    {
                                        insertrole.CommandText = "insert into AM_GlobalRole_User(GlobalRoleId,UserId)  values (" + c + "," + ds.Tables[0].Rows[0][0] + ")";
                                        insertrole.CommandType = CommandType.Text;
                                        insertrole.ExecuteNonQuery();
                                    }
                                }
                                if (ssodata.UserglobalAccess.Count > 0)
                                {
                                    foreach (var Role in ssodata.UserglobalAccess)
                                    {
                                        insertrole.CommandText = "insert into AM_GlobalRole_User(GlobalRoleId,UserId)  values ( " + Role + "," + ds.Tables[0].Rows[0][0] + ")";
                                        insertrole.CommandType = CommandType.Text;
                                        insertrole.ExecuteNonQuery();
                                    }

                                }
                                SqlCommand insertDefaultAppNotification = new SqlCommand();
                                insertDefaultAppNotification.Connection = sqlcon;
                                string defaultsubscriptionquery = "DECLARE @subscrlist VARCHAR(MAX)" +
                                        "DECLARE @mailsubscrlist VARCHAR(MAX)" +
                                        "SELECT @subscrlist = COALESCE(@subscrlist+',' ,'') + cast(id as nvarchar(50))" +
                                        "FROM CM_SubscriptionType  where isAppDefault=1" +
                                        "SELECT @mailsubscrlist = COALESCE(@mailsubscrlist+',' ,'') + cast(id as nvarchar(50))" +
                                        "FROM CM_SubscriptionType  where isMailDefault=1" +
                                        "insert into CM_UserDefaultSubscription values('" + ds.Tables[0].Rows[0][0] + "',@subscrlist,@mailsubscrlist)";
                                insertDefaultAppNotification.CommandText = defaultsubscriptionquery;
                                insertDefaultAppNotification.CommandType = CommandType.Text;
                                insertDefaultAppNotification.ExecuteNonQuery();
                            }
                        }
                    }
                }
                return (string)email;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region GenerateNextAttributeSequenceValue
        public string GenerateNextAttributeSequenceValue(string query, CommandType type)
        {

            using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
            {
                SqlConnection conn = new SqlConnection(strtenantconn);
                SqlCommand cmd = new SqlCommand(query, conn);

                string strResult = "";
                cmd.CommandType = type;
                try
                {
                    sqlcon.Open();
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************clsDb Connection open //////" + query + "//////// " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandType = type;
                    sqlcmd.Connection = sqlcon;
                    sqlcmd.CommandText = query;
                    SqlDataReader dr;
                    dr = sqlcmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        return dr["seqno"].ToString();
                    }
                    else
                    {
                        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("************************ clsDb  unable to get user data " + DateTime.Now + " ************************", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return strResult.ToString();
            }
        }
        #endregion

        public BrandSystems.Marcom.Core.User.StorageBlock getcloudsettings(int? tenantID)
        {
            BrandSystems.Marcom.Core.User.StorageBlock amazonInfo = new BrandSystems.Marcom.Core.User.StorageBlock();
            try
            {
                if (tenantID != null)
                {
                    string xmlpath = ConfigurationManager.AppSettings["MarcomPresentation"] + "Tenants\\TenantsInfo.xml";
                    XDocument tenantInfoXdoc = XDocument.Load(xmlpath);

                    var AWSSetup = tenantInfoXdoc.Descendants("Tenants").Elements("Tenant").Where(e => Convert.ToInt32(e.Element("TenantID").Value) == tenantID).Select(s => s.Elements("AWSSetup")).FirstOrDefault();
                    var FileSystemsMode = tenantInfoXdoc.Descendants("Tenants").Elements("Tenant").Where(e => Convert.ToInt32(e.Element("TenantID").Value) == tenantID).Select(s => s.Element("FileSystem")).FirstOrDefault();

                    amazonInfo.AWSAccessKeyID = AWSSetup.Elements("AWSAccessKeyID").FirstOrDefault().Value.ToString();
                    amazonInfo.AWSSecretAccessKey = AWSSetup.Elements("AWSSecretAccessKey").FirstOrDefault().Value.ToString();
                    amazonInfo.BucketName = AWSSetup.Elements("BucketName").FirstOrDefault().Value.ToString();
                    amazonInfo.ServiceURL = AWSSetup.Elements("ServiceURL").FirstOrDefault().Value.ToString();
                    amazonInfo.RegionEndpoint = AWSSetup.Elements("RegionEndpoint").FirstOrDefault().Value.ToString();
                    amazonInfo.Uploaderurl = AWSSetup.Elements("UploaderUrl").FirstOrDefault().Value.ToString();
                    amazonInfo.storageType = Convert.ToInt32(FileSystemsMode.Value);
                }
                return amazonInfo;
            }
            catch
            {
                return amazonInfo;

            }
        }
        public IList<string> GetUserRegLogoBGImage(int TenantID)
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    IList<string> BGImagePath = new List<string>();
                    string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID) + "TitleLogo" + @"\UserRegistration.css");
                    BGImagePath.Add(GetTenantFilePath(TenantID) + "TitleLogo" + @"\UserRegistration.css");
                    return BGImagePath;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public IList<string> GetUserRegLogoDetails(int TenantID)
        {
            try
            {
                BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
                S3Obj = getcloudsettings(TenantID);
                Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                using (SqlConnection sqlcon = new SqlConnection(strtenantconn))
                {
                    sqlcon.Open();
                    string title = ""; IList<string> logoDetails = new List<string>();
                    string xelementName = "LogoSettings";
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                    var xmlElement = xelementFilepath.Element(xelementName);
                    foreach (var c in xmlElement.Descendants())
                    {
                        if (c.Name == "Title")
                        {
                            title = c.Value;
                            logoDetails.Add(title);
                        }
                    }
                    //add tenant specific logo path
                    if (S3Obj.storageType == (int)StorageArea.Amazon)
                    {
                        logoDetails.Add((S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/" + GetTenantFilePath(TenantID) + "UserReglogo.png").Replace("\\", "/"));
                    }
                    else
                    {
                        logoDetails.Add(GetTenantFilePath(TenantID) + "UserReglogo.png");
                    }
                    return logoDetails;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static void writeCacheLog(string IP, string userName, string SessionID, string msgType, string message)
        {
            string ConnectionDB = ConfigurationManager.AppSettings["CacheConn"].ToString();
            using (SqlConnection sqlcon = new SqlConnection(ConnectionDB))
            {
                sqlcon.Open();
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.CommandText = "insert into CacheLogTest(IP,UserName,SessionID,Type,Message,Date_Time) values('" + IP + "','" + userName + "','" + SessionID + "','" + msgType + "','" + message + "','" + DateTime.Now.ToString() + "')";
                sqlCmd.Connection = sqlcon;
                sqlCmd.ExecuteScalar();
                sqlcon.Close();

            }

        }
    }
}
