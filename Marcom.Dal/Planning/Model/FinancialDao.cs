using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// FinancialDao object for table 'PM_Financial'.
	/// </summary>
	
	public partial class FinancialDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _entityid;
		protected int _costcenterid;
		protected decimal _plannedamount;
		protected decimal _requestedamount;
		protected decimal _approvedallocatedamount;
        protected DateTimeOffset _lastupdateon;
        protected int _Status;
		protected decimal _approvedbudget;
		protected decimal _commited;
		protected decimal _spent;
		protected DateTimeOffset _approvedbudgetdate;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected decimal _q1;
        protected decimal _q2;
        protected decimal _q3;
        protected decimal _q4;
        protected decimal _m1;
        protected decimal _m2;
        protected decimal _m3;
        protected decimal _m4;
        protected decimal _m5;
        protected decimal _m6;
        protected decimal _m7;
        protected decimal _m8;
        protected decimal _m9;
        protected decimal _m10;
        protected decimal _m11;
        protected decimal _m12;
        protected decimal _h1;
        protected decimal _h2;
        protected decimal _y;
        protected decimal _actualq1;
        protected decimal _actualq2;
        protected decimal _actualq3;
        protected decimal _actualq4;
        protected decimal _actualm1;
        protected decimal _actualm2;
        protected decimal _actualm3;
        protected decimal _actualm4;
        protected decimal _actualm5;
        protected decimal _actualm6;
        protected decimal _actualm7;
        protected decimal _actualm8;
        protected decimal _actualm9;
        protected decimal _actualm10;
        protected decimal _actualm11;
        protected decimal _actualm12;
        protected decimal _actualh1;
        protected decimal _actualh2;
        protected decimal _actualy;
        protected decimal _subassignedamount;
        protected decimal _availableassignedamount;
        protected int _currencytype;
        protected DateTimeOffset _lastupdatedamounton;
		#endregion

       
		
		#region Constructors
		public FinancialDao() {}

        public FinancialDao(int pId, int pEntityid, int pCostCenterid, decimal pPlannedAmount, decimal pRequestedAmount, decimal pApprovedAllocatedAmount, DateTimeOffset pLastUpdateOn, int pStatus, decimal pApprovedBudget, decimal pCommited, decimal pSpent, DateTimeOffset pApprovedBudgetDate, decimal pQ1, decimal pQ2, decimal pQ3, decimal pQ4, decimal pm1, decimal pm2, decimal pm3, decimal pm4, decimal pm5, decimal pm6, decimal pm7, decimal pm8, decimal pm9, decimal pm10, decimal pm11, decimal pm12, decimal ph1, decimal ph2, decimal py, decimal pactualQ1, decimal pactualQ2, decimal pactualQ3, decimal pactualQ4, decimal pactualm1, decimal pactualm2, decimal pactualm3, decimal pactualm4, decimal pactualm5, decimal pactualm6, decimal pactualm7, decimal pactualm8, decimal pactualm9, decimal pactualm10, decimal pactualm11, decimal pactualm12, decimal pactualh1, decimal pactualh2, decimal pactualy, DateTimeOffset plastupdatedamounton, decimal pSubAssignedAmount, decimal pAvailableAssignedAmount, int pCurrencyType)
		{
            this._id = pId;
			this._entityid = pEntityid; 
			this._costcenterid = pCostCenterid; 
			this._plannedamount = pPlannedAmount; 
			this._requestedamount = pRequestedAmount; 
			this._approvedallocatedamount = pApprovedAllocatedAmount;
            this._lastupdateon = pLastUpdateOn;
            this._Status = pStatus; 
			this._approvedbudget = pApprovedBudget; 
			this._commited = pCommited; 
			this._spent = pSpent; 
			this._approvedbudgetdate = pApprovedBudgetDate;
            this._q1 = pQ1;
            this._q2 = pQ2;
            this._q3 = pQ3;
            this._q4 = pQ4;
            this._m1 = pm1;
            this._m2 = pm2;
            this._m3 = pm3;
            this._m4 = pm4;
            this._m5 = pm5;
            this._m6 = pm6;
            this._m7 = pm7;
            this._m8 = pm8;
            this._m9 = pm9;
            this._m11 = pm11;
            this._m12 = pm12;
            this._h1 = ph1;
            this._h2 = ph2;
            this._y = py;
            this._actualq1 = pactualQ1;
            this._actualq2 = pactualQ2;
            this._actualq3 = pactualQ3;
            this._actualq4 = pactualQ4;
            this._actualm1 = pactualm1;
            this._actualm2 = pactualm2;
            this._actualm3 = pactualm3;
            this._actualm4 = pactualm4;
            this._actualm5 = pactualm5;
            this._actualm6 = pactualm6;
            this._actualm7 = pactualm7;
            this._actualm8 = pactualm8;
            this._actualm9 = pactualm9;
            this._actualm11 = pactualm11;
            this._actualm12 = pactualm12;
            this._actualh1 = pactualh1;
            this._actualh2 = pactualh2;
            this._actualy = pactualy;
            this._lastupdatedamounton = plastupdatedamounton;
            this._subassignedamount = pSubAssignedAmount;
            this._availableassignedamount = pAvailableAssignedAmount;
            this._currencytype =  pCurrencyType;
		}
		
      
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual int CostCenterid
		{
			get { return _costcenterid; }
			set { _bIsChanged |= (_costcenterid != value); _costcenterid = value; }
			
		}
		
		public virtual decimal PlannedAmount
		{
			get { return _plannedamount; }
			set { _bIsChanged |= (_plannedamount != value); _plannedamount = value; }
			
		}
		
		public virtual decimal RequestedAmount
		{
			get { return _requestedamount; }
			set { _bIsChanged |= (_requestedamount != value); _requestedamount = value; }
			
		}
		
		public virtual decimal ApprovedAllocatedAmount
		{
			get { return _approvedallocatedamount; }
			set { _bIsChanged |= (_approvedallocatedamount != value); _approvedallocatedamount = value; }
			
		}

        public virtual DateTimeOffset LastUpdateOn
        {
            get { return _lastupdateon; }
            set { _bIsChanged |= (_lastupdateon != value); _lastupdateon = value; }

        }

        public virtual int Status
        {
            get { return _Status; }
            set { _bIsChanged |= (_Status != value); _Status = value; }

        }
		
		public virtual decimal ApprovedBudget
		{
			get { return _approvedbudget; }
			set { _bIsChanged |= (_approvedbudget != value); _approvedbudget = value; }
			
		}
		
		public virtual decimal Commited
		{
			get { return _commited; }
			set { _bIsChanged |= (_commited != value); _commited = value; }
			
		}
		
		public virtual decimal Spent
		{
			get { return _spent; }
			set { _bIsChanged |= (_spent != value); _spent = value; }
			
		}
		
		public virtual DateTimeOffset ApprovedBudgetDate
		{
			get { return _approvedbudgetdate; }
			set { _bIsChanged |= (_approvedbudgetdate != value); _approvedbudgetdate = value; }
			
		}

        public virtual decimal Q1
        {
            get { return _q1; }
            set { _bIsChanged |= (_q1 != value); _q1 = value; }

        }

        public virtual decimal Q2
        {
            get { return _q2; }
            set { _bIsChanged |= (_q2 != value); _q2 = value; }

        }

        public virtual decimal Q3
        {
            get { return _q3; }
            set { _bIsChanged |= (_q3 != value); _q3 = value; }

        }

        public virtual decimal Q4
        {
            get { return _q4; }
            set { _bIsChanged |= (_q4 != value); _q4 = value; }

        }


        public virtual decimal M1
        {
            get { return _m1; }
            set { _bIsChanged |= (_m1 != value); _m1 = value; }

        }

        public virtual decimal M2
        {
            get { return _m2; }
            set { _bIsChanged |= (_m2 != value); _m2 = value; }

        }

        public virtual decimal M3
        {
            get { return _m3; }
            set { _bIsChanged |= (_m3 != value); _m3 = value; }

        }

        public virtual decimal M4
        {
            get { return _m4; }
            set { _bIsChanged |= (_m4 != value); _m4 = value; }

        }

        public virtual decimal M5
        {
            get { return _m5; }
            set { _bIsChanged |= (_m5 != value); _m5 = value; }

        }

        public virtual decimal M6
        {
            get { return _m6; }
            set { _bIsChanged |= (_m6 != value); _m6 = value; }

        }

        public virtual decimal M7
        {
            get { return _m7; }
            set { _bIsChanged |= (_m7 != value); _m7 = value; }

        }

        public virtual decimal M8
        {
            get { return _m8; }
            set { _bIsChanged |= (_m8 != value); _m8 = value; }

        }

        public virtual decimal M9
        {
            get { return _m9; }
            set { _bIsChanged |= (_m9 != value); _m9 = value; }

        }

        public virtual decimal M10
        {
            get { return _m10; }
            set { _bIsChanged |= (_m10 != value); _m10 = value; }

        }

        public virtual decimal M11
        {
            get { return _m11; }
            set { _bIsChanged |= (_m11 != value); _m11 = value; }

        }

        public virtual decimal M12
        {
            get { return _m12; }
            set { _bIsChanged |= (_m12 != value); _m12 = value; }

        }

        public virtual decimal H1
        {
            get { return _h1; }
            set { _bIsChanged |= (_h1 != value); _h1 = value; }

        }

        public virtual decimal H2
        {
            get { return _h2; }
            set { _bIsChanged |= (_h2 != value); _h2 = value; }

        }

        public virtual decimal Y
        {
            get { return _y; }
            set { _bIsChanged |= (_y != value); _y = value; }

        }
        public virtual decimal ActualQ1
        {
            get { return _actualq1; }
            set { _bIsChanged |= (_actualq1 != value); _actualq1 = value; }

        }

        public virtual decimal ActualQ2
        {
            get { return _actualq2; }
            set { _bIsChanged |= (_actualq2 != value); _actualq2 = value; }

        }

        public virtual decimal ActualQ3
        {
            get { return _actualq3; }
            set { _bIsChanged |= (_actualq3 != value); _actualq3 = value; }

        }

        public virtual decimal ActualQ4
        {
            get { return _actualq4; }
            set { _bIsChanged |= (_actualq4 != value); _actualq4 = value; }

        }


        public virtual decimal ActualM1
        {
            get { return _actualm1; }
            set { _bIsChanged |= (_actualm1 != value); _actualm1 = value; }

        }

        public virtual decimal ActualM2
        {
            get { return _actualm2; }
            set { _bIsChanged |= (_actualm2 != value); _actualm2 = value; }

        }

        public virtual decimal ActualM3
        {
            get { return _actualm3; }
            set { _bIsChanged |= (_actualm3 != value); _actualm3 = value; }

        }

        public virtual decimal ActualM4
        {
            get { return _actualm4; }
            set { _bIsChanged |= (_actualm4 != value); _actualm4 = value; }

        }

        public virtual decimal ActualM5
        {
            get { return _actualm5; }
            set { _bIsChanged |= (_actualm5 != value); _actualm5 = value; }

        }

        public virtual decimal ActualM6
        {
            get { return _actualm6; }
            set { _bIsChanged |= (_actualm6 != value); _actualm6 = value; }

        }

        public virtual decimal ActualM7
        {
            get { return _actualm7; }
            set { _bIsChanged |= (_actualm7 != value); _actualm7 = value; }

        }

        public virtual decimal ActualM8
        {
            get { return _actualm8; }
            set { _bIsChanged |= (_actualm8 != value); _actualm8 = value; }

        }

        public virtual decimal ActualM9
        {
            get { return _actualm9; }
            set { _bIsChanged |= (_actualm9 != value); _actualm9 = value; }

        }

        public virtual decimal ActualM10
        {
            get { return _actualm10; }
            set { _bIsChanged |= (_actualm10 != value); _actualm10 = value; }

        }

        public virtual decimal ActualM11
        {
            get { return _actualm11; }
            set { _bIsChanged |= (_actualm11 != value); _actualm11 = value; }

        }

        public virtual decimal ActualM12
        {
            get { return _actualm12; }
            set { _bIsChanged |= (_actualm12 != value); _actualm12 = value; }

        }

        public virtual decimal ActualH1
        {
            get { return _actualh1; }
            set { _bIsChanged |= (_actualh1 != value); _actualh1 = value; }

        }

        public virtual decimal ActualH2
        {
            get { return _actualh2; }
            set { _bIsChanged |= (_actualh2 != value); _actualh2 = value; }

        }

        public virtual decimal ActualY
        {
            get { return _actualy; }
            set { _bIsChanged |= (_actualy != value); _actualy = value; }

        }

        public virtual decimal SubAssignedAmount
        {
            get { return _subassignedamount; }
            set { _bIsChanged |= (_subassignedamount != value); _subassignedamount = value; }

        }

        public virtual decimal AvailableAssignedAmount
        {
            get { return _availableassignedamount; }
            set { _bIsChanged |= (_availableassignedamount != value); _availableassignedamount = value; }

        }

        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set { _bIsChanged |= (_currencytype != value); _currencytype = value; }

        }

        public virtual DateTimeOffset LastUpdatedAmountOn
        {
            get { return _lastupdatedamounton; }
            set { _bIsChanged |= (_lastupdatedamounton != value); _lastupdatedamounton = value; }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{

            public const string Id = "Id";	
			public const string FinancialDao = "FinancialDao";			
			public const string Entityid = "Entityid";			
			public const string CostCenterid = "CostCenterid";			
			public const string PlannedAmount = "PlannedAmount";			
			public const string RequestedAmount = "RequestedAmount";			
			public const string ApprovedAllocatedAmount = "ApprovedAllocatedAmount";
            public const string LastUpdateOn = "LastUpdateOn";
            public const string Status = "Status";	
			public const string ApprovedBudget = "ApprovedBudget";			
			public const string Commited = "Commited";			
			public const string Spent = "Spent";			
			public const string ApprovedBudgetDate = "ApprovedBudgetDate";
            public const string Q1 = "Q1";
            public const string Q2 = "Q2";
            public const string Q3 = "Q3";
            public const string Q4 = "Q4";
            public const string M1 = "M1";
            public const string M2 = "M2";
            public const string M3 = "M3";
            public const string M4 = "M4";
            public const string M5 = "M5";
            public const string M6 = "M6";
            public const string M7 = "M7";
            public const string M8 = "M8";
            public const string M9 = "M9";
            public const string M10 = "M10";
            public const string M11 = "M11";
            public const string M12 = "M12";
            public const string H1 = "H1";
            public const string H2 = "H2";
            public const string Y = "Y";
            public const string ActualQ1 = "ActualQ1";
            public const string ActualQ2 = "ActualQ2";
            public const string ActualQ3 = "ActualQ3";
            public const string ActualQ4 = "ActualQ4";
            public const string ActualM1 = "ActualM1";
            public const string ActualM2 = "ActualM2";
            public const string ActualM3 = "ActualM3";
            public const string ActualM4 = "ActualM4";
            public const string ActualM5 = "ActualM5";
            public const string ActualM6 = "ActualM6";
            public const string ActualM7 = "ActualM7";
            public const string ActualM8 = "ActualM8";
            public const string ActualM9 = "ActualM9";
            public const string ActualM10 = "ActualM10";
            public const string ActualM11 = "ActualM11";
            public const string ActualM12 = "ActualM12";
            public const string ActualH1 = "ActualH1";
            public const string ActualH2 = "ActualH2";
            public const string ActualY = "ActualY";
            public const string LastUpdatedAmountOn = "LastUpdatedAmountOn";
            public const string SubAssignedAmount = "SubAssignedAmount";
            public const string AvailableAssignedAmount = "AvailableAssignedAmount";
            public const string CurrencyType = "CurrencyType";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";	
			public const string FinancialDao = "FinancialDao";			
			public const string Entityid = "Entityid";			
			public const string CostCenterid = "CostCenterid";			
			public const string PlannedAmount = "PlannedAmount";			
			public const string RequestedAmount = "RequestedAmount";			
			public const string ApprovedAllocatedAmount = "ApprovedAllocatedAmount";
            public const string LastUpdateOn = "LastUpdateOn";
            public const string Status = "Status";	
			public const string ApprovedBudget = "ApprovedBudget";			
			public const string Commited = "Commited";			
			public const string Spent = "Spent";			
			public const string ApprovedBudgetDate = "ApprovedBudgetDate";
            public const string Q1 = "Q1";
            public const string Q2 = "Q2";
            public const string Q3 = "Q3";
            public const string Q4 = "Q4";
            public const string M1 = "M1";
            public const string M2 = "M2";
            public const string M3 = "M3";
            public const string M4 = "M4";
            public const string M5 = "M5";
            public const string M6 = "M6";
            public const string M7 = "M7";
            public const string M8 = "M8";
            public const string M9 = "M9";
            public const string M10 = "M10";
            public const string M11 = "M11";
            public const string M12 = "M12";
            public const string H1 = "H1";
            public const string H2 = "H2";
            public const string Y = "Y";
            public const string ActualQ1 = "ActualQ1";
            public const string ActualQ2 = "ActualQ2";
            public const string ActualQ3 = "ActualQ3";
            public const string ActualQ4 = "ActualQ4";
            public const string ActualM1 = "ActualM1";
            public const string ActualM2 = "ActualM2";
            public const string ActualM3 = "ActualM3";
            public const string ActualM4 = "ActualM4";
            public const string ActualM5 = "ActualM5";
            public const string ActualM6 = "ActualM6";
            public const string ActualM7 = "ActualM7";
            public const string ActualM8 = "ActualM8";
            public const string ActualM9 = "ActualM9";
            public const string ActualM10 = "ActualM10";
            public const string ActualM11 = "ActualM11";
            public const string ActualM12 = "ActualM12";
            public const string ActualH1 = "ActualH1";
            public const string ActualH2 = "ActualH2";
            public const string ActualY = "ActualY";
            public const string LastUpdatedAmountOn = "LastUpdatedAmountOn";
            public const string SubAssignedAmount = "SubAssignedAmount";
            public const string AvailableAssignedAmount = "AvailableAssignedAmount";
            public const string CurrencyType = "CurrencyType";
		}

		#endregion
		
		
	}
	
}
