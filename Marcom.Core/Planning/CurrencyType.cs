﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// CurrencyType interface for table 'PM_CurrencyType'.
    /// </summary>

    internal class CurrencyType : ICurrencyType
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _shortname;
        protected string _symbol;
        protected int _staticcurrency;

        #endregion

        #region Constructors
        public CurrencyType() { }

        public CurrencyType(int pId, string pName, string pShortName, string pSymbol, int p_StaticCurrency)
        {
            this._id = pId;
            this._name = pName;
            this._shortname = pShortName;
            this._symbol = pSymbol;
            this._staticcurrency = p_StaticCurrency;
        }

        public CurrencyType(int pId)
        {
            this._id = pId;
        }

        #endregion


        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {

                _name = value;
            }

        }

        public string ShortName
        {
            get { return _shortname; }
            set
            {

                _shortname = value;
            }

        }

        public string Symbol
        {
            get { return _symbol; }
            set
            {

                _symbol = value;
            }

        }
        public int StaticCurrency
        {
            get { return _staticcurrency; }
            set { _staticcurrency = value; }

        }




        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            CurrencyType castObj = null;
            try
            {
                castObj = (CurrencyType)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
