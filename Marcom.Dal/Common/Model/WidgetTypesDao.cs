﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// FeedDao object for table 'CM_WidgetTypes'.
    /// </summary>
    public partial class WidgetTypesDao : BaseDao, ICloneable
    {
     
        #region Member Variables
        protected int _id;
        protected string _typename;
        protected int _iSdynamic;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
		public WidgetTypesDao() {}

        public WidgetTypesDao(string pTypeName, int piSdynamic)
		{
            this._typename = pTypeName;
            this._iSdynamic = piSdynamic; 
			            
		}
				
		public WidgetTypesDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string TypeName
        {
            get { return _typename; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("TypeName", "TypeName value, cannot contain more than 500 characters");
                _bIsChanged |= (_typename != value);
                _typename = value;
            }

        }

        public virtual int ISDynamic
        {
            get { return _iSdynamic; }
            set { _bIsChanged |= (_iSdynamic != value); _iSdynamic = value; }

        }

       public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string WidgetTypesDao = "WidgetTypesDao";
            public const string Id = "ID";
            public const string TypeName = "TypeName";
            public const string ISDynamic = "ISDynamic";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string WidgetTypesDao = "WidgetTypesDao";
            public const string Id = "ID";
            public const string TypeName = "TypeName";
            public const string ISDynamic = "ISDynamic";
        }

        #endregion
    }
}
