﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// Attachments object for table 'PM_Attachments'.
    /// </summary>

    internal class Task : ITask
    {
        #region Member Variables

        protected int _id;
        protected int _stepId;
        protected int _entityid;
        protected int _memberID;
        protected string _name;
        protected string _description;
        protected int _taskstatus;
        protected DateTime _duedate;
        protected int _predefinedTaskId;


        #endregion

        #region Constructors
        public Task() { }

        public Task(int pStepid, int pEntityid, int pMemberID, string pName, string pDescription, int pTaskStatus, DateTime pDueDate, int pPredefinedTaskID)
        {
            this._stepId = pStepid;
            this._entityid = pEntityid;
            this._memberID = pMemberID;
            this._name = pName;
            this._description = pDescription;
            this._taskstatus = pTaskStatus;
            this._duedate = pDueDate;
            this._memberID = pMemberID;
            this._predefinedTaskId = pPredefinedTaskID;

        }

        public Task(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }

        }

        public virtual int StepID
        {
            get { return _stepId; }
            set { _stepId = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }

        }


        public virtual int TaskStatus
        {
            get { return _taskstatus; }
            set { _taskstatus = value; }

        }

        public virtual DateTime DueDate
        {
            get { return _duedate; }
            set { _duedate = value; }

        }

        public virtual int PredefinedTaskID
        {
            get { return _predefinedTaskId; }
            set { _predefinedTaskId = value; }

        }

        public int Status { get; set; }
        public string TaskOwnerName { get; set; }
        public string TaskStatusName { get; set; }
        public int Duedates { get; set; }
        public dynamic TaskAttachment { get; set; }
        public IList<ITaskMember> TaskMembers { get; set; }
        public Boolean IsPredefined { get; set; }
        public int TaskTypeId { get; set; }
        public string TaskTypeName { get; set; }
        public string ParentEntityName { get; set; }
        public string ParentEntityTypeName { get; set; }
        public IList<IFundingRequest> fundrequestTask { get; set; }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Task castObj = null;
            try
            {
                castObj = (Task)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
