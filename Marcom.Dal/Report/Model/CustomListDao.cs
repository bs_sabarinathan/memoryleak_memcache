﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Report.Model
{
    public partial class CustomListDao : BaseDao, ICloneable 
    {
        #region Member Variables
        protected int _id;
 
        protected string _name;
        protected string _xmldata;
        protected string _description;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
		public CustomListDao() {}

        public CustomListDao(int Poid, string Pname, string pxmldata, string pdescription)
		{
            this._id = Poid;
            this._name = Pname;
            this._xmldata = pxmldata;
            this._description = pdescription;
		}


        public CustomListDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

   

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Name", "Reporturl value, cannot contain more than 100 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }
        public virtual string XmlData
        {
            get { return _xmldata; }
            set
            {
          
                _bIsChanged |= (_xmldata != value);
                _xmldata = value;
            }

        }
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 200)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 200 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }
     

        #endregion 				

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CustomListDao = "CustomListDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string XmlData = "XmlData";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CustomListDao = "CustomListDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string XmlData = "XmlData";
        }

        #endregion
		
    }
}
