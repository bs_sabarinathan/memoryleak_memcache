﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;


namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>


    internal class TaskMembers : ITaskMembers
    {
        #region Member Variables

        protected int _id;
        protected int _RoleID;
        protected  int _UserID;
        protected int _TaskID;
        protected int? _ApprovalStatus;
        protected int _ApprovalRount;
        protected int _StepId;
        protected int _PhaseID;
        protected int _VersionID;
        protected bool _IsGlobal;
        protected bool _IsActive;
        protected int _StepRoleID;
        protected int _EntityRoleID;
        protected DateTime? _OverdueMailSent;
        protected bool _IsTaskalertSent;
       
        #endregion

        #region constructors
        public TaskMembers() { }
        #endregion


        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int UserID
        {
            get;
            set;
        }

        public int TaskID
        {
            get;
            set;
        }

        public string FlagColorCode
        {
            get;
            set;
        }

        public int RoleID { get; set; }
        public int? ApprovalStatus { get; set; }
        public int ApprovalRount { get; set; }

        public string UserName
        {
            get;
            set;

        }

        public string DepartmentName
        {
            get;
            set;

        }

        public string Role
        {
            get;
            set;

        }

        public string Title
        {
            get;
            set;

        }

        public string UserEmail
        {
            get;
            set;

        }
        public DateTime? OverdueMailSent
        {
            get;
            set;
        }

        public bool IsTaskalertSent
        {
            get;
            set;
        }
        public int PhaseID
        {
            get;
            set;
        }
        public int StepId
        {
            get;
            set;
        }
        public int VersionID
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
        public bool IsGlobal
        {
            get;
            set;
        }
        public int StepRoleID
        {
            get;
            set;
        }
        public int EntityRoleID
        {
            get;
            set;
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskMembers castObj = null;
            try
            {
                castObj = (TaskMembers)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}



