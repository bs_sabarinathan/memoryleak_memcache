using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IEntityTypeRoleAcl interface for table 'AM_EntityTypeRoleAcl'.
    /// </summary>
    public interface IEntityTypeRoleAcl
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityRoleID
        {
            get;
            set;

        }

        int ModuleID
        {
            get;
            set;

        }

        int EntityTypeID
        {
            get;
            set;

        }

        int Sortorder
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
