﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetRolelink : IAssetRolelink
    {
        #region "Member variables"

         protected int _id;
        protected int _roleid;
        protected int _assetid;      

        #endregion

        #region "Constructor"

        public AssetRolelink() { }


        public AssetRolelink(int pid, int pRoleid,int pAssetid) 
        {
            this._id = pid;
            this._roleid = pRoleid;
            this._assetid = pAssetid;
        }

        #endregion

        #region "Public propertiers"

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Roleid
        {
            get { return _roleid; }
            set { _roleid = value; }

        }
        public int Assetid
        {
            get { return _assetid; }
            set { _assetid = value; }

        }

        #endregion
    }
}
