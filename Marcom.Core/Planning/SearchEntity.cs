﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    public class SearchEntity
    {
        #region Member Variables

        protected string _id;
        //protected int _moduleid;
        protected int _typeid;
        protected int _typecategoryid;
        protected string _name;
        protected string _path;
        protected string _description;
        protected string _thumbnailurl;
        protected List<int> _userids;
        protected List<KeyValuePair<string, string>> _metadata;
        protected int _parentid;
        protected int _fileid;
        protected string _filename;
        protected string _assetaccess;
        protected string _extension;
        protected string _searchtype;
        protected List<string> _tagword;
        protected string _uniqueKey;
        protected string _pathInfo;
        protected DateTime docDate { get; set; }
        #endregion

        #region Constructors
        public SearchEntity() { }

        public SearchEntity(string pId, int pTypeID, int pTypeCategoryId, string pName, string pPath, string pDescription, string pThumbnailUrl, List<int> pUserIDs, List<KeyValuePair<string, string>> pMetadata, int pParentID, int pFileId, string pFileName, string pAssetAccess, string pExtension, string pSearchType, List<string> pTagword, string pUniqueKey, string pPathInfo, DateTime pDate)
        {
            this._id = pId;
            //this._moduleid = pModuleId;
            this._typeid = pTypeID;
            this._typecategoryid = pTypeCategoryId;
            this._name = pName;
            this._path = pPath;
            this._description = pDescription;
            this._thumbnailurl = pThumbnailUrl;
            this._userids = pUserIDs;
            this._metadata = pMetadata;
            this._parentid = pParentID;
            this._fileid = pFileId;
            this._filename = pFileName;
            this._assetaccess = pAssetAccess;
            this._extension = pExtension;
            this._searchtype = pSearchType;
            this.TagWord = pTagword;
            this._uniqueKey = pUniqueKey;
            this._pathInfo = pPathInfo;
            this.docDate = pDate;
        }

        #endregion

        #region Public Properties

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }
        }
        public string TypeCaption { get; set; }
        public string TypeShortDescription { get; set; }

        public int TypeCategoryID
        {
            get { return _typecategoryid; }
            set { _typecategoryid = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public DateTime DocumentDate
        {
            get { return docDate; }
            set { docDate = value; }
        }
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string ThumbnailUrl
        {
            get { return _thumbnailurl; }
            set { _thumbnailurl = value; }
        }
        public List<int> UserIDs = new List<int>()
        {

        };
        public List<KeyValuePair<string, string>> Metadata = new List<KeyValuePair<string, string>>()
        {

        };

        public List<string> SearchDateMetadata = new List<string>()
        {

        };

        public int ParentID
        {
            get { return _parentid; }
            set { _parentid = value; }
        }

        public int FileID
        {
            get { return _fileid; }
            set { _fileid = value; }
        }
        public string FileName
        {
            get { return _filename; }
            set { _filename = value; }
        }
        public string AssetAccess
        {
            get { return _assetaccess; }
            set { _assetaccess = value; }
        }
        public string Extension
        {
            get { return _extension; }
            set { _extension = value; }
        }
        public string SearchType
        {
            get { return _searchtype; }
            set { _searchtype = value; }
        }

        public string UniqueKey
        {
            get { return _uniqueKey; }
            set { _uniqueKey = value; }
        }
        public string PathInfo
        {
            get { return _pathInfo; }
            set { _pathInfo = value; }
        }

        public List<string> TagWord = new List<string>()
        {

        };

        private string strSearchTerm;
        public string SearchTerm
        {
            get { return strSearchTerm; }
            set { strSearchTerm = value; }
        }
        private string strdescription;
        public string SearchTermDescription
        {
            get { return strdescription; }
            set { strdescription = value; }
        }
        private int strwatchers_count;
        public int SearchTermstrWatchersCount
        {
            get { return strwatchers_count; }
            set { strwatchers_count = value; }
        }
        private int strScore;
        public int SearchScore
        {
            get { return strScore; }
            set { strScore = value; }
        }

        private string strassetdetails;
        public string AssetDetails
        {
            get { return strassetdetails; }
            set { strassetdetails = value; }
        }
        private string strEntitytpedetails;
        public string EntitytypeDetails
        {
            get { return strEntitytpedetails; }
            set { strEntitytpedetails = value; }
        }
        private string strAttributes;
        public string AttributesDetails
        {
            get { return strAttributes; }
            set { strAttributes = value; }
        }

        private string strAssetValueDetails;
        public string AssetValueDetails
        {
            get { return strAssetValueDetails; }
            set { strAssetValueDetails = value; }
        }

        #endregion


    }


}
