﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;
using System.Linq;
using BrandSystems.Marcom.Dal.Task.Model;
namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IEntitySublevelTaskHolder
    {
        #region Public Properties

        int EntityID { get; set; }
        string EntityName { get; set; }
        int SortOrder { get; set; }
        string EntityUniqueKey { get; set; }
        string EntityTypeShortDescription { get; set; }
        int EntityTypeID { get; set; }
        string EntityTypeColorCode { get; set; }
        IList<ITaskLibraryTemplateHolder> TaskListGroup { get; set; }

        #endregion
    }
}
