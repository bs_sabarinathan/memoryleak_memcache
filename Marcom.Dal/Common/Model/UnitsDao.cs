﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// FeedDao object for table 'cm_addtionalsettings'.
    /// </summary>
    public partial class UnitsDao: BaseDao, ICloneable 
    {
        #region Member Variables

        protected int _id;
        protected string _caption;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion


        #region Constructors
		public UnitsDao() {}

        public UnitsDao( int pid, string pcaption)
		{
        this._id = pid;
        this._caption = pcaption;            
		}

        public UnitsDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion


        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }



        public virtual string Caption
        {
            get { return _caption; }
            set
            {
            _bIsChanged |= (_caption != value);
            _caption = value;
            }

        }

        


        public virtual bool IsDeleted
        {
            get
            {
            return _bIsDeleted;
            }
            set
            {
            _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
            return _bIsChanged;
            }
            set
            {
            _bIsChanged = value;
            }
        }

        #endregion 



        #region ICloneable methods

        public virtual object Clone()
        {
        return this.MemberwiseClone();
        }

        #endregion



        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AdditionalSettingsDao = "UnitsDao";
            public const string Id = "ID";
            public const string Caption = "Caption";
           

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AdditionalSettingsDao = "UnitsDao";
            public const string Id = "ID";
            public const string Caption = "Caption";

        }

        #endregion






    }
}
