﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Xml;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Metadata.Interface;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Core.Dam;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Dal.DAM.Model;

namespace BrandSystems.Marcom.Core.Core.Managers.Proxy
{
    internal partial class DigitalAssetManagerProxy : IDigitalAssetManager, IManagerProxy
    {
        // Reference to the MarcomManager
        /// <summary>
        /// The _marcom manager      
        /// </summary>
        private MarcomManager _marcomManager = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataManagerProxy" /> class.
        /// </summary>
        /// <param name="marcomManager">The marcom manager.</param>
        internal DigitalAssetManagerProxy(MarcomManager marcomManager)
        {
            _marcomManager = marcomManager;

            // Do some initialization.... 
            // i.e. cache logged in user specific things (or maybe use lazy loading for that)
        }


        // Reference to the MarcomManager (only internal)
        /// <summary>
        /// Gets the marcom manager.
        /// </summary>
        /// <value>
        /// The marcom manager.
        /// </value>
        internal MarcomManager MarcomManager
        {
            get { return _marcomManager; }
        }

        public IList<IEntityType> GetDAMEntityTypes()
        {
            return DigitalAssetManager.Instance.GetDAMEntityTypes(this);
        }

        public IList<IAttribute> GetAttributeDAMCreate(int EntityTypeID)
        {
            return DigitalAssetManager.Instance.GetAttributeDAMCreate(this, EntityTypeID);
        }
        public IList<IAttribute> GetDAMAttribute(int EntityTypeID)
        {
            return DigitalAssetManager.Instance.GetDAMAttribute(this, EntityTypeID);
        }

        public IList<IAttribute> GetDAMCommonAttribute()
        {
            return DigitalAssetManager.Instance.GetDAMCommonAttribute(this);
        }

        public bool DAMadminSettingsforRootLevelInsertUpdate(string jsondata, string LogoSettings, string key, int typeid)
        {
            return DigitalAssetManager.Instance.DAMadminSettingsforRootLevelInsertUpdate(this, jsondata, LogoSettings, key, typeid);

        }

        public bool DAMadminSettingsforRootLevelInsertUpdate(string jsondata, string LogoSettings)
        {
            return DigitalAssetManager.Instance.DAMadminSettingsforRootLevelInsertUpdate(this, jsondata, LogoSettings);

        }
        public string DAMGetAdminSettings(string LogoSettings, string key, int typeid)
        {
            return DigitalAssetManager.Instance.DAMGetAdminSettings(this, LogoSettings, key, typeid);
        }

        public string DAMGetAdminSettings(string LogoSettings)
        {
            return DigitalAssetManager.Instance.DAMGetAdminSettings(this, LogoSettings);
        }


        public Tuple<IList, IList> GetDAMExtensions()
        {
            return DigitalAssetManager.Instance.GetDAMExtensions(this);
        }


        public IList<IEntityTypeAttributeRelationwithLevels> GetDamAttributeRelation(int damID)
        {
            return DigitalAssetManager.Instance.GetDamAttributeRelation(this, damID);
        }


        /// <summary>
        /// Gets the DAM folder.
        /// </summary>
        /// <param name="attributeID">The EntityID.</param>
        /// <returns>string</returns>
        public string GetEntityDamFolder(int entityID)
        {
            return DigitalAssetManager.Instance.GetEntityDamFolder(this, entityID);
        }

        public int CreateAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, IList<IDamPeriod> listperiodvalues, string FileName, int VersionNo, string MimeType, string Extension, long Size, int EntityID, String FileGuid, string Description, bool IsforDuplicate = false, int Duplicatefilestatus = 0, int LinkedAssetID = 0, string fileAdditionalinfo = null, string strAssetAccess = null, int SourceAssetID = 0, bool IsforAdmin = false, int assetactioncode = 0, bool blnAttach = false, IList<IDAMFile> SourceAssetDAMFile = null, bool fromcrop = false, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null, bool IsTemplate = false, int ImpersonateUID = 0)
        {
            return DigitalAssetManager.Instance.CreateAsset(this, parentId, TypeId, Name, listattributevalues, listperiodvalues, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description, IsforDuplicate, Duplicatefilestatus, LinkedAssetID, fileAdditionalinfo, strAssetAccess, SourceAssetID, IsforAdmin, assetactioncode, blnAttach, SourceAssetDAMFile, fromcrop, listentityamountcurrencytype, IsTemplate, ImpersonateUID);
        }

        public List<object> GetAssets(int folderid, int entityID, int viewType, int orderby, int pageNo, bool ViewAll = false)
        {
            return DigitalAssetManager.Instance.GetAssets(this, folderid, entityID, viewType, orderby, pageNo, ViewAll);
        }

        public IAssets GetAssetAttributesDetails(int id)
        {
            return DigitalAssetManager.Instance.GetAssetAttributesDetails(this, id);
        }
        public List<object> GetDAMViewSettings()
        {
            return DigitalAssetManager.Instance.GetDAMViewSettings(this);
        }

        public int CreateFolder(int entityID, int parentNodeID, int Level, int nodeId, int id, string Key, int sortorder, string caption, string description, string colorcode)
        {
            return DigitalAssetManager.Instance.CreateFolder(this, entityID, parentNodeID, Level, nodeId, id, Key, sortorder, caption, description, colorcode);
        }

        public int UpdateFolder(int id, string caption, string description, string colorcode)
        {
            return DigitalAssetManager.Instance.UpdateFolder(this, id, caption, description, colorcode);
        }

        public bool DeleteFolder(int[] idArr)
        {
            return DigitalAssetManager.Instance.DeleteFolder(this, idArr);
        }

        public bool DeleteAssets(int[] idArr)
        {
            return DigitalAssetManager.Instance.DeleteAssets(this, idArr);
        }


        public DAMFileDao SaveFiletoAsset(int AssetID, int Status, string MimeType, long Size, string FileGuid, DateTime CreatedOn, string Extension, string Name, int VersionNo, string Description, int OwnerID)
        {
            return DigitalAssetManager.Instance.SaveFiletoAsset(this, AssetID, Status, MimeType, Size, FileGuid, CreatedOn, Extension, Name, VersionNo, Description, OwnerID);
        }

        public bool UpdateAssetVersion(int AssetID, int fileid)
        {
            return DigitalAssetManager.Instance.UpdateAssetVersion(this, AssetID, fileid);
        }

        public string DownloadDamFiles(int[] assetid, bool Isfiles, int sendDamFiles = 0, List<KeyValuePair<string, int>> CroppedList = null, bool Issendmail = false)
        {
            return DigitalAssetManager.Instance.DownloadDamFiles(this, assetid, Isfiles, sendDamFiles, CroppedList, Issendmail);
        }
        /// <summary>
        /// UpdateAssetAccess
        /// </summary>
        /// <param name="AssetID"></param>
        /// <param name="AccessID"></param>
        /// <returns>success</returns>
        public bool UpdateAssetAccess(int AssetID, string AccessID)
        {
            return DigitalAssetManager.Instance.UpdateAssetAccess(this, AssetID, AccessID);
        }

        public int AssetRoleAccess(int ID, int[] RoleID)
        {
            return DigitalAssetManager.Instance.AssetRoleAccess(this, ID, RoleID);
        }

        public bool SaveDetailBlockForAssets(int AssetID, int AttributeTypeid, int OldValue, List<object> NewValue, int Level)
        {
            return DigitalAssetManager.Instance.SaveDetailBlockForAssets(this, AssetID, AttributeTypeid, OldValue, NewValue, Level);
        }

        public IList<IOption> GetOptionDetailListByAssetID(int id, int AssetID)
        {
            return DigitalAssetManager.Instance.GetOptionDetailListByAssetID(this, id, AssetID);
        }

        public int DuplicateAssets(int[] assetid)
        {
            return DigitalAssetManager.Instance.DuplicateAssets(this, assetid);
        }

        public int AttachAssetsEntityCreation(int entityID, int[] assetid, IList<object> SelectedAssetIdFolderId = null, bool isalreadyexist = false, int UserID = 0)
        {
            return DigitalAssetManager.Instance.AttachAssetsEntityCreation(this, entityID, assetid, SelectedAssetIdFolderId, isalreadyexist,UserID);
        }

        /// <summary>
        /// CheckPreviewGenerator
        /// </summary>
        /// <param name="AssetIds"></param>
        /// <returns>returns preview generated ids</returns>
        public string CheckPreviewGenerator(string AssetIds)
        {
            return DigitalAssetManager.Instance.CheckPreviewGenerator(this, AssetIds);
        }

        /// <summary>
        /// PublishAssets
        /// </summary>
        /// <param name="idArr"></param>
        /// <param name="IsPublished"></param>
        /// <returns>true if ispublish value is updated</returns>
        public bool PublishAssets(int[] idArr, bool IsPublished, int PublisherID)
        {
            return DigitalAssetManager.Instance.PublishAssets(this, idArr, IsPublished, PublisherID);
        }

        public int CreateBlankAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, int EntityID, int Category, IList<IDamPeriod> listperiodvalues, string url = null, bool IsforDuplicate = false, int LinkedAssetID = 0, string strAssetAccess = null, int SourceAssetID = 0, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null)
        {
            return DigitalAssetManager.Instance.CreateBlankAsset(this, parentId, TypeId, Name, listattributevalues, EntityID, Category, listperiodvalues, url, IsforDuplicate, LinkedAssetID, strAssetAccess, SourceAssetID, 0, false, listentityamountcurrencytype);
        }

        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="idarr"></param>
        /// <returns>true if mail sent</returns>
        public bool SendMail(bool IncludeMdata, int[] idarr, string toAddress, string subject, List<KeyValuePair<string, int>> CroppedList = null)
        {
            return DigitalAssetManager.Instance.SendMail(this, IncludeMdata, idarr, toAddress, subject, CroppedList);
        }

        /// <summary>
        /// MoveFilesToUploadImagesToCreateTask
        /// </summary>used to move the files from dam files to uploadimages folder to create task
        /// <param name="proxy"></param>
        /// <param name="TaskFiles"></param>
        /// <returns>new guid</returns>
        public Tuple<IList<BrandSystems.Marcom.Core.Common.Interface.IFile>, int> MoveFilesToUploadImagesToCreateTask(IList<BrandSystems.Marcom.Core.Common.Interface.IFile> TaskFiles, int ParentEntityID)
        {
            return DigitalAssetManager.Instance.MoveFilesToUploadImagesToCreateTask(this, TaskFiles, ParentEntityID);
        }

        public bool DeleteAttachmentVersionByAssetID(int fileid)
        {
            return DigitalAssetManager.Instance.DeleteAttachmentVersionByAssetID(this, fileid);
        }

        public bool UpdateAssetAccessSettings(int RoleID, bool IsChecked)
        {
            return DigitalAssetManager.Instance.UpdateAssetAccessSettings(this, RoleID, IsChecked);
        }


        public bool UpdateAssetDetails(int assetID, string assetName)
        {
            return DigitalAssetManager.Instance.UpdateAssetDetails(this, assetID, assetName);
        }

        public IList<IDamFileProfile> GetProfileFiles()
        {
            return DigitalAssetManager.Instance.GetProfileFiles(this);
        }

        public bool InsertUpdateProfileFiles(int id, string ProfileName, double height, double width, string extension, int dpi, string Role)
        {
            return DigitalAssetManager.Instance.InsertUpdateProfileFiles(this, id, ProfileName, height, width, extension, dpi, Role);
        }

        public bool DeleteProfilefile(int id)
        {
            return DigitalAssetManager.Instance.DeleteProfilefile(this, id);
        }
        public IList<IOptimakerSettings> GetOptimakerSettings()
        {
            return DigitalAssetManager.Instance.GetOptimakerSettings(this);
        }

        public IList<ICategory> GetOptmakerCatagories()
        {
            return DigitalAssetManager.Instance.GetOptmakerCatagories(this);
        }
        public string GetCategoryTreeCollection()
        {
            return DigitalAssetManager.Instance.GetCategoryTreeCollection(this);
        }

        public bool InsertUpdateCatergory(JObject jobj)
        {
            return DigitalAssetManager.Instance.InsertUpdateCatergory(this, jobj);
        }

        public int InsertUpdateOptimakerSettings(int ID, string Name, int CategoryId, string Description, int DocId, int DeptId, int DocType, int DocVersionId, string PreviewImage, int AutoApproval, int TemplateEngineType, int AssetType, string ServerURL, bool EnableHighResPdf)
        {
            return DigitalAssetManager.Instance.InsertUpdateOptimakerSettings(this, ID, Name, CategoryId, Description, DocId, DeptId, DocType, DocVersionId, PreviewImage, AutoApproval, TemplateEngineType, AssetType, ServerURL, EnableHighResPdf);
        }

        public bool DeleteOptimakeSetting(int ID)
        {
            return DigitalAssetManager.Instance.DeleteOptimakeSetting(this, ID);
        }

        public bool DeleteOptimakerCategory(int ID)
        {
            return DigitalAssetManager.Instance.DeleteOptimakerCategory(this, ID);
        }

        public IList GetAllOptimakerSettings(int categoryId)
        {
            return DigitalAssetManager.Instance.GetAllOptimakerSettings(this, categoryId);
        }
        public string GetOptimakerSettingsBaseURL()
        {
            return DigitalAssetManager.Instance.GetOptimakerSettingsBaseURL(this);
        }
        public string GetMediaGeneratorSettingsBaseURL()
        {
            return DigitalAssetManager.Instance.GetMediaGeneratorSettingsBaseURL(this);
        }

        public int CreateMediaGeneratorAsset(int entityID, int folderID, int assetTypeID, int createdBy, int docVersionID, string assetName, string jpegname, long jpegsize, int jpegversion, string jpegdesc, string jpegGUID, string highResGUID, string lowResGUID, int docID, int TenantID)
        {
            return DigitalAssetManager.Instance.CreateMediaGeneratorAsset(entityID, folderID, assetTypeID, createdBy, docVersionID, assetName, jpegname, jpegsize, jpegversion, jpegdesc, jpegGUID, highResGUID, lowResGUID, docID, TenantID);
        }


        public List<object> GetDAMViewAdminSettings(string ViewType, int DamType)
        {
            return DigitalAssetManager.Instance.GetDAMViewAdminSettings(this, ViewType, DamType);
        }

        public bool UpdateDamViewStatus(string ViewType, int DamType, IList<IDamViewAttribtues> attributeslist)
        {
            return DigitalAssetManager.Instance.UpdateDamViewStatus(this, ViewType, DamType, attributeslist);
        }

        public List<object> GetDAMToolTipSettings()
        {
            return DigitalAssetManager.Instance.GetDAMToolTipSettings(this);
        }

        public int InsertUpdateWordTemplateSettings(int ID, string Name, int CategoryId, string Description, string Worddocpath, string PreviewImage)
        {
            return DigitalAssetManager.Instance.InsertUpdateWordTemplateSettings(this, ID, Name, CategoryId, Description, Worddocpath, PreviewImage);
        }

        public bool DeleteWordtempSetting(int ID)
        {
            return DigitalAssetManager.Instance.DeleteWordtempSetting(this, ID);
        }

        public IList<IWordTemplateSettings> GetWordTemplateSettings()
        {
            return DigitalAssetManager.Instance.GetWordTemplateSettings(this);
        }
        public List<object> GetStatusFilteredEntityAsset(int folderid, int entityID, int viewType, int orderby, int pageNo, string statusFilter)
        {
            return DigitalAssetManager.Instance.GetStatusFilteredEntityAsset(this, folderid, entityID, viewType, orderby, pageNo, statusFilter);
        }
        public void CreateDOCGeneratedAsset(int entityID, int folderID, int assetTypeID, int createdBy, string intialfilename, string assetName, int TenantID)
        {
            DigitalAssetManager.Instance.CreateDOCGeneratedAsset(this, entityID, folderID, assetTypeID, createdBy, intialfilename, assetName, TenantID);
        }


        public List<object> GetPublishedAssets(int viewType, int orderby, int pageNo)
        {
            return DigitalAssetManager.Instance.GetPublishedAssets(this, viewType, orderby, pageNo);
        }

        public int AddPublishedFilesToDAM(int[] assetids, int EntityID, int FolderID)
        {
            return DigitalAssetManager.Instance.AddPublishedFilesToDAM(this, assetids, EntityID, FolderID);
        }

        public void CreateversionDOCGeneratedAsset(int entityID, int folderID, int assetTypeID, int createdBy, string intialfilename, string assetName, int assetId, int TenantID)
        {
            DigitalAssetManager.Instance.CreateversionDOCGeneratedAsset(this, entityID, folderID, assetTypeID, createdBy, intialfilename, assetName, assetId, TenantID);
        }

        public string getkeyvaluefromwebconfig(string key)
        {
            return DigitalAssetManager.Instance.getkeyvaluefromwebconfig(this, key);
        }


        public IList<IDamFileProfile> GetProfileFilesByUser(int UserId)
        {
            return DigitalAssetManager.Instance.GetProfileFilesByUser(this, UserId);
        }
        public IList<DAMFileDao> GetAssetActiveFileinfo(int assetId)
        {
            return DigitalAssetManager.Instance.GetAssetActiveFileinfo(this, assetId);
        }

        public IList<DAMFiledownloadinfo> CropRescale(int assetId, int TopLeft, int TopRight, int bottomLeft, int bottomRight, int CropFormat, int ScaleWidth, int ScaleHeight, int Dpi, int profileid, string fileformate, int cropfrom)
        {
            return DigitalAssetManager.Instance.CropRescale(this, assetId, TopLeft, TopRight, bottomLeft, bottomRight, CropFormat, ScaleWidth, ScaleHeight, Dpi, profileid, fileformate, cropfrom);
        }
        public IList GetMimeType()
        {
            return DigitalAssetManager.Instance.GetMimeType(this);
        }

        public IList GetBreadCrumFolderPath(int EntityID, int CurrentFolderID)
        {
            return DigitalAssetManager.Instance.GetBreadCrumFolderPath(this, EntityID, CurrentFolderID);
        }

        public IList<object> GetAllFolderStructure(int EntityID)
        {
            return DigitalAssetManager.Instance.GetAllFolderStructure(this, EntityID);
        }

        public bool AddNewsFeedinfo(int assetId, string action, string TypeName)
        {
            return DigitalAssetManager.Instance.AddNewsFeedinfo(this, assetId, action, TypeName);
        }


        public bool SaveCropedImageForLightBox(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY)
        {
            return DigitalAssetManager.Instance.SaveCropedImageForLightBox(this, sourcepath, imgwidth, imgheight, imgX, imgY);
        }
        public List<object> GetMediaAssets(int viewType, int orderbyid, int pageNo, int[] assettypeArr, string filterSchema = null, int totalRecords = 0)
        {
            return DigitalAssetManager.Instance.GetMediaAssets(this, viewType, orderbyid, pageNo, assettypeArr, filterSchema, totalRecords);
        }

        public List<object> GetMediaBankFilterAttributes()
        {
            return DigitalAssetManager.Instance.GetMediaBankFilterAttributes(this);
        }

        public int MoveAssets(int[] assetid, int folderId, int entityid, int actioncode)
        {
            return DigitalAssetManager.Instance.MoveAssets(this, assetid, folderId, entityid, actioncode);
        }
        public bool DAMadminSettingsDeleteAttributeRelationAllViews(int typeid, int ID)
        {
            return DigitalAssetManager.Instance.DAMadminSettingsDeleteAttributeRelationAllViews(this, typeid, ID);

        }

        public List<object> GetSearchAssets(string assetIDs)
        {
            return DigitalAssetManager.Instance.GetSearchAssets(this, assetIDs);
        }

        public Tuple<List<object>, int> GetCustomFilterAttributes(int typeID)
        {
            return DigitalAssetManager.Instance.GetCustomFilterAttributes(this, typeID);
        }

        public int AttachAssets(int[] assetid, int entityid, int folderId, bool blnAttach)
        {
            return DigitalAssetManager.Instance.AttachAssets(this, assetid, entityid, folderId, blnAttach);
        }
        public Tuple<string, int> GetSearchCriteriaAdminSettings(string LogoSettings, string key, int typeid)
        {
            return DigitalAssetManager.Instance.GetSearchCriteriaAdminSettings(this, LogoSettings, key, typeid);
        }

        public List<int> AttachAssetsforProofTask(int[] assetid, int entityid, int folderId, bool blnAttach)
        {
            return DigitalAssetManager.Instance.AttachAssetsforProofTask(this, assetid, entityid, folderId, blnAttach);
        }

        /// <summary>
        /// Creates the proof by ProofHQ.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="taskid">The taskid.</param>
        /// <param name="assetids">int[] assetids.</param>
        /// <returns>bool</returns>
        public bool CreateProofTaskWithAttachment(int taskID, int[] assetIds, int Tasktypeid)
        {
            return DigitalAssetManager.Instance.CreateProofTaskWithAttachment(this, taskID, assetIds, Tasktypeid);
        }

        public int GetProofIDByTaskID(int taskid)
        {
            return DigitalAssetManager.Instance.GetProofIDByTaskID(this, taskid);
        }

        public int GetUserIDByEmail(string email)
        {
            return DigitalAssetManager.Instance.GetUserIDByEmail(this, email);
        }


        public IAssets ChangeAssettype(int assetTypeID, int damID)
        {
            return DigitalAssetManager.Instance.ChangeAssettype(this, assetTypeID, damID);
        }
        public IList<IEntityTypeAttributeRelationwithLevels> GetAssetAttributueswithTypeID(int assetTypeID)
        {
            return DigitalAssetManager.Instance.GetAssetAttributueswithTypeID(this, assetTypeID);
        }

        public int SaveAssetTypeofAsset(int typeid, string Name, IList<IAttributeData> listattributevalues, int AssetID, int oldAssetTypeId, IList<IDamPeriod> listperiodvalues)
        {
            return DigitalAssetManager.Instance.SaveAssetTypeofAsset(this, typeid, Name, listattributevalues, AssetID, oldAssetTypeId, listperiodvalues);
        }

        public bool SendMailWithMetaData(bool IncludeMdata, int[] idarr, string toAddress, string subject, List<KeyValuePair<string, int>> CroppedList = null)
        {
            return DigitalAssetManager.Instance.SendMailWithMetaData(this, IncludeMdata, idarr, toAddress, subject, CroppedList);
        }

        public string AssetMetadata(int[] idarr, bool IncludeMdata)
        {
            return DigitalAssetManager.Instance.AssetMetadata(this, idarr, IncludeMdata);
        }

        public string GetLuceneAssetValueDetails(int assetID, int assetTypeid)
        {
            return DigitalAssetManager.Instance.GetLuceneAssetValueDetails(this, assetID, assetTypeid);
        }

        public int AddPublishedLinkFiles(int[] assetid, int EntityID, int FolderID)
        {
            return DigitalAssetManager.Instance.AddPublishedLinkFiles(this, assetid, EntityID, FolderID);
        }
        public string GetDamTreeNode(int attributeID, bool isAdminSettings, int parententityID = 0)
        {
            return DigitalAssetManager.Instance.GetDamTreeNode(this, attributeID, isAdminSettings, parententityID);
        }
        public IDamPeriod Damperiodservice()
        {
            return DigitalAssetManager.Instance.Damperiodservice();
        }
        public IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParentDetail(int attributeID, bool isInheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0)
        {
            return DigitalAssetManager.Instance.GetDropDownTreePricingObjectFromParentDetail(this, attributeID, isInheritfromParent, isFetchParent, entityid, parentid);
        }

        public IList<DropDownTreePricing> GetDropDownTreePricingObjectDetail(int attributeID, bool isInheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0)
        {
            return DigitalAssetManager.Instance.GetDropDownTreePricingObjectDetail(this, attributeID, isInheritfromParent, isFetchParent, entityid, parentid);
        }
        public bool UpdateDropDownTreePricing(int EntityID, int AttributeTypeid, int Attributeid, IList<IDamTreeValue> NewValue)
        {
            return DigitalAssetManager.Instance.UpdateDropDownTreePricing(this, EntityID, AttributeTypeid, Attributeid, NewValue);
        }

        public int CreateAssetPeriod(int assetid, DateTime Startdate, DateTime EndDate, string Description, int SortOrder, int attributeid)
        {
            return DigitalAssetManager.Instance.CreateAssetPeriod(this, assetid, Startdate, EndDate, Description, SortOrder, attributeid);
        }

        public bool UpdateAssetPeriod(int ID, int assetID, string StartDate, string EndDate, int SortOrder, string Description, int attributeid)
        {
            return DigitalAssetManager.Instance.UpdateAssetPeriod(this, ID, assetID, StartDate, EndDate, SortOrder, Description, attributeid);
        }

        public bool DeleteAssetPeriod(int ID)
        {
            return DigitalAssetManager.Instance.DeleteAssetPeriod(this, ID);
        }
        public string GetAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent, bool isinheritfromParent = false)
        {
            return DigitalAssetManager.Instance.GetAttributeTreeNodeFromParent(this, attributeID, entityID, isChoosefromParent, isinheritfromParent);
        }
        public string GetDetailAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent)
        {
            return DigitalAssetManager.Instance.GetDetailAttributeTreeNodeFromParent(this, attributeID, entityID, isChoosefromParent);
        }
        public string GetAttributeTreeNode(int attributeID, int entityID)
        {
            return DigitalAssetManager.Instance.GetAttributeTreeNode(this, attributeID, entityID);
        }
        public bool SaveDetailBlockForTreeLevels(int EntityID, int AttributeTypeid, int attributeid, IList<IDamTreeValue> NewValue, JArray jroldTree, JArray jrnewtree)
        {
            return DigitalAssetManager.Instance.SaveDetailBlockForTreeLevels(this, EntityID, AttributeTypeid, attributeid, NewValue, jroldTree, jrnewtree);
        }
        public List<object> GetLightboxAssets(int[] assetArr, int viewType, int orderbyid, int pageNo, bool ViewAll = false)
        {
            return DigitalAssetManager.Instance.GetLightboxAssets(this, assetArr, viewType, orderbyid, pageNo, ViewAll);
        }
        public bool UpdateAssetThumpnail(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, string Preview, string OriginalAssetName, string ext, int assetId = 0)
        {
            return DigitalAssetManager.Instance.UpdateAssetThumpnail(this, sourcepath, imgwidth, imgheight, imgX, imgY, Preview, OriginalAssetName, ext, assetId);
        }

        public bool DeleteAssetAccess(int AssetID)
        {
            return DigitalAssetManager.Instance.DeleteAssetAccess(this, AssetID);
        }

        public IList GetlistoflinkedAsset(int EntityID, int[] assetArr)
        {
            return DigitalAssetManager.Instance.GetlistoflinkedAsset(this, EntityID, assetArr);
        }

        public List<object> IsActiveLinkEntity(int entityID)
        {
            return DigitalAssetManager.Instance.IsActiveLinkEntity(this, entityID);
        }

        public string[] DownloadAssetWithMetadata(int[] assetid, bool include)
        {
            return DigitalAssetManager.Instance.DownloadAssetWithMetadata(this, assetid, include);
        }
        public int CreateReformatProfile(int ProfileID, string Name, int Units, JObject srcValue)
        {
            return DigitalAssetManager.Instance.CreateReformatProfile(this, ProfileID, Name, Units, srcValue);
        }

        public IList<DamReformatProfileDao> GetReformatProfile(int ProfileID)
        {
            return DigitalAssetManager.Instance.GetReformatProfile(this, ProfileID);
        }

        public Tuple<bool, string, string, string, int> ReformatRescale(int assetId, int profileid, int Units, int Folderid, JObject srcValue)
        {
            return DigitalAssetManager.Instance.ReformatRescale(this, assetId, profileid, Units, Folderid, srcValue);
        }


        public bool UnPublishAssets(int[] idArr, bool IsPublished, int PublisherID)
        {
            return DigitalAssetManager.Instance.UnPublishAssets(this, idArr, IsPublished, PublisherID);
        }
        public int ReformateCreateAsset(int assetid, int EntityID, int FolderID)
        {
            return DigitalAssetManager.Instance.ReformateCreateAsset(this, assetid, EntityID, FolderID);
        }


        public List<object> GetAssetDetsforSelectedAssets(int folderid, int entityID, int viewType, int orderby, int pageNo, int[] assetList, bool ViewAll = false)
        {
            return DigitalAssetManager.Instance.GetAssetDetsforSelectedAssets(this, folderid, entityID, viewType, orderby, pageNo, assetList, ViewAll);
        }



        public List<object> GetAssetsbasedonFolderandEntity(int entityID, int folderID, int viewType, int orderby, bool ViewAll = false)
        {
            return DigitalAssetManager.Instance.GetAssetsbasedonFolderandEntity(this, entityID, folderID, viewType, orderby, ViewAll);
        }

        public string GetExternalAssetContent()
        {
            return DigitalAssetManager.Instance.GetExternalAssetContent(this);
        }

        public bool InsertUpdateExternalAssetContent(string htmlcontent)
        {
            return DigitalAssetManager.Instance.InsertUpdateExternalAssetContent(this, htmlcontent);
        }

        public string GetAssetCategoryTreeCollection()
        {
            return DigitalAssetManager.Instance.GetAssetCategoryTreeCollection(this);
        }

        public bool InsertUpdateAssetCatergory(JObject jobj)
        {
            return DigitalAssetManager.Instance.InsertUpdateAssetCatergory(this, jobj);
        }

        public int DeleteAssetCategory(int ID)
        {
            return DigitalAssetManager.Instance.DeleteAssetCategory(this, ID);
        }

        public IList GetAssetTypeAssetCategoryRelation()
        {
            return DigitalAssetManager.Instance.GetAssetTypeAssetCategoryRelation(this);
        }

        public string GetSelectedCategoryTreeCollection(int CategoryID)
        {
            return DigitalAssetManager.Instance.GetSelectedCategoryTreeCollection(this, CategoryID);
        }

        public IList InsertUpdateAssetCategoryRelation(int AssetTypeID, int CategoryID)
        {
            return DigitalAssetManager.Instance.InsertUpdateAssetCategoryRelation(this, AssetTypeID, CategoryID);
        }

        public string GetAssetCategoryTree(int AsetTypeID)
        {
            return DigitalAssetManager.Instance.GetAssetCategoryTree(this, AsetTypeID);
        }

        public List<object> GetAssetCategoryPathInAssetEdit(int AssetTypeID)
        {
            return DigitalAssetManager.Instance.GetAssetCategoryPathInAssetEdit(this, AssetTypeID);
        }
        public bool UpdateAssetImageName(int entityId, int attributeId, string imageName)
        {
            return DigitalAssetManager.Instance.UpdateAssetImageName(this, entityId, attributeId, imageName);
        }

        public IList GetAssetTypeFileExtensionRelation()
        {
            return DigitalAssetManager.Instance.GetAssetTypeFileExtensionRelation(this);
        }

        public bool InsertAssetTypeFileExtension(int ID, int AssetTypeID, string FileExt, string category)
        {
            return DigitalAssetManager.Instance.InsertAssetTypeFileExtension(this, ID, AssetTypeID, FileExt, category);
        }

        public bool DeleteAssetTypeFileExtension(int ID)
        {
            return DigitalAssetManager.Instance.DeleteAssetTypeFileExtension(this, ID);
        }

        public bool GetAssetSnippetTemplates(JObject jsonXML)
        {
            return DigitalAssetManager.Instance.GetAssetSnippetTemplates(this, jsonXML);
        }

        public string getassetSnippetData()
        {
            return DigitalAssetManager.Instance.getassetSnippetData(this);
        }

        public Tuple<string, int> Get2ImagineTemplateUrl(int entityID, int folderID, int userID, int templateType, string DocID, string serverURL, int assetType)
        {
            return DigitalAssetManager.Instance.Get2ImagineTemplateUrl(this, entityID, folderID, userID, templateType, DocID, serverURL, assetType);
        }

        public dynamic Download2ImagineAsset(int entityID, int folderID, int userID, int docVersionID, string serverURL)
        {
            return DigitalAssetManager.Instance.Download2ImagineAsset(this, entityID, folderID, userID, docVersionID, serverURL);
        }
        public IAssetAmountCurrencyType AssetAmountCurrencyTypeservice()
        {
            return DigitalAssetManager.Instance.AssetAmountCurrencyTypeservice();
        }
        //Get2ImagineTemplateUrl
        public int saveTemplateEngine(JObject jObj)
        {
            return DigitalAssetManager.Instance.saveTemplateEngine(this, jObj);
        }
        public IList<TemplateEnginesDao> getAllTemplateEngines()
        {
            return DigitalAssetManager.Instance.getAllTemplateEngines(this);
        }
        public bool deleteTemplateEngine(int ID)
        {
            return DigitalAssetManager.Instance.deleteTemplateEngine(this, ID);
        }
        public IList<TemplateEnginesDao> getServerUrls()
        {
            return DigitalAssetManager.Instance.getServerUrls(this);
        }
        public OptimakerSettingsDao getCurrentTemplateAssetInfo(int DocID)
        {
            return DigitalAssetManager.Instance.getCurrentTemplateAssetInfo(this, DocID);
        }
        public string Get2ImagineResourceUrl(string documentId, string serverURL)
        {
            return DigitalAssetManager.Instance.Get2ImagineResourceUrl(this, documentId, serverURL);
        }
        public bool IsFileExist(string filePath)
        {
            return DigitalAssetManager.Instance.IsFileExist(this, filePath);
        }

        public string S3Settings()
        {
            return DigitalAssetManager.Instance.S3Settings(this);
        }

        public string copydeleteTemplateAsset(string tempguid)
        {
            return DigitalAssetManager.Instance.copydeleteTemplateAsset(this, tempguid);
        }

        public string CheckAccessToPublish(int Userid, int EntityId)
        {
            return DigitalAssetManager.Instance.CheckAccessToPublish(this, Userid, EntityId);
        }
        public bool writeUploadErrorlog(JObject errorloginfo)
        {
            return DigitalAssetManager.Instance.writeUploadErrorlog(this, errorloginfo);
        }
        public bool CreateNewAssetWithFolder(int EntityID, JArray AssetObject,int ImpersonateUID=0)
        {
            return DigitalAssetManager.Instance.CreateNewAssetWithFolder(this, EntityID, AssetObject, ImpersonateUID);
        }
    }
}
