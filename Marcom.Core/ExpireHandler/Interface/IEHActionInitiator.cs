﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ExpireHandler.Interface
{
    public interface IEHActionInitiator
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int AttributeID
        {
            get;
            set;
        }

        int EntityID
        {
            get;
            set;
        }

        int ActionSrcID
        {
            get;
            set;
        }
        DateTime ExecutionDate
        {
            get;
            set;
        }

        int TimeDuration
        {
            get;
            set;
        }

        int ActionPlace
        {
            get;
            set;
        }

        int ActionType
        {
            get;
            set;
        }

        string ActionSrcValue
        {
            get;
            set;
        }

        
        int Status
        {
            get;
            set;
        }

        DateTime UpdatedOn
        {
            get;
            set;

        }

        int ActionownerId
        {
            get;
            set;
        }

        
        #endregion
    }
}
