﻿using System;
using System.Collections.Generic;
using System.Collections;


namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class FinancialOptionDao : BaseDao, ICloneable
    {

        #region member Variables

        protected int _id { get; set; }
        protected string _caption { get; set; }
        protected int _finattributeid { get; set; }
        protected int _sortorder { get; set; }
        #endregion

        #region Constructors
        public FinancialOptionDao() { }

        public FinancialOptionDao(int pid, string pcaption, int pfinattributeid, int psortorder)
        {
            this._id = pid;
            this._caption = pcaption;
            this._finattributeid = pfinattributeid;
            this._sortorder = psortorder;
        }
        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }
        public virtual int FinAttributeID
        {
            get { return _finattributeid; }
            set { _finattributeid = value; }

        }
        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        public static class PropertyNames
        {
            //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)

            public const string FinancialOptionDao = "FinancialOptionDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string FinAttributeID = "FinAttributeID";
            public const string SortOrder = "SortOrder";

        }


        public static class MappingNames
        {
            //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)

            public const string FinancialOptionDao = "FinancialOptionDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string FinAttributeID = "FinAttributeID";
            public const string SortOrder = "SortOrder";

        }


        #endregion
    }

}



