﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IAttributeGroupAttributeRelation
    {
        #region Public Properties

        int ID
        {
            get;
            set;
        }

        int AttributeGroupID
        {
            get;
            set;

        }
        int AttributeID
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }
        int SortOrder
        {
            get;
            set;

        }


        bool IsSearchable
        {

            get;
            set;

        }

        bool ShowAsColumn
        {

            get;
            set;

        }

        #endregion
    }
}
