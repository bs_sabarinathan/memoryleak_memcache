﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// AttachmentsDao object for table 'PM_Attachments'.
    /// </summary>

    public partial class TaskDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _stepId;
        protected int _entityid;
        protected int _memberID;
        protected string _name;
        protected string _description;
        protected int _taskstatus;
        protected DateTime _duedate;
        protected int _predefinedTaskId;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TaskDao() { }

        public TaskDao(int pStepid, int pEntityid,int pMemberID, string pName, string pDescription, int pTaskStatus, DateTime pDueDate,int pPredefinedTaskID)
        {
            this._stepId = pStepid;
            this._entityid = pEntityid;
            this._memberID = pMemberID;
            this._name = pName;
            this._description = pDescription;
            this._taskstatus = pTaskStatus;
            this._duedate = pDueDate;
            this._memberID = pMemberID;
            this._predefinedTaskId = pPredefinedTaskID;

        }

        public TaskDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual int MemberID
        {
            get { return _memberID; }
            set { _bIsChanged |= (_memberID != value); _memberID = value; }

        }

        public virtual int StepID
        {
            get { return _stepId; }
            set { _bIsChanged |= (_stepId != value); _stepId = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 500 characters");
                _bIsChanged |= (_name != value);
                _description = value;
            }

        }


        public virtual int TaskStatus
        {
            get { return _taskstatus; }
            set { _bIsChanged |= (_taskstatus != value); _taskstatus = value; }

        }
      
        public virtual DateTime DueDate
        {
            get { return _duedate; }
            set { _bIsChanged |= (_duedate != value); _duedate = value; }

        }

        public virtual int PredefinedTaskID
        {
            get { return _predefinedTaskId; }
            set { _bIsChanged |= (_predefinedTaskId != value); _predefinedTaskId = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttachmentsDao = "TaskDao";
            public const string ID = "ID";
            public const string Entityid = "EntityId";
            public const string MemberID = "MemberID";
            public const string StepID = "StepID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TaskStatus = "TaskStatus";
            public const string DueDate = "DueDate";
            public const string PredefinedTaskID = "PredefinedTaskID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttachmentsDao = "TaskDao";
            public const string ID = "ID";
            public const string Entityid = "EntityId";
            public const string MemberID = "MemberID";
            public const string StepID = "StepID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TaskStatus = "TaskStatus";
            public const string DueDate = "DueDate";
            public const string PredefinedTaskID = "PredefinedTaskID";
        }

        #endregion


    }

}
