﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolSubPageCreationCtrl($scope, $resource, $timeout, $location, $cookies, $window, $compile, $translate, $modalInstance, params, SubpagecreationService) {
        var model;
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath.replace(/\\/g, "\/");

        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            imagesrcpath = cloudpath;
        }
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope[model1] = true;
        };
        $scope.dynCalanderopen = function ($event, isopen, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };
        $scope.AutoCompleteSelectedUserObj = { "UserSelection": {} };
        $scope.SelectedDate = null;
        $scope.CmsTimeZone = "00:00";
        $scope.TimeAround = [{
            name: '00:00'
        }, {
            name: '00:30'
        }, {
            name: '01:00'
        }, {
            name: '01:30'
        }, {
            name: '02:00'
        }, {
            name: '02:30'
        }, {
            name: '03:00'
        }, {
            name: '03:30'
        }, {
            name: '04:00'
        }, {
            name: '04:30'
        }, {
            name: '05:00'
        }, {
            name: '05:30'
        }, {
            name: '06:00'
        }, {
            name: '06:30'
        }, {
            name: '07:00'
        }, {
            name: '07:30'
        }, {
            name: '08:00'
        }, {
            name: '08:30'
        }, {
            name: '09:00'
        }, {
            name: '09:30'
        }, {
            name: '10:00'
        }, {
            name: '10:30'
        }, {
            name: '11:00'
        }, {
            name: '11:30'
        }, {
            name: '12:00'
        }, {
            name: '12:30'
        }, {
            name: '13:00'
        }, {
            name: '13:30'
        }, {
            name: '14:00'
        }, {
            name: '14:30'
        }, {
            name: '15:00'
        }, {
            name: '15:30'
        }, {
            name: '16:00'
        }, {
            name: '16:30'
        }, {
            name: '17:00'
        }, {
            name: '17:30'
        }, {
            name: '18:00'
        }, {
            name: '18:30'
        }, {
            name: '19:00'
        }, {
            name: '19:30'
        }, {
            name: '20:00'
        }, {
            name: '20:30'
        }, {
            name: '21:00'
        }, {
            name: '21:30'
        }, {
            name: '22:00'
        }, {
            name: '22:30'
        }, {
            name: '23:00'
        }, {
            name: '23:30'
        }];
        $scope.metadatano = 1;
        $scope.atachmentno = 2;
        $scope.financialno = 3;
        $scope.objectiveno = 4;
        $scope.memberno = 3;
        GetEntityTypeTabCollections();

        function GetEntityTypeTabCollections() {
            SubpagecreationService.GetPlantabsettings().then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.ShowFinancial = gettabresult.Response.Financials;
                    $scope.ShowObjective = gettabresult.Response.Objectives;
                    if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false") {
                        $scope.memberno = 2;
                    } else if ($scope.ShowObjective == "false") {
                        $scope.financialno = 2;
                        $scope.memberno = 3;
                    } else if ($scope.ShowFinancial == "false") {
                        $scope.objectiveno = 2;
                        $scope.memberno = 3;
                    }
                }
            });
        }
        $scope.Dropdown = [];
        var totalWizardSteps = $('#subpageMyWizard').wizard('totnumsteps').totstep;
        $('#btnWizardFinish').hide();
        $('#btnWizardrNext').show();
        $('#btnWizardPrev').hide();
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.changeTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var step = $('#subpageMyWizard').wizard('selectedItem').step;
            var currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#subpageMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1 || currentWizardStep == 2) {
                $('#subpageMyWizard').wizard('next', '');
                currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            } else if (currentWizardStep > totalWizardSteps) {
                $('#subpageMyWizard').wizard('previous', '');
                $('#subpageMyWizard').wizard('manualpreviousstep', currentWizardStep - 3);
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === 3) { }
        }
        $scope.changeTab2 = function (e) {
            $("#btnTempSub").click();
            $("#subpageMyWizard").removeClass('notvalidate');
            if ($("#subpageMyWizard .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }
        $scope.changeSubentityTab2 = function (e) {
            $("#btnTempSub").click();
            $("#SubPageMetadata").removeClass('notvalidate');
            if ($("#SubPageMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() { }
        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#subpageMyWizard').wizard('totnumsteps').totstep;
            if ($scope.ShowObjective == "false" && currentWizardStep == totalWizardSteps) {
                currentWizardStep = 2;
                if ($scope.ShowFinancial == "false") {
                    currentWizardStep = 1;
                }
                $('#subpageMyWizard').wizard('manualpreviousstep', currentWizardStep);
            } else if ($scope.ShowFinancial == "false" && (currentWizardStep == 2 || currentWizardStep == 3)) {
                currentWizardStep = 1;
                $('#subpageMyWizard').wizard('manualpreviousstep', currentWizardStep);
            } else {
                $('#subpageMyWizard').wizard('previous', '');
                currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
            }
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#btnWizardNext').show();
            } else {
                $('#btnWizardNext').hide();
            }
        }
        $scope.changenexttab = function () {
            EntityMetadata();
            $("#SubPageMetadata").removeClass('notvalidate');
            $("#btnTempSub").click();
            if ($("#SubPageMetadata .error").length > 0) {
                return false;
            }
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#subpageMyWizard').wizard('totnumsteps').totstep;
            if ($scope.ShowFinancial == "false" && currentWizardStep == 1) {
                currentWizardStep = 3;
                if ($scope.ShowObjective == "false" && currentWizardStep == 3) {
                    currentWizardStep = 4;
                }
                $('#subpageMyWizard').wizard('manualnextstep', currentWizardStep);
            } else if ($scope.ShowObjective == "false" && currentWizardStep == 2) {
                currentWizardStep = 4;
                $('#subpageMyWizard').wizard('manualnextstep', currentWizardStep);
            } else {
                $('#subpageMyWizard').wizard('next', '');
                currentWizardStep = $('#subpageMyWizard').wizard('selectedItem').step;
            }
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
            } else {
                $('#btnWizardNext').show();
            }
        }
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.multiselecttreeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.Inherritingtreelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.Parentid = 0;
        $scope.costCenterList = [];
        $scope.MemberLists = [];
        $scope.costcemtreObject = [];
        $scope.count = 1;
        $scope.createCostCentre = function (CurrentEntityId) {
            SubpagecreationService.GetCostcentreforEntityCreation($scope.EntitytypeID, 2, parseInt(CurrentEntityId, 10)).then(function (costCenterList) {
                $scope.costCenterList = costCenterList.Response;
                $scope.costcemtreObject = [];
                $scope.MemberLists = [];
                for (var i = 0; i < $scope.costCenterList.length; i++) {
                    $scope.costcemtreObject.push({
                        "CostcenterId": $scope.costCenterList[i].Id,
                        "costcentername": $scope.costCenterList[i].costcentername,
                        "Sortorder": 1,
                        "Isassociate": 1,
                        "Isactive": 1,
                        "OwnerName": $scope.costCenterList[i].username,
                        "OwnerID": $scope.costCenterList[i].UserID
                    });
                    $scope.count = $scope.count + 1;
                }
            });
        }
        $scope.GlobalMembersList = [];
        $scope.createGlobalMembers = function (CurrentEntityId) {
            SubpagecreationService.GetGlobalMembers(parseInt(CurrentEntityId, 10)).then(function (GlobalMembersList) {
                if (GlobalMembersList.Response != null && GlobalMembersList.Response.length > 0) {
                    $scope.GlobalMembersList = GlobalMembersList.Response;
                    for (var i = 0; i < $scope.GlobalMembersList.length; i++) {
                        if (parseInt($scope.GlobalMembersList[i].RoleID, 10) == 1) $scope.MemberLists.push({
                            "TID": $scope.count,
                            "UserEmail": $scope.GlobalMembersList[i].Email,
                            "DepartmentName": "-",
                            "Title": "-",
                            "Roleid": 2,
                            "RoleName": "Editor",
                            "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10),
                            "UserName": $scope.GlobalMembersList[i].UserName,
                            "IsInherited": $scope.GlobalMembersList[i].IsInherited,
                            "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID,
                            "FromGlobal": 1,
                            "CostCentreID": 0,
                            "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName
                        });
                        else $scope.MemberLists.push({
                            "TID": $scope.count,
                            "UserEmail": $scope.GlobalMembersList[i].Email,
                            "DepartmentName": "-",
                            "Title": "-",
                            "Roleid": parseInt($scope.GlobalMembersList[i].RoleID, 10),
                            "RoleName": $scope.GlobalMembersList[i].Caption,
                            "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10),
                            "UserName": $scope.GlobalMembersList[i].UserName,
                            "IsInherited": $scope.GlobalMembersList[i].IsInherited,
                            "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID,
                            "FromGlobal": 1,
                            "CostCentreID": 0,
                            "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName
                        });
                        $scope.count = $scope.count + 1;
                    }
                }
            });
        }
        $scope.addcostCentre = function () {
            if ($scope.costcentreid != undefined && $scope.costcentreid != null && $scope.costcentreid != "") {
                var result = $.grep($scope.costcemtreObject, function (e) {
                    return e.CostcenterId == $scope.costcentreid;
                });
                if (result.length == 0) {
                    var costCentrevalues = $.grep($scope.costCenterList, function (e) {
                        return e.Id == parseInt($scope.costcentreid)
                    })[0];
                    $scope.costcemtreObject.push({
                        "CostcenterId": $scope.costcentreid,
                        "costcentername": costCentrevalues.costcentername,
                        "Sortorder": 1,
                        "Isassociate": 1,
                        "Isactive": 1,
                        "OwnerName": costCentrevalues.username,
                        "OwnerID": costCentrevalues.UserID
                    });
                    var memberObj = $.grep($scope.MemberLists, function (e) {
                        return e.Roleid == 8 && e.Userid == $scope.costcentreid;
                    });
                    if (memberObj.length == 0) {
                        $scope.MemberLists.push({
                            "TID": $scope.count,
                            "UserEmail": costCentrevalues.usermail,
                            "DepartmentName": costCentrevalues.Designation,
                            "Title": costCentrevalues.Title,
                            "Roleid": 8,
                            "RoleName": 'BudgetApprover',
                            "Userid": costCentrevalues.UserID,
                            "UserName": costCentrevalues.username,
                            "IsInherited": '0',
                            "InheritedFromEntityid": '0',
                            "FromGlobal": 1,
                            "CostCentreID": $scope.costcentreid
                        });
                        $scope.count = $scope.count + 1;
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1985.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
            }
            $scope.costcentreid = "";
        }
        $scope.deleteCostCentre = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
                        var memberToRempve = $.grep($scope.MemberLists, function (e) {
                            return e.CostCentreID == item.CostcenterId
                        });
                        if (memberToRempve.length > 0) $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };
        $scope.subEntityName = "";
        $scope.EntitytypeID = 0;
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.AutoCompleteSelectedObj = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};
        SubpagecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({
                "Roleid": 1,
                "RoleName": "Owner",
                "UserEmail": $scope.ownerEmail,
                "DepartmentName": $scope.owner.Designation,
                "Title": $scope.owner.Title,
                "Userid": parseInt($scope.OwnerID, 10),
                "UserName": $scope.OwnerName,
                "IsInherited": '0',
                "InheritedFromEntityid": '0',
                "QuickInfo1": $scope.owner.QuickInfo1,
                "QuickInfo2": $scope.owner.QuickInfo2
            });
        })
        $scope.contrls = '';

        function GetEntityTypeRoleAccess(rootID) {
            SubpagecreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }
        $scope.wizard = {
            newval: ''
        };
        $scope.OptionObj = {};
        $scope.UserimageNewTime = new Date.create().getTime().toString();
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.dyn_Cont = '';
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.AttributeData = [];
        $scope.AutoCompleteSelectedUserObj = {"UserSelection": {} };
        $scope.entityName = "";
        $scope.EntityMemberData = [];
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10) && e.FromGlobal == 0;
            });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.fields['userRoles'])
                })[0];
                if (membervalues != undefined) {
                    $scope.MemberLists.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                        "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                        "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                        "Roleid": parseInt($scope.fields['userRoles'], 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                        "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "InheritedFromEntityName": $scope.AutoCompleteSelectedUserObj.UserSelection.InheritedFromEntityName,
                        "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2
                    });
                    $scope.EntityMemberData.push({
                        "TID": $scope.count,
                        "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                        "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                        "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                        "Roleid": parseInt($scope.fields['userRoles'], 10),
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                        "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "InheritedFromEntityName": $scope.AutoCompleteSelectedUserObj.UserSelection.InheritedFromEntityName,
                        "QuickInfo1": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo1,
                        "QuickInfo2": $scope.AutoCompleteSelectedUserObj.UserSelection.QuickInfo2
                    });
                    $scope.count = $scope.count + 1;
                    $scope.fields.usersID = '';

                    $scope.AutoCompleteSelectedObj = [];
                    $scope.AutoCompleteSelectedUserObj.UserSelection = {};
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };
        $scope.deleteOptions = function (users) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.MemberLists.splice($.inArray(users, $scope.MemberLists), 1);
                        $scope.EntityMemberData.splice($.inArray(users, $scope.EntityMemberData), 1);
                    }, 100);
                }
            });
        };
        $scope.items = [];
        $scope.subentityperiods = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: [],
                    endDate: [],
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.FinancialRequestObj = [];
        $scope.ImageFileName = '';
        $scope.nonMandatoryIDList = [];

        $scope.saveCmsSubPageEntity = function () {
            $("#SubPageMetadata").removeClass('notvalidate');
            $("#btnTempSub").click();
            if ($("#SubPageMetadata .error").length > 0) {
                return false;
            }
            $("#btnsubPageEntity").attr('disabled', 'disabled');

            if (uploaderAttrInfo.length > 0) {
                SubpagecreationService.copyuploadedImage(uploaderAttrInfo[0].attrFileName).then(function (newuplAttrName) {
                    var attrRelObj = $.grep($scope.atributesRelationList, function (e) { return e.AttributeID == uploaderAttrInfo[0].attrID });
                    var attrRelIndex = $scope.atributesRelationList.indexOf(attrRelObj[0]);
                    $scope.atributesRelationList[attrRelIndex].attrFileID = newuplAttrName.Response;
                    saveCmsSubPageEntity();
                });
            }
            else
                saveCmsSubPageEntity();
        }

        function saveCmsSubPageEntity() {
            
            $scope.AttributeData = [];
            for (var i = 0; i < $scope.nonMandatoryIDList.length; i++) {
                $scope.IDList.push($scope.nonMandatoryIDList[i]);
            }
            if ($scope.IsObjectiveFired == false) {
                var metadata = [];
                metadata = FormObjectiveMetadata();
                var entityMetadataObj = {};
                entityMetadataObj.AttributeData = metadata;
                entityMetadataObj.Periods = [];
                GetObjectivePeriods();
                entityMetadataObj.Periods.push($scope.subentityperiods);
                entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
                SubpagecreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                    try {
                        UpdateObjectiveScope(entityAttributeDataObjData.Response);
                        $timeout(function () {
                            ProcessEntityCreation();
                        }, 100);
                    } catch (ex) {
                        $timeout(function () {
                            ProcessEntityCreation();
                        }, 100);
                    }
                });
            } else {
                $timeout(function () {
                    ProcessEntityCreation();
                }, 100);
            }
        };

        function ProcessEntityCreation() {
            $scope.Entityamountcurrencytypeitem = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                    });
                                }
                            } else {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(value, 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                   var MyDate = new Date.create();
                    var MyDateString;
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] == null) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5746.Caption'));
                            $("#btnsubPageEntity").removeAttr('disabled');
                            return false;
                        } else {
                            MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                        }
                    } else {
                        MyDateString = "";
                    }

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10)
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].attrFileID == undefined || $scope.atributesRelationList[i].attrFileID == null) {
                        $scope.atributesRelationList[i].attrFileID = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.atributesRelationList[i].attrFileID,
                        "Level": 0
                    });
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({
                        amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                        currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                        Attributeid: $scope.atributesRelationList[i].AttributeID
                    });
                }
            }
            var SaveEntity = {};
            SaveEntity.ParentId = parseInt($scope.Parentid, 10);
            SaveEntity.Typeid = parseInt($scope.EntitytypeID, 10);
            SaveEntity.Active = true;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.entityName;
            SaveEntity.EntityMembers = [];
            SaveEntity.EntityCostRelations = $scope.costcemtreObject;
            SaveEntity.Periods = [];
            SaveEntity.NavID = $scope.GlobalNavigationID.NavigationID;
            SaveEntity.TemplateID = 1;
            SaveEntity.PublishedDate = ConvertDateToString($scope.SelectedDate == null ? new Date.create() : $scope.SelectedDate);
            SaveEntity.PublishedTime = $scope.CmsTimeZone == undefined ? "00:00" : $scope.CmsTimeZone;
            $scope.savesubentityperiods = [];
            SaveEntity.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.savesubentityperiods.push({
                            startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                            endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                            comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
            }
            SaveEntity.Periods.push($scope.savesubentityperiods);
            SaveEntity.AttributeData = $scope.AttributeData;
            SaveEntity.EntityMembers = $scope.EntityMemberData;
            SaveEntity.IObjectiveEntityValue = {};
            SaveEntity.IObjectiveEntityValue = $scope.IDList;
            SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;
            SaveEntity.AssetArr = $scope.PageAsset.CmsAssetSelectionFiles;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({
                    amount: $scope.Entityamountcurrencytypeitem[m].amount,
                    currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                    Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                });
            }
            SaveEntity.Entityamountcurrencytype.push($scope.curram);
            SubpagecreationService.CreateCmsPageEntity(SaveEntity).then(function (EntityList) {
                if (EntityList.Response > 0) {
                    $scope.EntityMemberData = [];
                    $scope.MemberLists = [];
                    $scope.fields = [];
                    $scope.entityObjectiveList = {};
                    $scope.entityAllObjectives = {};
                    $scope.MandatoryEntityObjectives = {};
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    $scope.nonMandatoryIDList = [];
                    $('#dynamicControls').modal('hide');
                    $('#moduleContextmenu').modal('hide');
                    $scope.Activity.IsActivitySectionLoad = true;
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    $("#CmsListHolder").trigger("ReloadPageTree", [EntityList.Response, $scope.UniqueKey]);
                    $scope.OwnerList = [];
                    $scope.treePreviewObj = {};
                    $scope.OwnerList.push({
                        "Roleid": 1,
                        "RoleName": "Owner",
                        "UserEmail": $scope.ownerEmail,
                        "DepartmentName": "-",
                        "Title": "-",
                        "Userid": parseInt($scope.OwnerID, 10),
                        "UserName": $scope.OwnerName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "InheritedFromEntityName": "-"
                    });
                    $scope.closeSubPageCreationPopupClose();
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
                    $("#btnWizardFinish").attr('disabled', false);
                    $scope.closeSubPageCreationPopupClose();
                }
            });
        }
        $scope.UniqueKey = "";
        $(window).on("GloblaUniqueKeyAccess", function (event, UniqueKey) {
            $scope.UniqueKey = UniqueKey;
        });
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.MultiSelecttree = {};
        $scope.Options = {};
        $("#cmspageMetadataModel").on("onSubEntityCreation", function (event, Id, Caption, CurrentEntityId) {
            $("#btnsubPageEntity").removeAttr('disabled');
            GetEntityTypeRoleAccess(Id);
            $('#subpageMyWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
            $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.owner = {};
            SubpagecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
                $scope.owner = Getowner.Response;
                $scope.OwnerList.push({
                    "Roleid": 1,
                    "RoleName": "Owner",
                    "UserEmail": $scope.ownerEmail,
                    "DepartmentName": $scope.owner.Designation,
                    "Title": $scope.owner.Title,
                    "Userid": parseInt($scope.OwnerID, 10),
                    "UserName": $scope.OwnerName,
                    "IsInherited": '0',
                    "InheritedFromEntityid": '0',
                    "QuickInfo1": $scope.owner.QuickInfo1,
                    "QuickInfo2": $scope.owner.QuickInfo2
                });
            })
            $scope.Parentid = CurrentEntityId;
            $scope.createCostCentre(CurrentEntityId);
            $scope.createGlobalMembers(CurrentEntityId);
            $timeout(function () {
                $scope.CreateDynamicControls(Id, Caption, CurrentEntityId);
            }, 300);
        });
        function SubEntityCreation( Id, Caption, CurrentEntityId) {
            $("#btnsubPageEntity").removeAttr('disabled');
            GetEntityTypeRoleAccess(Id);
            $('#subpageMyWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#btnWizardNext').show();
            $('#btnWizardPrev').hide();
           // $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.owner = {};
            //SubpagecreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            //    $scope.owner = Getowner.Response;
            //    $scope.OwnerList.push({
            //        "Roleid": 1,
            //        "RoleName": "Owner",
            //        "UserEmail": $scope.ownerEmail,
            //        "DepartmentName": $scope.owner.Designation,
            //        "Title": $scope.owner.Title,
            //        "Userid": parseInt($scope.OwnerID, 10),
            //        "UserName": $scope.OwnerName,
            //        "IsInherited": '0',
            //        "InheritedFromEntityid": '0',
            //        "QuickInfo1": $scope.owner.QuickInfo1,
            //        "QuickInfo2": $scope.owner.QuickInfo2
            //    });
           // })
            $scope.Parentid = CurrentEntityId;
            $scope.createCostCentre(CurrentEntityId);
            $scope.createGlobalMembers(CurrentEntityId);
            $timeout(function () {
                $scope.CreateDynamicControls(Id, Caption, CurrentEntityId);
            }, 300);
        }
        $scope.CreateDynamicControls = function (Id, Caption, CurrentEntityId) {
            $("#btnWizardFinish").removeAttr('disabled');
            $("#SubPageMetadata").html('');
            $("#SubPageMetadata").scrollTop(0);
            $("#SubPageMetadata").html('<div class="loadingSubPopup"><img src="assets/img/loading.gif"> Loading...</div>');
            $scope.subEntityName = Caption;
            $scope.EntitytypeID = Id;
            $scope.items = [];
            SubpagecreationService.GetAttributeToAttributeRelationsByIDForEntity(Id).then(function (entityAttrToAttrRelation) {
                $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
            });
            SubpagecreationService.GetEntityTypeAttributeRelationWithLevelsByID(Id, CurrentEntityId).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                $scope.dyn_Cont = '';
                $scope.SelectedDate = null;
                $scope.CmsTimeZone = "00:00";
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }
                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        } catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        GetTreeObjecttoSave($scope.atributesRelationList[i].AttributeID);
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        } else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                } else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        for (k; k < ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].length) ; k++) {
                                            if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k].trim());
                                            } else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    } else {
                                        if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                            $scope.InheritingLevelsitems.push({
                                                caption: LabelObject[j].Label,
                                                level: j + 1
                                            });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                            $scope.settreeTexts();
                                        } else {
                                            $scope.InheritingLevelsitems.push({
                                                caption: LabelObject[j].Label,
                                                level: j + 1
                                            });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        } catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                            } else {
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            }
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        if ($scope.atributesRelationList[i].IsReadOnly) $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown disabled ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                        else $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\"></select></div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><div class=\"CMS-PublishedOn-DT-Picker\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'Isopened')\"  datepicker-popup=\"{{format}}\"  is-open=\"Isopened\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\">";
                            $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<select ui-select2 id=TimeZone name=TimeZone ng-model=\"CmsTimeZone\"><option value=\"{{timeperiod.name}}\" ng-repeat=\"timeperiod in TimeAround\">{{timeperiod.name}}</option></select>";
                            $scope.dyn_Cont += "</div></div></div>";
                        } else {
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><div class=\"period\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,Isopened"+ $scope.atributesRelationList[i].AttributeID +")\"  datepicker-popup=\"{{format}}\"  is-open=\"Isopened"+ $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\">";
                            $scope.dyn_Cont += "</div></div>";
                            var param1 = new Date.create();
                            var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            $scope.setFieldKeys();
                            $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        try {
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                if ($scope.atributesRelationList[i].ParentValue == "-") {
                                    $scope.items.push({
                                        startDate: [],
                                        endDate: [],
                                        comment: '',
                                        sortorder: 0
                                    });
                                } else {
                                    if ($scope.atributesRelationList[i].ParentValue.length > 0) {
                                        for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                            var datStartUTCval = "";
                                            var datstartval = "";
                                            var datEndUTCval = "";
                                            var datendval = "";
                                            datStartUTCval = $scope.atributesRelationList[i].ParentValue[0][j].Startdate.substr(6, ($scope.atributesRelationList[i].ParentValue[0][j].Startdate.indexOf('+') - 6));
                                            datstartval = new Date.create(parseInt(datStartUTCval));
                                            datEndUTCval = $scope.atributesRelationList[i].ParentValue[0][j].EndDate.substr(6, ($scope.atributesRelationList[i].ParentValue[0][j].EndDate.indexOf('+') - 6));
                                            datendval = new Date.create(parseInt(datEndUTCval));
                                            $scope.items.push({
                                                startDate: ConvertStringToDate(ConvertDateToString(datstartval)),
                                                endDate: ConvertStringToDate(ConvertDateToString(datendval)),
                                                comment: $scope.atributesRelationList[i].ParentValue[0][j].Description,
                                                sortorder: 0
                                            });
                                        }
                                    } else {
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0
                                        });
                                    }
                                }
                            } else {
                                $scope.items.push({
                                    startDate: [],
                                    endDate: [],
                                    comment: '',
                                    sortorder: 0
                                });
                            }
                        } catch (e) { }
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-click=\"Calanderopen($event,'item.startDate'] )\"  datepicker-popup=\"{{format}}\"  is-open=\"item.startDate\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/>";
                        $scope.dyn_Cont += "<input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"Calanderopen($event,'item.endDate')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.endDate\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/>";
                        $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\" ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        StrartUpload_UploaderAttr();
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div class="control-group"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        $scope.dyn_Cont += '<div class="width2x inlineBlock va-middle" style="">';
                        if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                            $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                        $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a></div>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";
                        if ($scope.atributesRelationList[i].InheritFromParent) { } else { }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div>";
                        if ($scope.atributesRelationList[i].InheritFromParent) {
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Amount;
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0].Currencytypeid;
                        } else {
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        }
                        $scope.setFieldKeys();
                    }
                    $scope.setFieldKeys();
                    $scope.setoptions();
                }
                $scope.dyn_Cont += '<input style="visibility:hidden" type="submit" id="btnTempSub" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#SubPageMetadata").html('<div class="row-fluid"><div data-col="1" class="span4"></div><div data-col="2" class="span8"></div></div> ');
                $("#SubPageMetadata").html($compile($scope.dyn_Cont)($scope));
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', {
                        aSep: ' ',
                        vMin: "0",
                        mDec: "1"
                    });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";
                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: $scope.mindec
                        });
                    } else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "0"
                        });
                    }
                }
                $("#SubPageMetadata").scrollTop(0);
                refreshAssetobjects();
                $scope.$broadcast('callbackforEntitycreation', 0, CurrentEntityId, 4);
                GetValidationList(Id);
                $("#SubPageMetadata").addClass('notvalidate');
                UpdateInheritFromParentScopes();
                $timeout(function () {
                    HideAttributeToAttributeRelationsOnPageLoad();
                });
            });
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.treesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            if (treeobj.length > 0) {
                for (var i = 0, node; node = treeobj[i++];) {
                    if (node.ischecked == true) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == node.AttributeId && e.id == node.id;
                        });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(node);
                        }
                        treeformflag = false;
                        if (ischildSelected(node.Children)) {
                            GenerateTreeStructure(node.Children);
                        } else {
                            GenerateTreeStructure(node.Children);
                        }
                    } else GenerateTreeStructure(node.Children);
                }
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) { } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                return e.Id == $scope.atributesRelationList[i].ParentValue[0]
                            }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        } else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) {
                                        return e.Id == $scope.atributesRelationList[i].ParentValue[0][j]
                                    }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }
                                }
                            }
                        } else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = DefaultValue;
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                        return e.Caption == $scope.treeTexts[dropdown_text]
                                    }))[0];
                                }
                            } else {
                                if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                    return e.Caption == $scope.treeTexts[dropdown_text]
                                }))[0];
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) {
                                            return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                        }))[0];
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, j, levelCount, 12);
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        } else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                return e.Caption == $scope.multiselecttreeTexts[dropdown_text]
                                            }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    } else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = DateConvertForCurrentFormat($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
            }
        }

        function GetValidationList(EntitypeId) {
            SubpagecreationService.GetValidationDationByEntitytype(EntitypeId).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#SubPageMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTempSub',
                        'silentSubmit': 'true'
                    });
                    if ($scope.listValidationResult.length != 0) {
                        for (var i = 0 ; i < $scope.listValidationResult.length; i++) {
                            if ($scope.listValidationResult[i][1] == "presence") {
                                $($scope.listValidationResult[i][0]).parent().addClass('relative');
                                $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listValidationResult[i][0]);
                            }
                        }
                    }
                }
            });
        }
        $scope.entityObjectiveList = {};
        $scope.entityAllObjectives = {};
        $scope.MandatoryEntityObjectives = {};
        $scope.MandatoryObjList = [];
        $scope.NonMandatoryObjList = [];
        $scope.IDList = [];
        $scope.nonMandatoryIDList = [];
        $scope.IsObjectiveFired = false;

        function EntityMetadata() {
            $scope.AttributeData = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            var entityMetadataObj = {};
            entityMetadataObj.AttributeData = $scope.AttributeData;
            entityMetadataObj.Periods = [];
            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({
                            startDate: ConvertDateToString($scope.items[m].startDate),
                            endDate: ConvertDateToString($scope.items[m].endDate),
                            comment: '',
                            sortorder: 0
                        });
                    }
                }
            }
            entityMetadataObj.Periods.push($scope.subentityperiods);
            entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
            SubpagecreationService.GettingPredefineObjectivesForEntityMetadata().then(function (entityAttributeDataObjData) {
                $scope.IsObjectiveFired = true;
                if (entityAttributeDataObjData.Response != null && entityAttributeDataObjData.Response.length > 0) {
                    $scope.entityAllObjectives = entityAttributeDataObjData.Response;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0; i < $scope.entityAllObjectives.length; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        } else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                } else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            });
        }

        function FormObjectiveMetadata() {
            var AttributeData = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return AttributeData;
        }

        function GetObjectivePeriods() {
            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({
                            startDate: ConvertDateToString($scope.items[m].startDate),
                            endDate: ConvertDateToString($scope.items[m].endDate),
                            comment: '',
                            sortorder: 0
                        });
                    }
                }
            }
        }

        function UpdateObjectiveScope(objResponse) {
            if (objResponse != null) {
                if (objResponse != null && objResponse.length > 0) {
                    $scope.entityAllObjectives = objResponse;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0; i < $scope.entityAllObjectives.length; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        } else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                } else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            } else {
                $scope.IDList = [];
            }
        }
       
        $scope.Clear = function () { }

        $scope.EntityObjSelect = function (objectiveId) {
            var IsExist = $.grep($scope.nonMandatoryIDList, function (e) {
                if (e == objectiveId) {
                    return e;
                }
            })
            if (IsExist.length == 0) {
                $scope.nonMandatoryIDList.push(objectiveId);
            }
        }
        $(document).on('click', '.checkbox-custom > input[id=SelectAllObjectives]', function (e) {
            var status = this.checked;
            $('#nonMandatoryObjectiveBody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
            for (var i = 0; i < $scope.entityObjectiveList.length; i++) {
                $scope.nonMandatoryIDList.push($scope.entityObjectiveList[i].ObjectiveId);
            }
            for (var i = 0; i < $scope.MandatoryEntityObjectives.length; i++) {
                $scope.IDList.push($scope.MandatoryEntityObjectives[i].ObjectiveId);
            }
        });

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#SubPageMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            } else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };
        $scope.CheckPreviousEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            } else {
                $("#SubPageMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null
                } else {
                    $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };
        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            if (enddate1 != null && startdate >= enddate1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                $scope.items[currentindex].endDate = null;
            }
        };

        function refreshAssetobjects() {
            $scope.PageNoobj = {
                pageno: 1
            };
            $scope.TaskStatusObj = [{
                "Name": "Thumbnail",
                ID: 1
            }, {
                "Name": "Summary",
                ID: 2
            }, {
                "Name": "List",
                ID: 3
            }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{
                "Name": "Name (Asc)",
                ID: 1
            }, {
                "Name": "Name (Desc)",
                ID: 2
            }, {
                "Name": "Date (Asc)",
                ID: 3
            }, {
                "Name": "Date (Desc)",
                ID: 4
            }];
            $scope.FilterStatus = {
                filterstatus: 1
            };
            $scope.DamViewName = {
                viewName: "Thumbnail"
            };
            $scope.OrderbyName = {
                orderbyname: "Date (Desc)"
            };
            $scope.OrderBy = {
                order: 4
            };
            $scope.PageAsset = {
                CmsAssetSelectionFiles: []
            };
        }
        refreshAssetobjects();
        SubpagecreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            SubpagecreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                }
            });
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };
        setTimeout(function () { SubEntityCreation(params.ID,params.Caption, params.EntityID); },100);
        $scope.closeSubPageCreationPopupClose = function () {
            $modalInstance.dismiss('cancel');
        }

        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesSubCMSUploaderAttr").click();
        }
        $scope.attributeGroupDetailAsTab = [];
        $scope.attributeGroupAddedValues = {};

        function StrartUpload_UploaderAttr() {
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                $('.moxie-shim').remove();
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesSubCMSUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_Attr.bind('Init', function (up, params) { });
                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesSubCMSUploaderAttr',
                    container: 'filescontaineroo',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }
        var uploaderAttrInfo = [];
        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + extension;
            uploaderAttrInfo.push({ attrID: $scope.UploadAttributeId, attrFileName: ImageFileName });
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#overviewUplaodImagediv').modal('hide');
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + ImageFileName);
        }
    }
    app.controller('mui.cms.SubPageCreationCtrl', ['$scope', '$resource', '$timeout', '$location', '$cookies', '$window', '$compile', '$translate', '$modalInstance', 'params', 'SubpagecreationService', muiplanningtoolSubPageCreationCtrl]);
})(angular, app);