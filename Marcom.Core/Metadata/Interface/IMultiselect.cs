using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IMultiSelect interface for table 'MM_MultiSelect'.
    /// </summary>
    public interface IMultiSelect
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        
        int Entityid
        {
            get;
            set;

        }

        int Attributeid
        {
            get;
            set;

        }
        int Optionid
        {
            get;
            set;

        }
      
        

        #endregion
    }
}
