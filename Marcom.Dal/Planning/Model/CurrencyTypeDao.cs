﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// CurrencyTypeDao interface for table 'PM_CurrencyType'.
    /// </summary>

    public partial class CurrencyTypeDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _shortname;
        protected string _symbol;
        protected int _staticcurrency;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public CurrencyTypeDao() { }

        public CurrencyTypeDao(int pId, string pName, string pShortName, string pSymbol, int pStaticCurrency)
        {
            this._id = pId;
            this._name = pName;
            this._shortname = pShortName;
            this._symbol = pSymbol;
            this._staticcurrency = pStaticCurrency;
        }

        public CurrencyTypeDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual string ShortName
        {
            get { return _shortname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("ShortName", "ShortName value, cannot contain more than 50 characters");
                _bIsChanged |= (_shortname != value);
                _shortname = value;
            }

        }

        public virtual string Symbol
        {
            get { return _symbol; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Symbol", "Symbol value, cannot contain more than 50 characters");
                _bIsChanged |= (_symbol != value);
                _symbol = value;
            }

        }
        public virtual int StaticCurrency
        {
            get { return _staticcurrency; }
            set { _bIsChanged |= (_staticcurrency != value); _staticcurrency = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CurrencyTypeDao = "CurrencyTypeDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string ShortName = "ShortName";
            public const string Symbol = "Symbol";
            public const string StaticCurrency = "StaticCurrency";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CurrencyTypeDao = "CurrencyTypeDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string ShortName = "ShortName";
            public const string Symbol = "Symbol";
            public const string StaticCurrency = "StaticCurrency";
        }

        #endregion


    }

}
