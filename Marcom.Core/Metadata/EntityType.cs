/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// EntityType object for table 'MM_EntityType'.
	/// </summary>
	
	internal class EntityType : IEntityType 
	{
		#region Member Variables

		protected int _id; 
		protected string _caption;
		protected string _description;
		protected int _moduleid;
        protected string _modulecaption;
		protected bool _issystemdefined;
        protected int _category;
		protected int _group;
		protected int? _attributesetid;
        //protected int? _parententitytypeid;
        protected string _shortdescription;
        protected string _colorcode;
        protected bool _isassociate;
        protected int? _workFlowID;
        protected bool _isrootlevel;
        protected bool _entityaccessisenabled;
		
		#endregion
		
		#region Constructors
		public EntityType() {}

        public EntityType(string pCaption, string pDescription, int pModuleid, bool pIsSystemDefined, int pCategory, int pGroup, int? pAttributeSetid, string pShortDescription, string pColorCode, bool pIsAssociate, int pWorkFlowID,bool pIsRootLevel, bool pEntityTypeAccessIsEnabled)
		{
			this._caption = pCaption; 
			this._description = pDescription; 
			this._moduleid = pModuleid; 
			this._issystemdefined = pIsSystemDefined; 
            this._category = pCategory; 
			this._group = pGroup; 
			this._attributesetid = pAttributeSetid;
           // this._parententitytypeid = pParentEntityTypeId;
            this._shortdescription = pShortDescription;
            this._colorcode = pColorCode;
            this._isassociate = pIsAssociate;
            this._workFlowID = pWorkFlowID;
            this._isrootlevel = pIsRootLevel;
            this._entityaccessisenabled = pEntityTypeAccessIsEnabled;
		}
				
		public EntityType(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}
		
		public int ModuleID
		{
			get { return _moduleid; }
			set { _moduleid = value; }
			
		}

        public string ModuleCaption
        {
            get { return _modulecaption; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _modulecaption = value;
            }

        }
		
        //public bool IsSystemDefined
        //{
        //    get { return _issystemdefined; }
        //    set { _issystemdefined = value; }
			
        //}

        public int Category
        {
            get { return _group; }
            set { _group = value; }
			
		}
		
		public int Group
		{
			get { return _group; }
			set { _group = value; }
			
		}
		
		public int? AttributeSetid
		{
			get { return _attributesetid; }
			set { _attributesetid = value; }
			
		}

        //public int? ParentEntityTypeId
        //{
        //    get { return _parententitytypeid; }
        //    set { _parententitytypeid = value; }

        //}

        public string ShortDescription
        {
            get { return _shortdescription; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("ShortDescription", "ShortDescription value, cannot contain more than 50 characters");
                _shortdescription = value;
            }

        }

        public string ColorCode
        {
            get { return _colorcode; }
            set { _colorcode = value; }
        }

        public virtual bool IsAssociate
        {
            get { return _isassociate; }
			set { _isassociate = value; }       

        }

        public int? WorkFlowID
        {
            get { return _workFlowID; }
            set { _workFlowID = value; }

        }

        public virtual bool IsRootLevel
        {
            get { return _isrootlevel; }
            set { _isrootlevel = value; }
        }

        public virtual bool EntityTypeAccessIsEnabled
        {
            get { return _entityaccessisenabled; }
            set { _entityaccessisenabled = value; }
        }

        public string WorkFlowName { get; set; }

        public int ParentTypeId { get; set; }

        public int TaskTypeId { get; set; }

        public int ObjectiveBaseType { get; set; }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			EntityType castObj = null;
			try
			{
				castObj = (EntityType)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
