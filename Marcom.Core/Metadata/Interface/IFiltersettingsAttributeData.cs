﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
   public interface IFiltersettingsAttributes
    {
        #region Public Properties
        int EntityTypeId { get; set; }
        int AttributeId {get; set;}
        string DisplayName{get; set;}
        int AttributeTypeId{get; set;}
        int TreeLevel { get; set; }
        bool IsSpecial { get; set; }
        IList<IOption> OptionValues { get; set; }
        IList<User.Interface.IUser> Users { get; set; }
        IList<User.Interface.IUser> Member { get; set; }
        int MemberId { get; set; }
        IList<ITreeNode> LevelTreeNodes { get; set; }
        IList<IEntityTypeStatusOptionsForFilter> EntityStatusOptionValues { get; set; }
        IList<DropDownTreePricing> DropdowntreePricingAttr { get; set; }
        string tree { get; set; }

        //IList<IEntityTypeStatusOptions> EntityStatusOptionValues { get; set; }
        #endregion
    }

   
}
