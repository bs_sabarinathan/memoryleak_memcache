﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// AdditionalObjectiveEntityValues object for table 'PM_AdditionalObjectiveEntityValues'.
    /// </summary>
    internal class AdditionalObjectiveEntityValues : IAdditionalObjectiveEntityValues
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _typeid;

        protected string _instruction;
        protected bool _isenablefeedback;
        protected int _unitid;
        protected decimal _plannedTarget;
        protected decimal _targetoutcome;
        protected int _ratingobjective;
        protected string _comments;
        protected int _fulfillment;
        protected bool _isActive;

        #endregion

        #region Constructors
        public AdditionalObjectiveEntityValues() { }

        public AdditionalObjectiveEntityValues(int pEntityid, int pTypeId, string pInstruction, bool pIsEnableFeedback, int pUnitID, decimal pPlannedTarget, decimal pTargetOutcome, int pRatingObjective, string pComments, int pFulfillment, bool pIsAcive)
        {
            this._entityid = pEntityid;
            this._typeid = pTypeId;
            this._instruction = pInstruction;
            this._isenablefeedback = pIsEnableFeedback;
            this._unitid = pUnitID;
            this._plannedTarget = pPlannedTarget;
            this._targetoutcome = pTargetOutcome;
            this._ratingobjective = pRatingObjective;
            this._comments = pComments;
            this._unitid = pUnitID;
            this._plannedTarget = pPlannedTarget;
            this._ratingobjective = pRatingObjective;
            this._comments = pComments;
            this._fulfillment = pFulfillment;
            this._isActive = pIsAcive;
        }

        public AdditionalObjectiveEntityValues(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }

        }
        public int TypeId
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public string Instruction
        {
            get { return _instruction; }
            set
            {
                _instruction = value;
            }

        }

        public bool IsEnableFeedback
        {
            get { return _isenablefeedback; }
            set
            {
                _isenablefeedback = value;
            }
        }

        public int UnitID
        {
            get { return _unitid; }
            set { _unitid = value; }

        }

        public decimal PlannedTarget
        {
            get { return _plannedTarget; }
            set { _plannedTarget = value; }

        }

        public virtual decimal TargetOutcome
        {
            get { return _targetoutcome; }
            set { _targetoutcome = value; }

        }

        public virtual int RatingObjective
        {
            get { return _ratingobjective; }
            set { _ratingobjective = value; }

        }
        public virtual string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public virtual int Fulfillment
        {
            get { return _fulfillment; }
            set { Fulfillment = value; }
        }
        public virtual bool IsActive
            {
            get { return _isActive; }
            set { _isActive = value; }

            }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AdditionalObjectiveEntityValues castObj = null;
            try
            {
                castObj = (AdditionalObjectiveEntityValues)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


    }
}
