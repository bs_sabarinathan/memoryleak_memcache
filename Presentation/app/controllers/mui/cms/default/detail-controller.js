﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope, $location, CmsService, $timeout, $compile, $sce, $stateParams, $resource, $translate, DetailService, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        setTimeout(function () { $scope.page_resize(); }, 100);
        $scope.ISFullPages = false;
        $scope.IsEditDeleteEnabled = 0;
        //$scope.$on("requestContextChanged", function () {
        //    $scope.ISFullPages = false;
        //    if (!renderContext.isChangeRelevant()) {
        //        $scope.subview = renderContext.getNextSection();
        //        if ($scope.ISFullPages == false) {
        //            if ($scope.subview == "list") {
        //                $location.path('/mui/cms/detail/list/' + $stateParams.ID + '/entitycontent');
        //                GetCmsRevisedContent($stateParams.ID);
        //            } else if ($scope.subview == "section") {
        //                $location.path('/mui/cms/detail/section/' + $stateParams.ID + '/content');
        //                $scope.ID = $stateParams.ID;
        //            }
        //        }
        //        return;
        //    }
        //    $scope.subview = renderContext.getNextSection();
        //    if ($scope.subview == null && $scope.ISFullPages == false) {
        //        GetAllCmsEntitiesByNavID();
        //    }
        //});
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detailCtrl']"));
        });
        var cleanUpFunc = $scope.$on('navigationClick', function (event, FeatureID) {
            cleanUpFunc();
            if (FeatureID == 34) {
                $('#CmsTreeCtrlCss').remove();
                $scope.ShowAddPage = false;
                $scope.ParentID = 0;
                $scope.UniqueKey = "";
                $scope.Level = 0;
                $scope.ID = 0;
                $scope.Index = -1;
                $scope.CmsItemCollections = {};
                GetAllCmsEntitiesByNavID();
            }
        });
        $scope.ShowEntityTemplate = false;
        $scope.ShowEntityContent = false;
        $scope.IsEditFeatureEnabled = false;
        $scope.AttributeGroupList = [];
        $('#CmsTreeCtrlCss').remove();
        $scope.ShowAddPage = false;
        $scope.ParentID = 0;
        $scope.UniqueKey = "";
        $scope.Level = 0;
        $scope.ID = 0;
        $scope.Index = -1;
        $scope.PageEntityType = [];
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        DetailService.GetCmsTreeStyle().then(function (resstyle) {
            if (resstyle.Response != "") {
                var valstyle = JSON.parse(resstyle.Response);
                var borderLeftColor = "rgba(" + hex2rgb(valstyle[3].leftborderhighlightcolor) + ", 0.5)";
                var hoverGrdLight = "rgba(" + hex2rgb(valstyle[5].mouseoverbackgroundcolor) + ", 0.1)";
                var hoverGrdDark = "rgba(" + hex2rgb(valstyle[5].mouseoverbackgroundcolor) + ", 0.5)";
                var hoverGradiant = "background-color: #FFFFFF; " + "background-image: -moz-linear-gradient(top, " + hoverGrdLight + ", " + hoverGrdDark + "); " + "background-image: -webkit-gradient(linear, 0 0, 0 100%, from(" + hoverGrdLight + "), to(" + hoverGrdDark + ")); " + "background-image: -webkit-linear-gradient(top, " + hoverGrdLight + ", " + hoverGrdDark + "); " + "background-image: -o-linear-gradient(top, " + hoverGrdLight + ", " + hoverGrdDark + "); " + "background-image: linear-gradient(to bottom, " + hoverGrdLight + ", " + hoverGrdDark + "); " + "background-repeat: repeat-x; " + "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff7fbfc', endColorstr='#ffecf4f9', GradientType=0);";
                $("<style>#cmsDetailsContainer</style>").html("");
                $("<style>#cmsDetailsContainer #cmsContent #left, #cmsDetailsContainer #cmsContent #left .sidebar-nav, #cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder {background-color : #" + valstyle[0].backgroundcolor + " !important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left,#cmsDetailsContainer #cmsContent #left .sidebar-nav,#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder,#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li>a,#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li>a span{color:# " + valstyle[1].foregroundcolor + "!important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li>a{border-bottom-color:# " + valstyle[2].bordercolor + "!important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li>a{border-left-color:" + borderLeftColor + "!important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li>a:hover{border-left-color:# " + valstyle[3].leftborderhighlightcolor + "!important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li.selected a,#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li.active a{border-bottom-color:# " + valstyle[4].selectionbackgroundcolor + "!important;}</style>").appendTo("head");
                $("<style>#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder .nav-list>li a:hover{" + hoverGradiant + "}</style>").appendTo("head");
            }
            $("<style>#cmsDetailsContainer #cmsContent #left,#cmsDetailsContainer #cmsContent #left .sidebar-nav,#cmsDetailsContainer #cmsContent #left ul.cmsTreeHolder{font-family:proxima-nova!important;font-weight:400;}</style>").appendTo("head");
        });

        function hex2rgb(hex) {
            var R = parseInt(hex.substring(0, 2), 16);
            var G = parseInt(hex.substring(2, 4), 16);
            var B = parseInt(hex.substring(4, 6), 16);
            var result = R + ", " + G + ", " + B;
            return result;
        }
        DetailService.GetEntityType(6).then(function (entitytypes) {
            $scope.PageEntityType = entitytypes.Response;
        });
        CmsService.GetIsEditFeatureEnabled().then(function (data) {
            if (data.Response != null && data.StatusCode == 200) {
                $scope.IsEditFeatureEnabled = data.Response;
            }
        });
        $scope.OpenCmsEntityCreationPopUp = function () {
            $scope.ParentID = 0;
            $scope.UniqueKey = "";
            $scope.Level = 0;
            $scope.ID = 0;
            $scope.Index = -1;
            $("#cmsEntityModel").trigger("onCMSEntityCreation")
        }
        $scope.OpenCmsEntityContent = function () {
            $location.path('mui/cms/detail/section/' + $scope.ID + '/content');
        }
        $scope.RootCmsScopeRefresh = function (pID, CmsEntityDesc, CmsEntityName, SelectedDate, CmsTimeZone, CmsTags, UniqueKey) {
            $timeout(function () {
                $scope.CmsItemCollections = [{
                    ID: pID,
                    ParentID: 0,
                    Active: true,
                    Version: 0,
                    Description: CmsEntityDesc,
                    Level: 0,
                    Name: CmsEntityName,
                    NavID: $scope.GlobalNavigationID.NavigationID,
                    PublishedDate: ConvertDateToString(SelectedDate),
                    PublishedTime: CmsTimeZone,
                    PublishedStatus: true,
                    TemplateID: 1,
                    UniqueKey: UniqueKey,
                    IsChildrenPresent: false,
                    Tag: CmsTags
                }];
                $scope.ShowAddPage = false;
                $location.path('/mui/cms/detail/list/' + pID + '/entitycontent');
                GetCmsRevisedContent(pID);
                $("#cmsentity_" + pID).addClass("active");
            }, 20);
        }
        $scope.DeleteCmsEntity = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                if (result) {
                    if ($scope.ID != 0) {
                        CmsService.DeleteCmsEntity($scope.ID).then(function (data) {
                            if (data.Response != false) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.CmsItemCollections.splice($scope.Index, 1);
                                $scope.Index = -1;
                                if ($scope.CmsItemCollections.length > 0) {
                                    $stateParams.ID = $scope.CmsItemCollections[0].ID;
                                    $location.path('/mui/cms/detail/list/' + $scope.CmsItemCollections[0].ID + '/entitycontent');
                                    GetCmsRevisedContent($scope.CmsItemCollections[0].ID);
                                } else $location.path('/mui/cms/detail');
                            }
                        });
                    }
                }
            });
        }
        $scope.CmsItemCollections = {};
        GetAllCmsEntitiesByNavID();

        function GetAllCmsEntitiesByNavID() {
            $scope.CmsItemCollections = {};
            CmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, 0, 1000).then(function (data) {
                if (data.Response != null && data.Response.length > 0) {
                    if ($.grep(data.Response, function (e) {
						return e.Permission < 4
                    }).length > 0) {
                        $scope.CmsItemCollections = $.grep(data.Response, function (e) {
                            return e.Permission < 4
                        });
                        if ($stateParams.ID == undefined) {
                            $stateParams.ID = $scope.CmsItemCollections[0].ID;
                        }
                        if ($scope.IsNavFromAssetToPage.IsFromAsset == false) {
                            if ($scope.IsfromassetlibrarytoCMS.IstoCMSattachments == false) $location.path('/mui/cms/detail/list/' + $stateParams.ID + '/entitycontent');
                        }
                        $scope.IsNavFromAssetToPage.IsFromAsset = false;
                        $scope.IsfromassetlibrarytoCMS.IstoCMSattachments = false;
                        GetCmsRevisedContent($stateParams.ID);
                        $("#cmsentity_" + $stateParams.ID).addClass("active");
                    }
                } else {
                    $timeout(function () {
                        if ($scope.IsEditFeatureEnabled) $scope.ShowAddPage = true;
                    }, 100);
                }
            });
        }
        function dynamiccontrol() {
            $("#dynamicControls").modal('show');
        }
        function GetAllCmsEntitiesLoadPageByNavID(StartPageNo, MaxPageNo) {
            CmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, StartPageNo, MaxPageNo).then(function (data) {
                if (data.Response != null && data.Response.length > 0 && $.grep(data.Response, function (e) {
					return e.Permission < 4
                }).length > 0) {
                    $scope.CmsItemCollections.push($.grep(data.Response, function (e) {
                        return e.Permission < 4
                    }));
                }
            });
        }
        $("#CmsListHolder").click(function (event) {
            if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
            var TargetControl = $(event.target);
            if (TargetControl[0].tagName == "I" && TargetControl.attr("data-role") == "menureorder") {
                $scope.ParentID = TargetControl.attr("data-parentid");
                $scope.UniqueKey = TargetControl.attr("data-uniquekey");
                $scope.Level = TargetControl.attr("data-level");
                event.preventDefault();
            } else if (TargetControl[0].tagName == "A" && TargetControl.attr("data-role") == "anchor") {
                var localScope = $(event.target).scope();
                event.preventDefault();
                localScope.$apply(function () {
                    $location.path('/mui/cms/detail/list/' + TargetControl.attr("data-id") + '/entitycontent');
                    $("#CmsListHolder li.active").removeClass("active");
                    $("#cmsentity_" + TargetControl.attr("data-id")).addClass("active");
                    $scope.ShowEntityTemplate = false;
                    $scope.ShowEntityContent = false;
                    GetCmsRevisedContent(TargetControl.attr("data-id"));
                });
            } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr("data-role") == "text") {
                var localScope = $(event.target).scope();
                event.preventDefault();
                localScope.$apply(function () {
                    $location.path('/mui/cms/detail/list/' + TargetControl.attr("data-id") + '/entitycontent');
                    $("#CmsListHolder li.active").removeClass("active");
                    $("#cmsentity_" + TargetControl.attr("data-id")).addClass("active");
                    $scope.ShowEntityTemplate = false;
                    $scope.ShowEntityContent = false;
                    GetCmsRevisedContent(TargetControl.attr("data-id"));
                });
            } else if (TargetControl[0].tagName == "I" && TargetControl.attr("data-role") == "arrow") {
                if (TargetControl.hasClass("icon-caret-down")) {
                    TargetControl.removeClass("icon-caret-down").addClass("icon-caret-right");
                    Collapse(TargetControl.attr("data-UniqueID"));
                } else {
                    TargetControl.removeClass("icon-caret-right").addClass("icon-caret-down");
                    Expand(TargetControl.attr("data-UniqueID"));
                }
                event.preventDefault();
            } else {
                event.preventDefault();
            }
        });

        function OpenAssetLibraryPopUp(AssetID) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: AssetID,
                            IsLock: true,
                            isNotify: true,
                            viewtype: 'ViewType'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        function GetCmsRevisedContent(ID) {
            if (ID != undefined && ID != "undefined") {
                CmsService.GetRevisedContentByFeature(ID).then(function (data) {
                    if (data.Response != null) {
                        var temphtml = $("<div/>").html($.parseHTML(data.Response.Content, document, true));
                        if (temphtml.find("#cmsLinksContainer").find('a').length == 0) {
                            temphtml.find("#cmsLinksContainer").remove();
                        } else {
                            temphtml.find("#cmsLinksContainer div").first().find('a').each(function () {
                                if ($(this).attr("target") == undefined) {
                                    $(this).attr('target', '_blank');
                                }
                            });
                            temphtml.find("#cmsLinksContainer").children().eq(1).find('a').each(function () {
                                if ($(this).attr("href") != "") {
                                    $(this).attr("data-toggle", "modal");
                                    $(this).attr("data-target", "#AssetEditpopup");
                                    $(this).removeAttr("href", "");
                                    $(this).click(function (event) {
                                        var targetCtrl = $(event.target);
                                        if (targetCtrl.attr("data-assetId") != undefined) {
                                            var modalInstance = $modal.open({
                                                templateUrl: 'views/mui/DAM/assetedit.html',
                                                controller: "mui.DAM.asseteditCtrl",
                                                resolve: {
                                                    params: function () {
                                                        return {
                                                            AssetID: targetCtrl.attr("data-assetId"),
                                                            IsLock: true,
                                                            isNotify: true,
                                                            viewtype: 'ViewType'
                                                        };
                                                    }
                                                },
                                                scope: $scope,
                                                windowClass: 'iv-Popup',
                                                backdrop: "static"
                                            });
                                            modalInstance.result.then(function (selectedItem) {
                                                $scope.selected = selectedItem;
                                            }, function () { });
                                        }
                                    });
                                }
                            });
                        }
                        $('#htmlEntityContent').html(temphtml);
                        $timeout(function () {
                            FindAttributeGroupInContentPage();
                            $('.btnbrowse').css("display", 'none');
                        }, 200);
                    }
                });
            }
        }
        function FindAttributeGroupInContentPage() {
            var attrgrpids = [];
            $('div[data-attributegroupid]').each(function () {
                if ($.inArray(attrgrpids, $(this).attr('data-attributegroupid')) == -1) attrgrpids.push($(this).attr('data-attributegroupid'));
                $(this).html('');
            });
            if (attrgrpids.length > 0) {
                DetailService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), 0).then(function (attributeGrpRelation) {
                    if (attributeGrpRelation.Response != null && attributeGrpRelation.Response != '') {
                        $scope.AttributeGroupList.length = 0;
                        for (var i = 0; i < attributeGrpRelation.Response.length; i++) {
                            $scope.AttributeGroupList.push({
                                AttributeGroupId: attributeGrpRelation.Response[i].AttributeGroupID,
                                Name: attributeGrpRelation.Response[i].Caption
                            });
                        }
                        for (var l = 0; l < attrgrpids.length; l++) {
                            BindAttributeGroupToContentPage(attrgrpids[l]);
                        }
                    }
                })
            }
        }

        function BindAttributeGroupToContentPage(AttrGrpId) {
            DetailService.GetEntityAttributesGroupValues(parseInt($stateParams.ID), 0, AttrGrpId, false).then(function (entitygroupvalues) {
                var AttrGrpName = $.grep($scope.AttributeGroupList, function (e) {
                    return e.AttributeGroupId == AttrGrpId;
                })[0];
                var attrBody = '<div class="attGrpOvrContent column full"><div class="attG-wrapper">';
                attrBody += '<h1 class="header">' + AttrGrpName.Name + '</h1>';
                attrBody += '<form class="form-horizontal">';
                if (entitygroupvalues.Response == null || entitygroupvalues.Response.length == 0) {
                    attrBody += '<span>No item present</span>';
                } else {
                    $scope.AGRecordList = entitygroupvalues.Response;
                    for (var i = 0; i < $scope.AGRecordList.length; i++) {
                        for (var j = 0; j < $scope.AGRecordList[i].length; j++) {
                            if ($scope.AGRecordList[i][j].TypeID == 6 || $scope.AGRecordList[i][j].TypeID == 12) {
                                for (var k = 0; k < $scope.AGRecordList[i][j].Lable.length; k++) {
                                    attrBody += '<div class="control-group">';
                                    attrBody += '<label class="control-label">' + $scope.AGRecordList[i][j].Lable[k].Label + '</label>';
                                    attrBody += ' <div class="controls">';
                                    attrBody += '<span>' + $scope.AGRecordList[i][j].Value[k] + '</span>';
                                    attrBody += '</div></div>';
                                }
                            } else {
                                attrBody += '<div class="control-group">';
                                attrBody += '<label class="control-label">' + $scope.AGRecordList[i][j].Lable + '</label>';
                                attrBody += '<div class="controls">';
                                if ($scope.AGRecordList[i][j].TypeID == 11){
                               // attrBody += '<img src="UploadedImages/' + $scope.AGRecordList[i][j].Value + '}}" class="widthauto heightauto entityDetailImgPreview">';
                                    if ($scope.AGRecordList[i].Value == "" || $scope.AGRecordList[i].Value == null && $scope.AGRecordList[i].Value == undefined) {
                                        $scope.AGRecordList[i].Value = "NoThumpnail.jpg";
                                }
                                    attrBody += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.AGRecordList[i].Value + '" alt="' + $scope.AGRecordList[i].Caption + '"';
                                    attrBody += 'class="widthauto heightauto entityDetailImgPreview">';
                            }    
                            else
                                    attrBody += '<span>' + $scope.AGRecordList[i][j].Caption + '</span>';
                                    attrBody += '</div></div>';
                            }
                        }
                    }
                }
                attrBody += '</form></div></div>';
                $('div[data-attributegroupid=' + AttrGrpId + ']').html(attrBody);
            });
        }
        $scope.ContextmenuClick = function (ID, ParentID, Uniquekey, level, Index, Permission) {
            $scope.ParentID = ID;
            $scope.UniqueKey = Uniquekey;
            $scope.Level = level;
            $scope.Index = Index;
            $scope.ID = ID;
            $scope.EntityParentID = ParentID;
            $scope.IsEditDeleteEnabled = Permission
            $scope.duplicateTimes = 1;
            $scope.IncludeSubItems = false;
            $("#duplicateCmsEnitynames").html("");
            var appendHtml = "<label class='control-label'>Page Name 1</label><div class='controls margin-bottom10x'><input id='duplicatCmsentityid0' type='text'/></div>";
            $("#duplicateCmsEnitynames").append(appendHtml);
        }
        $("#duplicateNoOfTimes").keypress(function (event) {
            var valid = (event.which >= 48 && event.which <= 57);
            if (!valid) {
                event.preventDefault();
                return false;
            }
        });
        $("#duplicateNoOfTimes").keyup(function (event) {
            var appendHtml = "";
            $("#duplicateCmsEnitynames").html("");
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                event.preventDefault();
                return false;
            }
            var cnt = 1;
            for (var i = 0; i < parseInt($scope.duplicateTimes) ; i++) {
                appendHtml = appendHtml + "<label class='control-label'>Page Name " + cnt + "</label><div class='controls margin-bottom10x'><input id='duplicatCmsentityid" + i + "' type='text'/></div>"
                cnt++;
            }
            $("#duplicateCmsEnitynames").append(appendHtml);
        });
        $scope.OpenSubCmsEntityCreationPopUp = function () {
            $("#cmsEntityModel").trigger("onSubCMSEntityCreation", [$scope.ParentID, $scope.UniqueKey, $scope.Level])
        }
        $scope.ADDSpace = function (Level) {
            if (Level != undefined) {
                return new Array(Level);
            }
        }
        $scope.AddClass = function (UniqueId) {
            UniqueId = UniqueKEY(UniqueId);
            var ClassName = GenateClass(UniqueId);
            ClassName += " mo" + UniqueId;
            return ClassName;
        }
        $scope.ADDUniqueID = function (UniKey) {
            return UniqueKEY(UniKey);
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = [];

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="CmsTreeCtrlCss">';
            $('#CmsTreeCtrlCss').remove();
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }
        $("#cmsContextmenu").click(function (event) {
            var targetCtrl = $(event.target);
            if (targetCtrl.attr('data-role') == 'RootPageCreation') $("#cmspageMetadataModel").trigger("onSubEntityCreation", [targetCtrl.attr("data-Id"), targetCtrl.attr("data-Caption"), 0]);
            else if (targetCtrl.attr('data-role') == 'SubPageCreation') $("#cmspageMetadataModel").trigger("onSubEntityCreation", [targetCtrl.attr("data-Id"), targetCtrl.attr("data-Caption"), $scope.ID]);
        });
        $("#CmsListHolder").on("ReloadPageTree", function (event, EntityID, UniqueKey) {
            $scope.CmsItemCollections = {};
            CmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, 0, 1000).then(function (data) {
                if (data.Response != null && data.Response.length > 0) {
                    if ($.grep(data.Response, function (e) {
						return e.Permission < 4
                    }).length > 0) {
                        $scope.CmsItemCollections = $.grep(data.Response, function (e) {
                            return e.Permission < 4
                        });
                        $stateParams.ID = EntityID;
                        $("#CmsListHolder li.active").removeClass("active");
                        $scope.ShowEntityTemplate = false;
                        $scope.ShowEntityContent = false;
                        $timeout(function () {
                            GetCmsRevisedContent(EntityID);
                            $("#cmsentity_" + EntityID).addClass("active");
                        }, 100);
                        $timeout(function () {
                            $location.path('/mui/cms/detail/list/' + $stateParams.ID + '/entitycontent');
                        }, 100);
                    }
                }
            });
        });
        $("#CmsListHolder").on("CMSPageTree", function (event, EntityID, UniqueKey) {
            $scope.CmsItemCollections = {};
            CmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, 0, 1000).then(function (data) {
                if (data.Response != null && data.Response.length > 0) {
                    if ($.grep(data.Response, function (e) {
						return e.Permission < 4
                    }).length > 0) {
                        $scope.CmsItemCollections = $.grep(data.Response, function (e) {
                            return e.Permission < 4
                        });
                        $stateParams.ID = EntityID;
                        $("#CmsListHolder li.active").removeClass("active");
                        $scope.ShowEntityTemplate = false;
                        $scope.ShowEntityContent = false;
                        $scope.ShowAddPage = false;
                        $timeout(function () {
                            GetAllCmsEntitiesByNavID();
                        }, 200);
                    }
                }
            });
        });
        $scope.DuplicateEntityPageType = "";
        $scope.EntitytoDuplicate = [];
        $scope.EntityleveltoDuplicate = 0;
        $scope.ClearDuplicateVar = function () {
            $scope.varDuplicate.IsEntity = false;
            $scope.varDuplicate.IsEntityStatus = false;
            $scope.varDuplicate.IsAttachments = false;
            $scope.varDuplicate.IsMember = false;
            $scope.varDuplicate.IsMilestone = false;
            $scope.varDuplicate.IsTasks = false;
            $scope.varDuplicate.IsTaskCheckList = false;
            $scope.varDuplicate.IsTaskAttachment = false;
            $scope.varDuplicate.IsCostcentre = false;
            $scope.varDuplicate.IsPlannedBudget = false;
            $("#duplicatePageEnitynames").html("");
        }
        $scope.ContextEntityDuplicate = function () {
            var entityId = [];
            entityId.push($scope.ID);
            $scope.DuplicatePageEntity(entityId, 0, "DETAIL");
        }
        $scope.DuplicatePageEntity = function (duplicatedata, entitylevel, PageName) {
            $scope.DuplicateEntityPageType = PageName;
            $("#duplicatePageEnitynames").html("");
            $("#noofpagetimes").show();
            $scope.EntitytoDuplicate = duplicatedata;
            $scope.EntityleveltoDuplicate = entitylevel;
            $scope.duplicateTimes = 1;
            $scope.DiscardChildDuplicate = false;
            $scope.varDuplicate = {
                IsEntity: false,
                IsEntityStatus: false,
                IsAttachments: false,
                IsMember: false,
                IsMilestone: false,
                IsTasks: false,
                IsTaskCheckList: false,
                IsTaskAttachment: false,
                IsCostcentre: false,
                IsPlannedBudget: false
            }
            var appendHtml = "<label class='control-label'>Page Name 1</label><div class='controls margin-bottom10x'><input id='duplicatentityid0' type='text'/></div>";
            $("#duplicatePageEnitynames").append(appendHtml);
            $("#duplicatPageeentity").show();
            $("#duplicatepagetask").hide();
            $("#duplicatecostcentre").hide();
        }
        $scope.CreateDuplicateEntity = function () {
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4591.Caption'));
                return false;
            }
            if ($scope.varDuplicate.IsEntity == false && $scope.varDuplicate.IsTasks == false && $scope.varDuplicate.IsCostcentre == false) {
                bootbox.alert($translate.instant('LanguageContents.Res_4606.Caption'));
                return false;
            }
            var tempentitynames = [];
            var stas = false;
            $('#duplicatePageEnitynames div').each(function (index) {
                if ($('#duplicatentityid' + index).val() == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_4592.Caption'));
                    stas = true;
                    return false;
                }
                tempentitynames.push($('#duplicatentityid' + index).val());
            });
            if (stas == true) return false;
            $("#showhidepageprogress").show();
            $('#duplicatPageeentity').attr('disabled', true);
            var duplicate = {};
            duplicate.EntityID = $scope.EntitytoDuplicate;
            duplicate.ParentLevel = $scope.EntityleveltoDuplicate;
            duplicate.duplicateEntityNames = tempentitynames;
            duplicate.DuplicateList = $scope.varDuplicate;
            if ($scope.duplicateTimes == undefined || $scope.duplicateTimes == "") duplicate.DuplicateTimes = 1;
            else duplicate.DuplicateTimes = $scope.duplicateTimes;
            duplicate.IsDuplicateChild = $scope.DiscardChildDuplicate;
            DetailService.DuplicateEntities(duplicate).then(function (SaveDuplicate) {
                if (SaveDuplicate.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4321.Caption'));
                } else {
                    $scope.duplicateTimes = "";
                    $scope.DiscardChildDuplicate = false;
                    $scope.ClearDuplicateVar();
                    var duplicateLists = SaveDuplicate.Response;
                    $scope.CmsItemCollections = {};
                    CmsService.GetAllCmsEntitiesByNavID($scope.GlobalNavigationID.NavigationID, 0, 1000).then(function (data) {
                        if (data.Response != null && data.Response.length > 0 && $.grep(data.Response, function (e) {
							return e.Permission < 4
                        }).length > 0) {
                            $scope.CmsItemCollections = $.grep(data.Response, function (e) {
                                return e.Permission < 4
                            });
                            $location.path('/mui/cms/detail/list/' + SaveDuplicate.Response[0] + '/entitycontent');
                            $("#CmsListHolder li.active").removeClass("active");
                            $scope.ShowEntityTemplate = false;
                            $scope.ShowEntityContent = false;
                            $timeout(function () {
                                GetCmsRevisedContent(SaveDuplicate.Response[0]);
                                $("#cmsentity_" + SaveDuplicate.Response[0]).addClass("active");
                            }, 100);
                        }
                    });
                    $timeout(function () {
                        $("#showhidepageprogress").hide();
                        $('#duplicatPageeentity').attr('disabled', false);
                        $('#duplicatePageModel').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4226.Caption'));
                    }, 1000);
                }
            });
        }
        $("#duplicateTimes").keypress(function (event) {
            var valid = (event.which >= 48 && event.which <= 57);
            if (!valid) {
                event.preventDefault();
                return false;
            }
        });
        $("#duplicateTimes").keyup(function (event) {
            var appendHtml = "";
            $("#duplicatePageEnitynames").html("");
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                event.preventDefault();
                return false;
            }
            var cnt = 1;
            for (var i = 0; i < parseInt($scope.duplicateTimes) ; i++) {
                appendHtml = appendHtml + "<label class='control-label'>Page Name " + cnt + "</label><div class='controls margin-bottom10x'><input id='duplicatentityid" + i + "' type='text'/></div>"
                cnt++;
            }
            $("#duplicatePageEnitynames").append(appendHtml);
        });
        $("#entitytype").click(function (e) {
            $("#duplicatPageeentity").show();
            $("#duplicatepagetask").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#tasktype").click(function (e) {
            $("#duplicatepagetask").show();
            $("#duplicatPageeentity").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#costcentretype").click(function (e) {
            $("#duplicatecostcentre").show();
            $("#duplicatepagetask").hide();
            $("#duplicatPageeentity").hide();
        });
        $("#chkentitytype").click(function (e) {
            if ($scope.varDuplicate.IsEntity == false) $("#entitytype").addClass("color-primary");
            else {
                $("#entitytype").removeClass("color-primary");
                $scope.varDuplicate.IsEntityStatus = false;
                $scope.varDuplicate.IsMilestone = false;
                $scope.varDuplicate.IsAttachments = false;
                $scope.DiscardChildDuplicate = false;
                $scope.varDuplicate.IsMember = false;
            }
        });
        $("#chktasktype").click(function (e) {
            if ($scope.varDuplicate.IsTasks == false) $("#tasktype").addClass("color-primary");
            else {
                $("#tasktype").removeClass("color-primary");
                $scope.varDuplicate.IsTaskAttachment = false;
                $scope.varDuplicate.IsTaskCheckList = false;
            }
        });
        $("#chkcostcentretype").click(function (e) {
            if ($scope.varDuplicate.IsCostcentre == false) $("#costcentretype").addClass("color-primary");
            else {
                $("#costcentretype").removeClass("color-primary");
                $scope.varDuplicate.IsPlannedBudget = false;
            }
        });
        $scope.$on('callBackAssetEditinMUI', function (event, ID, isLock, isNotify, viewtype) {
            $scope.Islock_temp = isLock;
            $timeout(function () {
                $scope.$broadcast('openassetpopup', ID, isLock, isNotify, viewtype);
            }, 100);
        });
        $scope.optmakerObj = {
            EntityID: 0,
            FolderID: 0,
            AssetTypeID: 0,
            CreatedByID: 0,
            IsWord: false
        };
        $scope.SetOptmakerObj = function (eid, fid, atype, uid, isWord) {
            $scope.optmakerObj.EntityID = eid;
            $scope.optmakerObj.FolderID = fid;
            $scope.optmakerObj.AssetTypeID = atype;
            $scope.optmakerObj.CreatedByID = uid;
            $scope.optmakerObj.IsWord = isWord;
        }
        $scope.getOptmakerObj = function () {
            return $scope.optmakerObj;
        }
        $scope.$on('CallBackThumbnailAssetViewfrmMUI', function (event, ID) {
            $scope.$broadcast('CallBackThumbnailAssetView', ID);
        });
        $(window).AdjustHeightWidth();
        $scope.rootValues = [];
        DetailService.GetEntityType(6).then(function (entitytypes) {
            $scope.rootValues = entitytypes.Response;
        });
        $scope.DetailOpenPopupForPageCreation = function (ID,Caption) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/cms/createcmsentity.html',
                controller: "mui.cms.createcmsentityCtrl",
                resolve: {
                    params: function () {
                        return {
                            ID: ID,
                            Caption: Caption
                        };
                    }
                },
                scope: $scope,
                windowClass: 'addCMSEntityModel popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.DetailOpenPopupForSubPageCreation = function (ID, Caption) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/cms/SubPageCreation.html',
                controller: "mui.cms.SubPageCreationCtrl",
                resolve: {
                    params: function () {
                        return {
                            ID: ID,
                            Caption: Caption,
                            EntityID: $scope.ID
                        };
                    }
                },
                scope: $scope,
                windowClass: 'SubEntityCreationModal popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
    }
    app.controller('mui.cms.default.detailCtrl', ['$scope', '$location', 'CmsService', '$timeout', '$compile', '$sce', '$stateParams', '$resource', '$translate', 'DetailService','$modal', muiplanningtooldefaultCtrl]);
    function DetailService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetIsEditFeatureEnabled:GetIsEditFeatureEnabled,
            DeleteCmsEntity:DeleteCmsEntity,
            GetAllCmsEntitiesByNavID:GetAllCmsEntitiesByNavID,
            GetRevisedContentByFeature:GetRevisedContentByFeature,
            GetCmsTreeStyle:GetCmsTreeStyle,
            GetEntityType:GetEntityType,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            GetEntityAttributesGroupValues:GetEntityAttributesGroupValues,
            DuplicateEntities:DuplicateEntities
        });
        function GetIsEditFeatureEnabled(){var request=$http({method:"get",url:"api/cms/GetIsEditFeatureEnabled/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
        function DeleteCmsEntity(ID){var request=$http({method:"delete",url:"api/cms/DeleteCmsEntity/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
        function GetAllCmsEntitiesByNavID(NavID,StartpageNo,MaxPageNo){var request=$http({method:"get",url:"api/cms/GetAllCmsEntitiesByNavID/"+NavID+"/"+StartpageNo+"/"+MaxPageNo,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
        function GetRevisedContentByFeature(EntityID){var request=$http({method:"get",url:"api/cms/GetRevisedContentByFeature/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
        function GetCmsTreeStyle(){var request=$http({method:'get',url:'api/metadata/GetCmsTreeStyle',params:{action:'get'}});return(request.then(handleSuccess,handleError));}
        function GetEntityType(ModuleID){var request=$http({method:"get",url:"api/Metadata/GetEntityType/"+ModuleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityAttributesGroupValues(EntityID,EntityTypeID,GroupID,IsCmsContent){var request=$http({method:"get",url:"api/Metadata/GetEntityAttributesGroupValues/"+EntityID+"/"+EntityTypeID+"/"+GroupID+"/"+IsCmsContent,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
        function DuplicateEntities(formobj) { var request = $http({ method: "post", url: "api/Planning/DuplicateEntities/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("DetailService", ['$http', '$q', DetailService]);
})(angular, app);