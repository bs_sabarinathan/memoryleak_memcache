﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    public partial class AdditionalObjectiveEntityValuesDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _typeid;
        protected string _instruction;
        protected bool _isenablefeedback;
        protected int _unitid;
        protected decimal _plannedTarget;
        protected decimal _targetoutcome;
        protected int _ratingobjective;
        protected string _comments;
        protected int _fulfillment;
        protected bool _isActive;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public AdditionalObjectiveEntityValuesDao() { }

        public AdditionalObjectiveEntityValuesDao(int pEntityid, int pTypeId, string pInstruction, bool pIsEnableFeedback, int pUnitID, int pPlannedTarget, int pTargetOutcome, int pRatingObjective, string pComments,int pFulfillment,bool pisactive)
        {
            this._entityid = pEntityid;
            this._typeid = pTypeId;
            this._instruction = pInstruction;
            this._isenablefeedback = pIsEnableFeedback;
            this._unitid = pUnitID;
            this._plannedTarget = pPlannedTarget;
            this._targetoutcome = pTargetOutcome;
            this._ratingobjective = pRatingObjective;
            this._comments = pComments;
            this._unitid = pUnitID;
            this._plannedTarget = pPlannedTarget;
            this._ratingobjective = pRatingObjective;
            this._comments = pComments;
            this._fulfillment = pFulfillment;
            this._isActive = pisactive;

        }

        public AdditionalObjectiveEntityValuesDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual int TypeId
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public virtual string Instruction
        {
            get { return _instruction; }
            set
            {
                _instruction = value;
            }

        }

        public virtual bool IsEnableFeedback
        {
            get { return _isenablefeedback; }
            set
            {
                _isenablefeedback = value;
            }
        }

        public virtual int UnitID
        {
            get { return _unitid; }
            set { _unitid = value; }

        }

        public virtual decimal PlannedTarget
        {
            get { return _plannedTarget; }
            set { _plannedTarget = value; }

        }

        public virtual decimal TargetOutcome
        {
            get { return _targetoutcome; }
            set { _targetoutcome = value; }

        }

        public virtual int RatingObjective
        {
            get { return _ratingobjective; }
            set { _ratingobjective = value; }

        }
        public virtual string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
      
        public virtual int Fulfillment
        {
            get { return _fulfillment; }
            set { _fulfillment = value; }
        }

        public virtual bool IsActive
            {
            get { return _isActive; }
            set { _isActive = value; }

            }
        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AdditionalObjectiveEntityValuesDao = "AdditionalObjectiveEntityValuesDao";
            public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string TypeId = "TypeId";
            public const string Instruction = "Instruction";
            public const string IsEnableFeedback = "IsEnableFeedback";
            public const string UnitID = "UnitID";
            public const string PlannedTarget = "PlannedTarget";
            public const string TargetOutcome = "TargetOutcome";
            public const string RatingObjective = "RatingObjective";
            public const string Comments = "Comments";
            public const string Fulfillment = "Fulfillment";
            public const string IsActive = "IsActive";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AdditionalObjectiveEntityValuesDao = "AdditionalObjectiveEntityValuesDao";
            public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string TypeId = "TypeId";
            public const string Instruction = "Instruction";
            public const string IsEnableFeedback = "IsEnableFeedback";
            public const string UnitID = "UnitID";
            public const string PlannedTarget = "PlannedTarget";
            public const string TargetOutcome = "TargetOutcome";
            public const string RatingObjective = "RatingObjective";
            public const string Comments = "Comments";
            public const string Fulfillment = "Fulfillment";
            public const string IsActive = "IsActive";
        }

        #endregion
    }
}
