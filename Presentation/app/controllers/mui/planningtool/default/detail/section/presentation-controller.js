﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailpresentationCtrl($scope, $timeout, $http, $compile, $window, $resource, $stateParams, PresentationService) {
        $scope.date2 = new Date();
        $scope.presentationList = [];
        $scope.SelectionList = [];
        var routeid = $stateParams.ID;
        $scope.UnShowEditPage = false;
        $scope.UnShowSave = false;
        var Timeoutvar = {};
        angular.element(document).ready(function () {
            Timeoutvar.lockstatus = $timeout(function () {
                if ($scope.IsLock == false) {
                    $scope.UnShowEditPage = true;
                    $scope.UnShowSave = true;
                } else {
                    $scope.UnShowEditPage = false;
                    $scope.UnShowSave = false;
                }
            }, 500);
        });
        this.setSelected = function (c) {
            var checkopt = $.grep($scope.SelectionList, function (e) {
                return e == c.id;
            });
            if (c.checked1 == true && checkopt.length == 0) {
                $scope.SelectionList.push(c.id);
            } else if (c.checked1 == false && checkopt.length > 0) {
                $scope.SelectionList.splice($.inArray(c.id, $scope.SelectionList), 1);
            }
        }
        $scope.toggledate = function () {
            $scope.isDateteVisible = !$scope.isDateteVisible;
        };
        $scope.isDateteVisible = false;
        $scope.toggletext = "Show publication for sub levels";
        $scope.isVisible = false;
        $scope.EnableEdit = {
            EditStatus: true,
            EditText: "Edit"
        };
        $scope.editPage = function () {
            if ($scope.EnableEdit.EditStatus == true) {
                $scope.EnableEdit.EditStatus = false;
                $scope.EnableEdit.EditText = "Close";
                $scope.showEditor = true;
                $scope.showHolder = false;
                $scope.htmlEditorText = $scope.presentationList.Content;
            } else {
                $scope.EnableEdit.EditStatus = true;
                $scope.EnableEdit.EditText = "Edit";
                $scope.showEditor = false;
                $scope.showHolder = false;
                $scope.htmlEditorText = $scope.presentationList.Content;
            }
        };
        $scope.myTree = [];
        loadPresentation();
        $scope.$on('LoadPresentations', function (event, ID) {
            $scope.myTree = [];
            $('#EditorHolder').html('');
            routeid = ID;
            loadPresentation();
        });

        function loadPresentation() {
            PresentationService.GetEntitydescendants(routeid).then(function (GetTree) {
                $scope.myTree.push(JSON.parse(GetTree.Response));
            });
            PresentationService.GetPresentationById(routeid).then(function (EntityPresentationlist) {
                if (EntityPresentationlist.Response != null) {
                    $scope.showEditor = false;
                    $scope.presentationList = EntityPresentationlist.Response;
                    $('#EditorHolder').html($scope.presentationList.Content);
                    $scope.showHolder = true;
                }
            });
        }
        $scope.SaveEntityPresentationContent = function () {
            var presentationObj = {};
            presentationObj.EntityID = routeid;
            presentationObj.entityList = $scope.SelectionList;
            presentationObj.PublishedOn = $scope.date2;
            if ($scope.htmlEditorText == undefined) {
                presentationObj.Content = null;
            } else {
                presentationObj.Content = $scope.htmlEditorText;
            }
            PresentationService.InsertPresentation(presentationObj).then(function () {
                $('#userModal').modal('hide');
                $scope.showEditor = false;
                $scope.showHolder = true;
                $('#EditorHolder').html($scope.htmlEditorText);
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.presentationCtrl']"));
            $timeout.cancel(Timeoutvar);
        });
    }
    app.controller("mui.planningtool.default.detail.presentationCtrl", ['$scope', '$timeout', '$http', '$compile', '$window', '$resource', '$stateParams', 'PresentationService', muiplanningtooldefaultdetailpresentationCtrl]);
})(angular, app);