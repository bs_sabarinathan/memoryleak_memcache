using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
    /// NavigationDao object for table 'CM_ApplicationUrlTrack'.
	/// </summary>

    public partial class ApplicationUrlTrackDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
        protected Guid _trackid;
		protected string _trackvalue;
		protected DateTime _doi;
		

		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ApplicationUrlTrackDao() {}

        public ApplicationUrlTrackDao(int pID,Guid pTrackId, string pTrackValue, DateTime pDoi)
		{
            this._id = pID;
            this._trackid = pTrackId;
            this._trackvalue = pTrackValue;
            this._doi = pDoi; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual Guid  TrackID
		{
			get { return _trackid; }
            set { _bIsChanged |= (_trackid != value); _trackid = value; }
			
		}

        public virtual  string TrackValue
		{
			get { return _trackvalue; }
			set 
			{
			  _bIsChanged |= (_trackvalue != value); 
			  _trackvalue = value; 
			}
			
		}



        public virtual DateTime DOI
        {
            get { return _doi; }
            set { _bIsChanged |= (_doi != value); _doi = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string NavigationDao = "ApplicationUrlTrackDao";
            public const string TrackID = "TrackID";
            public const string TrackValue = "TrackValue";
            public const string DOI = "DOI";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string NavigationDao = "ApplicationUrlTrackDao";
            public const string TrackID = "TrackID";
            public const string TrackValue = "TrackValue";
            public const string DOI = "DOI";				
		}

		#endregion
		
		
	}
	
}
