﻿app.directive("customtooltip", ['$compile', '$parse', function ($compile, $parse) {

    return function (scope, element, attrs) {


        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark qtip-desc");
        scope.closeButton = (attrs.closeButton == null ? false : true)

        scope.qtipPointerPos = "top center";
        scope.qtipContentPos = "top center";

        $(element).qtip({
            content: {
                text: function (api) {
                    return ($(this).attr('qtip-content') != "") ? $(this).attr('qtip-content') : "-";
                },
                button: scope.closeButton
            },
            style: {
                classes: scope.qtipSkin + " qtip-shadow qtip-rounded qtip-desc",
                title: { 'display': 'none' }
            },
            show: {
                event: 'mouseenter click unfocus',
                solo: true

            },
            events: {
                hide: function (event, api) {
                   event: 'unfocus click mouseleave'
                }
            },
            hide: {
                delay: 0,
                fixed: false,
                effect: function () { $(this).fadeOut(0); },
                event: 'unfocus click mouseleave'

            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window),
                adjust: {
                    method: 'shift',
                    mouse: true
                },
                corner: {
                    target: 'bottom center',
                    tooltip: 'bottom center',
                    mimic: 'top'
                },
                target: 'mouse'
            }
        });

        scope.$on("$destroy", function () {
            $(element).qtip('destroy', true); // Immediately destroy all tooltips belonging to the selected elements
            $(window).off("resize.Viewport"); //$destory Method which can be used to clean up DOM bindings before an element is removed from the DOM.
        });

    }
}]);
