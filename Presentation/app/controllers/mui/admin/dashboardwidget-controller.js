﻿(function (ng, app) {
    "use strict";

    function muiadmindashboardwidgetCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        $scope.Widgettype = {
            TypeName: '',
            ddlGlobalRole: 0,
            ID: 0
        };
        AdminService.GetGlobalRole().then(function (res) {
            $scope.GlobalRoles = res.Response;
        });
        var IsforAdmin = true;
        AdminService.GetWidgetTypesByID(parseInt($cookies['UserId'], 10), IsforAdmin, 0).then(function (res) {
            $scope.listdata = res.Response;
            $scope.widgetResult = $scope.listdata;
        });
        $scope.editableInPopup = '<a class="iconLink"  href="javascript:void(0);"ng-click="GetModuleByID(row)" ><i class="icon-edit"></i></a> '
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridwidgetdata = {
            data: 'widgetResult',
            enablePinning: false,
            enableColumnReordering: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60
            }, {
                field: "TypeName",
                displayName: $translate.instant('LanguageContents.Res_5008.Caption'),
                width: 120
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 60
            }]
        };
        $scope.GetModuleByID = function GetModuleByID(row) {

            $scope.Widgettype.ID = row.entity.Id;
            $scope.Widgettype.TypeName = row.entity.TypeName;
            $scope.EnableUpdate = true;
            $scope.EnableAdd = false;
            AdminService.GetGlobalRole().then(function (getroles) {
                $scope.GlobalRoles = getroles.Response;
                AdminService.GetWidgetTypeRolesByID(row.entity.Id).then(function (getroletemplatebyid) {
                    var a = getroletemplatebyid.Response;
                    $scope.Widgettype.ddlGlobalRole = a;
                });
            });
            $('#moduleModal').modal('show');
        };
        $scope.addModule = function () {
            $scope.ID = 0;
            $scope.EnableUpdate = false;
            $scope.EnableAdd = true;
            $('#moduleModal').modal('show');
        };
        $scope.save = function () {
            if ($scope.Widgettype.ddlGlobalRole.length > 0) {
                if ($scope.Widgettype.ddlGlobalRole.length > 0 && $scope.Widgettype.ID > 0) {
                    var AddGlobalRoleUser = {};
                    AddGlobalRoleUser.widgetTypeID = $scope.Widgettype.ID;
                    AddGlobalRoleUser.roleID = $scope.Widgettype.ddlGlobalRole;
                    AdminService.InsertWidgetTypeRoles(AddGlobalRoleUser);
                }
                $('#moduleModal').modal('hide');
            } else {
                NotifyError($translate.instant('LanguageContents.Res_4646.Caption'));
            }
        };
        $scope.update = function () {
            if ($scope.Widgettype.ddlGlobalRole.length > 0 && $scope.Widgettype.ID > 0) {
                var addGlobalroles = {};
                addGlobalroles.widgetTypeID = $scope.Widgettype.ID;
                addGlobalroles.roleID = $scope.Widgettype.ddlGlobalRole;
                AdminService.InsertWidgetTypeRoles(addGlobalroles).then(function () {
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                });
            } else {
                NotifyError($translate.instant('LanguageContents.Res_4646.Caption'));
            }
            $('#moduleModal').modal('hide');
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.dashboardwidgetCtrl']"));
        });
    }
    app.controller("mui.admin.dashboardwidgetCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadmindashboardwidgetCtrl]);
})(angular, app);