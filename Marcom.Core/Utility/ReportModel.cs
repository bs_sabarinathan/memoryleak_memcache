﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrandSystems.Marcom.Utility
{
    public class ReportModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Category { get; set; }
    }
}
