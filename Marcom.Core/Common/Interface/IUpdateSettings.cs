﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IUpdateSettings
    {
        int Id { get; set; }
        string CurrentVersion { get; set; }
        string Client { get; set; }
        string Licence { get; set; }
        int VersionId { get; set; }
        int InstanceId { get; set; }
        IList<IAvailableUpdates> AvailableUpdates { get; set; }
        IList<IPreviousUpdates> PreviousUpdates { get; set; }
    }
}
