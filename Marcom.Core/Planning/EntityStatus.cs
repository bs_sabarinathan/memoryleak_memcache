﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class EntityStatus : IEntityStatus
    {
         #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _status;
        protected int _timestatus;
        protected string _comment;

        #endregion

        #region Constructors
        public EntityStatus() { }

        public EntityStatus(int pID, int pEntityID, int pStatus, int pTimeStatus, string pComment)
        {
            this._id = pID;
            this._entityid = pEntityID;
            this._status = pStatus;
              this._timestatus = pTimeStatus;
            this._comment = pComment;
        }

        public EntityStatus(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _entityid; }
            set
            {
                _entityid = value;
            }

        }

        public int Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }

        }

        public int TimeStatus
        {
            get { return _timestatus; }
            set
            {
                _timestatus = value;
            }

        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
            }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityStatus castObj = null;
            try
            {
                castObj = (EntityStatus)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
