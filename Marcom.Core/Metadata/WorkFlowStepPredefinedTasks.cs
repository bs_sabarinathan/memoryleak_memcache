﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// Attribute object for table 'MM_Attribute'.
    /// </summary>

    internal class WorkFlowStepPredefinedTasks : IWorkFlowStepPredefinedTasks
    {
        #region Member Variables

        protected int _id;
        protected int _stepid;
        protected int _taskid;

        #endregion 

        #region Constructors
        public WorkFlowStepPredefinedTasks() { }

        public WorkFlowStepPredefinedTasks(int pId, int pStepID, int pTaskID)
        {
            this._id = pId;
            this._stepid = pStepID;
            this._taskid = pTaskID;
        }

        public WorkFlowStepPredefinedTasks(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int StepID
        {
            get { return _stepid; }
            set { _stepid = value; }

        }

        public int TaskID
        {
            get { return _taskid; }
            set { _taskid = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Attribute castObj = null;
            try
            {
                castObj = (Attribute)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>     
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
