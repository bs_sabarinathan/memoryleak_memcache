﻿(function (ng, app) {
    "use strict";

    function muimediabank1Ctrl($window, $location, $route, $timeout, $scope, $cookies, $resource, requestContext, $routeParams, $rootScope, $translate, DamService, PlanningService, _, $compile) {


        $scope.PageNo = 1;
        $scope.MediaAttributeOptions = { "Attributes": [] };
        $scope.MediaAssetTypes = { "AssetTypes": [] };
        $scope.isLibraryExpanded = false;
        $scope.SelectedTagWords = [];

        $scope.totalRecords = 0;
        $scope.damattributeshow = false;
        $scope.clearTagwords = false;
        $scope.RowsPerPage = 18;

        $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 },
       { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
        $scope.SettingsDamAttributes = {};

        $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1, "FieldName": "Name", "Descending": false },
       { "Name": "Name (Descending)", ID: 2, "FieldName": "Name", "Descending": true }, { "Name": "Published date (Ascending)", ID: 3, "FieldName": "Date", "Descending": false }, { "Name": "Published date (Descending)", ID: 4, "FieldName": "Date", "Descending": true }];

        $scope.FilterStatus = 1;

        $scope.DamViewName = "Thumbnail";

        $scope.OrderbyName = "Published date (Descending)";

        $scope.callbackid = 0;

        $scope.OrderBy = 4;

        var dynkeyvalpair = [], assettypes = [], attrPreselect = [], searchtext = "";

        var arrayConstructor = [].constructor;
        var objectConstructor = {}.constructor;

        var html = '';

        var searchCredential = {};

        searchCredential = { nb: 0, lb: 0, ST: "", AT: "", Criteria: {} };


        if ($routeParams.searchobj != undefined) {

            if ($("#damUI").hasClass("searchSnippetStyle")) {
                $("#damUI").removeClass('searchSnippetStyle').addClass('searchSnippetStyle');
            }
            else {
                $("#damUI").addClass('searchSnippetStyle');
            }


            searchCredential = JSON.parse($routeParams.searchobj);
            var dt = JSON.parse($routeParams.searchobj);
            if (dt.Criteria != undefined) {
                checkIt(dt.Criteria);
            }
            else
                dynkeyvalpair = [];
            if (searchCredential.AT != undefined && searchCredential.AT.length > 0) {
                assettypes = searchCredential.AT.split(',');
            }
            if (searchCredential.nb != undefined)
                if (searchCredential.nb == 0)
                    $(".navbar-mui-top").hide();
            if (searchCredential.lb != undefined)
                if (searchCredential.lb == 0) {
                    $(".navbar-mui-main").hide();
                    $timeout(function () {
                        LoadQuerystring(searchCredential.ST);
                    }, 100);
                }
        }
        else {
            $("#damui-Assettypes-quicksearch").html("");
            if ($("#damUI").hasClass("searchSnippetStyle")) {
                $("#damUI").removeClass('searchSnippetStyle');
            }
            if (searchCredential.Criteria != undefined)
                checkIt(searchCredential.Criteria);
            else
                dynkeyvalpair = [];
            if (searchCredential.AT != undefined && searchCredential.AT.length > 0) {
                assettypes = searchCredential.AT.split(',');
            }

        }


        function GetInitialQuerystringdata() {

            if ($routeParams.searchobj != undefined) {

                Loadresult();
            }
            else {

                Loadresult();
            }
        }

        function checkIt(object) {
            if (object === null) {

            }
            else if (object === undefined) {

            }
            else if (object.constructor === arrayConstructor) {

                readArray(object);
            }
            else if (object.constructor === objectConstructor) {
                readObject(object);
            }
            else {


            }
        }



        var productionsearchtypes = { "Assets": ["Attachments", "Assetlibrary"] };


        function cleartheSarchHistory() {
            $scope.isResultEmpty = true;
            $(".mediabanksearchresult #searchPartresultCaption").text("Total 0 hit");
            $(".noResultFound").text("Your search did not match any item.");
        }


        function Loadresult() {

            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0)
                assetypes = $scope.selectedAssettypes;
            else
                assetypes = $scope.MediaBankAssetTypes.Item1;
            var criteria = GetXML();

            $(window).AdjustHeightWidth();
            //var SearchResult = $resource('planning/CallbackSnippetSearch', { get: { method: 'GET' } });
            // var search = new SearchResult();
            var search = {};
            if (searchCredential.ST == undefined || searchCredential.ST == "") {
                search.Text = "";
            }
            else {
                search.Text = searchCredential.ST;
            }
            search.SearchTerm = {};
            search.ETypeID = assetypes;
            search.istag = false;
            search.Searchtype = "Assetlibrary";
            search.PageID = 1;
            search.IsGlobalAdmin = true;
            search.metadata = dynkeyvalpair != undefined ? dynkeyvalpair : [];
            search.isQuerystringdata = true;
            search.rowsperpage = 20;
            search.CurrentScroll = "Assetlibrary";
            var orderobj = $.grep($scope.OrderbyObj, function (e) {
                return e.ID == $scope.OrderBy;
            });
            search.orderbyfieldName = orderobj[0].FieldName;
            search.desc = orderobj[0].Descending;
            PlanningService.CallbackSnippetSearch(search).then(function (searchresponse) {
                //   var searchresponse = SearchResult.save(search, function () {
                isfilterRequest = true;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                var dyn_Contresult = "";
                if ($scope.PageNo == 1) {
                    refreshViewObj();
                }

                if (searchresponse.Response == false) {
                    cleartheSarchHistory();
                    refreshViewObj();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                        refreshViewObj();
                    }
                    else if (searchresponse.Response.SearchResultEntity.length == 0) {
                        cleartheSarchHistory();
                        refreshViewObj();

                    }
                    else {

                        if (searchresponse.Response == false) {
                            cleartheSarchHistory();
                            refreshViewObj();
                        }

                        if (searchresponse.Response.TotalPages != 0) {

                            $scope.totalRecords = searchresponse.Response.TotalPages;


                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);


                                if (arr.AssetDetails.length > 0) {
                                    for (var j = 0, asset; asset = arr.AssetDetails[j++];) {
                                        var isexist = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (rel) {
                                            return rel.AssetUniqueID == asset.AssetUniqueID;
                                        });
                                        if (isexist.length == 0)
                                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                                    }

                                    if ($scope.FilterStatus == 1)
                                        LoadAssetThimbnailView(arr);
                                    else if ($scope.FilterStatus == 2)
                                        LoadSummaryView(arr);
                                    else if ($scope.FilterStatus == 3)
                                        LoadListView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;

                            loadScrollSettings();

                        }

                    }
                }
            });


        }

        function RetreiveAssetDatafromXML(res) {
            var assetobj = [], attributerelations = [], result = {}, attributes = [], htmlsearchAttrtempl = '', attributewithValues = {};
            for (var i = 0, obj; obj = res[i++];) {
                htmlsearchAttrtempl = '';
                $($.parseXML(obj.AssetDetails)).find('p').each(function () {
                    assetobj.push({
                        "FileUniqueID": $(this).attr('FileUniqueID'), "FileName": $(this).attr('FileName'), "Extension": $(this).attr('Extension'), "Size": $(this).attr('Size'),
                        "VersionNo": $(this).attr('VersionNo'), "ProcessType": $(this).attr('ProcessType'), "OwnerID": $(this).attr('OwnerID'), "CreatedOn": $(this).attr('CreatedOn'), "FileGuid": $(this).attr('FileGuid'),
                        "Description": $(this).attr('Description'), "AssetID": $(this).attr('AssetID'), "FolderID": $(this).attr('FolderID'), "EntityID": $(this).attr('EntityID'),
                        "AssetUniqueID": $(this).attr('AssetUniqueID'), "AssetName": $(this).attr('AssetName'), "LinkURL": $(this).attr('LinkURL'), "MimeType": $(this).attr('MimeType'),
                        "AssetTypeid": $(this).attr('AssetTypeid'), "ActiveFileID": $(this).attr('ActiveFileID'), "ColorCode": $(this).attr('ColorCode'), "ShortDescription": $(this).attr('ShortDescription'),
                        "Status": $(this).attr('Status'), "Category": $(this).attr('Category'), "IsPublish": $(this).attr('IsPublish'), "LinkedAssetID": $(this).attr('LinkedAssetID')
                    });
                });
                var tempAttributes = [];

                var tempFiles = {};
                $($.parseXML(obj.AssetValueDetails)).find('p').each(function () {
                    var assetID = $(this).attr('ID');
                    $.each(this.attributes, function (i, attrib) {
                        var name = attrib.name.replace("Attr_", ""), value = attrib.value, matchtxt;
                        matchtxt = matchHighlightText(searchCredential.ST != undefined ? searchCredential.ST : "", value);
                        tempFiles[name] = value;
                    });
                    attributewithValues[assetID] = tempFiles;
                });
            }
            result = { "AssetDetails": assetobj, "attrRelation": attributewithValues }
            return result;
        }

        function matchHighlightText(sub, source) {
            var src_str = source;
            var term = sub, htmlsearchAttrtempl = "";
            term = term.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
            var pattern = new RegExp("(" + term + ")", "gi");
            src_str = src_str.replace(pattern, "<mark>$1</mark>");
            src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4<mark>$5</mark>$6<mark>$7</mark>");
            return src_str;
        }



        function readObject(myobj) {

            for (var key in myobj) {
                if (myobj.hasOwnProperty(key)) {
                    dynkeyvalpair.push({ "Id": key, "Values": myobj[key] != undefined ? myobj[key].split(",") : [] })
                    checkIt(myobj[key]);
                }
            }
        }

        function readArray(myobj) {
            var n = myobj.length;
            html += myobj;
            for (var i = 0; i < n; i++) {
                checkIt(myobj[i]);
                html += myobj[i];
            }
        }




        $scope.FilterByOptionChange = function (id) {

            if (id != $scope.FilterStatus) {

                $scope.PageNo = 1;
                $scope.FilterStatus = id;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.DamViewName = res[0].Name;
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }

        }



        $scope.OrderbyOption = function (orderid) {

            if (orderid != $scope.OrderBy) {
                $scope.PageNo = 1;
                $scope.OrderBy = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $scope.OrderbyName = orderobj[0].Name;

                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus;
                });
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }

        }

        $scope.MediaBankAssetTypes = [];
        $scope.selectedAssettypes = [];
        $scope.selectedAttributes = [];
        $scope.FilterSchema = "";
        $scope.FilterSchemaArr = [];

        $timeout(function () { PageLoad(); }, 10);
        function PageLoad() {

            $scope.RowsPerPage = (Math.floor($(".mediabankmainUIcontent").width() / 176) * 3); //176 is the default height of the thumnail box.so here we hardcoded

            $scope.subview = "Thumbnail";
            $scope.PageNo = 1;
            GetAllExtensionTypesforDAM();
            GetMediaBankFilterAttributes();
        }


        function GetAllExtensionTypesforDAM() {
            DamService.GetAllExtensionTypesforDAM()
                .then(
                    function (data) {
                        $scope.MediaBankAssetTypes = data.Response;
                        LoadAssettypes();
                        GetDAMViewSettings();
                        GetDAMToolTipSettings();
                    }
                );
        }

        function GetMediaBankFilterAttributes() {
            DamService.GetCustomFilterAttributes(4)
                .then(
                    function (data) {
                        var res = data.Response;
                        if (res != undefined)
                            $scope.damattributeshow = true;
                        LoadAssetAttributes(res);

                    }
                );
        }

        $scope.loadingfilter = true;

        var checkboxtypingTimer;                //timer identifier
        var checkboxdoneTypingInterval = 500;

        $scope.FilterbyAttributeSelection = function (event, attrid, attrtype, optionid, attrlevel) {

            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                if (remainRecord.length == 0) {
                    $scope.selectedAttributes.push({ "Condition": 0, "AttributeID": attrid, "AttributeTypeID": attrtype, "AttributeLevel": attrlevel, "AttributeCaption": $(checkbox).attr('attributecaption'), "Operator": "IN", "Values": ['' + $(checkbox).attr('optioncaption').toString() + ''], "ValuesIDArr": [optionid] });
                }
                else {
                    //update the values
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == $(checkbox).attr('optioncaption'); });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueRecords.length == 0) {
                        remainRecord[0].Values.push($(checkbox).attr('optioncaption'));
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                if (remainRecord.length > 0) {
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == $(checkbox).attr('optioncaption'); });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            if ($scope.loadingfilter == true) {

                $scope.loadingfilter = false;
                $scope.PageNo = 1;
                //$timeout(GetFilteredAssetsData, 200);

                $timeout.cancel(checkboxtypingTimer);
                checkboxtypingTimer = $timeout(function () { GetFilteredAssetsData(); }, checkboxdoneTypingInterval);
            }
        }

        $scope.RemoveCustomAttributefromFilter = function (attrid, valueCaption) {
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
            if (remainRecord.length > 0) {
                var valueRecords = [], valueIds = [];
                valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == valueCaption; });
                remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                if (remainRecord[0].Values.length == 0) {
                    $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                }
                var attrValue = {};
                attrValue = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Caption == valueCaption; })[0];
                if (attrValue != undefined) {
                    attrValue["Isselectd"] = false;
                    var valueIds = [];
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == attrValue.Id; });
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                }

                $scope.PageNo = 1;
                $timeout(function () { GetFilteredAssetsData(); }, 100);

            }
        }



        function LoadAssetAttributes(res) {

            $scope.MediaAttributeOptions.Attributes = res;
            $("#media-customsearch-AssetAttributes").html("");
            var html = "";
            for (var i = 0, type; type = res[i++];) {
                if (type.AttributeTypeID != 17) {
                    $scope.MediaAttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : MediaAttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    if (type.Options != null) {
                        if (type.Options.length > 0) {

                            var _qryAttrs = [];
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;

                                _qryAttrs = $.grep(dynkeyvalpair, function (e) {
                                    return e.Id == type.AttributeID;
                                });
                                if (_qryAttrs != undefined) {
                                    if (_qryAttrs.length > 0) {
                                        if (_qryAttrs[0].Values.length > 0)
                                            if (_qryAttrs[0].Values.indexOf(opt.Id.toString()) > -1) {


                                                var remainRecord = [];
                                                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == type.AttributeID; });
                                                if (remainRecord.length == 0) {
                                                    $scope.selectedAttributes.push({ "Condition": 0, "AttributeID": type.AttributeID, "AttributeTypeID": type.AttributeTypeID, "AttributeLevel": type.Level, "AttributeCaption": type.AttributeCaption, "Operator": "IN", "Values": ['' + opt.Caption + ''], "ValuesIDArr": [opt.Id] });
                                                }
                                                else {
                                                    //update the values
                                                    var valueRecords = [], valueIds = [];
                                                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == opt.Caption; });
                                                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == opt.Id; });
                                                    if (valueRecords.length == 0) {
                                                        remainRecord[0].Values.push(opt.Caption);
                                                    }
                                                    if (valueIds.length == 0) {
                                                        remainRecord[0].ValuesIDArr.push(opt.Id);
                                                    }
                                                }
                                            }
                                    }

                                }
                            }
                            $scope.MediaAttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.MediaAttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAttributeOptions.FilterExpand_' + type.AttributeID + '"> ';
                            html += '  <input class="searchBy" ng-model="MediaAttributeOptions.FilterText_' + type.AttributeID + '" type="text" placeholder="Search by ' + type.AttributeCaption + '" /> ';
                            html += '  <ul>';
                            html += ' <li ng-repeat="item in MediaAttributeOptions.FilterOption_' + type.AttributeID + ' | filter:MediaAttributeOptions.FilterText_' + type.AttributeID + ' " "> ';
                            html += '  <label class="checkbox checkbox-custom "><input data-ng-model="item.Isselectd" attributecaption="{{MediaAttributeOptions.FilterProp_' + type.AttributeID + '.AttributeCaption}}" optioncaption="{{item.Caption}}" ng-click=\"FilterbyAttributeSelection($event,' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeID + ',' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeTypeID + ',item.Id,' + $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""].Level + ')\" type="checkbox"><i ng-class="MediaoptionClass(' + type.AttributeID + ',item.Id)"></i>{{item.Caption}}</label>';
                            html += ' </li> ';
                            html += ' </ul>';
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                }
                else if (type.AttributeTypeID == 17) {
                    $scope.MediaAttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : MediaAttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    $scope.tempscope = [];
                    if (type.Options != null) {
                        if (type.Options.length > 0) {
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;

                                _qryAttrs = $.grep(dynkeyvalpair, function (e) {
                                    return e.Id == type.AttributeID;
                                });
                                if (_qryAttrs != undefined) {
                                    if (_qryAttrs.length > 0) {
                                        if (_qryAttrs[0].Values.length > 0)
                                            if (_qryAttrs[0].Values.indexOf(opt.Id.toString()) > -1) {


                                                var remainRecord = [];
                                                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == type.AttributeID; });
                                                if (remainRecord.length == 0) {
                                                    $scope.selectedAttributes.push({ "Condition": 0, "AttributeID": type.AttributeID, "AttributeTypeID": type.AttributeTypeID, "AttributeLevel": type.Level, "AttributeCaption": type.AttributeCaption, "Operator": "IN", "Values": ['' + opt.Caption + ''], "ValuesIDArr": [opt.Id] });
                                                }
                                                else {
                                                    //update the values
                                                    var valueRecords = [], valueIds = [];
                                                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == opt.Caption; });
                                                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == opt.Id; });
                                                    if (valueRecords.length == 0) {
                                                        remainRecord[0].Values.push(opt.Caption);
                                                    }
                                                    if (valueIds.length == 0) {
                                                        remainRecord[0].ValuesIDArr.push(opt.Id);
                                                    }
                                                }
                                            }
                                    }

                                }
                            }

                            if (dynkeyvalpair != undefined) {
                                if (dynkeyvalpair.length > 0) {
                                    var _qryTags = $.map(dynkeyvalpair, function (e) {
                                        return parseInt(e.Id);
                                    });
                                    if (_qryTags.indexOf(87) > -1) {
                                        _qryTags = $.grep(dynkeyvalpair, function (e) {
                                            return e.Id == 87;
                                        });
                                        var tagpreselects = $.grep(type.Options, function (e) {
                                            return _qryTags[0].Values.indexOf(e.Id);
                                        });
                                        if (tagpreselects != undefined)
                                            if (tagpreselects.length > 0) {
                                                _qryTags = $.map(tagpreselects, function (e) {
                                                    return parseInt(e.Id);
                                                });
                                                $scope.SelectedTagWords = _qryTags;
                                            }
                                    }
                                }
                            }

                            $scope.MediaAttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.MediaAttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.MediaAttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAttributeOptions.FilterExpand_' + type.AttributeID + '"> ';

                            html += "<div class=\"tagwordsDiv\">";
                            html += "<directive-tagwords item-attrid = \"" + type.AttributeID + "\" item-cleartag = \"clearTagwords\" searchfrtag=\"alert(attrid,optionid,isselected)\"  item-show-hide-progress =\"null\" item-tagword-id=\"SelectedTagWords\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                            html += "</div>";
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                }

                html += ' <hr class="sr-dv-filtergroupSeparator" />';
            }

            $("#media-customsearch-AssetAttributes").html($compile(html)($scope));
        }

        $scope.alert = function (attrid, optionid, isselected) {
            if (isselected) {
                $scope.pageno = 1;
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                var optionCaption = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Id == optionid })[0].Caption;
                var attrCaption = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeCaption;
                var attrType = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeTypeID;
                if (remainRecord.length == 0) {

                    $scope.selectedAttributes.push({ "Condition": 0, "AttributeID": attrid, "AttributeTypeID": attrType, "AttributeLevel": 0, "AttributeCaption": attrCaption, "Operator": "IN", "Values": ['' + optionCaption.toString() + ''], "ValuesIDArr": [optionid] });


                }
                else {
                    //update the values
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == optionCaption; });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueRecords.length == 0) {
                        remainRecord[0].Values.push(optionCaption);
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            }
            else {
                var remainRecord = [];
                $scope.pageno = 1;
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                var optionCaption = $.grep($scope.MediaAttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Id == optionid })[0].Caption;
                var attrCaption = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeCaption;
                var attrType = $scope.MediaAttributeOptions["FilterProp_" + attrid + ""].AttributeTypeID;
                if (remainRecord.length > 0) {
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == optionCaption; });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            $scope.PageNo = 1;
            $timeout(function () { GetFilteredAssetsData(); }, 200);
        }

        $scope.RemoveSearchType = function (id) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == id; })[0];
            if (selctionRecord != undefined) {
                $scope.selectedAssettypes.splice($.inArray(selctionRecord, $scope.selectedAssettypes), 1);
                $scope.PageNo = 1;
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            }
        }

        $scope.makeexpandcollapse = function (id) {
            $scope.MediaAttributeOptions["FilterExpand_" + id + ""] = !$scope.MediaAttributeOptions["FilterExpand_" + id + ""];
        }

        $scope.PerformClear = function (id) {
            $scope.MediaAttributeOptions["FilterText_" + id + ""] = "";
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == id; });
            if (remainRecord.length > 0) {
                $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
            }
            var selctionRecord = [];
            selctionRecord = $.grep($scope.MediaAttributeOptions.Attributes, function (e) { return e.AttributeID == id; })[0];
            if (selctionRecord != undefined) {
                for (var j = 0, attr; attr = selctionRecord.Options[j++];) {
                    attr["Isselectd"] = false;
                }
            }
            if (remainRecord[0].AttributeTypeID == 17) {
                $timeout(function () {
                    $scope.clearTagwords = true;
                }, 100);

            }
            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0)
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            else
                LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);
        }

        $scope.MediaoptionClass = function (attrid, optionid) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0) {
                    var valueIds = [];
                    valueIds = $.grep(selctionRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueIds != undefined)
                        if (valueIds.length > 0)
                            return "checkbox checked";
                        else
                            return "checkbox";
                    else
                        return "checkbox";
                }
            }
            return "checkbox";
        }



        function GetDAMViewSettings() {

            DamService.GetDAMViewSettings()
                .then(
                    function (data) {
                        if (data.Response != null) {
                            $scope.SettingsDamAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                            $scope.SettingsDamAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                            $scope.SettingsDamAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                            GetInitialQuerystringdata();
                        }
                        else {
                            GetInitialQuerystringdata();
                        }
                    }
                );
        }

        function GetDAMToolTipSettings() {
            DamService.GetDAMToolTipSettings()
                .then(
                    function (data) {
                        if (data.Response != null) {
                            $scope.SettingsDamAttributes["ThumbnailToolTipSettings"] = data.Response[0].ThumbnailSettings;
                            $scope.SettingsDamAttributes["ListViewToolTipSettings"] = data.Response[0].ListViewSettings;
                        }
                    }
                );
        }

        function GetAssetsData(filter, orderby, pageno) {
            // returns all the extension types and related DAM types in the system.

            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0)
                assetypes = $scope.selectedAssettypes;
            else
                assetypes = $scope.MediaBankAssetTypes.Item1;
            var criteria = GetXML();


            $(window).AdjustHeightWidth();
            // var SearchResult = $resource('planning/CallbackSnippetSearch', { get: { method: 'GET' } });
            var search = {};
            //var search = new SearchResult();
            if (searchCredential.ST == undefined || searchCredential.ST == "") {
                search.Text = "";
            }
            else {
                search.Text = searchCredential.ST;
            }
            search.SearchTerm = {};
            search.ETypeID = assetypes;
            search.istag = false;
            search.Searchtype = "Assetlibrary";
            search.PageID = $scope.PageNo;
            search.IsGlobalAdmin = true;
            search.metadata = $scope.selectedAttributes;
            search.isQuerystringdata = false;
            search.rowsperpage = $scope.RowsPerPage;
            search.CurrentScroll = "Assetlibrary";

            var orderobj = $.grep($scope.OrderbyObj, function (e) {
                return e.ID == $scope.OrderBy;
            });
            search.orderbyfieldName = orderobj[0].FieldName;
            search.desc = orderobj[0].Descending;
            PlanningService.CallbackSnippetSearch(search).then(function (searchresponse) {
                //  var searchresponse = SearchResult.save(search, function () {
                isfilterRequest = true;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                var dyn_Contresult = "";
                if ($scope.PageNo == 1) {
                    refreshViewObj();
                }

                if (searchresponse.Response == false) {
                    cleartheSarchHistory();
                    refreshViewObj();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                        refreshViewObj();
                    }
                    else if (searchresponse.Response.SearchResultEntity.length == 0) {
                        cleartheSarchHistory();
                        refreshViewObj();

                    }
                    else {

                        if (searchresponse.Response == false) {
                            cleartheSarchHistory();
                            refreshViewObj();
                        }

                        if (searchresponse.Response.TotalPages != 0) {


                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    for (var j = 0, asset; asset = arr.AssetDetails[j++];) {
                                        var isexist = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (rel) {
                                            return rel.AssetUniqueID == asset.AssetUniqueID;
                                        });
                                        if (isexist.length == 0)
                                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                                    }

                                    if ($scope.FilterStatus == 1)
                                        LoadAssetThimbnailView(arr);
                                    else if ($scope.FilterStatus == 2)
                                        LoadSummaryView(arr);
                                    else if ($scope.FilterStatus == 3)
                                        LoadListView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;

                        }

                    }
                }
            });

        }

        function _getNumberofItemsPerRow() {
            var _itemPerRow = (Math.floor($(".mediabankmainUIcontent").width() / 176)); //176 is the default height of the thumnail box.so here we hardcoded
            var _itemsLoaded = $(".mediabankcontainerbox").length;
            if ((_itemsLoaded % _itemPerRow) > 0) {
                $scope.RowsPerPage = _itemPerRow - (_itemsLoaded % _itemPerRow) + (_itemPerRow * 3);
            }
            else
                $scope.RowsPerPage = (_itemPerRow * 3); //176 is the default height of the thumnail box.so here we hardcoded

        }

        function GetFilteredAssetsData() {
            // returns all the extension types and related DAM types in the system.
            var criteria = GetXML();
            _getNumberofItemsPerRow();
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0)
                assetypes = $scope.selectedAssettypes;
            else
                assetypes = $scope.MediaBankAssetTypes.Item1;

            $(window).AdjustHeightWidth();
            //  var SearchResult = $resource('planning/CallbackSnippetSearch', { get: { method: 'GET' } });
            //  var search = new SearchResult();
            var search = {};
            if (searchCredential.ST == undefined || searchCredential.ST == "") {
                search.Text = "";
            }
            else {
                search.Text = searchCredential.ST;
            }
            search.SearchTerm = {};
            search.ETypeID = assetypes;
            search.istag = false;
            search.Searchtype = "Assetlibrary";
            search.PageID = $scope.PageNo;
            search.IsGlobalAdmin = true;
            search.metadata = $scope.selectedAttributes;
            search.isQuerystringdata = false;
            search.rowsperpage = $scope.RowsPerPage;
            search.CurrentScroll = "Assetlibrary";
            var orderobj = $.grep($scope.OrderbyObj, function (e) {
                return e.ID == $scope.OrderBy;
            });
            search.orderbyfieldName = orderobj[0].FieldName;
            search.desc = orderobj[0].Descending;
            PlanningService.CallbackSnippetSearch(search).then(function (searchresponse) {
                //var searchresponse = SearchResult.save(search, function () {
                isfilterRequest = true;
                $scope.loadingfilter = true;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                var dyn_Contresult = "";
                if ($scope.PageNo == 1) {

                    refreshViewObj();
                }

                if (searchresponse.Response == false) {
                    $scope.loadingfilter = true;
                    cleartheSarchHistory();
                    refreshViewObj();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        $scope.loadingfilter = true;
                        cleartheSarchHistory();
                        refreshViewObj();
                    }
                    else if (searchresponse.Response.SearchResultEntity.length == 0) {
                        $scope.loadingfilter = true;
                        cleartheSarchHistory();
                        refreshViewObj();

                    }
                    else {

                        if (searchresponse.Response == false) {
                            $scope.loadingfilter = true;
                            cleartheSarchHistory();
                            refreshViewObj();
                        }

                        if (searchresponse.Response.TotalPages != 0) {
                            $scope.loadingfilter = true;
                            $scope.totalRecords = searchresponse.Response.TotalPages;


                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    for (var j = 0, asset; asset = arr.AssetDetails[j++];) {
                                        var isexist = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (rel) {
                                            return rel.AssetUniqueID == asset.AssetUniqueID;
                                        });
                                        if (isexist.length == 0)
                                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                                    }

                                    if ($scope.FilterStatus == 1)
                                        LoadAssetThimbnailView(arr);
                                    else if ($scope.FilterStatus == 2)
                                        LoadSummaryView(arr);
                                    else if ($scope.FilterStatus == 3)
                                        LoadListView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;

                        }

                    }
                }
            });

        }

        $(window).resize(function () {
            _getNumberofItemsPerRow();
        });

        function _frequentLoadInitialSearchBlock() {
            //calling second page to draw
            $timeout(function () {
                if ($scope.PageNo < 5) {
                    checkListItemContents();
                }
            }, 50);
        }


        function LoadAssetsData(filter, orderby, pageno) {
            // returns all the extension types and related DAM types in the system.
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0)
                assetypes = $scope.selectedAssettypes;
            else
                assetypes = $scope.MediaBankAssetTypes.Item1;
            var criteria = GetXML();
            _getNumberofItemsPerRow();
            LoadAssettypes();

            $(window).AdjustHeightWidth();
            // var SearchResult = $resource('planning/CallbackSnippetSearch', { get: { method: 'GET' } });
            //  var search = new SearchResult();
            var search = {};
            if (searchCredential.ST == undefined || searchCredential.ST == "") {
                search.Text = "";
            }
            else {
                search.Text = searchCredential.ST;
            }
            search.SearchTerm = {};
            search.ETypeID = assetypes;
            search.istag = false;
            search.Searchtype = "Assetlibrary";
            search.PageID = $scope.PageNo;
            search.IsGlobalAdmin = true;
            search.metadata = $scope.selectedAttributes;
            search.isQuerystringdata = false;
            search.rowsperpage = $scope.RowsPerPage;
            search.CurrentScroll = "Assetlibrary";
            var orderobj = $.grep($scope.OrderbyObj, function (e) {
                return e.ID == $scope.OrderBy;
            });
            search.orderbyfieldName = orderobj[0].FieldName;
            search.desc = orderobj[0].Descending;
            PlanningService.CallbackSnippetSearch(search).then(function (searchresponse) {
                //  var searchresponse = SearchResult.save(search, function () {
                isfilterRequest = true;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                var dyn_Contresult = "";
                if ($scope.PageNo == 1) {

                    $("#Assets-library-results").html(
                             $compile('')($scope));
                }

                if (searchresponse.Response == false) {
                    cleartheSarchHistory();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                    }
                    else if (searchresponse.Response.SearchResultEntity.length == 0) {
                        cleartheSarchHistory();
                        refreshViewObj();

                    }
                    else {

                        if (searchresponse.Response == false) {
                            cleartheSarchHistory();
                        }

                        if (searchresponse.Response.TotalPages != 0) {

                            $scope.totalRecords = searchresponse.Response.TotalPages;


                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    for (var j = 0, asset; asset = arr.AssetDetails[j++];) {
                                        var isexist = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (rel) {
                                            return rel.AssetUniqueID == asset.AssetUniqueID;
                                        });
                                        if (isexist.length == 0)
                                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                                    }

                                    if ($scope.FilterStatus == 1)
                                        LoadAssetThimbnailView(arr);
                                    else if ($scope.FilterStatus == 2)
                                        LoadSummaryView(arr);
                                    else if ($scope.FilterStatus == 3)
                                        LoadListView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;

                        }

                    }
                }
            });

        }

        $scope.makeAssettypeexpandcollapse = function () {
            $scope.MediaAssetTypes["FilterExpand"] = !$scope.MediaAssetTypes["FilterExpand"];
        }
        $scope.makeQuerystringexpandcollapse = function () {
            $scope.MediaAssetTypes["FilterExpandQuerystring"] = !$scope.MediaAssetTypes["FilterExpandQuerystring"];
        }

        $scope.MediaTypeoptionClass = function (id) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == id; });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0)
                    return "checkbox checked";
                else
                    return "checkbox";
            }
            return "checkbox";
        }

        $scope.MediaTypePerformClear = function () {
            $scope.MediaAttributeOptions["FilterText"] = "";
            var remainRecord = [];

            $scope.selectedAssettypes = [];

            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0)
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            else
                LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);

        }

        $scope.QueryStringQuickSearchClear = function () {
            $scope.MediaAssetTypes["FilterQuerystringText"] = "";
            var remainRecord = [];

            $scope.selectedAssettypes = [];

            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0)
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            else
                LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);

        }

        function _autoselectAssetTypes() {
            var remainRecord = [], exists = [];
            for (var i = 0, val; val = $scope.MediaBankAssetTypes.Item1[i++];) {
                exists = $.grep(assettypes, function (e) {
                    return e == val.Id;
                });
                if (exists != undefined && exists.length > 0) {
                    remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == val.Id; });
                    if (remainRecord.length == 0) {
                        $scope.selectedAssettypes.push({ "Id": val.Id, "Name": val.damCaption });
                    }
                }
            }
        }

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 1 second for example

        //on keyup, start the countdown
        $scope.startquicksearch = function () {
            $timeout.cancel(typingTimer);
            typingTimer = $timeout(function () { doneTyping();}, doneTypingInterval);
        }

        //on keydown, clear the countdown 
        $scope.clearquicksearchtimer = function () {
            $timeout.cancel(typingTimer);
        }


        //user is "finished typing," do something
        function doneTyping() {
            //do something
            $scope.PageNo = 1;
            if ($scope.MediaAttributeOptions.Attributes.length > 0)
                $timeout(function () { GetFilteredAssetsData(); }, 200);
            else
                LoadAssetsData($scope.FilterStatus, $scope.OrderBy, $scope.PageNo);

        }

        function LoadQuerystring(qrystrng) {
            $("#damui-Assettypes-quicksearch").html("");
            var html = "";

            $scope.MediaAssetTypes["FilterExpandQuerystring"] = false;
            html += ' <div class="sr-dv-filterGroup">';
            html += ' <div class="sr-dv-filterGroupHeader"> ';
            html += '                       <h4 ng-click=\'makeQuerystringexpandcollapse()\'>' + $translate.instant('LanguageContents.Res_1195.Caption') + '</h4> ';
            html += '                       <span class="clearfilter"> ';
            html += '                            <span>|</span> ';
            html += '                            <a href="javascript:void(0);" ng-click="QueryStringQuickSearchClear()">' + $translate.instant('LanguageContents.Res_414.Caption') + '</a> ';
            html += '                        </span> ';
            html += "                        <i ng-click=\'makeQuerystringexpandcollapse()\' ng-class=\'{\"icon-collapse-alt\" :MediaAttributeOptions.FilterExpandQuerystring,\"icon-expand-alt\": !MediaAttributeOptions.FilterExpandQuerystring}\'></i> ";
            html += '</div> ';
            $scope.MediaAssetTypes["FilterQuerystringText"] = qrystrng;
            html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAssetTypes.FilterExpandQuerystring"> ';
            html += ' <span class="searchBy"><input ng-model="MediaAssetTypes.FilterQuerystringText" type="text" placeholder=\"' + $translate.instant('LanguageContents.Res_1194.Caption') + '\" /><button type="button" class="btn btn-small" ng-click="startquicksearch()"><i class="icon-search"></i></button></span>';
            html += '</div>';

            html += " </div>";
            html += ' <hr class="sr-dv-filtergroupSeparator" />';

            $("#damui-Assettypes-quicksearch").html($compile(html)($scope));
        }


        function LoadAssettypes() {

            $scope.MediaAssetTypes.AssetTypes = $scope.MediaBankAssetTypes.Item1;

            $("#damui-Assettypes").html("");
            var html = "";

            $scope.MediaAssetTypes["FilterExpand"] = false;
            html += ' <div class="sr-dv-filterGroup">';
            html += ' <div class="sr-dv-filterGroupHeader"> ';
            html += '                       <h4 ng-click=\'makeAssettypeexpandcollapse()\'>' + $translate.instant('LanguageContents.Res_2507.Caption') + '</h4> ';
            html += '                       <span class="clearfilter"> ';
            html += '                            <span>|</span> ';
            html += '                            <a href="javascript:void(0);" ng-click="MediaTypePerformClear()">' + $translate.instant('LanguageContents.Res_414.Caption') + '</a> ';
            html += '                        </span> ';
            html += "                        <i ng-click=\'makeAssettypeexpandcollapse()\' ng-class=\'{\"icon-collapse-alt\" :MediaAttributeOptions.FilterExpand,\"icon-expand-alt\": !MediaAttributeOptions.FilterExpand}\'></i> ";
            html += '</div> ';
            if ($scope.MediaBankAssetTypes.Item1 != null) {
                if ($scope.MediaBankAssetTypes.Item1.length > 0) {
                    for (var l = 0, type; type = $scope.MediaBankAssetTypes.Item1[l++];) {
                        type["Isselectd"] = false;
                    }
                    $scope.MediaAssetTypes["FilterText"] = "";
                    $scope.MediaAssetTypes["AssetTypeFilterOption"] = $scope.MediaBankAssetTypes.Item1;
                    html += ' <div class="sr-dv-filterGroupItems" ng-show="MediaAssetTypes.FilterExpand"> ';
                    html += '  <input class="searchBy" ng-model="MediaAssetTypes.FilterText" type="text" placeholder="Search by Asset type" /> ';
                    html += '  <ul>';
                    html += ' <li ng-repeat="item in MediaAssetTypes.AssetTypeFilterOption | filter:MediaAssetTypes.FilterText"> ';
                    html += '  <label class="checkbox checkbox-custom "><input  data-ng-model="item.Isselectd" ng-click=\"FilterbyAssettype($event,item.Id,item.damCaption)\" type="checkbox"><i ng-class="MediaTypeoptionClass(item.Id)" class="cutomfiltercheck"></i>{{item.damCaption}}</label>';
                    html += ' </li> ';
                    html += ' </ul>';
                    html += '</div>';
                }
            }
            html += " </div>";
            html += ' <hr class="sr-dv-filtergroupSeparator" />';

            $("#damui-Assettypes").html($compile(html)($scope));

            _autoselectAssetTypes();
        }

        var isfilterRequest = true;
        $scope.assettypefiltercheckbox = false;

        $scope.FilterbyAssettype = function (event, typeid, name) {

            //if (isfilterRequest) {
            $scope.assettypefiltercheckbox = true;
            isfilterRequest = false;


            //$('#mediasloadingDownloadPageModel').modal('show');
            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == typeid; });
                if (remainRecord.length == 0) {
                    $scope.selectedAssettypes.push({ "Id": typeid, "Name": name });
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == typeid; });
                if (remainRecord.length > 0) {
                    $scope.selectedAssettypes.splice($.inArray(remainRecord[0], $scope.selectedAssettypes), 1);
                }
            }
            //$scope.PageNo = 1;
            //$timeout(GetFilteredAssetsData, 200);

            if ($scope.loadingfilter == true) {
                $scope.loadingfilter = false;
                $scope.PageNo = 1;
                //$timeout(GetFilteredAssetsData, 200);

                $timeout.cancel(checkboxtypingTimer);
                checkboxtypingTimer = $timeout(function () { GetFilteredAssetsData(); },checkboxdoneTypingInterval);
            }
            //}
        }
        //---------------Generate XML for the meta
        function GetXML() {
            var xmlData = '<?xml version="1.0" encoding="utf-8"?>';
            xmlData += '<CustomList>';
            //get criteria
            xmlData += "<Criterias>";
            for (var c = 0, where; where = $scope.selectedAttributes[c++];) {
                var values = "";
                if (where.Values.length > 0) {
                    for (var v = 0, data; data = where.Values[v++];) {
                        if (values.length > 0) {
                            values = values + ",'" + data.toString() + "'";
                        } else {
                            values = values + "'" + data.toString() + "'";
                        }
                    }
                }
                xmlData += "<Criteria Condition=\"" + where.Condition + "\" AttributeID=\"" + where.AttributeID + "\" AttributeTypeID=\"" + where.AttributeTypeID + "\" AttributeLevel=\"" + where.AttributeLevel + "\" AttributeCaption=\"" + where.AttributeCaption + "\" Operator=\"" + where.Operator + "\" Value=\"" + values + "\"/>";
            }
            xmlData += "</Criterias>";
            //get criteria condition
            xmlData += '</CustomList>';
            return xmlData;
        }

        // I get more list items and append them to the list.
        function getMoreListItems() {

            $scope.PageNo = $scope.PageNo + 1;

            // returns all the extension types and related DAM types in the system.
            var assetypes = [];
            if ($scope.selectedAssettypes.length > 0)
                assetypes = $scope.selectedAssettypes;
            else
                assetypes = $scope.MediaBankAssetTypes.Item1;
            var criteria = GetXML();
            _getNumberofItemsPerRow();



            $(window).AdjustHeightWidth();
            // var SearchResult = $resource('planning/CallbackSnippetSearch', { get: { method: 'GET' } });
            // var search = new SearchResult();
            var search = {};
            if (searchCredential.ST == undefined || searchCredential.ST == "") {
                search.Text = "";
            }
            else {
                search.Text = searchCredential.ST;
            }
            search.SearchTerm = {};
            search.ETypeID = assetypes;
            search.istag = false;
            search.Searchtype = "Assetlibrary";
            search.PageID = $scope.PageNo;
            search.IsGlobalAdmin = true;
            search.metadata = $scope.selectedAttributes;
            search.isQuerystringdata = false;
            search.rowsperpage = $scope.RowsPerPage;
            search.CurrentScroll = "Assetlibrary";
            var orderobj = $.grep($scope.OrderbyObj, function (e) {
                return e.ID == $scope.OrderBy;
            });
            search.orderbyfieldName = orderobj[0].FieldName;
            search.desc = orderobj[0].Descending;
            PlanningService.CallbackSnippetSearch(search).then(function (searchresponse) {
                //var searchresponse = SearchResult.save(search, function () {
                isfilterRequest = true;
                loading = false;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                var dyn_Contresult = "";
                if ($scope.PageNo == 1) {

                    refreshViewObj();
                }

                if (searchresponse.Response == false) {
                    cleartheSarchHistory();
                    refreshViewObj();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                        refreshViewObj();
                    }
                    else if (searchresponse.Response.SearchResultEntity.length == 0) {
                        cleartheSarchHistory();
                        refreshViewObj();

                    }
                    else {

                        if (searchresponse.Response == false) {
                            cleartheSarchHistory();
                            refreshViewObj();
                        }

                        if (searchresponse.Response.TotalPages != 0) {


                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    for (var j = 0, asset; asset = arr.AssetDetails[j++];) {
                                        var isexist = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (rel) {
                                            return rel.AssetUniqueID == asset.AssetUniqueID;
                                        });
                                        if (isexist.length == 0)
                                            $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                                    }

                                    if ($scope.FilterStatus == 1)
                                        LoadAssetThimbnailView(arr);
                                    else if ($scope.FilterStatus == 2)
                                        LoadSummaryView(arr);
                                    else if ($scope.FilterStatus == 3)
                                        LoadListView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;

                        }

                    }
                }
            });


        }

        // I check to see if more list items are needed, and, if
        // they are, I load them.
        function checkListItemContents() {
            // Check to see if more items need to be loaded.
            // Load new items.
            getMoreListItems(
                function () {
                    // Once the list items have been loaded
                    // re-trigger this method to make sure
                    // that enough were loaded. This will make
                    // sure that there are always enough
                    // default items loaded to allow the
                    // window to scroll.
                    //checkListItemContents(container);
                }
            );

        }
        // -------------------------------------------------- //


        var loading = false;
        // When the DOM is ready, initialize document.
        function loadScrollSettings() {

            $(window).scroll(function () {

                if (loading) {
                    return;
                }

                if (($scope.RowsPerPage * $scope.PageNo) <= $scope.totalRecords) {
                    loading = true;
                    checkListItemContents();
                }
                else {
                    loading = true;
                    $scope.IsAllLaded = true;
                }
            });


        }




        $scope.AssetSelectionFiles = [];


        $scope.AssetChkClass = {};
        $scope.AssetSelectionClass = {};

        $scope.AssetFiles = [];

        var DynamicData = [];

        function refreshViewObj() {

            $(window).scrollTop(0);
            var html = '';
            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-summary").html(html);
            $("#MediaBank-dv-listview").html(html);
            $scope.AssetSelection = {};
            $scope.AssetChkClass = {};
            $scope.AssetSelectionClass = {};
            $scope.AssetFiles = [];
            $scope.AssetDynamicData = [];
        }

        function IsAssetSelected(assetid) {

            var remainRecord = [];

            remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
            if (remainRecord.length > 0) {
                return true;
            }
            return false;

        }


        $scope.AssetChkClass = {};
        $scope.AssetSelection = {};
        $scope.AssetSelectionClass = {};
        $scope.AssetSearchObj = { "AssetSearchfiles": [], "AssetDynamicData": [], "ThumbnailSettings": [] };

        function LoadAssetThimbnailView(res) {

            var html = '';

            $("#MediaBank-dv-summary").html(html);
            $("#MediaBank-dv-listview").html(html);

            if ($scope.PageNo == 1) {
                refreshViewObj();
            }


            if (res != null) {
                var assets = [];
                assets = res.AssetDetails;
                if (assets != null) {
                    for (var a = 0, fil; fil = assets[a++];) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                            return e.AssetUniqueID == fil.AssetUniqueID;
                        });
                        if (data == undefined || data.length == 0)
                            $scope.AssetSearchObj.AssetSearchfiles.push(fil);
                    }
                }

                var DynamicData = res.attrRelation, data;
                if (DynamicData != null) {
                    for (var d = 0, dt; dt = assets[d++];) {
                        data = DynamicData[dt.AssetUniqueID];
                        if (data != null) {
                            var existvalue = [];
                            existvalue = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                                return e.AssetID == dt.AssetUniqueID;
                            });
                            if (existvalue == undefined || existvalue.length == 0)
                                $scope.AssetSearchObj.AssetDynamicData.push({ "AssetID": dt.AssetUniqueID, "Data": data });
                        }
                    }
                }

                for (var i = 0, asset; asset = assets[i++];) {


                    if ($scope.PageNo == 1 && i == 1) {
                        var ht_Count = "hit";
                        if ($scope.totalRecords > 1)
                            ht_Count = "hits";
                        else
                            ht_Count = "hit";
                        $(".mediabanksearchresult #searchPartresultCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                    }


                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }

                    else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;

                    html += '<div id="AssetBlockId_' + asset.AssetUniqueID + '" class=\"th-Box mediabankcontainerbox\" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                    html += '<div ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + i + '" type="checkbox" />';
                    html += '<i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="thumbnailActionMenu"></i>';
                    html += '</div>';
                    html += '<div class="th-Block">';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2)
                                    html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                                else if (asset.Status == 1 || asset.Status == 0)
                                    html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';

                                }
                            }
                            else {
                                html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        }
                        else
                            html += '<img src="' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }

                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $("#MediaBank-dv-thumbnail").append(
                               $compile(html)($scope));

            }
            else {
                $scope.totalRecords = 0;
                $(".mediabanksearchresult #searchPartresultCaption").text("Total 0 hit");
                var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                $("#MediaBank-dv-thumbnail").html(emptyHtml);
            }
        }

        function GenereteAssetDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes.ThumbnailSettings, function (e) {
                return e.assetType == asset.AssetTypeid;
            });

            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {

                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                            return e.AssetID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = {}; attrval = data[0].Data;
                            if (attrval != undefined) {

                                if (attrval[attr.ID] != "" && attrval[attr.ID] != null && attrval[attr.ID] != undefined)
                                    if (attrval[attr.ID] != '1900-01-01')
                                        html += '<span class="' + cls.toString() + '">' + attrval[attr.ID] + '</span>';
                            }
                        }
                    }
                }
            }
            return html;
        }



        $scope.assetlibraryaccess = [];

        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;

            if ($scope.FilterStatus == 1)
                $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            else if ($scope.FilterStatus == 2)
                $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            else if ($scope.FilterStatus == 3)
                $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";

            var res = [];
            res = $.grep($scope.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });

            if (res.length > 0) {
                DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid);
            }


            if (checkbox.checked) {
                var remainRecord = [];

                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length == 0) {
                    if (res[0] != undefined)
                        $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                    else
                        $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length > 0) {
                    $scope.AssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetSelectionFiles), 1);

                }

            }
        }


        $scope.DownloadFileObj = [];
        function DownloadAssets(checkbox, assetid, name, ext, guid) {

            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.push({ AssetId: assetid, FileName: name, Fileguid: guid, Extension: ext });
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.splice($.inArray(remainRecord[0], $scope.DownloadFileObj), 1);
                }
            }

        }

        $scope.download = function () {

            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                //var remainRecord = [];

                var asset = [], data = [], dwnData = [];
                asset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid.AssetId;
                });

                dwnData = $.grep($scope.DownloadFileObj, function (e) {
                    return e.AssetId == assetid.AssetId;
                });
                if (dwnData.length == 0)
                    $scope.DownloadFileObj.push({ AssetId: assetid.AssetId, FileName: asset[0].FileName, Fileguid: asset[0].FileGuid, Extension: asset[0].Extension });
            }

            if ($scope.DownloadFileObj.length == 0) {

                bootbox.alert($translate.instant('LanguageContents.Res_4610.Caption'));
            }

            if ($scope.DownloadFileObj.length == 1) {

                var arr = $scope.DownloadFileObj;
                var ID = 0;
                if ($scope.DownloadFileObj[0].AssetId != undefined) {
                    ID = $scope.DownloadFileObj[0].AssetId;
                }
                $timeout(function () {
                    var fileid = $scope.DownloadFileObj[0].Fileguid, extn = $scope.DownloadFileObj[0].Extension;
                    var filename = $scope.DownloadFileObj[0].FileName;

                    var actualfilename = "";
                    for (var i = 0; i < filename.length; i++) {
                        if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                            actualfilename = actualfilename + (filename[i]);
                        }
                    }

                    blockUIForDownload();
                    location.href = 'DAMDownload.aspx?FileID=' + $scope.DownloadFileObj[0].Fileguid + '&FileFriendlyName=' + actualfilename + '&Ext=' + $scope.DownloadFileObj[0].Extension + '&token=' + $('#mediasdownload_token_value_id').val();
                    refreshDownLoadScope(arr);
                    $scope.DownloadFileObj = [];
                    clearassetSelection();
                }, 100);
            }
            else if ($scope.DownloadFileObj.length > 1) {
                // returns all the extension types and related DAM types in the system.
                var arr = $scope.DownloadFileObj;
                $timeout(function () {
                    DamService.DownloadFiles(arr)
                                .then(function (result) {
                                    if (result.Response == null) {
                                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                                        //$('#mediasloadingDownloadPageModel').modal("hide");
                                    }
                                    else {
                                        $scope.DownloadFileObj = [];
                                        var fileid = result.Response, extn = '.zip';
                                        var filename = result.Response + extn;
                                        blockUIForDownload();
                                        location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#mediasdownload_token_value_id').val();
                                        refreshDownLoadScope(arr);
                                        clearassetSelection();
                                    }
                                });

                }, 100);
            }

        }

        //File download code START
        var fileDownloadCheckTimer;
        function blockUIForDownload() {
            var token = new Date().getTime(); //use the current timestamp as the token value  
            $('#mediasdownload_token_value_id').val(token);
            $('#mediasloadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                var cookieValue = $.cookie('fileDownloadToken');
                if (cookieValue == token)
                    finishDownload();
                else {
                    finishDownload();
                }
            }, 1000);
        }
        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken'); //clears this cookie value  
            $('#mediasloadingDownloadPageModel').modal("hide");
        }
        //File download code end

        function refreshDownLoadScope(arr) {
            for (var i = 0, item; item = arr[i++];) {
                if ($scope.DamViewName == "Thumbnail")
                    $scope.AssetSelectionClass["asset_" + item.AssetId] = "th-Selection";
                else if ($scope.DamViewName == "Summary")
                    $scope.AssetSelectionClass["asset_" + item.AssetId] = "th-sum-Selection";
                else if ($scope.DamViewName == "List")
                    $scope.AssetSelectionClass["asset_" + item.AssetId] = "li-Selection";
                $scope.AssetSelection["asset_" + item.AssetId] = false;
                $scope.AssetChkClass["asset_" + item.AssetId + ""] = "checkbox";

            }
        }

        function GenereteDetailBlock(asset) {

            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {

                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                            return e.AssetID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["Data"]["" + attr.ID + ""] != "" && data[0]["Data"]["" + attr.ID + ""] != null && data[0]["Data"]["" + attr.ID + ""] != undefined)
                                if (data[0]["Data"]["" + attr.ID + ""] != '1900-01-01') {
                                    html += '<span class="' + cls.toString() + '">' + data[0]["Data"]["" + attr.ID + ""] + '</span>';
                                }
                        }

                    }

                }
            }
            return html;

        }

        function LoadSummaryView(res) {

            var html = '';

            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-listview").html(html);

            if ($scope.PageNo == 1) {

                refreshViewObj();
            }

            if (res != null) {

                var assets = [];
                assets = res.AssetDetails;
                if (assets != null) {
                    for (var a = 0, fil; fil = assets[a++];) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                            return e.AssetUniqueID == fil.AssetUniqueID;
                        });
                        if (data == undefined || data.length == 0)
                            $scope.AssetSearchObj.AssetSearchfiles.push(fil);
                    }
                }

                var DynamicData = res.attrRelation, data;
                if (DynamicData != null) {
                    for (var d = 0, dt; dt = assets[d++];) {
                        data = DynamicData[dt.AssetUniqueID];
                        if (data != null) {
                            var existvalue = [];
                            existvalue = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                                return e.AssetID == dt.AssetUniqueID;
                            });
                            if (existvalue == undefined || existvalue.length == 0)
                                $scope.AssetSearchObj.AssetDynamicData.push({ "AssetID": dt.AssetUniqueID, "Data": data });
                        }
                    }
                }

                for (var i = 0, asset; asset = assets[i++];) {
                    if ($scope.PageNo == 1 && i == 1) {
                        var ht_Count = "hit";
                        if ($scope.totalRecords > 1)
                            ht_Count = "hits";
                        else
                            ht_Count = "hit";
                        $(".mediabanksearchresult #searchPartresultCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                    }

                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";

                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-sum-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;

                    html += '<div id="AssetBlockId_' + asset.AssetUniqueID + '" class="th-sum-Box mediabankcontainerbox"> ';
                    html += '        <div id="select_' + i + '" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '   <i class="icon-stop"></i> ';
                    html += '   <label class="checkbox checkbox-custom"> ';
                    html += '   <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + i + '" type="checkbox" /> ';
                    html += '   <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '   </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-Block"> ';
                    html += '            <div class="th-sum-options">';
                    html += '                <i class="icon-stop"></i>';
                    html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="thumbnailActionMenu"></i>';
                    html += '            </div>';
                    html += '   <div class="th-sum-leftSection"> ';
                    html += '   <div class="th-sum-ImgBlock"> ';
                    html += '       <div class="th-sum-ImgContainer"> ';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2)
                                    html += '<img  src="' + TenantFilePath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '' + '.jpg' + '" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                                else if (asset.Status == 1 || asset.Status == 0)
                                    html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';

                                }
                            }
                            else {
                                html += '<img  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        }
                        else {
                            html += '<img src="' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\" onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }

                    }
                    else if (asset.Category == 1) {
                        html += '<img  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 2) {
                        html += '<img  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image" ng-click=\"Editasset(' + asset.AssetUniqueID + ',' + asset.EntityID + ')\"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    html += '       </div> ';
                    html += '   </div> ';
                    html += '   <div class="th-sum-baiscDetailBlock"> ';
                    html += '       <div class="th-sum-ActionButtonContainer"> ';
                    html += '           <span> ';
                    html += '  <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                    html += '  <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';

                    html += '           </span> ';
                    html += '       </div> ';
                    html += '       <div class="th-sum-detail-eIconContainer"> ';
                    html += ' <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '       </div> ';
                    html += '       <div class="th-sum-basicDetails"> ';
                    html += GenereteSummaryDetailBlock(asset);
                    html += '       </div> ';
                    html += '   </div> ';
                    html += '   </div> ';
                    html += '   <div class="th-sum-rightSection"> ';
                    html += '   <div class="th-sum-expandedDetailBlock"> ';
                    html += '       <div class="th-sum-expandedHeader"> ';
                    html += '           <h4>' + asset.AssetName + '</h4> ';
                    html += '       </div> ';
                    html += '       <div class="th-sum-expandedDetails"> ';
                    html += '           <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '           </form> ';
                    html += '       </div> ';
                    html += '   </div> ';
                    html += '   </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';

                }
                $("#MediaBank-dv-summary").append(
                                $compile(html)($scope));

            }
            else {
                if ($scope.PageNo == 1) {
                    $scope.totalRecords = 0;
                    $(".mediabanksearchresult #searchPartresultCaption").text("Total 0 hit");
                    var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                    $("#MediaBank-dv-summary").append(
                               $compile(html)($scope));
                }
            }
        }


        function GenereteSummaryDetailBlock(asset) {

            var html = '';
            if ($scope.SettingsDamAttributes["ThumbnailSettings"].length > 0) {
                var settings = $.grep($scope.SettingsDamAttributes["ThumbnailSettings"], function (e) {
                    return e.assetType == parseInt(asset.AssetTypeid);
                });
                if (settings != undefined && settings, length > 0) {
                    for (var i = 0, attr; attr = settings[i++];) {

                        var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                        if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                                return e.AssetID == asset.AssetUniqueID;
                            });
                            if (data.length > 0) {
                                var attrval = [];
                                if (data[0]["Data"]["" + attr.ID + ""] != "" && data[0]["Data"]["" + attr.ID + ""] != null && data[0]["Data"]["" + attr.ID + ""] != undefined)
                                    if (data[0]["Data"]["" + attr.ID + ""] != '1900-01-01') {
                                        html += '<span class="' + cls.toString() + '">' + data[0]["Data"]["" + attr.ID + ""] + '</span>';
                                    }
                            }

                        }

                    }
                }
            }
            return html;

        }




        function DrawSummaryBlock(asset) {
            var html = '';

            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsDamAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });

            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {


                    html += '<div class="control-group"> ';//recordRow for List controller
                    html += ' <label class="control-label">' + attr.Caption + '</label> ';
                    html += ' <div class="controls"> ';//rytSide for List controller

                    if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                            return e.AssetID == asset.AssetUniqueID;
                        });

                        if (data.length > 0) {
                            var attrval = {}; attrval = data[0].Data;
                            if (attrval != undefined) {

                                if (attrval[attr.ID] != "" && attrval[attr.ID] != null && attrval[attr.ID] != undefined)
                                    if (attrval[attr.ID] != '1900-01-01')
                                        html += '<span class="control-label">' + attrval[attr.ID] + '</span>';
                                    else
                                        html += '<span class="control-label">-</span>';
                            }
                        }
                        else {
                            html += '<label class="control-label">-</label> ';
                        }
                    }
                    else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += ' </div> ';
                    html += '  </div> ';

                }
            }
            return html;
        }

        function LoadListView(res) {
            var html = '';
            $("#MediaBank-dv-thumbnail").html(html);
            $("#MediaBank-dv-summary").html(html);
            if ($scope.PageNo == 1) {
                refreshViewObj();
            }
            if (res != null) {

                var assets = [];
                assets = res.AssetDetails;
                if (assets != null) {
                    for (var a = 0, fil; fil = assets[a++];) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                            return e.AssetUniqueID == fil.AssetUniqueID;
                        });
                        if (data == undefined || data.length == 0)
                            $scope.AssetSearchObj.AssetSearchfiles.push(fil);
                    }
                }

                var DynamicData = res.attrRelation, data;
                if (DynamicData != null) {
                    for (var d = 0, dt; dt = assets[d++];) {
                        data = DynamicData[dt.AssetUniqueID];
                        if (data != null) {
                            var existvalue = [];
                            existvalue = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                                return e.AssetID == dt.AssetUniqueID;
                            });
                            if (existvalue == undefined || existvalue.length == 0)
                                $scope.AssetSearchObj.AssetDynamicData.push({ "AssetID": dt.AssetUniqueID, "Data": data });
                        }
                    }
                }


                if ($scope.PageNo == 1) {
                    html += '<div>';
                    html += '    <table id="MediabankAssetListViewTable" class="table table-normal-left ListviewTable">';
                    html += '        <thead>';
                    html += GenerateHeader();
                    html += '        </thead>';
                    html += '        <tbody>';
                    html += GenerateList(assets);
                    html += '        </tbody>';
                    html += '    </table>';
                    html += '</div>';
                    $("#MediaBank-dv-listview").append($compile(html)($scope));
                } else {
                    html += GenerateList(assets);
                    $("#MediabankAssetListViewTable tbody").append($compile(html)($scope));
                }
            }
            else {
                if ($scope.PageNo == 1) {
                    $scope.totalRecords = 0;
                    $(".mediabanksearchresult #searchPartresultCaption").text("Total 0 hit");
                    var emptyHtml = '<div class="emptyFolder">No result found.</div>';
                    $("#MediaBank-dv-listview").html(emptyHtml);
                }
            }
        }



        function GenerateHeader() {
            var html = '';
            html += '<tr>';
            html += '   <th>';
            html += '       <label class="checkbox checkbox-custom">';
            html += '           <input type="checkbox" />';
            html += '           <i class="checkbox"></i>';
            html += '       </label>';
            html += '   </th>';
            html += '   <th>';
            html += '       <span>Header</span>';
            html += '   </th>';
            if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                    html += '   <th>';
                    html += '       <span>' + attr.Caption + '</span>';
                    html += '   </th>';
                }
            }

            //html += '<th></th>';
            html += '</tr>';
            return html;
        }

        function GenerateList(assets) {
            var html = '';
            for (var j = 0, asset; asset = assets[j++];) {
                if ($scope.PageNo == 1 && i == 1) {
                    var ht_Count = "hit";
                    if ($scope.totalRecords > 1)
                        ht_Count = "hits";
                    else
                        ht_Count = "hit";
                    $(".mediabanksearchresult #searchPartresultCaption").text("Total " + $scope.totalRecords + " " + ht_Count + "");
                }
                $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                var assetselection = "";

                if (IsAssetSelected(asset.AssetUniqueID)) {
                    assetselection = "th-li-Selection selected";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                }
                else {
                    assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
                    $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                }
                $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;


                html += '<tr id="AssetBlockId_' + asset.AssetUniqueID + '" class="mediabankcontainerbox" ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + 'data-backcolor=' + asset.ColorCode + 'data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <label class="checkbox checkbox-custom">';
                html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                html += '           <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                html += '       </label>';
                html += '   </td>';
                html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                html += '       <span class="thmbListviewImgSpan">';
                html += '           <div class="thmbListview-eIconContainer" > ';
                html += '               <span class="thmbListview-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                html += '           </div>';
                html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
                html += '       </span>';
                html += '   </td>';
                if ($scope.SettingsDamAttributes["ListViewSettings"].length > 0) {
                    for (var i = 0, attr; attr = $scope.SettingsDamAttributes["ListViewSettings"][i++];) {
                        html += '<td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                        if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                            var data = [];
                            data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                                return e.AssetID == asset.AssetUniqueID;
                            });

                            if (data.length > 0) {
                                var attrval = {}; attrval = data[0].Data;
                                if (attrval != undefined) {

                                    if (attrval[attr.ID] != "" && attrval[attr.ID] != null && attrval[attr.ID] != undefined)
                                        if (attrval[attr.ID] != '1900-01-01')
                                            html += '<span class="thmbListviewDescSpan">' + attrval[attr.ID] + '</span>';
                                        else
                                            html += '<span class="thmbListviewDescSpan">-</span>';
                                    else
                                        html += '<span class="thmbListviewDescSpan">-</span>';
                                }
                            }
                            else
                                html += '<span class="thmbListviewDescSpan">-</span>';
                        }
                        else
                            html += '<span class="thmbListviewDescSpan">-</span>';
                        html += '</td>';
                    }
                }
                html += '   <td><span class="thmbListview-options"><i id="assetlistoptionsIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="thumbnailActionMenu"></i></span></td>';
                html += '</tr>';
            }
            return html;
        }


        $scope.LoadAssetListViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamService.GetAttributeDetails(assetid)
                  .then(
                      function (attrList) {

                          $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) { return e.ID == attrList.Response.ActiveFileID; })[0];

                          var html = '';
                          html += '       <div class="lv-ImgDetailedPreview">';
                          html += '           <div class="idp-Box"> ';
                          html += '               <div class="idp-Block"> ';
                          html += '                   <div class="idp-leftSection"> ';
                          html += '                       <div class="idp-ImgBlock"> ';
                          html += '                           <div class="idp-ImgDiv"> ';
                          html += '                               <div class="idp-ImgContainer"> ';
                          if (attrList.Response.Category == 0) {
                              if ($scope.ActiveVersion.ProcessType > 0) {
                                  if ($scope.ActiveVersion.Status == 2)
                                      html += '<img src="' + TenantFilePath + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '' + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg\'" />';
                                  else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0)
                                      html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                  else if ($scope.ActiveVersion.Status == 3) {
                                      html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';

                                  }
                              }
                              else {
                                  html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                              }
                          }
                          else if (attrList.Response.Category == 1) {
                              html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                          }
                          else if (attrList.Response.Category == 2) {
                              html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                          }
                          html += '                               </div> ';
                          html += '                           </div> ';
                          html += '                       </div> ';
                          html += '                       <div class="idp-baiscDetailBlock"> ';
                          html += '                           <div class="idp-ActionButtonContainer"> ';
                          html += '                               <span> ';
                          html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
                          html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
                          html += '                               </span> ';
                          html += '                           </div> ';
                          html += '                           <div class="idp-detail-eIconContainer"> ';
                          html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
                          html += '                           </div> ';
                          html += '                       </div> ';
                          html += '                   </div> ';
                          html += '                   <div class="idp-rightSection"> ';
                          html += '                       <div class="idp-expandedDetailBlock"> ';
                          html += '                           <div class="idp-expandedDetails"> ';
                          html += '                               <form class="form-horizontal"> ';
                          try {
                              html += DrawListBlock(attrList);
                          }
                          catch (e) { }
                          html += '                               </form> ';
                          html += '                           </div> ';
                          html += '                       </div> ';
                          html += '                   </div> ';
                          html += '               </div> ';
                          html += '           </div> ';
                          html += '       </div>';
                          $('#' + qtip_id).html(html);
                      });

            }
        };


        function DrawListBlock(attrList) {
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsDamAttributes["ListViewToolTipSettings"].length > 0) {
                    var tooltipattrs = $.grep($scope.SettingsDamAttributes["ListViewToolTipSettings"], function (e) { return e.assetType == attrList.Response.AssetTypeid });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attrvar = $.grep(attrList.Response.AttributeData, function (e) { return e.ID == tooltipattrs[l].ID })[0];
                        if (attrvar != null)
                            $scope.attributedata.push(attrvar);
                    }

                }
                //$scope.attributedata = attrList.Response.AttributeData;
                html += '<div class="control-group recordRow">';
                html += '    <label class="control-label">Asset name</label>';
                html += '    <div class="controls rytSide">';
                html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
                html += '</div></div>';

                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined && $scope.attributedata[i].Caption != null) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                            else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }

                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                                }
                            }
                            else {

                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;

                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        }
                        else {

                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;

                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                            }
                        }
                        else {

                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;

                        if ($scope.attributedata[i].Value == "-") {

                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls rytSide">';
                            html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {

                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";

                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));

                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {

                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;

                                }
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls rytSide">';
                                //+ $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] +
                                html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls rytSide">';
                                html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }

                    }
                }
            }
            return html;
        }

        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                DamService.GetAttributeDetails(assetid)
                  .then(
                      function (attrList) {
                          $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) { return e.ID == attrList.Response.ActiveFileID; })[0];
                          var html = '';
                          html += '     <div class="th-HoverBox">';
                          html += '         <div class="th-HoverBox-Details"> ';
                          //html += '             <div class="th-HoverBox-Header"> ';
                          //html += '               <h5>' + attrList.Response.Name + '</h5>';
                          //html += '             </div> ';
                          html += '             <div class="th-HoverBox-content"> ';
                          html += '                 <form class="form-horizontal"> ';
                          html += DrawThumbnailBlock(attrList);
                          html += '                 </form> ';
                          html += '             </div> ';
                          html += '         </div> ';
                          html += '     </div> ';
                          $('#' + qtip_id).html(html);
                      });
            }
        };


        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.attributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SettingsDamAttributes["ThumbnailToolTipSettings"].length > 0) {
                    tooltipattrs = $.grep($scope.SettingsDamAttributes["ThumbnailToolTipSettings"], function (e) { return e.assetType == attrList.Response.AssetTypeid });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var attrvar = $.grep(attrList.Response.AttributeData, function (e) { return e.ID == tooltipattrs[l].ID })[0];
                        if (attrvar != null)
                            $scope.attributedata.push(attrvar);

                    }

                }
                //$scope.attributedata = attrList.Response.AttributeData;
                html += '<div class=\"control-group\"><label class=\"control-label\">Asset name</label>';
                html += '<div class=\"controls\">';
                html += '<label class=\"control-label\">' + attrList.Response.Name + '</label>';
                html += '</div></div>';

                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div  class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
                        }
                        html += '<div class=\"control-group\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        html += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] + '</label></div></div>';
                    }
                    else if ($scope.attributedata[i].TypeID == 3) {
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.attributedata[i].Caption[0] + '</label></div></div>';
                            }
                            else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls"><span>' + $scope.attributedata[i].Lable + '</span>';
                                html += '</div></div>';
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.attributedata[i].Caption[0] + '</label></div></div>';
                                }
                            }
                            else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 4) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;

                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                            }
                        }
                        else {

                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</label></div></div>';
                        }
                    }

                    else if ($scope.attributedata[i].TypeID == 17) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;

                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                            }
                        }
                        else {

                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</label></div></div>';
                        }
                    }

                    else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;

                        if ($scope.attributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
                            html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                            html += '<div class="controls">';
                            //+ $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] +
                            html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
                            html += '</label>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            html += '<div class="controls">';
                            html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</label>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {

                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";

                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));

                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.attributedata[i].Value[j].Description == undefined) {

                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;

                                }
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                html += '<div class="controls">';
                                //+ $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] +
                                html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
                                html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</label>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                html += '<div class="controls">';
                                html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</label>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }

                    }
                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];

                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];

                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = { usersID: '' };

            $scope.UserId = $cookies['UserId'];
        }


        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }

        $scope.Editasset = function (AssetID, entityId) {
            $scope.IsLockMediaEntity = false;
            //var GetLockStatus = $resource('planning/GetLockStatus/:EntityID', { EntityID: entityId }, { get: { method: 'GET' } });
            // var entitylockstatus = GetLockStatus.get({ EntityID: entityId }, function () {
            PlanningService.GetLockStatus(entityId).then(function (entitylockstatus) {
                $scope.IsLockMediaEntity = entitylockstatus.Response.Item2;
                $timeout(function () {
                    $scope.$emit('callBackAssetEditinMUI', AssetID, $scope.IsLockMediaEntity, true, 'ViewType');
                }, 100);
            });
        }

        $scope.AddFilesFromDAM = function () {
            var count = $scope.AssetSelectionFiles.length;
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr.push($scope.AssetSelectionFiles[i].AssetId);
            }
            var g = arr.length;
            if (g == 0) {

                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;
            }
            else {
                var data = {};
                data.AssetArr = arr;
                data.EntityID = $scope.MediaBankSettings.EntityId;
                data.FolderID = $scope.MediaBankSettings.FolderId;
                DamService.AddPublishedFilesToDAM(data)
                                .then(function (result) {
                                    if (result.Response == 0) {
                                        NotifyError($translate.instant('LanguageContents.Res_4262.Caption'));
                                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                                            $location.path('/mui/planningtool/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else {
                                            $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                    }
                                    else {
                                        NotifySuccess(count + " " + "File(s) Created Successfully ");
                                        $scope.MediaBankSettings.Redirectfromdam = false;
                                        $scope.MediaBankSettings.FolderId = '';
                                        $scope.MediaBankSettings.directfromMediaBank = true;
                                        //redirect to the dam page now
                                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                                            $location.path('/mui/planningtool/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else {
                                            $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                    }

                                });
            }

        }

        $scope.AddLinkFilesFromDAM = function () {
            var count = $scope.AssetSelectionFiles.length;
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr.push($scope.AssetSelectionFiles[i].AssetId);
            }
            var l = arr.length;
            if (l == 0) {

                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                return false;

            }
            else {

                DamService.AddPublishedLinkFiles(arr, $scope.MediaBankSettings.EntityId, $scope.MediaBankSettings.FolderId)
                                .then(function (result) {


                                    if (result.Response == 0) {
                                        NotifyError($translate.instant('LanguageContents.Res_4262.Caption'));
                                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                                            $location.path('/mui/planningtool/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else {
                                            $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                    }

                                    else {
                                        NotifySuccess(count + " " + "Files(s) created successfully");
                                        $scope.MediaBankSettings.Redirectfromdam = false;
                                        $scope.MediaBankSettings.FolderId = '';
                                        $scope.MediaBankSettings.directfromMediaBank = true;
                                        //redirect to the dam page now
                                        if ($scope.MediaBankSettings.ProductionTypeID == 5) {
                                            $location.path('/mui/planningtool/costcentre/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else if ($scope.MediaBankSettings.ProductionTypeID == 10) {
                                            $location.path('/mui/planningtool/objective/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                        else {
                                            $location.path('/mui/planningtool/default/detail/section/' + $scope.MediaBankSettings.EntityId + '/attachment');
                                        }
                                    }

                                }
                                );
            }

        }

        $scope.Addtolightbox = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];

                    var asset = [], data = [];
                    asset = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                        return e.AssetUniqueID == assetid.AssetId;
                    });
                    data = $.grep($scope.AssetDynamicData, function (e) {
                        return e.ID == assetid.AssetId;
                    });
                    if (asset[0].FileGuid != null)
                        if (asset.length > 0)
                            $scope.damlightbox.lightboxselection.push({ "AssetRawData": asset[0], "EntityID": asset[0].EntityID, "ShortDesc": asset[0].ShortDescription, "ColorCode": asset[0].ColorCode, "AssetId": assetid.AssetId, "AssetName": asset[0].Name, "Description": asset[0].Description != null ? asset[0].Description : "-", "Guid": asset[0].FileGuid, "Ext": asset[0].Extension, "AssetStatus": asset[0].Status, "FileSize": parseSize(asset[0].Size), "DynamicData": data, "MimeType": asset[0].MimeType, "Extension": asset[0].Extension.substring(1, asset[0].Extension.length).toLowerCase(), "Category": asset[0].Category, "Iscropped": false });
                    //$('#LightBoxIcon_' + assetid.AssetId).removeClass('displayNone');
                }

                $timeout(function () { $scope.AssetSelectionFiles = []; }, 200);
                NotifySuccess(" " + $scope.AssetSelectionFiles.length + $translate.instant('LanguageContents.Res_5811.Caption'));
                $scope.DownloadFileObj.DownloadFilesArr = [];
                //$scope.AssetFilesObj.AssetTaskObj = [];
                clearassetSelection();
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }

        }

        $scope.DownloadWithMetadata = function (result) {
            if ($scope.AssetSelectionFiles.length > 0) {
                var arr = [];
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    arr[i] = $scope.AssetSelectionFiles[i].AssetId;
                }
                var include = result;
                DamService.DownloadAssetWithMetadata(arr, result)
                .then(function (result) {
                    if (result.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                        // $('#loadingDownloadPageModel').modal("hide");
                    }
                    else {
                        if (arr.length == 1) {
                            if (include == false) {
                                var fileid = result.Response[1], extn = '.xml';
                                if (result.Response[0] == null || result.Response[0] == "")
                                    var filename = "MediabankAssets" + extn;
                                else
                                    var filename = result.Response[0];
                            }
                            else {
                                var fileid = result.Response[1], extn = '.zip';
                                var filename = fileid + extn;
                            }
                        }
                        else {
                            var fileid = result.Response[1], extn = '.zip';
                            var filename = fileid + extn;
                        }
                        //clearassetSelection();
                        blockUIForDownload();
                        location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                        //refreshDownLoadScope($scope.AssetSelectionFiles);
                        $timeout(function () { clearassetSelection(); }, 100);

                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }

        function clearassetSelection() {
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                $('#chkBoxIcon_' + $scope.AssetSelectionFiles[i].AssetId).attr("class", "checkbox");
                if ($scope.DamViewName == "Thumbnail")
                    $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "th-Selection";
                else if ($scope.DamViewName == "Summary")
                    $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "th-sum-Selection";
                else if ($scope.DamViewName == "List")
                    $scope.AssetSelectionClass["asset_" + $scope.AssetSelectionFiles[i].AssetId + ""] = "li-Selection";
            }
            $scope.AssetSelectionFiles.splice(0, $scope.AssetSelectionFiles.length);
        }

        $scope.PerformAssetOperation = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];

                    $scope.damclipboard.assetselection.push({ "AssetId": assetid.AssetId });
                }
                $timeout(function () { $scope.AssetSelectionFiles = []; }, 200);
                NotifySuccess($translate.instant('LanguageContents.Res_1139.Caption'));
                $scope.DownloadFileObj.DownloadFilesArr = [];
                //$scope.AssetFilesObj.AssetTaskObj = [];
                clearassetSelection();
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.sendMetadatMailSubject = "";
        //$scope.mediabank.mailtags = [];

        $scope.openSendAssetMail = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                $scope.sendMetadatMailSubject = "";
                $scope.mediabank.mailtags = [];
                $('#sendMediabankMailPopup').modal("show");
                $scope.includemdata = false;
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked)
                $scope.includemdata = true;
            else
                $scope.includemdata = false;

        }

        $scope.sendmailOptions = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }

        $scope.SendAssetWitMwtadata = function () {

            $scope.MailprocessMailFlag = true;

            var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var assetSelection = [], toArray = new Array(), mailSubject = $scope.sendMetadatMailSubject != "" ? $scope.sendMetadatMailSubject : "Marcom Media Bank";

            for (var i = 0, mail; mail = $scope.mediabank.mailtags[i++];) {
                if (mail.text.match(emailvalid)) {
                    toArray.push(mail.text);
                    //toArray.push($scope.mediabank.mailtags);
                }
                else {
                    $scope.MailprocessMailFlag = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_1137.Caption'));
                    //$scope.sendMetadatMailSubject = "";
                    //$scope.mediabank.mailtags = [];
                    //$scope.includemdata = false;
                    return false;
                }
            }
            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                var remainRecord = [];

                assetSelection.push({ "AssetId": assetid.AssetId });
            }
            //assetSelection.push({ "AssetId": $scope.AssetEditID });
            if ($scope.AssetSelectionFiles.length > 0 && toArray.length > 0) {

                var srcObj = {};
                srcObj.Toaddress = toArray.join(",");
                srcObj.subject = mailSubject;
                srcObj.AssetArr = assetSelection;
                srcObj.metadata = $scope.includemdata;
                $timeout(function () {
                    DamService.SendMailWithMetaData(srcObj)
                                .then(function (result) {
                                    if (result.Response == null) {
                                        //$('#sendMail').removeClass('disabled');
                                        //$scope.IsMailSender = false;
                                        $scope.sendMetadatMailSubject = "";
                                        $scope.MailprocessMailFlag = false;
                                        $scope.mediabank.mailtags = [];
                                        NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                                    }
                                    else {
                                        //$('#sendMail').removeClass('disabled');
                                        $scope.sendMetadatMailSubject = "";
                                        $scope.MailprocessMailFlag = false;
                                        $scope.mediabank.mailtags = [];
                                        //$scope.IsMailSender = false;
                                        NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                        $('#sendMediabankMailPopup').modal("hide");
                                        clearassetSelection();
                                    }
                                });

                }, 100);
            }

            else {
                $scope.MailprocessMailFlag = false;
                //$('#sendMail').removeClass('disabled');
                //$scope.IsMailSender = false;
                bootbox.alert($translate.instant('LanguageContents.Res_1138.Caption'));
            }
            //$('#sendAssetMailPopup').modal("hide");
            //}


        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel(); $timeout.cancel(typingTimer);
            $timeout.cancel(PreviewGeneratorTimer);
            RecursiveUnbindAndRemove($("[ng-controller='mui.muimediabank1Ctrl']"));
        });

        $rootScope.currentPathName = $location.path().replace('/', '');
        if ($rootScope.currentPathName === "mediabank") {
            $scope.Copy = false;
            $scope.div = true;
            $scope.div2 = false;
            $scope.div3 = false;
            $scope.showCopyandLink = true;
            $scope.IsMarcomMediabank = false;

        }
        else {
            $scope.Copy = true;
            $scope.div = true;
            $scope.div2 = true;
            $scope.div3 = true;
            $scope.showCopyandLink = false;
            $scope.IsMarcomMediabank = true;
        }

        $scope.SetAssetActionId = function (assetid) {

            $('#chkBoxIcon_' + assetid + '').addClass('checked');
            if ($scope.AssetSelectionFiles.length > 0) {
                var asset = [], data = [];
                asset = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e.AssetId == assetid;
                });
                if (asset.length == 0) {
                    $scope.AssetSelectionFiles.push({ "AssetId": assetid });

                    if ($scope.FilterStatus == 1)
                        $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                    else if ($scope.FilterStatus == 2)
                        $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                    else if ($scope.FilterStatus == 3)
                        $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";

                    $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
                }
            }
            else {
                $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                $scope.AssetSelectionFiles.selectedAssetId = assetid;
                $scope.CustomSelect = assetid;

                if ($scope.FilterStatus == 1)
                    $scope.AssetSelectionClass["asset_" + assetid] = "th-Selection selected";
                else if ($scope.FilterStatus == 2)
                    $scope.AssetSelectionClass["asset_" + assetid] = "th-sum-Selection selected";
                else if ($scope.FilterStatus == 3)
                    $scope.AssetSelectionClass["asset_" + assetid] = "li-Selection selected";

                $scope.AssetChkClass["asset_" + assetid + ""] = "checkbox checked";
            }
        }
        $scope.UnPublishAssets = function () {
            var arr = [];
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    arr[i] = $scope.AssetSelectionFiles[i].AssetId;
                }
                DamService.UnPublishAssets(arr)
                .then(function (result) {
                    if (result.Response == true) {
                        if (arr.length > 1) {
                            NotifySuccess("" + arr.length + "" + $translate.instant('LanguageContents.Res_5809.Caption'));
                        }
                        else {
                            NotifySuccess("1"+ $translate.instant('LanguageContents.Res_5753.Caption'));
                        }
                        for (var j = 0; j < arr.length; j++) {
                            $('#AssetBlockId_' + arr[j]).addClass('displayNone');

                            for (var i = 0; i < $scope.AssetFilesObj_Temp.AssetFiles.length; i++) {

                                var temparr = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                                    return e.IsPublish == true && e.AssetUniqueID == arr[j];
                                });
                                if (temparr.length > 0) {
                                    $scope.AssetFilesObj_Temp.AssetFiles[i].IsPublish = false;
                                }
                            }
                        }
                        clearassetSelection();
                    }
                    else {
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }

        $scope.viewlightbox = function () {
            $scope.$emit('gotoviewlightbox');
            clearassetSelection();
        }

        $scope.Reformatprocess = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                var reformate = [], temparr = [];
                for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                    temparr = $.grep($scope.AssetFilesObj_Temp.AssetFiles, function (e) {
                        return e.Category == 0 && e.ProcessType == 1 && e.Status == 2 && e.Extension != ".pdf" && e.AssetUniqueID == $scope.AssetSelectionFiles[i].AssetId

                    });
                    if (temparr != null) {
                        if (temparr.length > 0) {
                            reformate.push(temparr[0]);
                        }
                    }
                }
                if ($scope.AssetSelectionFiles.length > 0 && reformate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1165.Caption'));
                    refreshTaskAssetSelection();
                    return false;
                }
                else if ($scope.AssetSelectionFiles.length > 0 && reformate.length > 1) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1166.Caption'));
                    refreshTaskAssetSelection();
                    return false;
                }
                else if ($scope.AssetSelectionFiles.length > 0 && reformate.length == 1) {
                    $timeout(function () {
                        var folderid;
                        var EntityID;

                        folderid = reformate[0].FolderID;
                        EntityID = reformate[0].EntityID;

                        //$scope.$emit('ReformatInitiatorDam', reformate[0].FileGuid, reformate[0].AssetUniqueID, folderid, EntityID)

                        /********** Load assetreformat page **********/

                        var modalInstance = $modal.open({
                            templateUrl: 'views/mui/assetreformat.html',
                            controller: "mui.muiassetreformatCtrl",
                            resolve: {
                                params: function () {
                                    return { thubmnailGuid: reformate[0].FileGuid, assetid: reformate[0].AssetUniqueID, folderid: folderid, EntityID: EntityID };
                                }
                            },
                            scope: $scope,
                            windowClass: 'reformatAssetPopup popup-widthXL',
                            backdrop: "static"
                        });

                        modalInstance.result.then(function (selectedItem) {
                            $scope.selected = selectedItem;
                        }, function () {
                            //console.log('Modal dismissed at: ' + new Date());
                        });
                    }, 100);

                    clearassetSelection();
                }
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }


    }
    app.controller("mui.muimediabank1Ctrl", ['$window', '$location', '$route', '$timeout', '$scope', '$cookies', '$resource', 'requestContext', '$routeParams', '$rootScope', '$translate', 'DamService', 'PlanningService', '_', '$compile', muimediabank1Ctrl]);
})(angular, app);