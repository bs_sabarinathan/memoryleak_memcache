﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICostCenter interface for table 'PM_CostCenter'.
    /// </summary>
    public interface ICostCentreData : IBaseEntity
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        decimal? AssignedAmount
        {
            get;
            set;

        }

        IList<IAttributeData> EntityAttributeData
        {
            get;
            set;
        }
        IList<IEntityRoleUser> EntityMembers
        {
            get;
            set;
        }


        #endregion
    }
}
