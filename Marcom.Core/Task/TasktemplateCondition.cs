﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class TasktemplateCondition : ITasktemplateCondition
    {
        #region Member Variables

        protected int _id;
        protected int _TaskTempID;
        protected int _TypeID;
        protected int _AttributeID;
        protected int _AttributeTypeID;
        protected int _AttributeLevel;
        protected string _Value;
        protected int _ConditionType;
        protected int _Sortorder;
        #endregion

        #region Constructors
        public TasktemplateCondition() { }

        public TasktemplateCondition(int pTaskTempID, int pTypeID, int pAttributeID, int pAttributeTypeID, int pAttributeLevel, string pValue, int pConditionType, int pSortorder)
        {
            this._TaskTempID = pTaskTempID;
            this._TypeID = pTypeID;
            this._AttributeID = pAttributeID;
            this._AttributeTypeID = pAttributeTypeID;
            this._AttributeLevel = pAttributeLevel;
            this._Value = pValue;
            this._ConditionType = pConditionType;
            this._Sortorder = pSortorder;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }


        public int TaskTempID
        {
            get { return _TaskTempID; }
            set { _TaskTempID = value; }

        }

        public int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }

        }

        public int AttributeID
        {
            get { return _AttributeID; }
            set { _AttributeID = value; }

        }

        public int AttributeTypeID
        {
            get { return _AttributeTypeID; }
            set { _AttributeTypeID = value; }

        }

        public int AttributeLevel
        {
            get { return _AttributeLevel; }
            set { _AttributeLevel = value; }

        }

        public string Value
        {
            get { return _Value; }
            set { _Value = value; }

        }


        public int SortOder
        {
            get { return _Sortorder; }
            set { _Sortorder = value; }

        }

        public int ConditionType
        {
            get { return _ConditionType; }
            set { _ConditionType = value; }

        }


       


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TasktemplateCondition castObj = null;
            try
            {
                castObj = (TasktemplateCondition)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


