﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Dal.Common.Model;

namespace BrandSystems.Marcom.Core.Common
{
    public class FinancialView
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public string Template { get; set; }
        public bool IsDefault { get; set; }
        public List<FinancialViewColumnsDao> viewColumns { get; set; }

    }
}
