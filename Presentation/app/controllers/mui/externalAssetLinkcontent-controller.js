﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtoolCtrl",
    function externalAssetLinkcontentCtrl($scope, DamService, $timeout, $location, requestContext, _) {
        // --- Define Controller Methods. ------------------- //
        // ...

        // --- Define Scope Variables. ---------------------- //

        $scope.ShowHideContentPage = false;
        $scope.ShowAssetSearchDiv = false;
        $scope.htmlcontent = "";
        $scope.Customtoolbar = { header: { title: 'Asset Library', callback: function (obj) { BindAssetSearchDiv(); } } };


        function BindAssetSearchDiv() {
            var divhtml = "<div><input id='AssetLibrarySearch'></input></div>";
            $("#htmlAssetText").prev().append(divhtml);
        };

        // --- Define Scope Methods. ------------------------ //

        //$timeout(function () {
        //    angular.element(document).ready(function () {
        //        $('#htmlAssetText').redactor({
        //            focus: true,
        //            buttonsAdd: ['|', 'token'],
        //            buttonsCustom: {
        //                token: {
        //                    title: 'Asset Library',
        //                    dropdown: $scope.Customtoolbar
        //                }
        //            }
        //        });
        //    });
        //}, 200);


      
        //$scope.EditContent = function () {
        //    DamService.GetExternalAssetContent().then(function (res) {
        //        if (res.Response != null) {
        //            $scope.htmlcontent = res.Response.toString();
        //            $('#htmlAssetText').val(res.Response.toString());

        //            $('#htmlAssetText').redactor({
        //                focus: true,
        //                buttonsAdd: ['|', 'token'],
        //                buttonsCustom: {
        //                    token: {
        //                        title: 'Asset Library',
        //                        dropdown: {
        //                            header: {
        //                                title: 'Entête', callback: function (obj) {
        //                                    var html = "<pre class='brush:'></pre>";
        //                                    obj.execCommand('inserthtml', html);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            });
        //        }
        //    });
        //    $timeout(function () {
        //        $scope.ShowHideContentPage = true;
        //    }, 100);
        //}


        $scope.EditContent = function () {
            DamService.GetExternalAssetContent().then(function (res) {
                if (res.Response != null) {
                    $scope.htmlcontent = res.Response.toString();
                    $('#htmlAssetText').val(res.Response.toString());

                    $('#htmlAssetText').redactor({
                        focus: true,
                        buttonsAdd: ['|', 'token'],
                        buttonsCustom: {
                            token: {
                                title: 'Asset Library',
                                dropdown: $scope.Customtoolbar
                            }
                        }
                    });
                }
            });
            $timeout(function () {
                $scope.ShowHideContentPage = true;
            }, 100);
        }

        $scope.LoadAssetContent = function () {
            DamService.GetExternalAssetContent().then(function (res) {
                if (res.Response != null) {
                    $scope.htmlcontent = res.Response.toString();
                    $('#LoadLibraryContent').html(res.Response.toString());
                    $timeout(function () { $scope.BindEventForSearchDiv() }, 10);
                }
            });
        }
        $scope.LoadAssetContent();

        $scope.SaveAssetContent = function () {
            var htmlcontent = $("#htmlAssetText").prev().html().trim();
            DamService.InsertUpdateExternalAssetContent(htmlcontent).then(function (data) {
                if (data.Response == true && data.StatusCode == 200) {
                    $scope.LoadAssetContent();
                    $timeout(function () {
                        $scope.ShowHideContentPage = false;
                    }, 100);
                    NotifySuccess($scope.LanguageContents.Res_4717);
                }
                else {
                    $timeout(function () {
                        $scope.ShowHideContentPage = false;
                    }, 100);
                    NotifyError($scope.LanguageContents.Res_4329);
                }
            });
        };

        $scope.BindEventForSearchDiv = function () {
            $("#AssetLibrarySearch").keyup(function (event) {
                $timeout(function () {
                    $location.path('/mediabank');
                }, 100);
                event.preventDefault();
            });
        }

        // ...

        // --- Define Controller Variables. ----------------- //
        // Get the render context local to this controller (and relevant params).
        var renderContext = requestContext.getRenderContext("mui.externalAssetLinkcontent");


        // The subview indicates which view is going to be rendered on the page.
        $scope.subview = renderContext.getNextSection();

        // --- Bind To Scope Events. ------------------------ //
        // I handle changes to the request context.
        $scope.$on(
            "requestContextChanged",
            function () {
                // Make sure this change is relevant to this controller.
                if (!renderContext.isChangeRelevant()) {
                    return;
                }
                // Update the view that is being rendered.
                $scope.subview = renderContext.getNextSection();
            }
        );

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.externalAssetLinkcontentCtrl']"));
        });
        // --- Initialize. ---------------------------------- //
        // ...
    }
    //);
    app.controller('mui.externalAssetLinkcontentCtrl', ['$scope', 'DamService', '$timeout', '$location', 'requestContext', '_', externalAssetLinkcontentCtrl]);
})(angular, app);