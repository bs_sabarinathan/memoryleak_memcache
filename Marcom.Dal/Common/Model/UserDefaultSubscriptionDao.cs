using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserDefaultSubscription object for table 'CM_UserDefaultSubscription'.
	/// </summary>
	
	public partial class UserDefaultSubscriptionDao : BaseDao, ICloneable 
	{
		#region Member Variables
        protected int _id;
		protected int _userid;
		protected string _subscriptiontypeid;
        protected string _mailsubscriptiontypeid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    UserDefaultSubscriptionDao toCompare = obj as UserDefaultSubscriptionDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Userid, toCompare.Userid))
        //        return false;
        //    if (!Object.Equals(this.SubscriptionTypeid, toCompare.SubscriptionTypeid))
        //        return false;

        //    return true;
        //}

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + UserID.GetHashCode();
            hashCode = (hashCode * 7) + SubscriptionTypeID.GetHashCode();
            hashCode = (hashCode * 7) + MailSubscriptionTypeID.GetHashCode();
            return hashCode;
        }

        #endregion
		
		#region Constructors
		public UserDefaultSubscriptionDao() {}

        public UserDefaultSubscriptionDao(int pUserid, string pSubscriptionTypeid, string pMailSubscriptionTypeid)
		{
			this._userid = pUserid; 
			this._subscriptiontypeid = pSubscriptionTypeid;
            this._mailsubscriptiontypeid = pMailSubscriptionTypeid; 
		}
        public UserDefaultSubscriptionDao(int pId)
		{
			this._id = pId; 
		}
		#endregion
		
		#region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
		
        public virtual int UserID
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}

        public virtual string SubscriptionTypeID
		{
			get { return _subscriptiontypeid; }
			set { _bIsChanged |= (_subscriptiontypeid != value); _subscriptiontypeid = value; }
			
		}
        public virtual string MailSubscriptionTypeID
        {
            get { return _mailsubscriptiontypeid; }
            set { _bIsChanged |= (_mailsubscriptiontypeid != value); _mailsubscriptiontypeid = value; }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string UserDefaultSubscriptionDao = "UserDefaultSubscriptionDao";
            public const string Id = "Id";			
            public const string UserID = "UserID";
            public const string SubscriptionTypeID = "SubscriptionTypeID";
            public const string MailSubscriptionTypeID = "MailSubscriptionTypeID";			

		}

		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string UserDefaultSubscriptionDao = "UserDefaultSubscriptionDao";
            public const string Id = "Id";			
            public const string UserID = "UserID";
            public const string SubscriptionTypeID = "SubscriptionTypeID";
            public const string MailSubscriptionTypeID = "MailSubscriptionTypeID";			
		
		}

		#endregion
		
		
	}
	
}
