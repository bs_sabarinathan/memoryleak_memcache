﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class MediaGeneratorAssetDao : BaseDao, ICloneable
    {
        protected int _id;
        protected int _assetid;
        protected int _docversionid;
        protected string _highrespdf;
        protected string _lowrespdf;
        protected string _jpeg;
        protected int _version;
        protected int _docid;
        protected bool _approved;
        protected int _templateType;

        public MediaGeneratorAssetDao() { }

        public MediaGeneratorAssetDao(int pAssetId, int pDocVersionId, string pHighResPdf, string pLowResPdf, string pJpeg, int pVersion, int pDocId, bool pApproved, int pTemplateType)
        {
            this._assetid = pAssetId;
            this._docversionid = pDocVersionId;
            this._highrespdf = pHighResPdf;
            this._lowrespdf = pLowResPdf;
            this._jpeg = pJpeg;
            this._version = pVersion;
            this._docid = pDocId;
            this._approved = pApproved;
            this._templateType = pTemplateType;
        }

        public MediaGeneratorAssetDao(int pId)
        {
            this._id = pId;
        }


        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int AssetID
        {
            get { return _assetid; }
            set { _assetid = value; }

        }

        public virtual int DocVersionID
        {
            get { return _docversionid; }
            set { _docversionid = value; }

        }

        public virtual string HighResPdf
        {
            get { return _highrespdf; }
            set { _highrespdf = value; }

        }

        public virtual string LowResPdf
        {
            get { return _lowrespdf; }
            set { _lowrespdf = value; }

        }

        public virtual string Jpeg
        {
            get { return _jpeg; }
            set { _jpeg = value; }

        }

        public virtual int Version
        {
            get { return _version; }
            set { _version = value; }

        }

        public virtual int DocID
        {
            get { return _docid; }
            set { _docid = value; }

        }
        public virtual bool Approved
        {
            get { return _approved; }
            set { _approved = value; }

        }
        public virtual int TemplateType
        {
            get { return _templateType; }
            set { _templateType = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string MediaGeneratorAssetDao = "MediaGeneratorAssetDao";
            public const string ID = "ID";
            public const string AssetID = "AssetID";
            public const string DocVersionID = "DocVersionID";
            public const string HighResPdf = "HighResPdf";
            public const string LowResPdf = "LowResPdf";
            public const string Jpeg = "Jpeg";
            public const string Version = "Version";
            public const string DocID = "DocID";
            public const string Approved = "Approved";
            public const string TemplateType = "TemplateType";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string MediaGeneratorAssetDao = "MediaGeneratorAssetDao";
            public const string ID = "ID";
            public const string AssetID = "AssetID";
            public const string DocVersionID = "DocVersionID";
            public const string HighResPdf = "HighResPdf";
            public const string LowResPdf = "LowResPdf";
            public const string Jpeg = "Jpeg";
            public const string Version = "Version";
            public const string DocID = "DocID";
            public const string Approved = "Approved";
            public const string TemplateType = "TemplateType";
        }

        #endregion
    }
}
