﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Core.Metadata.Interface
    {
    public interface IFinancialAttribute
        {

        /// <summary>
        /// IFinancialAttribute interface for table '[MM_Fin_Attribute]'.
        /// </summary>
        #region Public Properties

        int ID { get; set; }
        int FinTypeID { get; set; }
        string Caption { get; set; }
        int AttributeTypeID { get; set; }
        bool IsSystemDefined { get; set; }
        string Description { get; set; }
        bool IsColumn { get; set; }
        bool IsTooltip { get; set; }
        bool IsCommitTooltip { get; set; }
        int SortOrder { get; set; }
        bool _IsHelptextEnabled { get; set; }
        string _HelptextDecsription { get; set; }

        #endregion
        }
    }

