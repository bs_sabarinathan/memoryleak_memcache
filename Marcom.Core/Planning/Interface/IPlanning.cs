﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IPurchaseOrder interface for table 'PM_PurchaseOrder'.
    /// </summary>
    public interface IPlanning
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        int CostCentreID
        {
            get;
            set;

        }

        DateTime PlanDate
        {
            get;
            set;

        }

        decimal PlanAmount
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }


        int CurrencyType
        {
            get;
            set;

        }


        int? Status
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }

        bool IsActive
        {
            get;
            set;

        }

        string CCName { get; set; }
        string currencytypeName { get; set; }
        string strPlanDate { get; set; }
        string strAmount { get; set; }
        string strStatusName { get; set; }

        #endregion
    }
}
