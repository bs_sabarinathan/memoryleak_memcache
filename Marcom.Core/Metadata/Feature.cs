/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// Feature object for table 'MM_Feature'.
	/// </summary>
	
	internal class Feature : IFeature 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _description;
        protected int _moduleid;
        protected bool _isenable;
        protected bool _istopnavigation;
		
		#endregion
		
		#region Constructors
		public Feature() {}

        public Feature(string pCaption, string pDescription, int pModuleID, bool pIsEnable, bool pIsTopNavigation)
		{
			this._caption = pCaption; 
			this._description = pDescription;
            this._moduleid = pModuleID;
            this._isenable = pIsEnable;
            this._istopnavigation = pIsTopNavigation;
		}
				
		public Feature(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set {  _moduleid = value; }

        }

        public virtual bool IsEnable
        {
            get { return _isenable; }
            set {  _isenable = value; }

        }
        public virtual bool IsTopNavigation
        {
            get { return _istopnavigation; }
            set { _istopnavigation = value; }

        }
                 

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Feature castObj = null;
			try
			{
				castObj = (Feature)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
