﻿using System;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CostCenterDao object for table 'PM_CostCenter'.
	/// </summary>

    public partial class EntityCostReleations : IEntityCostReleations
	{
		#region Member Variables

		protected int _id;
		protected int _entityid;
        protected int _costcenterid;
		protected int _sortorder;
        protected bool _isassociate;
        protected bool _isactive;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EntityCostReleations() {}

        public EntityCostReleations(int pId, int pEntityId, int pCostcenterId, int pSortorder, bool pIsassociate, bool pIsactive)
		{
			this._id = pId;
            this._entityid = pEntityId;
            this._costcenterid = pCostcenterId;
            this._sortorder = pSortorder;
            this._isassociate = pIsassociate;
            this._isactive = pIsactive;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual int CostcenterId
        {
            get { return _costcenterid; }
            set { _bIsChanged |= (_costcenterid != value); _costcenterid = value; }

        }

        public virtual int Sortorder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }

        public virtual bool Isassociate
        {
            get { return _isassociate; }
            set { _bIsChanged |= (_isassociate != value); _isassociate = value; }

        }

        public virtual bool Isactive
        {
            get { return _isactive; }
            set { _bIsChanged |= (_isactive != value); _isactive = value; }

        }


       public string CCName
        {
            get;
            set;
        }

		public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityCostReleations castObj = null;
            try
            {
                castObj = (EntityCostReleations)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
		
		
	}
	
}

