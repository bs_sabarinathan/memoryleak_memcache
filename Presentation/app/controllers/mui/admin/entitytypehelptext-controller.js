﻿(function (ng, app) {
    "use strict";

    function entitytypehelptextctrl($scope, $resource, $location, $compile, $timeout, $translate, AdminService) {

        GetEntityTypeAttributes();
        $scope.EntityTypeOptionsLists = [];
        $scope.EntityTypeAttributesLists = [];
        $scope.IsHeptext = false;
        $scope.IsToltip = false;
        $scope.entitytypehelptextobj = { "selectedentitytypes": [] };
        function GetEntityTypeAttributes() {

            AdminService.GetEntityTypeAttributes().then(function (getattributes) {
                $scope.Entitytypes = "";
                $scope.EntityTypeAttributes = "";
                $scope.EntityTypeOptionsLists = [];
                $scope.EntityTypeAttributesLists = [];
                $scope.Entitytypes = getattributes.Response.m_Item1;
                $scope.EntityTypeAttributes = getattributes.Response.m_Item2;

                for (var attr in $scope.EntityTypeAttributes) {
                    $scope.EntityTypeAttributesLists.push({ "ID": 0, "Caption": $scope.EntityTypeAttributes[attr].Caption, "AttributeID": $scope.EntityTypeAttributes[attr].AttributeID, "IsHelptextEnabled": $scope.EntityTypeAttributes[attr].IsHelptextEnabled, "_IsHelptextEnabled": $scope.EntityTypeAttributes[attr].IsHelptextEnabled, "HelptextDecsription": $scope.EntityTypeAttributes[attr].HelptextDecsription, "_HelptextDecsription": $scope.EntityTypeAttributes[attr].HelptextDecsription, "EntityTypeID": $scope.EntityTypeAttributes[attr].EntityTypeID });
                }

                for (var key in $scope.Entitytypes) {
                    if ($scope.Entitytypes[key].Id != null && $scope.Entitytypes[key].Id != undefined && $scope.Entitytypes[key].Id != "") {
                        var res = $.grep($scope.EntityTypeAttributesLists, function (e) {
                            return e.EntityTypeID == $scope.Entitytypes[key].Id;
                        });
                        if (res.length > 0) {
                            $scope.EntityTypeOptionsLists.push({ "ID": $scope.Entitytypes[key].Id, "Caption": $scope.Entitytypes[key].Caption, "AttributeData": res });
                        }
                    }
                }
            });
        }

        $scope.GetChckedEntityTypehelptext = function (item, helptext) {

            var updateentitytypehelptext = {};
            var res1 = $.grep($scope.EntityTypeOptionsLists, function (e) {
                return e.ID == item.EntityTypeID;
            });

            if (res1.length > 0) {

                var attr_res = $.grep(res1[0].AttributeData, function (e) {
                    return e.AttributeID == item.AttributeID;
                });
                if (attr_res.length > 0) {

                    var tempres = $.grep($scope.entitytypehelptextobj.selectedentitytypes, function (e) {
                        return e.EntityTypeId == item.EntityTypeID && e.AttributeID == item.AttributeID;
                    });

                    if (attr_res[0].IsHelptextEnabled == true) {
                        updateentitytypehelptext.IsHelptextEnabled = 0;
                        $timeout(function () {
                            attr_res[0].IsHelptextEnabled = false;
                            attr_res[0]._IsHelptextEnabled = false;
                        }, 10);


                        if (tempres.length > 0) {
                            tempres[0].IsHelptextEnabled = false;
                            tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                        }
                        else
                            $scope.entitytypehelptextobj.selectedentitytypes.push({ IsHelptextEnabled: false, EntityTypeId: item.EntityTypeID, AttributeID: item.AttributeID, HelptextDecsription: item.HelptextDecsription.trim() });
                    }
                    else {
                        updateentitytypehelptext.IsHelptextEnabled = 1;
                        $timeout(function () {
                            attr_res[0].IsHelptextEnabled = true;
                            attr_res[0]._IsHelptextEnabled = true;
                        }, 10);
                        if (tempres.length > 0) {
                            tempres[0].IsHelptextEnabled = true;
                            tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                        }
                        else
                            $scope.entitytypehelptextobj.selectedentitytypes.push({ IsHelptextEnabled: true, EntityTypeId: item.EntityTypeID, AttributeID: item.AttributeID, HelptextDecsription: item.HelptextDecsription.trim() });
                    }
                }
            }
        }

        $scope.GetHelptextdescrption = function (item, description, IsEnabled) {

            var updateentitytypehelptext = {};

            var res1 = $.grep($scope.EntityTypeOptionsLists, function (e) {
                return e.ID == item.EntityTypeID;
            });

            if (res1.length > 0) {

                var attr_res = $.grep(res1[0].AttributeData, function (e) {
                    return e.AttributeID == item.AttributeID;
                });

                var tempres = $.grep($scope.entitytypehelptextobj.selectedentitytypes, function (e) {
                    return e.EntityTypeId == item.EntityTypeID && e.AttributeID == item.AttributeID;
                });

                if (attr_res.length > 0) {
                    if (attr_res[0].HelptextDecsription.trim() == attr_res[0]._HelptextDecsription.trim()) {
                        attr_res[0].HelptextDecsription = attr_res[0].HelptextDecsription.trim();
                        attr_res[0]._HelptextDecsription = attr_res[0]._HelptextDecsription.trim();

                        if (tempres.length > 0) {
                            tempres[0].IsHelptextEnabled = attr_res[0].IsHelptextEnabled;
                            tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                        }
                        else
                            $scope.entitytypehelptextobj.selectedentitytypes.push({ IsHelptextEnabled: attr_res[0].IsHelptextEnabled, EntityTypeId: item.EntityTypeID, AttributeID: item.AttributeID, HelptextDecsription: item.HelptextDecsription.trim() });
                    }
                    else {
                        attr_res[0].HelptextDecsription = attr_res[0].HelptextDecsription.trim();
                        if (tempres.length > 0) {
                            tempres[0].IsHelptextEnabled = attr_res[0].IsHelptextEnabled;
                            tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                        }
                        else
                            $scope.entitytypehelptextobj.selectedentitytypes.push({ IsHelptextEnabled: attr_res[0].IsHelptextEnabled, EntityTypeId: item.EntityTypeID, AttributeID: item.AttributeID, HelptextDecsription: item.HelptextDecsription.trim() });

                    }
                }
            }
        }

        $scope.updateentitytypehelptext = function (updateentitytypehelptext) {
            var ss = $scope.entitytypehelptextobj;
            AdminService.updateentitytypehelptext(ss).then(function (getresult) {
                if (getresult.Response == true || getresult.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                }
                else
                    NotifyError($translate.instant('LanguageContents.Res_4941.Caption'));
            });
        }
        ///-------------------------------- Import and Export Functionality---------------------------//
        function generateUUID() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        };

        $scope.Export = function () {
            $('#ExportHelpReportPageModel').modal("show");
            $scope.filename = "";
            var dt = new Date();
            $scope.filename = dt.getDate() + "/" + dt.getMonth() + 1 + "/" + dt.getFullYear() + ":" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var NewGuid = generateUUID();

            AdminService.ExportEntityHelpTextListtoExcel().then(function (GetExportValuesRes) {

                if (GetExportValuesRes.Response != null) {
                    var a = document.createElement('a'), fileid = GetExportValuesRes.Response, extn = '.xlsx';
                    var filename = 'AttributehelptextReport.xlsx';
                    a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                    a.download = fileid + extn;
                    document.body.appendChild(a);
                    a.click();
                    NotifySuccess($translate.instant('LanguageContents.Res_5800.Caption'));
                    $('#ExportHelpReportPageModel').modal("hide");
                }
                else {
                    $('#ExportHelpReportPageModel').modal("hide");
                    NotifyError($translate.instant('LanguageContents.Res_5801.Caption'));
                }
            });
        }

        $scope.ImportPopUp = function () {
            $("#ImportingVal").click();
            $scope.ImpPopup();
        }

        var imgfileid = '';
        $scope.ImpPopup = function () {
            $('.moxie-shim').remove();
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'ImportingVal',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/CustomHandler.ashx?Path=Files/ImportExportFiles&typeoffile=Doc',
                //ImportExportFiles
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',

                resize: { width: 320, height: 240, quality: 90 }
            });

            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });

            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                up.refresh(); // Reposition Flash/Silverlight
                uploader.start();
            });

            uploader.bind('Error', function (up, err) {
                $('#filelist').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );

                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.FileID = response.response.split(',')[0];
                Importing($scope.FileID);
            });
        };


        //------------------>  IMPORT ATTRIBUTEHELPTEXT<--------------
        function Importing(val) {
            $('#ImportHelpReportPageModel').modal("show");

            var rst = {};
            rst.FileImport = val;

            AdminService.InsertEntityHelpTextImport(rst).then(function (addoptionsResponse) {
                if (addoptionsResponse.StatusCode == 200 && addoptionsResponse.Response == "Success") {
                    $('#ImportHelpReportPageModel').modal("hide");
                    GetEntityTypeAttributes();
                    NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                }
                else {
                    $('#ImportHelpReportPageModel').modal("hide");
                    NotifyError($translate.instant('LanguageContents.Res_5823.Caption'));
                }
            });
        }
    }

    app.controller("mui.admin.entitytypehelptextctrl", ['$scope', '$resource', '$location', '$compile', '$timeout', '$translate', 'AdminService', entitytypehelptextctrl]);
})(angular, app);