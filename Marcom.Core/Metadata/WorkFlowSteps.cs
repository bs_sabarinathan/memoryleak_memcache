﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// Attribute object for table 'MM_Attribute'.
    /// </summary>

    internal class WorkFlowSteps : IWorkFlowSteps
    {
        #region Member Variables

        protected int _id;
        protected int _workflowid;
        protected string _name;
        protected string _helptext;
        protected int _sortorder;
        protected IList<IWorkFlowStepPredefinedTasks> _workflowtasks;
        #endregion

        #region Constructors
        public WorkFlowSteps() { }

        public WorkFlowSteps(int pId, int pWorkFlowID, string pName, string pHelpText, int pSortOrder)
        {
            this._id = pId;
            this._workflowid = pWorkFlowID;
            this._name = pName;
            this._helptext = pHelpText;
            this._sortorder = pSortOrder;
        }

        public WorkFlowSteps(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int WorkFlowID
        {
            get { return _workflowid; }
            set { _workflowid = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _name = value;
            }

        }

        public string HelpText
        {
            get { return _helptext; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _helptext = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        public virtual IList<IWorkFlowStepPredefinedTasks> WorkFlowTasks
        {
            get { return _workflowtasks; }
            set { _workflowtasks = value; }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Attribute castObj = null;
            try
            {
                castObj = (Attribute)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
