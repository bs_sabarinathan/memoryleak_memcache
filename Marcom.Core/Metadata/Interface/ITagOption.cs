﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface ITagOption
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        int AttributeID
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        string CreatedOn
        {
            get;
            set;

        }

        int TotalHits
        {
            get;
            set;

        }

        string UsedOn
        {
            get;
            set;

        }

        string text
        {
            get;
            set;

        }

        int PopularTagsToShow
        {
            get;
            set;

        }



        #endregion
    }
}
