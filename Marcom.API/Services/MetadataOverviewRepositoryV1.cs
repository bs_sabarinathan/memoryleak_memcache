﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.RequestHeaderDetails;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Report.Interface;
using Marcom.API.Models;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Helpers;
using System.IO;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using System.Configuration;
using BrandSystems.Marcom.Core.User.Interface;

namespace Marcom.API.Services
{
    public class MetadataOverviewRepositoryV1
    {

        // get the base directory
        public static string baseDir = AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt";
        // search the file below the current directory
        private static string retFilePath = baseDir;
        private static bool bReturnLog = false;

        public IList<MetadataModelV1> GetEntityByID(int EntityID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();

            var metadataobj = marcommanager.CommonManager.GetAPIEntityDetails(true, true, true, true, EntityID, false, false, 0, 20);
            if (metadataobj != null)
            {
                IList<MetadataModelV1> Metadatacollection = new List<MetadataModelV1>();
                foreach (var item in metadataobj)
                {
                    MetadataModelV1 currentObj = new MetadataModelV1();
                    currentObj.ID = item.ID;
                    currentObj.Name = item.Name;
                    currentObj.TypeID = item.TypeID;
                    currentObj.ShortDescription = item.ShortDescription;

                    List<FinancialDetails> ffindelCollection = new List<FinancialDetails>();
                    if (item.FinancialCollections != null)
                    {
                        foreach (var CurrentfinDetl in item.FinancialCollections)
                        {
                            FinancialDetails finobj = new FinancialDetails();
                            finobj.ApprovedAllocation = Convert.ToDouble(CurrentfinDetl["ApprovedAllocation"]);
                            finobj.ApprovedBudget = Convert.ToDouble(CurrentfinDetl["ApprovedBudget"]);
                            finobj.AvailableToSpend = Convert.ToDouble(CurrentfinDetl["AvailableToSpend"]);
                            finobj.BudgetDeviation = Convert.ToDouble(CurrentfinDetl["BudgetDeviation"]);
                            finobj.Commited = Convert.ToDouble(CurrentfinDetl["Commited"]);
                            finobj.Planned = Convert.ToDouble(CurrentfinDetl["Planned"]);
                            finobj.Requested = Convert.ToDouble(CurrentfinDetl["Requested"]);
                            finobj.Spent = Convert.ToDouble(CurrentfinDetl["Spent"]);

                            ffindelCollection.Add(finobj);
                        }

                        currentObj.FinancialSummary = ffindelCollection;
                    }

                    //currentObj.FinancialSummary = item.FinancialCollections;
                    currentObj.Level = item.Level;
                    currentObj.Members = item.MemberCollections;
                    currentObj.Metadata = item.MetadataCollections;

                    currentObj.Tasks = item.TaskCollections;
                    currentObj.TaskLists = item.TaskListCollections;
                    currentObj.TaskOverviewSummary = item.TaskOverviewSummary;
                    currentObj.ColorCode = item.ColorCode;

                    List<CostcenterDetl> CcCollections = new List<CostcenterDetl>();
                    if (item.CostcentreCollections != null)
                    {
                        foreach (var CurrentfinDetl in item.CostcentreCollections)
                        {
                            CostcenterDetl finobj = new CostcenterDetl();
                            finobj.Href = Convert.ToString(CurrentfinDetl["Href"]);
                            finobj.CostCenterID = Convert.ToInt32(CurrentfinDetl["CostCenterID"]);
                            finobj.ApprovedAllocation = Convert.ToDouble(CurrentfinDetl["ApprovedAllocation"]);
                            finobj.ApprovedBudget = Convert.ToDouble(CurrentfinDetl["ApprovedBudget"]);
                            finobj.AvailableToSpend = Convert.ToDouble(CurrentfinDetl["AvailableToSpend"]);
                            finobj.BudgetDeviation = Convert.ToDouble(CurrentfinDetl["BudgetDeviation"]);
                            finobj.Commited = Convert.ToDouble(CurrentfinDetl["Commited"]);
                            finobj.Planned = Convert.ToDouble(CurrentfinDetl["Planned"]);
                            finobj.Name = Convert.ToString(CurrentfinDetl["Name"]);
                            finobj.Requested = Convert.ToDouble(CurrentfinDetl["Requested"]);
                            finobj.Spent = Convert.ToDouble(CurrentfinDetl["Spent"]);

                            CcCollections.Add(finobj);
                        }

                        currentObj.Costcentres = CcCollections;
                    }

                    //currentObj.Costcentres = item.CostcentreCollections;
                    currentObj.Objectives = item.ObjectiveCollections;

                    Metadatacollection.Add(currentObj);

                }

                return Metadatacollection;

            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Format("Requested enitity id does not exists.")),
                    ReasonPhrase = "Requested enitity id does not exists."
                };
                throw new HttpResponseException(resp);
            }

        }

        public IList<MetadataModelV1> GetAllRootLevelEntity()
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();

            var metadataobj = marcommanager.CommonManager.GetAPIEntityDetails(true, true, true, true, 0, false, true, 0, 20);
            if (metadataobj != null)
            {
                IList<MetadataModelV1> Metadatacollection = new List<MetadataModelV1>();
                foreach (var item in metadataobj)
                {
                    MetadataModelV1 currentObj = new MetadataModelV1();
                    currentObj.ID = item.ID;
                    currentObj.Name = item.Name;
                    currentObj.TypeID = item.TypeID;
                    currentObj.ShortDescription = item.ShortDescription;


                    List<FinancialDetails> ffindelCollection = new List<FinancialDetails>();
                    if (item.FinancialCollections != null)
                    {
                        foreach (var CurrentfinDetl in item.FinancialCollections)
                        {
                            FinancialDetails finobj = new FinancialDetails();
                            finobj.ApprovedAllocation = Convert.ToDouble(CurrentfinDetl["ApprovedAllocation"]);
                            finobj.ApprovedBudget = Convert.ToDouble(CurrentfinDetl["ApprovedBudget"]);
                            finobj.AvailableToSpend = Convert.ToDouble(CurrentfinDetl["AvailableToSpend"]);
                            finobj.BudgetDeviation = Convert.ToDouble(CurrentfinDetl["BudgetDeviation"]);
                            finobj.Commited = Convert.ToDouble(CurrentfinDetl["Commited"]);
                            finobj.Planned = Convert.ToDouble(CurrentfinDetl["Planned"]);
                            finobj.Requested = Convert.ToDouble(CurrentfinDetl["Requested"]);
                            finobj.Spent = Convert.ToDouble(CurrentfinDetl["Spent"]);

                            ffindelCollection.Add(finobj);
                        }

                        currentObj.FinancialSummary = ffindelCollection;
                    }
                    //currentObj.FinancialSummary = item.FinancialCollections;
                    currentObj.Level = item.Level;
                    currentObj.Members = item.MemberCollections;
                    currentObj.Metadata = item.MetadataCollections;

                    currentObj.Tasks = item.TaskCollections;
                    currentObj.TaskLists = item.TaskListCollections;
                    currentObj.TaskOverviewSummary = item.TaskOverviewSummary;
                    currentObj.ColorCode = item.ColorCode;


                    List<CostcenterDetl> CcCollections = new List<CostcenterDetl>();
                    if (item.CostcentreCollections != null)
                    {
                        foreach (var CurrentfinDetl in item.CostcentreCollections)
                        {
                            CostcenterDetl finobj = new CostcenterDetl();
                            finobj.Href = Convert.ToString(CurrentfinDetl["Href"]);
                            finobj.CostCenterID = Convert.ToInt32(CurrentfinDetl["CostCenterID"]);
                            finobj.ApprovedAllocation = Convert.ToDouble(CurrentfinDetl["ApprovedAllocation"]);
                            finobj.ApprovedBudget = Convert.ToDouble(CurrentfinDetl["ApprovedBudget"]);
                            finobj.AvailableToSpend = Convert.ToDouble(CurrentfinDetl["AvailableToSpend"]);
                            finobj.BudgetDeviation = Convert.ToDouble(CurrentfinDetl["BudgetDeviation"]);
                            finobj.Commited = Convert.ToDouble(CurrentfinDetl["Commited"]);
                            finobj.Planned = Convert.ToDouble(CurrentfinDetl["Planned"]);
                            finobj.Name = Convert.ToString(CurrentfinDetl["Name"]);
                            finobj.Requested = Convert.ToDouble(CurrentfinDetl["Requested"]);
                            finobj.Spent = Convert.ToDouble(CurrentfinDetl["Spent"]);

                            CcCollections.Add(finobj);
                        }

                        currentObj.Costcentres = CcCollections;
                    }
                    //currentObj.Costcentres = item.CostcentreCollections;
                    currentObj.Objectives = item.ObjectiveCollections;

                    Metadatacollection.Add(currentObj);

                }
                return Metadatacollection;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Format("Requested enitity id does not exists.")),
                    ReasonPhrase = "Requested enitity id does not exists."
                };
                throw new HttpResponseException(resp);
            }

        }

        public bool UpdateEntityLocks(int id, int lockstatus)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            bool HasAccess = marcommanager.PlanningManager.CheckForMemberAvailabilityForEntity(id);
            if (HasAccess == false)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Format("This user does does not have access for this operation.")),
                    ReasonPhrase = "This user does does not have access for this operation."
                };
                throw new HttpResponseException(resp);
            }
            bool lockresponse = marcommanager.PlanningManager.UpdateLock((int)id, (int)lockstatus);
            return lockresponse;
        }

        public bool DuplicateEntities(JObject ids)
        {
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                JArray arrEntityIds = (JArray)ids["DuplicatesIds"];
                foreach (var item in arrEntityIds)
                    marcommanager.PlanningManager.DuplicateEntity((int)item, 0, 1, false);

                return true;
            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public dynamic GetUserInfo(int userid, int entityid)
        {
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                var userobj = marcommanager.PlanningManager.GetEntityLevelAccess(userid, entityid);
                return JObject.Parse(userobj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public IListofRecord GetEntitiesByUserID(int UserId)
        {
            try
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");
                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("in GetEntitiesByUserID repository reached" + Environment.NewLine);

                }
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>("[]");
                List<int> Typeids = marcommanager.MetadataManager.GetAllEntityTypesofAllLevels();
                var lstSettings = MultiRootLevelSettings("RootView", 6, Typeids);
                IListofRecord lstrecord = new ListofRecord();
                lstrecord = marcommanager.MetadataManager.GetListOfEntity((int)0, 10000, 0, iFiltervalues, Idarr, "", true, lstSettings, true, 1, 0, false, UserId, 0, false, 0);
                return lstrecord;
            }
            catch (Exception e)
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");

                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("in GetEntitiesByUserID repository catch" + e.ToString() + Environment.NewLine);

                }

                return null;
            }


        }

        public IListofRecord GetCustomEntitiesByUserID(int UserId, List<int> Typeids, List<int> attrIds, int startRow = 0, int endRow = 20)
        {
            ErrorLog.LogFilePath = retFilePath;

            try
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");
                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("in GetCustomEntitiesByUserID repository reached" + Environment.NewLine);

                }



                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>("[]");
                var lstSettings = CustomAttributesSettings("RootView", 6, Typeids, attrIds);
                IListofRecord lstrecord = new ListofRecord();
                lstrecord = marcommanager.MetadataManager.BriefListofRecords(startRow, endRow, 0, iFiltervalues, Idarr, "", true, lstSettings, true, 1, 0, false, UserId, 0, false, 0, false, 6, true, true, Typeids);
                return lstrecord;
            }
            catch (Exception e)
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");

                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("in GetCustomEntitiesByUserID repository catch" + e.ToString() + Environment.NewLine);

                }
                return null;
            }


        }

        public IList GetUserInvolvedEntities(string UniqueKey, int Userid)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList resultset = marcommanager.MetadataManager.GetUserInvolvedEntities(UniqueKey, Userid);
            return resultset;
        }

        public static ListSettings CustomAttributesSettings(string elementNode, int typeid, List<int> EntityTypeids, List<int> attrIds)
        {


            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            ListSettings listSettings = new ListSettings();
            listSettings.Name = elementNode;
            listSettings.Description = elementNode;
            listSettings.EntityTypes = EntityTypeids;
            listSettings.Attributes = marcommanager.MetadataManager.GetSpefictypeAttributefromDB(EntityTypeids, attrIds);

            return listSettings;
        }


        public static ListSettings MultiRootLevelSettings(string elementNode, int typeid, List<int> EntityTypeids)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = marcommanager.User.TenantPath;

            //string xmlpath = Path.Combine(HttpRuntime.AppDomainAppPath, "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);


            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }



            ListSettings listSettings = new ListSettings();
            if (xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements(elementNode).Count() > 0)
            {
                listSettings.Name = elementNode;
                listSettings.Description = elementNode;
                listSettings.EntityTypes = EntityTypeids;
                listSettings.Attributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(elementNode).Elements("Attributes").Elements("Attribute").Select(a => new BrandSystems.Marcom.Metadata.AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
            }
            return listSettings;
        }

        public IList GetEntitiesList(int UserID, int ParentID, int TypeID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.MetadataManager.GetEntitiesList(UserID, ParentID, TypeID);
        }

        public IList<IAttributeToAttributeRelations> GetAttributeToAttributeRelationsByIDForEntity(int entityTypeID, int entityId = 0)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList<IAttributeToAttributeRelations> resultset = marcommanager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(entityTypeID, entityId);
            return resultset;
        }

        /// <summary>
        /// Gets the entitytype relation.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="version">The version.</param>
        /// <returns>List of IEntityTypeAttributeRelationWithLevels</returns>
        public IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(int typeid, int ParentID = 0, int ImpersonateUID=0)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList<IEntityTypeAttributeRelationwithLevels> resultset = marcommanager.MetadataManager.GetEntityTypeAttributeRelationWithLevelsByID(typeid, ParentID, ImpersonateUID);
            return resultset;
        }

        public List<List<string>> GetValidationDationByEntitytype(int EntityTypeID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            List<List<string>> resultset = marcommanager.MetadataManager.GetValidationDationByEntitytype(EntityTypeID);
            return resultset;
        }

        public IList GetGlobalMembers(int[] roleArr, int entitytypeid)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList resultset = marcommanager.PlanningManager.GetCustomGlobalRoleMembers(roleArr, entitytypeid);
            return resultset;
        }

        public IList CheckCustomHierarchy(int parentEntitytypeID, int[] subtype, int userId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList resultset = marcommanager.MetadataManager.CheckCustomHierarchy(parentEntitytypeID, subtype, userId);
            return resultset;
        }

        public int CreateEntity(int OwnerID, int parentId, int typeId, Boolean active, Boolean isLock, string name, IList<IEntityRoleUser> entityMembers, IList<IEntityCostReleations> entityCostcentres, IList<IEntityPeriod> entityPeriods, IList<IFundingRequest> listFundrequest, IList<IAttributeData> entityattributedata, int[] assetIdArr = null, IList<IObjectiveEntityValue> entityObjectvalues = null, IList<object> attributes = null)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[OwnerID]);
            var owninfo1 = entityattributedata.Where(a => Convert.ToInt32(a.ID) == 69);
            var owninfo = owninfo1.Where(a => Convert.ToInt32(a.Value) == OwnerID).Count();
            if (owninfo > 0)
                OwnerID = 0;

            int EntityID = marcommanager.PlanningManager.CreateEntity(parentId, typeId, active, isLock, name, entityMembers, entityCostcentres, entityPeriods, listFundrequest, entityattributedata, assetIdArr, entityObjectvalues, null, null, OwnerID);
            return EntityID;
        }
        public int ownerchange(int EntityID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            return marcommanager.PlanningManager.ownerchange(EntityID);

        }

        public List<IEntityPredefineObjectiveAttributes> GettingPredefineObjectivesForEntityMetadata(IList<IAttributeData> attributeData, DateTime startDate, DateTime endDate, int entityTypeID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            List<IEntityPredefineObjectiveAttributes> resultset = marcommanager.PlanningManager.GettingPredefineObjectivesForEntityMetadata(attributeData, startDate, endDate, entityTypeID);
            return resultset;

        }



        public List<object> GetCurrentBriefRelatedData(JObject jObj)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            List<object> ResultsData = new List<object>();

            ResultsData.Add(marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity((int)jObj["EntityTypeID"], (int)jObj["BriefID"]));
            ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesValidationDetails((int)jObj["BriefID"], (int)jObj["EntityTypeID"]));
            ResultsData.Add(marcomManager.PlanningManager.GettingCostcentreFinancialOverview((int)jObj["BriefID"]));
            ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesDetails((int)jObj["BriefID"]));

            return ResultsData;
        }

        public string GetAttributeTreeNode(JObject jObj)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string result = "";
            if ((bool)jObj["isdam"])
            {
                result = marcommanager.MetadataManager.GetAttributeTreeNode((int)jObj["attributeID"], (int)jObj["CurrentBriefID"]);
            }
            else
            {
                result = marcommanager.DigitalAssetManager.GetAttributeTreeNode((int)jObj["attributeID"], (int)jObj["CurrentBriefID"]);
            }
            return result;
        }

        public IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParentDetail(JObject jObj)
        {
            IList<DropDownTreePricing> result;
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            if ((bool)jObj["isdam"])
            {
                result = marcommanager.DigitalAssetManager.GetDropDownTreePricingObjectFromParentDetail((int)jObj["attributeID"], (bool)jObj["inheritfromparent"], (bool)jObj["parent"], (int)jObj["CurrentBriefID"]);
            }
            else
            {
                result = marcommanager.MetadataManager.GetDropDownTreePricingObjectFromParentDetail((int)jObj["attributeID"], (bool)jObj["inheritfromparent"], (bool)jObj["parent"], (int)jObj["CurrentBriefID"]);
            }
            return result;
        }


        public IList<DropDownTreePricing> GetDropDownTreePricingObjectDetail(JObject jObj)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

            IList<DropDownTreePricing> result;
            if ((bool)jObj["isdam"])
            {
                result = marcommanager.DigitalAssetManager.GetDropDownTreePricingObjectDetail((int)jObj["attributeID"], (bool)jObj["inheritfromparent"], (bool)jObj["parent"], (int)jObj["CurrentBriefID"], (int)jObj["parentID"]);
            }
            else
            {
                result = marcommanager.MetadataManager.GetDropDownTreePricingObjectDetail((int)jObj["attributeID"], (bool)jObj["inheritfromparent"], (bool)jObj["parent"], (int)jObj["CurrentBriefID"], (int)jObj["parentID"]);
            }
            return result;
        }

        public IList<ICurrencyType> GetCurrencyListFFsettings()
        {
            IList<ICurrencyType> result;
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            result = marcommanager.PlanningManager.GetCurrencyListFFsettings();

            return result;
        }

        public string GetTreeNode(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string result = marcommanager.MetadataManager.GetTreeNode((int)jObj["attributeid"], false);

            return result;
        }

        public string GetTreeNodeByEntityID(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string result = marcommanager.MetadataManager.GetTreeNode((int)jObj["attributeid"], false, (int)jObj["BriefID"]);

            return result;
        }

        public IList<IOption> GetOptionDetailListByID(JObject jObj)
        {
            IList<IOption> result;
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            if ((bool)jObj["isdam"])
            {
                result = marcommanager.MetadataManager.GetOptionDetailListByID((int)jObj["attributeid"], (int)jObj["BriefID"]);
            }
            else
            {
                result = marcommanager.MetadataManager.GetOptionDetailListByID((int)jObj["attributeid"], (int)jObj["BriefID"]);
            }

            return result;
        }

        public string GetDamTreeNode(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string result = marcommanager.DigitalAssetManager.GetDamTreeNode((int)jObj["attributeid"], false, (int)jObj["BriefID"]);

            return result;
        }

        public bool SaveDetailBlockForLevels(JObject Jobje)
        {
            JObject jObj = (JObject)Jobje["updateObj"];
            int apiUserID = (int)Jobje["ImpersonateUID"];
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[apiUserID]);
          
            List<object> obj1 = new List<object>();
            if (jObj["NewValue"].Type == JTokenType.Date)
            {
                obj1.Add(jObj["NewValue"].ToString());
            }
            else
            {
                JArray jrnewValue = (JArray)jObj["NewValue"];
                //dynamic valueList = jrnewValue.Select(jv => (dynamic)jv).ToArray();

                foreach (var obj in jrnewValue)
                {
                    if (obj.Type == JTokenType.Boolean)
                    {
                        obj1.Add(Convert.ToBoolean(obj.ToString()));
                    }
                    else if (obj.Type == JTokenType.Date)
                    {
                        obj1.Add(obj.ToString());

                    }
                    else if (obj.Type == JTokenType.Integer)
                    {

                        obj1.Add(int.Parse(obj.ToString()));

                    }
                    else
                        obj1.Add(obj.ToString());
                }

            }
            bool result = marcommanager.MetadataManager.SaveDetailBlockForLevels((int)jObj["BriefID"], (int)jObj["AttributetypeID"], (int)jObj["AttributeID"], obj1, (int)jObj["Level"], apiUserID);

            return result;
        }

        public int InsertEntityPeriod(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            int result = marcommanager.PlanningManager.InsertEntityPeriod((int)jObj["EntityID"], (string)jObj["StartDate"], (string)jObj["EndDate"], (int)jObj["SortOrder"], (string)jObj["Description"]);

            return result;
        }

        public string GetEntitiPeriodByIdForGantt(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            string result = marcommanager.PlanningManager.GetEntitiPeriodByIdForGantt((int)jObj["BriefID"]);

            return result;
        }

        public bool PostEntityPeriod(JObject jObj)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            bool result = marcommanager.PlanningManager.UpdateEntityPeriod((int)jObj["ID"], (int)jObj["EntityID"], (string)jObj["StartDate"], (string)jObj["EndDate"], (int)jObj["SortOrder"], (string)jObj["Description"]);

            return result;
        }


        public bool DeleteEntityPeriod(int PeriodID)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            bool result = marcommanager.PlanningManager.DeleteEntityPeriod(PeriodID);

            return result;
        }

        public string GetAutoCompleteMemberList(string QueryString)
        {

            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.UserManager.GetAutoCompleteMemberList(QueryString);
        }
        public IList<HolidayDetailsDao> GetNonBusinessDays()
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.CommonManager.GetNonBusinessDays();
        }
        public IList<HolidayDetailsDao> GetHolidaysDetails()
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.CommonManager.GetHolidaysDetails();
        }

        public JObject InsertUpdatePredefinedAttributeGroupData(string AttributeGroupData, string AttributeGroupHeaderCaption, int AttributeGroupID)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.InsertPreDefinedAttributeGroupDataFromMiddleWare(AttributeGroupData, AttributeGroupHeaderCaption, AttributeGroupID);
        }

        public JObject GetListOfEntityAttributesGroupValues(int EntityId, int AttrGroupId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.MetadataManager.GetListOfAttributesGroupValuesToMiddleWare(EntityId, AttrGroupId);
        }

        public JObject GetEntityTypeAttributeDetailsList(int EntityId, int ParentId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.GetEntityTypeAttributeDetailsListToMiddleWare(EntityId, ParentId);
        }

        public JObject GetEntityList(string EntityId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.GetEntityListToMiddleWare(EntityId);
        }

        public JObject GetObjectiveList(string EntityId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.GetObjectiveListToMiddleWare(EntityId);
        }

        public bool UpdateObjectiveList(int EntityId, int ObjectiveID, decimal PlannedTrgt, decimal TrgtOutcome)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.UpdateObjectiveListInMiddleWare(EntityId, ObjectiveID, PlannedTrgt, TrgtOutcome);
        }

        public JObject GetEntityFinancialList(string EntityId)
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.PlanningManager.GetEntityFinancialListToMiddleWare(EntityId);
        }

        public IList<IUser> GetUsers()
        {
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcommanager.UserManager.GetUsers();
        }

    }


}