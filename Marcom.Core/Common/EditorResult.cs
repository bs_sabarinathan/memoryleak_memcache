﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common
{
    public class EditorResult
    {
        public string ColumnName { get; set; }
        public dynamic ColumnValue { get; set; }
    }
}
