﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Marcom.API.Models;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Marcom.API.RequestHeaderDetails;

namespace Marcom.API.Services
{
    public class SessionGenerator : SessionModel
    {

        // get the base directory
        public static string baseDir = AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt";
        // search the file below the current directory
        private static string retFilePath = baseDir;
        private static bool bReturnLog = false;

        public Guid GenerateSessionID(Guid AuthorizationToken, int userid, string TenantHost, bool impersonateuser)
        {
            ErrorLog.LogFilePath = retFilePath;

            try
            {
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "SessionGenerator authenticate token started at " + DateTime.Now.ToString(), DateTime.Now);

                sessionid = MarcomManagerFactory.CreateAPISession(AuthorizationToken, userid, TenantHost, impersonateuser);

                bReturnLog = ErrorLog.CustomErrorRoutine(false, "SessionGenerator authenticate token end at " + DateTime.Now.ToString() + " and the session id is " + sessionid.ToString(), DateTime.Now);

                return sessionid;



            }
            catch (Exception ex)
            {

                ErrorLog.LogFilePath = retFilePath;
                bReturnLog = ErrorLog.ErrorRoutine(false, ex);

                return Guid.Empty;

            }
        }
    }
}