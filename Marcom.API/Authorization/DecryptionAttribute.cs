﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace Marcom.API.Authorization
{
    public class DecryptionAttribute
    {
        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        public string DecryptToken(string data)
        {
            try
            {
                var rsa = new RSACryptoServiceProvider();
                byte[] signature = Convert.FromBase64String(data);

                string xmlpath = (System.Web.HttpContext.Current.Server.MapPath("~/Keys/pair.xml"));
                var xPKDocument = XDocument.Load(xmlpath);
                string PKxml = xPKDocument.ToString();
                rsa.FromXmlString(PKxml);
                var decryptedByte = rsa.Decrypt(signature, false);
                return _encoder.GetString(decryptedByte);
            }
            catch
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
        }

    }

    

}