using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

	/// <summary>
	/// GlobalAclDao object for table 'AM_GlobalAclDao'.
	/// </summary>
	public partial class GlobalAclDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _globalroleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected int _featureid;
		protected bool _accesspermission;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    GlobalAclDao toCompare = obj as GlobalAclDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.GlobalRoleid, toCompare.GlobalRoleid))
        //        return false;
        //    if (!Object.Equals(this.Moduleid, toCompare.Moduleid))
        //        return false;
        //    if (!Object.Equals(this.EntityTypeid, toCompare.EntityTypeid))
        //        return false;
        //    if (!Object.Equals(this.Featureid, toCompare.Featureid))
        //        return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + GlobalRoleid.GetHashCode();
        //    hashCode = (hashCode * 7) + Moduleid.GetHashCode();
        //    hashCode = (hashCode * 7) + EntityTypeid.GetHashCode();
        //    hashCode = (hashCode * 7) + Featureid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public GlobalAclDao() {}
			
        public GlobalAclDao(int pId,int pGlobalRoleid, int pModuleid, int pEntityTypeid, int pFeatureid, bool pAccessPermission)
		{
            this._id = pId;
			this._globalroleid = pGlobalRoleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._featureid = pFeatureid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion
		
		#region Public Properties        
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        
		public virtual int GlobalRoleid
		{
			get { return _globalroleid; }
			set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }
			
		}
		
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual int Featureid
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}
		
		public virtual bool AccessPermission
		{
			get { return _accesspermission; }
			set { _bIsChanged |= (_accesspermission != value); _accesspermission = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
			public const string GlobalAclDao = "GlobalAclDao";           
			public const string GlobalRoleid = "GlobalRoleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Featureid = "Featureid";			
			public const string AccessPermission = "AccessPermission";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";	
			public const string GlobalAclDao = "GlobalAclDao";
			public const string GlobalRoleid = "GlobalRoleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Featureid = "Featureid";			
			public const string AccessPermission = "AccessPermission";			
		}

		#endregion
		
		
	}
	
}
