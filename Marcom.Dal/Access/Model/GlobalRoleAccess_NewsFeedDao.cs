﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class GlobalRoleAccess_NewsFeedDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _globalroleid;
        protected int _newsfeedgroupid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public GlobalRoleAccess_NewsFeedDao() { }

        public GlobalRoleAccess_NewsFeedDao(int ID, int GlobalRoleID, int NewsFeedGroupID)
        {
            this._id = ID;
            this._globalroleid = GlobalRoleID;
            this._newsfeedgroupid = NewsFeedGroupID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }

        }

        public virtual int NewsFeedGroupID
        {
            get { return _newsfeedgroupid; }
            set { _bIsChanged |= (_newsfeedgroupid != value); _newsfeedgroupid = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string GlobalRoleID = "GlobalRoleID";
            public const string NewsFeedGroupID = "NewsFeedGroupID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string GlobalRoleID = "GlobalRoleID";
            public const string NewsFeedGroupID = "NewsFeedGroupID";
        }

        #endregion
    }
}
