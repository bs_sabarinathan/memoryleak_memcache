﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Marcom.API.Models
{
    public class MetadataModelV1 
    {

        int _id;
        string _name;
        string _colorcode;
        string _shortdescription;
        Hashtable _metadatacollections;
        List<Hashtable> _membercollections;
        Hashtable _taskoverviewsummary;
        List<Hashtable> _taskcollections;
        List<Hashtable> _financialcollections;
        int _typeid;
        int _level;
        List<Hashtable> _tasklistcollections;


        public MetadataModelV1()
        { 
        
        }


        public MetadataModelV1(int _pid, string _pname, string _pcolorcode, string _pshortdescription, Hashtable _pmetadatacollections, List<Hashtable> _pmembercollections, Hashtable _ptaskoverviewsummary, List<Hashtable> _ptaskcollections, List<Hashtable> _pfinancialcollections, int _ptypeid, int _plevel, List<Hashtable> _ptasklistcollections)
        {
            this._id = _pid;
            this._name = _pname;
            this._colorcode = _pcolorcode;
            this._shortdescription = _pshortdescription;
            this._metadatacollections = _pmetadatacollections;
            this._membercollections = _pmembercollections;
            this._financialcollections = _pfinancialcollections;
            this._taskcollections = _ptaskcollections;
            this._taskoverviewsummary = _ptaskoverviewsummary;
            this._typeid = _ptypeid;
            this._level = _plevel;
            this._tasklistcollections = _ptasklistcollections;
        }

        public int ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int TypeID
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public string ColorCode
        {
            get;
            set;
        }

        public string ShortDescription
        {
            get;
            set;
        }

        public Hashtable Metadata
        {
            get;
            set;
        }

        public List<Hashtable> Members
        {
            get;
            set;
        }

        public Hashtable TaskOverviewSummary
        {
            get;
            set;
        }

        public List<Hashtable> Tasks
        {
            get;
            set;
        }

        public List<Hashtable> TaskLists
        {
            get;
            set;
        }
         
        public List<FinancialDetails> FinancialSummary
        {
            get;
            set;
        }

        public List<CostcenterDetl> Costcentres { get; set; }
        public List<Hashtable> Objectives { get; set; }
      
    }
}


public class CostcenterDetl 
{

    public int CostCenterID { get; set; }

    public string Href { get; set; }

    public double Planned { get; set; }

     public double Requested { get; set; }

       public double ApprovedAllocation { get; set; }

     public double ApprovedBudget { get; set; }

       public double BudgetDeviation { get; set; }

     public double Commited { get; set; }

       public double Spent { get; set; }

     public double AvailableToSpend { get; set; }

    public string Name { get; set; }

}


public class FinancialDetails
{

    public double Planned { get; set; }

    public double Requested { get; set; }

    public double ApprovedAllocation { get; set; }

    public double ApprovedBudget { get; set; }

    public double BudgetDeviation { get; set; }

    public double Commited { get; set; }

    public double Spent { get; set; }

    public double AvailableToSpend { get; set; }

}
