﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Interface
{
    interface IManager
    {
        void Initialize(IMarcomManager marcomManager);

        void CommitCaches();
        void RollbackCaches();
    }
}
