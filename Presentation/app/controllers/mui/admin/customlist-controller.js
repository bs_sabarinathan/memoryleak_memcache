﻿(function (ng, app) {
    function muiadmincustomviewCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $translate, AdminService) {
        $scope.Entitypes = [];
        $scope.Attributes = [];
        $scope.selectedattributes = [];
        $scope.SelectedAttributeText = '';
        GetEntityTypes();
        GetCustomListOnLoad();

        function GetCustomListOnLoad() {
            AdminService.GetAllCustomList().then(function (customlistResult) {
                if (customlistResult.Response != null) {
                    $scope.CustomListCollection = customlistResult.Response;
                }
            });
        };
        $scope.DeleteCustomListByID = function (currentindex, Id) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2006.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteCustomList(Id).then(function (DeleteCustomresult) {
                            if (DeleteCustomresult.Response != false) {
                                $scope.CustomListCollection.splice(currentindex, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        };
        var edit = false;
        $scope.CustomListByID = function (currentindex) {
            $scope.ViewID = $scope.CustomListCollection[currentindex].Id;
            $scope.Name = $scope.CustomListCollection[currentindex].Name;
            $scope.Description = $scope.CustomListCollection[currentindex].Description;
            $scope.XmlData = $scope.CustomListCollection[currentindex].XmlData;
            var xmlDoc = $.parseXML($scope.XmlData);
            $scope.EnableValidate = true;
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.Namereadonly = true;
            edit = true;
            $('#typeholder').html('');
            $('#attrholder').html('');
            $('#addholder').html('');
            $('#criteria').html('');
            var typehtml = '';
            var TypeIds = [];
            $(xmlDoc).find('EntityType').each(function () {
                typehtml += '<li class="form-inline">' + '<select data-entitytype="true">' + '<option value="0">' + $translate.instant('LanguageContents.Res_4734.Caption') + '</option>';
                for (var i = 0; i < $scope.Entitypes.length; i++) {
                    if ($(this).attr('Id') == $scope.Entitypes[i].Id) {
                        typehtml += '<option selected="selected" value="' + $scope.Entitypes[i].Id + '">' + $scope.Entitypes[i].Caption + '</option>';
                    } else {
                        typehtml += '<option value="' + $scope.Entitypes[i].Id + '">' + $scope.Entitypes[i].Caption + '</option>';
                    }
                }
                typehtml += '</select>' + '<button class="btn margin-right5x" data-add><i class="icon-plus"></i></button>' + '<button class="btn" data-remove><i class="icon-remove"></i></button>' + '</li>';
                TypeIds.push($(this).attr('Id'));
            });
            $('#typeholder').html(typehtml);
            var Attributes = GetAttributes(TypeIds, $(xmlDoc));
            var Addhtml = '';
            var AdditionalInfo = [{
                Id: 1,
                Caption: $translate.instant('LanguageContents.Res_1651.Caption')
            }, {
                Id: 2,
                Caption: $translate.instant('LanguageContents.Res_4994.Caption')
            }, {
                Id: 3,
                Caption: $translate.instant('LanguageContents.Res_5026.Caption')
            }, {
                Id: 4,
                Caption: $translate.instant('LanguageContents.Res_4535.Caption')
            }, {
                Id: 5,
                Caption: $translate.instant('LanguageContents.Res_35.Caption')
            }, {
                Id: 6,
                Caption: $translate.instant('LanguageContents.Res_1212.Caption')
            }];
            $(xmlDoc).find('AdditionalInfo').each(function () {
                Addhtml += '<li class="form-inline">' + '<select data-addinfo="true">' + '<option value="0">' + $translate.instant('LanguageContents.Res_5024.Caption') + '</option>';
                for (var i = 0; i < AdditionalInfo.length; i++) {
                    if ($(this).attr('Id') == AdditionalInfo[i].Id) {
                        Addhtml += '<option selected="selected" value="' + AdditionalInfo[i].Id + '">' + AdditionalInfo[i].Caption + '</option>';
                    } else {
                        Addhtml += '<option value="' + AdditionalInfo[i].Id + '">' + AdditionalInfo[i].Caption + '</option>';
                    }
                }
                Addhtml += '</select>' + '<button class="btn margin-right5x" data-add><i class="icon-plus"></i></button>' + '<button class="btn" data-remove><i class="icon-remove"></i></button>' + '</li>';
            });
            $('#addholder').html(Addhtml);
        };
        $scope.SaveCustomList = function () {
            var custvalParams = {};
            custvalParams.Name = "";
            custvalParams.XmlData = GetXML();
            AdminService.ValidateCustomList(custvalParams).then(function (savecustomvalidationResult) {
                if (savecustomvalidationResult.Response != null) {
                    if (savecustomvalidationResult.Response.m_Item2 == "0") {
                        var custParams = {};
                        custParams.ID = $scope.ViewID;
                        custParams.Name = $scope.Name;
                        custParams.Description = $scope.Description;
                        custParams.XmlData = GetXML();
                        custParams.ValidatedQuery = savecustomvalidationResult.Response.m_Item1;
                        AdminService.InsertUpdateCustomlist(custParams).then(function (savecustomlistResult) {
                            if (savecustomlistResult.Response != 0) {
                                if (edit == false) {
                                    if (savecustomlistResult.Response == -1) {
                                        $scope.Namereadonly = false;
                                        bootbox.alert($translate.instant('LanguageContents.Res_4195.Caption'));
                                        return false;
                                    }
                                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                    $scope.CustomListCollection.push({
                                        Id: savecustomlistResult.Response,
                                        Name: savecustomlistResult.Name,
                                        Description: savecustomlistResult.Description,
                                        XmlData: savecustomlistResult.XmlData
                                    });
                                } else {
                                    var updatedlist = $.grep($scope.CustomListCollection, function (e) {
                                        return e.Id == savecustomlistResult.Response;
                                    });
                                    updatedlist[0].Description = savecustomlistResult.Description;
                                    updatedlist[0].XmlData = savecustomlistResult.XmlData;
                                    updatedlist[0].ValidatedQuery = validatedquery
                                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                }
                                $("#customListPopup").modal('hide');
                                GetCustomListOnLoad();
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            }
                        });
                    }
                }
            });
        };
        var validatedquery = '';
        $scope.ValidateCustomList = function () {
            if ($scope.Name == undefined || $scope.Name == "" || $scope.Name == "Name" || $scope.Name.trim().indexOf(' ') > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4499.Caption'));
                return false;
            } else {
                var custParams = {};
                custParams.Name = "";
                custParams.XmlData = GetXML();
                AdminService.ValidateCustomList(custParams).then(function (savecustomlistResult) {
                    if (savecustomlistResult.Response != null) {
                        if (savecustomlistResult.Response.m_Item2 == "0") {
                            NotifySuccess($translate.instant('LanguageContents.Res_4893.Caption'));
                            $scope.Namereadonly = true;
                            validatedquery = savecustomlistResult.Response.m_Item1;
                            if (edit == false) {
                                $scope.EnableAdd = true;
                            } else {
                                $scope.EnableUpdate = true;
                            }
                            $scope.EnableValidate = false;
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4932.Caption'));
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4933.Caption'));
                    }
                });
            }
        };

        function GetXML() {
            var xmlData = '<?xml version="1.0" encoding="utf-8"?>';
            xmlData += '<CustomList>';
            xmlData += '<EntityTypes>';
            var listOfEntityType = [];
            $('select[data-entitytype="true"]').each(function () {
                if ($(this).val() != '0') {
                    if ($.inArray($(this).val(), listOfEntityType) == -1) {
                        listOfEntityType.push($(this).val());
                        xmlData += '<EntityType Id="' + $(this).val() + '" Caption="' + $(this).find('option[value="' + $(this).val() + '"]').html() + '"/>';
                    }
                }
            });
            listOfEntityType = null;
            xmlData += '</EntityTypes>';
            xmlData += '<Attributes>';
            var listOfAttribute = [];
            $('select[data-attribute="true"]').each(function () {
                if ($(this).val() != '0') {
                    if ($.inArray($(this).val(), listOfAttribute) == -1) {
                        listOfAttribute.push($(this).val());
                        xmlData += '<Attribute Id="' + $(this).find('option[value="' + $(this).val() + '"]').attr('data-value') + '" Type="' + $(this).find('option[value="' + $(this).val() + '"]').attr('data-typeid') + '" Level="' + $(this).find('option[value="' + $(this).val() + '"]').attr('data-level') + '" Caption="' + $(this).find('option[value="' + $(this).val() + '"]').html() + '" />';
                    }
                }
            });
            listOfAttribute = null;
            xmlData += '</Attributes>';
            xmlData += '<AdditionalInfos>';
            var listOfAdditionalInfos = [];
            $('select[data-addinfo="true"]').each(function () {
                if ($(this).val() != '0') {
                    if ($.inArray($(this).val(), listOfAdditionalInfos) == -1) {
                        listOfAdditionalInfos.push($(this).val());
                        xmlData += '<AdditionalInfo Id="' + $(this).val() + '" Caption="' + $(this).find('option[value="' + $(this).val() + '"]').html() + '" />';
                    }
                }
            });
            xmlData += '</AdditionalInfos>';
            xmlData += '<Criterias>';
            $('#criteria > li').each(function () {
                var condition = $(this).find('select[data-condition="true"]').val();
                var AttributeID = $(this).find('select[data-selectedattr="true"]').find('option[value="' + $(this).find('select[data-selectedattr="true"]').val() + '"]').attr('data-value');
                var AttributeTypeID = $(this).find('select[data-selectedattr="true"]').find('option[value="' + $(this).find('select[data-selectedattr="true"]').val() + '"]').attr('data-typeid');
                var AttributeLevel = $(this).find('select[data-selectedattr="true"]').find('option[value="' + $(this).find('select[data-selectedattr="true"]').val() + '"]').attr('data-level');
                var AttributeCaption = $(this).find('select[data-selectedattr="true"]').find('option[value="' + $(this).find('select[data-selectedattr="true"]').val() + '"]').html();
                var Operator = $(this).find('select[data-operator="true"]').val();
                var Value = '';
                var Options = '';
                if (typeof AttributeID != 'undefined') {
                    switch (Operator) {
                        case '0':
                            break;
                        case 'IN':
                        case 'NOT IN':
                            if ($(this).find('select[data-selectedattr="true"]').val() == '-1-0') {
                                Value = $(this).find('input[data-value="true"][type="text"]').val();
                            } else {
                                var list = $(this).find('select[data-value="true"]').val() || [];
                                for (var i = 0; i < list.length; i++) {
                                    if (Value.length > 0) {
                                        Value = Value + ",'" + $(this).find('select[data-value="true"]').find('option[value="' + list[i] + '"]').html() + "'";
                                    } else {
                                        Value = Value + "'" + $(this).find('select[data-value="true"]').find('option[value="' + list[i] + '"]').html() + "'";
                                    }
                                }
                                var alllist = $(this).find('select[data-value="true"]').find('option');
                                for (var i = 0; i < alllist.length; i++) {
                                    if (Options.length > 0) {
                                        Options = Options + "###" + $(alllist[i]).html() + "@@@" + $(alllist[i]).html();
                                    } else {
                                        Options = Options + "" + $(alllist[i]).html() + "@@@" + $(alllist[i]).html();
                                    }
                                }
                            }
                            break;
                        case 'LIKE':
                            Value = $(this).find('input[data-value="true"][type="text"]').val();
                            break;
                        case '>':
                        case '<':
                        case '=':
                            Value = $(this).find('input[data-value="true"][type="date"]').val();
                            break;
                    }
                    xmlData += '<Criteria Condition="' + condition + '" AttributeID="' + AttributeID + '" AttributeTypeID="' + AttributeTypeID + '" AttributeLevel="' + AttributeLevel + '" AttributeCaption="' + AttributeCaption + '" Operator="' + Operator + '" Value="' + Value + '" Options="' + Options + '"/>';
                }
            });
            xmlData += '</Criterias>';
            xmlData += '</CustomList>';
            return xmlData;
        }

        function LoadFillEntityTypeOptions(entityAttribtueId, entityAttributeLevel, obj) {
            AdminService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                    obj.html('');
                    for (var i = 0; i < fulfillmentAttributeOptionsObj.Response.length; i++) {
                        obj.append('<option value="' + fulfillmentAttributeOptionsObj.Response[i].Id + '">' + fulfillmentAttributeOptionsObj.Response[i].Caption + '</option>');
                    }
                }
            });
        }

        function GetEntityTypes() {
            AdminService.GetTaskFulfillmentEntityTypes().then(function (fulFillmentEntityTypesObj) {
                $scope.Entitypes = fulFillmentEntityTypesObj.Response;
            });
        }

        function LoadEntityType() {
            for (var i = 0; i < $scope.Entitypes.length; i++) {
                $('select[data-entitytype="true"]').append('<option value="' + $scope.Entitypes[i].Id + '">' + $scope.Entitypes[i].Caption + '</option>');
            }
        }

        function GetAttributes(TypeIds, xmlDoc) {
            var fullfilamParms = {};
            fullfilamParms.EntityTypeIDs = TypeIds;
            AdminService.GetFulfillmentAttribute(fullfilamParms).then(function (fulfillmentAttributeOptionsObj) {
                if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                    $scope.Attributes = fulfillmentAttributeOptionsObj.Response;
                    var attrhtml = '';
                    var AttributeList = [];
                    $(xmlDoc).find('Attribute').each(function () {
                        attrhtml += '<li class="form-inline">' + '<select data-attribute="true" data-id="' + $(this).attr('Id') + '-' + $(this).attr('Level') + '">' + '<option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option>';
                        for (var i = 0; i < $scope.Attributes.length; i++) {
                            if ($(this).attr('Id') == $scope.Attributes[i].Id && $(this).attr('Level') == $scope.Attributes[i].Level) {
                                attrhtml += '<option selected="selected" data-typeId="' + $scope.Attributes[i].AttributeTypeID + '" data-level="' + $scope.Attributes[i].Level + '" data-value="' + $scope.Attributes[i].Id + '" value="' + $scope.Attributes[i].Id + '-' + $scope.Attributes[i].Level + '">' + $scope.Attributes[i].Caption + '</option>';
                            } else {
                                attrhtml += '<option data-typeId="' + $scope.Attributes[i].AttributeTypeID + '" data-level="' + $scope.Attributes[i].Level + '" data-value="' + $scope.Attributes[i].Id + '" value="' + $scope.Attributes[i].Id + '-' + $scope.Attributes[i].Level + '">' + $scope.Attributes[i].Caption + '</option>';
                            }
                        }
                        attrhtml += '</select>' + '<button class="btn margin-right5x" data-add><i class="icon-plus"></i></button>' + '<button class="btn" data-remove><i class="icon-remove"></i></button>' + '</li>';
                        AttributeList.push({
                            Id: $(this).attr('Id'),
                            Type: $(this).attr('Type'),
                            Level: $(this).attr('Level'),
                            Caption: $(this).attr('Caption'),
                            Value: $(this).attr('Id') + '-' + $(this).attr('Level')
                        });
                    });
                    $('#attrholder').html(attrhtml);
                    var criteria = '';
                    $(xmlDoc).find('Criteria').each(function () {
                        criteria += '<li class="form-inline">' + '<select data-condition="true">';
                        if ($(this).attr('Condition') == '0') {
                            criteria += '<option selected="selected" value="0">' + $translate.instant('LanguageContents.Res_812.Caption') + '</option>' + '<option value="1">' + $translate.instant('LanguageContents.Res_5027.Caption') + '</option>';
                        } else {
                            criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_812.Caption') + '</option>' + '<option  selected="selected" value="1">' + $translate.instant('LanguageContents.Res_5027.Caption') + '</option>';
                        }
                        criteria += '</select>' + '<select data-selectedattr="true">' + '<option value="0">+</option>';
                        $scope.SelectedAttributeText = '<option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option>';
                        for (var i = 0; i < AttributeList.length; i++) {
                            if ($(this).attr('AttributeID') == AttributeList[i].Id && $(this).attr('AttributeLevel') == AttributeList[i].Level) {
                                criteria += '<option  selected="selected" data-typeId="' + AttributeList[i].Type + '" data-level="' + AttributeList[i].Level + '" data-value="' + AttributeList[i].Id + '" value="' + AttributeList[i].Value + '">' + AttributeList[i].Caption + '</option>';
                                $scope.SelectedAttributeText += '<option  selected="selected" data-typeId="' + AttributeList[i].Type + '" data-level="' + AttributeList[i].Level + '" data-value="' + AttributeList[i].Id + '" value="' + AttributeList[i].Value + '">' + AttributeList[i].Caption + '</option>';
                            } else {
                                criteria += '<option data-typeId="' + AttributeList[i].Type + '" data-level="' + AttributeList[i].Level + '" data-value="' + AttributeList[i].Id + '" value="' + AttributeList[i].Value + '">' + AttributeList[i].Caption + '</option>';
                                $scope.SelectedAttributeText += '<option data-typeId="' + AttributeList[i].Type + '" data-level="' + AttributeList[i].Level + '" data-value="' + AttributeList[i].Id + '" value="' + AttributeList[i].Value + '">' + AttributeList[i].Caption + '</option>';
                            }
                        }
                        criteria += '</select>' + '<select data-operator="true">';
                        switch (parseInt($(this).attr('AttributeTypeID'))) {
                            case 0:
                                break;
                            case 1:
                            case 2:
                                if ($(this).attr('Operator') == 'LIKE') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option selected="selected" value="LIKE">Like</option>';
                                } else {
                                    criteria += '<option selected="selected" value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value="LIKE">Like</option>';
                                }
                                break;
                            case 3:
                            case 4:
                            case 6:
                            case 7:
                            case 12:
                                if ($(this).attr('Operator') == 'IN') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option selected="selected" value="IN">In</option>' + '<option value="NOT IN">Not in</option>';
                                } else if ($(this).attr('Operator') == 'NOT IN') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value="IN">In</option>' + '<option selected="selected" value="NOT IN">Not in</option>';
                                } else {
                                    criteria += '<option selected="selected" value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value="IN">In</option>' + '<option value="NOT IN">Not in</option>';
                                }
                                break;
                            case 10:
                            case 5:
                                if ($(this).attr('Operator') == '>') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option selected="selected" value=">">Greater than</option>' + '<option value="<">Less than</option>' + '<option value="=">Equal to</option>';
                                } else if ($(this).attr('Operator') == '<') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value=">">Greater than</option>' + '<option selected="selected" value="<">Less than</option>' + '<option value="=">Equal to</option>';
                                } else if ($(this).attr('Operator') == '=') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value=">">Greater than</option>' + '<option value="<">Less than</option>' + '<option selected="selected" value="=">Equal to</option>';
                                } else {
                                    criteria += '<option selected="selected" value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value=">">Greater than</option>' + '<option value="<">Less than</option>' + '<option value="=">Equal to</option>';
                                }
                                break;
                            case 8:
                            case 9:
                            case 11:
                                break;
                            case -1:
                                if ($(this).attr('Operator') == 'IN') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option selected="selected" value="IN">In</option>' + '<option value="NOT IN">Not in</option>';
                                } else if ($(this).attr('Operator') == 'NOT IN') {
                                    criteria += '<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value="IN">In</option>' + '<option selected="selected" value="NOT IN">Not in</option>';
                                } else {
                                    criteria += '<option selected="selected" value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option>' + '<option value="IN">In</option>' + '<option value="NOT IN">Not in</option>';
                                }
                                break;
                            default:
                        }
                        criteria += '</select>';
                        switch ($(this).attr('Operator')) {
                            case '0':
                                break;
                            case 'IN':
                            case 'NOT IN':
                                if ($(this).parent().find('select[data-selectedattr="true"]').val() == '-1-0') {
                                    criteria += '<select data-value="true" multiple style="display: none">' + '</select>' + '<input data-value="true" type="text" value="' + $(this).attr('Value') + '" />' + '<input data-value="true" type="date" style="display: none" />';
                                } else {
                                    criteria += '<select data-value="true" multiple>';
                                    var OptionsList = $(this).attr('Options').split('###');
                                    var SelectedVal = $(this).attr('Value').split(',');
                                    for (var j = 0; j < OptionsList.length; j++) {
                                        if ($.inArray("'" + OptionsList[j].split('@@@')[1] + "'", SelectedVal) != -1) {
                                            criteria += '<option selected="selected" value="' + OptionsList[j].split('@@@')[0] + '">' + OptionsList[j].split('@@@')[1] + '</option>';
                                        } else {
                                            criteria += '<option value="' + OptionsList[j].split('@@@')[0] + '">' + OptionsList[j].split('@@@')[1] + '</option>';
                                        }
                                    }
                                    criteria += '</select>' + '<input data-value="true" type="text" style="display: none" />' + '<input data-value="true" type="date" style="display: none" />';
                                }
                                break;
                            case 'LIKE':
                                criteria += '<select data-value="true" multiple style="display: none">' + '</select>' + '<input data-value="true" type="text" value="' + $(this).attr('Value') + '" />' + '<input data-value="true" type="date" style="display: none" />';
                                break;
                            case '>':
                            case '<':
                            case '=':
                                criteria += '<select data-value="true" multiple style="display: none">' + '</select>' + '<input data-value="true" type="text" style="display: none" />' + '<input data-value="true" type="date" value="' + $(this).attr('Value') + '" />';
                                break;
                        }
                        criteria += '<button class="btn margin-right5x" data-add><i class="icon-plus"></i></button>' + '<button class="btn" data-remove><i class="icon-remove"></i></button>' + '</li>';
                    });
                    $('#criteria').html(criteria);
                    $('#customListPopup').find('select').select2();
                    $("#customListPopup").modal('show');
                } else {
                    $('#attrholder').html('<li class="form-inline"><select data-attribute="true"><option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option></select><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
                    $('#criteria').html('<li class="form-inline"><select data-condition="true"><option value="0">' + $translate.instant('LanguageContents.Res_812.Caption') + '</option><option value="1">' + $translate.instant('LanguageContents.Res_5027.Caption') + '</option></select><select data-selectedattr="true"><option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option></select><select data-operator="true"><option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option></select><select data-value="true" multiple style="display: none"><option value="0">Select option</option></select><input data-value="true" type="text" /><input data-value="true" type="date" style="display: none" /><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
                    $('#customListPopup').find('select').select2();
                    $("#customListPopup").modal('show');
                }
            });
        };

        function LoadAttributes() {
            var TypeIds = [];
            for (var i = 0; i < $('select[data-entitytype="true"]').length; i++) {
                TypeIds[i] = $($('select[data-entitytype="true"]')[i]).val();
            }
            var fullfilamParms = {};
            fullfilamParms.EntityTypeIDs = TypeIds;
            AdminService.GetFulfillmentAttribute(fullfilamParms).then(function (fulfillmentAttributeOptionsObj) {
                if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                    $scope.Attributes = fulfillmentAttributeOptionsObj.Response;
                    $('select[data-attribute="true"]').html('<option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option>');
                    for (var i = 0; i < $scope.Attributes.length; i++) {
                        $('select[data-attribute="true"]').append('<option data-typeId="' + $scope.Attributes[i].AttributeTypeID + '" data-level="' + $scope.Attributes[i].Level + '" data-value="' + $scope.Attributes[i].Id + '" value="' + $scope.Attributes[i].Id + '-' + $scope.Attributes[i].Level + '">' + $scope.Attributes[i].Caption + '</option>');
                    }
                    $('select[data-attribute="true"]').each(function () {
                        if ($(this).attr('data-id') != undefined) {
                            if ($(this).attr('data-id') != '0') {
                                if ($(this).find('option[value="' + $(this).attr('data-id') + '"]').length > 0) {
                                    $(this).val($(this).attr('data-id'));
                                } else {
                                    $(this).val('0');
                                }
                            }
                        }
                    });
                }
            });
        }
        $scope.OpenAddTabs = function () {
            $scope.ViewID = 0;
            $scope.Name = "";
            $scope.Description = "";
            $scope.XmlData = "";
            $scope.EnableValidate = true;
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.Namereadonly = false;
            edit = false;
            $('#typeholder').html('');
            $('#attrholder').html('');
            $('#addholder').html('');
            $('#criteria').html('');
            $('#typeholder').html('<li class="form-inline"><select data-entitytype="true"><option value="0">' + $translate.instant('LanguageContents.Res_4734.Caption') + '</option></select><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
            $('#attrholder').html('<li class="form-inline"><select data-attribute="true"><option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option></select><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
            $('#addholder').html('<li class="form-inline"><select data-addinfo="true"><option value="0">' + $translate.instant('LanguageContents.Res_5024.Caption') + '</option><option value="1">' + $translate.instant('LanguageContents.Res_1651.Caption') + '</option><option value="2">' + $translate.instant('LanguageContents.Res_4994.Caption') + '</option><option value="3">' + $translate.instant('LanguageContents.Res_5026.Caption') + '</option><option value="4">' + $translate.instant('LanguageContents.Res_4535.Caption') + '</option><option value="5">' + $translate.instant('LanguageContents.Res_35.Caption') + '</option><option value="6">' + $translate.instant('LanguageContents.Res_1212.Caption') + '</option></select><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
            $('#criteria').html('<li class="form-inline"><select data-condition="true"><option value="0">' + $translate.instant('LanguageContents.Res_812.Caption') + '</option><option value="1">' + $translate.instant('LanguageContents.Res_5027.Caption') + '</option></select><select data-selectedattr="true"><option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option></select><select data-operator="true"><option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option></select><select data-value="true" multiple style="display: none"><option value="0">Select option</option></select><input data-value="true" type="text" /><input data-value="true" type="date" style="display: none" /><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
            LoadEntityType();
            $('#customListPopup').find('select').select2();
            $("#customListPopup").modal('show');
        };
        $(document).on('change', 'select[data-entitytype="true"]', function () {
            LoadAttributes();
        });
        $(document).on('change', 'select[data-attribute="true"]', function () {
            var obj = $(this);
            $(obj).attr('data-id', $(obj).val());
            $('select[data-SelectedAttr="true"]').html('<option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option>');
            $scope.SelectedAttributeText = '<option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option>'
            $('select[data-attribute="true"]').each(function () {
                if ($(this).attr('data-id') != undefined) {
                    if ($(this).attr('data-id') != '0') {
                        var option = $(this).find('option[value="' + $(this).attr('data-id') + '"]');
                        $('select[data-SelectedAttr="true"]').append('<option data-typeId="' + option.attr("data-typeId") + '" data-level="' + option.attr("data-level") + '" data-value="' + option.attr("data-value") + '" value="' + option.attr("value") + '">' + option.html() + '</option>');
                        $scope.SelectedAttributeText += '<option data-typeId="' + option.attr("data-typeId") + '" data-level="' + option.attr("data-level") + '" data-value="' + option.attr("data-value") + '" value="' + option.attr("value") + '">' + option.html() + '</option>';
                    }
                }
            });
        });
        $(document).on('change', 'select[data-selectedattr="true"]', function () {
            var obj = $(this);
            var typeId = $(obj).find('option[value="' + $(obj).val() + '"]').attr("data-typeId");
            $(obj).parent().find('select[data-operator="true"]').html('');
            $(this).parent().find('select[data-value="true"]').html('');
            $(this).parent().find('input[data-value="true"][type="text"]').html('');
            $(this).parent().find('input[data-value="true"][type="date"]').html('');
            switch (parseInt(typeId)) {
                case 0:
                    break;
                case 1:
                case 2:
                    $(obj).parent().find('select[data-operator="true"]').append('<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option><option value="LIKE">Like</option>');
                    break;
                case 3:
                case 4:
                case 6:
                case 7:
                case 12:
                    $(obj).parent().find('select[data-operator="true"]').append('<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option><option value="IN">In</option><option value="NOT IN">Not in</option>');
                    break;
                case 10:
                case 5:
                    $(obj).parent().find('select[data-operator="true"]').append('<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option><option value=">">Greater than</option><option value="<">Less than</option><option value="=">Equal to</option>');
                    break;
                case 8:
                case 9:
                case 11:
                    break;
                case -1:
                    $(obj).parent().find('select[data-operator="true"]').append('<option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option><option value="IN">In</option><option value="NOT IN">Not in</option>');
                    break;
                default:
            }
        });
        $(document).on('change', 'select[data-operator="true"]', function () {
            var value = $(this).val();
            switch (value) {
                case '0':
                    break;
                case 'IN':
                case 'NOT IN':
                    if ($(this).parent().find('select[data-selectedattr="true"]').val() == '-1-0') {
                        $(this).parent().find('select[data-value="true"]').hide();
                        $(this).parent().find('select[data-value="true"]').prev().hide();
                        $(this).parent().find('input[data-value="true"][type="text"]').show();
                        $(this).parent().find('input[data-value="true"][type="date"]').hide();
                    } else {
                        $(this).parent().find('select[data-value="true"]').hide();
                        $(this).parent().find('select[data-value="true"]').prev().show();
                        $(this).parent().find('input[data-value="true"][type="text"]').hide();
                        $(this).parent().find('input[data-value="true"][type="date"]').hide();
                        var option = $(this).parent().find('select[data-selectedattr="true"]').find('option[value="' + $(this).parent().find('select[data-selectedattr="true"]').val() + '"]');
                        LoadFillEntityTypeOptions(option.attr('data-value'), option.attr('data-level'), $(this).parent().find('select[data-value="true"]'));
                    }
                    break;
                case 'LIKE':
                    $(this).parent().find('select[data-value="true"]').hide();
                    $(this).parent().find('select[data-value="true"]').prev().hide();
                    $(this).parent().find('input[data-value="true"][type="text"]').show();
                    $(this).parent().find('input[data-value="true"][type="date"]').hide();
                    break;
                case '>':
                case '<':
                case '=':
                    $(this).parent().find('select[data-value="true"]').hide();
                    $(this).parent().find('select[data-value="true"]').prev().hide();
                    $(this).parent().find('input[data-value="true"][type="text"]').hide();
                    $(this).parent().find('input[data-value="true"][type="date"]').show();
                    break;
            }
        });
        $(document).on('click', 'button[data-add]', function () {
            var obj = $(this);
            if (obj[0].tagName == 'I') {
                obj = $(obj).parent();
            }
            var newRow = $(obj).parent().clone();
            $(newRow).find('input').val('');
            $(newRow).find('input:checked').prop('checked', false);
            $(newRow).find('.chzn-container').remove();
            $(newRow).find('select').find('option').removeAttr('selected');
            $(newRow).find('select').val('').removeAttr('id').css('display', 'inline-block');
            $(newRow).find(".select2-container").remove();
            $(newRow).find("select").removeClass('select2-offscreen');
            var a = '';
            if (obj.parent().parent().attr('id') == 'criteria') {
                var temp = $('<div></div>');
                temp.append('<li class="form-inline"><select data-condition="true"><option value="0">' + $translate.instant('LanguageContents.Res_812.Caption') + '</option><option value="1">' + $translate.instant('LanguageContents.Res_5027.Caption') + '</option></select><select data-selectedattr="true"><option value="0">' + $translate.instant('LanguageContents.Res_4727.Caption') + '</option></select><select data-operator="true"><option value="0">' + $translate.instant('LanguageContents.Res_5025.Caption') + '</option></select><select data-value="true" multiple style="display: none"><option value="0">Select option</option></select><input data-value="true" type="text" /><input data-value="true" type="date" style="display: none" /><button class="btn margin-right5x" data-add><i class="icon-plus"></i></button><button class="btn" data-remove><i class="icon-remove"></i></button></li>');
                temp.find('select[data-selectedattr="true"]').html($scope.SelectedAttributeText);
                a = $(obj).parent().after(temp.html());
            } else {
                a = $(obj).parent().after(newRow[0].outerHTML);
            }
            $(a).next().find('select').select2();
        });
        $(document).on('click', 'button[data-remove]', function () {
            var obj = $(this);
            if (obj[0].tagName == 'I') {
                obj = $(obj).parent();
            }
            if ($(obj).parent().parent().find('>li').length > 1) {
                $(obj).parent().remove();
            } else {
                $(obj).parent().parent().find('input').val('');
                $(obj).parent().parent().find('input:checked').prop('checked', false);
                $(obj).parent().parent().find('.chzn-container').remove();
                $(obj).parent().parent().find('select').val('').css('display', 'inline-block');
                $(obj.target).parent().parent().find(".select2-container").remove();
                $(obj.target).parent().parent().find("select").removeClass('select2-offscreen');
                $(obj.target).parent().parent().find("select").select2();
            }
            if ($(obj).parent().find('select[data-entitytype="true"]').length > 0) {
                $scope.LoadAttribues();
            }
        });

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.controller("mui.admin.customlistCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$translate', 'AdminService', muiadmincustomviewCtrl]);
})(angular, app);