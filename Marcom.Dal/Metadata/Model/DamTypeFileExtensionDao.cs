﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// DamTypeFileExtensionDao object for table 'MM_DamType_FileExtension'.
    /// </summary>

    public partial class DamTypeFileExtensionDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected string _ExtensionOptions;
        protected int _sortorder;
        protected bool _isremoved;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public DamTypeFileExtensionDao() { }

        public DamTypeFileExtensionDao(int pID, int pEntityTypeID, string pExtensionOptions, int pSortOrder, bool pIsRemoved)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._ExtensionOptions = pExtensionOptions;
            this._sortorder = pSortOrder;
        }

        public DamTypeFileExtensionDao(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public virtual string ExtensionOptions
        {
            get { return _ExtensionOptions; }
            set
            {
                _ExtensionOptions = value;
            }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set
            {
                _sortorder = value;
            }

        }
        public virtual bool IsRemoved
        {
            get { return _isremoved; }
            set { _isremoved = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string DamTypeFileExtensionDao = "DamTypeFileExtensionDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string ExtensionOptions = "ExtensionOptions";
            public const string SortOrder = "SortOrder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string DamTypeFileExtensionDao = "DamTypeFileExtensionDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string ExtensionOptions = "ExtensionOptions";
            public const string SortOrder = "SortOrder";
        }

        #endregion


    }

}
