﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Dam.Interface;
using System.Collections;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Dal.DAM.Model;
using BrandSystems.Marcom.Dal.ExpireHandler.Model;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;
namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IExpireHandlerManager
    {

        int CreateExpireAction(int ActionTypeID, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, int ActionsourceId, int attributeid, JObject srcValue);
        IList<EHActionInitiatorDao> GetExpireActions(int SourceID, int AttributeID, int ActionID);

        bool UpdateExpireActionDate(int SourceID, string DateActionexpiredate, int SourcetypeID, int ActionType, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID);

        bool DeleteExpireAction(int ActionID);

        IList<IAttribute> GetEntityAttributes(int entityID);

        //int CreateExpireActionwithMetadata(int ActionType, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, bool p, bool ispublish, int ActionsourceId, IList<IAttributeData> listattributevalues, string Taskinputvalue);

        IList<IExpireActionSourcedatas> GetExpireHandlermetadata(int actionsourceID);

        IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID( int entityID);
        bool ExpiryupdateEntityAttribute(IList<IAttributeData> attributes, int entityId,int updaterid);
        bool ValidExpireActionDate(int SourceID, string DateActionexpiredate, int SourcetypeID, int ActionType, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID, string DueActionexutedays);
        IList<IEntityTypeExpireRoleAcl> GetEntityTypeExpireRoleAccess(int EntityTypeID);
    }
}
