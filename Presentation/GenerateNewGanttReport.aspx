﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateNewGanttReport.aspx.cs" Inherits="Presentation.GenerateNewGanttReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="Scripts/jQuery/Jquery-1.4.4.min.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-size: 62.5%;
        }
    h1 {
    color: #404040;
    display: inline-block;
    font-family: Segoe UI,Tahoma,Verdana,Arial;
    font-size: 18px;
    font-weight: normal;
    margin-bottom: 1em;
}
span {
    color: #404040;
    font-family: Verdana,Arial,sans-serif;
    font-size: 10px;
}
a.Button {
    border-radius: 4px 4px 4px 4px;
    color: #FFFFFF;
    font-weight: bold;
    padding: 0.3em 1.5em;
    text-decoration: none;
    cursor:pointer;
    background: url("App_Themes/Default/image/03_highlight_soft.png") repeat-x scroll 50% 50% #A6A6A6;
    border: 2px solid #A6A6A6;
}
    </style>
</head>
<body>
    <div id="Progress" style="height: 100%; width: 100%; text-align: center; position: relative;
        padding-top: 15px;">
        <img width="32" height="32" align="middle" style="" src="App_Themes/Default/image/ajax-loader.gif"
            alt=""><br>
        <h1>
            Generating Report...</h1>
        <br />
        <span>This could take several minutes based on your selection.</span>
    </div>
    <div id="Done" style="display: none;padding-top: 10px; text-align: center;">
        <h1>Report generated</h1> 
        <br />
        <span>Please save or open the report before you close this window.</span>
        <br />
        <br />
        <br />
        <a href="javascript:void(0);" class="Button"
            onclick="window.close();" id="nextMetadata"><span>Close</span></a>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            var location = window.opener.GrantReportURL;

            var jqxhr = $.get(location, function (data) {
                if (data != '' && data.length <= 36) {

                    $('#Progress').hide();
                    $('#Done').show();
                    window.location = "GenerateNewGanttReport.aspx?xid=" + data;

                } else {

                    $('#Progress').hide();
                    $('#Done span').html("Unable to generate report.");
                    $('#Done').show();

                }
            })

        });

    </script>
</body>
</html>

