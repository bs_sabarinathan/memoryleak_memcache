﻿(function (ng, app) {
    "use strict";

    function muiadminuserregistrationlogoCtrl($scope, $timeout, $resource, $translate, AdminService) {
        try {

            var LogoSettings = 'UserLogoSettings';
            var IslogoChanged = false;
            var IsBackgroudIMgChanged = false;
            AdminService.GetUserRegAdminSettings(LogoSettings, 0).then(function (getadminlogosettings) {
                if (getadminlogosettings.Response != undefined) {
                    var result = JSON.parse(getadminlogosettings.Response);                   
                    $("#UserRegLogoimg").attr('src', result.UserLogoSettings.logo)
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4326.Caption'));
                }
                $(window).AdjustHeightWidth();
            });
            var UserRegBlock = 'UserRegBlock';
            AdminService.GetUserRegLogoSettings(UserRegBlock).then(function (ThemeResultData) {
                var rst = [];
                rst = JSON.parse(ThemeResultData.Response);
                $scope.data = rst;
                $("#BackGrdImg").attr('src', $scope.data.root.UserRegBlock.BackGroundImage);
            });

            $scope.save = function () {             
                var adminSettings = {};
                adminSettings.Key = 'UserLogoSettings';
                adminSettings.LogoSettings = {             
                    HomeUrl: '',
                    logo: $("#ImageUpload > img").attr('src')
                };
                var insertLogoparams = {};
                var rst = $scope.data;
                insertLogoparams.titleBlockData = {
                    root: {
                        UserRegBlock: {                          
                            Logo: $("#ImageUpload > img").attr('src'),
                            BackGrdColor: $scope.data.root.UserRegBlock.BackGrdColor,
                            BackGroundImage: $("#UserRegBgImage > img").attr('src'),                          
                            LogoAlign: $scope.data.root.UserRegBlock.LogoAlign
                        }
                    }
                }
                $('#savinguserreglogo').show();
                insertLogoparams.titleID = "1";
                var userReglogoobjects = {};
                userReglogoobjects.UserRegSettings = insertLogoparams;
                userReglogoobjects.AdminSettings = adminSettings;
                userReglogoobjects.IsImageChanged = {};
                userReglogoobjects.IsImageChanged.IslogoChanged = IslogoChanged;
                userReglogoobjects.IsImageChanged.IsBackgroudIMgChanged = IsBackgroudIMgChanged;
                AdminService.UpdateUserRegSettings(userReglogoobjects).then(function (result) {
                    $('#savinguserreglogo').hide();
                    if (result.Response != null || result.StatusCode != 405) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption'));
                        var date = new Date();
                        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                            $.cookie('LogoImage', amazonURL + cloudsetup.BucketName + '/' + TenantFilePath + 'logo.png')
                        }

                        var srcpath = $.cookie('LogoImage');
                        $.cookie('LogoImage', srcpath + '?ts=');
                        $('#pageheadertitle').attr('src', $.cookie('LogoImage') + '?ts=' + date.getTime());
                        $('#loginpageTitle').attr('src', $.cookie('LogoImage') + '?ts=' + date.getTime());
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4360.Caption'));
                    }
                });               
            };
            var imgfileid = '';
            $(function () {
                if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                    var uploader = new plupload.Uploader({
                        runtimes: 'gears,html5,flash,silverlight,browserplus',
                        browse_button: 'pickfiles',
                        container: 'container',
                        max_file_size: '10mb',
                        url: amazonURL + cloudsetup.BucketName,
                        flash_swf_url: 'assets/js/plupload/Moxie.swf',
                        silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                        filters: [{
                            title: "Image files",
                            extensions: "jpg,gif,png"
                        }],
                        resize: {
                            width: 320,
                            height: 240,
                            quality: 90
                        },
                        multipart_params: {
                            'key': '${filename}',
                            'Filename': '${filename}',
                            'acl': 'public-read',
                            'success_action_status': '201',
                            'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                            'policy': cloudsetup.PolicyDocument,
                            'signature': cloudsetup.PolicyDocumentSignature,
                        }
                    });
                    uploader.bind('Init', function (up, params) {
                        $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                    });
                    uploader.init();
                    uploader.bind('FilesAdded', function (up, files) {
                        up.refresh();
                        uploader.start();
                        IslogoChanged = true;
                    });
                    uploader.bind('Error', function (up, err) {
                        $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                        up.refresh();
                    });
                    uploader.bind('FileUploaded', function (up, file, response) {
                        var fileext = "." + file.name.split('.').pop();
                        response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                        $('#UserRegLogoimg').attr('src', amazonURL + cloudsetup.BucketName + '/assets/img/Temp/' + response.response.split(',')[0] + response.response.split(',')[2]);
                    });
                    uploader.bind('BeforeUpload', function (up, file) {
                        $.extend(up.settings.multipart_params, {
                            id: file.id.replace("_", ""),
                            size: file.size
                        });
                        console.log("File upload about to start.");
                        var keyName = file.name;
                        var keySplit = keyName.split('/');
                        var fileName = keySplit[keySplit.Length - 1];
                        var fileext = "." + file.name.split('.').pop();
                        var uniqueKey = ("assets/img/Temp/" + file.id + fileext);
                        uploader.settings.multipart_params.key = uniqueKey;
                        uploader.settings.multipart_params.Filename = uniqueKey;
                    });
                } else {
                    var uploader = new plupload.Uploader({
                        runtimes: 'gears,html5,flash,silverlight,browserplus',
                        browse_button: 'pickfiles',
                        container: 'container',
                        max_file_size: '10mb',
                        url: 'Handlers/UserTitleImageHandler.ashx?Type=Attachment',
                        flash_swf_url: 'assets/js/plupload/Moxie.swf',
                        silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                        filters: [{
                            title: "Image files",
                            extensions: "jpg,gif,png"
                        }],
                        resize: {
                            width: 320,
                            height: 240,
                            quality: 90
                        }
                    });
                    uploader.bind('Init', function (up, params) {
                        $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                    });
                    uploader.init();
                    uploader.bind('FilesAdded', function (up, files) {
                        up.refresh();
                        uploader.start();
                        IslogoChanged = true;
                    });
                    uploader.bind('Error', function (up, err) {
                        $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                        up.refresh();
                    });
                    uploader.bind('FileUploaded', function (up, file, response) {
                        $('#UserRegLogoimg').attr('src', 'assets/img/Temp/' + response.response.split(',')[0]);
                    });
                }
            });
            $scope.ColorCodeGlobal = 'ffffff';
            $scope.ColorOptions = {
                preferredFormat: "hex",
                showInput: true,
                showAlpha: false,
                allowEmpty: true,
                showPalette: true,
                showPaletteOnly: false,
                togglePaletteOnly: true,
                togglePaletteMoreText: 'more',
                togglePaletteLessText: 'less',
                showSelectionPalette: true,
                chooseText: "Choose",
                cancelText: "Cancel",
                showButtons: true,
                clickoutFiresChange: true,
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            };
            $scope.colorchange = function (color) { }
            $(".pick-a-color").pickAColor({
                showSpectrum: true,
                showSavedColors: false,
                saveColorsPerElement: true,
                fadeMenuToggle: true,
                showAdvanced: true,
                showHexInput: true,
                showBasicColors: true
            });
            var imgfileid = '';
            $(function () {
                if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                    var uploader = new plupload.Uploader({
                        runtimes: 'gears,html5,flash,silverlight,browserplus',
                        browse_button: 'BgPickImg',
                        container: 'container',
                        max_file_size: '20000mb',
                        url: amazonURL + cloudsetup.BucketName,
                        flash_swf_url: 'assets/js/plupload/Moxie.swf',
                        silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                        filters: [{
                            title: "Image files",
                            extensions: "jpg,gif,png,jpeg"
                        }],
                        multipart_params: {
                            'key': '${filename}',
                            'Filename': '${filename}',
                            'acl': 'public-read',
                            'success_action_status': '201',
                            'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                            'policy': cloudsetup.PolicyDocument,
                            'signature': cloudsetup.PolicyDocumentSignature,
                        }
                    });
                    uploader.bind('Init', function (up, params) {
                        $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                    });
                    uploader.init();
                    uploader.bind('FilesAdded', function (up, files) {
                        up.refresh();
                        uploader.start();
                        IsBackgroudIMgChanged = true;
                        $('#BackGrdImg').attr('src', 'assets/img/loading.gif');
                    });
                    uploader.bind('Error', function (up, err) {
                        if (err.code == "-601") {
                            bootbox.alert("File format '" + err.file.name.split('.')[1] + "' not support.")
                        }
                        else {
                            bootbox.alert(err.message);
                        }
                        $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                        up.refresh();
                    });
                    uploader.bind('FileUploaded', function (up, file, response) {
                        var fileext = "." + file.name.split('.').pop();
                        response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                        $('#BackGrdImg').attr('src', amazonURL + cloudsetup.BucketName + '/assets/img/Temp/' + response.response.split(',')[0] + response.response.split(',')[2]);
                    });
                    uploader.bind('BeforeUpload', function (up, file) {
                        $.extend(up.settings.multipart_params, {
                            id: file.id.replace("_", ""),
                            size: file.size
                        });
                        console.log("File upload about to start.");
                        var keyName = file.name;
                        var keySplit = keyName.split('/');
                        var fileName = keySplit[keySplit.Length - 1];
                        var fileext = "." + file.name.split('.').pop();
                        var uniqueKey = ("assets/img/Temp/" + file.id + fileext);
                        uploader.settings.multipart_params.key = uniqueKey;
                        uploader.settings.multipart_params.Filename = uniqueKey;
                    });
                } else {
                    var uploader = new plupload.Uploader({
                        runtimes: 'gears,html5,flash,silverlight,browserplus',
                        browse_button: 'BgPickImg',
                        container: 'container',
                        max_file_size: '20000mb',
                        url: 'Handlers/UserTitleImageHandler.ashx?Type=Attachment',
                        flash_swf_url: 'assets/js/plupload/Moxie.swf',
                        silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                        filters: [{
                            title: "Image files",
                            extensions: "jpg,gif,png,jpeg"
                        }]
                    });
                    uploader.bind('Init', function (up, params) {
                        $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
                    });
                    uploader.init();
                    uploader.bind('FilesAdded', function (up, files) {
                        up.refresh();
                        uploader.start();
                        IsBackgroudIMgChanged = true;
                        $('#BackGrdImg').attr('src', 'assets/img/loading.gif');
                    });
                    uploader.bind('Error', function (up, err) {
                        if (err.code == "-601") {
                            bootbox.alert("File format '" + err.file.name.split('.')[1] + "' not support.")
                        }
                        else {
                            bootbox.alert(err.message);
                        }
                        $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                        up.refresh();
                    });
                    uploader.bind('FileUploaded', function (up, file, response) {
                        $('#BackGrdImg').attr('src', 'assets/img/Temp/' + response.response.split(',')[0]);
                    });
                }
            });
            $scope.data = {
                root: {
                    TitleBlock: {
                        Title: "",
                        Logo: "",
                        BackGrdColor: "fff",
                        BackGroundImage: "",
                        LoginPosition: "",
                        LogoAlign: ""
                    }
                }
            };
            $scope.intentOptions = [{
                "id": 1,
                "class": "icon-align-left",
                "Lable": "left"
            }, {
                "id": 2,
                "class": "icon-align-center",
                "Lable": "center"
            }, {
                "id": 3,
                "class": "icon-align-right",
                "Lable": "right"
            }];
            $scope.Delete = function () {
                $('#BackGrdImg').attr('src', '');
            };
        }
        catch (e) {
            console.log(e.message);
        }
    }
    app.controller("mui.admin.userregistrationlogoCtrl", ['$scope', '$timeout', '$resource', '$translate', 'AdminService', muiadminuserregistrationlogoCtrl]);


})(angular, app);