using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserMailSubscription object for table 'CM_UserMailSubscription'.
	/// </summary>
	
	public partial class UserMailSubscriptionDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected DateTimeOffset _lastsenton;
		protected DateTimeOffset _lastupdatedon;
		protected bool _isemailenable;
		protected string _dayname;
        protected TimeSpan _timing;
		protected bool _recapreport;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public UserMailSubscriptionDao() {}

        public UserMailSubscriptionDao(int pUserid, DateTimeOffset pLastSentOn, DateTimeOffset pLastUpdatedOn, bool pIsEmailEnable, string pDayName, TimeSpan pTiming, bool pRecapReport)
		{
			this._userid = pUserid; 
			this._lastsenton = pLastSentOn; 
			this._lastupdatedon = pLastUpdatedOn; 
			this._isemailenable = pIsEmailEnable; 
			this._dayname = pDayName; 
			this._timing = pTiming; 
			this._recapreport = pRecapReport; 
		}
				
		public UserMailSubscriptionDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		public virtual DateTimeOffset LastSentOn
		{
			get { return _lastsenton; }
			set { _bIsChanged |= (_lastsenton != value); _lastsenton = value; }
			
		}
		
		public virtual DateTimeOffset LastUpdatedOn
		{
			get { return _lastupdatedon; }
			set { _bIsChanged |= (_lastupdatedon != value); _lastupdatedon = value; }
			
		}
		
		public virtual bool IsEmailEnable
		{
			get { return _isemailenable; }
			set { _bIsChanged |= (_isemailenable != value); _isemailenable = value; }
			
		}
		
		public virtual string DayName
		{
			get { return _dayname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("DayName", "DayName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_dayname != value); 
			  _dayname = value; 
			}
			
		}

        public virtual TimeSpan Timing
		{
			get { return _timing; }
			set { _bIsChanged |= (_timing != value); _timing = value; }
			
		}
		
		public virtual bool RecapReport
		{
			get { return _recapreport; }
			set { _bIsChanged |= (_recapreport != value); _recapreport = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserMailSubscription = "UserMailSubscription";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string LastSentOn = "LastSentOn";			
			public const string LastUpdatedOn = "LastUpdatedOn";			
			public const string IsEmailEnable = "IsEmailEnable";			
			public const string DayName = "DayName";			
			public const string Timing = "Timing";			
			public const string RecapReport = "RecapReport";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserMailSubscription = "UserMailSubscription";			
			public const string Id = "Id";			
			public const string Userid = "Userid";			
			public const string LastSentOn = "LastSentOn";			
			public const string LastUpdatedOn = "LastUpdatedOn";			
			public const string IsEmailEnable = "IsEmailEnable";			
			public const string DayName = "DayName";			
			public const string Timing = "Timing";			
			public const string RecapReport = "RecapReport";			
		}

		#endregion
		
		
	}
	
}
