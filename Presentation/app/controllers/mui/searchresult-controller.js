﻿(function (ng, app) {
    "use strict";
    function muisearchresultCntrl($scope, $cookies, $resource,$compile, $window, $timeout, $location,$translate, SearchresultService,$modal) {
        $('#searchlist').removeClass('dropdown-menu search').addClass('dropdown-menu searchs');
        // --- Define Controller Methods. ------------------- //
        if ($scope.search != undefined) {
            $scope.searchtext = $scope.search;
        }

        $scope.Pathinfo = {};
        Loadresult($scope.searchtext);
        var searchtext = $scope.searchtext;

        $(window).on('loadsearchresult', function (event, searchvar) {
            Loadresult(searchvar);
        });


        var pageno = 1;
        var typeid = 0;
        var clicktype = '';
        $scope.dyn_Cont = '';
        $scope.dyn_Contresult = '';
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.fieldKeys = [];
        $scope.costcemtreObject = [];

        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }

        $scope.fields = {
        };
        var SearchTerm = {};

        $scope.CountObjects = { "Totalcount": 0, "Production": 0, "Task": 0, "Asset": 0 };

        function Loadresult(searchvar) {
            $(window).AdjustHeightWidth();
            $scope.searchresultdata = [];
            $scope.resultdata = [];
            $scope.resultfortypes = [];
            $scope.entitytypedetails = [];
            $scope.breadcrumresult = [];
            $scope.searchvalues = [];
            //            var SearchResult = $resource('planning/Search', { get: { method: 'GET' } });
            var search = {};
            //            var search = new SearchResult();
            if ($scope.searchtext != "") {
                search.Text = searchvar;
            }
            else {
                search.Text = searchvar;
            }
            search.SearchTerm = SearchTerm;
            search.ETypeID = 0;
            search.PageID = 0;
            search.IsGlobalAdmin = IsGlobalAdmin;
            if ($scope.searchtext != "") {
                $scope.search = searchvar;
            }
            else {
                $scope.search = searchvar;
            }

            pageno = 1;
            SearchresultService.Search(search).then(function (searchresponse) {
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultdata = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                $scope.dyn_Contresult = "";
                $("#resultsearchvaluestask").html(
                         $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesasset").html(
                         $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesproduction").html(
                         $compile($scope.dyn_Contresult)($scope));
                if ($scope.resultfortypesfound.length != 0) {
                    var production = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Production" });
                    if (production != undefined) {

                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Production = production.length;

                        $scope.dyn_Contresult = "";
                        for (var i = 0, val; val = production[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"'" + val.ColorCode + "' !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluesproduction").html(
                                $compile($scope.dyn_Contresult)($scope));

                    }
                    var task = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Task" });
                    if (task != undefined) {
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Task = task.length;
                        $scope.dyn_Contresult = "";
                        for (var i = 0, val; val = task[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"'" + val.ColorCode + "' !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluestask").html($compile($scope.dyn_Contresult)($scope));
                    }

                    var asset = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Asset" });
                    if (asset != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = asset.length;
                        LoadThimbnailView(searchresponse.Response.AssetData);
                    }

                    $.cookie("searchtext", searchvar);
                }
            });
        }



        $scope.Editasset = function (AssetID) {

            /********** Load assetedit page **********/

            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return { AssetID: AssetID, IsLock: false, isNotify: false, viewtype: 'ViewType' };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup popup-widthL',
                backdrop: "static"
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                //console.log('Modal dismissed at: ' + new Date());
            });
            //$timeout(function () {
            //    $scope.$emit('callBackAssetEditinMUI', AssetID, false, false, 'ViewType');
            //}, 10);
        }

        $("#loadtypesfound").click(function (event) {
            typeid = 0;
            $scope.searchvalues = [];
            //          var SearchResult = $resource('planning/Search', { get: { method: 'GET' } });
            var search = {};
            //          var search = new SearchResult();
            search.Text = $scope.searchtext;
            search.SearchTerm = SearchTerm;
            search.ETypeID = 0;
            search.PageID = 1;
            search.IsGlobalAdmin = IsGlobalAdmin;
            if ($scope.searchtext != "") {
                search.Text = $scope.search;
                $scope.search = $scope.search;
            }
            else {
                search.Text = $scope.search;
                $scope.search = $scope.search;
            }
            pageno = 1;
            SearchresultService.Search(search).then(function (searchresponse) {
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultdata = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                $scope.dyn_Contresult = "";
                $("#resultsearchvaluestask").html(
                        $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesasset").html(
                         $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesproduction").html(
                         $compile($scope.dyn_Contresult)($scope));
                if ($scope.resultfortypesfound.length != 0) {

                    var production = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Production" });
                    if (production != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Production = production.length;
                        for (var i = 0, val; val = production[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"'" + val.ColorCode + "' !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluesproduction").html(
                                $compile($scope.dyn_Contresult)($scope));

                    }
                    var task = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Task" });
                    if (task != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Task = production.length;
                        for (var i = 0, val; val = task[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"'" + val.ColorCode + "' !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluestask").html($compile($scope.dyn_Contresult)($scope));
                    }

                    var asset = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Asset" });
                    if (asset != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = asset.length;
                        LoadThimbnailView(searchresponse.Response.AssetData);
                    }


                }
            });
        });

        $('#resultsearchvalues').scroll(function (event) {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                pageno = pageno + 1;
                clicktype = '';
                var search = {};
                search.Text = $scope.searchtext;
                search.SearchTerm = SearchTerm;
                search.ETypeID = typeid;
                search.PageID = pageno;
                search.IsGlobalAdmin = IsGlobalAdmin;
                search.Text = $scope.search;
                SearchresultService.Search(search).then(function (searchresponse) {
                    $scope.resultfortypesfound = [];
                    $scope.array = [];
                    $scope.resultdata = [];
                    $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                    $scope.dyn_Contresult = "";
                    if ($scope.resultfortypesfound.length != 0) {

                        var production = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Production" });
                        if (production != undefined) {
                            $scope.dyn_Contresult = "";
                            $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                            $scope.CountObjects.Task = production.length;
                            for (var i = 0, val; val = production[i++];) {
                                $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                                $scope.dyn_Contresult += "<li >";
                                $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                                $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                                $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                                $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                                $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += " </div>";
                                $scope.dyn_Contresult += "<div class='block'>";
                                $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                                $scope.dyn_Contresult += "<span class='body'>";
                                var CurrentPath = [];

                                $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                    CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                                });
                                var pathHtml = "";
                                $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                                $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                                $scope.dyn_Contresult += '<ul class="breadcrumb">';
                                $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                                $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                                $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                                $scope.dyn_Contresult += '</ul>';
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                                $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                                $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                                $scope.dyn_Contresult += "</span>";
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += " </li>";
                                $scope.dyn_Contresult += "</ul>";
                            }
                            $("#resultsearchvaluesproduction").html(
                                    $compile($scope.dyn_Contresult)($scope));

                        }
                        var task = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Task" });
                        if (task != undefined) {
                            $scope.dyn_Contresult = "";
                            $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                            $scope.CountObjects.Task = task.length;
                            for (var i = 0, val; val = task[i++];) {
                                $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                                $scope.dyn_Contresult += "<li >";
                                $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                                $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                                $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                                $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                                $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += " </div>";
                                $scope.dyn_Contresult += "<div class='block'>";
                                $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                                $scope.dyn_Contresult += "<span class='body'>";
                                var CurrentPath = [];

                                $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                    CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                                });
                                var pathHtml = "";
                                $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                                $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                                $scope.dyn_Contresult += '<ul class="breadcrumb">';
                                $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                                $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                                $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                                $scope.dyn_Contresult += '</ul>';
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                                $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                                $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                                $scope.dyn_Contresult += "</span>";
                                $scope.dyn_Contresult += "</div>";
                                $scope.dyn_Contresult += " </li>";
                                $scope.dyn_Contresult += "</ul>";
                            }
                            $("#resultsearchvaluestask").html($compile($scope.dyn_Contresult)($scope));
                        }

                        var asset = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Asset" });
                        if (asset != undefined) {
                            $scope.dyn_Contresult = "";
                            $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                            $scope.CountObjects.Asset = asset.length;
                            LoadThimbnailView(searchresponse.Response.AssetData);
                        }


                    }
                });
            }
        });

        $("#typesfound").click(function (event) {
            typeid = $(event.target).attr('data-id');
            var search = {};
            if ($scope.searchtext != "") {
                search.Text = $scope.searchtext;
            }
            else {
                search.Text = $scope.search;
                $scope.search = $scope.search;
            }
            pageno = 1;
            search.SearchTerm = SearchTerm;
            search.ETypeID = typeid;
            search.PageID = 1;
            search.IsGlobalAdmin = IsGlobalAdmin;
            SearchresultService.Search(search).then(function (searchresponse) {
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultdata = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                $scope.dyn_Contresult = "";
                $("#resultsearchvaluestask").html(
                           $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesasset").html(
                         $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesproduction").html(
                         $compile($scope.dyn_Contresult)($scope));
                if ($scope.resultfortypesfound.length != 0) {

                    var production = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Production" });
                    if (production != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = production.length;
                        for (var i = 0, val; val = production[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluesproduction").html(
                                $compile($scope.dyn_Contresult)($scope));

                    }
                    var task = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Task" });
                    if (task != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = task.length;
                        for (var i = 0, val; val = task[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluestask").html($compile($scope.dyn_Contresult)($scope));
                    }

                    var asset = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Asset" });
                    if (asset != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = asset.length;
                        LoadThimbnailView(searchresponse.Response.AssetData);
                    }


                }
            });

            $scope.searchtext = "";
        });
        $scope.funperformsearch = function (value) {
            clicktype = '';
            var search = {};
            search.Text = $scope.searchtext;
            search.SearchTerm = SearchTerm;
            search.ETypeID = value;
            search.PageID = 1;
            search.IsGlobalAdmin = IsGlobalAdmin;
            $scope.search = $scope.searchtext;
            pageno = 1;
            SearchresultService.Search(search).then(function (searchresponse) {
                $scope.resultfortypesfound = [];
                $scope.array = [];
                $scope.resultdata = [];
                $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                $scope.dyn_Contresult = "";
                $("#resultsearchvaluestask").html(
                           $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesasset").html(
                         $compile($scope.dyn_Contresult)($scope));
                $("#resultsearchvaluesproduction").html(
                         $compile($scope.dyn_Contresult)($scope));
                if ($scope.resultfortypesfound.length != 0) {

                    var production = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Production" });
                    if (production != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = production.length;
                        for (var i = 0, val; val = production[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluesproduction").html(
                                $compile($scope.dyn_Contresult)($scope));

                    }
                    var task = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Task" });
                    if (task != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = task.length;
                        for (var i = 0, val; val = task[i++];) {
                            $scope.dyn_Contresult += "<ul id='loadactivity' class='searchcontent'>";
                            $scope.dyn_Contresult += "<li >";
                            $scope.dyn_Contresult += "<span ng-if=\"" + val.ColorCode + " !='' && '" + val.ThumbnailUrl + "' != 'DAM'\" class='icon color-white' style='background-color: #" + val.ColorCode + "'>" + val.ShortDescription + "</span>";
                            $scope.dyn_Contresult += "<div ng-if=\"'" + val.ThumbnailUrl + "' == 'DAM'\" class=\"searchResult-imgPrev\">";
                            $scope.dyn_Contresult += "<div class=\"sr-ip-container\">";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' == 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/Original/" + val.Path + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "<img ng-if=\"'" + val.Extension + "' != 'jpg'\" src=\"" + TenantFilePath + "DAMFiles/StaticPreview_small/" + val.Extension.toUpperCase() + ".jpg\" alt=\"thumbnail\" />";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </div>";
                            $scope.dyn_Contresult += "<div class='block'>";
                            $scope.dyn_Contresult += "<span class='name'>" + val.Name + "</span>";
                            $scope.dyn_Contresult += "<span class='body'>";
                            var CurrentPath = [];

                            $($.parseXML(val.pathInfo[0].pathinfo)).find('p').each(function () {
                                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p') });
                            });
                            var pathHtml = "";
                            $scope.Pathinfo["Path_" + val.ID] = CurrentPath;
                            $scope.dyn_Contresult += "<div id='div_path_" + val.ID + "'>";
                            $scope.dyn_Contresult += '<ul class="breadcrumb">';
                            $scope.dyn_Contresult += '<li class="active" ng-repeat="EntityPath in Pathinfo.Path_' + val.ID + '">';
                            $scope.dyn_Contresult += '<a  ng-click="IsActiveEntity(EntityPath.ID,EntityPath.TypeID,EntityPath.ParentID)" href="JavaScript: void(0);">{{DecodedTaskName(EntityPath.Name)}} </a>';
                            $scope.dyn_Contresult += '<span  ng-if="$index < (Pathinfo.Path_' + val.ID + '.length-1)">/</span></li>';
                            $scope.dyn_Contresult += '</ul>';
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += "<div><b>Description:</b> " + val.Description + "</div>";
                            $scope.dyn_Contresult += "<div><b>Type:</b> " + val.Caption + "</div>";
                            $scope.dyn_Contresult += "<div><b>Status:</b>" + val.Status + "</div>";
                            $scope.dyn_Contresult += "</span>";
                            $scope.dyn_Contresult += "</div>";
                            $scope.dyn_Contresult += " </li>";
                            $scope.dyn_Contresult += "</ul>";
                        }
                        $("#resultsearchvaluestask").html($compile($scope.dyn_Contresult)($scope));
                    }

                    var asset = $.grep($scope.resultfortypesfound, function (rel) { return rel.Searchentitytype == "Asset" });
                    if (asset != undefined) {
                        $scope.dyn_Contresult = "";
                        $scope.CountObjects.Totalcount = searchresponse.Response.SearchResultEntity.length;
                        $scope.CountObjects.Asset = asset.length;
                        LoadThimbnailView(searchresponse.Response.AssetData);

                    }


                }
            });
        }

        //Returns Decoded Name
        $scope.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            }
            else {
                return "";
            }
        };

        $scope.AssetSearchObj = { "AssetSearchfiles": [], "AssetDynamicData": [], "ThumbnailSettings": [] };

        function LoadThimbnailView(res) {
            var html = '';
            $("#resultsearchvaluesasset").html(
                                  $compile(html)($scope));

            if (res != null) {
                var assets = [];
                assets = res[0].AssetFiles;

                $scope.AssetSearchObj.AssetSearchfiles = asset;
                var DynamicData = res[0].AssetDynData;
                $scope.AssetSearchObj.AssetDynamicData = DynamicData;
                $scope.AssetSearchObj.ThumbnailSettings = res[0].thumbnailxmlsettings;
                for (var i = 0, asset; asset = assets[i++];) {
                    html += '<div class="th-Box"  data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                    html += '<div id="select_' + i + '"  class="th-Selection">';
                    html += '<div class="th-Block" >';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.Extension.toLowerCase() == '.jpg' || asset.Extension.toLowerCase() == '.jpeg' || asset.Extension.toLowerCase() == '.png' || asset.Extension.toLowerCase() == '.psd' || asset.Extension.toLowerCase() == '.bmp' || asset.Extension.toLowerCase() == '.tif' || asset.Extension.toLowerCase() == '.tiff' || asset.Extension.toLowerCase() == '.gif' || asset.Extension.toLowerCase() == '.emf' || asset.Extension.toLowerCase() == '.eps' || asset.Extension.toLowerCase() == '.pdf') {
                                if (asset.Status == 2)
                                    html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                else if (asset.Status == 1 || asset.Status == 0)
                                    html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                    html += '<div class="th-Error-preview" >';
                                    html += '<span class="errorMsg"><i class="icon-info-sign"></i>';
                                    html += '<span class="errorTxt">Unable to generate Preview</span></span>';
                                    html += '</div>';
                                }
                            }
                            else {
                                html += '<img src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        }
                        else
                            html += '<img src="' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }

                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-ok displayNone" title="Approved and Combined with ..."></i>';
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $("#resultsearchvaluesasset").append(
                                $compile(html)($scope));
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.AssetSearchObj.ThumbnailSettings, function (e) {
                return e.assetType == asset.AssetTypeid;
            });

            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {

                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined)
                                html += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }

        $timeout(function () { CheckPreviewGenerator() }, 5000);
        function CheckPreviewGenerator() {
            getPreviewIDs();
        }
        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                var data = {};
                data.AssetIDs = assetids;
                SearchresultService.CheckPreviewGeneratorID(data)
                .then(function (result) {
                    var generatedids = result.Response.split(',');
                    for (var j = 0; j < generatedids.length; j++) {
                        if (generatedids[j] != "") {
                            var src = $('#' + generatedids[j]).attr('data-src');
                            //var extn = $('#' + generatedids[j]).attr('data-extn');
                            try {
                                var upd = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) { return e.ActiveFileID == generatedids[j] });
                                if (upd.length > 0) {
                                    $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) { return e.ActiveFileID == generatedids[j] })[0].Status = 2;
                                }
                            } catch (e) {

                            }
                            var extn = '.jpg';
                            if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                if (GlobalAssetEditID == generatedids[j]) {
                                    $('#asseteditimage').attr('src', TenantFilePath + 'DAMFiles/Preview/Big_' + src + '' + extn);
                                }
                            }
                            $('#' + generatedids[j]).removeClass('loadingImg');
                            $('#' + generatedids[j]).attr('src', TenantFilePath + 'DAMFiles/Preview/Small_' + src + '' + extn);
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () { CheckPreviewGenerator() }, 3000);
        }

        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel(PreviewGeneratorTimer);
        });

        $scope.IsActiveEntity = function (EntityID, TypeId, parentId) {

            if (TypeId == 2 || TypeId == 3 || TypeId == 31 || TypeId == 30) {
                var entityid = parentId;
                var taskid = EntityID;
                var typeid = TypeId;

                SearchresultService.IsActiveEntity(taskid).then(function (res) {
                    if (res.Response == true) {
                        $("#loadNotificationtask").modal("show");
                        $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, typeid, entityid]);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });


            }

            else {
                SearchresultService.IsActiveEntity(EntityID).then(function (res) {
                    if (res.Response == true) {
                        if (TypeId == 5) {
                            $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID+'/overview');
                        }
                        else if (TypeId == 10) {
                            $location.path('/mui/objective/detail/section/' + EntityID + '/overview');
                        }
                        else {
                            $location.path('/mui/planningtool/default/detail/section/' + EntityID + '/overview');
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                    }
                });
            }
        }


        $(".searchcontent").click(function (event) {

            var targetControl = $(event.target);
            var id;
            var typeid;
            var parentid;
            if (targetControl[0].tagName == "SPAN") {
                id = targetControl.parents('a').attr('data-id');
                typeid = targetControl.parents('a').attr('data-typeid');
                parentid = targetControl.parents('a').attr('data-ParentID');
            }
            else if (targetControl[0].tagName == "A") {
                id = targetControl.attr('data-id');
                typeid = targetControl.attr('data-typeid');
                parentid = targetControl.attr('data-ParentID');
            }

            if (id == undefined) {
                var localScope = $(event.target).scope();
                // Invoke the expression in the local scope
                // context to make sure we adhere to the
                // proper scope chain prototypal inheritance.
                localScope.$apply(
                function () {
                    if ($('.searchresultAvailability').length > 0) {
                        $(window).trigger('loadsearchresult')
                    } else {
                        $location.path('/mui/searchresult/');
                    }
                });
            }
            else {
                EntityTypeID = typeid;
                SearchEntityID = id;
                if (Thumbnailurl == "DAM" || Thumbnailurl == undefined) {
                    $scope.$emit('callBackAssetEditinMUI', id, false, true, 'ViewType');
                } else {
                    if (typeid == 6) {
                        $location.path('/mui/planningtool/default/detail/section/' + id + '/overview');
                    }
                    else if (typeid == 5) {
                        $location.path('/mui/planningtool/costcentre/detail/section/' + id + '/overview');
                    }
                    else if (typeid == 10) {
                        $location.path('/mui/objective/detail/section/' + id + '/overview');
                    }
                    else if (typeid == 2 || typeid == 3 || typeid == 31 || typeid == 30) {
                        $scope.TaskID = id;
                        $scope.EntityTypeID = typeid;
                        $scope.EntityID = parentid;
                        $location.path('/mui/mytask');
                    }
                    else if (typeid == 4) {
                    }
                    else {
                        $location.path('/mui/planningtool/default/detail/section/' + id + '/overview');
                    }
                }
                $scope.searchtext = "";
            }
            $('#searchlist').removeClass('dropdown-menu search').addClass('dropdown-menu searchs');
        });


        $scope.RedirectCustomSearch = function (type) {
            $scope.SearchObjtxt.SearchType = type;
            $location.path('/mui/customsearchresult/');
        }

        // Get the current year for copyright output.
        $scope.copyrightYear = (new Date()).getFullYear();

        // --- Bind To Scope Events. ------------------------ //

     
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.searchresultCntrl']"));
        });
      
    }
    app.controller("mui.searchresultCntrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$timeout', '$location', '$translate', 'SearchresultService','$modal', muisearchresultCntrl]);

    function SearchresultService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            Search: Search,
            IsActiveEntity: IsActiveEntity,
            CheckPreviewGeneratorID: CheckPreviewGeneratorID
        });
        function Search(formobj) { var request = $http({ method: "post", url: "api/Planning/Search/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function CheckPreviewGeneratorID(Assetids) { var request = $http({ method: "post", ignoreLoadingBar: true, url: "api/dam/CheckPreviewGenerator/", params: { action: "add" }, data: { AssetIDs: Assetids } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        } function handleSuccess(response) { return (response.data); }
    } app.service("SearchresultService", ['$http', '$q', SearchresultService]);

})(angular, app);